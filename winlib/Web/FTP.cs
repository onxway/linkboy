﻿
using System;
using System.IO;
using System.Net;
using n_OS;

namespace n_Web
{

public static class FTP
{
	//初始化
	public static void Init()
	{
		
	}
	
	public static string UploadFile(string file, string ftpServerIP, string ftpUserName, string ftpPassword)
	{
		FileInfo fileInf = new FileInfo(file);
		
		string uri = ftpServerIP + @"/" + fileInf.Name;
		FtpWebRequest reqFtp = (FtpWebRequest)FtpWebRequest.Create(new Uri(uri));
		reqFtp.Credentials = new NetworkCredential(ftpUserName, ftpPassword);
		
		reqFtp.Method = WebRequestMethods.Ftp.UploadFile;
		
		reqFtp.UsePassive = true;
		reqFtp.UseBinary = true;
		int buffLength = 2048;
		byte[] buff = new byte[buffLength];
		int contentLen = 0;
		FileStream fs = fileInf.OpenRead();
		try
		{
			Stream strm = reqFtp.GetRequestStream();
			contentLen = fs.Read(buff, 0, buffLength);
			while (contentLen != 0) {
				strm.Write(buff, 0, contentLen);
				contentLen = fs.Read(buff, 0, buffLength);
			}
			strm.Close();
			fs.Close();
		}
		catch (Exception ex)
		{
			fs.Close();
			return ex.ToString();
		}
		return null;
	}
}
//FTP
public static class Downloader
{
	public delegate void D_Progress( int i );
	public static D_Progress Progress;
	
	public static string  Download()
	{
		
		string filePath = OS.SystemRoot + "Resource" + OS.PATH_S + "temp" + OS.PATH_S;
		string userId = "byu2623270001";
		string pwd = "net19870304";
		
		string ftpPath = "FTP://byu2623270001.my3w.com/htdocs/download/";
		string fileName = "newversion.txt";
		
		string sRet = null;
		FileStream outputStream = null;
		Stream ftpStream = null;
		FtpWebResponse response = null;
		try
		{
			Log( "开始" );
			
			outputStream = new FileStream(filePath + fileName, FileMode.Create);
			FtpWebRequest reqFTP = (FtpWebRequest)FtpWebRequest.Create(new Uri(ftpPath + fileName));
			reqFTP.Method = WebRequestMethods.Ftp.DownloadFile;
			
			reqFTP.UseBinary = true;
			reqFTP.UsePassive = true;
			
			reqFTP.Credentials = new NetworkCredential(userId, pwd);
			
			response = (FtpWebResponse)reqFTP.GetResponse();
			
			ftpStream = response.GetResponseStream();
			
			long cl = response.ContentLength;
			
			int bufferSize = 2048;
			int readCount;
			byte[] buffer = new byte[bufferSize];
			
			//Log( "长度:" + ftpStream.Length.ToString() );
			
			while( true ) {
				readCount = ftpStream.Read(buffer, 0, bufferSize);
				if( readCount <= 0 ) {
					break;
				}
				outputStream.Write(buffer, 0, readCount);
			}
			
			
			outputStream.Close();
			response.Close();
			
			ftpStream.Close();
		}
		catch (Exception ex)
		{
			if( outputStream != null ) {
				try {
					outputStream.Close();
				}
				catch {}
			}
			if( ftpStream != null ) {
				try {
					ftpStream.Close();
				}
				catch {}
			}
			if( response != null ) {
				try {
					response.Close();
				}
				catch {}
			}
			sRet = ex.Message;
		}
		return sRet;
		
		
		
		//return null;
	}
	
	static void Log( string mes )
	{
		//System.Windows.Forms.MessageBox.Show( mes );
	}
}
//HTTP下载
public static class HTTP
{
	public static bool HttpDownloadFile(string url, string path)
	{
		//家里电脑
		//不加这一句会报错 System.Net.WebException: 请求被中止: 未能创建 SSL/TLS 安全通道。
		//访问linkboy网址不用加, 但是访问其他网址可能需要加
		//System.Net.ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12; //加上这一句
		
		//单位电脑
		System.Net.ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls; //加上这一句
		
		
		// 设置参数
		HttpWebRequest request = WebRequest.Create(url) as HttpWebRequest;
		
		//发送请求并获取相应回应数据
		HttpWebResponse response = request.GetResponse() as HttpWebResponse;
		
		//直到request.GetResponse()程序才开始向目标网页发送Post请求
		Stream responseStream = response.GetResponseStream();
		
		//创建本地文件写入流
		Stream stream = new FileStream(path, FileMode.Create);
		byte[] bArr = new byte[1024];
		int size = responseStream.Read(bArr, 0, (int)bArr.Length);
		
		while (size > 0) {
			stream.Write(bArr, 0, size);
			size = responseStream.Read(bArr, 0, (int)bArr.Length);
		}
		
		stream.Close();
		responseStream.Close();
		
		return true;
	}
}
}



