﻿
using System;
using System.Drawing;
using System.Windows.Forms;
using System.IO;
using System.Diagnostics;

namespace n_PyLoaderForm
{
	public partial class PyLoaderForm : Form
	{
		string Code;
		
		//构造函数
		public PyLoaderForm()
		{
			InitializeComponent();
			
			this.Visible = false;
		}
		
		//运行窗体
		public void Run( string text, string ctext )
		{
			if( Visible ) {
				Visible = false;
				return;
			}
			
			label1.Text = "";
			
			Code = text;
			
			richTextBox1.Text = Code.Replace( "<<sys_tick>>", trackBar1.Value.ToString() );
			richTextBox2.Text = ctext;
			
			try {
				richTextBox1.Font = new Font( "Consolas", 11 );
				richTextBox1.SelectAll();
				richTextBox1.SelectionFont = new Font( "Consolas", 11 );
				richTextBox1.SelectionLength = 0;
			}
			catch {
				
			}
			
			//if( checkBoxHide.Checked ) {
			//	ButtonOkClick( null, null );
			//}
			//else {
				//注意,直接设置Visible不能使窗体获得焦点
				this.Visible = true;
			//}
			while( this.Visible ) {
				System.Windows.Forms.Application.DoEvents();
			}
		}
		
		//窗口关闭
		void AVRdudeSetFormFormClosing(object sender, FormClosingEventArgs e)
		{
			this.Visible = false;
			e.Cancel = true;
		}
		
		void ButtonOkClick(object sender, EventArgs e)
		{
			System.Windows.Forms.Clipboard.SetText( richTextBox1.Text );
			
			//this.Visible = false;
			
			label1.Text = "已复制到剪贴板，可在mPythonX软件中粘贴此代码并点击“刷入掌控”按钮";
		}
		
		void TrackBar1Scroll(object sender, EventArgs e)
		{
			richTextBox1.Text = Code.Replace( "<<sys_tick>>", trackBar1.Value.ToString() );
		}
	}
}

