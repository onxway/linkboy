﻿/*
 * 由SharpDevelop创建。
 * 用户： dell
 * 日期: 2015/11/15
 * 时间: 8:29
 * 
 * 要改变这种模板请点击 工具|选项|代码编写|编辑标准头文件
 */
namespace n_VexLoaderForm_temp
{
	partial class VexLoaderForm
	{
		/// <summary>
		/// Designer variable used to keep track of non-visual components.
		/// </summary>
		private System.ComponentModel.IContainer components = null;
		private System.Windows.Forms.ComboBox comboBoxPort;
		private System.Windows.Forms.Label label1;
		private System.Windows.Forms.Button buttonOk;
		
		/// <summary>
		/// Disposes resources used by the form.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing) {
				if (components != null) {
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}
		
		/// <summary>
		/// This method is required for Windows Forms designer support.
		/// Do not change the method contents inside the source code editor. The Forms designer might
		/// not be able to load this method if it was changed manually.
		/// </summary>
		private void InitializeComponent()
		{
			this.components = new System.ComponentModel.Container();
			this.comboBoxPort = new System.Windows.Forms.ComboBox();
			this.label1 = new System.Windows.Forms.Label();
			this.buttonOk = new System.Windows.Forms.Button();
			this.checkBoxHide = new System.Windows.Forms.CheckBox();
			this.richTextBox1 = new System.Windows.Forms.RichTextBox();
			this.richTextBox2 = new System.Windows.Forms.RichTextBox();
			this.checkBoxDTR_RTS = new System.Windows.Forms.CheckBox();
			this.checkBoxHoldingPort = new System.Windows.Forms.CheckBox();
			this.comboBox波特率 = new System.Windows.Forms.ComboBox();
			this.label2 = new System.Windows.Forms.Label();
			this.timer1 = new System.Windows.Forms.Timer(this.components);
			this.panel1 = new System.Windows.Forms.Panel();
			this.buttonVersion = new System.Windows.Forms.Button();
			this.labelBoardMes = new System.Windows.Forms.Label();
			this.labelVersion = new System.Windows.Forms.Label();
			this.buttonOpenVEX = new System.Windows.Forms.Button();
			this.label4 = new System.Windows.Forms.Label();
			this.label3 = new System.Windows.Forms.Label();
			this.labelMes = new System.Windows.Forms.Label();
			this.buttonRead = new System.Windows.Forms.Button();
			this.buttonDriver = new System.Windows.Forms.Button();
			this.label8 = new System.Windows.Forms.Label();
			this.labelVersionList = new System.Windows.Forms.Label();
			this.panel1.SuspendLayout();
			this.SuspendLayout();
			// 
			// comboBoxPort
			// 
			this.comboBoxPort.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
			this.comboBoxPort.Font = new System.Drawing.Font("微软雅黑", 10F);
			this.comboBoxPort.ForeColor = System.Drawing.Color.Blue;
			this.comboBoxPort.FormattingEnabled = true;
			this.comboBoxPort.Location = new System.Drawing.Point(91, 9);
			this.comboBoxPort.Name = "comboBoxPort";
			this.comboBoxPort.Size = new System.Drawing.Size(438, 27);
			this.comboBoxPort.TabIndex = 0;
			this.comboBoxPort.Click += new System.EventHandler(this.ComboBoxPortClick);
			// 
			// label1
			// 
			this.label1.Font = new System.Drawing.Font("微软雅黑", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
			this.label1.ForeColor = System.Drawing.Color.Black;
			this.label1.Location = new System.Drawing.Point(12, 9);
			this.label1.Name = "label1";
			this.label1.Size = new System.Drawing.Size(73, 29);
			this.label1.TabIndex = 2;
			this.label1.Text = "串口号";
			this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// buttonOk
			// 
			this.buttonOk.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(128)))), ((int)(((byte)(255)))));
			this.buttonOk.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
			this.buttonOk.Font = new System.Drawing.Font("微软雅黑", 21.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
			this.buttonOk.ForeColor = System.Drawing.Color.White;
			this.buttonOk.Location = new System.Drawing.Point(12, 81);
			this.buttonOk.Name = "buttonOk";
			this.buttonOk.Size = new System.Drawing.Size(517, 62);
			this.buttonOk.TabIndex = 6;
			this.buttonOk.Text = "开始下载";
			this.buttonOk.UseVisualStyleBackColor = false;
			this.buttonOk.Click += new System.EventHandler(this.ButtonOkClick);
			this.buttonOk.MouseDown += new System.Windows.Forms.MouseEventHandler(this.ButtonOkMouseDown);
			// 
			// checkBoxHide
			// 
			this.checkBoxHide.Checked = true;
			this.checkBoxHide.CheckState = System.Windows.Forms.CheckState.Checked;
			this.checkBoxHide.Font = new System.Drawing.Font("微软雅黑", 10.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
			this.checkBoxHide.ForeColor = System.Drawing.Color.Gray;
			this.checkBoxHide.Location = new System.Drawing.Point(324, 46);
			this.checkBoxHide.Name = "checkBoxHide";
			this.checkBoxHide.Size = new System.Drawing.Size(205, 29);
			this.checkBoxHide.TabIndex = 10;
			this.checkBoxHide.Text = "不再出现对话框，直接下载";
			this.checkBoxHide.UseVisualStyleBackColor = true;
			// 
			// richTextBox1
			// 
			this.richTextBox1.BackColor = System.Drawing.Color.Gainsboro;
			this.richTextBox1.BorderStyle = System.Windows.Forms.BorderStyle.None;
			this.richTextBox1.Location = new System.Drawing.Point(1001, 408);
			this.richTextBox1.Name = "richTextBox1";
			this.richTextBox1.Size = new System.Drawing.Size(517, 242);
			this.richTextBox1.TabIndex = 12;
			this.richTextBox1.Text = "";
			// 
			// richTextBox2
			// 
			this.richTextBox2.BackColor = System.Drawing.Color.Gainsboro;
			this.richTextBox2.BorderStyle = System.Windows.Forms.BorderStyle.None;
			this.richTextBox2.Location = new System.Drawing.Point(1001, 17);
			this.richTextBox2.Name = "richTextBox2";
			this.richTextBox2.Size = new System.Drawing.Size(179, 293);
			this.richTextBox2.TabIndex = 13;
			this.richTextBox2.Text = "";
			// 
			// checkBoxDTR_RTS
			// 
			this.checkBoxDTR_RTS.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
			this.checkBoxDTR_RTS.ForeColor = System.Drawing.Color.Gray;
			this.checkBoxDTR_RTS.Location = new System.Drawing.Point(280, 375);
			this.checkBoxDTR_RTS.Name = "checkBoxDTR_RTS";
			this.checkBoxDTR_RTS.Size = new System.Drawing.Size(104, 24);
			this.checkBoxDTR_RTS.TabIndex = 15;
			this.checkBoxDTR_RTS.Text = "打开DTR/RTS";
			this.checkBoxDTR_RTS.UseVisualStyleBackColor = true;
			// 
			// checkBoxHoldingPort
			// 
			this.checkBoxHoldingPort.Font = new System.Drawing.Font("微软雅黑", 9F);
			this.checkBoxHoldingPort.ForeColor = System.Drawing.Color.Gray;
			this.checkBoxHoldingPort.Location = new System.Drawing.Point(1001, 372);
			this.checkBoxHoldingPort.Name = "checkBoxHoldingPort";
			this.checkBoxHoldingPort.Size = new System.Drawing.Size(173, 30);
			this.checkBoxHoldingPort.TabIndex = 16;
			this.checkBoxHoldingPort.Text = "下载后保持串口打开状态";
			this.checkBoxHoldingPort.UseVisualStyleBackColor = true;
			// 
			// comboBox波特率
			// 
			this.comboBox波特率.Font = new System.Drawing.Font("微软雅黑", 11F);
			this.comboBox波特率.ForeColor = System.Drawing.Color.DarkGray;
			this.comboBox波特率.FormattingEnabled = true;
			this.comboBox波特率.Items.AddRange(new object[] {
									"1200",
									"2400",
									"4800",
									"9600",
									"19200",
									"38400",
									"57600",
									"76800",
									"115200"});
			this.comboBox波特率.Location = new System.Drawing.Point(91, 44);
			this.comboBox波特率.Name = "comboBox波特率";
			this.comboBox波特率.Size = new System.Drawing.Size(116, 28);
			this.comboBox波特率.TabIndex = 17;
			// 
			// label2
			// 
			this.label2.Font = new System.Drawing.Font("微软雅黑", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
			this.label2.ForeColor = System.Drawing.Color.DarkGray;
			this.label2.Location = new System.Drawing.Point(12, 42);
			this.label2.Name = "label2";
			this.label2.Size = new System.Drawing.Size(73, 29);
			this.label2.TabIndex = 22;
			this.label2.Text = "波特率";
			this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// timer1
			// 
			this.timer1.Tick += new System.EventHandler(this.Timer1Tick);
			// 
			// panel1
			// 
			this.panel1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(213)))), ((int)(((byte)(216)))), ((int)(((byte)(223)))));
			this.panel1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
			this.panel1.Controls.Add(this.buttonVersion);
			this.panel1.Controls.Add(this.labelBoardMes);
			this.panel1.Controls.Add(this.labelVersion);
			this.panel1.Controls.Add(this.buttonOpenVEX);
			this.panel1.Controls.Add(this.label4);
			this.panel1.Controls.Add(this.label3);
			this.panel1.Location = new System.Drawing.Point(13, 196);
			this.panel1.Name = "panel1";
			this.panel1.Size = new System.Drawing.Size(517, 143);
			this.panel1.TabIndex = 23;
			// 
			// buttonVersion
			// 
			this.buttonVersion.BackColor = System.Drawing.Color.LightSlateGray;
			this.buttonVersion.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
			this.buttonVersion.Font = new System.Drawing.Font("微软雅黑", 12F);
			this.buttonVersion.ForeColor = System.Drawing.Color.White;
			this.buttonVersion.Location = new System.Drawing.Point(331, 55);
			this.buttonVersion.Name = "buttonVersion";
			this.buttonVersion.Size = new System.Drawing.Size(172, 35);
			this.buttonVersion.TabIndex = 30;
			this.buttonVersion.Text = "vos版本历史";
			this.buttonVersion.UseVisualStyleBackColor = false;
			this.buttonVersion.Click += new System.EventHandler(this.ButtonVersionClick);
			// 
			// labelBoardMes
			// 
			this.labelBoardMes.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(202)))), ((int)(((byte)(208)))));
			this.labelBoardMes.Font = new System.Drawing.Font("微软雅黑", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
			this.labelBoardMes.ForeColor = System.Drawing.Color.DarkOrchid;
			this.labelBoardMes.Location = new System.Drawing.Point(8, 55);
			this.labelBoardMes.Name = "labelBoardMes";
			this.labelBoardMes.Size = new System.Drawing.Size(317, 34);
			this.labelBoardMes.TabIndex = 26;
			this.labelBoardMes.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
			// 
			// labelVersion
			// 
			this.labelVersion.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(202)))), ((int)(((byte)(208)))));
			this.labelVersion.Font = new System.Drawing.Font("微软雅黑", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
			this.labelVersion.ForeColor = System.Drawing.Color.DarkOrchid;
			this.labelVersion.Location = new System.Drawing.Point(88, 12);
			this.labelVersion.Name = "labelVersion";
			this.labelVersion.Size = new System.Drawing.Size(237, 34);
			this.labelVersion.TabIndex = 0;
			this.labelVersion.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
			// 
			// buttonOpenVEX
			// 
			this.buttonOpenVEX.BackColor = System.Drawing.Color.LightSlateGray;
			this.buttonOpenVEX.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
			this.buttonOpenVEX.Font = new System.Drawing.Font("微软雅黑", 11.25F);
			this.buttonOpenVEX.ForeColor = System.Drawing.Color.White;
			this.buttonOpenVEX.Location = new System.Drawing.Point(331, 11);
			this.buttonOpenVEX.Name = "buttonOpenVEX";
			this.buttonOpenVEX.Size = new System.Drawing.Size(172, 36);
			this.buttonOpenVEX.TabIndex = 24;
			this.buttonOpenVEX.Text = "打开 vos 引擎目录";
			this.buttonOpenVEX.UseVisualStyleBackColor = false;
			this.buttonOpenVEX.Click += new System.EventHandler(this.ButtonOpenVEXClick);
			// 
			// label4
			// 
			this.label4.Font = new System.Drawing.Font("微软雅黑", 10F);
			this.label4.ForeColor = System.Drawing.Color.SlateGray;
			this.label4.Location = new System.Drawing.Point(8, 89);
			this.label4.Name = "label4";
			this.label4.Size = new System.Drawing.Size(495, 52);
			this.label4.TabIndex = 25;
			this.label4.Text = "vos 是linkboy生态下，面向嵌入式的物联网操作系统，RISC-V、ARM等架构的处理器安装相应的 vos 引擎后，即可支持linkboy用户程序的运行。";
			this.label4.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// label3
			// 
			this.label3.Font = new System.Drawing.Font("微软雅黑", 11.25F, System.Drawing.FontStyle.Bold);
			this.label3.ForeColor = System.Drawing.Color.SlateGray;
			this.label3.Location = new System.Drawing.Point(3, 12);
			this.label3.Name = "label3";
			this.label3.Size = new System.Drawing.Size(108, 34);
			this.label3.TabIndex = 24;
			this.label3.Text = "vos 版本：";
			this.label3.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// labelMes
			// 
			this.labelMes.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
									| System.Windows.Forms.AnchorStyles.Right)));
			this.labelMes.Font = new System.Drawing.Font("微软雅黑", 10F);
			this.labelMes.ForeColor = System.Drawing.Color.DarkRed;
			this.labelMes.Location = new System.Drawing.Point(13, 346);
			this.labelMes.Name = "labelMes";
			this.labelMes.Size = new System.Drawing.Size(767, 61);
			this.labelMes.TabIndex = 26;
			this.labelMes.TextAlign = System.Drawing.ContentAlignment.TopCenter;
			// 
			// buttonRead
			// 
			this.buttonRead.BackColor = System.Drawing.Color.LightSlateGray;
			this.buttonRead.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
			this.buttonRead.Font = new System.Drawing.Font("微软雅黑", 12F);
			this.buttonRead.ForeColor = System.Drawing.Color.White;
			this.buttonRead.Location = new System.Drawing.Point(1001, 331);
			this.buttonRead.Name = "buttonRead";
			this.buttonRead.Size = new System.Drawing.Size(111, 35);
			this.buttonRead.TabIndex = 26;
			this.buttonRead.Text = "读取";
			this.buttonRead.UseVisualStyleBackColor = false;
			this.buttonRead.Click += new System.EventHandler(this.ButtonReadClick);
			// 
			// buttonDriver
			// 
			this.buttonDriver.BackColor = System.Drawing.Color.WhiteSmoke;
			this.buttonDriver.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
			this.buttonDriver.Font = new System.Drawing.Font("微软雅黑", 12F);
			this.buttonDriver.ForeColor = System.Drawing.Color.DimGray;
			this.buttonDriver.Location = new System.Drawing.Point(345, 149);
			this.buttonDriver.Name = "buttonDriver";
			this.buttonDriver.Size = new System.Drawing.Size(172, 35);
			this.buttonDriver.TabIndex = 28;
			this.buttonDriver.Text = "打开自带驱动目录";
			this.buttonDriver.UseVisualStyleBackColor = false;
			this.buttonDriver.Click += new System.EventHandler(this.ButtonDriverClick);
			// 
			// label8
			// 
			this.label8.Font = new System.Drawing.Font("微软雅黑", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
			this.label8.ForeColor = System.Drawing.Color.LightSlateGray;
			this.label8.Location = new System.Drawing.Point(12, 152);
			this.label8.Name = "label8";
			this.label8.Size = new System.Drawing.Size(450, 28);
			this.label8.TabIndex = 27;
			this.label8.Text = "如没有串口号，请先安装相应驱动：";
			this.label8.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// labelVersionList
			// 
			this.labelVersionList.Font = new System.Drawing.Font("微软雅黑", 10F);
			this.labelVersionList.ForeColor = System.Drawing.Color.SlateGray;
			this.labelVersionList.Location = new System.Drawing.Point(536, 9);
			this.labelVersionList.Name = "labelVersionList";
			this.labelVersionList.Size = new System.Drawing.Size(219, 337);
			this.labelVersionList.TabIndex = 29;
			this.labelVersionList.TextAlign = System.Drawing.ContentAlignment.TopCenter;
			// 
			// VexLoaderForm
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.BackColor = System.Drawing.Color.WhiteSmoke;
			this.ClientSize = new System.Drawing.Size(767, 397);
			this.Controls.Add(this.labelVersionList);
			this.Controls.Add(this.buttonDriver);
			this.Controls.Add(this.label8);
			this.Controls.Add(this.buttonRead);
			this.Controls.Add(this.labelMes);
			this.Controls.Add(this.panel1);
			this.Controls.Add(this.label2);
			this.Controls.Add(this.comboBox波特率);
			this.Controls.Add(this.buttonOk);
			this.Controls.Add(this.checkBoxHoldingPort);
			this.Controls.Add(this.checkBoxDTR_RTS);
			this.Controls.Add(this.richTextBox2);
			this.Controls.Add(this.richTextBox1);
			this.Controls.Add(this.checkBoxHide);
			this.Controls.Add(this.label1);
			this.Controls.Add(this.comboBoxPort);
			this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
			this.MaximizeBox = false;
			this.MinimizeBox = false;
			this.Name = "VexLoaderForm";
			this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
			this.Text = "vos应用程序 下载器";
			this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.AVRdudeSetFormFormClosing);
			this.panel1.ResumeLayout(false);
			this.ResumeLayout(false);
		}
		private System.Windows.Forms.Label labelBoardMes;
		private System.Windows.Forms.Button buttonVersion;
		private System.Windows.Forms.Label labelVersionList;
		private System.Windows.Forms.Label label8;
		private System.Windows.Forms.Button buttonDriver;
		private System.Windows.Forms.Button buttonRead;
		private System.Windows.Forms.Label labelMes;
		private System.Windows.Forms.Label label4;
		private System.Windows.Forms.Button buttonOpenVEX;
		private System.Windows.Forms.Label label3;
		private System.Windows.Forms.Label labelVersion;
		private System.Windows.Forms.Panel panel1;
		private System.Windows.Forms.Timer timer1;
		private System.Windows.Forms.Label label2;
		private System.Windows.Forms.ComboBox comboBox波特率;
		private System.Windows.Forms.CheckBox checkBoxHoldingPort;
		private System.Windows.Forms.CheckBox checkBoxDTR_RTS;
		private System.Windows.Forms.RichTextBox richTextBox2;
		private System.Windows.Forms.RichTextBox richTextBox1;
		private System.Windows.Forms.CheckBox checkBoxHide;
	}
}
