﻿
namespace n_FlashForm
{
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Windows.Forms;
using System.IO;
using System.Threading;
using System.Runtime.InteropServices;
using HWND = System.IntPtr;
using c_FormMover;

public partial class FlashForm : Form
{
	int TimeOut;
	public bool isShowMes;
	
	FormMover fm;
	
	//主窗口
	public FlashForm()
	{
		InitializeComponent();
		Rectangle E= Screen.PrimaryScreen.Bounds;
		
		//位置是屏幕左下角
		Point p0 = new Point(
			Screen.PrimaryScreen.WorkingArea.Width- this.Width,
			Screen.PrimaryScreen.WorkingArea.Height - this.Height );
		
		//位置是屏幕中间
		Point p1 = new Point(
			( Screen.PrimaryScreen.WorkingArea.Width- this.Width ) / 2,
			( Screen.PrimaryScreen.WorkingArea.Height - this.Height ) / 2 );
		
		this.Location = p1;
		isShowMes = true;
		this.Visible = false;
		
		fm = new FormMover( this );
	}
	
	//设置版本号
	public void SetVersion( string v )
	{
		Text += v;
	}
	
	//获取信息
	public string GetMessage()
	{
		if( this.Visible ) {
			return this.richTextBox1.Text;
		}
		return null;
	}
	
	//显示
	public void Run( string Message )
	{
		if( this.Visible ) {
			return;
		}
		if( !isShowMes ) {
			return;
		}
		TimeOut = 0;
		this.Opacity = 1;
		this.richTextBox1.Text = Message;
		this.Visible = true;
		//this.timer1.Enabled = true;
		//A.WorkBox.Focus();
	}
	
	//计时
	void Timer1Tick(object sender, EventArgs e)
	{
		++TimeOut;
		int Length = 100;
		int mid1 = 2 * Length / 20;
		int mid2 = 4 * Length / 20;
		int mid3 = 15 * Length / 20;
		float midOp = 0.9f;
		
		if( TimeOut < mid1 ) {
			this.Opacity = (float)( TimeOut - 0 ) / ( mid1 - 0 );
			return;
		}
		if( TimeOut < mid2 ) {
			this.Opacity = 0.99f - (float)( TimeOut - mid1 ) / ( mid2 - mid1 ) * ( 1 - midOp );
			return;
		}
		if( TimeOut < mid3 ) {
			return;
		}
		if( TimeOut < Length ) {
			this.Opacity = midOp - (float)( TimeOut - mid3 ) / ( Length - mid3 ) * midOp;
			return;
		}
		if( TimeOut == Length ) {
			this.Visible = false;
			this.timer1.Enabled = false;
			return;
		}
	}
	
	void FlashFormFormClosing(object sender, FormClosingEventArgs e)
	{
		e.Cancel = true;
		Visible = false;
	}
}
}


