﻿
using System;
using System.Drawing;
using System.Windows.Forms;

using System.Collections.Generic;
using System.ComponentModel;
using System.Text;
using System.IO;
using System.Net;
using System.Diagnostics;
using Tamir.SharpSsh;
using Tamir.SharpSsh.jsch;

namespace n_SSHLoaderForm
{
	public partial class SSHLoaderForm : Form
	{
		string BFile;
		
		SshTransferProtocolBase sshCp;
		Tamir.SharpSsh.SshStream inputstream;
		
		//构造函数
		public SSHLoaderForm()
		{
			InitializeComponent();
			
			this.Visible = false;
		}
		
		//运行窗体
		public void Run( string vFile )
		{
			BFile = vFile;
			
			if( checkBoxHide.Checked ) {
				ButtonOkClick( null, null );
			}
			else {
				//注意,直接设置Visible不能使窗体获得焦点
				this.Visible = true;
				//this.ShowDialog();
			}
			
			//while( this.Visible ) {
			//	System.Windows.Forms.Application.DoEvents();
			//}
			//return;
		}
		
		//结束程序
		public void MyClose()
		{
			try {
				if( inputstream != null ) {
					inputstream.Close();
				}
			}
			catch {
				
			}
			try {
				if( sshCp != null ) {
					sshCp.Close();
				}
			}
			catch {
				
			}
		}
		
		//确定键按下
		void ButtonOkClick(object sender, EventArgs e)
		{
			string Host = this.textBoxIP.Text;
			string UserName = this.textBoxUserName.Text;
			string Password = this.textBoxPassword.Text;
			
			//判断SCH连接是否存在
			if( sshCp == null ) {
				sshCp = new Scp( Host, UserName );
				sshCp.Password = Password;
				try {
					sshCp.Connect();
				}
				catch {
					MessageBox.Show( "连接主机失败" );
					return;
				}
			}
			//复制代码文件
			try {
				
				//sshCp.Get( "/usr/share/remo/uservex", "D:\\test" );
				//MessageBox.Show( "下载成功" );
				
				sshCp.Put( BFile, "/usr/share/remo/uservex" );
			}
			catch( Exception ee) {
				MessageBox.Show( "文件传送失败\n" + ee );
			}
			
			//重新启动remo
			try {
				if( inputstream != null ) {
					inputstream.Close();
					inputstream = null;
				}
				if( inputstream== null ) {
					System.IO.MemoryStream outputstream = new MemoryStream();
					inputstream = new Tamir.SharpSsh.SshStream( Host, UserName, Password );
				}
				//MessageBox.Show( "部署成功, 点击确定后开始重启remo..." );
				
				inputstream.Write( "killall remo\n" );
				inputstream.Flush();
				//System.Threading.Thread.Sleep(1000);
				inputstream.Write( "/usr/share/remo/remo;exit\n" );
				inputstream.Flush();
				//System.Threading.Thread.Sleep( 5000 );
				
				//string outinfo = Encoding.UTF8.GetString(outputstream.ToArray());
				// MessageBox.Show( outinfo );
				//outputstream.Flush();
			}
			catch( Exception ee ) {
				MessageBox.Show( "重启remo失败:\n" + ee );
			}
			this.Visible = false;
		}
		
		//窗口关闭
		void AVRdudeSetFormFormClosing(object sender, FormClosingEventArgs e)
		{
			this.Visible = false;
			e.Cancel = true;
		}
	}
}


/*
				SshShell shell = new SshShell(Host, UserName);
                shell.Password = Password;

                shell.RedirectToConsole();
               
                shell.Connect();
                
                while( !shell.Connected || !shell.ShellConnected || !shell.ShellOpened ) {
                	System.Windows.Forms.Application.DoEvents();
                }
                MessageBox.Show( "SSH OK!" );
                
                shell.WriteLine( "killall remo" );
                shell.WriteLine( "/usr/share/remo/remo;exit" );
                
                
                shell.Close();
				*/
				
				
				
				/*
				Tamir.SharpSsh.SshStream inputstream = new Tamir.SharpSsh.SshStream( Host, UserName, Password );
				inputstream.Write( "killall remo" );
				inputstream.Flush();
				
				inputstream.Write( "/usr/share/remo/remo;exit" );
				inputstream.Flush();
				
				inputstream.Close();
				*/
				
				
				
				
				/*
				//Create a new JSch instance
				JSch jsch=new JSch();
			
				//Create a new SSH session
				Session session=jsch.getSession( UserName, Host, 22 );

				// username and password will be given via UserInfo interface.
				UserInfo ui=new MyUserInfo();
				session.setUserInfo(ui);

				//Connect to remote SSH server
				session.connect();

				//Open a new Shell channel on the SSH session
				Channel channel=session.openChannel("shell");

				//Redirect standard I/O to the SSH channel
				//Stream i = Console.OpenStandardInput();
				
				Tamir.SharpSsh.SshStream inputstream = new Tamir.SharpSsh.SshStream( Host, UserName, Password );
				Tamir.SharpSsh.SshStream OutPutstream = new Tamir.SharpSsh.SshStream( Host, UserName, Password );
				
				Strea s = new Stream();
				
				channel.setInputStream( s );
				channel.setOutputStream(Console.OpenStandardOutput());

				//Connect the channel
				channel.connect();
				
				
				s.Write( "killall remo\n" );
				s.Write( "/usr/share/remo/remo;exit\n" );
				
				
				inputstream.Close();
				//Disconnect from remote server
				channel.disconnect();
				session.disconnect();	
				*/
