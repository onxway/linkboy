﻿/*
 * 由SharpDevelop创建。
 * 用户： dell
 * 日期: 2015/11/15
 * 时间: 8:29
 * 
 * 要改变这种模板请点击 工具|选项|代码编写|编辑标准头文件
 */
namespace n_SSHLoaderForm
{
	partial class SSHLoaderForm
	{
		/// <summary>
		/// Designer variable used to keep track of non-visual components.
		/// </summary>
		private System.ComponentModel.IContainer components = null;
		private System.Windows.Forms.Button buttonOk;
		
		/// <summary>
		/// Disposes resources used by the form.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing) {
				if (components != null) {
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}
		
		/// <summary>
		/// This method is required for Windows Forms designer support.
		/// Do not change the method contents inside the source code editor. The Forms designer might
		/// not be able to load this method if it was changed manually.
		/// </summary>
		private void InitializeComponent()
		{
			this.buttonOk = new System.Windows.Forms.Button();
			this.checkBoxHide = new System.Windows.Forms.CheckBox();
			this.label7 = new System.Windows.Forms.Label();
			this.richTextBox1 = new System.Windows.Forms.RichTextBox();
			this.richTextBox2 = new System.Windows.Forms.RichTextBox();
			this.textBoxIP = new System.Windows.Forms.TextBox();
			this.textBoxUserName = new System.Windows.Forms.TextBox();
			this.textBoxPassword = new System.Windows.Forms.TextBox();
			this.SuspendLayout();
			// 
			// buttonOk
			// 
			this.buttonOk.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(128)))), ((int)(((byte)(255)))));
			this.buttonOk.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
			this.buttonOk.Font = new System.Drawing.Font("微软雅黑", 21.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
			this.buttonOk.ForeColor = System.Drawing.Color.White;
			this.buttonOk.Location = new System.Drawing.Point(12, 377);
			this.buttonOk.Name = "buttonOk";
			this.buttonOk.Size = new System.Drawing.Size(441, 49);
			this.buttonOk.TabIndex = 6;
			this.buttonOk.Text = "开始下载";
			this.buttonOk.UseVisualStyleBackColor = false;
			this.buttonOk.Click += new System.EventHandler(this.ButtonOkClick);
			// 
			// checkBoxHide
			// 
			this.checkBoxHide.Font = new System.Drawing.Font("微软雅黑", 12F);
			this.checkBoxHide.ForeColor = System.Drawing.Color.Green;
			this.checkBoxHide.Location = new System.Drawing.Point(13, 296);
			this.checkBoxHide.Name = "checkBoxHide";
			this.checkBoxHide.Size = new System.Drawing.Size(401, 32);
			this.checkBoxHide.TabIndex = 10;
			this.checkBoxHide.Text = "以后不再出现这个对话框，直接下载程序";
			this.checkBoxHide.UseVisualStyleBackColor = true;
			// 
			// label7
			// 
			this.label7.Font = new System.Drawing.Font("微软雅黑", 10.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
			this.label7.ForeColor = System.Drawing.Color.Green;
			this.label7.Location = new System.Drawing.Point(13, 324);
			this.label7.Name = "label7";
			this.label7.Size = new System.Drawing.Size(414, 22);
			this.label7.TabIndex = 11;
			this.label7.Text = "（如需重现此对话框，可在软件界面右上角的系统设置中开启）";
			// 
			// richTextBox1
			// 
			this.richTextBox1.BorderStyle = System.Windows.Forms.BorderStyle.None;
			this.richTextBox1.Location = new System.Drawing.Point(12, 39);
			this.richTextBox1.Name = "richTextBox1";
			this.richTextBox1.Size = new System.Drawing.Size(441, 104);
			this.richTextBox1.TabIndex = 12;
			this.richTextBox1.Text = "";
			// 
			// richTextBox2
			// 
			this.richTextBox2.BorderStyle = System.Windows.Forms.BorderStyle.None;
			this.richTextBox2.Location = new System.Drawing.Point(12, 149);
			this.richTextBox2.Name = "richTextBox2";
			this.richTextBox2.Size = new System.Drawing.Size(441, 141);
			this.richTextBox2.TabIndex = 13;
			this.richTextBox2.Text = "";
			// 
			// textBoxIP
			// 
			this.textBoxIP.Location = new System.Drawing.Point(12, 12);
			this.textBoxIP.Name = "textBoxIP";
			this.textBoxIP.Size = new System.Drawing.Size(93, 21);
			this.textBoxIP.TabIndex = 15;
			this.textBoxIP.Text = "192.168.2.15";
			// 
			// textBoxUserName
			// 
			this.textBoxUserName.Location = new System.Drawing.Point(111, 12);
			this.textBoxUserName.Name = "textBoxUserName";
			this.textBoxUserName.Size = new System.Drawing.Size(93, 21);
			this.textBoxUserName.TabIndex = 16;
			this.textBoxUserName.Text = "root";
			// 
			// textBoxPassword
			// 
			this.textBoxPassword.Location = new System.Drawing.Point(210, 12);
			this.textBoxPassword.Name = "textBoxPassword";
			this.textBoxPassword.Size = new System.Drawing.Size(93, 21);
			this.textBoxPassword.TabIndex = 17;
			this.textBoxPassword.Text = "19870304";
			// 
			// SSHLoaderForm
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.BackColor = System.Drawing.Color.Gainsboro;
			this.ClientSize = new System.Drawing.Size(465, 438);
			this.Controls.Add(this.textBoxPassword);
			this.Controls.Add(this.textBoxUserName);
			this.Controls.Add(this.textBoxIP);
			this.Controls.Add(this.buttonOk);
			this.Controls.Add(this.richTextBox2);
			this.Controls.Add(this.richTextBox1);
			this.Controls.Add(this.label7);
			this.Controls.Add(this.checkBoxHide);
			this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
			this.Name = "SSHLoaderForm";
			this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
			this.Text = "SSH下载参数设置框";
			this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.AVRdudeSetFormFormClosing);
			this.ResumeLayout(false);
			this.PerformLayout();
		}
		private System.Windows.Forms.TextBox textBoxPassword;
		private System.Windows.Forms.TextBox textBoxUserName;
		private System.Windows.Forms.TextBox textBoxIP;
		private System.Windows.Forms.RichTextBox richTextBox2;
		private System.Windows.Forms.RichTextBox richTextBox1;
		private System.Windows.Forms.Label label7;
		private System.Windows.Forms.CheckBox checkBoxHide;
	}
}
