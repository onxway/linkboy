﻿
using System;
using System.Drawing;
using System.Windows.Forms;
using System.IO;
using System.Diagnostics;
using System.IO.Ports;
using n_MySerialPort;
using n_VexProtocol;
using n_VosVersForm;

using n_HexCoder;
using n_OS;
using n_CMD;

namespace n_ExportForm
{
public partial class ExportForm : Form
{
	string BasePath;
	string FileName;
	
	public int BLength;
	public byte[] CodeList;
	
	bool AutoDown = false;
	bool UserClose;
	
	//构造函数
	public ExportForm()
	{
		InitializeComponent();
		
		n_HexCoder.HexCoder.Init();
		HardInit();
		CMD.Init();
		
		//Width = 790;
		//Height = 625;
		
		panelInstallButton.Dock = DockStyle.Top;
		panelUart.Dock = DockStyle.Top;
		panelMes.Dock = DockStyle.Top;
		panelInstallSource.Dock = DockStyle.Top;
		panelExportBin.Dock = DockStyle.Top;
		panelInstallBin.Dock = DockStyle.Fill;
		labelComMes.Dock = DockStyle.Top;
		richTextBoxCMD.Dock = DockStyle.Fill;
		panelStart.Dock = DockStyle.Top;
		
		//=================================================
		HideAll();
		panelInstallButton.Visible = true;
		panelMes.Visible = true;
		panelUart.Visible = true;
		//=================================================
		
		HideAll();
		
		panelStart.Visible = true;
		
		n_wlibCom.wlibCom.SetFont( richTextBoxCMD );
		
		LastEPath = null;
	}
	
	//运行窗体
	public bool Run( string basePath, string fileName, byte[] ByteCodeList, int BCLength )
	{
		BasePath = basePath;
		FileName = fileName;
		
		UserClose = false;
		
		richTextBoxApp.Text = GetCode( ByteCodeList, BCLength );
		n_wlibCom.wlibCom.SetFont( richTextBoxApp );
		
		RunHardware( null );
		
		if( AutoDown ) {
			ButtonOkClick( null, null );
			Visible = false;
		}
		
		//HideAll();
		richTextBoxCMD.Text = "";
		
		if( NeedShow ) {
			labelVersion.Text = "";
			labelBoardMes.Text = "";
			this.Visible = true;
			while( this.Visible ) {
				System.Windows.Forms.Application.DoEvents();
			}
		}
		AutoDown = !NeedShow;
		
		return UserClose;
	}
	
	public string GetCode( byte[] ByteCodeList, int BCLength )
	{
		CodeList = ByteCodeList;
		BLength = BCLength;
		
		int len = BCLength;
		if( isv ) {
			len = 4096 * 8; //字节数
			buttonInstallSource.Visible = true;
		}
		string res = "";
		for( int i = 0; i < len; i += 4 ) {
			
			UInt64 data = 0;
			
			if( i < BCLength ) {
				byte b0 = CodeList[i];
				byte b1 = CodeList[i+1];
				byte b2 = CodeList[i+2];
				byte b3 = CodeList[i+3];
				data = b3; data <<= 8;
				data += b2; data <<= 8;
				data += b1; data <<= 8;
				data += b0;
			}
			
			if( isv ) {
				/*
				res += data.ToString( "X" ).PadLeft( 8, '0' ) + " ";
				if( (i/4) % 4 == 3 ) {
					res += "\r\n";
				}
				 */
				
				res += Convert.ToString( (long)data, 2 ).PadLeft( 32, '0' ) + "\n";
			}
			else {
				res += "0x" + data.ToString( "X" ).PadLeft( 8, '0' ) + ", ";
				if( (i/4) % 4 == 3 ) {
					res += " // " + ((i/16)*16).ToString( "X" ).PadLeft( 4, '0' ) + "\n";
				}
			}
		}
		res += "\n//!end\n";
		return res;
	}
	
	//窗口关闭
	void AVRdudeSetFormFormClosing(object sender, FormClosingEventArgs e)
	{
		if( timer1.Enabled ) {
			e.Cancel = true;
			return;
		}
		if( port != null && port.IsOpen ) {
			port.Close();
		}
		port = null;
		
		this.Visible = false;
		e.Cancel = true;
		
		UserClose = true;
	}
	
	void HideAll()
	{
		panelStart.Visible = false;
		
		panelInstallButton.Visible = false;
		panelUart.Visible = false;
		panelMes.Visible = false;
		panelInstallSource.Visible = false;
		panelExportBin.Visible = false;
		panelInstallBin.Visible = false;
		richTextBoxCMD.Visible = false;
		labelComMes.Visible = false;
		e_Type = E_TYPE.E_NULL;
	}
	
	void ButtonVosVerClick(object sender, EventArgs e)
	{
		if( n_wlibCom.wlibCom.VBox == null ) {
			n_wlibCom.wlibCom.VBox = new n_VosVersForm.VosVersForm();
		}
		n_wlibCom.wlibCom.VBox.Run( 1 );
	}
	
	//==============================================================================
	//下载方式
	
	enum E_TYPE
	{
		E_NULL, E_VOS_UART, E_8266, E_ESP32, E_ESP32C3, E_PICO, E_Microbit, E_ExportBin, E_InstallBin, E_InstallSource, 
	}
	E_TYPE e_Type;
	
	Button LastButton;
	
	void DealLast( Button b )
	{
		if( LastButton != null ) {
			LastButton.BackColor = Color.WhiteSmoke;
		}
		LastButton = b;
		b.BackColor = Color.Silver; //Color.FromArgb( 89, 165, 210 );
	}
	
	//通过 vos 串口协议下载
	void ButtonLinkboyProClick(object sender, EventArgs e)
	{
		comboBox波特率.Text = "115200";
		
		if( isv ) {
			comboBox波特率.Text = "9600";
		}
		DealLast( buttonLinkboyPro );
		HideAll();
		panelInstallButton.Visible = true;
		panelMes.Visible = true;
		panelUart.Visible = true;
		e_Type = E_TYPE.E_VOS_UART;
		
		//需要调用, 否则先点击了 8266 会导致位置错乱
		panelUart.SendToBack();
		panelInstallButton.SendToBack();
	}
	
	//ESP8266下载
	void Button8266Click(object sender, EventArgs e)
	{
		DealLast( button8266 );
		HideAll();
		richTextBoxCMD.Visible = true;
		panelInstallButton.Visible = true;
		//panelMes.Visible = true;
		panelUart.Visible = true;
		labelComMes.Visible = true;
		labelComMes.Text = "仅需烧录一次, 之后用vos串口协议下载即可. 如提示失败，请先将GPIO0/软件界面的D0引脚(对应开发板丝印可能是D8引脚)接地或按下Flash键，再尝试烧录固件；完成后松开D0针脚。";
		e_Type = E_TYPE.E_8266;
		
		comboBox波特率.Text = "921600";
	}
	
	//ESP32下载
	void ButtonESP32Click(object sender, EventArgs e)
	{
		DealLast( buttonESP32 );
		HideAll();
		richTextBoxCMD.Visible = true;
		panelInstallButton.Visible = true;
		//panelMes.Visible = true;
		panelUart.Visible = true;
		labelComMes.Visible = true;
		labelComMes.Text = "仅需烧录一次, 之后用vos串口协议下载即可. 如提示失败，请按下Flash/boot键，再尝试烧录固件；完成后松开按键。";
		e_Type = E_TYPE.E_ESP32;
		
		comboBox波特率.Text = "921600";
	}
	
	void ButtonESP32C3Click(object sender, EventArgs e)
	{
		DealLast( buttonESP32C3 );
		HideAll();
		richTextBoxCMD.Visible = true;
		panelInstallButton.Visible = true;
		//panelMes.Visible = true;
		panelUart.Visible = true;
		labelComMes.Visible = true;
		labelComMes.Text = "仅需烧录一次, 之后用vos串口协议下载即可. 如提示失败，请按下Flash/boot键，再尝试烧录固件；完成后松开按键。";
		e_Type = E_TYPE.E_ESP32C3;
		
		comboBox波特率.Text = "921600";
	}
	
	//树莓派pico下载
	void ButtonPicoClick(object sender, EventArgs e)
	{
		DealLast( buttonPico );
		HideAll();
		richTextBoxCMD.Visible = true;
		panelInstallButton.Visible = true;
		labelComMes.Visible = true;
		labelComMes.Text = "先按住pico主板上的BOOTSEL键，再通过USB数据线连接到电脑，最后点击开始下载按钮。";
		e_Type = E_TYPE.E_PICO;
	}
	
	//Micro:bit下载
	void ButtonMicrobitClick(object sender, EventArgs e)
	{
		DealLast( buttonMicrobit );
		HideAll();
		richTextBoxCMD.Visible = true;
		panelInstallButton.Visible = true;
		labelComMes.Visible = true;
		labelComMes.Text = "通过USB数据线把micro:bit开发板连接到电脑，再点击开始下载按钮。";
		e_Type = E_TYPE.E_Microbit;
	}
	
	//导出机器码文件
	void ButtonExportBinFileClick(object sender, EventArgs e)
	{
		DealLast( buttonExportBinFile );
		HideAll();
		panelExportBin.Visible = true;
		e_Type = E_TYPE.E_ExportBin;
	}
	
	//外挂模式: 安装到固件文件
	void ButtonInstallBinClick(object sender, EventArgs e)
	{
		DealLast( buttonInstallBin );
		HideAll();
		panelInstallBin.Visible = true;
		e_Type = E_TYPE.E_InstallBin;
	}
	
	//外挂模式: 安装到源码文件
	void ButtonInstallSourceClick(object sender, EventArgs e)
	{
		DealLast( buttonInstallSource );
		HideAll();
		panelInstallSource.Visible = true;
		e_Type = E_TYPE.E_InstallSource;
	}
	
	//--------------------------------------------------------------
	//安装到硬件 - 协议和命令行调用方式
	
	//确定键按下
	void ButtonOkClick(object sender, EventArgs e)
	{
		buttonOk.Text = "执行中...";
		buttonOk.BackColor = Color.Goldenrod;
		buttonOk.Enabled = false;
		
		if( e_Type == E_TYPE.E_NULL ) {
			MessageBox.Show( "请先选择一个设备类型" );
		}
		if( e_Type == E_TYPE.E_VOS_UART ) {
			VOS_Uart( sender );
		}
		if( e_Type == E_TYPE.E_8266 ) {
			//Button8266();
			Button8266once();
		}
		if( e_Type == E_TYPE.E_ESP32 ) {
			ButtonESP32once();
		}
		if( e_Type == E_TYPE.E_ESP32C3 ) {
			ButtonESP32C3once();
		}
		if( e_Type == E_TYPE.E_PICO ) {
			ButtonPico();
		}
		if( e_Type == E_TYPE.E_Microbit ) {
			ButtonMicrobit();
		}
		buttonOk.BackColor = Color.FromArgb( 153, 204, 251 );
		buttonOk.Text = "开始下载";
		buttonOk.Enabled = true;
	}
	
	void ButtonPico()
	{
		//复制vos文件
		if( File.Exists( AppDomain.CurrentDomain.BaseDirectory + @"Resource\tools\pico\Blink.ino.elf" ) ) {
			File.Delete( AppDomain.CurrentDomain.BaseDirectory + @"Resource\tools\pico\Blink.ino.elf" );
		}
		File.Copy( AppDomain.CurrentDomain.BaseDirectory + @"Resource\tools\pico\vos\Blink.ino.elf", AppDomain.CurrentDomain.BaseDirectory + @"Resource\tools\pico\Blink.ino.elf" );
		
		//安装App到指定vos文件
		CurrentVosFile = AppDomain.CurrentDomain.BaseDirectory + @"Resource\tools\pico\Blink.ino.elf";
		ButtonSwitchClick( null, null );
		
		richTextBoxCMD.Focus();
		/*
		CMD.Download( richTextBoxCMD, AppDomain.CurrentDomain.BaseDirectory + @"Resource\tools\pico\rp2040tools\1.0.2\rp2040load.exe",
		              @"-v -D ", AppDomain.CurrentDomain.BaseDirectory + @"Resource\tools\pico\Blink.ino.elf", "" );
		*/
		//注意: 用引号括起来路径, 防止带有空格目录异常
		CMD.Download( richTextBoxCMD, AppDomain.CurrentDomain.BaseDirectory + @"Resource\tools\pico\rp2040tools\1.0.2\rp2040load.exe",
		             "-v -D \"", AppDomain.CurrentDomain.BaseDirectory + "Resource\\tools\\pico\\Blink.ino.elf\"", "" );
	}
	
	void Button8266()
	{
		if( this.comboBoxPort.SelectedIndex == -1 ) {
			MessageBox.Show( "请先选择串口号" );
			return;
		}
		string Name = this.comboBoxPort.Text;
		if( Name.StartsWith( "COM1 " ) ) {
			if( MessageBox.Show( "您可能选择了错误的的串口号， 一般来说不能使用COM1下载程序，需要选择硬件主板所对应的串口号。您确定要继续吗？",  "注意", MessageBoxButtons.YesNo ) == DialogResult.No ) {
				return;
			}
		}
		string pname = MySerialPort.GetComName( Name );
		
		//复制vos文件
		if( File.Exists( AppDomain.CurrentDomain.BaseDirectory + @"Resource\tools\esptool\Blink.ino.bin" ) ) {
			File.Delete( AppDomain.CurrentDomain.BaseDirectory + @"Resource\tools\esptool\Blink.ino.bin" );
		}
		File.Copy( AppDomain.CurrentDomain.BaseDirectory + @"Resource\tools\esptool\vos\Blink.ino.bin", AppDomain.CurrentDomain.BaseDirectory + @"Resource\tools\esptool\Blink.ino.bin" );
		
		//安装App到指定vos文件
		CurrentVosFile = AppDomain.CurrentDomain.BaseDirectory + @"Resource\tools\esptool\Blink.ino.bin";
		ButtonSwitchClick( null, null );
		
		richTextBoxCMD.Focus();
		
		/*
		CMD.Download( richTextBoxCMD, AppDomain.CurrentDomain.BaseDirectory + @"Resource\tools\esptool\0.4.13\esptool.exe",
		              @"-vv -cd ck -cb 115200 -cp COM4 -ca 0x00000 -cf ",
		              AppDomain.CurrentDomain.BaseDirectory + @"Resource\tools\esptool\Blink.ino.bin", "" );
		
		//注意: 用引号括起来路径, 防止带有空格目录异常
		CMD.Download( richTextBoxCMD, AppDomain.CurrentDomain.BaseDirectory + @"Resource\tools\esptool\0.4.13\esptool.exe",
		              "-vv -cd ck -cb 115200 -cp COM4 -ca 0x00000 -cf \"",
		              AppDomain.CurrentDomain.BaseDirectory + "Resource\\tools\\esptool\\Blink.ino.bin\"", "" );
		*/
		
		//注意: 用引号括起来路径, 防止带有空格目录异常
		CMD.Download( richTextBoxCMD, AppDomain.CurrentDomain.BaseDirectory + @"Resource\tools\esptool\0.4.13\esptool.exe",
		              "-vv -cd ck -cb " + comboBox波特率.Text + " -cp " + pname + " -ca 0x00000 -cf \"",
		              AppDomain.CurrentDomain.BaseDirectory + "Resource\\tools\\esptool\\Blink.ino.bin\"", "" );
	}
	
	void Button8266once()
	{
		if( this.comboBoxPort.SelectedIndex == -1 ) {
			MessageBox.Show( "请先选择串口号" );
			return;
		}
		string Name = this.comboBoxPort.Text;
		if( Name.StartsWith( "COM1 " ) ) {
			if( MessageBox.Show( "您可能选择了错误的的串口号， 一般来说不能使用COM1下载程序，需要选择硬件主板所对应的串口号。您确定要继续吗？",  "注意", MessageBoxButtons.YesNo ) == DialogResult.No ) {
				return;
			}
		}
		string pname = MySerialPort.GetComName( Name );
		
		richTextBoxCMD.Focus();
		
		/*
		CMD.Download( richTextBoxCMD, AppDomain.CurrentDomain.BaseDirectory + @"Resource\tools\esptool\0.4.13\esptool.exe",
		              @"-vv -cd ck -cb 115200 -cp COM4 -ca 0x00000 -cf ",
		              AppDomain.CurrentDomain.BaseDirectory + @"Resource\tools\esptool\Blink.ino.bin", "" );
		
		//注意: 用引号括起来路径, 防止带有空格目录异常
		CMD.Download( richTextBoxCMD, AppDomain.CurrentDomain.BaseDirectory + @"Resource\tools\esptool\0.4.13\esptool.exe",
		              "-vv -cd ck -cb 115200 -cp COM4 -ca 0x00000 -cf \"",
		              AppDomain.CurrentDomain.BaseDirectory + "Resource\\tools\\esptool\\Blink.ino.bin\"", "" );
		*/
		
		//注意: 用引号括起来路径, 防止带有空格目录异常
		CMD.Download( richTextBoxCMD, AppDomain.CurrentDomain.BaseDirectory + @"Resource\tools\esptool\0.4.13\esptool.exe",
		              "-vv -cd nodemcu -cb " + comboBox波特率.Text + " -cp " + pname + " -ca 0x00000 -cf \"",
		              AppDomain.CurrentDomain.BaseDirectory + "Resource\\tools\\esptool\\BASIC.ino.bin\"", "" );
	}
	
	void ButtonESP32once()
	{
		if( this.comboBoxPort.SelectedIndex == -1 ) {
			MessageBox.Show( "请先选择串口号" );
			return;
		}
		string Name = this.comboBoxPort.Text;
		if( Name.StartsWith( "COM1 " ) ) {
			if( MessageBox.Show( "您可能选择了错误的的串口号， 一般来说不能使用COM1下载程序，需要选择硬件主板所对应的串口号。您确定要继续吗？",  "注意", MessageBoxButtons.YesNo ) == DialogResult.No ) {
				return;
			}
		}
		string pname = MySerialPort.GetComName( Name );
		
		richTextBoxCMD.Focus();
		
		/*
		CMD.Download( richTextBoxCMD, AppDomain.CurrentDomain.BaseDirectory + @"Resource\tools\esptool\0.4.13\esptool.exe",
		              @"-vv -cd ck -cb 115200 -cp COM4 -ca 0x00000 -cf ",
		              AppDomain.CurrentDomain.BaseDirectory + @"Resource\tools\esptool\Blink.ino.bin", "" );
		
		//注意: 用引号括起来路径, 防止带有空格目录异常
		CMD.Download( richTextBoxCMD, AppDomain.CurrentDomain.BaseDirectory + @"Resource\tools\esptool\0.4.13\esptool.exe",
		              "-vv -cd ck -cb 115200 -cp COM4 -ca 0x00000 -cf \"",
		              AppDomain.CurrentDomain.BaseDirectory + "Resource\\tools\\esptool\\Blink.ino.bin\"", "" );
		*/
		
		//C:\Users\dell\AppData\Local\Arduino15\packages\esp32\tools\esptool_py\3.0.0/esptool.exe
		//--chip esp32 --port COM61 --baud 921600 --before default_reset --after hard_reset write_flash -z --flash_mode dio --flash_freq 80m --flash_size detect 
		//0xe000 C:\Users\dell\AppData\Local\Arduino15\packages\esp32\hardware\esp32\2.0.0-alpha1/tools/partitions/boot_app0.bin 
		//0x1000 C:\Users\dell\AppData\Local\Temp\arduino_build_559848/BASIC.ino.bootloader.bin 
		//0x10000 C:\Users\dell\AppData\Local\Temp\arduino_build_559848/BASIC.ino.bin 
		//0x8000 C:\Users\dell\AppData\Local\Temp\arduino_build_559848/BASIC.ino.partitions.bin
		
		//注意: 用引号括起来路径, 防止带有空格目录异常
		CMD.Download( richTextBoxCMD, AppDomain.CurrentDomain.BaseDirectory + @"Resource\tools\esptool_py\3.0.0\esptool.exe",
		              "--chip esp32 --port " + pname + " --baud " + comboBox波特率.Text + " --before default_reset --after hard_reset write_flash -z --flash_mode dio --flash_freq 80m --flash_size detect ",
		              "0xe000 \"" + AppDomain.CurrentDomain.BaseDirectory + "Resource\\tools\\esptool_py\\esp32\\boot_app0.bin\" " +
		              "0x1000 \"" + AppDomain.CurrentDomain.BaseDirectory + "Resource\\tools\\esptool_py\\esp32\\bootloader.bin\" " +
		              "0x10000 \"" + AppDomain.CurrentDomain.BaseDirectory + "Resource\\tools\\esptool_py\\esp32\\vos.bin\" " +
		              "0x8000 \"" + AppDomain.CurrentDomain.BaseDirectory + "Resource\\tools\\esptool_py\\esp32\\partitions.bin\"",
		              "" );
	}
	
	void ButtonESP32C3once()
	{
		if( this.comboBoxPort.SelectedIndex == -1 ) {
			MessageBox.Show( "请先选择串口号" );
			return;
		}
		string Name = this.comboBoxPort.Text;
		if( Name.StartsWith( "COM1 " ) ) {
			if( MessageBox.Show( "您可能选择了错误的的串口号， 一般来说不能使用COM1下载程序，需要选择硬件主板所对应的串口号。您确定要继续吗？",  "注意", MessageBoxButtons.YesNo ) == DialogResult.No ) {
				return;
			}
		}
		string pname = MySerialPort.GetComName( Name );
		
		richTextBoxCMD.Focus();
		
		/*
		CMD.Download( richTextBoxCMD, AppDomain.CurrentDomain.BaseDirectory + @"Resource\tools\esptool\0.4.13\esptool.exe",
		              @"-vv -cd ck -cb 115200 -cp COM4 -ca 0x00000 -cf ",
		              AppDomain.CurrentDomain.BaseDirectory + @"Resource\tools\esptool\Blink.ino.bin", "" );
		
		//注意: 用引号括起来路径, 防止带有空格目录异常
		CMD.Download( richTextBoxCMD, AppDomain.CurrentDomain.BaseDirectory + @"Resource\tools\esptool\0.4.13\esptool.exe",
		              "-vv -cd ck -cb 115200 -cp COM4 -ca 0x00000 -cf \"",
		              AppDomain.CurrentDomain.BaseDirectory + "Resource\\tools\\esptool\\Blink.ino.bin\"", "" );
		*/
		
		//C:\Users\dell\AppData\Local\Arduino15\packages\esp32\tools\esptool_py\3.0.0/esptool.exe 
		//--chip esp32c3 --port COM1 --baud 921600 --before default_reset --after hard_reset write_flash -z --flash_mode dio --flash_freq 80m --flash_size detect 
		//0xe000 C:\Users\dell\AppData\Local\Arduino15\packages\esp32\hardware\esp32\2.0.0-alpha1/tools/partitions/boot_app0.bin 
		//0x0 C:\Users\dell\AppData\Local\Temp\arduino_build_559848/BASIC.ino.bootloader.bin 
		//0x10000 C:\Users\dell\AppData\Local\Temp\arduino_build_559848/BASIC.ino.bin 
		//0x8000 C:\Users\dell\AppData\Local\Temp\arduino_build_559848/BASIC.ino.partitions.bin
		
		//注意: 用引号括起来路径, 防止带有空格目录异常
		CMD.Download( richTextBoxCMD, AppDomain.CurrentDomain.BaseDirectory + @"Resource\tools\esptool_py\3.0.0\esptool.exe",
		              "--chip esp32c3 --port " + pname + " --baud " + comboBox波特率.Text + " --before default_reset --after hard_reset write_flash -z --flash_mode dio --flash_freq 80m --flash_size detect ",
		              "0xe000 \"" + AppDomain.CurrentDomain.BaseDirectory + "Resource\\tools\\esptool_py\\esp32c3\\boot_app0.bin\" " +
		              "0x0 \"" + AppDomain.CurrentDomain.BaseDirectory + "Resource\\tools\\esptool_py\\esp32c3\\bootloader.bin\" " +
		              "0x10000 \"" + AppDomain.CurrentDomain.BaseDirectory + "Resource\\tools\\esptool_py\\esp32c3\\vos.bin\" " +
		              "0x8000 \"" + AppDomain.CurrentDomain.BaseDirectory + "Resource\\tools\\esptool_py\\esp32c3\\partitions.bin\"",
		              "" );
	}
	
	void ButtonMicrobit()
	{
		//复制vos文件
		if( File.Exists( AppDomain.CurrentDomain.BaseDirectory + @"Resource\tools\microbit\Arduino-Microbit.ino.hex" ) ) {
			File.Delete( AppDomain.CurrentDomain.BaseDirectory + @"Resource\tools\microbit\Arduino-Microbit.ino.hex" );
		}
		File.Copy( AppDomain.CurrentDomain.BaseDirectory + @"Resource\tools\microbit\vos\Arduino-Microbit.ino.hex", AppDomain.CurrentDomain.BaseDirectory + @"Resource\tools\microbit\Arduino-Microbit.ino.hex" );
		
		//安装App到指定vos文件
		CurrentVosFile = AppDomain.CurrentDomain.BaseDirectory + @"Resource\tools\microbit\Arduino-Microbit.ino.hex";
		ButtonSwitchClick( null, null );
		
		//C:\Users\dell\AppData\Local\Arduino15\packages\sandeepmistry\tools\openocd\0.10.0-dev.nrf5/bin/openocd.exe
		//-d2 -f interface/cmsis-dap.cfg -c transport select swd; -f target/nrf51.cfg -c program {{C:\Users\dell\AppData\Local\Temp\arduino_build_538004/Arduino-Microbit.ino.hex}} verify reset; shutdown;
		
		richTextBoxCMD.Focus();
		CMD.Download( richTextBoxCMD, AppDomain.CurrentDomain.BaseDirectory + @"Resource\tools\microbit\openocd-0.10.0-dev.nrf5/bin/openocd.exe",
		              "-d2 -f interface/cmsis-dap.cfg -c \"transport select swd;\" -f target/nrf51.cfg -c \"program {{",
		              AppDomain.CurrentDomain.BaseDirectory + @"Resource\tools\microbit\Arduino-Microbit.ino.hex",
					  "}} verify reset; shutdown;\"" );
	}
	
	//==============================================================================
	//安装到vos硬件
	
	public delegate void deleProgressStep( int Max, int Current );
	public static deleProgressStep ProgressStep;
	
	SerialPort port;
	byte[] ReadBuffer;
	
	byte[] Buffer;
	
	//发送字节码
	byte[] BufferHead;
	
	string BFile;
	bool NeedShow;
	
	int ClickNumber = 0;
	
	public static int RAM_Size;
	public static int ROM_Size;
	public static int UseROM;
	public static int UseRAM;
	bool VersionUpShow;
	
	string Result;
	
	//初始化
	void HardInit()
	{
		ReadBuffer = new byte[ 100 ];
		
		Buffer = new byte[300000];
		BufferHead = new byte[6];
		
		this.Visible = false;
		
		VexProtocol.Init();
		VexProtocol.ReceiveType = ReType;
		NeedShow = true;
		
		TempText = label4.Text;
	}
	
	//运行
	public void RunHardware( string vFile )
	{
		if( isv || n_AVRdude.AVRdude.BoardType == "Curie" ) {
			this.comboBox波特率.Text = "9600";
		}
		else {
			this.comboBox波特率.Text = "115200";
		}
		BFile = vFile;
		labelVersion.Text = "";
		labelMes.Text = "";
	}
	
	void ComboBoxPortClick(object sender, EventArgs e)
	{
		//需要调用两次
		//RefreshPortList();
		//RefreshPortList();
		
		this.comboBoxPort.Items.Clear();
		
		//通过WMI获取COM端口
		string[] ss = MySerialPort.GetFullNameList();
		if( ss != null ) {
			this.comboBoxPort.Items.AddRange( ss );
		}
	}
	
	void VOS_Uart( object sender )
	{
		buttonOk.Enabled = false;
		timer1.Enabled = true;
		
		Result = "";
		
		labelVersion.Text = "";
		
		if( port == null || !port.IsOpen ) {
			string port_err = TryOpenPortError();
			if( port_err != null ) {
				timer1.Enabled = false;
				NeedShow = true;
				labelMes.Text = port_err;
				buttonOk.Enabled = true;
				return;
			}
		}
		
		VexProtocol.Result = "";
		
		//发送程序数据
		string err = SendProgramOK();
		
		if( err == "NZIP" || err == "ROM" || err == "RAM" || err == "NDKERR" ) {
			if( err == "NZIP" ) {
				n_Decode.Decode.Zip = false;
				MessageBox.Show( "板载虚拟机固件版本低于2.60, 已自动切换为非压缩指令集, 请重新点击下载按钮即可" );
			}
			else if( err == "ROM" ) {
				MessageBox.Show( "程序量已超出主板ROM容量, 请重新点击下载按钮, 系统将自动进行优化..." );
			}
			else if( err == "RAM" ) {
				MessageBox.Show( "数据区已超出主板RAM容量, 请重新点击下载按钮, 系统将自动进行优化..." );
			}
			else if( err == "NDKERR" ) {
				MessageBox.Show( "主板固件版本过低, 无法运行当前的程序, 请升级主板固件" );
			}
			else {
				MessageBox.Show( "未知的错误: " + err );
			}
			buttonOk.Enabled = true;
			timer1.Enabled = false;
			
			this.Visible = false;
			if( port != null && port.IsOpen ) {
				port.Close();
			}
			port = null;
			return;
		}
		
		if( err != null ) {
			labelMes.Text = err;
		}
		
		//关闭串口
		if( !this.checkBoxHoldingPort.Checked ) {
			System.Threading.Thread.Sleep( 100 );
			try {
				port.DtrEnable = false;
				port.RtsEnable = false;
				port.Close();
			} catch {
				MessageBox.Show( "串口关闭失败" );
			}
			port = null;
		}
		timer1.Enabled = false;
		buttonOk.Enabled = true;
		
		NeedShow = (err != null) || checkBoxHoldingPort.Checked;
		
		if( sender != null ) {
			this.Visible = NeedShow;
		}
	}
	
	void ButtonOkMouseDown(object sender, MouseEventArgs e)
	{
		if( e.Button == MouseButtons.Right ) {
			ClickNumber++;
			if( ClickNumber > 10 ) {
				checkBoxHoldingPort.Checked = true;
			}
		}
	}
	
	string TryOpenPortError()
	{
		if( this.comboBoxPort.SelectedIndex == -1 ) {
			//MessageBox.Show( "请选择端口!" );
			if( checkBoxHide.Checked ) {
				//this.Visible = true;
			}
			return "请选择一个有效的串口号";
		}
		try {
			string Name = this.comboBoxPort.Text;
			if( Name.StartsWith( "COM1 " ) ) {
				if( MessageBox.Show( "您可能选择了错误的的串口号， 一般来说不能使用COM1下载程序，需要选择硬件主板所对应的串口号。您确定要继续吗？",  "注意", MessageBoxButtons.YesNo ) == DialogResult.No ) {
					return "请选择一个有效的串口号";
				}
			}
			string pname = MySerialPort.GetComName( Name );
			port = new System.IO.Ports.SerialPort( pname );
			port.BaudRate = int.Parse( this.comboBox波特率.Text );
			port.DataBits = 8;
			port.Parity = System.IO.Ports.Parity.None;
			port.StopBits = System.IO.Ports.StopBits.One;
			port.ReadTimeout = 1;
			if( checkBoxDTR_RTS.Checked ) {
				port.DtrEnable = true;
				port.RtsEnable = true;
			}
			port.DataReceived += new SerialDataReceivedEventHandler( DataReceived );
			port.Open();
			
			//不加这个可能会读取版本失败
			System.Threading.Thread.Sleep( 200 );
			
			n_AVRdude.AVRdude.PortName = pname;
		}
		catch {
			return "串口打开失败, 请先关闭其他占用串口的程序";
		}
		return null;
	}
	
	string SendProgramOK()
	{
		n_AVRdude.AVRdude.OK = false;
		
		try {
			//----------------------------------------
			//导入文件
			int RNumber = 0;
			if( BFile == null ) {
				RNumber = n_Decode.Decode.BCLength;
				for( int i = 0; i < n_Decode.Decode.BCLength; ++i ) {
					Buffer[i] = n_Decode.Decode.ByteCodeList[i];
				}
			}
			else {
				FileStream fs = File.Open( BFile, FileMode.OpenOrCreate );
				RNumber = fs.Read( Buffer, 0, Buffer.Length );
				fs.Close();
			}
			
			//计算校验和
			byte CheckSum = 0;
			for( int i = 0; i < RNumber; ++i ) {
				CheckSum ^= Buffer[i];
			}
			
			Result += "CHECK:" + CheckSum.ToString( "X" ).PadLeft( 2, '0' ) + " NUM:" + RNumber + " -> ";
			
			string temptext = buttonOk.Text;
			buttonOk.Text = "等待按下主板复位键";
			if( WaitReceiveError_Start( VexProtocol.VM_VERSION, 100 ) ) {
				
				buttonOk.Text = temptext;
				//MessageBox.Show( "vos引擎版本获取失败. 可尝试按下复位键后, 再立刻点击下载按钮, 1秒钟之内有效", "请尝试按下复位键后再下载" );
				return "vos引擎版本获取失败. 可尝试按一下复位键, 再立刻点击下载按钮, 1秒钟之内有效";
			}
			buttonOk.Text = temptext;
			
			//版本校验
			
			int Datalen = VexProtocol.RE_Data[1];
			Datalen += 256 * VexProtocol.RE_Data[2];
			Datalen += 3;
			
			int bV1 = VexProtocol.RE_Data[4];
			int bV2 = VexProtocol.RE_Data[5];
			
			int MesStart = 9;
			if( bV1 > 2 || (bV1 == 2 && bV2 >= 40) ) {
				RAM_Size = VexProtocol.RE_Data[9];
				RAM_Size += 256 * VexProtocol.RE_Data[10];
				RAM_Size += 256 * 256 * VexProtocol.RE_Data[11];
				//... 未处理最高位
				
				ROM_Size = VexProtocol.RE_Data[13];
				ROM_Size += 256 * VexProtocol.RE_Data[14];
				ROM_Size += 256 * 256 * VexProtocol.RE_Data[15];
				//... 未处理最高位
				
				MesStart = 17;
			}
			else {
				RAM_Size = 1024 * VexProtocol.RE_Data[6];
				ROM_Size = 1024 * VexProtocol.RE_Data[7];
			}
			VexProtocol.NDKLen = 0;
			char bVX = 'X';
			string VosMes = null;
			if( bV1 > 2 || (bV1 == 2 && bV2 > 2) ) {
				
				bVX = (char)VexProtocol.RE_Data[8];
				
				if( bV1 == 2 && bV2 <= 60 ) {
					//while( VexProtocol.RE_Data[i] != 0 ) {
					while( MesStart < Datalen ) {
						VosMes += ((char)VexProtocol.RE_Data[MesStart]).ToString();
						MesStart++;
						if( MesStart > 300 ) {
							break;
						}
					}
				}
				//这里是新版 带有扩展卡序号参数
				else {
					int end_CM = 0;
					
					//芯片信息
					end_CM = MesStart + VexProtocol.RE_Data[MesStart];
					MesStart += 1;
					while( MesStart <= end_CM ) {
						VosMes += ((char)VexProtocol.RE_Data[MesStart]).ToString();
						MesStart++;
					}
					//MessageBox.Show( "A:"+ VosMes + " - " + VosMes.Length );
					
					//扩展卡信息
					int ndki = 0;
					
					end_CM = MesStart + VexProtocol.RE_Data[MesStart];
					MesStart += 1;
					while( MesStart <= end_CM ) {
						VexProtocol.NDKList[ndki] = VexProtocol.RE_Data[MesStart];
						MesStart++;
						VexProtocol.NDKList[ndki] += 256 * VexProtocol.RE_Data[MesStart];
						MesStart++;
						ndki++;
					}
					VexProtocol.NDKLen = ndki;
				}
				//判断SDK是否支持
				if( NDK_Error() != -1 ) {
					return "NDKERR";
				}
			}
			
			labelVersion.Text = "v" + bV1 + "." + bV2.ToString().PadLeft( 2, '0' ) + "." + (char)bVX +
				" (RAM:" + RAM_Size + " ROM:" + ROM_Size + ")";
			
			//判断是否需要切换回非压缩指令集
			if( bV1 < 2 || (bV1 == 2 && bV2 < 60) ) {
				if( n_Decode.Decode.Zip ) {
					return "NZIP";
				}
			}
			//这里临时增加ROM溢出判断
			if( UseROM >= ROM_Size ) {
				return "ROM";
			}
			if( UseRAM >= RAM_Size ) {
				return "RAM";
			}
			//版本匹配
			int cV1 = 0;
			int cV2 = 0;
			char cVX = (char)0;
			string CPU = null;
			if( VosMes != null ) {
				labelBoardMes.Text = VosMes;
				string[] line0 = n_VosVer.VosVer.CPU_VersionList.Split( '\n' );
				string[][] CPUVL = new string[line0.Length][];
				for( int i = 0; i < line0.Length; ++i ) {
					CPUVL[i] = line0[i].Split( ':' );
					CPUVL[i][1] = CPUVL[i][1].Trim( " \t".ToCharArray() );
				}
				string[] line = VosMes.Split( ',' );
				for( int i = 0; i < line.Length; ++i ) {
					string[] cut = line[i].Split( ':' );
					if( cut[0] == "CPU" ) {
						CPU = cut[1];
						break;
					}
				}
				if( CPU != null ) {
					bool ok = false;
					for( int j = 0; j < CPUVL.Length; ++j ) {
						if( CPU == CPUVL[j][0] ) {
							string[] vl = CPUVL[j][1].Split( '.' );
							cV1 = int.Parse( vl[0] );
							cV2 = int.Parse( vl[1] );
							cVX = vl[2][0];
							ok = true;
							break;
						}
					}
					if( !ok ) {
						//... 软件版本比较旧 ... 可以忽略这个情况
					}
				}
			}
			if( cV1 == 0 || cV2 == 0 ) {
				cV1 = 2;
				cV2 = 30;
				cVX = 'A';
			}
			if( bV1 < cV1 || (bV1 == cV1 && bV2 < cV2) || (bV1 == cV1 && bV2 == cV2 && bVX < cVX) ) {
				if( CPU == null ) {
					CPU = "未知型号";
				}
				
				/*
				MessageBox.Show( "检测到主板的vos引擎版本为 " + bV1 + "." + bV2 + bVX + "[" + CPU + "], 已过时.\n" +
				                 "强烈建议升级到针对[" + CPU + "]的最新版本 " + cV1 + "." + cV2 + cVX + "\n" +
				                 "不升级的话部分模块可能运行异常.\n各型号CPU的VOS最新版本列表:\n" + CPU_VersionList,
				                 "vos版本更新提示" );
				 */
				
				if( !VersionUpShow ) {
					
					string mes = "主板的vos引擎版本为" + bV1 + "." + bV2.ToString().PadLeft( 2, '0' ) + "." + bVX + "[" + CPU + "], 已过时. " +
						"强烈建议升级到针对[" + CPU + "]的最新版本" + cV1 + "." + cV2.ToString().PadLeft( 2, '0' ) + "." + cVX + ". " +
						"不升级的话程序可能运行异常. ";
					
					VersionUpShow = true;
					
					//Width = 768;
					//Height = 398;
					return mes + "再点击一下<开始下载>按钮将继续下载程序到主板.";
				}
			}
			
			//----------------------------------------
			//设置地址
			VexProtocol.ClearReType();
			BufferHead[0] = 0xAA;
			BufferHead[1] = 0x03;
			BufferHead[2] = 0x00;
			BufferHead[3] = 0xF0;
			BufferHead[4] = 0x00;
			BufferHead[5] = 0x00;
			port.Write( BufferHead, 0, 6 );
			//设置地址时会清空flash, 时间较长. 时间单位: 0.1秒
			if( WaitReceiveError( VexProtocol.VM_SETADDR, 50 ) ) {
				NeedShow = true;
				return "设置下载起始地址无回应";
			}
			//----------------------------------------
			//循环写入数据
			int Times = RNumber / 256;
			int Mode = RNumber % 256;
			for( int i = 0; i < Times; ++i ) {
				
				VexProtocol.ClearReType();
				BufferHead[0] = 0xAA;
				BufferHead[1] = 0x01;
				BufferHead[2] = 0x01;
				BufferHead[3] = 0xF1;
				port.Write( BufferHead, 0, 4 );
				port.Write( Buffer, i * 256, 256 );
				if( WaitReceiveError( VexProtocol.VM_STORE, 10 ) ) {
					NeedShow = true;
					return "写入数据无回应";
				}
				if( ProgressStep != null ) {
					ProgressStep( Times, i );
				}
			}
			//----------------------------------------
			//写入剩余数据
			if( Mode != 0 ) {
				
				VexProtocol.ClearReType();
				BufferHead[0] = 0xAA;
				BufferHead[1] = (byte)(Mode + 1);
				BufferHead[2] = 0;
				BufferHead[3] = 0xF1;
				port.Write( BufferHead, 0, 4 );
				port.Write( Buffer, Times * 256, Mode );
				
				if( WaitReceiveError( VexProtocol.VM_STORE, 10 ) ) {
					NeedShow = true;
					return "写入结束数据无回应";
				}
			}
			//----------------------------------------
			//读取校验和
			VexProtocol.ClearReType();
			BufferHead[0] = 0xAA;
			BufferHead[1] = 0x01;
			BufferHead[2] = 0x00;
			BufferHead[3] = 0xF3;
			port.Write( BufferHead, 0, 4 );
			if( WaitReceiveError( VexProtocol.VM_CHECK, 10 ) ) {
				NeedShow = true;
				return "读取校验和无回应";
			}
		}
		catch(Exception e) {
			MessageBox.Show( e.ToString() );
			return "下载过程发生错误!\n" + e;
		}
		n_AVRdude.AVRdude.OK = true;
		
		//数据校验
		return null;
	}
	
	int NDK_Error()
	{
		/*
		string ext = VexProtocol.NDKLen + ": ";
		for( int i = 0; i < VexProtocol.NDKLen; ++i ) {
			ext += VexProtocol.NDKList[i] + ", ";
		}
		MessageBox.Show( ext );
		MessageBox.Show( "<" + n_InterNumberList.InterNumberList.UseList + ">" );
		*/
		
		string[] uselist = n_InterNumberList.InterNumberList.UseList.Trim( ' ' ).Split( ' ' );
		if( uselist.Length == 0 ) {
			return -1;
		}
		for( int i = 0; i < uselist.Length; ++i ) {
			bool isok = false;
			int uid = int.Parse( uselist[i] );
			if( uid < 0x10000 ) {
				continue;
			}
			uid -= 0x10000;
			for( int j = 0; j < VexProtocol.NDKLen; j += 2 ) {
				if( uid >= VexProtocol.NDKList[j] && uid <= VexProtocol.NDKList[j + 1] ) {
					isok = true;
					break;
				}
			}
			if( !isok ) {
				return uid;
			}
		}
		return -1;
	}
	
	//等待经过 n*100 毫秒后没有数据则返回失败
	bool WaitReceiveError_Start( byte Tar, int time )
	{
		Tick = 0;
		int lasttick = -10;
		do {
			if( Tick > time ) {
				Result += "没有回应数据, 结束下载.";
				return true;
			}
			if( Tick > 10 && !Visible ) {
				Visible = true;
			}
			if( Tick - lasttick > 5 ) {
				
				//----------------------------------------
				//读取版本信息
				VexProtocol.ClearReType();
				BufferHead[0] = 0xAA;
				BufferHead[1] = 0x01;
				BufferHead[2] = 0x00;
				BufferHead[3] = 0xFF;
				port.Write( BufferHead, 0, 4 );
				
				lasttick = Tick;
			}
			if( buttonOk.Text.StartsWith( "等待" ) ) {
				buttonOk.Text = "等待按下主板复位键 " + (Tick/10) + "." + (Tick%10) + "s " + ">".PadLeft( (Tick/5)%4 + 1, '-' ).PadRight( 4, '-' );
			}
			
			Application.DoEvents();
		}
		while( VexProtocol.RE_Type != Tar );
		return false;
	}
	
	//等待经过 n*100 毫秒后没有数据则返回失败
	bool WaitReceiveError( byte Tar, int time )
	{
		Tick = 0;
		int lasttick = 0;
		while( VexProtocol.RE_Type != Tar ) {
			if( Tick > time ) {
				Result += "没有回应数据, 结束下载.";
				return true;
			}
			if( Tick != lasttick ) {
				lasttick = Tick;
			}
			Application.DoEvents();
		}
		return false;
	}
	
	void ReType( byte retype )
	{
		Result += retype.ToString( "X" ).PadLeft( 2, '0' ) + " ";
	}
	
	void DataReceived(object sender, EventArgs e)
	{
		try {
			int n = port.Read(ReadBuffer, 0, 100 );
			if( n == 0 ) {
				return;
			}
			for( int i = 0; i < n; ++i ) {
				byte b = ReadBuffer[ i ];
				
				//this.richTextBox1.Text += b.ToString( "X" ).PadLeft( 2, '0' ) + " ";
				//this.richTextBox2.Text += ((char)b).ToString();
				
				VexProtocol.Deal( b );
			}
			Result += VexProtocol.Result;
		}
		catch {
			
		}
	}
	
	int Tick;
	void Timer1Tick(object sender, EventArgs e)
	{
		Tick++;
	}
	
	void ButtonDriverClick(object sender, EventArgs e)
	{
		string fileToSelect = n_OS.OS.SystemRoot + "常见USB接口芯片驱动" + n_OS.OS.PATH_S + "新版CH341usb_com驱动.EXE";
		
		string args = string.Format("/Select, {0}", fileToSelect);
		System.Diagnostics.Process.Start( new System.Diagnostics.ProcessStartInfo("Explorer.exe", args ) );
	}
	
	void ButtonOpenVEXClick(object sender, EventArgs e)
	{
		string fileToSelect = n_OS.OS.SystemRoot + "vos" + n_OS.OS.PATH_S + "vos简介.doc";
		
		string args = string.Format("/Select, {0}", fileToSelect);
		System.Diagnostics.Process.Start( new System.Diagnostics.ProcessStartInfo("Explorer.exe", args ) );
	}
	
	//==============================================================================
	//导出机器码文件
	bool NeedEnglish = false;
	string TempText;
	string LastEPath;
	
	void ButtonExportBinClick(object sender, EventArgs e)
	{
		if( comboBoxExportBin.SelectedIndex == -1 ) {
			MessageBox.Show( "请先选择对应的的芯片型号" );
			return;
		}
		buttonExportBin.Text = "导出中...";
		buttonExportBin.Enabled = false;
		buttonExportBin.BackColor = Color.Gray;
		
		string ctype = comboBoxExportBin.Text;
		string ModFile = null;
		
		NeedEnglish = false;
		
		//根据芯片型号进行app安装处理
		if( ctype == n_VosVer.CHIP.STM32F103C8T6 ||
		    ctype == n_VosVer.CHIP.GD32VF103C8T6 ||
		    ctype == n_VosVer.CHIP.GD32VF103CBT6 ||
		    ctype == n_VosVer.CHIP.CH32V103R8T6
		  ) {
			ModFile = AppDomain.CurrentDomain.BaseDirectory + @"Resource\tools\vos\" + ctype + ".hex";
		}
		else if( ctype == n_VosVer.CHIP.N32WB031 || ctype == n_VosVer.CHIP.GD32W515 ) {
			ModFile = AppDomain.CurrentDomain.BaseDirectory + @"Resource\tools\vos\" + ctype + ".axf";
		}
		else if( ctype == n_VosVer.CHIP.hi3861 ) {
			ModFile = AppDomain.CurrentDomain.BaseDirectory + @"Resource\tools\vos\" + ctype + ".bin";
		}
		else if( ctype == n_VosVer.CHIP.TG7100C ) {
			ModFile = AppDomain.CurrentDomain.BaseDirectory + @"Resource\tools\vos\" + ctype + ".bin";
			NeedEnglish = true;
		}
		//以下为需要特殊处理的固件文件
		else if( ctype == n_VosVer.CHIP.AB32VG1 ) {
			string SrFile = AppDomain.CurrentDomain.BaseDirectory + @"Resource\tools\vos\" + ctype + ".bin";
			string tempFile = AppDomain.CurrentDomain.BaseDirectory + @"Resource\tools\AB32VG1\rtthread.bin";
			InstallApp( SrFile, tempFile );
			
			//命令行合并dcf文件 原版的不行, 必须要把 rtthread.xm 路径写全, 不知道为什么
			//CMD.Download( richTextBoxCMD, AppDomain.CurrentDomain.BaseDirectory + @"Resource\tools\AB32VG1\riscv32-elf-xmaker.exe", "-b rtthread.xm", "", "" );
			CMD.Download( richTextBoxCMD, AppDomain.CurrentDomain.BaseDirectory + @"Resource\tools\AB32VG1\riscv32-elf-xmaker.exe",
			             "-b \"" + AppDomain.CurrentDomain.BaseDirectory + "Resource\\tools\\AB32VG1\\rtthread.xm\"", "", "" );
			
			ModFile = AppDomain.CurrentDomain.BaseDirectory + @"Resource\tools\AB32VG1\rtthread.dcf";	
		}
		else {
			MessageBox.Show( "此类型芯片暂不支持导出机器码文件: " + ctype );
			buttonExportBin.Enabled = true;
			buttonExportBin.Text = "导出机器码文件";
			buttonExportBin.BackColor  = Color.FromArgb( 153, 204, 251 );
			return;
		}
		//保存文件对话框
		SaveFileDialog SaveFileDlg = new SaveFileDialog();
		SaveFileDlg.Filter = "机器码文件 | *" + Path.GetExtension( ModFile );
		SaveFileDlg.Title = "保存文件...";
		
		if( LastEPath != null ) {
			SaveFileDlg.InitialDirectory = Path.GetDirectoryName( LastEPath );
			SaveFileDlg.FileName = Path.GetFileName( LastEPath );
		}
		else {
			SaveFileDlg.InitialDirectory = BasePath;
			SaveFileDlg.FileName = FileName;
		}
		
		
		if( NeedEnglish ) {
			SaveFileDlg.FileName = "linkboy";
		}
		
		DialogResult dlgResult = SaveFileDlg.ShowDialog();
		if(dlgResult == DialogResult.OK) {
			string UserFilePath = SaveFileDlg.FileName;
			
			LastEPath = UserFilePath;
			
			if( NeedEnglish ) {
				for( int i = 0; i < UserFilePath.Length; ++i ) {
					if( ((int)UserFilePath[i]) > 127 ) {
						MessageBox.Show( "检测到保存的路径中有中文符号, 可能无法在烧录软件中打开, 建议换一个目录导出或者手工将此文件复制到纯英文目录下 (例如D盘根目录)" );
						break;
					}
				}
			}
			
			if( ctype == n_VosVer.CHIP.AB32VG1 ) {
				//复制文件...
				if( File.Exists( UserFilePath ) ) {
					File.Delete( UserFilePath );
				}
				File.Copy( ModFile, UserFilePath );
			}
			else {
				InstallApp( ModFile, UserFilePath );
			}
		}
		buttonExportBin.Enabled = true;
		buttonExportBin.Text = "导出机器码文件";
		buttonExportBin.BackColor  = Color.FromArgb( 153, 204, 251 );
	}
	
	//打开指定的vos模板并安装到指定的文件中
	void InstallApp( string ModFile, string TarFile )
	{
		//复制vos文件
		if( File.Exists( TarFile ) ) {
			
			//windows系统会给出是否替换提示
			//MessageBox.Show( "已存在同名文件, 请换一个文件名称: " + TarFile );
			//return;
			
			File.Delete( TarFile );
		}
		File.Copy( ModFile, TarFile );
		
		//安装App到指定vos文件
		CurrentVosFile = TarFile;
		ButtonSwitchClick( null, null );
	}
	
	void ButtonOpenFlashToolClick(object sender, EventArgs e)
	{
		string fileToSelect = n_OS.OS.SystemRoot + "vos" + n_OS.OS.PATH_S + "常用芯片固件写入方法和工具" + n_OS.OS.PATH_S + "STM32固件通用写入方法.doc";
		
		string args = string.Format("/Select, {0}", fileToSelect);
		System.Diagnostics.Process.Start( new System.Diagnostics.ProcessStartInfo("Explorer.exe", args ) );
	}
	
	void ComboBoxExportBinSelectedIndexChanged(object sender, EventArgs e)
	{
		if( comboBoxExportBin.Text == n_VosVer.CHIP.TG7100C ) {
			label4.Text = "注意: TG7100C的烧录工具软件不支持中文文件名, 请将 TG7100C_FlashEnv_106 文件夹复制到D盘或其他盘符根目录下再使用, 路径不能带有中文! 机器码文件导出时将会默认设置为英文文件名 (linkboy.bin)";
			label4.ForeColor = Color.OrangeRed;
		}
		else {
			label4.Text = TempText;
			label4.ForeColor = Color.MediumSeaGreen;
		}
	}
	
	//==============================================================================
	//安装到vos源码 / 安装到vos固件
	
	string CurrentVosFile;
	
	//复制App数据
	void ButtonCopyClick(object sender, EventArgs e)
	{
		try {
			System.Windows.Forms.Clipboard.SetText( richTextBoxApp.Text );
			this.Visible = false;
			UserClose = true;
		}
		catch (Exception ee) {
			try {
				System.Windows.Forms.Clipboard.SetText( richTextBoxApp.Text );
				this.Visible = false;
				UserClose = true;
			}
			catch (Exception ee1) {
				MessageBox.Show( "复制失败: " + ee1 );
			}
		}
	}
	
	//打开选择固件文件
	void ButtonOpenVosClick(object sender, EventArgs e)
	{
		OpenFileDialog open = new OpenFileDialog();
		open.Filter = "vos固件文件|*.*";
		open.Title = "请选择一个vos固件文件(.hex .bin .elf)";
		
		if( open.ShowDialog() == DialogResult.OK ) {
			CurrentVosFile = open.FileName;
			labelVosPath.Text = CurrentVosFile;
			this.CodeText.Text = HexCoder.Load( CurrentVosFile );
			n_wlibCom.wlibCom.SetFont( CodeText );
		}
		
	}
	
	//安装APP到选定的目录
	void ButtonSwitchClick(object sender, EventArgs e)
	{
		/*
		string res = "";
		for( int i = 0; i < n_ISPcommon.ISPcommon.CodeLength; i += 1 ) {
			byte b0 = n_ISPcommon.ISPcommon.CodeList[i];
			
			res += b0.ToString( "X" ).PadLeft( 2, '0' ) + " ";
			if( i % 16 == 15 ) {
				res += "\r\n";
			}
		}
		res += "\r\n\r\n";
		*/
		
		if( CurrentVosFile == null ) {
			MessageBox.Show( "请先打开vos固件文件" );
			return;
		}
		if( !File.Exists( CurrentVosFile ) ) {
			MessageBox.Show( "不存在的文件: " + CurrentVosFile );
			return;
		}
		
		buttonSwitch.Text = "安装中...";
		Refresh();
		
		this.CodeText.Text = HexCoder.Load( CurrentVosFile );
		
		int firstok = 0;
		for( int i = 0; i < n_HexCoder.HexCoder.CodeLength - 8; i += 4 ) {
			long data = 0;
			data += n_HexCoder.HexCoder.CodeList[i + 3];
			data <<= 8;
			data += n_HexCoder.HexCoder.CodeList[i + 2];
			data <<= 8;
			data += n_HexCoder.HexCoder.CodeList[i + 1];
			data <<= 8;
			data += n_HexCoder.HexCoder.CodeList[i + 0];
			
			if( firstok == 0 ) {
				if( data == 0x6B6E696C ) {
					firstok = 1;
				}
			}
			else {
				firstok = 0;
				if( data == 0x3A796F62 ) {
					firstok = 2;
				}
			}
			if( firstok == 2 ) {
				//MessageBox.Show( "CODE-ADDR: 0x" + (i+4).ToString( "X" ).PadLeft( 4, '0' ) );
				
				for( int j = 0; j < BLength; j++ ) {
					try {
						n_HexCoder.HexCoder.CodeList[i+4+j] = CodeList[j];
					}
					catch {
						MessageBox.Show( "安装失败! " + i + "," + j + " " + n_HexCoder.HexCoder.CodeList.Length );
						buttonSwitch.Text = "安装 App";
						return;
					}
					
				}
				break;
			}
		}
		if( firstok != 2 ) {
			MessageBox.Show( "安装失败! 固件中没有vos虚拟机" );
			buttonSwitch.Text = "安装 App";
			return;
		}
		richTextBoxOut.Text = HexCoder.Save( CurrentVosFile );
		
		n_wlibCom.wlibCom.SetFont( richTextBoxOut );
		
		buttonSwitch.Text = "安装 App";
	}
	
	public static bool isv = false;
	
	int ttt = 0;
	void Panel4MouseDown(object sender, MouseEventArgs e)
	{
		ttt++;
		if( ttt == 3 ) {
			buttonInstallSource.Visible = true;
			buttonInstallBin.Visible = true;
			button8266.Visible = true;
		}
	}
}
}

