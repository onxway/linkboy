﻿/*
 * 由SharpDevelop创建。
 * 用户： dell
 * 日期: 2015/11/15
 * 时间: 8:29
 * 
 * 要改变这种模板请点击 工具|选项|代码编写|编辑标准头文件
 */
namespace n_ExportForm
{
	partial class ExportForm
	{
		/// <summary>
		/// Designer variable used to keep track of non-visual components.
		/// </summary>
		private System.ComponentModel.IContainer components = null;
		
		/// <summary>
		/// Disposes resources used by the form.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing) {
				if (components != null) {
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}
		
		/// <summary>
		/// This method is required for Windows Forms designer support.
		/// Do not change the method contents inside the source code editor. The Forms designer might
		/// not be able to load this method if it was changed manually.
		/// </summary>
		private void InitializeComponent()
		{
			this.components = new System.ComponentModel.Container();
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ExportForm));
			this.panel1 = new System.Windows.Forms.Panel();
			this.panelStart = new System.Windows.Forms.Panel();
			this.panel2 = new System.Windows.Forms.Panel();
			this.label5 = new System.Windows.Forms.Label();
			this.richTextBoxCMD = new System.Windows.Forms.RichTextBox();
			this.panelInstallBin = new System.Windows.Forms.Panel();
			this.tabControl2 = new System.Windows.Forms.TabControl();
			this.tabPage6 = new System.Windows.Forms.TabPage();
			this.richTextBoxApp = new System.Windows.Forms.RichTextBox();
			this.tabPage7 = new System.Windows.Forms.TabPage();
			this.CodeText = new System.Windows.Forms.RichTextBox();
			this.tabPage8 = new System.Windows.Forms.TabPage();
			this.richTextBoxOut = new System.Windows.Forms.RichTextBox();
			this.panelInstallBinInner = new System.Windows.Forms.Panel();
			this.buttonOpenVos = new System.Windows.Forms.Button();
			this.buttonSwitch = new System.Windows.Forms.Button();
			this.label22 = new System.Windows.Forms.Label();
			this.label23 = new System.Windows.Forms.Label();
			this.label24 = new System.Windows.Forms.Label();
			this.label25 = new System.Windows.Forms.Label();
			this.labelVosPath = new System.Windows.Forms.Label();
			this.panelExportBin = new System.Windows.Forms.Panel();
			this.buttonOpenFlashTool = new System.Windows.Forms.Button();
			this.label4 = new System.Windows.Forms.Label();
			this.label3 = new System.Windows.Forms.Label();
			this.label2 = new System.Windows.Forms.Label();
			this.label1 = new System.Windows.Forms.Label();
			this.buttonExportBin = new System.Windows.Forms.Button();
			this.label27 = new System.Windows.Forms.Label();
			this.comboBoxExportBin = new System.Windows.Forms.ComboBox();
			this.checkBoxHide = new System.Windows.Forms.CheckBox();
			this.panelInstallSource = new System.Windows.Forms.Panel();
			this.buttonCopy = new System.Windows.Forms.Button();
			this.label19 = new System.Windows.Forms.Label();
			this.checkBoxDTR_RTS = new System.Windows.Forms.CheckBox();
			this.checkBoxHoldingPort = new System.Windows.Forms.CheckBox();
			this.panelMes = new System.Windows.Forms.Panel();
			this.labelBoardMes = new System.Windows.Forms.Label();
			this.label32 = new System.Windows.Forms.Label();
			this.labelVersion = new System.Windows.Forms.Label();
			this.labelMes = new System.Windows.Forms.Label();
			this.label29 = new System.Windows.Forms.Label();
			this.labelComMes = new System.Windows.Forms.Label();
			this.panelUart = new System.Windows.Forms.Panel();
			this.comboBoxPort = new System.Windows.Forms.ComboBox();
			this.label28 = new System.Windows.Forms.Label();
			this.label31 = new System.Windows.Forms.Label();
			this.comboBox波特率 = new System.Windows.Forms.ComboBox();
			this.label30 = new System.Windows.Forms.Label();
			this.buttonDriver = new System.Windows.Forms.Button();
			this.panelInstallButton = new System.Windows.Forms.Panel();
			this.buttonOk = new System.Windows.Forms.Button();
			this.buttonOpenVEX = new System.Windows.Forms.Button();
			this.panel4 = new System.Windows.Forms.Panel();
			this.buttonESP32C3 = new System.Windows.Forms.Button();
			this.label6 = new System.Windows.Forms.Label();
			this.buttonESP32 = new System.Windows.Forms.Button();
			this.buttonVosVer = new System.Windows.Forms.Button();
			this.buttonLinkboyPro = new System.Windows.Forms.Button();
			this.buttonPico = new System.Windows.Forms.Button();
			this.button8266 = new System.Windows.Forms.Button();
			this.buttonInstallSource = new System.Windows.Forms.Button();
			this.buttonInstallBin = new System.Windows.Forms.Button();
			this.buttonMicrobit = new System.Windows.Forms.Button();
			this.buttonExportBinFile = new System.Windows.Forms.Button();
			this.timer1 = new System.Windows.Forms.Timer(this.components);
			this.panel1.SuspendLayout();
			this.panelStart.SuspendLayout();
			this.panelInstallBin.SuspendLayout();
			this.tabControl2.SuspendLayout();
			this.tabPage6.SuspendLayout();
			this.tabPage7.SuspendLayout();
			this.tabPage8.SuspendLayout();
			this.panelInstallBinInner.SuspendLayout();
			this.panelExportBin.SuspendLayout();
			this.panelInstallSource.SuspendLayout();
			this.panelMes.SuspendLayout();
			this.panelUart.SuspendLayout();
			this.panelInstallButton.SuspendLayout();
			this.panel4.SuspendLayout();
			this.SuspendLayout();
			// 
			// panel1
			// 
			this.panel1.Controls.Add(this.panelStart);
			this.panel1.Controls.Add(this.richTextBoxCMD);
			this.panel1.Controls.Add(this.panelInstallBin);
			this.panel1.Controls.Add(this.panelExportBin);
			this.panel1.Controls.Add(this.checkBoxHide);
			this.panel1.Controls.Add(this.panelInstallSource);
			this.panel1.Controls.Add(this.checkBoxDTR_RTS);
			this.panel1.Controls.Add(this.checkBoxHoldingPort);
			this.panel1.Controls.Add(this.panelMes);
			this.panel1.Controls.Add(this.labelComMes);
			this.panel1.Controls.Add(this.panelUart);
			this.panel1.Controls.Add(this.panelInstallButton);
			this.panel1.Dock = System.Windows.Forms.DockStyle.Fill;
			this.panel1.Location = new System.Drawing.Point(220, 0);
			this.panel1.Name = "panel1";
			this.panel1.Size = new System.Drawing.Size(589, 577);
			this.panel1.TabIndex = 67;
			// 
			// panelStart
			// 
			this.panelStart.Controls.Add(this.panel2);
			this.panelStart.Controls.Add(this.label5);
			this.panelStart.Location = new System.Drawing.Point(600, 696);
			this.panelStart.Name = "panelStart";
			this.panelStart.Size = new System.Drawing.Size(560, 298);
			this.panelStart.TabIndex = 67;
			// 
			// panel2
			// 
			this.panel2.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("panel2.BackgroundImage")));
			this.panel2.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
			this.panel2.Location = new System.Drawing.Point(25, 84);
			this.panel2.Name = "panel2";
			this.panel2.Size = new System.Drawing.Size(98, 114);
			this.panel2.TabIndex = 64;
			// 
			// label5
			// 
			this.label5.BackColor = System.Drawing.Color.Transparent;
			this.label5.Font = new System.Drawing.Font("微软雅黑", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
			this.label5.ForeColor = System.Drawing.Color.SlateGray;
			this.label5.Location = new System.Drawing.Point(150, 9);
			this.label5.Name = "label5";
			this.label5.Size = new System.Drawing.Size(374, 261);
			this.label5.TabIndex = 63;
			this.label5.Text = "树莓派pico、micro:bit 开发板直接点击对应的页面烧录程序；\r\n\r\n其他开发板都通过 vos 串口协议页面下载（需要事先安装对应的vos固件）；\r\n\r\n" +
			"部分开发板也支持导出hex/bin等文件，并自行使用厂家烧录工具写入芯片。";
			this.label5.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// richTextBoxCMD
			// 
			this.richTextBoxCMD.BackColor = System.Drawing.Color.White;
			this.richTextBoxCMD.BorderStyle = System.Windows.Forms.BorderStyle.None;
			this.richTextBoxCMD.Font = new System.Drawing.Font("Courier New", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.richTextBoxCMD.Location = new System.Drawing.Point(22, 333);
			this.richTextBoxCMD.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
			this.richTextBoxCMD.Name = "richTextBoxCMD";
			this.richTextBoxCMD.ReadOnly = true;
			this.richTextBoxCMD.Size = new System.Drawing.Size(560, 51);
			this.richTextBoxCMD.TabIndex = 48;
			this.richTextBoxCMD.Text = "";
			// 
			// panelInstallBin
			// 
			this.panelInstallBin.Controls.Add(this.tabControl2);
			this.panelInstallBin.Controls.Add(this.panelInstallBinInner);
			this.panelInstallBin.Location = new System.Drawing.Point(600, 281);
			this.panelInstallBin.Name = "panelInstallBin";
			this.panelInstallBin.Size = new System.Drawing.Size(560, 384);
			this.panelInstallBin.TabIndex = 65;
			// 
			// tabControl2
			// 
			this.tabControl2.Controls.Add(this.tabPage6);
			this.tabControl2.Controls.Add(this.tabPage7);
			this.tabControl2.Controls.Add(this.tabPage8);
			this.tabControl2.Dock = System.Windows.Forms.DockStyle.Fill;
			this.tabControl2.Font = new System.Drawing.Font("微软雅黑", 10.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
			this.tabControl2.Location = new System.Drawing.Point(0, 215);
			this.tabControl2.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
			this.tabControl2.Name = "tabControl2";
			this.tabControl2.SelectedIndex = 0;
			this.tabControl2.Size = new System.Drawing.Size(560, 169);
			this.tabControl2.TabIndex = 22;
			// 
			// tabPage6
			// 
			this.tabPage6.Controls.Add(this.richTextBoxApp);
			this.tabPage6.Location = new System.Drawing.Point(4, 29);
			this.tabPage6.Name = "tabPage6";
			this.tabPage6.Padding = new System.Windows.Forms.Padding(3);
			this.tabPage6.Size = new System.Drawing.Size(552, 136);
			this.tabPage6.TabIndex = 4;
			this.tabPage6.Text = "App文件";
			this.tabPage6.UseVisualStyleBackColor = true;
			// 
			// richTextBoxApp
			// 
			this.richTextBoxApp.BackColor = System.Drawing.Color.White;
			this.richTextBoxApp.BorderStyle = System.Windows.Forms.BorderStyle.None;
			this.richTextBoxApp.Dock = System.Windows.Forms.DockStyle.Fill;
			this.richTextBoxApp.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.richTextBoxApp.Location = new System.Drawing.Point(3, 3);
			this.richTextBoxApp.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
			this.richTextBoxApp.Name = "richTextBoxApp";
			this.richTextBoxApp.ReadOnly = true;
			this.richTextBoxApp.Size = new System.Drawing.Size(546, 130);
			this.richTextBoxApp.TabIndex = 17;
			this.richTextBoxApp.Text = "";
			// 
			// tabPage7
			// 
			this.tabPage7.Controls.Add(this.CodeText);
			this.tabPage7.Location = new System.Drawing.Point(4, 29);
			this.tabPage7.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
			this.tabPage7.Name = "tabPage7";
			this.tabPage7.Padding = new System.Windows.Forms.Padding(3, 2, 3, 2);
			this.tabPage7.Size = new System.Drawing.Size(552, 136);
			this.tabPage7.TabIndex = 2;
			this.tabPage7.Text = "vos固件";
			this.tabPage7.UseVisualStyleBackColor = true;
			// 
			// CodeText
			// 
			this.CodeText.BackColor = System.Drawing.Color.White;
			this.CodeText.BorderStyle = System.Windows.Forms.BorderStyle.None;
			this.CodeText.Dock = System.Windows.Forms.DockStyle.Fill;
			this.CodeText.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.CodeText.Location = new System.Drawing.Point(3, 2);
			this.CodeText.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
			this.CodeText.Name = "CodeText";
			this.CodeText.ReadOnly = true;
			this.CodeText.Size = new System.Drawing.Size(546, 132);
			this.CodeText.TabIndex = 15;
			this.CodeText.Text = "";
			// 
			// tabPage8
			// 
			this.tabPage8.Controls.Add(this.richTextBoxOut);
			this.tabPage8.Location = new System.Drawing.Point(4, 29);
			this.tabPage8.Name = "tabPage8";
			this.tabPage8.Padding = new System.Windows.Forms.Padding(3);
			this.tabPage8.Size = new System.Drawing.Size(552, 136);
			this.tabPage8.TabIndex = 3;
			this.tabPage8.Text = "输出固件";
			this.tabPage8.UseVisualStyleBackColor = true;
			// 
			// richTextBoxOut
			// 
			this.richTextBoxOut.BackColor = System.Drawing.Color.White;
			this.richTextBoxOut.BorderStyle = System.Windows.Forms.BorderStyle.None;
			this.richTextBoxOut.Dock = System.Windows.Forms.DockStyle.Fill;
			this.richTextBoxOut.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.richTextBoxOut.Location = new System.Drawing.Point(3, 3);
			this.richTextBoxOut.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
			this.richTextBoxOut.Name = "richTextBoxOut";
			this.richTextBoxOut.ReadOnly = true;
			this.richTextBoxOut.Size = new System.Drawing.Size(546, 130);
			this.richTextBoxOut.TabIndex = 16;
			this.richTextBoxOut.Text = "";
			// 
			// panelInstallBinInner
			// 
			this.panelInstallBinInner.Controls.Add(this.buttonOpenVos);
			this.panelInstallBinInner.Controls.Add(this.buttonSwitch);
			this.panelInstallBinInner.Controls.Add(this.label22);
			this.panelInstallBinInner.Controls.Add(this.label23);
			this.panelInstallBinInner.Controls.Add(this.label24);
			this.panelInstallBinInner.Controls.Add(this.label25);
			this.panelInstallBinInner.Controls.Add(this.labelVosPath);
			this.panelInstallBinInner.Dock = System.Windows.Forms.DockStyle.Top;
			this.panelInstallBinInner.Font = new System.Drawing.Font("微软雅黑", 12F);
			this.panelInstallBinInner.Location = new System.Drawing.Point(0, 0);
			this.panelInstallBinInner.Name = "panelInstallBinInner";
			this.panelInstallBinInner.Size = new System.Drawing.Size(560, 215);
			this.panelInstallBinInner.TabIndex = 23;
			// 
			// buttonOpenVos
			// 
			this.buttonOpenVos.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(224)))), ((int)(((byte)(192)))));
			this.buttonOpenVos.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
			this.buttonOpenVos.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
			this.buttonOpenVos.Font = new System.Drawing.Font("微软雅黑", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
			this.buttonOpenVos.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(64)))), ((int)(((byte)(0)))));
			this.buttonOpenVos.Location = new System.Drawing.Point(90, 90);
			this.buttonOpenVos.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
			this.buttonOpenVos.Name = "buttonOpenVos";
			this.buttonOpenVos.Size = new System.Drawing.Size(178, 46);
			this.buttonOpenVos.TabIndex = 36;
			this.buttonOpenVos.Text = "选择 vos 虚拟机固件";
			this.buttonOpenVos.UseVisualStyleBackColor = false;
			this.buttonOpenVos.Click += new System.EventHandler(this.ButtonOpenVosClick);
			// 
			// buttonSwitch
			// 
			this.buttonSwitch.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(153)))), ((int)(((byte)(204)))), ((int)(((byte)(251)))));
			this.buttonSwitch.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
			this.buttonSwitch.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
			this.buttonSwitch.Font = new System.Drawing.Font("微软雅黑", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
			this.buttonSwitch.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(27)))), ((int)(((byte)(87)))), ((int)(((byte)(156)))));
			this.buttonSwitch.Location = new System.Drawing.Point(90, 154);
			this.buttonSwitch.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
			this.buttonSwitch.Name = "buttonSwitch";
			this.buttonSwitch.Size = new System.Drawing.Size(178, 46);
			this.buttonSwitch.TabIndex = 37;
			this.buttonSwitch.Text = "安装 App";
			this.buttonSwitch.UseVisualStyleBackColor = false;
			this.buttonSwitch.Click += new System.EventHandler(this.ButtonSwitchClick);
			// 
			// label22
			// 
			this.label22.BackColor = System.Drawing.Color.Transparent;
			this.label22.Font = new System.Drawing.Font("微软雅黑", 12F);
			this.label22.ForeColor = System.Drawing.Color.LightSlateGray;
			this.label22.Location = new System.Drawing.Point(257, 90);
			this.label22.Name = "label22";
			this.label22.Size = new System.Drawing.Size(294, 46);
			this.label22.TabIndex = 42;
			this.label22.Text = "支持  *.hex  *.bin  *.elf  *.axf 类型";
			this.label22.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
			// 
			// label23
			// 
			this.label23.BackColor = System.Drawing.Color.Transparent;
			this.label23.ForeColor = System.Drawing.Color.SlateGray;
			this.label23.Location = new System.Drawing.Point(5, 166);
			this.label23.Name = "label23";
			this.label23.Size = new System.Drawing.Size(89, 23);
			this.label23.TabIndex = 41;
			this.label23.Text = "第 2 步：";
			this.label23.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
			// 
			// label24
			// 
			this.label24.BackColor = System.Drawing.Color.Transparent;
			this.label24.Font = new System.Drawing.Font("微软雅黑", 12F);
			this.label24.ForeColor = System.Drawing.Color.SlateGray;
			this.label24.Location = new System.Drawing.Point(5, 102);
			this.label24.Name = "label24";
			this.label24.Size = new System.Drawing.Size(89, 23);
			this.label24.TabIndex = 40;
			this.label24.Text = "第 1 步：";
			this.label24.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
			// 
			// label25
			// 
			this.label25.BackColor = System.Drawing.Color.Transparent;
			this.label25.Font = new System.Drawing.Font("微软雅黑", 12F);
			this.label25.ForeColor = System.Drawing.Color.LightSlateGray;
			this.label25.Location = new System.Drawing.Point(274, 154);
			this.label25.Name = "label25";
			this.label25.Size = new System.Drawing.Size(257, 46);
			this.label25.TabIndex = 39;
			this.label25.Text = "安装 APP 时将自动重新载入 vos 虚拟机固件文件";
			this.label25.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
			// 
			// labelVosPath
			// 
			this.labelVosPath.BackColor = System.Drawing.Color.WhiteSmoke;
			this.labelVosPath.Font = new System.Drawing.Font("微软雅黑", 10.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
			this.labelVosPath.ForeColor = System.Drawing.Color.SlateGray;
			this.labelVosPath.Location = new System.Drawing.Point(5, 11);
			this.labelVosPath.Name = "labelVosPath";
			this.labelVosPath.Size = new System.Drawing.Size(552, 61);
			this.labelVosPath.TabIndex = 38;
			this.labelVosPath.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
			// 
			// panelExportBin
			// 
			this.panelExportBin.Controls.Add(this.buttonOpenFlashTool);
			this.panelExportBin.Controls.Add(this.label4);
			this.panelExportBin.Controls.Add(this.label3);
			this.panelExportBin.Controls.Add(this.label2);
			this.panelExportBin.Controls.Add(this.label1);
			this.panelExportBin.Controls.Add(this.buttonExportBin);
			this.panelExportBin.Controls.Add(this.label27);
			this.panelExportBin.Controls.Add(this.comboBoxExportBin);
			this.panelExportBin.Location = new System.Drawing.Point(22, 563);
			this.panelExportBin.Name = "panelExportBin";
			this.panelExportBin.Size = new System.Drawing.Size(560, 385);
			this.panelExportBin.TabIndex = 64;
			// 
			// buttonOpenFlashTool
			// 
			this.buttonOpenFlashTool.BackColor = System.Drawing.Color.WhiteSmoke;
			this.buttonOpenFlashTool.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
			this.buttonOpenFlashTool.Font = new System.Drawing.Font("微软雅黑", 12F);
			this.buttonOpenFlashTool.ForeColor = System.Drawing.Color.DimGray;
			this.buttonOpenFlashTool.Location = new System.Drawing.Point(163, 336);
			this.buttonOpenFlashTool.Name = "buttonOpenFlashTool";
			this.buttonOpenFlashTool.Size = new System.Drawing.Size(227, 35);
			this.buttonOpenFlashTool.TabIndex = 45;
			this.buttonOpenFlashTool.Text = "打开自带烧录软件目录";
			this.buttonOpenFlashTool.UseVisualStyleBackColor = false;
			this.buttonOpenFlashTool.Click += new System.EventHandler(this.ButtonOpenFlashToolClick);
			// 
			// label4
			// 
			this.label4.Font = new System.Drawing.Font("微软雅黑", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
			this.label4.ForeColor = System.Drawing.Color.MediumSeaGreen;
			this.label4.Location = new System.Drawing.Point(53, 222);
			this.label4.Name = "label4";
			this.label4.Size = new System.Drawing.Size(458, 109);
			this.label4.TabIndex = 67;
			this.label4.Text = "有些第三方烧录软件可能不支持中文的文件名。如烧录失败，可尝试将固件文件复制到硬盘根目录下并改为英文名称。更多芯片支持请关注后续更新。";
			this.label4.TextAlign = System.Drawing.ContentAlignment.TopCenter;
			// 
			// label3
			// 
			this.label3.Font = new System.Drawing.Font("微软雅黑", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
			this.label3.ForeColor = System.Drawing.Color.DodgerBlue;
			this.label3.Location = new System.Drawing.Point(54, 21);
			this.label3.Name = "label3";
			this.label3.Size = new System.Drawing.Size(441, 28);
			this.label3.TabIndex = 66;
			this.label3.Text = "注意选择的芯片型号要和图形界面的主板芯片型号一致";
			this.label3.TextAlign = System.Drawing.ContentAlignment.TopCenter;
			// 
			// label2
			// 
			this.label2.Font = new System.Drawing.Font("微软雅黑", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
			this.label2.ForeColor = System.Drawing.Color.LightSlateGray;
			this.label2.Location = new System.Drawing.Point(43, 176);
			this.label2.Name = "label2";
			this.label2.Size = new System.Drawing.Size(468, 69);
			this.label2.TabIndex = 65;
			this.label2.Text = "第3步 使用芯片厂家配套烧录软件将机器码文件写入芯片";
			// 
			// label1
			// 
			this.label1.Font = new System.Drawing.Font("微软雅黑", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
			this.label1.ForeColor = System.Drawing.Color.LightSlateGray;
			this.label1.Location = new System.Drawing.Point(43, 120);
			this.label1.Name = "label1";
			this.label1.Size = new System.Drawing.Size(218, 28);
			this.label1.TabIndex = 64;
			this.label1.Text = "第2步 导出hex/bin/elf文件";
			this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// buttonExportBin
			// 
			this.buttonExportBin.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(153)))), ((int)(((byte)(204)))), ((int)(((byte)(251)))));
			this.buttonExportBin.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
			this.buttonExportBin.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
			this.buttonExportBin.Font = new System.Drawing.Font("微软雅黑", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
			this.buttonExportBin.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(27)))), ((int)(((byte)(87)))), ((int)(((byte)(156)))));
			this.buttonExportBin.Location = new System.Drawing.Point(267, 111);
			this.buttonExportBin.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
			this.buttonExportBin.Name = "buttonExportBin";
			this.buttonExportBin.Size = new System.Drawing.Size(244, 46);
			this.buttonExportBin.TabIndex = 63;
			this.buttonExportBin.Text = "导出机器码文件";
			this.buttonExportBin.UseVisualStyleBackColor = false;
			this.buttonExportBin.Click += new System.EventHandler(this.ButtonExportBinClick);
			// 
			// label27
			// 
			this.label27.Font = new System.Drawing.Font("微软雅黑", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
			this.label27.ForeColor = System.Drawing.Color.LightSlateGray;
			this.label27.Location = new System.Drawing.Point(43, 65);
			this.label27.Name = "label27";
			this.label27.Size = new System.Drawing.Size(218, 28);
			this.label27.TabIndex = 52;
			this.label27.Text = "第1步 选择芯片型号";
			this.label27.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// comboBoxExportBin
			// 
			this.comboBoxExportBin.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
			this.comboBoxExportBin.Font = new System.Drawing.Font("微软雅黑", 12F);
			this.comboBoxExportBin.FormattingEnabled = true;
			this.comboBoxExportBin.Items.AddRange(new object[] {
									"STM32F103C8T6",
									"GD32VF103C8T6",
									"GD32VF103CBT6",
									"GD32W515",
									"CH32V103R8T6",
									"AB32VG1",
									"N32WB031",
									"TG7100C"});
			this.comboBoxExportBin.Location = new System.Drawing.Point(267, 66);
			this.comboBoxExportBin.Name = "comboBoxExportBin";
			this.comboBoxExportBin.Size = new System.Drawing.Size(244, 29);
			this.comboBoxExportBin.TabIndex = 51;
			this.comboBoxExportBin.SelectedIndexChanged += new System.EventHandler(this.ComboBoxExportBinSelectedIndexChanged);
			// 
			// checkBoxHide
			// 
			this.checkBoxHide.Checked = true;
			this.checkBoxHide.CheckState = System.Windows.Forms.CheckState.Checked;
			this.checkBoxHide.Font = new System.Drawing.Font("微软雅黑", 10.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
			this.checkBoxHide.ForeColor = System.Drawing.Color.Gray;
			this.checkBoxHide.Location = new System.Drawing.Point(1204, 383);
			this.checkBoxHide.Name = "checkBoxHide";
			this.checkBoxHide.Size = new System.Drawing.Size(212, 29);
			this.checkBoxHide.TabIndex = 33;
			this.checkBoxHide.Text = "不再出现对话框，直接下载";
			this.checkBoxHide.UseVisualStyleBackColor = true;
			// 
			// panelInstallSource
			// 
			this.panelInstallSource.Controls.Add(this.buttonCopy);
			this.panelInstallSource.Controls.Add(this.label19);
			this.panelInstallSource.Location = new System.Drawing.Point(600, 103);
			this.panelInstallSource.Name = "panelInstallSource";
			this.panelInstallSource.Size = new System.Drawing.Size(560, 172);
			this.panelInstallSource.TabIndex = 63;
			// 
			// buttonCopy
			// 
			this.buttonCopy.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(153)))), ((int)(((byte)(204)))), ((int)(((byte)(251)))));
			this.buttonCopy.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
			this.buttonCopy.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
			this.buttonCopy.Font = new System.Drawing.Font("微软雅黑", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
			this.buttonCopy.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(27)))), ((int)(((byte)(87)))), ((int)(((byte)(156)))));
			this.buttonCopy.Location = new System.Drawing.Point(191, 22);
			this.buttonCopy.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
			this.buttonCopy.Name = "buttonCopy";
			this.buttonCopy.Size = new System.Drawing.Size(154, 46);
			this.buttonCopy.TabIndex = 61;
			this.buttonCopy.Text = "复制App数据";
			this.buttonCopy.UseVisualStyleBackColor = false;
			this.buttonCopy.Click += new System.EventHandler(this.ButtonCopyClick);
			// 
			// label19
			// 
			this.label19.BackColor = System.Drawing.Color.Transparent;
			this.label19.Font = new System.Drawing.Font("微软雅黑", 12F);
			this.label19.ForeColor = System.Drawing.Color.SlateGray;
			this.label19.Location = new System.Drawing.Point(25, 76);
			this.label19.Name = "label19";
			this.label19.Size = new System.Drawing.Size(506, 96);
			this.label19.TabIndex = 62;
			this.label19.Text = "先确保已安装vos到第三方IDE目录下，复制App数据后粘贴到vos文件夹的 user/app.h，并使用第三方IDE编译生成hex文件，下载到芯片即可。";
			this.label19.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// checkBoxDTR_RTS
			// 
			this.checkBoxDTR_RTS.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
			this.checkBoxDTR_RTS.ForeColor = System.Drawing.Color.Gray;
			this.checkBoxDTR_RTS.Location = new System.Drawing.Point(1204, 317);
			this.checkBoxDTR_RTS.Name = "checkBoxDTR_RTS";
			this.checkBoxDTR_RTS.Size = new System.Drawing.Size(104, 24);
			this.checkBoxDTR_RTS.TabIndex = 45;
			this.checkBoxDTR_RTS.Text = "打开DTR/RTS";
			this.checkBoxDTR_RTS.UseVisualStyleBackColor = true;
			this.checkBoxDTR_RTS.Visible = false;
			// 
			// checkBoxHoldingPort
			// 
			this.checkBoxHoldingPort.Font = new System.Drawing.Font("微软雅黑", 9F);
			this.checkBoxHoldingPort.ForeColor = System.Drawing.Color.Gray;
			this.checkBoxHoldingPort.Location = new System.Drawing.Point(1204, 347);
			this.checkBoxHoldingPort.Name = "checkBoxHoldingPort";
			this.checkBoxHoldingPort.Size = new System.Drawing.Size(173, 30);
			this.checkBoxHoldingPort.TabIndex = 46;
			this.checkBoxHoldingPort.Text = "下载后保持串口打开状态";
			this.checkBoxHoldingPort.UseVisualStyleBackColor = true;
			this.checkBoxHoldingPort.Visible = false;
			// 
			// panelMes
			// 
			this.panelMes.BackColor = System.Drawing.Color.WhiteSmoke;
			this.panelMes.Controls.Add(this.labelBoardMes);
			this.panelMes.Controls.Add(this.label32);
			this.panelMes.Controls.Add(this.labelVersion);
			this.panelMes.Controls.Add(this.labelMes);
			this.panelMes.Controls.Add(this.label29);
			this.panelMes.Location = new System.Drawing.Point(22, 387);
			this.panelMes.Name = "panelMes";
			this.panelMes.Size = new System.Drawing.Size(560, 168);
			this.panelMes.TabIndex = 40;
			// 
			// labelBoardMes
			// 
			this.labelBoardMes.BackColor = System.Drawing.Color.Gainsboro;
			this.labelBoardMes.Font = new System.Drawing.Font("微软雅黑", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
			this.labelBoardMes.ForeColor = System.Drawing.Color.DarkOrchid;
			this.labelBoardMes.Location = new System.Drawing.Point(82, 55);
			this.labelBoardMes.Name = "labelBoardMes";
			this.labelBoardMes.Size = new System.Drawing.Size(473, 34);
			this.labelBoardMes.TabIndex = 26;
			this.labelBoardMes.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
			// 
			// label32
			// 
			this.label32.Font = new System.Drawing.Font("微软雅黑", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
			this.label32.ForeColor = System.Drawing.Color.SlateGray;
			this.label32.Location = new System.Drawing.Point(3, 55);
			this.label32.Name = "label32";
			this.label32.Size = new System.Drawing.Size(108, 34);
			this.label32.TabIndex = 43;
			this.label32.Text = "硬件信息：";
			this.label32.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// labelVersion
			// 
			this.labelVersion.BackColor = System.Drawing.Color.Gainsboro;
			this.labelVersion.Font = new System.Drawing.Font("微软雅黑", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
			this.labelVersion.ForeColor = System.Drawing.Color.DarkOrchid;
			this.labelVersion.Location = new System.Drawing.Point(82, 12);
			this.labelVersion.Name = "labelVersion";
			this.labelVersion.Size = new System.Drawing.Size(473, 34);
			this.labelVersion.TabIndex = 0;
			this.labelVersion.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
			// 
			// labelMes
			// 
			this.labelMes.Font = new System.Drawing.Font("微软雅黑", 10F);
			this.labelMes.ForeColor = System.Drawing.Color.DarkRed;
			this.labelMes.Location = new System.Drawing.Point(3, 103);
			this.labelMes.Name = "labelMes";
			this.labelMes.Size = new System.Drawing.Size(552, 55);
			this.labelMes.TabIndex = 42;
			this.labelMes.TextAlign = System.Drawing.ContentAlignment.TopCenter;
			// 
			// label29
			// 
			this.label29.Font = new System.Drawing.Font("微软雅黑", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
			this.label29.ForeColor = System.Drawing.Color.SlateGray;
			this.label29.Location = new System.Drawing.Point(3, 12);
			this.label29.Name = "label29";
			this.label29.Size = new System.Drawing.Size(108, 34);
			this.label29.TabIndex = 24;
			this.label29.Text = "vb版本：";
			this.label29.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// labelComMes
			// 
			this.labelComMes.BackColor = System.Drawing.Color.Transparent;
			this.labelComMes.Font = new System.Drawing.Font("微软雅黑", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
			this.labelComMes.ForeColor = System.Drawing.Color.MediumSeaGreen;
			this.labelComMes.Location = new System.Drawing.Point(22, 253);
			this.labelComMes.Name = "labelComMes";
			this.labelComMes.Size = new System.Drawing.Size(560, 73);
			this.labelComMes.TabIndex = 66;
			this.labelComMes.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
			// 
			// panelUart
			// 
			this.panelUart.Controls.Add(this.comboBoxPort);
			this.panelUart.Controls.Add(this.label28);
			this.panelUart.Controls.Add(this.label31);
			this.panelUart.Controls.Add(this.comboBox波特率);
			this.panelUart.Controls.Add(this.label30);
			this.panelUart.Controls.Add(this.buttonDriver);
			this.panelUart.Location = new System.Drawing.Point(22, 114);
			this.panelUart.Name = "panelUart";
			this.panelUart.Size = new System.Drawing.Size(560, 136);
			this.panelUart.TabIndex = 58;
			// 
			// comboBoxPort
			// 
			this.comboBoxPort.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
			this.comboBoxPort.Font = new System.Drawing.Font("微软雅黑", 10F);
			this.comboBoxPort.ForeColor = System.Drawing.Color.Blue;
			this.comboBoxPort.FormattingEnabled = true;
			this.comboBoxPort.Location = new System.Drawing.Point(120, 19);
			this.comboBoxPort.Name = "comboBoxPort";
			this.comboBoxPort.Size = new System.Drawing.Size(388, 27);
			this.comboBoxPort.TabIndex = 30;
			this.comboBoxPort.Click += new System.EventHandler(this.ComboBoxPortClick);
			// 
			// label28
			// 
			this.label28.Font = new System.Drawing.Font("微软雅黑", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
			this.label28.ForeColor = System.Drawing.Color.LightSlateGray;
			this.label28.Location = new System.Drawing.Point(41, 89);
			this.label28.Name = "label28";
			this.label28.Size = new System.Drawing.Size(283, 28);
			this.label28.TabIndex = 43;
			this.label28.Text = "如没有串口号，请先安装相应驱动 ->";
			this.label28.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
			// 
			// label31
			// 
			this.label31.Font = new System.Drawing.Font("微软雅黑", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
			this.label31.ForeColor = System.Drawing.Color.Black;
			this.label31.Location = new System.Drawing.Point(45, 19);
			this.label31.Name = "label31";
			this.label31.Size = new System.Drawing.Size(73, 29);
			this.label31.TabIndex = 31;
			this.label31.Text = "串口号";
			this.label31.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// comboBox波特率
			// 
			this.comboBox波特率.Font = new System.Drawing.Font("微软雅黑", 11F);
			this.comboBox波特率.ForeColor = System.Drawing.Color.DarkGray;
			this.comboBox波特率.FormattingEnabled = true;
			this.comboBox波特率.Items.AddRange(new object[] {
									"1200",
									"2400",
									"4800",
									"9600",
									"19200",
									"38400",
									"57600",
									"76800",
									"115200",
									"921600"});
			this.comboBox波特率.Location = new System.Drawing.Point(330, 52);
			this.comboBox波特率.Name = "comboBox波特率";
			this.comboBox波特率.Size = new System.Drawing.Size(178, 28);
			this.comboBox波特率.TabIndex = 38;
			// 
			// label30
			// 
			this.label30.Font = new System.Drawing.Font("微软雅黑", 12F);
			this.label30.ForeColor = System.Drawing.Color.LightSlateGray;
			this.label30.Location = new System.Drawing.Point(251, 52);
			this.label30.Name = "label30";
			this.label30.Size = new System.Drawing.Size(73, 29);
			this.label30.TabIndex = 39;
			this.label30.Text = "波特率";
			this.label30.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
			// 
			// buttonDriver
			// 
			this.buttonDriver.BackColor = System.Drawing.Color.WhiteSmoke;
			this.buttonDriver.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
			this.buttonDriver.Font = new System.Drawing.Font("微软雅黑", 12F);
			this.buttonDriver.ForeColor = System.Drawing.Color.DimGray;
			this.buttonDriver.Location = new System.Drawing.Point(330, 86);
			this.buttonDriver.Name = "buttonDriver";
			this.buttonDriver.Size = new System.Drawing.Size(178, 35);
			this.buttonDriver.TabIndex = 44;
			this.buttonDriver.Text = "打开自带驱动目录";
			this.buttonDriver.UseVisualStyleBackColor = false;
			this.buttonDriver.Click += new System.EventHandler(this.ButtonDriverClick);
			// 
			// panelInstallButton
			// 
			this.panelInstallButton.Controls.Add(this.buttonOk);
			this.panelInstallButton.Location = new System.Drawing.Point(22, 8);
			this.panelInstallButton.Name = "panelInstallButton";
			this.panelInstallButton.Size = new System.Drawing.Size(560, 100);
			this.panelInstallButton.TabIndex = 59;
			// 
			// buttonOk
			// 
			this.buttonOk.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(153)))), ((int)(((byte)(204)))), ((int)(((byte)(251)))));
			this.buttonOk.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
			this.buttonOk.Font = new System.Drawing.Font("微软雅黑", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
			this.buttonOk.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(27)))), ((int)(((byte)(87)))), ((int)(((byte)(156)))));
			this.buttonOk.Location = new System.Drawing.Point(45, 18);
			this.buttonOk.Name = "buttonOk";
			this.buttonOk.Size = new System.Drawing.Size(467, 60);
			this.buttonOk.TabIndex = 0;
			this.buttonOk.Text = "开始下载";
			this.buttonOk.UseVisualStyleBackColor = false;
			this.buttonOk.Click += new System.EventHandler(this.ButtonOkClick);
			this.buttonOk.MouseDown += new System.Windows.Forms.MouseEventHandler(this.ButtonOkMouseDown);
			// 
			// buttonOpenVEX
			// 
			this.buttonOpenVEX.BackColor = System.Drawing.Color.WhiteSmoke;
			this.buttonOpenVEX.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
			this.buttonOpenVEX.Font = new System.Drawing.Font("微软雅黑", 12F);
			this.buttonOpenVEX.ForeColor = System.Drawing.Color.DimGray;
			this.buttonOpenVEX.ImeMode = System.Windows.Forms.ImeMode.NoControl;
			this.buttonOpenVEX.Location = new System.Drawing.Point(14, 361);
			this.buttonOpenVEX.Name = "buttonOpenVEX";
			this.buttonOpenVEX.Size = new System.Drawing.Size(191, 40);
			this.buttonOpenVEX.TabIndex = 103;
			this.buttonOpenVEX.Text = "其它芯片 烧录vos";
			this.buttonOpenVEX.UseVisualStyleBackColor = false;
			this.buttonOpenVEX.Click += new System.EventHandler(this.ButtonOpenVEXClick);
			// 
			// panel4
			// 
			this.panel4.BackColor = System.Drawing.Color.White;
			this.panel4.Controls.Add(this.buttonOpenVEX);
			this.panel4.Controls.Add(this.buttonESP32C3);
			this.panel4.Controls.Add(this.label6);
			this.panel4.Controls.Add(this.buttonESP32);
			this.panel4.Controls.Add(this.buttonVosVer);
			this.panel4.Controls.Add(this.buttonLinkboyPro);
			this.panel4.Controls.Add(this.buttonPico);
			this.panel4.Controls.Add(this.button8266);
			this.panel4.Controls.Add(this.buttonInstallSource);
			this.panel4.Controls.Add(this.buttonInstallBin);
			this.panel4.Controls.Add(this.buttonMicrobit);
			this.panel4.Controls.Add(this.buttonExportBinFile);
			this.panel4.Dock = System.Windows.Forms.DockStyle.Left;
			this.panel4.Location = new System.Drawing.Point(0, 0);
			this.panel4.Name = "panel4";
			this.panel4.Size = new System.Drawing.Size(220, 577);
			this.panel4.TabIndex = 60;
			this.panel4.MouseDown += new System.Windows.Forms.MouseEventHandler(this.Panel4MouseDown);
			// 
			// buttonESP32C3
			// 
			this.buttonESP32C3.BackColor = System.Drawing.Color.WhiteSmoke;
			this.buttonESP32C3.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
			this.buttonESP32C3.Font = new System.Drawing.Font("微软雅黑", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
			this.buttonESP32C3.ForeColor = System.Drawing.Color.DimGray;
			this.buttonESP32C3.Location = new System.Drawing.Point(14, 315);
			this.buttonESP32C3.Name = "buttonESP32C3";
			this.buttonESP32C3.Size = new System.Drawing.Size(191, 40);
			this.buttonESP32C3.TabIndex = 69;
			this.buttonESP32C3.Text = "ESP32C3 烧录vos";
			this.buttonESP32C3.UseVisualStyleBackColor = false;
			this.buttonESP32C3.Click += new System.EventHandler(this.ButtonESP32C3Click);
			// 
			// label6
			// 
			this.label6.Font = new System.Drawing.Font("微软雅黑", 10.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
			this.label6.ForeColor = System.Drawing.Color.LightSlateGray;
			this.label6.Location = new System.Drawing.Point(3, 192);
			this.label6.Name = "label6";
			this.label6.Size = new System.Drawing.Size(211, 28);
			this.label6.TabIndex = 68;
			this.label6.Text = "↓新板首次使用需烧录vos↓";
			this.label6.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
			// 
			// buttonESP32
			// 
			this.buttonESP32.BackColor = System.Drawing.Color.WhiteSmoke;
			this.buttonESP32.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
			this.buttonESP32.Font = new System.Drawing.Font("微软雅黑", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
			this.buttonESP32.ForeColor = System.Drawing.Color.DimGray;
			this.buttonESP32.Location = new System.Drawing.Point(14, 269);
			this.buttonESP32.Name = "buttonESP32";
			this.buttonESP32.Size = new System.Drawing.Size(191, 40);
			this.buttonESP32.TabIndex = 59;
			this.buttonESP32.Text = "ESP32 烧录vos";
			this.buttonESP32.UseVisualStyleBackColor = false;
			this.buttonESP32.Click += new System.EventHandler(this.ButtonESP32Click);
			// 
			// buttonVosVer
			// 
			this.buttonVosVer.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
			this.buttonVosVer.BackColor = System.Drawing.Color.WhiteSmoke;
			this.buttonVosVer.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
			this.buttonVosVer.Font = new System.Drawing.Font("微软雅黑", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
			this.buttonVosVer.ForeColor = System.Drawing.Color.DimGray;
			this.buttonVosVer.Location = new System.Drawing.Point(14, 525);
			this.buttonVosVer.Name = "buttonVosVer";
			this.buttonVosVer.Size = new System.Drawing.Size(191, 40);
			this.buttonVosVer.TabIndex = 58;
			this.buttonVosVer.Text = "vos 固件版本";
			this.buttonVosVer.UseVisualStyleBackColor = false;
			this.buttonVosVer.Click += new System.EventHandler(this.ButtonVosVerClick);
			// 
			// buttonLinkboyPro
			// 
			this.buttonLinkboyPro.BackColor = System.Drawing.Color.WhiteSmoke;
			this.buttonLinkboyPro.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
			this.buttonLinkboyPro.Font = new System.Drawing.Font("微软雅黑", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
			this.buttonLinkboyPro.ForeColor = System.Drawing.Color.DodgerBlue;
			this.buttonLinkboyPro.Location = new System.Drawing.Point(14, 11);
			this.buttonLinkboyPro.Name = "buttonLinkboyPro";
			this.buttonLinkboyPro.Size = new System.Drawing.Size(191, 40);
			this.buttonLinkboyPro.TabIndex = 0;
			this.buttonLinkboyPro.Text = "vos 串口下载程序";
			this.buttonLinkboyPro.UseVisualStyleBackColor = false;
			this.buttonLinkboyPro.Click += new System.EventHandler(this.ButtonLinkboyProClick);
			// 
			// buttonPico
			// 
			this.buttonPico.BackColor = System.Drawing.Color.WhiteSmoke;
			this.buttonPico.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
			this.buttonPico.Font = new System.Drawing.Font("微软雅黑", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
			this.buttonPico.ForeColor = System.Drawing.Color.DimGray;
			this.buttonPico.Location = new System.Drawing.Point(14, 57);
			this.buttonPico.Name = "buttonPico";
			this.buttonPico.Size = new System.Drawing.Size(191, 40);
			this.buttonPico.TabIndex = 47;
			this.buttonPico.Text = "树莓派 Pico";
			this.buttonPico.UseVisualStyleBackColor = false;
			this.buttonPico.Click += new System.EventHandler(this.ButtonPicoClick);
			// 
			// button8266
			// 
			this.button8266.BackColor = System.Drawing.Color.WhiteSmoke;
			this.button8266.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
			this.button8266.Font = new System.Drawing.Font("微软雅黑", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
			this.button8266.ForeColor = System.Drawing.Color.DimGray;
			this.button8266.Location = new System.Drawing.Point(14, 223);
			this.button8266.Name = "button8266";
			this.button8266.Size = new System.Drawing.Size(191, 40);
			this.button8266.TabIndex = 51;
			this.button8266.Text = "ESP8266 烧录vos";
			this.button8266.UseVisualStyleBackColor = false;
			this.button8266.Click += new System.EventHandler(this.Button8266Click);
			// 
			// buttonInstallSource
			// 
			this.buttonInstallSource.BackColor = System.Drawing.Color.WhiteSmoke;
			this.buttonInstallSource.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
			this.buttonInstallSource.Font = new System.Drawing.Font("微软雅黑", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
			this.buttonInstallSource.ForeColor = System.Drawing.Color.DimGray;
			this.buttonInstallSource.Location = new System.Drawing.Point(14, 453);
			this.buttonInstallSource.Name = "buttonInstallSource";
			this.buttonInstallSource.Size = new System.Drawing.Size(191, 40);
			this.buttonInstallSource.TabIndex = 57;
			this.buttonInstallSource.Text = "外挂模式：源码";
			this.buttonInstallSource.UseVisualStyleBackColor = false;
			this.buttonInstallSource.Visible = false;
			this.buttonInstallSource.Click += new System.EventHandler(this.ButtonInstallSourceClick);
			// 
			// buttonInstallBin
			// 
			this.buttonInstallBin.BackColor = System.Drawing.Color.WhiteSmoke;
			this.buttonInstallBin.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
			this.buttonInstallBin.Font = new System.Drawing.Font("微软雅黑", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
			this.buttonInstallBin.ForeColor = System.Drawing.Color.DimGray;
			this.buttonInstallBin.Location = new System.Drawing.Point(14, 407);
			this.buttonInstallBin.Name = "buttonInstallBin";
			this.buttonInstallBin.Size = new System.Drawing.Size(191, 40);
			this.buttonInstallBin.TabIndex = 56;
			this.buttonInstallBin.Text = "外挂模式：固件";
			this.buttonInstallBin.UseVisualStyleBackColor = false;
			this.buttonInstallBin.Visible = false;
			this.buttonInstallBin.Click += new System.EventHandler(this.ButtonInstallBinClick);
			// 
			// buttonMicrobit
			// 
			this.buttonMicrobit.BackColor = System.Drawing.Color.WhiteSmoke;
			this.buttonMicrobit.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
			this.buttonMicrobit.Font = new System.Drawing.Font("微软雅黑", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
			this.buttonMicrobit.ForeColor = System.Drawing.Color.DimGray;
			this.buttonMicrobit.Location = new System.Drawing.Point(14, 103);
			this.buttonMicrobit.Name = "buttonMicrobit";
			this.buttonMicrobit.Size = new System.Drawing.Size(191, 40);
			this.buttonMicrobit.TabIndex = 53;
			this.buttonMicrobit.Text = "micro:bit";
			this.buttonMicrobit.UseVisualStyleBackColor = false;
			this.buttonMicrobit.Click += new System.EventHandler(this.ButtonMicrobitClick);
			// 
			// buttonExportBinFile
			// 
			this.buttonExportBinFile.BackColor = System.Drawing.Color.WhiteSmoke;
			this.buttonExportBinFile.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
			this.buttonExportBinFile.Font = new System.Drawing.Font("微软雅黑", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
			this.buttonExportBinFile.ForeColor = System.Drawing.Color.DimGray;
			this.buttonExportBinFile.Location = new System.Drawing.Point(14, 149);
			this.buttonExportBinFile.Name = "buttonExportBinFile";
			this.buttonExportBinFile.Size = new System.Drawing.Size(191, 40);
			this.buttonExportBinFile.TabIndex = 54;
			this.buttonExportBinFile.Text = "导出机器码文件";
			this.buttonExportBinFile.UseVisualStyleBackColor = false;
			this.buttonExportBinFile.Click += new System.EventHandler(this.ButtonExportBinFileClick);
			// 
			// timer1
			// 
			this.timer1.Tick += new System.EventHandler(this.Timer1Tick);
			// 
			// ExportForm
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.BackColor = System.Drawing.Color.WhiteSmoke;
			this.ClientSize = new System.Drawing.Size(809, 577);
			this.Controls.Add(this.panel1);
			this.Controls.Add(this.panel4);
			this.MaximizeBox = false;
			this.MinimizeBox = false;
			this.Name = "ExportForm";
			this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
			this.Text = "linkboy 程序下载模式选择";
			this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.AVRdudeSetFormFormClosing);
			this.panel1.ResumeLayout(false);
			this.panelStart.ResumeLayout(false);
			this.panelInstallBin.ResumeLayout(false);
			this.tabControl2.ResumeLayout(false);
			this.tabPage6.ResumeLayout(false);
			this.tabPage7.ResumeLayout(false);
			this.tabPage8.ResumeLayout(false);
			this.panelInstallBinInner.ResumeLayout(false);
			this.panelExportBin.ResumeLayout(false);
			this.panelInstallSource.ResumeLayout(false);
			this.panelMes.ResumeLayout(false);
			this.panelUart.ResumeLayout(false);
			this.panelInstallButton.ResumeLayout(false);
			this.panel4.ResumeLayout(false);
			this.ResumeLayout(false);
		}
		private System.Windows.Forms.Button buttonESP32C3;
		private System.Windows.Forms.Label label6;
		private System.Windows.Forms.Button buttonESP32;
		private System.Windows.Forms.Button buttonOpenFlashTool;
		private System.Windows.Forms.Label label5;
		private System.Windows.Forms.Panel panel2;
		private System.Windows.Forms.Panel panelStart;
		private System.Windows.Forms.Label label4;
		private System.Windows.Forms.Label label3;
		private System.Windows.Forms.Label label2;
		private System.Windows.Forms.Button buttonExportBin;
		private System.Windows.Forms.Label label1;
		private System.Windows.Forms.Button buttonVosVer;
		private System.Windows.Forms.Button buttonOpenVEX;
		private System.Windows.Forms.ComboBox comboBoxExportBin;
		private System.Windows.Forms.Label label27;
		private System.Windows.Forms.Panel panel1;
		private System.Windows.Forms.Label labelComMes;
		private System.Windows.Forms.Panel panelUart;
		private System.Windows.Forms.Panel panelInstallButton;
		private System.Windows.Forms.Button buttonExportBinFile;
		private System.Windows.Forms.Button buttonInstallBin;
		private System.Windows.Forms.Button buttonInstallSource;
		private System.Windows.Forms.Button buttonLinkboyPro;
		private System.Windows.Forms.Panel panel4;
		private System.Windows.Forms.Panel panelInstallSource;
		private System.Windows.Forms.Panel panelExportBin;
		private System.Windows.Forms.Panel panelInstallBin;
		private System.Windows.Forms.Button buttonMicrobit;
		private System.Windows.Forms.Button button8266;
		public System.Windows.Forms.RichTextBox richTextBoxCMD;
		private System.Windows.Forms.Button buttonCopy;
		private System.Windows.Forms.Button buttonPico;
		private System.Windows.Forms.CheckBox checkBoxDTR_RTS;
		private System.Windows.Forms.CheckBox checkBoxHoldingPort;
		private System.Windows.Forms.Label label32;
		private System.Windows.Forms.Timer timer1;
		private System.Windows.Forms.ComboBox comboBoxPort;
		private System.Windows.Forms.Label label31;
		private System.Windows.Forms.CheckBox checkBoxHide;
		private System.Windows.Forms.Button buttonOk;
		private System.Windows.Forms.ComboBox comboBox波特率;
		private System.Windows.Forms.Label label30;
		private System.Windows.Forms.Label label29;
		private System.Windows.Forms.Label labelVersion;
		private System.Windows.Forms.Label labelBoardMes;
		private System.Windows.Forms.Panel panelMes;
		private System.Windows.Forms.Label labelMes;
		private System.Windows.Forms.Button buttonDriver;
		private System.Windows.Forms.Label label28;
		private System.Windows.Forms.Label labelVosPath;
		private System.Windows.Forms.Label label25;
		private System.Windows.Forms.Label label24;
		private System.Windows.Forms.Label label23;
		private System.Windows.Forms.Label label22;
		private System.Windows.Forms.Button buttonSwitch;
		private System.Windows.Forms.Button buttonOpenVos;
		private System.Windows.Forms.Panel panelInstallBinInner;
		public System.Windows.Forms.RichTextBox richTextBoxOut;
		private System.Windows.Forms.TabPage tabPage8;
		public System.Windows.Forms.RichTextBox CodeText;
		private System.Windows.Forms.TabPage tabPage7;
		public System.Windows.Forms.RichTextBox richTextBoxApp;
		private System.Windows.Forms.TabPage tabPage6;
		private System.Windows.Forms.TabControl tabControl2;
		private System.Windows.Forms.Label label19;
	}
}
