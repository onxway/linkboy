﻿/*
 * 由SharpDevelop创建。
 * 用户： dell
 * 日期: 2016/9/23
 * 时间: 15:39
 * 
 * 要改变这种模板请点击 工具|选项|代码编写|编辑标准头文件
 */
using System;
using System.Drawing;
using System.Windows.Forms;

namespace winlib.MyColorForm
{
public partial class MyColorForm : Form
{
	bool isOK;
	
	//构造函数
	public MyColorForm()
	{
		InitializeComponent();	
	}
	
	//运行
	public Color Run()
	{
		isOK = false;
		
		//注意,直接设置Visible不能使窗体获得焦点
		//this.Visible = true;
		this.ShowDialog();
		
		while( this.Visible ) {
			System.Windows.Forms.Application.DoEvents();
		}
		if( isOK ) {
			return Color.Red;
		}
		return Color.Empty;
	}
	
	void MyColorFormFormClosing(object sender, FormClosingEventArgs e)
	{
		e.Cancel = true;
	}
	
	void ButtonOKClick(object sender, EventArgs e)
	{
		isOK = true;
		this.Visible = false;
	}
}
}


