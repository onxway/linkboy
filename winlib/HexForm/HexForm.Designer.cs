﻿namespace n_HexForm
{
    partial class HexForm
    {
        /// <summary>
        /// 必需的设计器变量。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// 清理所有正在使用的资源。
        /// </summary>
        /// <param name="disposing">如果应释放托管资源，为 true；否则为 false。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows 窗体设计器生成的代码

        /// <summary>
        /// 设计器支持所需的方法 - 不要
        /// 使用代码编辑器修改此方法的内容。
        /// </summary>
        private void InitializeComponent()
        {
        	System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(HexForm));
        	this.tabControl1 = new System.Windows.Forms.TabControl();
        	this.tabPage1 = new System.Windows.Forms.TabPage();
        	this.label3 = new System.Windows.Forms.Label();
        	this.label2 = new System.Windows.Forms.Label();
        	this.label1 = new System.Windows.Forms.Label();
        	this.labelVosPath = new System.Windows.Forms.Label();
        	this.buttonSwitch = new System.Windows.Forms.Button();
        	this.OpenFileButton = new System.Windows.Forms.Button();
        	this.tabPage4 = new System.Windows.Forms.TabPage();
        	this.richTextBoxApp = new System.Windows.Forms.RichTextBox();
        	this.buttonCopy = new System.Windows.Forms.Button();
        	this.tabPage3 = new System.Windows.Forms.TabPage();
        	this.CodeText = new System.Windows.Forms.RichTextBox();
        	this.tabPage2 = new System.Windows.Forms.TabPage();
        	this.richTextBoxOut = new System.Windows.Forms.RichTextBox();
        	this.label4 = new System.Windows.Forms.Label();
        	this.tabControl1.SuspendLayout();
        	this.tabPage1.SuspendLayout();
        	this.tabPage4.SuspendLayout();
        	this.tabPage3.SuspendLayout();
        	this.tabPage2.SuspendLayout();
        	this.SuspendLayout();
        	// 
        	// tabControl1
        	// 
        	this.tabControl1.Controls.Add(this.tabPage1);
        	this.tabControl1.Controls.Add(this.tabPage4);
        	this.tabControl1.Controls.Add(this.tabPage3);
        	this.tabControl1.Controls.Add(this.tabPage2);
        	this.tabControl1.Dock = System.Windows.Forms.DockStyle.Fill;
        	this.tabControl1.Font = new System.Drawing.Font("微软雅黑", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
        	this.tabControl1.Location = new System.Drawing.Point(0, 0);
        	this.tabControl1.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
        	this.tabControl1.Name = "tabControl1";
        	this.tabControl1.SelectedIndex = 0;
        	this.tabControl1.Size = new System.Drawing.Size(609, 393);
        	this.tabControl1.TabIndex = 21;
        	// 
        	// tabPage1
        	// 
        	this.tabPage1.BackColor = System.Drawing.Color.WhiteSmoke;
        	this.tabPage1.Controls.Add(this.OpenFileButton);
        	this.tabPage1.Controls.Add(this.buttonSwitch);
        	this.tabPage1.Controls.Add(this.label4);
        	this.tabPage1.Controls.Add(this.label3);
        	this.tabPage1.Controls.Add(this.label2);
        	this.tabPage1.Controls.Add(this.label1);
        	this.tabPage1.Controls.Add(this.labelVosPath);
        	this.tabPage1.ForeColor = System.Drawing.Color.DarkSlateGray;
        	this.tabPage1.Location = new System.Drawing.Point(4, 30);
        	this.tabPage1.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
        	this.tabPage1.Name = "tabPage1";
        	this.tabPage1.Padding = new System.Windows.Forms.Padding(3, 2, 3, 2);
        	this.tabPage1.Size = new System.Drawing.Size(601, 359);
        	this.tabPage1.TabIndex = 0;
        	this.tabPage1.Text = "安装App到vos固件";
        	// 
        	// label3
        	// 
        	this.label3.BackColor = System.Drawing.Color.Transparent;
        	this.label3.ForeColor = System.Drawing.Color.SlateGray;
        	this.label3.Location = new System.Drawing.Point(10, 195);
        	this.label3.Name = "label3";
        	this.label3.Size = new System.Drawing.Size(89, 23);
        	this.label3.TabIndex = 34;
        	this.label3.Text = "第 2 步：";
        	this.label3.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
        	// 
        	// label2
        	// 
        	this.label2.BackColor = System.Drawing.Color.Transparent;
        	this.label2.ForeColor = System.Drawing.Color.SlateGray;
        	this.label2.Location = new System.Drawing.Point(10, 73);
        	this.label2.Name = "label2";
        	this.label2.Size = new System.Drawing.Size(89, 23);
        	this.label2.TabIndex = 33;
        	this.label2.Text = "第 1 步：";
        	this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
        	// 
        	// label1
        	// 
        	this.label1.BackColor = System.Drawing.Color.Transparent;
        	this.label1.ForeColor = System.Drawing.Color.LightSlateGray;
        	this.label1.Location = new System.Drawing.Point(8, 253);
        	this.label1.Name = "label1";
        	this.label1.Size = new System.Drawing.Size(585, 23);
        	this.label1.TabIndex = 32;
        	this.label1.Text = "安装 APP 时将自动重新载入 vos 虚拟机固件文件";
        	this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
        	// 
        	// labelVosPath
        	// 
        	this.labelVosPath.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(245)))), ((int)(((byte)(239)))), ((int)(((byte)(231)))));
        	this.labelVosPath.Font = new System.Drawing.Font("微软雅黑", 10.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
        	this.labelVosPath.ForeColor = System.Drawing.Color.SlateGray;
        	this.labelVosPath.Location = new System.Drawing.Point(8, 2);
        	this.labelVosPath.Name = "labelVosPath";
        	this.labelVosPath.Size = new System.Drawing.Size(585, 48);
        	this.labelVosPath.TabIndex = 31;
        	// 
        	// buttonSwitch
        	// 
        	this.buttonSwitch.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(175)))), ((int)(((byte)(220)))), ((int)(((byte)(250)))));
        	this.buttonSwitch.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
        	this.buttonSwitch.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
        	this.buttonSwitch.Font = new System.Drawing.Font("微软雅黑", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
        	this.buttonSwitch.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(130)))), ((int)(((byte)(179)))));
        	this.buttonSwitch.Location = new System.Drawing.Point(150, 161);
        	this.buttonSwitch.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
        	this.buttonSwitch.Name = "buttonSwitch";
        	this.buttonSwitch.Size = new System.Drawing.Size(280, 90);
        	this.buttonSwitch.TabIndex = 30;
        	this.buttonSwitch.Text = "安装 APP";
        	this.buttonSwitch.UseVisualStyleBackColor = false;
        	this.buttonSwitch.Click += new System.EventHandler(this.ButtonSwitchClick);
        	// 
        	// OpenFileButton
        	// 
        	this.OpenFileButton.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(224)))), ((int)(((byte)(192)))));
        	this.OpenFileButton.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
        	this.OpenFileButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
        	this.OpenFileButton.Font = new System.Drawing.Font("微软雅黑", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
        	this.OpenFileButton.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(64)))), ((int)(((byte)(0)))));
        	this.OpenFileButton.Location = new System.Drawing.Point(150, 61);
        	this.OpenFileButton.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
        	this.OpenFileButton.Name = "OpenFileButton";
        	this.OpenFileButton.Size = new System.Drawing.Size(280, 46);
        	this.OpenFileButton.TabIndex = 29;
        	this.OpenFileButton.Text = "选择 vos 虚拟机固件";
        	this.OpenFileButton.UseVisualStyleBackColor = false;
        	this.OpenFileButton.Click += new System.EventHandler(this.OpenFileButton_Click);
        	// 
        	// tabPage4
        	// 
        	this.tabPage4.Controls.Add(this.richTextBoxApp);
        	this.tabPage4.Controls.Add(this.buttonCopy);
        	this.tabPage4.Location = new System.Drawing.Point(4, 30);
        	this.tabPage4.Name = "tabPage4";
        	this.tabPage4.Padding = new System.Windows.Forms.Padding(3);
        	this.tabPage4.Size = new System.Drawing.Size(601, 359);
        	this.tabPage4.TabIndex = 4;
        	this.tabPage4.Text = "App文件";
        	this.tabPage4.UseVisualStyleBackColor = true;
        	// 
        	// richTextBoxApp
        	// 
        	this.richTextBoxApp.BackColor = System.Drawing.Color.White;
        	this.richTextBoxApp.BorderStyle = System.Windows.Forms.BorderStyle.None;
        	this.richTextBoxApp.Dock = System.Windows.Forms.DockStyle.Fill;
        	this.richTextBoxApp.Font = new System.Drawing.Font("Courier New", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
        	this.richTextBoxApp.Location = new System.Drawing.Point(3, 40);
        	this.richTextBoxApp.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
        	this.richTextBoxApp.Name = "richTextBoxApp";
        	this.richTextBoxApp.ReadOnly = true;
        	this.richTextBoxApp.Size = new System.Drawing.Size(595, 316);
        	this.richTextBoxApp.TabIndex = 17;
        	this.richTextBoxApp.Text = "";
        	// 
        	// buttonCopy
        	// 
        	this.buttonCopy.Dock = System.Windows.Forms.DockStyle.Top;
        	this.buttonCopy.ForeColor = System.Drawing.Color.DodgerBlue;
        	this.buttonCopy.Location = new System.Drawing.Point(3, 3);
        	this.buttonCopy.Name = "buttonCopy";
        	this.buttonCopy.Size = new System.Drawing.Size(595, 37);
        	this.buttonCopy.TabIndex = 18;
        	this.buttonCopy.Text = "复制";
        	this.buttonCopy.UseVisualStyleBackColor = true;
        	this.buttonCopy.Click += new System.EventHandler(this.ButtonCopyClick);
        	// 
        	// tabPage3
        	// 
        	this.tabPage3.Controls.Add(this.CodeText);
        	this.tabPage3.Location = new System.Drawing.Point(4, 30);
        	this.tabPage3.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
        	this.tabPage3.Name = "tabPage3";
        	this.tabPage3.Padding = new System.Windows.Forms.Padding(3, 2, 3, 2);
        	this.tabPage3.Size = new System.Drawing.Size(601, 359);
        	this.tabPage3.TabIndex = 2;
        	this.tabPage3.Text = "vos固件";
        	this.tabPage3.UseVisualStyleBackColor = true;
        	// 
        	// CodeText
        	// 
        	this.CodeText.BackColor = System.Drawing.Color.White;
        	this.CodeText.BorderStyle = System.Windows.Forms.BorderStyle.None;
        	this.CodeText.Dock = System.Windows.Forms.DockStyle.Fill;
        	this.CodeText.Font = new System.Drawing.Font("Courier New", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
        	this.CodeText.Location = new System.Drawing.Point(3, 2);
        	this.CodeText.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
        	this.CodeText.Name = "CodeText";
        	this.CodeText.ReadOnly = true;
        	this.CodeText.Size = new System.Drawing.Size(595, 355);
        	this.CodeText.TabIndex = 15;
        	this.CodeText.Text = "";
        	// 
        	// tabPage2
        	// 
        	this.tabPage2.Controls.Add(this.richTextBoxOut);
        	this.tabPage2.Location = new System.Drawing.Point(4, 30);
        	this.tabPage2.Name = "tabPage2";
        	this.tabPage2.Padding = new System.Windows.Forms.Padding(3);
        	this.tabPage2.Size = new System.Drawing.Size(601, 359);
        	this.tabPage2.TabIndex = 3;
        	this.tabPage2.Text = "输出固件";
        	this.tabPage2.UseVisualStyleBackColor = true;
        	// 
        	// richTextBoxOut
        	// 
        	this.richTextBoxOut.BackColor = System.Drawing.Color.White;
        	this.richTextBoxOut.BorderStyle = System.Windows.Forms.BorderStyle.None;
        	this.richTextBoxOut.Dock = System.Windows.Forms.DockStyle.Fill;
        	this.richTextBoxOut.Font = new System.Drawing.Font("Courier New", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
        	this.richTextBoxOut.Location = new System.Drawing.Point(3, 3);
        	this.richTextBoxOut.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
        	this.richTextBoxOut.Name = "richTextBoxOut";
        	this.richTextBoxOut.ReadOnly = true;
        	this.richTextBoxOut.Size = new System.Drawing.Size(595, 353);
        	this.richTextBoxOut.TabIndex = 16;
        	this.richTextBoxOut.Text = "";
        	// 
        	// label4
        	// 
        	this.label4.BackColor = System.Drawing.Color.Transparent;
        	this.label4.ForeColor = System.Drawing.Color.LightSlateGray;
        	this.label4.Location = new System.Drawing.Point(150, 109);
        	this.label4.Name = "label4";
        	this.label4.Size = new System.Drawing.Size(280, 23);
        	this.label4.TabIndex = 35;
        	this.label4.Text = "支持  *.hex  *.bin  *.elf  类型";
        	this.label4.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
        	// 
        	// HexForm
        	// 
        	this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 15F);
        	this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
        	this.BackColor = System.Drawing.Color.LightGray;
        	this.ClientSize = new System.Drawing.Size(609, 393);
        	this.Controls.Add(this.tabControl1);
        	this.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
        	this.ForeColor = System.Drawing.Color.MidnightBlue;
        	this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
        	this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
        	this.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
        	this.MaximizeBox = false;
        	this.MinimizeBox = false;
        	this.Name = "HexForm";
        	this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
        	this.Text = "App 安装器";
        	this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.Form1FormClosing);
        	this.tabControl1.ResumeLayout(false);
        	this.tabPage1.ResumeLayout(false);
        	this.tabPage4.ResumeLayout(false);
        	this.tabPage3.ResumeLayout(false);
        	this.tabPage2.ResumeLayout(false);
        	this.ResumeLayout(false);
        }
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Button buttonCopy;
        public System.Windows.Forms.RichTextBox richTextBoxApp;
        private System.Windows.Forms.TabPage tabPage4;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label labelVosPath;
        public System.Windows.Forms.RichTextBox richTextBoxOut;
        private System.Windows.Forms.TabPage tabPage2;
        private System.Windows.Forms.Button buttonSwitch;
        private System.Windows.Forms.Button OpenFileButton;
        public System.Windows.Forms.RichTextBox CodeText;
        private System.Windows.Forms.TabPage tabPage3;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.TabControl tabControl1;

        #endregion

    }
}

