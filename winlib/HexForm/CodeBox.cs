﻿
using System;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using n_HexCoder;

namespace n_HexCodeBox_temp
{
	//****************************************************************
	//文件显示窗口
	public static class CodeBox
	{
		static RichTextBox CodeText;
		static Label LineLabel;
		
		public static void Init( RichTextBox r, Label l )
		{
			LineLabel = l;
			LineLabel.Paint += new PaintEventHandler( LineLabel_Paint );
			
			CodeText = r;
			CodeText.MouseDown += new MouseEventHandler( CodeText_MouseDown );
			CodeText.VScroll += new EventHandler( CodeText_VScroll );
			CodeText.TextChanged += new EventHandler( CodeText_TextChanged );
		}
		
		//判断缓冲区是否为空
		public static bool BufferIsEmpty()
		{
			if( CodeText.Text == "" ) {
				return true;
			}
			return false;
		}
		
		//设置打开的文本, 格式为 hex 文件
		public static void SetCodeTextFromHex( string Code )
		{
			CodeText.Text = "";
			CodeText.Refresh();
			CodeText.Text = HexCoder.LoadHex( Code );
		}
		
		//设置打开的文本, 格式为 bin 文件
		public static void SetCodeTextFromBin( byte[] buffer )
		{
			CodeText.Text = "";
			CodeText.Refresh();
			//CodeText.Text = HexCoder.SwitchCode( Code );
		}
		
		//转换当前缓冲区文本为二维数组
		public static string GetText()
		{
			return CodeText.Text;
		}
		
		//=======================================
		//用于读取Flash
		static string TextBuffer;
		static string TextChar;
		
		//清除当前缓冲区,为添加函数作准备
		public static void Clear()
		{
			CodeText.Text = "";
			TextBuffer = null;
			TextChar = null;
			CodeText.Refresh();
		}
		
		//添加一段机器码
		public static void Add( byte[] Buffer )
		{
			string Number = "0123456789ABCDEF";
			for( int i = 0; i < Buffer.Length; ) {
				string SHex = " " + Number[ Buffer[ i ] / 16 ] + Number[ Buffer[ i ] % 16 ];
				TextBuffer += SHex;
				if( Buffer[ i ] >= 32 && Buffer[ i ] <= 126 ) {
					TextChar += (char)Buffer[ i ];
				}
				else {
					TextChar += '.';
				}
				++i;
				if( i % 16 == 0 ) {
					TextBuffer += "    " + TextChar + "\n";
					TextChar = null;
				}
			}
		}
		
		//显示添加完成的内容
		public static void ShowBuffer()
		{
			CodeText.Text = TextBuffer;
		}
		
		//=======================================
		//文本改变
		static void CodeText_TextChanged(object sender, EventArgs e)
		{
			LineLabel.Invalidate();
		}
		
		//滚动条移动
		static void CodeText_VScroll(object sender, EventArgs e)
		{
			int p = CodeText.GetPositionFromCharIndex( 0 ).Y % ( CodeText.Font.Height + 1 );
			LineLabel.Invalidate();
		}
		
		//画行号
		static void LineLabel_Paint( object sender, PaintEventArgs e )
		{
			Point pos = new Point( 0, 0 );
			int firstIndex = CodeText.GetCharIndexFromPosition( pos );
			int firstLine = CodeText.GetLineFromCharIndex( firstIndex );
			pos.X = CodeText.ClientRectangle.Width;
			pos.Y = CodeText.ClientRectangle.Height;
			int endIndex = CodeText.GetCharIndexFromPosition( pos );
			int endLine = CodeText.GetLineFromCharIndex( endIndex );
			
			for( int i = firstLine; i <= endLine; ++i ) {
				int y = CodeText.GetPositionFromCharIndex( CodeText.GetFirstCharIndexFromLine( i ) ).Y;
				Rectangle rec = new Rectangle( 0, y, CodeText.Width, CodeText.Font.Height );
				string LineLabel = ( i * 16 ).ToString( "X" ).PadLeft( 5, '0' );
				e.Graphics.DrawString( LineLabel, CodeText.Font, Brushes.DarkSlateGray, rec );
			}
		}
		
		//鼠标点击
		static void CodeText_MouseDown(object sender, MouseEventArgs e)
		{
			int LineFirstIndex = CodeText.GetFirstCharIndexOfCurrentLine();
			int StartIndex = CodeText.SelectionStart - LineFirstIndex;
			if( StartIndex != 0 && StartIndex <= 3 * 16 ) {
				CodeText.SelectionLength = 1;
				if( StartIndex % 3 == 0 ) {
					--CodeText.SelectionStart;
				}
			}
			else {
				CodeText.SelectionLength = 0;
			}
		}
	}
}
