﻿
namespace n_HexForm
{
using System;
using System.Drawing;
using System.IO;
using System.Windows.Forms;

using n_HexCoder;
using n_OS;

public partial class HexForm : Form
{
	public int BLength;
	public byte[] CodeList;
	
	string CurrentVosFile;
	
	//构造函数
	public HexForm()
	{
		InitializeComponent();
		
		n_HexCoder.HexCoder.Init();
	}
	
	//运行
	public void Run( byte[] ByteCodeList, int BCLength )
	{
		CodeList = ByteCodeList;
		BLength = BCLength;
		
		
		string res = "";
		for( int i = 0; i < BCLength; i += 4 ) {
			byte b0 = CodeList[i];
			byte b1 = CodeList[i+1];
			byte b2 = CodeList[i+2];
			byte b3 = CodeList[i+3];
			UInt64 data = b3; data <<= 8;
			data += b2; data <<= 8;
			data += b1; data <<= 8;
			data += b0;
			
			res += "0x" + data.ToString( "X" ).PadLeft( 8, '0' ) + ", ";
			if( (i/4) % 6 == 5 ) {
				res += "\r\n";
			}
		}
		res += "\r\n\r\n";
		richTextBoxApp.Text = res;
		
		
		this.Visible = true;
		this.Activate();
	}
	
	//窗体关闭
	void Form1FormClosing(object sender, FormClosingEventArgs e)
	{
		this.Visible = false;
		e.Cancel = true;
	}
	
	//打开文件
	void OpenFileButton_Click(object sender, EventArgs e)
	{
		OpenFileDialog open = new OpenFileDialog();
		open.Filter = "vos固件文件|*.*";
		open.Title = "请选择一个vos固件文件(.hex .bin .elf)";
		
		if( open.ShowDialog() == DialogResult.OK ) {
			CurrentVosFile = open.FileName;
			labelVosPath.Text = CurrentVosFile;
			this.CodeText.Text = HexCoder.Load( CurrentVosFile );
		}
	}
	
	//安装APP到选定的目录
	void ButtonSwitchClick(object sender, EventArgs e)
	{
		/*
		string res = "";
		for( int i = 0; i < n_ISPcommon.ISPcommon.CodeLength; i += 1 ) {
			byte b0 = n_ISPcommon.ISPcommon.CodeList[i];
			
			res += b0.ToString( "X" ).PadLeft( 2, '0' ) + " ";
			if( i % 16 == 15 ) {
				res += "\r\n";
			}
		}
		res += "\r\n\r\n";
		*/
		
		if( CurrentVosFile == null ) {
			MessageBox.Show( "请先打开vos固件文件" );
			return;
		}
		if( !File.Exists( CurrentVosFile ) ) {
			MessageBox.Show( "不存在的文件: " + CurrentVosFile );
			return;
		}
		
		this.CodeText.Text = HexCoder.Load( CurrentVosFile );
		
		int firstok = 0;
		for( int i = 0; i < n_HexCoder.HexCoder.CodeLength - 8; i += 4 ) {
			long data = 0;
			data += n_HexCoder.HexCoder.CodeList[i + 3];
			data <<= 8;
			data += n_HexCoder.HexCoder.CodeList[i + 2];
			data <<= 8;
			data += n_HexCoder.HexCoder.CodeList[i + 1];
			data <<= 8;
			data += n_HexCoder.HexCoder.CodeList[i + 0];
			
			if( firstok == 0 ) {
				if( data == 0x6B6E696C ) {
					firstok = 1;
				}
			}
			else {
				firstok = 0;
				if( data == 0x3A796F62 ) {
					firstok = 2;
				}
			}
			if( firstok == 2 ) {
				//MessageBox.Show( "CODE-ADDR: 0x" + (i+4).ToString( "X" ).PadLeft( 4, '0' ) );
				
				for( int j = 0; j < BLength; j++ ) {
					n_HexCoder.HexCoder.CodeList[i+4+j] = CodeList[j];
				}
				break;
			}
		}
		if( firstok != 2 ) {
			MessageBox.Show( "安装失败!" );
			return;
		}
		richTextBoxOut.Text = HexCoder.Save( CurrentVosFile );
	}
	
	//复制App数据
	void ButtonCopyClick(object sender, EventArgs e)
	{
		try {
			System.Windows.Forms.Clipboard.SetText( richTextBoxApp.Text );
		}
		catch (Exception ee) {
			MessageBox.Show( "复制失败: " + ee );
		}
	}
}
}
