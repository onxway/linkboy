﻿/*
 * 由SharpDevelop创建。
 * 用户： dell
 * 日期: 2015/11/15
 * 时间: 8:29
 * 
 * 要改变这种模板请点击 工具|选项|代码编写|编辑标准头文件
 */
namespace n_STlinkForm
{
	partial class STlinkForm
	{
		/// <summary>
		/// Designer variable used to keep track of non-visual components.
		/// </summary>
		private System.ComponentModel.IContainer components = null;
		private System.Windows.Forms.Button buttonOk;
		
		/// <summary>
		/// Disposes resources used by the form.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing) {
				if (components != null) {
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}
		
		/// <summary>
		/// This method is required for Windows Forms designer support.
		/// Do not change the method contents inside the source code editor. The Forms designer might
		/// not be able to load this method if it was changed manually.
		/// </summary>
		private void InitializeComponent()
		{
			this.buttonOk = new System.Windows.Forms.Button();
			this.textBoxShowStdRet = new System.Windows.Forms.RichTextBox();
			this.SuspendLayout();
			// 
			// buttonOk
			// 
			this.buttonOk.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(128)))), ((int)(((byte)(255)))));
			this.buttonOk.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
			this.buttonOk.Font = new System.Drawing.Font("微软雅黑", 21.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
			this.buttonOk.ForeColor = System.Drawing.Color.White;
			this.buttonOk.Location = new System.Drawing.Point(12, 377);
			this.buttonOk.Name = "buttonOk";
			this.buttonOk.Size = new System.Drawing.Size(543, 49);
			this.buttonOk.TabIndex = 6;
			this.buttonOk.Text = "安装LiteOS操作系统到目标板上";
			this.buttonOk.UseVisualStyleBackColor = false;
			this.buttonOk.Click += new System.EventHandler(this.ButtonOkClick);
			// 
			// textBoxShowStdRet
			// 
			this.textBoxShowStdRet.BorderStyle = System.Windows.Forms.BorderStyle.None;
			this.textBoxShowStdRet.Font = new System.Drawing.Font("微软雅黑", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
			this.textBoxShowStdRet.Location = new System.Drawing.Point(12, 12);
			this.textBoxShowStdRet.Name = "textBoxShowStdRet";
			this.textBoxShowStdRet.Size = new System.Drawing.Size(543, 359);
			this.textBoxShowStdRet.TabIndex = 7;
			this.textBoxShowStdRet.Text = "如果您的nucleo-F411RE开发板安装过LiteOS操作系统, 请直接关闭本窗口.\n\n没有安装过的话, 请点击 \"安装操作系统到目标板上\" 之后, 会弹出一" +
			"个命令行窗口, 稍等一会儿执行完之后, 可以输入回车退出命令行, 这时操作系统也安装完成. 然后请断开开发板数据线(从电脑的USB端口拔下并重新插上, 这样操作" +
			"系统开始正式运行. 您便可关闭本窗口, 开始下一步的下载字节码的步骤";
			// 
			// STlinkForm
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.BackColor = System.Drawing.Color.Gainsboro;
			this.ClientSize = new System.Drawing.Size(567, 438);
			this.Controls.Add(this.textBoxShowStdRet);
			this.Controls.Add(this.buttonOk);
			this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
			this.Name = "STlinkForm";
			this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
			this.Text = "LiteOS安装器";
			this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.AVRdudeSetFormFormClosing);
			this.ResumeLayout(false);
		}
		private System.Windows.Forms.RichTextBox textBoxShowStdRet;
	}
}
