﻿
namespace n_VosVersForm
{
using System;
using System.IO;
using System.Net;
using System.Text;
using System.Windows.Forms;
using System.Drawing;
using n_OS;

/// <summary>
/// Description of WorkForm.
/// </summary>
public partial class VosVersForm : Form
{
	string[] VList;
	
	//主窗口
	public VosVersForm()
	{
		//
		// The InitializeComponent() call is required for Windows Forms designer support.
		//
		InitializeComponent();
		
		//得到原文件根目录下的所有文件
		string fold = OS.SystemRoot + "vos" + OS.PATH_S + "0版本历史";
		string[] files = System.IO.Directory.GetFiles( fold );
		
		Array.Sort(files, delegate(string x, string y) { return x.CompareTo(y); });  
		
		VList = new string[files.Length];
		int i = 0;
		foreach (string file in files) {
			string name = System.IO.Path.GetFileName(file);
			string dest = System.IO.Path.Combine( fold, name );
			
			VList[i] = VIO.OpenTextFileGB2312( dest );
			listBox1.Items.Add( name.Remove( name.Length - 4 ) );
			i++;
		}
		
		labelVersionList.Text = n_VosVer.VosVer.CPU_VersionList;
	}
	
	public void Run( int i )
	{
		if( i == 0 ) {
			tabControl1.SelectedTab = tabPageMAP;
		}
		if( i == 1 ) {
			tabControl1.SelectedTab = tabPageGUJIAN;
		}
		if( i == 2 ) {
			tabControl1.SelectedTab = tabPageVER;
		}
		this.Visible = true;
	}
	
	void CopyRightFormFormClosing(object sender, FormClosingEventArgs e)
	{
		this.Visible = false;
		e.Cancel = true;
	}
	
	void ListBox1SelectedIndexChanged(object sender, EventArgs e)
	{
		int i = listBox1.SelectedIndex;
		this.richTextBox_vos.Text = VList[i];
		this.richTextBox_vos.SelectionStart = this.richTextBox_vos.TextLength - 1;
		this.richTextBox_vos.ScrollToCaret();
	}
	
	void ButtonSourceClick(object sender, EventArgs e)
	{
		string fileToSelect = n_OS.OS.SystemRoot + "vos" + n_OS.OS.PATH_S + "vos" + n_OS.OS.PATH_S + "core";
		
		string args = string.Format("/Select, {0}", fileToSelect);
		System.Diagnostics.Process.Start( new System.Diagnostics.ProcessStartInfo("Explorer.exe", args ) );
	}
}
}



