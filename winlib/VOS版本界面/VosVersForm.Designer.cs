﻿/*
 * 由 SharpDevelop 创建。
 * 用户: bbb_2
 * 日期: 2009-11-17
 * 时间: 11:15
 * 
 * 如果你想要更改该模板，那么请使用“工具 | 选项 | 编辑 | 编辑标准标题”。
 */
namespace n_VosVersForm
{
	partial class VosVersForm
	{
		/// <summary>
		/// Designer variable used to keep track of non-visual components.
		/// </summary>
		private System.ComponentModel.IContainer components = null;
		
		/// <summary>
		/// Disposes resources used by the form.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing) {
				if (components != null) {
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}
		
		/// <summary>
		/// This method is required for Windows Forms designer support.
		/// Do not change the method contents inside the source code editor. The Forms designer might
		/// not be able to load this method if it was changed manually.
		/// </summary>
		private void InitializeComponent()
		{
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(VosVersForm));
			this.panel1 = new System.Windows.Forms.Panel();
			this.label1 = new System.Windows.Forms.Label();
			this.tabControl1 = new System.Windows.Forms.TabControl();
			this.tabPageMAP = new System.Windows.Forms.TabPage();
			this.buttonSource = new System.Windows.Forms.Button();
			this.label20 = new System.Windows.Forms.Label();
			this.label4 = new System.Windows.Forms.Label();
			this.panel4 = new System.Windows.Forms.Panel();
			this.label33 = new System.Windows.Forms.Label();
			this.label12 = new System.Windows.Forms.Label();
			this.label18 = new System.Windows.Forms.Label();
			this.label3 = new System.Windows.Forms.Label();
			this.label26 = new System.Windows.Forms.Label();
			this.label34 = new System.Windows.Forms.Label();
			this.label13 = new System.Windows.Forms.Label();
			this.label11 = new System.Windows.Forms.Label();
			this.label14 = new System.Windows.Forms.Label();
			this.label15 = new System.Windows.Forms.Label();
			this.label16 = new System.Windows.Forms.Label();
			this.label19 = new System.Windows.Forms.Label();
			this.label21 = new System.Windows.Forms.Label();
			this.label22 = new System.Windows.Forms.Label();
			this.label24 = new System.Windows.Forms.Label();
			this.label25 = new System.Windows.Forms.Label();
			this.label27 = new System.Windows.Forms.Label();
			this.label28 = new System.Windows.Forms.Label();
			this.label29 = new System.Windows.Forms.Label();
			this.label30 = new System.Windows.Forms.Label();
			this.label17 = new System.Windows.Forms.Label();
			this.label23 = new System.Windows.Forms.Label();
			this.tabPageGUJIAN = new System.Windows.Forms.TabPage();
			this.label2 = new System.Windows.Forms.Label();
			this.labelVersionList = new System.Windows.Forms.Label();
			this.tabPageVER = new System.Windows.Forms.TabPage();
			this.panel2 = new System.Windows.Forms.Panel();
			this.richTextBox_vos = new System.Windows.Forms.RichTextBox();
			this.panel3 = new System.Windows.Forms.Panel();
			this.listBox1 = new System.Windows.Forms.ListBox();
			this.panel1.SuspendLayout();
			this.tabControl1.SuspendLayout();
			this.tabPageMAP.SuspendLayout();
			this.panel4.SuspendLayout();
			this.tabPageGUJIAN.SuspendLayout();
			this.tabPageVER.SuspendLayout();
			this.panel2.SuspendLayout();
			this.SuspendLayout();
			// 
			// panel1
			// 
			this.panel1.BackColor = System.Drawing.Color.WhiteSmoke;
			this.panel1.Controls.Add(this.label1);
			resources.ApplyResources(this.panel1, "panel1");
			this.panel1.Name = "panel1";
			// 
			// label1
			// 
			resources.ApplyResources(this.label1, "label1");
			this.label1.Name = "label1";
			// 
			// tabControl1
			// 
			this.tabControl1.Controls.Add(this.tabPageMAP);
			this.tabControl1.Controls.Add(this.tabPageGUJIAN);
			this.tabControl1.Controls.Add(this.tabPageVER);
			resources.ApplyResources(this.tabControl1, "tabControl1");
			this.tabControl1.Name = "tabControl1";
			this.tabControl1.SelectedIndex = 0;
			// 
			// tabPageMAP
			// 
			this.tabPageMAP.BackColor = System.Drawing.Color.WhiteSmoke;
			this.tabPageMAP.Controls.Add(this.buttonSource);
			this.tabPageMAP.Controls.Add(this.label20);
			this.tabPageMAP.Controls.Add(this.label4);
			this.tabPageMAP.Controls.Add(this.panel4);
			this.tabPageMAP.Controls.Add(this.label3);
			this.tabPageMAP.Controls.Add(this.label26);
			this.tabPageMAP.Controls.Add(this.label34);
			this.tabPageMAP.Controls.Add(this.label13);
			this.tabPageMAP.Controls.Add(this.label11);
			this.tabPageMAP.Controls.Add(this.label14);
			this.tabPageMAP.Controls.Add(this.label15);
			this.tabPageMAP.Controls.Add(this.label16);
			this.tabPageMAP.Controls.Add(this.label19);
			this.tabPageMAP.Controls.Add(this.label21);
			this.tabPageMAP.Controls.Add(this.label22);
			this.tabPageMAP.Controls.Add(this.label24);
			this.tabPageMAP.Controls.Add(this.label25);
			this.tabPageMAP.Controls.Add(this.label27);
			this.tabPageMAP.Controls.Add(this.label28);
			this.tabPageMAP.Controls.Add(this.label29);
			this.tabPageMAP.Controls.Add(this.label30);
			this.tabPageMAP.Controls.Add(this.label17);
			this.tabPageMAP.Controls.Add(this.label23);
			resources.ApplyResources(this.tabPageMAP, "tabPageMAP");
			this.tabPageMAP.Name = "tabPageMAP";
			// 
			// buttonSource
			// 
			resources.ApplyResources(this.buttonSource, "buttonSource");
			this.buttonSource.Name = "buttonSource";
			this.buttonSource.UseVisualStyleBackColor = true;
			this.buttonSource.Click += new System.EventHandler(this.ButtonSourceClick);
			// 
			// label20
			// 
			this.label20.BackColor = System.Drawing.Color.Transparent;
			resources.ApplyResources(this.label20, "label20");
			this.label20.ForeColor = System.Drawing.Color.Chocolate;
			this.label20.Name = "label20";
			// 
			// label4
			// 
			this.label4.BackColor = System.Drawing.Color.Transparent;
			resources.ApplyResources(this.label4, "label4");
			this.label4.ForeColor = System.Drawing.Color.DodgerBlue;
			this.label4.Name = "label4";
			// 
			// panel4
			// 
			this.panel4.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(226)))), ((int)(((byte)(240)))), ((int)(((byte)(250)))));
			this.panel4.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
			this.panel4.Controls.Add(this.label33);
			this.panel4.Controls.Add(this.label12);
			this.panel4.Controls.Add(this.label18);
			resources.ApplyResources(this.panel4, "panel4");
			this.panel4.Name = "panel4";
			// 
			// label33
			// 
			this.label33.BackColor = System.Drawing.Color.Transparent;
			resources.ApplyResources(this.label33, "label33");
			this.label33.ForeColor = System.Drawing.Color.DodgerBlue;
			this.label33.Name = "label33";
			// 
			// label12
			// 
			this.label12.BackColor = System.Drawing.Color.Transparent;
			resources.ApplyResources(this.label12, "label12");
			this.label12.ForeColor = System.Drawing.Color.DodgerBlue;
			this.label12.Name = "label12";
			// 
			// label18
			// 
			this.label18.BackColor = System.Drawing.Color.Transparent;
			resources.ApplyResources(this.label18, "label18");
			this.label18.ForeColor = System.Drawing.Color.DodgerBlue;
			this.label18.Name = "label18";
			// 
			// label3
			// 
			resources.ApplyResources(this.label3, "label3");
			this.label3.ForeColor = System.Drawing.Color.SlateGray;
			this.label3.Name = "label3";
			// 
			// label26
			// 
			resources.ApplyResources(this.label26, "label26");
			this.label26.ForeColor = System.Drawing.Color.Chocolate;
			this.label26.Name = "label26";
			// 
			// label34
			// 
			resources.ApplyResources(this.label34, "label34");
			this.label34.ForeColor = System.Drawing.Color.DodgerBlue;
			this.label34.Name = "label34";
			// 
			// label13
			// 
			resources.ApplyResources(this.label13, "label13");
			this.label13.ForeColor = System.Drawing.Color.DodgerBlue;
			this.label13.Name = "label13";
			// 
			// label11
			// 
			resources.ApplyResources(this.label11, "label11");
			this.label11.ForeColor = System.Drawing.Color.LightSlateGray;
			this.label11.Name = "label11";
			// 
			// label14
			// 
			resources.ApplyResources(this.label14, "label14");
			this.label14.ForeColor = System.Drawing.Color.LightSlateGray;
			this.label14.Name = "label14";
			// 
			// label15
			// 
			resources.ApplyResources(this.label15, "label15");
			this.label15.ForeColor = System.Drawing.Color.LightSlateGray;
			this.label15.Name = "label15";
			// 
			// label16
			// 
			resources.ApplyResources(this.label16, "label16");
			this.label16.ForeColor = System.Drawing.Color.LightSlateGray;
			this.label16.Name = "label16";
			// 
			// label19
			// 
			resources.ApplyResources(this.label19, "label19");
			this.label19.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(174)))), ((int)(((byte)(189)))), ((int)(((byte)(206)))));
			this.label19.Name = "label19";
			// 
			// label21
			// 
			resources.ApplyResources(this.label21, "label21");
			this.label21.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(174)))), ((int)(((byte)(189)))), ((int)(((byte)(206)))));
			this.label21.Name = "label21";
			// 
			// label22
			// 
			resources.ApplyResources(this.label22, "label22");
			this.label22.ForeColor = System.Drawing.Color.DodgerBlue;
			this.label22.Name = "label22";
			// 
			// label24
			// 
			resources.ApplyResources(this.label24, "label24");
			this.label24.Name = "label24";
			// 
			// label25
			// 
			resources.ApplyResources(this.label25, "label25");
			this.label25.ForeColor = System.Drawing.Color.LightSlateGray;
			this.label25.Name = "label25";
			// 
			// label27
			// 
			resources.ApplyResources(this.label27, "label27");
			this.label27.ForeColor = System.Drawing.Color.LightSlateGray;
			this.label27.Name = "label27";
			// 
			// label28
			// 
			resources.ApplyResources(this.label28, "label28");
			this.label28.Name = "label28";
			// 
			// label29
			// 
			resources.ApplyResources(this.label29, "label29");
			this.label29.Name = "label29";
			// 
			// label30
			// 
			resources.ApplyResources(this.label30, "label30");
			this.label30.Name = "label30";
			// 
			// label17
			// 
			resources.ApplyResources(this.label17, "label17");
			this.label17.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(174)))), ((int)(((byte)(189)))), ((int)(((byte)(206)))));
			this.label17.Name = "label17";
			// 
			// label23
			// 
			resources.ApplyResources(this.label23, "label23");
			this.label23.ForeColor = System.Drawing.Color.LightSlateGray;
			this.label23.Name = "label23";
			// 
			// tabPageGUJIAN
			// 
			this.tabPageGUJIAN.BackColor = System.Drawing.Color.WhiteSmoke;
			this.tabPageGUJIAN.Controls.Add(this.label2);
			this.tabPageGUJIAN.Controls.Add(this.labelVersionList);
			resources.ApplyResources(this.tabPageGUJIAN, "tabPageGUJIAN");
			this.tabPageGUJIAN.Name = "tabPageGUJIAN";
			// 
			// label2
			// 
			resources.ApplyResources(this.label2, "label2");
			this.label2.Name = "label2";
			// 
			// labelVersionList
			// 
			this.labelVersionList.BackColor = System.Drawing.Color.WhiteSmoke;
			resources.ApplyResources(this.labelVersionList, "labelVersionList");
			this.labelVersionList.ForeColor = System.Drawing.Color.SlateGray;
			this.labelVersionList.Name = "labelVersionList";
			// 
			// tabPageVER
			// 
			this.tabPageVER.Controls.Add(this.panel2);
			this.tabPageVER.Controls.Add(this.panel1);
			resources.ApplyResources(this.tabPageVER, "tabPageVER");
			this.tabPageVER.Name = "tabPageVER";
			this.tabPageVER.UseVisualStyleBackColor = true;
			// 
			// panel2
			// 
			this.panel2.Controls.Add(this.richTextBox_vos);
			this.panel2.Controls.Add(this.panel3);
			this.panel2.Controls.Add(this.listBox1);
			resources.ApplyResources(this.panel2, "panel2");
			this.panel2.Name = "panel2";
			// 
			// richTextBox_vos
			// 
			this.richTextBox_vos.BackColor = System.Drawing.Color.White;
			this.richTextBox_vos.BorderStyle = System.Windows.Forms.BorderStyle.None;
			resources.ApplyResources(this.richTextBox_vos, "richTextBox_vos");
			this.richTextBox_vos.Name = "richTextBox_vos";
			this.richTextBox_vos.ReadOnly = true;
			// 
			// panel3
			// 
			this.panel3.BackColor = System.Drawing.Color.WhiteSmoke;
			resources.ApplyResources(this.panel3, "panel3");
			this.panel3.Name = "panel3";
			// 
			// listBox1
			// 
			resources.ApplyResources(this.listBox1, "listBox1");
			this.listBox1.FormattingEnabled = true;
			this.listBox1.Name = "listBox1";
			this.listBox1.SelectedIndexChanged += new System.EventHandler(this.ListBox1SelectedIndexChanged);
			// 
			// VosVersForm
			// 
			resources.ApplyResources(this, "$this");
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.BackColor = System.Drawing.Color.WhiteSmoke;
			this.Controls.Add(this.tabControl1);
			this.Name = "VosVersForm";
			this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.CopyRightFormFormClosing);
			this.panel1.ResumeLayout(false);
			this.tabControl1.ResumeLayout(false);
			this.tabPageMAP.ResumeLayout(false);
			this.panel4.ResumeLayout(false);
			this.tabPageGUJIAN.ResumeLayout(false);
			this.tabPageVER.ResumeLayout(false);
			this.panel2.ResumeLayout(false);
			this.ResumeLayout(false);
		}
		private System.Windows.Forms.Button buttonSource;
		private System.Windows.Forms.Panel panel4;
		private System.Windows.Forms.Label label4;
		private System.Windows.Forms.Panel panel3;
		private System.Windows.Forms.ListBox listBox1;
		private System.Windows.Forms.Panel panel2;
		private System.Windows.Forms.Label label3;
		private System.Windows.Forms.Label label2;
		private System.Windows.Forms.Label labelVersionList;
		private System.Windows.Forms.TabPage tabPageGUJIAN;
		private System.Windows.Forms.Label label30;
		private System.Windows.Forms.Label label29;
		private System.Windows.Forms.Label label28;
		private System.Windows.Forms.Label label27;
		private System.Windows.Forms.Label label25;
		private System.Windows.Forms.Label label24;
		private System.Windows.Forms.Label label23;
		private System.Windows.Forms.Label label22;
		private System.Windows.Forms.Label label21;
		private System.Windows.Forms.Label label19;
		private System.Windows.Forms.Label label17;
		private System.Windows.Forms.Label label16;
		private System.Windows.Forms.Label label15;
		private System.Windows.Forms.Label label14;
		private System.Windows.Forms.Label label11;
		private System.Windows.Forms.Label label33;
		private System.Windows.Forms.Label label13;
		private System.Windows.Forms.Label label12;
		private System.Windows.Forms.Label label34;
		private System.Windows.Forms.Label label18;
		private System.Windows.Forms.Label label26;
		private System.Windows.Forms.Label label20;
		private System.Windows.Forms.TabPage tabPageVER;
		private System.Windows.Forms.TabPage tabPageMAP;
		private System.Windows.Forms.TabControl tabControl1;
		private System.Windows.Forms.Label label1;
		private System.Windows.Forms.RichTextBox richTextBox_vos;
		private System.Windows.Forms.Panel panel1;
	}
}
