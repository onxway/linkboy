﻿
namespace n_LaterFilesManager
{
using System;
using System.IO;
using System.Text;
using System.Windows.Forms;
using n_OS;

//近期文件列表管理类
public static class LaterFilesManager
{
	//当系统需要打开一个文件时就会触发这个委托, 例如错误列表点击时
	public delegate void D_FileListChanged();
	public static D_FileListChanged FileListChanged;
	
	public const int MaxNumber = 15;
	static string LaterFiles;
	
	//加载近期文件列表
	public static void LoadLaterFiles()
	{
		string LaterFilePath = OS.SystemRoot + "Resource" + OS.PATH_S + "LaterFilePanel" + OS.PATH_S + "近期文件路径.lst";
		LaterFiles = VIO.OpenTextFileGB2312( LaterFilePath );
	}
	
	//保存近期文件列表
	public static void SaveLaterFiles()
	{
		string LaterFilePath = OS.SystemRoot + "Resource" + OS.PATH_S + "LaterFilePanel" + OS.PATH_S + "近期文件路径.lst";
		VIO.SaveTextFileGB2312( LaterFilePath, LaterFiles );
	}
	
	//向近期文件列表添加一个文件
	public static void AddFile( string AddFilePath )
	{
		//忽略库文件的记录
		if( AddFilePath.StartsWith( OS.ModuleLibPath ) ) {
			//return;
		}
		//添加到记录文件中
		string[] Lines = LaterFiles.Split( '\n' );
		LaterFiles = AddFilePath + "\n";
		for( int i = 0; i < Lines.Length; ++i ) {
			if( i == MaxNumber ) {
				break;
			}
			//if( Lines[ i ] != AddFilePath && File.Exists( Lines[ i ] ) ) {
			//	LaterFiles += Lines[ i ] + "\n";
			//}
			if( Lines[ i ] != AddFilePath ) {
				LaterFiles += Lines[ i ] + "\n";
			}
		}
		LaterFiles += "<end>";
		
		if( FileListChanged != null ) {
			FileListChanged();
		}
	}
	
	//加载最近的文件路径列表到一个字符串数组中
	//没有近期文件则返回null
	public static string[] GetFileList()
	{
		if( LaterFiles.StartsWith( "<end>" ) ) {
			return null;
		}
		string s = LaterFiles.Remove( LaterFiles.IndexOf( "\n<end>" ) );
		return s.Split( '\n' );
	}
}
}







