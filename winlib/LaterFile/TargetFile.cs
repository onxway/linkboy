﻿
namespace n_TargetFile
{
using System;
using n_OS;

//目标文件列表
public static class TargetFile
{
	public static string[] TargetFileList;
	public static string[] TargetHEXFileList;
	
	public static string TargetPyFile;
	
	//获取当前的目标文件路径
	public static string GetTargetFilePath()
	{
		if( TargetFileList == null ) {
			VIO.Show( "当前没有目标文件" );
			return null;
		}
		if( TargetFileList[ 0 ] == null ) {
			VIO.Show( "目标文件为空" );
			return null;
		}
		if( TargetFileList.Length != 1 ) {
			//A.FlashBox.Run( "有多个目标文件,不知道您要导入哪个(此项功能有待改进请联系作者...)" );
			//return null;
		}
		return TargetFileList[ 0 ];
	}
	
	//获取当前的目标文件
	public static string GetTargetFile( int CI )
	{
		if( TargetFileList == null ) {
			VIO.Show( "当前没有目标文件" );
			return null;
		}
		if( TargetFileList[ CI ] == null ) {
			VIO.Show( "目标文件为空" );
			return null;
		}
		if( TargetFileList.Length != 1 ) {
			//A.FlashBox.Run( "有多个目标文件,不知道您要导入哪个(此项功能有待改进请联系作者...)" );
			//return null;
		}
		return TargetHEXFileList[ CI ];
	}
}
}

