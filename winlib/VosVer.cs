﻿
using System;
using System.Windows.Forms;

namespace n_VosVer
{
public static class VosVer
{
	public static string CPU_VersionList;
	
	//初始化 - 未调用
	public static void Init()
	{
		AddVersion( "GD32E103CBT6:   2.32.B" );
		AddVersion( "GD32E230C8T6:   2.41.A" );
		AddVersion( "GD32E231C8T6:   2.41.A" );
		AddVersion( "GD32F303CGT6:   2.30.A" );
		AddVersion( "GD32F303RGT6:   2.30.A" );
		AddVersion( "GD32F330C8T6:   2.50.A" );
		AddVersion( "GD32VF103C8T6:  2.30.B" );
		AddVersion( "GD32VF103CBT6:  2.32.B" );
		AddVersion( "STM32F103C8T6:  2.50.A" );
		AddVersion( "STM32F103RCT6:  2.30.A" );
		AddVersion( "STM32F103RET6:  2.30.A" );
		AddVersion( "STM32F103ZET6:  2.30.A" );
		AddVersion( "STM32F405RGT6:  2.30.A" );
		AddVersion( "STM32F411RET6:  2.30.A" );
		AddVersion( "hi3861:         2.40.A" );
		AddVersion( "ESP32:          2.61.A" );
		AddVersion( "ESP32C3:        2.61.A" );
		AddVersion( "M482SIDAE:      2.32.B" );
		AddVersion( "W800:           2.40.A" );
		AddVersion( "W806:           2.40.A" );
		
		CPU_VersionList = CPU_VersionList.TrimEnd( '\n' );
	}
	
	static void AddVersion( string v )
	{
		CPU_VersionList += v + "\n";
	}
}
//芯片类型
public static class CHIP
{
	public static string STM32F103C8T6 = "STM32F103C8T6";
	public static string GD32VF103C8T6 = "GD32VF103C8T6";
	public static string GD32VF103CBT6 = "GD32VF103CBT6";
	public static string GD32W515 = "GD32W515";
	public static string hi3861 = "hi3861";
	public static string CH32V103R8T6 = "CH32V103R8T6";
	public static string AB32VG1 = "AB32VG1";
	public static string N32WB031 = "N32WB031";
	public static string TG7100C = "TG7100C";
}
}



