﻿
namespace n_HexCoder
{
using System;
using System.Collections.Generic;
using System.Text;
using System.Windows.Forms;
using n_OS;
using System.IO;

public static class HexCoder
{
	public static byte[] CodeList;
	public static int CodeLength;
	
	static string CFile;
	static string Ext;
	
	//初始化
	public static void Init()
	{
		
	}
	
	//打开
	public static string Load( string FilePath )
	{
		CFile = FilePath;
		Ext = FilePath.Remove( 0, FilePath.LastIndexOf( '.' ) ).ToLower();
		
		if( Ext == ".hex" ) {
			return LoadHex( FilePath );
		}
		else if( Ext == ".elf" ) {
			return LoadBin( FilePath );
		}
		else if( Ext == ".bin" ) {
			return LoadBin( FilePath );
		}
		else if( Ext == ".axf" ) {
			return LoadBin( FilePath );
		}
		else {
			return LoadBin( FilePath );
		}
	}
	
	//保存
	public static string Save( string FilePath )
	{
		if( Ext == ".hex" ) {
			return SaveHex( FilePath );
		}
		else if( Ext == ".elf" ) {
			return SaveBin( FilePath );
		}
		else if( Ext == ".bin" ) {
			return SaveBin( FilePath );
		}
		else if( Ext == ".axf" ) {
			return SaveBin( FilePath );
		}
		else {
			return SaveBin( FilePath );
		}
	}
	
	//-------------------------------------------------------
	//1. 转换 bin 文件
	
	//转换 bin 文件数据为byte格式
	public static string LoadBin( string FilePath )
	{
		FileStream fs = new System.IO.FileStream( FilePath, System.IO.FileMode.Open );
		BinaryReader br = new System.IO.BinaryReader( fs, Encoding.Default );
		
		CodeLength = (int)fs.Length;
		CodeList = br.ReadBytes( CodeLength );
		
		br.Close();
		fs.Close();
		
		Res_Clear();
		for( int i = 0; i < CodeLength; ++i ) {
			Res_AddByte( i, CodeList[i] );
			if( i % 16 == 15 ) {
				Res_AddLine();
			}
		}
		return Res_get();
	}
	
	//保存bin文件
	public static string SaveBin( string FilePath )
	{
		FileStream fs = new System.IO.FileStream( FilePath, System.IO.FileMode.Open );
		BinaryWriter bw = new System.IO.BinaryWriter( fs, Encoding.Default );
		
		bw.Write( CodeList, 0, CodeLength );
		bw.Flush();
		
		bw.Close();
		fs.Close();
		
		Res_Clear();
		for( int i = 0; i < CodeLength; i++ ) {
			
			Res_AddByte( i, CodeList[i] );
			if( i % 16 == 15 ) {
				Res_AddLine();
			}
		}
		return Res_get();
	}
	
	//-------------------------------------------------------
	//2. 转换 hex 文件
	
	static string LinearAddr;
	static string StartAddr;
	static string SectionAddr;
	
	static bool[] NullList;
	
	//全局基地址, 数值为 0xXXXX0000;
	static int ST_BaseAddr;
	
	//转换HEX文件数据为byte格式
	public static string LoadHex( string FilePath )
	{
		string Code = VIO.OpenTextFileUTF8( FilePath );
		
		//hex文件需要每次创建, 因为其他bin文件不需要创建缓冲区, 读取文件即可获得
		NullList = new bool[200000];
		CodeList = new byte[200000];
		CodeLength = 0;
		
		LinearAddr = null;
		StartAddr = null;
		SectionAddr = null;
		
		ST_BaseAddr = 0;
		//当前段的基地址, 数值为 0xXXXX;
		int CurBaseAddr = 0;
		
		Code = Code.TrimEnd( "\n\t ".ToCharArray() );
		string[] Lines = Code.Split( '\n' );
		
		Res_Clear();
		
		int CAddress = 0;	//当前地址
		
		for( int i = 0; i < Lines.Length; ++i ) {
			//判断格式是否正确
			if( !Lines[ i ].StartsWith( ":" ) ) {
				MessageBox.Show( "HEX文件格式错误, 每条记录应该以字符':'开始" );
				return null;
			}
			string SLine = Lines[ i ];
			if( SLine.Length % 2 != 1 || SLine.Length < 11 ) {
				MessageBox.Show( "HEX文件格式错误, 当前记录的长度无效" );
				return null;
			}
			//读取当前记录的各个字段
			string DataLength = SLine.Substring( 1, 2 );
			string Address = SLine.Substring( 3, 4 );
			string Type = SLine.Substring( 7, 2 );
			string Data = SLine.Substring( 9, SLine.Length - 11 );
			string CheckSum = SLine.Substring( SLine.Length - 2, 2 );
			
			//判断记录长度是否合法
			int IdataLength = int.Parse( DataLength, System.Globalization.NumberStyles.HexNumber );
			if( IdataLength * 2 != Data.Length ) {
				MessageBox.Show( "HEX文件格式错误, 当前记录的长度无效: " + IdataLength + " " + DataLength );
				return null;
			}
			//判断校验和
			int IcheckSum = 0;
			for( int j = 1; j < SLine.Length; j += 2 ) {
				IcheckSum += int.Parse( SLine[ j ].ToString() + SLine[ j + 1 ], System.Globalization.NumberStyles.HexNumber );
				IcheckSum %= 256;
			}
			if( IcheckSum != 0 ) {
				MessageBox.Show( "HEX文件格式错误, 校验和出错: " + SLine );
				return null;
			}
			//判断是否结束记录
			if( Type == "01" ) {
				if( i != Lines.Length - 1 ) {
					MessageBox.Show( "HEX文件格式错误, 结束记录标志之后不能有记录" );
					return null;
				}
				break;
			}
			//解析03记录, 开始段地址
			if( Type == "03" ) {
				//LinearAddr = int.Parse( Data, System.Globalization.NumberStyles.HexNumber );
				SectionAddr = Lines[i] + "\n";
				continue;
			}
			//解析04记录, 扩展线性地址, 此地址和00字段的16位地址合并构成32位地址
			if( Type == "04" ) {
				//LinearAddr = int.Parse( Data, System.Globalization.NumberStyles.HexNumber );
				LinearAddr = Lines[i] + "\n";
				
				CurBaseAddr = int.Parse( Data, System.Globalization.NumberStyles.HexNumber );
				if( ST_BaseAddr == 0 ) {
					ST_BaseAddr = CurBaseAddr;
				}
				CAddress = CurBaseAddr * 0x10000;
				CodeLength = CurBaseAddr * 0x10000 - ST_BaseAddr * 0x10000;
				continue;
			}
			//解析5记录, 起始地址
			if( Type == "05" ) {
				//StartAddr = int.Parse( Data, System.Globalization.NumberStyles.HexNumber );
				StartAddr = Lines[i] + "\n";
				continue;
			}
			//忽略其他类型记录
			if( Type != "00" ) {
				MessageBox.Show( "未处理的HEX文件格式: " + Type );
				continue;
			}
			//填充默认数据
			int IAddress = CurBaseAddr * 0x10000 + int.Parse( Address, System.Globalization.NumberStyles.HexNumber );
			while( CAddress < IAddress ) {
				
				Res_AddByte( CodeLength, 0xFF );
				CodeList[CodeLength] = 0xFF;
				NullList[CodeLength] = true;
				CodeLength++;
				
				++CAddress;
				if( CAddress % 16 == 0 ) {
					Res_AddLine();
				}
			}
			//填充实际数据
			int Index = 0;
			while( CAddress < IAddress + Data.Length / 2 ) {
				
				int Value = int.Parse( Data[ Index ].ToString() + Data[ Index + 1 ], System.Globalization.NumberStyles.HexNumber );
				
				Res_AddByte( CodeLength, (byte)Value );
				CodeList[CodeLength] = (byte)Value;
				CodeLength++;
				
				Index += 2;
				++CAddress;
				if( CAddress % 16 == 0 ) {
					Res_AddLine();
				}
			}
		}
		return Res_get();
	}
	
	//转换为机器代码记录
	public static string SaveHex( string FilePath )
	{
		string result = ""; //LinearAddr;
		
		string Current = "";
		int StAddr = 0;
		int LastStartBaseAddr = -1;
		
		bool IgnoreNone = true;
		
		for( int i = 0; i < CodeLength; ++i ) {
			
			//判断是否需要添加起始段地址
			int addr = (StAddr / 0x10000) + ST_BaseAddr;
			if( LastStartBaseAddr != addr ) {
				LastStartBaseAddr = addr;
				string Check = "02000004" + addr.ToString( "X" ).PadLeft( 4, '0' );
				result += ":" + Check + GetCheckSum( Check ) + "\n";
			}
			
			Current += CodeList[i].ToString( "X" ).PadLeft( 2, '0' );
			
			//暂不处理空数据区, 直接填充
			bool isNull = IgnoreNone && (i+1 < CodeLength && NullList[i+1]);
			
			if( i == CodeLength - 1 || i % 16 == 15 || isNull ) {
				int SubAddr = StAddr % 0x10000;
				string Check = (Current.Length / 2).ToString( "X" ).PadLeft( 2, '0' ) + SubAddr.ToString( "X" ).PadLeft( 4, '0' ) + "00" + Current;
				result += ":" + Check + GetCheckSum( Check ) + "\n";
				Current = "";
				
				if( IgnoreNone ) {
					while( i+1 < CodeLength && NullList[i+1] ) {
						i++;
					}
				}
				
				StAddr = ( i + 1 );
			}
		}
		result += StartAddr;
		result += SectionAddr;
		result += ":00000001FF\n";
		
		VIO.SaveTextFileUTF8( FilePath, result );
		return result;
		
		
	}
	
	//计算校验和
	static string GetCheckSum( string number )
	{
		int Sum = 0;
		for( int i = 0; i < number.Length; i += 2 ) {
			Sum += int.Parse( number[ i ].ToString() + number[ i + 1 ].ToString(), System.Globalization.NumberStyles.HexNumber );
			Sum = Sum % 256;
		}
		Sum = 256 - Sum;
		if( Sum == 256 ) {
			Sum = 0;
		}
		return Sum.ToString( "X" ).PadLeft( 2, '0' );
	}
	
	//-------------------------------------------------------
	//公共类
	static StringBuilder Res_TextBuffer;
	static string Res_TextChar;
	
	static void Res_Clear()
	{
		Res_TextBuffer = new StringBuilder( "" );
		Res_TextChar = null;
	}
	
	static void Res_AddByte( int Addr, byte c )
	{
		if( Res_TextChar == null ) {
			Res_TextBuffer.Append( Addr.ToString( "X" ).PadLeft( 6, '0' ) + ": " );
		}
		Res_TextBuffer.Append( c.ToString( "X" ).PadLeft( 2, '0' ) + " " );
		if( c >= 32 && c <= 126 ) {
			Res_TextChar += (char)c;
		}
		else {
			Res_TextChar += '.';
		}
	}
	
	static void Res_AddLine()
	{
		Res_TextBuffer.Append( "    " );
		Res_TextBuffer.Append( Res_TextChar );
		Res_TextBuffer.Append( "\n" );
		Res_TextChar = null;
	}
	
	static string Res_get()
	{
		return Res_TextBuffer.ToString();
	}
}
}

