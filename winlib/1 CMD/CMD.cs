﻿
using System;
using System.Diagnostics;
using System.Windows.Forms;
using n_AVRdudeSetForm;
using System.IO;

namespace n_CMD
{
public static class CMD
{
	public delegate void deleProgressStep( int Max, int Current );
	public static deleProgressStep ProgressStep;
	
	public static string OutputResult;
	
	static int PerTime;
	static RichTextBox rr;
	
	//初始化
	public static void Init()
	{
		PerTime = 20;
	}
	
	//下载
	public static void Download( RichTextBox r, string ToolPath, string CmdVar1, string BinFile, string CmdVar2 )
	{
		rr = r;
		
		Process Proc = new Process();
		Proc.StartInfo.WindowStyle = ProcessWindowStyle.Hidden;
		Proc.StartInfo.UseShellExecute = false;
		Proc.StartInfo.CreateNoWindow = true;
		Proc.StartInfo.RedirectStandardOutput = true;
		Proc.StartInfo.RedirectStandardError = true;
		//Proc.StartInfo.WorkingDirectory = AppDomain.CurrentDomain.BaseDirectory + @"Resource\tools\" + AVRdudePath;
		
		//Proc.StartInfo.FileName = AppDomain.CurrentDomain.BaseDirectory +  @"Resource\tools\pico\rp2040tools\1.0.2\rp2040load.exe";
		//Proc.StartInfo.Arguments = @"-v -D " + AppDomain.CurrentDomain.BaseDirectory + @"Resource\tools\pico\Blink.ino.elf";
		
		Proc.StartInfo.FileName = ToolPath;
		Proc.StartInfo.Arguments = CmdVar1 + BinFile + CmdVar2;
		
		OutputResult = "";
		
		Proc.Start();
		
		Proc.BeginOutputReadLine();
		Proc.BeginErrorReadLine();
		
		Proc.OutputDataReceived += new DataReceivedEventHandler(DataHandler);
		Proc.ErrorDataReceived += new DataReceivedEventHandler(EDataHandler);
		
		int i = 0;
		
		int maxnumber = 10000;
		while( !Proc.HasExited ) {
			System.Windows.Forms.Application.DoEvents();
			if( ProgressStep != null ) {
				int x = i * PerTime;
				if( x > maxnumber ) {
					x = maxnumber;
				}
				ProgressStep( maxnumber, x );
			}
			
			//rr.Text = Proc.StandardOutput.ReadToEnd();
			//rr.SelectionStart = rr.TextLength - 1;
			
			i++;
		}
		int tPerTime = maxnumber / i;
		if( tPerTime != 0 ) {
			PerTime = tPerTime;
		}
	}
	
	static void DataHandler(object sendingProcess, DataReceivedEventArgs e)
	{
		if(!String.IsNullOrEmpty(e.Data)) {
			OutputResult += e.Data + "\n";
			rr.Text = OutputResult;
			rr.SelectionStart = rr.TextLength - 1;
			//n_wlibCom.wlibCom.SetFont( rr );
		}
	}
	
	static void EDataHandler(object sendingProcess, DataReceivedEventArgs e)
	{
		if(!String.IsNullOrEmpty(e.Data)) {
			OutputResult += e.Data + "\n";
			rr.Text = OutputResult;
			rr.SelectionStart = rr.TextLength - 1;
			//n_wlibCom.wlibCom.SetFont( rr );
		}
	}
}
}



