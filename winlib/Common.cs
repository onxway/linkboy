﻿
using System;
using System.Drawing;
using System.Windows.Forms;

namespace n_wlibCom
{
public static class wlibCom
{
	public static n_VosVersForm.VosVersForm VBox;
	
	//初始化
	public static void Init()
	{
		n_VosVer.VosVer.Init();
	}
	
	//设置一个RichTextBox控件为等宽文本
	public static void SetFont( RichTextBox richTextBox )
	{
		try {
			richTextBox.SelectAll();
			richTextBox.SelectionFont = new Font( "微软雅黑", 11 );
			richTextBox.Font = new Font( "微软雅黑", 11 );
			richTextBox.SelectionLength = 0;
		}
		catch {}
		
		try {
			richTextBox.SelectAll();
			richTextBox.SelectionFont = new Font( "Courier New", 11 );
			richTextBox.Font = new Font( "Courier New", 11 );
			richTextBox.SelectionLength = 0;
		}
		catch {}
		
		try {
			richTextBox.SelectAll();
			richTextBox.SelectionFont = new Font( "consolas", 11 );
			richTextBox.Font = new Font( "consolas", 11 );
			richTextBox.SelectionLength = 0;
		}
		catch {}
	}
}
public static class ColorDlg
{
	public static Color SelectColor;
	
	static int[] CList;
	static ColorDialog cd;
	static int Cindex = 0;
	
	public static bool ShowColorDlgOk( Color c )
	{
		if( cd == null ) {
			cd = new ColorDialog();
		}
		AddNewColor( c );
		
		cd.CustomColors = CList;
		if( cd.ShowDialog() == DialogResult.OK ) {
			SelectColor = cd.Color;
			
			AddNewColor( cd.Color );
			return true;
		}
		else {
			return false;
		}
	}
	
	//添加一个颜色
	static void AddNewColor( Color c )
	{
		int cidx = ColorTranslator.ToOle( c );
		
		if( CList == null ) {
			CList = new int[1];
			CList[0] = cidx;
		}
		else {
			bool ok = false;
			for( int i = 0; i < CList.Length; ++i ) {
				if( CList[i] == cidx ) {
					ok = true;
					break;
				}
			}
			if( !ok ) {
				if( CList.Length >= 8 ) {
					CList[Cindex] = cidx;
					Cindex++;
					Cindex %= 8;
				}
				else {
					int[] CListt = new int[CList.Length+1];
					for( int i = 0; i < CList.Length; ++i ) {
						CListt[i] = CList[i];
					}
					CListt[CList.Length] = cidx;
					CList = CListt;
				}
			}
		}
	}
}
}



