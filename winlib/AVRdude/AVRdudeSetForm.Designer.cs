﻿/*
 * 由SharpDevelop创建。
 * 用户： dell
 * 日期: 2015/11/15
 * 时间: 8:29
 * 
 * 要改变这种模板请点击 工具|选项|代码编写|编辑标准头文件
 */
namespace n_AVRdudeSetForm
{
	partial class AVRdudeSetForm
	{
		/// <summary>
		/// Designer variable used to keep track of non-visual components.
		/// </summary>
		private System.ComponentModel.IContainer components = null;
		public System.Windows.Forms.ComboBox comboBoxPort;
		private System.Windows.Forms.ComboBox comboBoxChip;
		private System.Windows.Forms.Label label1;
		private System.Windows.Forms.Label label2;
		private System.Windows.Forms.Label label3;
		private System.Windows.Forms.ComboBox comboBoxBaud;
		private System.Windows.Forms.Button buttonOk;
		
		/// <summary>
		/// Disposes resources used by the form.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing) {
				if (components != null) {
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}
		
		/// <summary>
		/// This method is required for Windows Forms designer support.
		/// Do not change the method contents inside the source code editor. The Forms designer might
		/// not be able to load this method if it was changed manually.
		/// </summary>
		private void InitializeComponent()
		{
			this.comboBoxPort = new System.Windows.Forms.ComboBox();
			this.comboBoxChip = new System.Windows.Forms.ComboBox();
			this.label1 = new System.Windows.Forms.Label();
			this.label2 = new System.Windows.Forms.Label();
			this.label3 = new System.Windows.Forms.Label();
			this.comboBoxBaud = new System.Windows.Forms.ComboBox();
			this.buttonOk = new System.Windows.Forms.Button();
			this.label8 = new System.Windows.Forms.Label();
			this.buttonDriver = new System.Windows.Forms.Button();
			this.button1 = new System.Windows.Forms.Button();
			this.pictureBox1 = new System.Windows.Forms.PictureBox();
			this.label5 = new System.Windows.Forms.Label();
			((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
			this.SuspendLayout();
			// 
			// comboBoxPort
			// 
			this.comboBoxPort.BackColor = System.Drawing.Color.SandyBrown;
			this.comboBoxPort.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
			this.comboBoxPort.Font = new System.Drawing.Font("微软雅黑", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
			this.comboBoxPort.ForeColor = System.Drawing.Color.Blue;
			this.comboBoxPort.FormattingEnabled = true;
			this.comboBoxPort.Location = new System.Drawing.Point(123, 9);
			this.comboBoxPort.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
			this.comboBoxPort.Name = "comboBoxPort";
			this.comboBoxPort.Size = new System.Drawing.Size(452, 39);
			this.comboBoxPort.TabIndex = 0;
			this.comboBoxPort.Click += new System.EventHandler(this.ComboBoxPortClick);
			// 
			// comboBoxChip
			// 
			this.comboBoxChip.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
			this.comboBoxChip.Font = new System.Drawing.Font("微软雅黑", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
			this.comboBoxChip.ForeColor = System.Drawing.Color.Black;
			this.comboBoxChip.FormattingEnabled = true;
			this.comboBoxChip.Items.AddRange(new object[] {
									"m328p",
									"m168p"});
			this.comboBoxChip.Location = new System.Drawing.Point(384, 120);
			this.comboBoxChip.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
			this.comboBoxChip.Name = "comboBoxChip";
			this.comboBoxChip.Size = new System.Drawing.Size(192, 39);
			this.comboBoxChip.TabIndex = 1;
			this.comboBoxChip.SelectedIndexChanged += new System.EventHandler(this.ComboBoxChipSelectedIndexChanged);
			// 
			// label1
			// 
			this.label1.Font = new System.Drawing.Font("微软雅黑", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
			this.label1.ForeColor = System.Drawing.Color.Black;
			this.label1.Location = new System.Drawing.Point(16, 8);
			this.label1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
			this.label1.Name = "label1";
			this.label1.Size = new System.Drawing.Size(110, 44);
			this.label1.TabIndex = 2;
			this.label1.Text = "串口号";
			this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// label2
			// 
			this.label2.Font = new System.Drawing.Font("微软雅黑", 12F);
			this.label2.ForeColor = System.Drawing.Color.Gray;
			this.label2.Location = new System.Drawing.Point(18, 114);
			this.label2.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
			this.label2.Name = "label2";
			this.label2.Size = new System.Drawing.Size(357, 50);
			this.label2.TabIndex = 3;
			this.label2.Text = "型号";
			this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// label3
			// 
			this.label3.Font = new System.Drawing.Font("微软雅黑", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
			this.label3.ForeColor = System.Drawing.Color.Chocolate;
			this.label3.Location = new System.Drawing.Point(18, 68);
			this.label3.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
			this.label3.Name = "label3";
			this.label3.Size = new System.Drawing.Size(357, 42);
			this.label3.TabIndex = 4;
			this.label3.Text = "如下载失败请尝试57600 →";
			this.label3.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// comboBoxBaud
			// 
			this.comboBoxBaud.Font = new System.Drawing.Font("微软雅黑", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
			this.comboBoxBaud.ForeColor = System.Drawing.Color.DarkGray;
			this.comboBoxBaud.FormattingEnabled = true;
			this.comboBoxBaud.Items.AddRange(new object[] {
									"57600",
									"115200"});
			this.comboBoxBaud.Location = new System.Drawing.Point(384, 68);
			this.comboBoxBaud.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
			this.comboBoxBaud.Name = "comboBoxBaud";
			this.comboBoxBaud.Size = new System.Drawing.Size(192, 39);
			this.comboBoxBaud.TabIndex = 5;
			this.comboBoxBaud.SelectedIndexChanged += new System.EventHandler(this.ComboBoxBaudSelectedIndexChanged);
			// 
			// buttonOk
			// 
			this.buttonOk.BackColor = System.Drawing.Color.CornflowerBlue;
			this.buttonOk.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
			this.buttonOk.Font = new System.Drawing.Font("微软雅黑", 21.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
			this.buttonOk.ForeColor = System.Drawing.Color.White;
			this.buttonOk.Location = new System.Drawing.Point(18, 172);
			this.buttonOk.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
			this.buttonOk.Name = "buttonOk";
			this.buttonOk.Size = new System.Drawing.Size(560, 74);
			this.buttonOk.TabIndex = 6;
			this.buttonOk.Text = "开始下载";
			this.buttonOk.UseVisualStyleBackColor = false;
			this.buttonOk.Click += new System.EventHandler(this.ButtonOkClick);
			// 
			// label8
			// 
			this.label8.Font = new System.Drawing.Font("微软雅黑", 10.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
			this.label8.ForeColor = System.Drawing.Color.LightSlateGray;
			this.label8.Location = new System.Drawing.Point(18, 267);
			this.label8.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
			this.label8.Name = "label8";
			this.label8.Size = new System.Drawing.Size(556, 42);
			this.label8.TabIndex = 14;
			this.label8.Text = "如找不到串口号，请先安装相应驱动：";
			this.label8.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// buttonDriver
			// 
			this.buttonDriver.BackColor = System.Drawing.Color.LightSlateGray;
			this.buttonDriver.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
			this.buttonDriver.Font = new System.Drawing.Font("微软雅黑", 10.5F);
			this.buttonDriver.ForeColor = System.Drawing.Color.White;
			this.buttonDriver.Location = new System.Drawing.Point(384, 262);
			this.buttonDriver.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
			this.buttonDriver.Name = "buttonDriver";
			this.buttonDriver.Size = new System.Drawing.Size(194, 52);
			this.buttonDriver.TabIndex = 15;
			this.buttonDriver.Text = "查看自带驱动";
			this.buttonDriver.UseVisualStyleBackColor = false;
			this.buttonDriver.Click += new System.EventHandler(this.ButtonDriverClick);
			// 
			// button1
			// 
			this.button1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(209)))), ((int)(((byte)(217)))), ((int)(((byte)(222)))));
			this.button1.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
			this.button1.Font = new System.Drawing.Font("微软雅黑", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
			this.button1.ForeColor = System.Drawing.Color.LightSlateGray;
			this.button1.Location = new System.Drawing.Point(568, 676);
			this.button1.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
			this.button1.Name = "button1";
			this.button1.Size = new System.Drawing.Size(560, 52);
			this.button1.TabIndex = 16;
			this.button1.Text = "仿真正常的程序下载到主板后不工作，怎么办？";
			this.button1.UseVisualStyleBackColor = false;
			this.button1.Click += new System.EventHandler(this.Button1Click);
			// 
			// pictureBox1
			// 
			this.pictureBox1.Location = new System.Drawing.Point(596, 9);
			this.pictureBox1.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
			this.pictureBox1.Name = "pictureBox1";
			this.pictureBox1.Size = new System.Drawing.Size(642, 556);
			this.pictureBox1.TabIndex = 17;
			this.pictureBox1.TabStop = false;
			// 
			// label5
			// 
			this.label5.Font = new System.Drawing.Font("微软雅黑", 10.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
			this.label5.ForeColor = System.Drawing.Color.ForestGreen;
			this.label5.Location = new System.Drawing.Point(740, 585);
			this.label5.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
			this.label5.Name = "label5";
			this.label5.Size = new System.Drawing.Size(558, 258);
			this.label5.TabIndex = 1;
			this.label5.Text = "1、检查实物接线确保和程序连线完全一致；\r\n2、编写程序单独测试每个模块，例如初始化让蜂鸣器发声、按钮控制主板指示灯亮灭等；\r\n3、找到最终出现问题的模块后，重新" +
			"插拔相关的杜邦线、面包板线，防止接触不良；\r\n4、注意新版nano主板固件为115200（旧版为57600）\r\n5、如果实在解决不了，可联系linkboy官方技" +
			"术支持 QQ910360201或者电话13693200752";
			// 
			// AVRdudeSetForm
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 18F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.BackColor = System.Drawing.Color.WhiteSmoke;
			this.ClientSize = new System.Drawing.Size(596, 336);
			this.Controls.Add(this.comboBoxPort);
			this.Controls.Add(this.pictureBox1);
			this.Controls.Add(this.button1);
			this.Controls.Add(this.label5);
			this.Controls.Add(this.comboBoxBaud);
			this.Controls.Add(this.comboBoxChip);
			this.Controls.Add(this.buttonDriver);
			this.Controls.Add(this.label8);
			this.Controls.Add(this.buttonOk);
			this.Controls.Add(this.label3);
			this.Controls.Add(this.label2);
			this.Controls.Add(this.label1);
			this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
			this.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
			this.MaximizeBox = false;
			this.MinimizeBox = false;
			this.Name = "AVRdudeSetForm";
			this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
			this.Text = "arduino串口下载器";
			this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.AVRdudeSetFormFormClosing);
			((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
			this.ResumeLayout(false);
		}
		private System.Windows.Forms.PictureBox pictureBox1;
		private System.Windows.Forms.Button button1;
		private System.Windows.Forms.Label label5;
		private System.Windows.Forms.Button buttonDriver;
		private System.Windows.Forms.Label label8;
	}
}
