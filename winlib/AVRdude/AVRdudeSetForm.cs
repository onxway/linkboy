﻿
using System;
using System.Drawing;
using System.Windows.Forms;
using System.IO.Ports;
using n_MySerialPort;

namespace n_AVRdudeSetForm
{
	public partial class AVRdudeSetForm : Form
	{
		public string Result;
		string InChip;
		
		bool tempWPath;
		string WPath;
		Bitmap mesImage;
		
		string spath;
		
		//构造函数
		public AVRdudeSetForm()
		{
			InitializeComponent();
			comboBoxChip.SelectedIndex = 0;
			
			this.Visible = false;
		}
		
		//运行窗体
		public string Run( string Chip, string Baud )
		{
			InChip = Chip;
			
			//判断是否需要设置提示图片/
			//这里高分辨率DPI下导致窗体显示一半...
			if( spath == null ) {
				tempWPath = false;
				//this.Width = 398;
			}
			else {
				tempWPath = true;
				//this.Width = 842;
				//this.Height = 427;
				if( WPath != spath ) {
					WPath = spath;
					
					//重新装载图片文件
					mesImage = new Bitmap( spath );
					pictureBox1.BackgroundImage = mesImage;
					pictureBox1.BackgroundImageLayout = ImageLayout.Zoom;
					//this.Text += " L";
				}
			}
			
			label3.Visible = n_AVRdude.AVRdude.isNano;
			
			Result = null;
			
			if( Chip == "m328p" ) {
				this.comboBoxChip.Visible = true;
				label2.Visible = true;
			}
			else {
				this.comboBoxChip.Visible = false;
				label2.Visible = false;
			}
			this.comboBoxBaud.Text = Baud;
			
			if( !n_AVRdude.AVRdude.isIDE && !tempWPath && this.comboBoxPort.SelectedIndex != -1 ) {
				ButtonOkClick( null, null );
			}
			else {
				//注意,直接设置Visible不能使窗体获得焦点
				//this.Visible = true;
				this.ShowDialog();
			}
			
			while( this.Visible ) {
				System.Windows.Forms.Application.DoEvents();
			}
			return Result;
		}
		
		//设置下载提示图片路径
		public void SetWarningImage( string path )
		{
			spath = path;
		}
		
		//判断是否存在提示图片
		public bool ExistWarning()
		{
			return tempWPath;
		}
		
		//确定键按下
		void ButtonOkClick(object sender, EventArgs e)
		{
			if( this.comboBoxPort.SelectedIndex == -1 ) {
				MessageBox.Show( "请选择串口号!" );
				return;
			}
			string Name = this.comboBoxPort.Text;
			
			if( Name.StartsWith( "COM1 " ) ) {
				if( MessageBox.Show( "您可能选择了错误的的串口号， 一般来说不能使用COM1下载程序，需要选择硬件主板所对应的串口号。您确定要继续吗？",  "注意", MessageBoxButtons.YesNo ) == DialogResult.No ) {
					return;
				}
			}
			if( this.comboBoxChip.Visible ) {
				InChip = this.comboBoxChip.Text;
			}
			Result = MySerialPort.GetComName( Name ) + " " + InChip + " " + this.comboBoxBaud.Text;
			
			this.Visible = false;
		}
		
		//窗口关闭
		void AVRdudeSetFormFormClosing(object sender, FormClosingEventArgs e)
		{
			e.Cancel = true;
			
			this.Visible = false;
		}
		
		void ComboBoxPortClick(object sender, EventArgs e)
		{
			this.comboBoxPort.Items.Clear();
			
			//通过WMI获取COM端口
			string[] ss = MySerialPort.GetFullNameList();
			if( ss != null ) {
				this.comboBoxPort.Items.AddRange( ss );
			}
		}
		
		void ButtonDriverClick(object sender, EventArgs e)
		{
			string fileToSelect = n_OS.OS.SystemRoot + "常见USB接口芯片驱动" + n_OS.OS.PATH_S + "新版CH341usb_com驱动.EXE";
			
			string args = string.Format("/Select, {0}", fileToSelect);
			System.Diagnostics.Process.Start( new System.Diagnostics.ProcessStartInfo("Explorer.exe", args ) );
		}
		
		void Button1Click(object sender, EventArgs e)
		{
			this.Height = 412;
		}
		
		void ComboBoxBaudSelectedIndexChanged(object sender, EventArgs e)
		{
			if( n_AVRdude.AVRdude.isNano ) {
				if( comboBoxBaud.Text == "57600" ) {
					n_AVRdude.AVRdude.NanoNew = false;
					label3.Text = "如下载失败请尝试115200 →";
				}
				else {
					n_AVRdude.AVRdude.NanoNew = true;
					label3.Text = "如下载失败请尝试57600 →";
				}
			}
		}
		
		void ComboBoxChipSelectedIndexChanged(object sender, EventArgs e)
		{
			if( comboBoxChip.SelectedIndex != 0 ) {
				n_AVRdude.AVRdude.is168 = true;
			}
		}
	}
}

