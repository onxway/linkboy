﻿
//词法分析类
//CutSource() 函数处理原始输入程序
//识别基本单词和符号,获取每个词的位置信息
//如行数, 列数等,用于错误跟踪
using System;
using n_PyWordNode;
using System.Text;

namespace n_PyWordList
{
public static class WordList
{
	//初始化
	public static void Init()
	{
		Word = new WordNode[ 100000 ];
	}
	
	//清除词法表
	public static void Reset()
	{
		Length = 0;
	}
	
	//添加单词
	public static void Add( WordNode n )
	{
		Word[ Length ] = n;
		++Length;
	}
	
	//获取索引为 Index 的词
	public static string GetWord( int Index )
	{
		return Word[ Index ].Word;
	}
	
	//获取所在的文件序号
	public static int GetFileIndex( int Index )
	{
		return Word[ Index ].FileIndex;
	}
	
	//获取索引为Index的词所在的行数
	public static int GetLine( int Index )
	{
		return Word[ Index ].Line;
	}
	
	//获取索引为Index的词所在的列数
	public static int GetColumn( int Index )
	{
		return Word[ Index ].Column;
	}
	
	//获取长度
	public static int GetLength()
	{
		return Length;
	}
	
	//设置新的词法列表
	public static void SetList( WordNode[] n, int newLength )
	{
		Word = n;
		Length = newLength;
	}
	
	//获取
	public static WordNode[] GetList()
	{
		return Word;
	}
	
	//显示词语表
	public static string Show()
	{
		StringBuilder Result = new StringBuilder( "" );
		for( int i = 0; i < Length; ++i ) {
			Result.Append( i + ":\t" +
			              "文件序号-" + Word[ i ].FileIndex + ",\t" +
			              "行-" + Word[ i ].Line + ",\t" +
			              "列-" + Word[ i ].Column + ",\t" +
			              "类型序号-" + Word[ i ].TypeIndex + ",\t" +
			              "颜色序号-" + Word[ i ].ColorIndex + ",\t" +
			              "词-" + Word[ i ].Word + "," +
			              "\n" );
		}
		return Result.ToString();
	}
	
	//长度
	static int Length;
	//词法表
	static WordNode[] Word;
}
}





