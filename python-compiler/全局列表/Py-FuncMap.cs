﻿
namespace n_PyFuncMap
{
using System;

public static class PyFuncMap
{
	static int[][] ClassFuncMap;
	
	//初始化
	public static void Init()
	{
		ClassFuncMap = new int[6][];
		for(int i = 0; i < ClassFuncMap.Length; ++i ) {
			ClassFuncMap[i] = new int[n_PyByteCode.PyInnerFunc.InnerFuncNumber];
		}
	}
	
	//编译复位
	public static void Clear()
	{
		for(int i = 0; i < ClassFuncMap.Length; ++i ) {
			for(int j = 0; j < ClassFuncMap[i].Length; ++j ) {
				ClassFuncMap[i][j] = 0;
			}
		}
	}
	
	//设置指定的类和函数索引位置的地址
	public static void SetAddr( int clsi, int funci, int addr )
	{
		ClassFuncMap[clsi][funci] = addr;
	}
	
	//获取函数图的文本化结果
	public static string GetResult()
	{
		string r = "";
		
		for(int i = 0; i < ClassFuncMap.Length; ++i ) {
			for(int j = 0; j < ClassFuncMap[i].Length; ++j ) {
				r += ClassFuncMap[i][j] + ", ";
			}
			r += "\n";
		}
		return r;
	}
}
}



