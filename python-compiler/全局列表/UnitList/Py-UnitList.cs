﻿
//元件列表类
//每个节点包含如下属性:

//	string UnitName;		//元件名称
//	bool isReal;  			//是实际存储器,为真时以下参数有意义
//	
//	string DataType;		//存储区的基本数据类型 uint8 uint16 uint24 uint32
//	string AddrType;		//存储区的地址类型 uint8 uint16 uint24 uint32
//	string VarTypeSet;		//已实现的变量类型, set/get_type
//		格式: set_type1;set_type2;set_type3; ... get_type1;get_type2; ...

//	bool AutoAllot;			//自动分配,为真时以下参数有意义
//	int StartAddress;		//元件存储区域起点地址
//	int EndAddress;			//元件存储区域终点地址

namespace n_PyUnitList
{
using System;
using n_PyUnitNode;
using n_PyMemberType;
using System.Collections.Generic;

public static class UnitList
{
	//初始化
	public static void Init()
	{
		NodeSet = new UnitNode[ 1000 ];
		NodeDic = new Dictionary<string, int>();
	}
	
	//清除元件记录
	public static void Clear()
	{
		length = 0;
		NodeDic.Clear();
	}
	
	//显示
	public static string Show()
	{
		string Result = "";
		for( int i = 0; i < length; ++i ) {
			Result += i + ": " + NodeSet[ i ].ToString() + "\n";
		}
		return Result;
	}
	
	//添加元件定义
	public static int Add( UnitNode node )
	{
		NodeDic.Add( node.UnitName, length );
		
		NodeSet[ length ] = node;
		++length;
		
		return length-1;
	}
	
	//判断元件名是否存在
	public static bool isExist( string Name )
	{
		return NodeDic.ContainsKey( Name );
		
//		for( int i = 0; i < length; ++i ) {
//			if( NodeSet[ i ].UnitName == Name ) {
//				return true;
//			}
//		}
//		return false;
	}
	
	//根据元件名获取元件直接索引
	public static int GetDirectIndex( string Name )
	{
		if( NodeDic.ContainsKey( Name ) ) {
			return NodeDic[Name];
		}
		else {
			return -1;
		}
		
//		for( int i = 0; i < length; ++i ) {
//			if( NodeSet[ i ].UnitName == Name ) {
//				return i;
//			}
//		}
//		return -1;
	}
	
	//根据元件名获取元件索引
	public static int GetIndex( string Name )
	{
		if( Name == null ) {
			return -1;
		}
		int UnitIndex = GetDirectIndex( Name );
		if( UnitIndex == -1 ) {
			string[] Split = Name.Split( '.' );
			string BaseUnitName = Split[ 0 ];
			for( int i = 1; i < Split.Length; ++i ) {
				BaseUnitName += "." + Split[ i ];
				UnitIndex = GetDirectIndex( BaseUnitName );
				if( UnitIndex == -1 ) {
					return UnitIndex;
				}
				if( NodeSet[ UnitIndex ].RealRefType == MemberType.RealRefType.link ) {
					BaseUnitName = NodeSet[ UnitIndex ].TargetMemberName;
					int Rindex = GetIndex( BaseUnitName );
					if( Rindex == -1 ) {
						return Rindex;
					}
					BaseUnitName = NodeSet[ Rindex ].UnitName;
				}
			}
		}
		if( UnitIndex == -1 ) {
			return UnitIndex;
		}
		if( NodeSet[ UnitIndex ].RealRefType == MemberType.RealRefType.Real ) {
			return UnitIndex;
		}
		else {
			return GetIndex( NodeSet[ UnitIndex ].TargetMemberName );
		}
	}
	
	//获取元件信息
	public static UnitNode Get( int Index )
	{
		return NodeSet[ Index ];
	}
	
	//获取在某个元件中的组件名集合
	public static string GetUnitNames( string UnitName )
	{
		string result = null;
		for( int i = 0; i < length; ++i ) {
			if( //NodeSet[ i ].VisitType == MemberType.VisitType.Public &&
			    NodeSet[ i ].UnitName.StartsWith( UnitName + "." ) ) {
				string Name = NodeSet[ i ].UnitName;
				Name = Name.Remove( 0, Name.LastIndexOf( "." ) + 1 );
				if( NodeSet[ i ].UnitName != UnitName + "." + Name ) {
					continue;
				}
				if( !Name.StartsWith( "OS_" ) ) {
					result += Name + ";";
				}
			}
		}
		return result;
	}
	
	static Dictionary<string, int> NodeDic;
	
	static UnitNode[] NodeSet;
	public static int length;
}
}
