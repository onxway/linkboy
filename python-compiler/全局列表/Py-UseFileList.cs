﻿
namespace n_PyUseFileList
{
using System;

public static class UseFileList
{
	//初始化
	public static void Clear()
	{
		PathList = "";
		FileNumber = 0;
	}
	
	//添加一个路径
	public static void Add( string Path )
	{
		++FileNumber;
		PathList += Path + "\n";
	}
	
	//规格化路径列表
	public static void SplitPath()
	{
		FilePathList = PathList.Split( '\n' );
	}
	
	//获取文件列表数目
	public static int GetNumber()
	{
		return FileNumber;
	}
	
	//获取第index个路径名
	public static string GetPath( int Index )
	{
		return FilePathList[ Index ];
	}
	
	static int FileNumber;
	static string[] FilePathList;
	static string PathList;
}
}



