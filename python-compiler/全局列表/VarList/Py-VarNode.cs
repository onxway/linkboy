﻿
//变量节点类
namespace n_PyVarNode
{
using System;
using n_PyDeploy;

public class VarNode
{
	//构造函数
	public VarNode( string name, int funcindex, int location, bool mstatic )
	{
		Name = name;
		FunctionIndex = funcindex;
		Location = location;
		Static = mstatic;
		
		isFunctionName = false;
		Addr = -1;
	}
	
	public string Name;
	public int Addr;
	public int FunctionIndex;
	public int Location;
	public bool Static;
	public bool isFunctionName;
}
}
