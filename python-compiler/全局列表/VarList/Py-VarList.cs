﻿
//变量记录表
//	string Name;		//变量名
//	string Type;		//变量类型
//	string ActiveType;	//变量的活动类型:  静态类型-static  局部类型-local  临时类型-temp
//	string VisitType;	//变量访问类型:  public private
//	int FunctionIndex;	//变量所在的函数索引
//	string UnitName;	//变量所在的元件名
//	int Location;		//变量的使用位置
//	int Level;			//变量所在的层数
//	bool isExist;		//变量是否存在
//	string Address;		//变量的地址
//	bool isComp;		//是否复合运算
//	bool isOut;			//是否编外的变量, 是的话 分配地址时忽略


namespace n_PyVarList
{
using System;
using n_PyET;
using n_PyMemberType;
using n_PyUnitList;
using n_PyVarNode;

public static class VarList
{
	static VarNode[] NodeSet;	//节点集合
	public static int length;	//长度
	
	public static int StaticAddr;
	public static int LocalAddr;
	
	public static int FunctionNumber;
	
	//--------------------------------------------------------
	
	//软件启动初始化
	public static void Init()
	{
		NodeSet = new VarNode[ 20000 ];
	}
	
	//清除所有变量记录,重新编译
	public static void Clear()
	{
		length = 0;
		StaticAddr = 0;
		FunctionNumber = 0;
	}
	
	//判断是否可以定义静态变量
	public static bool CanDefineStatic( string Name )
	{
		for( int i = 0; i < length; ++i ) {
			if( NodeSet[ i ].Name == Name && NodeSet[ i ].Static ) {
				return false;
			}
		}
		return true;
	}
	
	//判断是否可以定义局部变量
	public static bool CanDefineLocal( int FunctionIndex, string Name )
	{
		for( int i = 0; i < length; ++i ) {
			if( NodeSet[ i ].FunctionIndex == FunctionIndex &&
				NodeSet[ i ].Name == Name ) {
				return false;
			}
		}
		return true;
	}
	
	//获取局部变量索引
	public static int GetLocalIndex( int FunctionIndex, string Name )
	{
		for( int i = length - 1; i >= 0; --i ) {
			if( !NodeSet[ i ].Static && NodeSet[ i ].Name == Name && NodeSet[ i ].FunctionIndex == FunctionIndex ) {
				return i;
			}
		}
		return -1;
	}
	
	//根据元件名获取元件直接索引
	public static int GetStaticIndex( string Name )
	{
		for( int i = length - 1; i >= 0; --i ) {
			if( NodeSet[ i ].Name == Name && NodeSet[ i ].Static ) {
				return i;
			}
		}
		return -1;
	}
	
	//添加变量
	public static int Add( VarNode v )
	{
		if( v.isFunctionName ) {
			FunctionNumber++;
		}
		
		//静态变量从1开始分配 (0为NONE)
		//局部变量从0开始分配
		if( v.Static ) {
			StaticAddr++;
			v.Addr = StaticAddr;
		}
		else {
			v.Addr = LocalAddr;
			LocalAddr++;
		}
		NodeSet[ length ] = v;
		++length;
		return length - 1;
	}
	
	//显示
	public static string Show()
	{
		string r = "";
		for( int i = 0; i < length; ++i ) {
			r += "Name:" + NodeSet[i].Name + ", FIndex:" + NodeSet[i].FunctionIndex + ", Static:" + NodeSet[i].Static + ", Addr:" + NodeSet[i].Addr + "\n";
		}
		return r;
	}
	
	//获取变量
	public static VarNode Get( int Index )
	{
		return NodeSet[ Index ];
	}
}
}


