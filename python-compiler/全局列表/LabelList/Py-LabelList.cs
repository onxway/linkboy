﻿
//标号列表类
namespace n_PyLabelList
{
using System;
using n_PyLabelNode;
using System.Collections.Generic;

public static class LabelList
{
	//初始化
	public static void Init()
	{
		NodeSet = new LabelNode[ 1000 ];
		length = 0;
	}
	
	//清除标号列表
	public static void Clear()
	{
		length = 0;
	}
	
	//显示
	public static string Show()
	{
		string Result = "";
		for( int i = 0; i < length; ++i ) {
			Result += NodeSet[ i ].ToString() + "\n";
		}
		return Result;
	}
	
	//添加标号
	public static void Add( string n, int Addr )
	{
		LabelNode f = new LabelNode( n,Addr );
		NodeSet[ length ] = f;
		++length;
	}
	
	//判断标号是否存在
	public static bool isExist( string Name )
	{
		for( int i = 0; i < length; ++i ) {
			if( NodeSet[ i ].Name == Name ) {
				return true;
			}
		}
		return false;
	}
	
	//获取地址
	public static int GetAddr( string Name )
	{
		for( int i = 0; i < length; ++i ) {
			if( NodeSet[ i ].Name == Name ) {
				return NodeSet[ i ].Addr;
			}
		}
		return -1;
	}
	
	static LabelNode[] NodeSet;
	public static int length;
}
}
