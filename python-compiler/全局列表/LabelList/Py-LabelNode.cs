﻿
//标号节点类
namespace n_PyLabelNode
{
using System;

public class LabelNode
{
	//构造函数
	public LabelNode( string name, int a )
	{
		Name = name;
		Addr = a;
	}
	
	//显示
	public override string ToString()
	{
		return "名称: " + Name + " 地址: " + Addr;
	}
	
	public string Name;
	public int Addr;
}
}
