﻿
namespace n_PyUserString
{
using System;

public static class PyUserString
{
	static string[][] List;
	static int Number;
	
	//初始化
	public static void Init()
	{
		List = new string[200][];
	}
	
	//清空
	public static void Clear()
	{
		Number = 0;
	}
	
	//添加一个路径
	public static string Add( string ustring )
	{
		string name = "PY_SYS_STRING" + Number;
		List[Number] = new string[2];
		List[Number][0] = name;
		List[Number][1] = ustring;
		++Number;
		
		return name;
	}
	
	//获取字符串定义
	public static string GetResult()
	{
		string r = null;
		for( int i = 0; i < Number; ++i ) {
			r += "[]#.code uint8 " + List[i][0] + " = { " + List[i][1] + " };\n";
		}
		return r;
	}
}
}



