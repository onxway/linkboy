﻿
//结构体列表节点信息:
// UnitName:  所在的元件名称
// Name:      结构体名称
// Size:	  结构体尺寸
// StoreType: 存储类型
// MemberSet: 成员列表是一个二维数组,
// 每个维包含5个分量:0-类型  1-名称  2-词位置  3-大小  4-偏移量

namespace n_PyStructList
{
using System;
using n_PyStructNode;
using n_PyUnitList;
using n_PyMemberType;
using System.Collections.Generic;

public static class StructList
{
	//初始化
	public static void Init()
	{
		NodeSet = new StructNode[ 1000 ];
		NodeDic = new Dictionary<string, int>();
	}
	
	//清除列表信息
	public static void Clear()
	{
		length = 0;
		NodeDic.Clear();
	}
	
	//显示
	public static string Show()
	{
		string Result = "";
		for( int i = 0; i < length; ++i ) {
			Result += NodeSet[ i ].ToString() + "\n";
		}
		return Result;
	}
	
	//添加结构体
	public static void Add( StructNode f )
	{
		NodeDic.Add( f.Name, length );
		
		NodeSet[ length ] = f;
		++length;
	}
	
	//根据元件名获取元件直接索引
	public static int GetDirectIndex( string Name )
	{
		if( NodeDic.ContainsKey( Name ) ) {
			return NodeDic[Name];
		}
		else {
			return -1;
		}
		
//		for( int i = 0; i < length; ++i ) {
//			if( NodeSet[ i ].Name == Name ) {
//				return i;
//			}
//		}
//		return -1;
	}
	
	//获取结构体索引
	public static int GetIndex( string Name )
	{
		if( Name == null ) {
			return -1;
		}
		int StructIndex = GetDirectIndex( Name );
		if( StructIndex == -1 ) {
			int SplitIndex = Name.LastIndexOf( "." );
			string UnitName = Name.Remove( SplitIndex );
			string StructName = Name.Remove( 0, SplitIndex );
			int UnitIndex = UnitList.GetIndex( UnitName );
			if( UnitIndex == -1 ) {
				return UnitIndex;
			}
			string NewName = UnitList.Get( UnitIndex ).UnitName + StructName;
			StructIndex = GetDirectIndex( NewName );
		}
		if( StructIndex == -1 ) {
			return StructIndex;
		}
		if( NodeSet[ StructIndex ].RealRefType == MemberType.RealRefType.Real ) {
			return StructIndex;
		}
		return GetIndex( NodeSet[ StructIndex ].TargetMemberName );
	}
	
	//获取元件信息
	public static StructNode Get( int Index )
	{
		return NodeSet[ Index ];
	}
	
	//获取在某个元件中的函数名集合
	public static string GetStructNames( string UnitName )
	{
		string result = null;
		for( int i = 0; i < length; ++i ) {
			if( NodeSet[ i ].UnitName == UnitName ) {//&&
			    //NodeSet[ i ].VisitType == MemberType.VisitType.Public ) {
				int fi = GetIndex( NodeSet[ i ].Name );
				string Name = NodeSet[ i ].Name;
				Name = Name.Remove( 0, Name.LastIndexOf( "." ) + 1 );
				if( !Name.StartsWith( "OS_" ) && fi != -1 ) {
					result += Name + " \t(TYPE);";
				}
			}
		}
		return result;
	}
	
	static Dictionary<string, int> NodeDic;
	
	static StructNode[] NodeSet;	//节点集合
	public static int length;		//长度
}
}
