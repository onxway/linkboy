﻿
//函数信息节点类
namespace n_PyFunctionNode
{
using System;
using System.Text;
using n_PyUnitList;
using n_PyVarType;
using n_PyVarList;

//函数节点类
public class FunctionNode
{
	//构造函数
	public FunctionNode( string vVisitType, string vRealRefType, string vInterType, string vReturnType,
	                    string vFunctionName, string vVarIndexList, int vWordIndex, string vRefValue )
	{
		VisitType = vVisitType;
		FunctionName = vFunctionName;
		VarIndexList = vVarIndexList;
		WordIndex = vWordIndex;
		
		isFast = false;
		isUsed = false;
		
		RefValue = vRefValue;
		
		LocalVarNumber = 0;
		Innercall = false;
	}
	
	//获取函数基本名字(不含所在类名称)
	public string GetBaseName()
	{
		return FunctionName.Remove( 0, FunctionName.LastIndexOf( '.' ) + 1 );
	}
	
	//显示字符串到输出信息列表
	public override string ToString()
	{
		string Result = "访问类型:" + VisitType + "\t函数名:" + FunctionName + "\t局部变量数量:" + LocalVarNumber;
		
		return Result;
	}
	
	//获取函数所在元件
	public string UnitName
	{
		get
		{
			return FunctionName.Remove( FunctionName.LastIndexOf( "." ) );
		}
	}
	
	public bool isUsed;							//函数是否被调用过, 用于代码优化
	public readonly string VisitType;			//访问类型  private  public
	public readonly string FunctionName;		//函数名
	public readonly int WordIndex;				//代表函数的词法序号
	
	public readonly string RefValue;			//接口调用类型的函数所引用的调用信息
	
	public readonly string VarIndexList;		//形参序号列表
	
	public bool Innercall;
	
	//局部变量数目(含形参)
	public int LocalVarNumber;
	
	//中断函数是否保存现场
	public bool isFast;
}
}




