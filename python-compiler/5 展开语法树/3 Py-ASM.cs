﻿
using System;
using n_PyLabelList;
using n_PyByteCode;
using n_PyUserString;

namespace n_ASM
{
public static class ASM
{
	//初始化
	public static void Init()
	{
	}
	
	//翻译到ASM代码
	public static string GetASM( string r0 )
	{
		LabelList.Clear();
		
		string SysInit = "";
		for( int i = 0; i < n_PyVarList.VarList.FunctionNumber; ++i ) {
			SysInit += "\n\n";
		}
		string[] cut = (SysInit + r0).TrimEnd( '\n' ).Split( '\n' );
		
		//提取标签和对应的地址
		int Addr = 0;
		
		for( int i = 0; i < cut.Length; ++i ) {
			cut[i] = cut[i].TrimStart( '\t' );
			if( cut[i].StartsWith( "//" ) ) {
				continue;
			}
			if( cut[i].EndsWith( ":" ) ) {
				string n = cut[i].Remove( cut[i].Length - 1 );
				LabelList.Add( n, Addr );
				cut[i] = "//" + cut[i];
				continue;
			}
			else {
				cut[i] += "//[" + Addr + "]";
			}
			Addr += 2;
		}
		//字节码处理
		for( int i = 0; i < cut.Length; ++i ) {
			cut[i] = cut[i].TrimStart( '\t' );
			if( cut[i].StartsWith( "//" ) ) {
				continue;
			}
			int notei = cut[i].IndexOf( "//" );
			string note = null;
			if( notei != -1 ) {
				note = cut[i].Remove( 0, notei );
				cut[i] = cut[i].Remove( notei );
			}
			
			string[] c = cut[i].Split( ' ' );
			
			//处理函数调用设置函数属性
			if( c[0] == PyByteCode.CALL_FUNCTION ) {
				int funcvar = int.Parse( c[1] );
				int funci = funcvar / 256;
				int varnum = funcvar % 256;
				int localN = 4;//n_PyFunctionList.FunctionList.Get( funci ).LocalVarNumber;
				
				c[1] = (localN * 256 + varnum).ToString();
			}
			
			//计算跳转标签
			if( ExistJump( c[0] ) ) {
				int a = LabelList.GetAddr( c[1] );
				c[1] = a.ToString();
			}
			if( c.Length == 1 ) {
				cut[i] = c[0] + " 0x0000";
			}
			else {
				cut[i] = c[0] + " " + c[1];
			}
			cut[i] += note;
		}
		
		//生成字符串常量定义
		string r = PyUserString.GetResult();
		
		//生成代码
		r += "[]code uint16 PYVM_INSLIST = {\n";
		
		//生成系统代码(函数索引初始化)
		int fidx  = 0;
		
		/*
		int addr0 = 8;
		n_PyVarNode.VarNode node0 = n_PyVarList.VarList.Get( 0 );
		cut[fidx*2] = PyByteCode.LOAD_CONST + " " + addr0 + cut[fidx*2];
		cut[fidx*2+1] = PyByteCode.STORE_GLOBAL + " " + node0.Addr + cut[fidx*2+1];
		fidx++;
		*/
		
		for( int i = 0; i < n_PyVarList.VarList.length; ++i ) {
			n_PyVarNode.VarNode node = n_PyVarList.VarList.Get( i );
			if( node.isFunctionName ) {
				int addr = LabelList.GetAddr( node.Name );
				
				cut[fidx*2] = PyByteCode.LOAD_CONST + " " + addr + cut[fidx*2];
				cut[fidx*2+1] = PyByteCode.STORE_GLOBAL + " " + node.Addr + cut[fidx*2+1];
				fidx++;
			}
		}
		
		//生成用户代码
		for( int i = 0; i < cut.Length; ++i ) {
		
			if( cut[i].StartsWith( "//" ) ) {
				r += "\t" + cut[i] + "\n";
				continue;
			}
			int notei = cut[i].IndexOf( "//" );
			string note = null;
			if( notei != -1 ) {
				note = cut[i].Remove( 0, notei );
				cut[i] = cut[i].Remove( notei );
			}
			r += "\tPYVM.c_" + cut[i].Replace( ' ', ',' ) + "," + note + "\n";
		}
		r += "];\n";
		
		r = r.Replace( "#addr+", "#addr " );
		r = r.Replace( "#addrw0+", "#addrw0 " );
		
		//生成函数图
		r += "[]#.code uint8 PYVM_FMAP = \n{\n" + n_PyFuncMap.PyFuncMap.GetResult() + "};\n";
		
		//配置虚拟机函数图
		r += "PYVM.FMAP = PYVM_FMAP;\n";
		
		//配置内置函数数目
		r += "const uint16 PYVM_InnerFuncNumber = " + (n_PyByteCode.PyInnerFunc.InnerFuncNumber) + ";\n";
		r += "PYVM.InnerFuncNumber = PYVM_InnerFuncNumber;\n";
		
		//配置局部变量起点
		r += "const uint16 PYVM_LOC_START = " + (n_PyVarList.VarList.StaticAddr + 1) + ";\n";
		r += "PYVM.LOC_START = PYVM_LOC_START;\n";
		
		r += "PYVM.inslist = PYVM_INSLIST;\n";
		r += "PYVM =\n";
		r += "#include <system\\python-vm\\PyVM.cx>\n";
		
		return r;
	}
	
	//判断一个指令是否为跳转类
	static bool ExistJump( string ins )
	{
		if( ins == PyByteCode.POP_JUMP_IF_FALSE ||
		    ins == PyByteCode.JUMP_IF_ERROR ||
 		    ins == PyByteCode.JUMP_FORWARD ||
 		    ins == PyByteCode.SETUP_LOOP ||
 		    ins == PyByteCode.JUMP_ABSOLUTE ) {
			return true;
		}
		else {
			return false;
		}
	}
}
}




