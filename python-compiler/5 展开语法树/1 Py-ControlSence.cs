﻿
//控制语句转化器
namespace n_PyControlSence
{
using System;
using n_PyDeploy;
using n_PyET;
using n_PyExpression;
using n_PyParseNet;
using n_PyVarList;
using n_PyVarNode;
using n_PyVarType;
using n_PyWordList;
using n_PyByteCode;

public static class ControlSence
{
	//控制语句转化器, 每个控制语句中只允许最多一个中间变量和布尔变量
	public static void 控制语句( int Index, int LastControlIndex )
	{
		++Deploy.VarLevel;
		++Deploy.SenceNumber;
		
		if( ParseNet.NodeSet[ Index ][ 0 ] == ParseNet.Node.如果语句 ) {
			if语句( Index, LastControlIndex );
		}
		else if( ParseNet.NodeSet[ Index ][ 0 ] == ParseNet.Node.如果否则语句 ) {
			ifelse语句( Index, LastControlIndex );
		}
		else if( ParseNet.NodeSet[ Index ][ 0 ] == ParseNet.Node.迭代语句 ) {
			for语句( Index );
		}
		else if( ParseNet.NodeSet[ Index ][ 0 ] == ParseNet.Node.while语句 ) {
			while语句( Index );
		}
		else {
			ET.ShowError( "<控制语句> 未定义的控制语句: " + ParseNet.NodeSet[ Index ][ 0 ] );
		}
		--Deploy.VarLevel;
	}
	
	static void if语句( int Index, int CurrentControlIndex )
	{
		int ControlNumber = Deploy.SenceNumber;
		
		Deploy.SetCLine( ParseNet.NodeSet[ Index ][ 1 ] );
		Deploy.AddNewIns();
		
		int BoolIndex = int.Parse( ParseNet.NodeSet[ Index ][ 2 ] );
		string MidVar = 计算表达式( BoolIndex );
		if( MidVar == null ) {
			return;
		}
		Deploy.AddCode( MidVar, Deploy.VarLevel );
		
		Deploy.AddCode( PyByteCode.POP_JUMP_IF_FALSE + " ifend_" + ControlNumber + "\n", Deploy.VarLevel - 1 );
		int MainSenceIndex = int.Parse( ParseNet.NodeSet[ Index ][ 5 ] );
		Deploy.语句列表( MainSenceIndex, CurrentControlIndex );
		Deploy.AddCode( "ifend_" + ControlNumber + ":\n", Deploy.VarLevel - 1 );
	}
	
	static void ifelse语句( int Index, int CurrentControlIndex )
	{
		int ControlNumber = Deploy.SenceNumber;
		
		int BoolIndex = int.Parse( ParseNet.NodeSet[ Index ][ 2 ] );
		string MidVar = 计算表达式( BoolIndex );
		if( MidVar == null ) {
			return;
		}
		Deploy.SetCLine( ParseNet.NodeSet[ Index ][ 1 ] );
		Deploy.AddNewIns();
		Deploy.AddCode( MidVar, Deploy.VarLevel );
		Deploy.AddCode( PyByteCode.POP_JUMP_IF_FALSE + " ifelse_" + ControlNumber + "\n", Deploy.VarLevel - 1 );
		int IfSenceIndex = int.Parse( ParseNet.NodeSet[ Index ][ 5 ] );
		Deploy.语句列表( IfSenceIndex, CurrentControlIndex );
		Deploy.AddCode( PyByteCode.JUMP_FORWARD + " ifend_" + ControlNumber + "\n", Deploy.VarLevel - 1 );
		Deploy.AddCode( "ifelse_" + ControlNumber + ":\n", Deploy.VarLevel - 1 );
		Deploy.SetCLine( ParseNet.NodeSet[ Index ][ 7 ] );
		Deploy.AddNewIns();
		int ElseSenceIndex = int.Parse( ParseNet.NodeSet[ Index ][ 10 ] );
		Deploy.语句列表( ElseSenceIndex, CurrentControlIndex );
		Deploy.AddCode( "ifend_" + ControlNumber + ":\n", Deploy.VarLevel - 1 );
	}
	
	static void while语句( int Index )
	{
		int ControlNumber = Deploy.SenceNumber;
		
		int BoolIndex = int.Parse( ParseNet.NodeSet[ Index ][ 2 ] );
		string MidVar = 计算表达式( BoolIndex );
		if( MidVar == null ) {
			return;
		}
		Deploy.AddCode( PyByteCode.SETUP_LOOP + " while_loop_end_" + ControlNumber + "\n", Deploy.VarLevel - 1 );
		Deploy.AddCode( "while_start_" + ControlNumber + ":\n", Deploy.VarLevel - 1 );
		
		Deploy.SetCLine( ParseNet.NodeSet[ Index ][ 1 ] );
		Deploy.AddNewIns();
		Deploy.AddCode( MidVar, Deploy.VarLevel );
		Deploy.AddCode( PyByteCode.POP_JUMP_IF_FALSE + " while_loop_else_" + ControlNumber + "\n", Deploy.VarLevel - 1 );
		int MainSenceIndex = int.Parse( ParseNet.NodeSet[ Index ][ 5 ] );
		Deploy.语句列表( MainSenceIndex, ControlNumber );
		
		//continue语句到这里
		Deploy.AddCode( "while_end_" + ControlNumber + ":\n", Deploy.VarLevel - 1 );
		Deploy.AddCode( PyByteCode.JUMP_ABSOLUTE + " while_start_" + ControlNumber + "\n", Deploy.VarLevel - 1 );
		
		
		Deploy.AddCode( "while_loop_else_" + ControlNumber + ":\n", Deploy.VarLevel - 1 );
		
		int elseindex = int.Parse( ParseNet.NodeSet[ Index ][ 7 ] );
		if( elseindex != -1 ) {
			int ElseSenceIndex = int.Parse( ParseNet.NodeSet[ elseindex ][ 4 ] );
			Deploy.语句列表( ElseSenceIndex, ControlNumber );
		}
		
		//Deploy.AddCode( PyByteCode.POP_BLOCK + "\n", Deploy.VarLevel - 1 );
		
		//break语句到这里
		Deploy.AddCode( "while_loop_end_" + ControlNumber + ":\n", Deploy.VarLevel - 1 );
	}
	
	static void for语句( int Index )
	{
		//为了每个循环只增加一个断点, 就不再开头加上断点了
		//Deploy.SetCLine( ParseNet.NodeSet[ Index ][ 1 ] );
		//Deploy.AddNewIns();
		
		int ControlNumber = Deploy.SenceNumber;
		
		int NameIndex = int.Parse( ParseNet.NodeSet[ Index ][ 2 ] );
		string Name = WordList.GetWord( NameIndex );
		
		int namei = -1;
		//判断是否为局部变量
		if( VarList.CanDefineLocal( Deploy.FunctionIndex, Name ) ) {
		
			//判断是否需要定义新变量
			if( VarList.CanDefineStatic( Name ) ) {
				
				//添加变量
				VarNode v = new VarNode( Name, Deploy.AddMain?-1:Deploy.FunctionIndex, Index, Deploy.AddMain );
				namei = VarList.Add( v );
			}
			else {
				
				//这里需要判断下是否导入了global标志
				
				namei = VarList.GetStaticIndex( Name );
			}
		}
		else {
			namei = VarList.GetLocalIndex( Deploy.FunctionIndex, Name );
		}
		
		//计算迭代对象
		int ExpIndex = int.Parse( ParseNet.NodeSet[ Index ][ 4 ] );
		string MidVar = 计算表达式( ExpIndex );
		if( MidVar == null ) {
			return;
		}
		Deploy.AddCode( MidVar, Deploy.VarLevel - 1 );
		
		//调用 __iter__ 函数
		Deploy.AddCode( Expression.SetClassFunc( PyInnerFunc.__iter__ ), Deploy.VarLevel - 1 );
		Deploy.AddCode( PyByteCode.CALL_FUNCTION + " 1\n", Deploy.VarLevel - 1 );
		
		Deploy.AddCode( PyByteCode.SETUP_LOOP + " while_loop_end_" + ControlNumber + "\n", Deploy.VarLevel - 1 );
		Deploy.AddCode( "while_start_" + ControlNumber + ":\n", Deploy.VarLevel - 1 );
		
		//每次循环都结束都会转到这里, 所以需要增加断点
		Deploy.SetCLine( ParseNet.NodeSet[ Index ][ 1 ] );
		Deploy.AddNewIns();
		
		//调用 __next__ 函数
		Deploy.AddCode( Expression.SetClassFuncSave( PyInnerFunc.__next__ ), Deploy.VarLevel - 1 );
		Deploy.AddCode( PyByteCode.CALL_FUNCTION + " 1\n", Deploy.VarLevel - 1 );
		
		Deploy.AddCode( PyByteCode.JUMP_IF_ERROR + " while_loop_else_" + ControlNumber + "\n", Deploy.VarLevel - 1 );
		
		//设置迭代变量
		if( Deploy.AddMain ) {
			Deploy.AddCode( PyByteCode.STORE_GLOBAL + " " + VarList.Get( namei ).Addr + "\n", Deploy.VarLevel - 1 );
		}
		else {
			Deploy.AddCode( PyByteCode.STORE_FAST + " " + VarList.Get( namei ).Addr + "\n", Deploy.VarLevel - 1 );
		}
		//执行函数体语句序列
		int MainSenceIndex = int.Parse( ParseNet.NodeSet[ Index ][ 7 ] );
		Deploy.语句列表( MainSenceIndex, ControlNumber );
		
		//continue语句到这里
		Deploy.AddCode( "while_end_" + ControlNumber + ":\n", Deploy.VarLevel - 1 );
		Deploy.AddCode( PyByteCode.JUMP_ABSOLUTE + " while_start_" + ControlNumber + "\n", Deploy.VarLevel - 1 );
		
		//Deploy.AddCode( PyByteCode.POP_BLOCK + "\n", Deploy.VarLevel - 1 );
		
		Deploy.AddCode( "while_loop_else_" + ControlNumber + ":\n", Deploy.VarLevel - 1 );
		
		int elseindex = int.Parse( ParseNet.NodeSet[ Index ][ 9 ] );
		if( elseindex != -1 ) {
			int ElseSenceIndex = int.Parse( ParseNet.NodeSet[ elseindex ][ 4 ] );
			Deploy.语句列表( ElseSenceIndex, ControlNumber );
		}
		
		Deploy.AddCode( "while_loop_end_" + ControlNumber + ":\n", Deploy.VarLevel - 1 );
		
		//这里需要弹出两个栈元素 第一个是next对象, 这里应该为ERROR, 第二个是迭代对象iter
		Deploy.AddCode( PyByteCode.POP_TOP + "\n", Deploy.VarLevel - 1 );
		
		Deploy.AddCode( "while_break_" + ControlNumber + ":\n", Deploy.VarLevel - 1 );
		
		Deploy.AddCode( PyByteCode.POP_TOP + "\n", Deploy.VarLevel - 1 );
	}
	
	static string 计算表达式( int Index )
	{
		if( Index == -1 ) {
			return null;
		}
		string MidVar = Expression.表达式( Index );
		if( MidVar == null ) {
			return null;
		}
		return MidVar;
	}
}
}




