﻿
//输入: 语法树 --> 中间语言指令序列
//每个指令的元素都用空格分隔(用+表示空格, 用&表示字符串连接)
//变量索引是 int 类型

namespace n_PyDeploy
{
using System;
using n_PyConfig;
using n_PyControlSence;
using n_PyET;
using n_PyExpression;
using n_PyFunctionList;
using n_PyLabelList;
using n_PyLabelNode;
using n_PyMemberType;
using n_PyOperation;
using n_PyParse;
using n_PyParseNet;
using n_PyVarList;
using n_PyVarType;
using n_PyWordList;
using n_PyCallList;
using n_PyCDebug;
using n_PyParam;
using n_PyOpti;
using n_PyByteCode;
using n_PyVarNode;
using n_PySysFlag;

public static class Deploy
{
	//把语法树的根节点转化为中间语言指令序列
	public static void TurnToSupperASM()
	{
		FunctionIndex = 0;
		SenceNumber = 0;
		
		Result = "";
		MainResult = "";
		AddMain = false;
		
		//清零标号列表
		LabelList.Clear();
		
		CallList.Clear();
		
		//清除用户常量列表
		n_PyUserString.PyUserString.Clear();
		
		Parse.DealWith( ref UnitName, new Parse.MemberListHandle( 成员列表 ) );
		
		string DefVarList = "";
		for( int i = 0; i < VarList.length; ++i ) {
			if( VarList.Get( i ).Static ) {
				DefVarList += "int32 " + VarList.Get( i ).Name + ";\n";
			}
		}
		//Result = DefVarList + "int32 _;\nvoid begin()\n{\n" + MainResult + "}\n" + Result;
		
		AddMain = true;
		AddCode( PyByteCode.LOAD_NONE + "\n", Deploy.VarLevel );
		AddCode( PyByteCode.RETURN_VALUE + "\n", Deploy.VarLevel );
		
		Result = "__main__:\n" + MainResult + Result;
	}
	
	static void 成员列表( int Index )
	{
		VarLevel = 1;
		for( int i = 1; i < ParseNet.NodeSet[ Index].Length; ++i ) {
			
			int index = int.Parse( ParseNet.NodeSet[ Index ][ i ] );
			
			//判断是否元件定义
			if( ParseNet.NodeSet[ index ][ 0 ] == ParseNet.Node.元件 ) {
				元件( index );
				continue;
			}
			//判断是否函数定义
			if( ParseNet.NodeSet[ index ][ 0 ] == ParseNet.Node.函数 ) {
				函数( index );
				++FunctionIndex;
				continue;
			}
			//判断系统标志
			if( ParseNet.NodeSet[ index ][ 0 ] == ParseNet.Node.系统标志 ) {
				系统标志( index );
				continue;
			}
			//到这里一定是语句
			/*
			if( ParseNet.NodeSet[ index ][ 0 ] == ParseNet.Node.语句 ) {
				语句( index, -1 );
				continue;
			}
			*/
			AddMain = true;
			语句( index, -1 );
			AddMain = false;
		}
	}
	
	static void 元件( int Index )
	{
		Parse.Unit( ref UnitName, Index, new Parse.MemberListHandle( 成员列表 ) );
	}
	
	static void 系统标志( int Index )
	{
		int NameIndex = int.Parse( ParseNet.NodeSet[ Index ][ 4 ] );
		string Name = WordList.GetWord( NameIndex );
		
		if( Name == PySysFlag.c_AddLine ) {
			PySysFlag.AddLine = true;
		}
		else if( Name == PySysFlag.c_Noline ) {
			PySysFlag.AddLine = false;
		}
		else {
			//ET.WriteParseError( NameIndex, "未知的系统标志:" + Name );
		}
	}
	
	static void 函数( int Index )
	{
		if( Param.function_used ) {
			Param.function_used = false;
			FunctionList.Get( FunctionIndex ).isUsed = true;
		}
		
		//重置局部变量空间分配地址
		n_PyVarList.VarList.LocalAddr = 0;
		
		ExpLevel = 1;
		
		//获取函数形参列表并检查是否和全局变量重名
		int index = int.Parse( ParseNet.NodeSet[ Index ][ 4 ] );
		if( index != -1 ) {
			
			for( int i = 1; i < ParseNet.NodeSet[ index ].Length; i += 2 ) {
				int j = int.Parse( ParseNet.NodeSet[ index ][ i ] );
				
				//获取变量名
				string Name = WordList.GetWord( j );
				
				if( i == ParseNet.NodeSet[ index ].Length - 1 ) {
					//
				}
				else {
					//
				}
				
				//添加变量
				VarNode v = new VarNode( Name, Deploy.FunctionIndex, Index, false );
				int vi = VarList.Add( v );
				
				/*
				//判断变量名是否有效
				if( VarList.GetDirectStaticIndex( UnitName + "." + Name ) != -1 ) {
					ET.WriteParseError( VarNameIndex, "函数形参名称无效, 已经定义了同名的变量: " + Name );
				}
				*/
			}
		}
		
		AddCode( FunctionList.Get( FunctionIndex ).FunctionName.Remove( 0, 2 ) + ":\n", Deploy.VarLevel );
		//AddCode( "\n", Deploy.VarLevel );
		
		
		if( FunctionList.Get( FunctionIndex ).Innercall ) {
			isInnerCall = true;
		}
		string BaseName = FunctionList.Get( FunctionIndex ).GetBaseName();
		
		if( BaseName == PyInnerFunc.__init__ ) {
			
			AddCode( PyByteCode.LOAD_CONST + " #addr+PYVM.New_Class\n", Deploy.VarLevel );
			AddCode( PyByteCode.CALL_FUNCTION_C + " 0\n", Deploy.VarLevel );
			AddCode( PyByteCode.POP_TOP + "\n", Deploy.VarLevel );
		}
		
		int SenceIndex = int.Parse( ParseNet.NodeSet[ Index ][ 8 ] );
		
		语句列表( SenceIndex, -1 );
		
		if( !FunctionList.Get( FunctionIndex ).Innercall ) {
			
			if( BaseName == PyInnerFunc.__init__ ) {
				AddCode( PyByteCode.LOAD_FAST + " 0\n", Deploy.VarLevel );
			}
			else {
				AddCode( PyByteCode.LOAD_NONE + "\n", Deploy.VarLevel );
			}
		}
		AddCode( PyByteCode.RETURN_VALUE + "\n", Deploy.VarLevel );
		
		isInnerCall = false;
		
		FunctionList.Get( FunctionIndex ).LocalVarNumber = n_PyVarList.VarList.LocalAddr;
	}
	
	public static void 语句列表( int Index, int ControlIndex )
	{
		for( int i = 1; i < ParseNet.NodeSet[ Index ].Length; ++i ) {
			语句( int.Parse( ParseNet.NodeSet[ Index ][ i ] ), ControlIndex );
		}
	}
	
	public static void 复合语句( int Index, int ControlIndex )
	{
		int ListIndex = int.Parse( ParseNet.NodeSet[ Index ][ 2 ] );
		if( ListIndex != -1 ) {
			语句列表( ListIndex, ControlIndex );
		}
	}
	
	public static void 语句( int Index, int ControlIndex )
	{
		string Head = ParseNet.NodeSet[ Index ][ 0 ];
		switch( Head ) {
			case ParseNet.Node.标号语句:			标号语句( Index ); return;
			case ParseNet.Node.表达式语句:		表达式语句( Index ); return;
			case ParseNet.Node.复合语句:			复合语句( Index, ControlIndex ); return;
			case ParseNet.Node.流程语句:			流程语句( Index, ControlIndex ); return;
			//case ParseNet.Node.空语句:				return;
			default : 							控制语句( Index, ControlIndex ); return;
		}
	}
	
	public static void 流程语句( int Index, int ControlIndex )
	{
		int ExpIndex = int.Parse( ParseNet.NodeSet[ Index ][ 1 ] );
		string Head = ParseNet.NodeSet[ ExpIndex ][ 0 ];
		switch( Head ) {
			case ParseNet.Node.跳至控制:			goto语句( ExpIndex ); return;
			case ParseNet.Node.跳至开始控制:		start语句( ExpIndex, ControlIndex ); return;
			case ParseNet.Node.跳至结尾控制:		continue语句( ExpIndex, ControlIndex ); return;
			case ParseNet.Node.中断控制:			break语句( ExpIndex, ControlIndex ); return;
			case ParseNet.Node.空语句控制:		空语句控制( ExpIndex, ControlIndex ); return;
			case ParseNet.Node.返回控制:			return语句( ExpIndex ); return;
			case ParseNet.Node.异常控制:			raise语句( ExpIndex ); return;
			default : 							ET.WriteParseError( ExpIndex, "未知的语句: " + Head ); return;
		}
	}
	
	static void continue语句( int Index, int ControlIndex )
	{
		if( ControlIndex == -1 ) {
			ET.WriteParseError( int.Parse( ParseNet.NodeSet[ Index ][ 1 ] ), "continue语句不能独立出现" );
		}
		跳转( "while_end_" + ControlIndex );
		//AddCode( "break;\n", Deploy.VarLevel );
	}
	
	static void start语句( int Index, int ControlIndex )
	{
		if( ControlIndex == -1 ) {
			ET.WriteParseError( int.Parse( ParseNet.NodeSet[ Index ][ 1 ] ), "start语句不能独立出现" );
		}
		跳转( "head_" + ControlIndex );
	}
	
	static void break语句( int Index, int ControlIndex )
	{
		if( ControlIndex == -1 ) {
			ET.WriteParseError( int.Parse( ParseNet.NodeSet[ Index ][ 1 ] ), "break语句不能独立出现" );
		}
		跳转( "while_break_" + ControlIndex );
		//AddCode( "break;\n", Deploy.VarLevel );
	}
	
	static void 空语句控制( int Index, int ControlIndex )
	{
		//AddCode( "\n", Deploy.VarLevel );
	}
	
	static void goto语句( int Index )
	{
		跳转( "label_" + ParseNet.NodeSet[ Index ][ 2 ] );
	}
	
	static void return语句( int Index )
	{
		SetCLine( ParseNet.NodeSet[ Index ][ 1 ] );
		AddNewIns();
		
		string r = Expression.表达式( int.Parse( ParseNet.NodeSet[ Index ][ 2 ] ) );
		r += PyByteCode.RETURN_VALUE + "\n";
		AddCode( r, Deploy.VarLevel );
	}
	
	static void raise语句( int Index )
	{
		SetCLine( ParseNet.NodeSet[ Index ][ 1 ] );
		AddNewIns();
		
		string r = Expression.表达式( int.Parse( ParseNet.NodeSet[ Index ][ 2 ] ) );
		r += PyByteCode.RETURN_VALUE + "\n";
		
		//这里临时忽略表达式计算结果
		r = PyByteCode.RAISE + "\n";
		
		AddCode( r, Deploy.VarLevel );
	}
	
	static void 标号语句( int Index )
	{
		/*
		string Name = WordList.GetWord( int.Parse( ParseNet.NodeSet[ Index ][ 1 ] ) );
		
		//这里修改了标签的定义,同一个函数中不允许出现同名的标签
		//string LabelName = "label_" + ParseNet.NodeSet[ Index ][ 1 ];
		string LabelName = "f_goto_" + FunctionIndex + "_" + Name;
		
		if( LabelList.isExist( Name ) ) {
			ET.WriteParseError( int.Parse( ParseNet.NodeSet[ Index ][ 1 ] ), "标号已经定义: " + Name );
			return;
		}
		LabelNode v = new LabelNode( Name, LabelName, VarLevel, FunctionIndex );
		LabelList.Add( v );
		AddCode( Name + ":\n", Deploy.VarLevel );
		*/
	}
	
	static void 表达式语句( int Index )
	{
		int ExpIndex = int.Parse( ParseNet.NodeSet[ Index ][ 1 ] );
		ExpLevel = 0;
		SetValue = false;
		string r = Expression.表达式( ExpIndex );
		
		AddNewIns();
		AddCode( r, Deploy.VarLevel );
		
		//2019.6.17 不是设置值指令时, 并且当前函数不是内置函数调用类型, 则丢弃计算结果
		if( !SetValue && !isInnerCall ) {
			AddCode( PyByteCode.POP_TOP + "\n", Deploy.VarLevel );
		}
	}
	
	static void 控制语句( int Index, int ControlIndex )
	{
		ControlSence.控制语句( Index, ControlIndex );
	}
	
	public static void 跳转( string Label )
	{
		Deploy.AddCode( PyByteCode.JUMP_ABSOLUTE + " " + Label + "\n", Deploy.VarLevel );
	}
	
	//设置当前所在行
	public static void SetCLine( string word )
	{
		int OperIndex = int.Parse( word );
		CurrentLine = WordList.GetLine( OperIndex );
	}
	
	//添加一个新指令标记语句
	public static void AddNewIns()
	{
		if( PySysFlag.AddLine ) {
			AddCode( PyByteCode.NEW_INS + " " + CurrentLine + "\n", Deploy.VarLevel );
		}
	}
	
	public static void AddCode( string code, int varleval )
	{
		string r = "";
		for( int i = 0; i < varleval - 1; ++i ) {
			r += "\t";
		}
		r += code;
		
		if( AddMain ) {
			MainResult += r;
		}
		else {
			Result += r;
		}
	}
	
	static bool isInnerCall = false;
	
	public static string Result;
	public static string MainResult;
	public static bool AddMain;
	
	public static bool SetValue;
	
	public static int CurrentLine;
	
	public static int VarLevel;		//变量层数
	public static int SenceNumber;	//程序中已经定义的控制语句数目
	public static int FunctionIndex;//当前函数的索引
	public static string UnitName;	//当前元件名
	public static int ChipIndex;	//当前的芯片索引
	public static int ExpLevel;		//表达式层,每次从表达式语句开始调用时初始化为0
}
}



