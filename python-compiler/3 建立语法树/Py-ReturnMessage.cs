﻿
//返回信息类
namespace n_PyParseNet_inside
{
using System;

public class ReturnMessage
{
	//构造函数
	public ReturnMessage( bool isRight )
	{
		this.isRight = isRight;
		this.result = null;
	}
	
	public bool isRight;
	public string result;
}
}
