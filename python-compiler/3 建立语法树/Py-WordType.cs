﻿
//功能描述:
//判断一个字符串的类型
namespace n_PyParseNet_inside
{
using System;
using n_PyCharType;

public static class WordType
{
	//判断一个词是否为词语 0-9 a-z A-Z _ ' "
	public static bool isName( string Name )
	{
		if( Name.Length == 0 || !isNameChar( Name[ 0 ] ) ) {
			return false;
		}
		return true;
	}
	
	//判断一个词是否为用户词语 0-9 a-z A-Z _ ' "
	public static bool isUserName( string Name )
	{
		if( Name.Length == 0 || !isNameChar( Name[ 0 ] ) || isKeyWord( Name ) ) {
			return false;
		}
		return true;
	}
	
	//判断一个词是否为标识符 0-9 a-z A-Z _
	public static bool isIdentifier( string Name )
	{
		if( Name.Length == 0 || !CharType.isLetter( Name[ 0 ] ) ) {
			return false;
		}
		for( int i = 1; i < Name.Length; ++i ) {
			if( !CharType.isLetterOrNumber( Name[ i ] ) ) {
				return false;
			}
		}
		if( isKeyWord( Name ) ) {
			return false;
		}
		return true;
	}
	
	static bool isKeyWord( string Name )
	{
		return KeyWordList.IndexOf( " " + Name + " " ) != -1;
	}
	
	//是否为名称字符 a-z A-Z _ 中文 0-9 ' "
	static bool isNameChar( char c )
	{
		if( CharType.isLetterOrNumber( c ) || c == '\'' || c == '"' )
		{
			return true;
		}
		return false;
	}

	//注意: default需要列为关键字, 否则switch语句编译不通过, 另外下列字符串的起点和终点都需要加上空格
	const string KeyWordList = " default void float bool bit uint int uint8 uint16 uint32 int8 int16 int32 ";
}
}



