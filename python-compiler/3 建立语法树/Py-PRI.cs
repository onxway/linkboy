﻿
//运算类包含所有运算的信息,如优先级,结合性等
//数据格式:
//运算符号名  优先级
namespace n_PyParseNet_inside
{
using System;
using System.IO;
using n_OS;

public static class PRI
{
	//编译器启动初始化,加载运算信息文件
	public static void Init()
	{
		//加载运算信息表
		string s = i_Compiler.Compiler.OpenCompileFile( n_PyConfig.Config.Path_pycompiler + "pri.lst" );
		s = s.Remove( s.IndexOf( "\n<end>" ) );
		string[] Lines = s.Split( '\n' );
		OperationList = new PRI_Node[ Lines.Length ];
		for( int i = 0; i < Lines.Length; ++i ) {
			Lines[ i ] = Lines[ i ].Remove( Lines[ i ].IndexOf( "<<<<" ) );
			string[] sp = Lines[ i ].Split( ' ' );
			OperationList[ i ] = new PRI_Node( sp[ 0 ], int.Parse( sp[ 1 ] ) );
		}
	}
	
	//查找一个运算的优先级,没有的运算返回 -1
	public static int GetPRI( string Oper )
	{
		for( int i = 0; i < OperationList.Length; ++i ) {
			if( OperationList[ i ].Oper == Oper ) {
				return OperationList[ i ].Level;
			}
		}
		return -1;
	}
	
	static PRI_Node[] OperationList;	//优先级信息表
}
public class PRI_Node
{
	//构造函数
	public PRI_Node( string v_Oper, int v_Level )
	{
		Oper = v_Oper;
		Level = v_Level;
	}
	
	public string Oper;
	public int Level;
}
}
