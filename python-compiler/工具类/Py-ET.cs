﻿
//编译错误输出类
namespace n_PyET
{
using System;
using n_PyWordList;
using i_Compiler;

public static class ET
{
	//清空错误列表
	public static void Clear()
	{
		ErrorMessage = "";
		isError = false;
	}
	
	//是否出错
	public static bool isErrors()
	{
		return isError;
	}
	
	//显示错误列表
	public static string Show()
	{
		return ErrorMessage.Trim( '\n' );
	}
	
	//向错误输出流写入一个语法错误项描述
	//格式: 词序号, 错误描述
	public static void ShowError( string Describe )
	{
		Compiler.Show( Describe );
	}
	
	//向错误输出流写入一个语法错误项描述
	//格式: 词序号, 错误描述
	public static void WriteParseError( int Index, string Describe )
	{
		isError = true;
		if( Index == -1 ) {
			Index = 0;
		}
		try {
		int Line = WordList.GetLine( Index ) + 1;
		int Number = WordList.GetColumn( Index );
		ErrorMessage += "词 " + WordList.GetFileIndex( Index ) + " " + Line + " " + Number + " " + 
			            WordList.GetWord( Index ).Length + " " +
			",第" + Line + "行有错: " + Describe + "\n";
		}
		catch( Exception e ) {
			ET.ShowError( "输出错误时发生异常, 当前的错误描述:\n" + Describe + "\n出错点: " + Index + "\n" + e  );
		}
	}
	
	//向错误输出流写入一个语法树错误项描述
	//格式: 行号, 错误描述
	public static void WriteLineError( int FileIndex, int Line, string Describe )
	{
		isError = true;
		++Line;
		ErrorMessage += "行 " + FileIndex + " " + Line + "," + FileIndex + "-" + "第" + Line + "行有错: " + Describe + "\n";
	}
	
	static string ErrorMessage;		//编译错误信息
	static bool isError; 			//是否出错标志
}
}
