﻿
//着色词列表类
namespace n_PyKeyWord
{
using System;
using i_Compiler;
using n_OS;

public static class KeyWord
{
	//初始化,加载着色词列表文件
	public static void Init()
	{
		//加载着色词列表
		string vLanguage = "english_UL";
		string s = Compiler.OpenCompileFile( n_PyConfig.Config.Path_pycompiler + "c_" + vLanguage + ".txt" );
		s = s.Remove( s.IndexOf( "\n<end>" ) );
		string[] Lines = s.Split( '\n' );
		
		ColorWordListLength = 0;
		ColorWordList = new string[ Lines.Length ];
		ColorList = new int[ Lines.Length ];
		
		ColorTypeListLength = 0;
		ColorTypeList = new string[ Lines.Length ];
		
		for( int i = 0; i < Lines.Length; ++i ) {
			string line = Lines[ i ].Remove( Lines[ i ].IndexOf( "<<<<" ) );
			if( line.StartsWith( "color " ) ) {
				ColorTypeList[ ColorTypeListLength ] = line.Remove( 0, 6 );
				++ColorTypeListLength;
			}
			else {
				ColorList[ ColorWordListLength ] = ColorTypeListLength - 1;
				ColorWordList[ ColorWordListLength ] = line;
				++ColorWordListLength;
			}
		}
	}
	
	//判断是否为着色词
	public static bool isKeyWord( string name, ref int Index )
	{
		for( int i = 0; i < ColorWordListLength; ++i ) {
			if( name == ColorWordList[ i ] ) {
				Index = ColorList[ i ];
				return true;
			}
		}
		return false;
	}
	
	static int ColorWordListLength;
	static string[] ColorWordList;	//着色词列表
	static int[] ColorList;			//颜色列表
	
	static int ColorTypeListLength;
	static string[] ColorTypeList;	//着色词类型列表
}
}

