﻿
//编译错误输出类
namespace n_PyCDebug
{
using System;
using n_PyWordList;
using n_PyET;

public static class Debug
{
	//清空错误列表
	public static void Clear()
	{
		ShowMessage = false;
	}
	
	//处理一个系统命令
	public static void Deal( string Command )
	{
		if( Command == "message_on" ) {
			ShowMessage = true;
		}
		else if( Command == "message_off" ) {
			ShowMessage = false;
		}
		else {
			ET.ShowError( "<Debug> 未知的命令: " + Command );
		}
	}
	
	public static bool ShowMessage;
}
}
