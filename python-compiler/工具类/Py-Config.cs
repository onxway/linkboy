﻿
//编译配置

namespace n_PyConfig
{
using System;
using n_PyCPUType;
using n_PyET;
using n_PyFunctionList;
using n_PyVarType;
using i_Compiler;
using n_OS;

public static class Config
{
	public static readonly string LOAD = "import";
	public static readonly string PathPre_ModuleLib = "ModuleLib";
	public static readonly string PathPre_CurrentDir = "CurrentDir";
	
	public static readonly string Path_pycompiler = @"Resource" + n_OS.OS.PATH_S + "cpl" + n_OS.OS.PATH_S + "p-c" + n_OS.OS.PATH_S;
	
	public static string linkboy_ui = "ui.";
	public static string linkboy_py = "py.";
	
	//配置类初始化
	public static void Init()
	{
		
	}
	
	//复位编译配置
	public static void Reset()
	{
		
	}
}
}


