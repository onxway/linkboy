﻿
namespace n_PyByteCode
{
using System;

public static class PyByteCode
{
	public const string NEW_INS = 				"NEW_INS";
	public const string LOAD_NONE = 			"LOAD_NONE";
	public const string LOAD_CONST = 			"LOAD_CONST";
	public const string LOAD_CONST32 = 			"LOAD_CONST32";
	public const string LOAD_BOOL = 			"LOAD_BOOL";
	public const string LOAD_STRING = 			"LOAD_STRING";
	public const string LOAD_FAST = 			"LOAD_FAST";
	public const string LOAD_FAST_CMP = 		"LOAD_FAST_CMP";
	public const string STORE_FAST = 			"STORE_FAST";
	public const string POP_JUMP_IF_FALSE = 	"POP_JUMP_IF_FALSE";
	public const string JUMP_IF_ERROR = 		"JUMP_IF_ERROR";
	public const string JUMP_FORWARD = 			"JUMP_FORWARD";
	//public const string COMPARE_OP = 			"COMPARE_OP";
	//public const string UNARY_NEGATIVE = 		"UNARY_NEGATIVE";
	public const string SETUP_LOOP = 			"SETUP_LOOP";
	public const string JUMP_ABSOLUTE = 		"JUMP_ABSOLUTE";
	public const string POP_BLOCK = 			"POP_BLOCK";
	public const string CALL_FUNCTION = 		"CALL_FUNCTION";
	public const string GET_CLASS_FUNCTION = 	"GET_CLASS_FUNCTION";
	public const string GET_CLASS_FUNCTION_SAVE = 	"GET_CLASS_FUNCTION_SAVE";
	public const string CALL_FUNCTION_C = 		"CALL_FUNCTION_C";
	public const string CALL_FUNCTION_PY = 		"CALL_FUNCTION_PY";
	public const string RETURN_VALUE =			"RETURN_VALUE";
	public const string RAISE =					"RAISE";
	public const string POP_TOP = 				"POP_TOP";
	public const string LOAD_GLOBAL = 			"LOAD_GLOBAL";
	public const string LOAD_GLOBAL_CMP = 		"LOAD_GLOBAL_CMP";
	public const string LOAD_GLOBAL_C = 		"LOAD_GLOBAL_C";
	public const string STORE_GLOBAL = 			"STORE_GLOBAL";
	public const string STORE_GLOBAL_C = 		"STORE_GLOBAL_C";
	public const string BUILD_LIST = 			"BUILD_LIST";
	
	public const string STORE_SUBSCR = 			"STORE_SUBSCR";
	public const string BINARY_SUBSCR = 		"BINARY_SUBSCR";
	
	public const string STORE_DOT = 			"STORE_DOT";
	public const string LOAD_DOT = 				"LOAD_DOT";
	
	//public const string BINARY_ADD = 			"BINARY_ADD";
	//public const string BINARY_SUBTRACT = 		"BINARY_SUBTRACT";
	//public const string BINARY_MULTIPLY = 		"BINARY_MULTIPLY";
	//public const string BINARY_TRUE_DIVIDE = 	"BINARY_TRUE_DIVIDE";
	//public const string BINARY_FLOOR_DIVIDE = 	"BINARY_FLOOR_DIVIDE";
	//public const string BINARY_MODULO = 		"BINARY_MODULO";
}
public static class PyInnerVar
{
	public const int InnerVarNumber = 1;
	
	public const string __realn__ = 			"__realn__";
}
public static class PyInnerFunc
{
	public const int InnerFuncNumber = 27;
	
	public const string __init__ = 			"__init__";
	
	public const string __neg__ = 			"__neg__";
	
	public const string __add__ = 			"__add__";
	public const string __sub__ = 			"__sub__";
	public const string __power__ = 		"__power__";
	public const string __mul__ = 			"__mul__";
	public const string __div__ = 			"__div__";
	public const string __d_div__ = 		"__d_div__";
	public const string __mod__ = 			"__mod__";
	
	public const string __inp_add__ = 		"__inp_add__";
	public const string __inp_sub__ = 		"__inp_sub__";
	public const string __inp_power__ = 	"__inp_power__";
	public const string __inp_mul__ = 		"__inp_mul__";
	public const string __inp_div__ = 		"__inp_div__";
	public const string __inp_d_div__ = 	"__inp_d_div__";
	public const string __inp_mod__ = 		"__inp_mod__";
	
	public const string __cmp_le__ = 		"__cmp_le__";
	public const string __cmp_se__ = 		"__cmp_se__";
	public const string __cmp_l__ = 		"__cmp_l__";
	public const string __cmp_s__ = 		"__cmp_s__";
	public const string __cmp_e__ = 		"__cmp_e__";
	public const string __cmp_ne__ = 		"__cmp_ne__";
	
	public const string __iter__ = 		"__iter__";
	public const string __next__ = 		"__next__";
	public const string __getitem__ = 	"__getitem__";
	public const string __setitem__ = 	"__setitem__";
	
	public const string append = 	"append";
	
	//获取指定函数的索引 目前版本索引和函数的定义位置是无关的， 可以放心修改
	public static int GetFuncIndex( string name )
	{
		int FuncID = -1;
		switch( name ) {
			case __init__:		FuncID = 0; break;
			
			case __neg__:		FuncID = 1; break;
			case __add__:		FuncID = 2; break;
			case __sub__:		FuncID = 3; break;
			case __power__:		FuncID = 4; break;
			case __mul__:		FuncID = 5; break;
			case __div__:		FuncID = 6; break;
			case __d_div__:		FuncID = 7; break;
			case __mod__:		FuncID = 8; break;
			
			case __inp_add__:		FuncID = 9; break;
			case __inp_sub__:		FuncID = 10; break;
			case __inp_power__:		FuncID = 11; break;
			case __inp_mul__:		FuncID = 12; break;
			case __inp_div__:		FuncID = 13; break;
			case __inp_d_div__:		FuncID = 14; break;
			case __inp_mod__:		FuncID = 15; break;
			
			case __cmp_le__:		FuncID = 16; break;
			case __cmp_se__:		FuncID = 17; break;
			case __cmp_l__:		FuncID = 18; break;
			case __cmp_s__:		FuncID = 19; break;
			case __cmp_e__:		FuncID = 20; break;
			case __cmp_ne__:	FuncID = 21; break;
			
			case append:		FuncID = 22; break;
			
			case __iter__:	FuncID = 23; break;
			case __next__:	FuncID = 24; break;
			case __getitem__:	FuncID = 25; break;
			case __setitem__:	FuncID = 26; break;
			
			default:			n_OS.VIO.Show( name ); FuncID = -1; break;
		}
		return FuncID;
	}
}
}




