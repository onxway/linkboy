﻿
//元件定义信息分析类
namespace n_PyUnit
{
using System;
using n_PyET;
using n_PyMemberType;
using n_PyParse;
using n_PyParseNet;
using n_PyUnitList;
using n_PyUnitNode;
using n_PyWordList;

public static class Unit
{
	static string UnitName;
	
	//获取元件信息,生成: 元件列表 函数列表 变量列表
	public static void Record()
	{
		//元件列表初始化
		UnitList.Clear();
		
		Parse.DealWith( ref UnitName, new Parse.MemberListHandle( 成员列表 ) );
	}
	
	static void 成员列表( int Index )
	{
		for( int i = 1; i < ParseNet.NodeSet[ Index ].Length; ++i ) {
			int index = int.Parse( ParseNet.NodeSet[ Index ][ i ] );
			
			//判断是否元件定义
			if( ParseNet.NodeSet[ index ][ 0 ] == ParseNet.Node.元件 ) {
				
				记录元件信息( index );
				元件( index );
				continue;
			}
		}
	}
	
	static void 元件( int Index )
	{
		Parse.Unit( ref UnitName, Index, new Parse.MemberListHandle( 成员列表 ) );
	}
	
	static void 记录元件信息( int Index )
	{
		//获取成员类型
		string VisitType = null;
		string RealRefType = null;
		string ThisUnitName = null;
		int NameIndex = 0;
		int WordNameIndex = int.Parse( ParseNet.NodeSet[ Index ][ 3 ] );
		
		//处理重命名项
		int RenameIndex = int.Parse( ParseNet.NodeSet[ Index ][ 1 ] );
		if( RenameIndex == -1 ) {
			int MemberTypeIndex = int.Parse( ParseNet.NodeSet[ Index ][ 2 ] );
			Parse.GetMemberType( MemberTypeIndex, ref VisitType, ref RealRefType );
			NameIndex = int.Parse( ParseNet.NodeSet[ Index ][ 4 ] );
			ThisUnitName = UnitName + "." + WordList.GetWord( NameIndex );
		}
		else {
			NameIndex = Parse.GetRename( RenameIndex, UnitName, ref VisitType, ref RealRefType, ref ThisUnitName );
		}
		if( UnitList.isExist( ThisUnitName ) ) {
			ET.WriteParseError( NameIndex, "已经定义了同名元件: " + ThisUnitName );
		}
		//判断成员列表是否为空
		int MemberIndex = int.Parse( ParseNet.NodeSet[ Index ][ 10 ] );
		if( ParseNet.NodeSet[ MemberIndex ].Length >= 2 && RealRefType == MemberType.RealRefType.link ) {
			ET.WriteParseError( NameIndex, "元件类型为链接类型时不可以包含成员列表" );
		}
		UnitNode node = new UnitNode( VisitType, RealRefType, ThisUnitName, WordNameIndex );
		int idx = UnitList.Add( node );
		
		//处理链接类型
		int LinkIndex = int.Parse( ParseNet.NodeSet[ Index ][ 12 ] );
		if( LinkIndex != -1 ) {
			//提取第二个成员信息
			int SecondIndex = int.Parse( ParseNet.NodeSet[ LinkIndex ][ 2 ] );
			int ErrorIndex2 = 0;
			string SecondName = null;
			Parse.GetMemberNames( UnitName, SecondIndex, ref SecondName, ref ErrorIndex2 );
			if( RealRefType != MemberType.RealRefType.link ) {
				ET.WriteParseError( int.Parse( ParseNet.NodeSet[ Index ][ 4 ] ), "连接器的左端必须为引用类型成员:" + UnitName );
			}
			node.TargetMemberName = SecondName;
		}
	}
}
}


