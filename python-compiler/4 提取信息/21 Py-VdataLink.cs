﻿
//元件引用分析类
namespace n_PyVdataLink
{
using System;
using n_PyET;
using n_PyMemberType;
using n_PyParse;
using n_PyParseNet;
using n_PyVdataList;

public static class VdataLink
{
	static string UnitName;
	
	//获取元件信息,生成: 元件列表 函数列表 变量列表
	public static void Record()
	{
		Parse.DealWith( ref UnitName, new Parse.MemberListHandle( 成员列表 ) );
	}
	
	static void 成员列表( int Index )
	{
		for( int i = 1; i < ParseNet.NodeSet[ Index ].Length; ++i ) {
			int index = int.Parse( ParseNet.NodeSet[ Index ][ i ] );
			
			//判断是否元件定义
			if( ParseNet.NodeSet[ index ][ 0 ] == ParseNet.Node.元件 ) {
				元件( index );
				continue;
			}
			//判断是否连接器定义
			if( ParseNet.NodeSet[ index ][ 0 ] == ParseNet.Node.连接器 ) {
				连接器( index );
				continue;
			}
		}
	}
	
	static void 元件( int Index )
	{
		Parse.Unit( ref UnitName, Index, new Parse.MemberListHandle( 成员列表 ) );
	}
	
	static void 连接器( int Index )
	{
		//提取第一个成员信息
		int FirstIndex = int.Parse( ParseNet.NodeSet[ Index ][ 2 ] );
		int ErrorIndex1 = 0;
		string FirstName = null;
		Parse.GetMemberNames( UnitName, FirstIndex, ref FirstName, ref ErrorIndex1 );
		
		//提取第二个成员信息
		int SecondIndex = int.Parse( ParseNet.NodeSet[ Index ][ 4 ] );
		int ErrorIndex2 = 0;
		string SecondName = null;
		Parse.GetMemberNames( UnitName, SecondIndex, ref SecondName, ref ErrorIndex2 );
		
		//判断成员是否为虚拟数据类型
		int FirstVdataIndex = VdataList.GetDirectIndex( FirstName );
		if( FirstVdataIndex != -1 ) {
			if( VdataList.Get( FirstVdataIndex ).RealRefType != MemberType.RealRefType.link ) {
				ET.WriteParseError( ErrorIndex1, "连接器的左端必须为引用类型元件:" + FirstName );
				return;
			}
			if( VdataList.GetDirectIndex( SecondName ) == -1 ) {
				ET.WriteParseError( ErrorIndex2, "未定义的虚拟类型: " + SecondName );
			}
			VdataList.Get( FirstVdataIndex ).TargetMemberName = SecondName;
			return;
		}
	}
}
}
