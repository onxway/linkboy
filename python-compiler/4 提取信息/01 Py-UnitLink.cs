﻿
//元件引用分析类
namespace n_PyUnitLink
{
using System;
using n_PyET;
using n_PyMemberType;
using n_PyParse;
using n_PyParseNet;
using n_PyUnitList;

public static class UnitLink
{
	static string UnitName;
	
	//获取元件信息,生成: 元件列表 函数列表 变量列表
	public static void Record()
	{
		Parse.DealWith( ref UnitName, new Parse.MemberListHandle( 成员列表 ) );
	}
	
	static void 成员列表( int Index )
	{
		for( int i = 1; i < ParseNet.NodeSet[ Index ].Length; ++i ) {
			int index = int.Parse( ParseNet.NodeSet[ Index ][ i ] );
			
			//判断是否元件定义
			if( ParseNet.NodeSet[ index ][ 0 ] == ParseNet.Node.元件 ) {
				元件( index );
				continue;
			}
			//判断是否连接器定义
			if( ParseNet.NodeSet[ index ][ 0 ] == ParseNet.Node.连接器 ) {
				连接器( index );
				continue;
			}
		}
	}
	
	static void 元件( int Index )
	{
		Parse.Unit( ref UnitName, Index, new Parse.MemberListHandle( 成员列表 ) );
	}
	
	static void 连接器( int Index )
	{
		//提取当前连接器的权限
		bool Super = false;
		int SuperIndex = int.Parse( ParseNet.NodeSet[ Index ][ 1 ] );
		if( SuperIndex != -1 ) {
			Super = true;
		}
		//提取第一个成员信息
		int FirstIndex = int.Parse( ParseNet.NodeSet[ Index ][ 2 ] );
		int ErrorIndex1 = 0;
		string FirstName = null;
		Parse.GetMemberNames( UnitName, FirstIndex, ref FirstName, ref ErrorIndex1 );
		
		//提取第二个成员信息
		int SecondIndex = int.Parse( ParseNet.NodeSet[ Index ][ 4 ] );
		int ErrorIndex2 = 0;
		string SecondName = null;
		Parse.GetMemberNames( UnitName, SecondIndex, ref SecondName, ref ErrorIndex2 );
		
		//判断成员是否为元件类型
		int FirstUnitIndex = UnitList.GetDirectIndex( FirstName );
		
		//2016.12.22 新增引用置换, 当目标link所在的Lunit为link类型时, 先解析Lunit的实际元件
		if( FirstUnitIndex == -1 ) {
			string LastName = FirstName.Remove( 0, FirstName.LastIndexOf( '.' ) + 1 );
			string FName = FirstName.Remove( FirstName.LastIndexOf( '.' ) );
			int i = UnitList.GetDirectIndex( FName );
			if( i != -1 ) {
				FirstName = UnitList.Get( i ).TargetMemberName + "." + LastName;
				FirstUnitIndex = UnitList.GetDirectIndex( FirstName );
			}
		}
		if( FirstUnitIndex != -1 ) {
			if( UnitList.Get( FirstUnitIndex ).RealRefType != MemberType.RealRefType.link ) {
				if( Super ) {
					UnitList.Get( FirstUnitIndex ).RealRefType = MemberType.RealRefType.link;
				}
				else {
					ET.WriteParseError( ErrorIndex1, "连接器的左端必须为引用类型成员:" + FirstName );
					return;
				}
			}
			if( UnitList.GetDirectIndex( SecondName ) == -1 ) {
				ET.WriteParseError( ErrorIndex2, "未定义的元件: " + SecondName );
			}
			UnitList.Get( FirstUnitIndex ).TargetMemberName = SecondName;
			
			ParseNet.NodeSet[ Index ][ 3 ] = "-1";
			
			return;
		}
		//后续可能还会进行判断, 所以这里不报错, 直接退出
		//else {
		//	ET.WriteParseError( ErrorIndex1, "未定义的元素: " + FirstName );
		//}
	}
}
}
