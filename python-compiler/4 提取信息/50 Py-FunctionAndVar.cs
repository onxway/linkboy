﻿
//函数和静态变量信息分析类
namespace n_PyFunctionAndVar
{
using System;
using n_PyConfig;
using n_PyConstString;
using n_PyET;
using n_PyFunctionList;
using n_PyFunctionNode;
using n_PyMemberType;
using n_PyParse;
using n_PyParseNet;
using n_PyUnitList;
using n_PyVarList;
using n_PyVarNode;
using n_PyVarType;
using n_PyVdataList;
using n_PyWordList;


public static class FunctionAndVar
{
	static string UnitName;
	static int FunctionIndex;
	
	
	//static TreeNode PublicFunctionNode;
	//static TreeNode PrivateFunctionNode;
	//static TreeNode PublicVarNode;
	//static TreeNode PrivateVarNode;
	//static TreeNode PublicConstNode;
	//static TreeNode PrivateConstNode;
	
	//public delegate void D_UnitTreeAdd( TreeNode n );
	
	//获取元件成员信息,生成: 函数列表 变量列表 常量列表
	public static void Record()
	{
		//函数列表初始化
		FunctionList.Clear();
		
		FunctionIndex = 0;
		
		
		//添加系统内置变量
		VarNode vn = new VarNode( "__realn__", -1, -1, true );
		int ni = VarList.Add( vn );
		
		
		Parse.DealWith( ref UnitName, new Parse.MemberListHandle( 成员列表 ) );
	}
	
	static void 成员列表( int Index )
	{
		for( int i = 1; i < ParseNet.NodeSet[ Index ].Length; ++i ) {
			int index = int.Parse( ParseNet.NodeSet[ Index ][ i ] );
			
			//判断是否元件定义
			if( ParseNet.NodeSet[ index ][ 0 ] ==ParseNet.Node.元件 ) {
				元件( index );
				continue;
			}
			//判断函数定义
			if( ParseNet.NodeSet[ index ][ 0 ] == ParseNet.Node.函数 ) {
				函数( index );
				++FunctionIndex;
				continue;
			}
			//判断系统标志
			if( ParseNet.NodeSet[ index ][ 0 ] == ParseNet.Node.系统标志 ) {
				系统标志( index );
				continue;
			}
		}
	}
	
	static void 元件( int Index )
	{
		int UnitNameIndex = int.Parse( ParseNet.NodeSet[ Index ][ 3 ] );
		
		//TreeNode fnode = new TreeNode( "【" + UnitName + "】" );
		//if( Compiler.uTreeView != null ) {
		//	Compiler.uTreeView.Nodes.Add( fnode );
		//}
		//fnode.Name = WordList.GetLine( UnitNameIndex ) + ",unit";
		
		
//		PublicFunctionNode = new TreeNode( "公开函数" );
//		PublicFunctionNode.Name = "!,PublicFunctionSet";
//		PrivateFunctionNode = new TreeNode( "内部函数" );
//		PrivateFunctionNode.Name = "!,PrivateFunctionSet";
//		PublicVarNode = new TreeNode( "公开变量" );
//		PublicVarNode.Name = "!,PublicVarSet";
//		PrivateVarNode = new TreeNode( "内部变量" );
//		PrivateVarNode.Name = "!,PrivateVarSet";
//		PublicConstNode = new TreeNode( "公开常量" );
//		PublicConstNode.Name = "!,PublicConstSet";
//		PrivateConstNode = new TreeNode( "内部常量" );
//		PrivateConstNode.Name = "!,PrivateConstSet";
		
		Parse.Unit( ref UnitName, Index, new Parse.MemberListHandle( 成员列表 ) );
		
//		if( PublicFunctionNode.Nodes.Count != 0 ) {
//			fnode.Nodes.Add( PublicFunctionNode );
//		}
//		if( PrivateFunctionNode.Nodes.Count != 0 ) {
//			fnode.Nodes.Add( PrivateFunctionNode );
//		}
//		if( PublicVarNode.Nodes.Count != 0 ) {
//			fnode.Nodes.Add( PublicVarNode );
//		}
//		if( PrivateVarNode.Nodes.Count != 0 ) {
//			fnode.Nodes.Add( PrivateVarNode );
//		}
//		if( PublicConstNode.Nodes.Count != 0 ) {
//			fnode.Nodes.Add( PublicConstNode );
//		}
//		if( PrivateConstNode.Nodes.Count != 0 ) {
//			fnode.Nodes.Add( PrivateConstNode );
//		}
		
	}
	
	static void 函数( int Index )
	{
		//获取成员类型
		string VisitType = null;
		string RealRefType = null;
		string RefValue = null;
		//int FuncPreIndex = int.Parse( ParseNet.NodeSet[ Index ][ 1 ] );
		//int MemberTypeIndex = int.Parse( ParseNet.NodeSet[ FuncPreIndex ][ 1 ] );
		//Parse.GetMemberType( MemberTypeIndex, ref VisitType, ref RealRefType );
		VisitType = MemberType.VisitType.Public;
		RealRefType = MemberType.RealRefType.Real;
		
		//获取函数类型 general task interface interrupt start
		string InterType = MemberType.FunctionType.General;
		
		//设置函数名为单击定位点
		int NameIndex = int.Parse( ParseNet.NodeSet[ Index ][ 2 ] );
		string baseName = WordList.GetWord( NameIndex );
		string FunctionName = UnitName + "." + baseName;
		
		//添加函数名内置变量
		VarNode vn = new VarNode( FunctionName.Remove( 0, 2 ), -1, NameIndex, true );
		vn.isFunctionName = true;
		int ni = VarList.Add( vn );
		
		//判断是否可以定义函数
		if( FunctionList.GetDirectIndex( FunctionName ) != -1 ||
		    UnitList.isExist( FunctionName )
		  ) {
			//ET.WriteParseError( NameIndex, "函数名非法:已经定义了同名的成员: " + FunctionName );
		}
		//获取函数返回类型
		string ReturnType = VarType.Void;
		
		//获取函数形参列表
		int index = int.Parse( ParseNet.NodeSet[ Index ][ 4 ] );
		
		//string VarIndexList = null;
		
		if( index != -1 ) {
			for( int i = 1; i < ParseNet.NodeSet[ index ].Length; i += 2 ) {
				int j = int.Parse( ParseNet.NodeSet[ index ][ i ] );
				
				//获取变量名
				string Name = WordList.GetWord( j );
				
				//添加变量
				//VarNode v = new VarNode( Name, FunctionIndex, j, false );
				//VarIndexList += VarList.Add( v ) + " ";
			}
			//VarIndexList = VarIndexList.TrimEnd( ' ' );
		}
		int UnitIndex = UnitList.GetDirectIndex( UnitName );
		FunctionNode node = new FunctionNode(
			VisitType, RealRefType, InterType, ReturnType, FunctionName, null, NameIndex, RefValue );
		
		if( n_PySysFlag.PySysFlag.InnerCall ) {
			n_PySysFlag.PySysFlag.InnerCall = false;
			node.Innercall = true;
		}
		
		try {
			FunctionList.Add( node );
		}
		catch {
			ET.WriteParseError( NameIndex, "已经定义了同名的函数: " + FunctionName );
		}
		
		//只有类函数才提取动态索引
		if( UnitIndex != -1 ) {
			int FmapIndex = n_PyByteCode.PyInnerFunc.GetFuncIndex( baseName );
			if( FmapIndex == -1 ) {
				ET.WriteParseError( NameIndex, "类函数未知: " + UnitName + "-" + FunctionName );
				FmapIndex = 0;
			}
			n_PyFuncMap.PyFuncMap.SetAddr( UnitIndex, FmapIndex, vn.Addr );
		}
		
		/*
		//处理链接类型
		int LinkIndex = int.Parse( ParseNet.NodeSet[ Index ][ 10 ] );
		if( LinkIndex != -1 ) {
			//提取第二个成员信息
			int SecondIndex = int.Parse( ParseNet.NodeSet[ LinkIndex ][ 2 ] );
			int ErrorIndex2 = 0;
			string SecondName = null;
			Parse.GetMemberNames( UnitName, SecondIndex, ref SecondName, ref ErrorIndex2 );
			if( RealRefType != MemberType.RealRefType.link ) {
				ET.WriteParseError( int.Parse( ParseNet.NodeSet[ Index ][ 4 ] ), "连接器的左端必须为引用类型成员:" + UnitName );
			}
			node.TargetMemberName = SecondName;
		}
		*/
	}
	
	static void 系统标志( int Index )
	{
		int NameIndex = int.Parse( ParseNet.NodeSet[ Index ][ 4 ] );
		string Name = WordList.GetWord( NameIndex );
		
		if( Name == n_PySysFlag.PySysFlag.c_innercall ) {
			n_PySysFlag.PySysFlag.InnerCall = true;
		}
	}
}
}


