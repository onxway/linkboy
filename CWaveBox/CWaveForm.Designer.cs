﻿/*
 * 由 SharpDevelop 创建。
 * 用户: bbb_2
 * 日期: 2009-11-17
 * 时间: 11:15
 * 
 * 如果你想要更改该模板，那么请使用“工具 | 选项 | 编辑 | 编辑标准标题”。
 */
namespace n_CWaveForm
{
	partial class CWaveForm
	{
		/// <summary>
		/// Designer variable used to keep track of non-visual components.
		/// </summary>
		private System.ComponentModel.IContainer components = null;
		
		/// <summary>
		/// Disposes resources used by the form.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing) {
				if (components != null) {
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}
		
		/// <summary>
		/// This method is required for Windows Forms designer support.
		/// Do not change the method contents inside the source code editor. The Forms designer might
		/// not be able to load this method if it was changed manually.
		/// </summary>
		private void InitializeComponent()
		{
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(CWaveForm));
			this.button打开串口 = new System.Windows.Forms.Button();
			this.comboBox串口号 = new System.Windows.Forms.ComboBox();
			this.comboBox波特率 = new System.Windows.Forms.ComboBox();
			this.checkBoxDTR_RTS = new System.Windows.Forms.CheckBox();
			this.panel1 = new System.Windows.Forms.Panel();
			this.label1 = new System.Windows.Forms.Label();
			this.panel2 = new System.Windows.Forms.Panel();
			this.panel1.SuspendLayout();
			this.SuspendLayout();
			// 
			// button打开串口
			// 
			this.button打开串口.BackColor = System.Drawing.Color.Gainsboro;
			resources.ApplyResources(this.button打开串口, "button打开串口");
			this.button打开串口.ForeColor = System.Drawing.Color.Black;
			this.button打开串口.Name = "button打开串口";
			this.button打开串口.UseVisualStyleBackColor = false;
			this.button打开串口.Click += new System.EventHandler(this.Button打开串口Click);
			// 
			// comboBox串口号
			// 
			this.comboBox串口号.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
			resources.ApplyResources(this.comboBox串口号, "comboBox串口号");
			this.comboBox串口号.FormattingEnabled = true;
			this.comboBox串口号.Name = "comboBox串口号";
			this.comboBox串口号.Click += new System.EventHandler(this.ComboBox串口号Click);
			// 
			// comboBox波特率
			// 
			resources.ApplyResources(this.comboBox波特率, "comboBox波特率");
			this.comboBox波特率.FormattingEnabled = true;
			this.comboBox波特率.Items.AddRange(new object[] {
									resources.GetString("comboBox波特率.Items"),
									resources.GetString("comboBox波特率.Items1"),
									resources.GetString("comboBox波特率.Items2"),
									resources.GetString("comboBox波特率.Items3"),
									resources.GetString("comboBox波特率.Items4"),
									resources.GetString("comboBox波特率.Items5"),
									resources.GetString("comboBox波特率.Items6"),
									resources.GetString("comboBox波特率.Items7"),
									resources.GetString("comboBox波特率.Items8")});
			this.comboBox波特率.Name = "comboBox波特率";
			// 
			// checkBoxDTR_RTS
			// 
			resources.ApplyResources(this.checkBoxDTR_RTS, "checkBoxDTR_RTS");
			this.checkBoxDTR_RTS.ForeColor = System.Drawing.Color.Black;
			this.checkBoxDTR_RTS.Name = "checkBoxDTR_RTS";
			this.checkBoxDTR_RTS.UseVisualStyleBackColor = true;
			// 
			// panel1
			// 
			this.panel1.BackColor = System.Drawing.Color.WhiteSmoke;
			this.panel1.Controls.Add(this.label1);
			this.panel1.Controls.Add(this.button打开串口);
			this.panel1.Controls.Add(this.comboBox波特率);
			this.panel1.Controls.Add(this.checkBoxDTR_RTS);
			this.panel1.Controls.Add(this.comboBox串口号);
			resources.ApplyResources(this.panel1, "panel1");
			this.panel1.Name = "panel1";
			// 
			// label1
			// 
			this.label1.BackColor = System.Drawing.Color.Transparent;
			resources.ApplyResources(this.label1, "label1");
			this.label1.ForeColor = System.Drawing.Color.DarkGray;
			this.label1.Name = "label1";
			// 
			// panel2
			// 
			resources.ApplyResources(this.panel2, "panel2");
			this.panel2.Name = "panel2";
			// 
			// CWaveForm
			// 
			resources.ApplyResources(this, "$this");
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.BackColor = System.Drawing.Color.Black;
			this.Controls.Add(this.panel2);
			this.Controls.Add(this.panel1);
			this.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
			this.Name = "CWaveForm";
			this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.UARTFormFormClosing);
			this.panel1.ResumeLayout(false);
			this.ResumeLayout(false);
		}
		private System.Windows.Forms.Label label1;
		private System.Windows.Forms.Panel panel2;
		private System.Windows.Forms.Panel panel1;
		private System.Windows.Forms.CheckBox checkBoxDTR_RTS;
		private System.Windows.Forms.ComboBox comboBox波特率;
		private System.Windows.Forms.ComboBox comboBox串口号;
		private System.Windows.Forms.Button button打开串口;
	}
}
