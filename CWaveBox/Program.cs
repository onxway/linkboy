﻿
using System;
using n_CWaveForm;
using System.Windows.Forms;


namespace n_CWave
{
	class Program
	{
		public static void Main(string[] args)
		{
			Application.EnableVisualStyles();
			Application.SetCompatibleTextRenderingDefault(false);
			
			CWaveForm f = new CWaveForm();
			f.SingleMode = true;
			Application.Run( f );
		}
	}
}


