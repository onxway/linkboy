# linkboy

#### 介绍
linkboy源代码

#### 编译构建

1.  首先安装编译环境：SharpDevelop，推荐使用4.4版本，可自行网上搜索安装，或者到网盘下载,链接: https://pan.baidu.com/s/1yeAYYFKSV0ZT36Wgri9QbQ?pwd=link 提取码: link 安装过程如提示需要vcredist_x86.exe，可在网上搜索或者到网盘下载，链接: https://pan.baidu.com/s/1Uw5aMCaC2HzJEiHgo0H7jg?pwd=link 提取码: link

2.  将本页面项目fork到本地
3.  双击运行SharpDevelop，点击菜单栏 文件 -> 打开 -> 项目/解决方案，找到linkboy.sln并打开
4.  根据功能需要，修改相关文件，然后按F9生成当前工程（如修改了多个工程，可点击：生成->重新生成解决方案）
5.  linkboy目录即为最终发布文件夹，可以压缩之后复制到其他电脑进行使用。

#### 问题排查

1. 首次下载源码后，先选择 生成->重新生成解决方案

2. 如果在生成工程时，提示：由于文件 xxx.resx 处于 Internet 区域或受限制区域，或者该文件具有 Web 标记，因此无法处理该文件。如果要处理这些文件，请删除 Web 标记。则需要打开命令行工具，执行如下命令，将全部文件解锁即可：

Get-ChildItem -Path 'C:\Users\ABC\Desktop\linkboy-master' -Recurse | Unblock-File

注意：其中的路径 C:\Users\ABC\Desktop\linkboy-master 需要替换成你fork到本地电脑后的实际路径

#### 参与贡献

1.  Fork 本仓库
2.  新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request


