﻿

using System;
using System.Windows.Forms;
using System.Diagnostics;
using System.IO;

namespace n_App
{
	/// <summary>
	/// Class with program entry point.
	/// </summary>
	internal sealed class Program
	{
		/// <summary>
		/// Program entry point.
		/// </summary>
		[STAThread]
		private static void Main(string[] args)
		{
			Application.EnableVisualStyles();
			Application.SetCompatibleTextRenderingDefault(false);
			
			//Application.Run(new MainForm());
			string tfile = @"linkboy\m-eye\OpenCamera.exe";
			
			if( !File.Exists( AppDomain.CurrentDomain.BaseDirectory + tfile ) ) {
				MainForm mf = new MainForm();
				Application.Run( mf );
				return;
			}
			
			string DefaultFilePath = null;
			
			//+++++++++++++++++++++++++++++++++++++++++++++
			//判断是否加载文件
			if( ( args != null ) && ( args.Length > 0 ) ) {
				string filePath = "";
				int si = 0;
				for( int i = si; i < args.Length; ++i ) {
					filePath += " " + args[ i ];
				}
				DefaultFilePath = filePath.Trim( ' ' );
			}
			
			Process Proc = new Process();
			Proc.StartInfo.WindowStyle = ProcessWindowStyle.Normal;
			//Proc.StartInfo.UseShellExecute = false;
			//Proc.StartInfo.CreateNoWindow = true;
			//Proc.StartInfo.RedirectStandardOutput = true;
			
			Proc.StartInfo.WorkingDirectory = AppDomain.CurrentDomain.BaseDirectory;
			
			Proc.StartInfo.FileName = tfile;
			
			string StartMes = "wq";
			Proc.StartInfo.Arguments = StartMes;
			
			Proc.Start();
		}
	}
}



