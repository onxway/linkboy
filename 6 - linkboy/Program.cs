﻿

using System;
using System.Windows.Forms;
using System.Diagnostics;

namespace n_App
{
	/// <summary>
	/// Class with program entry point.
	/// </summary>
	internal sealed class Program
	{
		/// <summary>
		/// Program entry point.
		/// </summary>
		[STAThread]
		private static void Main(string[] args)
		{
			Application.EnableVisualStyles();
			Application.SetCompatibleTextRenderingDefault(false);
			
			//Application.Run(new MainForm());
			
			
			string DefaultFilePath = null;
			
			//+++++++++++++++++++++++++++++++++++++++++++++
			//判断是否加载文件
			DefaultFilePath = p_Win.CommandLine.GetStartPath( args );
			
			Process Proc = new Process();
			Proc.StartInfo.WindowStyle = ProcessWindowStyle.Normal;
			//Proc.StartInfo.UseShellExecute = false;
			//Proc.StartInfo.CreateNoWindow = true;
			//Proc.StartInfo.RedirectStandardOutput = true;
			
			Proc.StartInfo.WorkingDirectory = AppDomain.CurrentDomain.BaseDirectory;
			
			Proc.StartInfo.FileName = @"linkboy\linkboy.exe";
			
			string StartMes = "*Code*"; //Circuit or ...
			Proc.StartInfo.Arguments = StartMes + DefaultFilePath;
			
			Proc.Start();
		}
	}
}



