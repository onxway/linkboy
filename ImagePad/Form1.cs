﻿
namespace n_ImageEditForm
{
using System;
using System.Windows.Forms;
using System.Drawing;
//using c_FormMover;
using n_ImageEditPanel;
using winlib.MyColorForm;

public partial class Form1 : Form
{
	public delegate void D_ImageChanged( Bitmap b, bool bo );
	public D_ImageChanged ImageChanged;
	
	public bool SingleMode = true;
	
	//FormMover fm;
	
	MyColorForm MyColorBox;
	
	static string v_FileName;
	public string FileName {
		set {
			v_FileName = value;
			this.buttonRol.Enabled = true;
			this.textBoxAngle.Enabled = true;
			IPanel.isRol = false;
		}
		get {
			return v_FileName;
		}
	}
	
	ImageEditPanel IPanel;
	
	const int BackOff = 30;
	
	//主窗口
	public Form1( string FName )
	{
		InitializeComponent();
		
		foreach( Control c in panelToolColor.Controls ) {
			if( c is Button ) {
				Button b = (Button)c;
				if( b.Name != "buttonSetColor" && b.Name != "buttonGetColor" ) {
					b.Click += new EventHandler( ButtonSysColorClick );
					b.ForeColor = Color.Gainsboro;
				}
			}
		}
		
		this.trackBarBack.Maximum = 255 + BackOff;
		
		panel2.Height = panelToolShape.Height + labelMessage.Height;
		
		//fm = new FormMover( this );
		
		IPanel = new ImageEditPanel( this, Screen.PrimaryScreen.WorkingArea.Width, Screen.PrimaryScreen.WorkingArea.Height );
		this.panel1.Controls.Add( IPanel );
		
		IPanel.D_GetColor = SetColor;
		
		this.MouseWheel += UserMouseWheel;
		
		//ButtonOpenClick( null, null );
		IPanel.isRol = false;
		IPanel.CommonEvent = this.CommonEvent;
		IPanel.SetImage( new Bitmap( 400, 400 ) );
		
		TextBox色调容差TextChanged( null, null );
		TextBoxEraseRTextChanged( null, null );
		
		TrackBarBackScroll( null, null );
		
		MyColorBox = new MyColorForm();
		
		if( FName != null ) {
			//BackBitmap = new Bitmap( FileName );
			Image i = Image.FromFile( FName );
			IPanel.SetImage( new Bitmap( i ) );
			i.Dispose();
			
			//这个需要放到最后
			FileName = FName;
		}
		
		
		panelToolImageSwap.Dock = DockStyle.Left;
		panelToolTrimBack.Dock = DockStyle.Left;
		panelToolLine.Dock = DockStyle.Left;
		panelToolPen.Dock = DockStyle.Left;
		panelToolShape.Dock = DockStyle.Left;
		panelToolColor.Dock = DockStyle.Left;
		
		panelToolColor.SendToBack();
		labelMessage.SendToBack();
		
		/*
		panelToolImageSwap.Location = new Point( 0, 0 );
		panelToolTrimBack.Location = new Point( panelToolColor.Width, 0 );
		panelToolErase.Location = new Point( panelToolColor.Width, 0 );
		*/
		
		ButtonToolClick( buttonImageSwap, null );
	}
	
	//运行
	public void Run( Bitmap b )
	{
		if( b != null ) {
			IPanel.SetImage( b );
		}
		this.Visible = true;
	}
	
	//添加图片
	void ButtonOpenClick(object sender, EventArgs e)
	{
		//打开文件对话框
		OpenFileDialog OpenFileDlg = new OpenFileDialog();
		OpenFileDlg.Filter = "所有类型文件 | *.*";
		OpenFileDlg.Title = "请选择文件";
		
		//图片编辑不需要放到当前文件下
		//if( !G.ccode.isStartFile ) {
		//	OpenFileDlg.InitialDirectory = G.ccode.FilePath;
		//}
		//else {
		//	string Mes = "您的这个文件尚未保存到电脑硬盘上, 所以无法设置图片资源的默认文件夹,\n";
		//	Mes += "强烈建议您先保存一下这个文件到电脑上, 再选择图片路径, 这样方便系统记录您的默认文件夹.\n";
		//	Mes += "否则系统将会使用全局文件路径记录, 这样会导致您的vex程序复制到其他目录后运行异常.\n";
		//	MessageBox.Show( Mes );
		//}
		
		DialogResult dlgResult = OpenFileDlg.ShowDialog();
		if(dlgResult == DialogResult.OK) {
			
			//BackBitmap = new Bitmap( FileName );
			Image i = Image.FromFile( OpenFileDlg.FileName );
			IPanel.SetImage( new Bitmap( i ) );
			IPanel.MyRefresh();
			i.Dispose();
			
			if( ImageChanged != null ) {
				ImageChanged( IPanel.BackBitmap, false );
			}
			
			//这个需要放到最后
			FileName = OpenFileDlg.FileName;
		}
	}
	
	//------------------------------
	
	void CommonEvent( int t )
	{
		if( t == 1 ) {
			this.buttonRol.Enabled = false;
			this.textBoxAngle.Enabled = false;
		}
	}
	
	public void ChangeEraseSize( int delta )
	{
		try {
			IPanel.PenW = int.Parse( this.textBoxEraseR.Text );
			if( delta > 0 ) {
				if( IPanel.PenW < 1024 ) {
					IPanel.PenW *= 2;
				}
				
			}
			else {
				if( IPanel.PenW >= 2 ) {
					IPanel.PenW /= 2;
				}
			}
			int t = IPanel.PenW;
			this.textBoxEraseR.Text = t.ToString();
			IPanel.RefreshPen();
			IPanel.MyRefresh();
		}
		catch {
			//...
		}
	}
	
	void ButtonSaveClick(object sender, EventArgs e)
	{
		//保存文件对话框
		SaveFileDialog SaveFileDlg = new SaveFileDialog();
		SaveFileDlg.Filter = "图片类型文件 | *.png";
		SaveFileDlg.Title = "文件另存为...";
		
		if( FileName != null ) {
			SaveFileDlg.InitialDirectory = FileName.Remove( FileName.LastIndexOf( "\\" ) + 1 );
			string myname = FileName.Remove( 0, FileName.LastIndexOf( "\\" ) + 1 );
			myname = myname.Remove( myname.LastIndexOf( "." ) );
			SaveFileDlg.FileName = "remo-" + myname + ".png";
		}
		else {
			SaveFileDlg.FileName = "我的图片.png";
		}
		
		DialogResult dlgResult = SaveFileDlg.ShowDialog();
		if(dlgResult == DialogResult.OK) {
			string FilePath = SaveFileDlg.FileName;
			IPanel.BackBitmap.Save( FilePath, System.Drawing.Imaging.ImageFormat.Png );
		}
	}
	
	void ImageEditFormFormClosing(object sender, FormClosingEventArgs e)
	{
		if( !SingleMode ) {
			this.Visible = false;
			e.Cancel = true;
			
			if( ImageChanged != null ) {
				ImageChanged( IPanel.BackBitmap, true );
			}
			
			return;
		}
		
		DialogResult dr = MessageBox.Show( "需要保存图片吗?", "保存提示", MessageBoxButtons.YesNoCancel );
		if( dr == DialogResult.Yes ) {
			e.Cancel = true;
			ButtonSaveClick( null, null );
		}
		else if( dr == DialogResult.No ) {
			//...
		}
		else {
			e.Cancel = true;
		}
	}
	
	void ButtonUndoClick(object sender, EventArgs e)
	{
		IPanel.Undo();
		IPanel.MyRefresh();
		
		if( ImageChanged != null ) {
			ImageChanged( IPanel.BackBitmap, false );
		}
	}
	
	void UserMouseWheel( object sender, MouseEventArgs e )
	{
		this.labelMessage.Text = "不按下鼠标右键时, 通过滚轮调节画布大小; 如按下鼠标右键, 则通过滚轮调节画笔尺寸";
		
		IPanel.UserMouseWheel( e.X - panel1.Location.X, e.Y - panel1.Location.Y, e.Delta );
	}
	
	//--------------------------------------------------------------------------------------
	
	Button LastButton;
	
	void ButtonToolClick(object sender, EventArgs e)
	{
		if( LastButton != null ) {
			LastButton.BackColor = Color.White;
			LastButton.ForeColor = Color.Black;
		}
		Button b = (Button)sender;
		b.BackColor = Color.CornflowerBlue;
		b.ForeColor = Color.White;
		LastButton = b;
		
		DealTool( b );
	}
	
	void DealTool( Button b )
	{
		panelToolColor.Visible = false;
		panelToolImageSwap.Visible = false;
		panelToolTrimBack.Visible = false;
		panelToolPen.Visible = false;
		panelToolShape.Visible = false;
		panelToolLine.Visible = false;
		
		if( b.Name == "buttonImageSwap" ) {
			this.labelMessage.Text = "对图像进行各种变换";
			panelToolImageSwap.Visible = true;
		}
		if( b.Name == "buttonTrimBack" ) {
			this.labelMessage.Text = "去除背景工具 - 点击图片的某个位置后, 相近颜色的区域会变为指定的颜色";
			panelToolColor.Visible = true;
			panelToolTrimBack.Visible = true;
			IPanel.Flag = 1;
		}
		if( b.Name == "buttonErase" ) {
			this.labelMessage.Text = "画笔工具 - 用指定颜色绘制屏幕上的鼠标点";
			panelToolColor.Visible = true;
			panelToolPen.Visible = true;
			panelToolLine.Visible = true;
			IPanel.Flag = 2;
		}
		if( b.Name == "buttonShape" ) {
			this.labelMessage.Text = "形状工具 - 用指定形状绘图";
			panelToolColor.Visible = true;
			panelToolPen.Visible = true;
			panelToolShape.Visible = true;
			IPanel.Flag = 3;
			IPanel.ShapeFlag = 0;
		}
		IPanel.MyRefresh();
	}
	
	//--------------------------------------------------------------------------------------
	
	void TextBox色调容差TextChanged(object sender, EventArgs e)
	{
		try {
			IPanel.RC_H = int.Parse( this.textBox色调容差.Text );
			IPanel.MyRefresh();
			this.labelMessage.Text = "";
		}
		catch {
			this.labelMessage.Text = "色调容差参数错误";
		}
	}
	
	void ImageEditFormSizeChanged(object sender, EventArgs e)
	{
		//由用户反映这里报错, 所以防护一下, 有时间再研究下
		
		try {
		if( IPanel != null ) {
			IPanel.MyRefresh();
		}
		}
		catch {
			//...
		}
	}
	
	void TextBoxEraseRTextChanged(object sender, EventArgs e)
	{
		try {
			IPanel.PenW = int.Parse( this.textBoxEraseR.Text );
			if( IPanel.PenW == 0 ) {
				IPanel.PenW = 1;
				this.textBoxEraseR.Text = "1";
			}
			IPanel.RefreshPen();
			IPanel.MyRefresh();
		}
		catch {
			//...
		}
	}
	
	//------------------------------
	
	void ButtonRolClick(object sender, EventArgs e)
	{
		IPanel.Rol( int.Parse( this.textBoxAngle.Text ) );
		IPanel.MyRefresh();
	}
	
	void ButtonSwapXClick(object sender, EventArgs e)
	{
		IPanel.SwapX();
		IPanel.MyRefresh();
	}
	
	void ButtonSwapYClick(object sender, EventArgs e)
	{
		IPanel.SwapY();
		IPanel.MyRefresh();
	}
	
	void TrackBarBackScroll(object sender, EventArgs e)
	{
		IPanel.BackBrush.Dispose();
		int c = this.trackBarBack.Value;
		//IPanel.BackBrush = new SolidBrush( Color.FromArgb( c, c, c ) );
		Color c1, c2;
		if( c >= BackOff ) {
			c1 = Color.FromArgb( c - BackOff, c - BackOff, c - BackOff );
		}
		else {
			c1 = Color.FromArgb( 0, 0, 0 );
		}
		if( c > 255 ) {
			c2 = Color.FromArgb( 255, 255, 255 );
		}
		else {
			c2 = Color.FromArgb( c, c, c );
		}
		IPanel.BackBrush = new System.Drawing.Drawing2D.HatchBrush( System.Drawing.Drawing2D.HatchStyle.Sphere, c1, c2 );
		
		IPanel.MyRefresh();
		
		this.buttonFocus.Focus();
	}
	
	void CheckBox消除锯齿CheckedChanged(object sender, EventArgs e)
	{
		IPanel.HighQ = this.checkBox消除锯齿.Checked;
		if( IPanel.HighQ ) {
			IPanel.g.SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.HighQuality;
		}
		else {
			IPanel.g.SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.HighSpeed;
		}
		IPanel.MyRefresh();
	}
	
	void CheckBox圆形画笔CheckedChanged(object sender, EventArgs e)
	{
		IPanel.isRoundPen = this.checkBox圆形画笔.Checked;
		IPanel.RefreshPen();
		IPanel.MyRefresh();
	}
	
	void CheckBoxAutoLineCheckedChanged(object sender, EventArgs e)
	{
		IPanel.AutoLine = this.checkBoxAutoLine.Checked;
		IPanel.MyRefresh();
	}
	
	void ButtonSetColorClick(object sender, EventArgs e)
	{
		ColorDialog c = new ColorDialog();
		if( c.ShowDialog() == DialogResult.OK ) {
			this.buttonSetColor.BackColor = c.Color;
			SetColor( c.Color );
			
			buttonSetColor.BackColor = c.Color;
		}
		
		/*
		Color c = MyColorBox.Run();
		if( c != Color.Empty ) {
			this.buttonColor.BackColor = c;
			IPanel.PenColor = c;
			IPanel.RefreshPen();
			this.labelColor.Text = c.ToString();
		}
		*/
	}
	
	void ButtonSysColorClick(object sender, EventArgs e)
	{
		Button b = (Button)sender;
		if( b.Name == "buttonTrans" ) {
			SetColor( Color.Transparent );
		}
		else {
			SetColor( b.BackColor );
		}
	}
	
	//设置颜色
	void SetColor( Color c )
	{
		this.labelColor.BackColor = c;
		this.labelColor.Text = "红绿蓝：\n" + c.R + ", " + c.G + ", " + c.B + "\n" + "透明度：" + c.A;
		IPanel.PenColor = c;
		IPanel.RefreshPen();
		IPanel.MyRefresh();
	}
	
	void CheckBoxFreeCheckedChanged(object sender, EventArgs e)
	{
		IPanel.FreePoint = checkBoxFree.Checked;
		IPanel.MyRefresh();
	}
	
	void ButtonSetImgSizeClick(object sender, EventArgs e)
	{
		try {
			int w = int.Parse( textBoxImgWidth.Text );
			int h = int.Parse( textBoxImgHeight.Text );
			
			IPanel.SetImage( new Bitmap( w, h ) );
			IPanel.MyRefresh();
			
			if( ImageChanged != null ) {
				ImageChanged( IPanel.BackBitmap, false );
			}
		}
		catch {
			MessageBox.Show( "请输入合法的图片尺寸数据" );
		}
		
	}
	
	void RadioButtonToolShapeClick(object sender, EventArgs e)
	{
		if( radioButtonToolLine.Checked ) {
			IPanel.ShapeType = ImageEditPanel.E_ShapeType.Line;
		}
		else if( radioButtonToolRect.Checked ) {
			IPanel.ShapeType = ImageEditPanel.E_ShapeType.Rectangle;
		}
		else if( radioButtonToolCircle.Checked ) {
			IPanel.ShapeType = ImageEditPanel.E_ShapeType.Circle;
		}
		else if( radioButtonToolArc.Checked ) {
			IPanel.ShapeType = ImageEditPanel.E_ShapeType.Arc;
		}
		else {
			//...
		}
	}
	
	//清空图片
	void ButtonToolClearClick(object sender, EventArgs e)
	{
		IPanel.ClearImage( Color.White );
		IPanel.MyRefresh();
	}
	
	void ButtonGetColorClick(object sender, EventArgs e)
	{
		IPanel.GetColor = true;
	}
}
}



