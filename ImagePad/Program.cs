﻿
using System;
using System.Windows.Forms;

namespace n_ImageEditForm
{
internal sealed class Program
{
	[STAThread]
	private static void Main(string[] args)
	{
		Application.EnableVisualStyles();
		Application.SetCompatibleTextRenderingDefault(false);
		
		string DefaultFilePath = null;
		
		//判断是否加载文件
		DefaultFilePath = p_Win.CommandLine.GetStartPath( args );
		
		Application.Run( new Form1( DefaultFilePath ) );
	}
}
}


