﻿

namespace n_ImageEditForm
{
	partial class Form1
	{
		/// <summary>
		/// Designer variable used to keep track of non-visual components.
		/// </summary>
		private System.ComponentModel.IContainer components = null;
		
		/// <summary>
		/// Disposes resources used by the form.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing) {
				if (components != null) {
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}
		
		/// <summary>
		/// This method is required for Windows Forms designer support.
		/// Do not change the method contents inside the source code editor. The Forms designer might
		/// not be able to load this method if it was changed manually.
		/// </summary>
		private void InitializeComponent()
		{
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));
			this.buttonOpen = new System.Windows.Forms.Button();
			this.label1 = new System.Windows.Forms.Label();
			this.buttonSave = new System.Windows.Forms.Button();
			this.textBox色调容差 = new System.Windows.Forms.TextBox();
			this.buttonUndo = new System.Windows.Forms.Button();
			this.buttonTrimBack = new System.Windows.Forms.Button();
			this.buttonErase = new System.Windows.Forms.Button();
			this.labelMessage = new System.Windows.Forms.Label();
			this.panel1 = new System.Windows.Forms.Panel();
			this.panel2 = new System.Windows.Forms.Panel();
			this.panelToolLine = new System.Windows.Forms.Panel();
			this.checkBoxAutoLine = new System.Windows.Forms.CheckBox();
			this.panelToolPen = new System.Windows.Forms.Panel();
			this.textBoxEraseR = new System.Windows.Forms.TextBox();
			this.checkBoxFree = new System.Windows.Forms.CheckBox();
			this.checkBox圆形画笔 = new System.Windows.Forms.CheckBox();
			this.label4 = new System.Windows.Forms.Label();
			this.checkBox消除锯齿 = new System.Windows.Forms.CheckBox();
			this.panelToolTrimBack = new System.Windows.Forms.Panel();
			this.panelToolShape = new System.Windows.Forms.Panel();
			this.radioButtonToolArc = new System.Windows.Forms.RadioButton();
			this.radioButtonToolCircle = new System.Windows.Forms.RadioButton();
			this.radioButtonToolRect = new System.Windows.Forms.RadioButton();
			this.radioButtonToolLine = new System.Windows.Forms.RadioButton();
			this.panelToolImageSwap = new System.Windows.Forms.Panel();
			this.buttonToolClear = new System.Windows.Forms.Button();
			this.buttonSwapX = new System.Windows.Forms.Button();
			this.buttonRol = new System.Windows.Forms.Button();
			this.textBoxAngle = new System.Windows.Forms.TextBox();
			this.buttonSwapY = new System.Windows.Forms.Button();
			this.panelToolColor = new System.Windows.Forms.Panel();
			this.buttonGetColor = new System.Windows.Forms.Button();
			this.buttonSetColor = new System.Windows.Forms.Button();
			this.button37 = new System.Windows.Forms.Button();
			this.button38 = new System.Windows.Forms.Button();
			this.button39 = new System.Windows.Forms.Button();
			this.button40 = new System.Windows.Forms.Button();
			this.button41 = new System.Windows.Forms.Button();
			this.buttonTrans = new System.Windows.Forms.Button();
			this.button29 = new System.Windows.Forms.Button();
			this.button30 = new System.Windows.Forms.Button();
			this.button31 = new System.Windows.Forms.Button();
			this.button32 = new System.Windows.Forms.Button();
			this.button33 = new System.Windows.Forms.Button();
			this.button34 = new System.Windows.Forms.Button();
			this.button35 = new System.Windows.Forms.Button();
			this.button15 = new System.Windows.Forms.Button();
			this.button16 = new System.Windows.Forms.Button();
			this.button17 = new System.Windows.Forms.Button();
			this.button18 = new System.Windows.Forms.Button();
			this.button19 = new System.Windows.Forms.Button();
			this.button20 = new System.Windows.Forms.Button();
			this.button21 = new System.Windows.Forms.Button();
			this.button22 = new System.Windows.Forms.Button();
			this.button23 = new System.Windows.Forms.Button();
			this.button24 = new System.Windows.Forms.Button();
			this.button25 = new System.Windows.Forms.Button();
			this.button26 = new System.Windows.Forms.Button();
			this.button27 = new System.Windows.Forms.Button();
			this.button28 = new System.Windows.Forms.Button();
			this.button5 = new System.Windows.Forms.Button();
			this.button9 = new System.Windows.Forms.Button();
			this.button10 = new System.Windows.Forms.Button();
			this.button11 = new System.Windows.Forms.Button();
			this.button12 = new System.Windows.Forms.Button();
			this.button13 = new System.Windows.Forms.Button();
			this.button14 = new System.Windows.Forms.Button();
			this.button6 = new System.Windows.Forms.Button();
			this.button7 = new System.Windows.Forms.Button();
			this.button8 = new System.Windows.Forms.Button();
			this.button3 = new System.Windows.Forms.Button();
			this.button4 = new System.Windows.Forms.Button();
			this.button2 = new System.Windows.Forms.Button();
			this.button1 = new System.Windows.Forms.Button();
			this.labelColor = new System.Windows.Forms.Label();
			this.buttonFocus = new System.Windows.Forms.Button();
			this.textBoxImgHeight = new System.Windows.Forms.TextBox();
			this.label3 = new System.Windows.Forms.Label();
			this.textBoxImgWidth = new System.Windows.Forms.TextBox();
			this.buttonSetImgSize = new System.Windows.Forms.Button();
			this.label2 = new System.Windows.Forms.Label();
			this.label5 = new System.Windows.Forms.Label();
			this.trackBarBack = new System.Windows.Forms.TrackBar();
			this.panel5 = new System.Windows.Forms.Panel();
			this.label7 = new System.Windows.Forms.Label();
			this.buttonShape = new System.Windows.Forms.Button();
			this.buttonImageSwap = new System.Windows.Forms.Button();
			this.panel8 = new System.Windows.Forms.Panel();
			this.label6 = new System.Windows.Forms.Label();
			this.panel2.SuspendLayout();
			this.panelToolLine.SuspendLayout();
			this.panelToolPen.SuspendLayout();
			this.panelToolTrimBack.SuspendLayout();
			this.panelToolShape.SuspendLayout();
			this.panelToolImageSwap.SuspendLayout();
			this.panelToolColor.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.trackBarBack)).BeginInit();
			this.panel5.SuspendLayout();
			this.panel8.SuspendLayout();
			this.SuspendLayout();
			// 
			// buttonOpen
			// 
			this.buttonOpen.BackColor = System.Drawing.Color.CornflowerBlue;
			resources.ApplyResources(this.buttonOpen, "buttonOpen");
			this.buttonOpen.ForeColor = System.Drawing.Color.White;
			this.buttonOpen.Name = "buttonOpen";
			this.buttonOpen.UseVisualStyleBackColor = false;
			this.buttonOpen.Click += new System.EventHandler(this.ButtonOpenClick);
			// 
			// label1
			// 
			resources.ApplyResources(this.label1, "label1");
			this.label1.Name = "label1";
			// 
			// buttonSave
			// 
			this.buttonSave.BackColor = System.Drawing.Color.CornflowerBlue;
			resources.ApplyResources(this.buttonSave, "buttonSave");
			this.buttonSave.ForeColor = System.Drawing.Color.White;
			this.buttonSave.Name = "buttonSave";
			this.buttonSave.UseVisualStyleBackColor = false;
			this.buttonSave.Click += new System.EventHandler(this.ButtonSaveClick);
			// 
			// textBox色调容差
			// 
			resources.ApplyResources(this.textBox色调容差, "textBox色调容差");
			this.textBox色调容差.Name = "textBox色调容差";
			this.textBox色调容差.TextChanged += new System.EventHandler(this.TextBox色调容差TextChanged);
			// 
			// buttonUndo
			// 
			this.buttonUndo.BackColor = System.Drawing.Color.CornflowerBlue;
			resources.ApplyResources(this.buttonUndo, "buttonUndo");
			this.buttonUndo.ForeColor = System.Drawing.Color.White;
			this.buttonUndo.Name = "buttonUndo";
			this.buttonUndo.UseVisualStyleBackColor = false;
			this.buttonUndo.Click += new System.EventHandler(this.ButtonUndoClick);
			// 
			// buttonTrimBack
			// 
			this.buttonTrimBack.BackColor = System.Drawing.Color.WhiteSmoke;
			resources.ApplyResources(this.buttonTrimBack, "buttonTrimBack");
			this.buttonTrimBack.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
			this.buttonTrimBack.Name = "buttonTrimBack";
			this.buttonTrimBack.UseVisualStyleBackColor = false;
			this.buttonTrimBack.Click += new System.EventHandler(this.ButtonToolClick);
			// 
			// buttonErase
			// 
			this.buttonErase.BackColor = System.Drawing.Color.WhiteSmoke;
			resources.ApplyResources(this.buttonErase, "buttonErase");
			this.buttonErase.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
			this.buttonErase.Name = "buttonErase";
			this.buttonErase.UseVisualStyleBackColor = false;
			this.buttonErase.Click += new System.EventHandler(this.ButtonToolClick);
			// 
			// labelMessage
			// 
			resources.ApplyResources(this.labelMessage, "labelMessage");
			this.labelMessage.ForeColor = System.Drawing.Color.SlateGray;
			this.labelMessage.Name = "labelMessage";
			// 
			// panel1
			// 
			resources.ApplyResources(this.panel1, "panel1");
			this.panel1.Name = "panel1";
			// 
			// panel2
			// 
			this.panel2.BackColor = System.Drawing.Color.White;
			resources.ApplyResources(this.panel2, "panel2");
			this.panel2.Controls.Add(this.panelToolLine);
			this.panel2.Controls.Add(this.panelToolPen);
			this.panel2.Controls.Add(this.panelToolTrimBack);
			this.panel2.Controls.Add(this.panelToolShape);
			this.panel2.Controls.Add(this.panelToolImageSwap);
			this.panel2.Controls.Add(this.panelToolColor);
			this.panel2.Controls.Add(this.labelMessage);
			this.panel2.Name = "panel2";
			// 
			// panelToolLine
			// 
			this.panelToolLine.BackColor = System.Drawing.Color.Transparent;
			this.panelToolLine.Controls.Add(this.checkBoxAutoLine);
			resources.ApplyResources(this.panelToolLine, "panelToolLine");
			this.panelToolLine.Name = "panelToolLine";
			// 
			// checkBoxAutoLine
			// 
			this.checkBoxAutoLine.Checked = true;
			this.checkBoxAutoLine.CheckState = System.Windows.Forms.CheckState.Checked;
			resources.ApplyResources(this.checkBoxAutoLine, "checkBoxAutoLine");
			this.checkBoxAutoLine.Name = "checkBoxAutoLine";
			this.checkBoxAutoLine.UseVisualStyleBackColor = true;
			this.checkBoxAutoLine.CheckedChanged += new System.EventHandler(this.CheckBoxAutoLineCheckedChanged);
			// 
			// panelToolPen
			// 
			this.panelToolPen.BackColor = System.Drawing.Color.Transparent;
			this.panelToolPen.Controls.Add(this.textBoxEraseR);
			this.panelToolPen.Controls.Add(this.checkBoxFree);
			this.panelToolPen.Controls.Add(this.checkBox圆形画笔);
			this.panelToolPen.Controls.Add(this.label4);
			this.panelToolPen.Controls.Add(this.checkBox消除锯齿);
			resources.ApplyResources(this.panelToolPen, "panelToolPen");
			this.panelToolPen.Name = "panelToolPen";
			// 
			// textBoxEraseR
			// 
			resources.ApplyResources(this.textBoxEraseR, "textBoxEraseR");
			this.textBoxEraseR.Name = "textBoxEraseR";
			this.textBoxEraseR.TextChanged += new System.EventHandler(this.TextBoxEraseRTextChanged);
			// 
			// checkBoxFree
			// 
			this.checkBoxFree.Checked = true;
			this.checkBoxFree.CheckState = System.Windows.Forms.CheckState.Checked;
			resources.ApplyResources(this.checkBoxFree, "checkBoxFree");
			this.checkBoxFree.Name = "checkBoxFree";
			this.checkBoxFree.UseVisualStyleBackColor = true;
			this.checkBoxFree.CheckedChanged += new System.EventHandler(this.CheckBoxFreeCheckedChanged);
			// 
			// checkBox圆形画笔
			// 
			this.checkBox圆形画笔.Checked = true;
			this.checkBox圆形画笔.CheckState = System.Windows.Forms.CheckState.Checked;
			resources.ApplyResources(this.checkBox圆形画笔, "checkBox圆形画笔");
			this.checkBox圆形画笔.Name = "checkBox圆形画笔";
			this.checkBox圆形画笔.UseVisualStyleBackColor = true;
			this.checkBox圆形画笔.CheckedChanged += new System.EventHandler(this.CheckBox圆形画笔CheckedChanged);
			// 
			// label4
			// 
			resources.ApplyResources(this.label4, "label4");
			this.label4.Name = "label4";
			// 
			// checkBox消除锯齿
			// 
			this.checkBox消除锯齿.Checked = true;
			this.checkBox消除锯齿.CheckState = System.Windows.Forms.CheckState.Checked;
			resources.ApplyResources(this.checkBox消除锯齿, "checkBox消除锯齿");
			this.checkBox消除锯齿.Name = "checkBox消除锯齿";
			this.checkBox消除锯齿.UseVisualStyleBackColor = true;
			this.checkBox消除锯齿.CheckedChanged += new System.EventHandler(this.CheckBox消除锯齿CheckedChanged);
			// 
			// panelToolTrimBack
			// 
			this.panelToolTrimBack.BackColor = System.Drawing.Color.Transparent;
			this.panelToolTrimBack.Controls.Add(this.textBox色调容差);
			this.panelToolTrimBack.Controls.Add(this.label1);
			resources.ApplyResources(this.panelToolTrimBack, "panelToolTrimBack");
			this.panelToolTrimBack.Name = "panelToolTrimBack";
			// 
			// panelToolShape
			// 
			this.panelToolShape.BackColor = System.Drawing.Color.Transparent;
			this.panelToolShape.Controls.Add(this.radioButtonToolArc);
			this.panelToolShape.Controls.Add(this.radioButtonToolCircle);
			this.panelToolShape.Controls.Add(this.radioButtonToolRect);
			this.panelToolShape.Controls.Add(this.radioButtonToolLine);
			resources.ApplyResources(this.panelToolShape, "panelToolShape");
			this.panelToolShape.Name = "panelToolShape";
			// 
			// radioButtonToolArc
			// 
			resources.ApplyResources(this.radioButtonToolArc, "radioButtonToolArc");
			this.radioButtonToolArc.Name = "radioButtonToolArc";
			this.radioButtonToolArc.UseVisualStyleBackColor = true;
			this.radioButtonToolArc.Click += new System.EventHandler(this.RadioButtonToolShapeClick);
			// 
			// radioButtonToolCircle
			// 
			resources.ApplyResources(this.radioButtonToolCircle, "radioButtonToolCircle");
			this.radioButtonToolCircle.Name = "radioButtonToolCircle";
			this.radioButtonToolCircle.UseVisualStyleBackColor = true;
			this.radioButtonToolCircle.Click += new System.EventHandler(this.RadioButtonToolShapeClick);
			// 
			// radioButtonToolRect
			// 
			resources.ApplyResources(this.radioButtonToolRect, "radioButtonToolRect");
			this.radioButtonToolRect.Name = "radioButtonToolRect";
			this.radioButtonToolRect.UseVisualStyleBackColor = true;
			this.radioButtonToolRect.Click += new System.EventHandler(this.RadioButtonToolShapeClick);
			// 
			// radioButtonToolLine
			// 
			this.radioButtonToolLine.Checked = true;
			resources.ApplyResources(this.radioButtonToolLine, "radioButtonToolLine");
			this.radioButtonToolLine.Name = "radioButtonToolLine";
			this.radioButtonToolLine.TabStop = true;
			this.radioButtonToolLine.UseVisualStyleBackColor = true;
			this.radioButtonToolLine.Click += new System.EventHandler(this.RadioButtonToolShapeClick);
			// 
			// panelToolImageSwap
			// 
			this.panelToolImageSwap.BackColor = System.Drawing.Color.Transparent;
			this.panelToolImageSwap.Controls.Add(this.buttonToolClear);
			this.panelToolImageSwap.Controls.Add(this.buttonSwapX);
			this.panelToolImageSwap.Controls.Add(this.buttonRol);
			this.panelToolImageSwap.Controls.Add(this.textBoxAngle);
			this.panelToolImageSwap.Controls.Add(this.buttonSwapY);
			resources.ApplyResources(this.panelToolImageSwap, "panelToolImageSwap");
			this.panelToolImageSwap.Name = "panelToolImageSwap";
			// 
			// buttonToolClear
			// 
			this.buttonToolClear.BackColor = System.Drawing.Color.CornflowerBlue;
			resources.ApplyResources(this.buttonToolClear, "buttonToolClear");
			this.buttonToolClear.ForeColor = System.Drawing.Color.White;
			this.buttonToolClear.Name = "buttonToolClear";
			this.buttonToolClear.UseVisualStyleBackColor = false;
			this.buttonToolClear.Click += new System.EventHandler(this.ButtonToolClearClick);
			// 
			// buttonSwapX
			// 
			this.buttonSwapX.BackColor = System.Drawing.Color.CornflowerBlue;
			resources.ApplyResources(this.buttonSwapX, "buttonSwapX");
			this.buttonSwapX.ForeColor = System.Drawing.Color.White;
			this.buttonSwapX.Name = "buttonSwapX";
			this.buttonSwapX.UseVisualStyleBackColor = false;
			this.buttonSwapX.Click += new System.EventHandler(this.ButtonSwapXClick);
			// 
			// buttonRol
			// 
			this.buttonRol.BackColor = System.Drawing.Color.CornflowerBlue;
			resources.ApplyResources(this.buttonRol, "buttonRol");
			this.buttonRol.ForeColor = System.Drawing.Color.White;
			this.buttonRol.Name = "buttonRol";
			this.buttonRol.UseVisualStyleBackColor = false;
			this.buttonRol.Click += new System.EventHandler(this.ButtonRolClick);
			// 
			// textBoxAngle
			// 
			resources.ApplyResources(this.textBoxAngle, "textBoxAngle");
			this.textBoxAngle.Name = "textBoxAngle";
			// 
			// buttonSwapY
			// 
			this.buttonSwapY.BackColor = System.Drawing.Color.CornflowerBlue;
			resources.ApplyResources(this.buttonSwapY, "buttonSwapY");
			this.buttonSwapY.ForeColor = System.Drawing.Color.White;
			this.buttonSwapY.Name = "buttonSwapY";
			this.buttonSwapY.UseVisualStyleBackColor = false;
			this.buttonSwapY.Click += new System.EventHandler(this.ButtonSwapYClick);
			// 
			// panelToolColor
			// 
			this.panelToolColor.BackColor = System.Drawing.Color.Transparent;
			this.panelToolColor.Controls.Add(this.buttonGetColor);
			this.panelToolColor.Controls.Add(this.buttonSetColor);
			this.panelToolColor.Controls.Add(this.button37);
			this.panelToolColor.Controls.Add(this.button38);
			this.panelToolColor.Controls.Add(this.button39);
			this.panelToolColor.Controls.Add(this.button40);
			this.panelToolColor.Controls.Add(this.button41);
			this.panelToolColor.Controls.Add(this.buttonTrans);
			this.panelToolColor.Controls.Add(this.button29);
			this.panelToolColor.Controls.Add(this.button30);
			this.panelToolColor.Controls.Add(this.button31);
			this.panelToolColor.Controls.Add(this.button32);
			this.panelToolColor.Controls.Add(this.button33);
			this.panelToolColor.Controls.Add(this.button34);
			this.panelToolColor.Controls.Add(this.button35);
			this.panelToolColor.Controls.Add(this.button15);
			this.panelToolColor.Controls.Add(this.button16);
			this.panelToolColor.Controls.Add(this.button17);
			this.panelToolColor.Controls.Add(this.button18);
			this.panelToolColor.Controls.Add(this.button19);
			this.panelToolColor.Controls.Add(this.button20);
			this.panelToolColor.Controls.Add(this.button21);
			this.panelToolColor.Controls.Add(this.button22);
			this.panelToolColor.Controls.Add(this.button23);
			this.panelToolColor.Controls.Add(this.button24);
			this.panelToolColor.Controls.Add(this.button25);
			this.panelToolColor.Controls.Add(this.button26);
			this.panelToolColor.Controls.Add(this.button27);
			this.panelToolColor.Controls.Add(this.button28);
			this.panelToolColor.Controls.Add(this.button5);
			this.panelToolColor.Controls.Add(this.button9);
			this.panelToolColor.Controls.Add(this.button10);
			this.panelToolColor.Controls.Add(this.button11);
			this.panelToolColor.Controls.Add(this.button12);
			this.panelToolColor.Controls.Add(this.button13);
			this.panelToolColor.Controls.Add(this.button14);
			this.panelToolColor.Controls.Add(this.button6);
			this.panelToolColor.Controls.Add(this.button7);
			this.panelToolColor.Controls.Add(this.button8);
			this.panelToolColor.Controls.Add(this.button3);
			this.panelToolColor.Controls.Add(this.button4);
			this.panelToolColor.Controls.Add(this.button2);
			this.panelToolColor.Controls.Add(this.button1);
			this.panelToolColor.Controls.Add(this.labelColor);
			resources.ApplyResources(this.panelToolColor, "panelToolColor");
			this.panelToolColor.Name = "panelToolColor";
			// 
			// buttonGetColor
			// 
			this.buttonGetColor.BackColor = System.Drawing.Color.CornflowerBlue;
			resources.ApplyResources(this.buttonGetColor, "buttonGetColor");
			this.buttonGetColor.ForeColor = System.Drawing.Color.White;
			this.buttonGetColor.Name = "buttonGetColor";
			this.buttonGetColor.UseVisualStyleBackColor = false;
			this.buttonGetColor.Click += new System.EventHandler(this.ButtonGetColorClick);
			// 
			// buttonSetColor
			// 
			this.buttonSetColor.BackColor = System.Drawing.Color.DarkGray;
			resources.ApplyResources(this.buttonSetColor, "buttonSetColor");
			this.buttonSetColor.ForeColor = System.Drawing.Color.Gray;
			this.buttonSetColor.Name = "buttonSetColor";
			this.buttonSetColor.UseVisualStyleBackColor = false;
			this.buttonSetColor.Click += new System.EventHandler(this.ButtonSetColorClick);
			// 
			// button37
			// 
			this.button37.BackColor = System.Drawing.Color.Purple;
			resources.ApplyResources(this.button37, "button37");
			this.button37.ForeColor = System.Drawing.Color.White;
			this.button37.Name = "button37";
			this.button37.UseVisualStyleBackColor = false;
			// 
			// button38
			// 
			this.button38.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
			resources.ApplyResources(this.button38, "button38");
			this.button38.ForeColor = System.Drawing.Color.White;
			this.button38.Name = "button38";
			this.button38.UseVisualStyleBackColor = false;
			// 
			// button39
			// 
			this.button39.BackColor = System.Drawing.Color.Fuchsia;
			resources.ApplyResources(this.button39, "button39");
			this.button39.ForeColor = System.Drawing.Color.White;
			this.button39.Name = "button39";
			this.button39.UseVisualStyleBackColor = false;
			// 
			// button40
			// 
			this.button40.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(255)))));
			resources.ApplyResources(this.button40, "button40");
			this.button40.ForeColor = System.Drawing.Color.White;
			this.button40.Name = "button40";
			this.button40.UseVisualStyleBackColor = false;
			// 
			// button41
			// 
			this.button41.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(255)))));
			resources.ApplyResources(this.button41, "button41");
			this.button41.ForeColor = System.Drawing.Color.White;
			this.button41.Name = "button41";
			this.button41.UseVisualStyleBackColor = false;
			// 
			// buttonTrans
			// 
			this.buttonTrans.BackColor = System.Drawing.Color.WhiteSmoke;
			resources.ApplyResources(this.buttonTrans, "buttonTrans");
			this.buttonTrans.ForeColor = System.Drawing.Color.Gray;
			this.buttonTrans.Name = "buttonTrans";
			this.buttonTrans.UseVisualStyleBackColor = false;
			// 
			// button29
			// 
			this.button29.BackColor = System.Drawing.Color.Navy;
			resources.ApplyResources(this.button29, "button29");
			this.button29.ForeColor = System.Drawing.Color.White;
			this.button29.Name = "button29";
			this.button29.UseVisualStyleBackColor = false;
			// 
			// button30
			// 
			this.button30.BackColor = System.Drawing.Color.Teal;
			resources.ApplyResources(this.button30, "button30");
			this.button30.ForeColor = System.Drawing.Color.White;
			this.button30.Name = "button30";
			this.button30.UseVisualStyleBackColor = false;
			// 
			// button31
			// 
			this.button31.BackColor = System.Drawing.Color.Green;
			resources.ApplyResources(this.button31, "button31");
			this.button31.ForeColor = System.Drawing.Color.White;
			this.button31.Name = "button31";
			this.button31.UseVisualStyleBackColor = false;
			// 
			// button32
			// 
			this.button32.BackColor = System.Drawing.Color.Olive;
			resources.ApplyResources(this.button32, "button32");
			this.button32.ForeColor = System.Drawing.Color.White;
			this.button32.Name = "button32";
			this.button32.UseVisualStyleBackColor = false;
			// 
			// button33
			// 
			this.button33.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(64)))), ((int)(((byte)(0)))));
			resources.ApplyResources(this.button33, "button33");
			this.button33.ForeColor = System.Drawing.Color.White;
			this.button33.Name = "button33";
			this.button33.UseVisualStyleBackColor = false;
			// 
			// button34
			// 
			this.button34.BackColor = System.Drawing.Color.Maroon;
			resources.ApplyResources(this.button34, "button34");
			this.button34.ForeColor = System.Drawing.Color.White;
			this.button34.Name = "button34";
			this.button34.UseVisualStyleBackColor = false;
			// 
			// button35
			// 
			this.button35.BackColor = System.Drawing.Color.Black;
			resources.ApplyResources(this.button35, "button35");
			this.button35.ForeColor = System.Drawing.Color.White;
			this.button35.Name = "button35";
			this.button35.UseVisualStyleBackColor = false;
			// 
			// button15
			// 
			this.button15.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
			resources.ApplyResources(this.button15, "button15");
			this.button15.ForeColor = System.Drawing.Color.White;
			this.button15.Name = "button15";
			this.button15.UseVisualStyleBackColor = false;
			// 
			// button16
			// 
			this.button16.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(192)))));
			resources.ApplyResources(this.button16, "button16");
			this.button16.ForeColor = System.Drawing.Color.White;
			this.button16.Name = "button16";
			this.button16.UseVisualStyleBackColor = false;
			// 
			// button17
			// 
			this.button17.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
			resources.ApplyResources(this.button17, "button17");
			this.button17.ForeColor = System.Drawing.Color.White;
			this.button17.Name = "button17";
			this.button17.UseVisualStyleBackColor = false;
			// 
			// button18
			// 
			this.button18.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
			resources.ApplyResources(this.button18, "button18");
			this.button18.ForeColor = System.Drawing.Color.White;
			this.button18.Name = "button18";
			this.button18.UseVisualStyleBackColor = false;
			// 
			// button19
			// 
			this.button19.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(64)))), ((int)(((byte)(0)))));
			resources.ApplyResources(this.button19, "button19");
			this.button19.ForeColor = System.Drawing.Color.White;
			this.button19.Name = "button19";
			this.button19.UseVisualStyleBackColor = false;
			// 
			// button20
			// 
			this.button20.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
			resources.ApplyResources(this.button20, "button20");
			this.button20.ForeColor = System.Drawing.Color.White;
			this.button20.Name = "button20";
			this.button20.UseVisualStyleBackColor = false;
			// 
			// button21
			// 
			this.button21.BackColor = System.Drawing.Color.Gray;
			resources.ApplyResources(this.button21, "button21");
			this.button21.ForeColor = System.Drawing.Color.White;
			this.button21.Name = "button21";
			this.button21.UseVisualStyleBackColor = false;
			// 
			// button22
			// 
			this.button22.BackColor = System.Drawing.Color.Blue;
			resources.ApplyResources(this.button22, "button22");
			this.button22.ForeColor = System.Drawing.Color.White;
			this.button22.Name = "button22";
			this.button22.UseVisualStyleBackColor = false;
			// 
			// button23
			// 
			this.button23.BackColor = System.Drawing.Color.Cyan;
			resources.ApplyResources(this.button23, "button23");
			this.button23.ForeColor = System.Drawing.Color.White;
			this.button23.Name = "button23";
			this.button23.UseVisualStyleBackColor = false;
			// 
			// button24
			// 
			this.button24.BackColor = System.Drawing.Color.Lime;
			resources.ApplyResources(this.button24, "button24");
			this.button24.ForeColor = System.Drawing.Color.White;
			this.button24.Name = "button24";
			this.button24.UseVisualStyleBackColor = false;
			// 
			// button25
			// 
			this.button25.BackColor = System.Drawing.Color.Yellow;
			resources.ApplyResources(this.button25, "button25");
			this.button25.ForeColor = System.Drawing.Color.White;
			this.button25.Name = "button25";
			this.button25.UseVisualStyleBackColor = false;
			// 
			// button26
			// 
			this.button26.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(0)))));
			resources.ApplyResources(this.button26, "button26");
			this.button26.ForeColor = System.Drawing.Color.White;
			this.button26.Name = "button26";
			this.button26.UseVisualStyleBackColor = false;
			// 
			// button27
			// 
			this.button27.BackColor = System.Drawing.Color.Red;
			resources.ApplyResources(this.button27, "button27");
			this.button27.ForeColor = System.Drawing.Color.White;
			this.button27.Name = "button27";
			this.button27.UseVisualStyleBackColor = false;
			// 
			// button28
			// 
			this.button28.BackColor = System.Drawing.Color.Silver;
			resources.ApplyResources(this.button28, "button28");
			this.button28.ForeColor = System.Drawing.Color.White;
			this.button28.Name = "button28";
			this.button28.UseVisualStyleBackColor = false;
			// 
			// button5
			// 
			this.button5.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(128)))), ((int)(((byte)(255)))));
			resources.ApplyResources(this.button5, "button5");
			this.button5.ForeColor = System.Drawing.Color.White;
			this.button5.Name = "button5";
			this.button5.UseVisualStyleBackColor = false;
			// 
			// button9
			// 
			this.button9.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
			resources.ApplyResources(this.button9, "button9");
			this.button9.ForeColor = System.Drawing.Color.White;
			this.button9.Name = "button9";
			this.button9.UseVisualStyleBackColor = false;
			// 
			// button10
			// 
			this.button10.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(255)))), ((int)(((byte)(128)))));
			resources.ApplyResources(this.button10, "button10");
			this.button10.ForeColor = System.Drawing.Color.White;
			this.button10.Name = "button10";
			this.button10.UseVisualStyleBackColor = false;
			// 
			// button11
			// 
			this.button11.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(128)))));
			resources.ApplyResources(this.button11, "button11");
			this.button11.ForeColor = System.Drawing.Color.White;
			this.button11.Name = "button11";
			this.button11.UseVisualStyleBackColor = false;
			// 
			// button12
			// 
			this.button12.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(128)))));
			resources.ApplyResources(this.button12, "button12");
			this.button12.ForeColor = System.Drawing.Color.White;
			this.button12.Name = "button12";
			this.button12.UseVisualStyleBackColor = false;
			// 
			// button13
			// 
			this.button13.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(128)))));
			resources.ApplyResources(this.button13, "button13");
			this.button13.ForeColor = System.Drawing.Color.White;
			this.button13.Name = "button13";
			this.button13.UseVisualStyleBackColor = false;
			// 
			// button14
			// 
			this.button14.BackColor = System.Drawing.Color.Gainsboro;
			resources.ApplyResources(this.button14, "button14");
			this.button14.ForeColor = System.Drawing.Color.White;
			this.button14.Name = "button14";
			this.button14.UseVisualStyleBackColor = false;
			// 
			// button6
			// 
			this.button6.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(192)))), ((int)(((byte)(255)))));
			resources.ApplyResources(this.button6, "button6");
			this.button6.ForeColor = System.Drawing.Color.White;
			this.button6.Name = "button6";
			this.button6.UseVisualStyleBackColor = false;
			// 
			// button7
			// 
			this.button7.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
			resources.ApplyResources(this.button7, "button7");
			this.button7.ForeColor = System.Drawing.Color.White;
			this.button7.Name = "button7";
			this.button7.UseVisualStyleBackColor = false;
			// 
			// button8
			// 
			this.button8.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
			resources.ApplyResources(this.button8, "button8");
			this.button8.ForeColor = System.Drawing.Color.White;
			this.button8.Name = "button8";
			this.button8.UseVisualStyleBackColor = false;
			// 
			// button3
			// 
			this.button3.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
			resources.ApplyResources(this.button3, "button3");
			this.button3.ForeColor = System.Drawing.Color.White;
			this.button3.Name = "button3";
			this.button3.UseVisualStyleBackColor = false;
			// 
			// button4
			// 
			this.button4.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(224)))), ((int)(((byte)(192)))));
			resources.ApplyResources(this.button4, "button4");
			this.button4.ForeColor = System.Drawing.Color.White;
			this.button4.Name = "button4";
			this.button4.UseVisualStyleBackColor = false;
			// 
			// button2
			// 
			this.button2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(192)))));
			resources.ApplyResources(this.button2, "button2");
			this.button2.ForeColor = System.Drawing.Color.White;
			this.button2.Name = "button2";
			this.button2.UseVisualStyleBackColor = false;
			// 
			// button1
			// 
			this.button1.BackColor = System.Drawing.Color.White;
			resources.ApplyResources(this.button1, "button1");
			this.button1.ForeColor = System.Drawing.Color.LightGray;
			this.button1.Name = "button1";
			this.button1.UseVisualStyleBackColor = false;
			// 
			// labelColor
			// 
			this.labelColor.BackColor = System.Drawing.Color.Transparent;
			resources.ApplyResources(this.labelColor, "labelColor");
			this.labelColor.ForeColor = System.Drawing.Color.Black;
			this.labelColor.Name = "labelColor";
			// 
			// buttonFocus
			// 
			this.buttonFocus.BackColor = System.Drawing.Color.Transparent;
			resources.ApplyResources(this.buttonFocus, "buttonFocus");
			this.buttonFocus.ForeColor = System.Drawing.Color.Maroon;
			this.buttonFocus.Name = "buttonFocus";
			this.buttonFocus.UseVisualStyleBackColor = false;
			// 
			// textBoxImgHeight
			// 
			resources.ApplyResources(this.textBoxImgHeight, "textBoxImgHeight");
			this.textBoxImgHeight.Name = "textBoxImgHeight";
			// 
			// label3
			// 
			resources.ApplyResources(this.label3, "label3");
			this.label3.Name = "label3";
			// 
			// textBoxImgWidth
			// 
			resources.ApplyResources(this.textBoxImgWidth, "textBoxImgWidth");
			this.textBoxImgWidth.Name = "textBoxImgWidth";
			// 
			// buttonSetImgSize
			// 
			this.buttonSetImgSize.BackColor = System.Drawing.Color.CornflowerBlue;
			resources.ApplyResources(this.buttonSetImgSize, "buttonSetImgSize");
			this.buttonSetImgSize.ForeColor = System.Drawing.Color.White;
			this.buttonSetImgSize.Name = "buttonSetImgSize";
			this.buttonSetImgSize.UseVisualStyleBackColor = false;
			this.buttonSetImgSize.Click += new System.EventHandler(this.ButtonSetImgSizeClick);
			// 
			// label2
			// 
			resources.ApplyResources(this.label2, "label2");
			this.label2.Name = "label2";
			// 
			// label5
			// 
			resources.ApplyResources(this.label5, "label5");
			this.label5.Name = "label5";
			// 
			// trackBarBack
			// 
			resources.ApplyResources(this.trackBarBack, "trackBarBack");
			this.trackBarBack.Maximum = 255;
			this.trackBarBack.Name = "trackBarBack";
			this.trackBarBack.TickStyle = System.Windows.Forms.TickStyle.None;
			this.trackBarBack.Value = 255;
			this.trackBarBack.Scroll += new System.EventHandler(this.TrackBarBackScroll);
			// 
			// panel5
			// 
			this.panel5.BackColor = System.Drawing.Color.White;
			this.panel5.Controls.Add(this.label7);
			this.panel5.Controls.Add(this.buttonShape);
			this.panel5.Controls.Add(this.buttonImageSwap);
			this.panel5.Controls.Add(this.buttonTrimBack);
			this.panel5.Controls.Add(this.buttonErase);
			resources.ApplyResources(this.panel5, "panel5");
			this.panel5.Name = "panel5";
			// 
			// label7
			// 
			this.label7.BackColor = System.Drawing.Color.LightGray;
			resources.ApplyResources(this.label7, "label7");
			this.label7.Name = "label7";
			// 
			// buttonShape
			// 
			this.buttonShape.BackColor = System.Drawing.Color.WhiteSmoke;
			resources.ApplyResources(this.buttonShape, "buttonShape");
			this.buttonShape.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
			this.buttonShape.Name = "buttonShape";
			this.buttonShape.UseVisualStyleBackColor = false;
			this.buttonShape.Click += new System.EventHandler(this.ButtonToolClick);
			// 
			// buttonImageSwap
			// 
			this.buttonImageSwap.BackColor = System.Drawing.Color.WhiteSmoke;
			resources.ApplyResources(this.buttonImageSwap, "buttonImageSwap");
			this.buttonImageSwap.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
			this.buttonImageSwap.Name = "buttonImageSwap";
			this.buttonImageSwap.UseVisualStyleBackColor = false;
			this.buttonImageSwap.Click += new System.EventHandler(this.ButtonToolClick);
			// 
			// panel8
			// 
			this.panel8.BackColor = System.Drawing.Color.WhiteSmoke;
			this.panel8.Controls.Add(this.label6);
			this.panel8.Controls.Add(this.trackBarBack);
			this.panel8.Controls.Add(this.textBoxImgHeight);
			this.panel8.Controls.Add(this.buttonOpen);
			this.panel8.Controls.Add(this.label3);
			this.panel8.Controls.Add(this.buttonSave);
			this.panel8.Controls.Add(this.buttonFocus);
			this.panel8.Controls.Add(this.textBoxImgWidth);
			this.panel8.Controls.Add(this.buttonUndo);
			this.panel8.Controls.Add(this.label2);
			this.panel8.Controls.Add(this.buttonSetImgSize);
			this.panel8.Controls.Add(this.label5);
			resources.ApplyResources(this.panel8, "panel8");
			this.panel8.Name = "panel8";
			// 
			// label6
			// 
			this.label6.BackColor = System.Drawing.Color.LightGray;
			resources.ApplyResources(this.label6, "label6");
			this.label6.Name = "label6";
			// 
			// Form1
			// 
			resources.ApplyResources(this, "$this");
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.BackColor = System.Drawing.Color.DarkGray;
			this.Controls.Add(this.panel1);
			this.Controls.Add(this.panel2);
			this.Controls.Add(this.panel5);
			this.Controls.Add(this.panel8);
			this.Cursor = System.Windows.Forms.Cursors.Default;
			this.Name = "Form1";
			this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.ImageEditFormFormClosing);
			this.SizeChanged += new System.EventHandler(this.ImageEditFormSizeChanged);
			this.panel2.ResumeLayout(false);
			this.panelToolLine.ResumeLayout(false);
			this.panelToolPen.ResumeLayout(false);
			this.panelToolPen.PerformLayout();
			this.panelToolTrimBack.ResumeLayout(false);
			this.panelToolTrimBack.PerformLayout();
			this.panelToolShape.ResumeLayout(false);
			this.panelToolImageSwap.ResumeLayout(false);
			this.panelToolImageSwap.PerformLayout();
			this.panelToolColor.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.trackBarBack)).EndInit();
			this.panel5.ResumeLayout(false);
			this.panel8.ResumeLayout(false);
			this.panel8.PerformLayout();
			this.ResumeLayout(false);
		}
		private System.Windows.Forms.Label label6;
		private System.Windows.Forms.Label label7;
		private System.Windows.Forms.Button buttonGetColor;
		private System.Windows.Forms.CheckBox checkBoxAutoLine;
		private System.Windows.Forms.Panel panelToolLine;
		private System.Windows.Forms.Button buttonToolClear;
		private System.Windows.Forms.RadioButton radioButtonToolArc;
		private System.Windows.Forms.RadioButton radioButtonToolLine;
		private System.Windows.Forms.RadioButton radioButtonToolRect;
		private System.Windows.Forms.RadioButton radioButtonToolCircle;
		private System.Windows.Forms.Button buttonShape;
		private System.Windows.Forms.Panel panelToolShape;
		private System.Windows.Forms.Button buttonImageSwap;
		private System.Windows.Forms.Panel panel8;
		private System.Windows.Forms.Panel panelToolImageSwap;
		private System.Windows.Forms.Label label2;
		private System.Windows.Forms.Button buttonSetImgSize;
		private System.Windows.Forms.TextBox textBoxImgWidth;
		private System.Windows.Forms.Label label3;
		private System.Windows.Forms.TextBox textBoxImgHeight;
		private System.Windows.Forms.Button buttonSetColor;
		private System.Windows.Forms.Button button28;
		private System.Windows.Forms.Button button27;
		private System.Windows.Forms.Button button26;
		private System.Windows.Forms.Button button25;
		private System.Windows.Forms.Button button24;
		private System.Windows.Forms.Button button23;
		private System.Windows.Forms.Button button22;
		private System.Windows.Forms.Button button21;
		private System.Windows.Forms.Button button20;
		private System.Windows.Forms.Button button19;
		private System.Windows.Forms.Button button18;
		private System.Windows.Forms.Button button17;
		private System.Windows.Forms.Button button16;
		private System.Windows.Forms.Button button15;
		private System.Windows.Forms.Button button35;
		private System.Windows.Forms.Button button34;
		private System.Windows.Forms.Button button33;
		private System.Windows.Forms.Button button32;
		private System.Windows.Forms.Button button31;
		private System.Windows.Forms.Button button30;
		private System.Windows.Forms.Button button29;
		private System.Windows.Forms.Button buttonTrans;
		private System.Windows.Forms.Button button41;
		private System.Windows.Forms.Button button40;
		private System.Windows.Forms.Button button39;
		private System.Windows.Forms.Button button38;
		private System.Windows.Forms.Button button37;
		private System.Windows.Forms.Button button1;
		private System.Windows.Forms.Button button2;
		private System.Windows.Forms.Button button4;
		private System.Windows.Forms.Button button3;
		private System.Windows.Forms.Button button8;
		private System.Windows.Forms.Button button7;
		private System.Windows.Forms.Button button6;
		private System.Windows.Forms.Button button14;
		private System.Windows.Forms.Button button13;
		private System.Windows.Forms.Button button12;
		private System.Windows.Forms.Button button11;
		private System.Windows.Forms.Button button10;
		private System.Windows.Forms.Button button9;
		private System.Windows.Forms.Button button5;
		private System.Windows.Forms.Panel panelToolColor;
		private System.Windows.Forms.Panel panel5;
		private System.Windows.Forms.CheckBox checkBoxFree;
		private System.Windows.Forms.Panel panelToolTrimBack;
		private System.Windows.Forms.Panel panelToolPen;
		private System.Windows.Forms.Label labelColor;
		private System.Windows.Forms.CheckBox checkBox圆形画笔;
		private System.Windows.Forms.CheckBox checkBox消除锯齿;
		private System.Windows.Forms.TextBox textBoxEraseR;
		private System.Windows.Forms.Label label4;
		private System.Windows.Forms.Panel panel2;
		private System.Windows.Forms.Panel panel1;
		private System.Windows.Forms.Button buttonOpen;
		private System.Windows.Forms.Button buttonErase;
		private System.Windows.Forms.Button buttonTrimBack;
		private System.Windows.Forms.Label labelMessage;
		private System.Windows.Forms.Button buttonUndo;
		private System.Windows.Forms.TextBox textBox色调容差;
		private System.Windows.Forms.Button buttonSave;
		private System.Windows.Forms.Label label1;
		private System.Windows.Forms.TextBox textBoxAngle;
		private System.Windows.Forms.Button buttonRol;
		private System.Windows.Forms.Button buttonSwapY;
		private System.Windows.Forms.Button buttonSwapX;
		private System.Windows.Forms.Label label5;
		private System.Windows.Forms.TrackBar trackBarBack;
		private System.Windows.Forms.Button buttonFocus;
	}
}
