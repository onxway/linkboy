﻿
namespace n_ImageEditPanel
{
using System;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Windows.Forms;

using n_ImageEditForm;

//*****************************************************
//图形编辑器容器类
public class ImageEditPanel : Panel
{
	
	public delegate void DD_GetColor( Color t );
	public DD_GetColor D_GetColor;
	
	public delegate void D_CommonEvent( int t );
	public D_CommonEvent CommonEvent;
	const int T_RolDisable = 1;
	
	//画笔宽度
	public int PenW;
	
	//自由绘图模式(不基于画笔宽度所形成的网格绘图-虚拟像素)
	public bool FreePoint;
	
	//画笔或者橡皮擦的颜色
	public Color PenColor;
	
	//画笔形状
	public bool isRoundPen;
	
	//是否去除锯齿
	public bool HighQ;
	
	Font f;
	
	Pen DrawPen;
	SolidBrush DrawBrush;
	
	public System.Drawing.Drawing2D.HatchBrush BackBrush;
	
	Pen EageLine;
	Pen PenWLine1;
	Pen PenWLine2;
	
	bool LeftisPress;
	
	Bitmap[] UndoList;
	int UndoIndex;
	
	public Bitmap BackBitmap;
	const int Per = 10;
	
	Bitmap MaskBitmap;
	Point[] PointSet;
	int CIndex;
	int Length;
	
	Color cc;
	
	int Last_X, Last_Y;
	
	int BWidth;
	int BHeight;
	
	public int RC_H;
	
	public bool GetColor;
	
	//1: 去除背景
	//2: 橡皮擦
	//3: 绘制形状
	public int Flag;
	
	
	public enum E_ShapeType {
		Line, Rectangle, Circle, Arc
	}
	public E_ShapeType ShapeType;
	public int ShapeFlag;
	int ShapeX1;
	int ShapeY1;
	
	int StartX;
	int StartY;
	
	int RightLastX;
	int RightLastY;
	bool RightisPress;
	
	bool MidPress;
	
	int FlScale;
	int MouseX;
	int MouseY;
	
	//画笔是否自动连线
	public bool AutoLine;
	
	public bool isRol;
	public Bitmap RolTempBitmap;
	
	Brush PointBrush;
	
	Form1 Fm;
	
	public Graphics g;
	
	//构造函数
	public ImageEditPanel( Form1 fm, int Width, int Height ) : base()
	{
		Fm = fm;
		
		//CellBitmap = new Bitmap( AppDomain.CurrentDomain.BaseDirectory + "Resource\\CloseIconOn.ico" );
		
		this.BorderStyle = BorderStyle.None;
		this.BackColor = Color.Black;
		
		SetStyle(ControlStyles.UserPaint |
		         ControlStyles.AllPaintingInWmPaint |
		         ControlStyles.OptimizedDoubleBuffer |
		         ControlStyles.ResizeRedraw |
		         ControlStyles.SupportsTransparentBackColor,true);
		
		this.Dock = DockStyle.Fill;
		this.ImeMode = ImeMode.NoControl;
		
		this.Width = Width;
		this.Height = Height;
		
		Cursor Cursor1 = new Cursor( AppDomain.CurrentDomain.BaseDirectory + "Resource\\myPointEdit.cur" );
		//Cursor2 = new Cursor( OS.SystemRoot + "Resource" + OS.PATH_S + "myPoint2.cur" );
		
		this.Cursor = Cursor1;
		
		this.MouseMove += new MouseEventHandler( UserMouseMove );
		this.MouseUp += new MouseEventHandler( UserMouseUp );
		this.MouseDown += new MouseEventHandler( UserMouseDown );
		
		//BackBrush = new SolidBrush( Color.Black );
		BackBrush = new System.Drawing.Drawing2D.HatchBrush( System.Drawing.Drawing2D.HatchStyle.Percent05, Color.WhiteSmoke, Color.DarkGray );
		
		EageLine = new Pen( Color.Gray );
		EageLine.DashStyle = DashStyle.Dot;
		
		PenWLine1 = new Pen( Color.Blue );
		PenWLine1.DashStyle = DashStyle.Dot;
		
		PenWLine2 = new Pen( Color.Blue );
		PenWLine2.DashStyle = DashStyle.Dash;
		
		PenColor = Color.Black;
		PenW = 1;
		isRoundPen = true;
		RefreshPen();
		
		UndoList = new Bitmap[30];
		UndoIndex = 0;
		
		GetColor = false;
		AutoLine = true;
		
		HighQ = true;
		
		Flag = 0;
		LeftisPress = false;
		
		RightisPress = false;
		RightLastX = 0;
		RightLastY = 0;
		
		MidPress = false;
		
		FreePoint = true;
		
		StartX = 0;
		StartY = 0;
		
		FlScale = 1;
		
		//PointBrush = new HatchBrush( HatchStyle.Cross, Color.Blue, Color.Transparent );
		PointBrush = new SolidBrush( Color.FromArgb( 100, 0, 0, 255 ) );
		
		f = new Font( "宋体", 13 );
	}
	
	//设置图片
	public void SetImage( Bitmap b )
	{
		BackBitmap = b;
		BWidth = BackBitmap.Width;
		BHeight = BackBitmap.Height;
		MaskBitmap = new Bitmap( BWidth, BHeight );
		PointSet = new Point[BWidth*BHeight];
		StartX = (Width - BWidth)/2;
		StartY = (Height - BHeight)/2;
		FlScale = 1;
		
		g = Graphics.FromImage( BackBitmap );
		
		
		if( HighQ ) {
			g.SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.HighQuality;
		}
		else {
			g.SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.HighSpeed;
		}
	}
	
	//填充图片颜色
	public void ClearImage( Color c )
	{
		g.Clear( c );
	}
	
	//快速设置图片, 忽略其他参数
	public void FastSetImage( Bitmap b )
	{
		BackBitmap = b;
		BWidth = BackBitmap.Width;
		BHeight = BackBitmap.Height;
		MaskBitmap = new Bitmap( BWidth, BHeight );
		PointSet = new Point[BWidth*BHeight];
		
		g = Graphics.FromImage( BackBitmap );
	}
	
	//==========================================
	
	//触发绘图事件
	public void MyRefresh()
	{
		Invalidate();
	}
	
	//重绘事件
	protected override void OnPaint(PaintEventArgs e)
	{
		//禁用图像模糊效果
		e.Graphics.InterpolationMode = InterpolationMode.NearestNeighbor;
		e.Graphics.PixelOffsetMode = PixelOffsetMode.Half;
		
		try {
		ShowImage( e.Graphics );
		}
		catch(Exception ee) {
			MessageBox.Show( ee.ToString() );
		}
	}
	
	//显示图像
	public void ShowImage( Graphics g )
	{
		//绘制背景
		g.FillRectangle( BackBrush, 0, 0, Width, Height );
		//g.Clear( BackBrush );
		
		//绘制网格
		//DrawCross();
		
		if( BackBitmap != null ) {
			
			int AllWidth = BackBitmap.Width * FlScale;
			int AllHeight = BackBitmap.Height * FlScale;
			
			g.DrawImage( BackBitmap, StartX, StartY, AllWidth, AllHeight );
			
			int PerWidth = FlScale;
			
			/*
			int ox = StartX % (PerWidth * PenW) / PerWidth;
			int oy = StartY % (PerWidth * PenW) / PerWidth;
			if( ox < 0 ) {
				ox += PenW;
			}
			if( oy < 0 ) {
				oy += PenW;
			}
			*/
			
			for( int i = StartX, j = 0; i < StartX + AllWidth && i < Width; i += PerWidth, j++ ) {
				if( PenW != 1 && j % PenW == 0 ) {
					Pen p = null;
					if( FlScale >= 4 && PenW >= 4 ) {
						p = PenWLine1;
					}
					if( PenW >= 8 ) {
						p = PenWLine1;
					}
					if( FlScale >= 8 ) {
						p = PenWLine2;
					}
					if( p != null ) {
						g.DrawLine( p, i, StartY, i, StartY + AllHeight );
					}
				}
				else {
					if( FlScale >= 8 ) {
						g.DrawLine( EageLine, i, StartY, i, StartY + AllHeight );
					}
				}
			}
			for( int i = StartY, j = 0; i < StartY + AllHeight && i < Height; i += PerWidth, j++ ) {
				if( PenW != 1 && j % PenW == 0 ) {
					Pen p = null;
					if( FlScale >= 4 && PenW >= 4 ) {
						p = PenWLine1;
					}
					if( PenW >= 8 ) {
						p = PenWLine1;
					}
					if( FlScale >= 8 ) {
						p = PenWLine2;
					}
					if( p != null ) {
						g.DrawLine( p, StartX, i, StartX + AllWidth, i );
					}
				}
				else {
					if( FlScale >= 8 ) {
						g.DrawLine( EageLine, StartX, i, StartX + AllWidth, i );
					}
				}
			}
			
			
			/*
				for( int i = StartX % PerWidth; i < Width; i += PerWidth ) {
					for( int j = StartY % PerWidth; j < Height; j += PerWidth ) {
						g.DrawImage( CellBitmap, i, j, PerWidth, PerWidth );
					}
				}
			 */
		}
		
		//绘制图片边界
		g.DrawRectangle( Pens.OrangeRed, StartX, StartY, BackBitmap.Width * FlScale, BackBitmap.Height * FlScale );
		
		//绘制鼠标当前位置
		int ex = MouseX * FlScale;
		int ey = MouseY * FlScale;
		int t = FlScale * PenW;
		if( !FreePoint || (FreePoint && PenW == 1) ) {
			ex = ex / t * t + t/2;
			ey = ey / t * t + t/2;
		}
		else {
			if( PenW % 2 != 0 ) {
				ex = ex + FlScale/2;
				ey = ey + FlScale/2;;
			}
		}
		ex += StartX;
		ey += StartY;
		
		int ew = PenW * FlScale;
		int eh = PenW * FlScale;
		g.FillRectangle( PointBrush, ex - t/2, ey - t/2, ew, eh  );
		
		//绘制待画形状
		if( Flag == 3 ) {
			DrawShape( false, g, MouseX * FlScale + StartX, MouseY * FlScale + StartY );
		}
	}
	
	//旋转图片
	public void Rol( int angle )
	{
		int Maxw = BackBitmap.Width;
		int tw = BackBitmap.Width;
		int th = BackBitmap.Height;
		if( !isRol ) {
			if( Maxw < BackBitmap.Height ) {
				Maxw = BackBitmap.Height;
			}
			Maxw = Maxw* 15 / 10;
			
			Bitmap tb = new Bitmap( BackBitmap );
			BackBitmap = new Bitmap( Maxw, Maxw );
			Graphics tg = Graphics.FromImage( BackBitmap );
			tg.DrawImage( tb, (Maxw - tw)/2, (Maxw - th)/2 );
			RolTempBitmap = new Bitmap( BackBitmap );
			
			SetImage( BackBitmap );
		}
		Graphics g = Graphics.FromImage( BackBitmap );
		g.SmoothingMode = SmoothingMode.HighSpeed;
		g.TranslateTransform( Maxw/2, Maxw/2 );
		g.RotateTransform( angle );
		g.TranslateTransform( -Maxw/2, -Maxw/2 );
		
		g.Clear( Color.Transparent );
		g.DrawImage( RolTempBitmap, 0, 0 );
		
		isRol = true;
	}
	
	//水平翻转图片
	public void SwapX()
	{
		Bitmap b = new Bitmap( BackBitmap );
		Graphics g = Graphics.FromImage( BackBitmap );
		g.SmoothingMode = SmoothingMode.HighSpeed;
		g.Clear( Color.Transparent );
		g.DrawImage( b, b.Width, 0, -b.Width, b.Height );
	}
	
	//竖直翻转图片
	public void SwapY()
	{
		Bitmap b = new Bitmap( BackBitmap );
		Graphics g = Graphics.FromImage( BackBitmap );
		g.SmoothingMode = SmoothingMode.HighSpeed;
		g.Clear( Color.Transparent );
		g.DrawImage( b, 0, b.Height, b.Width, -b.Height );
	}
	
	//撤销一步
	public void Undo()
	{
		UndoIndex--;
		if( UndoIndex < 0 ) {
			UndoIndex = UndoList.Length - 1;
			if( UndoList[UndoIndex] == null ) {
				UndoIndex = 0;
				return;
			}
		}
		FastSetImage( UndoList[UndoIndex] );
	}
	
	//重做一步
	public void Redo()
	{
		
	}
	
	//刷新画笔
	public void RefreshPen()
	{
		DrawPen = new Pen( PenColor, PenW );
		if( isRoundPen ) {
			DrawPen.StartCap = LineCap.Round;
			DrawPen.EndCap = LineCap.Round;
		}
		else {
			DrawPen.StartCap = LineCap.Square;
			DrawPen.EndCap = LineCap.Square;
		}
		DrawBrush = new SolidBrush( PenColor );
	}
	
	//绘制背景点阵
	void DrawCross( Graphics g )
	{
		int WidthNumber = Width / Per;
		int HeightNumber = Height / Per;
		
		for( int i = 0; i <= WidthNumber; ++i ) {
			int x = i * Per;
			g.DrawLine( Pens.Red, x, 0, x, base.Height - 1 );
		}
		for( int i = 0; i <= HeightNumber; ++i ) {
			int y = i * Per;
			g.DrawLine( Pens.Red, 0, y, base.Width - 1, y );
		}
		for( int i = 0; i <= WidthNumber; ++i ) {
			int x = i * Per;
			if( i % 8 == 0 ) {
				g.DrawLine( Pens.Red, x, 0, x, base.Height - 1 );
			}
		}
		for( int i = 0; i <= HeightNumber; ++i ) {
			int y = i * Per;
			if( i % 8 == 0 ) {
				g.DrawLine( Pens.Red, 0, y, base.Width - 1, y );
			}
		}
	}
	
	//鼠标滚轮事件
	public void UserMouseWheel( int px, int py, int Delta )
	{
		if( !RightisPress ) {
			Point bp = GetBitmapLocation( px, py );
			
			if( Delta > 0 ) {
				FlScale *= 2;
			}
			else {
				if( FlScale >= 2 ) {
					FlScale /= 2;
				}
			}
			StartX = px - bp.X * FlScale;
			StartY = py - bp.Y * FlScale;
		}
		else {
			Fm.ChangeEraseSize( Delta );
		}
		MyRefresh();
	}
	
	//鼠标按下事件
	void UserMouseDown( object sender, MouseEventArgs e )
	{
		if( e.Button == MouseButtons.Left ) {
			
			/*
			if( isRol ) {
				MessageBox.Show( "请注意: 因为旋转图片会导致图片像素细节失真, 且没有必要对图片进行多次旋转, 所以接下来您无法再次对图片进行旋转操作了.", "您只有一次调整图片角度的机会, 现已用完" );
				if( CommonEvent != null ) {
					CommonEvent( T_RolDisable );
				}
				isRol = false;
			}
			*/
			LeftisPress = true;
			Point p = GetBitmapLocation( e.X, e.Y );
			int cx = p.X;
			int cy = p.Y;
			
			Last_X = cx;
			Last_Y = cy;
			
			if( cx < 0 || cx >= BWidth || cy < 0 || cy >= BHeight ) {
				return;
			}
			
			if( GetColor ) {
				GetColor = false;
				LeftisPress = false;
				if( D_GetColor != null ) {
					D_GetColor( BackBitmap.GetPixel( cx, cy ) );
				}
				return;
			}
			
			
			//记录操作序列
			UndoList[UndoIndex] = new Bitmap( BackBitmap );
			UndoIndex++;
			UndoIndex %= UndoList.Length;
			
			if( Flag == 1 ) {
				cc = BackBitmap.GetPixel( cx, cy );
				TrimBack( cx, cy );
			}
			else if( Flag == 2 ) {
				//TempBitmap = new Bitmap( BackBitmap );
				Erase( cx, cy, false );
			}
			else if( Flag == 3 ) {
				
				DrawShape( true, g, cx, cy );
				
			}
			else {
				//...
			}
		}
		if( e.Button == MouseButtons.Middle ) {
			MidPress = true;
			Point p = GetBitmapLocation( e.X, e.Y );
			int cx = p.X;
			int cy = p.Y;
			
			Last_X = cx;
			Last_Y = cy;
			
			if( cx >= BWidth || cy >= BHeight ) {
				return;
			}
			//记录操作序列
			UndoList[UndoIndex] = new Bitmap( BackBitmap );
			UndoIndex++;
			UndoIndex %= UndoList.Length;
			
			if( Flag == 2 ) {
				//TempBitmap = new Bitmap( BackBitmap );
				Erase( cx, cy, true );
			}
			else {
				//...
			}
		}
		if( e.Button == MouseButtons.Right ) {
			RightLastX = e.X;
			RightLastY = e.Y;
			RightisPress = true;
		}
		MyRefresh();
	}
	
	//鼠标松开事件
	void UserMouseUp( object sender, MouseEventArgs e )
	{
		if( e.Button == MouseButtons.Left ) {
			LeftisPress = false;
		}
		if( e.Button == MouseButtons.Middle ) {
			MidPress = false;
		}
		if( e.Button == MouseButtons.Right ) {
			RightisPress = false;
		}
		MyRefresh();
	}
	
	//鼠标移动事件
	void UserMouseMove( object sender, MouseEventArgs e )
	{
		Point p = GetBitmapLocation( e.X, e.Y );
		MouseX = p.X;
		MouseY = p.Y;
		
		if( LeftisPress ) {
			int cx = p.X;
			int cy = p.Y;
			if( Flag == 1 ) {
				//...
			}
			else if( Flag == 2 ) {
				Erase( cx, cy, false );
				Last_X = cx;
				Last_Y = cy;
			}
			else {
				//...
			}
		}
		if( MidPress ) {
			int cx = p.X;
			int cy = p.Y;
			if( Flag == 1 ) {
				//...
			}
			else if( Flag == 2 ) {
				Erase( cx, cy, true );
				Last_X = cx;
				Last_Y = cy;
			}
			else {
				//...
			}
		}
		if( RightisPress ) {
			StartX += e.X - RightLastX;
			StartY += e.Y - RightLastY;
			RightLastX = e.X;
			RightLastY = e.Y;
		}
		MyRefresh();
	}
	
	//==========================================
	
	//绘制形状
	void DrawShape( bool OK, Graphics g, int cx,int cy )
	{
		int tx, ty;
		
		if( OK ) {
			tx = ShapeX1;
			ty = ShapeY1;
		}
		else {
			tx = ShapeX1 * FlScale + StartX;
			ty = ShapeY1 * FlScale + StartY;
		}
		
		//绘制直线
		if( ShapeType == E_ShapeType.Line ) {
			if( ShapeFlag == 0 ) {
				if( OK ) {
					ShapeFlag = 1;
					ShapeX1 = cx;
					ShapeY1 = cy;
				}
			}
			else {
				if( OK ) {
					ShapeFlag = 0;
				}
				g.DrawLine( DrawPen, tx, ty, cx, cy );
			}
		}
		//绘制矩形
		else if( ShapeType == E_ShapeType.Rectangle ) {
			if( ShapeFlag == 0 ) {
				if( OK ) {
					ShapeFlag = 1;
					ShapeX1 = cx;
					ShapeY1 = cy;
				}
			}
			else {
				if( OK ) {
					ShapeFlag = 0;
				}
				int w, h;
				if( cx < tx ) {
					w = tx - cx;
					tx = cx;
				}
				else {
					w = cx - tx;
				}
				if( cy < ty ) {
					h = ty - cy;
					ty = cy;
				}
				else {
					h = cy - ty;
				}
				g.DrawRectangle( DrawPen, tx, ty, w, h );
			}
		}
		//绘制圆形
		else if( ShapeType == E_ShapeType.Circle ) {
			if( ShapeFlag == 0 ) {
				if( OK ) {
					ShapeFlag = 1;
					ShapeX1 = cx;
					ShapeY1 = cy;
				}
			}
			else {
				if( OK ) {
					ShapeFlag = 0;
				}
				g.DrawEllipse( DrawPen, tx, ty, cx - tx, cy - ty );
			}
		}
		//绘制圆弧
		else if( ShapeType == E_ShapeType.Arc ) {
			if( ShapeFlag == 0 ) {
				if( OK ) {
					ShapeFlag = 1;
					ShapeX1 = cx;
					ShapeY1 = cy;
				}
			}
			else {
				if( OK ) {
					ShapeFlag = 0;
				}
				try {
					g.DrawArc( DrawPen, tx, ty, cx - tx, cy - ty, 0, 90 );
				}
				catch {
					
				}
			}
		}
		else {
			//...
		}
	}
	
	//==========================================
	
	void TrimBack( int cx, int cy )
	{
		CIndex = 0;
		Length = 0;
		
		//必须要重新新建才能有透明颜色, 不知道为何
		//MaskBitmap.MakeTransparent();
		MaskBitmap = new Bitmap( BWidth, BHeight );
		
		//添加初始点
		MaskPoint( cx, cy );
		PointSet[Length] = new Point( cx, cy );
		Length++;
		
		do {
			int x = PointSet[CIndex].X;
			int y = PointSet[CIndex].Y;
			CIndex++;
			
			if( x - 1 >= 0 ) {
				AddPoint( x - 1, y );
			}
			if( x + 1 < BWidth ) {
				AddPoint( x + 1, y );
			}
			if( y - 1 >= 0 ) {
				AddPoint( x, y - 1 );
			}
			if( y + 1 < BHeight ) {
				AddPoint( x, y + 1 );
			}
		}
		while( CIndex != Length );
	}
	
	void AddPoint( int x, int y )
	{
		if( !isDeal( x, y ) && isSame( x, y ) ) {
			MaskPoint( x, y );
			PointSet[Length] = new Point( x, y );
			Length++;
		}
	}
	
	bool isDeal( int x, int y )
	{
		return MaskBitmap.GetPixel( x, y ).A != 0;
	}
	
	//------------------------------
	//以下两个函数需要同时调用
	
	bool isSame( int x, int y )
	{
		Color c = BackBitmap.GetPixel( x, y );
		if( c.A == 0 && cc.A != 0 || c.A != 0 && cc.A == 0 ) {
			return false;
		}
		
		
		int rc_R = Math.Abs( c.R - cc.R );
		int rc_G = Math.Abs( c.G - cc.G );
		int rc_B = Math.Abs( c.B - cc.B );
		if( rc_R <= RC_H && rc_G <= RC_H && rc_B <= RC_H ) {
			return true;
		}
		return false;
		
		/*
		rc_H = Math.Abs( c.GetHue() - cc.GetHue() );  //0 - 360
		rc_S = Math.Abs( c.GetSaturation() - cc.GetSaturation() ); //0 - 1
		rc_V = Math.Abs( c.GetBrightness() - cc.GetBrightness() ); //0 - 1
		
		if( rc_H <= RC_H && rc_S <= RC_S && rc_V <= RC_V ) {
			return true;
		}
		return false;
		*/
	}
	
	void MaskPoint( int x, int y )
	{
		//if( rc_H < RC_H && rc_S < RC_S && rc_V < RC_V ) {
			BackBitmap.SetPixel( x, y, PenColor );
		//}
		//else {
		//	Color c = BackBitmap.GetPixel( x, y );
		//	Color nc = Color.FromArgb( 255 - 255*(ERV - dv)/(ERV - RC), c.R, c.G, c.B );
		//	BackBitmap.SetPixel( x, y, nc );
		//}
		MaskBitmap.SetPixel( x, y, Color.Red );
	}
	
	void Erase( int cx, int cy, bool Er )
	{
		Brush b;
		Pen p;
		if( Er ) {
			b = Brushes.Transparent;
			p = Pens.Transparent;
		}
		else {
			b = DrawBrush;
			p = DrawPen;
		}
		if( p.Color == Color.Transparent ) {
			g.CompositingMode = CompositingMode.SourceCopy;
		}
		else {
			g.CompositingMode = CompositingMode.SourceOver;
		}
		
		if( isRoundPen ) {
			g.FillEllipse( b, cx - PenW/2 - 1, cy - PenW/2 - 1, PenW + 1, PenW + 1 );
		}
		else {
			g.FillRectangle( b, cx - PenW/2, cy - PenW/2, PenW, PenW );
		}
		if( AutoLine ) {
			g.DrawLine( p, cx, cy, Last_X, Last_Y );
		}
		
//		int sx = cx - EraseR;
//		int sy = cy - EraseR;
//		
//		for( int i = sx; i < sx + 1 + EraseR*2; ++i ) {
//			if( i < 0 || i >= BWidth ) {
//				continue;
//			}
//			for( int j = sy; j < sy + 1 + EraseR*2; ++j ) {
//				if( j < 0 || j >= BHeight ) {
//					continue;
//				}
//				BackBitmap.SetPixel( i, j, PenColor );
//			}
//		}
	}
	
	//-------------------------------------------
	//根据鼠标单击位置, 计算出对应的图片位置
	Point GetBitmapLocation( int x, int y )
	{
		x = (x - StartX) / FlScale;
		y = (y - StartY) / FlScale;
		
		int t = PenW;
		if( !FreePoint ) {
			x = x / t * t + t/2;
			y = y / t * t + t/2;
		}
		
		return new Point( x, y );
	}
}
}


