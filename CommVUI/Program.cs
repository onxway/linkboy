﻿
using System;
using n_CommVUIForm;
using System.Windows.Forms;


namespace CommVUI
{
	class Program
	{
		public static void Main(string[] args)
		{
			Application.EnableVisualStyles();
			Application.SetCompatibleTextRenderingDefault(false);
			
			CommVUIForm f = new CommVUIForm();
			Application.Run( f );
		}
	}
}


