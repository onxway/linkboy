﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Windows.Forms;
using System.IO;
using System.Threading;
using System.IO.Ports;
using n_MySerialPort;
using System.Text.RegularExpressions;
using System.Globalization;
using System.Text;

namespace n_CommVUIForm
{
	/// <summary>
	/// Description of MainForm.
	/// </summary>
	public partial class CommVUIForm : Form
	{
		public bool SingleMode = true;
		
		const byte E_MouseXY = 0x02;
		const byte E_MouseBt = 0x03;
		const byte E_MouseWl = 0x04;
		
		//串行端口
		System.IO.Ports.SerialPort port;
		//数据发送缓冲区
		byte[] write_buffer;
		
		public bool isOpen;
		
		bool isUp = false;
		
		//主窗口
		public CommVUIForm()
		{
			InitializeComponent();
			
			this.comboBox波特率.Text = "115200";
			//RefreshUARTList();
			write_buffer = new byte[ 100 ];
			
			isOpen = false;
		}
		
		//运行
		public void Run()
		{
			this.Visible = true;
		}
		
		void ErrorShow( string m )
		{
			
		}
		
		//打开端口
		void Open( string COMNumber, int Baud )
		{
			port = new System.IO.Ports.SerialPort( COMNumber );
			port.BaudRate = Baud;
			port.DataBits = 8;
			port.Parity = System.IO.Ports.Parity.None;
			port.StopBits = System.IO.Ports.StopBits.One;
			port.ReadTimeout = 1;
			
			if( checkBoxDTR_RTS.Checked ) {
				port.DtrEnable = true;
				port.RtsEnable = true;
			}
			else {
				port.DtrEnable = false;
				port.RtsEnable = false;
			}
			try {
				port.Open();
				port.DataReceived += new SerialDataReceivedEventHandler( DataReceived );
			}
			catch {
				ErrorShow( "串口打开失败, 可能被其他程序占用, 请关闭其他使用串口的软件后重试" );
				port = null;
			}
			
//			port = new Port();
//			port.PortNum = COMNumber;
//			port.BaudRate = Baud;
//			port.ByteSize = 8;
//			port.Parity = (byte)System.IO.Ports.Parity.None;
//			port.StopBits = (byte)System.IO.Ports.StopBits.One;
//			port.ReadTimeout = 1;
//			try {
//				if( port.Opened ) {
//					port.Close();
//					port.Open(); //打开串口
//				}
//				else {
//					port.Open();//打开串口
//				}
//			}
//			catch {
//				ErrorShow( "串口初始化失败" );
//			}
			
			//timer.Enabled = true;
		}
		
		//数据接收事件
		void DataReceived(object sender, EventArgs e)
		{
			/*
			try {
				int n = port.Read( buffer, 0, 10 );
				if( n == 0 ) {
					return;
				}
				for( int i = 0; i < n; ++i ) {
					byte b = buffer[ i ];
					AddByte( b );
				}
			}
			catch {
				
			}
			*/
		}
		
		void AddByte( byte b )
		{
			if( !isOpen ) {
				return;
			}
		}
		
		public void ReceiveByte( byte b )
		{
			if( !isOpen ) {
				return;
			}
		}
		
		//窗体关闭事件
		void UARTFormFormClosing(object sender, FormClosingEventArgs e)
		{
			if( !SingleMode ) {
				this.Visible = false;
				e.Cancel = true;
				return;
			}
			
			try {
				if( port != null && port.IsOpen ) {
					port.Close();
				}
			}
			catch {
				ErrorShow( "CLOSE ERROR" );
				port = null;
			}
			//this.Visible = false;
			//e.Cancel = true;
			//timer.Enabled = false;
		}
		
		public void Button打开串口Click(object sender, EventArgs e)
		{
			if( button打开串口.Text == "关闭串口" ) {
				
				
					try {
						//port.DiscardInBuffer();
						//port.DiscardOutBuffer();
						port.DataReceived -= new SerialDataReceivedEventHandler( DataReceived );
						port.Close();
						port = null;
					}
					catch {
						ErrorShow( "串口关闭失败!" );
					}
				
				isOpen = false;
				button打开串口.Text = "打开串口";
				button打开串口.BackColor = Color.LightGray;
				return;
			}
			else {
				try{
					string Name = this.comboBox串口号.Text;
					string Baud = this.comboBox波特率.Text;
					int BaudN = int.Parse( Baud );
					Open( MySerialPort.GetComName( Name ), BaudN );
				}
				catch {
					ErrorShow( "串口打开失败!" );
					return;
				}
				if( port == null ) {
					return;
				}
				isOpen = true;
				button打开串口.Text = "关闭串口";
				button打开串口.BackColor = Color.DarkSeaGreen;
				//timerModbus.Enabled = true;
			}
		}
		
		void ComboBox串口号Click(object sender, EventArgs e)
		{
			this.comboBox串口号.Items.Clear();
			
			//通过WMI获取COM端口
			string[] ss = MySerialPort.GetFullNameList();
			if( ss != null ) {
				this.comboBox串口号.Items.AddRange( ss );
			}
		}
		
		//-------------------------------------------------------------
		
		void Port_WriteK210( byte cmd, byte val0, byte val1, byte val2, byte val3 )
		{
			write_buffer[0] = 0xAA;
			write_buffer[1] = 0x07;
			write_buffer[2] = 0x00;
			write_buffer[3] = 0xE0;
			write_buffer[4] = cmd;
			write_buffer[5] = val0;
			write_buffer[6] = val1;
			write_buffer[7] = val2;
			write_buffer[8] = val3;
			write_buffer[9] = (byte)((cmd + val0 + val1 + val2 + val3) % 256);
			
			port.Write( write_buffer, 0, 10 );
		}
		
		void Panel1MouseMove(object sender, MouseEventArgs e)
		{
			if( !isOpen ) {
				ErrorShow( "请先打开串口" );
				return;
			}
			if( isUp ) {
				isUp = false;
				return;
			}
			int x = e.X;
			int y = e.Y;
			byte b0 = (byte)( x % 256 );
			byte b1 = (byte)( x / 256 );
			byte b2 = (byte)( y % 256 );
			byte b3 = (byte)( y / 256 );
			Port_WriteK210( E_MouseXY, b0, b1, b2, b3 );
		}
		
		void Panel1MouseDown(object sender, MouseEventArgs e)
		{
			if( !isOpen ) {
				ErrorShow( "请先打开串口" );
				return;
			}
			Port_WriteK210( E_MouseBt, 1, 0, 0, 0 );
		}
		
		void Panel1MouseUp(object sender, MouseEventArgs e)
		{
			if( !isOpen ) {
				ErrorShow( "请先打开串口" );
				return;
			}
			Port_WriteK210( E_MouseBt, 0, 0, 0, 0 );
			
			isUp = true;
		}
	}
}

