﻿/*
 * 由 SharpDevelop 创建。
 * 用户: bbb_2
 * 日期: 2009-11-17
 * 时间: 11:15
 * 
 * 如果你想要更改该模板，那么请使用“工具 | 选项 | 编辑 | 编辑标准标题”。
 */
namespace n_CommVUIForm
{
	partial class CommVUIForm
	{
		/// <summary>
		/// Designer variable used to keep track of non-visual components.
		/// </summary>
		private System.ComponentModel.IContainer components = null;
		
		/// <summary>
		/// Disposes resources used by the form.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing) {
				if (components != null) {
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}
		
		/// <summary>
		/// This method is required for Windows Forms designer support.
		/// Do not change the method contents inside the source code editor. The Forms designer might
		/// not be able to load this method if it was changed manually.
		/// </summary>
		private void InitializeComponent()
		{
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(CommVUIForm));
			this.panel4 = new System.Windows.Forms.Panel();
			this.comboBox波特率 = new System.Windows.Forms.ComboBox();
			this.button打开串口 = new System.Windows.Forms.Button();
			this.checkBoxDTR_RTS = new System.Windows.Forms.CheckBox();
			this.comboBox串口号 = new System.Windows.Forms.ComboBox();
			this.panel1 = new System.Windows.Forms.Panel();
			this.pictureBox1 = new System.Windows.Forms.PictureBox();
			this.label1 = new System.Windows.Forms.Label();
			this.panel4.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
			this.SuspendLayout();
			// 
			// panel4
			// 
			this.panel4.BackColor = System.Drawing.Color.WhiteSmoke;
			this.panel4.Controls.Add(this.comboBox波特率);
			this.panel4.Controls.Add(this.button打开串口);
			this.panel4.Controls.Add(this.checkBoxDTR_RTS);
			this.panel4.Controls.Add(this.comboBox串口号);
			resources.ApplyResources(this.panel4, "panel4");
			this.panel4.Name = "panel4";
			// 
			// comboBox波特率
			// 
			resources.ApplyResources(this.comboBox波特率, "comboBox波特率");
			this.comboBox波特率.FormattingEnabled = true;
			this.comboBox波特率.Items.AddRange(new object[] {
									resources.GetString("comboBox波特率.Items"),
									resources.GetString("comboBox波特率.Items1"),
									resources.GetString("comboBox波特率.Items2"),
									resources.GetString("comboBox波特率.Items3"),
									resources.GetString("comboBox波特率.Items4"),
									resources.GetString("comboBox波特率.Items5"),
									resources.GetString("comboBox波特率.Items6"),
									resources.GetString("comboBox波特率.Items7"),
									resources.GetString("comboBox波特率.Items8")});
			this.comboBox波特率.Name = "comboBox波特率";
			// 
			// button打开串口
			// 
			this.button打开串口.BackColor = System.Drawing.Color.LightGray;
			resources.ApplyResources(this.button打开串口, "button打开串口");
			this.button打开串口.ForeColor = System.Drawing.Color.Black;
			this.button打开串口.Name = "button打开串口";
			this.button打开串口.UseVisualStyleBackColor = false;
			this.button打开串口.Click += new System.EventHandler(this.Button打开串口Click);
			// 
			// checkBoxDTR_RTS
			// 
			resources.ApplyResources(this.checkBoxDTR_RTS, "checkBoxDTR_RTS");
			this.checkBoxDTR_RTS.ForeColor = System.Drawing.Color.Black;
			this.checkBoxDTR_RTS.Name = "checkBoxDTR_RTS";
			this.checkBoxDTR_RTS.UseVisualStyleBackColor = true;
			// 
			// comboBox串口号
			// 
			this.comboBox串口号.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
			resources.ApplyResources(this.comboBox串口号, "comboBox串口号");
			this.comboBox串口号.FormattingEnabled = true;
			this.comboBox串口号.Name = "comboBox串口号";
			this.comboBox串口号.Click += new System.EventHandler(this.ComboBox串口号Click);
			// 
			// panel1
			// 
			this.panel1.BackColor = System.Drawing.Color.White;
			this.panel1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
			resources.ApplyResources(this.panel1, "panel1");
			this.panel1.Name = "panel1";
			this.panel1.MouseDown += new System.Windows.Forms.MouseEventHandler(this.Panel1MouseDown);
			this.panel1.MouseMove += new System.Windows.Forms.MouseEventHandler(this.Panel1MouseMove);
			this.panel1.MouseUp += new System.Windows.Forms.MouseEventHandler(this.Panel1MouseUp);
			// 
			// pictureBox1
			// 
			resources.ApplyResources(this.pictureBox1, "pictureBox1");
			this.pictureBox1.Name = "pictureBox1";
			this.pictureBox1.TabStop = false;
			// 
			// label1
			// 
			resources.ApplyResources(this.label1, "label1");
			this.label1.Name = "label1";
			// 
			// CommVUIForm
			// 
			resources.ApplyResources(this, "$this");
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.BackColor = System.Drawing.Color.WhiteSmoke;
			this.Controls.Add(this.label1);
			this.Controls.Add(this.pictureBox1);
			this.Controls.Add(this.panel1);
			this.Controls.Add(this.panel4);
			this.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
			this.Name = "CommVUIForm";
			this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.UARTFormFormClosing);
			this.panel4.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
			this.ResumeLayout(false);
		}
		private System.Windows.Forms.Label label1;
		private System.Windows.Forms.PictureBox pictureBox1;
		private System.Windows.Forms.Panel panel4;
		private System.Windows.Forms.CheckBox checkBoxDTR_RTS;
		private System.Windows.Forms.Panel panel1;
		private System.Windows.Forms.ComboBox comboBox波特率;
		private System.Windows.Forms.ComboBox comboBox串口号;
		private System.Windows.Forms.Button button打开串口;
	}
}
