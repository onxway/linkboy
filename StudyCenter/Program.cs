﻿/*
 * 由SharpDevelop创建。
 * 用户： dell
 * 日期: 2016/9/21
 * 时间: 15:34
 * 
 * 要改变这种模板请点击 工具|选项|代码编写|编辑标准头文件
 */
using System;
using System.Windows.Forms;
using n_StartForm;
using n_OS;
using n_DescriptionForm;
using n_ExampleForm;
using n_PPTForm;

namespace n_App
{
	/// <summary>
	/// Class with program entry point.
	/// </summary>
	internal sealed class Program
	{
		public static DescriptionForm DescriptionBox;
		public static ExampleForm ExampleBox;
		
		public static PPTForm PPTBox;
		
		/// <summary>
		/// Program entry point.
		/// </summary>
		[STAThread]
		private static void Main(string[] args)
		{
			Application.EnableVisualStyles();
			Application.SetCompatibleTextRenderingDefault(false);
			
			string DefaultFilePath = null;
			
			
			
			//+++++++++++++++++++++++++++++++++++++++++++++
			//判断是否加载文件
			if( ( args != null ) && ( args.Length > 0 ) ) {
				string filePath = "";
				int si = 0;
				for( int i = si; i < args.Length; ++i ) {
					filePath += " " + args[ i ];
				}
				DefaultFilePath = filePath.Trim( ' ' );
			}
			
			if( DefaultFilePath != "wq" ) {
				//MessageBox.Show( "系统工具，用户无需使用" );
				//return;
			}
			
			//初始化底层移植库
			OS.Init();
			
			try{
			n_GUIset.GUIset.Init();
			//初始化版本转换器
			n_GUIcoder.VersionSwitch.Init();
			
			DescriptionBox = new DescriptionForm();
			ExampleBox = new ExampleForm();
			
			PPTBox = new PPTForm();
			
			MainForm mf = new MainForm();
			
			Application.Run( mf );
			}catch(Exception e) {
				MessageBox.Show( e.ToString() );
			}
		}
		
	}
}


