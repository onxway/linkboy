﻿/*
 * 由SharpDevelop创建。
 * 用户： dell
 * 日期: 2017/12/1
 * 时间: 16:31
 * 
 * 要改变这种模板请点击 工具|选项|代码编写|编辑标准头文件
 */
using System;
using System.ComponentModel;
using System.Drawing;
using System.Windows.Forms;

namespace n_GroupControl
{
	/// <summary>
	/// Description of GroupControl.
	/// </summary>
	public partial class GroupControl : UserControl
	{
		public Panel owner;
		
		public GroupControl( Panel o )
		{
			//
			// The InitializeComponent() call is required for Windows Forms designer support.
			//
			InitializeComponent();
			
			owner = o;
			
			
			foreach( Control c in this.Controls ) {
				c.Click += new EventHandler( MyClick );
				//c.MouseEnter += new EventHandler( MyMouseEnter );
			}
			this.Click += new EventHandler( MyClick );
		}
		
		//设置名字
		public void SetName( string Name )
		{
			labelName.Text = Name;
			labelName.Text = Name;
			if( Name == "结束" ) {
				labelName.Visible = false;
			}
		}
		
		//获取名字
		public string GetName()
		{
			return labelName.Text;
		}
		
		void MyClick(object sender, EventArgs e)
		{
			owner.Select();
		}
	}
}


