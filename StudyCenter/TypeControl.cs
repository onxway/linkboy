﻿/*
 * 由SharpDevelop创建。
 * 用户： dell
 * 日期: 2018/2/9
 * 时间: 16:26
 * 
 * 要改变这种模板请点击 工具|选项|代码编写|编辑标准头文件
 */
using System;
using System.ComponentModel;
using System.Drawing;
using System.Windows.Forms;

namespace n_TypeControl
{
	/// <summary>
	/// Description of TypeControl.
	/// </summary>
	public partial class TypeControl : UserControl
	{
		public Panel owner;
		
		public TypeControl( Panel o )
		{
			//
			// The InitializeComponent() call is required for Windows Forms designer support.
			//
			InitializeComponent();
			
			owner = o;
			
			SearchControl( this );
		}
		
		//搜索一个控件
		void SearchControl( Control sc )
		{
			if( sc is UserControl || sc is Panel || sc is PictureBox || sc is GroupBox ||sc is Label || sc is ListBox || sc is Button ) {
				sc.MouseDown += new MouseEventHandler( ItemMouseDown );
				sc.MouseEnter += new EventHandler( ItemMouseEnter );
				
				foreach( Control c in sc.Controls ) {
					SearchControl( c );
				}
			}
		}
		
		void ItemMouseEnter(object sender, EventArgs e)
		{
			n_App.MainForm.mf.MouseOnTypeObj = this;
		}
		
		void ItemMouseDown(object sender, MouseEventArgs e)
		{
			owner.Select();
			n_App.MainForm.mf.SelectTypeObj = this;
		}
		
		//==========================================================================================
		
		//设置名字
		public void SetName( string Name )
		{
			label1.Text = Name;
		}
		
		//设置图片
		public void SetImage( Bitmap bmp )
		{
			//pictureBox1.BackgroundImage = bmp;
		}
		
		//设置为鼠标悬停状态
		public void SetMouseOn()
		{
			if( n_App.MainForm.mf.SelectTypeObj != this ) {
				this.BackColor = Color.LightSkyBlue;
			}
		}
		
		//取消鼠标悬停状态
		public void CancelMouseOn()
		{
			if( n_App.MainForm.mf.SelectTypeObj != this ) {
				this.BackColor = Color.Transparent;
			}
		}
		
		//设置为选中状态
		public void SetSelect()
		{
			this.BackColor = Color.SteelBlue;
		}
		
		//取消选中状态
		public void CancelSelect()
		{
			this.BackColor = Color.Transparent;
		}
	}
}



