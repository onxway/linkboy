﻿
using System;
using System.Drawing;
using System.Windows.Forms;


namespace n_TechPanel
{
public class TechPanel : Panel
{
	public int StartY;
	
	public static Font extendFont;
	public static Font extendHeadFont;
	
	bool LeftPress;
	//bool RightPress;
	
	int LastY;
	
	public int GWidth;
	
	Image[] ImageList;
	
	//构造函数
	public TechPanel() : base()
	{
		this.BorderStyle = BorderStyle.None;
		this.BackColor = Color.Black;
		
		SetStyle(ControlStyles.UserPaint |
		         ControlStyles.AllPaintingInWmPaint |
		         ControlStyles.OptimizedDoubleBuffer |
		         ControlStyles.ResizeRedraw |
		         ControlStyles.SupportsTransparentBackColor,true);
		
		this.Dock = DockStyle.Fill;
		this.ImeMode = ImeMode.NoControl;
		
		this.MouseMove += new MouseEventHandler( UserMouseMove );
		this.MouseUp += new MouseEventHandler( UserMouseUp );
		this.MouseDown += new MouseEventHandler( UserMouseDown );
		this.MouseWheel += new MouseEventHandler( MyMouseWheel );
		
		extendFont = new Font( "宋体", 11 );
		extendHeadFont = new Font( "宋体", 12, FontStyle.Bold );
		
		StartY = 0;
	}
	
	//重绘事件
	protected override void OnPaint(PaintEventArgs e)
	{
		//文字起点为下方 20, 宽度间距为 20
		Graphics g = e.Graphics;
		
		g.TranslateTransform( 0, StartY );
		
		//填充背景
		g.Clear( Color.WhiteSmoke );
		
		int CurrentHeight = 20;
		int MesWidth = GWidth - 20 - 20;
		
		//绘制模块图片
		if( ImageList != null ) {
			for( int i = 0; i < ImageList.Length; ++i ) {
				if( ImageList[i] == null ) {
					break;
				}
				int nw = ImageList[i].Width;
				int nh = ImageList[i].Height;
				if( nw > this.Width - 40 ) {
					nw = this.Width - 40;
					nh = ImageList[i].Height * nw / ImageList[i].Width;
				}
				g.DrawImage( ImageList[i], (this.Width - nw)/2, CurrentHeight, nw, nh );
				
				CurrentHeight += nh;
				
				CurrentHeight += 15;
				
				g.FillRectangle( Brushes.SlateGray, 0, CurrentHeight, Width, 5 );
				
				CurrentHeight += 15;
			}
		}
	}
	
	//设置图片列表
	public void SetImageList( Image[] tImageList)
	{
		ImageList = tImageList;
	}
	
	void MyMouseWheel( object sender, MouseEventArgs e)
	{
		if( e.Delta > 0 ) {
			StartY += 50;
		}
		else {
			StartY -= 50;
		}
		this.Invalidate();
	}
	
	//鼠标按下事件
	void UserMouseDown( object sender, MouseEventArgs e )
	{
		if( e.Button == MouseButtons.Left ) {
			LastY = e.Y;
			LeftPress = true;
		}
		if( e.Button == MouseButtons.Right ) {
			//RightPress = true;
		}
		this.Invalidate();
		
		this.Focus();
	}
	
	//鼠标松开事件
	void UserMouseUp( object sender, MouseEventArgs e )
	{
		if( e.Button == MouseButtons.Left ) {
			LeftPress = false;
		}
		if( e.Button == MouseButtons.Right ) {
			//RightPress = false;
		}
		this.Invalidate();
	}
	
	//鼠标移动事件
	void UserMouseMove( object sender, MouseEventArgs e )
	{
		if( LeftPress ) {
			StartY += e.Y - LastY;
			LastY = e.Y;
			this.Invalidate();
		}
	}
}
}


