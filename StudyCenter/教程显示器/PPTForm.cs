﻿
namespace n_PPTForm
{
using System;
using System.Drawing;
using System.Windows.Forms;
using System.IO;
using System.Collections;

using n_TechPanel;

/// <summary>
/// Description of WorkForm.
/// </summary>
public partial class PPTForm : Form
{
	TechPanel GPanel;
	
	const int GWidth = 900;
	const int MesWidth = GWidth - 20 - 20;
	
	SolidBrush backb;
	
	//主窗口
	public PPTForm()
	{
		InitializeComponent();
		
		//添加图形编程界面
		GPanel = new TechPanel();
		
		GPanel.Location = new Point( 0, 0 );
		GPanel.Visible = true;
		this.panel2.Controls.Add( GPanel );
		
		backb = new SolidBrush( Color.WhiteSmoke );
	}
	
	//运行
	public void Run( string Name, string Folder )
	{
		this.Text = "easyPPT阅读器 - " + Name;
		
		Image[] ImageList = new Image[40];
		int INumber = 0;
		
		DirectoryInfo Dir = new DirectoryInfo( Folder );
		
		FileInfo[] lstFile = Dir.GetFiles();
		Array.Sort(lstFile, new FileSort(FileOrder.Name));
		
		//遍历当前文件夹中的所有示例文件
		foreach (FileInfo file in lstFile) {
			if( file.FullName.EndsWith( ".jpg" ) ) {
				ImageList[INumber] = new Bitmap( file.FullName );
				++INumber;
			}
		}
		
		GPanel.SetImageList( ImageList );
		GPanel.StartY = 0;
		
		//TextHeight = SearchHeight( MM, MesWidth );
		GPanel.GWidth = this.Width;
		
		GPanel.Invalidate();
		this.Visible = true;
	}
	
	//窗体关闭事件
	void UnitLibFormFormClosing(object sender, FormClosingEventArgs e)
	{
		e.Cancel = true;
		this.Visible = false;
	}
	
	void DescriptionFormSizeChanged(object sender, EventArgs e)
	{
		if( GPanel != null ) {
			GPanel.GWidth = this.Width;
			GPanel.Invalidate();
		}
	}
	
	/// <summary>
	/// 文件排序类
	/// </summary>
	public class FileSort : IComparer
	{
		private FileOrder _fileorder;
		private FileAsc _fileasc;
		
		/// <summary>
		/// 构造函数
		/// </summary>
		public FileSort()
			: this(FileOrder.Name, FileAsc.Asc)
		{ }
		
		/// <summary>
		/// 构造函数
		/// </summary>
		/// <param name="fileorder"></param>
		public FileSort(FileOrder fileorder)
			: this(fileorder, FileAsc.Asc)
		{ }
		
		/// <summary>
		/// 构造函数
		/// </summary>
		/// <param name="fileorder"></param>
		/// <param name="fileasc"></param>
		public FileSort(FileOrder fileorder, FileAsc fileasc)
		{
			_fileorder = fileorder;
			_fileasc = fileasc;
		}
		
		/// <summary>
		/// 比较函数
		/// </summary>
		/// <param name="x"></param>
		/// <param name="y"></param>
		/// <returns></returns>
		public int Compare(object x, object y)
		{
			FileInfo file1 = x as FileInfo;
			FileInfo file2 = y as FileInfo;
			FileInfo file3;
			
			if (file1 == null || file2 == null)
				throw new ArgumentException("参数不是FileInfo类实例.");
			
			if (_fileasc == FileAsc.Desc)
			{
				file3 = file1;
				file1 = file2;
				file2 = file3;
			}
			
			if( file1.Name.Length < file2.Name.Length ) return -1;
			if( file1.Name.Length > file2.Name.Length ) return 1;
			
			switch (_fileorder)
			{
				case FileOrder.Name:
					return file1.Name.CompareTo(file2.Name);
				case FileOrder.Length:
					return file1.Length.CompareTo(file2.Length);
				case FileOrder.Extension:
					return file1.Extension.CompareTo(file2.Extension);
				case FileOrder.CreationTime:
					return file1.CreationTime.CompareTo(file2.CreationTime);
				case FileOrder.LastAccessTime:
					return file1.LastAccessTime.CompareTo(file2.LastAccessTime);
				case FileOrder.LastWriteTime:
					return file1.LastWriteTime.CompareTo(file2.LastWriteTime);
				default:
					return 0;
			}
		}
	}
	
	/// <summary>
	/// 排序依据
	/// </summary>
	public enum FileOrder
	{
		/// <summary>
		/// 文件名
		/// </summary>
		Name,
		/// <summary>
		/// 大小
		/// </summary>
		Length,
		/// <summary>
		/// 类型
		/// </summary>
		Extension,
		/// <summary>
		///         /// 创建时间
		/// </summary>
		CreationTime,
		/// <summary>
		/// 访问时间
		/// </summary>
		LastAccessTime,
		/// <summary>
		/// 修改时间
		/// </summary>
		LastWriteTime
	}
	
	/// <summary>
	/// 升序降序
	/// </summary>
	public enum FileAsc
	{
		/// <summary>
		/// 升序
		/// </summary>
		Asc,
		/// <summary>
		/// 降序
		/// </summary>
		Desc
	}
}
}



