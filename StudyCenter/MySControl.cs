﻿/*
 * 由SharpDevelop创建。
 * 用户： cap_gpu
 * 日期: 2017/11/27
 * 时间: 0:19
 * 
 * 要改变这种模板请点击 工具|选项|代码编写|编辑标准头文件
 */
using System;
using System.ComponentModel;
using System.Drawing;
using System.Windows.Forms;
using n_MyFileObject;
using System.Diagnostics;
using n_OS;
using n_GUIcoder;
using n_MainSystemData;

namespace n_MyControl
{
	/// <summary>
	/// Description of UserControl1.
	/// </summary>
	public partial class MySControl : UserControl
	{
		public Panel owner;
		
		public string MyName;
		public string MyType;
		public string GroupName;
		public string ExamFlag;
		public string ExamDir;
		
		public enum WType {
			Word, Pdf, PPT, Dir,
		};
		public WType WordType;
		
		
		public MySControl( Panel o, WType wt )
		{
			//
			// The InitializeComponent() call is required for Windows Forms designer support.
			//
			InitializeComponent();
			
			owner = o;
			WordType = wt;
			ExamDir = null;
			
			switch( WordType ) {
				case WType.Word:	buttonStart.Text += " Word"; break;
				case WType.Pdf:	buttonStart.Text += " Pdf"; break;
				case WType.PPT:		buttonStart.Text += " PPT"; break;
				case WType.Dir:		buttonStart.Text += " 文件夹"; buttonStart.Width = 181; break;
				default:			MessageBox.Show( "未知类型" ); break;
			}
			
			foreach( Control c in this.panel1.Controls ) {
				
				if( c is Button ) {
					continue;
				}
				
				c.MouseDown += new MouseEventHandler( MyDown );
				c.MouseEnter += new EventHandler( MyMouseEnter );
				
				//c.MouseDown += new MouseEventHandler( MyMouseDown );
				//c.MouseMove += new MouseEventHandler( MyMouseMove );
				//c.MouseUp += new MouseEventHandler( MyMouseUp );
			}
			this.MouseDown += new MouseEventHandler( MyDown );
			this.panel1.MouseEnter += new EventHandler( MyMouseEnter );
			
			this.panel1.MouseDown += new MouseEventHandler( MyDown );
			this.panel1.MouseEnter += new EventHandler( MyMouseEnter );
			
			//this.MouseDown += new MouseEventHandler( MyMouseDown );
			//this.MouseMove += new MouseEventHandler( MyMouseMove );
			//this.MouseUp += new MouseEventHandler( MyMouseUp );
		}
		
		//设置名字
		public void SetName( string Name )
		{
			MyName = Name;
			label1.Text = Name;
		}
		
		//设置示例
		public void SetExample( string examFlag )
		{
			ExamFlag = examFlag;
		}
		
		//设置模块说明
		public void SetMes( string Mes )
		{
			label2.Text = Mes;
		}
		
		//设置模块说明
		public void SetExDir( string Mes )
		{
			ExamDir = Mes;
		}
		
		//设置所在的组
		public void SetGroup( string GName )
		{
			GroupName = GName;
		}
		
		//设置没有示例
		public void SetNoExam()
		{
			this.buttonExample.Enabled = false;
			this.buttonExample.BackColor = Color.LightGray;
		}
		
		//设置为鼠标悬停状态
		public void SetMouseOn()
		{
			//if( n_App.MainForm.mf.SelectObj != this ) {
				this.BackColor = Color.LightSkyBlue;
				//this.Location = new Point( this.Location.X, this.Location.Y - 1 );
			//}
		}
		
		//取消鼠标悬停状态
		public void CancelMouseOn()
		{
			//if( n_App.MainForm.mf.SelectObj != this ) {
				this.BackColor = Color.Gainsboro;
				//this.Location = new Point( this.Location.X, this.Location.Y + 1 );
			//}
		}
		
		//设置为选中状态
		public void SetSelect()
		{
			//this.BackColor = Color.SteelBlue;
		}
		
		//取消选中状态
		public void CancelSelect()
		{
			//this.BackColor = Color.Gainsboro;
		}
		
		//-------------------------------------------------------------------
		
		void MyMouseDown(object sender, MouseEventArgs e)
		{
			//n_App.MainForm.mf.MyMouseDown( null, e );
		}
		
		void MyMouseMove(object sender, MouseEventArgs e)
		{
			//n_App.MainForm.mf.MyMouseMove( null, e );
		}
		
		void MyMouseUp(object sender, MouseEventArgs e)
		{
			//n_App.MainForm.mf.MyMouseUp( null, e );
		}
		
		void MyMouseEnter(object sender, EventArgs e)
		{
			n_App.MainForm.mf.MouseOnObj = this;
		}
		
		void MyDown(object sender, MouseEventArgs e)
		{
			owner.Select();
			n_App.MainForm.mf.SelectObj = this;
		}
		
		void ButtonStartClick(object sender, EventArgs e)
		{
			if( WordType == WType.Word ) {
				Process Proc = new Process();
				Proc.StartInfo.WindowStyle = ProcessWindowStyle.Normal;
				//Proc.StartInfo.UseShellExecute = false;
				//Proc.StartInfo.CreateNoWindow = true;
				//Proc.StartInfo.RedirectStandardOutput = true;
				
				Proc.StartInfo.WorkingDirectory = AppDomain.CurrentDomain.BaseDirectory;
				Proc.StartInfo.FileName = "study\\" + GroupName + "\\"+ MyName + ".doc";
				
				try {
				Proc.Start();
				}
				catch {
					MessageBox.Show( "无法打开教材文件，您需要先在电脑上安装任意一款能编辑查看word的软件，并确保双击.doc文件能打开: " + Proc.StartInfo.FileName );
				}
			}
			else if( WordType == WType.Pdf ) {
				Process Proc = new Process();
				Proc.StartInfo.WindowStyle = ProcessWindowStyle.Normal;
				//Proc.StartInfo.UseShellExecute = false;
				//Proc.StartInfo.CreateNoWindow = true;
				//Proc.StartInfo.RedirectStandardOutput = true;
				
				Proc.StartInfo.WorkingDirectory = AppDomain.CurrentDomain.BaseDirectory;
				Proc.StartInfo.FileName = "study\\" + GroupName + "\\"+ MyName + ".pdf";
				
				try {
				Proc.Start();
				}
				catch {
					MessageBox.Show( "无法打开教材文件，您需要先在电脑上安装任意一款能编辑查看pdf的软件，并确保双击.pdf文件能打开: " + Proc.StartInfo.FileName );
				}
			}
			else if( WordType == WType.PPT ) {
				n_App.Program.PPTBox.Run( MyName, OS.SystemRoot + "study\\" + GroupName + "\\"+ MyName + "\\" );
			}
			else if( WordType == WType.Dir ) {
				string fileToSelect = OS.SystemRoot + "study\\" + GroupName + "\\"+ MyName;
				string args = string.Format("/Select, {0}", fileToSelect);
				//System.Diagnostics.Process.Start( new System.Diagnostics.ProcessStartInfo( "Explorer.exe" ) );
				System.Diagnostics.Process.Start( fileToSelect );
			}
			else {
				MessageBox.Show( "未知课程类型: " + WordType );
			}
		}
		
		void ButtonExampleClick(object sender, EventArgs e)
		{
			if( ExamDir != null ) {
				string fileToSelect = OS.SystemRoot + ExamDir;
				string args = string.Format("/Select, {0}", fileToSelect);
				//System.Diagnostics.Process.Start( new System.Diagnostics.ProcessStartInfo( "Explorer.exe" ) );
				System.Diagnostics.Process.Start( fileToSelect );
			}
			else {
				n_App.Program.ExampleBox.Run( ExamFlag );
			}
		}
	}
}


