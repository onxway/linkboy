﻿
namespace n_StartForm
{
using System;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Windows.Forms;
using c_BufferPanel_old;
using n_OS;

/// <summary>
/// Description of WorkForm.
/// </summary>
public partial class StartForm : Form
{
	BufferPanel GPanel;
	Font fText;
	Bitmap back;
	
	Pen BackPen1;
	Pen BackPen2;
	Pen BackPen3;
	Pen ForePenA1;
	Pen ForePenA2;
	Pen ForePenA3;
	Pen ForePenB1;
	Pen ForePenB2;
	Pen ForePenB3;
	Pen ForePenC1;
	Pen ForePenC2;
	Pen ForePenC3;
	
	string FullMes;
	int CurrentIndex;
	public int FullIndex;
	float Per;
	
	const int LineXX = 25;
	
	public bool NotLinkboy;
	
	Brush logoBrush;
	
	//主窗口
	public StartForm()
	{
		InitializeComponent();
		/*
		if( OS.DefaultUser != null ) {
			back = new Bitmap( OS.SystemPrePath + OS.DefaultUser + OS.PATH_S + "start.png" );
		}
		else {
			back = new Bitmap( OS.SystemRoot + "Resource" + OS.PATH_S + "StartBox" + OS.PATH_S + OS.USER + ".png" );
		}
		*/
		//back = new Bitmap( OS.SystemRoot + "sercenter" + OS.PATH_S + "StartBox.png" );
		back = new Bitmap( this.BackgroundImage );
		
		this.Size = new Size( back.Width, back.Height );
		this.MaximumSize = this.Size;
		this.MinimumSize = this.Size;
		
		GPanel = new BufferPanel( back.Width, back.Height );
		GPanel.Click += new EventHandler( StartFormClick );
		this.Controls.Add( GPanel );
		
		CurrentIndex = 0;
		FullIndex = 0;
		
		Color mBackColor = Color.WhiteSmoke;
		Color mForeColor = Color.LimeGreen;
		
		fText = new Font( "微软雅黑", 10 );
		BackPen1 = new Pen( mBackColor, 1 );
		BackPen1.StartCap = LineCap.Round;
		BackPen1.EndCap = LineCap.Round;
		BackPen2 = new Pen( mBackColor, 3 );
		BackPen2.StartCap = LineCap.Round;
		BackPen2.EndCap = LineCap.Round;
		BackPen3 = new Pen( mBackColor, 5 );
		BackPen3.StartCap = LineCap.Round;
		BackPen3.EndCap = LineCap.Round;
		
		ForePenC1 = new Pen( mForeColor, 1 );
		ForePenC1.StartCap = LineCap.Round;
		ForePenC1.EndCap = LineCap.Round;
		ForePenC2 = new Pen( mForeColor, 3 );
		ForePenC2.StartCap = LineCap.Round;
		ForePenC2.EndCap = LineCap.Round;
		ForePenC3 = new Pen( mForeColor, 5 );
		ForePenC3.StartCap = LineCap.Round;
		ForePenC3.EndCap = LineCap.Round;
		
		ForePenA1 = new Pen( Color.White, 1 );
		ForePenA1.StartCap = LineCap.Round;
		ForePenA1.EndCap = LineCap.Round;
		ForePenA2 = new Pen( Color.Red, 3 );
		ForePenA2.StartCap = LineCap.Round;
		ForePenA2.EndCap = LineCap.Round;
		ForePenA3 = new Pen( Color.DarkRed, 5 );
		ForePenA3.StartCap = LineCap.Round;
		ForePenA3.EndCap = LineCap.Round;
		
		ForePenB1 = new Pen( Color.White, 1 );
		ForePenB1.StartCap = LineCap.Round;
		ForePenB1.EndCap = LineCap.Round;
		ForePenB2 = new Pen( Color.Blue, 3 );
		ForePenB2.StartCap = LineCap.Round;
		ForePenB2.EndCap = LineCap.Round;
		ForePenB3 = new Pen( Color.DarkBlue, 5 );
		ForePenB3.StartCap = LineCap.Round;
		ForePenB3.EndCap = LineCap.Round;
		
		logoBrush = new SolidBrush( Color.FromArgb( 150, 40, 130, 180 ) );
	}
	
	//运行
	public void Run()
	{
		this.Visible = true;
		GPanel.Invalidate();
		this.Refresh();
	}
	
	//设置全信息
	public void SetFullMes( string vFullMes, int N )
	{
		FullMes = vFullMes;
		FullIndex = N;
		
		Per = (float)(this.Width - LineXX * 2) / N;
		
		AddMessage( null );
	}
	
	//添加信息
	public void AddMessage( string m )
	{
		int ShowHeight = 49;
		
		int LineY = this.Height - 14;
		
		++CurrentIndex;
		
		//绘制背景
		//GPanel.g.FillRectangle( Brushes.Gray, 0, 0, GPanel.BufferImageWidth, GPanel.BufferImageHeight );
		GPanel.g.DrawImage( back, 0, 0, Width, Height );
		
		GPanel.g.DrawString( FullMes, fText, Brushes.White, LineXX, this.Height - ShowHeight );
		
		//绘制进度条
		GPanel.g.DrawLine( BackPen3, LineXX, LineY, LineXX + (int)(FullIndex * Per), LineY );
		//GPanel.g.DrawLine( BackPen2, LineXX, LineY, LineXX + FullIndex * Per, LineY );
		//GPanel.g.DrawLine( BackPen1, LineXX, LineY, LineXX + FullIndex * Per, LineY );
		
		//GPanel.g.DrawLine( ForePenC3, LineXX, LineY, LineXX + CurrentIndex * Per, LineY );
		GPanel.g.DrawLine( ForePenC2, LineXX, LineY, LineXX + CurrentIndex * Per, LineY );
		//GPanel.g.DrawLine( ForePenC1, LineXX, LineY, LineXX + CurrentIndex * Per, LineY );
		
		//刷新显示
		//GPanel.Invalidate();
		this.Refresh();
	}
	
	void StartFormClick(object sender, EventArgs e)
	{
		this.Visible = false;
	}
}
}




