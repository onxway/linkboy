﻿/*
 * 由SharpDevelop创建。
 * 用户： dell
 * 日期: 2017/12/1
 * 时间: 16:31
 * 
 * 要改变这种模板请点击 工具|选项|代码编写|编辑标准头文件
 */
namespace n_GroupControl
{
	partial class GroupControl
	{
		/// <summary>
		/// Designer variable used to keep track of non-visual components.
		/// </summary>
		private System.ComponentModel.IContainer components = null;
		
		/// <summary>
		/// Disposes resources used by the control.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing) {
				if (components != null) {
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}
		
		/// <summary>
		/// This method is required for Windows Forms designer support.
		/// Do not change the method contents inside the source code editor. The Forms designer might
		/// not be able to load this method if it was changed manually.
		/// </summary>
		private void InitializeComponent()
		{
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(GroupControl));
			this.labelName = new System.Windows.Forms.Label();
			this.label2 = new System.Windows.Forms.Label();
			this.SuspendLayout();
			// 
			// labelName
			// 
			this.labelName.BackColor = System.Drawing.Color.Transparent;
			this.labelName.Dock = System.Windows.Forms.DockStyle.Left;
			this.labelName.Font = new System.Drawing.Font("微软雅黑", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
			this.labelName.ForeColor = System.Drawing.Color.Gray;
			this.labelName.Image = ((System.Drawing.Image)(resources.GetObject("labelName.Image")));
			this.labelName.Location = new System.Drawing.Point(0, 0);
			this.labelName.Name = "labelName";
			this.labelName.Size = new System.Drawing.Size(257, 28);
			this.labelName.TabIndex = 0;
			this.labelName.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
			// 
			// label2
			// 
			this.label2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(210)))), ((int)(((byte)(210)))), ((int)(((byte)(210)))));
			this.label2.Dock = System.Windows.Forms.DockStyle.Bottom;
			this.label2.ForeColor = System.Drawing.Color.Tomato;
			this.label2.Location = new System.Drawing.Point(0, 28);
			this.label2.Name = "label2";
			this.label2.Size = new System.Drawing.Size(700, 5);
			this.label2.TabIndex = 1;
			// 
			// GroupControl
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.BackColor = System.Drawing.Color.WhiteSmoke;
			this.Controls.Add(this.labelName);
			this.Controls.Add(this.label2);
			this.Name = "GroupControl";
			this.Size = new System.Drawing.Size(700, 33);
			this.ResumeLayout(false);
		}
		private System.Windows.Forms.Label label2;
		private System.Windows.Forms.Label labelName;
	}
}
