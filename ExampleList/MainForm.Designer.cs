﻿/*
 * 由SharpDevelop创建。
 * 用户： dell
 * 日期: 2016/9/21
 * 时间: 15:34
 * 
 * 要改变这种模板请点击 工具|选项|代码编写|编辑标准头文件
 */
namespace n_App
{
	partial class MainForm
	{
		/// <summary>
		/// Designer variable used to keep track of non-visual components.
		/// </summary>
		private System.ComponentModel.IContainer components = null;
		
		/// <summary>
		/// Disposes resources used by the form.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing) {
				if (components != null) {
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}
		
		/// <summary>
		/// This method is required for Windows Forms designer support.
		/// Do not change the method contents inside the source code editor. The Forms designer might
		/// not be able to load this method if it was changed manually.
		/// </summary>
		private void InitializeComponent()
		{
			this.buttonStart = new System.Windows.Forms.Button();
			this.richTextBox1 = new System.Windows.Forms.RichTextBox();
			this.buttonCheck = new System.Windows.Forms.Button();
			this.panel1 = new System.Windows.Forms.Panel();
			this.progressBar1 = new System.Windows.Forms.ProgressBar();
			this.buttonOpenFile = new System.Windows.Forms.Button();
			this.richTextBoxExt = new System.Windows.Forms.RichTextBox();
			this.labelMes = new System.Windows.Forms.Label();
			this.panel1.SuspendLayout();
			this.SuspendLayout();
			// 
			// buttonStart
			// 
			this.buttonStart.BackColor = System.Drawing.Color.NavajoWhite;
			this.buttonStart.Font = new System.Drawing.Font("微软雅黑", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
			this.buttonStart.Location = new System.Drawing.Point(12, 12);
			this.buttonStart.Name = "buttonStart";
			this.buttonStart.Size = new System.Drawing.Size(98, 35);
			this.buttonStart.TabIndex = 2;
			this.buttonStart.Text = "收集引用";
			this.buttonStart.UseVisualStyleBackColor = false;
			this.buttonStart.Click += new System.EventHandler(this.ButtonStartClick);
			// 
			// richTextBox1
			// 
			this.richTextBox1.BorderStyle = System.Windows.Forms.BorderStyle.None;
			this.richTextBox1.Dock = System.Windows.Forms.DockStyle.Fill;
			this.richTextBox1.Location = new System.Drawing.Point(0, 113);
			this.richTextBox1.Name = "richTextBox1";
			this.richTextBox1.Size = new System.Drawing.Size(1081, 526);
			this.richTextBox1.TabIndex = 5;
			this.richTextBox1.Text = "";
			// 
			// buttonCheck
			// 
			this.buttonCheck.BackColor = System.Drawing.Color.Gainsboro;
			this.buttonCheck.Font = new System.Drawing.Font("微软雅黑", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
			this.buttonCheck.Location = new System.Drawing.Point(116, 12);
			this.buttonCheck.Name = "buttonCheck";
			this.buttonCheck.Size = new System.Drawing.Size(98, 35);
			this.buttonCheck.TabIndex = 6;
			this.buttonCheck.Text = "检查过时项";
			this.buttonCheck.UseVisualStyleBackColor = false;
			this.buttonCheck.Click += new System.EventHandler(this.ButtonCheckClick);
			// 
			// panel1
			// 
			this.panel1.BackColor = System.Drawing.Color.WhiteSmoke;
			this.panel1.Controls.Add(this.progressBar1);
			this.panel1.Controls.Add(this.buttonOpenFile);
			this.panel1.Controls.Add(this.richTextBoxExt);
			this.panel1.Controls.Add(this.labelMes);
			this.panel1.Controls.Add(this.buttonCheck);
			this.panel1.Controls.Add(this.buttonStart);
			this.panel1.Dock = System.Windows.Forms.DockStyle.Top;
			this.panel1.Location = new System.Drawing.Point(0, 0);
			this.panel1.Name = "panel1";
			this.panel1.Size = new System.Drawing.Size(1081, 113);
			this.panel1.TabIndex = 7;
			// 
			// progressBar1
			// 
			this.progressBar1.Location = new System.Drawing.Point(324, 12);
			this.progressBar1.Name = "progressBar1";
			this.progressBar1.Size = new System.Drawing.Size(359, 35);
			this.progressBar1.TabIndex = 13;
			// 
			// buttonOpenFile
			// 
			this.buttonOpenFile.BackColor = System.Drawing.Color.Gainsboro;
			this.buttonOpenFile.Font = new System.Drawing.Font("微软雅黑", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
			this.buttonOpenFile.Location = new System.Drawing.Point(220, 12);
			this.buttonOpenFile.Name = "buttonOpenFile";
			this.buttonOpenFile.Size = new System.Drawing.Size(98, 35);
			this.buttonOpenFile.TabIndex = 12;
			this.buttonOpenFile.Text = "打开文件";
			this.buttonOpenFile.UseVisualStyleBackColor = false;
			this.buttonOpenFile.Click += new System.EventHandler(this.ButtonOpenFileClick);
			// 
			// richTextBoxExt
			// 
			this.richTextBoxExt.BackColor = System.Drawing.SystemColors.Window;
			this.richTextBoxExt.BorderStyle = System.Windows.Forms.BorderStyle.None;
			this.richTextBoxExt.Location = new System.Drawing.Point(692, 12);
			this.richTextBoxExt.Name = "richTextBoxExt";
			this.richTextBoxExt.Size = new System.Drawing.Size(380, 89);
			this.richTextBoxExt.TabIndex = 11;
			this.richTextBoxExt.Text = "";
			// 
			// labelMes
			// 
			this.labelMes.BackColor = System.Drawing.Color.White;
			this.labelMes.Font = new System.Drawing.Font("微软雅黑", 10.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
			this.labelMes.Location = new System.Drawing.Point(12, 60);
			this.labelMes.Name = "labelMes";
			this.labelMes.Size = new System.Drawing.Size(671, 41);
			this.labelMes.TabIndex = 7;
			this.labelMes.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// MainForm
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(1081, 639);
			this.Controls.Add(this.richTextBox1);
			this.Controls.Add(this.panel1);
			this.Name = "MainForm";
			this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
			this.Text = "文件夹遍历器";
			this.panel1.ResumeLayout(false);
			this.ResumeLayout(false);
		}
		private System.Windows.Forms.ProgressBar progressBar1;
		private System.Windows.Forms.Button buttonOpenFile;
		private System.Windows.Forms.RichTextBox richTextBoxExt;
		private System.Windows.Forms.Label labelMes;
		private System.Windows.Forms.Panel panel1;
		private System.Windows.Forms.Button buttonCheck;
		private System.Windows.Forms.RichTextBox richTextBox1;
		private System.Windows.Forms.Button buttonStart;
	}
}
