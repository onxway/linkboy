﻿
using System;
using System.Text;
using System.Collections.Generic;
using System.Drawing;
using System.Windows.Forms;
using System.IO;
using n_OS;
using System.Diagnostics;

namespace n_App
{
public partial class MainForm : Form
{
	string VersionText;
	string FolderText;
	string LibFileText;
	
	//构造函数
	public MainForm()
	{
		//
		// The InitializeComponent() call is required for Windows Forms designer support.
		//
		InitializeComponent();
		
		n_OS.OS.Init();
		
		Refer.Init();
		Check.Init();
		
		VersionText = n_OS.OS.SystemRoot + @"Lib\versionswitch\2017.10.8.txt";
		FolderText = n_OS.OS.SystemRoot + @"example\";
		LibFileText = n_OS.OS.SystemRoot + @"Lib\linkboy.txt";
		
		progressBar1.Maximum = Refer.MAX;
	}
	
	//设置进度条
	public void SetProcee( int i )
	{
		if( i > progressBar1.Maximum ) {
			i = progressBar1.Maximum;
		}
		progressBar1.Value = i;
	}
	
	//收集引用按钮按下时
	void ButtonStartClick(object sender, EventArgs e)
	{
		Refer.OpenVersion( VersionText );
		
		Refer.Start( LibFileText, FolderText );
		
		MessageBox.Show( "完成, 一共处理了 " + Refer.FileNum + " 个文件" );
		
		string NullFile = null;
		this.richTextBox1.Text = Refer.GetList( ref NullFile );
		this.richTextBoxExt.Text = NullFile;
	}
	
	//检查过时项目
	void ButtonCheckClick(object sender, EventArgs e)
	{
		string r = Check.Start( LibFileText, FolderText );
		
		string[] data = r.TrimEnd( '\n' ).Split( '\n' );
		
		for (int i = 0; i < data.GetLength(0); i++) {
			for (int j = 1; j < data.GetLength(0) - i; j++) {
				if (data[j].CompareTo(data[j - 1]) < 0) {
					string t = data[j];
					data[j] = data[j - 1];
					data[j - 1] = t;
				}
			}
		}
		this.richTextBox1.Text = String.Join( "\n", data );
		
		labelMes.Text = Check.Mes;
		
		MessageBox.Show( "完成" );
	}
	
	//打开过时文件
	void ButtonOpenFileClick(object sender, EventArgs e)
	{
		int i = this.richTextBox1.GetLineFromCharIndex( this.richTextBox1.SelectionStart );
		string line = this.richTextBox1.Lines[i];
		line = line.Remove( 0, line.IndexOf( ' ' ) + 1 ).Trim( ' ' );
		if( !File.Exists( line ) ) {
			MessageBox.Show( "文件不存在: " + line );
			return;
		}
		Process Proc = new Process();
		Proc.StartInfo.WindowStyle = ProcessWindowStyle.Normal;
		//Proc.StartInfo.UseShellExecute = false;
		//Proc.StartInfo.CreateNoWindow = true;
		//Proc.StartInfo.RedirectStandardOutput = true;
		
		//Proc.StartInfo.WorkingDirectory = AppDomain.CurrentDomain.BaseDirectory;
		Proc.StartInfo.FileName = line;
		Proc.Start();
	}
	
	//-----------------------------------------------------------------------
	static class Refer
	{
		static string[][] LibVersion;
		
		static int Index;
		
		static string[] LibList;
		
		static string[][] FileList;
		
		static string PathRoot;
		
		//目前文件数量 2022.9.10
		public const int MAX = 1656;
		public static int FileNum;
		
		//初始化
		public static void Init()
		{
			FileList = new string[3000][];
			for( int i = 0; i < FileList.Length; ++i ) {
				FileList[i] = new string[500];
			}
		}
		
		//打开版本转换文件
		public static void OpenVersion( string FileName )
		{
			//读取配置文件
			string LibConfig = VIO.OpenTextFileGB2312( FileName );
			
			string[] ss = LibConfig.Trim( '\n' ).Split( '\n' );
			LibVersion = new string[ss.Length][];
			for( int i = 0; i < LibVersion.Length; ++i ) {
				LibVersion[i] = ss[i].Split( ',' );
			}
		}
		
		//开始
		public static void Start( string LFile, string Path )
		{
			string text = VIO.OpenTextFileGB2312( LFile );
			
			LibList = text.Split( '\n' );
			for( int i = 0; i < LibList.Length; ++i ) {
				LibList[ i ] = LibList[ i ].Trim( " \t\r".ToCharArray() );
				string LineData = LibList[i];
				
				
				
				if( LineData.StartsWith( "//" ) ) {
					LibList[ i ] = "";
					continue;
				}
				//如果是目录结束,返回
				if( LineData.StartsWith( "</fold>" ) ) {
					LibList[ i ] = "";
					continue;
				}
				//如果是新目录,则创建节点并递归调用自身
				if( LineData.StartsWith( "<fold>" ) ) {
					LibList[ i ] = "";
					continue;
				}
				//到这里说明是条目项
				if( LineData.StartsWith( "<item>" ) ) {
					LibList[ i ] = "";
					continue;
				}
				if( LineData.StartsWith( "</item>" ) ) {
					LibList[ i ] = "";
					continue;
				}
				if( LineData == "<nextcolumn>" ) {
					LibList[ i ] = "";
					continue;
				}
				if( LineData == "<<other>>" ) {
					LibList[ i ] = "";
					continue;
				}
				if( LineData == "" ) {
					continue;
				}
				
				
				string sPath = LineData.Remove( 0, 1 + LineData.IndexOf( ' ' ) );
				
				if( sPath.EndsWith( "SS:N," ) || sPath.EndsWith( "SS:O," ) ) {
					sPath = sPath.Remove( sPath.Length - 5 );
				}
				if( !sPath.EndsWith( "," ) ) {
					MessageBox.Show( "路径项缺少结尾逗号: " + sPath + "]" );
					continue;
				}
				sPath = sPath.Remove( sPath.Length - 1 );
				
				if( sPath.EndsWith( ",90" ) ) {
					sPath = sPath.Remove( sPath.Length - 3 );
				}
				if( sPath.EndsWith( ")" ) ) {
					int lp = sPath.LastIndexOf( "(" );
					sPath = sPath.Remove( lp );
				}
				
				LibList[i] = sPath;//.ToLower();
			}
			//Result = "";
			Index = 0;
			PathRoot = Path;
			FileNum = 0;
			
			A_FindFile( Path );
			//return Result;
		}
		
		//计算各个库对应的示例文件路径列表
		public static string GetList( ref string NullFile )
		{
			StringBuilder ss = new StringBuilder( "" );
			NullFile = "";
			
			for( int i = 0; i < LibList.Length; ++i ) {
				if( LibList[i] == "" ) {
					continue;
				}
				ss.Append( LibList[i] + "\n" );
				bool exist = false;
				for( int j = 0; j < FileList.Length; ++j ) {
					if( FileList[j] == null ) {
						continue;
					}
					for( int n = 1; n < FileList[j].Length; ++n ) {
						if( FileList[j][n] == LibList[i] ) {
							ss.Append( "\t" + FileList[j][0] + "\n" );
							exist = true;
							break;
						}
					}
				}
				if( !exist ) {
					NullFile += LibList[i] + "\n";
				}
			}
			
			return ss.ToString();
		}
		
		//遍历指定的文件夹中的全部文件
		static void A_FindFile(string sSourcePath )
		{
			//在指定目录及子目录下查找文件,在list中列出子目录及文件
			DirectoryInfo Dir = new DirectoryInfo( sSourcePath );
			DirectoryInfo[] DirSub = Dir.GetDirectories();
			
			//遍历当前文件夹中的所有文件
			foreach( FileInfo f in Dir.GetFiles("*.*", SearchOption.TopDirectoryOnly) ) {
				
				string FilePath = Dir + @"\" + f.ToString();
				
				string tText = VIO.OpenTextFileGB2312( FilePath );//.ToLower();
				StringBuilder myText = new StringBuilder( tText );
				
				
				//版本转换
				if( tText.IndexOf( "育松电子" ) != -1 ) {
					myText = myText.Replace( "育松电子", "group1" );
					MessageBox.Show( "发现旧版育松电子模块:" + FilePath );
				}
				for( int i = 0; i < LibVersion.Length; ++i ) {
					if( tText.IndexOf( LibVersion[i][0] ) != -1 ) {
						//MessageBox.Show( "发现旧版模块: " + LibVersion[i][0] );
						myText = myText.Replace( LibVersion[i][0], LibVersion[i][1] );
					}
				}
				
				
				//记录当前的文件路径
				FileList[Index][0] = FilePath.Remove( 0, PathRoot.Length );
				
				
				//Result += FileList[Index][0] + "\n";
				
				int xx = 0;
				
				for( int i = 0; i < LibList.Length; ++i ) {
					if( LibList[i] == "" ) {
						continue;
					}
					//这里要加空格, 防止个别出现相同子路径情况, 如光敏电阻
					if( tText.IndexOf( " " + LibList[i] ) != -1 ) {
						//Result += "\t" + LibList[i] + "\n";
						++xx;
						
						FileList[Index][xx] = LibList[i];
					}
				}
				Index++;
				FileNum++;
				
				//if( FileNum % 10 == 0 ) {
					Program.mf.Text = "已处理 " + FileNum + " / " + MAX;
					Program.mf.SetProcee( FileNum );
					System.Windows.Forms.Application.DoEvents();
				//}
			}
			//遍历所有的子文件夹
			foreach( DirectoryInfo d in DirSub ) {
				A_FindFile( Dir + @"\" + d.ToString() );
			}
		}
	}
	//-----------------------------------------------------------------------
	static class Check
	{
		static string Result;
		public static string Mes;
		
		static int FileNumber;
		static int ResFileNumber;
		
		//初始化
		public static void Init()
		{
			//...
		}
		
		//开始
		public static string Start( string LFile, string Path )
		{
			string text = VIO.OpenTextFileGB2312( LFile );
			
			Result = "";
			FileNumber = 0;
			ResFileNumber = 0;
			
			A_FindFile( text, Path );
			
			Mes = "文件总数:" + FileNumber + ",  结果总数:" + ResFileNumber;
			
			return Result;
		}
		
		//遍历指定的文件夹中的全部文件
		static void A_FindFile( string USEList, string sSourcePath )
		{
			//在指定目录及子目录下查找文件,在list中列出子目录及文件
			DirectoryInfo Dir = new DirectoryInfo( sSourcePath );
			DirectoryInfo[] DirSub = Dir.GetDirectories();
			
			//遍历当前文件夹中的所有文件
			foreach( FileInfo f in Dir.GetFiles("*.*", SearchOption.TopDirectoryOnly) ) {
				
				string FilePath = Dir + f.ToString();
				string Text = VIO.OpenTextFileGB2312( FilePath );//.ToLower();
				
				string[] cut = Text.Split( '\n' );
				
				FileNumber++;
				for( int i = 0; i < cut.Length; ++i ) {
					if( cut[i].StartsWith( "//[组件路径]" ) ) {
						string[] cc = cut[i].Split( ' ' );
						string path = cc[2];
						if( path.IndexOf( "第三方模块库" ) != -1 ) {
							continue;
						}
						path = path.Remove( path.Length - 1 );
						
						if( USEList.IndexOf( path ) == -1 ) {
							Result += path + "    " + FilePath + "\n";
							ResFileNumber++;
						}
					}
				}
			}
			//遍历所有的子文件夹
			foreach( DirectoryInfo d in DirSub ) {
				A_FindFile( USEList, Dir + d.ToString() + @"\" );
			}
		}
	}
	//-----------------------------------------------------------------------
	static class Rename
	{
		static bool Exist;
		
		//开始
		public static void Start( string LFile, string Path )
		{
			Exist = false;
			B_FindFile( Path );
		}
		
		//遍历指定的文件夹中的全部文件
		static void B_FindFile(string sSourcePath )
		{
			if( Exist ) {
				return;
			}
			
			//在指定目录及子目录下查找文件,在list中列出子目录及文件
			DirectoryInfo Dir = new DirectoryInfo( sSourcePath );
			DirectoryInfo[] DirSub = Dir.GetDirectories();
			
			//遍历当前文件夹中的所有文件
			foreach( FileInfo f in Dir.GetFiles("*.*", SearchOption.TopDirectoryOnly) ) {
				if( Exist ) {
					return;
				}
				string FilePath = Dir + @"\" + f.ToString();
				B_DealFile( FilePath );
			}
			//遍历所有的子文件夹
			foreach( DirectoryInfo d in DirSub ) {
				if( Exist ) {
					return;
				}
				
				//防护对话框
				DialogResult dr = MessageBox.Show( "需要处理新文件夹吗?\n" + d, "文件夹处理", MessageBoxButtons.YesNo );
				if( dr == DialogResult.No ) {
					Exist = true;
					return;
				}
				
				B_FindFile(Dir + @"\" + d.ToString());
			}
		}
		
		//处理文件
		static void B_DealFile( string FileName )
		{
			if( !FileName.EndsWith( ".txt" ) ) {
				return;
			}
			
			
			/*
		DialogResult dr = MessageBox.Show( "需要处理此文件吗?\n" + FileName, "文件处理", MessageBoxButtons.YesNoCancel );
		if( dr == DialogResult.Cancel ) {
			Exist = true;
			return;
		}
		else if( dr == DialogResult.No ) {
			return;
		}
			 */
			
			
			string NewName = FileName.Remove( FileName.Length - 3 ) + "lab";
			//MessageBox.Show( "<" + NewName + ">" );
			File.Move( FileName, NewName );
		}
	}
}
}



