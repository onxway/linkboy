﻿/*
 * 由 SharpDevelop 创建。
 * 用户: bbb_2
 * 日期: 2009-11-17
 * 时间: 11:15
 * 
 * 如果你想要更改该模板，那么请使用“工具 | 选项 | 编辑 | 编辑标准标题”。
 */
namespace n_VOS_ConfigForm
{
	partial class VOS_ConfigForm
	{
		/// <summary>
		/// Designer variable used to keep track of non-visual components.
		/// </summary>
		private System.ComponentModel.IContainer components = null;
		
		/// <summary>
		/// Disposes resources used by the form.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing) {
				if (components != null) {
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}
		
		/// <summary>
		/// This method is required for Windows Forms designer support.
		/// Do not change the method contents inside the source code editor. The Forms designer might
		/// not be able to load this method if it was changed manually.
		/// </summary>
		private void InitializeComponent()
		{
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(VOS_ConfigForm));
			this.label10 = new System.Windows.Forms.Label();
			this.panel4 = new System.Windows.Forms.Panel();
			this.label14 = new System.Windows.Forms.Label();
			this.label13 = new System.Windows.Forms.Label();
			this.checkBox_US_Tick = new System.Windows.Forms.CheckBox();
			this.checkBoxTickSystem = new System.Windows.Forms.CheckBox();
			this.buttonNDK = new System.Windows.Forms.Button();
			this.label8 = new System.Windows.Forms.Label();
			this.richTextBoxNDK = new System.Windows.Forms.RichTextBox();
			this.panel3 = new System.Windows.Forms.Panel();
			this.checkBoxExtDrv = new System.Windows.Forms.CheckBox();
			this.panel2 = new System.Windows.Forms.Panel();
			this.label12 = new System.Windows.Forms.Label();
			this.textBoxDefaultUART = new System.Windows.Forms.TextBox();
			this.label9 = new System.Windows.Forms.Label();
			this.label11 = new System.Windows.Forms.Label();
			this.checkBoxIN_ROM = new System.Windows.Forms.CheckBox();
			this.radioButtonRUN_FLASH = new System.Windows.Forms.RadioButton();
			this.radioButtonRUN_CODE = new System.Windows.Forms.RadioButton();
			this.panelCommon = new System.Windows.Forms.Panel();
			this.label5 = new System.Windows.Forms.Label();
			this.label6 = new System.Windows.Forms.Label();
			this.textBoxROM = new System.Windows.Forms.TextBox();
			this.label7 = new System.Windows.Forms.Label();
			this.label4 = new System.Windows.Forms.Label();
			this.label3 = new System.Windows.Forms.Label();
			this.textBoxRAM = new System.Windows.Forms.TextBox();
			this.label2 = new System.Windows.Forms.Label();
			this.panel1 = new System.Windows.Forms.Panel();
			this.buttonCheck = new System.Windows.Forms.Button();
			this.comboBoxCHIP = new System.Windows.Forms.ComboBox();
			this.label1 = new System.Windows.Forms.Label();
			this.buttonVersion = new System.Windows.Forms.Button();
			this.button打开 = new System.Windows.Forms.Button();
			this.buttonInstallVos = new System.Windows.Forms.Button();
			this.button保存 = new System.Windows.Forms.Button();
			this.panelMain = new System.Windows.Forms.Panel();
			this.panel5 = new System.Windows.Forms.Panel();
			this.textBox_VOS_Ver = new System.Windows.Forms.TextBox();
			this.label15 = new System.Windows.Forms.Label();
			this.panel4.SuspendLayout();
			this.panel3.SuspendLayout();
			this.panel2.SuspendLayout();
			this.panelCommon.SuspendLayout();
			this.panel1.SuspendLayout();
			this.panelMain.SuspendLayout();
			this.panel5.SuspendLayout();
			this.SuspendLayout();
			// 
			// label10
			// 
			resources.ApplyResources(this.label10, "label10");
			this.label10.ForeColor = System.Drawing.Color.LightSlateGray;
			this.label10.Name = "label10";
			// 
			// panel4
			// 
			this.panel4.Controls.Add(this.label14);
			this.panel4.Controls.Add(this.label13);
			this.panel4.Controls.Add(this.checkBox_US_Tick);
			this.panel4.Controls.Add(this.checkBoxTickSystem);
			resources.ApplyResources(this.panel4, "panel4");
			this.panel4.Name = "panel4";
			// 
			// label14
			// 
			resources.ApplyResources(this.label14, "label14");
			this.label14.ForeColor = System.Drawing.Color.Gray;
			this.label14.Name = "label14";
			// 
			// label13
			// 
			resources.ApplyResources(this.label13, "label13");
			this.label13.ForeColor = System.Drawing.Color.Gray;
			this.label13.Name = "label13";
			// 
			// checkBox_US_Tick
			// 
			resources.ApplyResources(this.checkBox_US_Tick, "checkBox_US_Tick");
			this.checkBox_US_Tick.Name = "checkBox_US_Tick";
			this.checkBox_US_Tick.UseVisualStyleBackColor = true;
			this.checkBox_US_Tick.CheckedChanged += new System.EventHandler(this.CheckBox_US_TickCheckedChanged);
			// 
			// checkBoxTickSystem
			// 
			resources.ApplyResources(this.checkBoxTickSystem, "checkBoxTickSystem");
			this.checkBoxTickSystem.Name = "checkBoxTickSystem";
			this.checkBoxTickSystem.UseVisualStyleBackColor = true;
			this.checkBoxTickSystem.CheckedChanged += new System.EventHandler(this.CheckBoxTickSystemCheckedChanged);
			// 
			// buttonNDK
			// 
			this.buttonNDK.BackColor = System.Drawing.Color.LightSteelBlue;
			resources.ApplyResources(this.buttonNDK, "buttonNDK");
			this.buttonNDK.ForeColor = System.Drawing.Color.Black;
			this.buttonNDK.Name = "buttonNDK";
			this.buttonNDK.UseVisualStyleBackColor = false;
			this.buttonNDK.Click += new System.EventHandler(this.ButtonNDKClick);
			// 
			// label8
			// 
			resources.ApplyResources(this.label8, "label8");
			this.label8.Name = "label8";
			// 
			// richTextBoxNDK
			// 
			this.richTextBoxNDK.BorderStyle = System.Windows.Forms.BorderStyle.None;
			resources.ApplyResources(this.richTextBoxNDK, "richTextBoxNDK");
			this.richTextBoxNDK.Name = "richTextBoxNDK";
			this.richTextBoxNDK.TextChanged += new System.EventHandler(this.RichTextBoxNDKTextChanged);
			// 
			// panel3
			// 
			this.panel3.Controls.Add(this.checkBoxExtDrv);
			resources.ApplyResources(this.panel3, "panel3");
			this.panel3.Name = "panel3";
			// 
			// checkBoxExtDrv
			// 
			resources.ApplyResources(this.checkBoxExtDrv, "checkBoxExtDrv");
			this.checkBoxExtDrv.Name = "checkBoxExtDrv";
			this.checkBoxExtDrv.UseVisualStyleBackColor = true;
			this.checkBoxExtDrv.CheckedChanged += new System.EventHandler(this.CheckBoxExtDrvCheckedChanged);
			// 
			// panel2
			// 
			this.panel2.Controls.Add(this.label12);
			this.panel2.Controls.Add(this.textBoxDefaultUART);
			this.panel2.Controls.Add(this.label9);
			this.panel2.Controls.Add(this.label11);
			this.panel2.Controls.Add(this.checkBoxIN_ROM);
			this.panel2.Controls.Add(this.radioButtonRUN_FLASH);
			this.panel2.Controls.Add(this.radioButtonRUN_CODE);
			resources.ApplyResources(this.panel2, "panel2");
			this.panel2.Name = "panel2";
			// 
			// label12
			// 
			resources.ApplyResources(this.label12, "label12");
			this.label12.ForeColor = System.Drawing.Color.Gray;
			this.label12.Name = "label12";
			// 
			// textBoxDefaultUART
			// 
			resources.ApplyResources(this.textBoxDefaultUART, "textBoxDefaultUART");
			this.textBoxDefaultUART.Name = "textBoxDefaultUART";
			this.textBoxDefaultUART.TextChanged += new System.EventHandler(this.TextBoxDefaultUARTTextChanged);
			// 
			// label9
			// 
			resources.ApplyResources(this.label9, "label9");
			this.label9.ForeColor = System.Drawing.Color.Gray;
			this.label9.Name = "label9";
			// 
			// label11
			// 
			resources.ApplyResources(this.label11, "label11");
			this.label11.Name = "label11";
			// 
			// checkBoxIN_ROM
			// 
			resources.ApplyResources(this.checkBoxIN_ROM, "checkBoxIN_ROM");
			this.checkBoxIN_ROM.Name = "checkBoxIN_ROM";
			this.checkBoxIN_ROM.UseVisualStyleBackColor = true;
			this.checkBoxIN_ROM.CheckedChanged += new System.EventHandler(this.CheckBoxIN_ROMCheckedChanged);
			// 
			// radioButtonRUN_FLASH
			// 
			resources.ApplyResources(this.radioButtonRUN_FLASH, "radioButtonRUN_FLASH");
			this.radioButtonRUN_FLASH.Name = "radioButtonRUN_FLASH";
			this.radioButtonRUN_FLASH.UseVisualStyleBackColor = true;
			// 
			// radioButtonRUN_CODE
			// 
			this.radioButtonRUN_CODE.Checked = true;
			resources.ApplyResources(this.radioButtonRUN_CODE, "radioButtonRUN_CODE");
			this.radioButtonRUN_CODE.Name = "radioButtonRUN_CODE";
			this.radioButtonRUN_CODE.TabStop = true;
			this.radioButtonRUN_CODE.UseVisualStyleBackColor = true;
			this.radioButtonRUN_CODE.CheckedChanged += new System.EventHandler(this.RadioButtonRUN_CODECheckedChanged);
			// 
			// panelCommon
			// 
			this.panelCommon.Controls.Add(this.label5);
			this.panelCommon.Controls.Add(this.label6);
			this.panelCommon.Controls.Add(this.textBoxROM);
			this.panelCommon.Controls.Add(this.label7);
			this.panelCommon.Controls.Add(this.label4);
			this.panelCommon.Controls.Add(this.label3);
			this.panelCommon.Controls.Add(this.textBoxRAM);
			this.panelCommon.Controls.Add(this.label2);
			resources.ApplyResources(this.panelCommon, "panelCommon");
			this.panelCommon.Name = "panelCommon";
			// 
			// label5
			// 
			resources.ApplyResources(this.label5, "label5");
			this.label5.ForeColor = System.Drawing.Color.DodgerBlue;
			this.label5.Name = "label5";
			// 
			// label6
			// 
			resources.ApplyResources(this.label6, "label6");
			this.label6.Name = "label6";
			// 
			// textBoxROM
			// 
			resources.ApplyResources(this.textBoxROM, "textBoxROM");
			this.textBoxROM.Name = "textBoxROM";
			this.textBoxROM.TextChanged += new System.EventHandler(this.TextBoxROMTextChanged);
			// 
			// label7
			// 
			resources.ApplyResources(this.label7, "label7");
			this.label7.Name = "label7";
			// 
			// label4
			// 
			resources.ApplyResources(this.label4, "label4");
			this.label4.ForeColor = System.Drawing.Color.DodgerBlue;
			this.label4.Name = "label4";
			// 
			// label3
			// 
			resources.ApplyResources(this.label3, "label3");
			this.label3.Name = "label3";
			// 
			// textBoxRAM
			// 
			resources.ApplyResources(this.textBoxRAM, "textBoxRAM");
			this.textBoxRAM.Name = "textBoxRAM";
			this.textBoxRAM.TextChanged += new System.EventHandler(this.TextBoxRAMTextChanged);
			// 
			// label2
			// 
			resources.ApplyResources(this.label2, "label2");
			this.label2.Name = "label2";
			// 
			// panel1
			// 
			this.panel1.BackColor = System.Drawing.Color.Gainsboro;
			this.panel1.Controls.Add(this.buttonCheck);
			this.panel1.Controls.Add(this.comboBoxCHIP);
			this.panel1.Controls.Add(this.label1);
			this.panel1.Controls.Add(this.buttonVersion);
			this.panel1.Controls.Add(this.button打开);
			this.panel1.Controls.Add(this.buttonInstallVos);
			this.panel1.Controls.Add(this.button保存);
			resources.ApplyResources(this.panel1, "panel1");
			this.panel1.Name = "panel1";
			// 
			// buttonCheck
			// 
			this.buttonCheck.BackColor = System.Drawing.Color.LightGray;
			resources.ApplyResources(this.buttonCheck, "buttonCheck");
			this.buttonCheck.ForeColor = System.Drawing.Color.Black;
			this.buttonCheck.Name = "buttonCheck";
			this.buttonCheck.UseVisualStyleBackColor = false;
			this.buttonCheck.Click += new System.EventHandler(this.ButtonCheckClick);
			// 
			// comboBoxCHIP
			// 
			this.comboBoxCHIP.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
			resources.ApplyResources(this.comboBoxCHIP, "comboBoxCHIP");
			this.comboBoxCHIP.FormattingEnabled = true;
			this.comboBoxCHIP.Items.AddRange(new object[] {
									resources.GetString("comboBoxCHIP.Items"),
									resources.GetString("comboBoxCHIP.Items1"),
									resources.GetString("comboBoxCHIP.Items2"),
									resources.GetString("comboBoxCHIP.Items3"),
									resources.GetString("comboBoxCHIP.Items4"),
									resources.GetString("comboBoxCHIP.Items5"),
									resources.GetString("comboBoxCHIP.Items6"),
									resources.GetString("comboBoxCHIP.Items7"),
									resources.GetString("comboBoxCHIP.Items8"),
									resources.GetString("comboBoxCHIP.Items9"),
									resources.GetString("comboBoxCHIP.Items10"),
									resources.GetString("comboBoxCHIP.Items11"),
									resources.GetString("comboBoxCHIP.Items12"),
									resources.GetString("comboBoxCHIP.Items13"),
									resources.GetString("comboBoxCHIP.Items14"),
									resources.GetString("comboBoxCHIP.Items15"),
									resources.GetString("comboBoxCHIP.Items16"),
									resources.GetString("comboBoxCHIP.Items17"),
									resources.GetString("comboBoxCHIP.Items18"),
									resources.GetString("comboBoxCHIP.Items19"),
									resources.GetString("comboBoxCHIP.Items20"),
									resources.GetString("comboBoxCHIP.Items21"),
									resources.GetString("comboBoxCHIP.Items22"),
									resources.GetString("comboBoxCHIP.Items23"),
									resources.GetString("comboBoxCHIP.Items24")});
			this.comboBoxCHIP.Name = "comboBoxCHIP";
			this.comboBoxCHIP.SelectedIndexChanged += new System.EventHandler(this.ComboBoxCHIPSelectedIndexChanged);
			// 
			// label1
			// 
			resources.ApplyResources(this.label1, "label1");
			this.label1.Name = "label1";
			// 
			// buttonVersion
			// 
			this.buttonVersion.BackColor = System.Drawing.Color.WhiteSmoke;
			resources.ApplyResources(this.buttonVersion, "buttonVersion");
			this.buttonVersion.ForeColor = System.Drawing.Color.SlateGray;
			this.buttonVersion.Name = "buttonVersion";
			this.buttonVersion.UseVisualStyleBackColor = false;
			this.buttonVersion.Click += new System.EventHandler(this.ButtonVersionClick);
			// 
			// button打开
			// 
			this.button打开.BackColor = System.Drawing.Color.Gainsboro;
			resources.ApplyResources(this.button打开, "button打开");
			this.button打开.ForeColor = System.Drawing.Color.Black;
			this.button打开.Name = "button打开";
			this.button打开.UseVisualStyleBackColor = false;
			this.button打开.Click += new System.EventHandler(this.Button打开Click);
			// 
			// buttonInstallVos
			// 
			this.buttonInstallVos.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(128)))));
			resources.ApplyResources(this.buttonInstallVos, "buttonInstallVos");
			this.buttonInstallVos.ForeColor = System.Drawing.Color.Black;
			this.buttonInstallVos.Name = "buttonInstallVos";
			this.buttonInstallVos.UseVisualStyleBackColor = false;
			this.buttonInstallVos.Click += new System.EventHandler(this.ButtonInstallVosClick);
			// 
			// button保存
			// 
			this.button保存.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(115)))), ((int)(((byte)(188)))), ((int)(((byte)(255)))));
			resources.ApplyResources(this.button保存, "button保存");
			this.button保存.ForeColor = System.Drawing.Color.Black;
			this.button保存.Name = "button保存";
			this.button保存.UseVisualStyleBackColor = false;
			this.button保存.Click += new System.EventHandler(this.Button保存Click);
			// 
			// panelMain
			// 
			this.panelMain.Controls.Add(this.panel5);
			this.panelMain.Controls.Add(this.label10);
			this.panelMain.Controls.Add(this.panelCommon);
			this.panelMain.Controls.Add(this.panel4);
			this.panelMain.Controls.Add(this.panel2);
			this.panelMain.Controls.Add(this.buttonNDK);
			this.panelMain.Controls.Add(this.panel3);
			this.panelMain.Controls.Add(this.label8);
			this.panelMain.Controls.Add(this.richTextBoxNDK);
			resources.ApplyResources(this.panelMain, "panelMain");
			this.panelMain.Name = "panelMain";
			// 
			// panel5
			// 
			this.panel5.Controls.Add(this.textBox_VOS_Ver);
			this.panel5.Controls.Add(this.label15);
			resources.ApplyResources(this.panel5, "panel5");
			this.panel5.Name = "panel5";
			// 
			// textBox_VOS_Ver
			// 
			resources.ApplyResources(this.textBox_VOS_Ver, "textBox_VOS_Ver");
			this.textBox_VOS_Ver.Name = "textBox_VOS_Ver";
			this.textBox_VOS_Ver.TextChanged += new System.EventHandler(this.TextBox_VOS_VerTextChanged);
			// 
			// label15
			// 
			resources.ApplyResources(this.label15, "label15");
			this.label15.Name = "label15";
			// 
			// VOS_ConfigForm
			// 
			resources.ApplyResources(this, "$this");
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.BackColor = System.Drawing.Color.WhiteSmoke;
			this.Controls.Add(this.panelMain);
			this.Controls.Add(this.panel1);
			this.MaximizeBox = false;
			this.MinimizeBox = false;
			this.Name = "VOS_ConfigForm";
			this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.ModuleConfigFormClosing);
			this.Load += new System.EventHandler(this.ModuleConfigFormLoad);
			this.panel4.ResumeLayout(false);
			this.panel3.ResumeLayout(false);
			this.panel2.ResumeLayout(false);
			this.panel2.PerformLayout();
			this.panelCommon.ResumeLayout(false);
			this.panelCommon.PerformLayout();
			this.panel1.ResumeLayout(false);
			this.panelMain.ResumeLayout(false);
			this.panel5.ResumeLayout(false);
			this.panel5.PerformLayout();
			this.ResumeLayout(false);
		}
		private System.Windows.Forms.Label label15;
		private System.Windows.Forms.TextBox textBox_VOS_Ver;
		private System.Windows.Forms.Panel panel5;
		private System.Windows.Forms.Button buttonCheck;
		private System.Windows.Forms.CheckBox checkBox_US_Tick;
		private System.Windows.Forms.Label label13;
		private System.Windows.Forms.Label label14;
		private System.Windows.Forms.Label label12;
		private System.Windows.Forms.Label label11;
		private System.Windows.Forms.TextBox textBoxDefaultUART;
		private System.Windows.Forms.Panel panelMain;
		private System.Windows.Forms.Button buttonInstallVos;
		private System.Windows.Forms.Button buttonVersion;
		private System.Windows.Forms.Label label10;
		private System.Windows.Forms.Label label9;
		private System.Windows.Forms.CheckBox checkBoxIN_ROM;
		private System.Windows.Forms.CheckBox checkBoxTickSystem;
		private System.Windows.Forms.Panel panel4;
		private System.Windows.Forms.Button buttonNDK;
		private System.Windows.Forms.Label label8;
		private System.Windows.Forms.RichTextBox richTextBoxNDK;
		private System.Windows.Forms.Label label7;
		private System.Windows.Forms.TextBox textBoxROM;
		private System.Windows.Forms.Label label6;
		private System.Windows.Forms.Label label5;
		private System.Windows.Forms.CheckBox checkBoxExtDrv;
		private System.Windows.Forms.Panel panel3;
		private System.Windows.Forms.RadioButton radioButtonRUN_CODE;
		private System.Windows.Forms.RadioButton radioButtonRUN_FLASH;
		private System.Windows.Forms.Panel panel2;
		private System.Windows.Forms.Label label4;
		private System.Windows.Forms.Label label2;
		private System.Windows.Forms.TextBox textBoxRAM;
		private System.Windows.Forms.Label label3;
		private System.Windows.Forms.Panel panelCommon;
		private System.Windows.Forms.Panel panel1;
		private System.Windows.Forms.Label label1;
		private System.Windows.Forms.ComboBox comboBoxCHIP;
		private System.Windows.Forms.Button button保存;
		private System.Windows.Forms.Button button打开;
	}
}
