﻿
using System;
using System.Windows.Forms;
//using n_OS;

//发现这个类放到 Win.cs 里边 编译报错, 不能引用同工程下的其他命名空间

namespace n_MyFile
{
//文件工具类
public static class MyFile
{
	//null: 不提示 直接覆盖
	//.vos ... 提示是否覆盖此类型
	public static string CoverExtShow = null;
	
	static string CompResult;
	
	//复制文件夹及文件
	public static bool CopyFolderOk(string sourceFolder, string destFolder)
	{
		try
		{
			//如果目标路径不存在,则创建目标路径
			if (!System.IO.Directory.Exists(destFolder))
			{
				System.IO.Directory.CreateDirectory(destFolder);
			}
			//得到原文件根目录下的所有文件
			string[] files = System.IO.Directory.GetFiles(sourceFolder);
			foreach (string file in files)
			{
				string name = System.IO.Path.GetFileName(file);
				string dest = System.IO.Path.Combine(destFolder, name);
				
				if( System.IO.File.Exists( dest ) ) {
					if( CoverExtShow != null &&
					    dest.EndsWith( CoverExtShow ) &&
					    MessageBox.Show( "已存在同名文件, 是否覆盖此文件? " + dest, "文件替换提示", MessageBoxButtons.YesNo ) == DialogResult.No ) {
						continue;
					}
					else {
						System.IO.File.Delete( dest );
					}
				}
				System.IO.File.Copy(file, dest);//复制文件
			}
			//得到原文件根目录下的所有文件夹
			string[] folders = System.IO.Directory.GetDirectories(sourceFolder);
			foreach (string folder in folders)
			{
				string name = System.IO.Path.GetFileName(folder);
				string dest = System.IO.Path.Combine(destFolder, name);
				bool ok = CopyFolderOk(folder, dest);//构建目标路径,递归复制文件
			}
			return true;
		}
		catch (Exception e)
		{
			MessageBox.Show(e.Message);
			return false;
		}
	}
	
	//文件对比
	public static string CompFile( string s, string t )
	{
		//....文件名存在判断....
		if (!System.IO.File.Exists( s )) {
			return "源文件不存在: " + s;
		}
		if (!System.IO.File.Exists( t )) {
			return "目标文件不存在: " + t;
		}
		string t_source = n_OS.VIO.OpenTextFileUTF8( s );
		string t_dest = n_OS.VIO.OpenTextFileUTF8( t );
		if( t_source != t_dest ) {
			return "文件不匹配:\n" + s + "\n" + t + "\n";
		}
		else {
			return null; //"通过: " + s + " " + t + "\n";
		}
	}
	
	//文件夹对比
	public static string CompFolder(string sourceFolder, string destFolder)
	{
		//如果目标路径不存在,则创建目标路径
		if (!System.IO.Directory.Exists(sourceFolder)) {
			return "源文件夹不存在: " + sourceFolder;
		}
		if (!System.IO.Directory.Exists(destFolder)) {
			return "目标文件夹不存在: " + destFolder;
		}
		
		CompResult = "";
		CompFolder_inner( sourceFolder, destFolder );
		return CompResult;
	}
	
	//文件夹对比(内部函数)
	static void CompFolder_inner(string sourceFolder, string destFolder)
	{
		try
		{
			//得到原文件根目录下的所有文件
			string[] files = System.IO.Directory.GetFiles(sourceFolder);
			foreach (string file in files)
			{
				string name = System.IO.Path.GetFileName(file);
				string dest = System.IO.Path.Combine(destFolder, name);
				
				if( !System.IO.File.Exists( dest ) ) {
					CompResult += "目标文件未找到: " + dest + "\n";
					continue;
				}
				//System.IO.File.Copy(file, dest);//复制文件
				//这里进行文件对比
				CompResult += CompFile( file, dest );
			}
			//得到原文件根目录下的所有文件夹
			string[] folders = System.IO.Directory.GetDirectories(sourceFolder);
			foreach (string folder in folders)
			{
				string name = System.IO.Path.GetFileName(folder);
				string dest = System.IO.Path.Combine(destFolder, name);
				CompFolder_inner(folder, dest);//构建目标路径,递归复制文件
			}
			return;
		}
		catch (Exception e)
		{
			CompResult += e.Message;
		}

	}
}
}



