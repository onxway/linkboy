﻿
namespace n_VOS_ConfigForm
{
using System;
using System.IO;
using System.Net;
using System.Text;
using System.Windows.Forms;
using System.Drawing;
using n_GUIcoder;
using System.Data;
using n_OS;
using System.Diagnostics;
using n_MyFile;

/// <summary>
/// Description of WorkForm.
/// </summary>
public partial class VOS_ConfigForm : Form
{
	string CurrentFile;
	string CFileName;	//文件名,不包含路径和扩展名
	string CFilePath;	//文件目录, 包含最后的"\", 不包含文件名
	
	//用于进行文件改变保存提示
	string FileText;
	bool Changed;
	
	//系统设置数据, 不需要触发改变事件
	bool SysSet;
	
	string LastChipType;
	
	//主窗口
	public VOS_ConfigForm( string vStartFile )
	{
		InitializeComponent();
		
		UserDir = null;
		ConfigList.Init();
		
		for( int i = 0; i < comboBoxCHIP.Items.Count; ++i ) {
			if( i == 1 && comboBoxCHIP.Items[i].ToString() != "Arduino(nano,UNO,2560)" ) {
				MessageBox.Show( "第2项名字不是: Arduino(nano,UNO,2560)" );
			}
		}
		panelMain.Enabled = false;
		buttonCheck.Visible = false;
		
		if( vStartFile != null ) {
			LoadFile( vStartFile );
		}
		n_wlibCom.wlibCom.SetFont( richTextBoxNDK );
		
		SetChanged( false );
	}
	
	//--------------------------------------------------------
	string UserDir;
	
	void ButtonInstallVosClick(object sender, EventArgs e)
	{
		if( comboBoxCHIP.SelectedIndex == -1 ) {
			MessageBox.Show( "先在右侧选择一个芯片型号, 再创建虚拟机" );
			return;
		}
		//判断是否为更新vos引擎
		if( CurrentFile != null ) {
			string vospath = n_OS.OS.SystemRoot + "vos" + n_OS.OS.PATH_S + "vos" + n_OS.OS.PATH_S;
			string tarpath = CFilePath;
			
			if( System.IO.Directory.Exists( tarpath + "core" ) || System.IO.Directory.Exists( tarpath + "user" ) || System.IO.File.Exists( tarpath + "vos_c.h" ) ) {
				MessageBox.Show( "先移除三个文件和文件夹, 只保留vos配置文件" );
				return;
			}
			MyFile.CoverExtShow = ".vos";
			if( MyFile.CopyFolderOk( vospath, tarpath ) ) {
				SetChanged( true );
			}
			else {
				MessageBox.Show( "vos虚拟机安装到目标路径失败: " + UserDir + "\n请尝试先重启电脑, 再删除已复制的vos文件夹, 并重新安装vos虚拟机" );
			}
			return;
		}
		
		//打开路径文件夹
		FolderBrowserDialog dilog = new FolderBrowserDialog();
		dilog.Description = "请选择目录";
		if( UserDir != null ) {
			dilog.SelectedPath = UserDir;
		}
		if (dilog.ShowDialog() ==  System.Windows.Forms.DialogResult.OK ) {
			
			UserDir = dilog.SelectedPath;
			string vospath = n_OS.OS.SystemRoot + "vos" + n_OS.OS.PATH_S + "vos" + n_OS.OS.PATH_S;
			string tarpath = dilog.SelectedPath + n_OS.OS.PATH_S + "vos" + n_OS.OS.PATH_S;
			
			if( System.IO.Directory.Exists( tarpath ) ) {
				MessageBox.Show( "已存在文件夹vos, 请先将vos文件夹移除后再安装: " + tarpath );
				return;
			}
			
			MyFile.CoverExtShow = ".vos";
			if( MyFile.CopyFolderOk( vospath, tarpath ) ) {
				
				/*
				Process Proc = new Process();
				Proc.StartInfo.WindowStyle = ProcessWindowStyle.Normal;
				//Proc.StartInfo.UseShellExecute = false;
				//Proc.StartInfo.CreateNoWindow = true;
				//Proc.StartInfo.RedirectStandardOutput = true;
				
				Proc.StartInfo.WorkingDirectory = tarpath;
				Proc.StartInfo.FileName = "我的虚拟机.vos";
				
				try {
				Proc.Start();
				}
				catch {
					MessageBox.Show( "无法打开vos文件, 可以尝试在vos配置器界面打开目标文件" );
				}
				*/
				
				File.Copy( vospath + "\\core\\vos_model\\" + comboBoxCHIP.Text + ".vos", tarpath + "\\我的虚拟机" + ".vos" );
				
				LoadFile( tarpath + "\\我的虚拟机.vos" );
				SetChanged( true );
				
				buttonInstallVos.Text = "更新vos虚拟机";
				panelCommon.Visible = true;
			}
			else {
				MessageBox.Show( "vos虚拟机安装到目标路径失败: " + UserDir + "\n请尝试先重启电脑, 再删除已复制的vos文件夹, 并重新安装vos虚拟机" );
			}
		}
	}
	
	//--------------------------------------------------------
	
	void SetCPath( string path )
	{
		CurrentFile = path;
		
		CFilePath = CurrentFile.Remove( CurrentFile.LastIndexOf( OS.PATH_S ) + 1 );
		CFileName = CurrentFile.Remove( 0, CurrentFile.LastIndexOf( OS.PATH_S ) + 1 );
		
		int dotindex = CFileName.LastIndexOf( '.' );
		if( dotindex != -1 ) {
			CFileName = CFileName.Remove( dotindex );
		}
		Text = "我的虚拟机: " + CurrentFile;
	}
	
	void LoadFile( string path )
	{
		SetCPath( path );
		FileText = VIO.OpenTextFileUTF8( path );
		//MessageBox.Show( t );
		
		ConfigList.Parse( FileText );
		
		SysSet = true;
		comboBoxCHIP.SelectedIndex = -1;
		for( int i = 0; i < comboBoxCHIP.Items.Count; ++i ) {
			if( comboBoxCHIP.Items[i].ToString() == ConfigList.CHIP ) {
				 comboBoxCHIP.SelectedItem = comboBoxCHIP.Items[i];
				 break;
			}
		}
		if( comboBoxCHIP.SelectedIndex == -1 ) {
			comboBoxCHIP.SelectedIndex = 0;
			MessageBox.Show( "未知的芯片类型: " + ConfigList.CHIP );
		}
		LastChipType = ConfigList.CHIP;
		textBox_VOS_Ver.Text = ConfigList.VOS_Ver;
		textBoxRAM.Text = ConfigList.VOS_BASE_LENGTH;
		textBoxROM.Text = ConfigList.VOS_ROM_LENGTH;
		if( ConfigList.VOS_FLASH == "0" ) {
			radioButtonRUN_CODE.Checked = true;
			radioButtonRUN_FLASH.Checked = false;
		}
		else {
			radioButtonRUN_CODE.Checked = false;
			radioButtonRUN_FLASH.Checked = true;
		}
		textBoxDefaultUART.Text = ConfigList.VOS_UART_DID;
		checkBoxIN_ROM.Checked = ConfigList.VOS_IN_ROM == "1";
		
		checkBoxTickSystem.Checked = ConfigList.VOS_Tick == "1";
		checkBox_US_Tick.Checked = ConfigList.VOS_US_Tick == "1";
		
		checkBoxExtDrv.Checked = ConfigList.VOS_ExtDriver == "1";
		
		richTextBoxNDK.Text = ConfigList.NDKCode;
		
		SysSet = false;
		panelCommon.Visible = true;
		panelMain.Enabled = true;
		
		n_wlibCom.wlibCom.SetFont( richTextBoxNDK );
		
		SetChanged( false );
		
		buttonInstallVos.Text = "更新vos虚拟机";
		buttonCheck.Visible = true;
	}
	
	void SetChanged( bool c )
	{
		if( c ) {
			//button保存.Enabled = true;
			//button保存.BackColor = Color.FromArgb( 130, 190, 255 );
		}
		else {
			//button保存.Enabled = false;
			//button保存.BackColor = Color.Silver;
		}
		Changed = c;
	}
	
	//--------------------------------------------------------
	
	//窗体加载
	void ModuleConfigFormLoad(object sender, EventArgs e)
	{
		
	}
	
	//窗体关闭事件
	void ModuleConfigFormClosing(object sender, FormClosingEventArgs e)
	{
		if( Changed && FileText != ConfigList.Export() ) {
			DialogResult d = MessageBox.Show( "文件已经被修改过, 需要保存吗?", "保存提示", MessageBoxButtons.YesNoCancel );
			if( d == DialogResult.Cancel ) {
				e.Cancel = true;
				return;
			}
			if( d == DialogResult.Yes ) {
				Button保存Click( null, null );
			}
		}
	}
	
	void Button打开Click(object sender, EventArgs e)
	{
		//打开文件对话框
		OpenFileDialog OpenFileDlg = new OpenFileDialog();
		OpenFileDlg.Filter = "驱动文件 *.vos | *.vos";
		OpenFileDlg.Title = "请选择一个vos文件";
		DialogResult dlgResult = OpenFileDlg.ShowDialog();
		if(dlgResult == DialogResult.OK) {
			LoadFile( OpenFileDlg.FileName );
		}
		OpenFileDlg.Dispose();
	}
	
	void Button保存Click(object sender, EventArgs e)
	{
		if( CurrentFile == null ) {
			MessageBox.Show( "当前没有打开某个配置文件,无法保存, 请先新建一个vos再进行配置" );
			return;
		}
		//打开配置define文件
		string Mod = VIO.OpenTextFileUTF8( CFilePath + "\\core\\_config_c.h" );
		
		bool is8Bit = false;
		bool isArduinoAVR = false;
		if( comboBoxCHIP.SelectedIndex == 1 ) {
			is8Bit = true;
			isArduinoAVR = true;
		}
		string Config = "";
		
		//虚拟机RAM配置
		Config += "#define VOS_SYS_BASE_LENGTH " + ConfigList.VOS_BASE_LENGTH + "\n";
		
		//虚拟机ROM配置
		Config += "#define VOS_SYS_ROM_LENGTH " + ConfigList.VOS_ROM_LENGTH + "\n";
		
		//是否AVR芯片 (读取flash需要专门函数)
		if( isArduinoAVR ) {
			Config += "#define VOS_ArduinoAVR" + "\n";
		}
		//是否8位芯片 (TypeDef 需要修改为8bit)
		if( is8Bit ) {
			Config += "#define VOS_8Bit" + "\n";
		}
		//虚拟机Flash模式配置
		if( ConfigList.VOS_FLASH == "1" ) {
			Config += "#define VOS_FLASH" + "\n";
		}
		//默认串口配置
		Mod = Mod.Replace( "<<VOS_UART_DID>>", ConfigList.VOS_UART_DID );
		
		//虚拟机IN_ROM模式配置
		if( ConfigList.VOS_IN_ROM == "1" ) {
			Config += "#define VOS_IN_ROM" + "\n";
		}
		
		//虚拟机Tick系统配置
		if( ConfigList.VOS_Tick == "1" ) {
			Config += "#define VOS_Tick" + "\n";
		}
		//虚拟机US_Tick系统配置
		if( ConfigList.VOS_US_Tick == "1" ) {
			Config += "#define VOS_US_Tick" + "\n";
		}
		
		//虚拟机扩展库配置
		if( ConfigList.VOS_ExtDriver == "1" ) {
			Config += "#define VOS_ExtDriver" + "\n";
		}
		//这里改为其他地方设置 (不对... 不知道为什么要其他地方设置, 又改回来了 2023.4.2)
		//2023.5.19 明白了, 各自的芯片vos_c里边根据情况自行定义 因此像GD2等, 可能需要在对应的vos_c文件中手工加一个预定义
		//参考ESP32 BASIC的案例
		if( ConfigList.CHIP == "ESP32" ) {
			//Config += "#define LB_INT IRAM_ATTR"+ "\n";
		}
		else {
			//Config += "#define LB_INT  " + "\n";
		}
		
		
		Mod = Mod.Replace( "//<<VOS_CONFIG>>", Config );
		
		//替换CPU型号
		Mod = Mod.Replace( "<<VOS_CPU>>", ConfigList.CHIP );
		
		//替换子版本号
		Mod = Mod.Replace( "<<VOS_VER>>", ConfigList.VOS_Ver );
		
		//获取NDK本地函数散转
		string c_proto = "";
		string c_var = "";
		string c_result = "";
		string c_ndklist_len = "";
		string c_ndklist = "";
		string ndk = GetNDK( ref c_proto, ref c_var, ref c_result, ref c_ndklist_len, ref c_ndklist );
		Mod = Mod.Replace( "//<<VOS_NDK_HAED>>", c_proto );
		Mod = Mod.Replace( "//<<VOS_NDK_VAR>>", c_var );
		Mod = Mod.Replace( "//<<VOS_NDK>>", c_result );
		
		Mod = Mod.Replace( "//<<VOS_NDK_LIST_LEN>>", c_ndklist_len );
		Mod = Mod.Replace( "<<VOS_NDK_LIST>>", c_ndklist );
		
		//根据flash尺寸填充app.h数据
		int Flash_len = 0;
		string AppRes = "";
		bool App_h = ConfigList.VOS_FLASH == "0";
		if( App_h ) {
			if( int.TryParse( ConfigList.VOS_ROM_LENGTH, out Flash_len ) ) {
				
				if( Flash_len > 256000 ) {
					MessageBox.Show( "目前版本虚拟机硬盘空间不能超过256K" );
					return;
				}
				
				if( Flash_len % 4 != 0 ) {
					Flash_len -= Flash_len % 4;
					Flash_len += 4;
				}
				for( int i = 0; i < Flash_len/4; ++i ) {
					AppRes += "0x" + (i*4).ToString( "X" ).PadLeft( 8, '0' ) + ", ";
					if( i % 10 == 9 ) {
						AppRes += "\r\n";
					}
				}
				AppRes += "\r\n\r\n";
			}
			else {
				MessageBox.Show( "虚拟机硬盘空间参数格式无效!" );
				return;
			}
		}
		//如果改变了芯片型号, 则重新导出模板文件
		if( !File.Exists( CFilePath + "\\vos_c.h" ) ) {
			File.Copy( CFilePath + "\\core\\vos_model\\" + ConfigList.CHIP + ".h", CFilePath + "\\vos_c.h" );
		}
		else {
			if( LastChipType != ConfigList.CHIP ) {
				MessageBox.Show( "已存在用户文件: " + CFilePath + "\\vos_c.h, 建议先备份此文件再重新生成配置." );
				return;
			}
		}
		LastChipType = ConfigList.CHIP;
		
		//--------------------------------------------------------
		//当执行到这里表示没问题, 才保存全部的文件
		
		//保存APP文件
		if( App_h ) {
			VIO.SaveTextFileUTF8( CFilePath + "\\user\\app.h", AppRes );
		}
		//保存配置数据文件
		VIO.SaveTextFileUTF8( CFilePath + "\\user\\config_c.h", Mod );
		
		//保存配置工程文件
		string Result = ConfigList.Export();
		FileText = Result;
		VIO.SaveTextFileUTF8( CurrentFile, Result );
		
		//设置状态
		SetChanged( false );
	}
	
	void ButtonNDKClick(object sender, EventArgs e)
	{
		string head = "";
		string vardef = "";
		string result = "";
		
		string c_ndklist_len = "";
		string c_ndklist = "";
		
		string NDK_Code = GetNDK( ref head, ref vardef, ref result, ref c_ndklist_len, ref c_ndklist );
		if( NDK_Code == null || NDK_Code == "" ) {
			MessageBox.Show( "NDK代码为空" );
			return;
		}
		try {
			System.Windows.Forms.Clipboard.Clear();
			System.Windows.Forms.Clipboard.SetText( NDK_Code );
		}
		catch {
			System.Threading.Thread.Sleep( 300 );
			System.Windows.Forms.Application.DoEvents();
			System.Windows.Forms.Clipboard.SetText( NDK_Code );
		}
	}
	
	string GetNDK( ref string c_head, ref string vardef, ref string Result, ref string ndklist_len, ref string ndklist )
	{
		int[] ndk_id = new int[100];
		
		string NDK_Code = "#define string []uint8\n";
		int idx = 55;
		string[] clist = richTextBoxNDK.Text.Split( '\n' );
		string LastNote = "";
		for( int i = 0; i < clist.Length; ++i ) {
			
			string Note = "";
			int ni = clist[i].IndexOf( "//" );
			if( ni != -1 ) {
				Note = clist[i].Remove( 0, ni );
			}
			string[] word = n_CodeFormat.CodeFormat.GetFunc( clist[i] );
			if( word.Length < 4 || word[0].StartsWith( "//" ) ) {
				LastNote = Note;
				continue;
			}
			string ret = "";
			
			bool isex = false;
			int UserID = 0;
			int st = 0;
			if( word[0].StartsWith( "@" ) ) {
				st = 1;
				isex = true;
				UserID = n_Data.Const.Parse( word[0].Remove( 0, 1 ) );
				ndk_id[UserID&0xFFFF] = 1;
			}
			
			if( word[st] == "void" ) {
				//...
			}
			else if( word[st] == "int32" ) {
				ret = "A = ";
			}
			else if( word[st] == "uint32" ) {
				ret = "A = ";
			}
			else if( word[st] == "bool" ) {
				ret = "A = ";
			}
			else {
				Error( "函数返回类型只支持 void, bool, int32, uint32 : " + word[st] );
			}
			string trans_val = "";
			string valu = "";
			int val_idx = 0;
			Int32 addr = 0;
			string func_line = word[st] + " " + word[st+1] + "(";
			string cx_func_line = func_line;
			for( int j = st + 3; j < word.Length - 3; j += 3 ) {
				
				string ctype = word[j];
				string cxtype = word[j];
				
				string myvardef = "";
				if( word[j] == "int32" ) {
					myvardef = "\tint32 i32_" + val_idx + ";";
					valu += "i32_" + val_idx + ", ";
					trans_val += "\t\t\ti32_" + val_idx + " = Mem_Get_int32( DP + " + addr + " );\n";
					addr += 4;
				}
				else if( word[j] == "uint32" ) {
					myvardef = "\tuint32 u32_" + val_idx + ";";
					valu += "u32_" + val_idx + ", ";
					trans_val += "\t\t\tu32_" + val_idx + " = Mem_Get_uint32( DP + " + addr + " );\n";
					addr += 4;
				}
				else if( word[j] == "uint16" ) {
					myvardef = "\tuint16 u16_" + val_idx + ";";
					valu += "u16_" + val_idx + ", ";
					trans_val += "\t\t\tu16_" + val_idx + " = Mem_Get_uint16( DP + " + addr + " );\n";
					addr += 2;
				}
				else if( word[j] == "string" ) {
					myvardef = "\tuint32 u32_" + val_idx + ";";
					valu += "u32_" + val_idx + ", ";
					trans_val += "\t\t\tu32_" + val_idx + " = Mem_Get_uint32( DP + " + addr + " );\n";
					addr += 4;
					ctype = "uint32";
					cxtype = "string";
				}
				else if( word[j] == "bool" ) {
					myvardef = "\tbool u8_" + val_idx + ";";
					valu += "u8_" + val_idx + ", ";
					trans_val += "\t\t\tu8_" + val_idx + " = Mem_Get_uint8( DP + " + addr + " );\n";
					addr += 1;
				}
				else if( word[j] == "void" ) {
					//...
				}
				else {
					Error( "函数参数类型目前支持 bool, uint32, int32, string, 不支持:" + word[j] );
				}
				if( vardef.IndexOf( myvardef ) == -1 ) {
					vardef += myvardef + "\n";
				}
				func_line += ctype + " val" + val_idx + ", ";
				cx_func_line += cxtype + " val" + val_idx + ", ";
				val_idx++;
			}
			cx_func_line = cx_func_line.Trim( " ,".ToCharArray() );
			func_line = func_line.Trim( " ,".ToCharArray() );
			c_head += func_line + ");\n";
			
			valu = valu.Trim( " ,".ToCharArray() );
			if( valu != "" ) {
				valu = " " + valu + " ";
			}
			int caseid = idx;
			if( isex ) {
				caseid = UserID;
			}
			if( LastNote != "" ) {
				NDK_Code += LastNote + "\n";
			}
			NDK_Code += "$SIM_NOTE$ interface[" + caseid + "] " + Note + "\n";
			NDK_Code += cx_func_line + ") {}\n";
			if( isex ) {
				caseid -= 0x10000;
			}
			Result += "\t\tcase " + caseid + ":\n" + trans_val + "\t\t\t" + ret + word[st+1] + "(" + valu + ");\n\t\t\tbreak;\n";
			if( !isex ) {
				idx++;
			}
			LastNote = Note;
		}
		NDK_Code += "#undefine string\n";
		
		//构建列表
		ndklist = "{";
		bool ok = false;
		int num = 0;
		for( int n = 0; n < ndk_id.Length; ++n ) {
			if( !ok && ndk_id[n] != 0 ) {
				ndklist += n + ",";
				ok = true;
				num++;
			}
			if( ok && ndk_id[n] == 0 ) {
				ndklist += (n-1) + ", ";
				ok = false;
				num++;
			}
		}
		ndklist += "}";
		ndklist_len = "#define VOS_NDK_LIST_LEN " + (num*2);
		
		return NDK_Code;
	}
	
	void Error( string mes )
	{
		MessageBox.Show( mes );
	}
	
	//--------------------------------------------------------
	
	void ComboBoxCHIPSelectedIndexChanged(object sender, EventArgs e)
	{
		if( !panelCommon.Enabled ) {
			return;
		}
		if( SysSet ) {
			return;
		}
		SetChanged( true );
		
		ConfigList.CHIP = comboBoxCHIP.Text;
	}
	
	void TextBoxRAMTextChanged(object sender, EventArgs e)
	{
		if( SysSet ) {
			return;
		}
		SetChanged( true );
		
		ConfigList.VOS_BASE_LENGTH = textBoxRAM.Text;
	}
	
	void TextBoxROMTextChanged(object sender, EventArgs e)
	{
		if( SysSet ) {
			return;
		}
		SetChanged( true );
		
		ConfigList.VOS_ROM_LENGTH = textBoxROM.Text;
	}
	
	void RadioButtonRUN_CODECheckedChanged(object sender, EventArgs e)
	{
		if( SysSet ) {
			return;
		}
		SetChanged( true );
		
		if( radioButtonRUN_CODE.Checked ) {
			ConfigList.VOS_FLASH = "0";
		}
		else {
			ConfigList.VOS_FLASH = "1";
		}
	}
	
	void CheckBoxExtDrvCheckedChanged(object sender, EventArgs e)
	{
		if( SysSet ) {
			return;
		}
		SetChanged( true );
		
		if( checkBoxExtDrv.Checked ) {
			ConfigList.VOS_ExtDriver = "1";
		}
		else {
			ConfigList.VOS_ExtDriver = "0";
		}
	}
	
	void RichTextBoxNDKTextChanged(object sender, EventArgs e)
	{
		if( SysSet ) {
			return;
		}
		SetChanged( true );
		
		ConfigList.NDKCode = richTextBoxNDK.Text;
	}
	
	void CheckBoxTickSystemCheckedChanged(object sender, EventArgs e)
	{
		if( SysSet ) {
			return;
		}
		SetChanged( true );
		
		if( checkBoxTickSystem.Checked ) {
			ConfigList.VOS_Tick = "1";
		}
		else {
			ConfigList.VOS_Tick = "0";
		}
	}
	
	void CheckBox_US_TickCheckedChanged(object sender, EventArgs e)
	{
		if( SysSet ) {
			return;
		}
		SetChanged( true );
		
		if( checkBox_US_Tick.Checked ) {
			ConfigList.VOS_US_Tick = "1";
		}
		else {
			ConfigList.VOS_US_Tick = "0";
		}
	}
	
	void CheckBoxIN_ROMCheckedChanged(object sender, EventArgs e)
	{
		if( SysSet ) {
			return;
		}
		SetChanged( true );
		
		if( checkBoxIN_ROM.Checked ) {
			ConfigList.VOS_IN_ROM = "1";
		}
		else {
			ConfigList.VOS_IN_ROM = "0";
		}
	}
	
	void TextBoxDefaultUARTTextChanged(object sender, EventArgs e)
	{
		if( SysSet ) {
			return;
		}
		SetChanged( true );
		
		ConfigList.VOS_UART_DID = textBoxDefaultUART.Text;
	}
	
	void TextBox_VOS_VerTextChanged(object sender, EventArgs e)
	{
		if( SysSet ) {
			return;
		}
		SetChanged( true );
		
		ConfigList.VOS_Ver = textBox_VOS_Ver.Text;
	}
	
	//--------------------------------------------------
	//关于vos
	
	void ButtonVersionClick(object sender, EventArgs e)
	{
		if( n_wlibCom.wlibCom.VBox == null ) {
			n_wlibCom.wlibCom.VBox = new n_VosVersForm.VosVersForm();
		}
		n_wlibCom.wlibCom.VBox.Run( 0 );
	}
	
	void ButtonCheckClick(object sender, EventArgs e)
	{
		//vos配置文件对比
		string spath_vos = CFilePath + "我的虚拟机.vos";
		string tpath_vos = CFilePath + "core\\vos_model\\" + ConfigList.CHIP + ".vos";
		string mes0 = MyFile.CompFile( spath_vos, tpath_vos );
		MessageBox.Show( mes0, "vos配置文件对比" );
		
		//导出程序文件对比
		string spath_c = CFilePath + "vos_c.h";
		string tpath_c = CFilePath + "core\\vos_model\\" + ConfigList.CHIP + ".h";
		string mes1 = MyFile.CompFile( spath_c, tpath_c );
		MessageBox.Show( mes1, "导出程序文件对比" );
		
		//文件夹对比
		string vospath = n_OS.OS.SystemRoot + "vos" + n_OS.OS.PATH_S + "vos";
		string tarpath = CFilePath;
		string mes2 = MyFile.CompFolder( vospath, tarpath );
		MessageBox.Show( mes2, "文件夹对比" );
	}
}
static class ConfigList
{
	//芯片型号
	public static string CHIP;
	
	//vos版本
	public static string VOS_Ver;
	
	//虚拟机内存
	public static string VOS_BASE_LENGTH;
	//虚拟机硬盘
	public static string VOS_ROM_LENGTH;
	
	//Flash运行模式
	public static string VOS_FLASH;
		public static string VOS_UART_DID;
		public static string VOS_IN_ROM;
	
	public static string VOS_Flash_PageSize;
	public static string VOS_Flash_StartPage;
	public static string VOS_Flash_PageNumber;
	
	//Tick系统
	public static string VOS_Tick;
	//US_Tick系统
	public static string VOS_US_Tick;
	
	//扩展驱动库
	public static string VOS_ExtDriver;
	
	//NDK代码
	public static string NDKCode;
	
	//---------------------------------------
	const string E_VERSION = "VERSION";
	
	const string E_CHIP = "CHIP";
	
	const string E_VOS_VER = "VOS_VER";
	
	const string E_VOS_FLASH = "VOS_FLASH";
		const string E_VOS_UART_DID = "VOS_UART_DID";
		const string E_VOS_IN_ROM = "VOS_IN_ROM";
		
	const string E_VOS_BASE_LENGTH = "VOS_BASE_LENGTH";
	const string E_VOS_ROM_LENGTH = "VOS_ROM_LENGTH";
	const string E_VOS_Flash_PageSize = "VOS_Flash_PageSize";
	const string E_VOS_Flash_StartPage = "VOS_Flash_StartPage";
	const string E_VOS_Flash_PageNumber = "VOS_Flash_PageNumber";
	
	const string E_VOS_Tick = "VOS_Tick";
	const string E_VOS_US_Tick = "VOS_US_Tick";
	
	const string E_VOS_ExtDriver = "VOS_ExtDriver";
	const string E_VOS_Circuit_Engine = "VOS_Circuit_Engine"; //过时
	
	const string E_VOS_NDK = "VOS_NDK";
	
	const char E_link = '=';
	
	//初始化
	public static void Init()
	{
		
	}
	
	//解析配置
	public static void Parse( string text )
	{
		CHIP = "NULL";
		VOS_Ver = "A";
		VOS_FLASH = "0";
		VOS_BASE_LENGTH = "2000";
		VOS_ROM_LENGTH = "20000";
		VOS_IN_ROM = "0";
		VOS_UART_DID = "-1";
		
		NDKCode = "";
		
		bool isNDK = false;
		string[] Line = text.Split( '\n' );
		for( int i = 0; i < Line.Length; ++i ) {
			
			if( isNDK ) {
				NDKCode += Line[i];
				if( i < Line.Length - 1 ) {
					NDKCode += "\n";
				}
				continue;
			}
			
			string line = Line[i].Trim( " \t".ToCharArray() );
			if( line== "" || line.StartsWith( "//" ) ) {
				continue;
			}
			int idx = line.IndexOf( '=' );
			string Name = line.Remove( idx );
			string Value = line.Remove( 0, idx + 1 );
			
			//-------------------------------
			
			if( Name == E_VERSION ) {
				//... 这里进行版本比较, 兼容性验证
			}
			else if( Name == E_CHIP ) {
				CHIP = Value;
			}
			else if( Name == E_VOS_VER ) {
				VOS_Ver = Value;
			}
			else if( Name == E_VOS_BASE_LENGTH ) {
				VOS_BASE_LENGTH = Value;
			}
			else if( Name == E_VOS_ROM_LENGTH ) {
				VOS_ROM_LENGTH = Value;
			}
			//Flash运行模式
			else if( Name == E_VOS_FLASH ) {
				VOS_FLASH = Value;
			}
			//默认串口号
			else if( Name == E_VOS_UART_DID ) {
				VOS_UART_DID = Value;
			}
			//IN_ROM运行模式
			else if( Name == E_VOS_IN_ROM ) {
				VOS_IN_ROM = Value;
			}
			else if( Name == E_VOS_Flash_PageSize ) {
				VOS_Flash_PageSize = Value;
			}
			else if( Name == E_VOS_Flash_StartPage ) {
				VOS_Flash_StartPage = Value;
			}
			else if( Name == E_VOS_Flash_PageNumber ) {
				VOS_Flash_PageNumber = Value;
			}
			//Tick系统
			else if( Name == E_VOS_Tick ) {
				VOS_Tick = Value;
			}
			//US_Tick系统
			else if( Name == E_VOS_US_Tick ) {
				VOS_US_Tick = Value;
			}
			//扩展驱动库
			else if( Name == E_VOS_ExtDriver ) {
				VOS_ExtDriver = Value;
			}
			//电路仿真引擎
			else if( Name == E_VOS_Circuit_Engine ) {
				//VOS_Circuit_Engine = Value;
			}
			else if( Name == E_VOS_NDK ) {
				isNDK = true;
			}
			//-------------------------------
			else {
				MessageBox.Show( "未知的配置参数: " + Name );
			}
		}
	}
	
	//合成文本描述
	public static string Export()
	{
		string res = "";
		res += E_VERSION + E_link + "0\n";
		
		res += E_CHIP + E_link + CHIP + "\n";
		
		res += E_VOS_VER + E_link + VOS_Ver + "\n";
		
		res += E_VOS_BASE_LENGTH + E_link + VOS_BASE_LENGTH + "\n";
		res += E_VOS_ROM_LENGTH + E_link + VOS_ROM_LENGTH + "\n";
		
		res += E_VOS_FLASH + E_link + VOS_FLASH + "\n";
		res += E_VOS_UART_DID + E_link + VOS_UART_DID + "\n";
		res += E_VOS_IN_ROM + E_link + VOS_IN_ROM + "\n";
		
		res += E_VOS_Flash_PageSize + E_link + VOS_Flash_PageSize + "\n";
		res += E_VOS_Flash_StartPage + E_link + VOS_Flash_StartPage + "\n";
		res += E_VOS_Flash_PageNumber + E_link + VOS_Flash_PageNumber + "\n";
		res += E_VOS_Tick + E_link + VOS_Tick + "\n";
		res += E_VOS_US_Tick + E_link + VOS_US_Tick + "\n";
		res += E_VOS_ExtDriver + E_link + VOS_ExtDriver + "\n";
		res += E_VOS_NDK + E_link + "\n" + NDKCode;
		return res;
	}
}
}



