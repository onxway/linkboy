﻿
namespace c_MIDI
{
	using System;
	using System.Windows.Forms;
	using System.Runtime.InteropServices;

	//*******************************************************
	//MIDI类
	public static class Music
	{
		//打开MIDI设备
		public static void Open()
		{
			int Err = midiOutOpen( out hMidi, -1, 0, 0, 0);
			if( Err != 0 ) {
				//MessageBox.Show( "MIDI设备打开失败, 错误代号:" + Err );
			}
		}
		
		//关闭MIDI设备
		public static void Close()
		{
			if( hMidi != 0 ) {
				midiOutClose( hMidi );
			}
		}
		
		//发出某个频率的声音, Scale: 0-127, Volume: 0-127
		public static void PlaySound( int Channel, int Scale, int Volume )
		{
			int Err = midiOutShortMsg( hMidi, 0x90 + Channel + Scale * 0x100 + Volume * 0x10000 );
			if( Err != 0 ) {
				//MessageBox.Show( "<播放音乐时>声卡请求失败, 错误代号:" + Err );
			}
		}
		
		//关闭某个频率的声音, Scale: 0-127
		public static void CloseSound( int Channel, int Scale )
		{
			int Err = midiOutShortMsg( hMidi, 0x80 + Channel + Scale * 0x100 );
			if( Err != 0 ) {
				//MessageBox.Show( "<停止播放音乐时>声卡请求失败, 错误代号:" + Err );
			}
		}
		
		//设置音色
		public static void SetTimbre( int Channel, int Timbre )
		{
			//??????
			int Err = midiOutShortMsg( hMidi, 0xb0 + Channel + Timbre * 0x10000 );
			if( Err != 0 ) {
				//MessageBox.Show( "<设置音色时>声卡请求失败, 错误代号:" + Err );
			}
			//??????
			Err = midiOutShortMsg( hMidi, 0xc0 + Channel + Timbre * 0x100 );
			if( Err != 0 ) {
				//MessageBox.Show( "<设置音色时>声卡请求失败, 错误代号:" + Err );
			}
		}
		
		//设备号码
		static int hMidi;
		
		[DllImport( "winmm.dll" )]
		static extern int midiOutOpen(
			out int lphMidiOut,
			int uDeviceID,
			int dwCallback,
			int dwInstance,
			int dwFlags );
		[DllImport( "winmm.dll" )]
		static extern int midiOutClose(
			int hMidiOut );
		[DllImport( "winmm.dll" )]
		static extern int midiOutShortMsg(
			int hMidiOut,
			int dwMsg );
	}
}
