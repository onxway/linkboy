﻿using System;
using System.Drawing;
using System.Runtime.InteropServices;

namespace n_Bitblt
{
	public static class Blt
	{
		public enum TernaryRasterOperations
		{
			SRCCOPY     = 0x00CC0020, /* dest = source*/
			SRCPAINT    = 0x00EE0086, /* dest = source OR dest*/
			SRCAND      = 0x008800C6, /* dest = source AND dest*/
			SRCINVERT   = 0x00660046, /* dest = source XOR dest*/
			SRCERASE    = 0x00440328, /* dest = source AND (NOT dest )*/
			NOTSRCCOPY  = 0x00330008, /* dest = (NOT source)*/
			NOTSRCERASE = 0x001100A6, /* dest = (NOT src) AND (NOT dest) */
			MERGECOPY   = 0x00C000CA, /* dest = (source AND pattern)*/
			MERGEPAINT  = 0x00BB0226, /* dest = (NOT source) OR dest*/
			PATCOPY     = 0x00F00021, /* dest = pattern*/
			PATPAINT    = 0x00FB0A09, /* dest = DPSnoo*/
			PATINVERT   = 0x005A0049, /* dest = pattern XOR dest*/
			DSTINVERT   = 0x00550009, /* dest = (NOT dest)*/
			BLACKNESS   = 0x00000042, /* dest = BLACK*/
			WHITENESS   = 0x00FF0062, /* dest = WHITE*/
		};
		[DllImport("gdi32.dll")]
		static extern bool BitBlt(
			IntPtr hdcDest, //目标设备的句柄
			int nXDest,     // 目标对象的左上角的X坐标
			int nYDest,     // 目标对象的左上角的X坐标
			int nWidth,     // 目标对象的矩形的宽度
			int nHeight,    // 目标对象的矩形的长度
			IntPtr hdcSrc,  // 源设备的句柄
			int nXSrc,      // 源对象的左上角的X坐标
			int nYSrc,      // 源对象的左上角的X坐标
			TernaryRasterOperations dwRop       // 光栅的操作值
		);
		
		[DllImport("gdi32.dll")]
		static extern bool StretchBlt(
			IntPtr hdcDest,
			int nXOriginDest, int nYOriginDest, int nWidthDest, int nHeightDest,
			IntPtr hdcSrc,
			int nXOriginSrc, int nYOriginSrc, int nWidthSrc, int nHeightSrc,
			TernaryRasterOperations dwRop );
		
		[DllImport("gdi32.dll", ExactSpelling = true, SetLastError = true)]
		public static extern IntPtr CreateCompatibleDC(IntPtr hdcPtr);
		[DllImport("gdi32.dll", ExactSpelling = true)]
		public static extern IntPtr SelectObject(IntPtr hdcPtr, IntPtr hObject);
		[DllImport("gdi32.dll", ExactSpelling = true)]
		public static extern bool DeleteDC(IntPtr hdcPtr);
		[System.Runtime.InteropServices.DllImport("gdi32.dll")]
		public static extern bool DeleteObject(IntPtr hObject);
		
		[DllImport("Msimg32.dll")]
        public static extern bool TransparentBlt(
			IntPtr hdcDest, int nXOriginDest, int nYOriginDest, int nWidthDest, int hHeightDest,
			IntPtr hdcSrc, int nXOriginSrc, int nYOriginSrc, int nWidthSrc, int nHeightSrc, uint crTransparent);
		
		static Bitmap[] BitmapList;
		static IntPtr[] bmpPtrList;
		static IntPtr[] memdcPtrList;
		const int MaxL = 50;
		
		public static int Number;
		
		static bool Used = false;
		
		//==================================================================
		//初始化
		public static void Init()
		{
			BitmapList = new Bitmap[MaxL];
			bmpPtrList = new IntPtr[MaxL];
			memdcPtrList = new IntPtr[MaxL];
			
			Used = true;
		}
		
		//退出程序
		public static void Close()
		{
			if( !Used ) {
				return;
			}
			for( int i = 0; i < MaxL; ++i ) {
				if( BitmapList[i]!= null ) {
					DeleteDC(memdcPtrList[i]);             // 释放内存
					DeleteObject(bmpPtrList[i]);           // 释放内存
				}
			}
		}
		
		//==================================================================
		//绘制图片到目标Graphics
		public static bool DrawImage( Graphics clientDC, Bitmap bmp, int x, int y, int w, int h )
		{
			//Graphics clientDC = this.CreateGraphics();
			
			IntPtr hdcPtr = clientDC.GetHdc();
			bool ok = false;
			for( int i = 0; i < MaxL; ++i ) {
				
				if( BitmapList[i] == null ) {
					BitmapList[i] = bmp;
					bmpPtrList[i] = bmp.GetHbitmap();
					memdcPtrList[i] = CreateCompatibleDC(hdcPtr);   // 创建兼容DC
					SelectObject( memdcPtrList[i], bmpPtrList[i] );
					Number = i;
				}
				if( BitmapList[i] == bmp ) {
					ok = StretchBlt(hdcPtr, x, y, w, h, memdcPtrList[i], 0, 0, bmp.Width, bmp.Height, TernaryRasterOperations.SRCCOPY);
					break;
				}
				else {
					//...
				}
			}
			clientDC.ReleaseHdc(hdcPtr);    // 释放内存
			
			return ok;
		}
		
		//==================================================================
		//绘制图片到目标Graphics
		public static void DrawImageOne( Graphics clientDC, Bitmap bmp, int x, int y, int w, int h )
		{
			//Graphics clientDC = this.CreateGraphics();
			
			IntPtr hdcPtr = clientDC.GetHdc();
			
			if( !ok ) {
				ok = true;
				memdcPtr = CreateCompatibleDC(hdcPtr);   // 创建兼容DC
				bmpPtr = bmp.GetHbitmap();
				SelectObject(memdcPtr, bmpPtr);
			}
			
			//BitBlt(hdcPtr, i, i, bmp.Width, bmp.Height, memdcPtr, 0, 0, TernaryRasterOperations.SRCCOPY);
			StretchBlt(hdcPtr, x, y, w, h, memdcPtr, 0, 0, bmp.Width, bmp.Height, TernaryRasterOperations.SRCCOPY);
			
			//DeleteDC(memdcPtr);             // 释放内存
			//DeleteObject(bmpPtr);           // 释放内存
			
			clientDC.ReleaseHdc(hdcPtr);    // 释放内存
		}
		static bool ok = false;
		static IntPtr bmpPtr;
		static IntPtr memdcPtr;
	}
}

