﻿
namespace c_PlaySound
{
	using System;
	using System.Windows.Forms;
	using System.Runtime.InteropServices;

	//*******************************************************
	//MIDI类
	public static class PlaySound
	{
		static Timer t;
		
		static string FileName;
		static int ID;
		
		static int Type;
		
		//初始化
		public static void Init()
		{
			FileName = null;
			
			t = new Timer();
			t.Interval = 100;
			t.Tick += new EventHandler( TimerTick );
			t.Enabled = true;
			
			Type = 0;
			ID = 0;
		}
		
		//播放声音
		public static void Play( int id, string fname )
		{
			ID = id;
			FileName = fname;
			Type = 1;
		}
		
		//停止播放
		public static void Stop( int id )
		{
			ID = id;
			Type = 2;
		}
		
		//暂停播放
		public static void pause( int id )
		{
			ID = id;
			Type = 3;
		}
		
		//继续播放
		public static void resume( int id )
		{
			ID = id;
			Type = 4;
		}
		
		static void TimerTick( object sender, EventArgs e )
		{
			if( Type == 0 ) {
				return;
			}
			if( Type == 1 ) {
				uint a = mciSendString("close linkboy" + ID, null, 0, 0);
				uint b = mciSendString("open \"" + FileName + "\" alias linkboy" + ID, null, 0, 0);
				uint c = mciSendString("play linkboy" + ID, null, 0, 0);
			}
			if( Type == 2 ) {
				uint b = mciSendString("close linkboy" + ID, null, 0, 0);
			}
			if( Type == 3 ) {
				uint b = mciSendString("pause linkboy" + ID, null, 0, 0);
			}
			if( Type == 4 ) {
				uint b = mciSendString("resume linkboy" + ID, null, 0, 0);
			}
			Type = 0;
		}
		
		[DllImport("winmm.dll")]
		static extern uint mciSendString(string lpstrCommand, string lpstrReturnString, uint uReturnLength, uint hWndCallback);
	}
}
