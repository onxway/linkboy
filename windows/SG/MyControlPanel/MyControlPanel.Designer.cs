﻿/*
 * 由SharpDevelop创建。
 * 用户： cap_gpu
 * 日期: 2016/11/4
 * 时间: 23:30
 * 
 * 要改变这种模板请点击 工具|选项|代码编写|编辑标准头文件
 */
namespace n_SG_MyControlPanel
{
	partial class MyControlPanel
	{
		/// <summary>
		/// Designer variable used to keep track of non-visual components.
		/// </summary>
		private System.ComponentModel.IContainer components = null;
		
		/// <summary>
		/// Disposes resources used by the control.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing) {
				if (components != null) {
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}
		
		/// <summary>
		/// This method is required for Windows Forms designer support.
		/// Do not change the method contents inside the source code editor. The Forms designer might
		/// not be able to load this method if it was changed manually.
		/// </summary>
		private void InitializeComponent()
		{
			this.labelX = new System.Windows.Forms.Label();
			this.textBoxX = new System.Windows.Forms.TextBox();
			this.textBoxY = new System.Windows.Forms.TextBox();
			this.label1 = new System.Windows.Forms.Label();
			this.textBoxWidth = new System.Windows.Forms.TextBox();
			this.label2 = new System.Windows.Forms.Label();
			this.textBoxHeight = new System.Windows.Forms.TextBox();
			this.label3 = new System.Windows.Forms.Label();
			this.textBoxName = new System.Windows.Forms.TextBox();
			this.label4 = new System.Windows.Forms.Label();
			this.SuspendLayout();
			// 
			// labelX
			// 
			this.labelX.BackColor = System.Drawing.Color.Transparent;
			this.labelX.Font = new System.Drawing.Font("微软雅黑", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
			this.labelX.Location = new System.Drawing.Point(0, 43);
			this.labelX.Name = "labelX";
			this.labelX.Size = new System.Drawing.Size(114, 29);
			this.labelX.TabIndex = 0;
			this.labelX.Text = "X坐标";
			this.labelX.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// textBoxX
			// 
			this.textBoxX.Font = new System.Drawing.Font("微软雅黑", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
			this.textBoxX.Location = new System.Drawing.Point(120, 43);
			this.textBoxX.Name = "textBoxX";
			this.textBoxX.Size = new System.Drawing.Size(75, 29);
			this.textBoxX.TabIndex = 1;
			this.textBoxX.Text = "0";
			// 
			// textBoxY
			// 
			this.textBoxY.Font = new System.Drawing.Font("微软雅黑", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
			this.textBoxY.Location = new System.Drawing.Point(120, 78);
			this.textBoxY.Name = "textBoxY";
			this.textBoxY.Size = new System.Drawing.Size(75, 29);
			this.textBoxY.TabIndex = 3;
			this.textBoxY.Text = "0";
			// 
			// label1
			// 
			this.label1.BackColor = System.Drawing.Color.Transparent;
			this.label1.Font = new System.Drawing.Font("微软雅黑", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
			this.label1.Location = new System.Drawing.Point(0, 78);
			this.label1.Name = "label1";
			this.label1.Size = new System.Drawing.Size(114, 29);
			this.label1.TabIndex = 2;
			this.label1.Text = "Y坐标";
			this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// textBoxWidth
			// 
			this.textBoxWidth.Font = new System.Drawing.Font("微软雅黑", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
			this.textBoxWidth.Location = new System.Drawing.Point(120, 113);
			this.textBoxWidth.Name = "textBoxWidth";
			this.textBoxWidth.Size = new System.Drawing.Size(75, 29);
			this.textBoxWidth.TabIndex = 5;
			this.textBoxWidth.Text = "0";
			// 
			// label2
			// 
			this.label2.BackColor = System.Drawing.Color.Transparent;
			this.label2.Font = new System.Drawing.Font("微软雅黑", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
			this.label2.Location = new System.Drawing.Point(0, 113);
			this.label2.Name = "label2";
			this.label2.Size = new System.Drawing.Size(114, 29);
			this.label2.TabIndex = 4;
			this.label2.Text = "宽度";
			this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// textBoxHeight
			// 
			this.textBoxHeight.Font = new System.Drawing.Font("微软雅黑", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
			this.textBoxHeight.Location = new System.Drawing.Point(120, 148);
			this.textBoxHeight.Name = "textBoxHeight";
			this.textBoxHeight.Size = new System.Drawing.Size(75, 29);
			this.textBoxHeight.TabIndex = 7;
			this.textBoxHeight.Text = "0";
			// 
			// label3
			// 
			this.label3.BackColor = System.Drawing.Color.Transparent;
			this.label3.Font = new System.Drawing.Font("微软雅黑", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
			this.label3.Location = new System.Drawing.Point(0, 148);
			this.label3.Name = "label3";
			this.label3.Size = new System.Drawing.Size(114, 29);
			this.label3.TabIndex = 6;
			this.label3.Text = "高度";
			this.label3.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// textBoxName
			// 
			this.textBoxName.Font = new System.Drawing.Font("微软雅黑", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
			this.textBoxName.Location = new System.Drawing.Point(120, 8);
			this.textBoxName.Name = "textBoxName";
			this.textBoxName.Size = new System.Drawing.Size(75, 29);
			this.textBoxName.TabIndex = 9;
			// 
			// label4
			// 
			this.label4.BackColor = System.Drawing.Color.Transparent;
			this.label4.Font = new System.Drawing.Font("微软雅黑", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
			this.label4.Location = new System.Drawing.Point(0, 8);
			this.label4.Name = "label4";
			this.label4.Size = new System.Drawing.Size(114, 29);
			this.label4.TabIndex = 8;
			this.label4.Text = "名称";
			this.label4.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// MyControlPanel
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.Controls.Add(this.textBoxName);
			this.Controls.Add(this.label4);
			this.Controls.Add(this.textBoxHeight);
			this.Controls.Add(this.label3);
			this.Controls.Add(this.textBoxWidth);
			this.Controls.Add(this.label2);
			this.Controls.Add(this.textBoxY);
			this.Controls.Add(this.label1);
			this.Controls.Add(this.textBoxX);
			this.Controls.Add(this.labelX);
			this.Name = "MyControlPanel";
			this.Size = new System.Drawing.Size(195, 206);
			this.ResumeLayout(false);
			this.PerformLayout();
		}
		private System.Windows.Forms.Label label4;
		private System.Windows.Forms.TextBox textBoxName;
		private System.Windows.Forms.Label label3;
		private System.Windows.Forms.TextBox textBoxHeight;
		private System.Windows.Forms.Label label2;
		private System.Windows.Forms.TextBox textBoxWidth;
		private System.Windows.Forms.Label label1;
		private System.Windows.Forms.TextBox textBoxY;
		private System.Windows.Forms.TextBox textBoxX;
		private System.Windows.Forms.Label labelX;
	}
}
