﻿/*
 * 由SharpDevelop创建。
 * 用户： cap_gpu
 * 日期: 2016/11/4
 * 时间: 23:30
 * 
 * 要改变这种模板请点击 工具|选项|代码编写|编辑标准头文件
 */
using System;
using System.ComponentModel;
using System.Drawing;
using System.Windows.Forms;
using n_SG_MyControl;
using n_SG_MySprite;

namespace n_SG_MyControlPanel
{
public partial class MyControlPanel : UserControl
{
	//是否刷新界面数据
	public bool AutoRefresh;
	
	//构造函数
	public MyControlPanel()
	{
		InitializeComponent();
		
		AutoRefresh = true;
	}
	
	//刷新控件数据
	public void RefreshData()
	{
		if( !AutoRefresh ) {
			return;
		}
		MyControl TargetCt = n_M.M.SelectedObj;
		
		if( TargetCt != null ) {
			this.textBoxName.Text = TargetCt.Name;
			this.textBoxX.Text = TargetCt.V_RolX.ToString();
			this.textBoxY.Text = (MyControl.Y_Default? TargetCt.V_RolY: -TargetCt.V_RolY).ToString();
			this.textBoxWidth.Text = TargetCt.DL_Width.ToString();
			this.textBoxHeight.Text = TargetCt.DL_Height.ToString();
		}
	}
	
	//更新目标控件数据
	public void Restore()
	{
		MyControl TargetCt = n_M.M.SelectedObj;
		
		if( TargetCt != null ) {
			TargetCt.Name = this.textBoxName.Text;
			TargetCt.V_RolX = int.Parse( this.textBoxX.Text );
			TargetCt.V_RolY= MyControl.Y_Default? int.Parse( this.textBoxY.Text ): -int.Parse( this.textBoxY.Text );
			TargetCt.DL_Width = int.Parse( this.textBoxWidth.Text );
			TargetCt.DL_Height= int.Parse( this.textBoxHeight.Text );
		}
	}
}
}


