﻿
namespace c_FormMover
{
using System;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Windows.Forms;

//窗体移动处理类
public class FormMover
{
	bool isMouseDown;
	int LastX;
	int LastY;
	
	Form form;
	
	//创建一个移动器
	public FormMover( Form f )
	{
		isMouseDown = false;
		LastX = 0;
		LastY = 0;
		
		form = f;
		SearchControl( f );
	}
	
	//搜索一个控件
	void SearchControl( Control sc )
	{
		if( sc is Form || sc is Panel || sc is GroupBox ||sc is Label || sc is ListBox || sc is Button ) {
			sc.MouseDown += new MouseEventHandler( FormMouseDown );
			sc.MouseUp += new MouseEventHandler( FormMouseUp );
			sc.MouseMove += new MouseEventHandler( FormMouseMove );
			
			foreach( Control c in sc.Controls ) {
				SearchControl( c );
			}
		}
	}
	
	void FormMouseDown(object sender, MouseEventArgs e)
	{
		isMouseDown = true;
		LastX = e.X;
		LastY = e.Y;
	}
	
	void FormMouseUp(object sender, MouseEventArgs e)
	{
		isMouseDown = false;
	}
	
	void FormMouseMove(object sender, MouseEventArgs e)
	{
		if( isMouseDown ) {
			int x = form.Location.X;
			int y = form.Location.Y;
			x += e.X - LastX;
			y += e.Y - LastY;
			form.Location = new Point( x, y );
		}
	}
}
}


