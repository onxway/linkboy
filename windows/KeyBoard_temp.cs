﻿
using System;
using System.Windows.Forms;
using Dongzr.MidiLite;

namespace n_KeyBoard
{
public static class KeyBoard
{
	static KeyNote[] KeyNoteList;
	
	static MmTimer t;
	
	static int PressedNumber;
	
	static int StartLevel;
	
	//初始化
	public static void Init()
	{
		PressedNumber = 0;
		
		KeyNoteList = new KeyNote[ 256 ];
		for( int i = 0; i < KeyNoteList.Length; ++i ) {
			KeyNoteList[i] = new KeyNote();
		}
		t = new MmTimer();
		t.Interval = 10;
		t.Tick += TimerTick;
		t.Start();
	}
	
	//打开按键
	public static void Open( int KeyValue )
	{
		KeyNoteList[KeyValue].isOpen = true;
	}
	
	//关闭按键
	public static void Close( int KeyValue )
	{
		KeyNoteList[KeyValue].isOpen = false;
	}
	
	//触发按键事件 0:按键松开, 1:按键按下
	public static void TrigKeyEvent( int KeyValue, int status )
	{
		//过滤掉系统自动按键触发, 只保留刚按下和松开事件
		if( status == 0 ) {
			if( KeyNoteList[KeyValue].isRealPress ) {
				KeyNoteList[KeyValue].isRealPress = false;
			}
		}
		else {
			if( KeyNoteList[KeyValue].isRealPress ) {
				return;
			}
			KeyNoteList[KeyValue].isRealPress = true;
		}
		
		//判断是否需要初始化开始级别
		if( PressedNumber == 0 ) {
			StartLevel = 0;
		}
		
		//判断是否实际触发键盘事件给用户
		if( status == 0 ) {
			if( KeyNoteList[KeyValue].isPress ) {
				KeyNoteList[KeyValue].isPress = false;
				KeyNoteList[KeyValue].Level = 0;
				PressedNumber--;
			}
			else {
				if( KeyNoteList[KeyValue].KIgnore == 1 && PressedNumber > 0 ) {
					return;
				}
			}
		}
		if( status == 1 ) {
			if( KeyNoteList[KeyValue].KIgnore == 1 && PressedNumber > 0 ) {
				return;
			}
			if( KeyNoteList[KeyValue].KIgnore == 2 && PressedNumber > 0 ) {
				PressedNumber = 0;
				for( int i = 0; i < KeyNoteList.Length; ++i ) {
					if( KeyNoteList[i].isPress ) {
						KeyNoteList[i].isPress = false;
						KeyNoteList[i].Level = 0;
					}
				}
			}
			if( KeyNoteList[KeyValue].KIgnore == 3 ) {
				StartLevel++;
				KeyNoteList[KeyValue].Level = StartLevel;
			}
			KeyNoteList[KeyValue].isPress = true;
			KeyNoteList[KeyValue].Tick = 0;
			PressedNumber++;
		}
		//触发按键事件给控件
		if( KeyNoteList[KeyValue].isOpen && KeyNoteList[KeyValue].KeyEvent != null ) {
			KeyNoteList[KeyValue].KeyEvent( status );
			if( status == 1 ) {
				KeyNoteList[KeyValue].KeyEvent( 2 );
			}
		}
	}
	
	//配置按键
	public static void SetKeyEvent( int KeyValue, KeyNote.D_KeyEvent KeyEvent, int KIgnore )
	{
		KeyNoteList[KeyValue].KeyEvent = KeyEvent;
		KeyNoteList[KeyValue].KIgnore = KIgnore;
	}
	
	//配置按键
	public static void SetKeyMaxTick( int KeyValue, int t )
	{
		KeyNoteList[KeyValue].MaxTick = t / 10;
	}
	
	static void TimerTick( object sender, EventArgs e )
	{
		int FirstKeyIndex = -1;
		int FirstKeyLevel = 0;
		for( int i = 0; i < KeyNoteList.Length; ++i ) {
			if( KeyNoteList[i].isPress && KeyNoteList[i].Level > FirstKeyLevel ) {
				FirstKeyIndex = i;
				FirstKeyLevel = KeyNoteList[i].Level;
			}
		}
		if( FirstKeyIndex != -1 ) {
			KeyNoteList[FirstKeyIndex].Tick++;
			if( KeyNoteList[FirstKeyIndex].Tick >= KeyNoteList[FirstKeyIndex].MaxTick ) {
				KeyNoteList[FirstKeyIndex].Tick = 0;
				if( KeyNoteList[FirstKeyIndex].isOpen && KeyNoteList[FirstKeyIndex].KeyEvent != null ) {
					KeyNoteList[FirstKeyIndex].KeyEvent( 2 );
				}
			}
		}
		for( int i = 0; i < KeyNoteList.Length; ++i ) {
			if( KeyNoteList[i].isPress && KeyNoteList[i].KIgnore == 0 ) {
				KeyNoteList[i].Tick++;
				if( KeyNoteList[i].Tick >= KeyNoteList[i].MaxTick ) {
					KeyNoteList[i].Tick = 0;
					if( KeyNoteList[i].isOpen && KeyNoteList[i].KeyEvent != null ) {
						KeyNoteList[i].KeyEvent( 2 );
					}
				}
			}
		}
		
	}
	
	//===============================================================
	public class KeyNote
	{
		public delegate void D_KeyEvent( int Status );
		public D_KeyEvent KeyEvent;
		
		//是否打开
		public bool isOpen;
		
		//单位为0.1秒
		public int Tick;
		public int MaxTick;
		
		//0: 各个按键互不相关
		//1: 其他键按下时忽略当前按键事件
		//2: 当前按键触发时取消其他按键(强制松开)
		//3: 当前按键触发时暂停其他按键
		public int KIgnore;
		
		//当前按键是否按下
		public bool isPress;
		
		//当前按键的响应级别
		public int Level;
		
		//按键是否底层被按下
		public bool isRealPress;
		
		//构造函数
		public KeyNote()
		{
			isOpen = true;
			
			Tick = 0;
			MaxTick = 50;
			KIgnore = 0;
			isPress = false;
			Level = 0;
		}
	}
}
}
