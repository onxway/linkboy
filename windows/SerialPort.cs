﻿
using System;
using System.Runtime.InteropServices;
using System.Collections.Generic;
using System.Management;
using System.Windows.Forms;

namespace n_MySerialPort
{
public static class MySerialPort
{
	static bool HasShow = false;
	
	//获取系统中的所有串口名称
	public static string[] GetFullNameList()
	{
		//通过WMI获取COM端口
		string[] ss = MulGetHardwareInfo( HardwareEnum.Win32_PnPEntity );
		
		//获取端口列表
		Microsoft.VisualBasic.Devices.Computer pc = new Microsoft.VisualBasic.Devices.Computer();
		List<string> strs = new List<string>();
		foreach( string name in pc.Ports.SerialPortNames ) {
			
			string t = name;
			if( ss != null ) {
				foreach (string s in ss ) {
					if( s.Contains( "(" + name + ")" ) ) {
						t = name + " " + s;
					}
				}
			}
			strs.Add( t );
		}
		return strs.ToArray();
	}
	
	//获取指定名称的串口的数字序号
	public static int GetIndex( string Name )
	{
		Name = Name.Split( ' ' )[0];
		int PortNumber = int.Parse( Name.Remove( 0, 3 ) );
		return PortNumber;
	}
	
	//获取指定名称的串口的串口名称
	public static string GetComName( string Name )
	{
		return Name.Split( ' ' )[0];
	}
	
	/// WMI取硬件信息
	/// </summary>
	/// <param name="hardType"></param>
	/// <param name="propKey"></param>
	/// <returns></returns>
	static string[] MulGetHardwareInfo( HardwareEnum hardType )
	{
		List<string> strs = new List<string>();
		try {
			using( ManagementObjectSearcher searcher = new ManagementObjectSearcher("select * from " + hardType)) {
				ManagementObjectCollection hardInfos = searcher.Get();
				string propKey = "Name";
				foreach( ManagementObject hardInfo in hardInfos ) {
					if( hardInfo.Properties[propKey].Value != null ) {
						if( hardInfo.Properties[propKey].Value.ToString().Contains( "COM" ) ) {
							strs.Add(hardInfo.Properties[propKey].Value.ToString());
						}
					}
				}
				searcher.Dispose();
			}
			return strs.ToArray();
		}
		catch(Exception e) {
			if( !HasShow ) {
				MessageBox.Show( "linkboy用户您好, 读取串口号的驱动文件名称时出错, 串口号列表中将不显示驱动名称(如CH340等), 只显示串口号(需要重新点击一次串口列表框). " +
			                 "您可以把下边的出错信息发给linkboy团队, 以便得到进一步帮助.\n" +
			                 e.ToString()
			               );
				HasShow = true;
			}
			
			return null;
		}
	}
	
	
	/// <summary>
	/// 枚举win32 api
	/// </summary>
	enum HardwareEnum
	{
		// 硬件
		Win32_Processor, // CPU 处理器
		Win32_PhysicalMemory, // 物理内存条
		Win32_Keyboard, // 键盘
		Win32_PointingDevice, // 点输入设备，包括鼠标。
		Win32_FloppyDrive, // 软盘驱动器
		Win32_DiskDrive, // 硬盘驱动器
		Win32_CDROMDrive, // 光盘驱动器
		Win32_BaseBoard, // 主板
		Win32_BIOS, // BIOS 芯片
		Win32_ParallelPort, // 并口
		Win32_SerialPort, // 串口
		Win32_SerialPortConfiguration, // 串口配置
		Win32_SoundDevice, // 多媒体设置，一般指声卡。
		Win32_SystemSlot, // 主板插槽 (ISA & PCI & AGP)
		Win32_USBController, // USB 控制器
		Win32_NetworkAdapter, // 网络适配器
		Win32_NetworkAdapterConfiguration, // 网络适配器设置
		Win32_Printer, // 打印机
		Win32_PrinterConfiguration, // 打印机设置
		Win32_PrintJob, // 打印机任务
		Win32_TCPIPPrinterPort, // 打印机端口
		Win32_POTSModem, // MODEM
		Win32_POTSModemToSerialPort, // MODEM 端口
		Win32_DesktopMonitor, // 显示器
		Win32_DisplayConfiguration, // 显卡
		Win32_DisplayControllerConfiguration, // 显卡设置
		Win32_VideoController, // 显卡细节。
		Win32_VideoSettings, // 显卡支持的显示模式。
		
		// 操作系统
		Win32_TimeZone, // 时区
		Win32_SystemDriver, // 驱动程序
		Win32_DiskPartition, // 磁盘分区
		Win32_LogicalDisk, // 逻辑磁盘
		Win32_LogicalDiskToPartition, // 逻辑磁盘所在分区及始末位置。
		Win32_LogicalMemoryConfiguration, // 逻辑内存配置
		Win32_PageFile, // 系统页文件信息
		Win32_PageFileSetting, // 页文件设置
		Win32_BootConfiguration, // 系统启动配置
		Win32_ComputerSystem, // 计算机信息简要
		Win32_OperatingSystem, // 操作系统信息
		Win32_StartupCommand, // 系统自动启动程序
		Win32_Service, // 系统安装的服务
		Win32_Group, // 系统管理组
		Win32_GroupUser, // 系统组帐号
		Win32_UserAccount, // 用户帐号
		Win32_Process, // 系统进程
		Win32_Thread, // 系统线程
		Win32_Share, // 共享
		Win32_NetworkClient, // 已安装的网络客户端
		Win32_NetworkProtocol, // 已安装的网络协议
		Win32_PnPEntity,//all device
	}
	/// <summary>
}
}
