﻿
//#define OS_ANDROID
#define OS_WIN


namespace n_OS
{
using System;

#if OS_WIN
	using System.IO;
#endif

#if OS_ANDROID
	using test20140822;
	using Android.Widget;
	using Android.App;
#endif

public static class OS
{
	//系统文件所在根目录
	public static string SystemRoot;
	
	//文件路径的分隔符
	public static string PATH_S;
	
	//文件路径的分隔符
	public static char PATH_SC;
	
	//初始化
	public static void Init()
	{
		//-------------------------------------
		#if OS_ANDROID
			PATH_S = @"/";
			PATH_SC = '/';
			SystemRoot = "";
		#elif OS_WIN
			PATH_S = @"\";
			PATH_SC = '\\';
			SystemRoot = AppDomain.CurrentDomain.BaseDirectory;
		#else
			#error "操作系统未定义"
		#endif
		
		//-------------------------------------
		//输入输出初始化
		VIO.Init();
	}
}
public static class VIO
{
	#if OS_WIN
		static System.Windows.Forms.RichTextBox r;
	#elif OS_ANDROID
		public static Activity ac;
	#endif
	
	//初始化
	public static void Init()
	{
		#if OS_WIN
			r = new System.Windows.Forms.RichTextBox();
		#endif
	}
	
	//打开RTF文件并提取文本
	public static string OpenRtfFile( string FileName )
	{
		r.LoadFile( FileName );
		return r.Text;
	}
	
	//打开文本文件
	public static string OpenTextFileGB2312( string FileName )
	{
		#if OS_WIN
			
			string s = File.ReadAllText( FileName, System.Text.Encoding.GetEncoding("gb2312") );
			return s.Replace( "\r\n", "\n" );
			
		#elif OS_ANDROID
			return A_IO.Open( FileName );
		#endif
	}
	
	//保存文本文件
	public static void SaveTextFileGB2312( string FileName, string Text )
	{
		Text = Text.Replace( "\r\n", "\n" );
		Text = Text.Replace( "\n", "\r\n" );
		System.IO.File.WriteAllText( FileName, Text, System.Text.Encoding.GetEncoding( "gb2312" ) );
	}
	
	//打开文本文件
	public static string OpenTextFileUTF8( string FileName )
	{
		#if OS_WIN
			
			string s = File.ReadAllText( FileName, System.Text.Encoding.GetEncoding("utf-8") );
			return s.Replace( "\r\n", "\n" );
			
		#elif OS_ANDROID
			return A_IO.Open( FileName );
		#endif
	}
	
	//保存文本文件
	public static void SaveTextFileUTF8( string FileName, string Text )
	{
		Text = Text.Replace( "\r\n", "\n" );
		Text = Text.Replace( "\n", "\r\n" );
		
		//保存时带有 BOM 标记
		//System.IO.File.WriteAllText( FileName, Text, System.Text.Encoding.GetEncoding( "utf-8" ) );
		
		//这个也是无BOM格式 - 未测试
		//System.Text.UTF8Encoding utf8 = new System.Text.UTF8Encoding(false);
		//File.WriteAllText(FilePath, strContent, utf8);
		
		//以UTF-8不带BOM格式重新写入文件
		System.Text.Encoding end = new System.Text.UTF8Encoding(false);
		using( StreamWriter sw = new StreamWriter( FileName, false, end) )
		{
			sw.Write( Text );
		}
	}
	
	//判断文件是否存在
	public static bool FileExists( string Path )
	{
		#if OS_WIN
			return File.Exists( Path );
		#elif OS_ANDROID
			return true;
		#else
			#error "操作系统未定义"
		#endif
	}
	
	//输出一个信息
	public static void Show( string m )
	{
		#if OS_WIN
			System.Windows.Forms.MessageBox.Show( "<" + m + ">" );
		#elif OS_ANDROID
			Toast.MakeText( ac, m, Android.Widget.ToastLength.Long ).Show();
		#endif
		
	}
}
}





