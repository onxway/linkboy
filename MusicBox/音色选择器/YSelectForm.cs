﻿
namespace n_YSelectForm
{
	using System;
	using System.Collections.Generic;
	using System.Drawing;
	using System.Drawing.Drawing2D;
	using System.Windows.Forms;
	using c_MIDI;
	using n_MusicFunction;
	using n_OS;
	using c_FormMover;
	
	//****************************************************************
	//乐器选择窗口
	public partial class YSelectForm : Form
	{
		bool isOK;
		FormMover fm;
		
		public int ChannelNumber;
		
		const int Channel = 0;
		const int TestTone = 65;
		const int PerHeight = 28;
		
		string TimbreName;
		int HighIndex, LowIndex;
		
		int LastIndex;
		int baseIndex;
		
		Color Name1SelectColor;
		Color Name2SelectColor;
		Color Name1Color;
		Color Name2Color;
		Color Name1ForeColor;
		Color Name2ForeColor;
		
		string[] NameSet;
		Button[] Name1Button;
		Button[] Name2Button;
		
		//主窗口
		public YSelectForm()
		{
			InitializeComponent();
			HighIndex = 0;
			LowIndex = 0;
			
			fm = new FormMover( this );
			isOK = false;
			ChannelNumber = 2;
			
			NameSet = LoadFile();
			Name1SelectColor = Color.LightBlue;
			Name2SelectColor = Color.LightBlue;
			Name1Color = Color.White;
			Name2Color = Color.White;
			Name1ForeColor = Color.Black;
			Name2ForeColor = Color.Black;
			
			Name1Button = new Button[ 16 ];
			for( int i = 0; i < Name1Button.Length; ++i ) {
				Name1Button[ i ] = new Button();
				Name1Button[ i ].Name = i.ToString();
				Name1Button[ i ].Font = new Font( "微软雅黑", 9, FontStyle.Regular );
				Name1Button[ i ].FlatStyle = FlatStyle.Flat;
				Name1Button[ i ].TextAlign = ContentAlignment.MiddleLeft;
				Name1Button[ i ].Text = NameSet[ i * 9 ];
				Name1Button[ i ].BackColor = Name1Color;
				Name1Button[ i ].ForeColor = Name1ForeColor;
				Name1Button[ i ].Width = 70;
				Name1Button[ i ].Height = PerHeight;
				Name1Button[ i ].Location = new Point( 5, 20 + (PerHeight - 1) * i );
				Name1Button[ i ].Click += new EventHandler( Name1Click );
				groupBox1.Controls.Add( Name1Button[ i ] );
			}
			Name2Button = new Button[ 8 ];
			for( int i = 0; i < Name2Button.Length; ++i ) {
				Name2Button[ i ] = new Button();
				Name2Button[ i ].Name = i.ToString();
				Name2Button[ i ].Font = new Font( "微软雅黑", 12, FontStyle.Regular );
				Name2Button[ i ].FlatStyle = FlatStyle.Flat;
				Name2Button[ i ].TextAlign = ContentAlignment.MiddleLeft;
				Name2Button[ i ].Text = NameSet[ i + 1 ];
				Name2Button[ i ].BackColor = Name2Color;
				Name2Button[ i ].ForeColor = Name2ForeColor;
				Name2Button[ i ].Width = 380;
				Name2Button[ i ].Height = PerHeight*2;
				Name2Button[ i ].Location = new Point( 5, 20 + (PerHeight * 2 - 1) * i );
				Name2Button[ i ].Click += new EventHandler( Name2Click );
				groupBox2.Controls.Add( Name2Button[ i ] );
			}
			Name1Button[ HighIndex ].BackColor = Name1SelectColor;
			Name2Button[ LowIndex ].BackColor = Name2SelectColor;
			TimbreName = NameSet[ HighIndex * 9 + LowIndex + 1 ];
		}
		
		//运行
		public string Run()
		{
			if( ChannelNumber == 1 ) {
				this.radioButton1.Visible = false;
				this.label1.Visible = false;
				this.radioButton2.Visible = false;
				this.label2.Visible = false;
			}
			else {
				this.radioButton2.Visible = true;
				this.label2.Visible = true;
			}
			
			Name1Button[ HighIndex ].BackColor = Name1SelectColor;
			for( int i = 0; i < Name2Button.Length; ++i ) {
				Name2Button[ i ].Text = NameSet[ HighIndex * 9 + i + 1 ];
			}
			//this.Show();
			
			Name1Button[ HighIndex ].BackColor = Name1Color;
			Name2Button[ LowIndex ].BackColor = Name2Color;
			
			this.radioButton1.Checked = true;
			
			LastIndex = -1;
			baseIndex = HighIndex*8 + LowIndex;
			
			//注意,直接设置Visible不能使窗体获得焦点
			//this.Visible = true;
			this.ShowDialog();
			
			while( this.Visible ) {
				System.Windows.Forms.Application.DoEvents();
			}
			if( isOK ) {
				return this.label1.Text + "*" + this.label2.Text;
			}
			return null;
		}
		
		//加载音色名称文件
		string[] LoadFile()
		{
			string r = VIO.OpenTextFileGB2312( OS.SystemRoot + "Resource" + OS.PATH_S + "音色列表.txt" );
			string NameSet = r.Remove( r.IndexOf( "\n<end>" ) ).Trim( '\n' );
			return NameSet.Split( '\n' );
		}
		
		//音色按钮单击事件
		void Name1Click( object sender, EventArgs e )
		{
			if( LastIndex != -1 ) {
				Music.CloseSound( Channel, TestTone );
				LastIndex = -1;
			}
			Name1Button[ HighIndex ].BackColor = Name1Color;
			HighIndex = int.Parse( ( (Button)sender ).Name );
			Name1Button[ HighIndex ].BackColor = Name1SelectColor;
			for( int i = 0; i < Name2Button.Length; ++i ) {
				Name2Button[ i ].Text = NameSet[ HighIndex * 9 + i + 1 ];
			}
		}
		
		//乐器按钮单击事件
		void Name2Click( object sender, EventArgs e )
		{
			Name2Button[ LowIndex ].BackColor = Name2Color;
			LowIndex = int.Parse( ( (Button)sender ).Name );
			Name2Button[ LowIndex ].BackColor = Name2SelectColor;
			
			if( this.radioButton1.Checked ) {
				this.label1.Text = NameSet[ HighIndex * 9 + LowIndex + 1 ];
			}
			else {
				this.label2.Text = NameSet[ HighIndex * 9 + LowIndex + 1 ];
			}
			if( LastIndex != -1 ) {
				Music.CloseSound( Channel, TestTone );
			}
			LastIndex = HighIndex * 8 + LowIndex;
			
			Music.SetTimbre( Channel, LastIndex );
			Music.PlaySound( Channel, TestTone, 127 );
		}
		
		//确定按钮
		void YesButtonClick(object sender, EventArgs e)
		{
			Music.CloseSound( Channel, TestTone );
			//int TimbreIndex = HighIndex * 8 + LowIndex;
			//isShort = MusicFunction.isShortTimbre( TimbreIndex );
			isOK = true;
			
			this.Visible = false;
		}
		
		//取消按钮
		void ButtonNOClick(object sender, EventArgs e)
		{
			Music.CloseSound( Channel, TestTone );
			
			Name1Button[ HighIndex ].BackColor = Name1Color;
			Name2Button[ LowIndex ].BackColor = Name2Color;
			
			isOK = false;
			Music.SetTimbre( Channel, baseIndex );
			this.Visible = false;
		}
	}
}
