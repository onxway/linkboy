﻿
//系统参数
namespace n_MusicSystemData
{
	using System;
	using System.Drawing;
	using System.Collections;
	using System.ComponentModel;
	using System.Windows.Forms;
	using System.Data;
	using System.Runtime.Serialization;
	using System.Runtime.Serialization.Formatters.Binary;
	using System.IO;
	using n_OS;
	
	public static class SystemData
	{
		static string SysDataPath;
		
		//初始化
		public static void Init()
		{
			SysDataPath = "Resource" + OS.PATH_S + "MusicBox" + OS.PATH_S + "系统数据.ser";
			
			isChanged = false;
			Timbre = new int[ 2 ];
		}
		
		//加载对象, 从:  系统数据.ser
		public static void Load()
		{
			stream = File.Open( OS.SystemRoot + SysDataPath, FileMode.Open );
			bin = new BinaryFormatter();
			
			StopAfterKeyUp = (bool)bin.Deserialize( stream );
			SoundIndex = (int)bin.Deserialize( stream );
			Timbre = (int[])bin.Deserialize( stream );
			obbligato_number = (int)bin.Deserialize( stream );
			MainVolume = (int)bin.Deserialize( stream );
			SecondVolume = (int)bin.Deserialize( stream );
			
			stream.Close();
		}
		
		//串行化对象,保存到:  系统数据.ser
		public static void Save()
		{
			stream = File.Open( OS.SystemRoot + SysDataPath, FileMode.Open );
			bin = new BinaryFormatter();
			
			bin.Serialize( stream, StopAfterKeyUp );
			bin.Serialize( stream, SoundIndex );
			bin.Serialize( stream, Timbre );
			bin.Serialize( stream, obbligato_number );
			bin.Serialize( stream, MainVolume );
			bin.Serialize( stream, SecondVolume );
			
			stream.Close();
		}
		
		// 输出文件流对象
		static Stream stream;
		
		// 串行化对象
		static BinaryFormatter bin;
		
		//是否改变
		public static bool isChanged;
		
		//松键停音
		public static bool StopAfterKeyUp;
		//转调序号
		public static int SoundIndex;
		//伴奏区按键数目
		public static int obbligato_number;
		//音色
		public static int[] Timbre;
		//主音量
		public static int MainVolume;
		//伴奏音量
		public static int SecondVolume;
	}
}
	
