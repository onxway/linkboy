﻿
using System;
using n_MusicForm;
using System.Windows.Forms;
using n_OS;
using n_MusicSystemData;
using n_SG;
using n_MusicPanel;

namespace n_Main
{
	class Program
	{
		static string DefaultFilePath;
		public static MusicForm f;
		
		[STAThread]
		public static void Main(string[] args)
		{
			/** 
			 * 当前用户是管理员的时候，直接启动应用程序
			 * 如果不是管理员，则使用启动对象启动程序，以确保使用管理员身份运行
			 */
			//获得当前登录的Windows用户标示
			System.Security.Principal.WindowsIdentity identity = System.Security.Principal.WindowsIdentity.GetCurrent();
			//创建Windows用户主题
			Application.EnableVisualStyles();
			
			System.Security.Principal.WindowsPrincipal principal = new System.Security.Principal.WindowsPrincipal(identity);
			//判断当前登录用户是否为管理员
			if (principal.IsInRole(System.Security.Principal.WindowsBuiltInRole.Administrator)) {
				//如果是管理员，则直接运行
				
				Application.EnableVisualStyles();
				
				//+++++++++++++++++++++++++++++++++++++++++++++
				//判断是否加载文件
				DefaultFilePath = p_Win.CommandLine.GetStartPath( args );
				
				OS.Init();
				SG.Init();
				SG.MString.AddNewFont( "宋体", true );
				
				try {
					f = new MusicForm( DefaultFilePath, true );
					Application.Run( f );
				} catch( Exception e ) {
					MessageBox.Show( e.ToString() );
				}
			}
			else {
				
				//创建启动对象
				System.Diagnostics.ProcessStartInfo startInfo = new System.Diagnostics.ProcessStartInfo();
				//设置运行文件
				startInfo.FileName = System.Windows.Forms.Application.ExecutablePath;
				//设置启动参数
				startInfo.Arguments = String.Join(" ", args);
				//设置启动动作,确保以管理员身份运行
				startInfo.Verb = "runas";
				//如果不是管理员，则启动UAC
				System.Diagnostics.Process.Start(startInfo);
				//退出
				System.Windows.Forms.Application.Exit();
			}
		}
		public static void MusicError( string mes )
		{
			MusicPanel.Error = mes;
		}
		
		public static void MusicTimeWarning( string mes )
		{
			MusicPanel.Warning = mes;
		}
	}
}


