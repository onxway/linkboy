﻿/*
 * 由SharpDevelop创建。
 * 用户： dell
 * 日期: 2016/11/24
 * 时间: 11:10
 * 
 * 要改变这种模板请点击 工具|选项|代码编写|编辑标准头文件
 */
using System;

namespace n_VersionConvert
{
public static class VersionConvert
{
	static Node[] ToneList;
	static int ToneNumber;
	
	//获取音乐名字
	public static string GetVersion( string text )
	{
		if( text.StartsWith( "V1 " ) ) {
			return "V1";
		}
		return "V0";
	}
	
	//获取音乐名字
	public static string GetTitle( string text )
	{
		if( text.StartsWith( "V1 " ) ) {
			
			text = text.Remove( 0, 3 );
			int mesi = text.IndexOf( ";" );
			if( mesi == -1 ) {
				return text;
			}
			return text.Remove( mesi );
		}
		return text;
	}
	
	//获取音乐信息
	public static string GetMes( string text )
	{
		if( text.StartsWith( "V1 " ) ) {
			
			text = text.Remove( 0, 3 );
			int mesi = text.IndexOf( ";" );
			if( mesi == -1 ) {
				return "";
			}
			return text.Remove( 0, mesi + 1 );
		}
		return "";
	}
	
	//转换为新版数据
	public static string Get( string m )
	{
		ToneList = new Node[500];
		DecodeArray( m );
		return "V1 我的音乐\n" + Convert();
	}
	
	//解码音乐数组
	static void DecodeArray( string m )
	{
		if( m == "" ) {
			return;
		}
		//获取音符数组
		string[] cut = m.Replace( '\n', ' ' ).Replace( '\t', ' ' ).Trim( ' ' ).Trim( ',' ).Split( ',' );
		
		//填充音符数组
		int Length = cut.Length;
		int[] DataList = new int[ Length ];
		for( int i = 0; i < cut.Length; ++i ) {
			cut[ i ] = cut[ i ].Trim( ' ' );
			if( cut[ i ].StartsWith( "0x" ) ) {
				DataList[ i ] = int.Parse( cut[ i ].Remove( 0, 2 ), System.Globalization.NumberStyles.HexNumber );
			}
			else {
				DataList[ i ] = int.Parse( cut[ i ] );
			}
		}
		//包装数据到音符列表
		ToneNumber = 0;
		int n = 0;
		while( n < Length ) {
			
			int T = DataList[ n ];
			Node t = new Node( T );
			if( T == Node.t_结束乐谱0 || T == Node.t_结束乐谱1 || T == Node.t_延音 || T == Node.t_连音开始 || T == Node.t_连音结束 ) {
				n += 1;
				ToneList[ ToneNumber ] = t;
				++ToneNumber;
				continue;
			}
			if( T == Node.t_发出音符 || T == Node.t_延时时值 || T == Node.t_设置音色 || T == Node.t_设置时值单位 || T == Node.t_设置移调偏移 || T == Node.t_设置音量 ) {
				t.Value1 = DataList[ n + 1 ];
				n += 2;
				ToneList[ ToneNumber ] = t;
				++ToneNumber;
				continue;
			}
			if( T == Node.t_设置音节时值 ) {
				t.Value1 = DataList[ n + 1 ];
				t.Value2 = DataList[ n + 2 ];
				n += 3;
				ToneList[ ToneNumber ] = t;
				++ToneNumber;
				continue;
			}
			//MessageBox.Show( "<DecodeArray> 未处理的类型: " + T );
			++n;
		}
	}
	
	//转换节点到新版数据格式
	static string Convert()
	{
		int ms = 25;
		int vol = 127;
		
		string r = "";
		
		int PreT = -1;
		int Last = 0;
		
		for( int i = 0; i < ToneNumber; ++i ) {
			
			int T = ToneList[ i ].Type;
			if( T == Node.t_结束乐谱0 || T == Node.t_结束乐谱1 ) {
				break;
			}
			if( T == Node.t_发出音符 ) {
				
				//Music.PlaySound( 0, MusicFunction.StartSoundIndex + ToneList[ i ].Value1 , Volume );
				r += ToneList[ i ].Value1 + ",";
				Last = ToneList[ i ].Value1;
				continue;
			}
			if( T == Node.t_设置音量 ) {
				vol = ToneList[ i ].Value1;
				continue;
			}
			if( T == Node.t_设置时值单位 ) {
				ms = ToneList[ i ].Value1;
				continue;
			}
			if( T == Node.t_延时时值 ) {
				if( PreT == Node.t_延音 ) {
					r += Last + ",";
					PreT = -1;
				}
				//System.Threading.Thread.Sleep( ToneList[ i ].Value1 * PerMS );
				r += ToneList[ i ].Value1 + ",";
				
				continue;
			}
			if( T == Node.t_设置音色 || T == Node.t_设置移调偏移 ) {
				continue;
			}
			if( T == Node.t_连音开始 || T == Node.t_连音结束 ) {
				continue;
			}
			if( T == Node.t_延音 ) {
				PreT = Node.t_延音;
				continue;
			}
			if( T == Node.t_设置音节时值 ) {
				r += "253," + (ToneList[ i ].Value1 + ToneList[ i ].Value2*16) + ",";
				continue;
			}
		}
		return r;
	}
	
	//音乐数据节点
	class Node
	{
		//节点类型
		public int Type;
		
		public const int t_结束乐谱1 =	0xFF;		//结束乐谱:		FF
		public const int t_结束乐谱0 =	0x00;		//结束乐谱:		00
		public const int t_发出音符 =	0xFE;		//发出音符:		FE n
		public const int t_延音 =		0xFD;		//延音:			FD	//延音后边需要带有一个延时时值指令
		public const int t_延时时值 =	0xFC;		//延时时值:		FC n
		public const int t_设置音色 =	0xFB;		//设置音色:		FB n
		public const int t_设置时值单位 = 0xFA;		//设置时值单位:		FA n	//单位是毫秒,一般为20-30左右
		public const int t_设置移调偏移 = 0xF9;		//设置移调偏移:		F9 n
		public const int t_设置音节时值 = 0xF8;		//设置音节时值:		F8 n m	//以m分音符为一拍,每小节有n拍
		public const int t_设置音量 =	0xF6;		//设置音量:		F6 n
		public const int t_连音开始 =	0xF5;		//连音开始:		F5	//在 F5 和 F4 之间的音符连续播放（待定。。。）
		public const int t_连音结束 =	0xF4;		//连音结束:		F4
		
		public int Value1;
		public int Value2;
		
		//构造函数
		public Node( int t )
		{
			Type = t;
		}
	}
}
}
