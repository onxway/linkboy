﻿
namespace n_MusicFunction
{
using System;
using System.Windows.Forms;
using System.Runtime.InteropServices;

//*******************************************************
//MIDI类
public static class MusicFunction
{
	//判断一个乐器是否为短音(渐变弱)
	public static bool isShortTimbre( int TimbreIndex )
	{
		if( TimbreIndex < 16 ||
			TimbreIndex >= 24 && TimbreIndex <= 28 ||
			TimbreIndex >= 31 && TimbreIndex <= 38 ||
			TimbreIndex >= 45 && TimbreIndex <= 47 ||
			TimbreIndex == 55 ||
			TimbreIndex >= 104 && TimbreIndex <= 108 ||
			TimbreIndex >= 112 && TimbreIndex <= 119 ||
			TimbreIndex == 127 ) {
			return true;
		}
		else {
			return false;
		}
	}
	
	//判断一个键值的音名
	public static string GetToneName( int Index )
	{
		switch( Index % 12 ) {
				case 0: return "C";
				case 2: return "D";
				case 4: return "E";
				case 5: return "F";
				case 7: return "G";
				case 9: return "A";
				case 11: return "B";
				default: return null;
		}
	}
	
	//判断一个键值是白键还是黑键
	public static bool isWhiteKey( int Index )
	{
		int Mod = Index % 12;
		if( ( Mod == 1 ) || ( Mod == 3 ) || ( Mod == 6 ) || ( Mod == 8 ) || ( Mod == 10 ) ) {
			return false;
		}
		return true;
	}
	
	//获取一个音符的简谱序号, 1-7
	public static int GetSimpleNumer( int ToneIndex, ref int offset )
	{
		if( ToneIndex == 0 ) {
			return 0;
		}
		switch( ToneIndex % 12 ) {
			case 0: 				return 1;
			case 1: offset = -1;	return 2;
			case 2: 				return 2;
			case 3: offset = -1;	return 3;
			case 4: 				return 3;
			case 5:					return 4;
			case 6: offset = -1;	return 5;
			case 7: 				return 5;
			case 8: offset = -1;	return 6;
			case 9: 				return 6;
			case 10: offset = -1;	return 7;
			case 11: 				return 7;
			default:				return 0;
		}
	}
	
	//获取一个音符的简谱级别
	public static int GetLevel( int ToneIndex )
	{
		if( ToneIndex == 0 ) {
			return 5;
		}
		return ToneIndex / 12;
	}
	
	//获取一个n分音符的时值,全音符输入为1,二分音符输入为2,四分音符输入为4,...
	//对照表:
	//n分音符	时值
	//1			128
	//2			64
	//4			32
	//8			16
	//16		8
	//32		4
	//64		2
	//128		1
	public static int GetNoteTime( int Note )
	{
		switch( Note ) {
			case 1:		return 128;
			case 2:		return 64;
			case 4:		return 32;
			case 8:		return 16;
			case 16:	return 8;
			case 32:	return 4;
			case 64:	return 2;
			case 128:	return 1;
		}
		return -1;
	}
	
	//获取一个时值的长度, 返回 1 ~ 4
	//获取一个时值占据的显示单元的数目
	public static int NumberOfTime( int Time )
	{
		switch( Time ) {
			case 1: return 1;
			case 2: return 1;
			case 3: return 2;
			case 4: return 1;
			case 6: return 2;
			case 8: return 1;
			case 12: return 2;
			case 16: return 1;
			case 24: return 2;
			case 32: return 1;
			case 48: return 2;
			case 64: return 2;
			case 96: return 3;
			case 128: return 4;
			default: n_Main.Program.MusicError( "不支持的时值: " + Time ); return 1;
		}
	}
}
}

