﻿/*
 * 由 SharpDevelop 创建。
 * 用户: bbb_2
 * 日期: 2009-11-17
 * 时间: 11:15
 * 
 * 如果你想要更改该模板，那么请使用“工具 | 选项 | 编辑 | 编辑标准标题”。
 */
namespace n_MusicForm
{
	partial class MusicForm
	{
		/// <summary>
		/// Designer variable used to keep track of non-visual components.
		/// </summary>
		private System.ComponentModel.IContainer components = null;
		
		/// <summary>
		/// Disposes resources used by the form.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing) {
				if (components != null) {
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}
		
		/// <summary>
		/// This method is required for Windows Forms designer support.
		/// Do not change the method contents inside the source code editor. The Forms designer might
		/// not be able to load this method if it was changed manually.
		/// </summary>
		private void InitializeComponent()
		{
			this.components = new System.ComponentModel.Container();
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MusicForm));
			this.button选择音色 = new System.Windows.Forms.Button();
			this.trackBar1 = new System.Windows.Forms.TrackBar();
			this.button休止 = new System.Windows.Forms.Button();
			this.textBox移调偏移 = new System.Windows.Forms.TextBox();
			this.label3 = new System.Windows.Forms.Label();
			this.textBox时值单位 = new System.Windows.Forms.TextBox();
			this.button开始记录 = new System.Windows.Forms.Button();
			this.textBox歌曲名称 = new System.Windows.Forms.TextBox();
			this.textBox音符 = new System.Windows.Forms.TextBox();
			this.textBox拍数 = new System.Windows.Forms.TextBox();
			this.button删除 = new System.Windows.Forms.Button();
			this.checkBox显示网格 = new System.Windows.Forms.CheckBox();
			this.trackBarMainVolume = new System.Windows.Forms.TrackBar();
			this.panelImage = new System.Windows.Forms.Panel();
			this.panel2 = new System.Windows.Forms.Panel();
			this.trackBarSecondVolume = new System.Windows.Forms.TrackBar();
			this.label1 = new System.Windows.Forms.Label();
			this.buttonOpen = new System.Windows.Forms.Button();
			this.buttonSave = new System.Windows.Forms.Button();
			this.labelJNumber = new System.Windows.Forms.Label();
			this.button确定 = new System.Windows.Forms.Button();
			this.panel5 = new System.Windows.Forms.Panel();
			this.buttonDown = new System.Windows.Forms.Button();
			this.buttonUp = new System.Windows.Forms.Button();
			this.labelTime = new System.Windows.Forms.Label();
			this.checkBoxLianYin = new System.Windows.Forms.CheckBox();
			this.label10 = new System.Windows.Forms.Label();
			this.label8 = new System.Windows.Forms.Label();
			this.label6 = new System.Windows.Forms.Label();
			this.label5 = new System.Windows.Forms.Label();
			this.label4 = new System.Windows.Forms.Label();
			this.buttonStop = new System.Windows.Forms.Button();
			this.button播放 = new System.Windows.Forms.Button();
			this.trackBarJNumber = new System.Windows.Forms.TrackBar();
			this.label7 = new System.Windows.Forms.Label();
			this.label9 = new System.Windows.Forms.Label();
			this.checkBox滑动模式 = new System.Windows.Forms.CheckBox();
			this.timerPlay = new System.Windows.Forms.Timer(this.components);
			this.panel1 = new System.Windows.Forms.Panel();
			this.panel4 = new System.Windows.Forms.Panel();
			this.panel3 = new System.Windows.Forms.Panel();
			this.trackBar2 = new System.Windows.Forms.TrackBar();
			this.radioButtonC2 = new System.Windows.Forms.RadioButton();
			this.radioButtonC1 = new System.Windows.Forms.RadioButton();
			this.label2 = new System.Windows.Forms.Label();
			this.label12 = new System.Windows.Forms.Label();
			((System.ComponentModel.ISupportInitialize)(this.trackBar1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.trackBarMainVolume)).BeginInit();
			this.panel2.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.trackBarSecondVolume)).BeginInit();
			this.panel5.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.trackBarJNumber)).BeginInit();
			this.panel1.SuspendLayout();
			this.panel3.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.trackBar2)).BeginInit();
			this.SuspendLayout();
			// 
			// button选择音色
			// 
			this.button选择音色.BackColor = System.Drawing.Color.CornflowerBlue;
			resources.ApplyResources(this.button选择音色, "button选择音色");
			this.button选择音色.ForeColor = System.Drawing.Color.White;
			this.button选择音色.Name = "button选择音色";
			this.button选择音色.UseVisualStyleBackColor = false;
			this.button选择音色.Click += new System.EventHandler(this.Button选择音色Click);
			// 
			// trackBar1
			// 
			this.trackBar1.BackColor = System.Drawing.Color.White;
			resources.ApplyResources(this.trackBar1, "trackBar1");
			this.trackBar1.Maximum = 127;
			this.trackBar1.Name = "trackBar1";
			this.trackBar1.TickStyle = System.Windows.Forms.TickStyle.None;
			this.trackBar1.Value = 127;
			this.trackBar1.Scroll += new System.EventHandler(this.TrackBar1Scroll);
			// 
			// button休止
			// 
			this.button休止.BackColor = System.Drawing.Color.CornflowerBlue;
			resources.ApplyResources(this.button休止, "button休止");
			this.button休止.ForeColor = System.Drawing.Color.White;
			this.button休止.Name = "button休止";
			this.button休止.UseVisualStyleBackColor = false;
			this.button休止.Click += new System.EventHandler(this.Button休止Click);
			// 
			// textBox移调偏移
			// 
			this.textBox移调偏移.BackColor = System.Drawing.Color.White;
			resources.ApplyResources(this.textBox移调偏移, "textBox移调偏移");
			this.textBox移调偏移.Name = "textBox移调偏移";
			this.textBox移调偏移.TextChanged += new System.EventHandler(this.MessageChanged);
			// 
			// label3
			// 
			this.label3.BackColor = System.Drawing.Color.Transparent;
			resources.ApplyResources(this.label3, "label3");
			this.label3.ForeColor = System.Drawing.Color.Black;
			this.label3.Name = "label3";
			// 
			// textBox时值单位
			// 
			this.textBox时值单位.BackColor = System.Drawing.Color.White;
			resources.ApplyResources(this.textBox时值单位, "textBox时值单位");
			this.textBox时值单位.Name = "textBox时值单位";
			this.textBox时值单位.TextChanged += new System.EventHandler(this.MessageChanged);
			// 
			// button开始记录
			// 
			this.button开始记录.BackColor = System.Drawing.Color.CornflowerBlue;
			resources.ApplyResources(this.button开始记录, "button开始记录");
			this.button开始记录.ForeColor = System.Drawing.Color.White;
			this.button开始记录.Name = "button开始记录";
			this.button开始记录.UseVisualStyleBackColor = false;
			this.button开始记录.Click += new System.EventHandler(this.Button开始记录Click);
			// 
			// textBox歌曲名称
			// 
			this.textBox歌曲名称.AcceptsReturn = true;
			resources.ApplyResources(this.textBox歌曲名称, "textBox歌曲名称");
			this.textBox歌曲名称.Name = "textBox歌曲名称";
			this.textBox歌曲名称.TextChanged += new System.EventHandler(this.MessageChanged);
			// 
			// textBox音符
			// 
			this.textBox音符.BackColor = System.Drawing.Color.White;
			resources.ApplyResources(this.textBox音符, "textBox音符");
			this.textBox音符.ForeColor = System.Drawing.Color.DarkRed;
			this.textBox音符.Name = "textBox音符";
			this.textBox音符.TextChanged += new System.EventHandler(this.MessageChanged);
			// 
			// textBox拍数
			// 
			this.textBox拍数.BackColor = System.Drawing.Color.White;
			resources.ApplyResources(this.textBox拍数, "textBox拍数");
			this.textBox拍数.ForeColor = System.Drawing.Color.DarkRed;
			this.textBox拍数.Name = "textBox拍数";
			this.textBox拍数.TextChanged += new System.EventHandler(this.MessageChanged);
			// 
			// button删除
			// 
			this.button删除.BackColor = System.Drawing.Color.CornflowerBlue;
			resources.ApplyResources(this.button删除, "button删除");
			this.button删除.ForeColor = System.Drawing.Color.White;
			this.button删除.Name = "button删除";
			this.button删除.UseVisualStyleBackColor = false;
			this.button删除.Click += new System.EventHandler(this.Button删除Click);
			// 
			// checkBox显示网格
			// 
			resources.ApplyResources(this.checkBox显示网格, "checkBox显示网格");
			this.checkBox显示网格.ForeColor = System.Drawing.Color.Black;
			this.checkBox显示网格.Name = "checkBox显示网格";
			this.checkBox显示网格.UseVisualStyleBackColor = true;
			this.checkBox显示网格.CheckedChanged += new System.EventHandler(this.CheckBox显示网格CheckedChanged);
			// 
			// trackBarMainVolume
			// 
			this.trackBarMainVolume.BackColor = System.Drawing.Color.White;
			resources.ApplyResources(this.trackBarMainVolume, "trackBarMainVolume");
			this.trackBarMainVolume.Maximum = 127;
			this.trackBarMainVolume.Name = "trackBarMainVolume";
			this.trackBarMainVolume.TickStyle = System.Windows.Forms.TickStyle.None;
			this.trackBarMainVolume.Value = 127;
			this.trackBarMainVolume.Scroll += new System.EventHandler(this.MessageChanged);
			// 
			// panelImage
			// 
			resources.ApplyResources(this.panelImage, "panelImage");
			this.panelImage.BackColor = System.Drawing.Color.SlateGray;
			this.panelImage.Name = "panelImage";
			this.panelImage.SizeChanged += new System.EventHandler(this.PanelImageSizeChanged);
			// 
			// panel2
			// 
			this.panel2.BackColor = System.Drawing.Color.White;
			this.panel2.Controls.Add(this.textBox移调偏移);
			this.panel2.Controls.Add(this.label12);
			this.panel2.Controls.Add(this.label2);
			this.panel2.Controls.Add(this.label1);
			this.panel2.Controls.Add(this.checkBox滑动模式);
			this.panel2.Controls.Add(this.textBox拍数);
			this.panel2.Controls.Add(this.textBox音符);
			this.panel2.Controls.Add(this.buttonOpen);
			this.panel2.Controls.Add(this.buttonSave);
			this.panel2.Controls.Add(this.labelJNumber);
			this.panel2.Controls.Add(this.button确定);
			this.panel2.Controls.Add(this.panel5);
			this.panel2.Controls.Add(this.label10);
			this.panel2.Controls.Add(this.button开始记录);
			this.panel2.Controls.Add(this.label8);
			this.panel2.Controls.Add(this.label6);
			this.panel2.Controls.Add(this.label5);
			this.panel2.Controls.Add(this.trackBar1);
			this.panel2.Controls.Add(this.label4);
			this.panel2.Controls.Add(this.checkBox显示网格);
			this.panel2.Controls.Add(this.buttonStop);
			this.panel2.Controls.Add(this.button播放);
			this.panel2.Controls.Add(this.button选择音色);
			this.panel2.Controls.Add(this.textBox歌曲名称);
			this.panel2.Controls.Add(this.textBox时值单位);
			this.panel2.Controls.Add(this.label3);
			this.panel2.Controls.Add(this.trackBarJNumber);
			this.panel2.Controls.Add(this.label7);
			this.panel2.Controls.Add(this.label9);
			this.panel2.Controls.Add(this.trackBarSecondVolume);
			this.panel2.Controls.Add(this.trackBarMainVolume);
			resources.ApplyResources(this.panel2, "panel2");
			this.panel2.Name = "panel2";
			// 
			// trackBarSecondVolume
			// 
			this.trackBarSecondVolume.BackColor = System.Drawing.Color.White;
			resources.ApplyResources(this.trackBarSecondVolume, "trackBarSecondVolume");
			this.trackBarSecondVolume.Maximum = 127;
			this.trackBarSecondVolume.Name = "trackBarSecondVolume";
			this.trackBarSecondVolume.TickStyle = System.Windows.Forms.TickStyle.None;
			this.trackBarSecondVolume.Value = 80;
			this.trackBarSecondVolume.Scroll += new System.EventHandler(this.MessageChanged);
			// 
			// label1
			// 
			this.label1.BackColor = System.Drawing.Color.Transparent;
			resources.ApplyResources(this.label1, "label1");
			this.label1.ForeColor = System.Drawing.Color.Black;
			this.label1.Name = "label1";
			// 
			// buttonOpen
			// 
			this.buttonOpen.BackColor = System.Drawing.Color.CornflowerBlue;
			resources.ApplyResources(this.buttonOpen, "buttonOpen");
			this.buttonOpen.ForeColor = System.Drawing.Color.White;
			this.buttonOpen.Name = "buttonOpen";
			this.buttonOpen.UseVisualStyleBackColor = false;
			this.buttonOpen.Click += new System.EventHandler(this.ButtonOpenClick);
			// 
			// buttonSave
			// 
			this.buttonSave.BackColor = System.Drawing.Color.CornflowerBlue;
			resources.ApplyResources(this.buttonSave, "buttonSave");
			this.buttonSave.ForeColor = System.Drawing.Color.White;
			this.buttonSave.Name = "buttonSave";
			this.buttonSave.UseVisualStyleBackColor = false;
			this.buttonSave.Click += new System.EventHandler(this.ButtonSaveClick);
			// 
			// labelJNumber
			// 
			this.labelJNumber.BackColor = System.Drawing.Color.Transparent;
			resources.ApplyResources(this.labelJNumber, "labelJNumber");
			this.labelJNumber.ForeColor = System.Drawing.Color.Black;
			this.labelJNumber.Name = "labelJNumber";
			// 
			// button确定
			// 
			this.button确定.BackColor = System.Drawing.Color.CornflowerBlue;
			resources.ApplyResources(this.button确定, "button确定");
			this.button确定.ForeColor = System.Drawing.Color.White;
			this.button确定.Name = "button确定";
			this.button确定.UseVisualStyleBackColor = false;
			this.button确定.Click += new System.EventHandler(this.Button确定Click);
			// 
			// panel5
			// 
			this.panel5.BackColor = System.Drawing.Color.WhiteSmoke;
			this.panel5.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
			this.panel5.Controls.Add(this.buttonDown);
			this.panel5.Controls.Add(this.buttonUp);
			this.panel5.Controls.Add(this.labelTime);
			this.panel5.Controls.Add(this.checkBoxLianYin);
			this.panel5.Controls.Add(this.button删除);
			resources.ApplyResources(this.panel5, "panel5");
			this.panel5.Name = "panel5";
			// 
			// buttonDown
			// 
			this.buttonDown.BackColor = System.Drawing.Color.CornflowerBlue;
			resources.ApplyResources(this.buttonDown, "buttonDown");
			this.buttonDown.ForeColor = System.Drawing.Color.White;
			this.buttonDown.Name = "buttonDown";
			this.buttonDown.UseVisualStyleBackColor = false;
			this.buttonDown.Click += new System.EventHandler(this.ButtonDownClick);
			// 
			// buttonUp
			// 
			this.buttonUp.BackColor = System.Drawing.Color.CornflowerBlue;
			resources.ApplyResources(this.buttonUp, "buttonUp");
			this.buttonUp.ForeColor = System.Drawing.Color.White;
			this.buttonUp.Name = "buttonUp";
			this.buttonUp.UseVisualStyleBackColor = false;
			this.buttonUp.Click += new System.EventHandler(this.ButtonUpClick);
			// 
			// labelTime
			// 
			this.labelTime.BackColor = System.Drawing.Color.White;
			this.labelTime.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
			resources.ApplyResources(this.labelTime, "labelTime");
			this.labelTime.Name = "labelTime";
			// 
			// checkBoxLianYin
			// 
			this.checkBoxLianYin.BackColor = System.Drawing.Color.Transparent;
			resources.ApplyResources(this.checkBoxLianYin, "checkBoxLianYin");
			this.checkBoxLianYin.ForeColor = System.Drawing.Color.Black;
			this.checkBoxLianYin.Name = "checkBoxLianYin";
			this.checkBoxLianYin.UseVisualStyleBackColor = false;
			this.checkBoxLianYin.CheckedChanged += new System.EventHandler(this.CheckBoxLianYinCheckedChanged);
			// 
			// label10
			// 
			this.label10.BackColor = System.Drawing.Color.Transparent;
			resources.ApplyResources(this.label10, "label10");
			this.label10.ForeColor = System.Drawing.Color.Black;
			this.label10.Name = "label10";
			// 
			// label8
			// 
			this.label8.BackColor = System.Drawing.Color.Transparent;
			resources.ApplyResources(this.label8, "label8");
			this.label8.ForeColor = System.Drawing.Color.Black;
			this.label8.Name = "label8";
			// 
			// label6
			// 
			this.label6.BackColor = System.Drawing.Color.Transparent;
			resources.ApplyResources(this.label6, "label6");
			this.label6.ForeColor = System.Drawing.Color.Black;
			this.label6.Name = "label6";
			// 
			// label5
			// 
			this.label5.BackColor = System.Drawing.Color.Transparent;
			resources.ApplyResources(this.label5, "label5");
			this.label5.ForeColor = System.Drawing.Color.Black;
			this.label5.Name = "label5";
			// 
			// label4
			// 
			this.label4.BackColor = System.Drawing.Color.Transparent;
			resources.ApplyResources(this.label4, "label4");
			this.label4.ForeColor = System.Drawing.Color.Black;
			this.label4.Name = "label4";
			// 
			// buttonStop
			// 
			this.buttonStop.BackColor = System.Drawing.Color.CornflowerBlue;
			resources.ApplyResources(this.buttonStop, "buttonStop");
			this.buttonStop.ForeColor = System.Drawing.Color.White;
			this.buttonStop.Name = "buttonStop";
			this.buttonStop.UseVisualStyleBackColor = false;
			this.buttonStop.Click += new System.EventHandler(this.ButtonStopClick);
			// 
			// button播放
			// 
			this.button播放.BackColor = System.Drawing.Color.CornflowerBlue;
			resources.ApplyResources(this.button播放, "button播放");
			this.button播放.ForeColor = System.Drawing.Color.White;
			this.button播放.Name = "button播放";
			this.button播放.UseVisualStyleBackColor = false;
			this.button播放.Click += new System.EventHandler(this.Button播放Click);
			// 
			// trackBarJNumber
			// 
			this.trackBarJNumber.BackColor = System.Drawing.Color.White;
			resources.ApplyResources(this.trackBarJNumber, "trackBarJNumber");
			this.trackBarJNumber.Maximum = 30;
			this.trackBarJNumber.Minimum = 1;
			this.trackBarJNumber.Name = "trackBarJNumber";
			this.trackBarJNumber.TickStyle = System.Windows.Forms.TickStyle.None;
			this.trackBarJNumber.Value = 7;
			this.trackBarJNumber.Scroll += new System.EventHandler(this.TrackBarJNumberScroll);
			// 
			// label7
			// 
			this.label7.BackColor = System.Drawing.Color.Transparent;
			resources.ApplyResources(this.label7, "label7");
			this.label7.ForeColor = System.Drawing.Color.Black;
			this.label7.Name = "label7";
			// 
			// label9
			// 
			this.label9.BackColor = System.Drawing.Color.Transparent;
			resources.ApplyResources(this.label9, "label9");
			this.label9.ForeColor = System.Drawing.Color.Black;
			this.label9.Name = "label9";
			// 
			// checkBox滑动模式
			// 
			this.checkBox滑动模式.BackColor = System.Drawing.Color.Transparent;
			this.checkBox滑动模式.ForeColor = System.Drawing.Color.Black;
			resources.ApplyResources(this.checkBox滑动模式, "checkBox滑动模式");
			this.checkBox滑动模式.Name = "checkBox滑动模式";
			this.checkBox滑动模式.UseVisualStyleBackColor = false;
			this.checkBox滑动模式.CheckedChanged += new System.EventHandler(this.CheckBox滑动模式CheckedChanged);
			// 
			// timerPlay
			// 
			this.timerPlay.Interval = 25;
			this.timerPlay.Tick += new System.EventHandler(this.TimerPlayTick);
			// 
			// panel1
			// 
			this.panel1.BackColor = System.Drawing.Color.Silver;
			this.panel1.Controls.Add(this.panel4);
			this.panel1.Controls.Add(this.panel3);
			resources.ApplyResources(this.panel1, "panel1");
			this.panel1.Name = "panel1";
			// 
			// panel4
			// 
			resources.ApplyResources(this.panel4, "panel4");
			this.panel4.Name = "panel4";
			// 
			// panel3
			// 
			this.panel3.BackColor = System.Drawing.Color.WhiteSmoke;
			this.panel3.Controls.Add(this.trackBar2);
			this.panel3.Controls.Add(this.radioButtonC2);
			this.panel3.Controls.Add(this.radioButtonC1);
			this.panel3.Controls.Add(this.button休止);
			resources.ApplyResources(this.panel3, "panel3");
			this.panel3.Name = "panel3";
			// 
			// trackBar2
			// 
			this.trackBar2.BackColor = System.Drawing.Color.WhiteSmoke;
			resources.ApplyResources(this.trackBar2, "trackBar2");
			this.trackBar2.Maximum = 700;
			this.trackBar2.Name = "trackBar2";
			this.trackBar2.TickStyle = System.Windows.Forms.TickStyle.None;
			this.trackBar2.Value = 350;
			this.trackBar2.Scroll += new System.EventHandler(this.TrackBar2Scroll);
			// 
			// radioButtonC2
			// 
			resources.ApplyResources(this.radioButtonC2, "radioButtonC2");
			this.radioButtonC2.Name = "radioButtonC2";
			this.radioButtonC2.UseVisualStyleBackColor = true;
			// 
			// radioButtonC1
			// 
			this.radioButtonC1.Checked = true;
			resources.ApplyResources(this.radioButtonC1, "radioButtonC1");
			this.radioButtonC1.Name = "radioButtonC1";
			this.radioButtonC1.TabStop = true;
			this.radioButtonC1.UseVisualStyleBackColor = true;
			// 
			// label2
			// 
			this.label2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(241)))), ((int)(((byte)(241)))), ((int)(((byte)(241)))));
			resources.ApplyResources(this.label2, "label2");
			this.label2.Name = "label2";
			// 
			// label12
			// 
			this.label12.BackColor = System.Drawing.Color.Transparent;
			resources.ApplyResources(this.label12, "label12");
			this.label12.ForeColor = System.Drawing.Color.Black;
			this.label12.Name = "label12";
			// 
			// MusicForm
			// 
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None;
			resources.ApplyResources(this, "$this");
			this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
			this.Controls.Add(this.panelImage);
			this.Controls.Add(this.panel2);
			this.Controls.Add(this.panel1);
			this.Name = "MusicForm";
			this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.SetIDEFormFormClosing);
			this.Load += new System.EventHandler(this.MusicFormLoad);
			((System.ComponentModel.ISupportInitialize)(this.trackBar1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.trackBarMainVolume)).EndInit();
			this.panel2.ResumeLayout(false);
			this.panel2.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.trackBarSecondVolume)).EndInit();
			this.panel5.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.trackBarJNumber)).EndInit();
			this.panel1.ResumeLayout(false);
			this.panel3.ResumeLayout(false);
			this.panel3.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.trackBar2)).EndInit();
			this.ResumeLayout(false);
		}
		private System.Windows.Forms.Label label12;
		private System.Windows.Forms.Label label2;
		private System.Windows.Forms.TrackBar trackBar2;
		private System.Windows.Forms.Panel panel4;
		private System.Windows.Forms.Label label1;
		private System.Windows.Forms.TrackBar trackBarSecondVolume;
		private System.Windows.Forms.RadioButton radioButtonC1;
		private System.Windows.Forms.RadioButton radioButtonC2;
		private System.Windows.Forms.Button buttonSave;
		private System.Windows.Forms.Button buttonOpen;
		private System.Windows.Forms.Label labelJNumber;
		private System.Windows.Forms.Panel panel5;
		private System.Windows.Forms.Label label10;
		private System.Windows.Forms.Label label9;
		private System.Windows.Forms.Label label7;
		private System.Windows.Forms.Label label5;
		private System.Windows.Forms.Label label6;
		private System.Windows.Forms.Label label8;
		private System.Windows.Forms.Label label4;
		private System.Windows.Forms.Panel panel3;
		private System.Windows.Forms.CheckBox checkBoxLianYin;
		private System.Windows.Forms.TrackBar trackBarJNumber;
		private System.Windows.Forms.Label labelTime;
		private System.Windows.Forms.Panel panel1;
		private System.Windows.Forms.Button buttonDown;
		private System.Windows.Forms.Button buttonUp;
		public System.Windows.Forms.Timer timerPlay;
		private System.Windows.Forms.Button buttonStop;
		private System.Windows.Forms.Button button播放;
		private System.Windows.Forms.CheckBox checkBox滑动模式;
		private System.Windows.Forms.Button button确定;
		private System.Windows.Forms.Panel panel2;
		private System.Windows.Forms.Panel panelImage;
		private System.Windows.Forms.TrackBar trackBarMainVolume;
		private System.Windows.Forms.TextBox textBox歌曲名称;
		private System.Windows.Forms.TextBox textBox拍数;
		private System.Windows.Forms.TextBox textBox音符;
		private System.Windows.Forms.Button button删除;
		private System.Windows.Forms.CheckBox checkBox显示网格;
		private System.Windows.Forms.TextBox textBox时值单位;
		private System.Windows.Forms.TextBox textBox移调偏移;
		private System.Windows.Forms.Button button开始记录;
		private System.Windows.Forms.Label label3;
		private System.Windows.Forms.Button button休止;
		private System.Windows.Forms.TrackBar trackBar1;
		private System.Windows.Forms.Button button选择音色;
	}
}
