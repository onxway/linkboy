﻿
//命名空间
using System;
using System.Windows.Forms;
using linkboy;
using n_AVRdude;
using n_Compiler;
using n_ControlCenter;
using n_FlashForm;
using n_GUIcoder;
using n_ISP;
using n_LaterFilesManager;
using n_MainSystemData;
using n_NoteForm;
using n_OS;
using n_SetIDEForm;
//using n_SSHLoaderForm;
using n_StartForm;
using n_TextForm;
using n_ValueTestForm;
using n_STlinkForm;
using n_EMesForm;

//主程序类
public static class A
{
	//当系统需要打开一个文件时就会触发这个委托, 例如错误列表点击时
	public delegate void D_AddNewFile( string Filename );
	public static D_AddNewFile AddNewFile;
	static void D_AddNewFileDefault( string s )
	{
		//添加到近期文档列表
		LaterFilesManager.AddFile( s );
	}
	
	//public const int VersionNumberYear = 2013;
	//public const int VersionNumberMonth = 7;
	//public const int VersionNumberDay = 5;
	
	public const int VersionNumberYear = 2013;
	public const int VersionNumberMonth = 10;
	public const int VersionNumberDay = 21;
	
	//启动文件
	public static string StartFileName;
	
	//启动器界面
	public static StartForm StartBox;
	
	//小精灵界面
	public static FlashForm CodeErrBox;
	//文本框窗体
	public static TextForm TextBox;
	//描述框窗体
	public static NoteForm NoteBox;
	
	//参数测试窗体
	public static ValueTestForm ValueTestBox;
	
	//设置开发环境界面
	public static SetIDEForm SetIDEBox;
	
	//组件库配置器
	public static ModuleLibMng ModuleLibMngBox;
	
	//Vex文件SSH下载器
	//public static SSHLoaderForm SSHLoaderBox;
	//操作系统安装器
	public static STlinkForm STlinkBox;
	
	//错误信息提示
	public static EMesForm EMesBox;
	
	
	//初始化
	public static string Init( string[] args )
	{
		StartFileName = null;
		
		//try {
		
		//判断是否加载文件
		StartFileName = p_Win.CommandLine.GetStartPath( args );
		
		//初始化底层移植库
		OS.Init();
		
		
		if( OS.USER == "hoperun" ) {
			string[] ext = OS.ExtMessage.Split( '.' );
			DateTime dt = new DateTime( int.Parse(ext[0]), int.Parse(ext[1]), int.Parse(ext[2]) );
			TimeSpan ts = dt.Subtract( DateTime.Now );
			
			if( dt.CompareTo(DateTime.Now) < 0  ) {
				MessageBox.Show( "授权已过期, 如需继续使用请联系我们" );
				return "ERROR";
			}
			else {
				MessageBox.Show( "当前软件为演示版, 仅供内部评估使用, 剩余有效期: " + (ts.Days) + "天" );
			}
		}
		
		
		G.AutoRunMode = false;
		
		if( StartFileName != null && StartFileName.StartsWith( "*" ) ) {
			string[] ss = StartFileName.Split( '*' );
			
			//判断是否代码模式
			if( ss[1] == "Code" || ss[1] == "Crux" ) {
				n_ModuleLibPanel.MyModuleLibPanel.LabType = n_GUIcoder.labType.Code;
				if( ss[1] == "Crux" ) {
					G.SetCruxMode();
				}
			}
			if( ss[1] == "Circuit" ) {
				//G.isCruxEXE = true;
				//G.isCV = true;
				//n_SST.SST.ModeC = false;
				//n_SST.SST.ModeV = true;
				n_ModuleLibPanel.MyModuleLibPanel.LabType = n_GUIcoder.labType.Circuit;
			}
			
			//判断是否为自动运行模式启动 - 可能是动画游戏运行模式
			if( ss[1] == "autorun" ) {
				G.AutoRunMode = true;
			}
			
			StartFileName = ss[2];
			if( StartFileName == "" ) {
				StartFileName = null;
			}
		}
		
		if( StartFileName != null && StartFileName.ToLower().EndsWith( "." + OS.CruxFile ) ) {
			G.SetCruxMode();
		}
		
		//判断是否为自动仿真运行模式
		if( G.AutoRunMode ) {
			//string FullMes0 =
			//	"初始化世界...\n";
			//StartBox.SetFullMes( FullMes0, 10 );
			//StartBox.Run();
			SystemData.Init();
			SystemData.Load();
			Compiler.FileLoadInit();
			ControlCenter.Init();
			ResourceList.Init();
			AVRdude.Init();
			G.InitMes = GInitMes;
			G.Init();
			G.SimBox.Text = System.IO.Path.GetFileNameWithoutExtension( StartFileName );
			//StartBox.Visible = false;
			//StartBox.Dispose();
			return StartFileName;
		}
		
		StartBox = new StartForm();
		string FullMes =
			"初始化运行环境...\n" +
			"初始化编译器...\n" +
			"初始化工作台开发框架...\n" +
			"建立外围辅助插件...\n" +
			"载入图形界面控件系统...\n" +
			"加载模块库  ■■■■■■■■■■";
		StartBox.SetFullMes( FullMes, 18 );
		StartBox.Run();
		//System.Threading.Thread.Sleep( 100 );
		
		StartBox.AddMessage( "初始化运行环境...\n" );
		System.Threading.Thread.Sleep( 100 );
		
		//初始化委托
		AddNewFile += A.D_AddNewFileDefault;
		G.SystemError = Exit;
		
		//+++++++++++++++++++++++++++++++++++++++++++++
		//系统数据初始化
		SystemData.Init();
		SystemData.Load();
		
		//+++++++++++++++++++++++++++++++++++++++++++++
		//判断是否自动编译模式
		if( StartFileName != null && StartFileName.StartsWith( "linkboy " ) ) {
			
			StartFileName = StartFileName.Remove( 0, StartFileName.IndexOf( ' ' ) + 1 );
			string Port = StartFileName.Substring( 0, StartFileName.IndexOf( ' ' ) + 1 );
			StartFileName = StartFileName.Remove( 0, StartFileName.IndexOf( ' ' ) + 1 );
			
			CodeErrBox = new FlashForm();
			Compiler.FileLoadInit();
			string[] Result = Compiler.Compile( StartFileName, SystemData.OptimizeFlag );
			if( n_ET.ET.isErrors() ) {
				MessageBox.Show( "程序出错!\n" + n_ET.ET.Show() );
				return "Q";
			}

//			DefaultFilePath = DefaultFilePath.Remove( DefaultFilePath.Length - 4 ) + ".hex";
//			VIO.SaveTextFile( DefaultFilePath, Result[ 0 ] );
			
//			AVRdude.Init();
//			AVRdude.BoardType = "NANO";
//			AVRdude.Port = Port;
//			AVRdude.Download( DefaultFilePath, true, false );
			
			ISP.Init();
			ISP.DownLoad( Result[ 0 ] );
			
			return "Q";
		}
		
		L.Init();
		
		//在G.Init()中会用到这个初始化
		ResourceList.Init();
		
		StartBox.AddMessage( "初始化编译器...\n" );
		System.Threading.Thread.Sleep( 100 );
		
		//加载编译器文件
		Compiler.FileLoadInit();
		//python编译器初始化
		n_PyCompiler.Compiler.FileLoadInit();
		
		StartBox.AddMessage( "初始化工作台开发框架...\n" );
		System.Threading.Thread.Sleep( 100 );
		
		//初始化工作台
		ControlCenter.Init();
		
		//初始化下载组件
		ISP.Init();
		
		//初始化AVRdude下载
		AVRdude.Init();
		
		n_wlibCom.wlibCom.Init();
		
		//初始化虚拟机SSH下载器
		//SSHLoaderBox = new SSHLoaderForm();
		
		STlinkBox = new STlinkForm();
		
		StartBox.AddMessage( "建立外围辅助插件...\n" );
		System.Threading.Thread.Sleep( 100 );
		
		//小精灵界面
		CodeErrBox = new FlashForm();
		
		//参数测试窗体
		ValueTestBox = new ValueTestForm();
		
		//设置开发环境界面
		SetIDEBox = new SetIDEForm();
		
		//错误信息提示
		EMesBox = new EMesForm();
		
		StartBox.AddMessage( "载入图形界面控件系统...\n" );
		System.Threading.Thread.Sleep( 100 );
		
		//初始化图形界面
		G.InitMes = GInitMes;
		G.Init();
		
		//} catch(Exception e) { MessageBox.Show( "A\n" + e.ToString() ); }
		
		System.Threading.Thread.Sleep( 100 );
		StartBox.AddMessage( "完成" );
		//System.Threading.Thread.Sleep( 200 );
		
		return StartFileName;
	}
	
	//初始化
	public static string TInit( string[] args )
	{
		string DefaultFilePath = null;
		
		try {
		
		//初始化委托
		AddNewFile += A.D_AddNewFileDefault;
		G.SystemError = Exit;
		
		//初始化底层移植库
		OS.Init();
		
		//+++++++++++++++++++++++++++++++++++++++++++++
		//判断是否加载文件
		DefaultFilePath = p_Win.CommandLine.GetStartPath( args );
		
		//+++++++++++++++++++++++++++++++++++++++++++++
		//系统数据初始化
		SystemData.Init();
		SystemData.Load();
		
		L.Init();
		
		//加载编译器文件
		Compiler.FileLoadInit();
		//python编译器初始化
		n_PyCompiler.Compiler.FileLoadInit();
		n_Config.Config.DealLockError = false;
		
		//初始化工作台
		ControlCenter.Init();
		
		//初始化下载组件
		ISP.Init();
		
		//初始化AVRdude下载
		AVRdude.Init();
		AVRdude.isIDE = true;
		
		//初始化虚拟机SSH下载器
		//SSHLoaderBox = new SSHLoaderForm();
		
		//小精灵界面
		CodeErrBox = new FlashForm();
		
		//参数测试窗体
		ValueTestBox = new ValueTestForm();
		
		//设置开发环境界面
		SetIDEBox = new SetIDEForm();
		
		//G.Init();
		
		} catch(Exception e) { MessageBox.Show( e.ToString() ); }
		
		return DefaultFilePath;
	}
	
	public static void GInitMes( string mes )
	{
		StartBox.AddMessage( mes );
	}
	
	//强制退出软件, 这里应该用一个方便复制粘贴的方式, 对话框, 另外还应该集成提示用户下载更新软件的按钮
	public static void Exit( string FileName, string ErrorMes )
	{
		System.Windows.Forms.Clipboard.SetText( ErrorMes );
		
		if( G.SimulateMode ) {
			try {
			ControlCenter.StopSim();
			}
			catch {}
			
			if( MessageBox.Show( "检测到一个严重错误, 已停止仿真. 是否要继续使用软件? (报错原因已复制到剪贴板中)", "是否继续使用", MessageBoxButtons.YesNo ) == DialogResult.Yes ) {
				return;
			}
		}
		
		string Mes = G.VersionShow + " " + G.VersionMessage + "\n";
		
		Mes += "请点击软件工具栏的<备份文件夹>按钮找一下, 可能有当前文件的备份历史. \n\n\n";
		
		Mes += "遇到了一个异常情况, 即将自动退出. ";
		Mes += "为了让linkboy更加好用, 建议您将下边的<异常信息>记录下来发送给我们(或者直接发送打开出错的这个文件).";
		Mes += "请发送至此邮箱: 910360201@QQ.COM, 如果这个出错的文件对您很重要, 需要恢复成可打开的状态, 也可以发送到上边的邮箱, 或者电话联系我:13693200752, ";
		Mes += "我们会尽快为您恢复.\n";
		Mes += "文件名:" + FileName + "\n";
		Mes += "异常信息:\n";
		Mes += ErrorMes + "\n";
		MessageBox.Show( Mes );
		
		
		MessageBox.Show( "在软件关闭之前, linkboy将尝试保存您的当前程序到一个新建文件中(原文件保持不变), 点击确定按钮后将弹出文件路径选择", "重要信息" );
		
		
		G.CGPanel.GHead.SaveAsButton_MouseDownEvent();
		
		
		A.Close();
		System.Environment.Exit(0);
	}
	
	//关闭
	public static void Close()
	{
		//保存配置文件
		if( SystemData.isChanged ) {
			SystemData.Save();
		}
		if( UserData.NanoNew != n_AVRdude.AVRdude.NanoNew ) {
			UserData.NanoNew = n_AVRdude.AVRdude.NanoNew;
			UserData.isChanged = true;
		}
		if( UserData.isChanged ) {
			UserData.Save();
		}
		Compiler.Close();
		
		ControlCenter.Close();
		
		if( !G.AutoRunMode ) {
			L.Close();
		}
		
		G.Close();
		
		//SSHLoaderBox.MyClose();
	}
}




