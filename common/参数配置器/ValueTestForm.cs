﻿
namespace n_ValueTestForm
{
using System;
using System.Windows.Forms;

/// <summary>
/// Description of WorkForm.
/// </summary>
public partial class ValueTestForm : Form
{
	//主窗口
	public ValueTestForm()
	{
		//
		// The InitializeComponent() call is required for Windows Forms designer support.
		//
		InitializeComponent();
		this.MinimumSize = this.Size;
		this.MaximumSize = this.Size;
		
		//初始化
	}
	
	//窗体运行
	public void Run()
	{
		Visible = true;
	}
	
	//窗体关闭事件
	void FindFormFormClosing(object sender, FormClosingEventArgs e)
	{
		this.Visible = false;
		e.Cancel = true;
	}
	
	void TrackBar1Scroll(object sender, EventArgs e)
	{
		this.textBox1.Text = this.trackBar1.Value.ToString();
		
		
		n_PyVar.DRef.Angle = this.trackBar1.Value;
		//GUIset.ExpFont = new Font( "宋体", this.trackBar1.Value, FontStyle.Bold );
		
		
		//G.CGPanel.MyRefresh();
	}
	
	void TrackBar2Scroll(object sender, EventArgs e)
	{
		this.textBox2.Text = this.trackBar2.Value.ToString();
		
		n_PyVar.DRef.STAngle = this.trackBar2.Value;
		
		//G.CGPanel.MyRefresh();
	}
}
}


