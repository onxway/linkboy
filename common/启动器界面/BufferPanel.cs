﻿
namespace c_BufferPanel_old
{
using System;
using System.Drawing;
using System.Windows.Forms;
using System.Drawing.Drawing2D;

//*****************************************************
//双缓冲显示容器类







//这个效率不高, 还是不要用了......  都用了好几年, 今晚才发现, 效率不高.. 可以直接重载 OnPaint 显示的..






public class BufferPanel : Panel
{
	public Graphics g;
	public Bitmap back;
	public int BufferImageWidth, BufferImageHeight;
	
	public int FScale;
	
	//构造函数
	public BufferPanel( int Width, int Height )
	{
		this.BorderStyle = BorderStyle.None;
		this.BackColor = Color.Black;
		
		SetStyle(ControlStyles.UserPaint |
		         ControlStyles.AllPaintingInWmPaint |
		         ControlStyles.OptimizedDoubleBuffer |
		         ControlStyles.ResizeRedraw |
		         ControlStyles.SupportsTransparentBackColor,true);
		
		this.Dock = DockStyle.Fill;
		this.ImeMode = ImeMode.NoControl;
		SetSize( Width, Height );
		
		FScale = 1024;
	}
	
	//设置放缩比例
	public void ScaleBig()
	{
		//FScale *= 2;
		//SetSize( Width / 2, Height / 2 );
	}
	
	//设置放缩比例
	public void ScaleSmall()
	{
		//FScale /= 2;
		//SetSize( Width * 2, Height * 2 );
	}
	
	//设置尺寸
	public void SetSize( int Width, int Height )
	{
		this.Width = Width;
		this.Height = Height;
		
		BufferImageWidth = Width;
		BufferImageHeight = Height;
		back = new Bitmap( BufferImageWidth, BufferImageHeight );
		g = Graphics.FromImage( back );
		g.SmoothingMode = SmoothingMode.HighQuality;
		//g.SmoothingMode = SmoothingMode.HighSpeed;
	}
	
	//重绘事件
	protected override void OnPaint(PaintEventArgs e)
	{
		//如下的坐标设置用于静态显示图片,始终显示在面板的正中间
		//StartX = ( this.Width - back.Width ) / 2;
		//StartY = ( this.Height - back.Height ) / 2;
		
		e.Graphics.DrawImage( back, 0, 0 );
		//e.Graphics.DrawImage( back, 0, 0, (int)(back.Width * FScale / 1024), (int)(back.Height * FScale / 1024) );
	}
}
}



