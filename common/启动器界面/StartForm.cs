﻿
namespace n_StartForm
{
using System;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Windows.Forms;

using c_BufferPanel_old;
using n_OS;

/// <summary>
/// Description of WorkForm.
/// </summary>
public partial class StartForm : Form
{
	BufferPanel GPanel;
	Font fText;
	Bitmap back;
	//Bitmap ship;
	
	Pen BackPen1;
	Pen BackPen2;
	Pen BackPen3;
	Pen ForePenA1;
	Pen ForePenA2;
	Pen ForePenA3;
	Pen ForePenB1;
	Pen ForePenB2;
	Pen ForePenB3;
	Pen ForePenC1;
	Pen ForePenC2;
	Pen ForePenC3;
	
	Brush T1 = new SolidBrush( Color.FromArgb( 125, 255, 255, 255 ) );
	Brush T0 = new SolidBrush( Color.FromArgb( 35, 0, 0, 0 ) );
	
	string FullMes;
	string MesString;
	int CurrentIndex;
	public int FullIndex;
	float Per;
	
	//const int LineXX = 11;
	const int LineXX = 4;
	
	public bool NotLinkboy;
	
	Brush logoBrush;
	
	bool BackBlack = false;
	
	//主窗口
	public StartForm()
	{
		InitializeComponent();
		
		if( G.isCruxEXE ) {
			back = new Bitmap( OS.SystemRoot + "RV" + OS.PATH_S + "crux" + OS.PATH_S + "StartBox.png" );
		}
		else if( n_ModuleLibPanel.MyModuleLibPanel.LabType == n_GUIcoder.labType.Circuit ) {
			back = new Bitmap( OS.SystemRoot + "RV" + OS.PATH_S + "circuit" + OS.PATH_S + "StartBox.png" );
		}
		else {
			back = new Bitmap( OS.SystemRoot + "RV" + OS.PATH_S + OS.USER + OS.PATH_S + "StartBox.png" );
		}
		
		//back = new Bitmap( OS.SystemRoot + "Resource" + OS.PATH_S + "StartBox" + OS.PATH_S + OS.USER + ".png" );
		
		
		this.Size = new Size( back.Width, back.Height );
		this.MaximumSize = this.Size;
		this.MinimumSize = this.Size;
		
		GPanel = new BufferPanel( back.Width, back.Height );
		GPanel.Click += new EventHandler( StartFormClick );
		this.Controls.Add( GPanel );
		
		MesString = "";
		CurrentIndex = 0;
		FullIndex = 0;
		
		/*
		Color mBackColor = Color.WhiteSmoke;
		//Color mForeColor = Color.OrangeRed;
		//Color mForeColor = Color.FromArgb( 100, 149, 237 );
		Color mForeColor = Color.FromArgb( 80, 120, 210 );
		*/
		
		/*
		Color mBackColor = Color.FromArgb( 80, 120, 210 );
		//Color mForeColor = Color.OrangeRed;
		//Color mForeColor = Color.FromArgb( 100, 149, 237 );
		Color mForeColor = Color.WhiteSmoke;
		*/
		
		Color mBackColor = Color.Silver;
		Color mForeColor = Color.OrangeRed;
		
		
		//mBackColor = Color.Gainsboro;
		
		
		
		fText = new Font( "微软雅黑", 10 );
		BackPen1 = new Pen( mBackColor, 1 );
		BackPen1.StartCap = LineCap.Round;
		BackPen1.EndCap = LineCap.Round;
		BackPen2 = new Pen( mBackColor, 3 );
		BackPen2.StartCap = LineCap.Round;
		BackPen2.EndCap = LineCap.Round;
		BackPen3 = new Pen( mBackColor, 5 );
		BackPen3.StartCap = LineCap.Round;
		BackPen3.EndCap = LineCap.Round;
		
		ForePenC1 = new Pen( mForeColor, 1 );
		ForePenC1.StartCap = LineCap.Round;
		ForePenC1.EndCap = LineCap.Round;
		ForePenC2 = new Pen( mForeColor, 3 );
		ForePenC2.StartCap = LineCap.Round;
		ForePenC2.EndCap = LineCap.Round;
		ForePenC3 = new Pen( mForeColor, 5 );
		ForePenC3.StartCap = LineCap.Round;
		ForePenC3.EndCap = LineCap.Round;
		
		ForePenA1 = new Pen( Color.White, 1 );
		ForePenA1.StartCap = LineCap.Round;
		ForePenA1.EndCap = LineCap.Round;
		ForePenA2 = new Pen( Color.Red, 3 );
		ForePenA2.StartCap = LineCap.Round;
		ForePenA2.EndCap = LineCap.Round;
		ForePenA3 = new Pen( Color.DarkRed, 5 );
		ForePenA3.StartCap = LineCap.Round;
		ForePenA3.EndCap = LineCap.Round;
		
		ForePenB1 = new Pen( Color.White, 1 );
		ForePenB1.StartCap = LineCap.Round;
		ForePenB1.EndCap = LineCap.Round;
		ForePenB2 = new Pen( Color.Blue, 3 );
		ForePenB2.StartCap = LineCap.Round;
		ForePenB2.EndCap = LineCap.Round;
		ForePenB3 = new Pen( Color.DarkBlue, 5 );
		ForePenB3.StartCap = LineCap.Round;
		ForePenB3.EndCap = LineCap.Round;
		
		logoBrush = new SolidBrush( Color.FromArgb( 150, 40, 130, 180 ) );
	}
	
	//运行
	public void Run()
	{
		this.Visible = true;
		GPanel.Invalidate();
		this.Refresh();
	}
	
	//等待
	public void Wait()
	{
		/*
		panel1.Visible = true;
		while( A.StartBox.Visible ) {
			System.Windows.Forms.Application.DoEvents();
		}
		*/
	}
	
	//设置全信息
	public void SetFullMes( string vFullMes, int N )
	{
		FullMes = vFullMes;
		FullIndex = N;
		
		Per = (float)(this.Width - 6 - LineXX * 2) / N;
		
		AddMessage( null );
	}
	/*
	int sX = 200;
	int sY = 290;
	*/
	//添加信息
	public void AddMessage( string m )
	{
		//int ShowHeight = 125;
		
		if( m != null ) {
			MesString += m;
			++CurrentIndex;
		}
		
		//绘制背景
		//GPanel.g.FillRectangle( Brushes.Gray, 0, 0, GPanel.BufferImageWidth, GPanel.BufferImageHeight );
		GPanel.g.DrawImage( back, 0, 0, back.Width, back.Height );
		
		/*
		if( ship != null ) {
			Random r = new Random();
			sX += 5;
			sY -= 10;
			//GPanel.g.DrawImage( ship, sX, sY, 153, 197 );
		}
		*/
		
		//绘制显示信息
		if( BackBlack ) {
			//GPanel.g.DrawString( FullMes, fText, Brushes.Gray, LineXX, this.Height - ShowHeight );
			//GPanel.g.DrawString( MesString, fText, Brushes.White, LineXX, this.Height - ShowHeight );
		}
		else{
			//GPanel.g.DrawString( FullMes, fText, Brushes.SlateGray, LineXX, this.Height - ShowHeight );
			//GPanel.g.DrawString( MesString, fText, Brushes.WhiteSmoke, LineXX, this.Height - ShowHeight );
		}
		
		/*
		GPanel.g.DrawString( FullMes, fText, T0, LineXX, this.Height - ShowHeight );
		GPanel.g.DrawString( MesString, fText, T1, LineXX, this.Height - ShowHeight );
		if( n_ModuleLibDecoder.ModuleLibDecoder.CurrentFolderName != null ) {
			GPanel.g.DrawString( "(" + n_ModuleLibDecoder.ModuleLibDecoder.CurrentFolderName + ")", fText, T1, LineXX + 160, this.Height - 37 );
		}
		*/
		
		//绘制进度条
		//GPanel.g.DrawLine( BackPen3, LineXX, LineY, LineXX + (int)(FullIndex * Per), LineY );
		//GPanel.g.DrawLine( BackPen2, LineXX, LineY, LineXX + FullIndex * Per, LineY );
		//GPanel.g.DrawLine( BackPen1, LineXX, LineY, LineXX + FullIndex * Per, LineY );
		
		//GPanel.g.DrawLine( ForePenC3, LineXX, LineY, LineXX + CurrentIndex * Per, LineY );
		//GPanel.g.DrawLine( ForePenC2, LineXX, LineY, LineXX + CurrentIndex * Per, LineY );
		//GPanel.g.DrawLine( ForePenC1, LineXX, LineY, LineXX + CurrentIndex * Per, LineY );
		
		
		//底层绘制
		int LineY = this.Height - 4;
		GPanel.g.FillRectangle( Brushes.DarkGray, LineXX - 1, LineY - 2, LineXX + CurrentIndex * Per, 2 );
		//GPanel.g.FillRectangle( Brushes.Silver, LineXX - 1, LineY - 7, LineXX + CurrentIndex * Per, 2 );
		//GPanel.g.FillRectangle( Brushes.Silver, LineXX - 1, LineY - 5, LineXX + CurrentIndex * Per, 3 );
		
		//刷新显示
		//GPanel.Invalidate();
		this.Refresh();
	}
	
	void StartFormClick(object sender, EventArgs e)
	{
		this.Visible = false;
	}
	
	void ButtonCodeLabClick(object sender, EventArgs e)
	{
		Visible = false;
	}
	
	void ButtonCircuitLabClick(object sender, EventArgs e)
	{
		Visible = false;
	}
}
}




