﻿
//开发环境用到的全局文件类型

namespace n_FileType
{
using System;
using Microsoft.Win32;
using System.Windows.Forms;

public static class FileType
{
	public const string VMachineFile = "vex";
	public const string InyFile = "iny";
	public const string ModFile = "mox";
	public const string VosConfigFile = "vos";
	public const string CruxFile = "cx";
	
	public const string DriverFile = "B";
	public const string ModuleFile = "M";
	public const string HEXFile = "hex";
	public const string EXEFile = "exe";
	
	//判断文件是否关联
	public static bool isSaveRegFileType( string p_Filename, string p_FileTypeName )
	{
		try {
		RegistryKey _RegKey = Registry.ClassesRoot.OpenSubKey("", true);		//打开注册表
		
		RegistryKey _VRPkey = _RegKey.OpenSubKey(p_FileTypeName);
		if (_VRPkey == null) return false;
		
		_VRPkey = _RegKey.OpenSubKey( "linkboy_" + p_FileTypeName, true);
		if (_VRPkey == null) return false;
		
		_VRPkey = _VRPkey.OpenSubKey("shell", true);
		if (_VRPkey == null) return false;
		
		_VRPkey = _VRPkey.OpenSubKey("open", true);
		if (_VRPkey == null) return false;
		
		_VRPkey = _VRPkey.OpenSubKey("command", true);
		if (_VRPkey == null) return false;
		
		string _PathString = "\"" + p_Filename + "\" \"%1\"";
		string CS = (string)_VRPkey.GetValue("");
		
		_VRPkey.Close();
		
		if( _PathString != CS ) {
			return false;
		}
		}catch( Exception e) {
			n_OS.VIO.Show( "读取注册表时发生异常:\n" + e.ToString() );
			return false;
		}
		//System.Windows.Forms.MessageBox.Show( "<" + _PathString + ">\n<" + CS + ">" );
		return true;
	}
	
	//设置文件关联
	//p_Filename">程序的名称</param>
	/// <param name="p_FileTypeName">扩展名 .VRD </param>
	public static bool SaveRegFileType( string p_Filename, string p_FileTypeName )
	{
		try {
		RegistryKey rk = Registry.ClassesRoot.OpenSubKey("", true);	//打开注册表
		rk = rk.OpenSubKey( "Applications", true );
		RegistryKey rk1 = rk.OpenSubKey( "linkboy_" + p_FileTypeName, true );
		if( rk1 != null ) {
			rk.DeleteSubKeyTree( "linkboy_" + p_FileTypeName );
			n_OS.VIO.Show( "注: 删除了一项关于 " + "linkboy_" + p_FileTypeName + " 的过时注册表信息, 为绑定新版软件做准备" );
		}
		
		RegistryKey _RegKey = Registry.ClassesRoot.OpenSubKey("", true);	//打开注册表
		
		RegistryKey _VRPkey = _RegKey.OpenSubKey(p_FileTypeName);
		
		//这里可能报错 当删除的项目还有子项目时, 不支持递归.
		//if (_VRPkey != null) _RegKey.DeleteSubKey(p_FileTypeName, true);
		if (_VRPkey != null) _RegKey.DeleteSubKeyTree(p_FileTypeName, true); //win10
		//if (_VRPkey != null) _RegKey.DeleteSubKeyTree(p_FileTypeName); //XP
		
		_RegKey.CreateSubKey(p_FileTypeName);
		_VRPkey = _RegKey.OpenSubKey(p_FileTypeName, true);
		_VRPkey.SetValue("", "linkboy_" + p_FileTypeName );
		
		_VRPkey = _RegKey.OpenSubKey( "linkboy_" + p_FileTypeName, true);
		if (_VRPkey != null) _RegKey.DeleteSubKeyTree( "linkboy_" + p_FileTypeName ); //如果不等于空 就删除此项注册表
		
		_RegKey.CreateSubKey( "linkboy_" + p_FileTypeName );
		_VRPkey = _RegKey.OpenSubKey( "linkboy_" + p_FileTypeName, true);
		_VRPkey.CreateSubKey("shell");
		_VRPkey = _VRPkey.OpenSubKey("shell", true);					//写入必须路径
		_VRPkey.CreateSubKey("open");
		_VRPkey = _VRPkey.OpenSubKey("open", true);
		_VRPkey.CreateSubKey("command");
		_VRPkey = _VRPkey.OpenSubKey("command", true);
		string _PathString = "\"" + p_Filename + "\" \"%1\"";
		_VRPkey.SetValue("", _PathString);								//写入数据
		
		_VRPkey.Close();
		
		}catch( Exception e) {
			n_OS.VIO.Show( "改写注册表时发生异常:\n" + e.ToString() );
			return false;
		}
		return true;
	}
}

//扩展名绑定
public static class ExtNameBind
{
	public static bool ShowMes = false;
	
	static bool FirstRun = true;
	
	//文件扩展名判断和设定
	public static void FileExternNameReg_Linkboy()
	{
		string FileName = n_OS.OS.USER;
		
		//判断是否已绑定 linkboy
		string SysLinkboyexe = n_OS.OS.SystemRoot + "linkboy.exe";
		
		bool isok = FileType.isSaveRegFileType( SysLinkboyexe, "." + n_OS.OS.LinkboyFile );
		
		if( !isok ) {
			
			ShowFirstMessage();
			
			string Mes = "为方便用户直接双击打开项目文件(." + n_OS.OS.LinkboyFile + ")并进行编辑,\n";
			Mes += "系统将会关联此类文件的打开方式为:\n " + SysLinkboyexe;
			
			if( ShowMes ) {
				MessageBox.Show( Mes );
			}
			
			bool ok = FileType.SaveRegFileType( SysLinkboyexe, "." + n_OS.OS.LinkboyFile );
			if( !ok ) {
				string m = "关联[." + n_OS.OS.LinkboyFile + "]文件打开方式时失败, 请您右键选择\"以管理员身份运行\"打开此软件完成此关联操作\n";
				m += "或者您也可以手工设置[." + n_OS.OS.LinkboyFile + "]后缀名文件的打开方式为 " + FileName + ".exe";
				MessageBox.Show( m );
			}
		}
	}
	
	//文件扩展名判断和设定
	public static void FileExternNameReg_Crux()
	{
		//判断是否已绑定 remo
		string myexe = n_OS.OS.SystemRoot + @"linkboy.exe";
		bool isok = FileType.isSaveRegFileType( myexe, "." + FileType.CruxFile );
		
		if( !isok ) {
			
			ShowFirstMessage();
			
			bool ok = FileType.SaveRegFileType( myexe, "." + FileType.CruxFile );
			if( !ok ) {
				string m = "关联cx文件打开方式时失败, 请您右键选择\"以管理员身份运行\"打开此软件完成此关联操作\n";
				m += "或者您也可以手工设置[.cx]后缀名文件的打开方式为 linkboy.exe";
				MessageBox.Show( m );
			}
		}
	}
	
	//文件扩展名判断和设定
	public static void FileExternNameReg_B()
	{
		//判断是否已绑定 B 文件
		string SysRemoexe = n_OS.OS.SystemRoot + @"LM-config.exe";
		
		bool isok = FileType.isSaveRegFileType( SysRemoexe, "." + FileType.DriverFile );
		
		if( !isok ) {
			
			ShowFirstMessage();
			
			string Mes = "为方便用户直接双击打开模块封装文件(." + FileType.DriverFile + "),\n";
			Mes += "系统将会关联此类文件的打开方式为:\n " + SysRemoexe;
			
			MessageBox.Show( Mes );
			
			bool ok = FileType.SaveRegFileType( SysRemoexe, "." + FileType.DriverFile );
			if( !ok ) {
				string m = "关联B文件打开方式时失败, 请您右键选择\"以管理员身份运行\"打开此软件完成此关联操作\n";
				m += "或者您也可以手工设置[." + FileType.DriverFile + "]后缀名文件的打开方式为 " + SysRemoexe;
				MessageBox.Show( m );
			}
		}
	}
	
	//文件扩展名判断和设定
	public static void FileExternNameReg_M()
	{
		//判断是否已绑定 B 文件
		string SysRemoexe = n_OS.OS.SystemRoot + @"LM-design.exe";
		bool isok = FileType.isSaveRegFileType( SysRemoexe, "." + FileType.ModuleFile );
		
		if( !isok ) {
			
			ShowFirstMessage();
			
			string Mes = "为方便用户直接双击打开模块设计文件(." + FileType.ModuleFile + "),\n";
			Mes += "系统将会关联此类文件的打开方式为:\n " + SysRemoexe;
			
			MessageBox.Show( Mes );
			
			bool ok = FileType.SaveRegFileType( SysRemoexe, "." + FileType.ModuleFile );
			if( !ok ) {
				string m = "关联M文件打开方式时失败, 请您右键选择\"以管理员身份运行\"打开此软件完成此关联操作\n";
				m += "或者您也可以手工设置[." + FileType.ModuleFile + "]后缀名文件的打开方式为 " + SysRemoexe;
				MessageBox.Show( m );
			}
		}
	}
	
	//文件扩展名判断和设定
	public static void FileExternNameReg_Iny()
	{
		//判断是否已绑定
		string SysInyexe = n_OS.OS.SystemRoot + @"tool-作曲大师.exe";
		bool isok = FileType.isSaveRegFileType( SysInyexe, "." + FileType.InyFile );
		if( !isok ) {
			
			ShowFirstMessage();
			
			string Mes = "为方便用户直接双击打开音乐简谱旋律文件(." + FileType.InyFile + "),\n";
			Mes += "系统将会关联此类文件的打开方式为:\n " + SysInyexe;
			
			if( ShowMes ) {
				MessageBox.Show( Mes );
			}
			
			bool ok = FileType.SaveRegFileType( SysInyexe, "." + FileType.InyFile );
			if( !ok ) {
				string m = "关联iny音乐文件打开方式时失败, 请您右键选择\"以管理员身份运行\"打开此软件完成此关联操作\n";
				m += "或者您也可以手工设置[.iny]后缀名文件的打开方式为 MusicBox.exe";
				
				if( ShowMes ) {
					MessageBox.Show( m );
				}
			}
		}
	}
	
	//文件扩展名判断和设定
	public static void FileExternNameReg_Vos()
	{
		//判断是否已绑定
		string Sysexe = n_OS.OS.SystemRoot + @"vos-config.exe";
		bool isok = FileType.isSaveRegFileType( Sysexe, "." + FileType.VosConfigFile );
		if( !isok ) {
			
			ShowFirstMessage();
			
			string Mes = "为方便用户直接双击打开vos配置文件(." + FileType.VosConfigFile + "),\n";
			Mes += "系统将会关联此类文件的打开方式为:\n " + Sysexe;
			
			if( ShowMes ) {
				MessageBox.Show( Mes );
			}
			
			bool ok = FileType.SaveRegFileType( Sysexe, "." + FileType.VosConfigFile );
			if( !ok ) {
				string m = "关联vos文件打开方式时失败, 请您右键选择\"以管理员身份运行\"打开此软件完成此关联操作\n";
				m += "或者您也可以手工设置[.iny]后缀名文件的打开方式为 vos-config.exe";
				
				if( ShowMes ) {
					MessageBox.Show( m );
				}
			}
		}
	}
	
	//文件扩展名判断和设定
	public static void FileExternNameReg_Mod()
	{
		//判断是否已绑定
		string SysModexe = n_OS.OS.SystemRoot + @"Chest.exe";
		bool isok = FileType.isSaveRegFileType( SysModexe, "." + FileType.ModFile );
		if( !isok ) {
			
			ShowFirstMessage();
			
			string Mes = "为方便用户直接双击打开mox模型文件(." + FileType.ModFile + "),\n";
			Mes += "系统将会关联此类文件的打开方式为:\n " + SysModexe;
			
			MessageBox.Show( Mes );
			
			bool ok = FileType.SaveRegFileType( SysModexe, "." + FileType.ModFile );
			if( !ok ) {
				string m = "关联mox模型文件打开方式时失败, 请您右键选择\"以管理员身份运行\"打开此软件完成此关联操作\n";
				m += "或者您也可以手工设置[.mox]后缀名文件的打开方式为 Chest.exe";
				MessageBox.Show( m );
			}
		}
	}
	
	//第一次运行提示信息
	public static void ShowFirstMessage()
	{
		if( FirstRun ) {
			string FileName = n_OS.OS.USER;
			
			if( !ShowMes ) {
				return;
			}
			string Mes = "用户您好! 系统检测到您是首次安装并运行此软件(或者您刚刚安装了更高的版本),";
			Mes += "为了更好地提升软件的用户体验, 接下来将会执行一些文件关联操作, 您只需点击 <确定> 按钮即可.";
			MessageBox.Show( Mes, "首次运行提示" );
		}
		FirstRun = false;
	}
}
}






