﻿
namespace n_ControlCenter
{
using System;
using System.Diagnostics;
using System.Windows.Forms;
using System.Threading;
using System.IO;

using n_ET;
using n_FileType;
using n_GUIcoder;
using n_ISP;
using n_OS;
using n_TargetFile;
using n_MainSystemData;
using n_AVRdude;
using n_RemoPad;
using n_Compiler;
using n_Config;
using n_CPUType;
using n_Decode;

//*****************************************************
//控制中心
public static class ControlCenter
{
	public delegate void deleStartCompile();
	public static deleStartCompile D_StartCompile;
	
	public delegate void deleShowError( bool d );
	public static deleShowError D_ShowError;
	public delegate void deleHiddError();
	public static deleHiddError D_HiddError;
	
	public delegate void deleEnd();
	public static deleEnd DealEnd;
	
	public delegate void D_UseSerialPort();
	public static D_UseSerialPort deleUseSerialPort;
	
	static bool HasShowExe = false;
	
	//构造函数
	public static void Init()
	{
		n_Interface.AVM.Init();
		n_VM.VM.Init();
		
		n_UserModule.UserModule.BASE = n_VM.VM.BASE;
		n_CLineList.CLineList.BASE = n_VM.VM.BASE;
		n_HardModule.Elm.BASE = n_VM.VM.BASE;
		
		n_CCEngine.CCEngine.BASE = n_VM.VM.BASE;
		n_CCEngine.CCEngine.ByteCodeList = n_VM.VM.ByteCodeList;
		
		n_SimForm.SimForm.Simlate = Simulate;
		
		RemoPad.DebugInit();
	}
	
	//关闭
	public static void Close()
	{
		n_Interface.AVM.Close();
	}
	
	//调试程序
	public static void Debug()
	{
		if( G.DebugMode ) {
			StopDebug();
		}
		else {
			if( G.SimulateMode ) {
				n_Debug.Warning.WarningMessage = "仿真模式下暂不支持开启示波器";
				return;
			}
			if( !RemoPad.OpenDebugOk() ) {
				return;
			}
			RemoPad.Debug_Open( 1 );
			
			G.DebugMode = true;
			//G.CGPanel.GHead.DebugButton.tempValue = "停止调试";
		}
	}
	
	//结束调试
	public static void StopDebug()
	{
		RemoPad.StopDebug();
		
		G.DebugMode = false;
		//G.CGPanel.GHead.DebugButton.tempValue = null;
	}
	
	//编译下载源程序
	public static void Auto()
	{
		if( G.ccode == null ) {
			return;
		}
		if( AbortForShowNumber() ) {
			return;
		}
		//如果正在仿真状态, 则停止仿真
		if( G.SimulateMode ) {
			StopSim();
		}
		//如果在调试模式, 结束调试
		//if( G.DebugMode ) {
			StopDebug();
		//}
		
		//清空信息 - 这里之后返回需要调用 EndPanel()
		if( D_StartCompile != null ) {
			D_StartCompile();
		}
		
		//是否正常保存编译
		if( !G.ccode.Save() ) {
			EndPanel();
			return;
		}
		//检查是不是有图形界面错误
		if( GUIcoderExistError() ) {
			EndPanel();
			return;
		}
		if( !G.isCruxEXE || !ET.isErrors() ) {
			n_Compiler.Compiler.SimTempSource = GUIcoder.CodeAll;
			Compile( SystemData.CreateHEXFile );
		}
		if( ET.isErrors() ) {
			DealError();
			EndPanel();
			return;
		}
		if( GUIcoder.cmd_download ) {
			DownLoad( 0 );
		}
		else {
			if( G.CExportBox == null ) {
				G.CExportBox = new n_CExportForm.CExportForm();
			}
			G.CExportBox.Run();
		}
		
		//结束界面的显示
		EndPanel();
	}
	
	//仿真
	public static void Simulate()
	{
		if( G.ccode == null ) {
			return;
		}
		//如果正在仿真状态, 则停止仿真
		if( G.SimulateMode ) {
			StopSim();
			
			//切换到代码界面
			if( G.isCruxEXE && !G.ShowCruxCode ) {
				if( G.CGPanel.GHead.DeleUserCode != null ) {
					G.CGPanel.GHead.DeleUserCode();
				}
			}
			return;
		}
		//如果在调试模式, 结束调试
		if( G.DebugMode ) {
			StopDebug();
		}
		if( AbortForShowNumber() ) {
			return;
		}
		//清空信息 - 这里之后返回需要调用 EndPanel()
		if( D_StartCompile != null ) {
			D_StartCompile();
		}
		//先进行编译,再仿真
		
		n_GUIcoder.GUIcoder.cmd_simulate = true;
		if( !G.ccode.Save() ) {
			EndPanel();
			return;
		}
		
		G.SimBox.SPanel.StartRun = true;
		
		if( G.isCruxEXE && G.ShowCruxCode ) {
			if( G.CGPanel.GHead.DeleUserCode != null ) {
				G.CGPanel.GHead.DeleUserCode();
			}
		}
		//检查是不是有错误
		if( GUIcoderExistError() ) {
			EndPanel();
			return;
		}
		if( !G.isCruxEXE || !ET.isErrors() ) {
			//获取临时仿真源文件
			n_Compiler.Compiler.SimTempSource = GUIcoder.CodeAll;
			
			G.ccode.isChanged = false;
			
			Compile( SystemData.CreateHEXFile );
		}
		if( ET.isErrors() ) {
			DealError();
			EndPanel();
			
			if( G.isCruxEXE && !G.ShowCruxCode ) {
				if( G.CGPanel.GHead.DeleUserCode != null ) {
					G.CGPanel.GHead.DeleUserCode();
				}
			}
			return;
		}
		//if( G.isCruxEXE ) {
			n_CruxSim.CruxSim.ResetSim();
		//}
		if( GUIcoder.LoadDoStringLib ) {
			n_HeapSim.HeapSim.ResetSim();
		}
		
		//切换到图形界面
		
		G.SimBox.SimStart();
		
		//没有效果
		n_Imm.Imm.SetEn( true );
		
		//调用所有模块的数据表初始化
		G.CGPanel.myModuleList.ResetSim();
		
		if( n_ModuleLibPanel.MyModuleLibPanel.LabType == n_GUIcoder.labType.Circuit ) {
			G.CGPanel.myCLineList.ResetSim();
		}
		n_CCEngine.CCEngine.ResetSim();
		
		G.CGPanel.GHead.SimButton.tempValue = "停止仿真";
		
		n_ESP8266.ESP8266.OpenSim();
		
		//不需要载入文件, 直接内部传递字节码数组
		//string Path = TargetFile.GetTargetFilePath();
		//n_Interface.AVM.Run( Path);
		
		n_Interface.AVM.Run( n_Decode.Decode.ByteCodeList, n_Decode.Decode.BCLength );
		
		n_SimPanel.SimPanel.FirstPress = 2;
		G.SimulateMode = true;
		
		if( GUIcoder.UseBindKey ) {
			GUIcoder.UseBindKeyIndex++;
			if( GUIcoder.UseBindKeyIndex == 1 ) {
				n_Debug.Warning.WarningMessage = "如果仿真时键盘按下无效, 原因可能是处于中文模式. 请先停止仿真, 将输入法切换到英文模式下再仿真即可.\n" +
					"(注: 本提示每次启动软件只显示一次, 后续将不再显示)";
			}
		}
		
		//结束界面的显示
		EndPanel();
	}
	
	//停止仿真
	public static void StopSim()
	{
		n_Interface.AVM.Stop();
		n_ESP8266.ESP8266.CloseSim();
		
		G.CGPanel.myModuleList.StopSim();
		G.CGPanel.GHead.SimButton.tempValue = null;
		
		G.SimulateMode = false;
		G.SimBox.SimEnd();
	}
	
	static bool AbortForShowNumber()
	{
		if( GUIcoder.ExistShowNumberIns && !GUIcoder.ExistClearNumberIns ) {
			//if( MessageBox.Show( "确定要继续???", "提示", MessageBoxButtons.YesNo ) == DialogResult.No ) {
			//	return true;
			//}
			
			//if( A.EMesBox.Run() ) {
			//	return true;
			//}
		}
		return false;
	}
	
	static bool GUIcoderExistError()
	{
		if( n_Debug.Warning.ClashMessage != null ) {
			n_Debug.Warning.WarningMessage = "程序存在错误, 请先解决已有的问题";
			return true;
		}
		if( GUIcoder.ExistError ) {
			G.CGPanel.AddMesPanel( GUIcoder.ErrorMessage, 3, GUIcoder.ErrorObject );
			GUIcoder.AutoMove = true;
			G.SimBox.SimEnd();
			return true;
		}
		return false;
	}
	
	//判断后台线程是否忙碌(检查语法, 编译, 下载)
	public static bool isBusy()
	{
		return false;
	}
	
	//编译进程
	public static void Compile( bool HEXFile )
	{
		string TargetPath = G.ccode.FilePath;
		string ChipName = G.ccode.FileName;
		string PathAndName = G.ccode.PathAndName;
		
		//编译并保存结果
		/*
		if( !DefaultL ) {
			string r = n_PyCompiler.Compiler.Compile( PathAndName );
			if( n_PyET.ET.isErrors() ) {
				Command = SHOW_PY_ERROR;
			}
			else {
				Command = CLOSE_ERROR;
			}
			return;
		}
		*/
		
		Compiler.isExportMode = GUIcoder.cmd_export;
		
		string[] Result = Compiler.Compile( PathAndName, SystemData.OptimizeFlag );
		
		if( !ET.isErrors() ) {
			
			if( GUIcoder.cmd_download && n_Config.Config.NDK_List != null ) {
				n_Debug.Warning.WarningMessage = "程序中存在对C语言变量/函数的引用, 只在导出代码模式下才会有预期的效果! 请点击导出代码按钮. " + n_Config.Config.NDK_List;
			}
			
			//保存文件并更新当前目标文件列表
			string[] TargetFileList = new string[ Result.Length ];
			
			for( int i = 0; i < Result.Length; ++i ) {
				Config.SetChipIndex( i );
				string targetFile = TargetPath + ChipName + ( Result.Length == 1? "" : Config.GetChipName() );
				
				if( Config.GetCPU() == CPUType.VM ) {
					targetFile += "." + FileType.VMachineFile;
				}
				else if( Config.GetCPU() == CPUType.PC80X86 ) {
					targetFile += "." + FileType.EXEFile;
				}
				else {
					targetFile += "." + FileType.HEXFile;
				}
				TargetFileList[ i ] = targetFile;
				
				//生成虚拟机文件
				if( Config.GetCPU() == CPUType.VM && n_GUIcoder.GUIcoder.cmd_download ) {
					
					//这种方式总是会发生异常, 文件被占用
					//if( !File.Exists( targetFile ) ) {
					//	File.Create( targetFile );
					//}
					
					if( HEXFile ) {
						//如果不存在文件则会新建一个
						FileStream fs = File.Open( targetFile, FileMode.OpenOrCreate );
						fs.Close();
						System.Threading.Thread.Sleep( 100 );
						
						//更新文件数据
						fs = File.Open( targetFile, FileMode.Truncate );
						fs.Write( Decode.ByteCodeList, 0, Decode.BCLength );
						fs.Close();
					}
				}
				if( Result[ i ] == null ) {
					continue;
				}
				bool CanSave = false;
				/*
				if( !A.isGForm && !Config.GetCPU().StartsWith( CPUType.MCS_X ) ) {
					if( MessageBox.Show( "是否需要生成和保存HEX文件?", "HEX文件生成提示", MessageBoxButtons.YesNo ) == DialogResult.Yes ) {
						CanSave = true;
					}
				}
				*/
				//生成HEX文件
				if( (n_AVRdude.AVRdude.BoardType != "ISP" && (Config.GetCPU() == "MEGA328" || Config.GetCPU() == "MEGA644" || Config.GetCPU() == "MEGA2560" || Config.GetCPU().StartsWith( CPUType.MCS_X ))) ||
				    CanSave ||
				    HEXFile ) {
					
					if( n_GUIcoder.GUIcoder.cmd_download ) {
						n_OS.VIO.SaveTextFileGB2312( targetFile, Result[ i ] );
						if( HEXFile ) {
							MessageBox.Show( "目标文件已经生成: " + targetFile );
						}
					}
				}
			}
			TargetFile.TargetFileList = TargetFileList;
			TargetFile.TargetHEXFileList = Result;
			
			if( D_HiddError != null ) {
				D_HiddError();
			}
		}
	}
	
	//结束界面下载和仿真进度提示
	static void EndPanel()
	{
		n_GUIcoder.GUIcoder.cmd_download = false;
		n_GUIcoder.GUIcoder.cmd_export = false;
		n_GUIcoder.GUIcoder.cmd_simulate = false;
		
		try {
			G.CGPanel.GHead.isCompile = false;
			G.CGPanel.GHead.isDownLoad = false;
			G.CGPanel.GHead.NewHeadLabel.Step = 0;
			G.CGPanel.MyRefresh();
			if( DealEnd != null ) {
				DealEnd();
			}
		}
		catch {}
	}
	
	static void DealError()
	{
		if( !G.isCruxEXE ) {
			
			string Errormes = ET.Show();
			string[] Lines = Errormes.Split( '\n' );
			
			try {
				for( int i = 0; i < Lines.Length; ++i ) {
					string[] s = Lines[i].Split( ' ' );
					
					//int FileIndex = int.Parse( s[ 1 ] );
					//string FilePath = UseFileList.GetPath( FileIndex );
					
					int Line = 0;
					if( s[ 0 ] == "词" ) {
						Line = int.Parse( s[ 2 ] ) - 1;
						int StartIndex = int.Parse( s[ 3 ] );
						int Length = int.Parse( s[ 4 ] );
					}
					if( s[ 0 ] == "行" ) {
						Line = int.Parse( s[ 2 ] ) - 1;
					}
					
					string[] cccc;
					/*
							if( SetSimMode ) {
								cccc = G.ccode.GetSimCode().Split( '\n' );
							}
							else {
								cccc = G.ccode.GetCode().Split( '\n' );
							}
					 */
					cccc = n_Compiler.Compiler.SimTempSource.Split( '\n' );
					
					string ss = cccc[Line].Remove( 0, cccc[Line].LastIndexOf( "//#" ) + 3 ).TrimEnd( ' ' );
					int ErrorIndex = int.Parse( ss );
					
					//MessageBox.Show( cccc[Line] + "  B " + ss );
					GUIcoder.SetErrorMes( G.CGPanel.myModuleList.ModuleList[ErrorIndex], Lines[i] );
					if( GUIcoderExistError() ) {
						return;
					}
					break;
				}
			}
			catch {
				n_Debug.Warning.AddErrMessage( "编译错误, 请检查程序:\n" + ET.Show() );
				//System.Windows.Forms.Clipboard.SetText( ET.Show() );
			}
		}
		else {
			/*
				//临时弹出
				if( !GUIcoder.isDownload ) {
					if( G.CExportBox == null ) {
						G.CExportBox = new n_CExportForm.CExportForm();
					}
					G.CExportBox.Run();
				}
			 */
			
			if( !G.ShowCruxCode && G.CGPanel.GHead.DeleUserCode != null ) {
				G.CGPanel.GHead.DeleUserCode();
			}
			if( D_ShowError != null ) {
				D_ShowError( true );
			}
		}
		G.SimBox.SimEnd();
	}
	
	//下载
	public static void DownLoad( int CI )
	{
		/*
		if( GUIcoder.isPyMode ) {
			if( G.PyLoderBox == null ) {
				G.PyLoderBox = new n_PyLoaderForm.PyLoaderForm();
			}
			G.PyLoderBox.Run( TargetFile.TargetPyFile, n_Py_GUIcoder.Py_GUIcoder.CSource );
			return;
		}
		*/
		
		string Path = TargetFile.GetTargetFilePath();
		if( Path == null ) {
			return;
		}
		if( n_Config.Config.GetCPU().StartsWith( n_CPUType.CPUType.MCS_X ) ) {
			return;
		}
		if( Path.EndsWith( "." + FileType.EXEFile ) || Path.EndsWith( "." + FileType.EXEFile.ToLower() ) ) {
			MessageBox.Show( Path );
			
			ProcessStartInfo Info = new ProcessStartInfo();
			//Info.WindowStyle = ProcessWindowStyle.Hidden;
			//Info.WorkingDirectory = OS.SystemRoot + "core" + OS.PATH_S + "bin";
			Info.FileName = Path;
			//Info.Arguments = "AIserene-C-D " + OS.SystemRoot + "core" + OS.PATH_S + "target.txt";
			Process Proc = Process.Start(Info);
			return;
		}
		if( CI != 0 ) {
			MessageBox.Show( "下载副板程序" );
		}
		
		//拦截RAM容量溢出警告
		if( G.CGPanel != null && G.CGPanel.ShowRamLimitWarning > 0 ) {
			
			/*
				int neddnumbmer = 1 + (G.CGPanel.ShowRamLimitWarning) / 65;
				MessageBox.Show( "RAM需求量已超出主板极限(" + G.CGPanel.ShowRamLimitWarning + "Bytes), 解决方案是删除一些模块的RAM占用(如减小数组长度), 或者更换为更大容量的主板, 如644/2560/ARM32位主板" );
				
				if( MessageBox.Show( "建议自动给您的程序进行优化, 预计可额外释放出大约几百字节的RAM, 点击<是>即可开始自动优化分析", "自动优化提示", MessageBoxButtons.YesNo ) == DialogResult.Yes ) {
					
					System.Threading.Thread.Sleep( 1000 );
					
					G.CGPanel.EventNumber -= 1; //neddnumbmer;
					if( G.CGPanel.EventNumber < 2 ) {
						G.CGPanel.EventNumber = 2;
						MessageBox.Show( "注意: 可能优化失败!" );
					}
					MessageBox.Show( "优化完成, 已释放出 " + neddnumbmer * 70 + " 字节的RAM空间. 稍后会自动重新下载.\n" +
									 "本次优化减小了系统事件池数量, 从默认的" + n_ImagePanel.ImagePanel.DefaultEventNumber + "个减小为" + G.CGPanel.EventNumber + "个, " +
									 "也就是系统上电后, 最多支持" + G.CGPanel.EventNumber + "个事件同时执行(其中一个会被系统占用)" );
					G.CGPanel.GHead.DeleAuto( false, true );
					return;
				}
			 */
			
			int neddnumbmer = 1 + (G.CGPanel.ShowRamLimitWarning) / 65;
			n_Debug.Warning.WarningMessage = ( "RAM需求量已超出主板极限(" + G.CGPanel.ShowRamLimitWarning + "Bytes), 解决方案是删除一些模块的RAM占用(如减小数组长度), 或者更换为更大容量的主板, 如644/2560/ARM32位主板" );
			
			//if( MessageBox.Show( "建议自动给您的程序进行优化, 预计可额外释放出大约几百字节的RAM, 点击<是>即可开始自动优化分析", "自动优化提示", MessageBoxButtons.YesNo ) == DialogResult.Yes ) {
			
			//System.Threading.Thread.Sleep( 1000 );
			
			G.CGPanel.EventNumber -= 1; //neddnumbmer;
			if( G.CGPanel.EventNumber < 2 ) {
				G.CGPanel.EventNumber = 2;
				MessageBox.Show( "注意: 可能优化失败!" );
			}
			else {
				n_Debug.Warning.WarningMessage = ( "优化完成, 已释放出 " + neddnumbmer * 70 + " 字节的RAM空间. 稍后会自动重新下载.\n" +
				                                  "本次优化减小了系统事件池数量, 从默认的" + n_ImagePanel.ImagePanel.DefaultEventNumber + "个减小为" + G.CGPanel.EventNumber + "个, " +
				                                  "也就是系统上电后, 最多支持" + G.CGPanel.EventNumber + "个事件同时执行(其中一个会被系统占用)" );
				G.CGPanel.GHead.DeleAuto();
				return;
			}
			//}
		}
		//拦截ROM容量溢出警告
		if( G.CGPanel != null && G.CGPanel.ShowFlashLimitWarning ) {
			
			if( GUIcoder.DebugOpenSet ) {
				n_CodeData.CodeData.TotalByteNumber = 0;
				GUIcoder.DebugOpenSet = false;
				MessageBox.Show( "程序量已超出主板容量, 将关闭遥控器组件并重新编译..." );
				G.CGPanel.GHead.DeleAuto();
				return;
			}
			MessageBox.Show( "程序量已超出主板容量, 解决方案是删除一些模块和指令, 或者更换为更大容量的主板,如644/2560/ARM32位主板" );
			if( MessageBox.Show( "由于程序量过大, 现通知您停止下载过程. 如果您要执意下载程序, 可能导致arduino主板报废. 点击'是'即可停止下载过程", "下载中断提示", MessageBoxButtons.YesNo ) == DialogResult.Yes ) {
				return;
			}
		}
		bool ShowFloatMes = true;
		
		if( n_AVRdude.AVRdude.BoardType == "ISP" ) {
			ISP.DownLoad( TargetFile.GetTargetFile( CI ) );
		}
		else if( n_AVRdude.AVRdude.BoardType == "Curie" || n_AVRdude.AVRdude.BoardType == "Nucleo" ) {
			if( deleUseSerialPort != null ) {
				deleUseSerialPort();
			}
			//G.VexLoaderBox.Run( TargetFile.TargetFileList[0] );
			
			/*
			if( G.VexLoaderBox == null ) {
				G.VexLoaderBox = new n_VexLoaderForm.VexLoaderForm();
			}
			G.VexLoaderBox.Run( null );
			*/
			
			if( G.ExportBox == null ) {
				G.ExportBox = new n_ExportForm.ExportForm();
			}
			ShowFloatMes = !G.ExportBox.Run( G.ccode.FilePath, G.ccode.FileName, n_Decode.Decode.ByteCodeList, n_Decode.Decode.BCLength );
		}
		else if( n_AVRdude.AVRdude.BoardType == "windows" ) {
			
			string tardir = G.ccode.FilePath + G.ccode.FileName;
			
			if( !Directory.Exists( tardir ) ) {
				Directory.CreateDirectory( tardir );
			}
			if( File.Exists( tardir + "\\" + G.ccode.FileName + ".exe" ) ) {
				File.Delete( tardir + "\\" + G.ccode.FileName + ".exe" );
			}
			File.Copy( n_OS.OS.SystemRoot + "vpc\\vpc.exe", tardir + "\\" + G.ccode.FileName + ".exe" );
			if( File.Exists( tardir + "\\ei.dll" ) ) {
				File.Delete( tardir + "\\ei.dll" );
			}
			File.Copy( n_OS.OS.SystemRoot + "vpc\\ei.dll", tardir + "\\ei.dll" );
			
			if( G.ExportBox == null ) {
				G.ExportBox = new n_ExportForm.ExportForm();
			}
			string code = G.ExportBox.GetCode( n_Decode.Decode.ByteCodeList, n_Decode.Decode.BCLength );
			n_OS.VIO.SaveTextFileUTF8( tardir + "\\app.h", code );
			
			if( !HasShowExe ) {
				HasShowExe = true;
				n_Debug.Warning.WarningMessage = "exe可执行程序已导出到当前目录下: " + tardir;
			}
		}
		else if( n_AVRdude.AVRdude.BoardType == "Edison" ) {
			
			//两种下载方式
			//A.VexLoaderBox.Run( TargetFile.TargetFileList[0] );
			//A.SSHLoaderBox.Run( TargetFile.TargetFileList[0] );
		}
		else if( n_Config.Config.GetCPU() == "ControlPad" ) {
			//...
		}
		else {
			if( Path.EndsWith( "."+ FileType.VMachineFile ) ) {
				return;
			}
			if( deleUseSerialPort != null ) {
				deleUseSerialPort();
			}
			if( AVRdude.Download( TargetFile.TargetFileList[0], SystemData.CreateHEXFile, true, true ) ) {
				AVRdude.Download( TargetFile.TargetFileList[0], SystemData.CreateHEXFile, true, false );
			}
		}
		
		//下载结束后提示信息
		if( ShowFloatMes && AVRdude.OK ) {
			
			if( !G.isCruxEXE ) {
				n_ImagePanel.ImagePanel.ShowMessage = true;
			}
			else {
				if( n_AVRdude.AVRdude.ErrorMes != null ) {
					if( G.ShowCruxCode && G.CGPanel.GHead.DeleUserCode != null ) {
						G.CGPanel.GHead.DeleUserCode();
					}
					n_ImagePanel.ImagePanel.ShowMessage = true;
				}
			}
		}
	}
}
}


