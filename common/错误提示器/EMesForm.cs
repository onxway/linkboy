﻿
namespace n_EMesForm
{
using System;
using System.Windows.Forms;

/// <summary>
/// Description of WorkForm.
/// </summary>
public partial class EMesForm : Form
{
	bool isOK;
	
	//主窗口
	public EMesForm()
	{
		//
		// The InitializeComponent() call is required for Windows Forms designer support.
		//
		InitializeComponent();
		this.MinimumSize = this.Size;
		this.MaximumSize = this.Size;
		//初始化
		//
		// TODO: Add constructor code after the InitializeComponent() call.
		//
	}
	
	//窗体运行
	public bool Run()
	{
		if( checkBox1.Checked ) {
			return false;
		}
		
		isOK = true;
		
		//注意,直接设置Visible不能使窗体获得焦点
		//this.Visible = true;
		this.ShowDialog();
		
		while( this.Visible ) {
			Application.DoEvents();
		}
		return isOK;
	}
	
	//窗体关闭事件
	void FindFormFormClosing(object sender, FormClosingEventArgs e)
	{
		e.Cancel = true;
		this.Visible = false;
	}
	
	void ButtonCancelClick(object sender, EventArgs e)
	{
		this.Visible = false;
	}
	
	void LinkLabel1LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
	{
		isOK = false;
		this.Visible = false;
	}
}
}


