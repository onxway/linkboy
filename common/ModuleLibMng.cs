﻿/*
 * 由SharpDevelop创建。
 * 用户： dell
 * 日期: 2016/8/12
 * 时间: 15:16
 * 
 * 要改变这种模板请点击 工具|选项|代码编写|编辑标准头文件
 */
using System;
using System.Drawing;
using System.Windows.Forms;
using n_OS;

namespace linkboy
{
	/// <summary>
	/// Description of ModuleLibMng.
	/// </summary>
	public partial class ModuleLibMng : Form
	{
		public ModuleLibMng()
		{
			InitializeComponent();
		}
		
		//运行
		public void Run()
		{
			ignoreCheck = true;
			this.treeView1.Nodes.Clear();
			LoadLib();
			ignoreCheck = false;
			
			this.Visible = true;
		}
		
		void ModuleLibMngFormClosing(object sender, FormClosingEventArgs e)
		{
			this.Visible = false;
			e.Cancel = true;
		}
		
		//----------------------------------------------------------------------------
		string CurrentFolderName;
		string LibConfig;
		bool ignoreFold;
		
		bool ignoreCheck;
		
		TreeNode CNode;
		
		//初始化, 加载元件库列表
		void LoadLib()
		{
			//读取库配置文件
			LibConfig = VIO.OpenTextFileGB2312( OS.SystemRoot + "RV" + OS.PATH_S + OS.USER + OS.PATH_S + "libconfig.txt" );
			
			//LibConfig = LibConfig.Split( '\n' )[0];
			
			string LibPath = OS.ModuleLibPath + OS.USER + ".txt";
			string text = VIO.OpenTextFileGB2312( LibPath );
			
			string[] Line = text.Split( '\n' );
			for( int i = 0; i < Line.Length; ++i ) {
				Line[ i ] = Line[ i ].Trim( " \t".ToCharArray() );
				if( Line[ i ].StartsWith( "//" ) ) {
					Line[ i ] = "";
				}
			}
			int Index = 0;
			
			ignoretemp = false;
			ReadFolderToNode( Line, ref Index );
		}
		
		bool ignoretemp;
		
		//从指定行开始添加元素到指定的节点上,遇到目录结束时返回
		void ReadFolderToNode( string[] Line, ref int Index )
		{
			for( ;Index < Line.Length; ++Index ) {
				string LineData = Line[ Index ];
				
				//如果是空项,跳过
				if( LineData == "" ) {
					continue;
				}
				//如果是目录结束,返回
				if( LineData.StartsWith( "</fold>" ) ) {
					ignoretemp = false;
					//deleLeaveFolder();
					
					break;
				}
				
				//如果是新目录,则创建节点并递归调用自身
				if( LineData.StartsWith( "<fold>" ) ) {
					int ld = LineData.LastIndexOf( ',' );
					string FoldName = LineData.Remove( ld ).Remove( 0, 6 );
					
					if( FoldName.StartsWith( "<" ) || FoldName.StartsWith( "——" ) ) {
						ignoretemp = true;
					}
					CurrentFolderName = FoldName;
					
					ignoreFold = LibConfig.IndexOf( "\n" + CurrentFolderName + "\n" ) != -1;
					
					//deleEnterFolder( FoldName );
					
					//if( !ignoretemp ) {
						CNode = this.treeView1.Nodes.Add( FoldName );
						CNode.Checked = !ignoreFold;
					//}
					
					++Index;
					ReadFolderToNode( Line, ref Index );
					continue;
				}
				//到这里说明是条目项
				if( LineData.StartsWith( "<item>" ) ) {
					if( !LineData.EndsWith( "," ) ) {
						MessageBox.Show( "<item> 缺少结尾逗号: " + LineData );
						continue;
					}
					string ItemName = LineData.Remove( LineData.Length - 1 ).Remove( 0, 6 );
					
					bool ignoreItem = LibConfig.IndexOf( "\n" + CurrentFolderName + ":" + ItemName + "\n" ) != -1;
					
					string Path = null;
					
					for( int i = Index + 1; i < Line.Length; ++i ) {
						if( Line[ i ] == "" ) {
							continue;
						}
						if( Line[ i ] == "</item>" ) {
							Index = i;
							break;
						}
						if( Line[ i ] != "<nextcolumn>" ) {
							string sPath = Line[ i ].Remove( 0, 1 + Line[ i ].IndexOf( ' ' ) );
							if( !sPath.EndsWith( "," ) ) {
								MessageBox.Show( "路径项缺少结尾逗号: " + sPath );
								continue;
							}
							sPath = sPath.Remove( sPath.Length - 1 );
							
							string ExtValue = null;
							if( sPath.EndsWith( ",90" ) ) {
								sPath = sPath.Remove( sPath.Length - 3 );
								ExtValue = "=90";
							}
							else {
								ExtValue = null;
							}
							Path += sPath + ExtValue + ",";
						}
						else {
							Path += Line[ i ] + ",";
						}
					}
					if( Path != null ) {
						//deleAddNode( ItemName, Path );
						if( !ignoretemp ) {
							TreeNode n = CNode.Nodes.Add( ItemName );
							n.Checked = !ignoreItem && !ignoreFold;
						}
					}
					continue;
				}
				if( LineData == "<<other>>" ) {
					continue;
					//...
				}
				//到这里为未知项
				MessageBox.Show( "装载组件库时遇到未知的类型: " + LineData );
			}
		}
		
		void TreeView1BeforeCheck(object sender, TreeViewCancelEventArgs e)
		{
			if( ignoreCheck ) {
				return;
			}
			ignoreCheck = true;
			if( !e.Node.Checked ) {
				if( e.Node.Parent != null ) {
					e.Node.Parent.Checked = true;
				}
			}
			else {
				foreach( TreeNode n in e.Node.Nodes ) {
					n.Checked = false;
				}
			}
			ignoreCheck = false;
		}
		
		void Button1Click(object sender, EventArgs e)
		{
			string R = "\n";
			foreach( TreeNode n in this.treeView1.Nodes ) {
				if( !n.Checked ) {
					R += n.Text + "\n";
				}
				else {
					foreach( TreeNode m in n.Nodes ) {
						if( !m.Checked ) {
							R += n.Text + ":" + m.Text + "\n";
						}
					}
				}
			}
			MessageBox.Show( R, "软件重启后将会隐藏的模块列表" );
			
			
			//写入库配置文件
			VIO.SaveTextFileGB2312( OS.SystemRoot + "RV" + OS.PATH_S + OS.USER + OS.PATH_S + "libconfig.txt", R );
		}
	}
}



