﻿

namespace n_VM
{
	using System;
	
	using n_Interface;
	using n_VCODE;
	
	//虚拟机
	public static class VM
	{
		public static bool isQuit;
		public static bool isError;
		
		static uint I_DP, I_TDP, I_TTDP;
		static long I_A, I_B;
		
		public static uint DP;
		public static uint TDP;
		public static uint TTDP;
		
		public static long A, B;
		public static byte[] BASE;
		const int BASE_LENGTH = 10000;
		
		static uint[] InterruptList;
		static byte[][] AVMParam;
		static int AVMParamNumber;
		static int InterruptAddIndex;
		static int InterruptReadIndex;
		static int VarInterruptIndex;
		const int InterruptLength = 2000;
		public static bool EA;
		static bool BEA;
		
		public static uint SP;
		
		public static uint PC;
		static uint BValue;
		static uint PCnext;
		
		public static byte[] ByteCodeList;
		
		public static uint StartPC;
		
		public static int[] InsListRec;
		public static int InsListRecLen;
		
		static bool Zip = true;
		
		public static void Init()
		{
			AVMParam = new byte[ InterruptLength ][];
			for( int i = 0; i < InterruptLength; ++i ) {
				AVMParam[ i ] = new byte[ 40 ];
			}
			InterruptList = new uint[ InterruptLength ];
			BASE = new byte[ BASE_LENGTH ];
			
			ByteCodeList = new byte[200000];
			
			InsListRec = new int[100000];
			InsListRecLen = 0;
		}
		
		public static string 显示()
		{
			string R = "0000:\n";
			for( int i = 0; i < 80; ++i ) {
				R += BASE[ i ].ToString( "X" ).PadLeft( 2, '0' ) + " ";
			}
			return
				R + "\n" +
				"INT:\t" + InterruptReadIndex + "\t" + InterruptAddIndex + "\n" +
				"SP:\t" + SP + "\n" +
				"PC:\t" + PC + "\n" +
			 	"DP:\t" + DP.ToString( "X" ).PadLeft( 4, '0' ) + "\t" + DP + "\n" +
				"TDP:\t" + TDP.ToString( "X" ).PadLeft( 4, '0' ) + "\t" + TDP + "\n" +
				"A:\t" + A.ToString( "X" ).PadLeft( 8, '0' ) + "\t" + A + "\n" +
				"B:\t" + B.ToString( "X" ).PadLeft( 8, '0' ) + "\t" + B;
		}
		
		public static void 复位()
		{
			PC = 0;
			SP = BASE_LENGTH - 1;
			DP = 0;
			TDP = 0;
			TTDP = 0;
			A = 0;
			B = 0;
			isQuit = false;
			isError = false;
			InterruptAddIndex = 0;
			InterruptReadIndex = 0;
			VarInterruptIndex = 0;
			AVMParamNumber = 0;
			EA = false;
			
			//清空内存区
			for( int i = 0; i < BASE_LENGTH; ++i ) {
				BASE[i] = 0;
			}
			
			//扩展插件
			p_VM.VM.Tick_ms = 0;
		}
		
		public static uint 运行()
		{
			PCnext = 1;
			
			byte BCode = ByteCodeList[ PC ];
			BValue = 0;
			ulong tB = 1;
			for( int i = 0; i < COM.GotoByteNumber; ++i ) {
				BValue += (uint)(tB * ByteCodeList[PC+i+1]);
				tB *= 0x100;
			}
			
			Next:
			switch( BCode ) {
				case C.N_接口调用:			接口调用(); break;
				case C.N_设置堆栈:		DP = BValue; PCnext = 4; break;
				case C.N_跳转:			PC = BValue; PCnext = 0; break;
				case C.N_函数调用:		函数调用(); PCnext = 0; break;
				case C.N_域返回:			域返回(); PCnext = 0; break;
				case C.N_返回:			返回(); PCnext = 0; break;
				case C.N_中断返回:		中断返回(); PCnext = 0; break;
				case C.N_堆栈递增:		TTDP = DP + BValue; TDP = TTDP; PCnext = 4; break;
				case C.N_堆栈递减:		DP -= BValue; PCnext = 4; break;
				case C.N_转移形参:		转移形参(); break;
				case C.N_保护现场:		保护现场(); break;
				case C.N_恢复现场:		恢复现场(); break;
				case C.N_STEP:
					if( n_GUIcoder.GUIcoder.isStep ) {
						n_GUIcoder.GUIcoder.isPause = 1;
					}
					break; //临时调试单步功能
				
				case C.N_空指令:
				case C.N_NULL:			break; //空指令();
				
				//case C.N_CLI:		EA = false; break;
				//case C.N_SEI:		EA = true; break;
				case C.N_CLI:		BEA = EA; EA = false; break;
				case C.N_SEI:		EA = BEA; break;
				
				case C.N_转移参数:		B = A; break;
				
				case C.N_出栈:			出栈(); break;
				case C.N_压栈:			压栈(); break;
				
				case C.N_SETA_uint8:
				case C.N_SETA_uint16:
				case C.N_SETA_uint32:
				case C.N_SETA_sint8:
				case C.N_SETA_sint16:
				case C.N_SETA_sint32:			SETA(); break;
				
				case C.N_SETB_uint8:
				case C.N_SETB_uint16:
				case C.N_SETB_uint32:
				case C.N_SETB_sint8:
				case C.N_SETB_sint16:
				case C.N_SETB_sint32:			SETB(); break;
				
				case C.N_指令地址:		A = BValue; PCnext = 4; break;
				
				case C.N_读取地址_local_uint32:		读取地址_local_uint32(); break;
				
				case C.N_分支散转_uint32:		分支散转_uint32(); break;
				case C.N_分支比较_uint32_uint8:	分支比较_uint32_uint8(); break;
				case C.N_肯定跳转_bool:		肯定跳转_bool(); break;
				case C.N_否定跳转_bool:		否定跳转_bool(); break;
				case C.N_跳转递减_x:		跳转递减_x(); break;
				
				
				case C.N_加载_0_static_bit:			A = Mem.Get_uint8( BValue ); PCnext = 4; break;
				case C.N_加载_0_static_bool:			A = Mem.Get_uint8( BValue ); PCnext = 4; break;
				case C.N_加载_0_static_uint8:		A = Mem.Get_uint8( BValue ); PCnext = 4; break;
				case C.N_加载_0_static_uint16:		A = Mem.Get_uint16( BValue ); PCnext = 4; break;
				case C.N_加载_0_static_uint24:		A = Mem.Get_uint24( BValue ); PCnext = 4; break;
				case C.N_加载_0_static_uint32:		A = Mem.Get_uint32( BValue ); PCnext = 4; break;
				case C.N_加载_0_static_sint8:		A = Mem.Get_sint8( BValue ); PCnext = 4; break;
				case C.N_加载_0_static_sint16:		A = Mem.Get_sint16( BValue ); PCnext = 4; break;
				case C.N_加载_0_static_sint24:		A = Mem.Get_sint24( BValue ); PCnext = 4; break;
				case C.N_加载_0_static_sint32:		A = Mem.Get_sint32( BValue ); PCnext = 4; break;
				
				case C.N_加载_1_static_bit:			B = Mem.Get_uint8( BValue ); PCnext = 4; break;
				case C.N_加载_1_static_bool:			B = Mem.Get_uint8( BValue ); PCnext = 4; break;
				case C.N_加载_1_static_uint8:		B = Mem.Get_uint8( BValue ); PCnext = 4; break;
				case C.N_加载_1_static_uint16:		B = Mem.Get_uint16( BValue ); PCnext = 4; break;
				case C.N_加载_1_static_uint24:		B = Mem.Get_uint24( BValue ); PCnext = 4; break;
				case C.N_加载_1_static_uint32:		B = Mem.Get_uint32( BValue ); PCnext = 4; break;
				case C.N_加载_1_static_sint8:		B = Mem.Get_sint8( BValue ); PCnext = 4; break;
				case C.N_加载_1_static_sint16:		B = Mem.Get_sint16( BValue ); PCnext = 4; break;
				case C.N_加载_1_static_sint24:		B = Mem.Get_sint24( BValue ); PCnext = 4; break;
				case C.N_加载_1_static_sint32:		B = Mem.Get_sint32( BValue ); PCnext = 4; break;
				
				case C.N_保存_0_static_bit:			Mem.Set_uint8( BValue, A ); PCnext = 4; break;
				case C.N_保存_0_static_bool:			Mem.Set_uint8( BValue, A ); PCnext = 4; break;
				case C.N_保存_0_static_uint8:		Mem.Set_uint8( BValue, A ); PCnext = 4; break;
				case C.N_保存_0_static_uint16:		Mem.Set_uint16( BValue, A ); PCnext = 4; break;
				case C.N_保存_0_static_uint24:		Mem.Set_uint24( BValue, A ); PCnext = 4; break;
				case C.N_保存_0_static_uint32:		Mem.Set_uint32( BValue, A ); PCnext = 4; break;
				case C.N_保存_0_static_sint8:		Mem.Set_sint8( BValue, A ); PCnext = 4; break;
				case C.N_保存_0_static_sint16:		Mem.Set_sint16( BValue, A ); PCnext = 4; break;
				case C.N_保存_0_static_sint24:		Mem.Set_sint24( BValue, A ); PCnext = 4; break;
				case C.N_保存_0_static_sint32:		Mem.Set_sint32( BValue, A ); PCnext = 4; break;
				
				case C.N_加载_0_local_bit:			A = Mem.Get_uint8( DP + BValue ); PCnext = 4; break;
				case C.N_加载_0_local_bool:			A = Mem.Get_uint8( DP + BValue ); PCnext = 4; break;
				case C.N_加载_0_local_uint8:			A = Mem.Get_uint8( DP + BValue ); PCnext = 4; break;
				case C.N_加载_0_local_uint16:		A = Mem.Get_uint16( DP + BValue ); PCnext = 4; break;
				case C.N_加载_0_local_uint24:		A = Mem.Get_uint24( DP + BValue ); PCnext = 4; break;
				case C.N_加载_0_local_uint32:		A = Mem.Get_uint32( DP + BValue ); PCnext = 4; break;
				case C.N_加载_0_local_sint8:			A = Mem.Get_sint8( DP + BValue ); PCnext = 4; break;
				case C.N_加载_0_local_sint16:		A = Mem.Get_sint16( DP + BValue ); PCnext = 4; break;
				case C.N_加载_0_local_sint24:		A = Mem.Get_sint24( DP + BValue ); PCnext = 4; break;
				case C.N_加载_0_local_sint32:		A = Mem.Get_sint32( DP + BValue ); PCnext = 4; break;
				
				case C.N_加载_1_local_bit:			B = Mem.Get_uint8( DP + BValue ); PCnext = 4; break;
				case C.N_加载_1_local_bool:			B = Mem.Get_uint8( DP + BValue ); PCnext = 4; break;
				case C.N_加载_1_local_uint8:			B = Mem.Get_uint8( DP + BValue ); PCnext = 4; break;
				case C.N_加载_1_local_uint16:		B = Mem.Get_uint16( DP + BValue ); PCnext = 4; break;
				case C.N_加载_1_local_uint24:		B = Mem.Get_uint24( DP + BValue ); PCnext = 4; break;
				case C.N_加载_1_local_uint32:		B = Mem.Get_uint32( DP + BValue ); PCnext = 4; break;
				case C.N_加载_1_local_sint8:			B = Mem.Get_sint8( DP + BValue ); PCnext = 4; break;
				case C.N_加载_1_local_sint16:		B = Mem.Get_sint16( DP + BValue ); PCnext = 4; break;
				case C.N_加载_1_local_sint24:		B = Mem.Get_sint24( DP + BValue ); PCnext = 4; break;
				case C.N_加载_1_local_sint32:		B = Mem.Get_sint32( DP + BValue ); PCnext = 4; break;
				
				case C.N_保存_0_local_bit:			Mem.Set_uint8( DP + BValue, A ); PCnext = 4; break;
				case C.N_保存_0_local_bool:			Mem.Set_uint8( DP + BValue, A ); PCnext = 4; break;
				case C.N_保存_0_local_uint8:			Mem.Set_uint8( DP + BValue, A ); PCnext = 4; break;
				case C.N_保存_0_local_uint16:		Mem.Set_uint16( DP + BValue, A ); PCnext = 4; break;
				case C.N_保存_0_local_uint24:		Mem.Set_uint24( DP + BValue, A ); PCnext = 4; break;
				case C.N_保存_0_local_uint32:		Mem.Set_uint32( DP + BValue, A ); PCnext = 4; break;
				case C.N_保存_0_local_sint8:			Mem.Set_sint8( DP + BValue, A ); PCnext = 4; break;
				case C.N_保存_0_local_sint16:		Mem.Set_sint16( DP + BValue, A ); PCnext = 4; break;
				case C.N_保存_0_local_sint24:		Mem.Set_sint24( DP + BValue, A ); PCnext = 4; break;
				case C.N_保存_0_local_sint32:		Mem.Set_sint32( DP + BValue, A ); PCnext = 4; break;
				
				case C.N_加载_SP_local_uint16:		SP = (uint)Mem.Get_uint16( DP + BValue ); PCnext = 4; break;
				case C.N_保存_SP_local_uint16:		Mem.Set_uint16( DP + BValue, SP ); PCnext = 4; break;
				
				//天哪, 忘了加 PCnext, 导致调试了两天 Delayer.... 郁闷
				//2019.3.15 临时改为了static方式读取参数, 用于python传递形参给原生函数
				case C.N_加载_PC_local_uint:
					if( COM.GotoByteNumber == 2 ) PC = (uint)Mem.Get_uint16( BValue );
					if( COM.GotoByteNumber == 3 ) PC = (uint)Mem.Get_uint24( BValue );
					if( COM.GotoByteNumber == 4 ) PC = (uint)Mem.Get_uint32( BValue );
					
					//switch( COM.GotoByteNumber ) {
					//	case 2:		PC = (uint)Mem.Get_uint16( DP + BValue ); break;
					//	case 3:		PC = (uint)Mem.Get_uint24( DP + BValue ); break;
					//	case 4:		PC = (uint)Mem.Get_uint32( DP + BValue ); break;
					//}
					PCnext = 0; break;
				
				/*
				case C.N_加载CODE_uint8:			A = Mem.GetCode_uint8( (uint)B ); break;
				case C.N_加载CODE_uint16:		A = Mem.GetCode_uint16( (uint)B ); break;
				case C.N_加载CODE_uint24:		A = Mem.GetCode_uint24( (uint)B ); break;
				case C.N_加载CODE_uint32:		A = Mem.GetCode_uint32( (uint)B ); break;
				*/
				case C.N_LOAD_local_uint8:
					if( (B & 0x00FF0000) == 0 ) {
						A = Mem.Get_uint8( (uint)B );
					}
					else {
						A = Mem.GetCode_uint8( ((uint)B & 0xFFFF) );
					}
					break;
				case C.N_LOAD_local_uint16:
					if( (B & 0x00FF0000) == 0 ) {
						A = Mem.Get_uint16( (uint)B );
					}
					else {
						A = Mem.GetCode_uint16( ((uint)B & 0xFFFF) );
					}
					break;
				case C.N_LOAD_local_uint24:
					if( (B & 0x00FF0000) == 0 ) {
						A = Mem.Get_uint24( (uint)B );
					}
					else {
						A = Mem.GetCode_uint24( ((uint)B & 0xFFFF) );
					}
					break;
				case C.N_LOAD_local_uint32:
					if( (B & 0x00FF0000) == 0 ) {
						A = Mem.Get_uint32( (uint)B );
					}
					else {
						A = Mem.GetCode_uint32( ((uint)B & 0xFFFF) );
					}
					break;
				
				case C.N_保存BASE_uint8:			Mem.Set_uint8( (uint)B, A ); break;
				case C.N_保存BASE_uint16:		Mem.Set_uint16( (uint)B, A ); break;
				case C.N_保存BASE_uint24:		Mem.Set_uint24( (uint)B, A ); break;
				case C.N_保存BASE_uint32:		Mem.Set_uint32( (uint)B, A ); break;
				
				case C.N_传递形参_bit:		Mem.Set_uint8( TDP, A ); TDP += 1; break;
				case C.N_传递形参_bool:		Mem.Set_uint8( TDP, A ); TDP += 1; break;
				case C.N_传递形参_uint8:		Mem.Set_uint8( TDP, A ); TDP += 1; break;
				case C.N_传递形参_uint16:		Mem.Set_uint16( TDP, A ); TDP += 2; break;
				case C.N_传递形参_uint24:		Mem.Set_uint24( TDP, A ); TDP += 3; break;
				case C.N_传递形参_uint32:		Mem.Set_uint32( TDP, A ); TDP += 4; break;
				case C.N_传递形参_sint8:		Mem.Set_uint8( TDP, A ); TDP += 1; break;
				case C.N_传递形参_sint16:		Mem.Set_uint16( TDP, A ); TDP += 2; break;
				case C.N_传递形参_sint24:		Mem.Set_uint24( TDP, A ); TDP += 3; break;
				case C.N_传递形参_sint32:		Mem.Set_uint32( TDP, A ); TDP += 4; break;
				
				case C.N_地址偏移_uintx_uintx_uintx:		A += B * BValue; PCnext = 4; break;
				case C.N_结构偏移_uintx_uintx:			A += BValue; PCnext = 4; break;
				
				case C.N_读系统量_sint8:		读系统量_sint8(); break;
				case C.N_读系统量_sint16:	读系统量_sint16(); break;
				case C.N_读系统量_sint24:	读系统量_sint24(); break;
				case C.N_读系统量_sint32:	读系统量_sint32(); break;
				
				case C.N_非_bool_bool:			非_bool_bool(); break;
				case C.N_与_bool_bool_bool:		与_bool_bool_bool(); break;
				case C.N_或_bool_bool_bool:		或_bool_bool_bool(); break;
				
				case C.N_强制转换_uint8_uint16:	强制转换_uint8_uint16(); break;
				case C.N_强制转换_uint8_uint32:	强制转换_uint8_uint32(); break;
				case C.N_强制转换_uint16_uint32:	强制转换_uint16_uint32(); break;
				case C.N_强制转换_sint8_sint16:	强制转换_sint8_sint16(); break;
				case C.N_强制转换_sint8_sint32:	强制转换_sint8_sint32(); break;
				case C.N_强制转换_sint16_sint32:	强制转换_sint16_sint32(); break;
				
				case C.N_强制转换_sint8_uint8:	强制转换_sint8_uint8(); break;
				case C.N_强制转换_sint16_uint16:	强制转换_sint16_uint16(); break;
				case C.N_强制转换_sint24_uint24:	强制转换_sint24_uint24(); break;
				case C.N_强制转换_sint32_uint32:	强制转换_sint32_uint32(); break;
				
				case C.N_强制转换_uint8_sint8:	强制转换_uint8_sint8(); break;
				case C.N_强制转换_uint16_sint16:	强制转换_uint16_sint16(); break;
				case C.N_强制转换_uint32_sint32:	强制转换_uint32_sint32(); break;
				
				case C.N_隐式转换_sint16_sint8:	隐式转换_sint16_sint8(); break;
				case C.N_隐式转换_sint32_sint8:	隐式转换_sint32_sint8(); break;
				case C.N_隐式转换_sint32_sint16:	隐式转换_sint32_sint16(); break;
				
				case C.N_隐式转换_fix_sint8:		隐式转换_fix_sint8(); break;
				case C.N_隐式转换_fix_sint16:	隐式转换_fix_sint16(); break;
				case C.N_隐式转换_fix_sint32:	隐式转换_fix_sint32(); break;
				case C.N_隐式转换_sint32_fix:	隐式转换_sint32_fix(); break;
				
				case C.N_取负_sint8_sint8:				取负_sint8_sint8(); break;
				case C.N_取负_sint16_sint16:				取负_sint16_sint16(); break;
				case C.N_取负_sint24_sint24:				取负_sint24_sint24(); break;
				case C.N_取负_sint32_sint32:				取负_sint32_sint32(); break;
				
				case C.N_取绝对值_x_x:			取绝对值_x_x(); break;
				
				case C.N_自加_x_x:				A++; break;
				case C.N_自减_x_x:				A--; break;
				
				case C.N_加_uint8_uint8_uint8:					加_uint8_uint8_uint8(); break;
				case C.N_加_uint16_uint16_uint16:				加_uint16_uint16_uint16(); break;
				case C.N_加_uint24_uint24_uint24:				加_uint24_uint24_uint24(); break;
				case C.N_加_uint32_uint32_uint32:				加_uint32_uint32_uint32(); break;
				case C.N_加_sint8_sint8_sint8:					加_sint8_sint8_sint8(); break;
				case C.N_加_sint16_sint16_sint16:				加_sint16_sint16_sint16(); break;
				case C.N_加_sint24_sint24_sint24:				加_sint24_sint24_sint24(); break;
				case C.N_加_sint32_sint32_sint32:				加_sint32_sint32_sint32(); break;
				
				case C.N_减_uint8_uint8_uint8:					减_uint8_uint8_uint8(); break;
				case C.N_减_uint16_uint16_uint16:				减_uint16_uint16_uint16(); break;
				case C.N_减_uint24_uint24_uint24:				减_uint24_uint24_uint24(); break;
				case C.N_减_uint32_uint32_uint32:				减_uint32_uint32_uint32(); break;
				case C.N_减_sint8_sint8_sint8:					减_sint8_sint8_sint8(); break;
				case C.N_减_sint16_sint16_sint16:				减_sint16_sint16_sint16(); break;
				case C.N_减_sint24_sint24_sint24:				减_sint24_sint24_sint24(); break;
				case C.N_减_sint32_sint32_sint32:				减_sint32_sint32_sint32(); break;
				
				case C.N_乘_uint8_uint8_uint8:					乘_uint8_uint8_uint8(); break;
				case C.N_乘_uint16_uint16_uint16:				乘_uint16_uint16_uint16(); break;
				case C.N_乘_uint24_uint24_uint24:				乘_uint24_uint24_uint24(); break;
				case C.N_乘_uint32_uint32_uint32:				乘_uint32_uint32_uint32(); break;
				case C.N_乘_sint8_sint8_sint8:					乘_sint8_sint8_sint8(); break;
				case C.N_乘_sint16_sint16_sint16:				乘_sint16_sint16_sint16(); break;
				case C.N_乘_sint24_sint24_sint24:				乘_sint24_sint24_sint24(); break;
				case C.N_乘_sint32_sint32_sint32:				乘_sint32_sint32_sint32(); break;
				case C.N_乘_fix_fix_fix:							乘_fix_fix_fix(); break;
				
				case C.N_除_uint8_uint8_uint8:					除_uint8_uint8_uint8(); break;
				case C.N_除_uint16_uint16_uint16:				除_uint16_uint16_uint16(); break;
				case C.N_除_uint24_uint24_uint24:				除_uint24_uint24_uint24(); break;
				case C.N_除_uint32_uint32_uint32:				除_uint32_uint32_uint32(); break;
				case C.N_除_sint8_sint8_sint8:					除_sint8_sint8_sint8(); break;
				case C.N_除_sint16_sint16_sint16:				除_sint16_sint16_sint16(); break;
				case C.N_除_sint24_sint24_sint24:				除_sint24_sint24_sint24(); break;
				case C.N_除_sint32_sint32_sint32:				除_sint32_sint32_sint32(); break;
				case C.N_除_fix_fix_fix:							除_fix_fix_fix(); break;
				
				case C.N_余_uint8_uint8_uint8:					余_uint8_uint8_uint8(); break;
				case C.N_余_uint16_uint16_uint16:				余_uint16_uint16_uint16(); break;
				case C.N_余_uint24_uint24_uint24:				余_uint24_uint24_uint24(); break;
				case C.N_余_uint32_uint32_uint32:				余_uint32_uint32_uint32(); break;
				case C.N_余_sint8_sint8_sint8:					余_sint8_sint8_sint8(); break;
				case C.N_余_sint16_sint16_sint16:				余_sint16_sint16_sint16(); break;
				case C.N_余_sint24_sint24_sint24:				余_sint24_sint24_sint24(); break;
				case C.N_余_sint32_sint32_sint32:				余_sint32_sint32_sint32(); break;
				
				case C.N_大于_bool_uint_uint:
				case C.N_大于_bool_sint_sint:		大于_bool_x_x(); break;
				case C.N_小于_bool_uint_uint:
				case C.N_小于_bool_sint_sint:		小于_bool_x_x(); break;
				case C.N_大于等于_bool_uint_uint:
				case C.N_大于等于_bool_sint_sint:		大于等于_bool_x_x(); break;
				case C.N_小于等于_bool_uint_uint:
				case C.N_小于等于_bool_sint_sint:		小于等于_bool_x_x(); break;
				case C.N_等于_bool_uint_uint:
				case C.N_等于_bool_sint_sint:		等于_bool_x_x(); break;
				case C.N_不等于_bool_uint_uint:
				case C.N_不等于_bool_sint_sint:		不等于_bool_x_x(); break;
				
				case C.N_取反_bit_bit:			A ^= 0x00000001; break;
				case C.N_取反_uint8_uint8:		A ^= 0x000000ff; break;
				case C.N_取反_uint16_uint16:		A ^= 0x0000ffff; break;
				case C.N_取反_uint24_uint24:		A ^= 0x00ffffff; break;
				case C.N_取反_uint32_uint32:		A ^= 0xffffffff; break;
				
				case C.N_与_x_x_x:					A &= B; break;
				case C.N_或_x_x_x:					A |= B; break;
				case C.N_异或_x_x_x:					A ^= B; break;
				
				case C.N_补0右移_x_x_x:						补0右移_x_x_x(); break;
				case C.N_补0左移_uint8_uint8_uint8:			补0左移_uint8_uint8_uint8(); break;
				case C.N_补0左移_uint16_uint16_uint8:		补0左移_uint16_uint16_uint8(); break;
				case C.N_补0左移_uint24_uint24_uint8:		补0左移_uint24_uint24_uint8(); break;
				case C.N_补0左移_uint32_uint32_uint8:		补0左移_uint32_uint32_uint8(); break;
				
				case C.N_读取分量_bit_uint8:		读取分量_bit_uint8(); break;
				case C.N_读取分量_bit_uint16:		读取分量_bit_uint16(); break;
				case C.N_读取分量_bit_uint32:		读取分量_bit_uint32(); break;
				case C.N_读取分量_uint8_uint32:		读取分量_uint8_uint32(); break;
				case C.N_读取分量_uint8_uint16:		读取分量_uint8_uint16(); break;
				case C.N_读取分量_uint16_uint32:	读取分量_uint16_uint32(); break;
				
				case C.N_写入分量_uint8_uint8_bit:		写入分量_uint8_uint8_bit(); break;
				case C.N_写入分量_uint16_uint16_bit:		写入分量_uint16_uint16_bit(); break;
				case C.N_写入分量_uint32_uint32_bit:		写入分量_uint32_uint32_bit(); break;
				case C.N_写入分量_uint16_uint16_uint8:		写入分量_uint16_uint16_uint8(); break;
				case C.N_写入分量_uint32_uint32_uint16:		写入分量_uint32_uint32_uint16(); break;
				case C.N_写入分量_uint32_uint32_uint8:		写入分量_uint32_uint32_uint8(); break;
				
				
				default: 
					string ErrMes = "<VM运行> 未知的虚拟机指令: " + BCode + " ADDR:" + PC + "(0x" + PC.ToString( "X" ) + ")   PreIns:" + ByteCodeList[PC-2] + " CIns:" + ByteCodeList[PC-1];
					n_Debug.Warning.BUG( ErrMes );
					//throw new Exception( ErrMes );
					n_Interface.AVM.Stop();
					break;
			}
			if( PCnext == 1 ) {
				PCnext = 4;
				if( Zip && BValue != 0 ) {
					
					//n_Debug.Debug.Message += BValue + " ";
					
					BCode = (byte)(BValue & 0xFF);
					BValue >>= 8;
					goto Next;
				}
			}
			PC += PCnext;
			
			if( EA && InterruptReadIndex != InterruptAddIndex ) {
				EA = false;
				
				uint Addr = PC;
				for( int i = 0; i < COM.GotoByteNumber; ++i ) {
					BASE[ SP ] = (byte)(Addr%256);
					--SP;
					Addr /= 256;
				}
				//BASE[ SP ] = (byte)(PC%256);
				//--SP;
				//BASE[ SP ] = (byte)(PC/256);
				//--SP;
				
				uint TPC = InterruptList[ InterruptReadIndex ];
				PC = TPC * (1 + COM.GotoByteNumber);
				
				VarInterruptIndex = InterruptReadIndex;
				++InterruptReadIndex;
				if( InterruptReadIndex == InterruptLength ) {
					InterruptReadIndex = 0;
				}
			}
			return PC;
		}
		
		public static void 添加中断事件( int Addr )
		{
			InterruptList[ InterruptAddIndex ] = (uint)Addr;
			++InterruptAddIndex;
			if( InterruptAddIndex == InterruptLength ) {
				InterruptAddIndex = 0;
			}
		}
		
		public static void 清空系统参数()
		{
			AVMParamNumber = 0;
		}
		
		public static void 添加系统参数_int( int Data )
		{
			ulong Data1;
			if( Data < 0 ) {
				Data1 = (ulong)-Data;
				Data1 ^= 0xffffffff;
				Data1 += 1;
			}
			else {
				Data1 = (ulong)Data;
			}
			AVMParam[ InterruptAddIndex ][ AVMParamNumber ] = (byte)( Data1 % 256u );
			AVMParam[ InterruptAddIndex ][ AVMParamNumber + 1 ] = (byte)( Data1 / 256u % 256u );
			AVMParam[ InterruptAddIndex ][ AVMParamNumber + 2 ] = (byte)( Data1 / 256u / 256u % 256u );
			AVMParam[ InterruptAddIndex ][ AVMParamNumber + 3 ] = (byte)( Data1 / 256u / 256u / 256u % 256u );
			AVMParamNumber += 4;
		}
		
		public static void 添加系统参数_uint( uint Data )
		{
			AVMParam[ InterruptAddIndex ][ AVMParamNumber ] = (byte)( Data % 256u );
			AVMParam[ InterruptAddIndex ][ AVMParamNumber + 1 ] = (byte)( Data / 256u % 256u );
			AVMParam[ InterruptAddIndex ][ AVMParamNumber + 2 ] = (byte)( Data / 256u / 256u % 256u );
			AVMParam[ InterruptAddIndex ][ AVMParamNumber + 3 ] = (byte)( Data / 256u / 256u / 256u % 256u );
			AVMParamNumber += 4;
		}
		
		//-------------------------------------------------------------
		
		static void 转移形参()
		{
			uint Number = BValue;
			PCnext = 4;
			for( uint i = 0; i < Number; ++i ) {
				BASE[ DP + i ] = AVMParam[ VarInterruptIndex ][ i ];
			}
		}
		
		static void 函数调用()
		{
			DP = TTDP;
			
			long Addr = PC + 4;
			for( int i = 0; i < COM.GotoByteNumber; ++i ) {
				BASE[ SP ] = (byte)(Addr%256);
				--SP;
				Addr /= 256;
			}
			
			//BASE[ SP ] = (byte)((PC + COM.GotoByteNumber)%256);
			//--SP;
			//BASE[ SP ] = (byte)((PC + COM.GotoByteNumber)/256);
			//--SP;
			
			PC = BValue;
		}
		
		static void 返回()
		{
			if( SP >= BASE_LENGTH ) {
				isQuit = true;
			}
			else {
				
				uint Addr = 0;
				for( int i = 0; i < COM.GotoByteNumber; ++i ) {
					++SP;
					Addr *= 256;
					Addr += (uint)BASE[ SP ];
				}
				PC = Addr;
				
				//++SP;
				//PC = 256 * (uint)BASE[ SP ];
				//++SP;
				//PC += (uint)BASE[ SP ];
			}
		}
		
		static void 中断返回()
		{
			返回();
			EA = true;
		}
		
		static void 域返回()
		{
			A = PC;
			返回();
		}
		
		static void 保护现场()
		{
			I_DP = DP;
			I_TDP = TDP;
			I_TTDP = TTDP;
			I_A = A;
			I_B = B;
			
			//设置中断函数的局部变量堆栈
			DP = BASE_LENGTH - 1000;
		}
		
		static void 恢复现场()
		{
			DP = I_DP;
			TDP = I_TDP;
			TTDP = I_TTDP;
			A = I_A;
			B = I_B;
		}
		
		static void 出栈()
		{
			++SP;
			DP = 256 * (uint)BASE[ SP ];
			++SP;
			DP += (uint)BASE[ SP ];
		}
		
		static void 压栈()
		{
			BASE[ SP ] = (byte)(DP%256);
			--SP;
			BASE[ SP ] = (byte)(DP/256);
			--SP;
		}
		
		//-------------------------------------------------------------
		
		static void 接口调用()
		{
			BValue = 0;
			ulong tB = 1;
			for( int i = 0; i < COM.InterfaceByteNumber; ++i ) {
				BValue += (uint)(tB * ByteCodeList[PC+i+4]);
				tB *= 0x100;
			}
			PCnext = 8;
			DP = TTDP;
			Interface.Deal( BValue );
		}
		
		static void SETA()
		{
			if( BValue < 0x800000 ) {
				PCnext = 4;
				A = BValue;
			}
			else {
				BValue = ByteCodeList[PC+7]; BValue <<= 8;
				BValue |= ByteCodeList[PC+6]; BValue <<= 8;
				BValue |= ByteCodeList[PC+5]; BValue <<= 8;
				BValue |= ByteCodeList[PC+4];
				PCnext = 8;
				A = BValue;
			}
		}
		
		static void SETB()
		{
			if( BValue < 0x800000 ) {
				PCnext = 4;
				B = BValue;
			}
			else {
				BValue = ByteCodeList[PC+7]; BValue <<= 8;
				BValue |= ByteCodeList[PC+6]; BValue <<= 8;
				BValue |= ByteCodeList[PC+5]; BValue <<= 8;
				BValue |= ByteCodeList[PC+4];
				PCnext = 8;
				B = BValue;
			}
		}
		
		static void 读取地址_local_uint32()
		{
			BValue = 0;
			ulong tB = 1;
			for( int i = 0; i < COM.ConstByteNumber; ++i ) {
				BValue += (uint)(tB * ByteCodeList[PC+i+4]);
				tB *= 0x100;
			}
			PCnext = 8;
			A = DP + BValue;
		}
		
		//-------------------------------------------------------------
		static void 分支比较_uint32_uint8()
		{
			uint addr = 0;
			ulong tB = 1;
			for( int i = 0; i < COM.GotoByteNumber; ++i ) {
				addr += (uint)(tB * ByteCodeList[PC+i+4]);
				tB *= 0x100;
			}
			PCnext = 8;
			bool ok = false;
			A &= 0xFFFF;
			for( int i = 0; i < BValue; ++i ) {
				long a = Mem.GetCode_uint8( (uint)(A + i) );
				if( a == B ) {
					B = i;
					ok = true;
					break;
				}
			}
			if( !ok ) {
				PC = addr;
				PCnext = 0;
			}
		}
		
		static void 分支散转_uint32()
		{
			A &= 0xFFFF;
			A += B << 2;
			PC = (uint)Mem.GetCode_uint32( (uint)A );
			PCnext = 0;
		}
		
		//-------------------------------------------------------------
		static void 肯定跳转_bool()
		{
			PCnext = 4;
			if( A == 1 ) {
				PC = BValue;
				PCnext = 0;
			}
		}
		
		static void 否定跳转_bool()
		{
			PCnext = 4;
			if( A == 0 ) {
				PC = BValue;
				PCnext = 0;
			}
		}
		
		static void 跳转递减_x()
		{
			PCnext = 4;
			if( A == 0 ) {
				PC = BValue;
				PCnext = 0;
			}
			else {
				--A;
			}
		}
		
		//-------------------------------------------------------------
		
		static void 读系统量_sint8()
		{
			if( ( A & 0x80 ) != 0 ) {
				A = -( ( A ^ 0xff ) + 1 );
			}
		}
		
		static void 读系统量_sint16()
		{
			if( ( A & 0x8000 ) != 0 ) {
				A = -( ( A ^ 0xffff ) + 1 );
			}
		}
		
		static void 读系统量_sint24()
		{
			if( ( A & 0x800000 ) != 0 ) {
				A = -( ( A ^ 0xffffff ) + 1 );
			}
		}
		
		static void 读系统量_sint32()
		{
			if( ( A & 0x80000000 ) != 0 ) {
				A = -( ( A ^ 0xffffffff ) + 1 );
			}
		}
		
		//-------------------------------------------------------------
		static void 非_bool_bool()
		{
			if( A == 0 ) {
				A = 1;
			}
			else {
				A = 0;
			}
		}
		
		static void 与_bool_bool_bool()
		{
			if( A != 0 && B != 0 ) {
				A = 1;
			}
			else {
				A = 0;
			}
		}
		
		static void 或_bool_bool_bool()
		{
			if( A != 0 || B != 0 ) {
				A = 1;
			}
			else {
				A = 0;
			}
		}
		
		//-------------------------------------------------------------
		static void 强制转换_uint8_uint16()
		{
			A &= 0xff;
		}
		
		static void 强制转换_uint8_uint32()
		{
			A &= 0xff;
		}
		
		static void 强制转换_uint16_uint32()
		{
			A &= 0xffff;
		}
		
		static void 强制转换_sint8_sint16()
		{
			A &= 0xff;
			读系统量_sint8();
		}
		
		static void 强制转换_sint8_sint32()
		{
			A &= 0xff;
			读系统量_sint8();
		}
		
		static void 强制转换_sint16_sint32()
		{
			A &= 0xffff;
			读系统量_sint16();
		}
		
		static void 强制转换_sint8_uint8()
		{
			读系统量_sint8();
		}
		
		static void 强制转换_sint16_uint16()
		{
			读系统量_sint16();
		}
		
		static void 强制转换_sint24_uint24()
		{
			读系统量_sint24();
		}
		
		static void 强制转换_sint32_uint32()
		{
			读系统量_sint32();
		}
		
		static void 强制转换_uint8_sint8()
		{
			A &= 0xFF;
		}
		
		static void 强制转换_uint16_sint16()
		{
			A &= 0xFFFF;
		}
		
		static void 强制转换_uint32_sint32()
		{
			A &= 0xFFFFFFFF;
		}
		
		//-------------------------------------------------------------
		static void 隐式转换_sint16_sint8()
		{
			A &= 0xff;
			读系统量_sint8();
		}
		
		static void 隐式转换_sint32_sint8()
		{
			A &= 0xff;
			读系统量_sint8();
		}
		
		static void 隐式转换_sint32_sint16()
		{
			A &= 0xffff;
			读系统量_sint16();
		}
		
		static void 隐式转换_fix_sint8()
		{
			读系统量_sint8();
			A *= n_ConstString.ConstString.NFixScale;
			A &= 0xFFFFFFFF;
			读系统量_sint32();
		}
		
		static void 隐式转换_fix_sint16()
		{
			读系统量_sint16();
			A *= n_ConstString.ConstString.NFixScale;
			A &= 0xFFFFFFFF;
			读系统量_sint32();
		}
		
		static void 隐式转换_fix_sint32()
		{
			A *= n_ConstString.ConstString.NFixScale;
			A &= 0xFFFFFFFF;
			读系统量_sint32();
		}
		
		static void 隐式转换_sint32_fix()
		{
			A /= n_ConstString.ConstString.NFixScale;
			A &= 0xFFFFFFFF;
			读系统量_sint32();
		}
		
		//-------------------------------------------------------------
		static void 取负_sint8_sint8()
		{
			A = -A;
			A &= 0xff;
			读系统量_sint8();
		}
		
		static void 取负_sint16_sint16()
		{
			A = -A;
			A &= 0xffff;
			读系统量_sint16();
		}
		
		static void 取负_sint24_sint24()
		{
			A = -A;
			A &= 0xffffff;
			读系统量_sint24();
		}
		
		static void 取负_sint32_sint32()
		{
			A = -A;
			A &= 0xffffffff;
			读系统量_sint32();
		}
		
		static void 取绝对值_x_x()
		{
			if( A < 0 ) {
				A = -A;
			}
		}
		
		static void 加_uint8_uint8_uint8()
		{
			A += B;
			A &= 0xff;
		}
		
		static void 加_uint16_uint16_uint16()
		{
			A += B;
			A &= 0xffff;
		}
		
		static void 加_uint24_uint24_uint24()
		{
			A += B;
			A &= 0xffffff;
		}
		
		static void 加_uint32_uint32_uint32()
		{
			A += B;
			A &= 0xffffffff;
		}
		
		static void 加_sint8_sint8_sint8()
		{
			A += B;
			A &= 0xff;
			读系统量_sint8();
		}
		
		static void 加_sint16_sint16_sint16()
		{
			A += B;
			A &= 0xffff;
			读系统量_sint16();
		}
		
		static void 加_sint24_sint24_sint24()
		{
			A += B;
			A &= 0xffffff;
			读系统量_sint24();
		}
		
		static void 加_sint32_sint32_sint32()
		{
			A += B;
			A &= 0xffffffff;
			读系统量_sint32();
		}
		
		static void 减_uint8_uint8_uint8()
		{
			A -= B;
			A &= 0xff;
		}
		
		static void 减_uint16_uint16_uint16()
		{
			A -= B;
			A &= 0xffff;
		}
		
		static void 减_uint24_uint24_uint24()
		{
			A -= B;
			A &= 0xffffff;
		}
		
		static void 减_uint32_uint32_uint32()
		{
			A -= B;
			A &= 0xffffffff;
		}
		
		static void 减_sint8_sint8_sint8()
		{
			A -= B;
			A &= 0xff;
			读系统量_sint8();
		}
		
		static void 减_sint16_sint16_sint16()
		{
			A -= B;
			A &= 0xffff;
			读系统量_sint16();
		}
		
		static void 减_sint24_sint24_sint24()
		{
			A -= B;
			A &= 0xffffff;
			读系统量_sint24();
		}
		
		static void 减_sint32_sint32_sint32()
		{
			A -= B;
			A &= 0xffffffff;
			读系统量_sint32();
		}
		
		static void 乘_uint8_uint8_uint8()
		{
			A *= B;
			A &= 0xff;
		}
		
		static void 乘_uint16_uint16_uint16()
		{
			A *= B;
			A &= 0xffff;
		}
		
		static void 乘_uint24_uint24_uint24()
		{
			A *= B;
			A &= 0xffffff;
		}
		
		static void 乘_uint32_uint32_uint32()
		{
			A *= B;
			A &= 0xffffffff;
		}
		
		static void 乘_sint8_sint8_sint8()
		{
			A *= B;
			A &= 0xff;
			读系统量_sint8();
		}
		
		static void 乘_sint16_sint16_sint16()
		{
			A *= B;
			A &= 0xffff;
			读系统量_sint16();
		}
		
		static void 乘_sint24_sint24_sint24()
		{
			A *= B;
			A &= 0xffffff;
			读系统量_sint24();
		}
		
		static void 乘_sint32_sint32_sint32()
		{
			A *= B;
			A &= 0xffffffff;
			读系统量_sint32();
		}
		
		static void 乘_fix_fix_fix()
		{
			A *= B;
			A /= n_ConstString.ConstString.NFixScale;
			A &= 0xffffffff;
			读系统量_sint32();
		}
		
		static void 除_uint8_uint8_uint8()
		{
			A /= B;
			A &= 0xff;
		}
		
		static void 除_uint16_uint16_uint16()
		{
			A /= B;
			A &= 0xffff;
		}
		
		static void 除_uint24_uint24_uint24()
		{
			A /= B;
			A &= 0xffffff;
		}
		
		static void 除_uint32_uint32_uint32()
		{
			A /= B;
			A &= 0xffffffff;
		}
		
		static void 除_sint8_sint8_sint8()
		{
			A /= B;
			A &= 0xff;
			读系统量_sint8();
		}
		
		static void 除_sint16_sint16_sint16()
		{
			A /= B;
			A &= 0xffff;
			读系统量_sint16();
		}
		
		static void 除_sint24_sint24_sint24()
		{
			A /= B;
			A &= 0xffffff;
			读系统量_sint24();
		}
		
		static void 除_sint32_sint32_sint32()
		{
			A /= B;
			A &= 0xffffffff;
			读系统量_sint32();
		}
		
		static void 除_fix_fix_fix()
		{
			if( B == 0 ) {
				return;
			}
			long m = A%B;
			A /= B;
			A *= n_ConstString.ConstString.NFixScale;
			A += m * n_ConstString.ConstString.NFixScale / B;
			A &= 0xffffffff;
			读系统量_sint32();
		}
		
		static void 余_uint8_uint8_uint8()
		{
			A %= B;
			A &= 0xff;
		}
		
		static void 余_uint16_uint16_uint16()
		{
			A %= B;
			A &= 0xffff;
		}
		
		static void 余_uint24_uint24_uint24()
		{
			A %= B;
			A &= 0xffffff;
		}
		
		static void 余_uint32_uint32_uint32()
		{
			A %= B;
			A &= 0xffffffff;
		}
		
		static void 余_sint8_sint8_sint8()
		{
			A %= B;
			A &= 0xff;
			读系统量_sint8();
		}
		
		static void 余_sint16_sint16_sint16()
		{
			A %= B;
			A &= 0xffff;
			读系统量_sint16();
		}
		
		static void 余_sint24_sint24_sint24()
		{
			A %= B;
			A &= 0xffffff;
			读系统量_sint24();
		}
		
		static void 余_sint32_sint32_sint32()
		{
			A %= B;
			A &= 0xffffffff;
			读系统量_sint32();
		}
		
		//-------------------------------------------------------------
		static void 大于_bool_x_x()
		{
			if( A > B ) {
				A = 1;
			}
			else {
				A = 0;
			}
		}
		
		static void 大于等于_bool_x_x()
		{
			if( A >= B ) {
				A = 1;
			}
			else {
				A = 0;
			}
		}
		
		static void 小于_bool_x_x()
		{
			if( A < B ) {
				A = 1;
			}
			else {
				A = 0;
			}
		}
		
		static void 小于等于_bool_x_x()
		{
			if( A <= B ) {
				A = 1;
			}
			else {
				A = 0;
			}
		}
		
		static void 等于_bool_x_x()
		{
			if( A == B ) {
				A = 1;
			}
			else {
				A = 0;
			}
		}
		
		static void 不等于_bool_x_x()
		{
			if( A != B ) {
				A = 1;
			}
			else {
				A = 0;
			}
		}
		
		//-------------------------------------------------------------
		static void 补0右移_x_x_x()
		{
			int b = (int)( B );
			
			//A >>= b;
			ulong ul = (ulong)A;
			ul >>= b;
			A = (long)ul;
		}
		
		//-------------------------------------------------------------
		static void 补0左移_uint8_uint8_uint8()
		{
			int b = (int)( B );
			
			//A <<= b;
			ulong ul = (ulong)A;
			ul <<= b;
			A = (long)ul;
			
			A &= 0xff;
		}
		
		static void 补0左移_uint16_uint16_uint8()
		{
			int b = (int)( B );
			
			//A <<= b;
			ulong ul = (ulong)A;
			ul <<= b;
			A = (long)ul;
			
			A &= 0xffff;
		}
		
		static void 补0左移_uint24_uint24_uint8()
		{
			int b = (int)( B );
			
			//A <<= b;
			ulong ul = (ulong)A;
			ul <<= b;
			A = (long)ul;
			
			A &= 0xffffff;
		}
		
		static void 补0左移_uint32_uint32_uint8()
		{
			int b = (int)( B );
			
			//A <<= b;
			ulong ul = (ulong)A;
			ul <<= b;
			A = (long)ul;
			
			A &= 0xffffffff;
		}
		
		//-------------------------------------------------------------
		static void 读取分量_bit_uint8()
		{
			PCnext = 4;
			int Index = (int)BValue;
			A = A >> Index;
			A &= 0x01;
		}
		
		static void 读取分量_bit_uint16()
		{
			PCnext = 4;
			int Index = (int)BValue;
			A = A >> Index;
			A &= 0x01;
		}
		
		static void 读取分量_bit_uint32()
		{
			PCnext = 4;
			int Index = (int)BValue;
			A = A >> Index;
			A &= 0x01;
		}
		
		static void 读取分量_uint8_uint32()
		{
			PCnext = 4;
			int Index = (int)BValue;
			A = A >> Index;
			A &= 0xFF;
		}
		
		static void 读取分量_uint8_uint16()
		{
			PCnext = 4;
			int Index = (int)BValue;
			A = A >> Index;
			A &= 0xFF;
		}
		
		static void 读取分量_uint16_uint32()
		{
			PCnext = 4;
			int Index = (int)BValue;
			A = A >> Index;
			A &= 0xFFFF;
		}
		
		static void 写入分量_uint8_uint8_bit()
		{
			PCnext = 4;
			int Index = (int)BValue;
			long BB = (1 << Index) & 0xFF;
			long NBB = BB ^ 0xFF;
			
			if( B == 0 ) {
				A &= NBB;
			}
			else {
				A |= BB;
			}
		}
		
		static void 写入分量_uint16_uint16_bit()
		{
			PCnext = 4;
			int Index = (int)BValue;
			long BB = (1 << Index) & 0xFFFF;
			long NBB = BB ^ 0xFFFF;
			
			if( B == 0 ) {
				A &= NBB;
			}
			else {
				A |= BB;
			}
		}
		
		static void 写入分量_uint32_uint32_bit()
		{
			PCnext = 4;
			int Index = (int)BValue;
			long BB = (1 << Index) & 0xFFFFFFFF;
			long NBB = BB ^ 0xFFFFFFFF;
			
			if( B == 0 ) {
				A &= NBB;
			}
			else {
				A |= BB;
			}
		}
		
		static void 写入分量_uint16_uint16_uint8()
		{
			PCnext = 4;
			int Index = (int)BValue;
			long BB = (B << Index);
			long CA = ((uint)0xFF << Index) ^ (long)0xFFFF;
			A &= CA;
			A |= BB;
		}
		
		static void 写入分量_uint32_uint32_uint16()
		{
			PCnext = 4;
			int Index = (int)BValue;
			long BB = (B << Index);
			long CA = ((uint)0xFFFF << Index) ^ (long)0xFFFFFFFF;
			A &= CA;
			A |= BB;
		}
		
		static void 写入分量_uint32_uint32_uint8()
		{
			PCnext = 4;
			int Index = (int)BValue;
			long BB = (B << Index);
			long CA = ((uint)0xFF << Index) ^ (long)0xFFFFFFFF;
			A &= CA;
			A |= BB;
		}
		
		//============================================================================
		//存储器访问接口类
		public static class Mem
		{
			public static long Get_uint8( uint Addr )
			{
				long data = VM.BASE[ Addr ];
				return data;
			}
			
			public static long Get_sint8( uint Addr )
			{
				long data = VM.BASE[ Addr ];
				if( ( data & 0x80 ) != 0 ) {
					data = -( ( data ^ 0xff ) + 1 );
				}
				return data;
			}
			
			public static long Get_uint16( uint Addr )
			{
				long data = VM.BASE[ Addr ];
				data += VM.BASE[ Addr + 1 ] * 256u;
				return data;
			}
			
			public static long Get_sint16( uint Addr )
			{
				long data = VM.BASE[ Addr ];
				data += VM.BASE[ Addr + 1 ] * 256u;
				if( ( data & 0x8000 ) != 0 ) {
					data = -( ( data ^ 0xffff ) + 1 );
				}
				return data;
			}
			
			public static long Get_uint24( uint Addr )
			{
				long data = VM.BASE[ Addr ];
				data += VM.BASE[ Addr + 1 ] * 256u;
				data += VM.BASE[ Addr + 2 ] * 256u * 256u;
				return data;
			}
			
			public static long Get_sint24( uint Addr )
			{
				long data = VM.BASE[ Addr ];
				data += VM.BASE[ Addr + 1 ] * 256u;
				data += VM.BASE[ Addr + 2 ] * 256u * 256u;
				if( ( data & 0x800000 ) != 0 ) {
					data = -( ( data ^ 0xffffff ) + 1 );
				}
				return data;
			}
			
			public static long Get_uint32( uint Addr )
			{
				long data = VM.BASE[ Addr ];
				data += VM.BASE[ Addr + 1 ] * 256u;
				data += VM.BASE[ Addr + 2 ] * 256u * 256u;
				data += VM.BASE[ Addr + 3 ] * 256u * 256u * 256u;
				return data;
			}
			
			public static long Get_sint32( uint Addr )
			{
				long data = VM.BASE[ Addr ];
				data += VM.BASE[ Addr + 1 ] * 256u;
				data += VM.BASE[ Addr + 2 ] * 256u * 256u;
				data += VM.BASE[ Addr + 3 ] * 256u * 256u * 256u;
				if( ( data & 0x80000000 ) != 0 ) {
					data = -( ( data ^ 0xffffffff ) + 1 );
				}
				return data;
			}
			
			public static void Set_uint8( uint Addr, long Data )
			{
				ulong Data1;
				if( Data < 0 ) {
					Data1 = (ulong)-Data;
					Data1 ^= 0xffffffff;
					Data1 += 1;
				}
				else {
					Data1 = (ulong)Data;
				}
				VM.BASE[ Addr ] = (byte)( Data1 % 256u );
			}
			
			public static void Set_sint8( uint Addr, long Data )
			{
				ulong Data1;
				if( Data < 0 ) {
					Data1 = (ulong)-Data;
					Data1 ^= 0xffffffff;
					Data1 += 1;
				}
				else {
					Data1 = (ulong)Data;
				}
				VM.BASE[ Addr ] = (byte)( Data1 % 256u );
			}
			
			public static void Set_uint16( uint Addr, long Data )
			{
				ulong Data1;
				if( Data < 0 ) {
					Data1 = (ulong)-Data;
					Data1 ^= 0xffffffff;
					Data1 += 1;
				}
				else {
					Data1 = (ulong)Data;
				}
				VM.BASE[ Addr ] = (byte)( Data1 % 256u );
				VM.BASE[ Addr + 1 ] = (byte)( Data1 / 256u % 256u );
			}
			
			public static void Set_sint16( uint Addr, long Data )
			{
				ulong Data1;
				if( Data < 0 ) {
					Data1 = (ulong)-Data;
					Data1 ^= 0xffffffff;
					Data1 += 1;
				}
				else {
					Data1 = (ulong)Data;
				}
				VM.BASE[ Addr ] = (byte)( Data1 % 256u );
				VM.BASE[ Addr + 1 ] = (byte)( Data1 / 256u % 256u );
			}
			
			public static void Set_uint24( uint Addr, long Data )
			{
				ulong Data1;
				if( Data < 0 ) {
					Data1 = (ulong)-Data;
					Data1 ^= 0xffffffff;
					Data1 += 1;
				}
				else {
					Data1 = (ulong)Data;
				}
				VM.BASE[ Addr ] = (byte)( Data1 % 256u );
				VM.BASE[ Addr + 1 ] = (byte)( Data1 / 256u % 256u );
				VM.BASE[ Addr + 2 ] = (byte)( Data1 / 256u / 256u % 256u );
			}
			
			public static void Set_sint24( uint Addr, long Data )
			{
				ulong Data1;
				if( Data < 0 ) {
					Data1 = (ulong)-Data;
					Data1 ^= 0xffffffff;
					Data1 += 1;
				}
				else {
					Data1 = (ulong)Data;
				}
				VM.BASE[ Addr ] = (byte)( Data1 % 256u );
				VM.BASE[ Addr + 1 ] = (byte)( Data1 / 256u % 256u );
				VM.BASE[ Addr + 2 ] = (byte)( Data1 / 256u / 256u % 256u );
			}
			
			public static void Set_uint32( uint Addr, long Data )
			{
				ulong Data1;
				if( Data < 0 ) {
					Data1 = (ulong)-Data;
					Data1 ^= 0xffffffff;
					Data1 += 1;
				}
				else {
					Data1 = (ulong)Data;
				}
				VM.BASE[ Addr ] = (byte)( Data1 % 256u );
				VM.BASE[ Addr + 1 ] = (byte)( Data1 / 256u % 256u );
				VM.BASE[ Addr + 2 ] = (byte)( Data1 / 256u / 256u % 256u );
				VM.BASE[ Addr + 3 ] = (byte)( Data1 / 256u / 256u / 256u % 256u );
			}
			
			public static void Set_sint32( uint Addr, long Data )
			{
				ulong Data1;
				if( Data < 0 ) {
					Data1 = (ulong)-Data;
					Data1 ^= 0xffffffff;
					Data1 += 1;
				}
				else {
					Data1 = (ulong)Data;
				}
				VM.BASE[ Addr ] = (byte)( Data1 % 256u );
				VM.BASE[ Addr + 1 ] = (byte)( Data1 / 256u % 256u );
				VM.BASE[ Addr + 2 ] = (byte)( Data1 / 256u / 256u % 256u );
				VM.BASE[ Addr + 3 ] = (byte)( Data1 / 256u / 256u / 256u % 256u );
			}
			
			//------------------------------------------
			
			public static long GetCode_uint8( uint Addr )
			{
				long data = VM.ByteCodeList[ Addr ];
				return data;
			}
			
			public static long GetCode_uint16( uint Addr )
			{
				long data = VM.ByteCodeList[ Addr ];
				data += VM.ByteCodeList[ Addr + 1 ] * 256u;
				return data;
			}
			
			public static long GetCode_uint24( uint Addr )
			{
				long data = VM.ByteCodeList[ Addr ];
				data += VM.ByteCodeList[ Addr + 1 ] * 256u;
				data += VM.ByteCodeList[ Addr + 2 ] * 256u * 256u;
				return data;
			}
			
			public static long GetCode_uint32( uint Addr )
			{
				long data = VM.ByteCodeList[ Addr ];
				data += VM.ByteCodeList[ Addr + 1 ] * 256u;
				data += VM.ByteCodeList[ Addr + 2 ] * 256u * 256u;
				data += VM.ByteCodeList[ Addr + 3 ] * 256u * 256u * 256u;
				return data;
			}
		}
	}
}


