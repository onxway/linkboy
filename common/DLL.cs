﻿
namespace n_Interface
{
using System;
using System.Drawing;
using System.IO.Ports;
using System.Runtime.InteropServices;
using System.Windows.Forms;
using Dongzr.MidiLite;
using System.Threading;
using n_VM;
using n_GZ;
using System.IO;
using n_CruxSim;

using uint8 = System.Byte;
using uint16 = System.UInt16;
using uint32 = System.UInt32;
using int32 = System.Int32;

public static class Interface
{
	const uint32 D_WIFI_AP =	300;
	const uint32 D_WIFI_STA =	301;
	const uint32 TCP_Server =	302;
	const uint32 TCP_Client =	303;
	
	const uint32 HMOS_usleep =	400;
	
	public const uint32 SYS_TICK =	0;
	public const uint32 SYS_LOOP =	4;
	
	
	public static void Deal( uint Type )
	{
		uint v24 = Type >> 8;
		uint v8 = Type & 0xFF;
		
		switch( v24 ) {
			case 0:				System_Deal( v8 ); break;
			case D_WIFI_AP:		WIFI_AP_Deal( v8 ); break;
			case D_WIFI_STA:	WIFI_STA_Deal( v8 ); break;
			case TCP_Server:	break;
			case TCP_Client:	break;
			case HMOS_usleep:	HMOS_Deal( v8 ); break;
			
			default:	n_Debug.Warning.BUG( "VM-Interface调用> 未知的Interface函数: " + Type ); break;
		}
	}
	
	public static void System_Deal( uint Type )
	{
		switch( Type ) {
			case 1:		AVM._ACI_SetInterrupt(); break;
			//case 2:		SG.WriteByte(); break;
			case 3:		AVM.Debug(); break;
			
			case 4:		AVM.ChannelWrite(); break;
			case 5:		AVM.ChannelRead(); break;
			case 6:		AVM.ChannelInit(); break;
			
			case 7:		AVM.ModuleWrite(); break;
			case 8:		AVM.ModuleRead(); break;
			case 9:		AVM.ResetSprite(); break;
			
			case 10:	break;
			case 11:	break; //短时间延时 不需要
			
			//case 12:	G.MyRefresh(); G.CGPanel.MyRefresh(); break;
			
			//电路仿真引擎
			case 13:	AVM.CCE_Reset(); break;
			case 14:	n_CCEngine.CCEngine.DLL_Run(); break;
			
			case 15:	AVM.LAPI(); break;
			case 16:	AVM.PortWrite(); break;
			case 17:	AVM.PortRead(); break;
			
			case 20:	AVM.crux_AddVar(); break;
			case 21:	AVM.crux_Run(); break;
			
			case 22:	break;
			case 23:	VUI_Interface(); break;
			
			default:	n_Debug.Warning.BUG( "VM-Interface调用> 未知的Interface函数: " + Type ); break;
		}
	}
	
	public static void WIFI_AP_Deal( uint op_id )
	{
		
	}
	
	public static void WIFI_STA_Deal( uint op_id )
	{
		
	}
	
	public static void HMOS_Deal( uint op_id )
	{
		switch( op_id ) {
			case 0:
				int V1 = (int)VM.Mem.Get_sint32( VM.DP + 0 );
				Thread.Sleep( V1 / 1000 );
				break;
			default: break;
		}
	}
	
	public static void VUI_Interface()
	{
		int V1 = (int)VM.Mem.Get_uint8( VM.DP + 0 );
		int V2 = (int)VM.Mem.Get_sint32( VM.DP + 1 );
		if( V1 == 0 ) {
			VM.A = n_SimulateObj.SimulateObj.LCD_MouseX;
		}
		if( V1 == 1 ) {
			VM.A = n_SimulateObj.SimulateObj.LCD_MouseY;
		}
		if( V1 == 2 ) {
			VM.A = n_SimulateObj.SimulateObj.LCD_MouseDown;
		}
	}
}
//*******************************************************
//AVM
public static class AVM
{
	public delegate void DE_ModuleWrite( int v1, int v2 );
	public static DE_ModuleWrite D_ModuleWrite;
	public delegate int DE_ModuleRead( int v1 );
	public static DE_ModuleRead D_ModuleRead;
	
	const int myTimerEnableIndex = 0x11;
	public static bool myTimerEnable;
	
	public static MmTimer SysTickTimer;
	
	static System.Windows.Forms.Timer VMTimer;
	
	//static object locker = new object();
	
	static bool MMTimerDrive = true;
	
	//初始化
	public static void Init()
	{
		SysTickTimer = new MmTimer();
		SysTickTimer.Mode = MmTimerMode.Periodic;
		
		SysTickTimer.Interval = n_GUIcoder.GUIcoder.SIM_Tick;
		SysTickTimer.Tick += new EventHandler( SysTimerTick );
		//SysTickTimer.Start();
		
		VMTimer = new System.Windows.Forms.Timer();
		VMTimer.Interval = 10;
		VMTimer.Tick += new EventHandler( SysTimerTick );
		VMTimer.Enabled = false;
		
		crux_Init();
		
		if( G.GuangZhouTest ) {
			GZ.Init();
		}
	}
	
	//结束程序
	public static void Close()
	{
		SysTickTimer.Stop();
		
		if( G.GuangZhouTest ) {
			GZ.Stop();
		}
	}
	
	//直接转移字节码载入程序并开始运行
	public static void Run( byte[] List, int Length )
	{
		VM.复位();
		
		//从文件中载入程序并开始运行
		//FileStream fs = File.Open( FilePath, FileMode.OpenOrCreate );
		//fs.Read( VM.ByteCodeList, 0, VM.ByteCodeList.Length );
		
		for( int i = 0; i < Length; ++i ) {
			VM.ByteCodeList[i] = List[i];
		}
		
		myTimerEnable = false;
		
		if( MMTimerDrive ) {
			SysTickTimer.Start();
		}
		else {
			VMTimer.Enabled = true;
		}
		
		if( G.GuangZhouTest ) {
			GZ.Start();
		}
		crux_Clear();
	}
	
	//停止虚拟机
	public static void Stop()
	{
		//VMTimer.Enabled = false;
		
		if( MMTimerDrive ) {
			SysTickTimer.Stop();
		}
		else {
			VMTimer.Enabled = false;
		}
		
		if( G.GuangZhouTest ) {
			GZ.Stop();
		}
	}
	
	static int FileReadTime = 0;
	
	
	//定时器事件触发
	static void SysTimerTick(object sender, EventArgs e)
	{
		if( n_GUIcoder.GUIcoder.isPause == 1 ) {
			
			p_VM.VM.Tick_ms += n_GUIcoder.GUIcoder.SIM_Tick;
			
			//这里每循环一遍统一触发绘图, 就不要在其他地方触发了
			G.MyRefresh();
			G.CGPanel.MySimRefresh();
			
			return;
		}
		if( n_GUIcoder.GUIcoder.isPause == 0 && myTimerEnable ) {
			//lock( locker ) {
				if( VM.EA ) {
					n_VM.VM.清空系统参数();
					n_VM.VM.添加中断事件( myTimerEnableIndex );
				}
			//}
			n_SimulateObj.SimulateObj.Tick();
		}
		if( n_GUIcoder.GUIcoder.isPause == 2 ) {
			n_GUIcoder.GUIcoder.isPause = 0;
		}
		try {
		
			int TTT = 0;
			int MAX = 100000;
			for( int i = 0; i < MAX; ++i ) {
				uint Line = VM.运行();
				
				if( n_GUIcoder.GUIcoder.isPause == 1 ) {
					break;
				}
				
				if( VM.Mem.Get_uint8( Interface.SYS_LOOP ) != 0 ) {
					VM.Mem.Set_uint8( Interface.SYS_LOOP, 0 );
					break;
				}
				TTT++;
			}
			//只有一开始初始化会执行这里
			if( TTT >= MAX - 10 ) {
				//n_Debug.Debug.Message = "程序运行异常, 请提交给我们解决";
			}
			//n_Debug.Debug.Message = n_GUIcoder.GUIcoder.cputype1 + " " + TTT + ",";
			
			p_VM.VM.Tick_ms += n_GUIcoder.GUIcoder.SIM_Tick;
			
			//这里每循环一遍统一触发绘图, 就不要在其他地方触发了
			G.MyRefresh();
			G.CGPanel.MySimRefresh();
		
		} catch( Exception ee ) {
			n_Debug.Warning.AddErrMessage( "仿真遇到异常: " + ee.ToString() );
			n_ControlCenter.ControlCenter.Simulate();
		}
		//文件自动读取 - 用于广州人工智能项目
		if( !G.GuangZhouTest ) {
			return;
		}
		FileReadTime++;
		if( FileReadTime == 100 ) {
			FileReadTime = 0;
			
			if( GZ.GZ_Type == GZ.GZ_Type_SH_Auto || GZ.GZ_Type == GZ.GZ_Type_SH_User ) {
				
				string filename = G.ccode.FilePath + "result.txt";
				if( GZ.GZ_Type == GZ.GZ_Type_SH_Auto ) {
					filename = n_OS.OS.SystemRoot + "tempGZ\\main.html";
				}
				if( File.Exists( filename ) ) {
					//RTime++;
					string txt = null;
					try {
						txt = n_OS.VIO.OpenTextFileUTF8( filename );
						//File.Delete( filename );
						
						/*
						string t = txt.Replace( "},", ";" );
						t = t.Trim( "[]".ToCharArray() );
						string[] cut = t.Split( ';' );
						string last = cut[cut.Length-2];
						last = last.Trim( "{}".ToCharArray() );
						cut = last.Split( ',' );
						n_SimulateObj.SimulateObj.GZ_Value0 = (int)(100 * float.Parse( cut[0].Split( ':' )[1] ));
						n_SimulateObj.SimulateObj.GZ_Value1 = (int)(100 * float.Parse( cut[1].Split( ':' )[1] ));
						n_SimulateObj.SimulateObj.GZ_Value2 = (int)(100 * float.Parse( cut[2].Split( ':' )[1] ));
						n_SimulateObj.SimulateObj.GZ_Value3 = (int)(100 * float.Parse( cut[3].Split( ':' )[1] ));
						*/
						
						string t = txt.Remove( 0, 9 );
						t = t.Remove( t.Length - 2 );
						string[] cut = t.Split( ',' );
						n_SimulateObj.SimulateObj.GZ_Value0 = (int)(100 * float.Parse( cut[0].Split( ':' )[1] ));
						n_SimulateObj.SimulateObj.GZ_Value2 = (int)(100 * float.Parse( cut[1].Split( ':' )[1] ));
						n_SimulateObj.SimulateObj.GZ_Value1 = (int)(100 * float.Parse( cut[2].Split( ':' )[1] ));
						n_SimulateObj.SimulateObj.GZ_Value3 = (int)(100 * float.Parse( cut[3].Split( ':' )[1] ));
						
						//n_RemoPad.RemoPad.SendCmdOk( 0, 0, 0, 0, 0, 0, 0, 0 );
					}
					catch( Exception ee ) {
						n_Debug.Debug.Message += "格式转换错误, 请确保输入了正确数据格式:\n" + txt + "\n" + ee;
					}
				}
				else {
					string test = "{\"data\":{\"rock\":0.0000022888661987963133,\"paper\":1.1020152568264052E-9,\"scissors\":0.000024704075258341618,\"blank\":0.9999730587005615}}";
					n_OS.VIO.SaveTextFileUTF8( filename, test );
					n_Debug.Debug.Message = "请将结果粘贴到当前文件目录下的 result.txt 文件中, 格式如下:\n" + test + "\n(按 Delete 键删除本消息)";
				}
			}
			if( GZ.GZ_Type == GZ.GZ_Type_SH_File ) {
				
				try {
					n_SimulateObj.SimulateObj.GZ_Value0 = 0;
					n_SimulateObj.SimulateObj.GZ_Value2 = 0;
					n_SimulateObj.SimulateObj.GZ_Value1 = 0;
					n_SimulateObj.SimulateObj.GZ_Value3 = 0;
					if( GZ.StrResult == "stone" ) {
						n_SimulateObj.SimulateObj.GZ_Value0 = 100;
					}
					else if( GZ.StrResult == "scissors" ) {
						n_SimulateObj.SimulateObj.GZ_Value1 = 100;
					}
					else if( GZ.StrResult == "cloth" ) {
						n_SimulateObj.SimulateObj.GZ_Value2 = 100;
					}
					else {
						n_SimulateObj.SimulateObj.GZ_Value3 = 100;
					}
				}
				catch {
					//...
				}
			}
			if( GZ.GZ_Type == GZ.GZ_Type_Color ) {
				
				try {
					n_SimulateObj.SimulateObj.GZ_ColorValue = GZ.StrResult;
				}
				catch {
					//...
				}
			}
			if( GZ.Message != null ) {
				n_Debug.Debug.Message = GZ.Message;
				GZ.Message = null;
			}
		}
		
		/*
		FileReadTime++;
		if( FileReadTime == 100 ) {
			FileReadTime = 0;
			if( G.GuangZhouTest ) {
				//SendKeys.Send 函数会导致死机
				System.Windows.Forms.SendKeys.SendWait( "^C" );
				System.Threading.Thread.Sleep( 10 );
				System.Windows.Forms.SendKeys.SendWait( "^A" );
				
				string Result = System.Windows.Forms.Clipboard.GetText();
				n_Debug.Debug.Message = Result;
			}
		}
		*/
	}
	
	//------------------------------------------------------------------------
	//接口函数
	
	public static void _ACI_SetInterrupt()
	{
		uint V0 = (uint)VM.Mem.Get_uint32( VM.DP + 0 );
		uint V1 = (uint)VM.Mem.Get_uint8( VM.DP + 4 );
		bool B = true;
		if( V1 == 0 ) {
			B = false;
		}
		switch( V0 ) {
//				case 0x01:		VMForm.myKeyDown = B; break;
//				case 0x02:		VMForm.myKeyPress = B; break;
//				case 0x03:		VMForm.myKeyUp = B; break;
//				case 0x04:		VMForm.myMouseDown = B; break;
//				case 0x05:		VMForm.myMouseEnter = B; break;
//				case 0x06:		VMForm.myMouseHover = B; break;
//				case 0x07:		VMForm.myMouseLeave = B; break;
//				case 0x08:		VMForm.myMouseMove = B; break;
//				case 0x09: 		VMForm.myMouseUp = B; break;
//				case 0x0A:		n_IO.IO.ReadData = B; break;
//				
//				case 0x10:		VMForm.mySerialPortReceived = B; break;
				case 0x11:		myTimerEnable = B; break;
				
				default: break;
		}
	}
	
	public static void Debug()
	{
		n_Debug.Debug.Message += ( "调试信息:" + VM.Mem.Get_sint32( VM.DP + 0 ).ToString() + "\n" );
		//n_SimPanel.SimPanel.DebugMes += "调试信息:" + VM.Mem.Get_sint32( VM.DP + 0 ).ToString() + " ";
		//n_V.V.VMBox.Text = VM.Mem.Get_sint32( VM.DP + 0 ) + " ";
	}
	
	
	public static void ChannelInit()
	{
		n_CPU.CPU.Init();
	}
	
	public static void ChannelWrite()
	{
		int V1 = (int)VM.Mem.Get_sint32( VM.DP + 0 );
		int V2 = (int)VM.Mem.Get_sint32( VM.DP + 4 );
		n_CPU.CPU.ChannelWrite( V1, V2 );
		
		//G.MyRefresh();
		//G.CGPanel.MyRefresh();
	}
	
	public static void  ChannelRead()
	{
		int V1 = (int)VM.Mem.Get_sint32( VM.DP + 0 );
		VM.A = n_CPU.CPU.ChannelRead( V1 );
	}
	
	
	public static void ModuleWrite()
	{
		int V1 = (int)VM.Mem.Get_sint32( VM.DP + 0 );
		int V2 = (int)VM.Mem.Get_sint32( VM.DP + 4 );
		
		if( D_ModuleWrite != null ) {
			D_ModuleWrite( V1, V2 );
		}
	}
	
	public static void  ModuleRead()
	{
		int V1 = (int)VM.Mem.Get_sint32( VM.DP + 0 );
		
		if( D_ModuleRead != null ) {
			VM.A = D_ModuleRead( V1 );
		}
	}
	
	public static void ResetSprite()
	{
		int V1 = (int)VM.Mem.Get_sint32( VM.DP + 0 );
		int V2 = (int)VM.Mem.Get_sint32( VM.DP + 4 );
		
		if( V2 != -1 ) {
			n_SOblect.SObject s = G.SimBox.SPanel.SList[V2];
			G.CGPanel.myModuleList.ModuleList[V1].TargetSObject = s;
			//n_SimPanel.SimPanel.DebugMes += "A" + V1 + "," + V2 + " ";
		}
		else {
			G.CGPanel.myModuleList.ModuleList[V1].TargetSObject = null;
			//n_SimPanel.SimPanel.DebugMes += "B" + V1 + "," + V2 + " ";
		}
	}
	
	public static void CCE_Reset()
	{
		uint32 V1 = (uint32)VM.Mem.Get_uint32( VM.DP + 0 );
		uint32 V2 = (uint32)VM.Mem.Get_uint32( VM.DP + 4 );
		uint16 V3= (uint16)VM.Mem.Get_uint16( VM.DP + 8 );
		uint32 V4 = (uint32)VM.Mem.Get_uint32( VM.DP + 10 );
		
		n_CCEngine.CCEngine.DLL_Reset( V1, V2, V3, V4 );
	}
	
	public static void LAPI()
	{
		n_LAPI.LAPI.ReceiveData();
	}
	
	public static void PortWrite()
	{
		int V1 = (int)VM.Mem.Get_sint32( VM.DP + 0 );
		if( n_SimulateObj.SimulateObj.delePortValue != null) {
			n_SimulateObj.SimulateObj.delePortValue( V1 );
		}
	}
	
	public static void PortRead()
	{
		if( n_SimulateObj.SimulateObj.delePortRead != null) {
			VM.A = n_SimulateObj.SimulateObj.delePortRead();
		}
	}
	
	//====================================================
	
	const int C_GetKey = 0;
	const int C_Clear = 10;
	const int C_DrawCircle = 11;
	const int C_FillCircle = 12;
	
	static int[] crux_ValList;
	static int crux_ValNum;
	
	public static void crux_Init()
	{
		crux_ValList = new int[20];
		crux_Clear();
	}
	
	public static void crux_Clear()
	{
		crux_ValNum = 0;
	}
	
	public static void crux_AddVar()
	{
		int V1 = (int)VM.Mem.Get_sint32( VM.DP + 0 );
		
		if( crux_ValNum < crux_ValList.Length ) {
			crux_ValList[crux_ValNum] = V1;
			crux_ValNum++;
		}
		
		VM.A = 0;
	}
	
	public static void crux_Run()
	{
		VM.A = 0;
		crux_ValNum = 0;
		
		int cmd = (int)VM.Mem.Get_sint32( VM.DP + 0 );
		
		
		if( cmd == C_GetKey ) {
			int k = CruxSim.ReadKey();
			VM.A = k;
		}
		else if( cmd == C_Clear ) {
			CruxSim.gndk.Clear( Color.White );
		}
		else if( cmd == C_DrawCircle ) {
			int x = crux_ValList[0];
			int y = crux_ValList[1];
			int r = crux_ValList[2];
			CruxSim.gndk.DrawEllipse( Pens.Black, x, y, r, r );
		}
		else if( cmd == C_FillCircle ) {
			int x = crux_ValList[0];
			int y = crux_ValList[1];
			int r = crux_ValList[2];
			CruxSim.gndk.FillEllipse( Brushes.Black, x, y, r, r );
		}
		else {
			//...
		}
	}
}
}




