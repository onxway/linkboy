﻿/*
 * 由 SharpDevelop 创建。
 * 用户: bbb_2
 * 日期: 2009-11-17
 * 时间: 11:15
 * 
 * 如果你想要更改该模板，那么请使用“工具 | 选项 | 编辑 | 编辑标准标题”。
 */
namespace n_SetIDEForm
{
	partial class SetIDEForm
	{
		/// <summary>
		/// Designer variable used to keep track of non-visual components.
		/// </summary>
		private System.ComponentModel.IContainer components = null;
		
		/// <summary>
		/// Disposes resources used by the form.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing) {
				if (components != null) {
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}
		
		/// <summary>
		/// This method is required for Windows Forms designer support.
		/// Do not change the method contents inside the source code editor. The Forms designer might
		/// not be able to load this method if it was changed manually.
		/// </summary>
		private void InitializeComponent()
		{
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(SetIDEForm));
			this.button2 = new System.Windows.Forms.Button();
			this.checkBox1 = new System.Windows.Forms.CheckBox();
			this.b0 = new System.Windows.Forms.Button();
			this.label1 = new System.Windows.Forms.Label();
			this.checkBox2 = new System.Windows.Forms.CheckBox();
			this.groupBox3 = new System.Windows.Forms.GroupBox();
			this.子项组 = new System.Windows.Forms.GroupBox();
			this.checkBox3 = new System.Windows.Forms.CheckBox();
			this.checkBox4 = new System.Windows.Forms.CheckBox();
			this.checkBoxWin8Touch = new System.Windows.Forms.CheckBox();
			this.label5 = new System.Windows.Forms.Label();
			this.b4 = new System.Windows.Forms.Button();
			this.groupBox2 = new System.Windows.Forms.GroupBox();
			this.b1 = new System.Windows.Forms.Button();
			this.radioButtonLight = new System.Windows.Forms.RadioButton();
			this.radioButtonDeep = new System.Windows.Forms.RadioButton();
			this.label9 = new System.Windows.Forms.Label();
			this.checkBox生成HEX机器码文件 = new System.Windows.Forms.CheckBox();
			this.groupBox1 = new System.Windows.Forms.GroupBox();
			this.comboBoxBoardType = new System.Windows.Forms.ComboBox();
			this.label4 = new System.Windows.Forms.Label();
			this.comboBoxOptimize = new System.Windows.Forms.ComboBox();
			this.label2 = new System.Windows.Forms.Label();
			this.groupBoxMes = new System.Windows.Forms.GroupBox();
			this.label8 = new System.Windows.Forms.Label();
			this.textBoxWeb = new System.Windows.Forms.TextBox();
			this.label3 = new System.Windows.Forms.Label();
			this.textBoxUserMessage = new System.Windows.Forms.TextBox();
			this.checkBoxShowOld = new System.Windows.Forms.CheckBox();
			this.checkBoxSwitchOldFile = new System.Windows.Forms.CheckBox();
			this.label7 = new System.Windows.Forms.Label();
			this.comboBoxLanguage = new System.Windows.Forms.ComboBox();
			this.groupBox7 = new System.Windows.Forms.GroupBox();
			this.checkBoxCreatTempSimFile = new System.Windows.Forms.CheckBox();
			this.checkBoxShowNew = new System.Windows.Forms.CheckBox();
			this.label6 = new System.Windows.Forms.Label();
			this.checkBoxMoHu = new System.Windows.Forms.CheckBox();
			this.checkBoxMoClick = new System.Windows.Forms.CheckBox();
			this.label11 = new System.Windows.Forms.Label();
			this.groupBox4 = new System.Windows.Forms.GroupBox();
			this.radioButtonStandardMode = new System.Windows.Forms.RadioButton();
			this.radioButtonSimpleMode = new System.Windows.Forms.RadioButton();
			this.label12 = new System.Windows.Forms.Label();
			this.label13 = new System.Windows.Forms.Label();
			this.groupBoxExt = new System.Windows.Forms.GroupBox();
			this.label15 = new System.Windows.Forms.Label();
			this.textBoxExtMes = new System.Windows.Forms.TextBox();
			this.button边框颜色 = new System.Windows.Forms.Button();
			this.label16 = new System.Windows.Forms.Label();
			this.checkBoxGNDBlue = new System.Windows.Forms.CheckBox();
			this.buttonExportCode = new System.Windows.Forms.Button();
			this.comboBoxUILanguage = new System.Windows.Forms.ComboBox();
			this.label17 = new System.Windows.Forms.Label();
			this.buttonISP = new System.Windows.Forms.Button();
			this.buttonShowMes = new System.Windows.Forms.Button();
			this.buttonSuper = new System.Windows.Forms.Button();
			this.groupBox5 = new System.Windows.Forms.GroupBox();
			this.button_OtherEditor = new System.Windows.Forms.Button();
			this.buttonRefreshExem = new System.Windows.Forms.Button();
			this.buttonLibPath = new System.Windows.Forms.Button();
			this.buttonLibMng = new System.Windows.Forms.Button();
			this.buttonCmpMes = new System.Windows.Forms.Button();
			this.buttonShowAllVar = new System.Windows.Forms.Button();
			this.buttonDebugMode = new System.Windows.Forms.Button();
			this.buttonLCC = new System.Windows.Forms.Button();
			this.groupBox3.SuspendLayout();
			this.子项组.SuspendLayout();
			this.groupBox2.SuspendLayout();
			this.groupBox1.SuspendLayout();
			this.groupBoxMes.SuspendLayout();
			this.groupBox7.SuspendLayout();
			this.groupBox4.SuspendLayout();
			this.groupBoxExt.SuspendLayout();
			this.groupBox5.SuspendLayout();
			this.SuspendLayout();
			// 
			// button2
			// 
			this.button2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(128)))));
			resources.ApplyResources(this.button2, "button2");
			this.button2.ForeColor = System.Drawing.Color.Black;
			this.button2.Name = "button2";
			this.button2.UseVisualStyleBackColor = false;
			this.button2.Click += new System.EventHandler(this.Button2Click);
			this.button2.MouseDown += new System.Windows.Forms.MouseEventHandler(this.Button2MouseDown);
			// 
			// checkBox1
			// 
			resources.ApplyResources(this.checkBox1, "checkBox1");
			this.checkBox1.Name = "checkBox1";
			this.checkBox1.UseVisualStyleBackColor = true;
			// 
			// b0
			// 
			resources.ApplyResources(this.b0, "b0");
			this.b0.Name = "b0";
			this.b0.UseVisualStyleBackColor = true;
			this.b0.Click += new System.EventHandler(this.ColorButtonClick);
			// 
			// label1
			// 
			resources.ApplyResources(this.label1, "label1");
			this.label1.Name = "label1";
			// 
			// checkBox2
			// 
			this.checkBox2.Checked = true;
			this.checkBox2.CheckState = System.Windows.Forms.CheckState.Checked;
			resources.ApplyResources(this.checkBox2, "checkBox2");
			this.checkBox2.Name = "checkBox2";
			this.checkBox2.UseVisualStyleBackColor = true;
			this.checkBox2.CheckedChanged += new System.EventHandler(this.CheckBox2CheckedChanged);
			// 
			// groupBox3
			// 
			this.groupBox3.Controls.Add(this.子项组);
			this.groupBox3.Controls.Add(this.checkBox2);
			resources.ApplyResources(this.groupBox3, "groupBox3");
			this.groupBox3.Name = "groupBox3";
			this.groupBox3.TabStop = false;
			// 
			// 子项组
			// 
			this.子项组.Controls.Add(this.checkBox3);
			this.子项组.Controls.Add(this.checkBox4);
			resources.ApplyResources(this.子项组, "子项组");
			this.子项组.Name = "子项组";
			this.子项组.TabStop = false;
			// 
			// checkBox3
			// 
			this.checkBox3.Checked = true;
			this.checkBox3.CheckState = System.Windows.Forms.CheckState.Checked;
			resources.ApplyResources(this.checkBox3, "checkBox3");
			this.checkBox3.Name = "checkBox3";
			this.checkBox3.UseVisualStyleBackColor = true;
			// 
			// checkBox4
			// 
			this.checkBox4.Checked = true;
			this.checkBox4.CheckState = System.Windows.Forms.CheckState.Checked;
			resources.ApplyResources(this.checkBox4, "checkBox4");
			this.checkBox4.Name = "checkBox4";
			this.checkBox4.UseVisualStyleBackColor = true;
			// 
			// checkBoxWin8Touch
			// 
			resources.ApplyResources(this.checkBoxWin8Touch, "checkBoxWin8Touch");
			this.checkBoxWin8Touch.Name = "checkBoxWin8Touch";
			this.checkBoxWin8Touch.UseVisualStyleBackColor = true;
			// 
			// label5
			// 
			resources.ApplyResources(this.label5, "label5");
			this.label5.Name = "label5";
			// 
			// b4
			// 
			resources.ApplyResources(this.b4, "b4");
			this.b4.Name = "b4";
			this.b4.UseVisualStyleBackColor = true;
			this.b4.Click += new System.EventHandler(this.ColorButtonClick);
			// 
			// groupBox2
			// 
			this.groupBox2.Controls.Add(this.b1);
			this.groupBox2.Controls.Add(this.b0);
			this.groupBox2.Controls.Add(this.radioButtonLight);
			this.groupBox2.Controls.Add(this.radioButtonDeep);
			this.groupBox2.Controls.Add(this.label9);
			this.groupBox2.Controls.Add(this.label1);
			resources.ApplyResources(this.groupBox2, "groupBox2");
			this.groupBox2.Name = "groupBox2";
			this.groupBox2.TabStop = false;
			// 
			// b1
			// 
			resources.ApplyResources(this.b1, "b1");
			this.b1.Name = "b1";
			this.b1.UseVisualStyleBackColor = true;
			this.b1.Click += new System.EventHandler(this.ColorButtonClick);
			// 
			// radioButtonLight
			// 
			resources.ApplyResources(this.radioButtonLight, "radioButtonLight");
			this.radioButtonLight.Name = "radioButtonLight";
			this.radioButtonLight.TabStop = true;
			this.radioButtonLight.UseVisualStyleBackColor = true;
			// 
			// radioButtonDeep
			// 
			resources.ApplyResources(this.radioButtonDeep, "radioButtonDeep");
			this.radioButtonDeep.Name = "radioButtonDeep";
			this.radioButtonDeep.TabStop = true;
			this.radioButtonDeep.UseVisualStyleBackColor = true;
			// 
			// label9
			// 
			resources.ApplyResources(this.label9, "label9");
			this.label9.Name = "label9";
			// 
			// checkBox生成HEX机器码文件
			// 
			resources.ApplyResources(this.checkBox生成HEX机器码文件, "checkBox生成HEX机器码文件");
			this.checkBox生成HEX机器码文件.Name = "checkBox生成HEX机器码文件";
			this.checkBox生成HEX机器码文件.UseVisualStyleBackColor = true;
			// 
			// groupBox1
			// 
			this.groupBox1.Controls.Add(this.comboBoxBoardType);
			this.groupBox1.Controls.Add(this.label4);
			this.groupBox1.Controls.Add(this.comboBoxOptimize);
			this.groupBox1.Controls.Add(this.label2);
			this.groupBox1.Controls.Add(this.checkBox生成HEX机器码文件);
			resources.ApplyResources(this.groupBox1, "groupBox1");
			this.groupBox1.Name = "groupBox1";
			this.groupBox1.TabStop = false;
			// 
			// comboBoxBoardType
			// 
			this.comboBoxBoardType.FormattingEnabled = true;
			this.comboBoxBoardType.Items.AddRange(new object[] {
									resources.GetString("comboBoxBoardType.Items"),
									resources.GetString("comboBoxBoardType.Items1"),
									resources.GetString("comboBoxBoardType.Items2"),
									resources.GetString("comboBoxBoardType.Items3"),
									resources.GetString("comboBoxBoardType.Items4"),
									resources.GetString("comboBoxBoardType.Items5")});
			resources.ApplyResources(this.comboBoxBoardType, "comboBoxBoardType");
			this.comboBoxBoardType.Name = "comboBoxBoardType";
			// 
			// label4
			// 
			resources.ApplyResources(this.label4, "label4");
			this.label4.Name = "label4";
			// 
			// comboBoxOptimize
			// 
			this.comboBoxOptimize.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
			this.comboBoxOptimize.FormattingEnabled = true;
			this.comboBoxOptimize.Items.AddRange(new object[] {
									resources.GetString("comboBoxOptimize.Items"),
									resources.GetString("comboBoxOptimize.Items1")});
			resources.ApplyResources(this.comboBoxOptimize, "comboBoxOptimize");
			this.comboBoxOptimize.Name = "comboBoxOptimize";
			// 
			// label2
			// 
			resources.ApplyResources(this.label2, "label2");
			this.label2.Name = "label2";
			// 
			// groupBoxMes
			// 
			this.groupBoxMes.Controls.Add(this.label8);
			this.groupBoxMes.Controls.Add(this.textBoxWeb);
			this.groupBoxMes.Controls.Add(this.label3);
			this.groupBoxMes.Controls.Add(this.textBoxUserMessage);
			resources.ApplyResources(this.groupBoxMes, "groupBoxMes");
			this.groupBoxMes.Name = "groupBoxMes";
			this.groupBoxMes.TabStop = false;
			// 
			// label8
			// 
			resources.ApplyResources(this.label8, "label8");
			this.label8.Name = "label8";
			// 
			// textBoxWeb
			// 
			this.textBoxWeb.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(225)))), ((int)(((byte)(236)))), ((int)(((byte)(225)))));
			resources.ApplyResources(this.textBoxWeb, "textBoxWeb");
			this.textBoxWeb.Name = "textBoxWeb";
			// 
			// label3
			// 
			resources.ApplyResources(this.label3, "label3");
			this.label3.Name = "label3";
			// 
			// textBoxUserMessage
			// 
			this.textBoxUserMessage.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(225)))), ((int)(((byte)(236)))), ((int)(((byte)(225)))));
			resources.ApplyResources(this.textBoxUserMessage, "textBoxUserMessage");
			this.textBoxUserMessage.Name = "textBoxUserMessage";
			// 
			// checkBoxShowOld
			// 
			resources.ApplyResources(this.checkBoxShowOld, "checkBoxShowOld");
			this.checkBoxShowOld.Name = "checkBoxShowOld";
			this.checkBoxShowOld.UseVisualStyleBackColor = true;
			// 
			// checkBoxSwitchOldFile
			// 
			resources.ApplyResources(this.checkBoxSwitchOldFile, "checkBoxSwitchOldFile");
			this.checkBoxSwitchOldFile.Name = "checkBoxSwitchOldFile";
			this.checkBoxSwitchOldFile.UseVisualStyleBackColor = true;
			// 
			// label7
			// 
			resources.ApplyResources(this.label7, "label7");
			this.label7.Name = "label7";
			// 
			// comboBoxLanguage
			// 
			this.comboBoxLanguage.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
			this.comboBoxLanguage.FormattingEnabled = true;
			this.comboBoxLanguage.Items.AddRange(new object[] {
									resources.GetString("comboBoxLanguage.Items"),
									resources.GetString("comboBoxLanguage.Items1")});
			resources.ApplyResources(this.comboBoxLanguage, "comboBoxLanguage");
			this.comboBoxLanguage.Name = "comboBoxLanguage";
			// 
			// groupBox7
			// 
			this.groupBox7.Controls.Add(this.checkBoxCreatTempSimFile);
			resources.ApplyResources(this.groupBox7, "groupBox7");
			this.groupBox7.Name = "groupBox7";
			this.groupBox7.TabStop = false;
			// 
			// checkBoxCreatTempSimFile
			// 
			resources.ApplyResources(this.checkBoxCreatTempSimFile, "checkBoxCreatTempSimFile");
			this.checkBoxCreatTempSimFile.Name = "checkBoxCreatTempSimFile";
			this.checkBoxCreatTempSimFile.UseVisualStyleBackColor = true;
			// 
			// checkBoxShowNew
			// 
			this.checkBoxShowNew.Checked = true;
			this.checkBoxShowNew.CheckState = System.Windows.Forms.CheckState.Checked;
			resources.ApplyResources(this.checkBoxShowNew, "checkBoxShowNew");
			this.checkBoxShowNew.Name = "checkBoxShowNew";
			this.checkBoxShowNew.UseVisualStyleBackColor = true;
			// 
			// label6
			// 
			this.label6.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(224)))), ((int)(((byte)(192)))));
			this.label6.ForeColor = System.Drawing.Color.Black;
			resources.ApplyResources(this.label6, "label6");
			this.label6.Name = "label6";
			// 
			// checkBoxMoHu
			// 
			this.checkBoxMoHu.Checked = true;
			this.checkBoxMoHu.CheckState = System.Windows.Forms.CheckState.Checked;
			resources.ApplyResources(this.checkBoxMoHu, "checkBoxMoHu");
			this.checkBoxMoHu.Name = "checkBoxMoHu";
			this.checkBoxMoHu.UseVisualStyleBackColor = true;
			// 
			// checkBoxMoClick
			// 
			this.checkBoxMoClick.Checked = true;
			this.checkBoxMoClick.CheckState = System.Windows.Forms.CheckState.Checked;
			resources.ApplyResources(this.checkBoxMoClick, "checkBoxMoClick");
			this.checkBoxMoClick.Name = "checkBoxMoClick";
			this.checkBoxMoClick.UseVisualStyleBackColor = true;
			// 
			// label11
			// 
			this.label11.BackColor = System.Drawing.Color.Transparent;
			this.label11.ForeColor = System.Drawing.Color.RoyalBlue;
			resources.ApplyResources(this.label11, "label11");
			this.label11.Name = "label11";
			// 
			// groupBox4
			// 
			this.groupBox4.Controls.Add(this.radioButtonStandardMode);
			this.groupBox4.Controls.Add(this.radioButtonSimpleMode);
			this.groupBox4.Controls.Add(this.label12);
			this.groupBox4.Controls.Add(this.label13);
			resources.ApplyResources(this.groupBox4, "groupBox4");
			this.groupBox4.Name = "groupBox4";
			this.groupBox4.TabStop = false;
			// 
			// radioButtonStandardMode
			// 
			resources.ApplyResources(this.radioButtonStandardMode, "radioButtonStandardMode");
			this.radioButtonStandardMode.Name = "radioButtonStandardMode";
			this.radioButtonStandardMode.TabStop = true;
			this.radioButtonStandardMode.UseVisualStyleBackColor = true;
			// 
			// radioButtonSimpleMode
			// 
			resources.ApplyResources(this.radioButtonSimpleMode, "radioButtonSimpleMode");
			this.radioButtonSimpleMode.Name = "radioButtonSimpleMode";
			this.radioButtonSimpleMode.TabStop = true;
			this.radioButtonSimpleMode.UseVisualStyleBackColor = true;
			// 
			// label12
			// 
			resources.ApplyResources(this.label12, "label12");
			this.label12.ForeColor = System.Drawing.Color.RoyalBlue;
			this.label12.Name = "label12";
			// 
			// label13
			// 
			resources.ApplyResources(this.label13, "label13");
			this.label13.ForeColor = System.Drawing.Color.RoyalBlue;
			this.label13.Name = "label13";
			// 
			// groupBoxExt
			// 
			this.groupBoxExt.Controls.Add(this.label15);
			this.groupBoxExt.Controls.Add(this.textBoxExtMes);
			resources.ApplyResources(this.groupBoxExt, "groupBoxExt");
			this.groupBoxExt.Name = "groupBoxExt";
			this.groupBoxExt.TabStop = false;
			// 
			// label15
			// 
			resources.ApplyResources(this.label15, "label15");
			this.label15.Name = "label15";
			// 
			// textBoxExtMes
			// 
			this.textBoxExtMes.BackColor = System.Drawing.Color.White;
			resources.ApplyResources(this.textBoxExtMes, "textBoxExtMes");
			this.textBoxExtMes.Name = "textBoxExtMes";
			// 
			// button边框颜色
			// 
			resources.ApplyResources(this.button边框颜色, "button边框颜色");
			this.button边框颜色.Name = "button边框颜色";
			this.button边框颜色.UseVisualStyleBackColor = true;
			this.button边框颜色.Click += new System.EventHandler(this.ColorButtonClick);
			// 
			// label16
			// 
			resources.ApplyResources(this.label16, "label16");
			this.label16.Name = "label16";
			// 
			// checkBoxGNDBlue
			// 
			resources.ApplyResources(this.checkBoxGNDBlue, "checkBoxGNDBlue");
			this.checkBoxGNDBlue.Name = "checkBoxGNDBlue";
			this.checkBoxGNDBlue.UseVisualStyleBackColor = true;
			// 
			// buttonExportCode
			// 
			this.buttonExportCode.ForeColor = System.Drawing.Color.DarkGray;
			resources.ApplyResources(this.buttonExportCode, "buttonExportCode");
			this.buttonExportCode.Name = "buttonExportCode";
			this.buttonExportCode.UseVisualStyleBackColor = true;
			this.buttonExportCode.Click += new System.EventHandler(this.ButtonExportCodeClick);
			// 
			// comboBoxUILanguage
			// 
			this.comboBoxUILanguage.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
			this.comboBoxUILanguage.FormattingEnabled = true;
			this.comboBoxUILanguage.Items.AddRange(new object[] {
									resources.GetString("comboBoxUILanguage.Items"),
									resources.GetString("comboBoxUILanguage.Items1")});
			resources.ApplyResources(this.comboBoxUILanguage, "comboBoxUILanguage");
			this.comboBoxUILanguage.Name = "comboBoxUILanguage";
			// 
			// label17
			// 
			resources.ApplyResources(this.label17, "label17");
			this.label17.Name = "label17";
			// 
			// buttonISP
			// 
			this.buttonISP.ForeColor = System.Drawing.Color.DarkGray;
			resources.ApplyResources(this.buttonISP, "buttonISP");
			this.buttonISP.Name = "buttonISP";
			this.buttonISP.UseVisualStyleBackColor = true;
			this.buttonISP.Click += new System.EventHandler(this.ButtonISPClick);
			// 
			// buttonShowMes
			// 
			this.buttonShowMes.ForeColor = System.Drawing.Color.DarkGray;
			resources.ApplyResources(this.buttonShowMes, "buttonShowMes");
			this.buttonShowMes.Name = "buttonShowMes";
			this.buttonShowMes.UseVisualStyleBackColor = true;
			this.buttonShowMes.Click += new System.EventHandler(this.ButtonShowMesClick);
			// 
			// buttonSuper
			// 
			this.buttonSuper.ForeColor = System.Drawing.Color.DarkGray;
			resources.ApplyResources(this.buttonSuper, "buttonSuper");
			this.buttonSuper.Name = "buttonSuper";
			this.buttonSuper.UseVisualStyleBackColor = true;
			this.buttonSuper.Click += new System.EventHandler(this.ButtonSuperClick);
			// 
			// groupBox5
			// 
			this.groupBox5.Controls.Add(this.buttonLCC);
			this.groupBox5.Controls.Add(this.button_OtherEditor);
			this.groupBox5.Controls.Add(this.buttonRefreshExem);
			this.groupBox5.Controls.Add(this.buttonLibPath);
			this.groupBox5.Controls.Add(this.buttonLibMng);
			this.groupBox5.Controls.Add(this.buttonCmpMes);
			this.groupBox5.Controls.Add(this.buttonShowAllVar);
			this.groupBox5.Controls.Add(this.buttonDebugMode);
			this.groupBox5.Controls.Add(this.buttonExportCode);
			this.groupBox5.Controls.Add(this.buttonSuper);
			this.groupBox5.Controls.Add(this.buttonISP);
			this.groupBox5.Controls.Add(this.buttonShowMes);
			resources.ApplyResources(this.groupBox5, "groupBox5");
			this.groupBox5.Name = "groupBox5";
			this.groupBox5.TabStop = false;
			// 
			// button_OtherEditor
			// 
			this.button_OtherEditor.ForeColor = System.Drawing.Color.DarkGray;
			resources.ApplyResources(this.button_OtherEditor, "button_OtherEditor");
			this.button_OtherEditor.Name = "button_OtherEditor";
			this.button_OtherEditor.UseVisualStyleBackColor = true;
			this.button_OtherEditor.Click += new System.EventHandler(this.Button_OtherEditorClick);
			// 
			// buttonRefreshExem
			// 
			this.buttonRefreshExem.ForeColor = System.Drawing.Color.DarkGray;
			resources.ApplyResources(this.buttonRefreshExem, "buttonRefreshExem");
			this.buttonRefreshExem.Name = "buttonRefreshExem";
			this.buttonRefreshExem.UseVisualStyleBackColor = true;
			this.buttonRefreshExem.Click += new System.EventHandler(this.ButtonRefreshExemClick);
			// 
			// buttonLibPath
			// 
			this.buttonLibPath.ForeColor = System.Drawing.Color.DarkGray;
			resources.ApplyResources(this.buttonLibPath, "buttonLibPath");
			this.buttonLibPath.Name = "buttonLibPath";
			this.buttonLibPath.UseVisualStyleBackColor = true;
			this.buttonLibPath.Click += new System.EventHandler(this.ButtonLibPathClick);
			// 
			// buttonLibMng
			// 
			this.buttonLibMng.ForeColor = System.Drawing.Color.DarkGray;
			resources.ApplyResources(this.buttonLibMng, "buttonLibMng");
			this.buttonLibMng.Name = "buttonLibMng";
			this.buttonLibMng.UseVisualStyleBackColor = true;
			this.buttonLibMng.Click += new System.EventHandler(this.ButtonLibMngClick);
			// 
			// buttonCmpMes
			// 
			this.buttonCmpMes.ForeColor = System.Drawing.Color.DarkGray;
			resources.ApplyResources(this.buttonCmpMes, "buttonCmpMes");
			this.buttonCmpMes.Name = "buttonCmpMes";
			this.buttonCmpMes.UseVisualStyleBackColor = true;
			this.buttonCmpMes.Click += new System.EventHandler(this.ButtonCmpMesClick);
			// 
			// buttonShowAllVar
			// 
			this.buttonShowAllVar.ForeColor = System.Drawing.Color.DarkGray;
			resources.ApplyResources(this.buttonShowAllVar, "buttonShowAllVar");
			this.buttonShowAllVar.Name = "buttonShowAllVar";
			this.buttonShowAllVar.UseVisualStyleBackColor = true;
			this.buttonShowAllVar.Click += new System.EventHandler(this.ButtonShowAllVarClick);
			// 
			// buttonDebugMode
			// 
			this.buttonDebugMode.ForeColor = System.Drawing.Color.DarkGray;
			resources.ApplyResources(this.buttonDebugMode, "buttonDebugMode");
			this.buttonDebugMode.Name = "buttonDebugMode";
			this.buttonDebugMode.UseVisualStyleBackColor = true;
			this.buttonDebugMode.Click += new System.EventHandler(this.ButtonDebugModeClick);
			// 
			// buttonLCC
			// 
			this.buttonLCC.ForeColor = System.Drawing.Color.DarkGray;
			resources.ApplyResources(this.buttonLCC, "buttonLCC");
			this.buttonLCC.Name = "buttonLCC";
			this.buttonLCC.UseVisualStyleBackColor = true;
			this.buttonLCC.Click += new System.EventHandler(this.ButtonLCCClick);
			// 
			// SetIDEForm
			// 
			resources.ApplyResources(this, "$this");
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.BackColor = System.Drawing.Color.WhiteSmoke;
			this.Controls.Add(this.groupBox5);
			this.Controls.Add(this.comboBoxUILanguage);
			this.Controls.Add(this.label17);
			this.Controls.Add(this.comboBoxLanguage);
			this.Controls.Add(this.checkBoxGNDBlue);
			this.Controls.Add(this.button边框颜色);
			this.Controls.Add(this.label16);
			this.Controls.Add(this.groupBoxExt);
			this.Controls.Add(this.groupBox4);
			this.Controls.Add(this.label11);
			this.Controls.Add(this.checkBoxMoClick);
			this.Controls.Add(this.checkBoxMoHu);
			this.Controls.Add(this.b4);
			this.Controls.Add(this.label6);
			this.Controls.Add(this.checkBox1);
			this.Controls.Add(this.checkBoxWin8Touch);
			this.Controls.Add(this.checkBoxShowNew);
			this.Controls.Add(this.checkBoxShowOld);
			this.Controls.Add(this.label5);
			this.Controls.Add(this.groupBox7);
			this.Controls.Add(this.groupBoxMes);
			this.Controls.Add(this.checkBoxSwitchOldFile);
			this.Controls.Add(this.label7);
			this.Controls.Add(this.groupBox1);
			this.Controls.Add(this.groupBox2);
			this.Controls.Add(this.groupBox3);
			this.Controls.Add(this.button2);
			this.MaximizeBox = false;
			this.MinimizeBox = false;
			this.Name = "SetIDEForm";
			this.TransparencyKey = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
			this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.SetIDEFormFormClosing);
			this.groupBox3.ResumeLayout(false);
			this.子项组.ResumeLayout(false);
			this.groupBox2.ResumeLayout(false);
			this.groupBox1.ResumeLayout(false);
			this.groupBoxMes.ResumeLayout(false);
			this.groupBoxMes.PerformLayout();
			this.groupBox7.ResumeLayout(false);
			this.groupBox4.ResumeLayout(false);
			this.groupBoxExt.ResumeLayout(false);
			this.groupBoxExt.PerformLayout();
			this.groupBox5.ResumeLayout(false);
			this.ResumeLayout(false);
		}
		private System.Windows.Forms.Button buttonLCC;
		private System.Windows.Forms.Button button_OtherEditor;
		private System.Windows.Forms.Button buttonRefreshExem;
		private System.Windows.Forms.Button buttonLibPath;
		private System.Windows.Forms.Button buttonLibMng;
		private System.Windows.Forms.Button buttonCmpMes;
		private System.Windows.Forms.Button buttonShowAllVar;
		private System.Windows.Forms.Button buttonDebugMode;
		private System.Windows.Forms.GroupBox groupBox5;
		private System.Windows.Forms.Button buttonSuper;
		private System.Windows.Forms.Button buttonShowMes;
		private System.Windows.Forms.Button buttonISP;
		private System.Windows.Forms.Label label17;
		private System.Windows.Forms.ComboBox comboBoxUILanguage;
		private System.Windows.Forms.Button buttonExportCode;
		private System.Windows.Forms.CheckBox checkBoxGNDBlue;
		private System.Windows.Forms.Label label16;
		private System.Windows.Forms.Button button边框颜色;
		private System.Windows.Forms.TextBox textBoxExtMes;
		private System.Windows.Forms.Label label15;
		private System.Windows.Forms.GroupBox groupBoxExt;
		private System.Windows.Forms.RadioButton radioButtonSimpleMode;
		private System.Windows.Forms.RadioButton radioButtonStandardMode;
		private System.Windows.Forms.Label label13;
		private System.Windows.Forms.Label label12;
		private System.Windows.Forms.GroupBox groupBox4;
		private System.Windows.Forms.Label label11;
		private System.Windows.Forms.CheckBox checkBoxMoClick;
		private System.Windows.Forms.CheckBox checkBoxMoHu;
		private System.Windows.Forms.RadioButton radioButtonDeep;
		private System.Windows.Forms.RadioButton radioButtonLight;
		private System.Windows.Forms.Button b1;
		private System.Windows.Forms.Label label9;
		private System.Windows.Forms.Label label8;
		private System.Windows.Forms.CheckBox checkBoxShowNew;
		private System.Windows.Forms.CheckBox checkBoxCreatTempSimFile;
		private System.Windows.Forms.GroupBox groupBox7;
		private System.Windows.Forms.ComboBox comboBoxBoardType;
		private System.Windows.Forms.Label label4;
		private System.Windows.Forms.ComboBox comboBoxLanguage;
		private System.Windows.Forms.Label label7;
		private System.Windows.Forms.TextBox textBoxUserMessage;
		private System.Windows.Forms.Label label3;
		private System.Windows.Forms.GroupBox groupBoxMes;
		private System.Windows.Forms.CheckBox checkBoxWin8Touch;
		private System.Windows.Forms.ComboBox comboBoxOptimize;
		private System.Windows.Forms.Label label2;
		private System.Windows.Forms.CheckBox checkBox生成HEX机器码文件;
		private System.Windows.Forms.GroupBox groupBox1;
		private System.Windows.Forms.Button b4;
		private System.Windows.Forms.GroupBox groupBox2;
		private System.Windows.Forms.Label label5;
		private System.Windows.Forms.GroupBox 子项组;
		private System.Windows.Forms.CheckBox checkBox4;
		private System.Windows.Forms.CheckBox checkBox3;
		private System.Windows.Forms.GroupBox groupBox3;
		private System.Windows.Forms.CheckBox checkBox2;
		private System.Windows.Forms.Label label6;
		private System.Windows.Forms.Button b0;
		private System.Windows.Forms.Label label1;
		private System.Windows.Forms.CheckBox checkBox1;
		private System.Windows.Forms.Button button2;
		private System.Windows.Forms.CheckBox checkBoxSwitchOldFile;
		private System.Windows.Forms.TextBox textBoxWeb;
		private System.Windows.Forms.CheckBox checkBoxShowOld;
	}
}
