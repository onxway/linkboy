﻿
using System;
using System.Windows.Forms;
using n_MainSystemData;
using n_AVRdude;
using System.IO;

namespace n_SetIDEForm
{
/// <summary>
/// Description of WorkForm.
/// </summary>
public partial class SetIDEForm : Form
{
	int Tick;
	
	//主窗口
	public SetIDEForm()
	{
		//
		// The InitializeComponent() call is required for Windows Forms designer support.
		//
		InitializeComponent();
		//this.MinimumSize = this.Size;
		//this.MaximumSize = this.Size;
		
		//初始化
		Tick = 0;
	}
	
	//显示
	public void Run()
	{
		this.checkBox1.Checked = SystemData.isSuperUser;
		
		this.b0.BackColor = SystemData.GUIbackColorBlack;
		this.b1.BackColor = SystemData.GUIbackColorWhite;
		this.b4.BackColor = SystemData.HeadMessageBackColor;
		this.button边框颜色.BackColor = SystemData.FormColor;
		
		this.checkBox2.Checked = SystemData.isShowLeftBox;
		this.checkBox4.Checked = SystemData.isShowLineNumber;
		this.checkBox3.Checked = SystemData.isShowCurrentIcon;
		this.radioButtonDeep.Checked = SystemData.isBlack;
		this.radioButtonLight.Checked = !SystemData.isBlack;
		this.checkBox生成HEX机器码文件.Checked = SystemData.CreateHEXFile;
		this.comboBoxOptimize.SelectedIndex = SystemData.OptimizeFlag;
		this.checkBoxWin8Touch.Checked = SystemData.isTouchMode;
		this.textBoxUserMessage.Text = SystemData.UserMessage;
		this.textBoxWeb.Text = SystemData.WebMessage;
		this.comboBoxBoardType.Text = SystemData.DefaultBoardType;
		this.checkBoxCreatTempSimFile.Checked = SystemData.CreatTempSimFile;
		this.checkBoxShowNew.Checked = SystemData.ShowNewVersion;
		this.checkBoxMoHu.Checked = SystemData.Mohu;
		this.checkBoxMoClick.Checked = SystemData.ModuleLibClick;
		this.radioButtonSimpleMode.Checked = SystemData.UIModeSimple;
		this.radioButtonStandardMode.Checked = !SystemData.UIModeSimple;
		this.textBoxExtMes.Text = SystemData.ExtMes;
		this.checkBoxGNDBlue.Checked = SystemData.ColorType == 0;
		this.comboBoxUILanguage.Text = SystemData.UILanguage;
		
		//读取语言配置
		if( SystemData.ModuleLanguage == "c" ) {
			this.comboBoxLanguage.SelectedIndex = 0;
		}
		else if( SystemData.ModuleLanguage == "chinese" ) {
			this.comboBoxLanguage.SelectedIndex = 1;
		}
		else {
			this.comboBoxLanguage.SelectedIndex = -1;
			MessageBox.Show( "未知的语言设置: " + SystemData.ModuleLanguage );
		}
		this.checkBoxSwitchOldFile.Checked = SystemData.SwitchOldFile;
		this.checkBoxShowOld.Checked = SystemData.ShowOldModule;
		
		this.Show();
		this.Activate();
		
		//System.Windows.Forms.Clipboard.SetText( n_VM.VM.temp );
	}
	
	//保存
	void Button2Click(object sender, EventArgs e)
	{
		SystemData.isSuperUser = this.checkBox1.Checked;
		
		SystemData.GUIbackColorBlack = this.b0.BackColor;
		SystemData.GUIbackColorWhite = this.b1.BackColor;
		
		SystemData.HeadMessageBackColor = this.b4.BackColor;
		SystemData.FormColor = this.button边框颜色.BackColor;
		
		SystemData.isShowLeftBox = this.checkBox2.Checked;
		SystemData.isShowLineNumber = this.checkBox4.Checked;
		SystemData.isShowCurrentIcon = this.checkBox3.Checked;
		SystemData.isBlack = this.radioButtonDeep.Checked;
		SystemData.CreateHEXFile = this.checkBox生成HEX机器码文件.Checked;
		SystemData.OptimizeFlag = this.comboBoxOptimize.SelectedIndex;
		
		SystemData.isTouchMode = this.checkBoxWin8Touch.Checked;
		
		SystemData.UserMessage = this.textBoxUserMessage.Text;
		SystemData.WebMessage = this.textBoxWeb.Text;
		
		SystemData.DefaultBoardType = this.comboBoxBoardType.Text;
		SystemData.CreatTempSimFile = this.checkBoxCreatTempSimFile.Checked;
		
		SystemData.ShowNewVersion = this.checkBoxShowNew.Checked;
		
		SystemData.Mohu = this.checkBoxMoHu.Checked;
		SystemData.ModuleLibClick = this.checkBoxMoClick.Checked;
		SystemData.UIModeSimple = this.radioButtonSimpleMode.Checked;
		
		SystemData.ExtMes = this.textBoxExtMes.Text;
		if( checkBoxGNDBlue.Checked ) {
			SystemData.ColorType = 0;
		}
		else {
			SystemData.ColorType = 1;
		}
		
		try {
			SystemData.ModuleLanguage = this.comboBoxLanguage.SelectedItem.ToString();
		}
		catch {
			MessageBox.Show( "模块语言未设置, 保存配置时将忽略此项" );
		}
		SystemData.SwitchOldFile = this.checkBoxSwitchOldFile.Checked;
		SystemData.ShowOldModule = this.checkBoxShowOld.Checked;
		SystemData.UILanguage = this.comboBoxUILanguage.Text;
		
		SystemData.isChanged = true;
		this.Visible = false;
	}
	
	//颜色选择按钮点击事件
	void ColorButtonClick(object sender, EventArgs e)
	{
		ColorDialog c = new ColorDialog();
		c.Color = ( (Button)sender ).BackColor;
		if( c.ShowDialog() == DialogResult.OK ) {
			( (Button)sender ).BackColor = c.Color;
		}
	}
	
	//边框显示选择
	void CheckBox2CheckedChanged(object sender, EventArgs e)
	{
		this.子项组.Enabled = this.checkBox2.Checked;
	}
	
	void SetIDEFormFormClosing(object sender, FormClosingEventArgs e)
	{
		this.Visible = false;
		e.Cancel = true;
	}
	
	void TextSetButtonClick(object sender, EventArgs e)
	{
		
	}
	
	void Button2MouseDown(object sender, MouseEventArgs e)
	{
		if( e.Button == MouseButtons.Right ) {
			Tick++;
			if( Tick == 15 ) {
				groupBoxExt.Visible = true;
				groupBoxMes.Visible = true;
			}
		}
	}
	
	void ButtonExportCodeClick(object sender, EventArgs e)
	{
		if( n_MainSystemData.SystemData.isSuperUser ) {
			string r = "";
			for( int i = 0; i < n_VM.VM.InsListRecLen; i += 4 ) {
				
				string ss = "";
				for( int k = 0; k < n_VM.VM.InsListRec[i]; k += 1000 ) {
					ss += "+";
				}
				
				byte c = n_VM.VM.ByteCodeList[i];
				r += i.ToString( "X" ).PadLeft( 4, '0' ) + ":\t" + c + "\t" + n_VM.VM.InsListRec[i] + "\t" + ss + "\n";
			}
			n_OS.VIO.SaveTextFileGB2312( "D:\\r.txt", r );
		}
	}
	
	void ButtonISPClick(object sender, EventArgs e)
	{
		n_AVRdude.AVRdude.tempISP = true;
	}
	
	void ButtonShowMesClick(object sender, EventArgs e)
	{
		n_AVRdude.AVRdude.tempShowError = true;
		
		string ss1 = n_AVRdude.AVRdude.OutputResult.Remove( 0, n_AVRdude.AVRdude.OutputResult.Length / 2 );
		string ss2 = n_AVRdude.AVRdude.OutputResult.Remove( n_AVRdude.AVRdude.OutputResult.Length / 2 );
		MessageBox.Show( ss2 );
		MessageBox.Show( ss1 );
	}
	
	void ButtonSuperClick(object sender, EventArgs e)
	{
		n_GUIcoder.GUIcoder.IgnoreError = true;
	}
	
	void ButtonDebugModeClick(object sender, EventArgs e)
	{
		n_ImagePanel.ImagePanel.DebugMode = true;
	}
	
	/*
	void ButtonSaveExtClick(object sender, EventArgs e)
	{
		System.Windows.Forms.Clipboard.SetText( n_GUIcoder.GUIcoder.CodeAll0_Temp +
												"\n********不带协程程序********\n" + n_GUIcoder.GUIcoder.CodeAll1_Temp +
												"********初始化和循环********\n" + n_GUIcoder.GUIcoder.InitCode_Temp + n_GUIcoder.GUIcoder.BackLoop_Temp +
												"CodeSysLen: " + n_GUIcoder.GUIcoder.CodeSysLine );
	}
	*/
	
	void ButtonShowAllVarClick(object sender, EventArgs e)
	{
		G.ShowAllVar = true;
	}
	
	void ButtonCmpMesClick(object sender, EventArgs e)
	{
		try {
		G.CompileMessageBox.Run();
		this.Visible = false;
		}
		catch {
			MessageBox.Show( "需要先点击下载按钮" );
		}
	}
	
	void ButtonLibMngClick(object sender, EventArgs e)
	{
		if( A.ModuleLibMngBox == null ) {
			A.ModuleLibMngBox = new linkboy.ModuleLibMng();
		}
		A.ModuleLibMngBox.Run();
	}
	
	void ButtonLibPathClick(object sender, EventArgs e)
	{
		n_ModuleLibDecoder.ModuleLibDecoder.TestPath();
	}
	
	void ButtonRefreshExemClick(object sender, EventArgs e)
	{
		MessageBox.Show( "未完成" );
	}
	
	void Button_OtherEditorClick(object sender, EventArgs e)
	{
		G.OtherEditor = true;
	}
	
	void ButtonLCCClick(object sender, EventArgs e)
	{
		Visible = false;
		if( G.LIDEBox == null ) {
			G.LIDEBox = new n_LIDEForm.LIDEForm();
		}
		G.LIDEBox.Run( null, null );
	}
}
}




