﻿
namespace n_HardModule
{
using System;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Windows.Forms;

using n_GUIcoder;
using n_MyFileObject;
using n_UserModule;
using n_UIModule;
using n_GUIset;
using n_MyFileObjectPanel;
using n_Shape;
using n_MyObjectList;
using n_MyObject;
using n_MainSystemData;
using n_MidPortList;
using n_SimulateObj;

//*****************************************************
//硬件组件类
public static class Elm
{
	public enum E_CC_Type {
		NULL, VCC, GND, 
		Capacity, Inductance, Diode, Key, OP_Amp, Power, Resister, Triode, LED, DResister,
		Input, Input1, Trans, Not, And, And3, AndNot, AndNot3, Or, Or3, OrNot, OrNot3, Xor, Xnor, 
		ZAdd, 
		T_RS, 
		M_D, 
		CP_D, CP_JK, 
		QCount8, QCount8L, 
		OSC, 
		ROM8X8, Lock8, DCD38, CMP, 
		D3Resister,
		PinInput, PinOutput,
	};
	static HardModule cm;
	
	//虚拟机内存
	public static byte[] BASE;
	
	const int LPD = 0;
	const int RPD = -11;
	const int S_UP = 8;
	const int SP = 9;
	
	static Pen CC_LinePen1;
	static Pen CC_LinePen2;
	static Pen CC_LinePen3;
	static Pen CC_LinePenArrow;
	
	static Font f;
	static Brush TextFunc;
	static Brush TextPin;
	static SolidBrush TextLED;
	
	
	//初始化
	public static void Init()
	{
		CC_LinePen1 = new Pen( Color.RoyalBlue, 1 );
		CC_LinePen1.StartCap = LineCap.Round;
		CC_LinePen1.EndCap = LineCap.Round;
		
		CC_LinePen2 = new Pen( Color.RoyalBlue, 2 );
		CC_LinePen2.StartCap = LineCap.Round;
		CC_LinePen2.EndCap = LineCap.Round;
		
		CC_LinePen3 = new Pen( Color.RoyalBlue, 3 );
		CC_LinePen3.StartCap = LineCap.Round;
		CC_LinePen3.EndCap = LineCap.Round;
		
		CC_LinePenArrow = new Pen( Color.RoyalBlue, 3 );
		CC_LinePenArrow.StartCap = LineCap.Round;
		CC_LinePenArrow.EndCap = LineCap.ArrowAnchor;
		
		TextPin = new SolidBrush( Color.LightSteelBlue );
		TextFunc = new SolidBrush( Color.OrangeRed );
		f = GUIset.ExpFont;
		
		TextLED = new SolidBrush( Color.FromArgb( 90, 255, 128, 0 ) );
	}
	
	//获取类型
	public static E_CC_Type GetCC_Type( string ImageName )
	{
		E_CC_Type CC_Type = E_CC_Type.NULL;
		switch( ImageName ) {
			case "VCC":			CC_Type = E_CC_Type.VCC; break;
			case "GND":			CC_Type = E_CC_Type.GND; break;
			
			case "Capacity":	CC_Type = E_CC_Type.Capacity; break;
			case "Inductance":	CC_Type = E_CC_Type.Inductance; break;
			case "Diode":		CC_Type = E_CC_Type.Diode; break;
			case "Key":			CC_Type = E_CC_Type.Key; break;
			case "OP_Amp":		CC_Type = E_CC_Type.OP_Amp; break;
			case "Power":		CC_Type = E_CC_Type.Power; break;
			case "Resister":	CC_Type = E_CC_Type.Resister; break;
			case "Triode":		CC_Type = E_CC_Type.Triode; break;
			case "LED":			CC_Type = E_CC_Type.LED; break;
			case "DResister":	CC_Type = E_CC_Type.DResister; break;
			case "D3Resister":	CC_Type = E_CC_Type.D3Resister; break;
			
			case "SYS_PinInput":CC_Type = E_CC_Type.PinInput; break;
			case "SYS_PinOutput":CC_Type = E_CC_Type.PinOutput; break;
			case "Trans":		CC_Type = E_CC_Type.Trans; break;
			case "And":			CC_Type = E_CC_Type.And; break;
			case "And3":		CC_Type = E_CC_Type.And3; break;
			case "AndNot":		CC_Type = E_CC_Type.AndNot; break;
			case "AndNot3":		CC_Type = E_CC_Type.AndNot3; break;
			case "Input":		CC_Type = E_CC_Type.Input; break;
			case "Input1":		CC_Type = E_CC_Type.Input1; break;
			case "Not":			CC_Type = E_CC_Type.Not; break;
			case "Or":			CC_Type = E_CC_Type.Or; break;
			case "Or3":			CC_Type = E_CC_Type.Or3; break;
			case "OrNot":		CC_Type = E_CC_Type.OrNot; break;
			case "OrNot3":		CC_Type = E_CC_Type.OrNot3; break;
			case "Xor":			CC_Type = E_CC_Type.Xor; break;
			case "Xnor":		CC_Type = E_CC_Type.Xnor; break;
			
			case "ZAdd":		CC_Type = E_CC_Type.ZAdd; break;
			
			case "T_RS":		CC_Type = E_CC_Type.T_RS; break;
			
			case "M_D":			CC_Type = E_CC_Type.M_D; break;
			
			case "CP_JK":		CC_Type = E_CC_Type.CP_JK; break;
			case "CP_D":		CC_Type = E_CC_Type.CP_D; break;
			
			case "QCount8":		CC_Type = E_CC_Type.QCount8; break;
			case "QCount8L":	CC_Type = E_CC_Type.QCount8L; break;
			
			case "OSC":			CC_Type = E_CC_Type.OSC; break;
			case "ROM8X8":		CC_Type = E_CC_Type.ROM8X8; break;
			case "Lock8":		CC_Type = E_CC_Type.Lock8; break;
			case "DCD38":		CC_Type = E_CC_Type.DCD38; break;
			case "CMP":			CC_Type = E_CC_Type.CMP; break;
			
			default:			n_OS.VIO.Show( "<isCircuit> 未知的电子元件名称: " + ImageName ); break;
		}
		return CC_Type;
	}
	
	//绘制电子元件
	public static void DrawElm( HardModule m, Graphics g, int xx, int yy, int ww, int hh )
	{
		/*
		g.DrawRectangle( Pens.SlateBlue, xx, yy, ww, hh );
		g.DrawRectangle( Pens.MediumSlateBlue, xx + 10, yy + 10, ww-20, hh - 20 );
		
		g.DrawLine( Pens.Brown, xx + 20, yy, xx + 20, yy + 40 );
		g.DrawLine( Pens.Green, xx, yy + 20, xx + 40, yy + 20 );
		*/
		cm = m;
		
		GraphicsState gs = g.Save();
		if( cm.SwapPort ) {
			//g.TranslateTransform( cm.MidX, cm.MidY );
			g.ScaleTransform( -1, 1 );
			g.TranslateTransform( -cm.MidX*2, 0 );
		}
		switch( cm.CC_Type ) {
			case E_CC_Type.Capacity:	if( cm.isBitmap ) B_DrawCapacity( g, xx, yy, ww, hh ); else DrawCapacity( g, xx, yy, ww, hh ); break;
			case E_CC_Type.Diode:		if( cm.isBitmap ) B_DrawDiode( g, xx, yy, ww, hh ); else DrawDiode( g, xx, yy, ww, hh ); break;
			case E_CC_Type.Key:			if( cm.isBitmap ) B_DrawKey( g, xx, yy, ww, hh ); else DrawKey( g, xx, yy, ww, hh ); break;
			case E_CC_Type.Power:		if( cm.isBitmap ) B_DrawPower( g, xx, yy, ww, hh ); else DrawPower( g, xx, yy, ww, hh ); break;
			case E_CC_Type.Resister:	if( cm.isBitmap ) B_DrawResister( g, xx, yy, ww, hh ); else DrawResister( g, xx, yy, ww, hh ); break;
			case E_CC_Type.DResister:	if( cm.isBitmap ) B_DrawDResister( g, xx, yy, ww, hh ); else DrawDResister( g, xx, yy, ww, hh ); break;
			case E_CC_Type.D3Resister:	if( cm.isBitmap ) B_DrawD3Resister( g, xx, yy, ww, hh ); else DrawD3Resister( g, xx, yy, ww, hh ); break;
			case E_CC_Type.Triode:		if( cm.isBitmap ) B_DrawTriode( g, xx, yy, ww, hh ); else DrawTriode( g, xx, yy, ww, hh ); break;
			case E_CC_Type.LED:			if( cm.isBitmap ) B_DrawLED( g, xx, yy, ww, hh ); else DrawLED( g, xx, yy, ww, hh ); break;
			
			case E_CC_Type.VCC:			DrawVCC( g, xx, yy, ww, hh ); break;
			case E_CC_Type.GND:			DrawGND( g, xx, yy, ww, hh ); break;
			case E_CC_Type.OP_Amp:		DrawOP_Amp( g, xx, yy, ww, hh ); break;
			case E_CC_Type.Inductance:	DrawInductance( g, xx, yy, ww, hh ); break;
			
			case E_CC_Type.PinInput:	DrawPinInput( g, xx, yy, ww, hh ); break;
			case E_CC_Type.PinOutput:	DrawPinOutput( g, xx, yy, ww, hh ); break;
			case E_CC_Type.Trans:		DrawTrans( g, xx, yy, ww, hh ); break;
			case E_CC_Type.And:			DrawAnd( g, xx, yy, ww, hh ); break;
			case E_CC_Type.And3:		DrawAnd3( g, xx, yy, ww, hh ); break;
			case E_CC_Type.AndNot:		DrawAndNot( g, xx, yy, ww, hh ); break;
			case E_CC_Type.AndNot3:		DrawAndNot3( g, xx, yy, ww, hh ); break;
			case E_CC_Type.Input:		DrawInput( g, xx, yy, ww, hh ); break;
			case E_CC_Type.Input1:		DrawInput1( g, xx, yy, ww, hh ); break;
			case E_CC_Type.Not:			DrawNot( g, xx, yy, ww, hh ); break;
			case E_CC_Type.Or:			DrawOr( g, xx, yy, ww, hh ); break;
			case E_CC_Type.Or3:			DrawOr3( g, xx, yy, ww, hh ); break;
			case E_CC_Type.OrNot:		DrawOrNot( g, xx, yy, ww, hh ); break;
			case E_CC_Type.OrNot3:		DrawOrNot3( g, xx, yy, ww, hh ); break;
			case E_CC_Type.Xor:			DrawXor( g, xx, yy, ww, hh ); break;
			case E_CC_Type.Xnor:		DrawXnor( g, xx, yy, ww, hh ); break;
			
			case E_CC_Type.CP_D:		DrawDtrig( g, xx, yy, ww, hh ); break;
			
			case E_CC_Type.ZAdd:		DrawZAdd( g, xx, yy, ww, hh ); break;
			case E_CC_Type.T_RS:		DrawTRS( g, xx, yy, ww, hh ); break;
			case E_CC_Type.CP_JK:		DrawCP_JK( g, xx, yy, ww, hh ); break;
			
			case E_CC_Type.QCount8:		DrawQCount8( g, xx, yy, ww, hh ); break;
			case E_CC_Type.QCount8L:	DrawQCount8L( g, xx, yy, ww, hh ); break;
			
			case E_CC_Type.OSC:			DrawOSC( g, xx, yy, ww, hh ); break;
			
			case E_CC_Type.ROM8X8:		DrawROM8X8( g, xx, yy, ww, hh ); break;
			case E_CC_Type.Lock8:		DrawLock8( g, xx, yy, ww, hh ); break;
			case E_CC_Type.DCD38:		DrawDCD38( g, xx, yy, ww, hh ); break;
			case E_CC_Type.CMP:			DrawCMP( g, xx, yy, ww, hh ); break;
			
			default:	break;
		}
		g.Restore( gs );
	}
	
	//----------------------------------------------
	
	//获取指定序号的端口电流值
	static long GetPort_I( int pidx )
	{
		int addr = cm.PORTList[pidx-1].I_Addr;
		long data = n_Data.Data.GetInt32( BASE, addr );
		return data;
	}
	
	//获取指定序号的端口电势
	static long GetPort_V( int pidx )
	{
		int addr = cm.PORTList[pidx-1].V_Addr;
		long data = n_Data.Data.GetInt32( BASE, addr );
		return data;
	}
	
	//----------------------------------------------
	
	//绘制电容
	static void B_DrawCapacity( Graphics g, int xx, int yy, int ww, int hh )
	{
		/*
		//元件
		g.DrawLine( CC_LinePen3, xx + 10, yy + 15, xx + 30, yy + 15 );
		g.DrawLine( CC_LinePen3, xx + 10, yy + 25, xx + 30, yy + 25 );
		*/
		
		g.DrawImage( cm.SourceImage, xx, yy, ww, hh );
		
		//引脚
		//g.DrawLine( CC_LinePen3, xx + 20, yy, xx + 20, yy + 15 );
		//g.DrawLine( CC_LinePen3, xx + 20, yy + 25, xx + 20, yy + 40 );
	}
	
	//绘制二极管
	static void B_DrawDiode( Graphics g, int xx, int yy, int ww, int hh )
	{
		/*
		g.DrawLine( CC_LinePen3, xx + 10, yy + 10, xx + 30, yy + 10 );
		g.DrawLine( CC_LinePen3, xx + 10, yy + 30, xx + 30, yy + 30 );
		g.DrawLine( CC_LinePen3, xx + 10, yy + 10, xx + 20, yy + 30 );
		g.DrawLine( CC_LinePen3, xx + 30, yy + 10, xx + 20, yy + 30 );
		*/
		
		g.DrawImage( cm.SourceImage, xx, yy, ww, hh );
		
		//引脚
		//g.DrawLine( CC_LinePen3, xx + 20, yy, xx + 20, yy + 40 );
	}
	
	//绘制发光二极管
	static void B_DrawLED( Graphics g, int xx, int yy, int ww, int hh )
	{
		/*
		g.DrawLine( CC_LinePen3, xx + 10, yy + 10, xx + 30, yy + 10 );
		g.DrawLine( CC_LinePen3, xx + 10, yy + 30, xx + 30, yy + 30 );
		g.DrawLine( CC_LinePen3, xx + 10, yy + 10, xx + 20, yy + 30 );
		g.DrawLine( CC_LinePen3, xx + 30, yy + 10, xx + 20, yy + 30 );
		
		//光线
		g.DrawLine( CC_LinePenArrow, xx + 30, yy + 20, xx + 40, yy + 20 );
		g.DrawLine( CC_LinePenArrow, xx + 30, yy + 25, xx + 40, yy + 25 );
		*/
		
		g.DrawImage( cm.SourceImage, xx, yy, ww, hh );
		
		//引脚
		//g.DrawLine( CC_LinePen3, xx + 20, yy, xx + 20, yy + 40 );
		
		if( G.SimulateMode ) {
			long i = (long)(GetPort_I( 2 ) / n_ConstString.ConstString.FixScale);
			
			//大于 60mA 烧坏
			i = i * 40 / 60;
			
			int r = 20+(int)i;
			if( cm.isCC_Error || r > 60 ) {
				cm.isCC_Error = true;
				r = 40;
				g.DrawLine( Pens.Red, xx+15 - r, yy+10 - r, xx+15 + r, yy+10 + r );
				g.DrawLine( Pens.Red, xx+15 - r, yy+10 + r, xx+15 + r, yy+10 - r );
			}
			else {
				int rr = (int)i * 8;
				if( rr > 180 ) rr = 180;
				TextLED.Color = Color.FromArgb( rr, 255, 128, 0 );
				
				g.FillEllipse( TextLED, xx+15 - r, yy+10 - r, r*2, r*2 );
				g.DrawEllipse( Pens.OrangeRed, xx+15 - r, yy+10 - r, r*2, r*2 );
				g.DrawEllipse( Pens.Orange, xx+15 - 20, yy+10 - 20, 40, 40 );
			}
		}
	}
	
	//绘制开关(常开)
	static void B_DrawKey( Graphics g, int xx, int yy, int ww, int hh )
	{
		//元件
		int b = cm.DataList[0];
		
		/*
		if( b == 0 ) {
			g.DrawLine( CC_LinePen3, xx + 20, yy + 40, xx + 30, yy + 10 );
		}
		else {
			g.DrawLine( CC_LinePen3, xx + 20, yy + 40, xx + 20, yy + 10 );
		}
		*/
		
		g.DrawImage( cm.SourceImage, xx, yy, ww, hh );
		
		//引脚
		//g.DrawLine( CC_LinePen3, xx + 20, yy, xx + 20, yy + 10 );
		//g.DrawLine( CC_LinePen3, xx + 20, yy + 40, xx + 20, yy + 50 );
	}
	
	//绘制电源
	static void B_DrawPower( Graphics g, int xx, int yy, int ww, int hh )
	{
		/*
		//元件
		g.DrawEllipse( CC_LinePen3, xx + 5, yy + 10, 30, 30 );
		
		//标记
		g.DrawLine( Pens.SlateGray, xx, yy + 40, xx + 10, yy + 40 ); //-
		g.DrawLine( Pens.SlateGray, xx, yy + 10, xx + 10, yy + 10 ); //+
		g.DrawLine( Pens.SlateGray, xx + 5, yy + 5, xx + 5, yy + 15 ); //+
		*/
		
		g.DrawImage( cm.SourceImage, xx, yy, ww, hh );
		
		//引脚
		//g.DrawLine( CC_LinePen3, xx + 20, yy, xx + 20, yy + 50 );
	}
	
	//绘制电阻
	static void B_DrawResister( Graphics g, int xx, int yy, int ww, int hh )
	{
		/*
		//元件
		g.DrawRectangle( CC_LinePen3, xx + 15, yy + 10, 10, 30 );
		*/
		
		g.DrawImage( cm.SourceImage, xx, yy, ww, hh );
		
		//引脚
		//g.DrawLine( CC_LinePen3, xx + 20, yy, xx + 20, yy + 10 );
		//g.DrawLine( CC_LinePen3, xx + 20, yy + 40, xx + 20, yy + 50 );
	}
	
	//绘制可变电阻
	static void B_DrawDResister( Graphics g, int xx, int yy, int ww, int hh )
	{
		/*
		//元件
		g.DrawRectangle( CC_LinePen3, xx + 15, yy + 10, 10, 30 );
		
		//可变滑头
		g.DrawLine( CC_LinePenArrow, xx + 35, yy + 25, xx + 25, yy + 25 ); //箭头
		g.DrawLine( CC_LinePen3, xx + 35, yy + 25, xx + 35, yy + 5 ); //竖线
		g.DrawLine( CC_LinePen3, xx + 35, yy + 5, xx + 20, yy + 5 ); //横线
		*/
		
		g.DrawImage( cm.SourceImage, xx, yy, ww, hh );
		
		//引脚
		//g.DrawLine( CC_LinePen3, xx + 20, yy, xx + 20, yy + 10 );
		//g.DrawLine( CC_LinePen3, xx + 20, yy + 40, xx + 20, yy + 50 );
	}
	
	//绘制可变电阻
	static void B_DrawD3Resister( Graphics g, int xx, int yy, int ww, int hh )
	{
		/*
		//元件
		g.DrawRectangle( CC_LinePen3, xx + 15, yy + 10, 10, 30 );
		
		//可变滑头
		g.DrawLine( CC_LinePenArrow, xx + 35, yy + 25, xx + 25, yy + 25 ); //箭头
		g.DrawLine( CC_LinePen3, xx + 35, yy + 25, xx + 35, yy + 5 ); //竖线
		g.DrawLine( CC_LinePen3, xx + 35, yy + 5, xx + 20, yy + 5 ); //横线
		*/
		
		g.DrawImage( cm.SourceImage, xx, yy, ww, hh );
		
		//引脚
		//g.DrawLine( CC_LinePen3, xx + 20, yy, xx + 20, yy + 10 );
		//g.DrawLine( CC_LinePen3, xx + 20, yy + 40, xx + 20, yy + 50 );
	}
	
	//绘制三极管
	static void B_DrawTriode( Graphics g, int xx, int yy, int ww, int hh )
	{
		/*
		//元件
		g.DrawLine( CC_LinePen3, xx + 15, yy + 10, xx + 15, yy + 30 ); //基板
		g.DrawLine( CC_LinePen3, xx + 15, yy + 17, xx + 30, yy + 10 );
		g.DrawLine( CC_LinePenArrow, xx + 15, yy + 23, xx + 30, yy + 30 );
		*/
		
		g.DrawImage( cm.SourceImage, xx, yy, ww, hh );
		
		//引脚
		//g.DrawLine( CC_LinePen3, xx, yy + 20, xx + 15, yy + 20 ); //基极
		//g.DrawLine( CC_LinePen3, xx + 30, yy, xx + 30, yy + 10 ); //集电极
		//g.DrawLine( CC_LinePen3, xx + 30, yy + 30, xx + 30, yy + 40 ); //发射极
	}
	
	//----------------------------------------------
	
	//绘制VCC
	static void DrawVCC( Graphics g, int xx, int yy, int ww, int hh )
	{
		//元件
		g.DrawLine( CC_LinePen3, xx + 5, yy + 10, xx + 35, yy + 10 );
		
		//引脚
		g.DrawLine( CC_LinePen3, xx + 20, yy + 10, xx + 20, yy + 30 );
	}
	
	//绘制GND
	static void DrawGND( Graphics g, int xx, int yy, int ww, int hh )
	{
		//元件
		g.DrawLine( CC_LinePen3, xx + 5, yy + 10, xx + 35, yy + 10 );
		g.DrawLine( CC_LinePen3, xx + 10, yy + 20, xx + 30, yy + 20 );
		g.DrawLine( CC_LinePen3, xx + 15, yy + 30, xx + 25, yy + 30 );
		
		//引脚
		g.DrawLine( CC_LinePen3, xx + 20, yy + 00, xx + 20, yy + 10 );
	}
	
	//绘制电容
	static void DrawCapacity( Graphics g, int xx, int yy, int ww, int hh )
	{
		//元件
		g.DrawLine( CC_LinePen3, xx + 10, yy + 15, xx + 30, yy + 15 );
		g.DrawLine( CC_LinePen3, xx + 10, yy + 25, xx + 30, yy + 25 );
		
		//引脚
		g.DrawLine( CC_LinePen3, xx + 20, yy, xx + 20, yy + 15 );
		g.DrawLine( CC_LinePen3, xx + 20, yy + 25, xx + 20, yy + 40 );
	}
	
	//绘制电感
	static void DrawInductance( Graphics g, int xx, int yy, int ww, int hh )
	{
		//元件
		g.DrawRectangle( CC_LinePen3, xx + 18, yy + 10, 4, 30 );
		
		//引脚
		g.DrawLine( CC_LinePen3, xx + 20, yy, xx + 20, yy + 10 );
		g.DrawLine( CC_LinePen3, xx + 20, yy + 40, xx + 20, yy + 50 );
	}
	
	//绘制二极管
	static void DrawDiode( Graphics g, int xx, int yy, int ww, int hh )
	{
		g.DrawLine( CC_LinePen3, xx + 10, yy + 10, xx + 30, yy + 10 );
		g.DrawLine( CC_LinePen3, xx + 10, yy + 30, xx + 30, yy + 30 );
		g.DrawLine( CC_LinePen3, xx + 10, yy + 10, xx + 20, yy + 30 );
		g.DrawLine( CC_LinePen3, xx + 30, yy + 10, xx + 20, yy + 30 );
		
		//引脚
		g.DrawLine( CC_LinePen3, xx + 20, yy, xx + 20, yy + 40 );
	}
	
	//绘制发光二极管
	static void DrawLED( Graphics g, int xx, int yy, int ww, int hh )
	{
		g.DrawLine( CC_LinePen3, xx + 10, yy + 10, xx + 30, yy + 10 );
		g.DrawLine( CC_LinePen3, xx + 10, yy + 30, xx + 30, yy + 30 );
		g.DrawLine( CC_LinePen3, xx + 10, yy + 10, xx + 20, yy + 30 );
		g.DrawLine( CC_LinePen3, xx + 30, yy + 10, xx + 20, yy + 30 );
		
		//光线
		g.DrawLine( CC_LinePenArrow, xx + 30, yy + 20, xx + 40, yy + 20 );
		g.DrawLine( CC_LinePenArrow, xx + 30, yy + 25, xx + 40, yy + 25 );
		
		//引脚
		g.DrawLine( CC_LinePen3, xx + 20, yy, xx + 20, yy + 40 );
		
		if( G.SimulateMode ) {
			long i = (long)(GetPort_I( 2 ) / n_ConstString.ConstString.FixScale);
			
			//大于 60mA 烧坏
			i = i * 40 / 60;
			
			int r = 20+(int)i;
			if( cm.isCC_Error || r > 60 ) {
				cm.isCC_Error = true;
				r = 40;
				g.DrawLine( Pens.Red, xx+20 - r, yy+20 - r, xx+20 + r, yy+20 + r );
				g.DrawLine( Pens.Red, xx+20 - r, yy+20 + r, xx+20 + r, yy+20 - r );
			}
			else {
				int rr = (int)i * 8;
				if( rr > 180 ) rr = 180;
				TextLED.Color = Color.FromArgb( rr, 255, 128, 0 );
				
				g.FillEllipse( TextLED, xx+20 - r, yy+20 - r, r*2, r*2 );
				g.DrawEllipse( Pens.OrangeRed, xx+20 - r, yy+20 - r, r*2, r*2 );
				g.DrawEllipse( Pens.Orange, xx+20 - 20, yy+20 - 20, 40, 40 );
			}
		}
	}
	
	//绘制开关(常开)
	static void DrawKey( Graphics g, int xx, int yy, int ww, int hh )
	{
		//元件
		int b = cm.DataList[0];
		if( b == 0 ) {
			g.DrawLine( CC_LinePen3, xx + 20, yy + 40, xx + 30, yy + 10 );
		}
		else {
			g.DrawLine( CC_LinePen3, xx + 20, yy + 40, xx + 20, yy + 10 );
		}
		
		//引脚
		g.DrawLine( CC_LinePen3, xx + 20, yy, xx + 20, yy + 10 );
		g.DrawLine( CC_LinePen3, xx + 20, yy + 40, xx + 20, yy + 50 );
	}
	
	//绘制运算放大器
	static void DrawOP_Amp( Graphics g, int xx, int yy, int ww, int hh )
	{
		//元件
		g.DrawLine( CC_LinePen3, xx + 10, yy + 10, xx + 10, yy + 50 );
		g.DrawLine( CC_LinePen3, xx + 10, yy + 10, xx + 50, yy + 30 );
		g.DrawLine( CC_LinePen3, xx + 10, yy + 50, xx + 50, yy + 30 );
		
		//标记
		g.DrawLine( Pens.SlateGray, xx + 12, yy + 20, xx + 20, yy + 20 ); //-
		g.DrawLine( Pens.SlateGray, xx + 16, yy + 16, xx + 16, yy + 24 ); //+
		g.DrawLine( Pens.SlateGray, xx + 12, yy + 40, xx + 20, yy + 40 ); //+
		
		//引脚
		g.DrawLine( CC_LinePen3, xx, yy + 20, xx + 10, yy + 20 ); //+
		g.DrawLine( CC_LinePen3, xx, yy + 40, xx + 10, yy + 40 ); //-
		g.DrawLine( CC_LinePen3, xx + 50, yy + 30, xx + 60, yy + 30 ); //O
		
		g.DrawLine( CC_LinePen3, xx + 30, yy, xx + 30, yy + 20 ); //VCC
		g.DrawLine( CC_LinePen3, xx + 30, yy + 40, xx + 30, yy + 60 ); //GND
	}
	
	//绘制电源
	static void DrawPower( Graphics g, int xx, int yy, int ww, int hh )
	{
		//元件
		g.DrawEllipse( CC_LinePen3, xx + 5, yy + 10, 30, 30 );
		
		//标记
		g.DrawLine( Pens.SlateGray, xx, yy + 40, xx + 10, yy + 40 ); //-
		g.DrawLine( Pens.SlateGray, xx, yy + 10, xx + 10, yy + 10 ); //+
		g.DrawLine( Pens.SlateGray, xx + 5, yy + 5, xx + 5, yy + 15 ); //+
		
		//引脚
		g.DrawLine( CC_LinePen3, xx + 20, yy, xx + 20, yy + 50 );
	}
	
	//绘制电阻
	static void DrawResister( Graphics g, int xx, int yy, int ww, int hh )
	{
		//元件
		g.DrawRectangle( CC_LinePen3, xx + 15, yy + 10, 10, 30 );
		
		//引脚
		g.DrawLine( CC_LinePen3, xx + 20, yy, xx + 20, yy + 10 );
		g.DrawLine( CC_LinePen3, xx + 20, yy + 40, xx + 20, yy + 50 );
	}
	
	//绘制可变电阻
	static void DrawDResister( Graphics g, int xx, int yy, int ww, int hh )
	{
		//元件
		g.DrawRectangle( CC_LinePen3, xx + 15, yy + 10, 10, 30 );
		
		//引脚
		g.DrawLine( CC_LinePen3, xx + 20, yy, xx + 20, yy + 10 );
		g.DrawLine( CC_LinePen3, xx + 20, yy + 40, xx + 20, yy + 50 );
		
		//可变滑头
		g.DrawLine( CC_LinePenArrow, xx + 35, yy + 25, xx + 25, yy + 25 ); //箭头
		g.DrawLine( CC_LinePen3, xx + 35, yy + 25, xx + 35, yy + 5 ); //竖线
		g.DrawLine( CC_LinePen3, xx + 35, yy + 5, xx + 20, yy + 5 ); //横线
	}
	
	//绘制可变电阻
	static void DrawD3Resister( Graphics g, int xx, int yy, int ww, int hh )
	{
		//元件
		g.DrawRectangle( CC_LinePen3, xx + 15, yy + 10, 10, 30 );
		
		//引脚
		g.DrawLine( CC_LinePen3, xx + 20, yy, xx + 20, yy + 10 );
		g.DrawLine( CC_LinePen3, xx + 20, yy + 40, xx + 20, yy + 50 );
		
		//可变滑头
		g.DrawLine( CC_LinePenArrow, xx + 35, yy + 25, xx + 25, yy + 25 ); //箭头
		g.DrawLine( CC_LinePen3, xx + 35, yy + 25, xx + 35, yy + 20 ); //竖线
		g.DrawLine( CC_LinePen3, xx + 35, yy + 20, xx + 40, yy + 20 ); //横线
	}
	
	//绘制三极管
	static void DrawTriode( Graphics g, int xx, int yy, int ww, int hh )
	{
		//元件
		g.DrawLine( CC_LinePen3, xx + 15, yy + 10, xx + 15, yy + 30 ); //基板
		g.DrawLine( CC_LinePen3, xx + 15, yy + 17, xx + 30, yy + 10 );
		g.DrawLine( CC_LinePenArrow, xx + 15, yy + 23, xx + 30, yy + 30 );
		
		//引脚
		g.DrawLine( CC_LinePen3, xx, yy + 20, xx + 15, yy + 20 ); //基极
		g.DrawLine( CC_LinePen3, xx + 30, yy, xx + 30, yy + 10 ); //集电极
		g.DrawLine( CC_LinePen3, xx + 30, yy + 30, xx + 30, yy + 40 ); //发射极
	}
	
	//----------------------------------------------
	
	//绘制输入引脚
	static void DrawPinInput( Graphics g, int xx, int yy, int ww, int hh )
	{
		//方框
		g.DrawRectangle( CC_LinePen3, xx + 0, yy + 0, 20, 20 );
		
		g.DrawString( "->", f, TextFunc, xx, yy + 10 - S_UP );
		
		//引脚
		g.DrawLine( CC_LinePen3, xx + 20, yy + 10, xx + 30, yy + 10 ); //OUTPUT
	}
	
	//绘制输出引脚
	static void DrawPinOutput( Graphics g, int xx, int yy, int ww, int hh )
	{
		//方框
		g.DrawRectangle( CC_LinePen3, xx + 0, yy + 0, 20, 20 );
		
		g.DrawString( "<-", f, TextFunc, xx, yy + 10 - S_UP );
		
		//引脚
		g.DrawLine( CC_LinePen3, xx + 20, yy + 10, xx + 30, yy + 10 ); //OUTPUT
	}
	
	//绘制与门
	static void DrawAnd( Graphics g, int xx, int yy, int ww, int hh )
	{
		//方框
		g.DrawRectangle( CC_LinePen3, xx + 10, yy + 0, 30, 40 );
		
		g.DrawString( "&", f, TextFunc, xx + 10 + SP, yy + 20 - S_UP );
		
		//引脚
		g.DrawLine( CC_LinePen3, xx, yy + 10, xx + 10, yy + 10 ); //A
		g.DrawLine( CC_LinePen3, xx, yy + 30, xx + 10, yy + 30 ); //B
		g.DrawLine( CC_LinePen3, xx + 40, yy + 20, xx + 50, yy + 20 ); //OUTPUT
	}
	
	//绘制与门3
	static void DrawAnd3( Graphics g, int xx, int yy, int ww, int hh )
	{
		//方框
		g.DrawRectangle( CC_LinePen3, xx + 10, yy + 0, 30, 40 );
		
		g.DrawString( "&", f, TextFunc, xx + 10 + SP, yy + 20 - S_UP );
		
		//引脚
		g.DrawLine( CC_LinePen3, xx, yy + 10, xx + 10, yy + 10 ); //A
		g.DrawLine( CC_LinePen3, xx, yy + 20, xx + 10, yy + 20 ); //B
		g.DrawLine( CC_LinePen3, xx, yy + 30, xx + 10, yy + 30 ); //C
		g.DrawLine( CC_LinePen3, xx + 40, yy + 20, xx + 50, yy + 20 ); //OUTPUT
	}
	
	//绘制与非门
	static void DrawAndNot( Graphics g, int xx, int yy, int ww, int hh )
	{
		//方框
		g.DrawRectangle( CC_LinePen3, xx + 10, yy + 0, 30, 40 );
		
		g.DrawString( "&", f, TextFunc, xx + 10+ SP, yy + 20 - S_UP );
		
		//引脚
		g.DrawLine( CC_LinePen3, xx, yy + 10, xx + 10, yy + 10 ); //A
		g.DrawLine( CC_LinePen3, xx, yy + 30, xx + 10, yy + 30 ); //B
		g.DrawLine( CC_LinePen3, xx + 45, yy + 20, xx + 50, yy + 20 ); //OUTPUT
		
		g.DrawEllipse( CC_LinePen2, xx + 40, yy + 17, 5, 6 ); //反相输出
	}
	
	//绘制与非门3
	static void DrawAndNot3( Graphics g, int xx, int yy, int ww, int hh )
	{
		//方框
		g.DrawRectangle( CC_LinePen3, xx + 10, yy + 0, 30, 40 );
		
		g.DrawString( "&", f, TextFunc, xx + 10 + SP, yy + 20 - S_UP );
		
		//引脚
		g.DrawLine( CC_LinePen3, xx, yy + 10, xx + 10, yy + 10 ); //A
		g.DrawLine( CC_LinePen3, xx, yy + 20, xx + 10, yy + 20 ); //B
		g.DrawLine( CC_LinePen3, xx, yy + 30, xx + 10, yy + 30 ); //C
		g.DrawLine( CC_LinePen3, xx + 45, yy + 20, xx + 50, yy + 20 ); //OUTPUT
		
		g.DrawEllipse( CC_LinePen2, xx + 40, yy + 17, 5, 6 ); //反相输出
	}
	
	//绘制数据输入
	static void DrawInput( Graphics g, int xx, int yy, int ww, int hh )
	{
		//方框
		g.DrawRectangle( CC_LinePen3, xx + 0, yy + 0, 20, 20 );
		
		g.DrawString( "->", f, TextFunc, xx, yy + 10 - S_UP );
		
		//引脚
		g.DrawLine( CC_LinePen3, xx + 20, yy + 10, xx + 30, yy + 10 ); //OUTPUT
	}
	
	//绘制数据输入1
	static void DrawInput1( Graphics g, int xx, int yy, int ww, int hh )
	{
		//方框
		g.DrawRectangle( CC_LinePen3, xx + 0, yy + 0, 20, 20 );
		
		g.DrawString( "->", f, TextFunc, xx, yy + 10 - S_UP );
		
		//引脚
		g.DrawLine( CC_LinePen3, xx + 25, yy + 10, xx + 30, yy + 10 ); //OUTPUT
		
		g.DrawEllipse( CC_LinePen2, xx + 20, yy + 7, 5, 6 ); //反相输出
	}
	
	//绘制单向传输门
	static void DrawTrans( Graphics g, int xx, int yy, int ww, int hh )
	{
		//方框
		//g.DrawRectangle( CC_LinePen3, xx + 5, yy + 5, 25, 30 );
		g.DrawLine( CC_LinePen3, xx + 10, yy + 0, xx + 50, yy + 20 );
		g.DrawLine( CC_LinePen3, xx + 10, yy + 40, xx + 50, yy + 20 );
		g.DrawLine( CC_LinePen3, xx + 10, yy + 0, xx + 10, yy + 40 );
		
		//g.DrawString( "1", f, TextB, xx + SP, yy + 20 - S_UP );
		
		//引脚
		g.DrawLine( CC_LinePen3, xx, yy + 20, xx + 10, yy + 20 ); //A
		g.DrawLine( CC_LinePen3, xx + 30, yy + 40, xx + 30, yy + 30 ); //CTRL
		
		g.DrawLine( CC_LinePen3, xx + 50, yy + 20, xx + 60, yy + 20 ); //OUTPUT
		
		
		//g.DrawEllipse( CC_LinePen3, xx + 30, yy + 17, 5, 6 ); //反相输出
	}
	
	//绘制非门
	static void DrawNot( Graphics g, int xx, int yy, int ww, int hh )
	{
		//方框
		g.DrawRectangle( CC_LinePen3, xx + 10, yy + 0, 30, 40 );
		
		g.DrawString( "1", f, TextFunc, xx + 10 + SP, yy + 20 - S_UP );
		
		//引脚
		g.DrawLine( CC_LinePen3, xx, yy + 20, xx + 10, yy + 20 ); //A
		
		g.DrawLine( CC_LinePen3, xx + 45, yy + 20, xx + 50, yy + 20 ); //OUTPUT
		
		g.DrawEllipse( CC_LinePen2, xx + 40, yy + 17, 5, 6 ); //反相输出
	}
	
	//绘制或门
	static void DrawOr( Graphics g, int xx, int yy, int ww, int hh )
	{
		//方框
		g.DrawRectangle( CC_LinePen3, xx + 10, yy + 0, 30, 40 );
		
		g.DrawString( "≥1", f, TextFunc, xx + 5 + SP, yy + 20 - S_UP );
		
		//引脚
		g.DrawLine( CC_LinePen3, xx, yy + 10, xx + 10, yy + 10 ); //A
		g.DrawLine( CC_LinePen3, xx, yy + 30, xx + 10, yy + 30 ); //B
		g.DrawLine( CC_LinePen3, xx + 40, yy + 20, xx + 50, yy + 20 ); //OUTPUT
	}
	
	//绘制或门3
	static void DrawOr3( Graphics g, int xx, int yy, int ww, int hh )
	{
		//方框
		g.DrawRectangle( CC_LinePen3, xx + 10, yy + 0, 30, 40 );
		
		g.DrawString( "≥1", f, TextFunc, xx + 5 + SP, yy + 20 - S_UP );
		
		//引脚
		g.DrawLine( CC_LinePen3, xx, yy + 10, xx + 10, yy + 10 ); //A
		g.DrawLine( CC_LinePen3, xx, yy + 20, xx + 10, yy + 20 ); //B
		g.DrawLine( CC_LinePen3, xx, yy + 30, xx + 10, yy + 30 ); //C
		g.DrawLine( CC_LinePen3, xx + 40, yy + 20, xx + 50, yy + 20 ); //OUTPUT
	}
	
	//绘制或非门
	static void DrawOrNot( Graphics g, int xx, int yy, int ww, int hh )
	{
		//方框
		g.DrawRectangle( CC_LinePen3, xx + 10, yy + 0, 30, 40 );
		
		g.DrawString( "≥1", f, TextFunc, xx + 5 + SP, yy + 20 - S_UP );
		
		//引脚
		g.DrawLine( CC_LinePen3, xx, yy + 10, xx + 10, yy + 10 ); //A
		g.DrawLine( CC_LinePen3, xx, yy + 30, xx + 10, yy + 30 ); //B
		g.DrawLine( CC_LinePen3, xx + 45, yy + 20, xx + 50, yy + 20 ); //OUTPUT
		
		g.DrawEllipse( CC_LinePen2, xx + 40, yy + 17, 5, 6 ); //反相输出
	}
	
	//绘制或非门3
	static void DrawOrNot3( Graphics g, int xx, int yy, int ww, int hh )
	{
		//方框
		g.DrawRectangle( CC_LinePen3, xx + 10, yy + 0, 30, 40 );
		
		g.DrawString( "≥1", f, TextFunc, xx + 5 + SP, yy + 20 - S_UP );
		
		//引脚
		g.DrawLine( CC_LinePen3, xx, yy + 10, xx + 10, yy + 10 ); //A
		g.DrawLine( CC_LinePen3, xx, yy + 20, xx + 10, yy + 20 ); //B
		g.DrawLine( CC_LinePen3, xx, yy + 30, xx + 10, yy + 30 ); //C
		g.DrawLine( CC_LinePen3, xx + 45, yy + 20, xx + 50, yy + 20 ); //OUTPUT
		
		g.DrawEllipse( CC_LinePen2, xx + 40, yy + 17, 5, 6 ); //反相输出
	}
	
	//绘制异或门
	static void DrawXor( Graphics g, int xx, int yy, int ww, int hh )
	{
		//方框
		g.DrawRectangle( CC_LinePen3, xx + 10, yy + 0, 30, 40 );
		
		g.DrawString( "=1", f, TextFunc, xx + 5 + SP, yy + 20 - S_UP );
		
		//引脚
		g.DrawLine( CC_LinePen3, xx, yy + 10, xx + 10, yy + 10 ); //A
		g.DrawLine( CC_LinePen3, xx, yy + 30, xx + 10, yy + 30 ); //B
		g.DrawLine( CC_LinePen3, xx + 40, yy + 20, xx + 50, yy + 20 ); //OUTPUT
	}
	
	//绘制同或门
	static void DrawXnor( Graphics g, int xx, int yy, int ww, int hh )
	{
		//方框
		g.DrawRectangle( CC_LinePen3, xx + 10, yy + 0, 30, 40 );
		
		g.DrawString( "==", f, TextFunc, xx + 5 + SP, yy + 20 - S_UP );
		
		//引脚
		g.DrawLine( CC_LinePen3, xx, yy + 10, xx + 10, yy + 10 ); //A
		g.DrawLine( CC_LinePen3, xx, yy + 30, xx + 10, yy + 30 ); //B
		g.DrawLine( CC_LinePen3, xx + 40, yy + 20, xx + 50, yy + 20 ); //OUTPUT
	}
	
	//----------------------------------------------
	
	//绘制D触发器
	static void DrawDtrig( Graphics g, int xx, int yy, int ww, int hh )
	{
		g.DrawString( "D", f, TextFunc, xx + 10 + SP, yy + 25 - S_UP );
		
		//引脚
		g.DrawString( "D", f, TextPin, xx + 10 + LPD, yy + 10 - S_UP ); g.DrawLine( CC_LinePen3, xx, yy + 10, xx + 10, yy + 10 ); //B
		
		g.DrawString( "CP", f, TextPin, xx + 15 + LPD, yy + 40 - S_UP ); g.DrawLine( CC_LinePen3, xx, yy + 40, xx + 10, yy + 40 ); //CLK
		g.DrawLine( CC_LinePen1, xx + 10, yy + 35, xx + 15, yy + 40 );
		g.DrawLine( CC_LinePen1, xx + 10, yy + 45, xx + 15, yy + 40 );
		
		g.DrawString( "Q", f, TextPin, xx + 48 + RPD, yy + 10 - S_UP ); g.DrawLine( CC_LinePen3, xx + 50, yy + 10, xx + 60, yy + 10 ); //OUTPUT
		g.DrawString( "Q", f, TextPin, xx + 48 + RPD, yy + 40 - S_UP ); g.DrawLine( CC_LinePen3, xx + 55, yy + 40, xx + 60, yy + 40 ); //~OUTPUT
		g.DrawEllipse( CC_LinePen2, xx + 50, yy + 37, 5, 6 ); //反相输出
		g.DrawLine( Pens.LightSteelBlue, xx + 50 + RPD, yy + 40 - S_UP, xx + 50 + RPD + 10, yy + 40 - S_UP );
		
		//方框
		g.DrawRectangle( CC_LinePen3, xx + 10, yy + 0, 40, 50 );
	}
	
	//----------------------------------------------
	
	//绘制加法器
	static void DrawZAdd( Graphics g, int xx, int yy, int ww, int hh )
	{
		//方框
		g.DrawRectangle( CC_LinePen3, xx + 5, yy + 5, 25, 30 );
		
		g.DrawString( "+", f, TextFunc, xx + SP, yy + 20 - S_UP );
		
		//引脚
		g.DrawLine( CC_LinePen3, xx, yy + 10, xx + 5, yy + 10 ); //A
		g.DrawLine( CC_LinePen3, xx, yy + 30, xx + 5, yy + 30 ); //B
		g.DrawLine( CC_LinePen3, xx + 30, yy + 20, xx + 40, yy + 20 ); //OUTPUT
	}
	
	//绘制RS触发器
	static void DrawTRS( Graphics g, int xx, int yy, int ww, int hh )
	{
		g.DrawString( "RS", f, TextFunc, xx + 10 + SP, yy + 25 - S_UP );
		
		//引脚
		g.DrawString( "R", f, TextPin, xx + 10 + LPD, yy + 10 - S_UP );
		g.DrawLine( CC_LinePen3, xx, yy + 10, xx + 10, yy + 10 ); //R
		
		g.DrawString( "S", f, TextPin, xx + 10 + LPD, yy + 40 - S_UP );
		g.DrawLine( CC_LinePen3, xx, yy + 40, xx + 10, yy + 40 ); //S
		
		g.DrawString( "Q", f, TextPin, xx + 48 + RPD, yy + 10 - S_UP );
		g.DrawLine( CC_LinePen3, xx + 50, yy + 10, xx + 60, yy + 10 ); //OUTPUT
		
		g.DrawString( "Q", f, TextPin, xx + 48 + RPD, yy + 40 - S_UP );
		g.DrawLine( CC_LinePen3, xx + 55, yy + 40, xx + 60, yy + 40 ); //~OUTPUT
		g.DrawEllipse( CC_LinePen2, xx + 50, yy + 37, 5, 6 ); //反相输出
		g.DrawLine( Pens.LightSteelBlue, xx + 50 + RPD, yy + 40 - S_UP, xx + 50 + RPD + 10, yy + 40 - S_UP );
		
		//方框
		g.DrawRectangle( CC_LinePen3, xx + 10, yy + 0, 40, 50 );
	}
	
	//绘制JK触发器
	static void DrawCP_JK( Graphics g, int xx, int yy, int ww, int hh )
	{
		g.DrawString( "JK", f, TextFunc, xx + 30, yy + 30 - S_UP );
		
		//引脚
		g.DrawString( "J", f, TextPin, xx + 10 + LPD, yy + 10 - S_UP );
		g.DrawLine( CC_LinePen3, xx, yy + 10, xx + 10, yy + 10 ); //J
		
		g.DrawString( "CP", f, TextPin, xx + 15 + LPD, yy + 30 - S_UP );
		g.DrawLine( CC_LinePen3, xx, yy + 30, xx + 10, yy + 30 ); //CP
		g.DrawLine( CC_LinePen1, xx + 10, yy + 25, xx + 15, yy + 30 );
		g.DrawLine( CC_LinePen1, xx + 10, yy + 35, xx + 15, yy + 30 );
		
		g.DrawString( "K", f, TextPin, xx + 10 + LPD, yy + 50 - S_UP );
		g.DrawLine( CC_LinePen3, xx, yy + 50, xx + 10, yy + 50 ); //K
		
		g.DrawString( "Q", f, TextPin, xx + 48 + RPD, yy + 10 - S_UP );
		g.DrawLine( CC_LinePen3, xx + 50, yy + 10, xx + 60, yy + 10 ); //OUTPUT1
		
		g.DrawString( "Q", f, TextPin, xx + 48 + RPD, yy + 50 - S_UP );
		g.DrawLine( CC_LinePen3, xx + 55, yy + 50, xx + 60, yy + 50 ); //OUTPUT2
		g.DrawEllipse( CC_LinePen2, xx + 50, yy + 47, 5, 6 ); //反相输出
		g.DrawLine( Pens.LightSteelBlue, xx + 50 + RPD, yy + 50 - S_UP, xx + 50 + RPD + 10, yy + 50 - S_UP );
		
		//方框
		g.DrawRectangle( CC_LinePen3, xx + 10, yy + 0, 40, 60 );
	}
	
	//绘制8位计数器
	static void DrawQCount8( Graphics g, int xx, int yy, int ww, int hh )
	{
		//方框
		g.DrawRectangle( CC_LinePen3, xx + 10, yy + 0, 40, 90 );
		
		g.DrawString( "CNT", f, TextFunc, xx + 15, yy + 45 - S_UP );
		
		//引脚
		g.DrawString( "CP", f, TextPin, xx + 15 + LPD, yy + 10 - S_UP ); g.DrawLine( CC_LinePen3, xx, yy + 10, xx + 10, yy + 10 ); //A
		g.DrawLine( CC_LinePen1, xx + 10, yy + 5, xx + 15, yy + 10 );
		g.DrawLine( CC_LinePen1, xx + 10, yy + 15, xx + 15, yy + 10 );
		
		//g.DrawLine( CC_LinePen, xx, yy + 30, xx + 5, yy + 30 ); //B
		
		g.DrawString( "0", f, TextPin, xx + 50 + RPD, yy + 10 - S_UP ); g.DrawLine( CC_LinePen3, xx + 50, yy + 10, xx + 60, yy + 10 ); //OUTPUT1
		g.DrawString( "1", f, TextPin, xx + 50 + RPD, yy + 20 - S_UP ); g.DrawLine( CC_LinePen3, xx + 50, yy + 20, xx + 60, yy + 20 ); //OUTPUT2
		g.DrawString( "2", f, TextPin, xx + 50 + RPD, yy + 30 - S_UP ); g.DrawLine( CC_LinePen3, xx + 50, yy + 30, xx + 60, yy + 30 ); //OUTPUT3
		g.DrawString( "3", f, TextPin, xx + 50 + RPD, yy + 40 - S_UP ); g.DrawLine( CC_LinePen3, xx + 50, yy + 40, xx + 60, yy + 40 ); //OUTPUT4
		g.DrawString( "4", f, TextPin, xx + 50 + RPD, yy + 50 - S_UP ); g.DrawLine( CC_LinePen3, xx + 50, yy + 50, xx + 60, yy + 50 ); //OUTPUT5
		g.DrawString( "5", f, TextPin, xx + 50 + RPD, yy + 60 - S_UP ); g.DrawLine( CC_LinePen3, xx + 50, yy + 60, xx + 60, yy + 60 ); //OUTPUT6
		g.DrawString( "6", f, TextPin, xx + 50 + RPD, yy + 70 - S_UP ); g.DrawLine( CC_LinePen3, xx + 50, yy + 70, xx + 60, yy + 70 ); //OUTPUT7
		g.DrawString( "7", f, TextPin, xx + 50 + RPD, yy + 80 - S_UP ); g.DrawLine( CC_LinePen3, xx + 50, yy + 80, xx + 60, yy + 80 ); //OUTPUT8
	}
	
	//绘制8位计数器 可置数
	static void DrawQCount8L( Graphics g, int xx, int yy, int ww, int hh )
	{
		//方框
		g.DrawRectangle( CC_LinePen3, xx + 10, yy + 10, 40, 90 );
		
		g.DrawString( "CNT", f, TextFunc, xx + 15, yy + 55 - S_UP );
		
		//引脚
		g.DrawLine( CC_LinePen3, xx + 20, yy + 0, xx + 20, yy + 10 ); //CP
		g.DrawLine( CC_LinePen1, xx + 15, yy + 10, xx + 20, yy + 15 );
		g.DrawLine( CC_LinePen1, xx + 25, yy + 10, xx + 20, yy + 15 );
		g.DrawString( "C", f, TextPin, xx + 19, yy + 10 );
		
		g.DrawLine( CC_LinePen3, xx + 40, yy + 0, xx + 40, yy + 10 ); //LCOK
		g.DrawLine( CC_LinePen1, xx + 35, yy + 10, xx + 40, yy + 15 );
		g.DrawLine( CC_LinePen1, xx + 45, yy + 10, xx + 40, yy + 15 );
		g.DrawString( "L", f, TextPin, xx + 40 - 9, yy + 10 );
		
		//g.DrawLine( CC_LinePen, xx, yy + 30, xx + 5, yy + 30 ); //B
		
		g.DrawString( "0", f, TextPin, xx + 10 + LPD, yy + 20 - S_UP ); g.DrawLine( CC_LinePen3, xx + 0, yy + 20, xx + 10, yy + 20 ); //INPUT1
		g.DrawString( "1", f, TextPin, xx + 10 + LPD, yy + 30 - S_UP ); g.DrawLine( CC_LinePen3, xx + 0, yy + 30, xx + 10, yy + 30 ); //INPUT2
		g.DrawString( "2", f, TextPin, xx + 10 + LPD, yy + 40 - S_UP ); g.DrawLine( CC_LinePen3, xx + 0, yy + 40, xx + 10, yy + 40 ); //INPUT3
		g.DrawString( "3", f, TextPin, xx + 10 + LPD, yy + 50 - S_UP ); g.DrawLine( CC_LinePen3, xx + 0, yy + 50, xx + 10, yy + 50 ); //INPUT4
		g.DrawString( "4", f, TextPin, xx + 10 + LPD, yy + 60 - S_UP ); g.DrawLine( CC_LinePen3, xx + 0, yy + 60, xx + 10, yy + 60 ); //INPUT5
		g.DrawString( "5", f, TextPin, xx + 10 + LPD, yy + 70 - S_UP ); g.DrawLine( CC_LinePen3, xx + 0, yy + 70, xx + 10, yy + 70 ); //INPUT6
		g.DrawString( "6", f, TextPin, xx + 10 + LPD, yy + 80 - S_UP ); g.DrawLine( CC_LinePen3, xx + 0, yy + 80, xx + 10, yy + 80 ); //INPUT7
		g.DrawString( "7", f, TextPin, xx + 10 + LPD, yy + 90 - S_UP ); g.DrawLine( CC_LinePen3, xx + 0, yy + 90, xx + 10, yy + 90 ); //INPUT8
		
		g.DrawString( "0", f, TextPin, xx + 50 + RPD, yy + 20 - S_UP ); g.DrawLine( CC_LinePen3, xx + 50, yy + 20, xx + 60, yy + 20 ); //OUTPUT1
		g.DrawString( "1", f, TextPin, xx + 50 + RPD, yy + 30 - S_UP ); g.DrawLine( CC_LinePen3, xx + 50, yy + 30, xx + 60, yy + 30 ); //OUTPUT2
		g.DrawString( "2", f, TextPin, xx + 50 + RPD, yy + 40 - S_UP ); g.DrawLine( CC_LinePen3, xx + 50, yy + 40, xx + 60, yy + 40 ); //OUTPUT3
		g.DrawString( "3", f, TextPin, xx + 50 + RPD, yy + 50 - S_UP ); g.DrawLine( CC_LinePen3, xx + 50, yy + 50, xx + 60, yy + 50 ); //OUTPUT4
		g.DrawString( "4", f, TextPin, xx + 50 + RPD, yy + 60 - S_UP ); g.DrawLine( CC_LinePen3, xx + 50, yy + 60, xx + 60, yy + 60 ); //OUTPUT5
		g.DrawString( "5", f, TextPin, xx + 50 + RPD, yy + 70 - S_UP ); g.DrawLine( CC_LinePen3, xx + 50, yy + 70, xx + 60, yy + 70 ); //OUTPUT6
		g.DrawString( "6", f, TextPin, xx + 50 + RPD, yy + 80 - S_UP ); g.DrawLine( CC_LinePen3, xx + 50, yy + 80, xx + 60, yy + 80 ); //OUTPUT7
		g.DrawString( "7", f, TextPin, xx + 50 + RPD, yy + 90 - S_UP ); g.DrawLine( CC_LinePen3, xx + 50, yy + 90, xx + 60, yy + 90 ); //OUTPUT8
	}
	
	//----------------------------------------------
	
	//绘制振荡器
	static void DrawOSC( Graphics g, int xx, int yy, int ww, int hh )
	{
		//方框
		g.DrawRectangle( CC_LinePen3, xx + 0, yy + 0, 30, 40 );
		
		g.DrawString( "f", f, TextFunc, xx + SP, yy + 20 - S_UP );
		
		//引脚
		g.DrawLine( CC_LinePen3, xx + 30, yy + 20, xx + 40, yy + 20 ); //OUTPUT
	}
	
	//绘制8位ROM
	static void DrawROM8X8( Graphics g, int xx, int yy, int ww, int hh )
	{
		//方框
		g.DrawRectangle( CC_LinePen3, xx + 10, yy + 0, 40, 90 );
		
		g.DrawString( "ROM", f, TextFunc, xx + 15, yy + 45 - S_UP );
		//g.DrawString( "->", f, TextFunc, xx + 10 + SP, yy + 60 - S_UP );
		
		//引脚
		g.DrawString( "0", f, TextPin, xx + 10 + LPD, yy + 10 - S_UP ); g.DrawLine( CC_LinePen3, xx, yy + 10, xx + 10, yy + 10 ); //1
		g.DrawString( "1", f, TextPin, xx + 10 + LPD, yy + 20 - S_UP ); g.DrawLine( CC_LinePen3, xx, yy + 20, xx + 10, yy + 20 ); //2
		g.DrawString( "2", f, TextPin, xx + 10 + LPD, yy + 30 - S_UP ); g.DrawLine( CC_LinePen3, xx, yy + 30, xx + 10, yy + 30 ); //3
		g.DrawString( "3", f, TextPin, xx + 10 + LPD, yy + 40 - S_UP ); g.DrawLine( CC_LinePen3, xx, yy + 40, xx + 10, yy + 40 ); //4
		g.DrawString( "4", f, TextPin, xx + 10 + LPD, yy + 50 - S_UP ); g.DrawLine( CC_LinePen3, xx, yy + 50, xx + 10, yy + 50 ); //5
		g.DrawString( "5", f, TextPin, xx + 10 + LPD, yy + 60 - S_UP ); g.DrawLine( CC_LinePen3, xx, yy + 60, xx + 10, yy + 60 ); //6
		g.DrawString( "6", f, TextPin, xx + 10 + LPD, yy + 70 - S_UP ); g.DrawLine( CC_LinePen3, xx, yy + 70, xx + 10, yy + 70 ); //7
		g.DrawString( "7", f, TextPin, xx + 10 + LPD, yy + 80 - S_UP ); g.DrawLine( CC_LinePen3, xx, yy + 80, xx + 10, yy + 80 ); //8
		
		g.DrawString( "0", f, TextPin, xx + 50 + RPD, yy + 10 - S_UP ); g.DrawLine( CC_LinePen3, xx + 50, yy + 10, xx + 60, yy + 10 ); //OUTPUT1
		g.DrawString( "1", f, TextPin, xx + 50 + RPD, yy + 20 - S_UP ); g.DrawLine( CC_LinePen3, xx + 50, yy + 20, xx + 60, yy + 20 ); //OUTPUT2
		g.DrawString( "2", f, TextPin, xx + 50 + RPD, yy + 30 - S_UP ); g.DrawLine( CC_LinePen3, xx + 50, yy + 30, xx + 60, yy + 30 ); //OUTPUT3
		g.DrawString( "3", f, TextPin, xx + 50 + RPD, yy + 40 - S_UP ); g.DrawLine( CC_LinePen3, xx + 50, yy + 40, xx + 60, yy + 40 ); //OUTPUT4
		g.DrawString( "4", f, TextPin, xx + 50 + RPD, yy + 50 - S_UP ); g.DrawLine( CC_LinePen3, xx + 50, yy + 50, xx + 60, yy + 50 ); //OUTPUT5
		g.DrawString( "5", f, TextPin, xx + 50 + RPD, yy + 60 - S_UP ); g.DrawLine( CC_LinePen3, xx + 50, yy + 60, xx + 60, yy + 60 ); //OUTPUT6
		g.DrawString( "6", f, TextPin, xx + 50 + RPD, yy + 70 - S_UP ); g.DrawLine( CC_LinePen3, xx + 50, yy + 70, xx + 60, yy + 70 ); //OUTPUT7
		g.DrawString( "7", f, TextPin, xx + 50 + RPD, yy + 80 - S_UP ); g.DrawLine( CC_LinePen3, xx + 50, yy + 80, xx + 60, yy + 80 ); //OUTPUT8
	}
	
	//绘制8位锁存器
	static void DrawLock8( Graphics g, int xx, int yy, int ww, int hh )
	{
		//方框
		g.DrawRectangle( CC_LinePen3, xx + 10, yy + 10, 40, 90 );
		
		g.DrawString( "LK", f, TextFunc, xx + 20, yy + 55 + RPD );
		
		//引脚
		g.DrawLine( CC_LinePen3, xx + 30, yy + 0, xx + 30, yy + 10 ); //A
		g.DrawString( "CP", f, TextPin, xx + 20, yy + 13 ); //CP
		
		g.DrawLine( CC_LinePen1, xx + 25, yy + 10, xx + 30, yy + 15 );
		g.DrawLine( CC_LinePen1, xx + 35, yy + 10, xx + 30, yy + 15 );
		
		//g.DrawLine( CC_LinePen, xx, yy + 30, xx + 5, yy + 30 ); //B
		
		g.DrawString( "0", f, TextPin, xx + 10 + LPD, yy + 20 - S_UP ); g.DrawLine( CC_LinePen3, xx + 0, yy + 20, xx + 10, yy + 20 ); //INPUT1
		g.DrawString( "1", f, TextPin, xx + 10 + LPD, yy + 30 - S_UP ); g.DrawLine( CC_LinePen3, xx + 0, yy + 30, xx + 10, yy + 30 ); //INPUT2
		g.DrawString( "2", f, TextPin, xx + 10 + LPD, yy + 40 - S_UP ); g.DrawLine( CC_LinePen3, xx + 0, yy + 40, xx + 10, yy + 40 ); //INPUT3
		g.DrawString( "3", f, TextPin, xx + 10 + LPD, yy + 50 - S_UP ); g.DrawLine( CC_LinePen3, xx + 0, yy + 50, xx + 10, yy + 50 ); //INPUT4
		g.DrawString( "4", f, TextPin, xx + 10 + LPD, yy + 60 - S_UP ); g.DrawLine( CC_LinePen3, xx + 0, yy + 60, xx + 10, yy + 60 ); //INPUT5
		g.DrawString( "5", f, TextPin, xx + 10 + LPD, yy + 70 - S_UP ); g.DrawLine( CC_LinePen3, xx + 0, yy + 70, xx + 10, yy + 70 ); //INPUT6
		g.DrawString( "6", f, TextPin, xx + 10 + LPD, yy + 80 - S_UP ); g.DrawLine( CC_LinePen3, xx + 0, yy + 80, xx + 10, yy + 80 ); //INPUT7
		g.DrawString( "7", f, TextPin, xx + 10 + LPD, yy + 90 - S_UP ); g.DrawLine( CC_LinePen3, xx + 0, yy + 90, xx + 10, yy + 90 ); //INPUT8
		
		g.DrawString( "0", f, TextPin, xx + 50 + RPD, yy + 20 - S_UP ); g.DrawLine( CC_LinePen3, xx + 50, yy + 20, xx + 60, yy + 20 ); //OUTPUT1
		g.DrawString( "1", f, TextPin, xx + 50 + RPD, yy + 30 - S_UP ); g.DrawLine( CC_LinePen3, xx + 50, yy + 30, xx + 60, yy + 30 ); //OUTPUT2
		g.DrawString( "2", f, TextPin, xx + 50 + RPD, yy + 40 - S_UP ); g.DrawLine( CC_LinePen3, xx + 50, yy + 40, xx + 60, yy + 40 ); //OUTPUT3
		g.DrawString( "3", f, TextPin, xx + 50 + RPD, yy + 50 - S_UP ); g.DrawLine( CC_LinePen3, xx + 50, yy + 50, xx + 60, yy + 50 ); //OUTPUT4
		g.DrawString( "4", f, TextPin, xx + 50 + RPD, yy + 60 - S_UP ); g.DrawLine( CC_LinePen3, xx + 50, yy + 60, xx + 60, yy + 60 ); //OUTPUT5
		g.DrawString( "5", f, TextPin, xx + 50 + RPD, yy + 70 - S_UP ); g.DrawLine( CC_LinePen3, xx + 50, yy + 70, xx + 60, yy + 70 ); //OUTPUT6
		g.DrawString( "6", f, TextPin, xx + 50 + RPD, yy + 80 - S_UP ); g.DrawLine( CC_LinePen3, xx + 50, yy + 80, xx + 60, yy + 80 ); //OUTPUT7
		g.DrawString( "7", f, TextPin, xx + 50 + RPD, yy + 90 - S_UP ); g.DrawLine( CC_LinePen3, xx + 50, yy + 90, xx + 60, yy + 90 ); //OUTPUT8
	}
	
	//绘制3-8译码器
	static void DrawDCD38( Graphics g, int xx, int yy, int ww, int hh )
	{
		//方框
		g.DrawRectangle( CC_LinePen3, xx + 10, yy + 0, 40, 90 );
		
		g.DrawString( "3-8", f, TextFunc, xx + 15 + LPD, yy + 45 - S_UP );
		
		//引脚
		g.DrawString( "0", f, TextPin, xx + 10 + LPD, yy + 10 - S_UP ); g.DrawLine( CC_LinePen3, xx + 0, yy + 10, xx + 10, yy + 10 ); //INPUT1
		g.DrawString( "1", f, TextPin, xx + 10 + LPD, yy + 20 - S_UP ); g.DrawLine( CC_LinePen3, xx + 0, yy + 20, xx + 10, yy + 20 ); //INPUT2
		g.DrawString( "2", f, TextPin, xx + 10 + LPD, yy + 30 - S_UP ); g.DrawLine( CC_LinePen3, xx + 0, yy + 30, xx + 10, yy + 30 ); //INPUT3
		
		g.DrawString( "0", f, TextPin, xx + 50 + RPD, yy + 10 - S_UP ); g.DrawLine( CC_LinePen3, xx + 50, yy + 10, xx + 60, yy + 10 ); //OUTPUT1
		g.DrawString( "1", f, TextPin, xx + 50 + RPD, yy + 20 - S_UP ); g.DrawLine( CC_LinePen3, xx + 50, yy + 20, xx + 60, yy + 20 ); //OUTPUT2
		g.DrawString( "2", f, TextPin, xx + 50 + RPD, yy + 30 - S_UP ); g.DrawLine( CC_LinePen3, xx + 50, yy + 30, xx + 60, yy + 30 ); //OUTPUT3
		g.DrawString( "3", f, TextPin, xx + 50 + RPD, yy + 40 - S_UP ); g.DrawLine( CC_LinePen3, xx + 50, yy + 40, xx + 60, yy + 40 ); //OUTPUT4
		g.DrawString( "4", f, TextPin, xx + 50 + RPD, yy + 50 - S_UP ); g.DrawLine( CC_LinePen3, xx + 50, yy + 50, xx + 60, yy + 50 ); //OUTPUT5
		g.DrawString( "5", f, TextPin, xx + 50 + RPD, yy + 60 - S_UP ); g.DrawLine( CC_LinePen3, xx + 50, yy + 60, xx + 60, yy + 60 ); //OUTPUT6
		g.DrawString( "6", f, TextPin, xx + 50 + RPD, yy + 70 - S_UP ); g.DrawLine( CC_LinePen3, xx + 50, yy + 70, xx + 60, yy + 70 ); //OUTPUT7
		g.DrawString( "7", f, TextPin, xx + 50 + RPD, yy + 80 - S_UP ); g.DrawLine( CC_LinePen3, xx + 50, yy + 80, xx + 60, yy + 80 ); //OUTPUT8
	}
	
	//绘制CMP
	static void DrawCMP( Graphics g, int xx, int yy, int ww, int hh )
	{
		g.DrawString( "CMP", f, TextFunc, xx + 15, yy + 50 - S_UP );
		
		//引脚
		g.DrawString( "0", f, TextPin, xx + 10 + LPD, yy + 10 - S_UP ); g.DrawLine( CC_LinePen3, xx, yy + 10, xx + 10, yy + 10 ); //1
		g.DrawString( "1", f, TextPin, xx + 10 + LPD, yy + 20 - S_UP ); g.DrawLine( CC_LinePen3, xx, yy + 20, xx + 10, yy + 20 ); //2
		g.DrawString( "2", f, TextPin, xx + 10 + LPD, yy + 30 - S_UP ); g.DrawLine( CC_LinePen3, xx, yy + 30, xx + 10, yy + 30 ); //3
		g.DrawString( "3", f, TextPin, xx + 10 + LPD, yy + 40 - S_UP ); g.DrawLine( CC_LinePen3, xx, yy + 40, xx + 10, yy + 40 ); //4
		
		g.DrawString( "0", f, TextPin, xx + 10 + LPD, yy + 60 - S_UP ); g.DrawLine( CC_LinePen3, xx, yy + 60, xx + 10, yy + 60 ); //5
		g.DrawString( "1", f, TextPin, xx + 10 + LPD, yy + 70 - S_UP ); g.DrawLine( CC_LinePen3, xx, yy + 70, xx + 10, yy + 70 ); //6
		g.DrawString( "2", f, TextPin, xx + 10 + LPD, yy + 80 - S_UP ); g.DrawLine( CC_LinePen3, xx, yy + 80, xx + 10, yy + 80 ); //7
		g.DrawString( "3", f, TextPin, xx + 10 + LPD, yy + 90 - S_UP ); g.DrawLine( CC_LinePen3, xx, yy + 90, xx + 10, yy + 90 ); //8
		
		g.DrawString( "Q", f, TextPin, xx + 48 + RPD, yy + 50 - S_UP ); g.DrawLine( CC_LinePen3, xx + 50, yy + 50, xx + 60, yy + 50 ); //OUTPUT1
		
		//方框
		g.DrawRectangle( CC_LinePen3, xx + 10, yy + 0, 40, 100 );
	}
}
}



