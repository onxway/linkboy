﻿
using System;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Windows.Forms;
using System.Drawing.Imaging;

using n_GUIcoder;
using n_GUIset;
using n_MyScrButton;

using Gif.Components;

namespace n_ScreenPanel
{
public class ScreenPanel : Panel
{
	public delegate void deleClose();
	public deleClose D_Close;
	
	bool LeftPress;
	bool RightPress;
	
	//图片裁剪框
	bool MouseOnS;
	int SX1;
	int SY1;
	int SX2;
	int SY2;
	
	public Image BackImage;
	Bitmap temp;
	
	int MX;
	int MY;
	
	int lastX;
	int lastY;
	
	const int SizeWidth = 14;
	
	SolidBrush Back;
	
	bool StartOK;
	bool SecondOK;
	
	int MouseOnSizeIndex;
	int MouseDownSizeIndex;
	
	MyScrButton OkButton;
	MyScrButton SaveButton;
	
	Color MouseOnColor;
	
	//构造函数
	public ScreenPanel() : base()
	{
		this.BorderStyle = BorderStyle.None;
		this.BackColor = Color.Black;
		
		SetStyle(ControlStyles.UserPaint |
		         ControlStyles.AllPaintingInWmPaint |
		         ControlStyles.OptimizedDoubleBuffer |
		         ControlStyles.ResizeRedraw |
		         ControlStyles.SupportsTransparentBackColor,true);
		
		this.Dock = DockStyle.Fill;
		this.ImeMode = ImeMode.NoControl;
		
		this.MouseMove += new MouseEventHandler( UserMouseMove );
		this.MouseUp += new MouseEventHandler( UserMouseUp );
		this.MouseDown += new MouseEventHandler( UserMouseDown );
		this.MouseWheel += new MouseEventHandler( MyMouseWheel );
		
		SX1 = 200;
		SY1 = 200;
		SX2 = 200;
		SY2 = 200;
		
		Back = new SolidBrush( Color.FromArgb( 90, 0, 0, 0 ) );
		
		OkButton = new MyScrButton( "确定截取并复制到剪贴板", n_OS.OS.SystemRoot + "Resource\\gui\\tools\\screen\\ok.png", n_OS.OS.SystemRoot + "Resource\\gui\\tools\\screen\\ok1.png", 48, 48 );
		OkButton.MouseUpEvent = OKButtonPress;
		
		SaveButton = new MyScrButton( "保存截图内容为图片文件", n_OS.OS.SystemRoot + "Resource\\gui\\tools\\screen\\save.png", n_OS.OS.SystemRoot + "Resource\\gui\\tools\\screen\\save1.png", 48, 48 );
		SaveButton.MouseUpEvent = SaveButtonPress;
		
		Gif_Init();
	}
	
	//重绘事件
	protected override void OnPaint(PaintEventArgs e)
	{
		Graphics g = e.Graphics;
		
		g.SmoothingMode = SmoothingMode.HighQuality;
		
		//填充背景
		g.Clear( Color.Silver );
		
		g.DrawImage( BackImage, 0, 0 );
		g.FillRectangle( Back, 0, 0, Width, Height );
		
		
		if( StartOK ) {
			int sx = SX1;
			int sy = SY1;
			int w = SX2 - SX1;
			int h = SY2 - SY1;
			if( w < 0 ) {
				sx = SX2;
				w = -w;
			}
			if( h < 0 ) {
				sy = SY2;
				h = -h;
			}
			g.DrawImage( BackImage, sx, sy, new Rectangle( sx, sy, w, h ), System.Drawing.GraphicsUnit.Pixel );
			
			g.DrawRectangle( Pens.Black, sx, sy, w, h );
			
			DrawSize( g, SX1, SY1, MouseOnSizeIndex == 1, MouseDownSizeIndex == 1 );
			DrawSize( g, SX2, SY1, MouseOnSizeIndex == 2, MouseDownSizeIndex == 2 );
			DrawSize( g, SX2, SY2, MouseOnSizeIndex == 3, MouseDownSizeIndex == 3 );
			DrawSize( g, SX1, SY2, MouseOnSizeIndex == 4, MouseDownSizeIndex == 4 );
			
			OkButton.X = sx + w - OkButton.Width;
			OkButton.Y = sy + h - OkButton.Height - SizeWidth/2;
			OkButton.Draw( g );
			SaveButton.X = sx + w - OkButton.Width - SaveButton.Width - 2;
			SaveButton.Y = sy + h - OkButton.Height - SizeWidth/2;
			SaveButton.Draw( g );
			
			string text = "X:" + MX + ", Y:" + MY + " " + "RGB = [" + MouseOnColor.R + "," + MouseOnColor.G + "," + MouseOnColor.B + "]";
			g.DrawString( "点击鼠标右键取消截图 " + text, GUIset.ExpFont, Brushes.White, sx, sy + h + 5 );
		}
	}
	
	void DrawSize( Graphics g, int x, int y, bool On, bool Down )
	{
		if( Down ) {
			g.FillEllipse( Brushes.CornflowerBlue, x - SizeWidth/2, y - SizeWidth/2, SizeWidth, SizeWidth );
			g.DrawEllipse( Pens.Black, x - SizeWidth/2, y - SizeWidth/2, SizeWidth, SizeWidth );
		}
		else if( On ) {
			g.FillEllipse( Brushes.CornflowerBlue, x - SizeWidth/2, y - SizeWidth/2, SizeWidth, SizeWidth );
			g.DrawEllipse( Pens.Black, x - SizeWidth/2, y - SizeWidth/2, SizeWidth, SizeWidth );
		}
		else {
			g.FillEllipse( Brushes.WhiteSmoke, x - SizeWidth/2, y - SizeWidth/2, SizeWidth, SizeWidth );
			g.DrawEllipse( Pens.SlateGray, x - SizeWidth/2, y - SizeWidth/2, SizeWidth, SizeWidth );
		}
	}
	
	//截取屏幕图片
	public void GetScreen()
	{
		Rectangle ScreenArea = System.Windows.Forms.Screen.GetBounds(this);
		BackImage = new Bitmap( ScreenArea.Width, ScreenArea.Height );
		Graphics g = Graphics.FromImage( BackImage );
		g.CopyFromScreen( new Point( 0, 0 ), new Point( 0, 0 ), BackImage.Size );
		
		temp = new Bitmap( BackImage );
		
		StartOK = false;
		SecondOK = false;
		
		MouseOnS = false;
		MouseOnSizeIndex = 0;
		MouseDownSizeIndex = 0;
	}
	
	void MyMouseWheel( object sender, MouseEventArgs e)
	{
		if( e.Delta > 0 ) {
			//StartY += 50;
		}
		else {
			//StartY -= 50;
		}
		this.Invalidate();
	}
	
	//鼠标按下事件
	void UserMouseDown( object sender, MouseEventArgs e )
	{
		MX = e.X;
		MY = e.Y;
		
		lastX = e.X;
		lastY = e.Y;
		
		if( e.Button == MouseButtons.Left ) {
			LeftPress = true;
			
			if( StartOK ) {
				
				int sx = SX1;
				int sy = SY1;
				int w = SX2 - SX1;
				int h = SY2 - SY1;
				if( w < 0 ) {
					sx = SX2;
					w = -w;
				}
				if( h < 0 ) {
					sy = SY2;
					h = -h;
				}
				
				if( MouseOnSizeIndex != 0 ) {
					MouseDownSizeIndex = MouseOnSizeIndex;
				}
				else {
					OkButton.MouseDown( e.X, e.Y );
					SaveButton.MouseDown( e.X, e.Y );
				}
			}
			else {
				SX1 = e.X;
				SY1 = e.Y;
				SX2 = e.X;
				SY2 = e.Y;
				StartOK = true;
			}
		}
		if( e.Button == MouseButtons.Right ) {
			RightPress = true;
			
			if( StartOK ) {
				StartOK = false;
				SecondOK = false;
			}
			else {
				if( D_Close != null ) {
					D_Close();
				}
			}
		}
		this.Invalidate();
		
		this.Focus();
	}
	
	//鼠标松开事件
	void UserMouseUp( object sender, MouseEventArgs e )
	{
		if( e.Button == MouseButtons.Left ) {
			LeftPress = false;
			MouseDownSizeIndex = 0;
			if( !SecondOK ) {
				
				SecondOK = true;
			}
			else {
				OkButton.MouseUp( e.X, e.Y );
				SaveButton.MouseUp( e.X, e.Y );
			}
		}
		if( e.Button == MouseButtons.Right ) {
			RightPress = false;
		}
		this.Invalidate();
	}
	
	//鼠标移动事件
	void UserMouseMove( object sender, MouseEventArgs e )
	{
		MX = e.X;
		MY = e.Y;
		
		MouseOnColor = temp.GetPixel( MX, MY );
		
		if( LeftPress ) {
			if( StartOK ) {
				if( !SecondOK ) {
					SX2 = e.X;
					SY2 = e.Y;
				}
				else {
					
					if( MouseDownSizeIndex != 0 ) {
						//判断是否移动端点
						if( MouseDownSizeIndex == 1 ) {
							SX1 = e.X;// - lastX;
							SY1 = e.Y;// - lastY;
							lastX = e.X;
							lastY = e.Y;
						}
						//判断是否移动端点
						if( MouseDownSizeIndex == 2 ) {
							SX2 = e.X;// - lastX;
							SY1 = e.Y;// - lastY;
							lastX = e.X;
							lastY = e.Y;
						}
						//判断是否移动端点
						if( MouseDownSizeIndex == 3 ) {
							SX2 = e.X;// - lastX;
							SY2 = e.Y;// - lastY;
							lastX = e.X;
							lastY = e.Y;
						}
						//判断是否移动端点
						if( MouseDownSizeIndex == 4 ) {
							SX1 = e.X;// - lastX;
							SY2 = e.Y;// - lastY;
							lastX = e.X;
							lastY = e.Y;
						}
					}
					else {
						//判断是否移动选区
						if( MouseOnS ) {
							
							int sx1 = SX1;
							int sy1 = SY1;
							int sx2 = SX2;
							int sy2 = SY2;
							
							sx1 += e.X - lastX;
							sy1 += e.Y - lastY;
							sx2 += e.X - lastX;
							sy2 += e.Y - lastY;
							
							if( sx1 < 0 || sx1 >= Width ||
							    sx2 < 0 || sx2 >= Width ) {
								//...
							}
							else {
								SX1 = sx1;
								SX2 = sx2;
								lastX = e.X;
							}
							if( sy1 < 0 || sy1 >= Height ||
							    sy2 < 0 || sy2 >= Height ) {
								//...
							}
							else {
								SY1 = sy1;
								SY2 = sy2;
								lastY = e.Y;
							}
						}
					}
				}
			}
		}
		else {
			
			if( StartOK ) {
				int sx = SX1;
				int sy = SY1;
				int w = SX2 - SX1;
				int h = SY2 - SY1;
				if( w < 0 ) {
					sx = SX2;
					w = -w;
				}
				if( h < 0 ) {
					sy = SY2;
					h = -h;
				}
				
				if( e.X >= sx && e.X < sx + w && e.Y >= sy && e.Y < sy + h ) {
					MouseOnS = true;
				}
				else {
					MouseOnS = false;
				}
				
				MouseOnSizeIndex = 0;
				if( e.X >= SX1 - SizeWidth/2 && e.X <= SX1 + SizeWidth/2 && e.Y >= SY1 - SizeWidth/2 && e.Y <= SY1 + SizeWidth/2 ) {
					MouseOnSizeIndex = 1;
				}
				if( e.X >= SX2 - SizeWidth/2 && e.X <= SX2 + SizeWidth/2 && e.Y >= SY1 - SizeWidth/2 && e.Y <= SY1 + SizeWidth/2 ) {
					MouseOnSizeIndex = 2;
				}
				if( e.X >= SX2 - SizeWidth/2 && e.X <= SX2 + SizeWidth/2 && e.Y >= SY2 - SizeWidth/2 && e.Y <= SY2 + SizeWidth/2 ) {
					MouseOnSizeIndex = 3;
				}
				if( e.X >= SX1 - SizeWidth/2 && e.X <= SX1 + SizeWidth/2 && e.Y >= SY2 - SizeWidth/2 && e.Y <= SY2 + SizeWidth/2 ) {
					MouseOnSizeIndex = 4;
				}
				OkButton.MouseMove( e.X, e.Y );
				SaveButton.MouseMove( e.X, e.Y );
				
				
				
				if( MouseOnSizeIndex == 1 ) {
					Cursor = System.Windows.Forms.Cursors.SizeNWSE;
				}
				else if( MouseOnSizeIndex == 2 ) {
					Cursor = System.Windows.Forms.Cursors.SizeNESW;
				}
				else if( MouseOnSizeIndex == 3 ) {
					Cursor = System.Windows.Forms.Cursors.SizeNWSE;
				}
				else if( MouseOnSizeIndex == 4 ) {
					Cursor = System.Windows.Forms.Cursors.SizeNESW;
				}
				else if( MouseOnS ) {
					Cursor = System.Windows.Forms.Cursors.SizeAll;
				}
				else {
					Cursor = System.Windows.Forms.Cursors.Default;
				}
			}
		}
		
		if( RightPress ) {
			//...
		}
		this.Invalidate();
	}
	
	//确定键按下时
	void OKButtonPress()
	{
		Bitmap clipm = GetSelectBitmap();
		
		Clipboard.SetImage(clipm);
		
		if( D_Close != null ) {
			D_Close();
		}
	}
	
	//=========================================================
	
	//保存键按下时
	void SaveButtonPress()
	{
		SaveImg( null );
	}
	
	public void SaveImg( Bitmap default_bmp )
	{
		SaveFileDialog sd = new SaveFileDialog();
		sd.Filter = "jpg图片类型文件|*.jpg|png图片类型文件|*.png";
		sd.Title = "文件另存为...";
		sd.FileName = "linkboy截图-" + (System.DateTime.Now).ToString().Replace( "/", "." ).Replace( ":", "-" );
		
		if( sd.ShowDialog() == DialogResult.OK ) {
			
			Bitmap bm = default_bmp;
			if( bm == null ) {
				bm = GetSelectBitmap();
			}
			
			if( sd.FileName.ToLower().EndsWith( ".jpg" ) ) {
				GetPicThumbnail( bm, sd.FileName, 68 );
			}
			else {
				bm.Save( sd.FileName );
			}
			if( D_Close != null ) {
				D_Close();
			}
		}
	}
	
	Bitmap GetSelectBitmap()
	{
		int sx = SX1;
		int sy = SY1;
		int w = SX2 - SX1;
		int h = SY2 - SY1;
		if( w < 0 ) {
			sx = SX2;
			w = -w;
		}
		if( h < 0 ) {
			sy = SY2;
			h = -h;
		}
		Bitmap clipm = new Bitmap( w, h );
		Graphics g = Graphics.FromImage( clipm );
		g.DrawImage( BackImage, 0, 0, new Rectangle( sx, sy, w, h ), System.Drawing.GraphicsUnit.Pixel );
		
		return clipm;
	}
	
	bool GetPicThumbnail( Bitmap iSource, string outPath, int flag )
	{
		ImageFormat tFormat = iSource.RawFormat;
		
		//以下代码为保存图片时，设置压缩质量
		EncoderParameters ep = new EncoderParameters();
		long[] qy = new long[1];
		qy[0] = flag;//设置压缩的比例1-100
		EncoderParameter eParam = new EncoderParameter(System.Drawing.Imaging.Encoder.Quality, qy);
		ep.Param[0] = eParam;
		try
		{
			ImageCodecInfo[] arrayICI = ImageCodecInfo.GetImageEncoders();
			ImageCodecInfo jpegICIinfo = null;
			for (int x = 0; x < arrayICI.Length; x++)
			{
				if (arrayICI[x].FormatDescription.Equals("JPEG"))
				{
					jpegICIinfo = arrayICI[x];
					break;
				}
			}
			if (jpegICIinfo != null)
			{
				iSource.Save(outPath, jpegICIinfo, ep);//dFile是压缩后的新路径
			}
			else
			{
				iSource.Save(outPath, tFormat);
			}
			return true;
		}
		catch
		{
			return false;
		}
	}
	
	//=========================================================
	//GIF截图
	
	Bitmap[] GifBmpList;
	int GifLen;
	bool Gif_StartFlg;
	
	void Gif_Init()
	{
		GifBmpList = new Bitmap[500];
		GifLen = 0;
		Gif_StartFlg = false;
	}
	
	public void Gif_Start()
	{
		GifLen = 0;
		Gif_StartFlg = true;
	}
	
	public void Gif_Run()
	{
		//n_Debug.Debug.Message = "GIF: " + GifLen;
		
		if( !Gif_StartFlg ) {
			return;
		}
		if( GifLen > 100 ) {
			
			String outputFilePath = "d:\\result.gif";
            AnimatedGifEncoder pGif = new AnimatedGifEncoder();
            pGif.Start(outputFilePath);
            pGif.SetDelay(100);
            //-1:no repeat,0:always repeat
            pGif.SetRepeat(0);
            for (int i = 0; i < GifLen; i++  ) {
            	pGif.AddFrame( GifBmpList[i] );
            }
            pGif.Finish();
			
			Gif_StartFlg = false;
			return;
		}
		Bitmap img = new Bitmap( 500, 500 );
		Graphics g = Graphics.FromImage( img );
		g.CopyFromScreen( new Point( 600, 500 ), new Point( 0, 0 ), img.Size );
		
		GifBmpList[GifLen] = img;
		GifLen++;
	}
}
}


