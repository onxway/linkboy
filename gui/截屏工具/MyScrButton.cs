﻿
namespace n_MyScrButton
{
using System;
using System.Drawing;
using n_MyObjectList;
using System.Windows.Forms;
using n_GUIset;
using n_Shape;
using System.Drawing.Drawing2D;

public class MyScrButton
{
	public delegate void D_MouseDownEvent();
	public D_MouseDownEvent MouseDownEvent;
	public delegate void D_MouseUpEvent();
	public D_MouseUpEvent MouseUpEvent;
	
	public bool isMouseOn;
	
	public Image BackImage;
	public Image BackImage1;
	
	public string Text;
	public string TipText;
	
	bool isPress;
	
	public bool Visible;
	
	public int X;
	public int Y;
	public int Width;
	public int Height;
	
	//构造函数
	public MyScrButton( string text, string path, string path1, int width, int height )
	{
		Width = width;
		Height = height;
		
		if( path != null ) {
			BackImage = new Bitmap( path );
		}
		if( path1 != null ) {
			BackImage1 = new Bitmap( path1 );
		}
		isPress = false;
		isMouseOn = false;
		
		Visible = true;
		Text = text;
		
		if( Width == 0 && Height == 0 ) {
			Width = BackImage.Width;
			Height = BackImage.Height;
		}
	}
	
	//设置坐标
	public void SetLocation( Point p )
	{
		X = p.X;
		Y = p.Y;
	}
	
	//绘制按钮样式
	public void Draw( Graphics g )
	{
		if( !Visible ) {
			return;
		}
		
		if( isPress ) {
			//g.FillRectangle( Brushes.Silver, X, Y, Width, Height );
			g.DrawImage( BackImage, X, Y, Width, Height );
			//g.DrawEllipse( Pens.Black, X, Y, Width, Height );
		}
		else if( isMouseOn ) {
			//g.FillRectangle( Brushes.WhiteSmoke, X, Y, Width, Height );
			g.DrawImage( BackImage1, X, Y, Width, Height );
			//g.DrawEllipse( Pens.SlateGray, X, Y, Width, Height );
		}
		else {
			//g.FillRectangle( Brushes.WhiteSmoke, X, Y, Width, Height );
			g.DrawImage( BackImage, X, Y, Width, Height );
			//g.DrawRectangle( Pens.Gray, X, Y, Width, Height );
		}
		
		if( Text != null && isMouseOn ) {
			g.DrawString( Text, GUIset.ExpFont, Brushes.WhiteSmoke, X, Y + Height + 5 );
		}
		
		if( TipText != null && isMouseOn ) {
			//g.DrawString( TipText, GUIset.Font12, Brushes.WhiteSmoke, n_Head.Head.CX, 20 );
		}
	}
	
	//鼠标按下事件
	public bool MouseDown( int mX, int mY )
	{
		if( !Visible ) {
			return false;
		}
		if( isMouseOn ) {
			if( MouseDownEvent != null ) {
				MouseDownEvent();
			}
			isPress = true;
			return true;
		}
		return false;
	}
	
	//鼠标松开事件
	public void MouseUp( int mX, int mY )
	{
		if( !Visible ) {
			return;
		}
		if( isPress ) {
			if( MouseUpEvent != null ) {
				MouseUpEvent();
			}
			isPress = false;
		}
	}
	
	//鼠标移动事件
	public void MouseMove( int mX, int mY )
	{
		if( !Visible ) {
			return;
		}
		isMouseOn = MouseIsInside( mX, mY );
	}
	
	//判断鼠标是否在当前组件上
	bool MouseIsInside( int mX, int mY )
	{
		if( mX >= X && mX <= X + Width && mY >= Y && mY <= Y + Height ) {
			return true;
		}
		else {
			return false;
		}
	}
}
}



