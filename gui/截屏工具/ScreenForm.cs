﻿
using System;
using System.Drawing;
using System.Windows.Forms;
using n_ScreenPanel;

namespace n_ScreenForm
{
/// <summary>
/// Description of LocateForm.
/// </summary>
public partial class ScreenForm : Form
{
	public ScreenPanel SPanel;
	
	public ScreenForm()
	{
		//
		// The InitializeComponent() call is required for Windows Forms designer support.
		//
		InitializeComponent();
		
		SPanel = new ScreenPanel();
		this.Controls.Add( SPanel );
		
		SPanel.D_Close = MyClose;
	}
	
	//退出
	void MyClose()
	{
		Visible = false;
	}
	
	//运行
	public void Run()
	{
		SPanel.GetScreen();
		this.Visible = true;
	}
}
}

