﻿
namespace n_FindForm
{
using System;
using System.IO;
using System.Net;
using System.Text;
using System.Windows.Forms;
using n_MainSystemData;
using System.Drawing;
using System.Drawing.Drawing2D;

/// <summary>
/// Description of WorkForm.
/// </summary>
public partial class FindForm : Form
{
	//主窗口
	public FindForm()
	{
		//
		// The InitializeComponent() call is required for Windows Forms designer support.
		//
		InitializeComponent();
		this.MinimumSize = this.Size;
		this.MaximumSize = this.Size;
		//初始化
		//
		// TODO: Add constructor code after the InitializeComponent() call.
		//
	}
	
	//窗体运行
	public void Run()
	{
		this.Show();
	}
	
	//窗体移动事件
	void FindFormMove(object sender, EventArgs e)
	{
		SystemData.FormLocationList[ SystemData.FindBoxIndex ] = this.Location;
		SystemData.isChanged = true;
	}
	
	//窗体关闭事件
	void FindFormFormClosing(object sender, FormClosingEventArgs e)
	{
		this.Visible = false;
		e.Cancel = true;
	}
	
	//查找下一个
	bool SearchNextAndFind( bool Replace )
	{
		string SourceText = this.SourcetextBox.Text;
		string TargetText = this.TargettextBox.Text;
		if( Replace && G.FindTextBox.SelectedText == SourceText ) {
			G.FindTextBox.SelectedText = TargetText;
		}
		int CurrentIndex = G.FindTextBox.SelectionStart + 1;
		int NextIndex = G.FindTextBox.Text.IndexOf( SourceText, CurrentIndex );
		if( NextIndex == -1 ) {
			return false;
		}
		G.FindTextBox.SelectionStart = NextIndex;
		G.FindTextBox.SelectionLength = SourceText.Length;
		G.FindTextBox.Focus();
		return true;
	}
	
	//查找
	void Button6Click(object sender, EventArgs e)
	{
		if( G.FindTextBox == null ) {
			MessageBox.Show( "请先打开一个文件" );
			return;
		}
		if( !SearchNextAndFind( false ) ) {
			MessageBox.Show( "没找到目标字符串" );
		}
	}
	
	//替换
	void Button7Click(object sender, EventArgs e)
	{
		if( G.FindTextBox == null ) {
			MessageBox.Show( "请先打开一个文件" );
			return;
		}
		if( !SearchNextAndFind( true ) ) {
			MessageBox.Show( "没找到目标字符串" );
		}
	}
	
	//替换全部
	void Button5Click(object sender, EventArgs e)
	{
		if( G.FindTextBox == null ) {
			MessageBox.Show( "请先打开一个文件" );
			return;
		}
		while( SearchNextAndFind( true ) ) {}
	}
}
}


