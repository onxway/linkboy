﻿/*
 * 由 SharpDevelop 创建。
 * 用户: bbb_2
 * 日期: 2009-11-17
 * 时间: 11:15
 * 
 * 如果你想要更改该模板，那么请使用“工具 | 选项 | 编辑 | 编辑标准标题”。
 */
namespace n_FindForm
{
	partial class FindForm
	{
		/// <summary>
		/// Designer variable used to keep track of non-visual components.
		/// </summary>
		private System.ComponentModel.IContainer components = null;
		
		/// <summary>
		/// Disposes resources used by the form.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing) {
				if (components != null) {
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}
		
		/// <summary>
		/// This method is required for Windows Forms designer support.
		/// Do not change the method contents inside the source code editor. The Forms designer might
		/// not be able to load this method if it was changed manually.
		/// </summary>
		private void InitializeComponent()
		{
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FindForm));
			this.button5 = new System.Windows.Forms.Button();
			this.button6 = new System.Windows.Forms.Button();
			this.button7 = new System.Windows.Forms.Button();
			this.label1 = new System.Windows.Forms.Label();
			this.label2 = new System.Windows.Forms.Label();
			this.SourcetextBox = new System.Windows.Forms.RichTextBox();
			this.TargettextBox = new System.Windows.Forms.RichTextBox();
			this.SuspendLayout();
			// 
			// button5
			// 
			this.button5.BackColor = System.Drawing.Color.Gainsboro;
			resources.ApplyResources(this.button5, "button5");
			this.button5.ForeColor = System.Drawing.Color.SaddleBrown;
			this.button5.Name = "button5";
			this.button5.UseVisualStyleBackColor = false;
			this.button5.Click += new System.EventHandler(this.Button5Click);
			// 
			// button6
			// 
			this.button6.BackColor = System.Drawing.Color.Gainsboro;
			resources.ApplyResources(this.button6, "button6");
			this.button6.ForeColor = System.Drawing.Color.Black;
			this.button6.Name = "button6";
			this.button6.UseVisualStyleBackColor = false;
			this.button6.Click += new System.EventHandler(this.Button6Click);
			// 
			// button7
			// 
			this.button7.BackColor = System.Drawing.Color.Gainsboro;
			resources.ApplyResources(this.button7, "button7");
			this.button7.ForeColor = System.Drawing.Color.SaddleBrown;
			this.button7.Name = "button7";
			this.button7.UseVisualStyleBackColor = false;
			this.button7.Click += new System.EventHandler(this.Button7Click);
			// 
			// label1
			// 
			resources.ApplyResources(this.label1, "label1");
			this.label1.ForeColor = System.Drawing.Color.Black;
			this.label1.Name = "label1";
			// 
			// label2
			// 
			resources.ApplyResources(this.label2, "label2");
			this.label2.ForeColor = System.Drawing.Color.SaddleBrown;
			this.label2.Name = "label2";
			// 
			// SourcetextBox
			// 
			this.SourcetextBox.AcceptsTab = true;
			this.SourcetextBox.BorderStyle = System.Windows.Forms.BorderStyle.None;
			resources.ApplyResources(this.SourcetextBox, "SourcetextBox");
			this.SourcetextBox.Name = "SourcetextBox";
			// 
			// TargettextBox
			// 
			this.TargettextBox.AcceptsTab = true;
			this.TargettextBox.BorderStyle = System.Windows.Forms.BorderStyle.None;
			resources.ApplyResources(this.TargettextBox, "TargettextBox");
			this.TargettextBox.Name = "TargettextBox";
			// 
			// FindForm
			// 
			resources.ApplyResources(this, "$this");
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.BackColor = System.Drawing.Color.WhiteSmoke;
			this.Controls.Add(this.TargettextBox);
			this.Controls.Add(this.SourcetextBox);
			this.Controls.Add(this.button7);
			this.Controls.Add(this.button6);
			this.Controls.Add(this.button5);
			this.Controls.Add(this.label1);
			this.Controls.Add(this.label2);
			this.MaximizeBox = false;
			this.MinimizeBox = false;
			this.Name = "FindForm";
			this.Opacity = 0.5D;
			this.ShowInTaskbar = false;
			this.TopMost = true;
			this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.FindFormFormClosing);
			this.Move += new System.EventHandler(this.FindFormMove);
			this.ResumeLayout(false);
		}
		private System.Windows.Forms.RichTextBox TargettextBox;
		private System.Windows.Forms.RichTextBox SourcetextBox;
		private System.Windows.Forms.Label label2;
		private System.Windows.Forms.Label label1;
		private System.Windows.Forms.Button button7;
		private System.Windows.Forms.Button button6;
		private System.Windows.Forms.Button button5;
	}
}
