﻿
namespace n_KeyboardForm
{
using System;
using System.Windows.Forms;
using System.IO;
using System.Drawing;

using n_VarType;
using n_UnitList;
using n_FunctionList;
using n_VarList;
using n_StructList;
using n_ImagePanel;
using n_MyObjectList;
using n_MyObject;
using n_HardModule;
using n_GVar;
using n_GUIcoder;
using n_MyFileObject;
using c_FormMover;
using n_EXP;
using n_EXPcommon;

/// <summary>
/// Description of WorkForm.
/// </summary>
public partial class KeyboardForm : Form
{
	bool isOK;
	
	string KeyList;
	Button LastButton;
	
	//FormMover fm;
	
	//主窗口
	public KeyboardForm()
	{
		InitializeComponent();
		isOK = false;
		
		//fm = new FormMover( this );
	}
	
	//运行
	public string Run( string s )
	{
		if( s == null || s == "" ) {
			s = "空格 500";
			//this.checkBox按键阻塞.Checked = false;
		}
		KeyList = "";
		LastButton = null;
		
		string[] cu = s.Split( ' ' );
		int t = int.Parse( cu[1] );
		textBoxTick.Text = (t/1000f).ToString( "0.00" );
		
		foreach( Control c in this.panel1.Controls ) {
			if( cu[0] == c.Text ) {
				c.BackColor = Color.CornflowerBlue;
				LastButton = (Button)c;
			}
			else {
				c.BackColor = Color.Gainsboro;
			}
		}
		isOK = false;
		
		//注意,直接设置Visible不能使窗体获得焦点
		//this.Visible = true;
		this.ShowDialog();
		
		while( this.Visible ) {
			System.Windows.Forms.Application.DoEvents();
		}
		if( isOK ) {
			return KeyList;
			
		}
		return null;
	}
	
	void Button确定Click(object sender, EventArgs e)
	{
		if( LastButton == null ) {
			MessageBox.Show( "请选中一个您要检测的按钮!" );
			return;
		}
		
		try {
			float t = float.Parse( textBoxTick.Text );
			int n = (int)(t * 1000);
			if( n <= 10 ) {
				n = 10;
			}
			KeyList = LastButton.Text + " " + (n);
		}
		catch {
			MessageBox.Show( "时间间隔数据格式不正确: " + textBoxTick.Text );
			return;
		}
		
		isOK = true;
		this.Visible = false;
	}
	
	void ButtonKClick(object sender, EventArgs e)
	{
		if( LastButton != null ) {
			LastButton.BackColor = Color.Gainsboro;
		}
		LastButton = (Button)sender;
		LastButton.BackColor = Color.CornflowerBlue;
	}
	
	void KeyboardFormFormClosing(object sender, FormClosingEventArgs e)
	{
		e.Cancel = true;
		this.Visible = false;
	}
}
}




