﻿

namespace n_KeyboardForm
{
	partial class KeyboardForm
	{
		/// <summary>
		/// Designer variable used to keep track of non-visual components.
		/// </summary>
		private System.ComponentModel.IContainer components = null;
		
		/// <summary>
		/// Disposes resources used by the form.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing) {
				if (components != null) {
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}
		
		/// <summary>
		/// This method is required for Windows Forms designer support.
		/// Do not change the method contents inside the source code editor. The Forms designer might
		/// not be able to load this method if it was changed manually.
		/// </summary>
		private void InitializeComponent()
		{
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(KeyboardForm));
			this.button确定 = new System.Windows.Forms.Button();
			this.button1 = new System.Windows.Forms.Button();
			this.panel1 = new System.Windows.Forms.Panel();
			this.button56 = new System.Windows.Forms.Button();
			this.button53 = new System.Windows.Forms.Button();
			this.button54 = new System.Windows.Forms.Button();
			this.button55 = new System.Windows.Forms.Button();
			this.button50 = new System.Windows.Forms.Button();
			this.button51 = new System.Windows.Forms.Button();
			this.button39 = new System.Windows.Forms.Button();
			this.button40 = new System.Windows.Forms.Button();
			this.button41 = new System.Windows.Forms.Button();
			this.button42 = new System.Windows.Forms.Button();
			this.button43 = new System.Windows.Forms.Button();
			this.button44 = new System.Windows.Forms.Button();
			this.button45 = new System.Windows.Forms.Button();
			this.button46 = new System.Windows.Forms.Button();
			this.button47 = new System.Windows.Forms.Button();
			this.button48 = new System.Windows.Forms.Button();
			this.button49 = new System.Windows.Forms.Button();
			this.button38 = new System.Windows.Forms.Button();
			this.button37 = new System.Windows.Forms.Button();
			this.button34 = new System.Windows.Forms.Button();
			this.button35 = new System.Windows.Forms.Button();
			this.button36 = new System.Windows.Forms.Button();
			this.button24 = new System.Windows.Forms.Button();
			this.button25 = new System.Windows.Forms.Button();
			this.button26 = new System.Windows.Forms.Button();
			this.button27 = new System.Windows.Forms.Button();
			this.button28 = new System.Windows.Forms.Button();
			this.button29 = new System.Windows.Forms.Button();
			this.button30 = new System.Windows.Forms.Button();
			this.button31 = new System.Windows.Forms.Button();
			this.button32 = new System.Windows.Forms.Button();
			this.button33 = new System.Windows.Forms.Button();
			this.button23 = new System.Windows.Forms.Button();
			this.button22 = new System.Windows.Forms.Button();
			this.button21 = new System.Windows.Forms.Button();
			this.button20 = new System.Windows.Forms.Button();
			this.button19 = new System.Windows.Forms.Button();
			this.button18 = new System.Windows.Forms.Button();
			this.button17 = new System.Windows.Forms.Button();
			this.button16 = new System.Windows.Forms.Button();
			this.button15 = new System.Windows.Forms.Button();
			this.button14 = new System.Windows.Forms.Button();
			this.button13 = new System.Windows.Forms.Button();
			this.button12 = new System.Windows.Forms.Button();
			this.button11 = new System.Windows.Forms.Button();
			this.button10 = new System.Windows.Forms.Button();
			this.button9 = new System.Windows.Forms.Button();
			this.button8 = new System.Windows.Forms.Button();
			this.button7 = new System.Windows.Forms.Button();
			this.button6 = new System.Windows.Forms.Button();
			this.button5 = new System.Windows.Forms.Button();
			this.button4 = new System.Windows.Forms.Button();
			this.button3 = new System.Windows.Forms.Button();
			this.button2 = new System.Windows.Forms.Button();
			this.label1 = new System.Windows.Forms.Label();
			this.textBoxTick = new System.Windows.Forms.TextBox();
			this.label2 = new System.Windows.Forms.Label();
			this.label3 = new System.Windows.Forms.Label();
			this.panel1.SuspendLayout();
			this.SuspendLayout();
			// 
			// button确定
			// 
			this.button确定.BackColor = System.Drawing.Color.DarkSeaGreen;
			resources.ApplyResources(this.button确定, "button确定");
			this.button确定.ForeColor = System.Drawing.Color.Black;
			this.button确定.Name = "button确定";
			this.button确定.UseVisualStyleBackColor = false;
			this.button确定.Click += new System.EventHandler(this.Button确定Click);
			// 
			// button1
			// 
			this.button1.BackColor = System.Drawing.Color.Gainsboro;
			resources.ApplyResources(this.button1, "button1");
			this.button1.ForeColor = System.Drawing.Color.Black;
			this.button1.Name = "button1";
			this.button1.UseVisualStyleBackColor = false;
			this.button1.Click += new System.EventHandler(this.ButtonKClick);
			// 
			// panel1
			// 
			this.panel1.Controls.Add(this.button56);
			this.panel1.Controls.Add(this.button53);
			this.panel1.Controls.Add(this.button54);
			this.panel1.Controls.Add(this.button55);
			this.panel1.Controls.Add(this.button50);
			this.panel1.Controls.Add(this.button51);
			this.panel1.Controls.Add(this.button39);
			this.panel1.Controls.Add(this.button40);
			this.panel1.Controls.Add(this.button41);
			this.panel1.Controls.Add(this.button42);
			this.panel1.Controls.Add(this.button43);
			this.panel1.Controls.Add(this.button44);
			this.panel1.Controls.Add(this.button45);
			this.panel1.Controls.Add(this.button46);
			this.panel1.Controls.Add(this.button47);
			this.panel1.Controls.Add(this.button48);
			this.panel1.Controls.Add(this.button49);
			this.panel1.Controls.Add(this.button38);
			this.panel1.Controls.Add(this.button37);
			this.panel1.Controls.Add(this.button34);
			this.panel1.Controls.Add(this.button35);
			this.panel1.Controls.Add(this.button36);
			this.panel1.Controls.Add(this.button24);
			this.panel1.Controls.Add(this.button25);
			this.panel1.Controls.Add(this.button26);
			this.panel1.Controls.Add(this.button27);
			this.panel1.Controls.Add(this.button28);
			this.panel1.Controls.Add(this.button29);
			this.panel1.Controls.Add(this.button30);
			this.panel1.Controls.Add(this.button31);
			this.panel1.Controls.Add(this.button32);
			this.panel1.Controls.Add(this.button33);
			this.panel1.Controls.Add(this.button23);
			this.panel1.Controls.Add(this.button22);
			this.panel1.Controls.Add(this.button21);
			this.panel1.Controls.Add(this.button20);
			this.panel1.Controls.Add(this.button19);
			this.panel1.Controls.Add(this.button18);
			this.panel1.Controls.Add(this.button17);
			this.panel1.Controls.Add(this.button16);
			this.panel1.Controls.Add(this.button15);
			this.panel1.Controls.Add(this.button14);
			this.panel1.Controls.Add(this.button13);
			this.panel1.Controls.Add(this.button12);
			this.panel1.Controls.Add(this.button11);
			this.panel1.Controls.Add(this.button10);
			this.panel1.Controls.Add(this.button9);
			this.panel1.Controls.Add(this.button8);
			this.panel1.Controls.Add(this.button7);
			this.panel1.Controls.Add(this.button6);
			this.panel1.Controls.Add(this.button5);
			this.panel1.Controls.Add(this.button4);
			this.panel1.Controls.Add(this.button3);
			this.panel1.Controls.Add(this.button2);
			this.panel1.Controls.Add(this.button1);
			resources.ApplyResources(this.panel1, "panel1");
			this.panel1.Name = "panel1";
			// 
			// button56
			// 
			this.button56.BackColor = System.Drawing.Color.Gainsboro;
			resources.ApplyResources(this.button56, "button56");
			this.button56.ForeColor = System.Drawing.Color.Black;
			this.button56.Name = "button56";
			this.button56.UseVisualStyleBackColor = false;
			this.button56.Click += new System.EventHandler(this.ButtonKClick);
			// 
			// button53
			// 
			this.button53.BackColor = System.Drawing.Color.Gainsboro;
			resources.ApplyResources(this.button53, "button53");
			this.button53.ForeColor = System.Drawing.Color.Black;
			this.button53.Name = "button53";
			this.button53.UseVisualStyleBackColor = false;
			this.button53.Click += new System.EventHandler(this.ButtonKClick);
			// 
			// button54
			// 
			this.button54.BackColor = System.Drawing.Color.Gainsboro;
			resources.ApplyResources(this.button54, "button54");
			this.button54.ForeColor = System.Drawing.Color.Black;
			this.button54.Name = "button54";
			this.button54.UseVisualStyleBackColor = false;
			this.button54.Click += new System.EventHandler(this.ButtonKClick);
			// 
			// button55
			// 
			this.button55.BackColor = System.Drawing.Color.Gainsboro;
			resources.ApplyResources(this.button55, "button55");
			this.button55.ForeColor = System.Drawing.Color.Black;
			this.button55.Name = "button55";
			this.button55.UseVisualStyleBackColor = false;
			this.button55.Click += new System.EventHandler(this.ButtonKClick);
			// 
			// button50
			// 
			this.button50.BackColor = System.Drawing.Color.Gainsboro;
			resources.ApplyResources(this.button50, "button50");
			this.button50.ForeColor = System.Drawing.Color.Black;
			this.button50.Name = "button50";
			this.button50.UseVisualStyleBackColor = false;
			this.button50.Click += new System.EventHandler(this.ButtonKClick);
			// 
			// button51
			// 
			this.button51.BackColor = System.Drawing.Color.Gainsboro;
			resources.ApplyResources(this.button51, "button51");
			this.button51.ForeColor = System.Drawing.Color.Black;
			this.button51.Name = "button51";
			this.button51.UseVisualStyleBackColor = false;
			this.button51.Click += new System.EventHandler(this.ButtonKClick);
			// 
			// button39
			// 
			this.button39.BackColor = System.Drawing.Color.Gainsboro;
			resources.ApplyResources(this.button39, "button39");
			this.button39.ForeColor = System.Drawing.Color.Black;
			this.button39.Name = "button39";
			this.button39.UseVisualStyleBackColor = false;
			this.button39.Click += new System.EventHandler(this.ButtonKClick);
			// 
			// button40
			// 
			this.button40.BackColor = System.Drawing.Color.Gainsboro;
			resources.ApplyResources(this.button40, "button40");
			this.button40.ForeColor = System.Drawing.Color.Black;
			this.button40.Name = "button40";
			this.button40.UseVisualStyleBackColor = false;
			this.button40.Click += new System.EventHandler(this.ButtonKClick);
			// 
			// button41
			// 
			this.button41.BackColor = System.Drawing.Color.Gainsboro;
			resources.ApplyResources(this.button41, "button41");
			this.button41.ForeColor = System.Drawing.Color.Black;
			this.button41.Name = "button41";
			this.button41.UseVisualStyleBackColor = false;
			this.button41.Click += new System.EventHandler(this.ButtonKClick);
			// 
			// button42
			// 
			this.button42.BackColor = System.Drawing.Color.Gainsboro;
			resources.ApplyResources(this.button42, "button42");
			this.button42.ForeColor = System.Drawing.Color.Black;
			this.button42.Name = "button42";
			this.button42.UseVisualStyleBackColor = false;
			this.button42.Click += new System.EventHandler(this.ButtonKClick);
			// 
			// button43
			// 
			this.button43.BackColor = System.Drawing.Color.Gainsboro;
			resources.ApplyResources(this.button43, "button43");
			this.button43.ForeColor = System.Drawing.Color.Black;
			this.button43.Name = "button43";
			this.button43.UseVisualStyleBackColor = false;
			this.button43.Click += new System.EventHandler(this.ButtonKClick);
			// 
			// button44
			// 
			this.button44.BackColor = System.Drawing.Color.Gainsboro;
			resources.ApplyResources(this.button44, "button44");
			this.button44.ForeColor = System.Drawing.Color.Black;
			this.button44.Name = "button44";
			this.button44.UseVisualStyleBackColor = false;
			this.button44.Click += new System.EventHandler(this.ButtonKClick);
			// 
			// button45
			// 
			this.button45.BackColor = System.Drawing.Color.Gainsboro;
			resources.ApplyResources(this.button45, "button45");
			this.button45.ForeColor = System.Drawing.Color.Black;
			this.button45.Name = "button45";
			this.button45.UseVisualStyleBackColor = false;
			this.button45.Click += new System.EventHandler(this.ButtonKClick);
			// 
			// button46
			// 
			this.button46.BackColor = System.Drawing.Color.Gainsboro;
			resources.ApplyResources(this.button46, "button46");
			this.button46.ForeColor = System.Drawing.Color.Black;
			this.button46.Name = "button46";
			this.button46.UseVisualStyleBackColor = false;
			this.button46.Click += new System.EventHandler(this.ButtonKClick);
			// 
			// button47
			// 
			this.button47.BackColor = System.Drawing.Color.Gainsboro;
			resources.ApplyResources(this.button47, "button47");
			this.button47.ForeColor = System.Drawing.Color.Black;
			this.button47.Name = "button47";
			this.button47.UseVisualStyleBackColor = false;
			this.button47.Click += new System.EventHandler(this.ButtonKClick);
			// 
			// button48
			// 
			this.button48.BackColor = System.Drawing.Color.Gainsboro;
			resources.ApplyResources(this.button48, "button48");
			this.button48.ForeColor = System.Drawing.Color.Black;
			this.button48.Name = "button48";
			this.button48.UseVisualStyleBackColor = false;
			this.button48.Click += new System.EventHandler(this.ButtonKClick);
			// 
			// button49
			// 
			this.button49.BackColor = System.Drawing.Color.Gainsboro;
			resources.ApplyResources(this.button49, "button49");
			this.button49.ForeColor = System.Drawing.Color.Black;
			this.button49.Name = "button49";
			this.button49.UseVisualStyleBackColor = false;
			this.button49.Click += new System.EventHandler(this.ButtonKClick);
			// 
			// button38
			// 
			this.button38.BackColor = System.Drawing.Color.Gainsboro;
			resources.ApplyResources(this.button38, "button38");
			this.button38.ForeColor = System.Drawing.Color.Black;
			this.button38.Name = "button38";
			this.button38.UseVisualStyleBackColor = false;
			this.button38.Click += new System.EventHandler(this.ButtonKClick);
			// 
			// button37
			// 
			this.button37.BackColor = System.Drawing.Color.Gainsboro;
			resources.ApplyResources(this.button37, "button37");
			this.button37.ForeColor = System.Drawing.Color.Black;
			this.button37.Name = "button37";
			this.button37.UseVisualStyleBackColor = false;
			this.button37.Click += new System.EventHandler(this.ButtonKClick);
			// 
			// button34
			// 
			this.button34.BackColor = System.Drawing.Color.Gainsboro;
			resources.ApplyResources(this.button34, "button34");
			this.button34.ForeColor = System.Drawing.Color.Black;
			this.button34.Name = "button34";
			this.button34.UseVisualStyleBackColor = false;
			this.button34.Click += new System.EventHandler(this.ButtonKClick);
			// 
			// button35
			// 
			this.button35.BackColor = System.Drawing.Color.Gainsboro;
			resources.ApplyResources(this.button35, "button35");
			this.button35.ForeColor = System.Drawing.Color.Black;
			this.button35.Name = "button35";
			this.button35.UseVisualStyleBackColor = false;
			this.button35.Click += new System.EventHandler(this.ButtonKClick);
			// 
			// button36
			// 
			this.button36.BackColor = System.Drawing.Color.Gainsboro;
			resources.ApplyResources(this.button36, "button36");
			this.button36.ForeColor = System.Drawing.Color.Black;
			this.button36.Name = "button36";
			this.button36.UseVisualStyleBackColor = false;
			this.button36.Click += new System.EventHandler(this.ButtonKClick);
			// 
			// button24
			// 
			this.button24.BackColor = System.Drawing.Color.Gainsboro;
			resources.ApplyResources(this.button24, "button24");
			this.button24.ForeColor = System.Drawing.Color.Black;
			this.button24.Name = "button24";
			this.button24.UseVisualStyleBackColor = false;
			this.button24.Click += new System.EventHandler(this.ButtonKClick);
			// 
			// button25
			// 
			this.button25.BackColor = System.Drawing.Color.Gainsboro;
			resources.ApplyResources(this.button25, "button25");
			this.button25.ForeColor = System.Drawing.Color.Black;
			this.button25.Name = "button25";
			this.button25.UseVisualStyleBackColor = false;
			this.button25.Click += new System.EventHandler(this.ButtonKClick);
			// 
			// button26
			// 
			this.button26.BackColor = System.Drawing.Color.Gainsboro;
			resources.ApplyResources(this.button26, "button26");
			this.button26.ForeColor = System.Drawing.Color.Black;
			this.button26.Name = "button26";
			this.button26.UseVisualStyleBackColor = false;
			this.button26.Click += new System.EventHandler(this.ButtonKClick);
			// 
			// button27
			// 
			this.button27.BackColor = System.Drawing.Color.Gainsboro;
			resources.ApplyResources(this.button27, "button27");
			this.button27.ForeColor = System.Drawing.Color.Black;
			this.button27.Name = "button27";
			this.button27.UseVisualStyleBackColor = false;
			this.button27.Click += new System.EventHandler(this.ButtonKClick);
			// 
			// button28
			// 
			this.button28.BackColor = System.Drawing.Color.Gainsboro;
			resources.ApplyResources(this.button28, "button28");
			this.button28.ForeColor = System.Drawing.Color.Black;
			this.button28.Name = "button28";
			this.button28.UseVisualStyleBackColor = false;
			this.button28.Click += new System.EventHandler(this.ButtonKClick);
			// 
			// button29
			// 
			this.button29.BackColor = System.Drawing.Color.Gainsboro;
			resources.ApplyResources(this.button29, "button29");
			this.button29.ForeColor = System.Drawing.Color.Black;
			this.button29.Name = "button29";
			this.button29.UseVisualStyleBackColor = false;
			this.button29.Click += new System.EventHandler(this.ButtonKClick);
			// 
			// button30
			// 
			this.button30.BackColor = System.Drawing.Color.Gainsboro;
			resources.ApplyResources(this.button30, "button30");
			this.button30.ForeColor = System.Drawing.Color.Black;
			this.button30.Name = "button30";
			this.button30.UseVisualStyleBackColor = false;
			this.button30.Click += new System.EventHandler(this.ButtonKClick);
			// 
			// button31
			// 
			this.button31.BackColor = System.Drawing.Color.Gainsboro;
			resources.ApplyResources(this.button31, "button31");
			this.button31.ForeColor = System.Drawing.Color.Black;
			this.button31.Name = "button31";
			this.button31.UseVisualStyleBackColor = false;
			this.button31.Click += new System.EventHandler(this.ButtonKClick);
			// 
			// button32
			// 
			this.button32.BackColor = System.Drawing.Color.Gainsboro;
			resources.ApplyResources(this.button32, "button32");
			this.button32.ForeColor = System.Drawing.Color.Black;
			this.button32.Name = "button32";
			this.button32.UseVisualStyleBackColor = false;
			this.button32.Click += new System.EventHandler(this.ButtonKClick);
			// 
			// button33
			// 
			this.button33.BackColor = System.Drawing.Color.Gainsboro;
			resources.ApplyResources(this.button33, "button33");
			this.button33.ForeColor = System.Drawing.Color.Black;
			this.button33.Name = "button33";
			this.button33.UseVisualStyleBackColor = false;
			this.button33.Click += new System.EventHandler(this.ButtonKClick);
			// 
			// button23
			// 
			this.button23.BackColor = System.Drawing.Color.Gainsboro;
			resources.ApplyResources(this.button23, "button23");
			this.button23.ForeColor = System.Drawing.Color.Black;
			this.button23.Name = "button23";
			this.button23.UseVisualStyleBackColor = false;
			this.button23.Click += new System.EventHandler(this.ButtonKClick);
			// 
			// button22
			// 
			this.button22.BackColor = System.Drawing.Color.Gainsboro;
			resources.ApplyResources(this.button22, "button22");
			this.button22.ForeColor = System.Drawing.Color.Black;
			this.button22.Name = "button22";
			this.button22.UseVisualStyleBackColor = false;
			this.button22.Click += new System.EventHandler(this.ButtonKClick);
			// 
			// button21
			// 
			this.button21.BackColor = System.Drawing.Color.Gainsboro;
			resources.ApplyResources(this.button21, "button21");
			this.button21.ForeColor = System.Drawing.Color.Black;
			this.button21.Name = "button21";
			this.button21.UseVisualStyleBackColor = false;
			this.button21.Click += new System.EventHandler(this.ButtonKClick);
			// 
			// button20
			// 
			this.button20.BackColor = System.Drawing.Color.Gainsboro;
			resources.ApplyResources(this.button20, "button20");
			this.button20.ForeColor = System.Drawing.Color.Black;
			this.button20.Name = "button20";
			this.button20.UseVisualStyleBackColor = false;
			this.button20.Click += new System.EventHandler(this.ButtonKClick);
			// 
			// button19
			// 
			this.button19.BackColor = System.Drawing.Color.Gainsboro;
			resources.ApplyResources(this.button19, "button19");
			this.button19.ForeColor = System.Drawing.Color.Black;
			this.button19.Name = "button19";
			this.button19.UseVisualStyleBackColor = false;
			this.button19.Click += new System.EventHandler(this.ButtonKClick);
			// 
			// button18
			// 
			this.button18.BackColor = System.Drawing.Color.Gainsboro;
			resources.ApplyResources(this.button18, "button18");
			this.button18.ForeColor = System.Drawing.Color.Black;
			this.button18.Name = "button18";
			this.button18.UseVisualStyleBackColor = false;
			this.button18.Click += new System.EventHandler(this.ButtonKClick);
			// 
			// button17
			// 
			this.button17.BackColor = System.Drawing.Color.Gainsboro;
			resources.ApplyResources(this.button17, "button17");
			this.button17.ForeColor = System.Drawing.Color.Black;
			this.button17.Name = "button17";
			this.button17.UseVisualStyleBackColor = false;
			this.button17.Click += new System.EventHandler(this.ButtonKClick);
			// 
			// button16
			// 
			this.button16.BackColor = System.Drawing.Color.Gainsboro;
			resources.ApplyResources(this.button16, "button16");
			this.button16.ForeColor = System.Drawing.Color.Black;
			this.button16.Name = "button16";
			this.button16.UseVisualStyleBackColor = false;
			this.button16.Click += new System.EventHandler(this.ButtonKClick);
			// 
			// button15
			// 
			this.button15.BackColor = System.Drawing.Color.Gainsboro;
			resources.ApplyResources(this.button15, "button15");
			this.button15.ForeColor = System.Drawing.Color.Black;
			this.button15.Name = "button15";
			this.button15.UseVisualStyleBackColor = false;
			this.button15.Click += new System.EventHandler(this.ButtonKClick);
			// 
			// button14
			// 
			this.button14.BackColor = System.Drawing.Color.Gainsboro;
			resources.ApplyResources(this.button14, "button14");
			this.button14.ForeColor = System.Drawing.Color.Black;
			this.button14.Name = "button14";
			this.button14.UseVisualStyleBackColor = false;
			this.button14.Click += new System.EventHandler(this.ButtonKClick);
			// 
			// button13
			// 
			this.button13.BackColor = System.Drawing.Color.Gainsboro;
			resources.ApplyResources(this.button13, "button13");
			this.button13.ForeColor = System.Drawing.Color.Black;
			this.button13.Name = "button13";
			this.button13.UseVisualStyleBackColor = false;
			this.button13.Click += new System.EventHandler(this.ButtonKClick);
			// 
			// button12
			// 
			this.button12.BackColor = System.Drawing.Color.Gainsboro;
			resources.ApplyResources(this.button12, "button12");
			this.button12.ForeColor = System.Drawing.Color.Black;
			this.button12.Name = "button12";
			this.button12.UseVisualStyleBackColor = false;
			this.button12.Click += new System.EventHandler(this.ButtonKClick);
			// 
			// button11
			// 
			this.button11.BackColor = System.Drawing.Color.Gainsboro;
			resources.ApplyResources(this.button11, "button11");
			this.button11.ForeColor = System.Drawing.Color.Black;
			this.button11.Name = "button11";
			this.button11.UseVisualStyleBackColor = false;
			this.button11.Click += new System.EventHandler(this.ButtonKClick);
			// 
			// button10
			// 
			this.button10.BackColor = System.Drawing.Color.Gainsboro;
			resources.ApplyResources(this.button10, "button10");
			this.button10.ForeColor = System.Drawing.Color.Black;
			this.button10.Name = "button10";
			this.button10.UseVisualStyleBackColor = false;
			this.button10.Click += new System.EventHandler(this.ButtonKClick);
			// 
			// button9
			// 
			this.button9.BackColor = System.Drawing.Color.Gainsboro;
			resources.ApplyResources(this.button9, "button9");
			this.button9.ForeColor = System.Drawing.Color.Black;
			this.button9.Name = "button9";
			this.button9.UseVisualStyleBackColor = false;
			this.button9.Click += new System.EventHandler(this.ButtonKClick);
			// 
			// button8
			// 
			this.button8.BackColor = System.Drawing.Color.Gainsboro;
			resources.ApplyResources(this.button8, "button8");
			this.button8.ForeColor = System.Drawing.Color.Black;
			this.button8.Name = "button8";
			this.button8.UseVisualStyleBackColor = false;
			this.button8.Click += new System.EventHandler(this.ButtonKClick);
			// 
			// button7
			// 
			this.button7.BackColor = System.Drawing.Color.Gainsboro;
			resources.ApplyResources(this.button7, "button7");
			this.button7.ForeColor = System.Drawing.Color.Black;
			this.button7.Name = "button7";
			this.button7.UseVisualStyleBackColor = false;
			this.button7.Click += new System.EventHandler(this.ButtonKClick);
			// 
			// button6
			// 
			this.button6.BackColor = System.Drawing.Color.Gainsboro;
			resources.ApplyResources(this.button6, "button6");
			this.button6.ForeColor = System.Drawing.Color.Black;
			this.button6.Name = "button6";
			this.button6.UseVisualStyleBackColor = false;
			this.button6.Click += new System.EventHandler(this.ButtonKClick);
			// 
			// button5
			// 
			this.button5.BackColor = System.Drawing.Color.Gainsboro;
			resources.ApplyResources(this.button5, "button5");
			this.button5.ForeColor = System.Drawing.Color.Black;
			this.button5.Name = "button5";
			this.button5.UseVisualStyleBackColor = false;
			this.button5.Click += new System.EventHandler(this.ButtonKClick);
			// 
			// button4
			// 
			this.button4.BackColor = System.Drawing.Color.Gainsboro;
			resources.ApplyResources(this.button4, "button4");
			this.button4.ForeColor = System.Drawing.Color.Black;
			this.button4.Name = "button4";
			this.button4.UseVisualStyleBackColor = false;
			this.button4.Click += new System.EventHandler(this.ButtonKClick);
			// 
			// button3
			// 
			this.button3.BackColor = System.Drawing.Color.Gainsboro;
			resources.ApplyResources(this.button3, "button3");
			this.button3.ForeColor = System.Drawing.Color.Black;
			this.button3.Name = "button3";
			this.button3.UseVisualStyleBackColor = false;
			this.button3.Click += new System.EventHandler(this.ButtonKClick);
			// 
			// button2
			// 
			this.button2.BackColor = System.Drawing.Color.Gainsboro;
			resources.ApplyResources(this.button2, "button2");
			this.button2.ForeColor = System.Drawing.Color.Black;
			this.button2.Name = "button2";
			this.button2.UseVisualStyleBackColor = false;
			this.button2.Click += new System.EventHandler(this.ButtonKClick);
			// 
			// label1
			// 
			resources.ApplyResources(this.label1, "label1");
			this.label1.ForeColor = System.Drawing.Color.Gray;
			this.label1.Name = "label1";
			// 
			// textBoxTick
			// 
			this.textBoxTick.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(169)))), ((int)(((byte)(206)))), ((int)(((byte)(231)))));
			resources.ApplyResources(this.textBoxTick, "textBoxTick");
			this.textBoxTick.Name = "textBoxTick";
			// 
			// label2
			// 
			resources.ApplyResources(this.label2, "label2");
			this.label2.Name = "label2";
			// 
			// label3
			// 
			resources.ApplyResources(this.label3, "label3");
			this.label3.Name = "label3";
			// 
			// KeyboardForm
			// 
			resources.ApplyResources(this, "$this");
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.BackColor = System.Drawing.Color.WhiteSmoke;
			this.Controls.Add(this.textBoxTick);
			this.Controls.Add(this.label3);
			this.Controls.Add(this.label2);
			this.Controls.Add(this.label1);
			this.Controls.Add(this.panel1);
			this.Controls.Add(this.button确定);
			this.Name = "KeyboardForm";
			this.ShowInTaskbar = false;
			this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.KeyboardFormFormClosing);
			this.panel1.ResumeLayout(false);
			this.ResumeLayout(false);
			this.PerformLayout();
		}
		private System.Windows.Forms.Label label3;
		private System.Windows.Forms.Label label2;
		private System.Windows.Forms.TextBox textBoxTick;
		private System.Windows.Forms.Label label1;
		private System.Windows.Forms.Button button确定;
		private System.Windows.Forms.Button button1;
		private System.Windows.Forms.Panel panel1;
		private System.Windows.Forms.Button button39;
		private System.Windows.Forms.Button button40;
		private System.Windows.Forms.Button button41;
		private System.Windows.Forms.Button button42;
		private System.Windows.Forms.Button button43;
		private System.Windows.Forms.Button button44;
		private System.Windows.Forms.Button button45;
		private System.Windows.Forms.Button button46;
		private System.Windows.Forms.Button button47;
		private System.Windows.Forms.Button button48;
		private System.Windows.Forms.Button button49;
		private System.Windows.Forms.Button button38;
		private System.Windows.Forms.Button button37;
		private System.Windows.Forms.Button button34;
		private System.Windows.Forms.Button button35;
		private System.Windows.Forms.Button button36;
		private System.Windows.Forms.Button button24;
		private System.Windows.Forms.Button button25;
		private System.Windows.Forms.Button button26;
		private System.Windows.Forms.Button button27;
		private System.Windows.Forms.Button button28;
		private System.Windows.Forms.Button button29;
		private System.Windows.Forms.Button button30;
		private System.Windows.Forms.Button button31;
		private System.Windows.Forms.Button button32;
		private System.Windows.Forms.Button button33;
		private System.Windows.Forms.Button button23;
		private System.Windows.Forms.Button button22;
		private System.Windows.Forms.Button button21;
		private System.Windows.Forms.Button button20;
		private System.Windows.Forms.Button button19;
		private System.Windows.Forms.Button button18;
		private System.Windows.Forms.Button button17;
		private System.Windows.Forms.Button button16;
		private System.Windows.Forms.Button button15;
		private System.Windows.Forms.Button button14;
		private System.Windows.Forms.Button button13;
		private System.Windows.Forms.Button button12;
		private System.Windows.Forms.Button button11;
		private System.Windows.Forms.Button button10;
		private System.Windows.Forms.Button button9;
		private System.Windows.Forms.Button button8;
		private System.Windows.Forms.Button button7;
		private System.Windows.Forms.Button button6;
		private System.Windows.Forms.Button button5;
		private System.Windows.Forms.Button button4;
		private System.Windows.Forms.Button button3;
		private System.Windows.Forms.Button button2;
		private System.Windows.Forms.Button button50;
		private System.Windows.Forms.Button button51;
		private System.Windows.Forms.Button button56;
		private System.Windows.Forms.Button button53;
		private System.Windows.Forms.Button button54;
		private System.Windows.Forms.Button button55;
	}
}
