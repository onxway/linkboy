﻿
namespace n_SimModule
{
using System;
using System.Drawing;
using System.Drawing.Drawing2D;
using n_HardModule;
using n_SimulateObj;
using n_SG;
using n_RA;
using n_SOblect;

public class SimModule : SObject
{
	public Loc[] LocList;
	
	public delegate void D_LocChanged( SimModule sm, float r );
	public D_LocChanged LocChanged;
	
	float CSpeed;
	
	//初始化
	public static void SimModuleInit()
	{
		//...
	}
	
	//构造函数
	public SimModule( HardModule o, int idx ) : base( o, idx )
	{
		isMotor = false;
		if( Owner != null &&
			Owner.SimList != null &&
			Owner.SimList.Length >= 1 &&
			Owner.SimList[0] != null &&
			Owner.SimList[0].SimType == 6 ) {
			isMotor = true;
		}
		isXunJi = false;
		if( Owner.SimList[0] != null && Owner.SimList[0].SimType == 102 ) {
			isXunJi = true;
		}
		
		if( isMotor ) {
			LocList = new Loc[20];
			for( int i = 0; i < LocList.Length; ++i ) {
				LocList[i] = new Loc();
			}
		}
		
		Width = Owner.SourceWidth;
		Height = Owner.SourceHeight;
		
		CSpeed = 0;
	}
	
	//复位仿真参数
	public override void ResetSimVar()
	{
		CSpeed = 0;
	}
	
	//运行一步
	public override void RunStep( Bitmap Back, float DotNumber, float ix, float iy )
	{
		//处理循迹传感器环境交互
		if( isXunJi ) {
			
			float mx = Owner.SimList[0].MidX;
			float my = Owner.SimList[0].MidY;
			
			float nx = 0;
			float ny = 0;
			RA.GetRolXY( out nx, out ny, mx, my, Angle );
			
			float x = MidX + nx;
			float y = MidY + ny;
			
			int xx = (int)((x - ix + DotNumber/2) / DotNumber);
			int yy = (int)((y - iy + DotNumber/2) / DotNumber);
			
			lock( Back ) {
				if( xx > 0 && xx < Back.Width && yy > 0 && yy < Back.Height ) {
					Color c = Back.GetPixel( xx, yy );
					if( c.A != 0 && (c.R + c.G + c.B) / 3 < 128 ) {
						Owner.DataList[0] = Owner.SimList[0].Value2;
					}
					else {
						Owner.DataList[0] = Owner.SimList[0].Value3;
					}
				}
				else {
					Owner.DataList[0] = Owner.SimList[0].Value3;
				}
			}
		}
		//处理马达运行
		if( isMotor ) {
			
			//获取马达功率
			int Speed = Owner.DataList[1];
			if( Speed <= StartPower ) {
				Speed = 0;
			}
			Speed = (Speed - StartPower);
			
			Speed = Speed * 100 / SpeedDown;
			
			if( Owner.DataList[0] == 0 ) {
				Speed = 0;
			}
			else if( Owner.DataList[0] == 1 ) {
				//...
			}
			else {
				Speed = -Speed;
			}
			//判断是否需要变换方向
			if( SwapPin ) {
				Speed = -Speed;
			}
			
			//进行速度变化惯性处理
			float a1 = 110f;
			float ad = 5f;
			if( CSpeed < Speed ) {
				CSpeed += (a1 - Inertia) / ad;
				if( CSpeed > Speed ) {
					CSpeed = Speed;
				}
			}
			if( CSpeed > Speed ) {
				CSpeed -= (a1 - Inertia) / ad;
				if( CSpeed < Speed ) {
					CSpeed = Speed;
				}
			}
			if( CSpeed != 0 ) {
				if( LocChanged != null ) {
					LocChanged( this, CSpeed / 0.2f );
				}
			}
		}
	}
	
	//绘制
	public override void Draw( Graphics g )
	{
		if( !Visible ) {
			return;
		}
		//摄像机按照中心位置进行旋转
		GraphicsState gs = g.Save();
		SG.Rotate( MidX, MidY, -Angle );
		
		g.DrawImage( Owner.SourceImage, SX, SY, Width, Height );
		
		if( Selected ) {
			g.DrawRectangle( SelectPen, SX - 1, SY - 1, Width + 2, Height + 2 );
		}
		else if( isMousePress || isMouseOn ) {
			g.DrawRectangle( Pens.SlateBlue, SX - 1, SY - 1, Width + 2, Height + 2 );
		}
		else {
			
		}
		
		if( MouseNear || isRolPress ) {
			if( isRolPress ) {
				g.FillEllipse( Brushes.OrangeRed, MidX - RolWidth/2, MidY - Height/2 - RolWidth, RolWidth, RolWidth );
				g.DrawEllipse( Pens.Red, MidX - RolWidth/2, MidY - Height/2 - RolWidth, RolWidth, RolWidth );
			}
			else if( isRolOn ) {
				g.FillEllipse( Brushes.SlateBlue, MidX - RolWidth/2, MidY - Height/2 - RolWidth, RolWidth, RolWidth );
				g.DrawEllipse( Pens.Blue, MidX - RolWidth/2, MidY - Height/2 - RolWidth, RolWidth, RolWidth );
			}
			else {
				g.FillEllipse( Brushes.DarkGray, MidX - RolWidth/2, MidY - Height/2 - RolWidth, RolWidth, RolWidth );
			}
		}
		
		if( G.SimulateMode && Owner.SimList.Length > 0 && Owner.SimList[0] != null ) {
			
			if( Owner.SimList[0].SimType == 100 ) {
				
				for( int i = 0; i < Owner.SimList.Length; ++i ) {
					if( Owner.SimList[i] != null ) {
						//SimList[i].SetColor( DataList[3] );
						Owner.SimList[i].Draw( g, (int)MidX, (int)MidY );
					}
				}
				g.Restore( gs );
			}
			else {
				for( int i = 0; i < Owner.SimList.Length; ++i ) {
					if( Owner.SimList[i] != null ) {
						if( Owner.SimList[i].SimType != SimulateObj.LED || Owner.DataList[Owner.SimList[i].Value1] != 0 ) {
							if( Owner.SimList[i].SimType == SimulateObj.LED ) {
								Owner.SimList[i].SetColor( Owner.DataList[3+i] );
							}
							Owner.SimList[i].Draw( g, (int)MidX, (int)MidY );
						}
					}
				}
				g.Restore( gs );
			}
		}
		
		g.Restore( gs );
	}
	
	//鼠标按下时
	public override bool LeftMouseDown( float eX, float eY )
	{
		if( !Visible ) {
			return false;
		}
		
		isMousePress = isMouseOn;
		isRolPress = isRolOn;
		
		if( isMouseOn ) {
			Last_mX = eX;
			Last_mY = eY;
		}
		GetWorldTrans( eX, eY, out eX, out eY );
		
		bool on = false;
		bool Quit = false;
		Owner.s_Angle = 0;
		Owner.s_t_Angle = true;
		Owner.SimMouseDown( out on, out Quit, (int)(eX - MidX), (int)(eY - MidY) );
		Owner.s_t_Angle = false;
		
		return isMousePress || isRolPress || on || Quit;
	}
	
	//鼠标松开时
	public override void LeftMouseUp( float eX, float eY )
	{
		if( !Visible ) {
			return;
		}
		
		isMousePress = false;
		isRolPress = false;
		
		GetWorldTrans( eX, eY, out eX, out eY );
		
		Owner.SimMouseUp();
	}
	
	//鼠标移动时
	public override bool LeftMouseMove( float eX, float eY, bool MoveAll )
	{
		if( !Visible ) {
			return false;
		}
		
		float eeX = 0;
		float eeY = 0;
		GetWorldTrans( eX, eY, out eeX, out eeY );
		
		MouseIsInside( eeX, eeY );
		
		isMouseOn |= MoveAll;
		
		Owner.isMouseOnBase = isMouseOn;
		G.CGPanel.MyRefresh();
		
		Owner.s_Angle = 0;
		Owner.s_t_Angle = true;
		Owner.SimMouseMove( (int)(eeX - MidX), (int)(eeY - MidY) );
		Owner.s_t_Angle = false;
		
		if( isMousePress ) {
			
			SX += eX - Last_mX;
			SY += eY - Last_mY;
			
			Last_mX = eX;
			Last_mY = eY;
		}
		if( isRolPress ) {
			float ox = eX - MidX;
			float oy = eY - MidY;
			float r = (float)Math.Sqrt( ox*ox + oy*oy );
			if( r < 0.001f ) {
				r = 0.001f;
			}
			float a = (float)Math.Acos( -oy / r );
			if( ox > 0 ) {
				a = (float)Math.PI * 2 - a;
			}
			Angle = 180 * a / (float)Math.PI;
		}
		return isMousePress | isRolPress;
	}           
}
public class Loc
{
	//目标的方位角度
	public float Angle;
	
	//目标的偏角
	public float SAngle;
	
	//目标距离
	public float R;
}
}


