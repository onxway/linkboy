﻿
namespace n_Sprite
{
using System;
using System.Drawing;
using System.Drawing.Drawing2D;
using n_HardModule;
using n_SimulateObj;
using n_SG;
using n_RA;
using n_SOblect;
using n_SimPanel;
using n_CommonData;
using n_OS;

public class Sprite : SObject, ICloneable
{
	public string[] ImagePathList;
	public Bitmap[] ImageList;
	public int ILength;
	public int IIndex;
	
	//排除碰撞列表
	// "":		表示所有的模块参与碰撞处理
	// 纯数字:	表示排除指定的模块参与碰撞处理
	// ALL:		表示排除所有的模块参与碰撞处理
	// SELF:	表示排除同类角色
	
	string v_NotHitList;
	public string NotHitList {
		get {
			return v_NotHitList;
		}
		set {
			v_NotHitList = value;
		}
	}
	
	float Scale;
	float InitWidth;
	float InitHeight;
	
	const int Addr_MidX = 0x01;
	const int Addr_MidY = 0x02;
	const int Addr_MidZ = 0x03;
	
	public const int Addr_Sysbool = 0x0c;
	public const int Addr_Sysmes = 0x0d;
	
	const int Addr_Angle = 0x10;
	const int Addr_Scale = 0x11;
	const int Addr_Opacity = 0x12;
	const int Addr_ImageIndex = 0x13;
	const int Addr_IgnoreHit = 0x14;
	const int Addr_RunStep = 0x15;
	
	const int SizeDragWidth = 15;
	
	int MouseOnSizeDragIndex;
	int DragIndex;
	float DragX;
	float DragY;
	
	float DragSX;
	float DragSY;
	float DragWidth;
	float DragHeight;
	
	static Pen RolPen1;
	//static Pen RolPen2;
	static Bitmap arrow;
	
	//初始化
	public static void SpriteInit()
	{
		System.Drawing.Drawing2D.AdjustableArrowCap lineCap = new System.Drawing.Drawing2D.AdjustableArrowCap(6, 6, true);
		
		RolPen1 = new Pen( Color.SlateGray, 1 );
		RolPen1.CustomEndCap = lineCap;
		
		//RolPen2 = new Pen( Color.Black, 1 );
		
		arrow = new Bitmap( OS.SystemRoot + "Resource" + OS.PATH_S + "gui" + OS.PATH_S + "GameBox" + OS.PATH_S + "a.png" );
	}
	
	//构造函数
	public Sprite( HardModule o, int idx ) : base( o, idx )
	{
		Width = 300;
		Height = 300;
		
		RolX = 0;
		RolY = 0;
		
		imgAngle = 45;
		
		NotHitIndex = -1;
		NotHitList = "";
		
		Z = 0;
		Scale = 1;
		EnableHit = true;
		
		ImagePathList = new string[20];
		ImageList = new Bitmap[50];
		ILength = 0;
		IIndex = 0;
		
		MouseOnSizeDragIndex = 0;
		DragIndex = 0;
	}
	
	public object Clone()
    {
        return MemberwiseClone();
    }
	
	//-------------------------------------------------------------
	
	//获取图片路径列表
	public string GetImagePathList()
	{
		string path = "";
		for( int i = 0; i < ILength; ++i ) {
			path += ImagePathList[i] + "|";
		}
		return path.TrimEnd( '|' );
	}
	
	//设置图片列表
	public void SetImageList( string pathlist )
	{
		ClearImage();
		if( pathlist == null ) {
			return;
		}
		string[] ss = pathlist.Split( '|' );
		for( int i = 0; i < ss.Length; ++i ) {
			AddImage( ss[i] );
		}
	}
	
	//添加一个图片
	public void AddImage( string ImagePath )
	{
		string t = ImagePath;
		
		ImagePath = CommonData.Flag_GetRealPath( ImagePath );
		
		if( System.IO.File.Exists( ImagePath ) ) {
			Bitmap b = new Bitmap( ImagePath );
			ImageList[ILength] = new Bitmap( b );
			b.Dispose();
		}
		else {
			ImageList[ILength] = null;
		}
		ImagePathList[ILength] = t;
		ILength += 1;
	}
	
	//清空图片
	public void ClearImage()
	{
		ILength = 0;
		IIndex = 0;
	}
	
	//设置图片序号
	public void SetImage( int i )
	{
		IIndex = i;
		
		//if( IIndex >= ILength ) {
		//	IIndex = 0;
		//}
	}
	
	//-------------------------------------------------------------
	
	//接收到参数设置指令时
	public bool DataReceiveError( int addr, int data )
	{
		if( addr == Addr_MidX ) {
			MidX = data / n_ConstString.ConstString.FixScale;
			return XExistHit;
		}
		else if( addr == Addr_MidY ) {
			MidY = -data / n_ConstString.ConstString.FixScale;
			return YExistHit;
		}
		else if( addr == Addr_MidZ ) {
			Z = data / n_ConstString.ConstString.FixScale;
		}
		else if( addr == Addr_Angle ) {
			Angle = data / n_ConstString.ConstString.FixScale;
		}
		else if( addr == Addr_ImageIndex ) {
			IIndex = data;
		}
		else if( addr == Addr_Scale ) {
			Scale = data / n_ConstString.ConstString.FixScale;
			Width = InitWidth * Scale;
			Height = InitHeight * Scale;
		}
		else if( addr == Addr_RunStep ) {
			float r = data / n_ConstString.ConstString.FixScale;
			float x = 0;
			float y = 0;
			RA.GetXY( out x, out y, Angle, r );
			MidX += x;
			MidY += y;
		}
		else if( addr == Addr_IgnoreHit ) {
			IgnoreHit = data != 0;
		}
		else {
			//...
		}
		
		return false;
	}
	
	//接收到参数设置指令时
	public int DataRead( int addr )
	{
		if( addr == Addr_MidX ) {
			return (int)(MidX * n_ConstString.ConstString.FixScale);
		}
		else if( addr == Addr_MidY ) {
			return -(int)(MidY * n_ConstString.ConstString.FixScale);
		}
		else if( addr == Addr_MidZ ) {
			return (int)(Z * n_ConstString.ConstString.FixScale);
		}
		else if( addr == Addr_Angle ) {
			return (int)(Angle * n_ConstString.ConstString.FixScale);
		}
		else if( addr == Addr_ImageIndex ) {
			return IIndex;
		}
		else if( addr == Addr_Scale ) {
			return (int)(Scale * n_ConstString.ConstString.FixScale);
		}
		else if( addr == Addr_Event ) {
			return Event;
		}
		else if( addr == Addr_IgnoreHit ) {
			return IgnoreHit? 1: 0;
		}
		else {
			return 0;
		}
	}
	
	//-------------------------------------------------------------
	
	//复位仿真参数
	public override void ResetSimVar()
	{
		InitWidth = Width;
		InitHeight = Height;
		
		Owner.DataList[Addr_MidX] = (int)(MidX * 1000);
		Owner.DataList[Addr_MidX] = (int)(MidY * 1000);
		Owner.DataList[Addr_Angle] = (int)(Angle * 1000);
		
		//设置碰撞元素
		if( NotHitList != "" ) {
			n_MyObject.MyObject mi = G.CGPanel.myModuleList.GetModuleFromName( NotHitList );
			if( mi != null ) {
				NotHitIndex = mi.Index;
			}
			else {
				SimPanel.DebugMes += "<Sprite.ResetSimVar> 没找到模块: " + NotHitList + "\n";
			}
		}
	}
	
	//运行一步
	public override void RunStep( Bitmap Back, float DotNumber, float ix, float iy )
	{
		if( IgnXY ) {
			return;
		}
		
		float eeX0 = 0;
		float eeY0 = 0;
		GetWorldTransEage( SimPanel.CamMidX - SimPanel.CamWidth/2, SimPanel.CamMidY - SimPanel.CamHeight/2, out eeX0, out eeY0 );
		float eeX1 = 0;
		float eeY1 = 0;
		GetWorldTransEage( SimPanel.CamMidX + SimPanel.CamWidth/2, SimPanel.CamMidY - SimPanel.CamHeight/2, out eeX1, out eeY1 );
		float eeX2 = 0;
		float eeY2 = 0;
		GetWorldTransEage( SimPanel.CamMidX - SimPanel.CamWidth/2, SimPanel.CamMidY + SimPanel.CamHeight/2, out eeX2, out eeY2 );
		if( MidX + Width < eeX0 || MidX - Width > eeX1 || MidY + Height < eeY0 || MidY - Height > eeY2 ) {
			Event |= 0x0400;
		}
		else {
			
		}
	}
	
	//---------------------------------------------------------------------------------------
	
	float Ttrans( float x, float t )
	{
		return t + (x - t) * SimPanel.StdDis / (Z - SimPanel.CamZ);
	}
	
	//绘制
	public override void Draw( Graphics g )
	{
		if( !Visible ) {
			return;
		}
		if( IgnXY ) {
			return;
		}
		
		if( Z - SimPanel.CamZ < SimPanel.CamFocusDis ) {
			return;
		}
		
		GraphicsState gs = g.Save();
		
		
		//按照摄像机中心位置进行放缩
		SG.Scale( SimPanel.CamMidX, SimPanel.CamMidY, GetFZTrans() );
		
		//----------------------------------------------------------------------------
		//调试用
		/*
		//这里绘制调试用碰撞边框
		if(HitType == 0 ) {
			g.DrawRectangle( Pens.Black, rX, rY, rWidth, rHeight );
		}
		else {
			g.DrawRectangle( Pens.Red, rX, rY, rWidth, rHeight );
		}
		
		g.FillEllipse( Brushes.Red, BX1 - 5, BY1 - 5, 10, 10 );
		g.FillEllipse( Brushes.Red, BX2 - 5, BY2 - 5, 10, 10 );
		g.FillEllipse( Brushes.Red, BX3 - 5, BY3 - 5, 10, 10 );
		g.FillEllipse( Brushes.Red, BX4 - 5, BY4 - 5, 10, 10 );
		//g.DrawString( rX + "," + rY, n_GUIset.GUIset.Font12, Brushes.Black, rX, rY );
		g.DrawString( HitType.ToString(), n_GUIset.GUIset.Font12, Brushes.Black, rX, rY );
		*/
		//----------------------------------------------------------------------------
		
		//按照中心位置进行旋转
		SG.Rotate( MidX, MidY, -Angle );
		
		g.SmoothingMode = SmoothingMode.HighQuality;
		g.InterpolationMode = System.Drawing.Drawing2D.InterpolationMode.Default;
		g.PixelOffsetMode = System.Drawing.Drawing2D.PixelOffsetMode.Half;
		
		GraphicsState gs1 = g.Save();
		
		//按照图片中心位置进行旋转
		SG.Rotate( SX + Width/2, SY + Height/2, -imgAngle );
		
		if( IIndex < ILength ) {
			if( ImageList[IIndex] == null ) {
				g.DrawRectangle( Pens.Red, SX, SY, Width, Height );
				g.DrawLine( Pens.Red, SX, SY, SX + Width, SY + Height );
				g.DrawLine( Pens.Red, SX + Width, SY, SX, SY + Height );
			}
			else {
				//float w = Width * Scale;
				//float h = Height * Scale;
				float w = Width;
				float h = Height;
				g.DrawImage( ImageList[IIndex], SX - (w-Width)/2, SY - (h-Height)/2, w, h );
			}
		}
		else {
			g.DrawRectangle( Pens.Red, SX, SY, Width, Height );
			g.DrawLine( Pens.Red, SX, SY, SX + Width, SY + Height );
			g.DrawLine( Pens.Red, SX + Width, SY, SX, SY + Height );
		}
		
		if( Selected || isMousePress || isMouseOn || isRolOn || isRolPress ) {
			
			if( isHit ) {
				g.DrawRectangle( Pens.Blue, SX - 1, SY - 1, Width + 2, Height + 2 );
			}
			
			else if( Selected ) {
				//g.DrawRectangle( SelectPen, SX, SY, Width, Height );
				g.DrawRectangle( Pens.Black, SX, SY, Width, Height );
			}
			else {
				g.DrawRectangle( Pens.SlateGray, SX, SY, Width, Height );
			}
			
			//绘制4个调节定位点
			if( EnableSize ) {
				DrawSizeDrag( g );
			}
			//绘制偏移XY中心点
			if( EnableRolXY ) {
				DrawRolXY( g );
			}
		}
		
		/*
		g.DrawLine( Pens.Green, tempx1, tempy1, tempx4, tempy4 );
		g.DrawLine( Pens.Blue, tempx2, tempy2, tempx3, tempy3 );
		g.DrawLine( Pens.Orange, ttempx1, ttempy1, ttempx4, ttempy4 );
		g.DrawLine( Pens.Purple, ttempx2, ttempy2, ttempx3, ttempy3 );
		*/
		g.Restore( gs1 );
		
		bool sh = Selected || isMousePress || isMouseOn || isRolOn || isRolPress;
		if( sh ) {
			//绘制坐标原点和方向角
			DrawCoor( g );
			
			if( EnableAngle ) {
				
				float ww = (Width+Height) / 4;
				if( isRolPress ) {
					g.FillEllipse( Brushes.White, MidX + ww - RolWidth, MidY - RolWidth/2, RolWidth, RolWidth );
					g.DrawEllipse( Pens.Red, MidX + ww - RolWidth, MidY - RolWidth/2, RolWidth, RolWidth );
				}
				else if( isRolOn ) {
					g.FillEllipse( Brushes.White, MidX + ww - RolWidth, MidY - RolWidth/2, RolWidth, RolWidth );
					g.DrawEllipse( Pens.Red, MidX + ww - RolWidth, MidY - RolWidth/2, RolWidth, RolWidth );
				}
				else {
					g.FillEllipse( Brushes.White, MidX + ww - RolWidth, MidY - RolWidth/2, RolWidth, RolWidth );
					g.DrawEllipse( Pens.Silver, MidX + ww - RolWidth, MidY - RolWidth/2, RolWidth, RolWidth );
				}
			}
		}
		
		//g.DrawString( Index + ":" + s, n_GUIset.GUIset.Font14, Brushes.Red, SX, SY );
		
		g.Restore( gs );
	}
	
	//绘制尺寸调节框
	void DrawSizeDrag( Graphics g )
	{
		if( MouseOnSizeDragIndex == 1 ) {
			g.FillRectangle( Brushes.White, SX, SY, SizeDragWidth, SizeDragWidth );
			g.DrawRectangle( Pens.Red, SX, SY, SizeDragWidth, SizeDragWidth );
		}
		else {
			g.FillRectangle( Brushes.WhiteSmoke, SX, SY, SizeDragWidth, SizeDragWidth );
			g.DrawRectangle( Pens.SlateGray, SX, SY, SizeDragWidth, SizeDragWidth );
		}
		
		if( MouseOnSizeDragIndex == 2 ) {
			g.FillRectangle( Brushes.White, SX + Width - SizeDragWidth, SY, SizeDragWidth, SizeDragWidth );
			g.DrawRectangle( Pens.Red, SX + Width - SizeDragWidth, SY, SizeDragWidth, SizeDragWidth );
		}
		else {
			g.FillRectangle( Brushes.WhiteSmoke, SX + Width - SizeDragWidth, SY, SizeDragWidth, SizeDragWidth );
			g.DrawRectangle( Pens.SlateGray, SX + Width - SizeDragWidth, SY, SizeDragWidth, SizeDragWidth );
		}
		
		
		if( MouseOnSizeDragIndex == 3 ) {
			g.FillRectangle( Brushes.White, SX + Width - SizeDragWidth, SY + Height - SizeDragWidth, SizeDragWidth, SizeDragWidth );
			g.DrawRectangle( Pens.Red, SX + Width - SizeDragWidth, SY + Height - SizeDragWidth, SizeDragWidth, SizeDragWidth );
		}
		else {
			g.FillRectangle( Brushes.WhiteSmoke, SX + Width - SizeDragWidth, SY + Height - SizeDragWidth, SizeDragWidth, SizeDragWidth );
			g.DrawRectangle( Pens.SlateGray, SX + Width - SizeDragWidth, SY + Height - SizeDragWidth, SizeDragWidth, SizeDragWidth );
		}
		
		if( MouseOnSizeDragIndex == 4 ) {
			g.FillRectangle( Brushes.White, SX, SY + Height - SizeDragWidth, SizeDragWidth, SizeDragWidth );
			g.DrawRectangle( Pens.Red, SX, SY + Height - SizeDragWidth, SizeDragWidth, SizeDragWidth );
		}
		else {
			g.FillRectangle( Brushes.WhiteSmoke, SX, SY + Height - SizeDragWidth, SizeDragWidth, SizeDragWidth );
			g.DrawRectangle( Pens.SlateGray, SX, SY + Height - SizeDragWidth, SizeDragWidth, SizeDragWidth );
		}
	}
	
	//绘制角色坐标原点
	void DrawCoor( Graphics g )
	{
		/*
		float w = Width;
		if( w > Height ) {
			w = Height;
		}
		w /= 20;
		*/
		
		/*
		g.DrawLine( RolPen1, MidX, MidY, MidX + Width/2, MidY );
		
		float w = RolWidth / 2;
		//g.FillEllipse( Brushes.WhiteSmoke, MidX - w, MidY - w, w * 2, w * 2 );
		g.DrawEllipse( Pens.SlateGray, MidX - w, MidY - w, w * 2, w * 2 );
		g.DrawLine( Pens.SlateGray, MidX - w, MidY, MidX + w, MidY );
		g.DrawLine( Pens.SlateGray, MidX, MidY - w, MidX, MidY + w );
		*/
		
		float ww = (Width + Height) / 4;
		float hh = arrow.Height * ww / arrow.Width;
		
		//对应 Height = 13
		g.DrawImage( arrow, MidX - hh/2, MidY - hh/2, ww, hh );
	}
	
	//绘制偏移量XY
	void DrawRolXY( Graphics g )
	{
		float w = Width;
		if( w > Height ) {
			w = Height;
		}
		w /= 8;
		
		if( isRolMoveOn || isRolMovePress ) {
			g.FillEllipse( Brushes.White, SX + Width/2 - w, SY + Height/2 - w, w * 2, w * 2 );
			g.DrawEllipse( Pens.Red, SX + Width/2 - w, SY + Height/2 - w, w * 2, w * 2 );
		}
		else {
			g.FillEllipse( Brushes.WhiteSmoke, SX + Width/2 - w, SY + Height/2 - w, w * 2, w * 2 );
			g.DrawEllipse( Pens.SlateGray, SX + Width/2 - w, SY + Height/2 - w, w * 2, w * 2 );
		}
		float rw = w / 2;
		
		//绘制图片角度调节钮
		/*
		if( isImgRolOn || isImgRolPress ) {
			g.FillEllipse( Brushes.Blue, SX + Width/2 - rw, SY, rw * 2, rw * 2 );
		}
		else {
			g.FillEllipse( Brushes.Yellow, SX + Width/2 - rw, SY, rw * 2, rw * 2 );
		}
		*/
	}
	
	//---------------------------------------------------------------------------------------
	
	//鼠标按下时
	public override bool LeftMouseDown( float eX, float eY )
	{
		if( !Visible ) {
			return false;
		}
		
		isMousePress = isMouseOn;
		isRolPress = isRolOn;
		isRolMovePress = isRolMoveOn;
		isImgRolPress = isImgRolOn;
		
		if( isMousePress || isRolPress || isRolMovePress ) {
			Last_mX = eX;
			Last_mY = eY;
		}
		GetWorldTrans( eX, eY, out eX, out eY );
		
		if( MouseOnSizeDragIndex != 0 ) {
			
			ImgTrans( eX, eY, out eX, out eY, true );
			
			DragIndex = MouseOnSizeDragIndex;
			DragX = eX;
			DragY = eY;
			
			DragSX = SX;
			DragSY = SY;
			DragWidth = Width;
			DragHeight = Height;
		}
		
		return isMousePress || isRolPress;
	}
	
	//鼠标松开时
	public override void LeftMouseUp( float eX, float eY )
	{
		if( !Visible ) {
			return;
		}
		DragIndex = 0;
		isMousePress = false;
		isRolPress = false;
		isRolMovePress = false;
		isImgRolPress = false;
		
		GetWorldTrans( eX, eY, out eX, out eY );
	}
	
	//鼠标移动时
	public override bool LeftMouseMove( float eX, float eY, bool MoveAll )
	{
		if( !Visible ) {
			return false;
		}
		
		float eeX = 0;
		float eeY = 0;
		GetWorldTrans( eX, eY, out eeX, out eeY );
		
		MouseIsInside( eeX, eeY );
		
		float eeeX = 0;
		float eeeY = 0;
		ImgTrans( eeX, eeY, out eeeX, out eeeY, true );
		MouseIsInsideRol( eeeX, eeeY );
		
		GetDragIndex( eeeX, eeeY );
		
		if( DragSizeBox( eeeX, eeeY ) ) {
			return true;
		}
		
		isMouseOn |= MoveAll;
		
		if( isRolPress ) {
			
			float miX = 0;
			float miY = 0;
			GetScreenTrans( MidX, MidY, out miX, out miY );
			
			float ox = eX - miX;
			float oy = eY - miY;
			float r = (float)Math.Sqrt( ox*ox + oy*oy );
			if( r < 0.001f ) {
				r = 0.001f;
			}
			float a = (float)Math.Acos( ox / r );
			if( oy > 0 ) {
				a = (float)Math.PI * 2 - a;
			}
			Angle = 180 * a / (float)Math.PI;
		}
		else if( isImgRolPress ) {
			
			float miX = 0;
			float miY = 0;
			GetScreenTrans( SX + Width/2, SY + Height/2, out miX, out miY );
			
			float ox = eX - miX;
			float oy = eY - miY;
			float r = (float)Math.Sqrt( ox*ox + oy*oy );
			if( r < 0.001f ) {
				r = 0.001f;
			}
			float a = (float)Math.Acos( -oy / r );
			if( ox > 0 ) {
				a = (float)Math.PI * 2 - a;
			}
			imgAngle = 180 * a / (float)Math.PI;
			
			imgAngle -= Angle;
			if( a < 0 ) {
				a += 360;
			}
		}
		else if( isRolMovePress ) {
			
			float ax = 0;
			float ay = 0;
			GetWorldTrans( eX, eY, out ax, out ay );
			float bx = 0;
			float by = 0;
			GetWorldTrans( Last_mX, Last_mY, out bx, out by );
			
			RolX += (ax - bx);
			RolY += (ay - by);
		}
		else if( isMousePress ) {
			
			SX += (eX - Last_mX) * GetZTrans();
			SY += (eY - Last_mY) * GetZTrans();
		}
		else {
			//...
		}
		Last_mX = eX;
		Last_mY = eY;
		
		return isMousePress | isRolPress;
	}           
	
	//拖动当前的尺寸框
	bool DragSizeBox(  float mX, float mY )
	{
		if( DragIndex == 1 ) {
			float ox = mX - DragX;
			float oy = mY - DragY;
			Width = DragWidth - ox * 2;
			Height = DragHeight - oy * 2;
			//SX = DragSX + ox;
			//SY = DragSY + oy;
			
			//n_SimPanel.SimPanel.DebugMes = ox.ToString( "f3" ) + "," + oy.ToString( "f3" ) + " " + SX + "," + SY + "," + Width + "," + Height;
		}
		if( DragIndex == 2 ) {
			float ox = mX - DragX;
			float oy = mY - DragY;
			Width = DragWidth + ox * 2;
			Height = DragHeight - oy * 2;
			//SY = DragSY + oy;
		}
		if( DragIndex == 3 ) {
			float ox = mX - DragX;
			float oy = mY - DragY;
			Width = DragWidth + ox * 2;
			Height = DragHeight + oy * 2;
		}
		if( DragIndex == 4 ) {
			float ox = mX - DragX;
			float oy = mY - DragY;
			Width = DragWidth - ox * 2;
			Height = DragHeight + oy * 2;
			//SX = DragSX + ox;
		}
		if( Width < SizeDragWidth * 2 ) {
			Width = SizeDragWidth * 2;
		}
		if( Height < SizeDragWidth * 2 ) {
			Height = SizeDragWidth * 2;
		}
		return DragIndex != 0;
	}
	
	//计算鼠标所在的尺寸调节框位置
	void GetDragIndex( float mX, float mY )
	{
		MouseOnSizeDragIndex = 0;
		
		if( !EnableSize ) {
			return;
		}
		if( mX >= SX && mX <= SX + SizeDragWidth && mY >= SY && mY <= SY + SizeDragWidth ) {
			MouseOnSizeDragIndex = 1;
			return;
		}
		if( mX >= SX + Width - SizeDragWidth && mX <= SX + Width && mY >= SY && mY <= SY + SizeDragWidth ) {
			MouseOnSizeDragIndex = 2;
			return;
		}
		if( mX >= SX + Width - SizeDragWidth && mX <= SX + Width && mY >= SY + Height - SizeDragWidth && mY <= SY + Height ) {
			MouseOnSizeDragIndex = 3;
			return;
		}
		if( mX >= SX && mX <= SX + SizeDragWidth && mY >= SY + Height - SizeDragWidth && mY <= SY + Height ) {
			MouseOnSizeDragIndex = 4;
			return;
		}
	}
}
}


