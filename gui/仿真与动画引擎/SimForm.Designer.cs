﻿/*
 * 由SharpDevelop创建。
 * 用户： cap_gpu
 * 日期: 2017/4/9
 * 时间: 8:03
 * 
 * 要改变这种模板请点击 工具|选项|代码编写|编辑标准头文件
 */
namespace n_SimForm
{
	partial class SimForm
	{
		/// <summary>
		/// Designer variable used to keep track of non-visual components.
		/// </summary>
		private System.ComponentModel.IContainer components = null;
		
		/// <summary>
		/// Disposes resources used by the form.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing) {
				if (components != null) {
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}
		
		/// <summary>
		/// This method is required for Windows Forms designer support.
		/// Do not change the method contents inside the source code editor. The Forms designer might
		/// not be able to load this method if it was changed manually.
		/// </summary>
		private void InitializeComponent()
		{
			this.components = new System.ComponentModel.Container();
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(SimForm));
			this.panel1 = new System.Windows.Forms.Panel();
			this.buttonSetting = new System.Windows.Forms.Button();
			this.label16 = new System.Windows.Forms.Label();
			this.buttonReset = new System.Windows.Forms.Button();
			this.buttonResetScaleAndLocate = new System.Windows.Forms.Button();
			this.buttonRun = new System.Windows.Forms.Button();
			this.buttonPS = new System.Windows.Forms.Button();
			this.buttonAll = new System.Windows.Forms.Button();
			this.button1 = new System.Windows.Forms.Button();
			this.label17 = new System.Windows.Forms.Label();
			this.textBoxMapScale = new System.Windows.Forms.TextBox();
			this.label6 = new System.Windows.Forms.Label();
			this.checkBoxShowBitImg = new System.Windows.Forms.CheckBox();
			this.buttonLoadImage = new System.Windows.Forms.Button();
			this.trackBarDotNumber = new System.Windows.Forms.TrackBar();
			this.buttonBColor = new System.Windows.Forms.Button();
			this.panel3 = new System.Windows.Forms.Panel();
			this.panelValue = new System.Windows.Forms.Panel();
			this.panelValueMap = new System.Windows.Forms.Panel();
			this.buttonClearMap = new System.Windows.Forms.Button();
			this.panelValueBackImage = new System.Windows.Forms.Panel();
			this.buttonClearImg = new System.Windows.Forms.Button();
			this.buttonBackImgYSize = new System.Windows.Forms.Button();
			this.textBoxBackImgHeight = new System.Windows.Forms.TextBox();
			this.label12 = new System.Windows.Forms.Label();
			this.textBoxBackImgWidth = new System.Windows.Forms.TextBox();
			this.label11 = new System.Windows.Forms.Label();
			this.textBoxBackImgY = new System.Windows.Forms.TextBox();
			this.label10 = new System.Windows.Forms.Label();
			this.textBoxBackImgX = new System.Windows.Forms.TextBox();
			this.label9 = new System.Windows.Forms.Label();
			this.buttonBackImage = new System.Windows.Forms.Button();
			this.panelValueBackColor = new System.Windows.Forms.Panel();
			this.label8 = new System.Windows.Forms.Label();
			this.panelValueSprite = new System.Windows.Forms.Panel();
			this.buttonAngleEnable = new System.Windows.Forms.Button();
			this.trackBarImgRol = new System.Windows.Forms.TrackBar();
			this.label13 = new System.Windows.Forms.Label();
			this.textBoxNotHitList = new System.Windows.Forms.TextBox();
			this.labelSpriteValue = new System.Windows.Forms.Label();
			this.buttonClearImage = new System.Windows.Forms.Button();
			this.listBoxImage = new System.Windows.Forms.ListBox();
			this.buttonAddImage = new System.Windows.Forms.Button();
			this.label7 = new System.Windows.Forms.Label();
			this.panelValueMotor = new System.Windows.Forms.Panel();
			this.label4 = new System.Windows.Forms.Label();
			this.trackBarInertia = new System.Windows.Forms.TrackBar();
			this.label5 = new System.Windows.Forms.Label();
			this.label3 = new System.Windows.Forms.Label();
			this.label2 = new System.Windows.Forms.Label();
			this.trackBarSpeedScale = new System.Windows.Forms.TrackBar();
			this.labelMaxSpeed = new System.Windows.Forms.Label();
			this.trackBarPower = new System.Windows.Forms.TrackBar();
			this.labelStartPower = new System.Windows.Forms.Label();
			this.label1 = new System.Windows.Forms.Label();
			this.checkBoxMotorDir = new System.Windows.Forms.CheckBox();
			this.panelCommon = new System.Windows.Forms.Panel();
			this.buttonSetUserControl = new System.Windows.Forms.Button();
			this.checkBoxVisible = new System.Windows.Forms.CheckBox();
			this.label15 = new System.Windows.Forms.Label();
			this.listBoxName = new System.Windows.Forms.ListBox();
			this.label14 = new System.Windows.Forms.Label();
			this.mmTimer1 = new Dongzr.MidiLite.MmTimer(this.components);
			this.timer1 = new System.Windows.Forms.Timer(this.components);
			this.panel1.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.trackBarDotNumber)).BeginInit();
			this.panel3.SuspendLayout();
			this.panelValue.SuspendLayout();
			this.panelValueMap.SuspendLayout();
			this.panelValueBackImage.SuspendLayout();
			this.panelValueBackColor.SuspendLayout();
			this.panelValueSprite.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.trackBarImgRol)).BeginInit();
			this.panelValueMotor.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.trackBarInertia)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.trackBarSpeedScale)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.trackBarPower)).BeginInit();
			this.panelCommon.SuspendLayout();
			this.SuspendLayout();
			// 
			// panel1
			// 
			this.panel1.BackColor = System.Drawing.Color.WhiteSmoke;
			this.panel1.Controls.Add(this.buttonSetting);
			this.panel1.Controls.Add(this.label16);
			this.panel1.Controls.Add(this.buttonReset);
			this.panel1.Controls.Add(this.buttonResetScaleAndLocate);
			this.panel1.Controls.Add(this.buttonRun);
			this.panel1.Controls.Add(this.buttonPS);
			this.panel1.Controls.Add(this.buttonAll);
			this.panel1.Controls.Add(this.button1);
			this.panel1.Controls.Add(this.label17);
			this.panel1.Dock = System.Windows.Forms.DockStyle.Top;
			this.panel1.Location = new System.Drawing.Point(0, 0);
			this.panel1.Name = "panel1";
			this.panel1.Size = new System.Drawing.Size(1002, 62);
			this.panel1.TabIndex = 5;
			// 
			// buttonSetting
			// 
			this.buttonSetting.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
			this.buttonSetting.BackColor = System.Drawing.Color.WhiteSmoke;
			this.buttonSetting.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
			this.buttonSetting.Font = new System.Drawing.Font("微软雅黑", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
			this.buttonSetting.ForeColor = System.Drawing.Color.SlateGray;
			this.buttonSetting.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
			this.buttonSetting.Location = new System.Drawing.Point(888, 6);
			this.buttonSetting.Name = "buttonSetting";
			this.buttonSetting.Size = new System.Drawing.Size(102, 46);
			this.buttonSetting.TabIndex = 22;
			this.buttonSetting.Text = "高级设置";
			this.buttonSetting.UseVisualStyleBackColor = false;
			this.buttonSetting.Click += new System.EventHandler(this.ButtonSettingClick);
			// 
			// label16
			// 
			this.label16.BackColor = System.Drawing.Color.LightGray;
			this.label16.Dock = System.Windows.Forms.DockStyle.Bottom;
			this.label16.Location = new System.Drawing.Point(0, 57);
			this.label16.Name = "label16";
			this.label16.Size = new System.Drawing.Size(1002, 5);
			this.label16.TabIndex = 20;
			// 
			// buttonReset
			// 
			this.buttonReset.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(241)))), ((int)(((byte)(249)))), ((int)(((byte)(255)))));
			this.buttonReset.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
			this.buttonReset.Font = new System.Drawing.Font("微软雅黑", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
			this.buttonReset.ForeColor = System.Drawing.Color.Silver;
			this.buttonReset.Location = new System.Drawing.Point(583, 16);
			this.buttonReset.Name = "buttonReset";
			this.buttonReset.Size = new System.Drawing.Size(57, 29);
			this.buttonReset.TabIndex = 19;
			this.buttonReset.Text = "复位";
			this.buttonReset.UseVisualStyleBackColor = false;
			this.buttonReset.Visible = false;
			this.buttonReset.Click += new System.EventHandler(this.ButtonResetClick);
			// 
			// buttonResetScaleAndLocate
			// 
			this.buttonResetScaleAndLocate.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(241)))), ((int)(((byte)(249)))), ((int)(((byte)(255)))));
			this.buttonResetScaleAndLocate.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
			this.buttonResetScaleAndLocate.Font = new System.Drawing.Font("微软雅黑", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
			this.buttonResetScaleAndLocate.ForeColor = System.Drawing.Color.Silver;
			this.buttonResetScaleAndLocate.Location = new System.Drawing.Point(785, 16);
			this.buttonResetScaleAndLocate.Name = "buttonResetScaleAndLocate";
			this.buttonResetScaleAndLocate.Size = new System.Drawing.Size(91, 29);
			this.buttonResetScaleAndLocate.TabIndex = 18;
			this.buttonResetScaleAndLocate.Text = "视图居中";
			this.buttonResetScaleAndLocate.UseVisualStyleBackColor = false;
			this.buttonResetScaleAndLocate.Visible = false;
			this.buttonResetScaleAndLocate.Click += new System.EventHandler(this.ButtonResetScaleAndLocateClick);
			// 
			// buttonRun
			// 
			this.buttonRun.BackColor = System.Drawing.Color.CornflowerBlue;
			this.buttonRun.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
			this.buttonRun.Font = new System.Drawing.Font("微软雅黑", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
			this.buttonRun.ForeColor = System.Drawing.Color.White;
			this.buttonRun.Image = ((System.Drawing.Image)(resources.GetObject("buttonRun.Image")));
			this.buttonRun.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
			this.buttonRun.Location = new System.Drawing.Point(12, 6);
			this.buttonRun.Name = "buttonRun";
			this.buttonRun.Size = new System.Drawing.Size(135, 46);
			this.buttonRun.TabIndex = 12;
			this.buttonRun.Text = "程序运行";
			this.buttonRun.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
			this.buttonRun.UseVisualStyleBackColor = false;
			this.buttonRun.Click += new System.EventHandler(this.ButtonRunClick);
			// 
			// buttonPS
			// 
			this.buttonPS.BackColor = System.Drawing.Color.CornflowerBlue;
			this.buttonPS.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
			this.buttonPS.Font = new System.Drawing.Font("微软雅黑", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
			this.buttonPS.ForeColor = System.Drawing.Color.White;
			this.buttonPS.Image = ((System.Drawing.Image)(resources.GetObject("buttonPS.Image")));
			this.buttonPS.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
			this.buttonPS.Location = new System.Drawing.Point(153, 6);
			this.buttonPS.Name = "buttonPS";
			this.buttonPS.Size = new System.Drawing.Size(135, 46);
			this.buttonPS.TabIndex = 5;
			this.buttonPS.Text = "修图神器";
			this.buttonPS.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
			this.buttonPS.UseVisualStyleBackColor = false;
			this.buttonPS.Click += new System.EventHandler(this.ButtonModuleListClick);
			// 
			// buttonAll
			// 
			this.buttonAll.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(241)))), ((int)(((byte)(249)))), ((int)(((byte)(255)))));
			this.buttonAll.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
			this.buttonAll.Font = new System.Drawing.Font("微软雅黑", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
			this.buttonAll.ForeColor = System.Drawing.Color.Silver;
			this.buttonAll.Location = new System.Drawing.Point(646, 16);
			this.buttonAll.Name = "buttonAll";
			this.buttonAll.Size = new System.Drawing.Size(56, 29);
			this.buttonAll.TabIndex = 2;
			this.buttonAll.Text = "全拖";
			this.buttonAll.UseVisualStyleBackColor = false;
			this.buttonAll.Visible = false;
			this.buttonAll.Click += new System.EventHandler(this.ButtonAllClick);
			// 
			// button1
			// 
			this.button1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(241)))), ((int)(((byte)(249)))), ((int)(((byte)(255)))));
			this.button1.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
			this.button1.Font = new System.Drawing.Font("微软雅黑", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
			this.button1.ForeColor = System.Drawing.Color.Silver;
			this.button1.Location = new System.Drawing.Point(708, 16);
			this.button1.Name = "button1";
			this.button1.Size = new System.Drawing.Size(56, 29);
			this.button1.TabIndex = 0;
			this.button1.Text = "单拖";
			this.button1.UseVisualStyleBackColor = false;
			this.button1.Visible = false;
			this.button1.Click += new System.EventHandler(this.Button1Click);
			// 
			// label17
			// 
			this.label17.Font = new System.Drawing.Font("微软雅黑", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
			this.label17.ForeColor = System.Drawing.Color.LightSlateGray;
			this.label17.Location = new System.Drawing.Point(294, 6);
			this.label17.Name = "label17";
			this.label17.Size = new System.Drawing.Size(292, 46);
			this.label17.TabIndex = 21;
			this.label17.Text = "提示：仿真时确保输入法为英文状态，键盘控制才有效";
			// 
			// textBoxMapScale
			// 
			this.textBoxMapScale.BackColor = System.Drawing.Color.WhiteSmoke;
			this.textBoxMapScale.BorderStyle = System.Windows.Forms.BorderStyle.None;
			this.textBoxMapScale.Font = new System.Drawing.Font("微软雅黑", 12F);
			this.textBoxMapScale.Location = new System.Drawing.Point(97, 48);
			this.textBoxMapScale.Name = "textBoxMapScale";
			this.textBoxMapScale.ReadOnly = true;
			this.textBoxMapScale.Size = new System.Drawing.Size(87, 22);
			this.textBoxMapScale.TabIndex = 17;
			// 
			// label6
			// 
			this.label6.Font = new System.Drawing.Font("微软雅黑", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
			this.label6.Location = new System.Drawing.Point(3, 51);
			this.label6.Name = "label6";
			this.label6.Size = new System.Drawing.Size(104, 23);
			this.label6.TabIndex = 15;
			this.label6.Text = "地图放缩：";
			// 
			// checkBoxShowBitImg
			// 
			this.checkBoxShowBitImg.Font = new System.Drawing.Font("微软雅黑", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
			this.checkBoxShowBitImg.ForeColor = System.Drawing.Color.Black;
			this.checkBoxShowBitImg.Location = new System.Drawing.Point(6, 135);
			this.checkBoxShowBitImg.Name = "checkBoxShowBitImg";
			this.checkBoxShowBitImg.Size = new System.Drawing.Size(91, 39);
			this.checkBoxShowBitImg.TabIndex = 14;
			this.checkBoxShowBitImg.Text = "显示贴图";
			this.checkBoxShowBitImg.UseVisualStyleBackColor = true;
			this.checkBoxShowBitImg.Visible = false;
			this.checkBoxShowBitImg.CheckedChanged += new System.EventHandler(this.CheckBoxShowBitImgCheckedChanged);
			// 
			// buttonLoadImage
			// 
			this.buttonLoadImage.BackColor = System.Drawing.Color.CornflowerBlue;
			this.buttonLoadImage.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
			this.buttonLoadImage.Font = new System.Drawing.Font("微软雅黑", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
			this.buttonLoadImage.ForeColor = System.Drawing.Color.White;
			this.buttonLoadImage.Location = new System.Drawing.Point(10, 8);
			this.buttonLoadImage.Name = "buttonLoadImage";
			this.buttonLoadImage.Size = new System.Drawing.Size(87, 29);
			this.buttonLoadImage.TabIndex = 13;
			this.buttonLoadImage.Text = "选择图片";
			this.buttonLoadImage.UseVisualStyleBackColor = false;
			this.buttonLoadImage.Click += new System.EventHandler(this.ButtonLoadImageClick);
			// 
			// trackBarDotNumber
			// 
			this.trackBarDotNumber.Location = new System.Drawing.Point(6, 84);
			this.trackBarDotNumber.Maximum = 400;
			this.trackBarDotNumber.Minimum = 1;
			this.trackBarDotNumber.Name = "trackBarDotNumber";
			this.trackBarDotNumber.Size = new System.Drawing.Size(191, 45);
			this.trackBarDotNumber.TabIndex = 4;
			this.trackBarDotNumber.Value = 1;
			this.trackBarDotNumber.ValueChanged += new System.EventHandler(this.TrackBarDotNumberValueChanged);
			// 
			// buttonBColor
			// 
			this.buttonBColor.BackColor = System.Drawing.Color.LightSalmon;
			this.buttonBColor.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
			this.buttonBColor.Font = new System.Drawing.Font("微软雅黑", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
			this.buttonBColor.Location = new System.Drawing.Point(104, 16);
			this.buttonBColor.Name = "buttonBColor";
			this.buttonBColor.Size = new System.Drawing.Size(32, 32);
			this.buttonBColor.TabIndex = 20;
			this.buttonBColor.UseVisualStyleBackColor = false;
			this.buttonBColor.Click += new System.EventHandler(this.ButtonBColorClick);
			// 
			// panel3
			// 
			this.panel3.BackColor = System.Drawing.Color.White;
			this.panel3.Controls.Add(this.panelValue);
			this.panel3.Controls.Add(this.panel1);
			this.panel3.Dock = System.Windows.Forms.DockStyle.Fill;
			this.panel3.Location = new System.Drawing.Point(0, 0);
			this.panel3.Name = "panel3";
			this.panel3.Size = new System.Drawing.Size(1002, 750);
			this.panel3.TabIndex = 7;
			// 
			// panelValue
			// 
			this.panelValue.BackColor = System.Drawing.Color.WhiteSmoke;
			this.panelValue.Controls.Add(this.panelValueMap);
			this.panelValue.Controls.Add(this.panelValueBackImage);
			this.panelValue.Controls.Add(this.panelValueBackColor);
			this.panelValue.Controls.Add(this.panelValueSprite);
			this.panelValue.Controls.Add(this.panelValueMotor);
			this.panelValue.Controls.Add(this.panelCommon);
			this.panelValue.Controls.Add(this.label15);
			this.panelValue.Controls.Add(this.listBoxName);
			this.panelValue.Controls.Add(this.label14);
			this.panelValue.Dock = System.Windows.Forms.DockStyle.Left;
			this.panelValue.Location = new System.Drawing.Point(0, 62);
			this.panelValue.Name = "panelValue";
			this.panelValue.Size = new System.Drawing.Size(987, 688);
			this.panelValue.TabIndex = 6;
			// 
			// panelValueMap
			// 
			this.panelValueMap.AutoScroll = true;
			this.panelValueMap.Controls.Add(this.buttonClearMap);
			this.panelValueMap.Controls.Add(this.buttonLoadImage);
			this.panelValueMap.Controls.Add(this.textBoxMapScale);
			this.panelValueMap.Controls.Add(this.checkBoxShowBitImg);
			this.panelValueMap.Controls.Add(this.label6);
			this.panelValueMap.Controls.Add(this.trackBarDotNumber);
			this.panelValueMap.Location = new System.Drawing.Point(728, 408);
			this.panelValueMap.Name = "panelValueMap";
			this.panelValueMap.Size = new System.Drawing.Size(230, 241);
			this.panelValueMap.TabIndex = 13;
			// 
			// buttonClearMap
			// 
			this.buttonClearMap.BackColor = System.Drawing.Color.CornflowerBlue;
			this.buttonClearMap.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
			this.buttonClearMap.Font = new System.Drawing.Font("微软雅黑", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
			this.buttonClearMap.ForeColor = System.Drawing.Color.White;
			this.buttonClearMap.Location = new System.Drawing.Point(97, 8);
			this.buttonClearMap.Name = "buttonClearMap";
			this.buttonClearMap.Size = new System.Drawing.Size(87, 29);
			this.buttonClearMap.TabIndex = 27;
			this.buttonClearMap.Text = "清除图片";
			this.buttonClearMap.UseVisualStyleBackColor = false;
			this.buttonClearMap.Click += new System.EventHandler(this.ButtonClearMapClick);
			// 
			// panelValueBackImage
			// 
			this.panelValueBackImage.AutoScroll = true;
			this.panelValueBackImage.Controls.Add(this.buttonClearImg);
			this.panelValueBackImage.Controls.Add(this.buttonBackImgYSize);
			this.panelValueBackImage.Controls.Add(this.textBoxBackImgHeight);
			this.panelValueBackImage.Controls.Add(this.label12);
			this.panelValueBackImage.Controls.Add(this.textBoxBackImgWidth);
			this.panelValueBackImage.Controls.Add(this.label11);
			this.panelValueBackImage.Controls.Add(this.textBoxBackImgY);
			this.panelValueBackImage.Controls.Add(this.label10);
			this.panelValueBackImage.Controls.Add(this.textBoxBackImgX);
			this.panelValueBackImage.Controls.Add(this.label9);
			this.panelValueBackImage.Controls.Add(this.buttonBackImage);
			this.panelValueBackImage.Location = new System.Drawing.Point(728, 126);
			this.panelValueBackImage.Name = "panelValueBackImage";
			this.panelValueBackImage.Size = new System.Drawing.Size(230, 265);
			this.panelValueBackImage.TabIndex = 12;
			// 
			// buttonClearImg
			// 
			this.buttonClearImg.BackColor = System.Drawing.Color.CornflowerBlue;
			this.buttonClearImg.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
			this.buttonClearImg.Font = new System.Drawing.Font("微软雅黑", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
			this.buttonClearImg.ForeColor = System.Drawing.Color.White;
			this.buttonClearImg.Location = new System.Drawing.Point(97, 10);
			this.buttonClearImg.Name = "buttonClearImg";
			this.buttonClearImg.Size = new System.Drawing.Size(87, 29);
			this.buttonClearImg.TabIndex = 26;
			this.buttonClearImg.Text = "清除图片";
			this.buttonClearImg.UseVisualStyleBackColor = false;
			this.buttonClearImg.Click += new System.EventHandler(this.ButtonClearImgClick);
			// 
			// buttonBackImgYSize
			// 
			this.buttonBackImgYSize.BackColor = System.Drawing.Color.CornflowerBlue;
			this.buttonBackImgYSize.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
			this.buttonBackImgYSize.Font = new System.Drawing.Font("微软雅黑", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
			this.buttonBackImgYSize.ForeColor = System.Drawing.Color.White;
			this.buttonBackImgYSize.Location = new System.Drawing.Point(97, 198);
			this.buttonBackImgYSize.Name = "buttonBackImgYSize";
			this.buttonBackImgYSize.Size = new System.Drawing.Size(87, 29);
			this.buttonBackImgYSize.TabIndex = 25;
			this.buttonBackImgYSize.Text = "原始大小";
			this.buttonBackImgYSize.UseVisualStyleBackColor = false;
			this.buttonBackImgYSize.Click += new System.EventHandler(this.ButtonBackImgYSizeClick);
			// 
			// textBoxBackImgHeight
			// 
			this.textBoxBackImgHeight.Font = new System.Drawing.Font("微软雅黑", 12F);
			this.textBoxBackImgHeight.Location = new System.Drawing.Point(97, 160);
			this.textBoxBackImgHeight.Name = "textBoxBackImgHeight";
			this.textBoxBackImgHeight.Size = new System.Drawing.Size(87, 29);
			this.textBoxBackImgHeight.TabIndex = 24;
			this.textBoxBackImgHeight.TextChanged += new System.EventHandler(this.TextBoxBackImgHeightTextChanged);
			// 
			// label12
			// 
			this.label12.Font = new System.Drawing.Font("微软雅黑", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
			this.label12.Location = new System.Drawing.Point(6, 163);
			this.label12.Name = "label12";
			this.label12.Size = new System.Drawing.Size(104, 23);
			this.label12.TabIndex = 23;
			this.label12.Text = "高度：";
			// 
			// textBoxBackImgWidth
			// 
			this.textBoxBackImgWidth.Font = new System.Drawing.Font("微软雅黑", 12F);
			this.textBoxBackImgWidth.Location = new System.Drawing.Point(97, 125);
			this.textBoxBackImgWidth.Name = "textBoxBackImgWidth";
			this.textBoxBackImgWidth.Size = new System.Drawing.Size(87, 29);
			this.textBoxBackImgWidth.TabIndex = 22;
			this.textBoxBackImgWidth.TextChanged += new System.EventHandler(this.TextBoxBackImgWidthTextChanged);
			// 
			// label11
			// 
			this.label11.Font = new System.Drawing.Font("微软雅黑", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
			this.label11.Location = new System.Drawing.Point(6, 128);
			this.label11.Name = "label11";
			this.label11.Size = new System.Drawing.Size(104, 23);
			this.label11.TabIndex = 21;
			this.label11.Text = "宽度：";
			// 
			// textBoxBackImgY
			// 
			this.textBoxBackImgY.Font = new System.Drawing.Font("微软雅黑", 12F);
			this.textBoxBackImgY.Location = new System.Drawing.Point(97, 90);
			this.textBoxBackImgY.Name = "textBoxBackImgY";
			this.textBoxBackImgY.Size = new System.Drawing.Size(87, 29);
			this.textBoxBackImgY.TabIndex = 20;
			this.textBoxBackImgY.TextChanged += new System.EventHandler(this.TextBoxBackImgYTextChanged);
			// 
			// label10
			// 
			this.label10.Font = new System.Drawing.Font("微软雅黑", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
			this.label10.Location = new System.Drawing.Point(6, 93);
			this.label10.Name = "label10";
			this.label10.Size = new System.Drawing.Size(104, 23);
			this.label10.TabIndex = 19;
			this.label10.Text = "Y：";
			// 
			// textBoxBackImgX
			// 
			this.textBoxBackImgX.Font = new System.Drawing.Font("微软雅黑", 12F);
			this.textBoxBackImgX.Location = new System.Drawing.Point(97, 55);
			this.textBoxBackImgX.Name = "textBoxBackImgX";
			this.textBoxBackImgX.Size = new System.Drawing.Size(87, 29);
			this.textBoxBackImgX.TabIndex = 18;
			this.textBoxBackImgX.TextChanged += new System.EventHandler(this.TextBoxBackImgXTextChanged);
			// 
			// label9
			// 
			this.label9.Font = new System.Drawing.Font("微软雅黑", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
			this.label9.Location = new System.Drawing.Point(6, 58);
			this.label9.Name = "label9";
			this.label9.Size = new System.Drawing.Size(104, 23);
			this.label9.TabIndex = 18;
			this.label9.Text = "X：";
			// 
			// buttonBackImage
			// 
			this.buttonBackImage.BackColor = System.Drawing.Color.CornflowerBlue;
			this.buttonBackImage.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
			this.buttonBackImage.Font = new System.Drawing.Font("微软雅黑", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
			this.buttonBackImage.ForeColor = System.Drawing.Color.White;
			this.buttonBackImage.Location = new System.Drawing.Point(10, 10);
			this.buttonBackImage.Name = "buttonBackImage";
			this.buttonBackImage.Size = new System.Drawing.Size(87, 29);
			this.buttonBackImage.TabIndex = 2;
			this.buttonBackImage.Text = "选择图片";
			this.buttonBackImage.UseVisualStyleBackColor = false;
			this.buttonBackImage.Click += new System.EventHandler(this.ButtonBackImageClick);
			// 
			// panelValueBackColor
			// 
			this.panelValueBackColor.AutoScroll = true;
			this.panelValueBackColor.Controls.Add(this.buttonBColor);
			this.panelValueBackColor.Controls.Add(this.label8);
			this.panelValueBackColor.Location = new System.Drawing.Point(492, 139);
			this.panelValueBackColor.Name = "panelValueBackColor";
			this.panelValueBackColor.Size = new System.Drawing.Size(230, 435);
			this.panelValueBackColor.TabIndex = 11;
			// 
			// label8
			// 
			this.label8.Font = new System.Drawing.Font("微软雅黑", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
			this.label8.Location = new System.Drawing.Point(9, 21);
			this.label8.Name = "label8";
			this.label8.Size = new System.Drawing.Size(102, 23);
			this.label8.TabIndex = 18;
			this.label8.Text = "背景颜色：";
			// 
			// panelValueSprite
			// 
			this.panelValueSprite.AutoScroll = true;
			this.panelValueSprite.Controls.Add(this.buttonAngleEnable);
			this.panelValueSprite.Controls.Add(this.trackBarImgRol);
			this.panelValueSprite.Controls.Add(this.label13);
			this.panelValueSprite.Controls.Add(this.textBoxNotHitList);
			this.panelValueSprite.Controls.Add(this.labelSpriteValue);
			this.panelValueSprite.Controls.Add(this.buttonClearImage);
			this.panelValueSprite.Controls.Add(this.listBoxImage);
			this.panelValueSprite.Controls.Add(this.buttonAddImage);
			this.panelValueSprite.Controls.Add(this.label7);
			this.panelValueSprite.Location = new System.Drawing.Point(256, 127);
			this.panelValueSprite.Name = "panelValueSprite";
			this.panelValueSprite.Size = new System.Drawing.Size(230, 447);
			this.panelValueSprite.TabIndex = 10;
			// 
			// buttonAngleEnable
			// 
			this.buttonAngleEnable.BackColor = System.Drawing.Color.Gainsboro;
			this.buttonAngleEnable.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
			this.buttonAngleEnable.Font = new System.Drawing.Font("微软雅黑", 12F);
			this.buttonAngleEnable.Location = new System.Drawing.Point(13, 139);
			this.buttonAngleEnable.Name = "buttonAngleEnable";
			this.buttonAngleEnable.Size = new System.Drawing.Size(177, 30);
			this.buttonAngleEnable.TabIndex = 19;
			this.buttonAngleEnable.UseVisualStyleBackColor = false;
			this.buttonAngleEnable.Click += new System.EventHandler(this.ButtonAngleEnableClick);
			// 
			// trackBarImgRol
			// 
			this.trackBarImgRol.Location = new System.Drawing.Point(3, 205);
			this.trackBarImgRol.Maximum = 360;
			this.trackBarImgRol.Name = "trackBarImgRol";
			this.trackBarImgRol.Size = new System.Drawing.Size(187, 45);
			this.trackBarImgRol.TabIndex = 18;
			this.trackBarImgRol.Value = 1;
			this.trackBarImgRol.Scroll += new System.EventHandler(this.TrackBarImgRolScroll);
			// 
			// label13
			// 
			this.label13.Font = new System.Drawing.Font("微软雅黑", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
			this.label13.ForeColor = System.Drawing.Color.Black;
			this.label13.Location = new System.Drawing.Point(13, 172);
			this.label13.Name = "label13";
			this.label13.Size = new System.Drawing.Size(152, 30);
			this.label13.TabIndex = 12;
			this.label13.Text = "图片旋转：";
			this.label13.TextAlign = System.Drawing.ContentAlignment.BottomLeft;
			// 
			// textBoxNotHitList
			// 
			this.textBoxNotHitList.Location = new System.Drawing.Point(13, 381);
			this.textBoxNotHitList.Name = "textBoxNotHitList";
			this.textBoxNotHitList.Size = new System.Drawing.Size(177, 21);
			this.textBoxNotHitList.TabIndex = 5;
			this.textBoxNotHitList.TextChanged += new System.EventHandler(this.TextBoxNotHitListTextChanged);
			// 
			// labelSpriteValue
			// 
			this.labelSpriteValue.BackColor = System.Drawing.Color.WhiteSmoke;
			this.labelSpriteValue.Font = new System.Drawing.Font("微软雅黑", 10.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
			this.labelSpriteValue.Location = new System.Drawing.Point(13, 253);
			this.labelSpriteValue.Name = "labelSpriteValue";
			this.labelSpriteValue.Size = new System.Drawing.Size(177, 89);
			this.labelSpriteValue.TabIndex = 4;
			// 
			// buttonClearImage
			// 
			this.buttonClearImage.BackColor = System.Drawing.Color.CornflowerBlue;
			this.buttonClearImage.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
			this.buttonClearImage.Font = new System.Drawing.Font("微软雅黑", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
			this.buttonClearImage.ForeColor = System.Drawing.Color.White;
			this.buttonClearImage.Location = new System.Drawing.Point(105, 9);
			this.buttonClearImage.Name = "buttonClearImage";
			this.buttonClearImage.Size = new System.Drawing.Size(85, 30);
			this.buttonClearImage.TabIndex = 2;
			this.buttonClearImage.Text = "清空";
			this.buttonClearImage.UseVisualStyleBackColor = false;
			this.buttonClearImage.Click += new System.EventHandler(this.ButtonClearImageClick);
			// 
			// listBoxImage
			// 
			this.listBoxImage.Font = new System.Drawing.Font("微软雅黑", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
			this.listBoxImage.FormattingEnabled = true;
			this.listBoxImage.ItemHeight = 21;
			this.listBoxImage.Location = new System.Drawing.Point(13, 45);
			this.listBoxImage.Name = "listBoxImage";
			this.listBoxImage.Size = new System.Drawing.Size(177, 88);
			this.listBoxImage.TabIndex = 1;
			this.listBoxImage.Click += new System.EventHandler(this.ListBoxImageClick);
			// 
			// buttonAddImage
			// 
			this.buttonAddImage.BackColor = System.Drawing.Color.CornflowerBlue;
			this.buttonAddImage.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
			this.buttonAddImage.Font = new System.Drawing.Font("微软雅黑", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
			this.buttonAddImage.ForeColor = System.Drawing.Color.White;
			this.buttonAddImage.Location = new System.Drawing.Point(13, 9);
			this.buttonAddImage.Name = "buttonAddImage";
			this.buttonAddImage.Size = new System.Drawing.Size(85, 30);
			this.buttonAddImage.TabIndex = 0;
			this.buttonAddImage.Text = "添加图片";
			this.buttonAddImage.UseVisualStyleBackColor = false;
			this.buttonAddImage.Click += new System.EventHandler(this.ButtonAddImageClick);
			// 
			// label7
			// 
			this.label7.Font = new System.Drawing.Font("微软雅黑", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
			this.label7.ForeColor = System.Drawing.Color.Black;
			this.label7.Location = new System.Drawing.Point(13, 348);
			this.label7.Name = "label7";
			this.label7.Size = new System.Drawing.Size(152, 30);
			this.label7.TabIndex = 11;
			this.label7.Text = "忽略碰撞列表：";
			this.label7.TextAlign = System.Drawing.ContentAlignment.BottomLeft;
			// 
			// panelValueMotor
			// 
			this.panelValueMotor.AutoScroll = true;
			this.panelValueMotor.Controls.Add(this.label4);
			this.panelValueMotor.Controls.Add(this.trackBarInertia);
			this.panelValueMotor.Controls.Add(this.label5);
			this.panelValueMotor.Controls.Add(this.label3);
			this.panelValueMotor.Controls.Add(this.label2);
			this.panelValueMotor.Controls.Add(this.trackBarSpeedScale);
			this.panelValueMotor.Controls.Add(this.labelMaxSpeed);
			this.panelValueMotor.Controls.Add(this.trackBarPower);
			this.panelValueMotor.Controls.Add(this.labelStartPower);
			this.panelValueMotor.Controls.Add(this.label1);
			this.panelValueMotor.Controls.Add(this.checkBoxMotorDir);
			this.panelValueMotor.Location = new System.Drawing.Point(20, 126);
			this.panelValueMotor.Name = "panelValueMotor";
			this.panelValueMotor.Size = new System.Drawing.Size(230, 494);
			this.panelValueMotor.TabIndex = 1;
			// 
			// label4
			// 
			this.label4.Font = new System.Drawing.Font("微软雅黑", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
			this.label4.ForeColor = System.Drawing.Color.Chocolate;
			this.label4.Location = new System.Drawing.Point(3, 416);
			this.label4.Name = "label4";
			this.label4.Size = new System.Drawing.Size(194, 67);
			this.label4.TabIndex = 10;
			this.label4.Text = "设置马达的惯性，数值越大，马达启动和停止时速度变化越慢";
			// 
			// trackBarInertia
			// 
			this.trackBarInertia.Location = new System.Drawing.Point(3, 384);
			this.trackBarInertia.Maximum = 100;
			this.trackBarInertia.Name = "trackBarInertia";
			this.trackBarInertia.Size = new System.Drawing.Size(194, 45);
			this.trackBarInertia.TabIndex = 9;
			this.trackBarInertia.Value = 10;
			this.trackBarInertia.Scroll += new System.EventHandler(this.TrackBarInertiaScroll);
			// 
			// label5
			// 
			this.label5.Font = new System.Drawing.Font("微软雅黑", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
			this.label5.ForeColor = System.Drawing.Color.Black;
			this.label5.Location = new System.Drawing.Point(3, 358);
			this.label5.Name = "label5";
			this.label5.Size = new System.Drawing.Size(100, 23);
			this.label5.TabIndex = 8;
			this.label5.Text = "惯性";
			// 
			// label3
			// 
			this.label3.Font = new System.Drawing.Font("微软雅黑", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
			this.label3.ForeColor = System.Drawing.Color.Chocolate;
			this.label3.Location = new System.Drawing.Point(3, 286);
			this.label3.Name = "label3";
			this.label3.Size = new System.Drawing.Size(194, 66);
			this.label3.TabIndex = 7;
			this.label3.Text = "设置马达的减速比，数值越大，同样功率下马达的转速越慢";
			// 
			// label2
			// 
			this.label2.Font = new System.Drawing.Font("微软雅黑", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
			this.label2.ForeColor = System.Drawing.Color.Chocolate;
			this.label2.Location = new System.Drawing.Point(3, 159);
			this.label2.Name = "label2";
			this.label2.Size = new System.Drawing.Size(194, 70);
			this.label2.TabIndex = 6;
			this.label2.Text = "设置马达的启动功率，实际功率大于启动功率时才会开始转动";
			// 
			// trackBarSpeedScale
			// 
			this.trackBarSpeedScale.Location = new System.Drawing.Point(3, 254);
			this.trackBarSpeedScale.Maximum = 100;
			this.trackBarSpeedScale.Minimum = 10;
			this.trackBarSpeedScale.Name = "trackBarSpeedScale";
			this.trackBarSpeedScale.Size = new System.Drawing.Size(194, 45);
			this.trackBarSpeedScale.TabIndex = 4;
			this.trackBarSpeedScale.Value = 10;
			this.trackBarSpeedScale.Scroll += new System.EventHandler(this.TrackBarSpeedScaleScroll);
			// 
			// labelMaxSpeed
			// 
			this.labelMaxSpeed.Font = new System.Drawing.Font("微软雅黑", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
			this.labelMaxSpeed.ForeColor = System.Drawing.Color.Black;
			this.labelMaxSpeed.Location = new System.Drawing.Point(3, 228);
			this.labelMaxSpeed.Name = "labelMaxSpeed";
			this.labelMaxSpeed.Size = new System.Drawing.Size(100, 23);
			this.labelMaxSpeed.TabIndex = 3;
			this.labelMaxSpeed.Text = "减速比";
			// 
			// trackBarPower
			// 
			this.trackBarPower.Location = new System.Drawing.Point(3, 126);
			this.trackBarPower.Maximum = 100;
			this.trackBarPower.Name = "trackBarPower";
			this.trackBarPower.Size = new System.Drawing.Size(194, 45);
			this.trackBarPower.TabIndex = 2;
			this.trackBarPower.Scroll += new System.EventHandler(this.TrackBarPowerScroll);
			// 
			// labelStartPower
			// 
			this.labelStartPower.Font = new System.Drawing.Font("微软雅黑", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
			this.labelStartPower.ForeColor = System.Drawing.Color.Black;
			this.labelStartPower.Location = new System.Drawing.Point(3, 100);
			this.labelStartPower.Name = "labelStartPower";
			this.labelStartPower.Size = new System.Drawing.Size(100, 23);
			this.labelStartPower.TabIndex = 1;
			this.labelStartPower.Text = "启动功率";
			// 
			// label1
			// 
			this.label1.Font = new System.Drawing.Font("微软雅黑", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
			this.label1.ForeColor = System.Drawing.Color.Chocolate;
			this.label1.Location = new System.Drawing.Point(3, 36);
			this.label1.Name = "label1";
			this.label1.Size = new System.Drawing.Size(194, 73);
			this.label1.TabIndex = 5;
			this.label1.Text = "勾选后会改变马达的转动方向（正转变为反转，反转变为正转）";
			// 
			// checkBoxMotorDir
			// 
			this.checkBoxMotorDir.Font = new System.Drawing.Font("微软雅黑", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
			this.checkBoxMotorDir.ForeColor = System.Drawing.Color.Black;
			this.checkBoxMotorDir.Location = new System.Drawing.Point(3, 3);
			this.checkBoxMotorDir.Name = "checkBoxMotorDir";
			this.checkBoxMotorDir.Size = new System.Drawing.Size(194, 39);
			this.checkBoxMotorDir.TabIndex = 0;
			this.checkBoxMotorDir.Text = "正转指令对应实际反转";
			this.checkBoxMotorDir.UseVisualStyleBackColor = true;
			this.checkBoxMotorDir.CheckedChanged += new System.EventHandler(this.CheckBoxMotorDirCheckedChanged);
			// 
			// panelCommon
			// 
			this.panelCommon.Controls.Add(this.buttonSetUserControl);
			this.panelCommon.Controls.Add(this.checkBoxVisible);
			this.panelCommon.Location = new System.Drawing.Point(244, 36);
			this.panelCommon.Name = "panelCommon";
			this.panelCommon.Size = new System.Drawing.Size(384, 80);
			this.panelCommon.TabIndex = 9;
			// 
			// buttonSetUserControl
			// 
			this.buttonSetUserControl.BackColor = System.Drawing.Color.CornflowerBlue;
			this.buttonSetUserControl.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
			this.buttonSetUserControl.Font = new System.Drawing.Font("微软雅黑", 11.25F);
			this.buttonSetUserControl.ForeColor = System.Drawing.Color.White;
			this.buttonSetUserControl.Location = new System.Drawing.Point(6, 6);
			this.buttonSetUserControl.Name = "buttonSetUserControl";
			this.buttonSetUserControl.Size = new System.Drawing.Size(147, 35);
			this.buttonSetUserControl.TabIndex = 15;
			this.buttonSetUserControl.Text = "用户键盘控制运动";
			this.buttonSetUserControl.UseVisualStyleBackColor = false;
			this.buttonSetUserControl.Visible = false;
			this.buttonSetUserControl.Click += new System.EventHandler(this.ButtonSetUserControlClick);
			// 
			// checkBoxVisible
			// 
			this.checkBoxVisible.Checked = true;
			this.checkBoxVisible.CheckState = System.Windows.Forms.CheckState.Checked;
			this.checkBoxVisible.Font = new System.Drawing.Font("微软雅黑", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
			this.checkBoxVisible.ForeColor = System.Drawing.Color.Black;
			this.checkBoxVisible.Location = new System.Drawing.Point(6, 47);
			this.checkBoxVisible.Name = "checkBoxVisible";
			this.checkBoxVisible.Size = new System.Drawing.Size(194, 29);
			this.checkBoxVisible.TabIndex = 8;
			this.checkBoxVisible.Text = "在模拟界面中显示";
			this.checkBoxVisible.UseVisualStyleBackColor = true;
			this.checkBoxVisible.Visible = false;
			this.checkBoxVisible.CheckedChanged += new System.EventHandler(this.CheckBoxVisibleCheckedChanged);
			// 
			// label15
			// 
			this.label15.BackColor = System.Drawing.Color.LightGray;
			this.label15.Dock = System.Windows.Forms.DockStyle.Top;
			this.label15.Location = new System.Drawing.Point(0, 23);
			this.label15.Name = "label15";
			this.label15.Size = new System.Drawing.Size(982, 5);
			this.label15.TabIndex = 16;
			// 
			// listBoxName
			// 
			this.listBoxName.BackColor = System.Drawing.Color.WhiteSmoke;
			this.listBoxName.BorderStyle = System.Windows.Forms.BorderStyle.None;
			this.listBoxName.Dock = System.Windows.Forms.DockStyle.Top;
			this.listBoxName.Font = new System.Drawing.Font("微软雅黑", 12.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
			this.listBoxName.FormattingEnabled = true;
			this.listBoxName.ItemHeight = 23;
			this.listBoxName.Location = new System.Drawing.Point(0, 0);
			this.listBoxName.Name = "listBoxName";
			this.listBoxName.Size = new System.Drawing.Size(982, 23);
			this.listBoxName.TabIndex = 14;
			this.listBoxName.Click += new System.EventHandler(this.ListBoxNameClick);
			// 
			// label14
			// 
			this.label14.BackColor = System.Drawing.Color.LightGray;
			this.label14.Dock = System.Windows.Forms.DockStyle.Right;
			this.label14.Location = new System.Drawing.Point(982, 0);
			this.label14.Name = "label14";
			this.label14.Size = new System.Drawing.Size(5, 688);
			this.label14.TabIndex = 17;
			// 
			// mmTimer1
			// 
			this.mmTimer1.Interval = 20;
			this.mmTimer1.Mode = Dongzr.MidiLite.MmTimerMode.Periodic;
			this.mmTimer1.Tick += new System.EventHandler(this.MmTimer1Tick);
			// 
			// timer1
			// 
			this.timer1.Tick += new System.EventHandler(this.Timer1Tick);
			// 
			// SimForm
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.BackColor = System.Drawing.Color.Silver;
			this.ClientSize = new System.Drawing.Size(1002, 750);
			this.Controls.Add(this.panel3);
			this.Name = "SimForm";
			this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
			this.Text = "环境模拟机";
			this.Activated += new System.EventHandler(this.SimFormActivated);
			this.Deactivate += new System.EventHandler(this.SimFormDeactivate);
			this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.LocateFormFormClosing);
			this.SizeChanged += new System.EventHandler(this.SimFormSizeChanged);
			this.panel1.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.trackBarDotNumber)).EndInit();
			this.panel3.ResumeLayout(false);
			this.panelValue.ResumeLayout(false);
			this.panelValueMap.ResumeLayout(false);
			this.panelValueMap.PerformLayout();
			this.panelValueBackImage.ResumeLayout(false);
			this.panelValueBackImage.PerformLayout();
			this.panelValueBackColor.ResumeLayout(false);
			this.panelValueSprite.ResumeLayout(false);
			this.panelValueSprite.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.trackBarImgRol)).EndInit();
			this.panelValueMotor.ResumeLayout(false);
			this.panelValueMotor.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.trackBarInertia)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.trackBarSpeedScale)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.trackBarPower)).EndInit();
			this.panelCommon.ResumeLayout(false);
			this.ResumeLayout(false);
		}
		private System.Windows.Forms.Button buttonClearMap;
		private System.Windows.Forms.Button buttonClearImg;
		private System.Windows.Forms.Button buttonAngleEnable;
		private System.Windows.Forms.Button buttonSetting;
		private System.Windows.Forms.Label label17;
		private System.Windows.Forms.TrackBar trackBarImgRol;
		private System.Windows.Forms.Label label16;
		private System.Windows.Forms.Label label14;
		private System.Windows.Forms.Label label13;
		private System.Windows.Forms.Label label15;
		private System.Windows.Forms.Timer timer1;
		private System.Windows.Forms.Label label9;
		private System.Windows.Forms.TextBox textBoxBackImgX;
		private System.Windows.Forms.Label label10;
		private System.Windows.Forms.TextBox textBoxBackImgY;
		private System.Windows.Forms.Label label11;
		public System.Windows.Forms.TextBox textBoxBackImgWidth;
		private System.Windows.Forms.Label label12;
		public System.Windows.Forms.TextBox textBoxBackImgHeight;
		private System.Windows.Forms.Button buttonBackImgYSize;
		private System.Windows.Forms.Label label8;
		private System.Windows.Forms.Button buttonBackImage;
		private System.Windows.Forms.Panel panelValueMap;
		private System.Windows.Forms.Panel panelValueBackImage;
		private System.Windows.Forms.Panel panelValueBackColor;
		private System.Windows.Forms.Label label7;
		private System.Windows.Forms.TextBox textBoxNotHitList;
		private System.Windows.Forms.Button buttonBColor;
		private System.Windows.Forms.Label labelSpriteValue;
		private System.Windows.Forms.Button buttonClearImage;
		private System.Windows.Forms.Button buttonAddImage;
		public System.Windows.Forms.ListBox listBoxImage;
		private System.Windows.Forms.Panel panelValueSprite;
		private System.Windows.Forms.Button buttonReset;
		private System.Windows.Forms.Button buttonResetScaleAndLocate;
		private System.Windows.Forms.TextBox textBoxMapScale;
		private System.Windows.Forms.Label label6;
		private System.Windows.Forms.Button buttonSetUserControl;
		private System.Windows.Forms.CheckBox checkBoxShowBitImg;
		private System.Windows.Forms.Button buttonLoadImage;
		private System.Windows.Forms.Button buttonRun;
		private Dongzr.MidiLite.MmTimer mmTimer1;
		private System.Windows.Forms.Label label5;
		private System.Windows.Forms.TrackBar trackBarInertia;
		private System.Windows.Forms.Label label4;
		private System.Windows.Forms.CheckBox checkBoxVisible;
		private System.Windows.Forms.Panel panelCommon;
		private System.Windows.Forms.ListBox listBoxName;
		private System.Windows.Forms.Button buttonPS;
		private System.Windows.Forms.Label label1;
		private System.Windows.Forms.Label label2;
		private System.Windows.Forms.Label label3;
		private System.Windows.Forms.Label labelMaxSpeed;
		private System.Windows.Forms.TrackBar trackBarSpeedScale;
		private System.Windows.Forms.Label labelStartPower;
		private System.Windows.Forms.TrackBar trackBarPower;
		private System.Windows.Forms.CheckBox checkBoxMotorDir;
		private System.Windows.Forms.Panel panelValueMotor;
		private System.Windows.Forms.Panel panelValue;
		private System.Windows.Forms.TrackBar trackBarDotNumber;
		private System.Windows.Forms.Button buttonAll;
		public System.Windows.Forms.Panel panel3;
		private System.Windows.Forms.Button button1;
		private System.Windows.Forms.Panel panel1;
	}
}
