﻿
using System;
using n_HardModule;
using System.Threading;

namespace n_PyVM
{

//键盘监控类
public static class PyVM
{
	public static int Data;
	
	public static bool PCStep;
	public static bool isOk;
	
	public static bool isFullSpeed;
	
	public static object o;
	
	public static EventWaitHandle _wh;
	public static EventWaitHandle _wh11;
	
	//初始化
	public static void Init()
	{
		isFullSpeed = true;
		PCStep = true;
		isOk = false;
		o = new object();
		_wh = new AutoResetEvent(false);
		_wh11 = new AutoResetEvent(false);
	}
	
	public static void ModuleWrite( int addr, int V2 )
	{
		isOk = true;
		
		if( isFullSpeed ) {
			//...
		}
		else if( PCStep ) {
			if( addr == 1 ) {
				
				_wh11.Set();
				_wh.Reset();
				_wh.WaitOne();
			}
		}
		else {
			if( addr == 0 ) {
				
				n_PyVar.PyVar.CLine =  V2;
				
				_wh11.Set();
				_wh.Reset();
				_wh.WaitOne();
			}
		}
	}
	
	public static void ModuleWrite111( int addr, int V2 )
	{
		if( PCStep ) {
			if( addr == 1 ) {
				
				isOk = false;
				while( !isOk ) {
					//System.Threading.Thread.Sleep( 1 );
				}
			}
		}
		else {
			if( addr == 0 ) {
				
				n_PyVar.PyVar.CLine =  V2;
				
				isOk = false;
				while( !isOk ) {
					//System.Threading.Thread.Sleep( 1 );
				}
			}
		}
	}
	
	public static int ModuleRead( int addr )
	{
		return 0;
	}
}
}


