﻿
using System;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Windows.Forms;

using n_ALG;
using n_VarType;

namespace n_CruxSim
{
//程序仿真类
public static class CruxSim
{
	static NVar[] NVarList;
	static int NLen;
	
	static int SY;
	static int SYB;
	
	const int WIDTH = 380;
	
	static Bitmap imgNDK;
	public static Graphics gndk;
	
	static bool[] KeyList;
	
	static ALG.Queue KeyQueue;
	
	//初始化
	public static void Init()
	{
		NVarList = new NVar[1000];
		NLen = 0;
		
		KeyList = new bool[300];
		
		KeyQueue = new ALG.Queue( 100 );
		
		SYB = n_Head.Head.HeadLabelHeight - 1;
		SY = SYB;
		
		imgNDK = new Bitmap( WIDTH, WIDTH );
	}
	
	//读取一个按键数据
	public static int ReadKey()
	{
		int key = 0;
		bool ok = KeyQueue.ReadOk( ref key );
		if( ok ) {
			return key;
		}
		else {
			return 0;
		}
	}
	
	//仿真复位
	public static void ResetSim()
	{
		for( int i = 0; i < KeyList.Length; ++i ) {
			KeyList[i] = false;
		}
		NLen = 0;
		string list = n_VarList.VarList.GetUserVarList( false );
		if( list != null ) {
			string[] slist = list.TrimEnd( ' ' ).Split( ' ' );
			for( int i = 0; i < slist.Length; ++i ) {
				int vid = int.Parse( slist[i] );
				n_VarNode.VarNode node = n_VarList.VarList.Get( vid );
				
				if( node.Name.EndsWith( n_CoroCompiler.CoroCompiler.E_En ) || node.Name.EndsWith( n_CoroCompiler.CoroCompiler.E_Flag ) ) {
					continue;
				}
				if( !VarType.isBase( node.Type ) ) {
					continue;
				}
				
				//&开头的地址会进入这里
				int d = 0;
				if( !int.TryParse( node.Address, out d ) ) {
					//n_Debug.Debug.Message += ":" + node.Address + "\n";
					continue;
				}
				
				NVar nv = new NVar();
				nv.Name = node.Name.Remove( 0, 2 );
				nv.Type = node.Type;
				nv.Addr = int.Parse( node.Address );
				NVarList[NLen] = nv;
				NLen++;
			}
		}
		
		SetGObj( imgNDK );
	}
	
	//设置绘图对象
	public static void SetGObj( Bitmap o )
	{
		gndk = Graphics.FromImage( o );
		
		gndk.SmoothingMode = SmoothingMode.HighQuality;
		
		//启用图像模糊效果
		gndk.InterpolationMode = System.Drawing.Drawing2D.InterpolationMode.Default;
		gndk.PixelOffsetMode = System.Drawing.Drawing2D.PixelOffsetMode.Half;
		
		gndk.Clear( Color.White );
	}
	
	public static void KeyDown( Keys k )
	{
		int ki = (int)k;
		
		if( !KeyList[ki] ) {
			KeyList[ki] = true;
			
			//n_Debug.Debug.Message += "*" + ki + "  ";
			bool wo = KeyQueue.WriteOk( 1000 + ki );
		}
	}
	
	public static void KeyUp( Keys k )
	{
		int ki = (int)k;
		
		if( KeyList[ki] ) {
			KeyList[ki] = false;
			
			//n_Debug.Debug.Message += "-" + ki + "  ";
			bool wo = KeyQueue.WriteOk( 2000 + ki );
		}
	}
	
	//滚动条移动
	public static void Scroll( int mX, int mY, int Delta )
	{
		//SY += Delta;
	}
	
	//绘制仿真效果
	public static void Draw( Graphics g )
	{
		if( G.ShowCruxCode ) {
			return;
		}
		int SX = 0;
		int W = WIDTH;
		int H = G.CGPanel.Height - SYB;
		
		int varx = 180;
		int hexx = 260;
		
		g.FillRectangle( Brushes.White, SX, SYB, W, H );
		//g.DrawRectangle( Pens.Black, SX, SY, W, H );
		
		g.DrawImage( imgNDK, 0, G.CGPanel.Height - WIDTH );
		g.DrawRectangle( Pens.Silver, 0, G.CGPanel.Height - WIDTH, WIDTH, WIDTH );
		
		g.DrawLine( Pens.Gainsboro, SX + W, SYB, SX + W, G.CGPanel.Height );
		
		for( int i = 0; i < NLen; ++i ) {
			g.DrawString( NVarList[i].Type, n_GUIset.GUIset.Font13, Brushes.DodgerBlue, SX + 20, SY + 5 + i * 40 );
			g.DrawString( NVarList[i].Name, n_GUIset.GUIset.Font13, Brushes.Black, SX + 90, SY + 5 + i * 40 );
			
			long val = NVarList[i].Value;
			
			if( NVarList[i].Type == VarType.BaseType.Bit ) {
				NVarList[i].Value = (int)n_Data.Data.GetUint8( n_UserModule.UserModule.BASE, NVarList[i].Addr );
				g.DrawString( val.ToString(), n_GUIset.GUIset.Font13, Brushes.OrangeRed, SX + varx, SY + 5 + i * 40 );
				g.DrawString( (val==0?"LOW":"HIGH"), n_GUIset.GUIset.Font13, Brushes.Gray, SX + hexx, SY + 5 + i * 40 );
			}
			if( NVarList[i].Type == VarType.BaseType.Bool ) {
				NVarList[i].Value = (int)n_Data.Data.GetUint8( n_UserModule.UserModule.BASE, NVarList[i].Addr );
				g.DrawString( val.ToString(), n_GUIset.GUIset.Font13, Brushes.OrangeRed, SX + varx, SY + 5 + i * 40 );
				g.DrawString( (val==0?"false":"true"), n_GUIset.GUIset.Font13, Brushes.Gray, SX + hexx, SY + 5 + i * 40 );
			}
			if( NVarList[i].Type == VarType.BaseType.Uint8 ) {
				NVarList[i].Value = (int)n_Data.Data.GetUint8( n_UserModule.UserModule.BASE, NVarList[i].Addr );
				g.DrawString( val.ToString(), n_GUIset.GUIset.Font13, Brushes.OrangeRed, SX + varx, SY + 5 + i * 40 );
				g.DrawString( "0x" + val.ToString( "X" ).PadLeft( 2, '0' ), n_GUIset.GUIset.Font13, Brushes.Gray, SX + hexx, SY + 5 + i * 40 );
			}
			if( NVarList[i].Type == VarType.BaseType.Uint16 ) {
				NVarList[i].Value = (int)n_Data.Data.GetUint16( n_UserModule.UserModule.BASE, NVarList[i].Addr );
				g.DrawString( val.ToString(), n_GUIset.GUIset.Font13, Brushes.OrangeRed, SX + varx, SY + 5 + i * 40 );
				g.DrawString( "0x" + val.ToString( "X" ).PadLeft( 4, '0' ), n_GUIset.GUIset.Font13, Brushes.Gray, SX + hexx, SY + 5 + i * 40 );
			}
			if( NVarList[i].Type == VarType.BaseType.Uint32 ) {
				NVarList[i].Value = (int)n_Data.Data.GetUint32( n_UserModule.UserModule.BASE, NVarList[i].Addr );
				g.DrawString( val.ToString(), n_GUIset.GUIset.Font13, Brushes.OrangeRed, SX + varx, SY + 5 + i * 40 );
				g.DrawString( "0x" + val.ToString( "X" ).PadLeft( 8, '0' ), n_GUIset.GUIset.Font13, Brushes.Gray, SX + hexx, SY + 5 + i * 40 );
			}
			if( NVarList[i].Type == VarType.BaseType.Sint8 ) {
				NVarList[i].Value = (int)n_Data.Data.GetInt8( n_UserModule.UserModule.BASE, NVarList[i].Addr );
				g.DrawString( val.ToString(), n_GUIset.GUIset.Font13, Brushes.OrangeRed, SX + varx, SY + 5 + i * 40 );
				if( val < 0 ) {
					val = 0xFF - (-val) + 1;
				}
				g.DrawString( "0x" + val.ToString( "X" ).PadLeft( 2, '0' ), n_GUIset.GUIset.Font13, Brushes.Gray, SX + hexx, SY + 5 + i * 40 );
			}
			if( NVarList[i].Type == VarType.BaseType.Sint16 ) {
				NVarList[i].Value = (int)n_Data.Data.GetInt16( n_UserModule.UserModule.BASE, NVarList[i].Addr );
				g.DrawString( val.ToString(), n_GUIset.GUIset.Font13, Brushes.OrangeRed, SX + varx, SY + 5 + i * 40 );
				if( val < 0 ) {
					val = 0xFFFF - (-val) + 1;
				}
				g.DrawString( "0x" + val.ToString( "X" ).PadLeft( 4, '0' ), n_GUIset.GUIset.Font13, Brushes.Gray, SX + hexx, SY + 5 + i * 40 );
			}
			if( NVarList[i].Type == VarType.BaseType.Sint32 ) {
				NVarList[i].Value = (int)n_Data.Data.GetInt32( n_UserModule.UserModule.BASE, NVarList[i].Addr );
				g.DrawString( val.ToString(), n_GUIset.GUIset.Font13, Brushes.OrangeRed, SX + varx, SY + 5 + i * 40 );
				if( val < 0 ) {
					val = 0xFFFFFFFF - (-val) + 1;
				}
				g.DrawString( "0x" + val.ToString( "X" ).PadLeft( 8, '0' ), n_GUIset.GUIset.Font13, Brushes.Gray, SX + hexx, SY + 5 + i * 40 );
			}
		}
		//g.DrawString( "TEST: " + test.ToString(), n_GUIset.GUIset.Font13, Brushes.OrangeRed, SX + varx, SY + 5 + 5 * 40 );
	}
	
	public static int test;
}
class NVar
{
	public string Name;
	public string Type;
	public int Addr;
	public long Value;
}
}


