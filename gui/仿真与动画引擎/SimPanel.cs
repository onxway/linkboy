﻿
namespace n_SimPanel
{
using System;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Windows.Forms;

using System.Diagnostics;
using c_ControlType;
using c_MyObjectSet;
using n_EventLink;
using n_GUIcoder;
using n_GUIset;
using n_GVar;
using n_HardModule;
using n_Head;
using n_MyControl;
using n_MyFileObject;
using n_MyObject;
using n_MyObjectList;
using n_OS;
using n_SelectPanel;
using n_Shape;
using n_StartTip;
using n_UIModule;
using n_GNote;
using n_EnginePair;
using n_SG;
using n_GUICommon;
using n_ObjectMesPanel;
using n_MidPortList;
using n_LineColorBox;
using n_SimModule;
using n_RA;
using n_Sprite;
using n_SOblect;
using n_Keyboard;
using System.Runtime.InteropServices;

//*****************************************************
//图形编辑器容器类
public class SimPanel : Panel
{
	
	/*
	    [System.Runtime.InteropServices.DllImportAttribute("gdi32.dll")]
        public static extern IntPtr CreateCompatibleDC(IntPtr hdc);
        
        [System.Runtime.InteropServices.DllImport("gdi32.dll")]
        private static extern int BitBlt(
        IntPtr hdcDest,     // handle to destination DC (device context)
        int nXDest,         // x-coord of destination upper-left corner
        int nYDest,         // y-coord of destination upper-left corner
        int nWidth,         // width of destination rectangle
        int nHeight,        // height of destination rectangle
        IntPtr hdcSrc,      // handle to source DC
        int nXSrc,          // x-coordinate of source upper-left corner
        int nYSrc,          // y-coordinate of source upper-left corner
        System.Int32 dwRop  // raster operation code
        );
 
        [System.Runtime.InteropServices.DllImportAttribute("gdi32.dll")]
        public static extern IntPtr SelectObject(IntPtr hdc, IntPtr obj);
 
        [System.Runtime.InteropServices.DllImportAttribute("gdi32.dll")]
        public static extern void DeleteObject(IntPtr obj);
        
	 	[DllImport("gdi32.dll")]
        static public extern IntPtr DeleteDC(IntPtr hDC);
	*/
	
	
	
	
	
	public float StartX;
	public float StartY;
	
	bool isMousePress;
	bool isRightMousePress;
	int Last_mX, Last_mY;
	
	/*
	public static int ScaleIndex;
	public static int[] ScaleList;
	public static int AScale;
	public const int AScaleMid = 1024;
	*/
	
	const int Addr_MidX = 0x01;
	const int Addr_MidY = 0x02;
	const int Addr_MidZ = 0x03;
	
	const int Addr_Angle = 0x10;
	
	//标准参考距离
	public const float StdDis = 1000;
	
	//摄像机焦距
	public static float CamFocusDis;
	
	//摄像机Z轴
	public static float CamZ;
	
	//摄像机位置
	static float v_CamMidX0;
	static float v_CamMidX;
	public static float CamMidX {
		set {
			v_CamMidX = value;
		}
		get {
			return v_CamMidX0;
		}
	}
	static float v_CamMidY0;
	static float v_CamMidY;
	public static float CamMidY {
		set {
			v_CamMidY = value;
		}
		get {
			return v_CamMidY0;
		}
	}
	
	//摄像机角度
	public static float CamAngle;
	
	//摄像机尺寸
	public static float CamWidth;
	public static float CamHeight;
	
	//摄像机位置
	public static float BackCamMidX;
	public static float BackCamMidY;
	//摄像机Z轴
	public static float BackCamZ;
	//摄像机角度
	public static float BackCamAngle;
	
	const int Addr_SetCamZ = 0x11;
	const int Addr_CamMesSX = 0x12;
	const int Addr_CamMesEX = 0x13;
	const int Addr_CamMesSY = 0x14;
	const int Addr_CamMesEY = 0x15;
	
	//参考点SX
	public static float CamMesSX;
	//参考点EX
	public static float CamMesEX;
	//参考点SY
	public static float CamMesSY;
	//参考点EY
	public static float CamMesEY;
	
	//窗口宽度高度
	public static int UWidth;
	public static int UHeight;
	
	//是否显示坐标系
	public bool ShowCross;
	
	//当前的鼠标移动位置
	PointF MousePoint;
	
	//硬件仿真对象列表
	public SObject[] SList;
	public int SLength;
	public int RealLength;
	
	//备份数据
	public SObject[] B_SList;
	public int B_SLength;
	
	Font textFont;
	Color StartCoverColor;
	Brush CoverBrush;
	
	int tempY;
	
	bool Busying;
	
	public bool Running;
	
	public int OperType;
	public const int OperMoveSi = 0;
	public const int OperMoveAll = 1;
	
	public static Bitmap Back;
	public static float ImgX = 0;
	public static float ImgY = 0;
	int BackWidth;
	int BackHeight;
	
	public static SObject O_Start;
	public static SObject O_End;
	
	Graphics bg;
	Pen PathPenSet;
	Pen PathPenClear;
	
	public static SObject UserObj;
	
	SObject v_SelObj;
	public SObject SelObj {
		set {
			bool bb = v_SelObj != value;
			if( bb ) {
				if( v_SelObj is Sprite ) {
					Sprite sp = (Sprite)v_SelObj;
					sp.HideMS();
				}
			}
			v_SelObj = value;
			if( bb ) {
				SelObjChanged();
			}
		}
		get {
			return v_SelObj;
		}
	}
	public delegate void D_SelObjChanged();
	public D_SelObjChanged SelObjChanged;
	
	//地图放大倍数
	public static int MapDotNumber;
	
	//背景颜色
	public static Color myBackColor;
	
	//地图文件路径
	public static string MapImgFile;
	
	//背景图片路径
	public static string BackIamgePath;
	public static Bitmap BackBmp;
	public static int BackImgX;
	public static int BackImgY;
	public static int BackImgWidth;
	public static int BackImgHeight;
	
	n_SimForm.SimForm Owner;
	
	public int FrameTick;
	public int Frame;
	public int ErrFrameTick;
	public int ErrFrame;
	
	//是否显示纹理贴图
	public bool ShowBitImg;
	
	public static int FirstPress;
	
	Image myImage0;
	Image myImage1;
	Image myImage2;
	Image myImage3;
	
	public static string DebugMes;
	
	public static bool Drawing;
	
	public static object isDrawing;
	public bool StartRun;
	
	Font TextFont;
	
	//初始化
	public static void Init()
	{
		isDrawing = new object();
		
		MapDotNumber = 1;
		
		FirstPress = 0;
		
		Keyboard.Init();
	}
	
	//构造函数
	public SimPanel( n_SimForm.SimForm o ) : base()
	{
		this.BorderStyle = BorderStyle.None;
		myBackColor = Color.Black;
		
		SetStyle(ControlStyles.UserPaint |
		         ControlStyles.AllPaintingInWmPaint |
		         ControlStyles.OptimizedDoubleBuffer |
		         ControlStyles.ResizeRedraw |
		         ControlStyles.SupportsTransparentBackColor,true);
		
		this.Dock = DockStyle.Fill;
		//this.Dock = DockStyle.None;
		this.ImeMode = ImeMode.NoControl;
		
		Owner = o;
		
		StartX = 0;
		StartY = 0;
		isMousePress = false;
		isRightMousePress = false;
		
		ShowCross = false;
		StartRun = false;
		
		PathPenSet = new Pen( Color.Black, 5 );
		PathPenSet.StartCap = System.Drawing.Drawing2D.LineCap.Round;
		PathPenSet.EndCap = System.Drawing.Drawing2D.LineCap.Round;
		PathPenClear = new Pen( Color.White, 5 );
		PathPenClear.StartCap = System.Drawing.Drawing2D.LineCap.Round;
		PathPenClear.EndCap = System.Drawing.Drawing2D.LineCap.Round;
		
		
		/*
		myImage0 = Image.FromFile( OS.SystemRoot + "game\\back0.png");
		myImage1 = Image.FromFile( OS.SystemRoot + "game\\back1.png");
		myImage2 = Image.FromFile( OS.SystemRoot + "game\\back2.png");
		myImage3 = Image.FromFile( OS.SystemRoot + "game\\back3.png");
		*/
		
		
		StartCoverColor = Color.FromArgb( 150, Color.CornflowerBlue );
		CoverBrush = new SolidBrush( StartCoverColor );
		
		//tBackColor = Color.FromArgb( 40, 45, 49 );
		myBackColor = Color.WhiteSmoke;
		//tBackColor = Color.Black;
		//tBackColor = Color.CornflowerBlue;
		
		Running = false;
		
		Busying = false;
		Drawing = false;
		
		ShowBitImg = false;
		
		OperType = OperMoveSi;
		
		DebugMes = null;
		
		Back = new Bitmap( 100, 100 );
		bg = Graphics.FromImage( Back );
		ChangeImgDotNumber( 1 );
		//DotNumber = 0;
		//ImgX = 0;
		//ImgY = 0;
		
		bg.SmoothingMode = SmoothingMode.HighSpeed;
		bg.InterpolationMode = System.Drawing.Drawing2D.InterpolationMode.NearestNeighbor;
		bg.PixelOffsetMode = System.Drawing.Drawing2D.PixelOffsetMode.None;
		
		bg.Clear( Color.White );
		
		this.MouseMove += new MouseEventHandler( UserMouseMove );
		this.MouseUp += new MouseEventHandler( UserMouseUp );
		this.MouseDown += new MouseEventHandler( UserMouseDown );
		this.MouseWheel += new MouseEventHandler( UserMouseWheel );
		this.KeyDown += new KeyEventHandler( SimPanel_KeyDown );
		this.KeyUp += new KeyEventHandler( SimPanel_KeyUp );
		this.SizeChanged += new EventHandler( SimPanel_SizeChanged );
		
		textFont = new Font( "微软雅黑", 13 );
		
		TextFont = new Font( "微软雅黑", 20 );
		
		SList = new SObject[1000];
		SLength = 0;
		B_SList = new SObject[1000];
		B_SLength = 0;
		
		InitCam();
		
		SObject.SOblectInit();
		SimModule.SimModuleInit();
		Sprite.SpriteInit();
		
		Clear();
	}
	
	//初始化系统放缩
	void InitCam()
	{
		CamMidX = 0;
		CamMidY = 0;
		CamAngle = 0;
		
		CamFocusDis = 1;
		CamZ = -StdDis;
		
		/*
		AScale = AScaleMid;
		
		ScaleIndex = 10;
		ScaleList = new int[21];
		
		float f1 = 100;
		float f2 = 100;
		float ff = 0.65f;
		
		ScaleList[10] = AScaleMid * 100 / 100;
		
		f1 *= ff;
		f2 /= ff;
		ScaleList[9] = AScaleMid * (int)f1 / 100;
		ScaleList[11] = AScaleMid * (int)f2 / 100;
		f1 *= ff;
		f2 /= ff;
		ScaleList[8] = AScaleMid * (int)f1 / 100;
		ScaleList[12] = AScaleMid * (int)f2 / 100;
		f1 *= ff;
		f2 /= ff;
		ScaleList[7] = AScaleMid * (int)f1 / 100;
		ScaleList[13] = AScaleMid * (int)f2 / 100;
		f1 *= ff;
		f2 /= ff;
		ScaleList[6] = AScaleMid * (int)f1 / 100;
		ScaleList[14] = AScaleMid * (int)f2 / 100;
		f1 *= ff;
		f2 /= ff;
		ScaleList[5] = AScaleMid * (int)f1 / 100;
		ScaleList[15] = AScaleMid * (int)f2 / 100;
		f1 *= ff;
		f2 /= ff;
		ScaleList[4] = AScaleMid * (int)f1 / 100;
		ScaleList[16] = AScaleMid * (int)f2 / 100;
		f1 *= ff;
		f2 /= ff;
		ScaleList[3] = AScaleMid * (int)f1 / 100;
		ScaleList[17] = AScaleMid * (int)f2 / 100;
		f1 *= ff;
		f2 /= ff;
		ScaleList[2] = AScaleMid * (int)f1 / 100;
		ScaleList[18] = AScaleMid * (int)f2 / 100;
		f1 *= ff;
		f2 /= ff;
		ScaleList[1] = AScaleMid * (int)f1 / 100;
		ScaleList[19] = AScaleMid * (int)f2 / 100;
		f1 *= ff;
		f2 /= ff;
		ScaleList[0] = AScaleMid * (int)f1 / 100;
		ScaleList[20] = AScaleMid * (int)f2 / 100;
		*/
	}
	
	//清空
	public void Clear()
	{
		O_Start = new Sprite( null, 0 );
		O_End = new Sprite( null, 0 );
		O_Start.O_Pre = null;
		O_Start.O_Next = O_End;
		O_End.O_Pre = O_Start;
		O_End.O_Next = null;
		
		for( int i = 0; i < SLength; ++i ) {
			SList[i] = null;
		}
		SLength = 0;
		tempY = 0;
		
		Owner.Clear();
		
		Keyboard.Clear();
	}
	
	//设置图片
	public void SetBitmap( Bitmap b )
	{
		if( b == null ) {
			MapImgFile = null;
			return;
		}
		lock( Back ) {
			Back = b;
			BackWidth = Back.Width;
			BackHeight = Back.Height;
		}
	}
	
	//获取图片
	public Bitmap GetBitmap()
	{
		Bitmap b;
		lock( Back ) {
			b = new Bitmap( Back );
		}
		return b;
	}
	
	//改变图片尺寸
	public void ChangeImgDotNumber( int f )
	{
		MapDotNumber = f;
		
		//ImgX = -BackWidth * DotNumber / 2;
		//ImgY = -BackWidth * DotNumber / 2;
	}
	
	//------------------------------------------------------------
	//摄像机功能
	
	//接收到新的数据
	public void DataReceive( int addr, int data )
	{
		if( addr == Addr_MidX ) {
			CamMidX = data / n_ConstString.ConstString.FixScale;
		}
		else if( addr == Addr_MidY ) {
			CamMidY = -data / n_ConstString.ConstString.FixScale;
		}
		else if( addr == Addr_MidZ ) {
			CamZ = data / n_ConstString.ConstString.FixScale;
		}
		else if( addr == Addr_Angle ) {
			CamAngle = data / n_ConstString.ConstString.FixScale;
		}
		else if( addr == Addr_SetCamZ ) {
			SetWorldMesZ( data / n_ConstString.ConstString.FixScale );
		}
		else{
			//...
		}
	}
	
	//接收到参数设置指令时
	public int DataRead( int addr )
	{
		if( addr == Addr_MidX ) {
			return (int)(CamMidX * n_ConstString.ConstString.FixScale);
		}
		else if( addr == Addr_MidY ) {
			return -(int)(CamMidY * n_ConstString.ConstString.FixScale);
		}
		else if( addr == Addr_MidZ ) {
			return (int)(CamZ * n_ConstString.ConstString.FixScale);
		}
		else if( addr == Addr_Angle ) {
			return (int)(CamAngle * n_ConstString.ConstString.FixScale);
		}
		else if( addr == Addr_CamMesSX ) {
			return (int)(CamMesSX * n_ConstString.ConstString.FixScale);
		}
		else if( addr == Addr_CamMesEX ) {
			return (int)(CamMesEX * n_ConstString.ConstString.FixScale);
		}
		else if( addr == Addr_CamMesSY ) {
			return (int)(CamMesSY * n_ConstString.ConstString.FixScale);
		}
		else if( addr == Addr_CamMesEY ) {
			return (int)(CamMesEY * n_ConstString.ConstString.FixScale);
		}
		else {
			return 0;
		}
	}
	
	//屏幕到世界坐标变换
	public void SetWorldMesZ( float mZ )
	{
		float CamMesWidth = CamWidth * (mZ - CamZ) / StdDis;
		float CamMesHeight = CamHeight * (mZ - CamZ) / StdDis;
		
		CamMesSX = CamMidX - CamMesWidth/2;
		CamMesEX = CamMidX + CamMesWidth/2;
		CamMesSY = -(CamMidY - CamMesHeight/2);
		CamMesEY = -(CamMidY + CamMesHeight/2);
	}
	
	//------------------------------------------------------------
	
	//复位仿真参数
	public void ResetSimVar()
	{
		for( int i = 0; i < SLength; ++i ) {
			if( SList[i] != null ) {
				SList[i].ResetSimVar();
			}
		}
	}
	
	Random r = new Random();
	
	//复制一个已有角色
	public Sprite Copy( Sprite s )
	{
		for( int i = 0; i < SList.Length; ++i ) {
			if( SList[i] == null ) {
				Sprite n = (Sprite)s.Clone();
				n.ByCopy = true;
				n.Index = i;
				AddNew( n, i );
				if( SLength < i + 1 ) {
					SLength = i + 1;
				}
				return n;
			}
		}
		return null;
	}
	
	//删除一个角色
	public Sprite Delete( Sprite s )
	{
		if( s == null ) {
			DebugMes += "删除空元素!\n";
			return null;
		}
		SList[s.Index] = null;
		
		SObject tp = s.O_Pre;
		SObject tn = s.O_Next;
		
		tn.O_Pre = tp;
		tp.O_Next = tn;
		return null;
	}
	
	//向后获取从指定索引开始的第一个指定类型的角色
	public Sprite GetFirst( MyObject hm, int idx )
	{
		for( int i = idx; i < SLength; ++i ) {
			if( SList[i] == null ) {
				continue;
			}
			if( G.SimulateMode && !SList[i].ByCopy ) {
				continue;
			}
			if( SList[i].Owner == hm ) {
				return (Sprite)SList[i];
			}
		}
		return null;
	}
	
	//向前获取从指定索引开始的第一个指定类型的角色
	public Sprite GetEnd( MyObject hm, int idx )
	{
		for( int i = idx; i >= 0; --i ) {
			if( SList[i] == null ) {
				continue;
			}
			if( G.SimulateMode && !SList[i].ByCopy ) {
				continue;
			}
			if( SList[i].Owner == hm ) {
				return (Sprite)SList[i];
			}
		}
		return null;
	}
	
	
	//获取事件扫描
	public void GetEvent( MyObject hm )
	{
		for( int i = 0; i < SLength; ++i ) {
			if( SList[i] == null ) {
				continue;
			}
			if( G.SimulateMode && !SList[i].ByCopy ) {
				continue;
			}
			if( SList[i].Owner == hm ) {
				Sprite s = (Sprite)SList[i];
				
				s.IgnXY = false;
				
				if( hm.DataList[Sprite.Addr_Event] == 0 && s.Event != 0 ) {
					hm.DataList[Sprite.Addr_Event] = s.Event;
					hm.DataList[Sprite.Addr_EventIndex] = s.Index;
					s.Event = 0;
					s.test = true;
					return;
				}
			}
		}
	}
	
	//获取指定的角色
	public Sprite GetIndex( int i )
	{
		return (Sprite)SList[i];
	}
	
	//添加一个模块
	public void Add( MyObject hm )
	{
		if( !(hm is HardModule) ) {
			return;
		}
		HardModule h = (HardModule)hm;
		if( h.ImageName == SPMoudleName.SYS_iport ) {
			return;
		}
		for( int i = 0; i < SList.Length; ++i ) {
			if( SList[i] == null ) {
				
				if( h.ImageName == SPMoudleName.SYS_Sprite ) {
					Sprite sp = new Sprite( h, i );
					if( hm.ExistSimValue ) {
						sp.EnableHit = false;
						sp.MidX = hm.SimMidX;
						sp.MidY = hm.SimMidY;
						sp.Width = hm.SimWidth;
						sp.Height = hm.SimHeight;
						
						sp.Angle = hm.SimAngle;
						sp.Visible = hm.SimVisible;
						
						sp.RolX = hm.SimRolX;
						sp.RolY = hm.SimRolY;
						
						sp.imgAngle = hm.SimimgAngle;
						
						sp.NotHitList = hm.NotHitList;
						//sp.EnableHit = true;
						
						//sp.ClearImage();
						//sp.AddImage( hm.SpriteFilePath );
						sp.SetImageList( hm.SpriteFilePath );
						sp.IIndex = hm.ImgIndex;
					}
					else {
						sp.SX = 200;
						sp.SY = tempY;
						sp.Angle = 0;
						sp.Visible = true;
						tempY += h.SourceHeight + 20;
					}
					hm.InitTargetSObject = sp;
					hm.CreateInitTargetSObject = sp;
					
					AddNew( sp, i );
					sp.IgnXY = false;
				}
				else if( h.ImageName == SPMoudleName.SYS_View ) {
					//...
				}
				else if( h.ImageName == SPMoudleName.SYS_Keyboard ) {
					//if( h.ExtendValue != "" ) {
						n_Keyboard.Keyboard.SetKeyTarget( h );
					//}
				}
				else {
					SimModule sm = new SimModule( h, i );
					sm.LocChanged = LocChanged;
					if( hm.ExistSimValue ) {
						sm.MidX = hm.SimMidX;
						sm.MidY = hm.SimMidY;
						sm.Angle = hm.SimAngle;
						sm.Visible = hm.SimVisible;
						
						sm.SwapPin = hm.SimSwapPin;
						sm.StartPower = hm.SimStartPower;
						sm.SpeedDown = hm.SimSpeedDown;
						sm.Inertia = hm.SimInertia;
					}
					else {
						sm.SX = 200;
						sm.SY = tempY;
						sm.Angle = 0;
						sm.Visible = true;
						tempY += h.SourceHeight + 20;
					}
					
					//临时隐藏 后续场景仿真再开放
					sm.Visible = false;
					
					hm.InitTargetSObject = sm;
					AddNew( sm, i );
					sm.IgnXY = false;
				}
				if( SLength < i + 1 ) {
					SLength = i + 1;
				}
				break;
			}
		}
		RefreshNameList();
	}
	
	//添加一个新的角色到指定位置
	void AddNew( SObject m, int i )
	{
		SList[i] = m;
		SObject first = O_Start.O_Next;
		O_Start.O_Next = m;
		m.O_Pre = O_Start;
		m.O_Next = first;
		first.O_Pre = m;
		
		m.RefreshZ();
		
		m.Reset();
	}
	
	//删除一个模块
	public void Remove( MyObject m )
	{
		if( !(m is HardModule) ) {
			return;
		}
		HardModule h = (HardModule)m;
		
		for( int i = 0; i < SLength; ++i ) {
			if( SList[i] != null ) {
				if( SList[i].Owner == h ) {
					
					if( SList[i] is Sprite ) {
						Delete( (Sprite)SList[i] );
					}
					SList[i] = null;
					Owner.Remove( h.Name );
					break;
				}
			}
		}
		RefreshNameList();
	}
	
	//更新名字列表
	public void RefreshNameList()
	{
		Owner.Clear();
		
		/*
		//添加其他元素
		for( int i = SLength - 1; i >= 0; --i ) {
			if( SList[i] != null ) {
				if( SList[i].Owner.ImageName == n_GUIcoder.SPMoudleName.SYS_View ) {
					continue;
				}
				//这里临时屏蔽其他模块, 后续增加环境仿真功能时需要再显示出来
				if( SList[i].Owner.ImageName != n_GUIcoder.SPMoudleName.SYS_Sprite &&
					SList[i].Owner.ImageName != n_GUIcoder.SPMoudleName.SYS_G_BackColor &&
					SList[i].Owner.ImageName != n_GUIcoder.SPMoudleName.SYS_G_BackImage &&
				    SList[i].Owner.ImageName != n_GUIcoder.SPMoudleName.SYS_G_Map ) {
					continue;
				}
				SList[i].Visible = SList[i].Owner.ImageName == n_GUIcoder.SPMoudleName.SYS_Sprite;
				
				Owner.Add( SList[i].Owner.Name );
			}
		}
		*/
		
		//添加角色元素
		for( int i = 0; i < SLength; i++ ) {
			if( SList[i] != null ) {
				
				//这里临时屏蔽其他模块, 后续增加环境仿真功能时需要再显示出来
				if( SList[i].Owner.ImageName != n_GUIcoder.SPMoudleName.SYS_Sprite ) {
					continue;
				}
				SList[i].Visible = true;
				Owner.Add( SList[i].Owner.Name );
			}
		}
		//添加地图元素
		for( int i = 0; i < SLength; i++ ) {
			if( SList[i] != null ) {
				
				//这里临时屏蔽其他模块, 后续增加环境仿真功能时需要再显示出来
				if( SList[i].Owner.ImageName != n_GUIcoder.SPMoudleName.SYS_G_Map ) {
					continue;
				}
				SList[i].Visible = false;
				Owner.Add( SList[i].Owner.Name );
			}
		}
		//添加背景图片元素
		for( int i = 0; i < SLength; i++ ) {
			if( SList[i] != null ) {
				
				//这里临时屏蔽其他模块, 后续增加环境仿真功能时需要再显示出来
				if( SList[i].Owner.ImageName != n_GUIcoder.SPMoudleName.SYS_G_BackImage ) {
					continue;
				}
				SList[i].Visible = false;
				Owner.Add( SList[i].Owner.Name );
			}
		}
		//添加背景颜色元素
		for( int i = 0; i < SLength; i++ ) {
			if( SList[i] != null ) {
				
				//这里临时屏蔽其他模块, 后续增加环境仿真功能时需要再显示出来
				if( SList[i].Owner.ImageName != n_GUIcoder.SPMoudleName.SYS_G_BackColor ) {
					continue;
				}
				SList[i].Visible = false;
				Owner.Add( SList[i].Owner.Name );
			}
		}
	}
	
	//False:  系统复位, 所有参数设置为默认值
	//True: 更新当前参数到系统初始化参数
	public void SetInit()
	{
		/*
		for( int i = 0; i < SLength; ++i ) {
			if( SList[i] != null ) {
				
				SList[i].Owner.SimMidX = SList[i].MidX;
				SList[i].Owner.SimMidY = SList[i].MidY;
				SList[i].Owner.SimAngle = SList[i].Angle;
				
				SList[i].Owner.SimVisible = SList[i].Visible;
				
				if( SList[i] is Sprite ) {
					
					SList[i].Owner.SimWidth = SList[i].Width;
					SList[i].Owner.SimHeight = SList[i].Height;
				}
				if( SList[i] is SimModule ) {
					
					SList[i].Owner.SimSwapPin = SList[i].SwapPin;
					SList[i].Owner.SimStartPower = SList[i].StartPower;
					SList[i].Owner.SimSpeedDown = SList[i].SpeedDown;
					SList[i].Owner.SimInertia = SList[i].inertia;
				}
			}
		}
		*/
		
		BackCamMidX = CamMidX;
		BackCamMidY = CamMidY;
		BackCamZ = CamZ;
		BackCamAngle = CamAngle;
	}
	
	public void Save()
	{
		BackCamMidX = CamMidX;
		BackCamMidY = CamMidY;
		BackCamZ = CamZ;
		BackCamAngle = CamAngle;
		
		RealLength = SLength;
		for( int i = 0; i < SLength; ++i ) {
			
			if( SList[i] is Sprite ) {
				Sprite sp = (Sprite)SList[i];
				if( sp.ByCopy ) {
					continue;
				}
				//这里临时用 ChipType 代替动态角色
				//if( sp.Owner.ChipType == "" ) {
					Sprite n = Copy( sp );
					n.Owner.TargetSObject = n;
				//}
			}
		}
	}
	
	public void Reload()
	{
		CamMidX = BackCamMidX;
		CamMidY = BackCamMidY;
		CamZ = BackCamZ;
		CamAngle = BackCamAngle;
		
		for( int i = 0; i < SLength; ++i ) {
			if( SList[i] != null && SList[i] is Sprite ) {
				
				//如果是自动创建的角色, 直接删除
				Sprite s = (Sprite)SList[i];
				if( s.ByCopy ) {
					Delete( s );
				}
			}
		}
		SLength = RealLength;
	}
	
	//------------------------------------------------------------
	
	//设置指定名字的模块为选中状态
	public void SetSelected( string Name )
	{
		for( int i = 0; i < SLength; ++i ) {
			if( SList[i] == null ) {
				continue;
			}
			if( SList[i].Owner.Name != Name ) {
				continue;
			}
			if( SelObj != null ) {
				SelObj.Selected = false;
			}
			SelObj = SList[i];
			SelObj.Selected = true;
		}
	}
	
	//更新模块物理约束
	public void ResetCal()
	{
		for( int i = 0; i < SLength; ++i ) {
			if( SList[i] == null ) {
				continue;
			}
			if( SList[i].isMotor ) {
				
				SimModule m0 = (SimModule)SList[i];
				
				for( int j = 0; j < SLength; ++j ) {
					if( SList[j] == null ) {
						continue;
					}
					if( SList[j] == m0 ) {
						continue;
					}
					if( !(SList[j] is SimModule) ) {
						continue;
					}
					SObject m1 = SList[j];
					
					float A = 0;
					float R = RA.GetR( out A, m1.MidX-m0.MidX, m1.MidY-m0.MidY );
					m0.LocList[j].Angle = A - m0.Angle;
					m0.LocList[j].R = R;
					m0.LocList[j].SAngle = m1.Angle + (180 - A);
				}
			}
		}
	}
	
	//演算物理约束
	void LocChanged( SimModule m1, float r )
	{
		//马达演算
		SimModule m0 = null;
		for( int i = 0; i < SLength; ++i ) {
			if( SList[i] == null ) {
				continue;
			}
			if( SList[i] == m1 ) {
				continue;
			}
			if( SList[i].isMotor ) {
				m0 = (SimModule)SList[i];
				break;
			}
		}
		if( m0 == null ) {
			//让马达走直线
			//代码未加。。。
		}
		else {
			float x = 0;
			float y = 0;
			RA.GetXY( out x, out y, m0.LocList[m1.Index].SAngle + 90, r );
			
			float a = -y / m0.LocList[m1.Index].R;
			
			Busying = true;
			m0.Angle += a;
			
			for( int i = 0; i < SLength; ++i ) {
				if( SList[i] == null ) {
					continue;
				}
				if( SList[i] == m0 ) {
					continue;
				}
				if( !(SList[i] is SimModule) ) {
					continue;
				}
				SObject m2 = SList[i];
				
				x = 0;
				y = 0;
				RA.GetXY( out x, out y, m0.Angle + m0.LocList[m2.Index].Angle, m0.LocList[m2.Index].R );
				m2.MidX = m0.MidX + x;
				m2.MidY = m0.MidY + y;
				
				m2.Angle = m0.LocList[m2.Index].SAngle - (180 - (m0.Angle + m0.LocList[m2.Index].Angle) );
			}
			Busying = false;
		}
	}
	
	//清空地图
	public void ClearBack()
	{
		bg.Clear( Color.White );
	}
	
	//触发刷新
	public void MyRefresh()
	{
		
	}
	
	//触发刷新
	public void MyRefreshTick()
	{
		/*
		GraphicsPath myPath = new GraphicsPath();
		
		SObject so = O_End.O_Pre;
		while( so != O_Start ) {
			
			if( so.NeedRedraw ) {
				myPath.AddRectangle( so.GetRefreshRect() );
				so.NeedRedraw = false;
			}
			
			so = so.O_Pre;
		}
		Region region = new Region(myPath);
		
		Invalidate( region );
		*/
		
		Invalidate();
	}
	
	//运行一步
	public void RunStep()
	{
		//传感器环境交互
		for( int i = 0; i < SLength; ++i ) {
			if( SList[i] == null ) {
				continue;
			}
			SList[i].RunStep( Back, MapDotNumber, ImgX, ImgY );
		}
	}
	
	//放缩居中
	public void GotoCenter()
	{
		for( int i = 0; i < SLength; ++i ) {
			if( SList[i] != null && SList[i].Visible ) {
				
				CamZ = 0;
				CamMidX = SList[i].MidX;
				CamMidY = SList[i].MidY;
				
				return;
			}
		}
	}
	
	//------------------------------------------------------------
	
	//重绘事件
	protected override void OnPaint(PaintEventArgs e)
	{
		if( Busying ) {
			ErrFrameTick++;
			Invalidate();
			return;
		}
		
		try {
		
		//Drawing = true;
		lock( isDrawing ) {
		MyDraw( e.Graphics );
		}
		Drawing = false;
		
		}
		catch(Exception ee) {
			
			if( G.SimulateMode && n_SimForm.SimForm.Simlate != null ) {
				n_SimForm.SimForm.Simlate();
			}
			n_Debug.Warning.BUG( "<SimPanel.OnPaint> 重绘发生异常: " + ee.ToString() );
		}
	}
	
	//绘制
	void MyDraw( Graphics g )
	{
		if( UserObj != null ) {
			
			float w = Width * 1 / 3;
			float h = Height * 1 / 3;
			
			if( CamMidX < UserObj.MidX - w ) {
				CamMidX = UserObj.MidX - w;
			}
			if( CamMidX > UserObj.MidX + w ) {
				CamMidX = UserObj.MidX + w;
			}
			if( CamMidY < UserObj.MidY - h ) {
				CamMidY = UserObj.MidY - h;
			}
			if( CamMidY > UserObj.MidY + h ) {
				CamMidY = UserObj.MidY + h;
			}
		}
		
		FrameTick++;
		
		//设置绘图目标
		SG.SetObject( g );
		
		GraphicsState gs = g.Save();
		
		//清空背景像素
		g.Clear( myBackColor );
		
		//这里应该根据程序配置判断是否有背景图片
		////g.DrawImage( BackBmp, 0, 0 ); //GDI+绘制较慢
		if( BackIamgePath != null  ) {
			if( BackBmp != null ) {
				n_Bitblt.Blt.DrawImage( g, BackBmp, BackImgX, BackImgY, BackImgWidth, BackImgHeight );
			}
			else {
				g.DrawLine( Pens.Red, BackImgX, BackImgY, BackImgX + BackImgWidth, BackImgY + BackImgHeight );
				g.DrawLine( Pens.Red, BackImgX, BackImgY + BackImgHeight, BackImgX + BackImgWidth, BackImgY );
				g.DrawRectangle( Pens.Red, BackImgX, BackImgY, BackImgWidth, BackImgHeight );
			}
		}
		
		//g.SmoothingMode = SmoothingMode.HighQuality;
		
		//启用图像模糊效果
		//g.InterpolationMode = System.Drawing.Drawing2D.InterpolationMode.Default;
		//g.PixelOffsetMode = System.Drawing.Drawing2D.PixelOffsetMode.Half;
		
		
		/*
		//开始界面放缩
		if( AScale != AScaleMid ) {
			g.ScaleTransform( (float)AScale/AScaleMid, (float)AScale/AScaleMid );
		}
		g.TranslateTransform( -StartX, -StartY );
		*/
		
		float RolX = Width/2;
		float RolY = Height/2;
		
		//更新摄像头位置
		v_CamMidX0 = v_CamMidX;
		v_CamMidY0 = v_CamMidY;	
		
		//摄像机按照中心位置进行旋转
		//SG.Rotate( RolX, RolY, CamAngle );
		//摄像机移动到指定点
		SG.Transfer( -(CamMidX - RolX), -(CamMidY - RolY) );
		
		
		g.SmoothingMode = SmoothingMode.HighSpeed;
		g.InterpolationMode = System.Drawing.Drawing2D.InterpolationMode.NearestNeighbor;
		g.PixelOffsetMode = System.Drawing.Drawing2D.PixelOffsetMode.None;
		
		
		GraphicsState gsb = g.Save();
		
		//背景图片位置放缩
		float oz = (0 - SimPanel.CamZ);
		if( Math.Abs( oz ) < 1 ) {
			oz = 1;
		}
		SG.Scale( SimPanel.CamMidX, SimPanel.CamMidY, SimPanel.StdDis / oz );
		
		//绘制坐标系原点
		if( !G.AutoRunMode && ShowCross ) {
			g.DrawLine( Pens.SlateGray, -1000, 0, 1000, 0 );
			g.DrawLine( Pens.SlateGray, 0, -1000, 0, 1000 );
		}
		
		//判断是否按照像素显示贴图还是原图
		if( ShowBitImg ) {
			ShowBitBmp( g );
		}
		else {
			if( MapImgFile != null ) {
				lock( Back ) {
					g.DrawImage( Back, ImgX, ImgY, Back.Width * MapDotNumber, Back.Height * MapDotNumber );
					
					//n_Bitblt.Blt.DrawImage( g, Back, (int)ImgX, (int)ImgY, Back.Width * MapDotNumber, Back.Height * MapDotNumber );
					
					//g.SmoothingMode = SmoothingMode.HighQuality;
					//g.InterpolationMode = System.Drawing.Drawing2D.InterpolationMode.Default;
					//g.PixelOffsetMode = System.Drawing.Drawing2D.PixelOffsetMode.Half;
					//g.DrawRectangle( Pens.Black, 0, 0, Back.Width * DotNumber, Back.Height * DotNumber );
				}
			}
		}
		g.Restore( gsb );
		
		//绘制动画精灵列表
		/*
		for( int i = SLength - 1; i >= 0; --i ) {
			if( SList[i] != null ) {
				SList[i].Draw( g );
				
				//g.DrawString( MList[i].Owner.Name, textFont, Brushes.Black, Width - 90, 20 + i*20 );
			}
		}
		*/
		
		int NotDrawNumber = 0;
		for( int i = SLength - 1; i >= 0; --i ) {
			if( SList[i] != null ) {
				if( G.SimulateMode && !SList[i].ByCopy ) {
					continue;
				}
				if( !SList[i].NotDraw ) {
					SList[i].NotDraw = true;
				}
				else {
					NotDrawNumber++;
				}
			}
		}
		int TotalNumber = 0;
		SObject so = O_End.O_Pre;
		while( so != O_Start ) {
			if( G.SimulateMode && !so.ByCopy ) {
				//...
			}
			else {
				so.Draw( g );
				so.NotDraw = false;
				TotalNumber++;
			}
			so = so.O_Pre;
		}
		
		bool NeedRefresh = false;
		for( int i = SLength - 1; i >= 0; --i ) {
			if( SList[i] != null ) {
				NeedRefresh |= SList[i].NeedRefresh();
			}
		}
		
		if( NeedRefresh ) {
			//DebugMes += "NeedRefresh\n";
			//Invalidate();
		}
		
		g.Restore( gs );
		g.DrawString( Frame + "FPS", textFont, Brushes.Gray, Width - 90, 10 );
		g.DrawString( ErrFrame + "EPS", textFont, Brushes.DarkOrange, Width - 90, 30 );
		g.DrawString( (CamZ).ToString( "0.0" ), textFont, Brushes.Gray, Width - 90, 70 );
		g.DrawString( "数目:" + TotalNumber, textFont, Brushes.Gray, Width - 90, 90 );
		if( NotDrawNumber != 0 ) {
			g.DrawString( "LOSS:" + NotDrawNumber, textFont, Brushes.Red, Width - 90, 110 );
		}
		
		/*
		SObject so1 = O_End.O_Pre;
		while( so1 != O_Start ) {
			g.DrawRectangle( Pens.Purple, so1.rrr.X, so1.rrr.Y, so1.rrr.Width, so1.rrr.Height );
			g.DrawString( so1.rrr.X + "," + so1.rrr.Y + "," + so1.rrr.Width + "," + so1.rrr.Height, textFont, Brushes.Gray, 100, 100 );
			so1 = so1.O_Pre;
		}
		*/
		
		/*
		string s = "";
		if( O_Start != null ) {
			s += "S" + O_Start.Index + " ";
		}
		if( O_End != null ) {
			s += "E" + O_End.Index;
		}
		g.DrawString( s, textFont, Brushes.Gray, Width - 90, 110 );
		*/
		
		
		if( DebugMes != null ) {
			g.DrawString( DebugMes, textFont, Brushes.OrangeRed, 10, 10 );
		}
		DrawStartMes( g );
		DrawmainStartMes( g );
	}
	
	//显示运行提示信息
	void DrawStartMes( Graphics g )
	{
		if( G.AutoRunMode ) {
			
			if( FirstPress == 0 ) {
				g.ResetTransform();
				g.FillRectangle( CoverBrush, 0, Height/3, Width, Height/3 );
				g.DrawRectangle( Pens.WhiteSmoke, -1, Height/3, Width + 2, Height/3 );
				g.DrawString( "点击鼠标左键开始游戏", TextFont, Brushes.WhiteSmoke, Width/2 - 120, Height / 2 - 20 );
				
				g.DrawString( G.CGPanel.UserMes, n_GUIset.GUIset.Font13, Brushes.WhiteSmoke, 15, Height/3 + 10 );
			}
			if( FirstPress == 1 ) {
				g.ResetTransform();
				g.FillRectangle( CoverBrush, 0, Height/3, Width, Height/3 );
				g.DrawRectangle( Pens.LightGreen, -1, Height/3, Width + 2, Height/3 );
				g.DrawString( "正在启动游戏, 请稍后...", TextFont, Brushes.LightGreen, Width/2 - 120, Height / 2 - 20 );
			}
		}
	}
	
	//显示运行提示信息
	void DrawmainStartMes( Graphics g )
	{
		if( StartRun && !G.SimulateMode ) {
			g.ResetTransform();
			g.FillRectangle( CoverBrush, 0, Height/3, Width, Height/3 );
			g.DrawRectangle( Pens.WhiteSmoke, -1, Height/3, Width + 2, Height/3 );
			g.DrawString( "游戏启动中...", n_GUIset.GUIset.Font13, Brushes.Yellow, 15, Height/3 + 5 );
			g.DrawString( G.CGPanel.UserMes, n_GUIset.GUIset.Font13, Brushes.WhiteSmoke, 15, Height/3 + 35 );
		}
	}
	
	//显示贴图
	void ShowBitBmp( Graphics g )
	{
		//PointF start = GetWorldPoint( 0, 0 );
		//PointF end = GetWorldPoint( Width, Height );
		
		for( int nx = 0; nx < BackWidth; ++nx ) {
			
			float bx = ImgX + nx * MapDotNumber;
			for( int ny = 0; ny < BackHeight; ++ny ) {
				
				float by = ImgY + ny * MapDotNumber;
				
				Color c;
				lock( Back ) {
					c = Back.GetPixel( nx, ny );
				}
				if( c.A == 0 ) {
					continue;
				}
				
				int t = 0;
				if( c.R > c.G && c.R > c.B ) {
					t = 1;
				}
				else if( c.G > c.R && c.G > c.B ) {
					t = 2;
				}
				else if( c.B > c.R && c.B > c.G ) {
					t = 3;
				}
				else {
					//...
				}
				if( t == 0 ) {
					g.DrawImage( myImage0, bx - MapDotNumber/2, by - MapDotNumber/2, MapDotNumber, MapDotNumber );
				}
				if( t == 1 ) {
					g.DrawImage( myImage1, bx - MapDotNumber/2, by - MapDotNumber/2, MapDotNumber, MapDotNumber );
				}
				if( t == 2 ) {
					g.DrawImage( myImage2, bx - MapDotNumber/2, by - MapDotNumber/2, MapDotNumber, MapDotNumber );
				}
				if( t == 3 ) {
					g.DrawImage( myImage3, bx - MapDotNumber/2, by - MapDotNumber/2, MapDotNumber, MapDotNumber );
				}
			}
		}
		
		
		//g.DrawString( nsx + "," + nsy + "," + nex + "," + ney, textFont, Brushes.Black, 10, 10 );
	}
	
	//------------------------------------------------------------
	
	//摄像机系统
	
	PointF GetWorldPoint( float mouseX, float mouseY )
	{
		mouseX = mouseX - Width/2;
		mouseY = mouseY - Height/2;
		
		double R = Math.Cos( -CamAngle * Math.PI/180 );
		double I = Math.Sin( -CamAngle * Math.PI/180 );
		double x = mouseX*R - mouseY*I;
		double y = mouseX*I + mouseY*R;
		
		return new PointF( (float)(CamMidX + x), (float)(CamMidY + y) );
	}
	
	PointF GetWorldPointBBB( float mouseX, float mouseY )
	{
		mouseX = mouseX - Width/2;
		mouseY = mouseY - Height/2;
		
		double R = Math.Cos( -CamAngle * Math.PI/180 );
		double I = Math.Sin( -CamAngle * Math.PI/180 );
		double x = mouseX*R - mouseY*I;
		double y = mouseX*I + mouseY*R;
		
		return new PointF( (float)(CamMidX + x), (float)(CamMidY + y) );
	}
	
	//------------------------------------------------------------
	
	//鼠标按下事件
	void UserMouseDown( object sender, MouseEventArgs e )
	{
		if( G.AutoRunMode && FirstPress == 0 ) {
			FirstPress = 1;
			if( n_SimForm.SimForm.Simlate != null ) {
				n_SimForm.SimForm.Simlate();
			}
		}
		
		this.Select();
		
		if( G.AutoRunMode ) {
			return;
		}
		int x = e.X;
		int y = e.Y;
		
		Last_mX = x;
		Last_mY = y;
		
		MousePoint = GetWorldPoint( x, y );
		
		//如果按下左键
		if( e.Button == MouseButtons.Left ) {
			bool MouseOnObj = false;
			
			for( int i = 0; i < SLength; ++i ) {
				if( SList[i] != null ) {
					
					if( G.SimulateMode && !SList[i].ByCopy ) {
						continue;
					}
					MouseOnObj |= SList[i].LeftMouseDown( MousePoint.X, MousePoint.Y );
					
					if( SList[i].isMousePress ) {
						
						if( SelObj != null ) {
							SelObj.Selected = false;
						}
						SelObj = SList[i];
						SelObj.Selected = true;
					}
					if( MouseOnObj && OperType != OperMoveAll ) {
						break;
					}
				}
			}
			if( !MouseOnObj ) {
				isMousePress = true;
				if( SelObj != null ) {
					SelObj.Selected = false;
				}
				SelObj = null;
			}
			Moved = false;
		}
		//判断是否按下右键
		if( e.Button == MouseButtons.Right ) {
			isRightMousePress = true;
		}
		//判断是否按下鼠标中键
		if( e.Button == MouseButtons.Middle ) {
			
		}
		//界面刷新
		if( !G.SimulateMode ) {
			MyRefresh();
		}
	}
	
	//鼠标松开事件
	void UserMouseUp( object sender, MouseEventArgs em )
	{
		if( G.AutoRunMode ) {
			return;
		}
		int x = em.X;
		int y = em.Y;
		
		MousePoint = GetWorldPoint( x, y );
		
		if( em.Button == MouseButtons.Left ) {
			for( int i = 0; i < SLength; ++i ) {
				if( SList[i] != null ) {
					if( G.SimulateMode && !SList[i].ByCopy ) {
						continue;
					}
					SList[i].LeftMouseUp( MousePoint.X, MousePoint.Y );
				}
			}
			if( !isMousePress ) {
				ResetCal();
			}
			isMousePress = false;
			
			if( Moved ) {
				Moved = false;
				MyRefresh();
			}
		}
		if( em.Button == MouseButtons.Right ) {
			isRightMousePress = false;
		}
		if( em.Button == MouseButtons.Middle ) {
			
		}
		
		//界面刷新
		if( !G.SimulateMode ) {
			MyRefresh();
		}
	}
	
	bool Moved = false;
	
	//鼠标移动事件
	void UserMouseMove( object sender, MouseEventArgs em )
	{
		if( G.AutoRunMode ) {
			return;
		}
		
		int x = em.X;
		int y = em.Y;
		
		MousePoint = GetWorldPoint( x, y );
		
		if( isMousePress ) {
			
			float ox = -(x - Last_mX);
			float oy = -(y - Last_mY);
			double R = Math.Cos( -CamAngle * Math.PI/180 );
			double I = Math.Sin( -CamAngle * Math.PI/180 );
			double dx = ox*R - oy*I;
			double dy = ox*I + oy*R;
			
			CamMidX += (float)dx;
			CamMidY += (float)dy;
			
			Last_mX = x;
			Last_mY = y;
		}
		else {
			for( int i = 0; i < SLength; ++i ) {
				if( SList[i] != null ) {
					if( G.SimulateMode && !SList[i].ByCopy ) {
						continue;
					}
					bool b = SList[i].LeftMouseMove( MousePoint.X, MousePoint.Y, OperType == OperMoveAll );
					Moved |= b;
					if( b && OperType != OperMoveAll ) {
						break;
					}
				}
			}
		}
		if( isRightMousePress ) {
			//...
		}
		
		if( !G.SimulateMode ) {
			MyRefresh();
		}
	}
	
	//鼠标滚轮事件
	void UserMouseWheel( object sender, MouseEventArgs e )
	{
		if( G.AutoRunMode ) {
			return;
		}
		if( isRightMousePress ) {
			if( e.Delta > 0 ) {
				CamAngle += 5;
			}
			else {
				CamAngle -= 5;
			}
		}
		else {
			
			CamZ += e.Delta / 2f;
			CamZ += e.Delta * 2f;
			
			/*
			PointF p = GetWorldPoint( e.X, e.Y );
			
			CamZ += e.Delta / 2f;
			CamZ += e.Delta * 2f;
			
			PointF p1 = GetWorldPoint( e.X, e.Y );
			CamMidX += p.X - p1.X;
			CamMidY += p.Y - p1.Y;
			
			DebugMes += p + "    " + p1 + "\n";
			*/
		}
		
		if( !G.SimulateMode ) {
			MyRefresh();
		}
	}
	
	//尺寸变化时
	void SimPanel_SizeChanged( object sender, EventArgs e )
	{
		CamWidth = Width;
		CamHeight = Height;
	}
	
	//------------------------------------------------------------
	
	//按键按下时
	void SimPanel_KeyDown( object sender, KeyEventArgs e )
	{
		Keyboard.KeyDown( e.KeyValue );
	}
	
	//按键松开时
	void SimPanel_KeyUp( object sender, KeyEventArgs e )
	{
		Keyboard.KeyUp( e.KeyValue );
	}
	
	//关闭
	public void Close()
	{
		
	}
}
}


