﻿
using System;
using System.IO;
using System.Text;

namespace n_SimCommon
{
public static class SimCommon
{
	static byte[] Asc16dot;
	static byte[] Hzk16dot;
	static char[] List;
	
	public static void Init()
	{
		Asc16dot = new byte[16*256];
		Hzk16dot = new byte[32*65536];
		
		//将"ASC16"字库文件读入
		FileStream fsAsc16 = new FileStream( n_OS.OS.SystemRoot + "Resource\\ASC16", FileMode.Open );
		fsAsc16.Seek( 0, SeekOrigin.Begin );
		fsAsc16.Read( Asc16dot, 0, Asc16dot.Length );
		
		//将"HZK16"字库文件读入
		FileStream fsHzk16 = new FileStream( n_OS.OS.SystemRoot + "Resource\\HZK16", FileMode.Open );
		fsHzk16.Seek( 0, SeekOrigin.Begin );
		fsHzk16.Read( Hzk16dot, 0, Hzk16dot.Length );
		
		List = new char[1];
		
		/*
        string s = "";
        for( int i = 0; i < 32; ++i ) {
        	s += "0x" + Hzk16dot[offset + i].ToString( "X" ).PadLeft( 2, '0' ) + ", ";
        }
        
        System.Windows.Forms.MessageBox.Show( s );
        */
	}
	
	//获取一个英文字符的指定像素字节
	public static byte GetAscii( char c, int index )
	{
		return Asc16dot[ (int)c*16 + index ];
	}
	
	//获取一个中文字符的指定像素字节
	public static byte GetHzk( char c, int index )
	{
		List[0] = c;
		byte[] bytes = Encoding.GetEncoding("GB2312").GetBytes( List );
		
        int offset = 0;
        if( bytes.Length >= 2 ) {
        	offset = 32 * (94 * (bytes[0] - 0xA1) + bytes[1] - 0xA1);
        }
        if( offset + index < 0 || offset + index >= Hzk16dot.Length ) {
        	return 0;
        }
		return Hzk16dot[ offset + index ];
	}
}
}



