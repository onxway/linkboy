﻿
using System;
using System.Drawing;
using System.Windows.Forms;
using n_SimPanel;
using n_Sprite;
using n_CommonData;
using n_ImgSetForm;

namespace n_SimForm
{
/// <summary>
/// Description of LocateForm.
/// </summary>
public partial class SimForm : Form
{
	public delegate void D_Simlate();
	public static D_Simlate Simlate;
	
	public SimPanel SPanel;
	
	public static bool USET = false;
	
	n_SimSetForm.SimSetForm SetBox;
	ImgSetForm ImgSetBox;
	
	//构造函数
	public SimForm()
	{
		//
		// The InitializeComponent() call is required for Windows Forms designer support.
		//
		InitializeComponent();
		
		SimPanel.Init();
		
		SPanel = new SimPanel( this );
		G.MyRefresh = ttt; //SPanel.MyRefresh;
		
		if( G.AutoRunMode ) {
			this.Controls.Clear();
			this.Controls.Add( SPanel );
		}
		else {
			this.panel3.Controls.Add( SPanel );
		}
		
		//panelCommon.SendToBack();
		//listBoxName.SendToBack();
		
		this.panel1.SendToBack();
		
		panelValue.Width = 220;
		panelValue.Visible = true;
		
		panelValueMotor.Dock = DockStyle.Fill;
		panelValueSprite.Dock = DockStyle.Fill;
		panelValueBackColor.Dock = DockStyle.Fill;
		panelValueBackImage.Dock = DockStyle.Fill;
		panelValueMap.Dock = DockStyle.Fill;
		
		SPanel.SelObjChanged = SelObjChanged;
		
		//G.BitmapBox.ImageChanged = ImageChanged;
		
		panelValue.BringToFront();
		SPanel.BringToFront();
		
		//手动刷新一下
		SPanel.SelObj = null;
		SelObjChanged();
	}
	
	void ttt()
	{
		
	}
	
	void ImageChanged( Bitmap b, bool bo )
	{
		if( bo ) {
			SPanel.SetBitmap( b );
		}
	}
	
	//根据程序配置设置放缩值, 仅用于GUIcoder中解码
	public void SetInitScale( int i, string ifile, string BackImg, Rectangle sz, bool ExistCam, float CamMidX, float CamMidY, float CamZ, float CamAngle, Color c, int w, int h )
	{
		SimPanel.MapDotNumber = i;
		
		//背景颜色
		SimPanel.myBackColor = c;
		
		//背景图片
		SimPanel.BackIamgePath = BackImg;
		
		SimPanel.BackBmp = null;
		if( BackImg != null ) {
			BackImg = CommonData.Flag_GetRealPath( BackImg );
			if( System.IO.File.Exists( BackImg ) ) {
				SimPanel.BackBmp = new Bitmap( BackImg );
			}
			else {
				n_Debug.Warning.AddErrMessage( "背景图片文件未找到: " + BackImg );
			}
		}
		SimPanel.BackImgX = sz.X;
		SimPanel.BackImgY = sz.Y;
		SimPanel.BackImgWidth = sz.Width;
		SimPanel.BackImgHeight = sz.Height;
		
		SimPanel.UWidth = w;
		SimPanel.UHeight = h;
		
		if( ExistCam ) {
			SimPanel.CamMidX = CamMidX;
			SimPanel.CamMidY = CamMidY;
			SimPanel.CamZ = CamZ;
			SimPanel.CamAngle = CamAngle;
			
			SimPanel.BackCamMidX = CamMidX;
			SimPanel.BackCamMidY = CamMidY;
			SimPanel.BackCamZ = CamZ;
			SimPanel.BackCamAngle = CamAngle;
		}
		//地图
		SimPanel.MapImgFile = ifile;
		if( ifile != null ) {
			ifile = CommonData.Flag_GetRealPath( ifile );
			if( System.IO.File.Exists( ifile ) ) {
				Bitmap b = new Bitmap( ifile );
				SPanel.SetBitmap( new Bitmap( b ) );
				b.Dispose();
			}
			else {
				SPanel.SetBitmap( null );
			}
		}
		else {
			SPanel.SetBitmap( null );
		}
		//Width = panelValue.Width + SimPanel.UWidth;
		//Height = panel1.Height + SimPanel.UHeight;
		
		ClientSize = new Size( panelValue.Width + SimPanel.UWidth, panel1.Height + SimPanel.UHeight );
	}
	
	//运行
	public void Run()
	{
		if( SetBox == null ) {
			SetBox = new n_SimSetForm.SimSetForm();
		}
		
		buttonBColor.BackColor = SimPanel.myBackColor;
		
		SPanel.ResetCal();
		
		panelValue.Visible = true;
		listBoxName.Height = (panelValue.Height) / 3;
		
		this.Visible = true;
		SPanel.Select();
		
		mmTimer1.Start();
		timer1.Enabled = true;
	}
	
	//清除模块列表
	public void Clear()
	{
		listBoxName.Items.Clear();
	}
	
	//添加模块
	public void Add( string Name )
	{
		listBoxName.Items.Add( Name );
	}
	
	//移除模块
	public void Remove( string Name )
	{
		listBoxName.Items.Remove( Name );
	}
	
	void SelObjChanged()
	{
		panelValueMotor.Visible = false;
		panelValueSprite.Visible = false;
		panelValueBackColor.Visible = false;
		panelValueBackImage.Visible = false;
		panelValueMap.Visible = false;
		
		if( SPanel.SelObj == null || SPanel.OperType == SimPanel.OperMoveAll ) {
			
			panelValue.Visible = true;
			listBoxName.SelectedIndex = -1;
			//panelValue.Width = listBoxName.Width;
		}
		else {
			
			for( int i = 0; i < listBoxName.Items.Count; ++i ) {
				if( listBoxName.Items[i].ToString() == SPanel.SelObj.Owner.Name ) {
					listBoxName.SelectedIndex = i;
					break;
				}
			}
			
			//这里隐藏所有模块类型的参数设置面板
			//panelValue.Width = 200;
			
			checkBoxVisible.Checked = SPanel.SelObj.Visible;
			
			if( SPanel.SelObj.Owner.ImageName == n_GUIcoder.SPMoudleName.SYS_G_BackColor ) {
				panelValueBackColor.Visible = true;
				panelValueBackImage.Visible = false;
				panelValueMap.Visible = false;
				
				panelValue.Visible = true;
			}
			else if( SPanel.SelObj.Owner.ImageName == n_GUIcoder.SPMoudleName.SYS_G_BackImage ) {
				
				USET = true;
				textBoxBackImgX.Text = SimPanel.BackImgX.ToString();
				textBoxBackImgY.Text = SimPanel.BackImgY.ToString();
				textBoxBackImgWidth.Text = SimPanel.BackImgWidth.ToString();
				textBoxBackImgHeight.Text = SimPanel.BackImgHeight.ToString();
				USET = false;
				
				panelValueBackColor.Visible = false;
				panelValueBackImage.Visible = true;
				panelValueMap.Visible = false;
				
				panelValue.Visible = true;
			}
			else if( SPanel.SelObj.Owner.ImageName == n_GUIcoder.SPMoudleName.SYS_G_Map ) {
				
				USET = true;
				trackBarDotNumber.Value = SimPanel.MapDotNumber;
				USET = false;
				
				panelValueBackColor.Visible = false;
				panelValueBackImage.Visible = false;
				panelValueMap.Visible = true;
				
				panelValue.Visible = true;
			}
			else if( SPanel.SelObj.Owner.ImageName == n_GUIcoder.SPMoudleName.SYS_View ) {
				
				USET = true;
				//trackBarDotNumber.Value = SimPanel.MapDotNumber;
				USET = false;
				
				panelValueBackColor.Visible = false;
				panelValueBackImage.Visible = false;
				panelValueMap.Visible = true;
				
				panelValue.Visible = true;
			}
			//判断是否为精灵
			else if( SPanel.SelObj is Sprite ) {
				panelValueSprite.Visible = true;
				Sprite sp = (Sprite)SPanel.SelObj;
				textBoxNotHitList.Text = sp.NotHitList;
				trackBarImgRol.Value = (int)sp.imgAngle % 360;
				listBoxImage.Items.Clear();
				for( int i = 0; i < sp.ILength; ++i ) {
					listBoxImage.Items.Add( i + ": " + sp.ImagePathList[i] );
				}
				SetMsStatus( sp.EnableAngle );
			}
			//判断是否为马达设置
			else if( SPanel.SelObj.isMotor ) {
				
				//马达参数刷新
				checkBoxMotorDir.Checked = SPanel.SelObj.SwapPin;
				trackBarPower.Value = SPanel.SelObj.StartPower;
				trackBarSpeedScale.Value = SPanel.SelObj.SpeedDown;
				trackBarInertia.Value = SPanel.SelObj.Inertia;
				
				panelValue.Width = listBoxName.Width + 200;
				panelValueMotor.Visible = true;
			}
			else {
				//...
			}
			panelValue.Visible = true;
		}
	}
	
	//进入仿真运行状态
	public void SimStart()
	{
		timer1.Enabled = false;
		SPanel.Save();
		SPanel.ResetSimVar();
		
		buttonRun.Text = "程序停止";
		buttonRun.BackColor = Color.DarkOrange;
		FormBorderStyle = FormBorderStyle.FixedSingle;
	}
	
	//退出仿真状态
	public void SimEnd()
	{
		buttonRun.Text = "程序运行";
		buttonRun.BackColor = Color.CornflowerBlue;
		FormBorderStyle = FormBorderStyle.Sizable;
		
		SPanel.StartRun = false;
		
		//SPanel.SetInit( false );
		SPanel.Reload();
		timer1.Enabled = true;
	}
	
	//===================================================================================
	
	void ListBoxNameClick(object sender, EventArgs e)
	{
		if( listBoxName.SelectedItem != null ) {
			string Name = listBoxName.SelectedItem.ToString();
			SPanel.SetSelected( Name );
			SPanel.MyRefresh();
		}
	}
	
	void ButtonModuleListClick(object sender, EventArgs e)
	{
		/*
		SPanel.OperType = SimPanel.OperMoveSi;
		
		panelValue.Visible = true;
		panelValue.Width = listBoxName.Width;
		
		panelValueMotor.Visible = false;
		panelValueSprite.Visible = false;
		panelValueBackColor.Visible = false;
		panelValueBackImage.Visible = false;
		panelValueMap.Visible = false;
		*/
		
		if( G.BitmapBox == null ) {
			G.BitmapBox = new n_ImageEditForm.Form1( null );
			G.BitmapBox.SingleMode = false;
		}
		G.BitmapBox.Run( null );
	}
	
	int TimerTick = 0;
	int RunTick = 0;
	void MmTimer1Tick(object sender, EventArgs e)
	{
		TimerTick++;
		if( TimerTick == 100 ) {
			TimerTick = 0;
			SPanel.Frame = SPanel.FrameTick;
			SPanel.FrameTick = 0;
			SPanel.ErrFrame = SPanel.ErrFrameTick;
			SPanel.ErrFrameTick = 0;
		}
		RunTick++;
		if( RunTick == 2 ) {
			RunTick = 0;
			if( G.SimulateMode ) {
				SPanel.RunStep();
			}
		}
		SPanel.MyRefreshTick();
		n_Keyboard.Keyboard.Run();
	}
	
	//开始运行
	void ButtonRunClick(object sender, EventArgs e)
	{
		if( Simlate != null ) {
			Simlate();
		}
		SPanel.Select();
	}
	
	void CheckBoxShowBitImgCheckedChanged(object sender, EventArgs e)
	{
		SPanel.ShowBitImg = checkBoxShowBitImg.Checked;
		SPanel.MyRefresh();
	}
	
	//设置为用户模块
	void ButtonSetUserControlClick(object sender, EventArgs e)
	{
		//SimPanel.UserObj = SPanel.SelObj;
	}
	
	//保存为复位参数
	void ButtonSetInitClick(object sender, EventArgs e)
	{
		/*
		if( G.SimulateMode ) {
			ButtonRunClick( null, null );
		}
		SPanel.SetInit( true );
		
		//if( MessageBox.Show( "您确定要设置当前各个模块的参数作为初始化参数吗?", "更新初始化参数", MessageBoxButtons.YesNo ) == DialogResult.Yes ) {
		//	SPanel.SetInit( true );
		//}
		*/
	}
	
	//环境复位
	void ButtonResetClick(object sender, EventArgs e)
	{
		/*
		if( G.SimulateMode ) {
			ButtonRunClick( null, null );
		}
		SPanel.SetInit( false );
		*/
	}
	
	//视图居中
	void ButtonResetScaleAndLocateClick(object sender, EventArgs e)
	{
		SPanel.GotoCenter();
		SPanel.MyRefresh();
		SPanel.Select();
	}
	
	void ButtonBColorClick(object sender, EventArgs e)
	{
		ColorDialog c = new ColorDialog();
		if( c.ShowDialog() == DialogResult.OK ) {
			SimPanel.myBackColor = c.Color;
			buttonBColor.BackColor = SimPanel.myBackColor;
		}
	}
	
	//===================================================================================
	
	void ShowImgForm( int type )
	{
		if( ImgSetBox == null ) {
			ImgSetBox = new ImgSetForm( this, SPanel );
		}
		ImgSetBox.Run( type );
	}
	
	//===================================================================================
	
	//窗体关闭时
	void LocateFormFormClosing(object sender, FormClosingEventArgs e)
	{
		//编译状态下禁止关闭退出
		if( G.AutoRunMode && SimPanel.FirstPress == 1 ) {
			e.Cancel = true;
			return;
		}
		
		if( !G.AutoRunMode || SimPanel.FirstPress == 1 ) {
			this.Visible = false;
			e.Cancel = true;
		}
		mmTimer1.Stop();
		timer1.Enabled = false;
	}
	
	void Button1Click(object sender, EventArgs e)
	{
		SPanel.OperType = SimPanel.OperMoveSi;
		SPanel.Select();
	}
	
	void ButtonAllClick(object sender, EventArgs e)
	{
		SPanel.OperType = SimPanel.OperMoveAll;
		SPanel.Select();
	}
	
	void ButtonClearClick(object sender, EventArgs e)
	{
		SPanel.ClearBack();
	}
	
	void TrackBarDotNumberValueChanged(object sender, EventArgs e)
	{
		if( USET ) return;
		
		int f = trackBarDotNumber.Value;
		textBoxMapScale.Text = f.ToString();
		SPanel.ChangeImgDotNumber( f );
		SPanel.MyRefresh();
	}
	
	//===================================================================================
	
	//编辑地图
	void ButtonMapEditClick(object sender, EventArgs e)
	{
		//G.BitmapBox.Run( SPanel.GetBitmap() );
	}
	
	//导入地图
	void ButtonLoadImageClick(object sender, EventArgs e)
	{
		ShowImgForm( 3 );
	}
	
	//清除地图
	void ButtonClearMapClick(object sender, EventArgs e)
	{
		SimPanel.MapImgFile = null;
	}
	
	//===================================================================================
	//角色属性
	
	//添加图片
	void ButtonAddImageClick(object sender, EventArgs e)
	{
		ShowImgForm( 1 );
	}
	
	//清空图片
	void ButtonClearImageClick(object sender, EventArgs e)
	{
		Sprite sp = (Sprite)SPanel.SelObj;
		sp.ClearImage();
		listBoxImage.Items.Clear();
	}
	
	void ListBoxImageClick(object sender, EventArgs e)
	{
		Sprite sp = (Sprite)SPanel.SelObj;
		sp.SetImage( listBoxImage.SelectedIndex );
	}
	
	void TextBoxNotHitListTextChanged(object sender, EventArgs e)
	{
		Sprite sp = (Sprite)SPanel.SelObj;
		sp.NotHitList = textBoxNotHitList.Text;
	}
	
	void TrackBarImgRolScroll(object sender, EventArgs e)
	{
		Sprite sp = (Sprite)SPanel.SelObj;
		sp.imgAngle = trackBarImgRol.Value % 360;
	}
	
	void ButtonAngleEnableClick(object sender, EventArgs e)
	{
		Sprite sp = (Sprite)SPanel.SelObj;
		sp.EnableAngle = !sp.EnableAngle;
		sp.EnableRolXY = sp.EnableAngle;
		sp.EnableSize = sp.EnableAngle;
		
		SetMsStatus( sp.EnableAngle );
	}
	
	//设置鼠标拖动调节状态
	void SetMsStatus( bool b )
	{
		if( b ) {
			buttonAngleEnable.BackColor = Color.CornflowerBlue;
			buttonAngleEnable.Text = "关闭鼠标拖动调节";
		}
		else {
			buttonAngleEnable.BackColor = Color.Gainsboro;
			buttonAngleEnable.Text = "开启鼠标拖动调节";
		}
	}
	
	//===================================================================================
	//公共基础属性设置
	
	void CheckBoxVisibleCheckedChanged(object sender, EventArgs e)
	{
		SPanel.SelObj.Visible = checkBoxVisible.Checked;
		SPanel.MyRefresh();
	}
	
	//马达转速设置
	
	void CheckBoxMotorDirCheckedChanged(object sender, EventArgs e)
	{
		SPanel.SelObj.SwapPin = checkBoxMotorDir.Checked;
	}
	
	void TrackBarPowerScroll(object sender, EventArgs e)
	{
		SPanel.SelObj.StartPower = trackBarPower.Value;
	}
	
	void TrackBarSpeedScaleScroll(object sender, EventArgs e)
	{
		SPanel.SelObj.SpeedDown = trackBarSpeedScale.Value;
	}
	
	void TrackBarInertiaScroll(object sender, EventArgs e)
	{
		SPanel.SelObj.Inertia = trackBarInertia.Value;
	}
	
	//===================================================================================
	//背景图片属性
	
	//设置背景图片
	void ButtonBackImageClick(object sender, EventArgs e)
	{
		ShowImgForm( 2 );
	}
	
	//清除图片
	void ButtonClearImgClick(object sender, EventArgs e)
	{
		SimPanel.BackIamgePath = null;
	}
	
	void TextBoxBackImgXTextChanged(object sender, EventArgs e)
	{
		if( USET ) return;
		
		if( textBoxBackImgX.Text == "" ) {
			return;
		}
		try {
			SimPanel.BackImgX = int.Parse( textBoxBackImgX.Text );
		}
		catch {
			MessageBox.Show( "请输入 X 数值, 不能包含非数字符号" );
		}
	}
	
	void TextBoxBackImgYTextChanged(object sender, EventArgs e)
	{
		if( USET ) return;
		
		if( textBoxBackImgY.Text == "" ) {
			return;
		}
		try {
			SimPanel.BackImgY = int.Parse( textBoxBackImgY.Text );
		}
		catch {
			MessageBox.Show( "请输入 Y 数值, 不能包含非数字符号" );
		}
	}
	
	void TextBoxBackImgWidthTextChanged(object sender, EventArgs e)
	{
		if( USET ) return;
		
		if( textBoxBackImgWidth.Text == "" ) {
			return;
		}
		try {
			SimPanel.BackImgWidth = int.Parse( textBoxBackImgWidth.Text );
		}
		catch {
			MessageBox.Show( "请输入宽度数值, 不能包含非数字符号" );
		}
	}
	
	void TextBoxBackImgHeightTextChanged(object sender, EventArgs e)
	{
		if( USET ) return;
		
		if( textBoxBackImgHeight.Text == "" ) {
			return;
		}
		try {
			SimPanel.BackImgHeight = int.Parse( textBoxBackImgHeight.Text );
		}
		catch {
			MessageBox.Show( "请输入高度数值, 不能包含非数字符号" );
		}
	}
	
	//恢复原始大小
	void ButtonBackImgYSizeClick(object sender, EventArgs e)
	{
		if( SimPanel.BackBmp != null ) {
			textBoxBackImgWidth.Text = SimPanel.BackBmp.Width.ToString();
			textBoxBackImgHeight.Text = SimPanel.BackBmp.Height.ToString();
		}
	}
	
	//===================================================================================
	
	void Timer1Tick(object sender, EventArgs e)
	{
		if( SPanel.SelObj != null && SPanel.SelObj is Sprite ) {
			Sprite sp = (Sprite)SPanel.SelObj;
			labelSpriteValue.Text = "MidX: " + sp.MidX + "\nMidY: " + sp.MidY + "\nZ: " + sp.Z + "\nAngle: " + sp.Angle;
		}
		
		if( !G.SimulateMode && Visible ) {
			SPanel.SetInit();
		}
		else {
			//SPanel.SetInit0( false );
		}
		//可以触发一次刷新
		//G.CGPanel.EnableOnceRefresh = true;
	}
	
	//注意: 最小化时候也会触发这个事件!!!!!
	void SimFormSizeChanged(object sender, EventArgs e)
	{
		//个别电脑兼容性!!!! 可能构造窗体时就运行
		if( SPanel == null ) {
			return;
		}
		//屏蔽窗口最小化事件
		if( SPanel.Width < 10 || SPanel.Height < 10 ) {
			return;
		}
		listBoxName.Height = (panelValue.Height) / 3;
		
		SimPanel.UWidth = SPanel.Width;
		SimPanel.UHeight = SPanel.Height;
	}
	
	public bool isAct;
	
	void SimFormActivated(object sender, EventArgs e)
	{
		isAct = !SetBox.checkBoxMainRefresh.Checked;
	}
	
	void SimFormDeactivate(object sender, EventArgs e)
	{
		isAct = false;
	}
	
	//高级设置
	void ButtonSettingClick(object sender, EventArgs e)
	{
		SetBox.Run();
	}
}
}



