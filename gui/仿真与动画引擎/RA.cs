﻿

//注意 本模块需要升级成原生坐标系, 即Y轴向下

namespace n_RA
{
using System;

public static class RA
{
	//根据传入的坐标计算对应的角度和半径
	public static float GetR( out float a, float x, float y )
	{
		float R = (float)Math.Sqrt( x*x + y*y );
		if( R < 0.001f ) R = 0.001f;
		
		float aa = (float)Math.Acos( x / R );
		if( y > 0 ) {
			aa = (float)Math.PI * 2 - aa;
		}
		a = (float)(aa * 180/Math.PI);
		return R;
	}
	
	//根据角度和距离计算坐标
	public static void GetXY( out float x, out float y, float A, float R )
	{
		x = (float)(R * Math.Cos( A * Math.PI/180 ));
		y = -(float)(R * Math.Sin( A * Math.PI/180 ));
	}
	
	//计算一个坐标旋转指定角度之后的坐标
	public static void GetRolXY( out float x, out float y, float ox, float oy, float a )
	{
		float na = 0;
		float r = GetR( out na, ox, oy );
		
		GetXY( out x, out y, na + a, r );
	}
}
}



