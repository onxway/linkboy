﻿
using System;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Windows.Forms;

using n_ALG;
using n_VarType;

namespace n_HeapSim
{
//堆栈仿真类
public static class HeapSim
{
	static int Heap_Addr;
	
	static int HID_Addr;
	static int HID_len;
	
	//初始化
	public static void Init()
	{
		
	}
	
	//仿真复位
	public static void ResetSim()
	{
		n_MyObject.MyObject.GroupReplace = true;
		Heap_Addr = n_VarList.VarList.GetStaticIndex( "#.sys_DString.heap.buffer" );
		HID_Addr = n_VarList.VarList.GetStaticIndex( "#.sys_DString.heap.HID" );
		HID_len = n_VarList.VarList.GetStaticIndex( "#.sys_DString.heap.HIDLength" );
		n_MyObject.MyObject.GroupReplace = false;
		
		Heap_Addr = int.Parse( n_VarList.VarList.GetAddr( Heap_Addr ) );
		HID_Addr = int.Parse( n_VarList.VarList.GetAddr( HID_Addr ) );
		HID_len = int.Parse( n_VarList.VarList.Get( HID_len ).Value );
	}
	
	//绘制仿真效果
	public static void Draw( Graphics g )
	{
		int H = n_Head.Head.HeadLabelHeight - 6;
		
		int SX = 860;
		int SY = 2;
		int wt = 5;
		int W = 16 * wt;
		//g.FillRectangle( Brushes.WhiteSmoke, SX - 5, SY - 5, W + 10, W + 10 );
		//g.DrawRectangle( Pens.Silver, SX - 5, SY - 5, W + 10, W + 10 );
		
		string r = " HID " + HID_Addr + ": ";
		int x = SX;
		int y = SY;
		int k = 0;
		for( int i = 0; i < HID_len; ++i ) {
			byte b = n_UserModule.UserModule.BASE[ HID_Addr + i ];
			r += b + " ";
			
			for( int n = 0; n < 8; ++n ) {
				if( (b & 0x01) != 0 ) {
					g.FillRectangle( Brushes.Green, x, y, wt - 1, wt - 1 );
					g.FillRectangle( Brushes.LimeGreen, x + 1, y + 1, wt - 3, wt - 3 );
				}
				else {
					g.FillRectangle( Brushes.Gainsboro, x, y, wt - 1, wt - 1 );
					g.FillRectangle( Brushes.DarkGray, x + 1, y + 1, wt - 3, wt - 3 );
					//g.DrawRectangle( Pens.Silver, x, y, wt - 1, wt - 1 );
				}
				b >>= 1;
				y += wt;
				k++;
				if( k % 10 == 0 ) {
					y = SY;
					x += wt;
					if( k % 100 == 0 ) {
						//x += 2;
					}
				}
			}
		}
		r += "\nHeap " + Heap_Addr + ": ";
		for( int i = 0; i < 40; ++i ) {
			byte b = n_UserModule.UserModule.BASE[ Heap_Addr + i ];
			r += b + " ";
		}
		g.DrawString( r, n_GUIset.GUIset.ExpFont, Brushes.Black, SX, SY - 40 );
		
		g.DrawString( "动态内存:\n" + (HID_len*16) + " Byte", n_GUIset.GUIset.ExpFont, Brushes.ForestGreen, SX - 90, SY + 4 );
	}
}
}


