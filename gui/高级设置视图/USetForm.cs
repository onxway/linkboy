﻿
using System;
using System.Drawing;
using System.Windows.Forms;
using n_LocatePanel;

namespace n_USetForm
{
/// <summary>
/// Description of LocateForm.
/// </summary>
public partial class USetForm : Form
{
	int Tick;
	
	public USetForm()
	{
		//
		// The InitializeComponent() call is required for Windows Forms designer support.
		//
		InitializeComponent();
		
		Tick = 0;
		
		if( n_Language.Language.isChinese ) {
			buttonTranslate.Visible = false;
		}
	}
	
	//运行
	public void Run()
	{
		/*
		//判断是否需要更新编译信息
		if( n_CodeData.CodeData.isChanged ) {
			n_CodeData.CodeData.isChanged = false;
			
			//获取软件堆栈起始地址
			int VdataIndex = n_VdataList.VdataList.GetIndex( n_VarType.VarType.VBase );
			int SoftSP_StartAddress = n_AddressList.AddressList.StaticMaxAddr[ VdataIndex ] + n_Interrupt.Interrupt.GetStackNumber() + n_ImagePanel.ImagePanel.StartNeedNumber;
			//int StartAddress = n_VdataList.VdataList.Get( VdataIndex ).StartAddressConst;
			//int EndAddress = n_VdataList.VdataList.Get( VdataIndex ).EndAddress;
			
			labelROM.Text = "已用FLASH空间: " + n_CodeData.CodeData.TotalByteNumber + "字节 / 共计" + G.CGPanel.R1 + "字节\n" +
								"已用RAM空间: " + SoftSP_StartAddress + "字节 / 共计" + G.CGPanel.R2 + "字节";
								//"中断空间: " + EndAddress + "\n" +
			
		}
		*/
		int voffset = G.CGPanel.voffset;
		int EventNumber = G.CGPanel.EventNumber;
		int EventVarStack = G.CGPanel.EventVarStack;
		int EventFuncStack = G.CGPanel.EventFuncStack;
		int VEXTick = G.CGPanel.VEX_Tick;
		int DRAM_Block = G.CGPanel.DRAM_Block;
		
		this.textBoxVOffset.Text = voffset.ToString();
		
		this.textBoxEventNumber.Text = EventNumber.ToString();
		this.textBoxVarStack.Text = EventVarStack.ToString();
		this.textBoxFuncStack.Text = EventFuncStack.ToString();
		
		this.textBoxVEX_Tick.Text = VEXTick.ToString();
		this.textBoxBDRAM.Text = DRAM_Block.ToString();
		
		this.checkBoxDisableHit.Checked = G.CGPanel.DisableHit;
		this.checkBoxHideLine.Checked = G.CGPanel.HideLine != 0;
		this.checkBoxHidName.Checked = G.CGPanel.HideName;
		
		if( G.CGPanel.OSPath != null ) {
			this.textBoxOS.Text = G.CGPanel.OSPath;
		}
		if( G.CGPanel.SysMes != null ) {
			this.textBoxSysMes.Text = G.CGPanel.SysMes;
		}
		this.Visible = true;
	}
	
	void LocateFormFormClosing(object sender, FormClosingEventArgs e)
	{
		this.Visible = false;
		e.Cancel = true;
	}
	
	void ButtonOKClick(object sender, EventArgs e)
	{
		/*
		int o = 0;
		int EventNumber = 0;
		int VEX_Tick = 0;
		
		//-----------------------------------------------------------
		try {
			o = int.Parse( textBoxVOffset.Text );
		}
		catch {
			MessageBox.Show( "请输入数字: " + textBoxVOffset.Text );
			return;
		}
		try {
			EventNumber = int.Parse( textBoxEventNumber.Text );
		}
		catch {
			MessageBox.Show( "请输入数字: " + textBoxEventNumber.Text );
			return;
		}
		try {
			VEX_Tick = int.Parse( textBoxVEX_Tick.Text );
		}
		catch {
			MessageBox.Show( "请输入数字: " + textBoxVEX_Tick.Text );
			return;
		}
		
		//-----------------------------------------------------------
		if( o > 100 ) {
			MessageBox.Show( "软加速器参数不能大于100: " + o );
			return;
		}
		if( EventNumber > 50 ) {
			MessageBox.Show( "事件池数目不能大于50: " + EventNumber );
			return;
		}
		if( EventNumber < 2 ) {
			MessageBox.Show( "事件池数目不能小于2: " + EventNumber );
			return;
		}
		if( VEX_Tick < 1 ) {
			MessageBox.Show( "VEXTick不能小于1: " + VEX_Tick );
			return;
		}
		G.CGPanel.voffset = o;
		G.CGPanel.EventNumber = EventNumber;
		G.CGPanel.VEX_Tick = VEX_Tick;
		
		if( textBoxOS.Text != "" ) {
			G.CGPanel.OSPath = textBoxOS.Text;
		}
		this.Visible = false;
		*/
	}
	
	void ButtonPackClick(object sender, EventArgs e)
	{
		G.CGPanel.Pack( this.textBoxHidMod.Text );
	}
	
	void ButtonUnpackClick(object sender, EventArgs e)
	{
		G.CGPanel.UnPack();
	}
	
	void ButtonOutputModClick(object sender, EventArgs e)
	{
		//保存文件对话框
		SaveFileDialog SaveFileDlg = new SaveFileDialog();
		SaveFileDlg.Filter = "模型文件(*.mox)|*.mox";
		SaveFileDlg.Title = "文件另存为...";
		
		SaveFileDlg.FileName = G.ccode.FileName;
		DialogResult dlgResult = SaveFileDlg.ShowDialog();
		if(dlgResult == DialogResult.OK) {
			G.ccode.Save();
			//n_SharpZip.SharpZip.ZipDirectory( G.ccode.FilePath, SaveFileDlg.FileName, "a40307891" );
		}
	}
	
	void ButtonGroupClick(object sender, EventArgs e)
	{
		string gname = textBoxGroupName.Text;
		if( textBoxVersion.Text != "" ) {
			gname = textBoxGroupName.Text + n_GroupList.Group.SPLIT + textBoxVersion.Text;
		}
		
		foreach( n_MyObject.MyObject m in G.CGPanel.myModuleList ) {
			if( !m.isSelect ) {
				continue;
			}
			m.GroupMes = gname;
		}
		if( G.CGPanel.mGroupList.FindGroup( textBoxGroupName.Text ) == null ) {
			G.CGPanel.mGroupList.AddGroup( gname );
		}
		G.CGPanel.MyRefresh();
	}
	
	void ButtonCancelGroupClick(object sender, EventArgs e)
	{
		foreach( n_MyObject.MyObject m in G.CGPanel.myModuleList ) {
			if( !m.isSelect ) {
				continue;
			}
			m.GroupMes = null;
		}
		G.CGPanel.MyRefresh();
	}
	
	void ButtonTranslateClick(object sender, EventArgs e)
	{
		G.CGPanel.Translate();
	}
	
	void ButtonSetNoteClick(object sender, EventArgs e)
	{
		foreach( n_MyObject.MyObject m in G.CGPanel.myModuleList ) {
			if( !m.isSelect ) {
				continue;
			}
			if( m is n_MyIns.MyIns ) {
				n_MyIns.MyIns mi = (n_MyIns.MyIns)m;
				mi.isNote = true;
			}
		}
		n_ImagePanel.ImagePanel.RefreshTick++;
		G.CGPanel.MyRefresh();
	}
	
	void ButtonSetCodeClick(object sender, EventArgs e)
	{
		foreach( n_MyObject.MyObject m in G.CGPanel.myModuleList ) {
			if( !m.isSelect ) {
				continue;
			}
			if( m is n_MyIns.MyIns ) {
				n_MyIns.MyIns mi = (n_MyIns.MyIns)m;
				mi.isNote = false;
			}
		}
		n_ImagePanel.ImagePanel.RefreshTick++;
		G.CGPanel.MyRefresh();
	}
	
	void CheckBoxDisableHitCheckedChanged(object sender, EventArgs e)
	{
		G.CGPanel.DisableHit = this.checkBoxDisableHit.Checked;
	}
	
	void TextBoxVOffsetTextChanged(object sender, EventArgs e)
	{
		if( textBoxVOffset.Text.Trim( ' ' ) == "" ) {
			return;
		}
		int o = 0;
		
		try {
			o = int.Parse( textBoxVOffset.Text );
		}
		catch {
			MessageBox.Show( "请输入数字: " + textBoxVOffset.Text );
			return;
		}
		
		if( o > 100 ) {
			MessageBox.Show( "软加速器参数不能大于100: " + o );
			return;
		}
		
		G.CGPanel.voffset = o;
	}
	
	//--------------------------------------
	
	void TextBoxEventNumberTextChanged(object sender, EventArgs e)
	{
		if( textBoxEventNumber.Text.Trim( ' ' ) == "" ) {
			return;
		}
		int EventNumber = 0;
		
		try {
			EventNumber = int.Parse( textBoxEventNumber.Text );
		}
		catch {
			MessageBox.Show( "请输入数字: " + textBoxEventNumber.Text );
			return;
		}
		
		if( EventNumber > 50 ) {
			MessageBox.Show( "事件池数目不能大于50: " + EventNumber );
			return;
		}
		if( EventNumber < 2 ) {
			MessageBox.Show( "事件池数目不能小于2: " + EventNumber );
			return;
		}
		
		G.CGPanel.EventNumber = EventNumber;
	}
	
	void TextBoxVarStackTextChanged(object sender, EventArgs e)
	{
		if( textBoxVarStack.Text.Trim( ' ' ) == "" ) {
			return;
		}
		int EventVarStack = 0;
		
		try {
			EventVarStack = int.Parse( textBoxVarStack.Text );
		}
		catch {
			MessageBox.Show( "请输入数字: " + textBoxVarStack.Text );
			return;
		}
		G.CGPanel.EventVarStack = EventVarStack;
	}
	
	void TextBoxFuncStackTextChanged(object sender, EventArgs e)
	{
		if( textBoxFuncStack.Text.Trim( ' ' ) == "" ) {
			return;
		}
		int EventFuncStack = 0;
		
		try {
			EventFuncStack = int.Parse( textBoxFuncStack.Text );
		}
		catch {
			MessageBox.Show( "请输入数字: " + textBoxFuncStack.Text );
			return;
		}
		G.CGPanel.EventFuncStack = EventFuncStack;
	}
	
	//--------------------------------------
	
	void TextBoxVEX_TickTextChanged(object sender, EventArgs e)
	{
		if( textBoxVEX_Tick.Text.Trim( ' ' ) == "" ) {
			return;
		}
		int VEX_Tick = 0;
		
		try {
			VEX_Tick = int.Parse( textBoxVEX_Tick.Text );
		}
		catch {
			MessageBox.Show( "请输入数字: " + textBoxVEX_Tick.Text );
			return;
		}
		
		if( VEX_Tick < 1 ) {
			MessageBox.Show( "VEXTick不能小于1: " + VEX_Tick );
			return;
		}
		
		G.CGPanel.VEX_Tick = VEX_Tick;
	}
	
	void TextBoxBDRAMTextChanged(object sender, EventArgs e)
	{
		if( textBoxBDRAM.Text.Trim( ' ' ) == "" ) {
			return;
		}
		int block = 0;
		
		try {
			block = int.Parse( textBoxBDRAM.Text );
		}
		catch {
			MessageBox.Show( "请输入数字: " + textBoxBDRAM );
			return;
		}
		
		if( block < 0 ) {
			MessageBox.Show( "VEXTick不能小于0: " + block );
			return;
		}
		
		G.CGPanel.DRAM_Block = block;
	}
	
	void CheckBoxHideLineCheckedChanged(object sender, EventArgs e)
	{
		G.CGPanel.HideLine = (this.checkBoxHideLine.Checked?1:0);
		G.CGPanel.MyRefresh();
	}
	
	void CheckBoxZheXianCheckedChanged(object sender, EventArgs e)
	{
		if( n_HardModule.Port.C_Rol == 0 ) {
			n_HardModule.Port.C_Rol = 50;
		}
		else {
			n_HardModule.Port.C_Rol = 0;
			G.CGPanel.myModuleList.AdjToCross();
		}
		G.CGPanel.myModuleList.NeedDrawLineMap = true;
		G.CGPanel.MyRefresh();
	}
	
	void CheckBoxHidNameCheckedChanged(object sender, EventArgs e)
	{
		G.CGPanel.HideName = this.checkBoxHidName.Checked;
		G.CGPanel.MyRefresh();
	}
	
	void Label15Click(object sender, EventArgs e)
	{
		Tick++;
		if( Tick == 3 ) {
			panelMe.Visible = true;
			this.FormBorderStyle = FormBorderStyle.Sizable;
		}
	}
	
	void ButtonOpenDirClick(object sender, EventArgs e)
	{
		string fileToSelect = G.ccode.PathAndName;
		
		string args = string.Format("/Select, {0}", fileToSelect);
		System.Diagnostics.Process.Start( new System.Diagnostics.ProcessStartInfo("Explorer.exe", args ) );
	}
	
	void LabelMoreClick(object sender, EventArgs e)
	{
		labelMore.Visible = false;
		Height = n_GUIset.GUIset.GetPix( 659 ); //721;
	}
}
}

