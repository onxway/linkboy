﻿/*
 * 由SharpDevelop创建。
 * 用户： cap_gpu
 * 日期: 2017/4/9
 * 时间: 8:03
 * 
 * 要改变这种模板请点击 工具|选项|代码编写|编辑标准头文件
 */
namespace n_USetForm
{
	partial class USetForm
	{
		/// <summary>
		/// Designer variable used to keep track of non-visual components.
		/// </summary>
		private System.ComponentModel.IContainer components = null;
		
		/// <summary>
		/// Disposes resources used by the form.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing) {
				if (components != null) {
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}
		
		/// <summary>
		/// This method is required for Windows Forms designer support.
		/// Do not change the method contents inside the source code editor. The Forms designer might
		/// not be able to load this method if it was changed manually.
		/// </summary>
		private void InitializeComponent()
		{
			this.label1 = new System.Windows.Forms.Label();
			this.textBoxVOffset = new System.Windows.Forms.TextBox();
			this.label2 = new System.Windows.Forms.Label();
			this.panel2 = new System.Windows.Forms.Panel();
			this.buttonPack = new System.Windows.Forms.Button();
			this.buttonUnpack = new System.Windows.Forms.Button();
			this.panelMe = new System.Windows.Forms.Panel();
			this.textBoxOS = new System.Windows.Forms.TextBox();
			this.label3 = new System.Windows.Forms.Label();
			this.textBoxSysMes = new System.Windows.Forms.TextBox();
			this.label4 = new System.Windows.Forms.Label();
			this.textBoxHidMod = new System.Windows.Forms.TextBox();
			this.buttonOutputMod = new System.Windows.Forms.Button();
			this.buttonGroup = new System.Windows.Forms.Button();
			this.buttonCancelGroup = new System.Windows.Forms.Button();
			this.textBoxGroupName = new System.Windows.Forms.TextBox();
			this.buttonTranslate = new System.Windows.Forms.Button();
			this.label5 = new System.Windows.Forms.Label();
			this.panel1 = new System.Windows.Forms.Panel();
			this.label26 = new System.Windows.Forms.Label();
			this.label24 = new System.Windows.Forms.Label();
			this.label25 = new System.Windows.Forms.Label();
			this.textBoxFuncStack = new System.Windows.Forms.TextBox();
			this.label22 = new System.Windows.Forms.Label();
			this.label23 = new System.Windows.Forms.Label();
			this.textBoxVarStack = new System.Windows.Forms.TextBox();
			this.label8 = new System.Windows.Forms.Label();
			this.label6 = new System.Windows.Forms.Label();
			this.textBoxEventNumber = new System.Windows.Forms.TextBox();
			this.label7 = new System.Windows.Forms.Label();
			this.panel3 = new System.Windows.Forms.Panel();
			this.textBoxVersion = new System.Windows.Forms.TextBox();
			this.label17 = new System.Windows.Forms.Label();
			this.label11 = new System.Windows.Forms.Label();
			this.label21 = new System.Windows.Forms.Label();
			this.panel4 = new System.Windows.Forms.Panel();
			this.label9 = new System.Windows.Forms.Label();
			this.buttonSetNote = new System.Windows.Forms.Button();
			this.buttonSetCode = new System.Windows.Forms.Button();
			this.checkBoxDisableHit = new System.Windows.Forms.CheckBox();
			this.panel5 = new System.Windows.Forms.Panel();
			this.label10 = new System.Windows.Forms.Label();
			this.label12 = new System.Windows.Forms.Label();
			this.textBoxVEX_Tick = new System.Windows.Forms.TextBox();
			this.label13 = new System.Windows.Forms.Label();
			this.label14 = new System.Windows.Forms.Label();
			this.label15 = new System.Windows.Forms.Label();
			this.panel6 = new System.Windows.Forms.Panel();
			this.label20 = new System.Windows.Forms.Label();
			this.checkBoxHidName = new System.Windows.Forms.CheckBox();
			this.label19 = new System.Windows.Forms.Label();
			this.checkBoxZheXian = new System.Windows.Forms.CheckBox();
			this.label18 = new System.Windows.Forms.Label();
			this.checkBoxHideLine = new System.Windows.Forms.CheckBox();
			this.label16 = new System.Windows.Forms.Label();
			this.panel7 = new System.Windows.Forms.Panel();
			this.label27 = new System.Windows.Forms.Label();
			this.buttonOpenDir = new System.Windows.Forms.Button();
			this.panel8 = new System.Windows.Forms.Panel();
			this.label28 = new System.Windows.Forms.Label();
			this.label29 = new System.Windows.Forms.Label();
			this.textBoxBDRAM = new System.Windows.Forms.TextBox();
			this.label30 = new System.Windows.Forms.Label();
			this.labelMore = new System.Windows.Forms.Label();
			this.panel2.SuspendLayout();
			this.panelMe.SuspendLayout();
			this.panel1.SuspendLayout();
			this.panel3.SuspendLayout();
			this.panel4.SuspendLayout();
			this.panel5.SuspendLayout();
			this.panel6.SuspendLayout();
			this.panel7.SuspendLayout();
			this.panel8.SuspendLayout();
			this.SuspendLayout();
			// 
			// label1
			// 
			this.label1.Font = new System.Drawing.Font("微软雅黑", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
			this.label1.Location = new System.Drawing.Point(12, 10);
			this.label1.Name = "label1";
			this.label1.Size = new System.Drawing.Size(124, 26);
			this.label1.TabIndex = 9;
			this.label1.Text = "软加速器参数：";
			// 
			// textBoxVOffset
			// 
			this.textBoxVOffset.Font = new System.Drawing.Font("微软雅黑", 12F);
			this.textBoxVOffset.Location = new System.Drawing.Point(164, 7);
			this.textBoxVOffset.Name = "textBoxVOffset";
			this.textBoxVOffset.Size = new System.Drawing.Size(65, 29);
			this.textBoxVOffset.TabIndex = 10;
			this.textBoxVOffset.Text = "0";
			this.textBoxVOffset.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
			this.textBoxVOffset.TextChanged += new System.EventHandler(this.TextBoxVOffsetTextChanged);
			// 
			// label2
			// 
			this.label2.Font = new System.Drawing.Font("微软雅黑", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
			this.label2.ForeColor = System.Drawing.Color.SlateGray;
			this.label2.Location = new System.Drawing.Point(12, 39);
			this.label2.Name = "label2";
			this.label2.Size = new System.Drawing.Size(632, 93);
			this.label2.TabIndex = 11;
			this.label2.Text = "当用户模块过多时可能会拖慢系统运行速度，例如延时1秒指令，实际延时时间可能会大于1秒。这时可通过软加速器进行提速。软加速器参数范围是0-100，默认为0不加速。如" +
			"果感觉延时指令的延时时间比实际稍长，可以适当增加这个参数，使实际的延时时间等于延时指令里的设置时间。";
			// 
			// panel2
			// 
			this.panel2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(211)))), ((int)(((byte)(243)))), ((int)(((byte)(243)))));
			this.panel2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
			this.panel2.Controls.Add(this.label1);
			this.panel2.Controls.Add(this.textBoxVOffset);
			this.panel2.Controls.Add(this.label2);
			this.panel2.Location = new System.Drawing.Point(763, 601);
			this.panel2.Name = "panel2";
			this.panel2.Size = new System.Drawing.Size(654, 137);
			this.panel2.TabIndex = 14;
			// 
			// buttonPack
			// 
			this.buttonPack.BackColor = System.Drawing.Color.YellowGreen;
			this.buttonPack.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
			this.buttonPack.Font = new System.Drawing.Font("微软雅黑", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
			this.buttonPack.ForeColor = System.Drawing.Color.White;
			this.buttonPack.Location = new System.Drawing.Point(851, 420);
			this.buttonPack.Name = "buttonPack";
			this.buttonPack.Size = new System.Drawing.Size(82, 36);
			this.buttonPack.TabIndex = 14;
			this.buttonPack.Text = "封装";
			this.buttonPack.UseVisualStyleBackColor = false;
			this.buttonPack.Click += new System.EventHandler(this.ButtonPackClick);
			// 
			// buttonUnpack
			// 
			this.buttonUnpack.BackColor = System.Drawing.Color.YellowGreen;
			this.buttonUnpack.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
			this.buttonUnpack.Font = new System.Drawing.Font("微软雅黑", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
			this.buttonUnpack.ForeColor = System.Drawing.Color.White;
			this.buttonUnpack.Location = new System.Drawing.Point(763, 420);
			this.buttonUnpack.Name = "buttonUnpack";
			this.buttonUnpack.Size = new System.Drawing.Size(82, 36);
			this.buttonUnpack.TabIndex = 15;
			this.buttonUnpack.Text = "解封";
			this.buttonUnpack.UseVisualStyleBackColor = false;
			this.buttonUnpack.Click += new System.EventHandler(this.ButtonUnpackClick);
			// 
			// panelMe
			// 
			this.panelMe.Controls.Add(this.textBoxOS);
			this.panelMe.Controls.Add(this.label3);
			this.panelMe.Controls.Add(this.textBoxSysMes);
			this.panelMe.Controls.Add(this.label4);
			this.panelMe.Location = new System.Drawing.Point(763, 325);
			this.panelMe.Name = "panelMe";
			this.panelMe.Size = new System.Drawing.Size(654, 89);
			this.panelMe.TabIndex = 17;
			this.panelMe.Visible = false;
			// 
			// textBoxOS
			// 
			this.textBoxOS.Font = new System.Drawing.Font("微软雅黑", 12F);
			this.textBoxOS.Location = new System.Drawing.Point(199, 47);
			this.textBoxOS.Name = "textBoxOS";
			this.textBoxOS.Size = new System.Drawing.Size(446, 29);
			this.textBoxOS.TabIndex = 20;
			// 
			// label3
			// 
			this.label3.Font = new System.Drawing.Font("微软雅黑", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
			this.label3.Location = new System.Drawing.Point(19, 50);
			this.label3.Name = "label3";
			this.label3.Size = new System.Drawing.Size(174, 26);
			this.label3.TabIndex = 19;
			this.label3.Text = "操作系统路径：";
			// 
			// textBoxSysMes
			// 
			this.textBoxSysMes.Font = new System.Drawing.Font("微软雅黑", 12F);
			this.textBoxSysMes.Location = new System.Drawing.Point(199, 12);
			this.textBoxSysMes.Name = "textBoxSysMes";
			this.textBoxSysMes.Size = new System.Drawing.Size(446, 29);
			this.textBoxSysMes.TabIndex = 18;
			// 
			// label4
			// 
			this.label4.Font = new System.Drawing.Font("微软雅黑", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
			this.label4.Location = new System.Drawing.Point(19, 15);
			this.label4.Name = "label4";
			this.label4.Size = new System.Drawing.Size(174, 26);
			this.label4.TabIndex = 17;
			this.label4.Text = "系统设置参数：";
			// 
			// textBoxHidMod
			// 
			this.textBoxHidMod.Font = new System.Drawing.Font("微软雅黑", 12F);
			this.textBoxHidMod.Location = new System.Drawing.Point(939, 424);
			this.textBoxHidMod.Name = "textBoxHidMod";
			this.textBoxHidMod.Size = new System.Drawing.Size(446, 29);
			this.textBoxHidMod.TabIndex = 16;
			// 
			// buttonOutputMod
			// 
			this.buttonOutputMod.BackColor = System.Drawing.Color.RoyalBlue;
			this.buttonOutputMod.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
			this.buttonOutputMod.Font = new System.Drawing.Font("微软雅黑", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
			this.buttonOutputMod.ForeColor = System.Drawing.Color.White;
			this.buttonOutputMod.Location = new System.Drawing.Point(704, 61);
			this.buttonOutputMod.Name = "buttonOutputMod";
			this.buttonOutputMod.Size = new System.Drawing.Size(182, 43);
			this.buttonOutputMod.TabIndex = 18;
			this.buttonOutputMod.Text = "导出 mox";
			this.buttonOutputMod.UseVisualStyleBackColor = false;
			this.buttonOutputMod.Click += new System.EventHandler(this.ButtonOutputModClick);
			// 
			// buttonGroup
			// 
			this.buttonGroup.BackColor = System.Drawing.Color.WhiteSmoke;
			this.buttonGroup.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
			this.buttonGroup.Font = new System.Drawing.Font("微软雅黑", 10F);
			this.buttonGroup.ForeColor = System.Drawing.Color.Black;
			this.buttonGroup.Location = new System.Drawing.Point(12, 7);
			this.buttonGroup.Name = "buttonGroup";
			this.buttonGroup.Size = new System.Drawing.Size(186, 38);
			this.buttonGroup.TabIndex = 21;
			this.buttonGroup.Text = "选中的模块进行分组";
			this.buttonGroup.UseVisualStyleBackColor = false;
			this.buttonGroup.Click += new System.EventHandler(this.ButtonGroupClick);
			// 
			// buttonCancelGroup
			// 
			this.buttonCancelGroup.BackColor = System.Drawing.Color.WhiteSmoke;
			this.buttonCancelGroup.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
			this.buttonCancelGroup.Font = new System.Drawing.Font("微软雅黑", 10F);
			this.buttonCancelGroup.ForeColor = System.Drawing.Color.Black;
			this.buttonCancelGroup.Location = new System.Drawing.Point(12, 54);
			this.buttonCancelGroup.Name = "buttonCancelGroup";
			this.buttonCancelGroup.Size = new System.Drawing.Size(186, 38);
			this.buttonCancelGroup.TabIndex = 22;
			this.buttonCancelGroup.Text = "选中的模块取消分组";
			this.buttonCancelGroup.UseVisualStyleBackColor = false;
			this.buttonCancelGroup.Click += new System.EventHandler(this.ButtonCancelGroupClick);
			// 
			// textBoxGroupName
			// 
			this.textBoxGroupName.Font = new System.Drawing.Font("微软雅黑", 10F);
			this.textBoxGroupName.Location = new System.Drawing.Point(306, 14);
			this.textBoxGroupName.Name = "textBoxGroupName";
			this.textBoxGroupName.Size = new System.Drawing.Size(114, 25);
			this.textBoxGroupName.TabIndex = 12;
			this.textBoxGroupName.Text = "我的分组";
			this.textBoxGroupName.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
			// 
			// buttonTranslate
			// 
			this.buttonTranslate.BackColor = System.Drawing.Color.CornflowerBlue;
			this.buttonTranslate.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
			this.buttonTranslate.Font = new System.Drawing.Font("微软雅黑", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
			this.buttonTranslate.ForeColor = System.Drawing.Color.White;
			this.buttonTranslate.Location = new System.Drawing.Point(704, 20);
			this.buttonTranslate.Name = "buttonTranslate";
			this.buttonTranslate.Size = new System.Drawing.Size(182, 34);
			this.buttonTranslate.TabIndex = 23;
			this.buttonTranslate.Text = "程序翻译到英文";
			this.buttonTranslate.UseVisualStyleBackColor = false;
			this.buttonTranslate.Click += new System.EventHandler(this.ButtonTranslateClick);
			// 
			// label5
			// 
			this.label5.Font = new System.Drawing.Font("微软雅黑", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
			this.label5.ForeColor = System.Drawing.Color.Silver;
			this.label5.Location = new System.Drawing.Point(939, 514);
			this.label5.Name = "label5";
			this.label5.Size = new System.Drawing.Size(446, 58);
			this.label5.TabIndex = 21;
			this.label5.Text = "设置参数和需要隐藏的模块请在前后分别用逗号隔开\r\n需要以逗号起始和结束";
			// 
			// panel1
			// 
			this.panel1.BackColor = System.Drawing.Color.White;
			this.panel1.Controls.Add(this.label26);
			this.panel1.Controls.Add(this.label24);
			this.panel1.Controls.Add(this.label25);
			this.panel1.Controls.Add(this.textBoxFuncStack);
			this.panel1.Controls.Add(this.label22);
			this.panel1.Controls.Add(this.label23);
			this.panel1.Controls.Add(this.textBoxVarStack);
			this.panel1.Controls.Add(this.label8);
			this.panel1.Controls.Add(this.label6);
			this.panel1.Controls.Add(this.textBoxEventNumber);
			this.panel1.Controls.Add(this.label7);
			this.panel1.Location = new System.Drawing.Point(919, 45);
			this.panel1.Name = "panel1";
			this.panel1.Size = new System.Drawing.Size(654, 240);
			this.panel1.TabIndex = 15;
			// 
			// label26
			// 
			this.label26.Font = new System.Drawing.Font("微软雅黑", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
			this.label26.ForeColor = System.Drawing.Color.DarkOrange;
			this.label26.Location = new System.Drawing.Point(12, 123);
			this.label26.Name = "label26";
			this.label26.Size = new System.Drawing.Size(605, 26);
			this.label26.TabIndex = 19;
			this.label26.Text = "如果实物运行异常或者仿真异常(如闪退), 可尝试增大如下两个堆栈";
			// 
			// label24
			// 
			this.label24.Font = new System.Drawing.Font("微软雅黑", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
			this.label24.ForeColor = System.Drawing.Color.CornflowerBlue;
			this.label24.Location = new System.Drawing.Point(246, 200);
			this.label24.Name = "label24";
			this.label24.Size = new System.Drawing.Size(406, 28);
			this.label24.TabIndex = 18;
			this.label24.Text = "如果函数调用层次过多, 建议设置更大一些, 如 150";
			// 
			// label25
			// 
			this.label25.Font = new System.Drawing.Font("微软雅黑", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
			this.label25.ForeColor = System.Drawing.Color.Black;
			this.label25.Location = new System.Drawing.Point(12, 200);
			this.label25.Name = "label25";
			this.label25.Size = new System.Drawing.Size(146, 26);
			this.label25.TabIndex = 16;
			this.label25.Text = "事件池调用堆栈：";
			// 
			// textBoxFuncStack
			// 
			this.textBoxFuncStack.Font = new System.Drawing.Font("微软雅黑", 12F);
			this.textBoxFuncStack.Location = new System.Drawing.Point(164, 197);
			this.textBoxFuncStack.Name = "textBoxFuncStack";
			this.textBoxFuncStack.Size = new System.Drawing.Size(65, 29);
			this.textBoxFuncStack.TabIndex = 17;
			this.textBoxFuncStack.Text = "60";
			this.textBoxFuncStack.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
			this.textBoxFuncStack.TextChanged += new System.EventHandler(this.TextBoxFuncStackTextChanged);
			// 
			// label22
			// 
			this.label22.Font = new System.Drawing.Font("微软雅黑", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
			this.label22.ForeColor = System.Drawing.Color.CornflowerBlue;
			this.label22.Location = new System.Drawing.Point(246, 161);
			this.label22.Name = "label22";
			this.label22.Size = new System.Drawing.Size(407, 28);
			this.label22.TabIndex = 15;
			this.label22.Text = "如果函数调用层次过多, 建议设置更大一些, 如 350";
			// 
			// label23
			// 
			this.label23.Font = new System.Drawing.Font("微软雅黑", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
			this.label23.ForeColor = System.Drawing.Color.Black;
			this.label23.Location = new System.Drawing.Point(12, 161);
			this.label23.Name = "label23";
			this.label23.Size = new System.Drawing.Size(146, 26);
			this.label23.TabIndex = 13;
			this.label23.Text = "事件池形参堆栈：";
			// 
			// textBoxVarStack
			// 
			this.textBoxVarStack.Font = new System.Drawing.Font("微软雅黑", 12F);
			this.textBoxVarStack.Location = new System.Drawing.Point(164, 158);
			this.textBoxVarStack.Name = "textBoxVarStack";
			this.textBoxVarStack.Size = new System.Drawing.Size(65, 29);
			this.textBoxVarStack.TabIndex = 14;
			this.textBoxVarStack.Text = "250";
			this.textBoxVarStack.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
			this.textBoxVarStack.TextChanged += new System.EventHandler(this.TextBoxVarStackTextChanged);
			// 
			// label8
			// 
			this.label8.Font = new System.Drawing.Font("微软雅黑", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
			this.label8.ForeColor = System.Drawing.Color.CornflowerBlue;
			this.label8.Location = new System.Drawing.Point(246, 10);
			this.label8.Name = "label8";
			this.label8.Size = new System.Drawing.Size(334, 28);
			this.label8.TabIndex = 12;
			this.label8.Text = "每个事件池占用大约80个字节的RAM";
			// 
			// label6
			// 
			this.label6.Font = new System.Drawing.Font("微软雅黑", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
			this.label6.ForeColor = System.Drawing.Color.Black;
			this.label6.Location = new System.Drawing.Point(12, 10);
			this.label6.Name = "label6";
			this.label6.Size = new System.Drawing.Size(146, 26);
			this.label6.TabIndex = 9;
			this.label6.Text = "事件池最大数目：";
			// 
			// textBoxEventNumber
			// 
			this.textBoxEventNumber.Font = new System.Drawing.Font("微软雅黑", 12F);
			this.textBoxEventNumber.Location = new System.Drawing.Point(164, 7);
			this.textBoxEventNumber.Name = "textBoxEventNumber";
			this.textBoxEventNumber.Size = new System.Drawing.Size(65, 29);
			this.textBoxEventNumber.TabIndex = 10;
			this.textBoxEventNumber.Text = "8";
			this.textBoxEventNumber.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
			this.textBoxEventNumber.TextChanged += new System.EventHandler(this.TextBoxEventNumberTextChanged);
			// 
			// label7
			// 
			this.label7.Font = new System.Drawing.Font("微软雅黑", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
			this.label7.ForeColor = System.Drawing.Color.SlateGray;
			this.label7.Location = new System.Drawing.Point(12, 39);
			this.label7.Name = "label7";
			this.label7.Size = new System.Drawing.Size(632, 74);
			this.label7.TabIndex = 11;
			this.label7.Text = "可设置事件池最大数目，默认为8个，一般不需要用户修改。如果系统RAM不够用，例如用到点阵屏幕、大数组或者其他占用RAM过多的模块时，可以适当减小这个数值。但减小这" +
			"个数值后，系统可同时运行的事件数目将会相应减小。";
			// 
			// panel3
			// 
			this.panel3.BackColor = System.Drawing.Color.White;
			this.panel3.Controls.Add(this.textBoxGroupName);
			this.panel3.Controls.Add(this.textBoxVersion);
			this.panel3.Controls.Add(this.label17);
			this.panel3.Controls.Add(this.label11);
			this.panel3.Controls.Add(this.buttonGroup);
			this.panel3.Controls.Add(this.buttonCancelGroup);
			this.panel3.Controls.Add(this.label21);
			this.panel3.Font = new System.Drawing.Font("微软雅黑", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
			this.panel3.Location = new System.Drawing.Point(12, 413);
			this.panel3.Name = "panel3";
			this.panel3.Size = new System.Drawing.Size(654, 100);
			this.panel3.TabIndex = 16;
			// 
			// textBoxVersion
			// 
			this.textBoxVersion.Font = new System.Drawing.Font("微软雅黑", 10F);
			this.textBoxVersion.Location = new System.Drawing.Point(530, 14);
			this.textBoxVersion.Name = "textBoxVersion";
			this.textBoxVersion.Size = new System.Drawing.Size(114, 25);
			this.textBoxVersion.TabIndex = 24;
			this.textBoxVersion.Text = "1.0.0";
			this.textBoxVersion.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
			// 
			// label17
			// 
			this.label17.Font = new System.Drawing.Font("微软雅黑", 10F);
			this.label17.ForeColor = System.Drawing.Color.Black;
			this.label17.Location = new System.Drawing.Point(209, 13);
			this.label17.Name = "label17";
			this.label17.Size = new System.Drawing.Size(100, 29);
			this.label17.TabIndex = 23;
			this.label17.Text = "分组名称：";
			this.label17.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
			// 
			// label11
			// 
			this.label11.Font = new System.Drawing.Font("微软雅黑", 10F);
			this.label11.ForeColor = System.Drawing.Color.SlateGray;
			this.label11.Location = new System.Drawing.Point(204, 45);
			this.label11.Name = "label11";
			this.label11.Size = new System.Drawing.Size(440, 54);
			this.label11.TabIndex = 11;
			this.label11.Text = "右键框选若干个模块之后，可以点击分组或者取消分组。分组后的模块形成一个整体，并可以折叠隐藏，看上去简洁。";
			// 
			// label21
			// 
			this.label21.Font = new System.Drawing.Font("微软雅黑", 10F);
			this.label21.ForeColor = System.Drawing.Color.Black;
			this.label21.Location = new System.Drawing.Point(426, 12);
			this.label21.Name = "label21";
			this.label21.Size = new System.Drawing.Size(100, 29);
			this.label21.TabIndex = 25;
			this.label21.Text = "版本：";
			this.label21.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
			// 
			// panel4
			// 
			this.panel4.BackColor = System.Drawing.Color.White;
			this.panel4.Controls.Add(this.label9);
			this.panel4.Controls.Add(this.buttonSetNote);
			this.panel4.Controls.Add(this.buttonSetCode);
			this.panel4.Font = new System.Drawing.Font("微软雅黑", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
			this.panel4.Location = new System.Drawing.Point(12, 519);
			this.panel4.Name = "panel4";
			this.panel4.Size = new System.Drawing.Size(654, 103);
			this.panel4.TabIndex = 23;
			// 
			// label9
			// 
			this.label9.Font = new System.Drawing.Font("微软雅黑", 10F);
			this.label9.ForeColor = System.Drawing.Color.SlateGray;
			this.label9.Location = new System.Drawing.Point(204, 12);
			this.label9.Name = "label9";
			this.label9.Size = new System.Drawing.Size(440, 73);
			this.label9.TabIndex = 11;
			this.label9.Text = "右键选中的指令被禁用后将会呈现出灰色，并且不生成代码，不会被系统执行。";
			// 
			// buttonSetNote
			// 
			this.buttonSetNote.BackColor = System.Drawing.Color.WhiteSmoke;
			this.buttonSetNote.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
			this.buttonSetNote.Font = new System.Drawing.Font("微软雅黑", 10F);
			this.buttonSetNote.ForeColor = System.Drawing.Color.Black;
			this.buttonSetNote.Location = new System.Drawing.Point(12, 7);
			this.buttonSetNote.Name = "buttonSetNote";
			this.buttonSetNote.Size = new System.Drawing.Size(186, 38);
			this.buttonSetNote.TabIndex = 21;
			this.buttonSetNote.Text = "禁用选中的指令";
			this.buttonSetNote.UseVisualStyleBackColor = false;
			this.buttonSetNote.Click += new System.EventHandler(this.ButtonSetNoteClick);
			// 
			// buttonSetCode
			// 
			this.buttonSetCode.BackColor = System.Drawing.Color.WhiteSmoke;
			this.buttonSetCode.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
			this.buttonSetCode.Font = new System.Drawing.Font("微软雅黑", 10F);
			this.buttonSetCode.ForeColor = System.Drawing.Color.Black;
			this.buttonSetCode.Location = new System.Drawing.Point(12, 54);
			this.buttonSetCode.Name = "buttonSetCode";
			this.buttonSetCode.Size = new System.Drawing.Size(186, 38);
			this.buttonSetCode.TabIndex = 22;
			this.buttonSetCode.Text = "启用选中的指令";
			this.buttonSetCode.UseVisualStyleBackColor = false;
			this.buttonSetCode.Click += new System.EventHandler(this.ButtonSetCodeClick);
			// 
			// checkBoxDisableHit
			// 
			this.checkBoxDisableHit.Font = new System.Drawing.Font("微软雅黑", 10F);
			this.checkBoxDisableHit.ForeColor = System.Drawing.Color.Black;
			this.checkBoxDisableHit.Location = new System.Drawing.Point(12, 9);
			this.checkBoxDisableHit.Name = "checkBoxDisableHit";
			this.checkBoxDisableHit.Size = new System.Drawing.Size(186, 24);
			this.checkBoxDisableHit.TabIndex = 23;
			this.checkBoxDisableHit.Text = "拖动时忽略碰撞推挤";
			this.checkBoxDisableHit.UseVisualStyleBackColor = true;
			this.checkBoxDisableHit.CheckedChanged += new System.EventHandler(this.CheckBoxDisableHitCheckedChanged);
			// 
			// panel5
			// 
			this.panel5.BackColor = System.Drawing.Color.White;
			this.panel5.Controls.Add(this.label10);
			this.panel5.Controls.Add(this.label12);
			this.panel5.Controls.Add(this.textBoxVEX_Tick);
			this.panel5.Controls.Add(this.label13);
			this.panel5.Font = new System.Drawing.Font("微软雅黑", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
			this.panel5.Location = new System.Drawing.Point(12, 247);
			this.panel5.Name = "panel5";
			this.panel5.Size = new System.Drawing.Size(654, 67);
			this.panel5.TabIndex = 16;
			// 
			// label10
			// 
			this.label10.Font = new System.Drawing.Font("微软雅黑", 10F);
			this.label10.ForeColor = System.Drawing.Color.CornflowerBlue;
			this.label10.Location = new System.Drawing.Point(246, 10);
			this.label10.Name = "label10";
			this.label10.Size = new System.Drawing.Size(364, 28);
			this.label10.TabIndex = 12;
			this.label10.Text = "仅适用于VOS软核模式 (如STM32, GD32等)";
			// 
			// label12
			// 
			this.label12.Font = new System.Drawing.Font("微软雅黑", 10F);
			this.label12.ForeColor = System.Drawing.Color.Black;
			this.label12.Location = new System.Drawing.Point(12, 10);
			this.label12.Name = "label12";
			this.label12.Size = new System.Drawing.Size(146, 26);
			this.label12.TabIndex = 9;
			this.label12.Text = "VOS 驱动周期：";
			// 
			// textBoxVEX_Tick
			// 
			this.textBoxVEX_Tick.Font = new System.Drawing.Font("微软雅黑", 10F);
			this.textBoxVEX_Tick.Location = new System.Drawing.Point(164, 7);
			this.textBoxVEX_Tick.Name = "textBoxVEX_Tick";
			this.textBoxVEX_Tick.Size = new System.Drawing.Size(65, 25);
			this.textBoxVEX_Tick.TabIndex = 10;
			this.textBoxVEX_Tick.Text = "[10]";
			this.textBoxVEX_Tick.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
			this.textBoxVEX_Tick.TextChanged += new System.EventHandler(this.TextBoxVEX_TickTextChanged);
			// 
			// label13
			// 
			this.label13.Font = new System.Drawing.Font("微软雅黑", 10F);
			this.label13.ForeColor = System.Drawing.Color.SlateGray;
			this.label13.Location = new System.Drawing.Point(12, 39);
			this.label13.Name = "label13";
			this.label13.Size = new System.Drawing.Size(632, 25);
			this.label13.TabIndex = 11;
			this.label13.Text = "默认为10毫秒, 数值越大, 各个驱动调度周期越长, 可腾出更多时间用于事件执行";
			// 
			// label14
			// 
			this.label14.Font = new System.Drawing.Font("微软雅黑", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
			this.label14.ForeColor = System.Drawing.Color.Silver;
			this.label14.Location = new System.Drawing.Point(939, 462);
			this.label14.Name = "label14";
			this.label14.Size = new System.Drawing.Size(446, 52);
			this.label14.TabIndex = 24;
			this.label14.Text = "开放主板接口: ,SYS_OPEN_MPORT,\r\n隐藏主板接口: ,SYS_HIDE_MPORT,";
			// 
			// label15
			// 
			this.label15.Font = new System.Drawing.Font("微软雅黑", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
			this.label15.ForeColor = System.Drawing.Color.SlateGray;
			this.label15.Location = new System.Drawing.Point(12, 7);
			this.label15.Name = "label15";
			this.label15.Size = new System.Drawing.Size(654, 34);
			this.label15.TabIndex = 24;
			this.label15.Text = "提示：以下所有设置，仅对当前程序有效";
			this.label15.TextAlign = System.Drawing.ContentAlignment.TopCenter;
			this.label15.Click += new System.EventHandler(this.Label15Click);
			// 
			// panel6
			// 
			this.panel6.BackColor = System.Drawing.Color.White;
			this.panel6.Controls.Add(this.label20);
			this.panel6.Controls.Add(this.checkBoxHidName);
			this.panel6.Controls.Add(this.label19);
			this.panel6.Controls.Add(this.checkBoxZheXian);
			this.panel6.Controls.Add(this.label18);
			this.panel6.Controls.Add(this.checkBoxHideLine);
			this.panel6.Controls.Add(this.label16);
			this.panel6.Controls.Add(this.checkBoxDisableHit);
			this.panel6.Font = new System.Drawing.Font("微软雅黑", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
			this.panel6.ForeColor = System.Drawing.SystemColors.ActiveCaption;
			this.panel6.Location = new System.Drawing.Point(12, 36);
			this.panel6.Name = "panel6";
			this.panel6.Size = new System.Drawing.Size(654, 133);
			this.panel6.TabIndex = 17;
			// 
			// label20
			// 
			this.label20.Font = new System.Drawing.Font("微软雅黑", 10F);
			this.label20.ForeColor = System.Drawing.Color.SlateGray;
			this.label20.Location = new System.Drawing.Point(204, 100);
			this.label20.Name = "label20";
			this.label20.Size = new System.Drawing.Size(440, 28);
			this.label20.TabIndex = 29;
			this.label20.Text = "选中后将隐藏所有模块的名称和型号信息。（左 CTRL+N）";
			// 
			// checkBoxHidName
			// 
			this.checkBoxHidName.Font = new System.Drawing.Font("微软雅黑", 10F);
			this.checkBoxHidName.ForeColor = System.Drawing.Color.Black;
			this.checkBoxHidName.Location = new System.Drawing.Point(12, 99);
			this.checkBoxHidName.Name = "checkBoxHidName";
			this.checkBoxHidName.Size = new System.Drawing.Size(186, 24);
			this.checkBoxHidName.TabIndex = 28;
			this.checkBoxHidName.Text = "隐藏模块名称";
			this.checkBoxHidName.UseVisualStyleBackColor = true;
			this.checkBoxHidName.CheckedChanged += new System.EventHandler(this.CheckBoxHidNameCheckedChanged);
			// 
			// label19
			// 
			this.label19.Font = new System.Drawing.Font("微软雅黑", 10F);
			this.label19.ForeColor = System.Drawing.Color.SlateGray;
			this.label19.Location = new System.Drawing.Point(204, 70);
			this.label19.Name = "label19";
			this.label19.Size = new System.Drawing.Size(440, 28);
			this.label19.TabIndex = 27;
			this.label19.Text = "选中为折线模式，支持网格对齐，不选中为曲线模式。（空格键）";
			// 
			// checkBoxZheXian
			// 
			this.checkBoxZheXian.Font = new System.Drawing.Font("微软雅黑", 10F);
			this.checkBoxZheXian.ForeColor = System.Drawing.Color.Black;
			this.checkBoxZheXian.Location = new System.Drawing.Point(12, 69);
			this.checkBoxZheXian.Name = "checkBoxZheXian";
			this.checkBoxZheXian.Size = new System.Drawing.Size(186, 24);
			this.checkBoxZheXian.TabIndex = 26;
			this.checkBoxZheXian.Text = "切换为折线模式";
			this.checkBoxZheXian.UseVisualStyleBackColor = true;
			this.checkBoxZheXian.CheckedChanged += new System.EventHandler(this.CheckBoxZheXianCheckedChanged);
			// 
			// label18
			// 
			this.label18.Font = new System.Drawing.Font("微软雅黑", 10F);
			this.label18.ForeColor = System.Drawing.Color.SlateGray;
			this.label18.Location = new System.Drawing.Point(204, 40);
			this.label18.Name = "label18";
			this.label18.Size = new System.Drawing.Size(440, 28);
			this.label18.TabIndex = 25;
			this.label18.Text = "隐藏导线状态下无法连接新导线。（左 CTRL+Y）";
			// 
			// checkBoxHideLine
			// 
			this.checkBoxHideLine.Font = new System.Drawing.Font("微软雅黑", 10F);
			this.checkBoxHideLine.ForeColor = System.Drawing.Color.Black;
			this.checkBoxHideLine.Location = new System.Drawing.Point(12, 39);
			this.checkBoxHideLine.Name = "checkBoxHideLine";
			this.checkBoxHideLine.Size = new System.Drawing.Size(186, 24);
			this.checkBoxHideLine.TabIndex = 24;
			this.checkBoxHideLine.Text = "隐藏界面导线";
			this.checkBoxHideLine.UseVisualStyleBackColor = true;
			this.checkBoxHideLine.CheckedChanged += new System.EventHandler(this.CheckBoxHideLineCheckedChanged);
			// 
			// label16
			// 
			this.label16.Font = new System.Drawing.Font("微软雅黑", 10F);
			this.label16.ForeColor = System.Drawing.Color.SlateGray;
			this.label16.Location = new System.Drawing.Point(204, 10);
			this.label16.Name = "label16";
			this.label16.Size = new System.Drawing.Size(440, 28);
			this.label16.TabIndex = 12;
			this.label16.Text = "忽略推挤后，拖动时不同模块可能会重叠到一起";
			// 
			// panel7
			// 
			this.panel7.BackColor = System.Drawing.Color.White;
			this.panel7.Controls.Add(this.label27);
			this.panel7.Controls.Add(this.buttonOpenDir);
			this.panel7.Font = new System.Drawing.Font("微软雅黑", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
			this.panel7.Location = new System.Drawing.Point(12, 319);
			this.panel7.Name = "panel7";
			this.panel7.Size = new System.Drawing.Size(654, 51);
			this.panel7.TabIndex = 24;
			// 
			// label27
			// 
			this.label27.Font = new System.Drawing.Font("微软雅黑", 10F);
			this.label27.ForeColor = System.Drawing.Color.SlateGray;
			this.label27.Location = new System.Drawing.Point(139, 7);
			this.label27.Name = "label27";
			this.label27.Size = new System.Drawing.Size(249, 37);
			this.label27.TabIndex = 11;
			this.label27.Text = "打开当前程序所在的文件夹";
			this.label27.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// buttonOpenDir
			// 
			this.buttonOpenDir.BackColor = System.Drawing.Color.WhiteSmoke;
			this.buttonOpenDir.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
			this.buttonOpenDir.Font = new System.Drawing.Font("微软雅黑", 10F);
			this.buttonOpenDir.ForeColor = System.Drawing.Color.Black;
			this.buttonOpenDir.Location = new System.Drawing.Point(12, 7);
			this.buttonOpenDir.Name = "buttonOpenDir";
			this.buttonOpenDir.Size = new System.Drawing.Size(121, 38);
			this.buttonOpenDir.TabIndex = 21;
			this.buttonOpenDir.Text = "打开文件夹";
			this.buttonOpenDir.UseVisualStyleBackColor = false;
			this.buttonOpenDir.Click += new System.EventHandler(this.ButtonOpenDirClick);
			// 
			// panel8
			// 
			this.panel8.BackColor = System.Drawing.Color.White;
			this.panel8.Controls.Add(this.label28);
			this.panel8.Controls.Add(this.label29);
			this.panel8.Controls.Add(this.textBoxBDRAM);
			this.panel8.Controls.Add(this.label30);
			this.panel8.Font = new System.Drawing.Font("微软雅黑", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
			this.panel8.Location = new System.Drawing.Point(12, 174);
			this.panel8.Name = "panel8";
			this.panel8.Size = new System.Drawing.Size(654, 67);
			this.panel8.TabIndex = 17;
			// 
			// label28
			// 
			this.label28.Font = new System.Drawing.Font("微软雅黑", 10F);
			this.label28.ForeColor = System.Drawing.Color.CornflowerBlue;
			this.label28.Location = new System.Drawing.Point(246, 10);
			this.label28.Name = "label28";
			this.label28.Size = new System.Drawing.Size(364, 28);
			this.label28.TabIndex = 12;
			this.label28.Text = "本设置只有用到动态字符串等才有效";
			// 
			// label29
			// 
			this.label29.Font = new System.Drawing.Font("微软雅黑", 10F);
			this.label29.ForeColor = System.Drawing.Color.Black;
			this.label29.Location = new System.Drawing.Point(12, 10);
			this.label29.Name = "label29";
			this.label29.Size = new System.Drawing.Size(146, 26);
			this.label29.TabIndex = 9;
			this.label29.Text = "动态内存块大小：";
			// 
			// textBoxBDRAM
			// 
			this.textBoxBDRAM.Font = new System.Drawing.Font("微软雅黑", 10F);
			this.textBoxBDRAM.Location = new System.Drawing.Point(164, 7);
			this.textBoxBDRAM.Name = "textBoxBDRAM";
			this.textBoxBDRAM.Size = new System.Drawing.Size(65, 25);
			this.textBoxBDRAM.TabIndex = 10;
			this.textBoxBDRAM.Text = "[30]";
			this.textBoxBDRAM.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
			this.textBoxBDRAM.TextChanged += new System.EventHandler(this.TextBoxBDRAMTextChanged);
			// 
			// label30
			// 
			this.label30.Font = new System.Drawing.Font("微软雅黑", 10F);
			this.label30.ForeColor = System.Drawing.Color.SlateGray;
			this.label30.Location = new System.Drawing.Point(12, 39);
			this.label30.Name = "label30";
			this.label30.Size = new System.Drawing.Size(632, 25);
			this.label30.TabIndex = 11;
			this.label30.Text = "默认30个内存块，每个内存块为16Byte，30个内存块为480Byte,可根据需要设置";
			// 
			// labelMore
			// 
			this.labelMore.Font = new System.Drawing.Font("微软雅黑", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
			this.labelMore.ForeColor = System.Drawing.Color.OrangeRed;
			this.labelMore.Location = new System.Drawing.Point(12, 375);
			this.labelMore.Name = "labelMore";
			this.labelMore.Size = new System.Drawing.Size(654, 35);
			this.labelMore.TabIndex = 25;
			this.labelMore.Text = "↓ 点击展开更多设置 ↓";
			this.labelMore.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
			this.labelMore.Click += new System.EventHandler(this.LabelMoreClick);
			// 
			// USetForm
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(241)))), ((int)(((byte)(241)))), ((int)(((byte)(241)))));
			this.ClientSize = new System.Drawing.Size(679, 407);
			this.Controls.Add(this.labelMore);
			this.Controls.Add(this.panel8);
			this.Controls.Add(this.panel7);
			this.Controls.Add(this.panel6);
			this.Controls.Add(this.label15);
			this.Controls.Add(this.label14);
			this.Controls.Add(this.panel5);
			this.Controls.Add(this.panel4);
			this.Controls.Add(this.panel3);
			this.Controls.Add(this.panel1);
			this.Controls.Add(this.label5);
			this.Controls.Add(this.buttonTranslate);
			this.Controls.Add(this.buttonOutputMod);
			this.Controls.Add(this.panelMe);
			this.Controls.Add(this.textBoxHidMod);
			this.Controls.Add(this.panel2);
			this.Controls.Add(this.buttonPack);
			this.Controls.Add(this.buttonUnpack);
			this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
			this.MaximizeBox = false;
			this.MinimizeBox = false;
			this.Name = "USetForm";
			this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
			this.Text = "参数设置";
			this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.LocateFormFormClosing);
			this.panel2.ResumeLayout(false);
			this.panel2.PerformLayout();
			this.panelMe.ResumeLayout(false);
			this.panelMe.PerformLayout();
			this.panel1.ResumeLayout(false);
			this.panel1.PerformLayout();
			this.panel3.ResumeLayout(false);
			this.panel3.PerformLayout();
			this.panel4.ResumeLayout(false);
			this.panel5.ResumeLayout(false);
			this.panel5.PerformLayout();
			this.panel6.ResumeLayout(false);
			this.panel7.ResumeLayout(false);
			this.panel8.ResumeLayout(false);
			this.panel8.PerformLayout();
			this.ResumeLayout(false);
			this.PerformLayout();
		}
		private System.Windows.Forms.Label labelMore;
		private System.Windows.Forms.Label label30;
		private System.Windows.Forms.TextBox textBoxBDRAM;
		private System.Windows.Forms.Label label29;
		private System.Windows.Forms.Label label28;
		private System.Windows.Forms.Panel panel8;
		private System.Windows.Forms.Button buttonOpenDir;
		private System.Windows.Forms.Label label27;
		private System.Windows.Forms.Panel panel7;
		private System.Windows.Forms.Label label26;
		private System.Windows.Forms.TextBox textBoxVarStack;
		private System.Windows.Forms.Label label23;
		private System.Windows.Forms.Label label22;
		private System.Windows.Forms.TextBox textBoxFuncStack;
		private System.Windows.Forms.Label label25;
		private System.Windows.Forms.Label label24;
		private System.Windows.Forms.Label label21;
		private System.Windows.Forms.TextBox textBoxVersion;
		private System.Windows.Forms.CheckBox checkBoxHidName;
		private System.Windows.Forms.Label label20;
		private System.Windows.Forms.CheckBox checkBoxZheXian;
		private System.Windows.Forms.Label label19;
		private System.Windows.Forms.Label label16;
		private System.Windows.Forms.CheckBox checkBoxHideLine;
		private System.Windows.Forms.Label label18;
		private System.Windows.Forms.Panel panel6;
		private System.Windows.Forms.Label label17;
		private System.Windows.Forms.Label label15;
		private System.Windows.Forms.CheckBox checkBoxDisableHit;
		private System.Windows.Forms.Label label14;
		private System.Windows.Forms.Label label13;
		private System.Windows.Forms.TextBox textBoxVEX_Tick;
		private System.Windows.Forms.Label label12;
		private System.Windows.Forms.Label label10;
		private System.Windows.Forms.Panel panel5;
		private System.Windows.Forms.Button buttonSetCode;
		private System.Windows.Forms.Button buttonSetNote;
		private System.Windows.Forms.Label label9;
		private System.Windows.Forms.Panel panel4;
		private System.Windows.Forms.Label label11;
		private System.Windows.Forms.Panel panel3;
		private System.Windows.Forms.Label label8;
		private System.Windows.Forms.Label label7;
		private System.Windows.Forms.TextBox textBoxEventNumber;
		private System.Windows.Forms.Label label6;
		private System.Windows.Forms.Panel panel1;
		private System.Windows.Forms.Label label5;
		private System.Windows.Forms.Button buttonTranslate;
		private System.Windows.Forms.TextBox textBoxGroupName;
		private System.Windows.Forms.Button buttonCancelGroup;
		private System.Windows.Forms.Button buttonGroup;
		private System.Windows.Forms.Button buttonOutputMod;
		private System.Windows.Forms.Label label3;
		private System.Windows.Forms.TextBox textBoxOS;
		private System.Windows.Forms.Label label4;
		private System.Windows.Forms.TextBox textBoxSysMes;
		private System.Windows.Forms.TextBox textBoxHidMod;
		private System.Windows.Forms.Panel panelMe;
		private System.Windows.Forms.Button buttonUnpack;
		private System.Windows.Forms.Button buttonPack;
		private System.Windows.Forms.Panel panel2;
		private System.Windows.Forms.Label label2;
		private System.Windows.Forms.TextBox textBoxVOffset;
		private System.Windows.Forms.Label label1;
	}
}
