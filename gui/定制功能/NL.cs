﻿
using System;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using n_OS;

namespace n_NL
{
public static class NL
{
	public static NNode CNode;
	
	static Stream stream;
	static BinaryFormatter bin;
	
	//共计46个
	const string Version = "A1";
	
	//初始化
	public static void Init()
	{
		CNode = new NNode();
		
		bin = new BinaryFormatter();
	}
	
	//关闭
	public static void Close()
	{
		/*
		if( CNode == null ) {
			return;
		}
		string filename = System.DateTime.Now.ToString().Replace( '/', '.' ).Replace( ':', '-' );
		
		stream = File.Open( OS.SystemRoot + "NL" + OS.PATH_S + filename + ".ser", FileMode.Create );
		bin = new BinaryFormatter();
		
		bin.Serialize( stream, Version );
		SerializeA1( CNode );
		
		stream.Close();
		*/
	}
	
	//保存
	static void SerializeA1( NNode c )
	{
		bin.Serialize( stream, c.oPER_set );
		
		bin.Serialize( stream, c.oPER_neg );
		bin.Serialize( stream, c.oPER_abs );
		
		bin.Serialize( stream, c.oPER_plus );
		bin.Serialize( stream, c.oPER_minute );
		bin.Serialize( stream, c.oPER_mul );
		bin.Serialize( stream, c.oPER_div );
		bin.Serialize( stream, c.oPER_mode );
		
		bin.Serialize( stream, c.oPER_e_plus );
		bin.Serialize( stream, c.oPER_e_minute );
		bin.Serialize( stream, c.oPER_e_mul );
		bin.Serialize( stream, c.oPER_e_div );
		bin.Serialize( stream, c.oPER_e_mode );
		
		bin.Serialize( stream, c.bOOL_l );
		bin.Serialize( stream, c.bOOL_le );
		bin.Serialize( stream, c.bOOL_s );
		bin.Serialize( stream, c.bOOL_se );
		bin.Serialize( stream, c.bOOL_e );
		bin.Serialize( stream, c.bOOL_ne );
		
		bin.Serialize( stream, c.lOGIC_and );
		bin.Serialize( stream, c.lOGIC_or );
		bin.Serialize( stream, c.lOGIC_not );
		
		bin.Serialize( stream, c.vALUE_int );
		bin.Serialize( stream, c.vALUE_bool );
		bin.Serialize( stream, c.vALUE_fix );
		bin.Serialize( stream, c.vALUE_music );
		bin.Serialize( stream, c.vALUE_img );
		bin.Serialize( stream, c.vALUE_font );
		
		bin.Serialize( stream, c.fUNC_v_void );
		bin.Serialize( stream, c.fUNC_v_int );
		bin.Serialize( stream, c.fUNC_v_bool );
		bin.Serialize( stream, c.fUNC_v_string );
		bin.Serialize( stream, c.fUNC_r_void );
		bin.Serialize( stream, c.fUNC_r_int );
		bin.Serialize( stream, c.fUNC_r_bool );
		bin.Serialize( stream, c.fUNC_r_string );
		
		bin.Serialize( stream, c.fLOW_forever );
		bin.Serialize( stream, c.fLOW_loop );
		bin.Serialize( stream, c.fLOW_while );
		bin.Serialize( stream, c.fLOW_if );
		bin.Serialize( stream, c.fLOW_wait );
		bin.Serialize( stream, c.fLOW_break );
		bin.Serialize( stream, c.fLOW_continue );
		bin.Serialize( stream, c.fLOW_return_int );
		bin.Serialize( stream, c.fLOW_return_fix );
		bin.Serialize( stream, c.fLOW_return_bool );
		bin.Serialize( stream, c.fLOW_return_void );
		bin.Serialize( stream, c.fLOW_return_string );
		
		bin.Serialize( stream, c.o_note );
		
		bin.Serialize( stream, c.eVENT_list );
	}
	
	//加载
	public static NNode Load( string filename )
	{
		NNode c = new NNode();
		
		//stream = File.Open( OS.SystemRoot + "NL" + OS.PATH_S + filename + ".ser", FileMode.Open );
		stream = File.Open( filename, FileMode.Open );
		bin = new BinaryFormatter();
		
		string version = (string)bin.Deserialize( stream );
		
		c.oPER_set = (int)bin.Deserialize( stream );
		
		c.oPER_neg = (int)bin.Deserialize( stream );
		c.oPER_abs = (int)bin.Deserialize( stream );
		
		c.oPER_plus = (int)bin.Deserialize( stream );
		c.oPER_minute = (int)bin.Deserialize( stream );
		c.oPER_mul = (int)bin.Deserialize( stream );
		c.oPER_div = (int)bin.Deserialize( stream );
		c.oPER_mode = (int)bin.Deserialize( stream );
		
		c.oPER_e_plus = (int)bin.Deserialize( stream );
		c.oPER_e_minute = (int)bin.Deserialize( stream );
		c.oPER_e_mul = (int)bin.Deserialize( stream );
		c.oPER_e_div = (int)bin.Deserialize( stream );
		c.oPER_e_mode = (int)bin.Deserialize( stream );
		
		c.bOOL_l = (int)bin.Deserialize( stream );
		c.bOOL_le = (int)bin.Deserialize( stream );
		c.bOOL_s = (int)bin.Deserialize( stream );
		c.bOOL_se = (int)bin.Deserialize( stream );
		c.bOOL_e = (int)bin.Deserialize( stream );
		c.bOOL_ne = (int)bin.Deserialize( stream );
		
		c.lOGIC_and = (int)bin.Deserialize( stream );
		c.lOGIC_or = (int)bin.Deserialize( stream );
		c.lOGIC_not = (int)bin.Deserialize( stream );
		
		c.vALUE_int = (int)bin.Deserialize( stream );
		c.vALUE_bool = (int)bin.Deserialize( stream );
		c.vALUE_fix = (int)bin.Deserialize( stream );
		c.vALUE_music = (int)bin.Deserialize( stream );
		c.vALUE_img = (int)bin.Deserialize( stream );
		c.vALUE_font = (int)bin.Deserialize( stream );
		
		c.fUNC_v_void = (int)bin.Deserialize( stream );
		c.fUNC_v_int = (int)bin.Deserialize( stream );
		c.fUNC_v_bool = (int)bin.Deserialize( stream );
		c.fUNC_v_string = (int)bin.Deserialize( stream );
		c.fUNC_r_void = (int)bin.Deserialize( stream );
		c.fUNC_r_int = (int)bin.Deserialize( stream );
		c.fUNC_r_bool = (int)bin.Deserialize( stream );
		c.fUNC_r_string = (int)bin.Deserialize( stream );
		
		c.fLOW_forever = (int)bin.Deserialize( stream );
		c.fLOW_loop = (int)bin.Deserialize( stream );
		c.fLOW_while = (int)bin.Deserialize( stream );
		c.fLOW_if = (int)bin.Deserialize( stream );
		c.fLOW_wait = (int)bin.Deserialize( stream );
		c.fLOW_break = (int)bin.Deserialize( stream );
		c.fLOW_continue = (int)bin.Deserialize( stream );
		c.fLOW_return_int = (int)bin.Deserialize( stream );
		c.fLOW_return_fix = (int)bin.Deserialize( stream );
		c.fLOW_return_bool = (int)bin.Deserialize( stream );
		c.fLOW_return_void = (int)bin.Deserialize( stream );
		c.fLOW_return_string = (int)bin.Deserialize( stream );
		
		c.o_note = (int)bin.Deserialize( stream );
		
		c.eVENT_list = (string)bin.Deserialize( stream );
		
		stream.Close();
		return c;
	}
}

public class NNode
{
	//构造函数
	public NNode()
	{
		eVENT_list = "";
	}
	
	//算数表达式选中时
	public void User_Oper( string name )
	{
		if( name.IndexOf( " = " ) != -1 ) {
			oPER_set++;
			return;
		}
		if( name.IndexOf( " + " ) != -1 ) {
			oPER_plus++;
			return;
		}
		if( name.IndexOf( " - " ) != -1 ) {
			oPER_minute++;
			return;
		}
		if( name.IndexOf( " * " ) != -1 ) {
			oPER_mul++;
			return;
		}
		if( name.IndexOf( " / " ) != -1 ) {
			oPER_div++;
			return;
		}
		if( name.IndexOf( " % " ) != -1 ) {
			oPER_mode++;
			return;
		}
		if( name.IndexOf( "绝对值 " ) != -1 ) {
			oPER_abs++;
			return;
		}
		if( name.IndexOf( "- " ) != -1 ) {
			oPER_neg++;
			return;
		}
		
		if( name.IndexOf( " += " ) != -1 ) {
			oPER_plus++;
			return;
		}
		if( name.IndexOf( " -= " ) != -1 ) {
			oPER_minute++;
			return;
		}
		if( name.IndexOf( " *= " ) != -1 ) {
			oPER_mul++;
			return;
		}
		if( name.IndexOf( " /= " ) != -1 ) {
			oPER_div++;
			return;
		}
		if( name.IndexOf( " %= " ) != -1 ) {
			oPER_mode++;
			return;
		}
		
		//条件判断类
		if( name.IndexOf( " > " ) != -1 ) {
			bOOL_l++;
			return;
		}
		if( name.IndexOf( " >= " ) != -1 ) {
			bOOL_le++;
			return;
		}
		if( name.IndexOf( " < " ) != -1 ) {
			bOOL_s++;
			return;
		}
		if( name.IndexOf( " <= " ) != -1 ) {
			bOOL_se++;
			return;
		}
		if( name.IndexOf( " == " ) != -1 ) {
			bOOL_e++;
			return;
		}
		if( name.IndexOf( " != " ) != -1 ) {
			bOOL_ne++;
			return;
		}
		
		//逻辑判断
		if( name.IndexOf( " 并且 " ) != -1 ) {
			lOGIC_and++;
			return;
		}
		if( name.IndexOf( " 或者 " ) != -1 ) {
			lOGIC_or++;
			return;
		}
		if( name.IndexOf( "非 " ) != -1 ) {
			lOGIC_not++;
			return;
		}
	}
	
	public void USER_funcins( string name )
	{
		if( name.IndexOf( "返回 (?int32 )" ) != -1 ) {
			fLOW_return_int++;
			return;
		}
		if( name.IndexOf( "返回 (?fix )" ) != -1 ) {
			fLOW_return_fix++;
			return;
		}
		if( name.IndexOf( "返回 (?bool )" ) != -1 ) {
			fLOW_return_bool++;
			return;
		}
		if( name.IndexOf( "返回" ) != -1 ) {
			fLOW_return_void++;
			return;
		}
		if( name.IndexOf( "退出循环" ) != -1 ) {
			fLOW_break++;
			return;
		}
		if( name.IndexOf( "继续循环" ) != -1 ) {
			fLOW_continue++;
			return;
		}
	}
	
	public override string ToString()
	{
		return string.Format("[NNode OPER_set={0}, OPER_plus={1}, OPER_minute={2}, OPER_mul={3}, OPER_div={4}, OPER_mode={5}, OPER_e_plus={6}, OPER_e_minute={7}, OPER_e_mul={8}, OPER_e_div={9}, OPER_e_mode={10}, BOOL_l={11}, BOOL_le={12}, BOOL_s={13}, BOOL_se={14}, BOOL_e={15}, BOOL_ne={16}, LOGIC_and={17}, LOGIC_or={18}, LOGIC_not={19}, VALUE_int={20}, VALUE_bool={21}, VALUE_fix={22}, VALUE_mucic={23}, VALUE_img={24}, VALUE_font={25}, FUNC_v_void={26}, FUNC_v_int={27}, FUNC_v_bool={28}, FUNC_v_string={29}, FUNC_r_void={30}, FUNC_r_int={31}, FUNC_r_bool={32}, FUNC_r_string={33}, FLOW_forever={34}, FLOW_loop={35}, FLOW_while={36}, FLOW_if={37}, FLOW_wait={38}, FLOW_break={39}, FLOW_continue={40}, FLOW_return_int={41}, FLOW_return_bool={42}, FLOW_return_void={43}, FLOW_return_string={44}, O_note={45}, EVENT_list={46}]", oPER_set, oPER_plus, oPER_minute, oPER_mul, oPER_div, oPER_mode, oPER_e_plus, oPER_e_minute, oPER_e_mul,
			oPER_e_div, oPER_e_mode, bOOL_l, bOOL_le, bOOL_s, bOOL_se, bOOL_e, bOOL_ne, lOGIC_and, lOGIC_or,
			lOGIC_not, vALUE_int, vALUE_bool, vALUE_fix, vALUE_music, vALUE_img, vALUE_font, fUNC_v_void, fUNC_v_int, fUNC_v_bool,
			fUNC_v_string, fUNC_r_void, fUNC_r_int, fUNC_r_bool, fUNC_r_string, fLOW_forever, fLOW_loop, fLOW_while, fLOW_if, fLOW_wait,
			fLOW_break, fLOW_continue, fLOW_return_int, fLOW_return_bool, fLOW_return_void, fLOW_return_string, o_note, eVENT_list).Replace( ", ", "\n" );
	}
	
	public DateTime DT;
	
	//算数运算
	public int oPER_set;		//赋值	
	public int OPER_set {
		get { return oPER_set; }
		set { oPER_set = value; }
	}
	
	public int oPER_neg;		//负数
	public int OPER_neg {
		get { return oPER_neg; }
		set { oPER_neg = value; }
	}
	
	public int oPER_abs;		//绝对值
	public int OPER_abs {
		get { return oPER_abs; }
		set { oPER_abs = value; }
	}
	
	public int oPER_plus;		//加法
	public int OPER_plus {
		get { return oPER_plus; }
		set { oPER_plus = value; }
	}
	
	public int oPER_minute;		//减法
	public int OPER_minute {
		get { return oPER_minute; }
		set { oPER_minute = value; }
	}
	
	public int oPER_mul;		//乘法
	public int OPER_mul {
		get { return oPER_mul; }
		set { oPER_mul = value; }
	}
	
	public int oPER_div;		//除法
	public int OPER_div {
		get { return oPER_div; }
		set { oPER_div = value; }
	}
	
	public int oPER_mode;		//取余数
	public int OPER_mode {
		get { return oPER_mode; }
		set { oPER_mode = value; }
	}
	
	//算数复合运算
	public int oPER_e_plus;		//加法
	public int OPER_e_plus {
		get { return oPER_e_plus; }
		set { oPER_e_plus = value; }
	}
	
	public int oPER_e_minute;	//减法
	public int OPER_e_minute {
		get { return oPER_e_minute; }
		set { oPER_e_minute = value; }
	}
	
	public int oPER_e_mul;		//乘法
	public int OPER_e_mul {
		get { return oPER_e_mul; }
		set { oPER_e_mul = value; }
	}
	
	public int oPER_e_div;		//除法
	public int OPER_e_div {
		get { return oPER_e_div; }
		set { oPER_e_div = value; }
	}
	
	public int oPER_e_mode;		//取余数
	public int OPER_e_mode {
		get { return oPER_e_mode; }
		set { oPER_e_mode = value; }
	}
	
	//条件判断
	public int bOOL_l;			//大于
	public int BOOL_l {
		get { return bOOL_l; }
		set { bOOL_l = value; }
	}
	
	public int bOOL_le;			//大于等于
	public int BOOL_le {
		get { return bOOL_le; }
		set { bOOL_le = value; }
	}
	
	public int bOOL_s;			//小于
	public int BOOL_s {
		get { return bOOL_s; }
		set { bOOL_s = value; }
	}
	
	public int bOOL_se;			//小于等于
	public int BOOL_se {
		get { return bOOL_se; }
		set { bOOL_se = value; }
	}
	
	public int bOOL_e;			//等于
	public int BOOL_e {
		get { return bOOL_e; }
		set { bOOL_e = value; }
	}
	
	public int bOOL_ne;			//不等于
	public int BOOL_ne {
		get { return bOOL_ne; }
		set { bOOL_ne = value; }
	}
	
	//逻辑判断
	public int lOGIC_and;		//与
	public int LOGIC_and {
		get { return lOGIC_and; }
		set { lOGIC_and = value; }
	}
	
	public int lOGIC_or;		//或
	public int LOGIC_or {
		get { return lOGIC_or; }
		set { lOGIC_or = value; }
	}
	
	public int lOGIC_not;		//非
	public int LOGIC_not {
		get { return lOGIC_not; }
		set { lOGIC_not = value; }
	}
	
	//变量类型
	public int vALUE_int;		//整型
	public int VALUE_int {
		get { return vALUE_int; }
		set { vALUE_int = value; }
	}
	
	public int vALUE_bool;		//条件
	public int VALUE_bool {
		get { return vALUE_bool; }
		set { vALUE_bool = value; }
	}
	
	public int vALUE_fix;		//小数
	public int VALUE_fix {
		get { return vALUE_fix; }
		set { vALUE_fix = value; }
	}
	
	public int vALUE_music;		//音乐
	public int VALUE_music {
		get { return vALUE_music; }
		set { vALUE_music = value; }
	}
	
	public int vALUE_img;		//图片
	public int VALUE_img {
		get { return vALUE_img; }
		set { vALUE_img = value; }
	}
	
	public int vALUE_font;		//字体
	public int VALUE_font {
		get { return vALUE_font; }
		set { vALUE_font = value; }
	}
	
	//函数使用
	public int fUNC_v_void;
	public int FUNC_v_void {
		get { return fUNC_v_void; }
		set { fUNC_v_void = value; }
	}
	
	public int fUNC_v_int;
	public int FUNC_v_int {
		get { return fUNC_v_int; }
		set { fUNC_v_int = value; }
	}
	
	public int fUNC_v_bool;
	public int FUNC_v_bool {
		get { return fUNC_v_bool; }
		set { fUNC_v_bool = value; }
	}
	
	public int fUNC_v_string;
	public int FUNC_v_string {
		get { return fUNC_v_string; }
		set { fUNC_v_string = value; }
	}
	
	public int fUNC_r_void;
	public int FUNC_r_void {
		get { return fUNC_r_void; }
		set { fUNC_r_void = value; }
	}
	
	public int fUNC_r_int;
	public int FUNC_r_int {
		get { return fUNC_r_int; }
		set { fUNC_r_int = value; }
	}
	
	public int fUNC_r_bool;
	public int FUNC_r_bool {
		get { return fUNC_r_bool; }
		set { fUNC_r_bool = value; }
	}
	
	public int fUNC_r_string;
	public int FUNC_r_string {
		get { return fUNC_r_string; }
		set { fUNC_r_string = value; }
	}
	
	//流程类
	public int fLOW_forever;		//反复执行
	public int FLOW_forever {
		get { return fLOW_forever; }
		set { fLOW_forever = value; }
	}
	
	public int fLOW_loop;			//执行次数
	public int FLOW_loop {
		get { return fLOW_loop; }
		set { fLOW_loop = value; }
	}
	
	public int fLOW_while;			//条件反复
	public int FLOW_while {
		get { return fLOW_while; }
		set { fLOW_while = value; }
	}
	
	public int fLOW_if;				//如果语句
	public int FLOW_if {
		get { return fLOW_if; }
		set { fLOW_if = value; }
	}
	
	public int fLOW_wait;			//等待
	public int FLOW_wait {
		get { return fLOW_wait; }
		set { fLOW_wait = value; }
	}
	
	public int fLOW_break;			//中断
	public int FLOW_break {
		get { return fLOW_break; }
		set { fLOW_break = value; }
	}
	
	public int fLOW_continue;		//继续
	public int FLOW_continue {
		get { return fLOW_continue; }
		set { fLOW_continue = value; }
	}
	
	public int fLOW_return_int;		//返回整数
	public int FLOW_return_int {
		get { return fLOW_return_int; }
		set { fLOW_return_int = value; }
	}
	
	public int fLOW_return_fix;		//返回小数
	public int FLOW_return_fix {
		get { return fLOW_return_fix; }
		set { fLOW_return_fix = value; }
	}
	
	public int fLOW_return_bool;	//返回条件
	public int FLOW_return_bool {
		get { return fLOW_return_bool; }
		set { fLOW_return_bool = value; }
	}
	
	public int fLOW_return_void;	//返回无值
	public int FLOW_return_void {
		get { return fLOW_return_void; }
		set { fLOW_return_void = value; }
	}
	
	public int fLOW_return_string;	//返回字符串
	public int FLOW_return_string {
		get { return fLOW_return_string; }
		set { fLOW_return_string = value; }
	}
	
	public int o_note;
	public int O_note {
		get { return o_note; }
		set { o_note = value; }
	}
	
	public string eVENT_list;
	public string EVENT_list {
		get { return eVENT_list; }
		set { eVENT_list = value; }
	}
}
}


