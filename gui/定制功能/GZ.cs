﻿
namespace n_GZ
{
using System;
using System.Drawing;
using System.IO.Ports;
using System.Runtime.InteropServices;
using System.Windows.Forms;
using System.Threading;
using System.IO;

using uint8 = System.Byte;
using uint16 = System.UInt16;
using uint32 = System.UInt32;
using int32 = System.Int32;

public static class GZ
{
	public static int GZ_Type = 0; //0手动手势识别  1自动手势识别  2颜色分拣文件读取  3手势文件读取
		public const int GZ_Type_SH_User = 0;
		public const int GZ_Type_SH_Auto = 1;
		public const int GZ_Type_Color = 2;
		public const int GZ_Type_SH_File = 3;
	
	public static bool OK;
	
	public static string StrResult;
	
	public static string Message;
	//------------------------------------
	static Thread UserThread;
	
	static int GZ_Tick;
	static int RTime;
	
	public static void Init()
	{
		GZ_Tick = 0;
		RTime = 0;
		
		//GZ_Result = "green";
		
		OK = false;
	}
	
	public static void Start()
	{
		UserThread = new Thread(new ThreadStart(UserThreadProc));
		UserThread.Start();
	}
	
	public static void Stop()
	{
		if( UserThread != null && UserThread.IsAlive ) {
			UserThread.Abort();
			UserThread.Join();
		}
	}
	
	static void UserThreadProc()
	{
		while( true ) {
			
			try {
			if( GZ_Tick % 40 == 0 && GZ_Type == GZ_Type_SH_Auto ) {
				string path = System.AppDomain.CurrentDomain.BaseDirectory + "tempGZ\\main.html";
				n_Web.HTTP.HttpDownloadFile( "http://demo.childaiedu.com/data/get", path );
				RTime++;
				Message = "手势识别结果读取 " + RTime + " 次";
				OK = true;
			}
			if( GZ_Tick % 20 == 0 && GZ_Type == GZ_Type_Color ) {
				string filename = @"C:\Users\Public\test.txt";
				if( File.Exists( filename ) ) {
					StrResult = n_OS.VIO.OpenTextFileUTF8( filename );
					Message = "颜色识别结果: " + StrResult;
					OK = true;
				}
				else {
					Message = "请开启颜色识别摄像头";
				}
			}
			if( GZ_Tick % 20 == 0 && GZ_Type == GZ_Type_SH_File ) {
				string filename = @"C:\Users\Public\ex.txt";
				if( File.Exists( filename ) ) {
					StrResult = n_OS.VIO.OpenTextFileUTF8( filename );
					
					if( StrResult == "stone" ) {
						Message = "手势识别结果: 石头";
					}
					else if( StrResult == "scissors" ) {
						Message = "手势识别结果: 剪刀";
					}
					else if( StrResult == "cloth" ) {
						Message = "手势识别结果: 布";
					}
					else {
						Message = "手势识别结果: 无";
					}
					OK = true;
				}
				else {
					Message = "请开启手势识别摄像头";
				}
			}
			if( (GZ_Tick % 20 == 0) && System.DateTime.Now.Month >= 5 ) {
				//n_Debug.Debug.Message += "已超出演示期限";
			}
			}
			catch( Exception e ) {
				//...
				Message = "GZ数据读取失败: " + e;
			}
			System.Threading.Thread.Sleep( 100 );
			GZ_Tick++;
		}
	}
}
}




