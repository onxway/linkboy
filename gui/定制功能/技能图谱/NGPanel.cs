﻿
using System;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Windows.Forms;
using System.Reflection;
using n_NL;
using n_NGList;

namespace n_NGPanel
{
public class NGPanel : Panel
{
	public float CIndex;
	
	public static Font font;
	
	bool LeftPress;
	bool RightPress;
	
	Pen p;
	
	//构造函数
	public NGPanel() : base()
	{
		this.BorderStyle = BorderStyle.None;
		this.BackColor = Color.Black;
		
		SetStyle(ControlStyles.UserPaint |
		         ControlStyles.AllPaintingInWmPaint |
		         ControlStyles.OptimizedDoubleBuffer |
		         ControlStyles.ResizeRedraw |
		         ControlStyles.SupportsTransparentBackColor,true);
		
		this.Dock = DockStyle.Fill;
		this.ImeMode = ImeMode.NoControl;
		
		this.MouseMove += new MouseEventHandler( UserMouseMove );
		this.MouseUp += new MouseEventHandler( UserMouseUp );
		this.MouseDown += new MouseEventHandler( UserMouseDown );
		this.MouseWheel += new MouseEventHandler( MyMouseWheel );
		
		font = new Font( "微软雅黑", 10 );
		p = new Pen( Color.Black );
	}
	
	//重绘事件
	protected override void OnPaint(PaintEventArgs e)
	{
		Graphics g = e.Graphics;
		g.SmoothingMode = SmoothingMode.HighQuality;
		
		//填充背景
		g.Clear( Color.WhiteSmoke );
		
		if( NGList.node == null ) {
			g.DrawString( "无数据", font, Brushes.Red, 100, 100 );
			return;
		}
		
		NGList.ClearHist();
		
		float pw = (Width-40.0f) / NGList.NLength;
		float ph = 10;
		int SY = Height - 50;
		float ldt = -200;
		for( int i = 0; i < NGList.NLength; ++i ) {
			if( i >= CIndex ) {
				break;
			}
			int idx = 0;
			Type t = NGList.node[i].GetType();//获得该类的Type
			foreach (PropertyInfo pi in t.GetProperties()) {
				
				string name = pi.Name;//获得属性的名字,后面就可以根据名字判断来进行些自己想要的操作
				object v = pi.GetValue(NGList.node[i], null);//用pi.GetValue获得值
				
				if( v is int ) {
					p.Color = Color.FromArgb( idx * 36 % 256, idx * 137 % 256, idx * 231 % 256 );
					
					int Value = (int)v;
					int Lst = NGList.HList[idx];
					NGList.HList[idx] += Value;
					
					g.DrawLine( p, 20 + (i-1) * pw, SY - Lst * ph, 20 + (i) * pw, SY - NGList.HList[idx] * ph );
					g.DrawRectangle( p, 20 + i * pw - 1, SY - NGList.HList[idx] * ph - 1, 2, 2 );
					
					idx++;
				}
				else {
					//g.DrawString( v.GetType().ToString(), font, Brushes.Red, 20 + i * 30, 620 );
				}
			}
			
			//显示日期
			if( ldt < 0 || 20 + i * pw - ldt > 110 ) {
				
				ldt = 20 + i * pw;
				DateTime dt = NGList.node[i].DT;
				g.DrawString( dt.ToString().Replace( ' ', '\n' ), font, Brushes.Black, 20 + i * pw, SY + 5 );
			}
		}
		g.DrawString( "数量: " + NGList.NLength.ToString(), font, Brushes.SlateBlue, 10, 5 );
	}
	
	void MyMouseWheel( object sender, MouseEventArgs e)
	{
		if( e.Delta > 0 ) {
			//StartY += 50;
		}
		else {
			//StartY -= 50;
		}
		this.Invalidate();
	}
	
	//鼠标按下事件
	void UserMouseDown( object sender, MouseEventArgs e )
	{
		if( e.Button == MouseButtons.Left ) {
			LeftPress = true;
		}
		if( e.Button == MouseButtons.Right ) {
			RightPress = true;
		}
		this.Invalidate();
		
		this.Focus();
	}
	
	//鼠标松开事件
	void UserMouseUp( object sender, MouseEventArgs e )
	{
		if( e.Button == MouseButtons.Left ) {
			LeftPress = false;
		}
		if( e.Button == MouseButtons.Right ) {
			RightPress = false;
		}
		this.Invalidate();
	}
	
	//鼠标移动事件
	void UserMouseMove( object sender, MouseEventArgs e )
	{
		if( LeftPress ) {
			//...
		}
		if( RightPress ) {
			//...
		}
		this.Invalidate();
	}
}
}


