﻿
using System;
using System.Drawing;
using System.IO;
using System.Windows.Forms;
using n_NGPanel;
using n_CNGPanel;
using n_NL;
using n_NGList;

namespace n_NGForm
{
/// <summary>
/// Description of LocateForm.
/// </summary>
public partial class NGForm : Form
{
	NGPanel GPanel;
	CNGPanel CGPanel;
	
	NNode[] NNodeList;
	int Length;
	
	public NGForm()
	{
		//
		// The InitializeComponent() call is required for Windows Forms designer support.
		//
		InitializeComponent();
		
		GPanel = new NGPanel();
		this.tabPage2.Controls.Add( GPanel );
		
		CGPanel = new CNGPanel();
		this.tabPage1.Controls.Add( CGPanel );
	}
	
	//运行
	public void Run()
	{
		//richTextBox1.Text = n_NL.NL.CNode.ToString();
		
		NNodeList = new NNode[1000];
		Length = 0;
		
		//A_FindFile( n_OS.OS.SystemRoot + "NL\\" );
		
		this.Visible = true;
		CGPanel.CIndex = 0;
		GPanel.CIndex = 0;
	}
	
	//遍历指定的文件夹中的全部文件
	void A_FindFile(string sSourcePath )
	{
		//在指定目录及子目录下查找文件,在list中列出子目录及文件
		DirectoryInfo Dir = new DirectoryInfo( sSourcePath );
		DirectoryInfo[] DirSub = Dir.GetDirectories();
		
		//遍历当前文件夹中的所有文件
		foreach( FileInfo f in Dir.GetFiles("*.*", SearchOption.TopDirectoryOnly) ) {
			
			string time = f.ToString();
			time = time.Remove( time.Length - 4 ).Replace( '.', '/' ).Replace( '-', ':' );
			
			string FilePath = Dir + f.ToString();
			try {
				NNode node = NL.Load( FilePath );
				node.DT = DateTime.Parse( time );
				NNodeList[Length] = node;
				Length++;
			}
			catch( Exception e) {
				MessageBox.Show( "ERROR: " + time + "+" + FilePath + "\n" + e );
				break;
			}
		}
		NGList.Set( NNodeList );
	}
	
	void LocateFormFormClosing(object sender, FormClosingEventArgs e)
	{
		this.Visible = false;
		e.Cancel = true;
	}
	
	void Button1Click(object sender, EventArgs e)
	{
		//打开文件对话框
		OpenFileDialog OpenFileDlg = new OpenFileDialog();
		//OpenFileDlg.Filter = n_CCode.CCode.Filter;
		OpenFileDlg.Title = "请选择文件";
		
		DialogResult dlgResult = OpenFileDlg.ShowDialog();
		if(dlgResult == DialogResult.OK) {
			NNode node = NL.Load( OpenFileDlg.FileName );
			
			//GPanel.node = node;
		}
	}
	
	void Timer1Tick(object sender, EventArgs e)
	{
		float o = NGList.NLength / 40.0f;
		GPanel.CIndex += o;
		GPanel.Invalidate();
		CGPanel.CIndex += o;
		CGPanel.Invalidate();
	}
	
	void TabControl1SelectedIndexChanged(object sender, EventArgs e)
	{
		CGPanel.CIndex = 0;
		GPanel.CIndex = 0;
	}
}
}

