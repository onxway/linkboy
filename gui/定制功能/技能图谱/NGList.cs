﻿
using System;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Windows.Forms;
using System.Reflection;
using n_NL;

namespace n_NGList
{
public static class NGList
{
	public static NNode[] node;
	public static int NLength;
	public static int VLength;
	
	public static int[] HList;
	
	//设置参数列表
	public static void Set( NNode[] list )
	{
		node = list;
		NLength = 0;
		for( int i = 0; i < node.Length; ++i ) {
			if( node[i] == null ) {
				break;
			}
			NLength++;
		}
		//计算属性数目
		VLength = 0;
		if( node[0] != null ) {
			Type t = node[0].GetType();//获得该类的Type
			foreach (PropertyInfo pi in t.GetProperties()) {
				
				string name = pi.Name;//获得属性的名字,后面就可以根据名字判断来进行些自己想要的操作
				object v = pi.GetValue(node[0], null);//用pi.GetValue获得值
				
				if( v is int ) {
					VLength++;
				}
				else {
					//...
				}
			}
		}
		HList = new int[100];
	}
	
	//清空历史累计数据
	public static void ClearHist()
	{
		for( int i = 0; i < HList.Length; ++i ) {
			HList[i] = 0;
		}
	}
}
}


