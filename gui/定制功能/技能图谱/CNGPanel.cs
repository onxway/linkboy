﻿
using System;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Windows.Forms;
using System.Reflection;
using n_NL;
using n_NGList;

namespace n_CNGPanel
{
public class CNGPanel : Panel
{
	public float CIndex;
	
	public static Font font;
	public static Font font0;
	public static Font font13;
	
	bool LeftPress;
	bool RightPress;
	
	Pen p;
	
	
	//构造函数
	public CNGPanel() : base()
	{
		this.BorderStyle = BorderStyle.None;
		this.BackColor = Color.Black;
		
		SetStyle(ControlStyles.UserPaint |
		         ControlStyles.AllPaintingInWmPaint |
		         ControlStyles.OptimizedDoubleBuffer |
		         ControlStyles.ResizeRedraw |
		         ControlStyles.SupportsTransparentBackColor,true);
		
		this.Dock = DockStyle.Fill;
		this.ImeMode = ImeMode.NoControl;
		
		this.MouseMove += new MouseEventHandler( UserMouseMove );
		this.MouseUp += new MouseEventHandler( UserMouseUp );
		this.MouseDown += new MouseEventHandler( UserMouseDown );
		this.MouseWheel += new MouseEventHandler( MyMouseWheel );
		
		font13 = new Font( "微软雅黑", 13 );
		font = new Font( "微软雅黑", 40 );
		p = new Pen( Color.Black );
	}
	
	//重绘事件
	protected override void OnPaint(PaintEventArgs e)
	{
		Graphics g = e.Graphics;
		g.SmoothingMode = SmoothingMode.HighQuality;
		
		//填充背景
		g.Clear( Color.WhiteSmoke );
		
		if( NGList.node == null ) {
			g.DrawString( "无数据", font, Brushes.Red, 100, 100 );
			return;
		}
		
		NGList.ClearHist();
		
		int[] SList = new int[4];
		
		int Score = 0;
		string mes = "";
		for( int i = 0; i < NGList.NLength; ++i ) {
			
			if( i >= CIndex ) {
				//g.DrawString( "日期: " + NGList.node[i].DT.ToString().Replace( ' ', ' ' ), font13, Brushes.SlateGray, 20, 20 );
				break;
			}
			int idx = 0;
			//float lox = 0;
			//float loy = 0;
			//float sox = 0;
			//float soy = 0;
			
			Score = 0;
			SList[0] = 0;
			SList[1] = 0;
			SList[2] = 0;
			SList[3] = 0;
			
			Type t = NGList.node[i].GetType();//获得该类的Type
			foreach (PropertyInfo pi in t.GetProperties()) {
				
				string name = pi.Name;//获得属性的名字,后面就可以根据名字判断来进行些自己想要的操作
				object v = pi.GetValue(NGList.node[i], null);//用pi.GetValue获得值
				
				if( v is int ) {
					//p.Color = Color.FromArgb( idx * 36 % 256, idx * 137 % 256, idx * 231 % 256 );
					p.Color = Color.CornflowerBlue;
					
					int vv = (int)v;
					NGList.HList[idx] += vv;
					
					Score += NGList.HList[idx];
					
					SList[idx * 4 / NGList.VLength] += NGList.HList[idx];
					
					
					mes = NGList.node[i].DT.ToString();
					
					idx++;
				}
				else {
					//g.DrawString( v.GetType().ToString(), font, Brushes.Red, 20 + i * 30, 620 );
				}
			}
		}
		
		float MidX = Width / 2;
		float MidY = Height / 2;
		float R = Width / 6;
		
		float h = R / 2;
		float w = R * 4;
		float xx = MidX - w / 2;
		float yy1 = MidY - R;
		float yy2 = MidY + h;
		
		g.FillRectangle( Brushes.CornflowerBlue, xx, yy1, w, h );
		g.FillRectangle( Brushes.CornflowerBlue, xx, yy2, w, h );
		
		g.FillEllipse( Brushes.CornflowerBlue, MidX - R, MidY - R, R*2, R*2 );
		g.DrawEllipse( Pens.Blue, MidX - R, MidY - R, R*2, R*2 );
		
		
		g.DrawString( "算法设计得分" + SList[0].ToString(), font13, Brushes.Black, MidX - w/2 + 10, MidY - R + 20 );
		g.DrawString( "结构设计得分" + SList[1].ToString(), font13, Brushes.Black, MidX + w/5, MidY - R + 20 );
		g.DrawString( "电路设计得分" + SList[2].ToString(), font13, Brushes.Black, MidX - w/2 + 10, MidY + R-h + 20 );
		g.DrawString( "元件应用得分" + SList[3].ToString(), font13, Brushes.Black, MidX + w/5, MidY + R-h + 20 );
		
		
		g.DrawString( "总分" + Score.ToString(), font, Brushes.Black, MidX - 100, MidY - 35 );
		
		
		int ii = (int)CIndex;
		if( ii > NGList.NLength - 1 ) {
			ii = NGList.NLength - 1;
		}
		g.DrawString( "日期: " + NGList.node[ii].DT.ToString().Replace( ' ', ' ' ), font13, Brushes.SlateGray, 20, 20 );
	}
	
	//重绘事件
	protected void OnPaint_Old(PaintEventArgs e)
	{
		Graphics g = e.Graphics;
		g.SmoothingMode = SmoothingMode.HighQuality;
		
		//填充背景
		g.Clear( Color.WhiteSmoke );
		
		if( NGList.node == null ) {
			g.DrawString( "无数据", font, Brushes.Red, 100, 100 );
			return;
		}
		
		NGList.ClearHist();
		
		float MidX = Width / 2;
		float MidY = Height / 2;
		float R = Width / 6;
		
		g.FillEllipse( Brushes.CornflowerBlue, MidX - R, MidY - R, R*2, R*2 );
		g.DrawEllipse( Pens.Blue, MidX - R, MidY - R, R*2, R*2 );
		
		int Score = 0;
		string mes = "";
		for( int i = 0; i < NGList.NLength; ++i ) {
			
			if( i >= CIndex ) {
				g.DrawString( "日期: " + NGList.node[i].DT.ToString().Replace( ' ', ' ' ), font, Brushes.SlateGray, 20, 20 );
				break;
			}
			int idx = 0;
			//float lox = 0;
			//float loy = 0;
			//float sox = 0;
			//float soy = 0;
			Score = 0;
			Type t = NGList.node[i].GetType();//获得该类的Type
			foreach (PropertyInfo pi in t.GetProperties()) {
				
				string name = pi.Name;//获得属性的名字,后面就可以根据名字判断来进行些自己想要的操作
				object v = pi.GetValue(NGList.node[i], null);//用pi.GetValue获得值
				
				if( v is int ) {
					//p.Color = Color.FromArgb( idx * 36 % 256, idx * 137 % 256, idx * 231 % 256 );
					p.Color = Color.CornflowerBlue;
					
					int vv = (int)v;
					NGList.HList[idx] += vv;
					
					Score += NGList.HList[idx];
					mes = NGList.node[i].DT.ToString();
					
					float oox = (float)((R + 10) * Math.Sin( idx * Math.PI * 2 / NGList.VLength ));
					float ooy = (float)((R + 10) * Math.Cos( idx * Math.PI * 2 / NGList.VLength ));
					
					float ox = (float)((R + 10 + NGList.HList[idx]*10) * Math.Sin( idx * Math.PI * 2 / NGList.VLength ));
					float oy = (float)((R + 10 + NGList.HList[idx]*10) * Math.Cos( idx * Math.PI * 2 / NGList.VLength ));
					
					//if( idx != 0 ) {
						g.DrawLine( p, MidX + oox, MidY + ooy, MidX + ox, MidY + oy );
					//}
					//else {
					//	sox = ox;
					//	soy = oy;
					//}
					//lox = ox;
					//loy = oy;
					
					g.FillEllipse( Brushes.CornflowerBlue, MidX + ox - 3, MidY + oy - 3, 7, 7 );
					
					idx++;
				}
				else {
					//g.DrawString( v.GetType().ToString(), font, Brushes.Red, 20 + i * 30, 620 );
				}
			}
			//g.DrawLine( p, MidX + lox, MidY + loy, MidX + sox, MidY + soy );
			
			//显示日期
			/*
			if( ldt < 0 || 20 + i * pw - ldt > 110 ) {
				ldt = 20 + i * pw;
				DateTime dt = node[i].DT;
				g.DrawString( dt.ToString().Replace( ' ', '\n' ), font, Brushes.Black, 20 + i * pw, SY + 5 );
			}
			*/
		}
		g.DrawString( Score.ToString(), font, Brushes.Black, MidX - 40, MidY - 40 );
		
		//g.DrawString( "数量: " + NGList.NLength.ToString(), font, Brushes.SlateBlue, 10, 5 );
		
		
	}
	
	void MyMouseWheel( object sender, MouseEventArgs e)
	{
		if( e.Delta > 0 ) {
			//StartY += 50;
		}
		else {
			//StartY -= 50;
		}
		this.Invalidate();
	}
	
	//鼠标按下事件
	void UserMouseDown( object sender, MouseEventArgs e )
	{
		if( e.Button == MouseButtons.Left ) {
			LeftPress = true;
		}
		if( e.Button == MouseButtons.Right ) {
			RightPress = true;
		}
		this.Invalidate();
		
		this.Focus();
	}
	
	//鼠标松开事件
	void UserMouseUp( object sender, MouseEventArgs e )
	{
		if( e.Button == MouseButtons.Left ) {
			LeftPress = false;
		}
		if( e.Button == MouseButtons.Right ) {
			RightPress = false;
		}
		this.Invalidate();
	}
	
	//鼠标移动事件
	void UserMouseMove( object sender, MouseEventArgs e )
	{
		if( LeftPress ) {
			//...
		}
		if( RightPress ) {
			//...
		}
		this.Invalidate();
	}
}
}


