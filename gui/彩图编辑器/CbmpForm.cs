﻿
namespace n_CbmpForm
{
using System;
using System.IO;
using System.Net;
using System.Text;
using System.Windows.Forms;
using System.Drawing;
using System.Drawing.Drawing2D;
using n_PointPanel;
using n_LatticeToCode;
using n_GUIcoder;
using c_MyObjectSet;

/// <summary>
/// Description of WorkForm.
/// </summary>
public partial class CbmpForm : Form
{
	bool isOK;
	
	public Bitmap bmp;
	bool ignoreRefresh;
	int Width;
	int Height;
	
	//主窗口
	public CbmpForm()
	{
		InitializeComponent();
		
		isOK = false;
		ignoreRefresh = false;
		Width = 32;
		Height = 32;
	}
	
	//运行
	public bool RunBitmap( ref string text, ref int W, ref int H )
	{
		isOK = false;
		
		bmp = null;
		pictureBox1.BackgroundImage = null;
		
		Width = W;
		Height = H;
		ignoreRefresh = true;
		this.textBox宽度.Text = (Width).ToString();
		this.textBox高度.Text = (Height).ToString();
		ignoreRefresh = false;
		
		if( text == null || text == "" ) {
			
		}
		else {
			
		}
		this.ShowDialog();
		
		if( isOK ) {
			W = Width;
			H = Height;
			if( bmp != null ) {
				StringBuilder btext = new StringBuilder( "" );
				for( int y = 0; y < bmp.Height; ++y ) {
					for( int x = 0; x < bmp.Width; ++x ) {
						Color c = bmp.GetPixel( x, y );
						btext.Append( c.R + "," + c.G + "," + c.B + " " );
					}
				}
				text = btext.ToString();
				
				W = bmp.Width;
				H = bmp.Height;
			}
			return true;
		}
		return false;
	}
	
	//关闭事件
	void ModuleFormClosing(object sender, FormClosingEventArgs e)
	{
		//isOK = false;
		this.Visible = false;
		//e.Cancel = true;
	}
	
	void TextBox宽度TextChanged(object sender, EventArgs e)
	{
		if( ignoreRefresh ) {
			return;
		}
		try {
			Width = int.Parse( this.textBox宽度.Text );
		}
		catch {
			return;
		}
	}
	
	void TextBox高度TextChanged(object sender, EventArgs e)
	{
		if( ignoreRefresh ) {
			return;
		}
		try {
			Height = int.Parse( this.textBox高度.Text );
		}
		catch {
			return;
		}
	}
	
	void Button确定Click(object sender, EventArgs e)
	{
		isOK = true;
		this.Visible = false;
	}
	
	void ButtonOpenBitmapClick(object sender, EventArgs e)
	{
		//打开文件对话框
		OpenFileDialog OpenFileDlg = new OpenFileDialog();
		OpenFileDlg.Filter = "所有图片类型文件 | *.*";
		OpenFileDlg.Title = "请选择图片文件";
		
		DialogResult dlgResult = OpenFileDlg.ShowDialog();
		if(dlgResult == DialogResult.OK) {
			
			Bitmap t = new Bitmap( OpenFileDlg.FileName );
			bmp = new Bitmap( t );
			pictureBox1.BackgroundImage = new Bitmap( bmp );
			t.Dispose();
			
			ignoreRefresh = true;
			this.textBox宽度.Text = (bmp.Width).ToString();
			this.textBox高度.Text = (bmp.Height).ToString();
			ignoreRefresh = false;
		}
	}
}
}





