﻿/*
 * 由 SharpDevelop 创建。
 * 用户: bbb_2
 * 日期: 2009-11-17
 * 时间: 11:15
 * 
 * 如果你想要更改该模板，那么请使用“工具 | 选项 | 编辑 | 编辑标准标题”。
 */
namespace n_CbmpForm
{
	partial class CbmpForm
	{
		/// <summary>
		/// Designer variable used to keep track of non-visual components.
		/// </summary>
		private System.ComponentModel.IContainer components = null;
		
		/// <summary>
		/// Disposes resources used by the form.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing) {
				if (components != null) {
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}
		
		/// <summary>
		/// This method is required for Windows Forms designer support.
		/// Do not change the method contents inside the source code editor. The Forms designer might
		/// not be able to load this method if it was changed manually.
		/// </summary>
		private void InitializeComponent()
		{
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(CbmpForm));
			this.textBox宽度 = new System.Windows.Forms.TextBox();
			this.textBox高度 = new System.Windows.Forms.TextBox();
			this.panel1 = new System.Windows.Forms.Panel();
			this.label2 = new System.Windows.Forms.Label();
			this.buttonOpenBitmap = new System.Windows.Forms.Button();
			this.label5 = new System.Windows.Forms.Label();
			this.button确定 = new System.Windows.Forms.Button();
			this.label1 = new System.Windows.Forms.Label();
			this.panel2 = new System.Windows.Forms.Panel();
			this.pictureBox1 = new System.Windows.Forms.PictureBox();
			this.panel1.SuspendLayout();
			this.panel2.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
			this.SuspendLayout();
			// 
			// textBox宽度
			// 
			this.textBox宽度.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
			resources.ApplyResources(this.textBox宽度, "textBox宽度");
			this.textBox宽度.Name = "textBox宽度";
			this.textBox宽度.TextChanged += new System.EventHandler(this.TextBox宽度TextChanged);
			// 
			// textBox高度
			// 
			this.textBox高度.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
			resources.ApplyResources(this.textBox高度, "textBox高度");
			this.textBox高度.Name = "textBox高度";
			this.textBox高度.TextChanged += new System.EventHandler(this.TextBox高度TextChanged);
			// 
			// panel1
			// 
			this.panel1.BackColor = System.Drawing.Color.WhiteSmoke;
			this.panel1.Controls.Add(this.textBox高度);
			this.panel1.Controls.Add(this.label2);
			this.panel1.Controls.Add(this.buttonOpenBitmap);
			this.panel1.Controls.Add(this.label5);
			this.panel1.Controls.Add(this.button确定);
			this.panel1.Controls.Add(this.textBox宽度);
			this.panel1.Controls.Add(this.label1);
			resources.ApplyResources(this.panel1, "panel1");
			this.panel1.Name = "panel1";
			// 
			// label2
			// 
			resources.ApplyResources(this.label2, "label2");
			this.label2.Name = "label2";
			// 
			// buttonOpenBitmap
			// 
			this.buttonOpenBitmap.BackColor = System.Drawing.Color.Silver;
			resources.ApplyResources(this.buttonOpenBitmap, "buttonOpenBitmap");
			this.buttonOpenBitmap.ForeColor = System.Drawing.Color.Black;
			this.buttonOpenBitmap.Name = "buttonOpenBitmap";
			this.buttonOpenBitmap.UseVisualStyleBackColor = false;
			this.buttonOpenBitmap.Click += new System.EventHandler(this.ButtonOpenBitmapClick);
			// 
			// label5
			// 
			this.label5.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(241)))), ((int)(((byte)(241)))), ((int)(((byte)(241)))));
			resources.ApplyResources(this.label5, "label5");
			this.label5.Name = "label5";
			// 
			// button确定
			// 
			this.button确定.BackColor = System.Drawing.Color.CornflowerBlue;
			resources.ApplyResources(this.button确定, "button确定");
			this.button确定.ForeColor = System.Drawing.Color.Black;
			this.button确定.Name = "button确定";
			this.button确定.UseVisualStyleBackColor = false;
			this.button确定.Click += new System.EventHandler(this.Button确定Click);
			// 
			// label1
			// 
			resources.ApplyResources(this.label1, "label1");
			this.label1.Name = "label1";
			// 
			// panel2
			// 
			resources.ApplyResources(this.panel2, "panel2");
			this.panel2.BackColor = System.Drawing.Color.WhiteSmoke;
			this.panel2.Controls.Add(this.pictureBox1);
			this.panel2.Name = "panel2";
			// 
			// pictureBox1
			// 
			resources.ApplyResources(this.pictureBox1, "pictureBox1");
			this.pictureBox1.Name = "pictureBox1";
			this.pictureBox1.TabStop = false;
			// 
			// CbmpForm
			// 
			resources.ApplyResources(this, "$this");
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.BackColor = System.Drawing.Color.White;
			this.Controls.Add(this.panel2);
			this.Controls.Add(this.panel1);
			this.MaximizeBox = false;
			this.MinimizeBox = false;
			this.Name = "CbmpForm";
			this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.ModuleFormClosing);
			this.panel1.ResumeLayout(false);
			this.panel1.PerformLayout();
			this.panel2.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
			this.ResumeLayout(false);
		}
		private System.Windows.Forms.PictureBox pictureBox1;
		private System.Windows.Forms.Label label1;
		private System.Windows.Forms.Label label2;
		private System.Windows.Forms.Label label5;
		private System.Windows.Forms.Button buttonOpenBitmap;
		private System.Windows.Forms.TextBox textBox宽度;
		private System.Windows.Forms.Button button确定;
		private System.Windows.Forms.TextBox textBox高度;
		private System.Windows.Forms.Panel panel2;
		private System.Windows.Forms.Panel panel1;
	}
}
