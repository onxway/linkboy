﻿
namespace n_SelectPanel
{
using System;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Windows.Forms;

using n_MyObject;
using n_MyObjectList;
using n_MyControl;
using n_MyIns;
using n_UserFunctionIns;
using n_IfElseIns;
using n_WhileIns;
using n_LoopIns;
using n_EventIns;
using n_ForeverIns;
using n_CondiEndIns;
using n_ElseIns;

//*****************************************************
//硬件组件面板类
public class SelectPanel
{
	int SX;
	int SY;
	int Width;
	int Height;
	
	bool isMouseOn;
	
	public int SelectState;
	int SelectStartX;
	int SelectStartY;
	int SelectEndX;
	int SelectEndY;
	
	Brush SelectBrush;
	Brush SelectedBrush;
	Brush MouseOnBrush;
	
	MyObjectList myModuleList;
	
	bool isMousePress;
	int Last_mX, Last_mY;
	int Start_X, Start_Y;
	
	public bool ExistSelect;
	
	//构造函数
	public SelectPanel( MyObjectList vmyModuleList )
	{
		myModuleList = vmyModuleList;
		
		SX = 0;
		SY = 0;
		Width = 0;
		Height = 0;
		
		SelectState = 0;
		SelectStartX = 0;
		SelectStartY = 0;
		SelectEndX = 0;
		SelectEndY = 0;
		SelectBrush = new SolidBrush( Color.FromArgb( 10, 200, 100, 0 ) );
		SelectedBrush = new SolidBrush( Color.FromArgb( 20, 200, 100, 0 ) );
		MouseOnBrush = new SolidBrush( Color.FromArgb( 30, 200, 100, 0 ) );
		
		isMousePress = false;
		Last_mX = 0;
		Last_mY = 0;
	}
	
	//组件绘制工作
	public void Draw( Graphics g )
	{
		//显示选中的区域
		if( SelectState == 0 ) {
			return;
		}
		
		//g.DrawString( "元素选中功能尚未完成, 请勿选中界面元素", n_GUIset.GUIset.ModuleNameFont, Brushes.WhiteSmoke, StartX + SX + 5, StartY + SY + 5 );
		
		if( SelectState == 1 ) {
			g.FillRectangle( SelectBrush, SX, SY, Width, Height );
			g.DrawRectangle( Pens.Goldenrod, SX, SY, Width, Height );
			int L = 8;
			g.DrawLine( Pens.Black, SelectStartX - L, SelectStartY - L, SelectStartX + L, SelectStartY + L );
			g.DrawLine( Pens.Black, SelectStartX - L, SelectStartY + L, SelectStartX + L, SelectStartY - L );
			
			g.DrawString( "按下删除键(Delete)可以删除选中的元素", n_GUIset.GUIset.ExpFont, Brushes.Goldenrod, SX, SY - 25 );
		}
		
		if( SelectState == 2 ) {
			
			if( isMouseOn ) {
				g.FillRectangle( MouseOnBrush, SX, SY, Width, Height );
				g.DrawRectangle( Pens.OrangeRed, SX, SY, Width, Height );
			}
			else {
				g.FillRectangle( SelectedBrush, SX, SY, Width, Height );
				g.DrawRectangle( Pens.Orange, SX, SY, Width, Height );
			}
		}
	}
	
	//鼠标左键按下事件
	public bool LeftMouseDown( int mX, int mY )
	{
		//if( SelectState != 0 ) {
			//G.CGPanel.GHead.CopyButton.Visible = false;
			//G.CGPanel.GHead.PasteButton.Visible = false;
			//G.CGPanel.GHead.BeifenButton.Visible = false;
			//SelectState = 0;
		//}
		
		if( isMouseOn ) {
			isMousePress = true;
			Last_mX = mX;
			Last_mY = mY;
			
			Start_X = mX;
			Start_Y = mY;
		}
		else {
			isMousePress = false;
		}
		
		return isMousePress;
	}
	
	//鼠标左键松开事件
	public void LeftMouseUp( int mX, int mY )
	{
		isMousePress = false;
		
		if ( SelectState == 2 ) {
			foreach( MyObject mo in myModuleList ) {
				
				//判断是否需要对齐网格
				if( mo.isSelect && mo is n_HardModule.HardModule ) {
					n_HardModule.HardModule hd = (n_HardModule.HardModule)mo;
					if( hd.isCircuit || n_HardModule.Port.C_Rol == 0 ) {
						hd.ADjToCross();
					}
				}
				mo.RefreshPanelLocation();
			}
		}
	}
	
	//鼠标右键按下事件
	public void RightMouseDown( int mX, int mY )
	{
		ClearList();
		//G.CGPanel.GHead.CopyButton.Visible = false;
		//G.CGPanel.GHead.PasteButton.Visible = false;
		//G.CGPanel.GHead.BeifenButton.Visible = false;
		
		if( SelectState != 0 ) {
			SelectState = 0;
			//return;
		}
		SelectState = 1;
		
		SelectStartX = mX;
		SelectStartY = mY;
		SelectEndX = SelectStartX;
		SelectEndY = SelectStartY;
		SX = 0;
		SY = 0;
		Width = 0;
		Height = 0;
		ExistSelect = false;
	}
	
	//鼠标右键松开事件
	public void RightMouseUp( int mX, int mY )
	{
		if( SelectState == 1 ) {
			SelectState = 2;
		}
		
		if( !ExistSelect ) {
			SelectState = 0;
		}
		foreach( MyObject m in myModuleList ) {
			if( m is MyIns && m.isSelect ) {
				MyIns mi = (MyIns)m;
				if( mi.PreIns != null && !mi.PreIns.isSelect || mi.NextIns != null && !mi.NextIns.isSelect ) {
					SelectState = 0;
					return;
				}
				if( mi.PreIns == null && !(mi is EventIns) && !(mi is UserFunctionIns) ) {
					SelectState = 0;
					return;
				}
			}
		}
	}
	
	//鼠标移动事件,当鼠标在组件上时返回1,组件被拖动时返回2
	public void MouseMove( int mX, int mY )
	{
		//正在选中组件
		if ( SelectState == 0 ) {
			isMouseOn = false;
			return;
		}
		if ( SelectState == 1 ) {
			
			SelectEndX = mX;
			SelectEndY = mY;
			
			//计算位置和尺寸
			if( SelectStartX < SelectEndX ) {
				Width = SelectEndX - SelectStartX;
				SX = SelectStartX;
			}
			else {
				Width = SelectStartX - SelectEndX;
				SX = SelectEndX;
			}
			if( SelectStartY < SelectEndY ) {
				Height = SelectEndY - SelectStartY;
				SY = SelectStartY;
			}
			else {
				Height = SelectStartY - SelectEndY;
				SY = SelectEndY;
			}
			ClearList();
			
			foreach( MyObject m in myModuleList ) {
				
				//判断范围
				//if( m.SX > SX && m.SX < SX + Width && m.SY > SY && m.SY < SY + Height ||
				//	m.SX + m.Width > SX && m.SX + m.Width < SX + Width && m.SY + m.Height > SY && m.SY + m.Height < SY + Height ||
				//	m.SX > SX && m.SX < SX + Width && m.SY + m.Height > SY && m.SY + m.Height < SY + Height ||
				//	m.SX + m.Width > SX && m.SX + m.Width < SX + Width && m.SY > SY && m.SY < SY + Height ) {
				
				if( !m.isVisible() ) {
					continue;
				}
				
				if( m.SX > SX && m.SX < SX + Width && m.SY + m.Height/2 > SY && m.SY + m.Height/2 < SY + Height ||
					m.SX + m.Width > SX && m.SX + m.Width < SX + Width && m.SY + m.Height/2 > SY && m.SY + m.Height/2 < SY + Height ) {
					
					//if( !m.isSelect ) {
					m.isSelect = true;
					
					ExistSelect = true;
					
					if( m is n_MyIns.MyIns ) {
						
						if( m is IfElseIns || m is WhileIns || m is ForeverIns || m is LoopIns || m is n_ExtIns.ExtIns || m is EventIns || m is UserFunctionIns ) {
							SelectAllIns( (n_MyIns.MyIns)m );
						}
						if( m is CondiEndIns ) {
							CondiEndIns ci = (CondiEndIns)m;
							ci.InsStart.isSelect = true;
							SelectAllIns( ci.InsStart );
						}
						if( m is ElseIns ) {
							ElseIns ci = (ElseIns)m;
							ci.InsStart.isSelect = true;
							SelectAllIns( ci.InsStart );
						}
					}
					//}
					//选中一部分组合式组件时, 也同时选中其他剩余的组件
					if( m is n_HardModule.HardModule && ((n_HardModule.HardModule)m).PackOwner != null ) {
						foreach( n_MyObject.MyObject mo in G.CGPanel.myModuleList ) {
							if( mo == m ) {
								continue;
							}
							if( !(mo is n_HardModule.HardModule) ) {
								continue;
							}
							n_HardModule.HardModule mm = (n_HardModule.HardModule)mo;
							if( mm.PackOwner != null ) {
								mm.isSelect = true;
							}
						}
					}
				}
				
				//else {
				//	m.isSelect = false;
				//}
			}
			foreach( n_GroupList.Group m in G.CGPanel.mGroupList ) {
				if( m.SX > SX && m.SX < SX + Width && m.SY + m.Height/2 > SY && m.SY + m.Height/2 < SY + Height ||
					m.SX + m.Width > SX && m.SX + m.Width < SX + Width && m.SY + m.Height/2 > SY && m.SY + m.Height/2 < SY + Height ) {
					
					foreach( MyObject m0 in myModuleList ) {
						if( !m.Expand && m0.GroupMes == m.GMes ) {
							m0.isSelect = true;
						}
					}
				}
			}
		}
		//已经选中组件
		else {
			
			isMouseOn = MouseIsInside( mX, mY );
			
			if( isMousePress ) {
				SX += (mX - Last_mX);
				SY += (mY - Last_mY);
				
				foreach( MyObject mo in myModuleList ) {
					if( mo.isSelect ) {
						mo.SX += (int)(mX - Last_mX);
						mo.SY += (int)(mY - Last_mY);
						mo.RefreshPanelLocation();
					}
				}
				Last_mX = mX;
				Last_mY = mY;
			}
		}
	}
	
	//添加新指令时取消所有指令的选择, 防止拖动出问题
	public void CancelSelect()
	{
		if( ExistSelect ) {
			ClearList();
			ExistSelect = false;
		}
	}
	
	//删除选中的模块
	public void DeleteSelect()
	{
		foreach( MyObject mo in myModuleList ) {
			if( SelectState == 0 && mo is MyIns ) {
				continue;
			}
			if( mo.isSelect ) {
				mo.Remove();
			}
		}
		G.CGPanel.RefreshResourceError();
		ExistSelect = false;
		SelectState = 0;
	}
	
	void SelectAllIns( MyIns StartIns )
	{
		MyIns temp = StartIns;
		int Level = 0;
		int ElseLevel = 0;
		
		while( true ) {
			
			if( temp is EventIns ) {
				Level++;
			}
			if( temp is UserFunctionIns ) {
				Level++;
			}
			if( temp is LoopIns ) {
				Level++;
			}
			if( temp is n_ExtIns.ExtIns ) {
				Level++;
			}
			if( temp is ForeverIns ) {
				Level++;
			}
			if( temp is WhileIns ) {
				Level++;
			}
			if( temp is IfElseIns ) {
				ElseLevel++;
				Level++;
			}
			if( temp is CondiEndIns ) {
				Level--;
			}
			if( temp is ElseIns ) {
				if( ElseLevel == 0 ) {
					Level--;
				}
				else {
					ElseLevel--;
				}
			}
			temp.isSelect = true;
			if( Level == 0 ) {
					temp = temp.PreIns;
					break;
			}
			if( temp.NextIns == null ) {
				break;
			}
			temp =  temp.NextIns;
		}
	}
	
	//判断鼠标是否在当前组件上
	bool MouseIsInside( int mX, int mY )
	{
		if( mX >= SX && mX <= SX + Width && mY >= SY && mY <= SY + Height ) {
			return true;
		}
		else {
			return false;
		}
	}
	
	//清空选中组件列表
	public void ClearList()
	{
		foreach( MyObject m in myModuleList ) {
			m.isSelect = false;
		}
	}
	
	bool isClickShowPanel( MyObject o )
	{
		if( !(this.myModuleList.MouseOnObject is n_MyFileObject.MyFileObject) &&
		    !(this.myModuleList.MouseOnObject is n_GVar.GVar) &&
			!(this.myModuleList.MouseOnObject is n_GNote.GNote) ) {
				return false;
		}
		return true;
	}
}
}



