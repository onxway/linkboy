﻿/*
 * 由 SharpDevelop 创建。
 * 用户: bbb_2
 * 日期: 2009-11-17
 * 时间: 11:15
 * 
 * 如果你想要更改该模板，那么请使用“工具 | 选项 | 编辑 | 编辑标准标题”。
 */
namespace n_UserFuncEXPForm
{
	partial class UserFuncEXPForm
	{
		/// <summary>
		/// Designer variable used to keep track of non-visual components.
		/// </summary>
		private System.ComponentModel.IContainer components = null;
		
		/// <summary>
		/// Disposes resources used by the form.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing) {
				if (components != null) {
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}
		
		/// <summary>
		/// This method is required for Windows Forms designer support.
		/// Do not change the method contents inside the source code editor. The Forms designer might
		/// not be able to load this method if it was changed manually.
		/// </summary>
		private void InitializeComponent()
		{
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(UserFuncEXPForm));
			this.button确定 = new System.Windows.Forms.Button();
			this.label1 = new System.Windows.Forms.Label();
			this.comboBoxReturn = new System.Windows.Forms.ComboBox();
			this.textBox1 = new System.Windows.Forms.RichTextBox();
			this.label2 = new System.Windows.Forms.Label();
			this.buttonBool = new System.Windows.Forms.Button();
			this.button_Int = new System.Windows.Forms.Button();
			this.textBox2 = new System.Windows.Forms.TextBox();
			this.label3 = new System.Windows.Forms.Label();
			this.label4 = new System.Windows.Forms.Label();
			this.buttonAddString = new System.Windows.Forms.Button();
			this.buttonSetEvent = new System.Windows.Forms.Button();
			this.buttonAddFix = new System.Windows.Forms.Button();
			this.SuspendLayout();
			// 
			// button确定
			// 
			this.button确定.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(128)))), ((int)(((byte)(255)))));
			resources.ApplyResources(this.button确定, "button确定");
			this.button确定.ForeColor = System.Drawing.Color.White;
			this.button确定.Name = "button确定";
			this.button确定.UseVisualStyleBackColor = false;
			this.button确定.Click += new System.EventHandler(this.Button确定Click);
			// 
			// label1
			// 
			resources.ApplyResources(this.label1, "label1");
			this.label1.ForeColor = System.Drawing.Color.DimGray;
			this.label1.Name = "label1";
			// 
			// comboBoxReturn
			// 
			this.comboBoxReturn.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
			resources.ApplyResources(this.comboBoxReturn, "comboBoxReturn");
			this.comboBoxReturn.FormattingEnabled = true;
			this.comboBoxReturn.Items.AddRange(new object[] {
									resources.GetString("comboBoxReturn.Items"),
									resources.GetString("comboBoxReturn.Items1"),
									resources.GetString("comboBoxReturn.Items2"),
									resources.GetString("comboBoxReturn.Items3")});
			this.comboBoxReturn.Name = "comboBoxReturn";
			// 
			// textBox1
			// 
			this.textBox1.BorderStyle = System.Windows.Forms.BorderStyle.None;
			this.textBox1.DetectUrls = false;
			resources.ApplyResources(this.textBox1, "textBox1");
			this.textBox1.Name = "textBox1";
			this.textBox1.TextChanged += new System.EventHandler(this.TextBox1TextChanged);
			// 
			// label2
			// 
			resources.ApplyResources(this.label2, "label2");
			this.label2.ForeColor = System.Drawing.Color.DimGray;
			this.label2.Name = "label2";
			// 
			// buttonBool
			// 
			this.buttonBool.BackColor = System.Drawing.Color.ForestGreen;
			resources.ApplyResources(this.buttonBool, "buttonBool");
			this.buttonBool.ForeColor = System.Drawing.Color.White;
			this.buttonBool.Name = "buttonBool";
			this.buttonBool.UseVisualStyleBackColor = false;
			this.buttonBool.Click += new System.EventHandler(this.ButtonBoolClick);
			// 
			// button_Int
			// 
			this.button_Int.BackColor = System.Drawing.Color.Chocolate;
			resources.ApplyResources(this.button_Int, "button_Int");
			this.button_Int.ForeColor = System.Drawing.Color.White;
			this.button_Int.Name = "button_Int";
			this.button_Int.UseVisualStyleBackColor = false;
			this.button_Int.Click += new System.EventHandler(this.ButtonIntClick);
			// 
			// textBox2
			// 
			this.textBox2.BorderStyle = System.Windows.Forms.BorderStyle.None;
			resources.ApplyResources(this.textBox2, "textBox2");
			this.textBox2.Name = "textBox2";
			// 
			// label3
			// 
			resources.ApplyResources(this.label3, "label3");
			this.label3.ForeColor = System.Drawing.Color.Black;
			this.label3.Name = "label3";
			// 
			// label4
			// 
			this.label4.BackColor = System.Drawing.Color.WhiteSmoke;
			resources.ApplyResources(this.label4, "label4");
			this.label4.Name = "label4";
			// 
			// buttonAddString
			// 
			this.buttonAddString.BackColor = System.Drawing.Color.LightSeaGreen;
			resources.ApplyResources(this.buttonAddString, "buttonAddString");
			this.buttonAddString.ForeColor = System.Drawing.Color.White;
			this.buttonAddString.Name = "buttonAddString";
			this.buttonAddString.UseVisualStyleBackColor = false;
			this.buttonAddString.Click += new System.EventHandler(this.ButtonAddStringClick);
			// 
			// buttonSetEvent
			// 
			this.buttonSetEvent.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
			resources.ApplyResources(this.buttonSetEvent, "buttonSetEvent");
			this.buttonSetEvent.ForeColor = System.Drawing.Color.Silver;
			this.buttonSetEvent.Name = "buttonSetEvent";
			this.buttonSetEvent.UseVisualStyleBackColor = false;
			this.buttonSetEvent.Click += new System.EventHandler(this.ButtonSetEventClick);
			// 
			// buttonAddFix
			// 
			this.buttonAddFix.BackColor = System.Drawing.Color.DarkKhaki;
			resources.ApplyResources(this.buttonAddFix, "buttonAddFix");
			this.buttonAddFix.ForeColor = System.Drawing.Color.White;
			this.buttonAddFix.Name = "buttonAddFix";
			this.buttonAddFix.UseVisualStyleBackColor = false;
			this.buttonAddFix.Click += new System.EventHandler(this.ButtonAddFixClick);
			// 
			// UserFuncEXPForm
			// 
			resources.ApplyResources(this, "$this");
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
			this.Controls.Add(this.textBox2);
			this.Controls.Add(this.label1);
			this.Controls.Add(this.buttonAddFix);
			this.Controls.Add(this.buttonSetEvent);
			this.Controls.Add(this.buttonAddString);
			this.Controls.Add(this.label4);
			this.Controls.Add(this.button_Int);
			this.Controls.Add(this.buttonBool);
			this.Controls.Add(this.textBox1);
			this.Controls.Add(this.comboBoxReturn);
			this.Controls.Add(this.button确定);
			this.Controls.Add(this.label2);
			this.Controls.Add(this.label3);
			this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
			this.MaximizeBox = false;
			this.MinimizeBox = false;
			this.Name = "UserFuncEXPForm";
			this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.UserFuncEXPFormFormClosing);
			this.ResumeLayout(false);
			this.PerformLayout();
		}
		private System.Windows.Forms.Button buttonAddFix;
		private System.Windows.Forms.Button buttonSetEvent;
		private System.Windows.Forms.Button buttonAddString;
		private System.Windows.Forms.Label label4;
		private System.Windows.Forms.Label label3;
		private System.Windows.Forms.TextBox textBox2;
		private System.Windows.Forms.Button button_Int;
		private System.Windows.Forms.Button buttonBool;
		private System.Windows.Forms.Label label2;
		private System.Windows.Forms.ComboBox comboBoxReturn;
		private System.Windows.Forms.Label label1;
		private System.Windows.Forms.RichTextBox textBox1;
		private System.Windows.Forms.Button button确定;
	}
}
