﻿
namespace n_UserFuncEXPForm
{
using System;
using System.Windows.Forms;
using System.IO;
using System.Drawing;
using n_VarType;
using n_UnitList;
using n_FunctionList;
using n_VarList;
using n_StructList;
using n_ImagePanel;
using n_MyObjectList;
using n_MyObject;
using n_HardModule;
using n_GVar;
using n_GUIcoder;
using n_MyFileObject;
using c_FormMover;
using n_EXP;
using n_EXPcommon;

/// <summary>
/// Description of WorkForm.
/// </summary>
public partial class UserFuncEXPForm : Form
{
	bool isOK;
	
	bool isEvent;
	FormMover fm;
	
	char V;
	
	//主窗口
	public UserFuncEXPForm()
	{
		InitializeComponent();
		isOK = false;
		
		fm = new FormMover( this );
	}
	
	//运行
	public string Run( string mes )
	{
		if( this.Visible ) {
			return null;
		}
		
		isOK = false;
		isEvent = false;
		
		V = 'b';
		this.textBox2.Text = V.ToString();
		
		string Rtype = mes.Remove( mes.IndexOf( ' ' ) );
		string FName = mes.Remove( 0, mes.IndexOf( ' ' ) + 1 );
		
		if( Rtype == "?void" ) {
			this.comboBoxReturn.SelectedIndex = 0;
		}
		else if( Rtype == "?bool" ) {
			this.comboBoxReturn.SelectedIndex = 1;
		}
		else if( Rtype == "?int32" ) {
			this.comboBoxReturn.SelectedIndex = 2;
		}
		else if( Rtype == "?fix" ) {
			this.comboBoxReturn.SelectedIndex = 3;
		}
		else {
			
		}
		
		string[] cut = FName.Split( ' ' );
		FName = "";
		for( int i = 0; i < cut.Length; ++i ) {
			if( cut[i] == "?int32" ) {
				cut[i] = "[";
				cut[i+1] += "]";
			}
			if( cut[i] == "?fix" ) {
				cut[i] = "{";
				cut[i+1] += "}";
			}
			if( cut[i] == "?bool" ) {
				cut[i] = "(";
				cut[i+1] += ")";
			}
			if( cut[i] == "?Cstring" ) {
				cut[i] = "<";
				cut[i+1] += ">";
			}
			FName += cut[i];
		}
		
		this.textBox1.SelectionStart = 0;
		this.textBox1.SelectionLength = 0;
		this.textBox1.Text = FName;
		
		//注意,直接设置Visible不能使窗体获得焦点
		//this.Visible = true;
		this.ShowDialog();
		
		while( this.Visible ) {
			System.Windows.Forms.Application.DoEvents();
		}
		if( isOK ) {
			string rr = this.textBox1.Text.Trim( "\t ".ToCharArray() );
			rr = rr.Replace( " ", "" );
			rr = rr.Replace( "{", " ?fix " );
			rr = rr.Replace( "[", " ?int32 " );
			rr = rr.Replace( "(", " ?bool " );
			rr = rr.Replace( "<", " ?Cstring " );
			rr = rr.Replace( "}", " " );
			rr = rr.Replace( "]", " " );
			rr = rr.Replace( ")", " " );
			rr = rr.Replace( ">", " " );
			
			Rtype = "";
			if( isEvent ) {
				Rtype = "*";
			}
			
			if( this.comboBoxReturn.SelectedIndex == 0 ) {
				Rtype += "?void";
			}
			else if( this.comboBoxReturn.SelectedIndex == 1 ) {
				Rtype += "?bool";
			}
			else if( this.comboBoxReturn.SelectedIndex == 2 ) {
				Rtype += "?int32";
			}
			else if( this.comboBoxReturn.SelectedIndex == 3 ) {
				Rtype += "?fix";
			}
			else {
				
			}
			return Rtype + " " + rr;
		}
		return null;
	}
	
	void Button确定Click(object sender, EventArgs e)
	{
		string rr = this.textBox1.Text.Trim( "\t ".ToCharArray() );
		if( rr == "" ) {
			MessageBox.Show( "自定义指令名称不能为空" );
			return;
		}
		if( !n_CharType.CharType.isLetter( rr[0] ) ) {
			MessageBox.Show( "自定义指令名称第一个字符应为字母或者中文" );
			return;
		}
		this.textBox1.Focus();
		isOK = true;
		this.Visible = false;
	}
	
	void TextBox1TextChanged(object sender, EventArgs e)
	{
		int ci = this.textBox1.SelectionStart;
		
		string s = this.textBox1.Text;
		this.textBox1.SelectAll();
		this.textBox1.SelectedText = "";
		this.textBox1.Text = "";
		this.textBox1.SelectedText = s;
		this.textBox1.SelectionColor = Color.Black;
		
		try {
		string mes = this.textBox1.Text;
		for( int i = 0; i < mes.Length; ++i ) {
			if( mes[i] == '[' ) {
				int start = i;
				for( int j = i; j < mes.Length; ++j ) {
					if( mes[j] == ']' ) {
						this.textBox1.SelectionStart = start;
						this.textBox1.SelectionLength = j - i + 1;
						this.textBox1.SelectionColor = Color.Chocolate;
						i = j;
						break;
					}
				}
			}
			if( mes[i] == '{' ) {
				int start = i;
				for( int j = i; j < mes.Length; ++j ) {
					if( mes[j] == '}' ) {
						this.textBox1.SelectionStart = start;
						this.textBox1.SelectionLength = j - i + 1;
						this.textBox1.SelectionColor = Color.DarkKhaki;
						i = j;
						break;
					}
				}
			}
			if( mes[i] == '(' ) {
				int start = i;
				for( int j = i; j < mes.Length; ++j ) {
					if( mes[j] == ')' ) {
						this.textBox1.SelectionStart = start;
						this.textBox1.SelectionLength = j - i + 1;
						this.textBox1.SelectionColor = Color.ForestGreen;
						i = j;
						break;
					}
				}
			}
			if( mes[i] == '<' ) {
				int start = i;
				for( int j = i; j < mes.Length; ++j ) {
					if( mes[j] == '>' ) {
						this.textBox1.SelectionStart = start;
						this.textBox1.SelectionLength = j - i + 1;
						this.textBox1.SelectionColor = Color.LightSeaGreen;
						i = j;
						break;
					}
				}
			}
		}
		}catch(Exception ee) {
			MessageBox.Show( ee.ToString() );
		}
		this.textBox1.SelectionStart = ci;
		this.textBox1.SelectionLength = 0;
	}
	
	void ButtonSetEventClick(object sender, EventArgs e)
	{
		if( MessageBox.Show( "本功能为高手专用, 小白勿用! 您确定要设置本指令为框架事件吗?", "创建框架事件", MessageBoxButtons.YesNo ) == DialogResult.Yes ) {
			isEvent = true;
		}
	}
	
	//================================================================================
	
	void ButtonIntClick(object sender, EventArgs e)
	{
		this.textBox1.SelectedText = "[_" + this.textBox2.Text + "]";
		V++;
		this.textBox2.Text = V.ToString();
	}
	
	void ButtonBoolClick(object sender, EventArgs e)
	{
		this.textBox1.SelectedText = "(_" + this.textBox2.Text + ")";
		V++;
		this.textBox2.Text = V.ToString();
	}
	
	void ButtonAddStringClick(object sender, EventArgs e)
	{
		this.textBox1.SelectedText = "<_" + this.textBox2.Text + ">";
		V++;
		this.textBox2.Text = V.ToString();
	}
	
	void ButtonAddFixClick(object sender, EventArgs e)
	{
		this.textBox1.SelectedText = "{_" + this.textBox2.Text + "}";
		V++;
		this.textBox2.Text = V.ToString();
	}
	
	//================================================================================
	
	void UserFuncEXPFormFormClosing(object sender, FormClosingEventArgs e)
	{
		e.Cancel = true;
		this.Visible = false;
	}
}
}

