﻿
namespace n_ExpPanel
{
using System;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Windows.Forms;

using n_GUIcoder;
using n_GUIset;

public class ExpPanel : Panel
{
	public static Font extendFont;
	public static Font extendHeadFont;
	
	//构造函数
	public ExpPanel() : base()
	{
		this.BorderStyle = BorderStyle.None;
		this.BackColor = Color.Black;
		
		SetStyle(ControlStyles.UserPaint |
		         ControlStyles.AllPaintingInWmPaint |
		         ControlStyles.OptimizedDoubleBuffer |
		         ControlStyles.ResizeRedraw |
		         ControlStyles.SupportsTransparentBackColor,true);
		
		this.Dock = DockStyle.Fill;
		this.ImeMode = ImeMode.NoControl;
		
		this.MouseMove += new MouseEventHandler( UserMouseMove );
		this.MouseUp += new MouseEventHandler( UserMouseUp );
		this.MouseDown += new MouseEventHandler( UserMouseDown );
		this.MouseWheel += new MouseEventHandler( MyMouseWheel );
		
		extendFont = new Font( "微软雅黑", 11 );
		extendHeadFont = new Font( "微软雅黑", 12, FontStyle.Bold );
	}
	
	//重绘事件
	protected override void OnPaint(PaintEventArgs e)
	{
		Graphics g = e.Graphics;
		g.SmoothingMode = SmoothingMode.HighQuality;
		g.Clear( Color.WhiteSmoke );
		
		//绘制组件信息
		g.DrawString( "12345", extendHeadFont, Brushes.DarkRed, 20, 70 );
		g.FillEllipse( Brushes.Blue, 30, 60, 40, 60 );
	}
	
	void MyMouseWheel( object sender, MouseEventArgs e)
	{
		if( e.Delta > 0 ) {
			//StartY += 50;
		}
		else {
			//StartY -= 50;
		}
		this.Invalidate();
	}
	
	//鼠标按下事件
	void UserMouseDown( object sender, MouseEventArgs e )
	{
		if( e.Button == MouseButtons.Left ) {
			
		}
		if( e.Button == MouseButtons.Right ) {
			
		}
		this.Invalidate();
		
		this.Focus();
	}
	
	//鼠标松开事件
	void UserMouseUp( object sender, MouseEventArgs e )
	{
		if( e.Button == MouseButtons.Left ) {
			
		}
		if( e.Button == MouseButtons.Right ) {
			
		}
		this.Invalidate();
	}
	
	//鼠标移动事件
	void UserMouseMove( object sender, MouseEventArgs e )
	{
		//...
		
		this.Invalidate();
	}
}
}


