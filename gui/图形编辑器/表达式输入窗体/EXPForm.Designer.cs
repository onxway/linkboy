﻿/*
 * 由 SharpDevelop 创建。
 * 用户: bbb_2
 * 日期: 2009-11-17
 * 时间: 11:15
 * 
 * 如果你想要更改该模板，那么请使用“工具 | 选项 | 编辑 | 编辑标准标题”。
 */
namespace n_EXPForm
{
	partial class EXPForm
	{
		/// <summary>
		/// Designer variable used to keep track of non-visual components.
		/// </summary>
		private System.ComponentModel.IContainer components = null;
		
		/// <summary>
		/// Disposes resources used by the form.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing) {
				if (components != null) {
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}
		
		/// <summary>
		/// This method is required for Windows Forms designer support.
		/// Do not change the method contents inside the source code editor. The Forms designer might
		/// not be able to load this method if it was changed manually.
		/// </summary>
		private void InitializeComponent()
		{
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(EXPForm));
			this.button确定 = new System.Windows.Forms.Button();
			this.button取消 = new System.Windows.Forms.Button();
			this.textBox1 = new System.Windows.Forms.TextBox();
			this.listBox成员列表 = new System.Windows.Forms.ListBox();
			this.listBox组件列表 = new System.Windows.Forms.ListBox();
			this.panelNumber = new System.Windows.Forms.Panel();
			this.buttonNO = new System.Windows.Forms.Button();
			this.buttonYES = new System.Windows.Forms.Button();
			this.buttonCE = new System.Windows.Forms.Button();
			this.button11 = new System.Windows.Forms.Button();
			this.button10 = new System.Windows.Forms.Button();
			this.button9 = new System.Windows.Forms.Button();
			this.button8 = new System.Windows.Forms.Button();
			this.button7 = new System.Windows.Forms.Button();
			this.button6 = new System.Windows.Forms.Button();
			this.button5 = new System.Windows.Forms.Button();
			this.button4 = new System.Windows.Forms.Button();
			this.button3 = new System.Windows.Forms.Button();
			this.button2 = new System.Windows.Forms.Button();
			this.button1 = new System.Windows.Forms.Button();
			this.panelOper = new System.Windows.Forms.Panel();
			this.button赋值 = new System.Windows.Forms.Button();
			this.button等于 = new System.Windows.Forms.Button();
			this.button小于等于 = new System.Windows.Forms.Button();
			this.button大于等于 = new System.Windows.Forms.Button();
			this.button小于 = new System.Windows.Forms.Button();
			this.button大于 = new System.Windows.Forms.Button();
			this.button不等于 = new System.Windows.Forms.Button();
			this.button或者 = new System.Windows.Forms.Button();
			this.button并且 = new System.Windows.Forms.Button();
			this.button余 = new System.Windows.Forms.Button();
			this.button除 = new System.Windows.Forms.Button();
			this.button乘 = new System.Windows.Forms.Button();
			this.button减 = new System.Windows.Forms.Button();
			this.button加 = new System.Windows.Forms.Button();
			this.button不成立 = new System.Windows.Forms.Button();
			this.button绝对值 = new System.Windows.Forms.Button();
			this.button取负 = new System.Windows.Forms.Button();
			this.labelExpType = new System.Windows.Forms.Label();
			this.panelNumber.SuspendLayout();
			this.panelOper.SuspendLayout();
			this.SuspendLayout();
			// 
			// button确定
			// 
			this.button确定.AccessibleDescription = null;
			this.button确定.AccessibleName = null;
			this.button确定.Anchor = ((System.Windows.Forms.AnchorStyles)(resources.GetObject("button确定.Anchor")));
			this.button确定.AutoSize = ((bool)(resources.GetObject("button确定.AutoSize")));
			this.button确定.AutoSizeMode = ((System.Windows.Forms.AutoSizeMode)(resources.GetObject("button确定.AutoSizeMode")));
			this.button确定.BackColor = System.Drawing.Color.Green;
			this.button确定.BackgroundImage = null;
			this.button确定.BackgroundImageLayout = ((System.Windows.Forms.ImageLayout)(resources.GetObject("button确定.BackgroundImageLayout")));
			this.button确定.Dock = ((System.Windows.Forms.DockStyle)(resources.GetObject("button确定.Dock")));
			this.button确定.FlatStyle = ((System.Windows.Forms.FlatStyle)(resources.GetObject("button确定.FlatStyle")));
			this.button确定.Font = ((System.Drawing.Font)(resources.GetObject("button确定.Font")));
			this.button确定.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
			this.button确定.ImageAlign = ((System.Drawing.ContentAlignment)(resources.GetObject("button确定.ImageAlign")));
			this.button确定.ImageIndex = ((int)(resources.GetObject("button确定.ImageIndex")));
			this.button确定.ImageKey = resources.GetString("button确定.ImageKey");
			this.button确定.ImeMode = ((System.Windows.Forms.ImeMode)(resources.GetObject("button确定.ImeMode")));
			this.button确定.Location = ((System.Drawing.Point)(resources.GetObject("button确定.Location")));
			this.button确定.Name = "button确定";
			this.button确定.RightToLeft = ((System.Windows.Forms.RightToLeft)(resources.GetObject("button确定.RightToLeft")));
			this.button确定.Size = ((System.Drawing.Size)(resources.GetObject("button确定.Size")));
			this.button确定.TabIndex = ((int)(resources.GetObject("button确定.TabIndex")));
			this.button确定.Text = resources.GetString("button确定.Text");
			this.button确定.TextAlign = ((System.Drawing.ContentAlignment)(resources.GetObject("button确定.TextAlign")));
			this.button确定.TextImageRelation = ((System.Windows.Forms.TextImageRelation)(resources.GetObject("button确定.TextImageRelation")));
			this.button确定.UseVisualStyleBackColor = false;
			this.button确定.Click += new System.EventHandler(this.Button确定Click);
			// 
			// button取消
			// 
			this.button取消.AccessibleDescription = null;
			this.button取消.AccessibleName = null;
			this.button取消.Anchor = ((System.Windows.Forms.AnchorStyles)(resources.GetObject("button取消.Anchor")));
			this.button取消.AutoSize = ((bool)(resources.GetObject("button取消.AutoSize")));
			this.button取消.AutoSizeMode = ((System.Windows.Forms.AutoSizeMode)(resources.GetObject("button取消.AutoSizeMode")));
			this.button取消.BackColor = System.Drawing.Color.Maroon;
			this.button取消.BackgroundImage = null;
			this.button取消.BackgroundImageLayout = ((System.Windows.Forms.ImageLayout)(resources.GetObject("button取消.BackgroundImageLayout")));
			this.button取消.Dock = ((System.Windows.Forms.DockStyle)(resources.GetObject("button取消.Dock")));
			this.button取消.FlatStyle = ((System.Windows.Forms.FlatStyle)(resources.GetObject("button取消.FlatStyle")));
			this.button取消.Font = ((System.Drawing.Font)(resources.GetObject("button取消.Font")));
			this.button取消.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(192)))));
			this.button取消.ImageAlign = ((System.Drawing.ContentAlignment)(resources.GetObject("button取消.ImageAlign")));
			this.button取消.ImageIndex = ((int)(resources.GetObject("button取消.ImageIndex")));
			this.button取消.ImageKey = resources.GetString("button取消.ImageKey");
			this.button取消.ImeMode = ((System.Windows.Forms.ImeMode)(resources.GetObject("button取消.ImeMode")));
			this.button取消.Location = ((System.Drawing.Point)(resources.GetObject("button取消.Location")));
			this.button取消.Name = "button取消";
			this.button取消.RightToLeft = ((System.Windows.Forms.RightToLeft)(resources.GetObject("button取消.RightToLeft")));
			this.button取消.Size = ((System.Drawing.Size)(resources.GetObject("button取消.Size")));
			this.button取消.TabIndex = ((int)(resources.GetObject("button取消.TabIndex")));
			this.button取消.Text = resources.GetString("button取消.Text");
			this.button取消.TextAlign = ((System.Drawing.ContentAlignment)(resources.GetObject("button取消.TextAlign")));
			this.button取消.TextImageRelation = ((System.Windows.Forms.TextImageRelation)(resources.GetObject("button取消.TextImageRelation")));
			this.button取消.UseVisualStyleBackColor = false;
			this.button取消.Click += new System.EventHandler(this.Button取消Click);
			// 
			// textBox1
			// 
			this.textBox1.AccessibleDescription = null;
			this.textBox1.AccessibleName = null;
			this.textBox1.Anchor = ((System.Windows.Forms.AnchorStyles)(resources.GetObject("textBox1.Anchor")));
			this.textBox1.BackgroundImage = null;
			this.textBox1.BackgroundImageLayout = ((System.Windows.Forms.ImageLayout)(resources.GetObject("textBox1.BackgroundImageLayout")));
			this.textBox1.Dock = ((System.Windows.Forms.DockStyle)(resources.GetObject("textBox1.Dock")));
			this.textBox1.Font = ((System.Drawing.Font)(resources.GetObject("textBox1.Font")));
			this.textBox1.ImeMode = ((System.Windows.Forms.ImeMode)(resources.GetObject("textBox1.ImeMode")));
			this.textBox1.Location = ((System.Drawing.Point)(resources.GetObject("textBox1.Location")));
			this.textBox1.MaxLength = ((int)(resources.GetObject("textBox1.MaxLength")));
			this.textBox1.Multiline = ((bool)(resources.GetObject("textBox1.Multiline")));
			this.textBox1.Name = "textBox1";
			this.textBox1.PasswordChar = ((char)(resources.GetObject("textBox1.PasswordChar")));
			this.textBox1.RightToLeft = ((System.Windows.Forms.RightToLeft)(resources.GetObject("textBox1.RightToLeft")));
			this.textBox1.ScrollBars = ((System.Windows.Forms.ScrollBars)(resources.GetObject("textBox1.ScrollBars")));
			this.textBox1.Size = ((System.Drawing.Size)(resources.GetObject("textBox1.Size")));
			this.textBox1.TabIndex = ((int)(resources.GetObject("textBox1.TabIndex")));
			this.textBox1.TextAlign = ((System.Windows.Forms.HorizontalAlignment)(resources.GetObject("textBox1.TextAlign")));
			this.textBox1.WordWrap = ((bool)(resources.GetObject("textBox1.WordWrap")));
			this.textBox1.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.TextBox1KeyPress);
			// 
			// listBox成员列表
			// 
			this.listBox成员列表.AccessibleDescription = null;
			this.listBox成员列表.AccessibleName = null;
			this.listBox成员列表.Anchor = ((System.Windows.Forms.AnchorStyles)(resources.GetObject("listBox成员列表.Anchor")));
			this.listBox成员列表.BackgroundImage = null;
			this.listBox成员列表.BackgroundImageLayout = ((System.Windows.Forms.ImageLayout)(resources.GetObject("listBox成员列表.BackgroundImageLayout")));
			this.listBox成员列表.ColumnWidth = ((int)(resources.GetObject("listBox成员列表.ColumnWidth")));
			this.listBox成员列表.Dock = ((System.Windows.Forms.DockStyle)(resources.GetObject("listBox成员列表.Dock")));
			this.listBox成员列表.Font = ((System.Drawing.Font)(resources.GetObject("listBox成员列表.Font")));
			this.listBox成员列表.FormattingEnabled = true;
			this.listBox成员列表.HorizontalExtent = ((int)(resources.GetObject("listBox成员列表.HorizontalExtent")));
			this.listBox成员列表.HorizontalScrollbar = ((bool)(resources.GetObject("listBox成员列表.HorizontalScrollbar")));
			this.listBox成员列表.ImeMode = ((System.Windows.Forms.ImeMode)(resources.GetObject("listBox成员列表.ImeMode")));
			this.listBox成员列表.IntegralHeight = ((bool)(resources.GetObject("listBox成员列表.IntegralHeight")));
			this.listBox成员列表.ItemHeight = ((int)(resources.GetObject("listBox成员列表.ItemHeight")));
			this.listBox成员列表.Location = ((System.Drawing.Point)(resources.GetObject("listBox成员列表.Location")));
			this.listBox成员列表.Name = "listBox成员列表";
			this.listBox成员列表.RightToLeft = ((System.Windows.Forms.RightToLeft)(resources.GetObject("listBox成员列表.RightToLeft")));
			this.listBox成员列表.ScrollAlwaysVisible = ((bool)(resources.GetObject("listBox成员列表.ScrollAlwaysVisible")));
			this.listBox成员列表.Size = ((System.Drawing.Size)(resources.GetObject("listBox成员列表.Size")));
			this.listBox成员列表.TabIndex = ((int)(resources.GetObject("listBox成员列表.TabIndex")));
			this.listBox成员列表.Click += new System.EventHandler(this.ListBox成员列表Click);
			// 
			// listBox组件列表
			// 
			this.listBox组件列表.AccessibleDescription = null;
			this.listBox组件列表.AccessibleName = null;
			this.listBox组件列表.Anchor = ((System.Windows.Forms.AnchorStyles)(resources.GetObject("listBox组件列表.Anchor")));
			this.listBox组件列表.BackgroundImage = null;
			this.listBox组件列表.BackgroundImageLayout = ((System.Windows.Forms.ImageLayout)(resources.GetObject("listBox组件列表.BackgroundImageLayout")));
			this.listBox组件列表.ColumnWidth = ((int)(resources.GetObject("listBox组件列表.ColumnWidth")));
			this.listBox组件列表.Dock = ((System.Windows.Forms.DockStyle)(resources.GetObject("listBox组件列表.Dock")));
			this.listBox组件列表.Font = ((System.Drawing.Font)(resources.GetObject("listBox组件列表.Font")));
			this.listBox组件列表.FormattingEnabled = true;
			this.listBox组件列表.HorizontalExtent = ((int)(resources.GetObject("listBox组件列表.HorizontalExtent")));
			this.listBox组件列表.HorizontalScrollbar = ((bool)(resources.GetObject("listBox组件列表.HorizontalScrollbar")));
			this.listBox组件列表.ImeMode = ((System.Windows.Forms.ImeMode)(resources.GetObject("listBox组件列表.ImeMode")));
			this.listBox组件列表.IntegralHeight = ((bool)(resources.GetObject("listBox组件列表.IntegralHeight")));
			this.listBox组件列表.ItemHeight = ((int)(resources.GetObject("listBox组件列表.ItemHeight")));
			this.listBox组件列表.Location = ((System.Drawing.Point)(resources.GetObject("listBox组件列表.Location")));
			this.listBox组件列表.Name = "listBox组件列表";
			this.listBox组件列表.RightToLeft = ((System.Windows.Forms.RightToLeft)(resources.GetObject("listBox组件列表.RightToLeft")));
			this.listBox组件列表.ScrollAlwaysVisible = ((bool)(resources.GetObject("listBox组件列表.ScrollAlwaysVisible")));
			this.listBox组件列表.Size = ((System.Drawing.Size)(resources.GetObject("listBox组件列表.Size")));
			this.listBox组件列表.TabIndex = ((int)(resources.GetObject("listBox组件列表.TabIndex")));
			this.listBox组件列表.Click += new System.EventHandler(this.ListBox组件列表Click);
			// 
			// panelNumber
			// 
			this.panelNumber.AccessibleDescription = null;
			this.panelNumber.AccessibleName = null;
			this.panelNumber.Anchor = ((System.Windows.Forms.AnchorStyles)(resources.GetObject("panelNumber.Anchor")));
			this.panelNumber.AutoScroll = ((bool)(resources.GetObject("panelNumber.AutoScroll")));
			this.panelNumber.AutoSize = ((bool)(resources.GetObject("panelNumber.AutoSize")));
			this.panelNumber.AutoSizeMode = ((System.Windows.Forms.AutoSizeMode)(resources.GetObject("panelNumber.AutoSizeMode")));
			this.panelNumber.BackColor = System.Drawing.Color.DarkGray;
			this.panelNumber.BackgroundImage = null;
			this.panelNumber.BackgroundImageLayout = ((System.Windows.Forms.ImageLayout)(resources.GetObject("panelNumber.BackgroundImageLayout")));
			this.panelNumber.Controls.Add(this.buttonNO);
			this.panelNumber.Controls.Add(this.buttonYES);
			this.panelNumber.Controls.Add(this.buttonCE);
			this.panelNumber.Controls.Add(this.button11);
			this.panelNumber.Controls.Add(this.button10);
			this.panelNumber.Controls.Add(this.button9);
			this.panelNumber.Controls.Add(this.button8);
			this.panelNumber.Controls.Add(this.button7);
			this.panelNumber.Controls.Add(this.button6);
			this.panelNumber.Controls.Add(this.button5);
			this.panelNumber.Controls.Add(this.button4);
			this.panelNumber.Controls.Add(this.button3);
			this.panelNumber.Controls.Add(this.button2);
			this.panelNumber.Controls.Add(this.button1);
			this.panelNumber.Dock = ((System.Windows.Forms.DockStyle)(resources.GetObject("panelNumber.Dock")));
			this.panelNumber.Font = null;
			this.panelNumber.ImeMode = ((System.Windows.Forms.ImeMode)(resources.GetObject("panelNumber.ImeMode")));
			this.panelNumber.Location = ((System.Drawing.Point)(resources.GetObject("panelNumber.Location")));
			this.panelNumber.Name = "panelNumber";
			this.panelNumber.RightToLeft = ((System.Windows.Forms.RightToLeft)(resources.GetObject("panelNumber.RightToLeft")));
			this.panelNumber.Size = ((System.Drawing.Size)(resources.GetObject("panelNumber.Size")));
			this.panelNumber.TabIndex = ((int)(resources.GetObject("panelNumber.TabIndex")));
			// 
			// buttonNO
			// 
			this.buttonNO.AccessibleDescription = null;
			this.buttonNO.AccessibleName = null;
			this.buttonNO.Anchor = ((System.Windows.Forms.AnchorStyles)(resources.GetObject("buttonNO.Anchor")));
			this.buttonNO.AutoSize = ((bool)(resources.GetObject("buttonNO.AutoSize")));
			this.buttonNO.AutoSizeMode = ((System.Windows.Forms.AutoSizeMode)(resources.GetObject("buttonNO.AutoSizeMode")));
			this.buttonNO.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(192)))), ((int)(((byte)(255)))));
			this.buttonNO.BackgroundImage = null;
			this.buttonNO.BackgroundImageLayout = ((System.Windows.Forms.ImageLayout)(resources.GetObject("buttonNO.BackgroundImageLayout")));
			this.buttonNO.Dock = ((System.Windows.Forms.DockStyle)(resources.GetObject("buttonNO.Dock")));
			this.buttonNO.FlatStyle = ((System.Windows.Forms.FlatStyle)(resources.GetObject("buttonNO.FlatStyle")));
			this.buttonNO.Font = ((System.Drawing.Font)(resources.GetObject("buttonNO.Font")));
			this.buttonNO.ForeColor = System.Drawing.Color.Blue;
			this.buttonNO.ImageAlign = ((System.Drawing.ContentAlignment)(resources.GetObject("buttonNO.ImageAlign")));
			this.buttonNO.ImageIndex = ((int)(resources.GetObject("buttonNO.ImageIndex")));
			this.buttonNO.ImageKey = resources.GetString("buttonNO.ImageKey");
			this.buttonNO.ImeMode = ((System.Windows.Forms.ImeMode)(resources.GetObject("buttonNO.ImeMode")));
			this.buttonNO.Location = ((System.Drawing.Point)(resources.GetObject("buttonNO.Location")));
			this.buttonNO.Name = "buttonNO";
			this.buttonNO.RightToLeft = ((System.Windows.Forms.RightToLeft)(resources.GetObject("buttonNO.RightToLeft")));
			this.buttonNO.Size = ((System.Drawing.Size)(resources.GetObject("buttonNO.Size")));
			this.buttonNO.TabIndex = ((int)(resources.GetObject("buttonNO.TabIndex")));
			this.buttonNO.Text = resources.GetString("buttonNO.Text");
			this.buttonNO.TextAlign = ((System.Drawing.ContentAlignment)(resources.GetObject("buttonNO.TextAlign")));
			this.buttonNO.TextImageRelation = ((System.Windows.Forms.TextImageRelation)(resources.GetObject("buttonNO.TextImageRelation")));
			this.buttonNO.UseVisualStyleBackColor = false;
			this.buttonNO.Click += new System.EventHandler(this.ButtonNOClick);
			// 
			// buttonYES
			// 
			this.buttonYES.AccessibleDescription = null;
			this.buttonYES.AccessibleName = null;
			this.buttonYES.Anchor = ((System.Windows.Forms.AnchorStyles)(resources.GetObject("buttonYES.Anchor")));
			this.buttonYES.AutoSize = ((bool)(resources.GetObject("buttonYES.AutoSize")));
			this.buttonYES.AutoSizeMode = ((System.Windows.Forms.AutoSizeMode)(resources.GetObject("buttonYES.AutoSizeMode")));
			this.buttonYES.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(192)))), ((int)(((byte)(255)))));
			this.buttonYES.BackgroundImage = null;
			this.buttonYES.BackgroundImageLayout = ((System.Windows.Forms.ImageLayout)(resources.GetObject("buttonYES.BackgroundImageLayout")));
			this.buttonYES.Dock = ((System.Windows.Forms.DockStyle)(resources.GetObject("buttonYES.Dock")));
			this.buttonYES.FlatStyle = ((System.Windows.Forms.FlatStyle)(resources.GetObject("buttonYES.FlatStyle")));
			this.buttonYES.Font = ((System.Drawing.Font)(resources.GetObject("buttonYES.Font")));
			this.buttonYES.ForeColor = System.Drawing.Color.Blue;
			this.buttonYES.ImageAlign = ((System.Drawing.ContentAlignment)(resources.GetObject("buttonYES.ImageAlign")));
			this.buttonYES.ImageIndex = ((int)(resources.GetObject("buttonYES.ImageIndex")));
			this.buttonYES.ImageKey = resources.GetString("buttonYES.ImageKey");
			this.buttonYES.ImeMode = ((System.Windows.Forms.ImeMode)(resources.GetObject("buttonYES.ImeMode")));
			this.buttonYES.Location = ((System.Drawing.Point)(resources.GetObject("buttonYES.Location")));
			this.buttonYES.Name = "buttonYES";
			this.buttonYES.RightToLeft = ((System.Windows.Forms.RightToLeft)(resources.GetObject("buttonYES.RightToLeft")));
			this.buttonYES.Size = ((System.Drawing.Size)(resources.GetObject("buttonYES.Size")));
			this.buttonYES.TabIndex = ((int)(resources.GetObject("buttonYES.TabIndex")));
			this.buttonYES.Text = resources.GetString("buttonYES.Text");
			this.buttonYES.TextAlign = ((System.Drawing.ContentAlignment)(resources.GetObject("buttonYES.TextAlign")));
			this.buttonYES.TextImageRelation = ((System.Windows.Forms.TextImageRelation)(resources.GetObject("buttonYES.TextImageRelation")));
			this.buttonYES.UseVisualStyleBackColor = false;
			this.buttonYES.Click += new System.EventHandler(this.ButtonYESClick);
			// 
			// buttonCE
			// 
			this.buttonCE.AccessibleDescription = null;
			this.buttonCE.AccessibleName = null;
			this.buttonCE.Anchor = ((System.Windows.Forms.AnchorStyles)(resources.GetObject("buttonCE.Anchor")));
			this.buttonCE.AutoSize = ((bool)(resources.GetObject("buttonCE.AutoSize")));
			this.buttonCE.AutoSizeMode = ((System.Windows.Forms.AutoSizeMode)(resources.GetObject("buttonCE.AutoSizeMode")));
			this.buttonCE.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(192)))));
			this.buttonCE.BackgroundImage = null;
			this.buttonCE.BackgroundImageLayout = ((System.Windows.Forms.ImageLayout)(resources.GetObject("buttonCE.BackgroundImageLayout")));
			this.buttonCE.Dock = ((System.Windows.Forms.DockStyle)(resources.GetObject("buttonCE.Dock")));
			this.buttonCE.FlatStyle = ((System.Windows.Forms.FlatStyle)(resources.GetObject("buttonCE.FlatStyle")));
			this.buttonCE.Font = ((System.Drawing.Font)(resources.GetObject("buttonCE.Font")));
			this.buttonCE.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(64)))), ((int)(((byte)(0)))));
			this.buttonCE.ImageAlign = ((System.Drawing.ContentAlignment)(resources.GetObject("buttonCE.ImageAlign")));
			this.buttonCE.ImageIndex = ((int)(resources.GetObject("buttonCE.ImageIndex")));
			this.buttonCE.ImageKey = resources.GetString("buttonCE.ImageKey");
			this.buttonCE.ImeMode = ((System.Windows.Forms.ImeMode)(resources.GetObject("buttonCE.ImeMode")));
			this.buttonCE.Location = ((System.Drawing.Point)(resources.GetObject("buttonCE.Location")));
			this.buttonCE.Name = "buttonCE";
			this.buttonCE.RightToLeft = ((System.Windows.Forms.RightToLeft)(resources.GetObject("buttonCE.RightToLeft")));
			this.buttonCE.Size = ((System.Drawing.Size)(resources.GetObject("buttonCE.Size")));
			this.buttonCE.TabIndex = ((int)(resources.GetObject("buttonCE.TabIndex")));
			this.buttonCE.Text = resources.GetString("buttonCE.Text");
			this.buttonCE.TextAlign = ((System.Drawing.ContentAlignment)(resources.GetObject("buttonCE.TextAlign")));
			this.buttonCE.TextImageRelation = ((System.Windows.Forms.TextImageRelation)(resources.GetObject("buttonCE.TextImageRelation")));
			this.buttonCE.UseVisualStyleBackColor = false;
			this.buttonCE.Click += new System.EventHandler(this.ButtonCEClick);
			// 
			// button11
			// 
			this.button11.AccessibleDescription = null;
			this.button11.AccessibleName = null;
			this.button11.Anchor = ((System.Windows.Forms.AnchorStyles)(resources.GetObject("button11.Anchor")));
			this.button11.AutoSize = ((bool)(resources.GetObject("button11.AutoSize")));
			this.button11.AutoSizeMode = ((System.Windows.Forms.AutoSizeMode)(resources.GetObject("button11.AutoSizeMode")));
			this.button11.BackColor = System.Drawing.Color.Silver;
			this.button11.BackgroundImage = null;
			this.button11.BackgroundImageLayout = ((System.Windows.Forms.ImageLayout)(resources.GetObject("button11.BackgroundImageLayout")));
			this.button11.Dock = ((System.Windows.Forms.DockStyle)(resources.GetObject("button11.Dock")));
			this.button11.FlatStyle = ((System.Windows.Forms.FlatStyle)(resources.GetObject("button11.FlatStyle")));
			this.button11.Font = ((System.Drawing.Font)(resources.GetObject("button11.Font")));
			this.button11.ImageAlign = ((System.Drawing.ContentAlignment)(resources.GetObject("button11.ImageAlign")));
			this.button11.ImageIndex = ((int)(resources.GetObject("button11.ImageIndex")));
			this.button11.ImageKey = resources.GetString("button11.ImageKey");
			this.button11.ImeMode = ((System.Windows.Forms.ImeMode)(resources.GetObject("button11.ImeMode")));
			this.button11.Location = ((System.Drawing.Point)(resources.GetObject("button11.Location")));
			this.button11.Name = "button11";
			this.button11.RightToLeft = ((System.Windows.Forms.RightToLeft)(resources.GetObject("button11.RightToLeft")));
			this.button11.Size = ((System.Drawing.Size)(resources.GetObject("button11.Size")));
			this.button11.TabIndex = ((int)(resources.GetObject("button11.TabIndex")));
			this.button11.Text = resources.GetString("button11.Text");
			this.button11.TextAlign = ((System.Drawing.ContentAlignment)(resources.GetObject("button11.TextAlign")));
			this.button11.TextImageRelation = ((System.Windows.Forms.TextImageRelation)(resources.GetObject("button11.TextImageRelation")));
			this.button11.UseVisualStyleBackColor = false;
			this.button11.Click += new System.EventHandler(this.NumberbuttonClick);
			// 
			// button10
			// 
			this.button10.AccessibleDescription = null;
			this.button10.AccessibleName = null;
			this.button10.Anchor = ((System.Windows.Forms.AnchorStyles)(resources.GetObject("button10.Anchor")));
			this.button10.AutoSize = ((bool)(resources.GetObject("button10.AutoSize")));
			this.button10.AutoSizeMode = ((System.Windows.Forms.AutoSizeMode)(resources.GetObject("button10.AutoSizeMode")));
			this.button10.BackColor = System.Drawing.Color.Silver;
			this.button10.BackgroundImage = null;
			this.button10.BackgroundImageLayout = ((System.Windows.Forms.ImageLayout)(resources.GetObject("button10.BackgroundImageLayout")));
			this.button10.Dock = ((System.Windows.Forms.DockStyle)(resources.GetObject("button10.Dock")));
			this.button10.FlatStyle = ((System.Windows.Forms.FlatStyle)(resources.GetObject("button10.FlatStyle")));
			this.button10.Font = ((System.Drawing.Font)(resources.GetObject("button10.Font")));
			this.button10.ImageAlign = ((System.Drawing.ContentAlignment)(resources.GetObject("button10.ImageAlign")));
			this.button10.ImageIndex = ((int)(resources.GetObject("button10.ImageIndex")));
			this.button10.ImageKey = resources.GetString("button10.ImageKey");
			this.button10.ImeMode = ((System.Windows.Forms.ImeMode)(resources.GetObject("button10.ImeMode")));
			this.button10.Location = ((System.Drawing.Point)(resources.GetObject("button10.Location")));
			this.button10.Name = "button10";
			this.button10.RightToLeft = ((System.Windows.Forms.RightToLeft)(resources.GetObject("button10.RightToLeft")));
			this.button10.Size = ((System.Drawing.Size)(resources.GetObject("button10.Size")));
			this.button10.TabIndex = ((int)(resources.GetObject("button10.TabIndex")));
			this.button10.Text = resources.GetString("button10.Text");
			this.button10.TextAlign = ((System.Drawing.ContentAlignment)(resources.GetObject("button10.TextAlign")));
			this.button10.TextImageRelation = ((System.Windows.Forms.TextImageRelation)(resources.GetObject("button10.TextImageRelation")));
			this.button10.UseVisualStyleBackColor = false;
			this.button10.Click += new System.EventHandler(this.NumberbuttonClick);
			// 
			// button9
			// 
			this.button9.AccessibleDescription = null;
			this.button9.AccessibleName = null;
			this.button9.Anchor = ((System.Windows.Forms.AnchorStyles)(resources.GetObject("button9.Anchor")));
			this.button9.AutoSize = ((bool)(resources.GetObject("button9.AutoSize")));
			this.button9.AutoSizeMode = ((System.Windows.Forms.AutoSizeMode)(resources.GetObject("button9.AutoSizeMode")));
			this.button9.BackColor = System.Drawing.Color.Silver;
			this.button9.BackgroundImage = null;
			this.button9.BackgroundImageLayout = ((System.Windows.Forms.ImageLayout)(resources.GetObject("button9.BackgroundImageLayout")));
			this.button9.Dock = ((System.Windows.Forms.DockStyle)(resources.GetObject("button9.Dock")));
			this.button9.FlatStyle = ((System.Windows.Forms.FlatStyle)(resources.GetObject("button9.FlatStyle")));
			this.button9.Font = ((System.Drawing.Font)(resources.GetObject("button9.Font")));
			this.button9.ImageAlign = ((System.Drawing.ContentAlignment)(resources.GetObject("button9.ImageAlign")));
			this.button9.ImageIndex = ((int)(resources.GetObject("button9.ImageIndex")));
			this.button9.ImageKey = resources.GetString("button9.ImageKey");
			this.button9.ImeMode = ((System.Windows.Forms.ImeMode)(resources.GetObject("button9.ImeMode")));
			this.button9.Location = ((System.Drawing.Point)(resources.GetObject("button9.Location")));
			this.button9.Name = "button9";
			this.button9.RightToLeft = ((System.Windows.Forms.RightToLeft)(resources.GetObject("button9.RightToLeft")));
			this.button9.Size = ((System.Drawing.Size)(resources.GetObject("button9.Size")));
			this.button9.TabIndex = ((int)(resources.GetObject("button9.TabIndex")));
			this.button9.Text = resources.GetString("button9.Text");
			this.button9.TextAlign = ((System.Drawing.ContentAlignment)(resources.GetObject("button9.TextAlign")));
			this.button9.TextImageRelation = ((System.Windows.Forms.TextImageRelation)(resources.GetObject("button9.TextImageRelation")));
			this.button9.UseVisualStyleBackColor = false;
			this.button9.Click += new System.EventHandler(this.NumberbuttonClick);
			// 
			// button8
			// 
			this.button8.AccessibleDescription = null;
			this.button8.AccessibleName = null;
			this.button8.Anchor = ((System.Windows.Forms.AnchorStyles)(resources.GetObject("button8.Anchor")));
			this.button8.AutoSize = ((bool)(resources.GetObject("button8.AutoSize")));
			this.button8.AutoSizeMode = ((System.Windows.Forms.AutoSizeMode)(resources.GetObject("button8.AutoSizeMode")));
			this.button8.BackColor = System.Drawing.Color.Silver;
			this.button8.BackgroundImage = null;
			this.button8.BackgroundImageLayout = ((System.Windows.Forms.ImageLayout)(resources.GetObject("button8.BackgroundImageLayout")));
			this.button8.Dock = ((System.Windows.Forms.DockStyle)(resources.GetObject("button8.Dock")));
			this.button8.FlatStyle = ((System.Windows.Forms.FlatStyle)(resources.GetObject("button8.FlatStyle")));
			this.button8.Font = ((System.Drawing.Font)(resources.GetObject("button8.Font")));
			this.button8.ImageAlign = ((System.Drawing.ContentAlignment)(resources.GetObject("button8.ImageAlign")));
			this.button8.ImageIndex = ((int)(resources.GetObject("button8.ImageIndex")));
			this.button8.ImageKey = resources.GetString("button8.ImageKey");
			this.button8.ImeMode = ((System.Windows.Forms.ImeMode)(resources.GetObject("button8.ImeMode")));
			this.button8.Location = ((System.Drawing.Point)(resources.GetObject("button8.Location")));
			this.button8.Name = "button8";
			this.button8.RightToLeft = ((System.Windows.Forms.RightToLeft)(resources.GetObject("button8.RightToLeft")));
			this.button8.Size = ((System.Drawing.Size)(resources.GetObject("button8.Size")));
			this.button8.TabIndex = ((int)(resources.GetObject("button8.TabIndex")));
			this.button8.Text = resources.GetString("button8.Text");
			this.button8.TextAlign = ((System.Drawing.ContentAlignment)(resources.GetObject("button8.TextAlign")));
			this.button8.TextImageRelation = ((System.Windows.Forms.TextImageRelation)(resources.GetObject("button8.TextImageRelation")));
			this.button8.UseVisualStyleBackColor = false;
			this.button8.Click += new System.EventHandler(this.NumberbuttonClick);
			// 
			// button7
			// 
			this.button7.AccessibleDescription = null;
			this.button7.AccessibleName = null;
			this.button7.Anchor = ((System.Windows.Forms.AnchorStyles)(resources.GetObject("button7.Anchor")));
			this.button7.AutoSize = ((bool)(resources.GetObject("button7.AutoSize")));
			this.button7.AutoSizeMode = ((System.Windows.Forms.AutoSizeMode)(resources.GetObject("button7.AutoSizeMode")));
			this.button7.BackColor = System.Drawing.Color.Silver;
			this.button7.BackgroundImage = null;
			this.button7.BackgroundImageLayout = ((System.Windows.Forms.ImageLayout)(resources.GetObject("button7.BackgroundImageLayout")));
			this.button7.Dock = ((System.Windows.Forms.DockStyle)(resources.GetObject("button7.Dock")));
			this.button7.FlatStyle = ((System.Windows.Forms.FlatStyle)(resources.GetObject("button7.FlatStyle")));
			this.button7.Font = ((System.Drawing.Font)(resources.GetObject("button7.Font")));
			this.button7.ImageAlign = ((System.Drawing.ContentAlignment)(resources.GetObject("button7.ImageAlign")));
			this.button7.ImageIndex = ((int)(resources.GetObject("button7.ImageIndex")));
			this.button7.ImageKey = resources.GetString("button7.ImageKey");
			this.button7.ImeMode = ((System.Windows.Forms.ImeMode)(resources.GetObject("button7.ImeMode")));
			this.button7.Location = ((System.Drawing.Point)(resources.GetObject("button7.Location")));
			this.button7.Name = "button7";
			this.button7.RightToLeft = ((System.Windows.Forms.RightToLeft)(resources.GetObject("button7.RightToLeft")));
			this.button7.Size = ((System.Drawing.Size)(resources.GetObject("button7.Size")));
			this.button7.TabIndex = ((int)(resources.GetObject("button7.TabIndex")));
			this.button7.Text = resources.GetString("button7.Text");
			this.button7.TextAlign = ((System.Drawing.ContentAlignment)(resources.GetObject("button7.TextAlign")));
			this.button7.TextImageRelation = ((System.Windows.Forms.TextImageRelation)(resources.GetObject("button7.TextImageRelation")));
			this.button7.UseVisualStyleBackColor = false;
			this.button7.Click += new System.EventHandler(this.NumberbuttonClick);
			// 
			// button6
			// 
			this.button6.AccessibleDescription = null;
			this.button6.AccessibleName = null;
			this.button6.Anchor = ((System.Windows.Forms.AnchorStyles)(resources.GetObject("button6.Anchor")));
			this.button6.AutoSize = ((bool)(resources.GetObject("button6.AutoSize")));
			this.button6.AutoSizeMode = ((System.Windows.Forms.AutoSizeMode)(resources.GetObject("button6.AutoSizeMode")));
			this.button6.BackColor = System.Drawing.Color.Silver;
			this.button6.BackgroundImage = null;
			this.button6.BackgroundImageLayout = ((System.Windows.Forms.ImageLayout)(resources.GetObject("button6.BackgroundImageLayout")));
			this.button6.Dock = ((System.Windows.Forms.DockStyle)(resources.GetObject("button6.Dock")));
			this.button6.FlatStyle = ((System.Windows.Forms.FlatStyle)(resources.GetObject("button6.FlatStyle")));
			this.button6.Font = ((System.Drawing.Font)(resources.GetObject("button6.Font")));
			this.button6.ImageAlign = ((System.Drawing.ContentAlignment)(resources.GetObject("button6.ImageAlign")));
			this.button6.ImageIndex = ((int)(resources.GetObject("button6.ImageIndex")));
			this.button6.ImageKey = resources.GetString("button6.ImageKey");
			this.button6.ImeMode = ((System.Windows.Forms.ImeMode)(resources.GetObject("button6.ImeMode")));
			this.button6.Location = ((System.Drawing.Point)(resources.GetObject("button6.Location")));
			this.button6.Name = "button6";
			this.button6.RightToLeft = ((System.Windows.Forms.RightToLeft)(resources.GetObject("button6.RightToLeft")));
			this.button6.Size = ((System.Drawing.Size)(resources.GetObject("button6.Size")));
			this.button6.TabIndex = ((int)(resources.GetObject("button6.TabIndex")));
			this.button6.Text = resources.GetString("button6.Text");
			this.button6.TextAlign = ((System.Drawing.ContentAlignment)(resources.GetObject("button6.TextAlign")));
			this.button6.TextImageRelation = ((System.Windows.Forms.TextImageRelation)(resources.GetObject("button6.TextImageRelation")));
			this.button6.UseVisualStyleBackColor = false;
			this.button6.Click += new System.EventHandler(this.NumberbuttonClick);
			// 
			// button5
			// 
			this.button5.AccessibleDescription = null;
			this.button5.AccessibleName = null;
			this.button5.Anchor = ((System.Windows.Forms.AnchorStyles)(resources.GetObject("button5.Anchor")));
			this.button5.AutoSize = ((bool)(resources.GetObject("button5.AutoSize")));
			this.button5.AutoSizeMode = ((System.Windows.Forms.AutoSizeMode)(resources.GetObject("button5.AutoSizeMode")));
			this.button5.BackColor = System.Drawing.Color.Silver;
			this.button5.BackgroundImage = null;
			this.button5.BackgroundImageLayout = ((System.Windows.Forms.ImageLayout)(resources.GetObject("button5.BackgroundImageLayout")));
			this.button5.Dock = ((System.Windows.Forms.DockStyle)(resources.GetObject("button5.Dock")));
			this.button5.FlatStyle = ((System.Windows.Forms.FlatStyle)(resources.GetObject("button5.FlatStyle")));
			this.button5.Font = ((System.Drawing.Font)(resources.GetObject("button5.Font")));
			this.button5.ImageAlign = ((System.Drawing.ContentAlignment)(resources.GetObject("button5.ImageAlign")));
			this.button5.ImageIndex = ((int)(resources.GetObject("button5.ImageIndex")));
			this.button5.ImageKey = resources.GetString("button5.ImageKey");
			this.button5.ImeMode = ((System.Windows.Forms.ImeMode)(resources.GetObject("button5.ImeMode")));
			this.button5.Location = ((System.Drawing.Point)(resources.GetObject("button5.Location")));
			this.button5.Name = "button5";
			this.button5.RightToLeft = ((System.Windows.Forms.RightToLeft)(resources.GetObject("button5.RightToLeft")));
			this.button5.Size = ((System.Drawing.Size)(resources.GetObject("button5.Size")));
			this.button5.TabIndex = ((int)(resources.GetObject("button5.TabIndex")));
			this.button5.Text = resources.GetString("button5.Text");
			this.button5.TextAlign = ((System.Drawing.ContentAlignment)(resources.GetObject("button5.TextAlign")));
			this.button5.TextImageRelation = ((System.Windows.Forms.TextImageRelation)(resources.GetObject("button5.TextImageRelation")));
			this.button5.UseVisualStyleBackColor = false;
			this.button5.Click += new System.EventHandler(this.NumberbuttonClick);
			// 
			// button4
			// 
			this.button4.AccessibleDescription = null;
			this.button4.AccessibleName = null;
			this.button4.Anchor = ((System.Windows.Forms.AnchorStyles)(resources.GetObject("button4.Anchor")));
			this.button4.AutoSize = ((bool)(resources.GetObject("button4.AutoSize")));
			this.button4.AutoSizeMode = ((System.Windows.Forms.AutoSizeMode)(resources.GetObject("button4.AutoSizeMode")));
			this.button4.BackColor = System.Drawing.Color.Silver;
			this.button4.BackgroundImage = null;
			this.button4.BackgroundImageLayout = ((System.Windows.Forms.ImageLayout)(resources.GetObject("button4.BackgroundImageLayout")));
			this.button4.Dock = ((System.Windows.Forms.DockStyle)(resources.GetObject("button4.Dock")));
			this.button4.FlatStyle = ((System.Windows.Forms.FlatStyle)(resources.GetObject("button4.FlatStyle")));
			this.button4.Font = ((System.Drawing.Font)(resources.GetObject("button4.Font")));
			this.button4.ImageAlign = ((System.Drawing.ContentAlignment)(resources.GetObject("button4.ImageAlign")));
			this.button4.ImageIndex = ((int)(resources.GetObject("button4.ImageIndex")));
			this.button4.ImageKey = resources.GetString("button4.ImageKey");
			this.button4.ImeMode = ((System.Windows.Forms.ImeMode)(resources.GetObject("button4.ImeMode")));
			this.button4.Location = ((System.Drawing.Point)(resources.GetObject("button4.Location")));
			this.button4.Name = "button4";
			this.button4.RightToLeft = ((System.Windows.Forms.RightToLeft)(resources.GetObject("button4.RightToLeft")));
			this.button4.Size = ((System.Drawing.Size)(resources.GetObject("button4.Size")));
			this.button4.TabIndex = ((int)(resources.GetObject("button4.TabIndex")));
			this.button4.Text = resources.GetString("button4.Text");
			this.button4.TextAlign = ((System.Drawing.ContentAlignment)(resources.GetObject("button4.TextAlign")));
			this.button4.TextImageRelation = ((System.Windows.Forms.TextImageRelation)(resources.GetObject("button4.TextImageRelation")));
			this.button4.UseVisualStyleBackColor = false;
			this.button4.Click += new System.EventHandler(this.NumberbuttonClick);
			// 
			// button3
			// 
			this.button3.AccessibleDescription = null;
			this.button3.AccessibleName = null;
			this.button3.Anchor = ((System.Windows.Forms.AnchorStyles)(resources.GetObject("button3.Anchor")));
			this.button3.AutoSize = ((bool)(resources.GetObject("button3.AutoSize")));
			this.button3.AutoSizeMode = ((System.Windows.Forms.AutoSizeMode)(resources.GetObject("button3.AutoSizeMode")));
			this.button3.BackColor = System.Drawing.Color.Silver;
			this.button3.BackgroundImage = null;
			this.button3.BackgroundImageLayout = ((System.Windows.Forms.ImageLayout)(resources.GetObject("button3.BackgroundImageLayout")));
			this.button3.Dock = ((System.Windows.Forms.DockStyle)(resources.GetObject("button3.Dock")));
			this.button3.FlatStyle = ((System.Windows.Forms.FlatStyle)(resources.GetObject("button3.FlatStyle")));
			this.button3.Font = ((System.Drawing.Font)(resources.GetObject("button3.Font")));
			this.button3.ImageAlign = ((System.Drawing.ContentAlignment)(resources.GetObject("button3.ImageAlign")));
			this.button3.ImageIndex = ((int)(resources.GetObject("button3.ImageIndex")));
			this.button3.ImageKey = resources.GetString("button3.ImageKey");
			this.button3.ImeMode = ((System.Windows.Forms.ImeMode)(resources.GetObject("button3.ImeMode")));
			this.button3.Location = ((System.Drawing.Point)(resources.GetObject("button3.Location")));
			this.button3.Name = "button3";
			this.button3.RightToLeft = ((System.Windows.Forms.RightToLeft)(resources.GetObject("button3.RightToLeft")));
			this.button3.Size = ((System.Drawing.Size)(resources.GetObject("button3.Size")));
			this.button3.TabIndex = ((int)(resources.GetObject("button3.TabIndex")));
			this.button3.Text = resources.GetString("button3.Text");
			this.button3.TextAlign = ((System.Drawing.ContentAlignment)(resources.GetObject("button3.TextAlign")));
			this.button3.TextImageRelation = ((System.Windows.Forms.TextImageRelation)(resources.GetObject("button3.TextImageRelation")));
			this.button3.UseVisualStyleBackColor = false;
			this.button3.Click += new System.EventHandler(this.NumberbuttonClick);
			// 
			// button2
			// 
			this.button2.AccessibleDescription = null;
			this.button2.AccessibleName = null;
			this.button2.Anchor = ((System.Windows.Forms.AnchorStyles)(resources.GetObject("button2.Anchor")));
			this.button2.AutoSize = ((bool)(resources.GetObject("button2.AutoSize")));
			this.button2.AutoSizeMode = ((System.Windows.Forms.AutoSizeMode)(resources.GetObject("button2.AutoSizeMode")));
			this.button2.BackColor = System.Drawing.Color.Silver;
			this.button2.BackgroundImage = null;
			this.button2.BackgroundImageLayout = ((System.Windows.Forms.ImageLayout)(resources.GetObject("button2.BackgroundImageLayout")));
			this.button2.Dock = ((System.Windows.Forms.DockStyle)(resources.GetObject("button2.Dock")));
			this.button2.FlatStyle = ((System.Windows.Forms.FlatStyle)(resources.GetObject("button2.FlatStyle")));
			this.button2.Font = ((System.Drawing.Font)(resources.GetObject("button2.Font")));
			this.button2.ImageAlign = ((System.Drawing.ContentAlignment)(resources.GetObject("button2.ImageAlign")));
			this.button2.ImageIndex = ((int)(resources.GetObject("button2.ImageIndex")));
			this.button2.ImageKey = resources.GetString("button2.ImageKey");
			this.button2.ImeMode = ((System.Windows.Forms.ImeMode)(resources.GetObject("button2.ImeMode")));
			this.button2.Location = ((System.Drawing.Point)(resources.GetObject("button2.Location")));
			this.button2.Name = "button2";
			this.button2.RightToLeft = ((System.Windows.Forms.RightToLeft)(resources.GetObject("button2.RightToLeft")));
			this.button2.Size = ((System.Drawing.Size)(resources.GetObject("button2.Size")));
			this.button2.TabIndex = ((int)(resources.GetObject("button2.TabIndex")));
			this.button2.Text = resources.GetString("button2.Text");
			this.button2.TextAlign = ((System.Drawing.ContentAlignment)(resources.GetObject("button2.TextAlign")));
			this.button2.TextImageRelation = ((System.Windows.Forms.TextImageRelation)(resources.GetObject("button2.TextImageRelation")));
			this.button2.UseVisualStyleBackColor = false;
			this.button2.Click += new System.EventHandler(this.NumberbuttonClick);
			// 
			// button1
			// 
			this.button1.AccessibleDescription = null;
			this.button1.AccessibleName = null;
			this.button1.Anchor = ((System.Windows.Forms.AnchorStyles)(resources.GetObject("button1.Anchor")));
			this.button1.AutoSize = ((bool)(resources.GetObject("button1.AutoSize")));
			this.button1.AutoSizeMode = ((System.Windows.Forms.AutoSizeMode)(resources.GetObject("button1.AutoSizeMode")));
			this.button1.BackColor = System.Drawing.Color.Silver;
			this.button1.BackgroundImage = null;
			this.button1.BackgroundImageLayout = ((System.Windows.Forms.ImageLayout)(resources.GetObject("button1.BackgroundImageLayout")));
			this.button1.Dock = ((System.Windows.Forms.DockStyle)(resources.GetObject("button1.Dock")));
			this.button1.FlatStyle = ((System.Windows.Forms.FlatStyle)(resources.GetObject("button1.FlatStyle")));
			this.button1.Font = ((System.Drawing.Font)(resources.GetObject("button1.Font")));
			this.button1.ImageAlign = ((System.Drawing.ContentAlignment)(resources.GetObject("button1.ImageAlign")));
			this.button1.ImageIndex = ((int)(resources.GetObject("button1.ImageIndex")));
			this.button1.ImageKey = resources.GetString("button1.ImageKey");
			this.button1.ImeMode = ((System.Windows.Forms.ImeMode)(resources.GetObject("button1.ImeMode")));
			this.button1.Location = ((System.Drawing.Point)(resources.GetObject("button1.Location")));
			this.button1.Name = "button1";
			this.button1.RightToLeft = ((System.Windows.Forms.RightToLeft)(resources.GetObject("button1.RightToLeft")));
			this.button1.Size = ((System.Drawing.Size)(resources.GetObject("button1.Size")));
			this.button1.TabIndex = ((int)(resources.GetObject("button1.TabIndex")));
			this.button1.Text = resources.GetString("button1.Text");
			this.button1.TextAlign = ((System.Drawing.ContentAlignment)(resources.GetObject("button1.TextAlign")));
			this.button1.TextImageRelation = ((System.Windows.Forms.TextImageRelation)(resources.GetObject("button1.TextImageRelation")));
			this.button1.UseVisualStyleBackColor = false;
			this.button1.Click += new System.EventHandler(this.NumberbuttonClick);
			// 
			// panelOper
			// 
			this.panelOper.AccessibleDescription = null;
			this.panelOper.AccessibleName = null;
			this.panelOper.Anchor = ((System.Windows.Forms.AnchorStyles)(resources.GetObject("panelOper.Anchor")));
			this.panelOper.AutoScroll = ((bool)(resources.GetObject("panelOper.AutoScroll")));
			this.panelOper.AutoSize = ((bool)(resources.GetObject("panelOper.AutoSize")));
			this.panelOper.AutoSizeMode = ((System.Windows.Forms.AutoSizeMode)(resources.GetObject("panelOper.AutoSizeMode")));
			this.panelOper.BackgroundImage = null;
			this.panelOper.BackgroundImageLayout = ((System.Windows.Forms.ImageLayout)(resources.GetObject("panelOper.BackgroundImageLayout")));
			this.panelOper.Controls.Add(this.button赋值);
			this.panelOper.Controls.Add(this.button等于);
			this.panelOper.Controls.Add(this.button小于等于);
			this.panelOper.Controls.Add(this.button大于等于);
			this.panelOper.Controls.Add(this.button小于);
			this.panelOper.Controls.Add(this.button大于);
			this.panelOper.Controls.Add(this.button不等于);
			this.panelOper.Controls.Add(this.button或者);
			this.panelOper.Controls.Add(this.button并且);
			this.panelOper.Controls.Add(this.button余);
			this.panelOper.Controls.Add(this.button除);
			this.panelOper.Controls.Add(this.button乘);
			this.panelOper.Controls.Add(this.button减);
			this.panelOper.Controls.Add(this.button加);
			this.panelOper.Controls.Add(this.button不成立);
			this.panelOper.Controls.Add(this.button绝对值);
			this.panelOper.Controls.Add(this.button取负);
			this.panelOper.Dock = ((System.Windows.Forms.DockStyle)(resources.GetObject("panelOper.Dock")));
			this.panelOper.Font = null;
			this.panelOper.ImeMode = ((System.Windows.Forms.ImeMode)(resources.GetObject("panelOper.ImeMode")));
			this.panelOper.Location = ((System.Drawing.Point)(resources.GetObject("panelOper.Location")));
			this.panelOper.Name = "panelOper";
			this.panelOper.RightToLeft = ((System.Windows.Forms.RightToLeft)(resources.GetObject("panelOper.RightToLeft")));
			this.panelOper.Size = ((System.Drawing.Size)(resources.GetObject("panelOper.Size")));
			this.panelOper.TabIndex = ((int)(resources.GetObject("panelOper.TabIndex")));
			// 
			// button赋值
			// 
			this.button赋值.AccessibleDescription = null;
			this.button赋值.AccessibleName = null;
			this.button赋值.Anchor = ((System.Windows.Forms.AnchorStyles)(resources.GetObject("button赋值.Anchor")));
			this.button赋值.AutoSize = ((bool)(resources.GetObject("button赋值.AutoSize")));
			this.button赋值.AutoSizeMode = ((System.Windows.Forms.AutoSizeMode)(resources.GetObject("button赋值.AutoSizeMode")));
			this.button赋值.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(255)))));
			this.button赋值.BackgroundImage = null;
			this.button赋值.BackgroundImageLayout = ((System.Windows.Forms.ImageLayout)(resources.GetObject("button赋值.BackgroundImageLayout")));
			this.button赋值.Dock = ((System.Windows.Forms.DockStyle)(resources.GetObject("button赋值.Dock")));
			this.button赋值.FlatStyle = ((System.Windows.Forms.FlatStyle)(resources.GetObject("button赋值.FlatStyle")));
			this.button赋值.Font = ((System.Drawing.Font)(resources.GetObject("button赋值.Font")));
			this.button赋值.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(0)))), ((int)(((byte)(64)))));
			this.button赋值.ImageAlign = ((System.Drawing.ContentAlignment)(resources.GetObject("button赋值.ImageAlign")));
			this.button赋值.ImageIndex = ((int)(resources.GetObject("button赋值.ImageIndex")));
			this.button赋值.ImageKey = resources.GetString("button赋值.ImageKey");
			this.button赋值.ImeMode = ((System.Windows.Forms.ImeMode)(resources.GetObject("button赋值.ImeMode")));
			this.button赋值.Location = ((System.Drawing.Point)(resources.GetObject("button赋值.Location")));
			this.button赋值.Name = "button赋值";
			this.button赋值.RightToLeft = ((System.Windows.Forms.RightToLeft)(resources.GetObject("button赋值.RightToLeft")));
			this.button赋值.Size = ((System.Drawing.Size)(resources.GetObject("button赋值.Size")));
			this.button赋值.TabIndex = ((int)(resources.GetObject("button赋值.TabIndex")));
			this.button赋值.Text = resources.GetString("button赋值.Text");
			this.button赋值.TextAlign = ((System.Drawing.ContentAlignment)(resources.GetObject("button赋值.TextAlign")));
			this.button赋值.TextImageRelation = ((System.Windows.Forms.TextImageRelation)(resources.GetObject("button赋值.TextImageRelation")));
			this.button赋值.UseVisualStyleBackColor = false;
			this.button赋值.Click += new System.EventHandler(this.OperButtonClick);
			// 
			// button等于
			// 
			this.button等于.AccessibleDescription = null;
			this.button等于.AccessibleName = null;
			this.button等于.Anchor = ((System.Windows.Forms.AnchorStyles)(resources.GetObject("button等于.Anchor")));
			this.button等于.AutoSize = ((bool)(resources.GetObject("button等于.AutoSize")));
			this.button等于.AutoSizeMode = ((System.Windows.Forms.AutoSizeMode)(resources.GetObject("button等于.AutoSizeMode")));
			this.button等于.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(128)))), ((int)(((byte)(255)))));
			this.button等于.BackgroundImage = null;
			this.button等于.BackgroundImageLayout = ((System.Windows.Forms.ImageLayout)(resources.GetObject("button等于.BackgroundImageLayout")));
			this.button等于.Dock = ((System.Windows.Forms.DockStyle)(resources.GetObject("button等于.Dock")));
			this.button等于.FlatStyle = ((System.Windows.Forms.FlatStyle)(resources.GetObject("button等于.FlatStyle")));
			this.button等于.Font = ((System.Drawing.Font)(resources.GetObject("button等于.Font")));
			this.button等于.ForeColor = System.Drawing.Color.Navy;
			this.button等于.ImageAlign = ((System.Drawing.ContentAlignment)(resources.GetObject("button等于.ImageAlign")));
			this.button等于.ImageIndex = ((int)(resources.GetObject("button等于.ImageIndex")));
			this.button等于.ImageKey = resources.GetString("button等于.ImageKey");
			this.button等于.ImeMode = ((System.Windows.Forms.ImeMode)(resources.GetObject("button等于.ImeMode")));
			this.button等于.Location = ((System.Drawing.Point)(resources.GetObject("button等于.Location")));
			this.button等于.Name = "button等于";
			this.button等于.RightToLeft = ((System.Windows.Forms.RightToLeft)(resources.GetObject("button等于.RightToLeft")));
			this.button等于.Size = ((System.Drawing.Size)(resources.GetObject("button等于.Size")));
			this.button等于.TabIndex = ((int)(resources.GetObject("button等于.TabIndex")));
			this.button等于.Text = resources.GetString("button等于.Text");
			this.button等于.TextAlign = ((System.Drawing.ContentAlignment)(resources.GetObject("button等于.TextAlign")));
			this.button等于.TextImageRelation = ((System.Windows.Forms.TextImageRelation)(resources.GetObject("button等于.TextImageRelation")));
			this.button等于.UseVisualStyleBackColor = false;
			this.button等于.Click += new System.EventHandler(this.OperButtonClick);
			// 
			// button小于等于
			// 
			this.button小于等于.AccessibleDescription = null;
			this.button小于等于.AccessibleName = null;
			this.button小于等于.Anchor = ((System.Windows.Forms.AnchorStyles)(resources.GetObject("button小于等于.Anchor")));
			this.button小于等于.AutoSize = ((bool)(resources.GetObject("button小于等于.AutoSize")));
			this.button小于等于.AutoSizeMode = ((System.Windows.Forms.AutoSizeMode)(resources.GetObject("button小于等于.AutoSizeMode")));
			this.button小于等于.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(128)))), ((int)(((byte)(255)))));
			this.button小于等于.BackgroundImage = null;
			this.button小于等于.BackgroundImageLayout = ((System.Windows.Forms.ImageLayout)(resources.GetObject("button小于等于.BackgroundImageLayout")));
			this.button小于等于.Dock = ((System.Windows.Forms.DockStyle)(resources.GetObject("button小于等于.Dock")));
			this.button小于等于.FlatStyle = ((System.Windows.Forms.FlatStyle)(resources.GetObject("button小于等于.FlatStyle")));
			this.button小于等于.Font = ((System.Drawing.Font)(resources.GetObject("button小于等于.Font")));
			this.button小于等于.ForeColor = System.Drawing.Color.Navy;
			this.button小于等于.ImageAlign = ((System.Drawing.ContentAlignment)(resources.GetObject("button小于等于.ImageAlign")));
			this.button小于等于.ImageIndex = ((int)(resources.GetObject("button小于等于.ImageIndex")));
			this.button小于等于.ImageKey = resources.GetString("button小于等于.ImageKey");
			this.button小于等于.ImeMode = ((System.Windows.Forms.ImeMode)(resources.GetObject("button小于等于.ImeMode")));
			this.button小于等于.Location = ((System.Drawing.Point)(resources.GetObject("button小于等于.Location")));
			this.button小于等于.Name = "button小于等于";
			this.button小于等于.RightToLeft = ((System.Windows.Forms.RightToLeft)(resources.GetObject("button小于等于.RightToLeft")));
			this.button小于等于.Size = ((System.Drawing.Size)(resources.GetObject("button小于等于.Size")));
			this.button小于等于.TabIndex = ((int)(resources.GetObject("button小于等于.TabIndex")));
			this.button小于等于.Text = resources.GetString("button小于等于.Text");
			this.button小于等于.TextAlign = ((System.Drawing.ContentAlignment)(resources.GetObject("button小于等于.TextAlign")));
			this.button小于等于.TextImageRelation = ((System.Windows.Forms.TextImageRelation)(resources.GetObject("button小于等于.TextImageRelation")));
			this.button小于等于.UseVisualStyleBackColor = false;
			this.button小于等于.Click += new System.EventHandler(this.OperButtonClick);
			// 
			// button大于等于
			// 
			this.button大于等于.AccessibleDescription = null;
			this.button大于等于.AccessibleName = null;
			this.button大于等于.Anchor = ((System.Windows.Forms.AnchorStyles)(resources.GetObject("button大于等于.Anchor")));
			this.button大于等于.AutoSize = ((bool)(resources.GetObject("button大于等于.AutoSize")));
			this.button大于等于.AutoSizeMode = ((System.Windows.Forms.AutoSizeMode)(resources.GetObject("button大于等于.AutoSizeMode")));
			this.button大于等于.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(128)))), ((int)(((byte)(255)))));
			this.button大于等于.BackgroundImage = null;
			this.button大于等于.BackgroundImageLayout = ((System.Windows.Forms.ImageLayout)(resources.GetObject("button大于等于.BackgroundImageLayout")));
			this.button大于等于.Dock = ((System.Windows.Forms.DockStyle)(resources.GetObject("button大于等于.Dock")));
			this.button大于等于.FlatStyle = ((System.Windows.Forms.FlatStyle)(resources.GetObject("button大于等于.FlatStyle")));
			this.button大于等于.Font = ((System.Drawing.Font)(resources.GetObject("button大于等于.Font")));
			this.button大于等于.ForeColor = System.Drawing.Color.Navy;
			this.button大于等于.ImageAlign = ((System.Drawing.ContentAlignment)(resources.GetObject("button大于等于.ImageAlign")));
			this.button大于等于.ImageIndex = ((int)(resources.GetObject("button大于等于.ImageIndex")));
			this.button大于等于.ImageKey = resources.GetString("button大于等于.ImageKey");
			this.button大于等于.ImeMode = ((System.Windows.Forms.ImeMode)(resources.GetObject("button大于等于.ImeMode")));
			this.button大于等于.Location = ((System.Drawing.Point)(resources.GetObject("button大于等于.Location")));
			this.button大于等于.Name = "button大于等于";
			this.button大于等于.RightToLeft = ((System.Windows.Forms.RightToLeft)(resources.GetObject("button大于等于.RightToLeft")));
			this.button大于等于.Size = ((System.Drawing.Size)(resources.GetObject("button大于等于.Size")));
			this.button大于等于.TabIndex = ((int)(resources.GetObject("button大于等于.TabIndex")));
			this.button大于等于.Text = resources.GetString("button大于等于.Text");
			this.button大于等于.TextAlign = ((System.Drawing.ContentAlignment)(resources.GetObject("button大于等于.TextAlign")));
			this.button大于等于.TextImageRelation = ((System.Windows.Forms.TextImageRelation)(resources.GetObject("button大于等于.TextImageRelation")));
			this.button大于等于.UseVisualStyleBackColor = false;
			this.button大于等于.Click += new System.EventHandler(this.OperButtonClick);
			// 
			// button小于
			// 
			this.button小于.AccessibleDescription = null;
			this.button小于.AccessibleName = null;
			this.button小于.Anchor = ((System.Windows.Forms.AnchorStyles)(resources.GetObject("button小于.Anchor")));
			this.button小于.AutoSize = ((bool)(resources.GetObject("button小于.AutoSize")));
			this.button小于.AutoSizeMode = ((System.Windows.Forms.AutoSizeMode)(resources.GetObject("button小于.AutoSizeMode")));
			this.button小于.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(128)))), ((int)(((byte)(255)))));
			this.button小于.BackgroundImage = null;
			this.button小于.BackgroundImageLayout = ((System.Windows.Forms.ImageLayout)(resources.GetObject("button小于.BackgroundImageLayout")));
			this.button小于.Dock = ((System.Windows.Forms.DockStyle)(resources.GetObject("button小于.Dock")));
			this.button小于.FlatStyle = ((System.Windows.Forms.FlatStyle)(resources.GetObject("button小于.FlatStyle")));
			this.button小于.Font = ((System.Drawing.Font)(resources.GetObject("button小于.Font")));
			this.button小于.ForeColor = System.Drawing.Color.Navy;
			this.button小于.ImageAlign = ((System.Drawing.ContentAlignment)(resources.GetObject("button小于.ImageAlign")));
			this.button小于.ImageIndex = ((int)(resources.GetObject("button小于.ImageIndex")));
			this.button小于.ImageKey = resources.GetString("button小于.ImageKey");
			this.button小于.ImeMode = ((System.Windows.Forms.ImeMode)(resources.GetObject("button小于.ImeMode")));
			this.button小于.Location = ((System.Drawing.Point)(resources.GetObject("button小于.Location")));
			this.button小于.Name = "button小于";
			this.button小于.RightToLeft = ((System.Windows.Forms.RightToLeft)(resources.GetObject("button小于.RightToLeft")));
			this.button小于.Size = ((System.Drawing.Size)(resources.GetObject("button小于.Size")));
			this.button小于.TabIndex = ((int)(resources.GetObject("button小于.TabIndex")));
			this.button小于.Text = resources.GetString("button小于.Text");
			this.button小于.TextAlign = ((System.Drawing.ContentAlignment)(resources.GetObject("button小于.TextAlign")));
			this.button小于.TextImageRelation = ((System.Windows.Forms.TextImageRelation)(resources.GetObject("button小于.TextImageRelation")));
			this.button小于.UseVisualStyleBackColor = false;
			this.button小于.Click += new System.EventHandler(this.OperButtonClick);
			// 
			// button大于
			// 
			this.button大于.AccessibleDescription = null;
			this.button大于.AccessibleName = null;
			this.button大于.Anchor = ((System.Windows.Forms.AnchorStyles)(resources.GetObject("button大于.Anchor")));
			this.button大于.AutoSize = ((bool)(resources.GetObject("button大于.AutoSize")));
			this.button大于.AutoSizeMode = ((System.Windows.Forms.AutoSizeMode)(resources.GetObject("button大于.AutoSizeMode")));
			this.button大于.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(128)))), ((int)(((byte)(255)))));
			this.button大于.BackgroundImage = null;
			this.button大于.BackgroundImageLayout = ((System.Windows.Forms.ImageLayout)(resources.GetObject("button大于.BackgroundImageLayout")));
			this.button大于.Dock = ((System.Windows.Forms.DockStyle)(resources.GetObject("button大于.Dock")));
			this.button大于.FlatStyle = ((System.Windows.Forms.FlatStyle)(resources.GetObject("button大于.FlatStyle")));
			this.button大于.Font = ((System.Drawing.Font)(resources.GetObject("button大于.Font")));
			this.button大于.ForeColor = System.Drawing.Color.Navy;
			this.button大于.ImageAlign = ((System.Drawing.ContentAlignment)(resources.GetObject("button大于.ImageAlign")));
			this.button大于.ImageIndex = ((int)(resources.GetObject("button大于.ImageIndex")));
			this.button大于.ImageKey = resources.GetString("button大于.ImageKey");
			this.button大于.ImeMode = ((System.Windows.Forms.ImeMode)(resources.GetObject("button大于.ImeMode")));
			this.button大于.Location = ((System.Drawing.Point)(resources.GetObject("button大于.Location")));
			this.button大于.Name = "button大于";
			this.button大于.RightToLeft = ((System.Windows.Forms.RightToLeft)(resources.GetObject("button大于.RightToLeft")));
			this.button大于.Size = ((System.Drawing.Size)(resources.GetObject("button大于.Size")));
			this.button大于.TabIndex = ((int)(resources.GetObject("button大于.TabIndex")));
			this.button大于.Text = resources.GetString("button大于.Text");
			this.button大于.TextAlign = ((System.Drawing.ContentAlignment)(resources.GetObject("button大于.TextAlign")));
			this.button大于.TextImageRelation = ((System.Windows.Forms.TextImageRelation)(resources.GetObject("button大于.TextImageRelation")));
			this.button大于.UseVisualStyleBackColor = false;
			this.button大于.Click += new System.EventHandler(this.OperButtonClick);
			// 
			// button不等于
			// 
			this.button不等于.AccessibleDescription = null;
			this.button不等于.AccessibleName = null;
			this.button不等于.Anchor = ((System.Windows.Forms.AnchorStyles)(resources.GetObject("button不等于.Anchor")));
			this.button不等于.AutoSize = ((bool)(resources.GetObject("button不等于.AutoSize")));
			this.button不等于.AutoSizeMode = ((System.Windows.Forms.AutoSizeMode)(resources.GetObject("button不等于.AutoSizeMode")));
			this.button不等于.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(128)))), ((int)(((byte)(255)))));
			this.button不等于.BackgroundImage = null;
			this.button不等于.BackgroundImageLayout = ((System.Windows.Forms.ImageLayout)(resources.GetObject("button不等于.BackgroundImageLayout")));
			this.button不等于.Dock = ((System.Windows.Forms.DockStyle)(resources.GetObject("button不等于.Dock")));
			this.button不等于.FlatStyle = ((System.Windows.Forms.FlatStyle)(resources.GetObject("button不等于.FlatStyle")));
			this.button不等于.Font = ((System.Drawing.Font)(resources.GetObject("button不等于.Font")));
			this.button不等于.ForeColor = System.Drawing.Color.Navy;
			this.button不等于.ImageAlign = ((System.Drawing.ContentAlignment)(resources.GetObject("button不等于.ImageAlign")));
			this.button不等于.ImageIndex = ((int)(resources.GetObject("button不等于.ImageIndex")));
			this.button不等于.ImageKey = resources.GetString("button不等于.ImageKey");
			this.button不等于.ImeMode = ((System.Windows.Forms.ImeMode)(resources.GetObject("button不等于.ImeMode")));
			this.button不等于.Location = ((System.Drawing.Point)(resources.GetObject("button不等于.Location")));
			this.button不等于.Name = "button不等于";
			this.button不等于.RightToLeft = ((System.Windows.Forms.RightToLeft)(resources.GetObject("button不等于.RightToLeft")));
			this.button不等于.Size = ((System.Drawing.Size)(resources.GetObject("button不等于.Size")));
			this.button不等于.TabIndex = ((int)(resources.GetObject("button不等于.TabIndex")));
			this.button不等于.Text = resources.GetString("button不等于.Text");
			this.button不等于.TextAlign = ((System.Drawing.ContentAlignment)(resources.GetObject("button不等于.TextAlign")));
			this.button不等于.TextImageRelation = ((System.Windows.Forms.TextImageRelation)(resources.GetObject("button不等于.TextImageRelation")));
			this.button不等于.UseVisualStyleBackColor = false;
			this.button不等于.Click += new System.EventHandler(this.OperButtonClick);
			// 
			// button或者
			// 
			this.button或者.AccessibleDescription = null;
			this.button或者.AccessibleName = null;
			this.button或者.Anchor = ((System.Windows.Forms.AnchorStyles)(resources.GetObject("button或者.Anchor")));
			this.button或者.AutoSize = ((bool)(resources.GetObject("button或者.AutoSize")));
			this.button或者.AutoSizeMode = ((System.Windows.Forms.AutoSizeMode)(resources.GetObject("button或者.AutoSizeMode")));
			this.button或者.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(255)))), ((int)(((byte)(128)))));
			this.button或者.BackgroundImage = null;
			this.button或者.BackgroundImageLayout = ((System.Windows.Forms.ImageLayout)(resources.GetObject("button或者.BackgroundImageLayout")));
			this.button或者.Dock = ((System.Windows.Forms.DockStyle)(resources.GetObject("button或者.Dock")));
			this.button或者.FlatStyle = ((System.Windows.Forms.FlatStyle)(resources.GetObject("button或者.FlatStyle")));
			this.button或者.Font = ((System.Drawing.Font)(resources.GetObject("button或者.Font")));
			this.button或者.ForeColor = System.Drawing.Color.Green;
			this.button或者.ImageAlign = ((System.Drawing.ContentAlignment)(resources.GetObject("button或者.ImageAlign")));
			this.button或者.ImageIndex = ((int)(resources.GetObject("button或者.ImageIndex")));
			this.button或者.ImageKey = resources.GetString("button或者.ImageKey");
			this.button或者.ImeMode = ((System.Windows.Forms.ImeMode)(resources.GetObject("button或者.ImeMode")));
			this.button或者.Location = ((System.Drawing.Point)(resources.GetObject("button或者.Location")));
			this.button或者.Name = "button或者";
			this.button或者.RightToLeft = ((System.Windows.Forms.RightToLeft)(resources.GetObject("button或者.RightToLeft")));
			this.button或者.Size = ((System.Drawing.Size)(resources.GetObject("button或者.Size")));
			this.button或者.TabIndex = ((int)(resources.GetObject("button或者.TabIndex")));
			this.button或者.Text = resources.GetString("button或者.Text");
			this.button或者.TextAlign = ((System.Drawing.ContentAlignment)(resources.GetObject("button或者.TextAlign")));
			this.button或者.TextImageRelation = ((System.Windows.Forms.TextImageRelation)(resources.GetObject("button或者.TextImageRelation")));
			this.button或者.UseVisualStyleBackColor = false;
			this.button或者.Click += new System.EventHandler(this.OperButtonClick);
			// 
			// button并且
			// 
			this.button并且.AccessibleDescription = null;
			this.button并且.AccessibleName = null;
			this.button并且.Anchor = ((System.Windows.Forms.AnchorStyles)(resources.GetObject("button并且.Anchor")));
			this.button并且.AutoSize = ((bool)(resources.GetObject("button并且.AutoSize")));
			this.button并且.AutoSizeMode = ((System.Windows.Forms.AutoSizeMode)(resources.GetObject("button并且.AutoSizeMode")));
			this.button并且.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(255)))), ((int)(((byte)(128)))));
			this.button并且.BackgroundImage = null;
			this.button并且.BackgroundImageLayout = ((System.Windows.Forms.ImageLayout)(resources.GetObject("button并且.BackgroundImageLayout")));
			this.button并且.Dock = ((System.Windows.Forms.DockStyle)(resources.GetObject("button并且.Dock")));
			this.button并且.FlatStyle = ((System.Windows.Forms.FlatStyle)(resources.GetObject("button并且.FlatStyle")));
			this.button并且.Font = ((System.Drawing.Font)(resources.GetObject("button并且.Font")));
			this.button并且.ForeColor = System.Drawing.Color.Green;
			this.button并且.ImageAlign = ((System.Drawing.ContentAlignment)(resources.GetObject("button并且.ImageAlign")));
			this.button并且.ImageIndex = ((int)(resources.GetObject("button并且.ImageIndex")));
			this.button并且.ImageKey = resources.GetString("button并且.ImageKey");
			this.button并且.ImeMode = ((System.Windows.Forms.ImeMode)(resources.GetObject("button并且.ImeMode")));
			this.button并且.Location = ((System.Drawing.Point)(resources.GetObject("button并且.Location")));
			this.button并且.Name = "button并且";
			this.button并且.RightToLeft = ((System.Windows.Forms.RightToLeft)(resources.GetObject("button并且.RightToLeft")));
			this.button并且.Size = ((System.Drawing.Size)(resources.GetObject("button并且.Size")));
			this.button并且.TabIndex = ((int)(resources.GetObject("button并且.TabIndex")));
			this.button并且.Text = resources.GetString("button并且.Text");
			this.button并且.TextAlign = ((System.Drawing.ContentAlignment)(resources.GetObject("button并且.TextAlign")));
			this.button并且.TextImageRelation = ((System.Windows.Forms.TextImageRelation)(resources.GetObject("button并且.TextImageRelation")));
			this.button并且.UseVisualStyleBackColor = false;
			this.button并且.Click += new System.EventHandler(this.OperButtonClick);
			// 
			// button余
			// 
			this.button余.AccessibleDescription = null;
			this.button余.AccessibleName = null;
			this.button余.Anchor = ((System.Windows.Forms.AnchorStyles)(resources.GetObject("button余.Anchor")));
			this.button余.AutoSize = ((bool)(resources.GetObject("button余.AutoSize")));
			this.button余.AutoSizeMode = ((System.Windows.Forms.AutoSizeMode)(resources.GetObject("button余.AutoSizeMode")));
			this.button余.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(128)))));
			this.button余.BackgroundImage = null;
			this.button余.BackgroundImageLayout = ((System.Windows.Forms.ImageLayout)(resources.GetObject("button余.BackgroundImageLayout")));
			this.button余.Dock = ((System.Windows.Forms.DockStyle)(resources.GetObject("button余.Dock")));
			this.button余.FlatStyle = ((System.Windows.Forms.FlatStyle)(resources.GetObject("button余.FlatStyle")));
			this.button余.Font = ((System.Drawing.Font)(resources.GetObject("button余.Font")));
			this.button余.ForeColor = System.Drawing.Color.Maroon;
			this.button余.ImageAlign = ((System.Drawing.ContentAlignment)(resources.GetObject("button余.ImageAlign")));
			this.button余.ImageIndex = ((int)(resources.GetObject("button余.ImageIndex")));
			this.button余.ImageKey = resources.GetString("button余.ImageKey");
			this.button余.ImeMode = ((System.Windows.Forms.ImeMode)(resources.GetObject("button余.ImeMode")));
			this.button余.Location = ((System.Drawing.Point)(resources.GetObject("button余.Location")));
			this.button余.Name = "button余";
			this.button余.RightToLeft = ((System.Windows.Forms.RightToLeft)(resources.GetObject("button余.RightToLeft")));
			this.button余.Size = ((System.Drawing.Size)(resources.GetObject("button余.Size")));
			this.button余.TabIndex = ((int)(resources.GetObject("button余.TabIndex")));
			this.button余.Text = resources.GetString("button余.Text");
			this.button余.TextAlign = ((System.Drawing.ContentAlignment)(resources.GetObject("button余.TextAlign")));
			this.button余.TextImageRelation = ((System.Windows.Forms.TextImageRelation)(resources.GetObject("button余.TextImageRelation")));
			this.button余.UseVisualStyleBackColor = false;
			this.button余.Click += new System.EventHandler(this.OperButtonClick);
			// 
			// button除
			// 
			this.button除.AccessibleDescription = null;
			this.button除.AccessibleName = null;
			this.button除.Anchor = ((System.Windows.Forms.AnchorStyles)(resources.GetObject("button除.Anchor")));
			this.button除.AutoSize = ((bool)(resources.GetObject("button除.AutoSize")));
			this.button除.AutoSizeMode = ((System.Windows.Forms.AutoSizeMode)(resources.GetObject("button除.AutoSizeMode")));
			this.button除.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(128)))));
			this.button除.BackgroundImage = null;
			this.button除.BackgroundImageLayout = ((System.Windows.Forms.ImageLayout)(resources.GetObject("button除.BackgroundImageLayout")));
			this.button除.Dock = ((System.Windows.Forms.DockStyle)(resources.GetObject("button除.Dock")));
			this.button除.FlatStyle = ((System.Windows.Forms.FlatStyle)(resources.GetObject("button除.FlatStyle")));
			this.button除.Font = ((System.Drawing.Font)(resources.GetObject("button除.Font")));
			this.button除.ForeColor = System.Drawing.Color.Maroon;
			this.button除.ImageAlign = ((System.Drawing.ContentAlignment)(resources.GetObject("button除.ImageAlign")));
			this.button除.ImageIndex = ((int)(resources.GetObject("button除.ImageIndex")));
			this.button除.ImageKey = resources.GetString("button除.ImageKey");
			this.button除.ImeMode = ((System.Windows.Forms.ImeMode)(resources.GetObject("button除.ImeMode")));
			this.button除.Location = ((System.Drawing.Point)(resources.GetObject("button除.Location")));
			this.button除.Name = "button除";
			this.button除.RightToLeft = ((System.Windows.Forms.RightToLeft)(resources.GetObject("button除.RightToLeft")));
			this.button除.Size = ((System.Drawing.Size)(resources.GetObject("button除.Size")));
			this.button除.TabIndex = ((int)(resources.GetObject("button除.TabIndex")));
			this.button除.Text = resources.GetString("button除.Text");
			this.button除.TextAlign = ((System.Drawing.ContentAlignment)(resources.GetObject("button除.TextAlign")));
			this.button除.TextImageRelation = ((System.Windows.Forms.TextImageRelation)(resources.GetObject("button除.TextImageRelation")));
			this.button除.UseVisualStyleBackColor = false;
			this.button除.Click += new System.EventHandler(this.OperButtonClick);
			// 
			// button乘
			// 
			this.button乘.AccessibleDescription = null;
			this.button乘.AccessibleName = null;
			this.button乘.Anchor = ((System.Windows.Forms.AnchorStyles)(resources.GetObject("button乘.Anchor")));
			this.button乘.AutoSize = ((bool)(resources.GetObject("button乘.AutoSize")));
			this.button乘.AutoSizeMode = ((System.Windows.Forms.AutoSizeMode)(resources.GetObject("button乘.AutoSizeMode")));
			this.button乘.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(128)))));
			this.button乘.BackgroundImage = null;
			this.button乘.BackgroundImageLayout = ((System.Windows.Forms.ImageLayout)(resources.GetObject("button乘.BackgroundImageLayout")));
			this.button乘.Dock = ((System.Windows.Forms.DockStyle)(resources.GetObject("button乘.Dock")));
			this.button乘.FlatStyle = ((System.Windows.Forms.FlatStyle)(resources.GetObject("button乘.FlatStyle")));
			this.button乘.Font = ((System.Drawing.Font)(resources.GetObject("button乘.Font")));
			this.button乘.ForeColor = System.Drawing.Color.Maroon;
			this.button乘.ImageAlign = ((System.Drawing.ContentAlignment)(resources.GetObject("button乘.ImageAlign")));
			this.button乘.ImageIndex = ((int)(resources.GetObject("button乘.ImageIndex")));
			this.button乘.ImageKey = resources.GetString("button乘.ImageKey");
			this.button乘.ImeMode = ((System.Windows.Forms.ImeMode)(resources.GetObject("button乘.ImeMode")));
			this.button乘.Location = ((System.Drawing.Point)(resources.GetObject("button乘.Location")));
			this.button乘.Name = "button乘";
			this.button乘.RightToLeft = ((System.Windows.Forms.RightToLeft)(resources.GetObject("button乘.RightToLeft")));
			this.button乘.Size = ((System.Drawing.Size)(resources.GetObject("button乘.Size")));
			this.button乘.TabIndex = ((int)(resources.GetObject("button乘.TabIndex")));
			this.button乘.Text = resources.GetString("button乘.Text");
			this.button乘.TextAlign = ((System.Drawing.ContentAlignment)(resources.GetObject("button乘.TextAlign")));
			this.button乘.TextImageRelation = ((System.Windows.Forms.TextImageRelation)(resources.GetObject("button乘.TextImageRelation")));
			this.button乘.UseVisualStyleBackColor = false;
			this.button乘.Click += new System.EventHandler(this.OperButtonClick);
			// 
			// button减
			// 
			this.button减.AccessibleDescription = null;
			this.button减.AccessibleName = null;
			this.button减.Anchor = ((System.Windows.Forms.AnchorStyles)(resources.GetObject("button减.Anchor")));
			this.button减.AutoSize = ((bool)(resources.GetObject("button减.AutoSize")));
			this.button减.AutoSizeMode = ((System.Windows.Forms.AutoSizeMode)(resources.GetObject("button减.AutoSizeMode")));
			this.button减.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(128)))));
			this.button减.BackgroundImage = null;
			this.button减.BackgroundImageLayout = ((System.Windows.Forms.ImageLayout)(resources.GetObject("button减.BackgroundImageLayout")));
			this.button减.Dock = ((System.Windows.Forms.DockStyle)(resources.GetObject("button减.Dock")));
			this.button减.FlatStyle = ((System.Windows.Forms.FlatStyle)(resources.GetObject("button减.FlatStyle")));
			this.button减.Font = ((System.Drawing.Font)(resources.GetObject("button减.Font")));
			this.button减.ForeColor = System.Drawing.Color.Maroon;
			this.button减.ImageAlign = ((System.Drawing.ContentAlignment)(resources.GetObject("button减.ImageAlign")));
			this.button减.ImageIndex = ((int)(resources.GetObject("button减.ImageIndex")));
			this.button减.ImageKey = resources.GetString("button减.ImageKey");
			this.button减.ImeMode = ((System.Windows.Forms.ImeMode)(resources.GetObject("button减.ImeMode")));
			this.button减.Location = ((System.Drawing.Point)(resources.GetObject("button减.Location")));
			this.button减.Name = "button减";
			this.button减.RightToLeft = ((System.Windows.Forms.RightToLeft)(resources.GetObject("button减.RightToLeft")));
			this.button减.Size = ((System.Drawing.Size)(resources.GetObject("button减.Size")));
			this.button减.TabIndex = ((int)(resources.GetObject("button减.TabIndex")));
			this.button减.Text = resources.GetString("button减.Text");
			this.button减.TextAlign = ((System.Drawing.ContentAlignment)(resources.GetObject("button减.TextAlign")));
			this.button减.TextImageRelation = ((System.Windows.Forms.TextImageRelation)(resources.GetObject("button减.TextImageRelation")));
			this.button减.UseVisualStyleBackColor = false;
			this.button减.Click += new System.EventHandler(this.OperButtonClick);
			// 
			// button加
			// 
			this.button加.AccessibleDescription = null;
			this.button加.AccessibleName = null;
			this.button加.Anchor = ((System.Windows.Forms.AnchorStyles)(resources.GetObject("button加.Anchor")));
			this.button加.AutoSize = ((bool)(resources.GetObject("button加.AutoSize")));
			this.button加.AutoSizeMode = ((System.Windows.Forms.AutoSizeMode)(resources.GetObject("button加.AutoSizeMode")));
			this.button加.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(128)))));
			this.button加.BackgroundImage = null;
			this.button加.BackgroundImageLayout = ((System.Windows.Forms.ImageLayout)(resources.GetObject("button加.BackgroundImageLayout")));
			this.button加.Dock = ((System.Windows.Forms.DockStyle)(resources.GetObject("button加.Dock")));
			this.button加.FlatStyle = ((System.Windows.Forms.FlatStyle)(resources.GetObject("button加.FlatStyle")));
			this.button加.Font = ((System.Drawing.Font)(resources.GetObject("button加.Font")));
			this.button加.ForeColor = System.Drawing.Color.Maroon;
			this.button加.ImageAlign = ((System.Drawing.ContentAlignment)(resources.GetObject("button加.ImageAlign")));
			this.button加.ImageIndex = ((int)(resources.GetObject("button加.ImageIndex")));
			this.button加.ImageKey = resources.GetString("button加.ImageKey");
			this.button加.ImeMode = ((System.Windows.Forms.ImeMode)(resources.GetObject("button加.ImeMode")));
			this.button加.Location = ((System.Drawing.Point)(resources.GetObject("button加.Location")));
			this.button加.Name = "button加";
			this.button加.RightToLeft = ((System.Windows.Forms.RightToLeft)(resources.GetObject("button加.RightToLeft")));
			this.button加.Size = ((System.Drawing.Size)(resources.GetObject("button加.Size")));
			this.button加.TabIndex = ((int)(resources.GetObject("button加.TabIndex")));
			this.button加.Text = resources.GetString("button加.Text");
			this.button加.TextAlign = ((System.Drawing.ContentAlignment)(resources.GetObject("button加.TextAlign")));
			this.button加.TextImageRelation = ((System.Windows.Forms.TextImageRelation)(resources.GetObject("button加.TextImageRelation")));
			this.button加.UseVisualStyleBackColor = false;
			this.button加.Click += new System.EventHandler(this.OperButtonClick);
			// 
			// button不成立
			// 
			this.button不成立.AccessibleDescription = null;
			this.button不成立.AccessibleName = null;
			this.button不成立.Anchor = ((System.Windows.Forms.AnchorStyles)(resources.GetObject("button不成立.Anchor")));
			this.button不成立.AutoSize = ((bool)(resources.GetObject("button不成立.AutoSize")));
			this.button不成立.AutoSizeMode = ((System.Windows.Forms.AutoSizeMode)(resources.GetObject("button不成立.AutoSizeMode")));
			this.button不成立.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(255)))), ((int)(((byte)(128)))));
			this.button不成立.BackgroundImage = null;
			this.button不成立.BackgroundImageLayout = ((System.Windows.Forms.ImageLayout)(resources.GetObject("button不成立.BackgroundImageLayout")));
			this.button不成立.Dock = ((System.Windows.Forms.DockStyle)(resources.GetObject("button不成立.Dock")));
			this.button不成立.FlatStyle = ((System.Windows.Forms.FlatStyle)(resources.GetObject("button不成立.FlatStyle")));
			this.button不成立.Font = ((System.Drawing.Font)(resources.GetObject("button不成立.Font")));
			this.button不成立.ForeColor = System.Drawing.Color.Green;
			this.button不成立.ImageAlign = ((System.Drawing.ContentAlignment)(resources.GetObject("button不成立.ImageAlign")));
			this.button不成立.ImageIndex = ((int)(resources.GetObject("button不成立.ImageIndex")));
			this.button不成立.ImageKey = resources.GetString("button不成立.ImageKey");
			this.button不成立.ImeMode = ((System.Windows.Forms.ImeMode)(resources.GetObject("button不成立.ImeMode")));
			this.button不成立.Location = ((System.Drawing.Point)(resources.GetObject("button不成立.Location")));
			this.button不成立.Name = "button不成立";
			this.button不成立.RightToLeft = ((System.Windows.Forms.RightToLeft)(resources.GetObject("button不成立.RightToLeft")));
			this.button不成立.Size = ((System.Drawing.Size)(resources.GetObject("button不成立.Size")));
			this.button不成立.TabIndex = ((int)(resources.GetObject("button不成立.TabIndex")));
			this.button不成立.Text = resources.GetString("button不成立.Text");
			this.button不成立.TextAlign = ((System.Drawing.ContentAlignment)(resources.GetObject("button不成立.TextAlign")));
			this.button不成立.TextImageRelation = ((System.Windows.Forms.TextImageRelation)(resources.GetObject("button不成立.TextImageRelation")));
			this.button不成立.UseVisualStyleBackColor = false;
			this.button不成立.Click += new System.EventHandler(this.OperButtonClick);
			// 
			// button绝对值
			// 
			this.button绝对值.AccessibleDescription = null;
			this.button绝对值.AccessibleName = null;
			this.button绝对值.Anchor = ((System.Windows.Forms.AnchorStyles)(resources.GetObject("button绝对值.Anchor")));
			this.button绝对值.AutoSize = ((bool)(resources.GetObject("button绝对值.AutoSize")));
			this.button绝对值.AutoSizeMode = ((System.Windows.Forms.AutoSizeMode)(resources.GetObject("button绝对值.AutoSizeMode")));
			this.button绝对值.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(0)))));
			this.button绝对值.BackgroundImage = null;
			this.button绝对值.BackgroundImageLayout = ((System.Windows.Forms.ImageLayout)(resources.GetObject("button绝对值.BackgroundImageLayout")));
			this.button绝对值.Dock = ((System.Windows.Forms.DockStyle)(resources.GetObject("button绝对值.Dock")));
			this.button绝对值.FlatStyle = ((System.Windows.Forms.FlatStyle)(resources.GetObject("button绝对值.FlatStyle")));
			this.button绝对值.Font = ((System.Drawing.Font)(resources.GetObject("button绝对值.Font")));
			this.button绝对值.ForeColor = System.Drawing.Color.Maroon;
			this.button绝对值.ImageAlign = ((System.Drawing.ContentAlignment)(resources.GetObject("button绝对值.ImageAlign")));
			this.button绝对值.ImageIndex = ((int)(resources.GetObject("button绝对值.ImageIndex")));
			this.button绝对值.ImageKey = resources.GetString("button绝对值.ImageKey");
			this.button绝对值.ImeMode = ((System.Windows.Forms.ImeMode)(resources.GetObject("button绝对值.ImeMode")));
			this.button绝对值.Location = ((System.Drawing.Point)(resources.GetObject("button绝对值.Location")));
			this.button绝对值.Name = "button绝对值";
			this.button绝对值.RightToLeft = ((System.Windows.Forms.RightToLeft)(resources.GetObject("button绝对值.RightToLeft")));
			this.button绝对值.Size = ((System.Drawing.Size)(resources.GetObject("button绝对值.Size")));
			this.button绝对值.TabIndex = ((int)(resources.GetObject("button绝对值.TabIndex")));
			this.button绝对值.Text = resources.GetString("button绝对值.Text");
			this.button绝对值.TextAlign = ((System.Drawing.ContentAlignment)(resources.GetObject("button绝对值.TextAlign")));
			this.button绝对值.TextImageRelation = ((System.Windows.Forms.TextImageRelation)(resources.GetObject("button绝对值.TextImageRelation")));
			this.button绝对值.UseVisualStyleBackColor = false;
			this.button绝对值.Click += new System.EventHandler(this.OperButtonClick);
			// 
			// button取负
			// 
			this.button取负.AccessibleDescription = null;
			this.button取负.AccessibleName = null;
			this.button取负.Anchor = ((System.Windows.Forms.AnchorStyles)(resources.GetObject("button取负.Anchor")));
			this.button取负.AutoSize = ((bool)(resources.GetObject("button取负.AutoSize")));
			this.button取负.AutoSizeMode = ((System.Windows.Forms.AutoSizeMode)(resources.GetObject("button取负.AutoSizeMode")));
			this.button取负.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(0)))));
			this.button取负.BackgroundImage = null;
			this.button取负.BackgroundImageLayout = ((System.Windows.Forms.ImageLayout)(resources.GetObject("button取负.BackgroundImageLayout")));
			this.button取负.Dock = ((System.Windows.Forms.DockStyle)(resources.GetObject("button取负.Dock")));
			this.button取负.FlatStyle = ((System.Windows.Forms.FlatStyle)(resources.GetObject("button取负.FlatStyle")));
			this.button取负.Font = ((System.Drawing.Font)(resources.GetObject("button取负.Font")));
			this.button取负.ForeColor = System.Drawing.Color.Maroon;
			this.button取负.ImageAlign = ((System.Drawing.ContentAlignment)(resources.GetObject("button取负.ImageAlign")));
			this.button取负.ImageIndex = ((int)(resources.GetObject("button取负.ImageIndex")));
			this.button取负.ImageKey = resources.GetString("button取负.ImageKey");
			this.button取负.ImeMode = ((System.Windows.Forms.ImeMode)(resources.GetObject("button取负.ImeMode")));
			this.button取负.Location = ((System.Drawing.Point)(resources.GetObject("button取负.Location")));
			this.button取负.Name = "button取负";
			this.button取负.RightToLeft = ((System.Windows.Forms.RightToLeft)(resources.GetObject("button取负.RightToLeft")));
			this.button取负.Size = ((System.Drawing.Size)(resources.GetObject("button取负.Size")));
			this.button取负.TabIndex = ((int)(resources.GetObject("button取负.TabIndex")));
			this.button取负.Text = resources.GetString("button取负.Text");
			this.button取负.TextAlign = ((System.Drawing.ContentAlignment)(resources.GetObject("button取负.TextAlign")));
			this.button取负.TextImageRelation = ((System.Windows.Forms.TextImageRelation)(resources.GetObject("button取负.TextImageRelation")));
			this.button取负.UseVisualStyleBackColor = false;
			this.button取负.Click += new System.EventHandler(this.OperButtonClick);
			// 
			// labelExpType
			// 
			this.labelExpType.AccessibleDescription = null;
			this.labelExpType.AccessibleName = null;
			this.labelExpType.Anchor = ((System.Windows.Forms.AnchorStyles)(resources.GetObject("labelExpType.Anchor")));
			this.labelExpType.AutoSize = ((bool)(resources.GetObject("labelExpType.AutoSize")));
			this.labelExpType.BackgroundImageLayout = ((System.Windows.Forms.ImageLayout)(resources.GetObject("labelExpType.BackgroundImageLayout")));
			this.labelExpType.Dock = ((System.Windows.Forms.DockStyle)(resources.GetObject("labelExpType.Dock")));
			this.labelExpType.Font = ((System.Drawing.Font)(resources.GetObject("labelExpType.Font")));
			this.labelExpType.ForeColor = System.Drawing.Color.White;
			this.labelExpType.ImageAlign = ((System.Drawing.ContentAlignment)(resources.GetObject("labelExpType.ImageAlign")));
			this.labelExpType.ImageIndex = ((int)(resources.GetObject("labelExpType.ImageIndex")));
			this.labelExpType.ImageKey = resources.GetString("labelExpType.ImageKey");
			this.labelExpType.ImeMode = ((System.Windows.Forms.ImeMode)(resources.GetObject("labelExpType.ImeMode")));
			this.labelExpType.Location = ((System.Drawing.Point)(resources.GetObject("labelExpType.Location")));
			this.labelExpType.Name = "labelExpType";
			this.labelExpType.RightToLeft = ((System.Windows.Forms.RightToLeft)(resources.GetObject("labelExpType.RightToLeft")));
			this.labelExpType.Size = ((System.Drawing.Size)(resources.GetObject("labelExpType.Size")));
			this.labelExpType.TabIndex = ((int)(resources.GetObject("labelExpType.TabIndex")));
			this.labelExpType.TextAlign = ((System.Drawing.ContentAlignment)(resources.GetObject("labelExpType.TextAlign")));
			// 
			// EXPForm
			// 
			this.AccessibleDescription = null;
			this.AccessibleName = null;
			this.AutoScaleDimensions = ((System.Drawing.SizeF)(resources.GetObject("$this.AutoScaleDimensions")));
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.AutoScroll = ((bool)(resources.GetObject("$this.AutoScroll")));
			this.AutoSize = ((bool)(resources.GetObject("$this.AutoSize")));
			this.AutoSizeMode = ((System.Windows.Forms.AutoSizeMode)(resources.GetObject("$this.AutoSizeMode")));
			this.BackColor = System.Drawing.Color.Gray;
			this.BackgroundImage = null;
			this.BackgroundImageLayout = ((System.Windows.Forms.ImageLayout)(resources.GetObject("$this.BackgroundImageLayout")));
			this.ClientSize = ((System.Drawing.Size)(resources.GetObject("$this.ClientSize")));
			this.Controls.Add(this.labelExpType);
			this.Controls.Add(this.listBox成员列表);
			this.Controls.Add(this.panelOper);
			this.Controls.Add(this.panelNumber);
			this.Controls.Add(this.listBox组件列表);
			this.Controls.Add(this.textBox1);
			this.Controls.Add(this.button取消);
			this.Controls.Add(this.button确定);
			this.Font = null;
			this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
			this.Icon = null;
			this.ImeMode = ((System.Windows.Forms.ImeMode)(resources.GetObject("$this.ImeMode")));
			this.Location = ((System.Drawing.Point)(resources.GetObject("$this.Location")));
			this.MaximumSize = ((System.Drawing.Size)(resources.GetObject("$this.MaximumSize")));
			this.Name = "EXPForm";
			this.Opacity = 0.85;
			this.RightToLeft = ((System.Windows.Forms.RightToLeft)(resources.GetObject("$this.RightToLeft")));
			this.RightToLeftLayout = ((bool)(resources.GetObject("$this.RightToLeftLayout")));
			this.ShowInTaskbar = false;
			this.StartPosition = ((System.Windows.Forms.FormStartPosition)(resources.GetObject("$this.StartPosition")));
			this.Text = resources.GetString("$this.Text");
			this.TopMost = true;
			this.panelNumber.ResumeLayout(false);
			this.panelOper.ResumeLayout(false);
			this.ResumeLayout(false);
			this.PerformLayout();
		}
		private System.Windows.Forms.Label labelExpType;
		private System.Windows.Forms.Button button赋值;
		private System.Windows.Forms.Button button加;
		private System.Windows.Forms.Button button并且;
		private System.Windows.Forms.Button button不成立;
		private System.Windows.Forms.Button button绝对值;
		private System.Windows.Forms.Button button取负;
		private System.Windows.Forms.Button button余;
		private System.Windows.Forms.Button button除;
		private System.Windows.Forms.Button button乘;
		private System.Windows.Forms.Button button减;
		private System.Windows.Forms.Button button或者;
		private System.Windows.Forms.Button button等于;
		private System.Windows.Forms.Button button小于等于;
		private System.Windows.Forms.Button button大于等于;
		private System.Windows.Forms.Button button小于;
		private System.Windows.Forms.Button button大于;
		private System.Windows.Forms.Button button不等于;
		private System.Windows.Forms.Panel panelNumber;
		private System.Windows.Forms.Panel panelOper;
		private System.Windows.Forms.Button buttonNO;
		private System.Windows.Forms.Button buttonYES;
		private System.Windows.Forms.Button buttonCE;
		private System.Windows.Forms.Button button1;
		private System.Windows.Forms.Button button2;
		private System.Windows.Forms.Button button3;
		private System.Windows.Forms.Button button4;
		private System.Windows.Forms.Button button5;
		private System.Windows.Forms.Button button6;
		private System.Windows.Forms.Button button7;
		private System.Windows.Forms.Button button8;
		private System.Windows.Forms.Button button9;
		private System.Windows.Forms.Button button10;
		private System.Windows.Forms.Button button11;
		private System.Windows.Forms.ListBox listBox成员列表;
		private System.Windows.Forms.ListBox listBox组件列表;
		private System.Windows.Forms.TextBox textBox1;
		private System.Windows.Forms.Button button取消;
		private System.Windows.Forms.Button button确定;
	}
}
