﻿
namespace n_protocolEXPForm
{
using System;
using System.Windows.Forms;
using System.IO;
using System.Drawing;
using System.Drawing.Drawing2D;

using n_VarType;
using n_UnitList;
using n_FunctionList;
using n_VarList;
using n_StructList;
using n_ImagePanel;
using n_MyObjectList;
using n_MyObject;
using n_HardModule;
using n_GVar;
using n_GUIcoder;
using n_MyFileObject;
using c_FormMover;
using n_EXP;
using n_EXPcommon;

/// <summary>
/// Description of WorkForm.
/// </summary>
public partial class protocolEXPForm : Form
{
	bool isOK;
	
	string vvPreType;	
	public string PreType {
		get { return vvPreType; }
		set {
			vvPreType = value;
		}
	}
	
	FormMover fm;
	
	string Result;
	
	Label[] IdxList;
	ComboBox[] ComboBoxList;
	TextBox[][] List;
	Label[] MesList;
	const int MaxLength = 50;
	
	const string DefaultValue = "[3,0x0000FFAA,0x00000000,0x00000000]";
	
	//主窗口
	public protocolEXPForm()
	{
		InitializeComponent();
		isOK = false;
		
		PreType = "#WORD";
		
		IdxList = new Label[ MaxLength ];
		ComboBoxList = new ComboBox[ MaxLength ];
		MesList = new Label[ MaxLength ];
		List = new TextBox[ MaxLength ][];
		
		int Start = 10;
		
		for( int i = 0; i < MaxLength; ++i ) {
			
			IdxList[i] = new Label();
			IdxList[i].Name = i.ToString();
			IdxList[i].TextAlign = ContentAlignment.MiddleLeft;
			IdxList[i].Location = new Point( 0, Start + i * 50 + 5 );
			IdxList[i].Width = 35;
			IdxList[i].Font = this.button确定.Font;
			IdxList[i].ForeColor = Color.Gray;
			IdxList[i].BackColor = Color.Transparent;
			IdxList[i].Visible = true;
			IdxList[i].TextAlign = ContentAlignment.MiddleLeft;
			IdxList[i].Text = (i + 1).ToString();
			
			this.panel1.Controls.Add( IdxList[i] );
			
			ComboBoxList[i] = new ComboBox();
			ComboBoxList[i].Name = i.ToString();
			ComboBoxList[i].Location = new Point( 35, Start + i * 50 );
			ComboBoxList[i].Width = 100;
			ComboBoxList[i].Font = this.button确定.Font;
			ComboBoxList[i].ForeColor = Color.Black;
			ComboBoxList[i].DropDownStyle = ComboBoxStyle.DropDownList;
			ComboBoxList[i].Items.Add( "未用" );
			ComboBoxList[i].Items.Add( "固定值" );
			ComboBoxList[i].Items.Add( "任意值" );
			ComboBoxList[i].Items.Add( "排除值" );
			ComboBoxList[i].SelectedIndex = 0;
			ComboBoxList[i].Visible = true;
			ComboBoxList[i].SelectedIndexChanged += new EventHandler( ComboBoxSelectedIndexChanged );
			this.panel1.Controls.Add( ComboBoxList[i] );
			
			List[i] = new TextBox[3];
			List[i][0] = new TextBox();
			List[i][0].Name = i.ToString();
			List[i][0].Location = new Point( 155, Start + i * 50 );
			List[i][0].Width = 50;
			List[i][0].BorderStyle = BorderStyle.FixedSingle;
			List[i][0].Font = this.button确定.Font;
			List[i][0].ForeColor = Color.Black;
			List[i][0].MouseClick += ListMouseClick;
			List[i][0].Visible = true;
			List[i][0].TextChanged += ListDecTextChanged;
			List[i][1] = new TextBox();
			List[i][1].Name = i.ToString();
			List[i][1].Location = new Point( 210, Start + i * 50 );
			List[i][1].Width = 50;
			List[i][1].BorderStyle = BorderStyle.FixedSingle;
			List[i][1].Font = this.button确定.Font;
			List[i][1].ForeColor = Color.Black;
			List[i][1].MouseClick += ListMouseClick;
			List[i][1].Visible = true;
			List[i][1].TextChanged += ListHexTextChanged;
			List[i][2] = new TextBox();
			List[i][2].Name = i.ToString();
			List[i][2].Location = new Point( 265, Start + i * 50 );
			List[i][2].Width = 50;
			List[i][2].BorderStyle = BorderStyle.FixedSingle;
			List[i][2].Font = this.button确定.Font;
			List[i][2].ForeColor = Color.Black;
			List[i][2].MouseClick += ListMouseClick;
			List[i][2].Visible = true;
			List[i][2].TextChanged += ListCharTextChanged;
			
			this.panel1.Controls.Add( List[i][0] );
			this.panel1.Controls.Add( List[i][1] );
			this.panel1.Controls.Add( List[i][2] );
			
			Font f = new Font( "微软雅黑", 11 );
			MesList[i] = new Label();
			MesList[i].Name = i.ToString();
			MesList[i].TextAlign = ContentAlignment.MiddleLeft;
			MesList[i].Location = new Point( 320, Start + i * 50 + 5 );
			MesList[i].Width = 400;
			MesList[i].Font = f;
			MesList[i].ForeColor = Color.SlateGray;
			MesList[i].BackColor = Color.Transparent;
			MesList[i].Visible = true;
			MesList[i].TextAlign = ContentAlignment.MiddleLeft;
			
			this.panel1.Controls.Add( MesList[i] );
		}
		//RefreshList( DefaultValue );
		
		fm = new FormMover( this );
		
		ignore = false;
	}
	
	//运行
	public string Run( string Value )
	{
		PreType = "@WORD";
		
		if( this.Visible ) {
			return null;
		}
		isOK = false;
		
		if( Value == null || Value == "" ) {
			Value = DefaultValue;
		}
		RefreshList( Value );
		
		//注意,直接设置Visible不能使窗体获得焦点
		//this.Visible = true;
		this.ShowDialog();
		
		while( this.Visible ) {
			System.Windows.Forms.Application.DoEvents();
		}
		if( isOK ) {
			return Result;
		}
		return null;
	}
	
	void Button确定Click(object sender, EventArgs e)
	{
		int Length = MaxLength;
		string s = "";
		try {
		for( int i = 0; i < MaxLength; ++i ) {
			if( ComboBoxList[i].Text == "未用" ) {
				Length = i;
				break;
			}
			if( ComboBoxList[i].Text == "固定值" ) {
				s += "0xFF" + (int.Parse( List[i][0].Text )).ToString( "X" ).PadLeft( 2, '0' ) + ",";
			}
			else if( ComboBoxList[i].Text == "任意值" ) {
				s += "0x00,";
			}
			else if( ComboBoxList[i].Text == "排除值" ) {
				s += "0x010000" + (int.Parse( List[i][0].Text )).ToString( "X" ).PadLeft( 2, '0' ) + ",";
			}
			else {
				
			}
		}
		}
		catch {
			MessageBox.Show( "只有<未用>类型的字段才能为空, 其他类型均需要填写数值" );
		}
		Result = "@WORD " + "[" + Length + "," + s + "]";
		
		isOK = true;
		this.Visible = false;
	}
	
	//===========================================================
	bool ignore;
	
	void Error( int i )
	{
		MesList[i].Text = "当前元素无法编辑";
	}
	
	void ListDecTextChanged( object sender, EventArgs e )
	{
		TextBox t = (TextBox)sender;
		
		int i = int.Parse( t.Name );
		if( ComboBoxList[i].SelectedIndex == 0 || ComboBoxList[i].SelectedIndex == 2 ) {
			t.Text = "";
			Error( i );
			return;
		}
		if( ignore ) {
			return;
		}
		if( t.Text == "" ) {
			return;
		}
		try {
		int d = int.Parse( t.Text );
		
		ignore = true;
		List[i][1].Text = d.ToString( "X" ).PadLeft( 2, '0' );
		List[i][2].Text = ((char)d).ToString();
		ignore = false;
		}
		catch {
			MessageBox.Show( "数据输入格式错误：" + t.Text );
			return;
		}
		
		RefreshMes( i );
	}
	
	void ListHexTextChanged( object sender, EventArgs e )
	{
		TextBox t = (TextBox)sender;
		
		int i = int.Parse( t.Name );
		if( ComboBoxList[i].SelectedIndex == 0 || ComboBoxList[i].SelectedIndex == 2 ) {
			t.Text = "";
			Error( i );
			return;
		}
		if( ignore ) {
			return;
		}
		if( t.Text == "" ) {
			return;
		}
		try {
		int d = int.Parse( t.Text, System.Globalization.NumberStyles.HexNumber );
		
		ignore = true;
		List[i][0].Text = d.ToString();
		List[i][2].Text = ((char)d).ToString();
		ignore = false;
		}
		catch {
			MessageBox.Show( "数据输入格式错误：" + t.Text );
			return;
		}
		
		RefreshMes( i );
	}
	
	void ListCharTextChanged( object sender, EventArgs e )
	{
		TextBox t = (TextBox)sender;
		
		int i = int.Parse( t.Name );
		if( ComboBoxList[i].SelectedIndex == 0 || ComboBoxList[i].SelectedIndex == 2 ) {
			t.Text = "";
			Error( i );
			return;
		}
		if( ignore ) {
			return;
		}
		if( t.Text == "" ) {
			return;
		}
		int d = (int)t.Text[0];
		
		ignore = true;
		List[i][0].Text = d.ToString();
		List[i][1].Text = d.ToString( "X" ).PadLeft( 2, '0' );
		ignore = false;
		
		RefreshMes( i );
	}
	
	
	void ListMouseClick( object sender, EventArgs e )
	{
		TextBox t = (TextBox)sender;
		
		//t.SelectAll();
	}
	
	//刷新控件列表
	void RefreshList( string Value )
	{
		if( List == null ) {
			return;
		}
		
		Value = Value.Remove( 0, 1 );
		Value = Value.Remove( Value.Length - 1 );
		string[] Cut = Value.Split( ',' );
		
		for( int i = 0; i < List.Length; ++i ) {
			if( i+1 < Cut.Length && Cut[i+1] != "" ) {
				Cut[i+1] = "0x" + Cut[i+1].Remove( 0, 2 ).PadLeft( 8, '0' );
				
				string Type = Cut[i+1].Substring( 2, 2 );
				string Mask = Cut[i+1].Substring( 6, 2 );
				string Data = Cut[i+1].Substring( 8, 2 );
				
				if( Type == "00" ) {
					if( Mask == "FF" ) {
						ComboBoxList[i].SelectedIndex = 1;
						List[i][1].Text = Data;
					}
					if( Mask == "00" ) {
						ComboBoxList[i].SelectedIndex = 2;
						List[i][0].Text = "";
						List[i][1].Text = "";
						List[i][2].Text = "";
					}
				}
				if( Type == "01" ) {
					ComboBoxList[i].SelectedIndex = 3;
					List[i][1].Text = Data;
				}
				RefreshMes( i );
			}
			else {
				ComboBoxList[i].SelectedIndex = 0;
			}
		}
	}
	
	//刷新指定的信息
	void RefreshMes( int i )
	{
		if( ComboBoxList[i].SelectedIndex == 0 ) {
			MesList[i].Text = "";
		}
		if( ComboBoxList[i].SelectedIndex == 1 ) {
			MesList[i].Text = "接收到数据 " + List[i][0].Text + " 时匹配下一数据, 否则触发协议出错";
		}
		if( ComboBoxList[i].SelectedIndex == 2 ) {
			MesList[i].Text = "接收任意一个数据并匹配下一数据";
		}
		if( ComboBoxList[i].SelectedIndex == 3 ) {
			MesList[i].Text = "反复接收多个数据, 直到数据为 " + List[i][0].Text + " 时匹配下一数据";
		}
	}
	
	void ComboBoxSelectedIndexChanged(object sender, EventArgs e)
	{
		ComboBox c = (ComboBox)sender;
		int i = int.Parse( c.Name );
		
		RefreshMes( i );
		
		if( c.SelectedIndex == 0 ) {
			while( i < MaxLength ) {
				ComboBoxList[i].SelectedIndex = 0;
				List[i][0].Text = "";
				List[i][1].Text = "";
				List[i][2].Text = "";
				i++;
			}
			i = 0;
		}
		if( c.SelectedIndex == 2 ) {
			List[i][0].Text = "";
			List[i][1].Text = "";
			List[i][2].Text = "";
		}
		else {
			for( int n = 0; n <= i; n++ ) {
				if( ComboBoxList[n].SelectedIndex == 0 ) {
					ComboBoxList[n].SelectedIndex = 2;
					List[n][0].Text = "";
					List[n][1].Text = "";
					List[n][2].Text = "";
				}
			}
		}
	}
	
	void ProtocolEXPFormFormClosing(object sender, FormClosingEventArgs e)
	{
		e.Cancel = true;
		Visible = false;
	}
}
}

