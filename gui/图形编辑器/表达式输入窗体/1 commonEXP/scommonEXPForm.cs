﻿
namespace n_scommonEXPForm
{
using System;
using System.Windows.Forms;
using System.Drawing;
using System.IO;

using n_VarType;
using n_UnitList;
using n_FunctionList;
using n_VarList;
using n_StructList;
using n_ImagePanel;
using n_MyObjectList;
using n_MyObject;
using n_HardModule;
using n_GVar;
using n_GUIcoder;
using n_MyFileObject;
using c_FormMover;
using n_EXP;
using n_EXPcommon;
using System.Collections.Generic;
using n_EPanel;
using n_GUIset;

/// <summary>
/// Description of WorkForm.
/// </summary>
public partial class scommonEXPForm : Form
{
	public MyFileObject cM;
	
	public string SelectedMessage;
	
	public EPanel ePanel;
	
	public EXP cExp;
	public int cExpIndex;
	
	public List<string> InnerNameList;
	List<string> ExpTypeList;
	List<string> VarTypeList;
	string myPreType;
	
	//FormMover fm;
	
	string NeedType;
	
	string Result;
	
	MyObject owner;
	
	string ReturnType;
	
	int SelectedIndex;
	
	n_GroupList.Group gr;
	
	string InsGroupMes;
	
	ColorDialog CD;
	
	public string MesText;
	
	Image ImgNumber;
	Image ImgOper;
	Image ImgUser;
	Image ImgFrame;
	Image ImgCode;
	
	Image ImgCircuit;
	Image ImgVui;
	
	bool LocationInit;
	
	bool CharNormal;
	
	//主窗口
	public scommonEXPForm()
	{
		InitializeComponent();
		//toolTip1.OwnerDraw = true;
		//toolTip1.Draw += new DrawToolTipEventHandler(toolTip1_Draw);
		//toolTip1.Popup += new PopupEventHandler(toolTip1_Popup);
		
		EXPMember.Init();
		
		LocationInit = false;
		
		ePanel = new EPanel( this );
		
		ePanel.Width = Width;
		ePanel.Height = Height;
		
		ePanel.Controls.Add( panel_Astring );
		ePanel.Controls.Add( panel_Number );
		panel_Astring.Location = new Point( EPanel.ModWidth, EPanel.HeadHeight );
		panel_Number.Location = new Point( EPanel.ModWidth, EPanel.HeadHeight );
		
		
		ePanel.BringToFront();
		ePanel.ItemSelect = Deal;
		ePanel.MouseOnIndexChanged = MouseOnIndexChanged;
		
		ExpTypeList = new List<string>();
		InnerNameList = new List<string>();
		VarTypeList = new List<string>();
		//fm = new FormMover( this );
		
		//this.Height = 391;
		
		ImgNumber = Image.FromFile( n_OS.OS.SystemRoot + @"Resource\gui\InsPanel\Number.ico" );
		ImgOper = Image.FromFile( n_OS.OS.SystemRoot + @"Resource\gui\InsPanel\Oper.ico" );
		ImgUser = Image.FromFile( n_OS.OS.SystemRoot + @"Resource\gui\InsPanel\User.ico" );
		ImgFrame = Image.FromFile( n_OS.OS.SystemRoot + @"Resource\gui\InsPanel\Frame.ico" );
		ImgCode = Image.FromFile( n_OS.OS.SystemRoot + @"Resource\gui\InsPanel\Code.ico" );
		ImgCircuit = Image.FromFile( n_OS.OS.SystemRoot + @"Resource\gui\InsPanel\Circuit.ico" );
		ImgVui = Image.FromFile( n_OS.OS.SystemRoot + @"Resource\gui\InsPanel\vui.ico" );
	}
	
	//运行
	public string Run( string RType, string v_NeedType, string ExpType, MyObject o, string Value, string GroupMes, bool FloatButtonShow )
	{
		if( G.SimulateMode ) {
			n_Debug.Warning.WarningMessage = "仿真模式下无法编辑指令, 请先结束仿真再操作";
			return null;
		}
		
		cM = null;
		InsGroupMes = GroupMes;
		
		owner = o;
		
//		DefaultCut = null;
//		if( ExpType ==  EXP.ENote.t_OPER ) {
//			string[] c = Value.Split( ' ' );
//			if( c.Length == 2 ) {
//				DefaultCut = new string[1];
//				DefaultCut[0] = c[1];
//			}
//			if( c.Length == 3 ) {
//				DefaultCut = new string[2];
//				DefaultCut[0] = c[0];
//				DefaultCut[1] = c[2];
//			}
//		}
		
		ReturnType = RType;
		
		NeedType = v_NeedType;
		this.buttonFloat.Visible = FloatButtonShow;
		if( this.Visible ) {
			return null;
		}
		//刷新组件列表
		RefreshModuleList();
		if( NeedType == EXP.ENote.v_Astring || NeedType == EXP.ENote.v_Cstring ) {
			if( Value == null ) {
				Value = "";
			}
			if( Value.EndsWith( "unicode)" ) ) {
				Value = Value.Remove( Value.LastIndexOf( "(" ) );
				this.radioButtonUnicode.Checked = true;
			}
			else {
				this.radioButtonAscii.Checked = true;
			}
			if( Value.StartsWith( "@" ) ) {
				Value = Value.Remove( 0, 1 );
				radioButtonNormal.Checked = true;
			}
			else {
				radioButtonSecond.Checked = true;
			}
			//强制为普通模式
			CharNormal = false; //后续调用 RadioButtonNormalClick 会设置为 true
			radioButtonNormal.Checked = true;
			
			if( Value.StartsWith( "\"" ) ) {
				Value = Value.Remove( 0, 1 );
			}
			if( Value.EndsWith( "\"" ) ) {
				Value = Value.Remove( Value.Length - 1 );
			}
			//if( radioButtonNormal.Checked ) {
			//	Value = Value.Replace( @"\\", @"\" ).Replace( "\\\"", "\"" );
			//}
			myPreType = "@WORD";
			
			if( this.Visible ) {
				return null;
			}
			this.textBoxAstring.Text = Value.Replace( EXP.U_SP, ' ' );
			RadioButtonNormalClick( null, null );
			this.textBoxAstring.SelectionLength = 0;
			this.textBoxAstring.SelectionStart = Value.Length;
		}
		else {
			labelColor.Text = "";
			this.textBoxNumber.Text = "";
			this.textBoxResult.Text = "";
			Result = "";
		}
		SelectMod( ePanel.ModList[0].Name );
		
		//注意,直接设置Visible不能使窗体获得焦点
		//this.Visible = true;
		//this.Focus();
		//while( this.Visible ) {
			//System.Windows.Forms.Application.DoEvents();
		//}
		
		if( !LocationInit ) {
			LocationInit = true;
			ePanel.Location = new Point( (G.CGPanel.Width-ePanel.Width)/2, (G.CGPanel.Height-ePanel.Height)/2 );
		}
		
		ePanel.Visible = true;
		
		return null;
	}
	
	//刷新组件列表
	void RefreshModuleList()
	{
		ePanel.ClearModList();
		
		if( NeedType != n_EXP.EXP.ENote.v_void ) {
			
			//判断表达式左边是否为需要可变类型
			if( NeedType != n_EXP.EXP.ENote.v_mbool && NeedType != n_EXP.EXP.ENote.v_mint32 && NeedType != n_EXP.EXP.ENote.v_mfix ) {
				ePanel.AddMod( n_Language.Language.EXP_Number, ImgNumber );
				ePanel.AddMod( n_Language.Language.EXP_Oper, ImgOper );
			}
		}
		//this.listBox组件列表.Items.Add( n_Language.Language.EXP_Var );
		
		//if( NeedType != n_EXP.EXP.ENote.v_mbool && NeedType != n_EXP.EXP.ENote.v_mint32 ) {
			ePanel.AddMod( n_Language.Language.EXP_User, ImgUser );
		//}
		
		//收集所有的分组信息
		foreach( n_GroupList.Group gr in G.CGPanel.mGroupList ) {
			if( gr.GMes != "" ) {
				ePanel.AddMod( "<" + gr.Name + ">", ImgFrame );
			}
		}
		
		//注意这里是要保持组件为选中状态, 防止窗体关闭后组件取消高亮造成的闪烁
		//TargetGPanel.SelPanel.SelectState = 3;
		
		bool temp = G.CGPanel.myModuleList.IterReverse;
		G.CGPanel.myModuleList.IterReverse = false;
		foreach( MyObject mo in G.CGPanel.myModuleList ) {
			
			//添加代码框
			if( mo is n_GNote.GNote ) {
				n_GNote.GNote gn = (n_GNote.GNote)mo;
				if( gn.GroupMes != null ) {
					//continue;
				}
				if( gn.isCode && gn.CodeType == n_GNote.GNote.T_CX ) {
					ePanel.AddMod( "#" + gn.Name, ImgCode );
				}
				continue;
			}
			if( !( mo is MyFileObject ) ) {
				continue;
			}
			MyFileObject mf = (MyFileObject)mo;
			
			if( mf is n_HardModule.HardModule && ((n_HardModule.HardModule)mf).PackHid ) {
				continue;
			}
			if( mf.ImageName == SPMoudleName.SYS_iport || mf.ImageName == SPMoudleName.mbb || mf.ImageName == SPMoudleName.SYS_PADSHAP ) {
				continue;
			}
			if( InsGroupMes == null ) {
				if( mf.GroupMes != null && mf.GroupMes != ""  ) {
					continue;
				}
			}
			else {
				if( mf.GroupMes != null && InsGroupMes != mf.GroupMes ) {
					continue;
				}
			}
			if( mf.ImageName != SPMoudleName.mbb && mf.ImageName != SPMoudleName.SYS_NULL ) {
				if( mo is n_UIModule.UIModule ) {
					ePanel.AddMod( mo.Name, ImgVui );
				}
				else {
					ePanel.AddMod( mo.Name, ((HardModule)mo).SourceImage );
				}
			}
		}
		G.CGPanel.myModuleList.IterReverse = temp;
		
		//this.listBox组件列表.SelectedIndex = 0;
	}
	
	void Deal( string Name, int Index )
	{
		if( Name.IndexOf( EXPMember.ErrorConst ) != -1 ) {
			return;
		}
		//if( myPreType != "@WORD" ) {
			myPreType = ExpTypeList[ Index ];
		//}
		
		//判断用户是否先选中了变量, 是的话自动添加对应的运算指令进行二次选择
		if( NeedType == EXP.ENote.v_void && (myPreType == EXP.ENote.t_VAR || myPreType.StartsWith( EXP.ENote.t_MVAR + " " ) ) ) {
			string ss;
			
			if( VarTypeList[ Index ] == EXP.ENote.v_int32 ) {
				ss = "( *int32 " + myPreType + " " + Name + " ) = ( ?int32 );";
				ss += "( *int32 " + myPreType + " " + Name + " ) += ( ?int32 );";
				ss += "( *int32 " + myPreType + " " + Name + " ) -= ( ?int32 );";
				ss += "( *int32 " + myPreType + " " + Name + " ) *= ( ?int32 );";
				ss += "( *int32 " + myPreType + " " + Name + " ) /= ( ?int32 );";
				ss += "( *int32 " + myPreType + " " + Name + " ) %= ( ?int32 )";
				ResetListBox();
				SetListBox( ss.Split( ';' ), myPreType );
				
				ExpTypeList.Clear();
				ExpTypeList.Add( EXP.ENote.t_OPER );
				ExpTypeList.Add( EXP.ENote.t_OPER );
				ExpTypeList.Add( EXP.ENote.t_OPER );
				ExpTypeList.Add( EXP.ENote.t_OPER );
				ExpTypeList.Add( EXP.ENote.t_OPER );
				ExpTypeList.Add( EXP.ENote.t_OPER );
				
				InnerNameList.Clear();
				InnerNameList.Add( "sys_oper_set" );
				InnerNameList.Add( "sys_oper_add" );
				InnerNameList.Add( "sys_oper_sub" );
				InnerNameList.Add( "sys_oper_mul" );
				InnerNameList.Add( "sys_oper_div" );
				InnerNameList.Add( "sys_oper_mod" );
			}
			else if( VarTypeList[ Index ] == EXP.ENote.v_fix ) {
				ss = "( *fix " + myPreType + " " + Name + " ) = ( ?fix );";
				ss += "( *fix " + myPreType + " " + Name + " ) += ( ?fix );";
				ss += "( *fix " + myPreType + " " + Name + " ) -= ( ?fix );";
				ss += "( *fix " + myPreType + " " + Name + " ) *= ( ?fix );";
				ss += "( *fix " + myPreType + " " + Name + " ) /= ( ?fix );";
				ss += "( *fix " + myPreType + " " + Name + " ) %= ( ?fix )";
				ResetListBox();
				SetListBox( ss.Split( ';' ), myPreType );
				
				ExpTypeList.Clear();
				ExpTypeList.Add( EXP.ENote.t_OPER );
				ExpTypeList.Add( EXP.ENote.t_OPER );
				ExpTypeList.Add( EXP.ENote.t_OPER );
				ExpTypeList.Add( EXP.ENote.t_OPER );
				ExpTypeList.Add( EXP.ENote.t_OPER );
				ExpTypeList.Add( EXP.ENote.t_OPER );
				
				InnerNameList.Clear();
				InnerNameList.Add( "sys_oper_set" );
				InnerNameList.Add( "sys_oper_add" );
				InnerNameList.Add( "sys_oper_sub" );
				InnerNameList.Add( "sys_oper_mul" );
				InnerNameList.Add( "sys_oper_div" );
				InnerNameList.Add( "sys_oper_mod" );
			}
			else {
				ss = "( *bool " + myPreType + " " + Name + " ) = ( ?bool )";
				ResetListBox();
				SetListBox( ss.Split( ';' ), myPreType );
				
				ExpTypeList.Clear();
				ExpTypeList.Add( EXP.ENote.t_OPER );
				
				InnerNameList.Clear();
				InnerNameList.Add( "sys_oper_bset" );
			}
			return;
		}
		//判断用户是否先选中了变量, 是的话自动添加对应的运算指令进行二次选择
		if( NeedType == EXP.ENote.v_bool && (myPreType == EXP.ENote.t_VAR || myPreType.StartsWith( EXP.ENote.t_MVAR + " " ) || myPreType.StartsWith( EXP.ENote.t_MFUNC + " " ) ) ) {
			string ss;
			
			if( VarTypeList[ Index ] == EXP.ENote.v_int32 || VarTypeList[ Index ] == EXP.ENote.v_fix ) {
				
				if( VarTypeList[ Index ] == EXP.ENote.v_int32 ) {
					ss = "( ?int32 " + myPreType + " " + Name + " ) == ( ?int32 );";
					ss += "( ?int32 " + myPreType + " " + Name + " ) != ( ?int32 );";
					ss += "( ?int32 " + myPreType + " " + Name + " ) > ( ?int32 );";
					ss += "( ?int32 " + myPreType + " " + Name + " ) >= ( ?int32 );";
					ss += "( ?int32 " + myPreType + " " + Name + " ) < ( ?int32 );";
					ss += "( ?int32 " + myPreType + " " + Name + " ) <= ( ?int32 )";
				}
				else {
					ss = "( ?fix " + myPreType + " " + Name + " ) == ( ?fix );";
					ss += "( ?fix " + myPreType + " " + Name + " ) != ( ?fix );";
					ss += "( ?fix " + myPreType + " " + Name + " ) > ( ?fix );";
					ss += "( ?fix " + myPreType + " " + Name + " ) >= ( ?fix );";
					ss += "( ?fix " + myPreType + " " + Name + " ) < ( ?fix );";
					ss += "( ?fix " + myPreType + " " + Name + " ) <= ( ?fix )";
				}
				
				ResetListBox();
				SetListBox( ss.Split( ';' ), myPreType );
				
				ExpTypeList.Clear();
				ExpTypeList.Add( EXP.ENote.t_OPER );
				ExpTypeList.Add( EXP.ENote.t_OPER );
				ExpTypeList.Add( EXP.ENote.t_OPER );
				ExpTypeList.Add( EXP.ENote.t_OPER );
				ExpTypeList.Add( EXP.ENote.t_OPER );
				ExpTypeList.Add( EXP.ENote.t_OPER );
				
				InnerNameList.Clear();
				InnerNameList.Add( "sys_oper_eq" );
				InnerNameList.Add( "sys_oper_neq" );
				InnerNameList.Add( "sys_oper_la" );
				InnerNameList.Add( "sys_oper_lae" );
				InnerNameList.Add( "sys_oper_sm" );
				InnerNameList.Add( "sys_oper_sme" );
				return;
			}
			if( VarTypeList[ Index ] == EXP.ENote.v_Cstring ) {
				ss = "( ?Cstring " + myPreType + " " + Name + " ) == ( ?Cstring );";
				ss += "( ?Cstring " + myPreType + " " + Name + " ) != ( ?Cstring )";
				
				ResetListBox();
				SetListBox( ss.Split( ';' ), myPreType );
				
				ExpTypeList.Clear();
				ExpTypeList.Add( EXP.ENote.t_OPERS );
				ExpTypeList.Add( EXP.ENote.t_OPERS );
				
				InnerNameList.Clear();
				InnerNameList.Add( "sys_oper_eq" );
				InnerNameList.Add( "sys_oper_neq" );
				return;
			}
		}
		SelectedIndex = Index;
		
		n_NL.NL.CNode.User_Oper( Name );
		
		Result = Name;
		ePanel.Visible = false;
		
		SetExp();
	}
	
	void SetExp()
	{
		string rr = Result.Trim( "\t ".ToCharArray() );
		if( gr != null && InsGroupMes != gr.GMes ) {
			rr = n_GroupList.Group.GetFullName( gr.GMes, rr );
		}
		
		if( rr == "" ) {
			return;
		}
		string s = myPreType + " " + rr;
		
		if( cExp != null ) {
			//设置当前指令的提示信息
			if( s != null && cM != null ) {
				
				//这里判断选中的模块是否需要显示警告信息
				string InsMes = null;
				string iName = InnerNameList[SelectedIndex];
				if( cM.MacroFilePath == n_OS.OS.ModuleLibPath + @"engine\Text_v1\Text.B" || cM.MacroFilePath == n_OS.OS.ModuleLibPath + @"engine\Text_v2\Text.B" ) {
					if( iName == "show_number" || iName == "show_number_f" || iName == "show_number_ff" ) {
						GUIcoder.ExistShowNumberIns = true;
					}
					if( iName == "clear" || iName == "clear_line" || iName == "set_format" ) {
						GUIcoder.ExistClearNumberIns = true;
					}
				}
				
				if( cM.MacroFilePath == n_OS.OS.ModuleLibPath + @"engine\Text\Text.B" &&
				   ( iName == "show_number" ) ) {
					//InsMes = mm.GetMemberMessageFromName( iName ).Mes;
					
					InsMes =
						"您用到了信息显示器的显示数字功能, 这个功能不会清除屏幕上对应位置的旧数字, 会导致显示的数字和屏幕原来的字符混到一起, " +
						"例如: 之前屏幕从第一行第一列开始显示数字12345, 这次通过这个指令从第一行第一列开始显示67, 那么屏幕上最终结果是 67345. " +
						"为了避免这种情况, 您需要清掉之前的数字, 可在显示数字之前通过清空屏幕指令实现. 或者也可以通过在指定位置显示若干个空格字符" +
						"间接达到清屏的效果. 推荐的最佳方案是: 在指定位置显示数字之前, 先通过显示(几个空格)信息指令, 覆盖之前的字符";
				}
				if( InsMes != null ) {
					//获取选中的条目的说明信息
					//ModuleMessage mm = G.commonEXPBox.cM.mm[G.commonEXPBox.cM.GetLIndex()];
					
					G.CGPanel.AddMesPanel( InsMes, 3, this.owner );
				}
			}
			if( s == "" ) {
				s = cExp.ExpTree[ cExpIndex ].GetOperType();
			}
			else {
				s = cExp.ExpTree[ cExpIndex ].GetOperType() + " " + s;
			}
			cExp.ExpTree[ cExpIndex ].WordList = new string[] { s };
			string strEXP = cExp.JionEXP( cExp.ExpTree[ 0 ] );
			cExp.Set( strEXP );
		}
	}
	
	public void SelectMod( string UnitName )
	{
		//需要清空
		cM = null;
		string[] s = null;
		this.panel_Astring.Visible = false;
		this.panel_Number.Visible = false;
		this.ePanel.ModInsPanelVisible = false;
		MesText = "";
		
		if( UnitName == n_Language.Language.EXP_User || UnitName.StartsWith( "<" ) || UnitName.StartsWith( "#" ) ) {
			
			string GName = null;
			if( UnitName.StartsWith( "<" ) ) {
				GName = UnitName.Remove( UnitName.Length - 1 ).Remove( 0, 1 );
				gr = G.CGPanel.mGroupList.FindGroupByName( GName );
				
				if( gr == null || gr.MM == null ) {
					MesText = "框架描述为空:" + n_Language.Language.EXP_UserMes;
				}
				else {
					MesText = gr.MM.ModuleMes;
				}
			}
			else {
				gr = null;
				MesText = n_Language.Language.EXP_UserMes;
			}
			
			ExpTypeList.Clear();
			VarTypeList.Clear();
			InnerNameList.Clear();
			
			string[] s0 = null;
			string[] s1 = null;
			
			if( UnitName.StartsWith( "#" ) ) {
				s1 = EXPMember.GetUserCodeMember( UnitName.Remove( 0, 1 ), NeedType, InnerNameList, ExpTypeList, VarTypeList );
			}
			else {
				s0 = EXPMember.GetVarlist( false, gr, InsGroupMes, NeedType, VarTypeList, InnerNameList, ExpTypeList, owner );
				s1 = EXPMember.GetUserInsMember( gr, InsGroupMes, NeedType, InnerNameList, ExpTypeList, false, true );
			}
			
			int Length = 0;
			int Length0 = 0;
			if( s0 != null ) {
				Length = s0.Length;
				Length0 = Length;
			}
			if( s1 != null ) {
				Length += s1.Length;
			}
			s = new string[Length];
			int i = 0;
			if( s0 != null ) {
				for( ; i < s0.Length; ++i ) {
					s[i] = s0[i];
				}
			}
			if( s1 != null ) {
				for( ; i < s.Length; ++i ) {
					s[i] = s1[i - Length0];
				}
			}
			
			myPreType = "";
			ResetListBox();
		}
		else if( UnitName == n_Language.Language.EXP_Number ) {
			MesText = n_Language.Language.EXP_NumberMes;
			if( NeedType == n_EXP.EXP.ENote.v_int32 || NeedType == n_EXP.EXP.ENote.v_fix || NeedType == n_EXP.EXP.ENote.v_Achar ) {
				this.panel_Number.Visible = true;
				this.textBoxResult.Focus();
			}
			if( NeedType == n_EXP.EXP.ENote.v_Astring || NeedType == n_EXP.EXP.ENote.v_Cstring ) {
				this.panel_Astring.Visible = true;
				this.textBoxAstring.Focus();
			}
			return;
		}
		else if( UnitName == n_Language.Language.EXP_Oper ) {
			MesText = n_Language.Language.EXP_OperMes;
			if( NeedType == n_EXP.EXP.ENote.v_void ) {
			}
			if( NeedType == n_EXP.EXP.ENote.v_bool ) {
				//this.panel_bool.Visible = true;
				ExpTypeList.Clear();
				InnerNameList.Clear();
				VarTypeList.Clear();
				s = GetBoolOperList( InnerNameList, ExpTypeList );
				ResetListBox();
			}
			if( NeedType == n_EXP.EXP.ENote.v_int32 || NeedType == n_EXP.EXP.ENote.v_Achar ) {
				//this.panel_int32.Visible = true;
				//this.button绝对值.Visible = true;
				//this.button取负.Visible = true;
				ExpTypeList.Clear();
				InnerNameList.Clear();
				VarTypeList.Clear();
				s = GetInt32OperList( InnerNameList, ExpTypeList );
				ResetListBox();
			}
			if( NeedType == n_EXP.EXP.ENote.v_fix ) {
				//this.panel_int32.Visible = true;
				//this.button绝对值.Visible = true;
				//this.button取负.Visible = true;
				ExpTypeList.Clear();
				InnerNameList.Clear();
				VarTypeList.Clear();
				s = GetFixOperList( InnerNameList, ExpTypeList );
				ResetListBox();
			}
		}
		else {
			ExpTypeList.Clear();
			InnerNameList.Clear();
			VarTypeList.Clear();
			cM = EXPMember.GetModule( UnitName );
			s = EXPMember.GetModuleMember( NeedType, UnitName, VarTypeList, InnerNameList, ExpTypeList, false, true, true );
			
			ModuleMessage mm = cM.mm[cM.GetLIndex()];
			MesText = mm.ModuleMes;
			
			myPreType = "";
			ResetListBox();
		}
		SetListBox( s, ExpTypeList );
	}
	
	string[] GetBoolOperList( List<string> InnerNameList, List<string> TypeList )
	{
		string[] eList = new string[13];
		
		TypeList.Add( EXP.ENote.t_WORD );
		VarTypeList.Add( "?" + EXP.ENote.v_bool );
		InnerNameList.Add( "sys_oper_bool_yes" );
		eList[0] = "真";
		
		TypeList.Add( EXP.ENote.t_WORD );
		VarTypeList.Add( "?" + EXP.ENote.v_bool );
		InnerNameList.Add( "sys_oper_bool_no" );
		eList[1] = "假";
		
		TypeList.Add( EXP.ENote.t_OPERS );
		VarTypeList.Add( "?" + EXP.ENote.v_bool );
		InnerNameList.Add( "sys_oper_bool_equl" );
		eList[2] = "( ?Cstring ) == ( ?Cstring )";
		
		TypeList.Add( EXP.ENote.t_OPERS );
		VarTypeList.Add( "?" + EXP.ENote.v_bool );
		InnerNameList.Add( "sys_oper_bool_nequl" );
		eList[3] = "( ?Cstring ) != ( ?Cstring )";
		
		TypeList.Add( EXP.ENote.t_OPER );
		VarTypeList.Add( "?" + EXP.ENote.v_bool );
		InnerNameList.Add( "sys_oper_bool_equl" );
		eList[4] = "( ?int32 ) == ( ?int32 )";
		
		TypeList.Add( EXP.ENote.t_OPER );
		VarTypeList.Add( "?" + EXP.ENote.v_bool );
		InnerNameList.Add( "sys_oper_bool_nequl" );
		eList[5] = "( ?int32 ) != ( ?int32 )";
		
		TypeList.Add( EXP.ENote.t_OPER );
		VarTypeList.Add( "?" + EXP.ENote.v_bool );
		InnerNameList.Add( "sys_oper_bool_la" );
		eList[6] = "( ?fix ) > ( ?fix )";
		
		TypeList.Add( EXP.ENote.t_OPER );
		VarTypeList.Add( "?" + EXP.ENote.v_bool );
		InnerNameList.Add( "sys_oper_bool_lae" );
		eList[7] = "( ?fix ) >= ( ?fix )";
		
		TypeList.Add( EXP.ENote.t_OPER );
		VarTypeList.Add( "?" + EXP.ENote.v_bool );
		InnerNameList.Add( "sys_oper_bool_sm" );
		eList[8] = "( ?fix ) < ( ?fix )";
		
		TypeList.Add( EXP.ENote.t_OPER );
		VarTypeList.Add( "?" + EXP.ENote.v_bool );
		InnerNameList.Add( "sys_oper_bool_sme" );
		eList[9] = "( ?fix ) <= ( ?fix )";
		
		TypeList.Add( EXP.ENote.t_OPER );
		VarTypeList.Add( "?" + EXP.ENote.v_bool );
		InnerNameList.Add( "sys_oper_bool_not" );
		//eList[10] = "不成立 ( ?bool )";
		eList[10] = "非 ( ?bool )";
		
		TypeList.Add( EXP.ENote.t_OPER );
		VarTypeList.Add( "?" + EXP.ENote.v_bool );
		InnerNameList.Add( "sys_oper_bool_and" );
		eList[11] = "( ?bool ) 并且 ( ?bool )";
		
		TypeList.Add( EXP.ENote.t_OPER );
		VarTypeList.Add( "?" + EXP.ENote.v_bool );
		InnerNameList.Add( "sys_oper_bool_or" );
		eList[12] = "( ?bool ) 或者 ( ?bool )";
		
		return eList;
	}
	
	string[] GetInt32OperList( List<string> InnerNameList, List<string> TypeList )
	{
		string[] eList = new string[7];
		
		TypeList.Add( EXP.ENote.t_OPER );
		VarTypeList.Add( "?" + EXP.ENote.v_int32 );
		InnerNameList.Add( "sys_oper_int32_abs" );
		eList[0] = "绝对值 ( ?int32 )";
		
		TypeList.Add( EXP.ENote.t_OPER );
		VarTypeList.Add( "?" + EXP.ENote.v_int32 );
		InnerNameList.Add( "sys_oper_int32_neg" );
		eList[1] = "- ( ?int32 )";
		
		TypeList.Add( EXP.ENote.t_OPER );
		VarTypeList.Add( "?" + EXP.ENote.v_int32 );
		InnerNameList.Add( "sys_oper_int32_add" );
		eList[2] = "( ?int32 ) + ( ?int32 )";
		
		TypeList.Add( EXP.ENote.t_OPER );
		VarTypeList.Add( "?" + EXP.ENote.v_int32 );
		InnerNameList.Add( "sys_oper_int32_sub" );
		eList[3] = "( ?int32 ) - ( ?int32 )";
		
		TypeList.Add( EXP.ENote.t_OPER );
		VarTypeList.Add( "?" + EXP.ENote.v_int32 );
		InnerNameList.Add( "sys_oper_int32_mul" );
		eList[4] = "( ?int32 ) * ( ?int32 )";
		
		TypeList.Add( EXP.ENote.t_OPER );
		VarTypeList.Add( "?" + EXP.ENote.v_int32 );
		InnerNameList.Add( "sys_oper_int32_div" );
		eList[5] = "( ?int32 ) / ( ?int32 )";
		
		TypeList.Add( EXP.ENote.t_OPER );
		VarTypeList.Add( "?" + EXP.ENote.v_int32 );
		InnerNameList.Add( "sys_oper_int32_mod" );
		eList[6] = "( ?int32 ) % ( ?int32 )";
		
		return eList;
	}
	
	string[] GetFixOperList( List<string> InnerNameList, List<string> TypeList )
	{
		string[] eList = new string[7];
		
		TypeList.Add( EXP.ENote.t_OPER );
		VarTypeList.Add( "?" + EXP.ENote.v_int32 );
		InnerNameList.Add( "sys_oper_fix_abs" );
		eList[0] = "绝对值 ( ?fix )";
		
		TypeList.Add( EXP.ENote.t_OPER );
		VarTypeList.Add( "?" + EXP.ENote.v_int32 );
		InnerNameList.Add( "sys_oper_fix_neg" );
		eList[1] = "- ( ?fix )";
		
		TypeList.Add( EXP.ENote.t_OPER );
		VarTypeList.Add( "?" + EXP.ENote.v_int32 );
		InnerNameList.Add( "sys_oper_fix_add" );
		eList[2] = "( ?fix ) + ( ?fix )";
		
		TypeList.Add( EXP.ENote.t_OPER );
		VarTypeList.Add( "?" + EXP.ENote.v_int32 );
		InnerNameList.Add( "sys_oper_fix_sub" );
		eList[3] = "( ?fix ) - ( ?fix )";
		
		TypeList.Add( EXP.ENote.t_OPER );
		VarTypeList.Add( "?" + EXP.ENote.v_int32 );
		InnerNameList.Add( "sys_oper_fix_mul" );
		eList[4] = "( ?fix ) * ( ?fix )";
		
		TypeList.Add( EXP.ENote.t_OPER );
		VarTypeList.Add( "?" + EXP.ENote.v_int32 );
		InnerNameList.Add( "sys_oper_fix_div" );
		eList[5] = "( ?fix ) / ( ?fix )";
		
		TypeList.Add( EXP.ENote.t_OPER );
		VarTypeList.Add( "?" + EXP.ENote.v_int32 );
		InnerNameList.Add( "sys_oper_fix_mod" );
		eList[6] = "( ?fix ) % ( ?fix )";
		
		return eList;
	}
	
	void ResetListBox()
	{
		//this.listBox成员列表.Items.Clear();
		//this.listBox成员列表.Visible = true;
		//this.listBox成员列表.Focus();
		//ePanel.Visible = true;
		
		this.ePanel.ModInsPanelVisible = true;
		ePanel.Focus();
	}
	
	void SetListBox( string[] s, List<string> pType )
	{
		//if( s != null ) {
		//	this.listBox成员列表.Items.AddRange( s );
		//	listBox成员列表.SelectedIndex = -1;
		//}
		
		GUIset.ExpFont = GUIset.UIFont;
		
		ePanel.SetExp( s, pType );
		
		GUIset.ExpFont = GUIset.ExpFontBack;
	}
	
	void SetListBox( string[] s, string pType )
	{
		//if( s != null ) {
		//	this.listBox成员列表.Items.AddRange( s );
		//	listBox成员列表.SelectedIndex = -1;
		//}
		
		GUIset.ExpFont = GUIset.UIFont;
		
		ePanel.SetExp( s, pType );
		
		GUIset.ExpFont = GUIset.ExpFontBack;
	}
	
	void NumberbuttonClick(object sender, EventArgs e)
	{
		string text = ((Button)sender).Text;
		if( text == "-" ) {
			if( this.textBoxResult.Text.StartsWith( "-" ) ) {
				this.textBoxResult.Text = this.textBoxResult.Text.Remove( 0, 1 );
			}
			else {
				this.textBoxResult.Text = "-" + this.textBoxResult.Text;
			}
		}
		else {
			this.textBoxResult.Text += text;
		}
	}
	
	void Button确定Click(object sender, EventArgs e)
	{
		if( isAchar ) {
			Result = "'" + this.textBoxNumber.Text.Replace( ' ', EXP.U_SP ) + "'";
		}
		else {
			if( this.textBoxResult.Text == "" ) {
				MessageBox.Show( "请输入数字!" );
				return;
			}
			if( this.textBoxResult.Text[0] != '-' && (this.textBoxResult.Text[0] < '0' || this.textBoxResult.Text[0] > '9') ) {
				MessageBox.Show( "请输入数字!" );
				return;
			}
			Result = this.textBoxResult.Text;
		}
		myPreType = "@WORD";
		
		if( Result == "''" || Result == "" ) {
			//isOK = false;
		}
		ePanel.Visible = false;
		
		SetExp();
	}
	
	void ButtonCEClick(object sender, EventArgs e)
	{
		isIgnore = true;
		this.textBoxNumber.Text = "";
		this.textBoxResult.Text = "";
		Result = "";
		isIgnore = false;
	}
	
	void TextBoxNumberMouseDown(object sender, MouseEventArgs e)
	{
		this.textBoxNumber.SelectAll();
	}
	
	bool isAchar;
	bool isIgnore = false;
	void TextBoxNumberTextChanged(object sender, EventArgs e)
	{
		if( isIgnore ) {
			return;
		}
		isIgnore = true;
		try {
			//计算unicode字符代码
			//this.textBoxResult.Text = ((int)this.textBoxNumber.Text[0]).ToString();
			
			//计算GBK代码
			if( this.textBoxNumber.Text.Length > 0 ) {
				char c = this.textBoxNumber.Text[0];
				if( (int)c > 255 ) {
					byte[] gbk = System.Text.Encoding.GetEncoding("GBK").GetBytes( c.ToString() );
					this.textBoxResult.Text = (gbk[0] * 256 + gbk[1]).ToString();
				}
				else {
					this.textBoxResult.Text = ((int)c).ToString();
				}
			}
		}
		catch(Exception ee) {
			this.textBoxResult.Text = "";
			MessageBox.Show( ee.ToString() );
		}
		isIgnore = false;
		
		this.textBoxNumber.SelectAll();
		isAchar = true;
	}
	
	void TextBoxResultTextChanged(object sender, EventArgs e)
	{
		if( isIgnore ) {
			return;
		}
		isAchar = false;
		
		isIgnore = true;
		try {
			int d = int.Parse( this.textBoxResult.Text );
			
			if( d >= 128 ) {
				byte[] bl = new byte[2];
				bl[0] = (byte)(d / 256);
				bl[1] = (byte)(d % 256);
				char c = System.Text.Encoding.GetEncoding("GBK").GetChars( bl )[0];
				this.textBoxNumber.Text = c.ToString();
			}
			else {
				this.textBoxNumber.Text = ((char)d ).ToString();
			}
		}
		catch {
			this.textBoxNumber.Text = "";
		}
		isIgnore = false;
	}
	
	void RadioButtonNormalClick(object sender, EventArgs e)
	{
		if( CharNormal ) {
			return;
		}
		CharNormal = true;
		this.textBoxAstring.Text = this.textBoxAstring.Text.Replace( @"\\", @"\" ).Replace( "\\\"", "\"" ).Replace( "\\t", "\t" );
	}
	
	void RadioButtonSecondClick(object sender, EventArgs e)
	{
		if( !CharNormal ) {
			return;
		}
		CharNormal = false;
		this.textBoxAstring.Text = this.textBoxAstring.Text.Replace( @"\", @"\\" ).Replace( "\"", "\\\"" ).Replace( "\t", "\\t" );
		this.textBoxAstring.Text = this.textBoxAstring.Text.Replace( @"\\r", @"\r" ).Replace( @"\\n", @"\n" );
	}
	
	void ButtonAstringOkClick(object sender, EventArgs e)
	{
		if( CharNormal ) {
			RadioButtonSecondClick( null, null );
		}
		Result = this.textBoxAstring.Text;
		if( this.radioButtonUnicode.Checked ) {
			Result = "\"" + Result + "\"(unicode)";
		}
		else {
			Result = "\"" + Result + "\"";
		}
		Result = Result.Replace( ' ', EXP.U_SP );
		
		myPreType = "@WORD";
		ePanel.Visible = false;
		
		SetExp();
	}
	
	void MouseOnIndexChanged( int mIndex )
	{
		if( cM == null || mIndex == -1 ) {
			
			//分组信息或者用户自定义信息
			if( mIndex != -1 ) {
				
				string iName0 = InnerNameList[mIndex];
				if( gr == null ) {
					//MessageBox.Show( "MouseOnIndexChanged 分组为空!" );
					
					//这里应该提取全局描述信息对象
					
					if( iName0.StartsWith( EXPMember.GMesHead ) ) {
						MesText = iName0.Remove( 0, EXPMember.GMesHead.Length );
					}
					return;
				}
				ModuleMessage mm0 = gr.MM;
				if( mm0 != null ) {
					MemberMes mmm = mm0.GetMemberMessageFromName( iName0 );
					if( mmm != null ) {
						MesText = mmm.Mes;
					}
					else {
						MesText = "未找到<" + iName0 + ">的描述信息, 请在分组的描述框中编辑描述信息";
					}
				}
				else {
					MesText = "未找到<" + iName0 + ">的描述信息, 请在分组中添加一个描述框并编辑描述信息";
				}
			}
			//如果没有放到任意条目上, 则显示主模块的说明信息
			else {
				if( cM != null ) {
					ModuleMessage mm1 = cM.mm[cM.GetLIndex()];
					MesText = mm1.ModuleMes;
				}
			}
			return;
		}
		string iName = InnerNameList[mIndex];
		if( iName.StartsWith( "sys_oper_" ) ) {
			return;
		}
		if( iName == "-" ) {
			return;
		}
		if( iName.StartsWith( EXPMember.GMesHead ) ) {
			MesText = iName.Remove( 0, EXPMember.GMesHead.Length );
			return;
		}
		/*
		if( iName == "-0" ) {
			MesText = "把事件作为指令来使用的话, 相当于手工触发执行一次事件";
			return;
		}
		if( iName == "-1" ) {
			MesText = "在启用指定事件的状态下，如果对应事件的条件成立，那么系统就会执行此事件里的用户指令序列 (注意:开机默认情况下所有的事件都是在启用状态)";
			return;
		}
		if( iName == "-2" ) {
			MesText = "如果禁用指定事件的话，那么系统以后将不再检测此事件是否成立，也不执行事件里的指令序列 (注意:如果指定的事件正在执行中, 那么会继续执行完, 不受影响)";
			return;
		}
		if( iName == "-3" ) {
			MesText = "结束指定事件是指立刻让指定事件停止运行， 例如事件处于延时、等待、或者反复执行的时候，可以用这个指令强制结束事件";
			return;
		}
		*/
		
		ModuleMessage mm = cM.mm[cM.GetLIndex()];
		MesText = mm.GetMemberMessageFromName( iName ).Mes;
	}
	
	void ScommonEXPFormFormClosing(object sender, FormClosingEventArgs e)
	{
		this.Visible = false;
	}
	
	//=====================================================================================
	
	void Button颜色Click(object sender, EventArgs e)
	{
		
		Button b = (Button)sender;
		if( b.Text == "透明" ) {
			//Result = "-1";
			Result = "{FF-FFFFFF}" + b.Text;
		}
		else {
			string r_hex = b.BackColor.R.ToString( "X" ).PadLeft( 2, '0' );
			string g_hex = b.BackColor.G.ToString( "X" ).PadLeft( 2, '0' );
			string b_hex = b.BackColor.B.ToString( "X" ).PadLeft( 2, '0' );
			int rgb = b.BackColor.R * 65536 + b.BackColor.G * 256 + b.BackColor.B;
			
			//Result = rgb.ToString();
			Result = "{00-" + r_hex + "" + g_hex + "" + b_hex + "}" + b.Text;
		}
		
		myPreType = "@WORD";
		ePanel.Visible = false;
		
		SetExp();
	}
	
	void Button颜色MouseEnter(object sender, EventArgs e)
	{
		Button b = (Button)sender;
		
		if( b.Text == "透明" ) {
			labelColor.Text = "(FF FF,FF,FF) -1";
		}
		else {
			string r_hex = b.BackColor.R.ToString( "X" ).PadLeft( 2, '0' );
			string g_hex = b.BackColor.G.ToString( "X" ).PadLeft( 2, '0' );
			string b_hex = b.BackColor.B.ToString( "X" ).PadLeft( 2, '0' );
			int rgb = b.BackColor.R * 65536 + b.BackColor.G * 256 + b.BackColor.B;
			
			labelColor.Text = "{00-" + r_hex + "," + g_hex + "," + b_hex + "} " + rgb;
		}
	}
	
	void Button颜色MouseLeave(object sender, EventArgs e)
	{
		labelColor.Text = "";
	}
	
	void ButtonEditColorClick(object sender, EventArgs e)
	{
		CD = new ColorDialog();
		DialogResult dr = CD.ShowDialog();
		if( dr == DialogResult.OK ) {
			
			//int rgb = CD.Color.R * 65536 + CD.Color.G * 256 + CD.Color.B;
			//Result = rgb.ToString();
			
			string r_hex = CD.Color.R.ToString( "X" ).PadLeft( 2, '0' );
			string g_hex = CD.Color.G.ToString( "X" ).PadLeft( 2, '0' );
			string b_hex = CD.Color.B.ToString( "X" ).PadLeft( 2, '0' );
			
			//Result = rgb.ToString();
			Result = "{00-" + r_hex + "" + g_hex + "" + b_hex + "}";
			
			myPreType = "@WORD";
			ePanel.Visible = false;
			
			SetExp();
		}
	}
	
	void LabelColorHideClick(object sender, EventArgs e)
	{
		labelColorHide.Visible = false;
	}
}
}
