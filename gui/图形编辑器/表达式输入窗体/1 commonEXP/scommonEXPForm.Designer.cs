﻿/*
 * 由 SharpDevelop 创建。
 * 用户: bbb_2
 * 日期: 2009-11-17
 * 时间: 11:15
 * 
 * 如果你想要更改该模板，那么请使用“工具 | 选项 | 编辑 | 编辑标准标题”。
 */
namespace n_scommonEXPForm
{
	partial class scommonEXPForm
	{
		/// <summary>
		/// Designer variable used to keep track of non-visual components.
		/// </summary>
		private System.ComponentModel.IContainer components = null;
		
		/// <summary>
		/// Disposes resources used by the form.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing) {
				if (components != null) {
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}
		
		/// <summary>
		/// This method is required for Windows Forms designer support.
		/// Do not change the method contents inside the source code editor. The Forms designer might
		/// not be able to load this method if it was changed manually.
		/// </summary>
		private void InitializeComponent()
		{
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(scommonEXPForm));
			this.button确定 = new System.Windows.Forms.Button();
			this.textBoxNumber = new System.Windows.Forms.TextBox();
			this.panel_Number = new System.Windows.Forms.Panel();
			this.panel2 = new System.Windows.Forms.Panel();
			this.labelColorHide = new System.Windows.Forms.Label();
			this.label17 = new System.Windows.Forms.Label();
			this.button52 = new System.Windows.Forms.Button();
			this.button51 = new System.Windows.Forms.Button();
			this.button50 = new System.Windows.Forms.Button();
			this.button49 = new System.Windows.Forms.Button();
			this.button48 = new System.Windows.Forms.Button();
			this.button47 = new System.Windows.Forms.Button();
			this.button40 = new System.Windows.Forms.Button();
			this.button39 = new System.Windows.Forms.Button();
			this.button35 = new System.Windows.Forms.Button();
			this.label4 = new System.Windows.Forms.Label();
			this.labelColor = new System.Windows.Forms.Label();
			this.button38 = new System.Windows.Forms.Button();
			this.button37 = new System.Windows.Forms.Button();
			this.buttonEditColor = new System.Windows.Forms.Button();
			this.button44 = new System.Windows.Forms.Button();
			this.button45 = new System.Windows.Forms.Button();
			this.button46 = new System.Windows.Forms.Button();
			this.button41 = new System.Windows.Forms.Button();
			this.button42 = new System.Windows.Forms.Button();
			this.button43 = new System.Windows.Forms.Button();
			this.button36 = new System.Windows.Forms.Button();
			this.button颜色 = new System.Windows.Forms.Button();
			this.button34 = new System.Windows.Forms.Button();
			this.button33 = new System.Windows.Forms.Button();
			this.button26 = new System.Windows.Forms.Button();
			this.button27 = new System.Windows.Forms.Button();
			this.button28 = new System.Windows.Forms.Button();
			this.button29 = new System.Windows.Forms.Button();
			this.button30 = new System.Windows.Forms.Button();
			this.button31 = new System.Windows.Forms.Button();
			this.button32 = new System.Windows.Forms.Button();
			this.button19 = new System.Windows.Forms.Button();
			this.button20 = new System.Windows.Forms.Button();
			this.button21 = new System.Windows.Forms.Button();
			this.button22 = new System.Windows.Forms.Button();
			this.button23 = new System.Windows.Forms.Button();
			this.button24 = new System.Windows.Forms.Button();
			this.button25 = new System.Windows.Forms.Button();
			this.button18 = new System.Windows.Forms.Button();
			this.button17 = new System.Windows.Forms.Button();
			this.button16 = new System.Windows.Forms.Button();
			this.button15 = new System.Windows.Forms.Button();
			this.button14 = new System.Windows.Forms.Button();
			this.button13 = new System.Windows.Forms.Button();
			this.button12 = new System.Windows.Forms.Button();
			this.label20 = new System.Windows.Forms.Label();
			this.label19 = new System.Windows.Forms.Label();
			this.label18 = new System.Windows.Forms.Label();
			this.label22 = new System.Windows.Forms.Label();
			this.label21 = new System.Windows.Forms.Label();
			this.label16 = new System.Windows.Forms.Label();
			this.label15 = new System.Windows.Forms.Label();
			this.label14 = new System.Windows.Forms.Label();
			this.label13 = new System.Windows.Forms.Label();
			this.label9 = new System.Windows.Forms.Label();
			this.label10 = new System.Windows.Forms.Label();
			this.label11 = new System.Windows.Forms.Label();
			this.label12 = new System.Windows.Forms.Label();
			this.label8 = new System.Windows.Forms.Label();
			this.label7 = new System.Windows.Forms.Label();
			this.label6 = new System.Windows.Forms.Label();
			this.label5 = new System.Windows.Forms.Label();
			this.buttonFloat = new System.Windows.Forms.Button();
			this.textBoxResult = new System.Windows.Forms.TextBox();
			this.label2 = new System.Windows.Forms.Label();
			this.label1 = new System.Windows.Forms.Label();
			this.buttonCE = new System.Windows.Forms.Button();
			this.button11 = new System.Windows.Forms.Button();
			this.button10 = new System.Windows.Forms.Button();
			this.button9 = new System.Windows.Forms.Button();
			this.button8 = new System.Windows.Forms.Button();
			this.button7 = new System.Windows.Forms.Button();
			this.button6 = new System.Windows.Forms.Button();
			this.button5 = new System.Windows.Forms.Button();
			this.button4 = new System.Windows.Forms.Button();
			this.button3 = new System.Windows.Forms.Button();
			this.button2 = new System.Windows.Forms.Button();
			this.button1 = new System.Windows.Forms.Button();
			this.radioButtonUnicode = new System.Windows.Forms.RadioButton();
			this.radioButtonAscii = new System.Windows.Forms.RadioButton();
			this.label3 = new System.Windows.Forms.Label();
			this.textBoxAstring = new System.Windows.Forms.TextBox();
			this.buttonAstringOk = new System.Windows.Forms.Button();
			this.panel_Astring = new System.Windows.Forms.Panel();
			this.groupBox2 = new System.Windows.Forms.GroupBox();
			this.radioButtonNormal = new System.Windows.Forms.RadioButton();
			this.label23 = new System.Windows.Forms.Label();
			this.label24 = new System.Windows.Forms.Label();
			this.radioButtonSecond = new System.Windows.Forms.RadioButton();
			this.groupBox1 = new System.Windows.Forms.GroupBox();
			this.panel1 = new System.Windows.Forms.Panel();
			this.panelUserCode = new System.Windows.Forms.Panel();
			this.label25 = new System.Windows.Forms.Label();
			this.textBoxUserCode = new System.Windows.Forms.TextBox();
			this.panel_Number.SuspendLayout();
			this.panel2.SuspendLayout();
			this.panel_Astring.SuspendLayout();
			this.groupBox2.SuspendLayout();
			this.groupBox1.SuspendLayout();
			this.panel1.SuspendLayout();
			this.panelUserCode.SuspendLayout();
			this.SuspendLayout();
			// 
			// button确定
			// 
			this.button确定.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(100)))), ((int)(((byte)(168)))), ((int)(((byte)(223)))));
			resources.ApplyResources(this.button确定, "button确定");
			this.button确定.ForeColor = System.Drawing.Color.Black;
			this.button确定.Name = "button确定";
			this.button确定.UseVisualStyleBackColor = false;
			this.button确定.Click += new System.EventHandler(this.Button确定Click);
			// 
			// textBoxNumber
			// 
			this.textBoxNumber.BackColor = System.Drawing.Color.White;
			this.textBoxNumber.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
			resources.ApplyResources(this.textBoxNumber, "textBoxNumber");
			this.textBoxNumber.Name = "textBoxNumber";
			this.textBoxNumber.TextChanged += new System.EventHandler(this.TextBoxNumberTextChanged);
			this.textBoxNumber.MouseDown += new System.Windows.Forms.MouseEventHandler(this.TextBoxNumberMouseDown);
			// 
			// panel_Number
			// 
			this.panel_Number.BackColor = System.Drawing.Color.White;
			this.panel_Number.Controls.Add(this.panel2);
			this.panel_Number.Controls.Add(this.buttonFloat);
			this.panel_Number.Controls.Add(this.textBoxResult);
			this.panel_Number.Controls.Add(this.label2);
			this.panel_Number.Controls.Add(this.label1);
			this.panel_Number.Controls.Add(this.buttonCE);
			this.panel_Number.Controls.Add(this.button11);
			this.panel_Number.Controls.Add(this.button10);
			this.panel_Number.Controls.Add(this.button9);
			this.panel_Number.Controls.Add(this.button8);
			this.panel_Number.Controls.Add(this.button7);
			this.panel_Number.Controls.Add(this.textBoxNumber);
			this.panel_Number.Controls.Add(this.button6);
			this.panel_Number.Controls.Add(this.button5);
			this.panel_Number.Controls.Add(this.button4);
			this.panel_Number.Controls.Add(this.button确定);
			this.panel_Number.Controls.Add(this.button3);
			this.panel_Number.Controls.Add(this.button2);
			this.panel_Number.Controls.Add(this.button1);
			resources.ApplyResources(this.panel_Number, "panel_Number");
			this.panel_Number.Name = "panel_Number";
			// 
			// panel2
			// 
			this.panel2.Controls.Add(this.labelColorHide);
			this.panel2.Controls.Add(this.label17);
			this.panel2.Controls.Add(this.button52);
			this.panel2.Controls.Add(this.button51);
			this.panel2.Controls.Add(this.button50);
			this.panel2.Controls.Add(this.button49);
			this.panel2.Controls.Add(this.button48);
			this.panel2.Controls.Add(this.button47);
			this.panel2.Controls.Add(this.button40);
			this.panel2.Controls.Add(this.button39);
			this.panel2.Controls.Add(this.button35);
			this.panel2.Controls.Add(this.label4);
			this.panel2.Controls.Add(this.labelColor);
			this.panel2.Controls.Add(this.button38);
			this.panel2.Controls.Add(this.button37);
			this.panel2.Controls.Add(this.buttonEditColor);
			this.panel2.Controls.Add(this.button44);
			this.panel2.Controls.Add(this.button45);
			this.panel2.Controls.Add(this.button46);
			this.panel2.Controls.Add(this.button41);
			this.panel2.Controls.Add(this.button42);
			this.panel2.Controls.Add(this.button43);
			this.panel2.Controls.Add(this.button36);
			this.panel2.Controls.Add(this.button颜色);
			this.panel2.Controls.Add(this.button34);
			this.panel2.Controls.Add(this.button33);
			this.panel2.Controls.Add(this.button26);
			this.panel2.Controls.Add(this.button27);
			this.panel2.Controls.Add(this.button28);
			this.panel2.Controls.Add(this.button29);
			this.panel2.Controls.Add(this.button30);
			this.panel2.Controls.Add(this.button31);
			this.panel2.Controls.Add(this.button32);
			this.panel2.Controls.Add(this.button19);
			this.panel2.Controls.Add(this.button20);
			this.panel2.Controls.Add(this.button21);
			this.panel2.Controls.Add(this.button22);
			this.panel2.Controls.Add(this.button23);
			this.panel2.Controls.Add(this.button24);
			this.panel2.Controls.Add(this.button25);
			this.panel2.Controls.Add(this.button18);
			this.panel2.Controls.Add(this.button17);
			this.panel2.Controls.Add(this.button16);
			this.panel2.Controls.Add(this.button15);
			this.panel2.Controls.Add(this.button14);
			this.panel2.Controls.Add(this.button13);
			this.panel2.Controls.Add(this.button12);
			this.panel2.Controls.Add(this.label20);
			this.panel2.Controls.Add(this.label19);
			this.panel2.Controls.Add(this.label18);
			this.panel2.Controls.Add(this.label22);
			this.panel2.Controls.Add(this.label21);
			this.panel2.Controls.Add(this.label16);
			this.panel2.Controls.Add(this.label15);
			this.panel2.Controls.Add(this.label14);
			this.panel2.Controls.Add(this.label13);
			this.panel2.Controls.Add(this.label9);
			this.panel2.Controls.Add(this.label10);
			this.panel2.Controls.Add(this.label11);
			this.panel2.Controls.Add(this.label12);
			this.panel2.Controls.Add(this.label8);
			this.panel2.Controls.Add(this.label7);
			this.panel2.Controls.Add(this.label6);
			this.panel2.Controls.Add(this.label5);
			resources.ApplyResources(this.panel2, "panel2");
			this.panel2.Name = "panel2";
			// 
			// labelColorHide
			// 
			this.labelColorHide.BackColor = System.Drawing.Color.White;
			resources.ApplyResources(this.labelColorHide, "labelColorHide");
			this.labelColorHide.ForeColor = System.Drawing.Color.LightSlateGray;
			this.labelColorHide.Name = "labelColorHide";
			this.labelColorHide.Click += new System.EventHandler(this.LabelColorHideClick);
			// 
			// label17
			// 
			this.label17.BackColor = System.Drawing.Color.Transparent;
			resources.ApplyResources(this.label17, "label17");
			this.label17.ForeColor = System.Drawing.Color.Black;
			this.label17.Name = "label17";
			// 
			// button52
			// 
			this.button52.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(192)))));
			resources.ApplyResources(this.button52, "button52");
			this.button52.ForeColor = System.Drawing.Color.Black;
			this.button52.Name = "button52";
			this.button52.UseVisualStyleBackColor = false;
			this.button52.Click += new System.EventHandler(this.Button颜色Click);
			this.button52.MouseEnter += new System.EventHandler(this.Button颜色MouseEnter);
			this.button52.MouseLeave += new System.EventHandler(this.Button颜色MouseLeave);
			// 
			// button51
			// 
			this.button51.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(255)))), ((int)(((byte)(205)))));
			resources.ApplyResources(this.button51, "button51");
			this.button51.ForeColor = System.Drawing.Color.Black;
			this.button51.Name = "button51";
			this.button51.UseVisualStyleBackColor = false;
			this.button51.Click += new System.EventHandler(this.Button颜色Click);
			this.button51.MouseEnter += new System.EventHandler(this.Button颜色MouseEnter);
			this.button51.MouseLeave += new System.EventHandler(this.Button颜色MouseLeave);
			// 
			// button50
			// 
			this.button50.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(215)))), ((int)(((byte)(255)))), ((int)(((byte)(128)))));
			resources.ApplyResources(this.button50, "button50");
			this.button50.ForeColor = System.Drawing.Color.Black;
			this.button50.Name = "button50";
			this.button50.UseVisualStyleBackColor = false;
			this.button50.Click += new System.EventHandler(this.Button颜色Click);
			this.button50.MouseEnter += new System.EventHandler(this.Button颜色MouseEnter);
			this.button50.MouseLeave += new System.EventHandler(this.Button颜色MouseLeave);
			// 
			// button49
			// 
			this.button49.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(0)))), ((int)(((byte)(64)))));
			resources.ApplyResources(this.button49, "button49");
			this.button49.ForeColor = System.Drawing.Color.Black;
			this.button49.Name = "button49";
			this.button49.UseVisualStyleBackColor = false;
			this.button49.Click += new System.EventHandler(this.Button颜色Click);
			this.button49.MouseEnter += new System.EventHandler(this.Button颜色MouseEnter);
			this.button49.MouseLeave += new System.EventHandler(this.Button颜色MouseLeave);
			// 
			// button48
			// 
			this.button48.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(88)))), ((int)(((byte)(128)))), ((int)(((byte)(0)))));
			resources.ApplyResources(this.button48, "button48");
			this.button48.ForeColor = System.Drawing.Color.Black;
			this.button48.Name = "button48";
			this.button48.UseVisualStyleBackColor = false;
			this.button48.Click += new System.EventHandler(this.Button颜色Click);
			this.button48.MouseEnter += new System.EventHandler(this.Button颜色MouseEnter);
			this.button48.MouseLeave += new System.EventHandler(this.Button颜色MouseLeave);
			// 
			// button47
			// 
			this.button47.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(128)))), ((int)(((byte)(77)))));
			resources.ApplyResources(this.button47, "button47");
			this.button47.ForeColor = System.Drawing.Color.Black;
			this.button47.Name = "button47";
			this.button47.UseVisualStyleBackColor = false;
			this.button47.Click += new System.EventHandler(this.Button颜色Click);
			this.button47.MouseEnter += new System.EventHandler(this.Button颜色MouseEnter);
			this.button47.MouseLeave += new System.EventHandler(this.Button颜色MouseLeave);
			// 
			// button40
			// 
			this.button40.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(0)))), ((int)(((byte)(128)))));
			resources.ApplyResources(this.button40, "button40");
			this.button40.ForeColor = System.Drawing.Color.Black;
			this.button40.Name = "button40";
			this.button40.UseVisualStyleBackColor = false;
			this.button40.Click += new System.EventHandler(this.Button颜色Click);
			this.button40.MouseEnter += new System.EventHandler(this.Button颜色MouseEnter);
			this.button40.MouseLeave += new System.EventHandler(this.Button颜色MouseLeave);
			// 
			// button39
			// 
			this.button39.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(255)))), ((int)(((byte)(155)))));
			resources.ApplyResources(this.button39, "button39");
			this.button39.ForeColor = System.Drawing.Color.Black;
			this.button39.Name = "button39";
			this.button39.UseVisualStyleBackColor = false;
			this.button39.Click += new System.EventHandler(this.Button颜色Click);
			this.button39.MouseEnter += new System.EventHandler(this.Button颜色MouseEnter);
			this.button39.MouseLeave += new System.EventHandler(this.Button颜色MouseLeave);
			// 
			// button35
			// 
			this.button35.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(175)))), ((int)(((byte)(255)))), ((int)(((byte)(0)))));
			resources.ApplyResources(this.button35, "button35");
			this.button35.ForeColor = System.Drawing.Color.Black;
			this.button35.Name = "button35";
			this.button35.UseVisualStyleBackColor = false;
			this.button35.Click += new System.EventHandler(this.Button颜色Click);
			this.button35.MouseEnter += new System.EventHandler(this.Button颜色MouseEnter);
			this.button35.MouseLeave += new System.EventHandler(this.Button颜色MouseLeave);
			// 
			// label4
			// 
			resources.ApplyResources(this.label4, "label4");
			this.label4.ForeColor = System.Drawing.Color.SlateGray;
			this.label4.Name = "label4";
			// 
			// labelColor
			// 
			this.labelColor.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
			resources.ApplyResources(this.labelColor, "labelColor");
			this.labelColor.ForeColor = System.Drawing.Color.SlateGray;
			this.labelColor.Name = "labelColor";
			// 
			// button38
			// 
			this.button38.BackColor = System.Drawing.Color.Gainsboro;
			resources.ApplyResources(this.button38, "button38");
			this.button38.ForeColor = System.Drawing.Color.Black;
			this.button38.Name = "button38";
			this.button38.UseVisualStyleBackColor = false;
			this.button38.Click += new System.EventHandler(this.Button颜色Click);
			this.button38.MouseEnter += new System.EventHandler(this.Button颜色MouseEnter);
			this.button38.MouseLeave += new System.EventHandler(this.Button颜色MouseLeave);
			// 
			// button37
			// 
			this.button37.BackColor = System.Drawing.Color.DarkGray;
			resources.ApplyResources(this.button37, "button37");
			this.button37.ForeColor = System.Drawing.Color.Black;
			this.button37.Name = "button37";
			this.button37.UseVisualStyleBackColor = false;
			this.button37.Click += new System.EventHandler(this.Button颜色Click);
			this.button37.MouseEnter += new System.EventHandler(this.Button颜色MouseEnter);
			this.button37.MouseLeave += new System.EventHandler(this.Button颜色MouseLeave);
			// 
			// buttonEditColor
			// 
			this.buttonEditColor.BackColor = System.Drawing.Color.Silver;
			resources.ApplyResources(this.buttonEditColor, "buttonEditColor");
			this.buttonEditColor.ForeColor = System.Drawing.Color.Black;
			this.buttonEditColor.Name = "buttonEditColor";
			this.buttonEditColor.UseVisualStyleBackColor = false;
			this.buttonEditColor.Click += new System.EventHandler(this.ButtonEditColorClick);
			// 
			// button44
			// 
			this.button44.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(211)))), ((int)(((byte)(157)))), ((int)(((byte)(255)))));
			resources.ApplyResources(this.button44, "button44");
			this.button44.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
			this.button44.Name = "button44";
			this.button44.UseVisualStyleBackColor = false;
			this.button44.Click += new System.EventHandler(this.Button颜色Click);
			this.button44.MouseEnter += new System.EventHandler(this.Button颜色MouseEnter);
			this.button44.MouseLeave += new System.EventHandler(this.Button颜色MouseLeave);
			// 
			// button45
			// 
			this.button45.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(67)))), ((int)(((byte)(0)))), ((int)(((byte)(140)))));
			resources.ApplyResources(this.button45, "button45");
			this.button45.ForeColor = System.Drawing.Color.Black;
			this.button45.Name = "button45";
			this.button45.UseVisualStyleBackColor = false;
			this.button45.Click += new System.EventHandler(this.Button颜色Click);
			this.button45.MouseEnter += new System.EventHandler(this.Button颜色MouseEnter);
			this.button45.MouseLeave += new System.EventHandler(this.Button颜色MouseLeave);
			// 
			// button46
			// 
			this.button46.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(0)))), ((int)(((byte)(255)))));
			resources.ApplyResources(this.button46, "button46");
			this.button46.ForeColor = System.Drawing.Color.Black;
			this.button46.Name = "button46";
			this.button46.UseVisualStyleBackColor = false;
			this.button46.Click += new System.EventHandler(this.Button颜色Click);
			this.button46.MouseEnter += new System.EventHandler(this.Button颜色MouseEnter);
			this.button46.MouseLeave += new System.EventHandler(this.Button颜色MouseLeave);
			// 
			// button41
			// 
			this.button41.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(142)))), ((int)(((byte)(206)))), ((int)(((byte)(255)))));
			resources.ApplyResources(this.button41, "button41");
			this.button41.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
			this.button41.Name = "button41";
			this.button41.UseVisualStyleBackColor = false;
			this.button41.Click += new System.EventHandler(this.Button颜色Click);
			this.button41.MouseEnter += new System.EventHandler(this.Button颜色MouseEnter);
			this.button41.MouseLeave += new System.EventHandler(this.Button颜色MouseLeave);
			// 
			// button42
			// 
			this.button42.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(64)))), ((int)(((byte)(128)))));
			resources.ApplyResources(this.button42, "button42");
			this.button42.ForeColor = System.Drawing.Color.Black;
			this.button42.Name = "button42";
			this.button42.UseVisualStyleBackColor = false;
			this.button42.Click += new System.EventHandler(this.Button颜色Click);
			this.button42.MouseEnter += new System.EventHandler(this.Button颜色MouseEnter);
			this.button42.MouseLeave += new System.EventHandler(this.Button颜色MouseLeave);
			// 
			// button43
			// 
			this.button43.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(128)))), ((int)(((byte)(255)))));
			resources.ApplyResources(this.button43, "button43");
			this.button43.ForeColor = System.Drawing.Color.Black;
			this.button43.Name = "button43";
			this.button43.UseVisualStyleBackColor = false;
			this.button43.Click += new System.EventHandler(this.Button颜色Click);
			this.button43.MouseEnter += new System.EventHandler(this.Button颜色MouseEnter);
			this.button43.MouseLeave += new System.EventHandler(this.Button颜色MouseLeave);
			// 
			// button36
			// 
			this.button36.BackColor = System.Drawing.Color.DimGray;
			resources.ApplyResources(this.button36, "button36");
			this.button36.ForeColor = System.Drawing.Color.Black;
			this.button36.Name = "button36";
			this.button36.UseVisualStyleBackColor = false;
			this.button36.Click += new System.EventHandler(this.Button颜色Click);
			this.button36.MouseEnter += new System.EventHandler(this.Button颜色MouseEnter);
			this.button36.MouseLeave += new System.EventHandler(this.Button颜色MouseLeave);
			// 
			// button颜色
			// 
			this.button颜色.BackColor = System.Drawing.Color.WhiteSmoke;
			resources.ApplyResources(this.button颜色, "button颜色");
			this.button颜色.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
			this.button颜色.Name = "button颜色";
			this.button颜色.UseVisualStyleBackColor = false;
			this.button颜色.Click += new System.EventHandler(this.Button颜色Click);
			this.button颜色.MouseEnter += new System.EventHandler(this.Button颜色MouseEnter);
			this.button颜色.MouseLeave += new System.EventHandler(this.Button颜色MouseLeave);
			// 
			// button34
			// 
			this.button34.BackColor = System.Drawing.Color.White;
			resources.ApplyResources(this.button34, "button34");
			this.button34.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
			this.button34.Name = "button34";
			this.button34.UseVisualStyleBackColor = false;
			this.button34.Click += new System.EventHandler(this.Button颜色Click);
			this.button34.MouseEnter += new System.EventHandler(this.Button颜色MouseEnter);
			this.button34.MouseLeave += new System.EventHandler(this.Button颜色MouseLeave);
			// 
			// button33
			// 
			this.button33.BackColor = System.Drawing.Color.Black;
			resources.ApplyResources(this.button33, "button33");
			this.button33.ForeColor = System.Drawing.Color.DarkGray;
			this.button33.Name = "button33";
			this.button33.UseVisualStyleBackColor = false;
			this.button33.Click += new System.EventHandler(this.Button颜色Click);
			this.button33.MouseEnter += new System.EventHandler(this.Button颜色MouseEnter);
			this.button33.MouseLeave += new System.EventHandler(this.Button颜色MouseLeave);
			// 
			// button26
			// 
			this.button26.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(255)))));
			resources.ApplyResources(this.button26, "button26");
			this.button26.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
			this.button26.Name = "button26";
			this.button26.UseVisualStyleBackColor = false;
			this.button26.Click += new System.EventHandler(this.Button颜色Click);
			this.button26.MouseEnter += new System.EventHandler(this.Button颜色MouseEnter);
			this.button26.MouseLeave += new System.EventHandler(this.Button颜色MouseLeave);
			// 
			// button27
			// 
			this.button27.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(192)))), ((int)(((byte)(255)))));
			resources.ApplyResources(this.button27, "button27");
			this.button27.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
			this.button27.Name = "button27";
			this.button27.UseVisualStyleBackColor = false;
			this.button27.Click += new System.EventHandler(this.Button颜色Click);
			this.button27.MouseEnter += new System.EventHandler(this.Button颜色MouseEnter);
			this.button27.MouseLeave += new System.EventHandler(this.Button颜色MouseLeave);
			// 
			// button28
			// 
			this.button28.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
			resources.ApplyResources(this.button28, "button28");
			this.button28.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
			this.button28.Name = "button28";
			this.button28.UseVisualStyleBackColor = false;
			this.button28.Click += new System.EventHandler(this.Button颜色Click);
			this.button28.MouseEnter += new System.EventHandler(this.Button颜色MouseEnter);
			this.button28.MouseLeave += new System.EventHandler(this.Button颜色MouseLeave);
			// 
			// button29
			// 
			this.button29.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
			resources.ApplyResources(this.button29, "button29");
			this.button29.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
			this.button29.Name = "button29";
			this.button29.UseVisualStyleBackColor = false;
			this.button29.Click += new System.EventHandler(this.Button颜色Click);
			this.button29.MouseEnter += new System.EventHandler(this.Button颜色MouseEnter);
			this.button29.MouseLeave += new System.EventHandler(this.Button颜色MouseLeave);
			// 
			// button30
			// 
			this.button30.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
			resources.ApplyResources(this.button30, "button30");
			this.button30.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
			this.button30.Name = "button30";
			this.button30.UseVisualStyleBackColor = false;
			this.button30.Click += new System.EventHandler(this.Button颜色Click);
			this.button30.MouseEnter += new System.EventHandler(this.Button颜色MouseEnter);
			this.button30.MouseLeave += new System.EventHandler(this.Button颜色MouseLeave);
			// 
			// button31
			// 
			this.button31.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(224)))), ((int)(((byte)(192)))));
			resources.ApplyResources(this.button31, "button31");
			this.button31.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
			this.button31.Name = "button31";
			this.button31.UseVisualStyleBackColor = false;
			this.button31.Click += new System.EventHandler(this.Button颜色Click);
			this.button31.MouseEnter += new System.EventHandler(this.Button颜色MouseEnter);
			this.button31.MouseLeave += new System.EventHandler(this.Button颜色MouseLeave);
			// 
			// button32
			// 
			this.button32.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(192)))));
			resources.ApplyResources(this.button32, "button32");
			this.button32.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
			this.button32.Name = "button32";
			this.button32.UseVisualStyleBackColor = false;
			this.button32.Click += new System.EventHandler(this.Button颜色Click);
			this.button32.MouseEnter += new System.EventHandler(this.Button颜色MouseEnter);
			this.button32.MouseLeave += new System.EventHandler(this.Button颜色MouseLeave);
			// 
			// button19
			// 
			this.button19.BackColor = System.Drawing.Color.Purple;
			resources.ApplyResources(this.button19, "button19");
			this.button19.ForeColor = System.Drawing.Color.Black;
			this.button19.Name = "button19";
			this.button19.UseVisualStyleBackColor = false;
			this.button19.Click += new System.EventHandler(this.Button颜色Click);
			this.button19.MouseEnter += new System.EventHandler(this.Button颜色MouseEnter);
			this.button19.MouseLeave += new System.EventHandler(this.Button颜色MouseLeave);
			// 
			// button20
			// 
			this.button20.BackColor = System.Drawing.Color.Navy;
			resources.ApplyResources(this.button20, "button20");
			this.button20.ForeColor = System.Drawing.Color.Silver;
			this.button20.Name = "button20";
			this.button20.UseVisualStyleBackColor = false;
			this.button20.Click += new System.EventHandler(this.Button颜色Click);
			this.button20.MouseEnter += new System.EventHandler(this.Button颜色MouseEnter);
			this.button20.MouseLeave += new System.EventHandler(this.Button颜色MouseLeave);
			// 
			// button21
			// 
			this.button21.BackColor = System.Drawing.Color.Teal;
			resources.ApplyResources(this.button21, "button21");
			this.button21.ForeColor = System.Drawing.Color.Black;
			this.button21.Name = "button21";
			this.button21.UseVisualStyleBackColor = false;
			this.button21.Click += new System.EventHandler(this.Button颜色Click);
			this.button21.MouseEnter += new System.EventHandler(this.Button颜色MouseEnter);
			this.button21.MouseLeave += new System.EventHandler(this.Button颜色MouseLeave);
			// 
			// button22
			// 
			this.button22.BackColor = System.Drawing.Color.Green;
			resources.ApplyResources(this.button22, "button22");
			this.button22.ForeColor = System.Drawing.Color.Silver;
			this.button22.Name = "button22";
			this.button22.UseVisualStyleBackColor = false;
			this.button22.Click += new System.EventHandler(this.Button颜色Click);
			this.button22.MouseEnter += new System.EventHandler(this.Button颜色MouseEnter);
			this.button22.MouseLeave += new System.EventHandler(this.Button颜色MouseLeave);
			// 
			// button23
			// 
			this.button23.BackColor = System.Drawing.Color.Olive;
			resources.ApplyResources(this.button23, "button23");
			this.button23.ForeColor = System.Drawing.Color.Black;
			this.button23.Name = "button23";
			this.button23.UseVisualStyleBackColor = false;
			this.button23.Click += new System.EventHandler(this.Button颜色Click);
			this.button23.MouseEnter += new System.EventHandler(this.Button颜色MouseEnter);
			this.button23.MouseLeave += new System.EventHandler(this.Button颜色MouseLeave);
			// 
			// button24
			// 
			this.button24.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(64)))), ((int)(((byte)(0)))));
			resources.ApplyResources(this.button24, "button24");
			this.button24.ForeColor = System.Drawing.Color.Black;
			this.button24.Name = "button24";
			this.button24.UseVisualStyleBackColor = false;
			this.button24.Click += new System.EventHandler(this.Button颜色Click);
			this.button24.MouseEnter += new System.EventHandler(this.Button颜色MouseEnter);
			this.button24.MouseLeave += new System.EventHandler(this.Button颜色MouseLeave);
			// 
			// button25
			// 
			this.button25.BackColor = System.Drawing.Color.Maroon;
			resources.ApplyResources(this.button25, "button25");
			this.button25.ForeColor = System.Drawing.Color.Silver;
			this.button25.Name = "button25";
			this.button25.UseVisualStyleBackColor = false;
			this.button25.Click += new System.EventHandler(this.Button颜色Click);
			this.button25.MouseEnter += new System.EventHandler(this.Button颜色MouseEnter);
			this.button25.MouseLeave += new System.EventHandler(this.Button颜色MouseLeave);
			// 
			// button18
			// 
			this.button18.BackColor = System.Drawing.Color.Fuchsia;
			resources.ApplyResources(this.button18, "button18");
			this.button18.ForeColor = System.Drawing.Color.Black;
			this.button18.Name = "button18";
			this.button18.UseVisualStyleBackColor = false;
			this.button18.Click += new System.EventHandler(this.Button颜色Click);
			this.button18.MouseEnter += new System.EventHandler(this.Button颜色MouseEnter);
			this.button18.MouseLeave += new System.EventHandler(this.Button颜色MouseLeave);
			// 
			// button17
			// 
			this.button17.BackColor = System.Drawing.Color.Blue;
			resources.ApplyResources(this.button17, "button17");
			this.button17.ForeColor = System.Drawing.Color.Black;
			this.button17.Name = "button17";
			this.button17.UseVisualStyleBackColor = false;
			this.button17.Click += new System.EventHandler(this.Button颜色Click);
			this.button17.MouseEnter += new System.EventHandler(this.Button颜色MouseEnter);
			this.button17.MouseLeave += new System.EventHandler(this.Button颜色MouseLeave);
			// 
			// button16
			// 
			this.button16.BackColor = System.Drawing.Color.Cyan;
			resources.ApplyResources(this.button16, "button16");
			this.button16.ForeColor = System.Drawing.Color.Black;
			this.button16.Name = "button16";
			this.button16.UseVisualStyleBackColor = false;
			this.button16.Click += new System.EventHandler(this.Button颜色Click);
			this.button16.MouseEnter += new System.EventHandler(this.Button颜色MouseEnter);
			this.button16.MouseLeave += new System.EventHandler(this.Button颜色MouseLeave);
			// 
			// button15
			// 
			this.button15.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
			resources.ApplyResources(this.button15, "button15");
			this.button15.ForeColor = System.Drawing.Color.Black;
			this.button15.Name = "button15";
			this.button15.UseVisualStyleBackColor = false;
			this.button15.Click += new System.EventHandler(this.Button颜色Click);
			this.button15.MouseEnter += new System.EventHandler(this.Button颜色MouseEnter);
			this.button15.MouseLeave += new System.EventHandler(this.Button颜色MouseLeave);
			// 
			// button14
			// 
			this.button14.BackColor = System.Drawing.Color.Yellow;
			resources.ApplyResources(this.button14, "button14");
			this.button14.ForeColor = System.Drawing.Color.Black;
			this.button14.Name = "button14";
			this.button14.UseVisualStyleBackColor = false;
			this.button14.Click += new System.EventHandler(this.Button颜色Click);
			this.button14.MouseEnter += new System.EventHandler(this.Button颜色MouseEnter);
			this.button14.MouseLeave += new System.EventHandler(this.Button颜色MouseLeave);
			// 
			// button13
			// 
			this.button13.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(0)))));
			resources.ApplyResources(this.button13, "button13");
			this.button13.ForeColor = System.Drawing.Color.Black;
			this.button13.Name = "button13";
			this.button13.UseVisualStyleBackColor = false;
			this.button13.Click += new System.EventHandler(this.Button颜色Click);
			this.button13.MouseEnter += new System.EventHandler(this.Button颜色MouseEnter);
			this.button13.MouseLeave += new System.EventHandler(this.Button颜色MouseLeave);
			// 
			// button12
			// 
			this.button12.BackColor = System.Drawing.Color.Red;
			resources.ApplyResources(this.button12, "button12");
			this.button12.ForeColor = System.Drawing.Color.Black;
			this.button12.Name = "button12";
			this.button12.UseVisualStyleBackColor = false;
			this.button12.Click += new System.EventHandler(this.Button颜色Click);
			this.button12.MouseEnter += new System.EventHandler(this.Button颜色MouseEnter);
			this.button12.MouseLeave += new System.EventHandler(this.Button颜色MouseLeave);
			// 
			// label20
			// 
			this.label20.BackColor = System.Drawing.Color.Transparent;
			resources.ApplyResources(this.label20, "label20");
			this.label20.ForeColor = System.Drawing.Color.SlateGray;
			this.label20.Name = "label20";
			// 
			// label19
			// 
			this.label19.BackColor = System.Drawing.Color.Transparent;
			resources.ApplyResources(this.label19, "label19");
			this.label19.ForeColor = System.Drawing.Color.SlateGray;
			this.label19.Name = "label19";
			// 
			// label18
			// 
			this.label18.BackColor = System.Drawing.Color.Transparent;
			resources.ApplyResources(this.label18, "label18");
			this.label18.ForeColor = System.Drawing.Color.SlateGray;
			this.label18.Name = "label18";
			// 
			// label22
			// 
			this.label22.BackColor = System.Drawing.Color.Transparent;
			resources.ApplyResources(this.label22, "label22");
			this.label22.ForeColor = System.Drawing.Color.Black;
			this.label22.Name = "label22";
			// 
			// label21
			// 
			this.label21.BackColor = System.Drawing.Color.Transparent;
			resources.ApplyResources(this.label21, "label21");
			this.label21.ForeColor = System.Drawing.Color.Black;
			this.label21.Name = "label21";
			// 
			// label16
			// 
			this.label16.BackColor = System.Drawing.Color.Transparent;
			resources.ApplyResources(this.label16, "label16");
			this.label16.ForeColor = System.Drawing.Color.SlateGray;
			this.label16.Name = "label16";
			// 
			// label15
			// 
			this.label15.BackColor = System.Drawing.Color.Transparent;
			resources.ApplyResources(this.label15, "label15");
			this.label15.ForeColor = System.Drawing.Color.SlateGray;
			this.label15.Name = "label15";
			// 
			// label14
			// 
			this.label14.BackColor = System.Drawing.Color.Transparent;
			resources.ApplyResources(this.label14, "label14");
			this.label14.ForeColor = System.Drawing.Color.SlateGray;
			this.label14.Name = "label14";
			// 
			// label13
			// 
			this.label13.BackColor = System.Drawing.Color.Transparent;
			resources.ApplyResources(this.label13, "label13");
			this.label13.ForeColor = System.Drawing.Color.Black;
			this.label13.Name = "label13";
			// 
			// label9
			// 
			this.label9.BackColor = System.Drawing.Color.Transparent;
			resources.ApplyResources(this.label9, "label9");
			this.label9.ForeColor = System.Drawing.Color.SlateGray;
			this.label9.Name = "label9";
			// 
			// label10
			// 
			this.label10.BackColor = System.Drawing.Color.Transparent;
			resources.ApplyResources(this.label10, "label10");
			this.label10.ForeColor = System.Drawing.Color.SlateGray;
			this.label10.Name = "label10";
			// 
			// label11
			// 
			this.label11.BackColor = System.Drawing.Color.Transparent;
			resources.ApplyResources(this.label11, "label11");
			this.label11.ForeColor = System.Drawing.Color.SlateGray;
			this.label11.Name = "label11";
			// 
			// label12
			// 
			this.label12.BackColor = System.Drawing.Color.Transparent;
			resources.ApplyResources(this.label12, "label12");
			this.label12.ForeColor = System.Drawing.Color.Black;
			this.label12.Name = "label12";
			// 
			// label8
			// 
			this.label8.BackColor = System.Drawing.Color.Transparent;
			resources.ApplyResources(this.label8, "label8");
			this.label8.ForeColor = System.Drawing.Color.SlateGray;
			this.label8.Name = "label8";
			// 
			// label7
			// 
			this.label7.BackColor = System.Drawing.Color.Transparent;
			resources.ApplyResources(this.label7, "label7");
			this.label7.ForeColor = System.Drawing.Color.SlateGray;
			this.label7.Name = "label7";
			// 
			// label6
			// 
			this.label6.BackColor = System.Drawing.Color.Transparent;
			resources.ApplyResources(this.label6, "label6");
			this.label6.ForeColor = System.Drawing.Color.SlateGray;
			this.label6.Name = "label6";
			// 
			// label5
			// 
			this.label5.BackColor = System.Drawing.Color.Transparent;
			resources.ApplyResources(this.label5, "label5");
			this.label5.ForeColor = System.Drawing.Color.Black;
			this.label5.Name = "label5";
			// 
			// buttonFloat
			// 
			this.buttonFloat.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
			resources.ApplyResources(this.buttonFloat, "buttonFloat");
			this.buttonFloat.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
			this.buttonFloat.Name = "buttonFloat";
			this.buttonFloat.UseVisualStyleBackColor = false;
			this.buttonFloat.MouseDown += new System.Windows.Forms.MouseEventHandler(this.NumberbuttonClick);
			// 
			// textBoxResult
			// 
			this.textBoxResult.BackColor = System.Drawing.Color.White;
			this.textBoxResult.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
			resources.ApplyResources(this.textBoxResult, "textBoxResult");
			this.textBoxResult.Name = "textBoxResult";
			this.textBoxResult.TextChanged += new System.EventHandler(this.TextBoxResultTextChanged);
			// 
			// label2
			// 
			resources.ApplyResources(this.label2, "label2");
			this.label2.Name = "label2";
			// 
			// label1
			// 
			resources.ApplyResources(this.label1, "label1");
			this.label1.Name = "label1";
			// 
			// buttonCE
			// 
			this.buttonCE.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(243)))), ((int)(((byte)(209)))), ((int)(((byte)(171)))));
			resources.ApplyResources(this.buttonCE, "buttonCE");
			this.buttonCE.ForeColor = System.Drawing.Color.Black;
			this.buttonCE.Name = "buttonCE";
			this.buttonCE.UseVisualStyleBackColor = false;
			this.buttonCE.Click += new System.EventHandler(this.ButtonCEClick);
			// 
			// button11
			// 
			this.button11.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
			resources.ApplyResources(this.button11, "button11");
			this.button11.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
			this.button11.Name = "button11";
			this.button11.UseVisualStyleBackColor = false;
			this.button11.MouseDown += new System.Windows.Forms.MouseEventHandler(this.NumberbuttonClick);
			// 
			// button10
			// 
			this.button10.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
			resources.ApplyResources(this.button10, "button10");
			this.button10.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
			this.button10.Name = "button10";
			this.button10.UseVisualStyleBackColor = false;
			this.button10.MouseDown += new System.Windows.Forms.MouseEventHandler(this.NumberbuttonClick);
			// 
			// button9
			// 
			this.button9.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
			resources.ApplyResources(this.button9, "button9");
			this.button9.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
			this.button9.Name = "button9";
			this.button9.UseVisualStyleBackColor = false;
			this.button9.MouseDown += new System.Windows.Forms.MouseEventHandler(this.NumberbuttonClick);
			// 
			// button8
			// 
			this.button8.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
			resources.ApplyResources(this.button8, "button8");
			this.button8.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
			this.button8.Name = "button8";
			this.button8.UseVisualStyleBackColor = false;
			this.button8.MouseDown += new System.Windows.Forms.MouseEventHandler(this.NumberbuttonClick);
			// 
			// button7
			// 
			this.button7.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
			resources.ApplyResources(this.button7, "button7");
			this.button7.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
			this.button7.Name = "button7";
			this.button7.UseVisualStyleBackColor = false;
			this.button7.MouseDown += new System.Windows.Forms.MouseEventHandler(this.NumberbuttonClick);
			// 
			// button6
			// 
			this.button6.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
			resources.ApplyResources(this.button6, "button6");
			this.button6.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
			this.button6.Name = "button6";
			this.button6.UseVisualStyleBackColor = false;
			this.button6.MouseDown += new System.Windows.Forms.MouseEventHandler(this.NumberbuttonClick);
			// 
			// button5
			// 
			this.button5.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
			resources.ApplyResources(this.button5, "button5");
			this.button5.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
			this.button5.Name = "button5";
			this.button5.UseVisualStyleBackColor = false;
			this.button5.MouseDown += new System.Windows.Forms.MouseEventHandler(this.NumberbuttonClick);
			// 
			// button4
			// 
			this.button4.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
			resources.ApplyResources(this.button4, "button4");
			this.button4.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
			this.button4.Name = "button4";
			this.button4.UseVisualStyleBackColor = false;
			this.button4.MouseDown += new System.Windows.Forms.MouseEventHandler(this.NumberbuttonClick);
			// 
			// button3
			// 
			this.button3.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
			resources.ApplyResources(this.button3, "button3");
			this.button3.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
			this.button3.Name = "button3";
			this.button3.UseVisualStyleBackColor = false;
			this.button3.MouseDown += new System.Windows.Forms.MouseEventHandler(this.NumberbuttonClick);
			// 
			// button2
			// 
			this.button2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
			resources.ApplyResources(this.button2, "button2");
			this.button2.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
			this.button2.Name = "button2";
			this.button2.UseVisualStyleBackColor = false;
			this.button2.MouseDown += new System.Windows.Forms.MouseEventHandler(this.NumberbuttonClick);
			// 
			// button1
			// 
			this.button1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
			resources.ApplyResources(this.button1, "button1");
			this.button1.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
			this.button1.Name = "button1";
			this.button1.UseVisualStyleBackColor = false;
			this.button1.MouseDown += new System.Windows.Forms.MouseEventHandler(this.NumberbuttonClick);
			// 
			// radioButtonUnicode
			// 
			resources.ApplyResources(this.radioButtonUnicode, "radioButtonUnicode");
			this.radioButtonUnicode.ForeColor = System.Drawing.Color.Silver;
			this.radioButtonUnicode.Name = "radioButtonUnicode";
			this.radioButtonUnicode.UseVisualStyleBackColor = true;
			// 
			// radioButtonAscii
			// 
			this.radioButtonAscii.Checked = true;
			resources.ApplyResources(this.radioButtonAscii, "radioButtonAscii");
			this.radioButtonAscii.Name = "radioButtonAscii";
			this.radioButtonAscii.TabStop = true;
			this.radioButtonAscii.UseVisualStyleBackColor = true;
			// 
			// label3
			// 
			resources.ApplyResources(this.label3, "label3");
			this.label3.ForeColor = System.Drawing.Color.Chocolate;
			this.label3.Name = "label3";
			// 
			// textBoxAstring
			// 
			this.textBoxAstring.AcceptsTab = true;
			this.textBoxAstring.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
			resources.ApplyResources(this.textBoxAstring, "textBoxAstring");
			this.textBoxAstring.Name = "textBoxAstring";
			// 
			// buttonAstringOk
			// 
			this.buttonAstringOk.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(100)))), ((int)(((byte)(168)))), ((int)(((byte)(223)))));
			resources.ApplyResources(this.buttonAstringOk, "buttonAstringOk");
			this.buttonAstringOk.ForeColor = System.Drawing.Color.Black;
			this.buttonAstringOk.Name = "buttonAstringOk";
			this.buttonAstringOk.UseVisualStyleBackColor = false;
			this.buttonAstringOk.Click += new System.EventHandler(this.ButtonAstringOkClick);
			// 
			// panel_Astring
			// 
			this.panel_Astring.BackColor = System.Drawing.Color.White;
			this.panel_Astring.Controls.Add(this.groupBox2);
			this.panel_Astring.Controls.Add(this.groupBox1);
			this.panel_Astring.Controls.Add(this.label3);
			this.panel_Astring.Controls.Add(this.buttonAstringOk);
			this.panel_Astring.Controls.Add(this.textBoxAstring);
			resources.ApplyResources(this.panel_Astring, "panel_Astring");
			this.panel_Astring.Name = "panel_Astring";
			// 
			// groupBox2
			// 
			this.groupBox2.Controls.Add(this.radioButtonNormal);
			this.groupBox2.Controls.Add(this.label23);
			this.groupBox2.Controls.Add(this.label24);
			this.groupBox2.Controls.Add(this.radioButtonSecond);
			resources.ApplyResources(this.groupBox2, "groupBox2");
			this.groupBox2.Name = "groupBox2";
			this.groupBox2.TabStop = false;
			// 
			// radioButtonNormal
			// 
			this.radioButtonNormal.Checked = true;
			resources.ApplyResources(this.radioButtonNormal, "radioButtonNormal");
			this.radioButtonNormal.Name = "radioButtonNormal";
			this.radioButtonNormal.TabStop = true;
			this.radioButtonNormal.UseVisualStyleBackColor = true;
			this.radioButtonNormal.Click += new System.EventHandler(this.RadioButtonNormalClick);
			// 
			// label23
			// 
			resources.ApplyResources(this.label23, "label23");
			this.label23.ForeColor = System.Drawing.Color.Green;
			this.label23.Name = "label23";
			// 
			// label24
			// 
			resources.ApplyResources(this.label24, "label24");
			this.label24.ForeColor = System.Drawing.Color.Green;
			this.label24.Name = "label24";
			// 
			// radioButtonSecond
			// 
			resources.ApplyResources(this.radioButtonSecond, "radioButtonSecond");
			this.radioButtonSecond.Name = "radioButtonSecond";
			this.radioButtonSecond.UseVisualStyleBackColor = true;
			this.radioButtonSecond.Click += new System.EventHandler(this.RadioButtonSecondClick);
			// 
			// groupBox1
			// 
			this.groupBox1.Controls.Add(this.radioButtonAscii);
			this.groupBox1.Controls.Add(this.radioButtonUnicode);
			resources.ApplyResources(this.groupBox1, "groupBox1");
			this.groupBox1.Name = "groupBox1";
			this.groupBox1.TabStop = false;
			// 
			// panel1
			// 
			this.panel1.BackColor = System.Drawing.Color.WhiteSmoke;
			this.panel1.Controls.Add(this.panelUserCode);
			this.panel1.Controls.Add(this.panel_Number);
			this.panel1.Controls.Add(this.panel_Astring);
			resources.ApplyResources(this.panel1, "panel1");
			this.panel1.Name = "panel1";
			// 
			// panelUserCode
			// 
			this.panelUserCode.Controls.Add(this.label25);
			this.panelUserCode.Controls.Add(this.textBoxUserCode);
			resources.ApplyResources(this.panelUserCode, "panelUserCode");
			this.panelUserCode.Name = "panelUserCode";
			// 
			// label25
			// 
			resources.ApplyResources(this.label25, "label25");
			this.label25.ForeColor = System.Drawing.Color.Chocolate;
			this.label25.Name = "label25";
			// 
			// textBoxUserCode
			// 
			this.textBoxUserCode.AcceptsTab = true;
			this.textBoxUserCode.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
			resources.ApplyResources(this.textBoxUserCode, "textBoxUserCode");
			this.textBoxUserCode.Name = "textBoxUserCode";
			// 
			// scommonEXPForm
			// 
			resources.ApplyResources(this, "$this");
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.BackColor = System.Drawing.Color.Gainsboro;
			this.Controls.Add(this.panel1);
			this.MaximizeBox = false;
			this.MinimizeBox = false;
			this.Name = "scommonEXPForm";
			this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.ScommonEXPFormFormClosing);
			this.panel_Number.ResumeLayout(false);
			this.panel_Number.PerformLayout();
			this.panel2.ResumeLayout(false);
			this.panel_Astring.ResumeLayout(false);
			this.panel_Astring.PerformLayout();
			this.groupBox2.ResumeLayout(false);
			this.groupBox1.ResumeLayout(false);
			this.panel1.ResumeLayout(false);
			this.panelUserCode.ResumeLayout(false);
			this.panelUserCode.PerformLayout();
			this.ResumeLayout(false);
		}
		private System.Windows.Forms.TextBox textBoxUserCode;
		private System.Windows.Forms.Label label25;
		private System.Windows.Forms.Panel panelUserCode;
		private System.Windows.Forms.RadioButton radioButtonSecond;
		private System.Windows.Forms.Label label24;
		private System.Windows.Forms.Label label23;
		private System.Windows.Forms.RadioButton radioButtonNormal;
		private System.Windows.Forms.GroupBox groupBox2;
		private System.Windows.Forms.Label label21;
		private System.Windows.Forms.Label label22;
		private System.Windows.Forms.Label label18;
		private System.Windows.Forms.Label label19;
		private System.Windows.Forms.Label label20;
		private System.Windows.Forms.Label label17;
		private System.Windows.Forms.Label label14;
		private System.Windows.Forms.Label label15;
		private System.Windows.Forms.Label label16;
		private System.Windows.Forms.Button button35;
		private System.Windows.Forms.Button button39;
		private System.Windows.Forms.Button button40;
		private System.Windows.Forms.Button button47;
		private System.Windows.Forms.Button button48;
		private System.Windows.Forms.Button button49;
		private System.Windows.Forms.Button button50;
		private System.Windows.Forms.Button button51;
		private System.Windows.Forms.Button button52;
		private System.Windows.Forms.Label label5;
		private System.Windows.Forms.Label label6;
		private System.Windows.Forms.Label label7;
		private System.Windows.Forms.Label label8;
		private System.Windows.Forms.Label label12;
		private System.Windows.Forms.Label label11;
		private System.Windows.Forms.Label label10;
		private System.Windows.Forms.Label label9;
		private System.Windows.Forms.Label label13;
		private System.Windows.Forms.Label label4;
		private System.Windows.Forms.Label labelColorHide;
		private System.Windows.Forms.Label labelColor;
		private System.Windows.Forms.Button button43;
		private System.Windows.Forms.Button button42;
		private System.Windows.Forms.Button button41;
		private System.Windows.Forms.Button button46;
		private System.Windows.Forms.Button button45;
		private System.Windows.Forms.Button button44;
		private System.Windows.Forms.Button buttonEditColor;
		private System.Windows.Forms.Button button12;
		private System.Windows.Forms.Button button13;
		private System.Windows.Forms.Button button14;
		private System.Windows.Forms.Button button15;
		private System.Windows.Forms.Button button16;
		private System.Windows.Forms.Button button17;
		private System.Windows.Forms.Button button18;
		private System.Windows.Forms.Button button25;
		private System.Windows.Forms.Button button24;
		private System.Windows.Forms.Button button23;
		private System.Windows.Forms.Button button22;
		private System.Windows.Forms.Button button21;
		private System.Windows.Forms.Button button20;
		private System.Windows.Forms.Button button19;
		private System.Windows.Forms.Button button32;
		private System.Windows.Forms.Button button31;
		private System.Windows.Forms.Button button30;
		private System.Windows.Forms.Button button29;
		private System.Windows.Forms.Button button28;
		private System.Windows.Forms.Button button27;
		private System.Windows.Forms.Button button26;
		private System.Windows.Forms.Button button33;
		private System.Windows.Forms.Button button34;
		private System.Windows.Forms.Button button颜色;
		private System.Windows.Forms.Button button36;
		private System.Windows.Forms.Button button37;
		private System.Windows.Forms.Button button38;
		private System.Windows.Forms.Panel panel2;
		private System.Windows.Forms.Panel panel1;
		private System.Windows.Forms.Button buttonFloat;
		private System.Windows.Forms.TextBox textBoxAstring;
		private System.Windows.Forms.Button buttonAstringOk;
		private System.Windows.Forms.Panel panel_Astring;
		private System.Windows.Forms.GroupBox groupBox1;
		private System.Windows.Forms.Label label3;
		private System.Windows.Forms.RadioButton radioButtonAscii;
		private System.Windows.Forms.RadioButton radioButtonUnicode;
		private System.Windows.Forms.Label label1;
		private System.Windows.Forms.Label label2;
		private System.Windows.Forms.TextBox textBoxNumber;
		private System.Windows.Forms.Panel panel_Number;
		private System.Windows.Forms.Button buttonCE;
		private System.Windows.Forms.Button button1;
		private System.Windows.Forms.Button button2;
		private System.Windows.Forms.Button button3;
		private System.Windows.Forms.Button button4;
		private System.Windows.Forms.Button button5;
		private System.Windows.Forms.Button button6;
		private System.Windows.Forms.Button button7;
		private System.Windows.Forms.Button button8;
		private System.Windows.Forms.Button button9;
		private System.Windows.Forms.Button button10;
		private System.Windows.Forms.Button button11;
		private System.Windows.Forms.Button button确定;
		private System.Windows.Forms.TextBox textBoxResult;
	}
}
