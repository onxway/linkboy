﻿
namespace n_timeEXPForm
{
using System;
using System.Windows.Forms;
using System.IO;

using n_VarType;
using n_UnitList;
using n_FunctionList;
using n_VarList;
using n_StructList;
using n_ImagePanel;
using n_MyObjectList;
using n_MyObject;
using n_HardModule;
using n_GVar;
using n_GUIcoder;
using n_MyFileObject;
using c_FormMover;
using n_EXP;
using n_EXPcommon;

/// <summary>
/// Description of WorkForm.
/// </summary>
public partial class timeEXPForm : Form
{
	bool isOK;
	
	string vvPreType;	
	public string PreType {
		get { return vvPreType; }
		set {
			vvPreType = value;
		}
	}
	
	FormMover fm;
	
	string Result;
	
	//主窗口
	public timeEXPForm()
	{
		InitializeComponent();
		isOK = false;
		
		PreType = "#WORD";
		
		fm = new FormMover( this );
	}
	
	//运行
	public string Run( bool Pre, string Value )
	{
		if( Value != null ) {
			int year = (Value[3] - '0') * 10 + (Value[4] - '0');
			int month = (Value[6] - '0') * 10 + (Value[7] - '0');
			int day = (Value[9] - '0') * 10 + (Value[10] - '0');
			int hour = (Value[12] - '0') * 10 + (Value[13] - '0');
			int minute = (Value[15] - '0') * 10 + (Value[16] - '0');
			int second = (Value[18] - '0') * 10 + (Value[19] - '0');
			
			if( year != 0 ) {
				this.checkBox年.Checked = true;
				try{
				this.numericUpDown年.Value = 2000 + year;
				}catch{}
			}
			else {
				this.checkBox年.Checked = false;
				try{
				this.numericUpDown年.Value = 2000;
				}catch{}
			}
			if( month != 0 ) {
				this.checkBox月.Checked = true;
				try{
				this.numericUpDown月.Value = month;
				}catch{}
			}
			else {
				this.checkBox月.Checked = false;
				try{
				this.numericUpDown月.Value = 1;
				}catch{}
			}
			if( day != 0 ) {
				this.checkBox日.Checked = true;
				try{
				this.numericUpDown日.Value = day;
				}catch{}
			}
			else {
				this.checkBox日.Checked = false;
				try{
				this.numericUpDown日.Value = 1;
				}catch{}
			}
			try{
			this.numericUpDown时.Value = hour;
			}catch{}
			try{
			this.numericUpDown分.Value = minute;
			}catch{}
			try{
			this.numericUpDown秒.Value = second;
			}catch{}
		}
		if( Pre ) {
			PreType = "@WORD \"";
		}
		else {
			PreType = "\"";
		}
		
		if( this.Visible ) {
			return null;
		}
		
		isOK = false;
		
		//注意,直接设置Visible不能使窗体获得焦点
		//this.Visible = true;
		this.ShowDialog();
		
		while( this.Visible ) {
			System.Windows.Forms.Application.DoEvents();
		}
		if( isOK ) {
			return Result;
		}
		return null;
	}
	
	void Button确定Click(object sender, EventArgs e)
	{
		Result = PreType;
		
		Result += this.checkBox年.Checked? this.numericUpDown年.Value.ToString() : "0000";
		Result += ".";
		Result += this.checkBox月.Checked? this.numericUpDown月.Value.ToString().PadLeft( 2, '0' ) : "00";
		Result += ".";
		Result += this.checkBox日.Checked? this.numericUpDown日.Value.ToString().PadLeft( 2, '0' ) : "00";
		Result += " ";
		
		Result += this.numericUpDown时.Value.ToString().PadLeft( 2, '0' );
		Result += ":";
		Result += this.numericUpDown分.Value.ToString().PadLeft( 2, '0' );
		Result += ":";
		Result += this.numericUpDown秒.Value.ToString().PadLeft( 2, '0' );
		
		Result += "\"";
		
		isOK = true;
		this.Visible = false;
	}
	
	void CheckBox年CheckedChanged(object sender, EventArgs e)
	{
		if( this.checkBox年.Checked ) {
			this.numericUpDown年.Enabled = true;
		}
		else {
			this.numericUpDown年.Enabled = false;
		}
	}
	
	void CheckBox月CheckedChanged(object sender, EventArgs e)
	{
		if( this.checkBox月.Checked ) {
			this.numericUpDown月.Enabled = true;
		}
		else {
			this.numericUpDown月.Enabled = false;
		}
	}
	
	void CheckBox日CheckedChanged(object sender, EventArgs e)
	{
		if( this.checkBox日.Checked ) {
			this.numericUpDown日.Enabled = true;
		}
		else {
			this.numericUpDown日.Enabled = false;
		}
	}
	
	void TimeEXPFormFormClosing(object sender, FormClosingEventArgs e)
	{
		e.Cancel = true;
		Visible = false;
	}
}
}

