﻿/*
 * 由 SharpDevelop 创建。
 * 用户: bbb_2
 * 日期: 2009-11-17
 * 时间: 11:15
 * 
 * 如果你想要更改该模板，那么请使用“工具 | 选项 | 编辑 | 编辑标准标题”。
 */
namespace n_timeEXPForm
{
	partial class timeEXPForm
	{
		/// <summary>
		/// Designer variable used to keep track of non-visual components.
		/// </summary>
		private System.ComponentModel.IContainer components = null;
		
		/// <summary>
		/// Disposes resources used by the form.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing) {
				if (components != null) {
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}
		
		/// <summary>
		/// This method is required for Windows Forms designer support.
		/// Do not change the method contents inside the source code editor. The Forms designer might
		/// not be able to load this method if it was changed manually.
		/// </summary>
		private void InitializeComponent()
		{
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(timeEXPForm));
			this.button确定 = new System.Windows.Forms.Button();
			this.label1 = new System.Windows.Forms.Label();
			this.numericUpDown年 = new System.Windows.Forms.NumericUpDown();
			this.numericUpDown月 = new System.Windows.Forms.NumericUpDown();
			this.label2 = new System.Windows.Forms.Label();
			this.numericUpDown日 = new System.Windows.Forms.NumericUpDown();
			this.label3 = new System.Windows.Forms.Label();
			this.numericUpDown分 = new System.Windows.Forms.NumericUpDown();
			this.label4 = new System.Windows.Forms.Label();
			this.numericUpDown时 = new System.Windows.Forms.NumericUpDown();
			this.label5 = new System.Windows.Forms.Label();
			this.numericUpDown秒 = new System.Windows.Forms.NumericUpDown();
			this.label6 = new System.Windows.Forms.Label();
			this.checkBox年 = new System.Windows.Forms.CheckBox();
			this.checkBox月 = new System.Windows.Forms.CheckBox();
			this.checkBox日 = new System.Windows.Forms.CheckBox();
			((System.ComponentModel.ISupportInitialize)(this.numericUpDown年)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.numericUpDown月)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.numericUpDown日)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.numericUpDown分)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.numericUpDown时)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.numericUpDown秒)).BeginInit();
			this.SuspendLayout();
			// 
			// button确定
			// 
			this.button确定.BackColor = System.Drawing.Color.CornflowerBlue;
			resources.ApplyResources(this.button确定, "button确定");
			this.button确定.ForeColor = System.Drawing.Color.White;
			this.button确定.Name = "button确定";
			this.button确定.UseVisualStyleBackColor = false;
			this.button确定.Click += new System.EventHandler(this.Button确定Click);
			// 
			// label1
			// 
			resources.ApplyResources(this.label1, "label1");
			this.label1.Name = "label1";
			// 
			// numericUpDown年
			// 
			resources.ApplyResources(this.numericUpDown年, "numericUpDown年");
			this.numericUpDown年.Maximum = new decimal(new int[] {
									2099,
									0,
									0,
									0});
			this.numericUpDown年.Minimum = new decimal(new int[] {
									2000,
									0,
									0,
									0});
			this.numericUpDown年.Name = "numericUpDown年";
			this.numericUpDown年.Value = new decimal(new int[] {
									2000,
									0,
									0,
									0});
			// 
			// numericUpDown月
			// 
			resources.ApplyResources(this.numericUpDown月, "numericUpDown月");
			this.numericUpDown月.Maximum = new decimal(new int[] {
									12,
									0,
									0,
									0});
			this.numericUpDown月.Minimum = new decimal(new int[] {
									1,
									0,
									0,
									0});
			this.numericUpDown月.Name = "numericUpDown月";
			this.numericUpDown月.Value = new decimal(new int[] {
									1,
									0,
									0,
									0});
			// 
			// label2
			// 
			resources.ApplyResources(this.label2, "label2");
			this.label2.Name = "label2";
			// 
			// numericUpDown日
			// 
			resources.ApplyResources(this.numericUpDown日, "numericUpDown日");
			this.numericUpDown日.Maximum = new decimal(new int[] {
									31,
									0,
									0,
									0});
			this.numericUpDown日.Minimum = new decimal(new int[] {
									1,
									0,
									0,
									0});
			this.numericUpDown日.Name = "numericUpDown日";
			this.numericUpDown日.Value = new decimal(new int[] {
									1,
									0,
									0,
									0});
			// 
			// label3
			// 
			resources.ApplyResources(this.label3, "label3");
			this.label3.Name = "label3";
			// 
			// numericUpDown分
			// 
			resources.ApplyResources(this.numericUpDown分, "numericUpDown分");
			this.numericUpDown分.Maximum = new decimal(new int[] {
									59,
									0,
									0,
									0});
			this.numericUpDown分.Name = "numericUpDown分";
			// 
			// label4
			// 
			resources.ApplyResources(this.label4, "label4");
			this.label4.Name = "label4";
			// 
			// numericUpDown时
			// 
			resources.ApplyResources(this.numericUpDown时, "numericUpDown时");
			this.numericUpDown时.Maximum = new decimal(new int[] {
									23,
									0,
									0,
									0});
			this.numericUpDown时.Name = "numericUpDown时";
			// 
			// label5
			// 
			resources.ApplyResources(this.label5, "label5");
			this.label5.Name = "label5";
			// 
			// numericUpDown秒
			// 
			resources.ApplyResources(this.numericUpDown秒, "numericUpDown秒");
			this.numericUpDown秒.Maximum = new decimal(new int[] {
									59,
									0,
									0,
									0});
			this.numericUpDown秒.Name = "numericUpDown秒";
			// 
			// label6
			// 
			resources.ApplyResources(this.label6, "label6");
			this.label6.Name = "label6";
			// 
			// checkBox年
			// 
			resources.ApplyResources(this.checkBox年, "checkBox年");
			this.checkBox年.Name = "checkBox年";
			this.checkBox年.UseVisualStyleBackColor = true;
			this.checkBox年.CheckedChanged += new System.EventHandler(this.CheckBox年CheckedChanged);
			// 
			// checkBox月
			// 
			resources.ApplyResources(this.checkBox月, "checkBox月");
			this.checkBox月.Name = "checkBox月";
			this.checkBox月.UseVisualStyleBackColor = true;
			this.checkBox月.CheckedChanged += new System.EventHandler(this.CheckBox月CheckedChanged);
			// 
			// checkBox日
			// 
			resources.ApplyResources(this.checkBox日, "checkBox日");
			this.checkBox日.Name = "checkBox日";
			this.checkBox日.UseVisualStyleBackColor = true;
			this.checkBox日.CheckedChanged += new System.EventHandler(this.CheckBox日CheckedChanged);
			// 
			// timeEXPForm
			// 
			resources.ApplyResources(this, "$this");
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.BackColor = System.Drawing.Color.WhiteSmoke;
			this.Controls.Add(this.numericUpDown秒);
			this.Controls.Add(this.label6);
			this.Controls.Add(this.numericUpDown分);
			this.Controls.Add(this.label4);
			this.Controls.Add(this.numericUpDown时);
			this.Controls.Add(this.label5);
			this.Controls.Add(this.numericUpDown日);
			this.Controls.Add(this.label3);
			this.Controls.Add(this.numericUpDown月);
			this.Controls.Add(this.label2);
			this.Controls.Add(this.numericUpDown年);
			this.Controls.Add(this.label1);
			this.Controls.Add(this.button确定);
			this.Controls.Add(this.checkBox日);
			this.Controls.Add(this.checkBox月);
			this.Controls.Add(this.checkBox年);
			this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
			this.MaximizeBox = false;
			this.MinimizeBox = false;
			this.Name = "timeEXPForm";
			this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.TimeEXPFormFormClosing);
			((System.ComponentModel.ISupportInitialize)(this.numericUpDown年)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.numericUpDown月)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.numericUpDown日)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.numericUpDown分)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.numericUpDown时)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.numericUpDown秒)).EndInit();
			this.ResumeLayout(false);
		}
		private System.Windows.Forms.CheckBox checkBox日;
		private System.Windows.Forms.CheckBox checkBox年;
		private System.Windows.Forms.CheckBox checkBox月;
		private System.Windows.Forms.NumericUpDown numericUpDown秒;
		private System.Windows.Forms.NumericUpDown numericUpDown时;
		private System.Windows.Forms.NumericUpDown numericUpDown分;
		private System.Windows.Forms.NumericUpDown numericUpDown日;
		private System.Windows.Forms.NumericUpDown numericUpDown月;
		private System.Windows.Forms.NumericUpDown numericUpDown年;
		private System.Windows.Forms.Label label6;
		private System.Windows.Forms.Label label5;
		private System.Windows.Forms.Label label4;
		private System.Windows.Forms.Label label3;
		private System.Windows.Forms.Label label2;
		private System.Windows.Forms.Label label1;
		private System.Windows.Forms.Button button确定;
	}
}
