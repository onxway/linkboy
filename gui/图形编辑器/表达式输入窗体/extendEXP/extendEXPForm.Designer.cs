﻿/*
 * 由 SharpDevelop 创建。
 * 用户: bbb_2
 * 日期: 2009-11-17
 * 时间: 11:15
 * 
 * 如果你想要更改该模板，那么请使用“工具 | 选项 | 编辑 | 编辑标准标题”。
 */
namespace n_extendEXPForm
{
	partial class extendEXPForm
	{
		/// <summary>
		/// Designer variable used to keep track of non-visual components.
		/// </summary>
		private System.ComponentModel.IContainer components = null;
		
		/// <summary>
		/// Disposes resources used by the form.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing) {
				if (components != null) {
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}
		
		/// <summary>
		/// This method is required for Windows Forms designer support.
		/// Do not change the method contents inside the source code editor. The Forms designer might
		/// not be able to load this method if it was changed manually.
		/// </summary>
		private void InitializeComponent()
		{
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(extendEXPForm));
			this.listBox组件列表 = new System.Windows.Forms.ListBox();
			this.label1 = new System.Windows.Forms.Label();
			this.label2 = new System.Windows.Forms.Label();
			this.SuspendLayout();
			// 
			// listBox组件列表
			// 
			this.listBox组件列表.BackColor = System.Drawing.Color.WhiteSmoke;
			this.listBox组件列表.BorderStyle = System.Windows.Forms.BorderStyle.None;
			resources.ApplyResources(this.listBox组件列表, "listBox组件列表");
			this.listBox组件列表.FormattingEnabled = true;
			this.listBox组件列表.Name = "listBox组件列表";
			this.listBox组件列表.Click += new System.EventHandler(this.ListBox组件列表Click);
			// 
			// label1
			// 
			this.label1.BackColor = System.Drawing.Color.CornflowerBlue;
			resources.ApplyResources(this.label1, "label1");
			this.label1.ForeColor = System.Drawing.Color.SeaShell;
			this.label1.Name = "label1";
			// 
			// label2
			// 
			this.label2.BackColor = System.Drawing.Color.WhiteSmoke;
			resources.ApplyResources(this.label2, "label2");
			this.label2.ForeColor = System.Drawing.Color.SeaShell;
			this.label2.Name = "label2";
			// 
			// extendEXPForm
			// 
			resources.ApplyResources(this, "$this");
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.BackColor = System.Drawing.Color.WhiteSmoke;
			this.Controls.Add(this.listBox组件列表);
			this.Controls.Add(this.label2);
			this.Controls.Add(this.label1);
			this.MaximizeBox = false;
			this.MinimizeBox = false;
			this.Name = "extendEXPForm";
			this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.ExtendEXPFormFormClosing);
			this.ResumeLayout(false);
		}
		private System.Windows.Forms.Label label2;
		private System.Windows.Forms.Label label1;
		private System.Windows.Forms.ListBox listBox组件列表;
	}
}
