﻿
namespace n_extendEXPForm
{
using System;
using System.Windows.Forms;
using System.IO;

using n_VarType;
using n_UnitList;
using n_FunctionList;
using n_VarList;
using n_StructList;
using n_ImagePanel;
using n_MyObjectList;
using n_MyObject;
using n_HardModule;
using n_GVar;
using n_GUIcoder;
using n_MyFileObject;
using c_FormMover;
using n_EXP;
using n_EXPcommon;
using System.Collections.Generic;

/// <summary>
/// Description of WorkForm.
/// </summary>
public partial class extendEXPForm : Form
{
	bool isOK;
	string NeedType;
	string Result;
	
	List<string> InnerNameList;
	List<string> ExpTypeList;
	List<string> VarTypeList;
	
	FormMover fm;
	
	//主窗口
	public extendEXPForm()
	{
		InitializeComponent();
		
		isOK = false;
		NeedType = null;
		Result = null;
		
		ExpTypeList = new List<string>();
		InnerNameList = new List<string>();
		VarTypeList = new List<string>();
		fm = new FormMover( this );
	}
	
	//运行
	public string Run( string vNeedType )
	{
		NeedType = vNeedType;
		
		if( this.Visible ) {
			return null;
		}
		//刷新组件列表
		RefreshModuleList();
		
		//注意,直接设置Visible不能使窗体获得焦点
		//this.Visible = true;
		this.ShowDialog();
		
		//while( this.Visible ) {
		//	Application.DoEvents();
		//}
		
		if( isOK ) {
			return Result;
		}
		return null;
	}
	
	void RefreshModuleList()
	{
		listBox组件列表.Items.Clear();
		
		ExpTypeList.Clear();
		InnerNameList.Clear();
		
		//添加用户自定义的变量列表
		string[] varList = EXPMember.GetVarlist( true, null, null, NeedType, VarTypeList, InnerNameList, ExpTypeList, null );
		if( varList != null ) {
			listBox组件列表.Items.AddRange( varList );
		}
		
		if( NeedType == "?" + InterfaceType.Vtype ) {
			this.listBox组件列表.Items.Add( "(默认位置)" );
			ExpTypeList.Add( "这个没用到" );
			//varList = new string[] {"这个没用到"};
		}
		InnerNameList.Clear();
		
		//添加各个模块的变量列表
		foreach( n_MyObject.MyObject mo in G.CGPanel.myModuleList ) {
			if( !( mo is MyFileObject ) ) {
				continue;
			}
			string UnitName = mo.Name;
			
			//这里应该是添加遍历循环语句, 适用于上位机精灵类
			if( NeedType == EXP.ENote.v_Module && ((MyFileObject)mo).ImageName == SPMoudleName.SYS_Sprite ) {
				this.listBox组件列表.Items.Add( UnitName );
				ExpTypeList.Add( "这个没用到" );
				continue;
			}
			
			string[] unitList = EXPMember.GetModuleMember( NeedType, UnitName, VarTypeList, InnerNameList, ExpTypeList, true, false, false );
			if( unitList != null ) {
				this.listBox组件列表.Items.AddRange( unitList );
			}
		}
		
		listBox组件列表.SelectedIndex = -1;
		
//		string s = "";
//		for( int i = 0; i < ExpTypeList.Count; ++i ) {
//			s += ExpTypeList[i] + "\n";
//		}
//		MessageBox.Show( s );
	}
	
	void ListBox组件列表Click(object sender, EventArgs e)
	{
		if( this.listBox组件列表.SelectedIndex == -1 ) {
			return;
		}
		string PreType = ExpTypeList[this.listBox组件列表.SelectedIndex];
		string Name = this.listBox组件列表.SelectedItem.ToString();
		
		if( Name.IndexOf( ' ' ) != -1 ) {
			//Name = Name.Remove( 0, Name.IndexOf( ' ' ) + 1 );
			Result = PreType + " " + Name;
		}
		else {
			Result = EXP.ENote.t_WORD + " " + Name;
		}
		isOK = true;
		this.Visible = false;
	}
	
	void ExtendEXPFormFormClosing(object sender, FormClosingEventArgs e)
	{
		//e.Cancel = true;
		//isOK = false;
		this.Visible = false;
	}
}
}

