﻿
namespace n_EXPcommon
{
using System;
using System.Windows.Forms;
using System.IO;
using System.Collections.Generic;

using n_VarType;
using n_UnitList;
using n_FunctionList;
using n_VarList;
using n_StructList;
using n_ImagePanel;
using n_MyObjectList;
using n_MyObject;
using n_HardModule;
using n_GVar;
using n_GUIcoder;
using n_MyFileObject;
using c_FormMover;
using n_EXP;
using n_UserFunctionIns;
using n_MyIns;
using n_EventIns;

//表达式元素集合类
static class EXPMember
{
	public const string ErrorConst = "(无效)";
	
	public const string GMesHead = ":";
	
	//初始化
	public static void Init()
	{
		//...
	}
	
	//根据用户指令成员列表,用回车分隔
	public static string[] GetUserInsMember( n_GroupList.Group gr, string InsGroupMes, string NeedType, List<string> InnerNameList, List<string> TypeList, bool isSingle, bool AddErrorItem )
	{
		string[] eList = new string[100];
		int Length = 0;
		
		//扫描用户图形界面定义的函数
		foreach( MyObject mo in G.CGPanel.myModuleList ) {
			if( mo is UserFunctionIns ) {
				
				if( gr != null && gr.GMes != null && mo.GroupMes != gr.GMes || gr == null && mo.GroupMes != null ) {
					continue;
				}
				UserFunctionIns m = (UserFunctionIns)mo;
				
				if( gr != null && InsGroupMes != gr.GMes && gr.MM != null ) {
					
					MemberMes mmm = gr.MM.GetMemberMessageFromName( m.GetFuncName() );
					if( mmm != null && mmm.Type != n_MemberType.MemberType.VisitType.Public ) {
						continue;
					}
				}
				TypeList.Add( EXP.ENote.t_USER );
				InnerNameList.Add( m.GetFuncName() );
				string UserType = m.GetUserType();
				string Error = "";
				if( "?" + UserType != NeedType ) {
					Error = ErrorConst;
				}
				eList[ Length ] = Error + m.GetFuncCall();
				++Length;
			}
		}
		/*
		//扫描用户代码里的函数列表
		foreach( MyObject mo in G.CGPanel.myModuleList ) {
			if( !(mo is n_GNote.GNote) ) {
				continue;
			}
			string[] Line = ((n_GNote.GNote)mo).Value.Split( '\n' );
			for( int i = 0; i < Line.Length; ++i ) {
				
				string[] word = n_CodeFormat.CodeFormat.GetFunc( Line[i] );
				
				if( word.Length < 3 || word[0].StartsWith( "//" ) ) {
					continue;
				}
				if( word[0] != "void" && word[0] != "bool" && word[0] != "int32" ) {
					continue;
				}
				string Note = "//请在函数定义<" + word[1] + ">的前一行添加注释, 会显示到这里";
				if( i > 0 ) {
					int ni = Line[i - 1].IndexOf( "//" );
					if( ni != -1 ) {
						Note = Line[i - 1].Remove( 0, ni );
					}
				}
				TypeList.Add( EXP.ENote.t_CODE );
				InnerNameList.Add( GMesHead + Note );
				string UserType = word[0];
				string Error = "";
				if( "?" + UserType != NeedType ) {
					Error = ErrorConst;
				}
				//eList[ Length ] = Error + w[1] + "my_function123 ( ?int32 ) 哈哈 ( ?bool ) dfg";
				string rrr = Error + word[1];
				for( int j = 3; j < word.Length - 2; j += 3 ) {
					
					if( word[j] == "int32" ) {
						rrr += " ( ?" + word[j] + " )";
					}
					else if( word[j] == "bool" ) {
						rrr += " ( ?" + word[j] + " )";
					}
					else {
						MessageBox.Show( "函数参数类型只支持 bool 和 int32 2种: " + word[j] );
					}
				}
				eList[ Length ] = rrr;
				++Length;
			}
		}
		*/
		
		//合并结果
		if( Length == 0 ) {
			return null;
		}
		string[] result = new string[ Length ];
		for( int n = 0; n < result.Length; ++n ) {
			result[ n ] = eList[ n ];
		}
		return result;
	}
	
	//根据用户指令成员列表,用回车分隔
	public static string[] GetUserCodeMember( string GName, string NeedType, List<string> InnerNameList, List<string> TypeList, List<string> VarTypeList )
	{
		string[] eList = new string[100];
		int Length = 0;
		
		//扫描用户代码里的函数列表
		foreach( MyObject mo in G.CGPanel.myModuleList ) {
			if( !(mo is n_GNote.GNote) ) {
				continue;
			}
			n_GNote.GNote gn = (n_GNote.GNote)mo;
			if( gn.Name != GName ) {
				continue;
			}
			string ucl = null;
			string code = gn.GetCxCode( ref ucl );
			string[] Line = code.Split( '\n' );
			
			for( int i = 0; i < Line.Length; ++i ) {
				
				string[] word = n_CodeFormat.CodeFormat.GetFunc( Line[i] );
				
				if( word.Length < 3 || word[0].StartsWith( "//" ) ) {
					continue;
				}
				if( word[0] != "const" && word[0] != "void" && word[0] != "bool" && word[0] != "int32" ) {
					continue;
				}
				
				string Note = "//请在变量/函数定义<" + Line[i] + ">的前一行添加注释, 会显示到这里";
				if( i > 0 ) {
					int ti = i - 1;
					if( Line[ti].IndexOf( "$SIM_NOTE$" ) != -1 ) {
						ti--;
					}
					if( ti >= 0 ) {
						int ni = Line[ti].IndexOf( "//" );
						if( ni != -1 ) {
							Note = Line[ti].Remove( 0, ni );
						}
					}
				}
				
				//这里说明是变量和常量
				if( Line[i].IndexOf( "(" )  == -1 || Line[i].IndexOf( ")" ) == -1 ) {
					
					bool isCosnt = word[0] == "const";
					string UType = word[0];
					string UName = word[1];
					if( isCosnt ) {
						UType = word[1];
						UName = word[2];
					}
					else {
						if( word[0] == "void" ) {
							continue;
						}
					}
					string Error0 = "";
					if( "?" + UType != NeedType ) {
						Error0 = ErrorConst;
					}
					eList[ Length ] = Error0 + UName;
					TypeList.Add( EXP.ENote.t_VAR );
					VarTypeList.Add( "?" + UType );
					InnerNameList.Add( Note );
					
					Length++;
					continue;
				}
				if( word[0] == "const" ) {
					continue;
				}
				
				TypeList.Add( EXP.ENote.t_CODE );
				InnerNameList.Add( GMesHead + Note );
				string UserType = word[0];
				string Error = "";
				if( "?" + UserType != NeedType ) {
					Error = ErrorConst;
				}
				//eList[ Length ] = Error + w[1] + "my_function123 ( ?int32 ) 哈哈 ( ?bool ) dfg";
				string rrr = Error + word[1];
				for( int j = 3; j < word.Length - 2; j += 3 ) {
					
					if( word[j] == ")" ) {
						break;
					}
					else if( word[j] == "int32" ) {
						rrr += " ( ?" + word[j] + " )";
					}
					else if( word[j] == "bool" ) {
						rrr += " ( ?" + word[j] + " )";
					}
					else if( word[j] == "string" ) {
						rrr += " ( ?" + GType.g_Cstring + " )";
					}
					else if( word[j] == "void" ) {
						break;
					}
					else {
						MessageBox.Show( "函数参数类型目前支持 bool, int32, string, 暂不支持: " + word[j] );
					}
					if( word[j+2] == ")" ) {
						break;
					}
				}
				eList[ Length ] = rrr;
				++Length;
			}
		}
		//合并结果
		if( Length == 0 ) {
			return null;
		}
		string[] result = new string[ Length ];
		for( int n = 0; n < result.Length; ++n ) {
			result[ n ] = eList[ n ];
		}
		return result;
	}
	
	//根据组件名获取组件对象
	public static MyFileObject GetModule( string ModuleName )
	{
		foreach( MyObject mo in G.CGPanel.myModuleList ) {
			if( !( mo is MyFileObject ) ) {
				continue;
			}
			MyFileObject m = (MyFileObject)mo;
			if( m.Name != ModuleName ) {
				continue;
			}
			return m;
		}
		return null;
	}
	
	//根据组件名获取组件成员列表,用回车分隔
	public static string[] GetModuleMember( string NeedType, string ModuleName, List<string> VarTypeList, List<string> InnerNameList, List<string> TypeList, bool isSingle, bool AddErrorItem, bool AddLine )
	{
		bool ExistFunc = false;
		
		foreach( MyObject mo in G.CGPanel.myModuleList ) {
			if( !( mo is MyFileObject ) ) {
				continue;
			}
			MyFileObject m = (MyFileObject)mo;
			if( m.Name != ModuleName ) {
				continue;
			}
			int Length = 0;
			string[] eList = new string[ m.ElementList.Length + 128 ];
			
			for( int n = 0; n < m.ElementList.Length; ++n ) {
				
				string Error = "";
				
				//扫描分割线
				if( AddLine && m.ElementList[ n ][ 1 ].StartsWith( InterfaceType.Split ) ) {
					
					//添加分隔线
					TypeList.Add( EXP.ENote.t_SPLIT );
					eList[ Length ] = "-";
					InnerNameList.Add( m.ElementList[ n ][ 2 ] );
					VarTypeList.Add( "未用到" );
					++Length;
				}
				//扫描存储类型成员
				if( m.ElementList[ n ][ 1 ]== InterfaceType.Vtype && !m.ElementList[ n ][ 2 ].StartsWith( "OS_" ) ) {
					
					if( "?" + InterfaceType.Vtype != NeedType ) {
						Error = ErrorConst;
						if( !AddErrorItem ) {
							continue;
						}
					}
					if( isSingle ) {
						eList[ Length ] = ModuleName + " " + Error + m.ElementList[ n ][ m.GetLC() ];
						TypeList.Add( EXP.ENote.t_MVAR );
					}
					else {
						eList[ Length ] = Error + m.ElementList[ n ][ m.GetLC() ];
						TypeList.Add( EXP.ENote.t_MVAR + " " + ModuleName );
					}
					VarTypeList.Add( "?" + InterfaceType.Vtype );
					InnerNameList.Add( m.ElementList[ n ][ 2 ] );
					++Length;
				}
				//扫描函数成员
				if( m.ElementList[ n ][ 1 ].StartsWith( InterfaceType.Function_ ) && !m.ElementList[ n ][ 2 ].StartsWith( "OS_" ) ) {
					
					string[] tempTypeList = m.ElementList[ n ][ 1 ].Split( '_' );
					if( tempTypeList.Length >= 3 && tempTypeList[2] == FuncExtType.Old ) {
						continue;
					}
					bool IntToFix = ("?" + tempTypeList[1] == EXP.ENote.v_int32 || "?" + tempTypeList[1] == EXP.ENote.v_fix) && NeedType == EXP.ENote.v_fix;
					if( ("?" + tempTypeList[1] != NeedType && NeedType != EXP.ENote.v_bool) && !IntToFix ||
					    "?" + tempTypeList[1] != EXP.ENote.v_fix && "?" + tempTypeList[1] != EXP.ENote.v_int32 && "?" + tempTypeList[1] != EXP.ENote.v_bool && NeedType == EXP.ENote.v_bool ) {
						Error = ErrorConst;
						if( !AddErrorItem ) {
							continue;
						}
					}
					
					if( !ExistFunc && Length != 0 && AddErrorItem ) {
						//添加分隔线
						TypeList.Add( EXP.ENote.t_SPLIT );
						eList[ Length ] = "-";
						InnerNameList.Add( "-" );
						VarTypeList.Add( "未用到" );
						++Length;
					}
					ExistFunc = true;
					
					string[] temp = m.ElementList[ n ][ m.GetLC() ].Split( '#' );
					
					string R = "";
					if( isSingle ) {
						R = ModuleName + " ";
						TypeList.Add( EXP.ENote.t_MFUNC );
					}
					else {
						TypeList.Add( EXP.ENote.t_MFUNC + " " + ModuleName );
					}
					
					R += Error;
					for( int ii = 0; ii < temp.Length - 1; ++ii ) {
						try {
						if( tempTypeList[ii + 2][0] == '*' ) {
							R += temp[ii] + "( " + tempTypeList[ii + 2] + " )";
						}
						else {
							R += temp[ii] + "( ?" + tempTypeList[ii + 2] + " )";
						}
						} catch {
							MessageBox.Show( "ERROR <GetModuleMember> " + eList[ Length ] + ":" + temp.Length + "," + ii + " " + tempTypeList.Length );
						}
					}
					R += temp[ temp.Length - 1 ];
					
					eList[ Length ] = R;
					InnerNameList.Add( m.ElementList[ n ][ 2 ] );
					VarTypeList.Add( "?" + tempTypeList[1] );
					++Length;
				}
				//扫描变量成员
				if( (m.ElementList[ n ][ 1 ].StartsWith( InterfaceType.Var_ ) || m.ElementList[ n ][ 1 ].StartsWith( InterfaceType.LinkVar_ ))
				   		&& !m.ElementList[ n ][ 2 ].StartsWith( "OS_" ) ) {
					
					string[] tempTypeList = m.ElementList[ n ][ 1 ].Split( '_' );
					
					if( tempTypeList.Length >= 3 && tempTypeList[2] == FuncExtType.Old ) {
						continue;
					}
					
					bool IntToFix = ("?" + tempTypeList[1] == EXP.ENote.v_int32 || "?" + tempTypeList[1] == EXP.ENote.v_fix) && NeedType == EXP.ENote.v_fix;
					if( ("?" + tempTypeList[1] != NeedType && "*" + tempTypeList[1] != NeedType) && NeedType != EXP.ENote.v_void && NeedType != EXP.ENote.v_bool && !IntToFix ||
					    
					   //这里进行递归运算判断, 需求bool类型时, 允许int, bool, 和 Cstring
					   "?" + tempTypeList[1] != EXP.ENote.v_Cstring && "?" + tempTypeList[1] != EXP.ENote.v_int32 && "?" + tempTypeList[1] != EXP.ENote.v_fix && "?" + tempTypeList[1] != EXP.ENote.v_bool && NeedType == EXP.ENote.v_bool ) {
						Error = ErrorConst;
						if( !AddErrorItem ) {
							continue;
						}
					}
					if( (NeedType == "*" + tempTypeList[1] || NeedType == EXP.ENote.v_void) && (tempTypeList.Length < 3 || tempTypeList[ 2 ] != "w") ) {
						Error = ErrorConst;
						if( !AddErrorItem ) {
							continue;
						}
					}
					if( isSingle ) {
						eList[ Length ] = ModuleName + " " + Error + m.ElementList[ n ][ m.GetLC() ];
						TypeList.Add( EXP.ENote.t_MVAR );
					}
					else {
						eList[ Length ] = Error + m.ElementList[ n ][ m.GetLC() ];
						TypeList.Add( EXP.ENote.t_MVAR + " " + ModuleName );
					}
					InnerNameList.Add( m.ElementList[ n ][ 2 ] );
					VarTypeList.Add( "?" + tempTypeList[1] );
					++Length;
				}
				//扫描常量成员
				if( m.ElementList[ n ][ 1 ].StartsWith( InterfaceType.Const_ ) && !m.ElementList[ n ][ 2 ].StartsWith( "OS_" ) ) {
					
					string[] tempTypeList = m.ElementList[ n ][ 1 ].Split( '_' );
					if( "?" + tempTypeList[1] != NeedType && NeedType != EXP.ENote.v_bool ||
					   ( "?" + tempTypeList[1] != EXP.ENote.v_int32 && "?" + tempTypeList[1] != EXP.ENote.v_fix && NeedType == EXP.ENote.v_fix ) ||
					   "?" + tempTypeList[1] != EXP.ENote.v_int32 && "?" + tempTypeList[1] != EXP.ENote.v_fix && "?" + tempTypeList[1] != EXP.ENote.v_bool && NeedType == EXP.ENote.v_bool  ) {
						Error = ErrorConst;
						if( !AddErrorItem ) {
							continue;
						}
					}
					if( isSingle ) {
						eList[ Length ] = ModuleName + " " + Error + m.ElementList[ n ][ m.GetLC() ];
						TypeList.Add( EXP.ENote.t_MVAR );
					}
					else {
						eList[ Length ] = Error + m.ElementList[ n ][ m.GetLC() ];
						TypeList.Add( EXP.ENote.t_MVAR + " " + ModuleName );
					}
					InnerNameList.Add( m.ElementList[ n ][ 2 ] );
					VarTypeList.Add( "?" + tempTypeList[1] );
					++Length;
				}
			}
			//添加分隔线
			if( AddErrorItem ) {
				TypeList.Add( EXP.ENote.t_SPLIT );
				eList[ Length ] = "-";
				InnerNameList.Add( "-" );
				VarTypeList.Add( "未用到" );
				++Length;
			}
			
			//添加事件指令
			foreach( MyObject m1 in G.CGPanel.myModuleList ) {
				if( m1 is EventIns ) {
					EventIns em = (EventIns)m1;
					
					if( em.owner != m ) {
						continue;
					}
					
					string UserType = "void";
					string Error = "";
					if( "?" + UserType != NeedType ) {
						Error = ErrorConst;
						if( !AddErrorItem ) {
							continue;
						}
					}
					
					/*
					//添加事件手工调用
					TypeList.Add( EXP.ENote.t_USER );
					eList[ Length ] = Error + em.EventEntry;
					InnerNameList.Add( GMesHead + "把事件作为指令来使用的话, 相当于手工触发执行一次事件" );
					VarTypeList.Add( "未用到" );
					++Length;
					*/
					
					TypeList.Add( EXP.ENote.t_SYS );
					eList[ Length ] = Error + "触发 " + em.EventEntry;
					InnerNameList.Add( GMesHead + "让指定的事件触发执行, 如果触发的是其他事件, 则当前事件继续向下执行; 如果触发的是自身的话, 则立即回到事件的开始处执行, 触发指令后边的所有指令将会忽略, 永远不会执行" );
					VarTypeList.Add( "未用到" );
					++Length;
					
					TypeList.Add( EXP.ENote.t_SYS );
					eList[ Length ] = Error + "结束 " + em.EventEntry;
					InnerNameList.Add( GMesHead + "结束指定事件是指立刻让指定事件停止运行， 例如事件处于延时、等待、或者反复执行的时候，可以用这个指令强制结束事件, 如果事件本来就处于结束状态则无效果" );
					VarTypeList.Add( "未用到" );
					++Length;
					
					//添加事件开关
					TypeList.Add( EXP.ENote.t_SYS );
					eList[ Length ] = Error + "启用 " + em.EventEntry;
					InnerNameList.Add( GMesHead + "如果启用指定事件，则允许后台对事件进行检测和调度. (注意:开机默认情况下所有的事件都是在启用状态)" );
					VarTypeList.Add( "未用到" );
					++Length;
					
					TypeList.Add( EXP.ENote.t_SYS );
					eList[ Length ] = Error + "禁用 " + em.EventEntry;
					InnerNameList.Add( GMesHead + "如果禁用指定事件，则禁止后台对事件进行检测和调度. 即使事件条件成立也不会执行其程序 (注意:如果指定的事件正在执行中, 那么会继续执行完才禁用)" );
					VarTypeList.Add( "未用到" );
					++Length;
					
					TypeList.Add( EXP.ENote.t_SPLIT );
					eList[ Length ] = "-";
					InnerNameList.Add( "-" );
					VarTypeList.Add( "未用到" );
					++Length;
				}
			}
			//格式化输出结果
			string[] result = new string[ Length ];
			for( int n = 0; n < result.Length; ++n ) {
				result[ n ] = eList[ n ].Replace( '+', ' ' );
			}
			return result;
		}
		return null;
	}
	
	//获取全局变量列表
	public static string[] GetVarlist( bool isExt, n_GroupList.Group gr, string InsGroupMes, string NeedType, List<string> VarTypeList, List<string> InnerNameList, List<string> TypeList, MyObject o )
	{
		string s = null;
		
		//添加所在函数的形参名称
		if( gr == null ) {
			if( o != null ) {
				MyIns mi = (MyIns)o;
				mi = mi.GetHeadIns();
				if( mi is n_UserFunctionIns.UserFunctionIns ) {
					string[] c = ((n_UserFunctionIns.UserFunctionIns)mi).GetVarList( NeedType, TypeList, VarTypeList );
					for( int n = 0; c != null && n < c.Length; ++n ) {
						s += c[n] + ";";
						InnerNameList.Add( c[n] );
					}
				}
			}
			//添加分隔线
			if( s != null ) {
				TypeList.Add( EXP.ENote.t_SPLIT );
				s += "-;";
				InnerNameList.Add( "-" );
				VarTypeList.Add( "未用到" );
			}
		}
		
		foreach( MyObject m in G.CGPanel.myModuleList ) {
			
			
			
			//MessageBox.Show( m.Name );
			//if( gr != null && gr.Name != null ) {
			//	MessageBox.Show( "A: " + m.Name + ":" + gr.Name + ":" + m.GroupMes );
			//}
			
			
			
			if( gr != null && gr.GMes != null && m.GroupMes != gr.GMes || !isExt && gr == null && m.GroupMes != null ) {
				continue;
			}
			
			if( !( m is GVar ) ) {
				continue;
			}
			GVar gv = (GVar)m;
			
			
			if( gr != null && InsGroupMes != gr.GMes && gr.MM != null && gr.MM.GetMemberMessageFromName( gv.Name ).Type != n_MemberType.MemberType.VisitType.Public ) {
				continue;
			}
			
			if( "?" + gv.VarType == NeedType ||
			    "*" + gv.VarType == NeedType ||
			    (NeedType == EXP.ENote.v_fix && "?" + gv.VarType == EXP.ENote.v_int32) ||
			    (NeedType == EXP.ENote.v_int32 && "?" + gv.VarType == EXP.ENote.v_fix) ||
			    (NeedType == EXP.ENote.v_void && ("?" + gv.VarType == EXP.ENote.v_bool || "?" + gv.VarType == EXP.ENote.v_int32 || "?" + gv.VarType == EXP.ENote.v_fix) ) ||
			    (NeedType == EXP.ENote.v_void && ("*" + gv.VarType == EXP.ENote.v_bool || "*" + gv.VarType == EXP.ENote.v_int32 || "*" + gv.VarType == EXP.ENote.v_fix) ) ||
			    NeedType == EXP.ENote.v_bool && ("?" + gv.VarType == EXP.ENote.v_int32 || "?" + gv.VarType == EXP.ENote.v_fix)
			    //NeedType == EXP.ENote.v_bool && ("*" + gv.VarType == EXP.ENote.v_int32 || "*" + gv.VarType == EXP.ENote.v_fix) || //这里好像永远执行不到
				 ) {
				
				//这里拦截图片常量属性
				if( NeedType == EXP.ENote.v_m_bitmap && "?" + gv.VarType == EXP.ENote.v_bitmap && gv.Value != "" ) {
					continue;
				}
				
				TypeList.Add( EXP.ENote.t_VAR );
				VarTypeList.Add( "?" + gv.VarType );
				InnerNameList.Add( gv.Name );
				s += m.Name + ";";
			}
		}
		if( s == null ) {
			return null;
		}
		return s.TrimEnd( ';' ).Split( ';' );
	}
}
}

