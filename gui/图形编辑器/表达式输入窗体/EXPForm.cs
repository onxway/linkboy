﻿
namespace n_EXPForm
{
using System;
using System.Windows.Forms;
using System.IO;

using n_VarType;
using n_UnitList;
using n_FunctionList;
using n_VarList;
using n_StructList;
using n_ImagePanel;
using n_MyObjectList;
using n_MyObject;
using n_HardModule;
using n_GVar;
using n_GUIcoder;
using n_MyFileObject;
using c_FormMover;
using n_EXP;
using n_EXPcommon;

/// <summary>
/// Description of WorkForm.
/// </summary>
public partial class EXPForm : Form
{
	bool isOK;
	
	string[] ExpTypeList;
	string vvPreType;	
	public string PreType {
		get { return vvPreType; }
		set {
			vvPreType = value;
			this.labelExpType.Text = vvPreType;
		}
	}
	
	FormMover fm;
	
	//主窗口
	public EXPForm()
	{
		InitializeComponent();
		EXPMember.Init();
		isOK = false;
		
		PreType = "#WORD";
		ExpTypeList = new string[ 50 ];
		
		fm = new FormMover( this );
	}
	
	//运行
	public string Run()
	{
		PreType = "@WORD";
		
		if( this.Visible ) {
			return null;
		}
		//刷新组件列表
		RefreshModuleList();
		
		this.textBox1.Text = "";
		
		//注意,直接设置Visible不能使窗体获得焦点
		//this.Visible = true;
		this.ShowDialog();
		
		while( this.Visible ) {
			System.Windows.Forms.Application.DoEvents();
		}
		if( isOK ) {
			string rr = this.textBox1.Text.Trim( "\t ".ToCharArray() );
			if( rr == "" ) {
				return "";
			}
			string r = PreType + " " + rr;
			return r;
			//return r.Replace( "#", "( ? )" );
		}
		return null;
	}
	
	//刷新组件列表
	void RefreshModuleList()
	{
		this.listBox组件列表.Items.Clear();
		
		this.listBox组件列表.Items.Add( "[变量]" );
		
		ImagePanel TargetGPanel = A.CGPanel;
		
		//注意这里是要保持组件为选中状态, 防止窗体关闭后组件取消高亮造成的闪烁
		//TargetGPanel.SelPanel.SelectState = 3;
		
		MyObjectList myList = TargetGPanel.myModuleList;
		MyObject[] ModuleList = myList.ModuleList;
		for( int i = 0; i < ModuleList.Length; ++i ) {
			if( ModuleList[ i ] == null ) {
				continue;
			}
			if( !( ModuleList[ i ] is MyFileObject ) ) {
				continue;
			}
			this.listBox组件列表.Items.Add( ModuleList[ i ].Name );
		}
		listBox组件列表.SelectedIndex = -1;
		this.listBox成员列表.Items.Clear();
	}
	
	void Button确定Click(object sender, EventArgs e)
	{
		this.textBox1.Focus();
		isOK = true;
		this.Visible = false;
	}
	
	void Button取消Click(object sender, EventArgs e)
	{
		this.textBox1.Focus();
		isOK = false;
		this.Visible = false;
	}
	
	void ListBox成员列表Click(object sender, EventArgs e)
	{
		if( listBox成员列表.SelectedIndex == -1 ) {
			return;
		}
		string Name = listBox成员列表.SelectedItem.ToString();
		if( PreType != "@WORD" ) {
			PreType = ExpTypeList[ listBox成员列表.SelectedIndex ];
		}
		this.textBox1.Text = Name;
	}
	
	void ListBox组件列表Click(object sender, EventArgs e)
	{
		if( listBox组件列表.SelectedIndex == -1 ) {
			return;
		}
		string UnitName = listBox组件列表.SelectedItem.ToString();
		string[] s = null;
		
		this.listBox成员列表.Items.Clear();
		
		if( UnitName == "[变量]" ) {
			s = EXPMember.GetVarlist();
			PreType = "@WORD";
		}
		else {
			s = EXPMember.GetModuleMember( UnitName, ExpTypeList );
			PreType = "";
		}
		if( s != null ) {
			this.listBox成员列表.Items.AddRange( s );
			listBox成员列表.SelectedIndex = -1;
		}
	}
	
	void NumberbuttonClick(object sender, EventArgs e)
	{
		string text = ((Button)sender).Text;
		if( text == "-" ) {
			if( this.textBox1.Text.StartsWith( "-" ) ) {
				this.textBox1.Text = this.textBox1.Text.Remove( 0, 1 );
			}
			else {
				this.textBox1.Text = "-" + this.textBox1.Text;
			}
		}
		else {
			this.textBox1.Text += text;
		}
		PreType = "@WORD";
	}
	
	void ButtonCEClick(object sender, EventArgs e)
	{
		this.textBox1.Clear();
		PreType = "@WORD";
	}
	
	void ButtonYESClick(object sender, EventArgs e)
	{
		this.textBox1.Text = "true";
		PreType = "@WORD bool";
	}
	
	void ButtonNOClick(object sender, EventArgs e)
	{
		this.textBox1.Text = "false";
		PreType = "@WORD bool";
	}
	
	void OperButtonClick(object sender, EventArgs e)
	{
		string text = ((Button)sender).Name;
		text = text.Remove( 0, 6 );
		
		string Res = null;
		if( text == "绝对值" ) {
			Res = "+ ( ?int32 )";
		}
		if( text == "取负" ) {
			Res = "- ( ?int32 )";
		}
		if( text == "加" ) {
			Res = "( ?int32 ) + ( ?int32 )";
		}
		if( text == "减" ) {
			Res = "( ?int32 ) - ( ?int32 )";
		}
		if( text == "乘" ) {
			Res = "( ?int32 ) * ( ?int32 )";
		}
		if( text == "除" ) {
			Res = "( ?int32 ) / ( ?int32 )";
		}
		if( text == "余" ) {
			Res = "( ?int32 ) % ( ?int32 )";
		}
		if( text == "等于" ) {
			Res = "( ?int32 ) == ( ?int32 )";
		}
		if( text == "不等于" ) {
			Res = "( ?int32 ) != ( ?int32 )";
		}
		if( text == "大于" ) {
			Res = "( ?int32 ) > ( ?int32 )";
		}
		if( text == "小于" ) {
			Res = "( ?int32 ) < ( ?int32 )";
		}
		if( text == "大于等于" ) {
			Res = "( ?int32 ) >= ( ?int32 )";
		}
		if( text == "小于等于" ) {
			Res = "( ?int32 ) <= ( ?int32 )";
		}
		if( text == "不成立" ) {
			Res = "! ( ?bool )";
		}
		if( text == "并且" ) {
			Res = "( ?bool ) && ( ?bool )";
		}
		if( text == "或者" ) {
			Res = "( ?bool ) || ( ?bool )";
		}
		if( text == "异或" ) {
			Res = "( ?bool ) ^^ ( ?bool )";
		}
		if( text == "赋值" ) {
			Res = "( ?base ) = ( ?base )";
		}
		if( Res == null ) {
			MessageBox.Show( "未处理的按键: " + text );
			return;
		}
		this.textBox1.Text = Res;
		PreType = "@OPER";
	}
	
	void TextBox1KeyPress(object sender, KeyPressEventArgs e)
	{
		PreType = "@WORD";
	}
}
}

