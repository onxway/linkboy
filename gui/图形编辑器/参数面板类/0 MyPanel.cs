﻿
namespace n_MyPanel
{
using System;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Windows.Forms;

using n_MyControl;
using n_Shape;
using n_MyObject;

//*****************************************************
//组件面板类
public class MyPanel
{
	public Pen MouseOnPen;
	
	public float X, Y;
	public float Width, Height;
	
	protected Brush BackBrush;
	protected bool isMouseOn;
	public bool Visible;
	
	protected MyControl[] MyControlList;
	protected int MyControlListLength;
	
	public static bool ExistPanel;
	
	//构造函数
	public MyPanel()
	{
		X = 0;
		Y = 0;
		Width = 100;
		Height = 300;
		isMouseOn = false;
		Visible = true;
		BackBrush = new SolidBrush( Color.FromArgb( 50, 0, 255, 0 ) );
		MyControlList = new MyControl[ 50 ];
		MyControlListLength = 0;
		
		MouseOnPen = Pens.Black;
	}
	
	//清除所有控件
	public void ClearControl()
	{
		MyControlListLength = 0;
	}
	
	//移除一个控件
	public void RemoveControl( MyControl m )
	{
		for( int i = 0; i < MyControlListLength; ++i ) {
			if( MyControlList[ i ] == null ) {
				continue;
			}
			if( MyControlList[ i ] == m ) {
				MyControlList[ i ] = null;
			}
		}
	}
	
	//添加一个控件
	public void AddControl( MyControl c )
	{
		MyControlList[ MyControlListLength ] = c;
		++MyControlListLength;
	}
	
	//判断鼠标是否在当前组件上
	public bool MouseIsInside( float mX, float mY )
	{
		if( mX >= X && mX <= X + Width && mY >= Y && mY <= Y + Height ) {
			return true;
		}
		else {
			return false;
		}
	}
	
	//组件绘制工作
	public void Draw( Graphics g )
	{
		if( !Visible ) {
			return;
		}
		ExistPanel = true;
		
		//绘制组件名称
		RectangleF r = new RectangleF( X, Y, Width, Height );
		GraphicsPath gp = Shape.CreateRoundedRectanglePath( r );
		g.FillPath( BackBrush, gp );
		if( isMouseOn ) {
			g.DrawPath( MouseOnPen, gp );
		}
		else {
			g.DrawPath( Pens.Gray, gp );
		}
		
		//绘制面板背景
		//g.FillRectangle( BackBrush, StartX + X - Width / 2, StartY + Y - Height / 2, Width, Height );
		//if( isMouseOn ) {
		//	g.DrawRectangle( Pens.White, StartX + X - Width / 2, StartY + Y - Height / 2, Width, Height );
		//}
		//else {
		//	g.DrawRectangle( Pens.Gray, StartX + X - Width / 2, StartY + Y - Height / 2, Width, Height );
		//}
		
		//绘制面板控件列表
		for( int i = 0; i < MyControlListLength; ++i ) {
			if( MyControlList[ i ] == null ) {
				continue;
			}
			if( !MyControlList[ i ].Visible ) {
				continue;
			}
			MyControlList[ i ].Draw( g );
		}
	}
	
	//鼠标按下事件
	public bool MouseDown( float mX, float mY )
	{
		if( !Visible || !MouseIsInside( mX, mY ) ) {
			return false;
		}
		//控件列表鼠标按下事件
		for( int i = 0; i < MyControlListLength; ++i ) {
			if( MyControlList[ i ] == null ) {
				continue;
			}
			if( !MyControlList[ i ].Visible ) {
				continue;
			}
			MyControlList[ i ].MouseDown( mX - this.X, mY - this.Y );
		}
		return true;
	}
	
	//鼠标松开事件
	public void MouseUp( float mX, float mY )
	{
		if( !Visible ) {
			return;
		}
		//控件列表鼠标松开事件
		for( int i = 0; i < MyControlListLength; ++i ) {
			if( MyControlList[ i ] == null ) {
				continue;
			}
			if( !MyControlList[ i ].Visible ) {
				continue;
			}
			MyControlList[ i ].MouseUp( mX - this.X, mY - this.Y );
		}
	}
	
	//鼠标移动事件
	public bool MouseMove( float mX, float mY )
	{
		if( !Visible ) {
			return false;
		}
		if( MouseIsInside( mX, mY ) ) {
			isMouseOn = true;
			//控件列表鼠标移动事件
			for( int i = 0; i < MyControlListLength; ++i ) {
				if( MyControlList[ i ] == null ) {
					continue;
				}
				if( !MyControlList[ i ].Visible ) {
					continue;
				}
				MyControlList[ i ].MouseMove( mX - this.X, mY - this.Y );
			}
		}
		else {
			isMouseOn = false;
		}
		return isMouseOn;
	}
	
	//-------------------------------
	
	protected bool isNameError( string OldName, string NewName )
	{
		if( NewName == "" ) {
			n_Debug.Warning.WarningMessage = "模块名称不能为空, 请修改为一个新名字: " + OldName + " -> " + NewName;
			return true;
		}
		if( G.CGPanel.myModuleList.NameisUsed( NewName ) ) {
			n_Debug.Warning.WarningMessage = "模块名称不能和其他模块重名, 请修改为一个新名字: " + OldName + " -> " + NewName;
			return true;
		}
		if( NewName[0] >= '0' && NewName[0] <= '9' ) {
			n_Debug.Warning.WarningMessage = "模块名称不能以纯数字开始: " + OldName + " -> " + NewName;
			return true;
		}
		if( NewName.IndexOf( ' ' ) != -1 || NewName.IndexOf( '\t' ) != -1 ) {
			n_Debug.Warning.WarningMessage = "模块名称中出现了空格或者制表符: " + OldName + " -> " + NewName;
			return true;
		}
		for( int i = 0; i < NewName.Length; ++i ) {
			char c = NewName[i];
			if( !n_CharType.CharType.isLetterOrNumber( c ) ) {
				n_Debug.Warning.WarningMessage = ( "名字中包含了非法字符: " + c + " (" + (int)c + ")" );
				return true;
			}
		}
		return false;
	}
	
	protected bool isNumberError( string OldName, string NewName )
	{
		if( NewName == "" ) {
			n_Debug.Warning.WarningMessage = "数值不能为空, 请修改: " + OldName + " -> " + NewName;
			return true;
		}
		if( NewName.IndexOf( ' ' ) != -1 || NewName.IndexOf( '\t' ) != -1 ) {
			n_Debug.Warning.WarningMessage = "数值中出现了空格或者制表符: " + OldName + " -> " + NewName;
			return true;
		}
		NewName = NewName.ToLower();
		for( int i = 0; i < NewName.Length; ++i ) {
			char c = NewName[i];
			if( !n_CharType.CharType.isNumber( c ) && c != '-' && c != '.' && c != 'x' && !(c >= 'a' && c <= 'f') ) {
				n_Debug.Warning.WarningMessage = ( "数值中包含了非法字符: " + c + " (" + (int)c + ") -> " + NewName );
				return true;
			}
		}
		return false;
	}
}
}



