﻿
namespace n_NotePanel
{
using System;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Windows.Forms;
using n_GUIcoder;
using n_MyPanel;
using n_GVar;
using n_MyButton;
using n_MyLabel;
using n_MyTextBox;
using n_GNote;
using n_NoteForm;
using n_GUIset;

//*****************************************************
//变量面板类
public class NotePanel : MyPanel
{
	GNote owner;
	
	public MyTextBox NameTextBox;
	public MyTextBox TypeBox;
	
	public MyButton NameButton;
	public MyButton DeleteButton;
	public MyButton EditButton;
	public MyButton TopButton;
	public MyButton BottomButton;
	public MyLabel MyLabelType;
	
	//构造函数
	public NotePanel( GNote v ) : base()
	{
		owner = v;
		
		BackBrush = new SolidBrush( Color.FromArgb( 220, Color.Silver ) );
		
		NameTextBox = new MyTextBox( this, owner.Name );
		NameTextBox.X = GUIset.GetPixTTT(5);
		NameTextBox.Y = GUIset.GetPixTTT(5);
		NameTextBox.Width = GUIset.GetPixTTT(100);
		NameTextBox.BackColor = Color.WhiteSmoke;
		NameTextBox.MyTextChanged += new MyTextBox.deleTextChanged( NameTextBoxTextChanged );
		this.AddControl( NameTextBox );
		
		if( owner.isCode ) {
			TypeBox = new MyTextBox( this, owner.CodeType.ToString() );
			TypeBox.X = GUIset.GetPixTTT(110);
			TypeBox.Y = GUIset.GetPixTTT(5);
			TypeBox.Width = GUIset.GetPixTTT(50);
			TypeBox.BackColor = Color.WhiteSmoke;
			TypeBox.MyTextChanged += new MyTextBox.deleTextChanged( TypeBoxTextChanged );
			this.AddControl( TypeBox );
		}
		
		DeleteButton = new MyButton( this );
		DeleteButton.Text = "删除";
		DeleteButton.X = GUIset.GetPixTTT(5);
		DeleteButton.Y = GUIset.GetPixTTT(40);
		DeleteButton.MyMouseDown += new MyButton.deleMouseDown( DeleteButtonMouseDown );
		this.AddControl( DeleteButton );
		
		EditButton = new MyButton( this );
		EditButton.Text = "编辑";
		EditButton.X = GUIset.GetPixTTT(50);
		EditButton.Y = GUIset.GetPixTTT(40);
		EditButton.MyMouseDown += new MyButton.deleMouseDown( EditButtonMouseDown );
		this.AddControl( EditButton );
		
		TopButton = new MyButton( this );
		TopButton.Text = "置于顶层";
		TopButton.X = GUIset.GetPixTTT(100);
		TopButton.Y = GUIset.GetPixTTT(40);
		TopButton.Width = GUIset.GetPixTTT(64);
		TopButton.MyMouseDown += new MyButton.deleMouseDown( TopButtonMouseDown );
		this.AddControl( TopButton );
		
		BottomButton = new MyButton( this );
		BottomButton.Text = "置于底层";
		BottomButton.X = GUIset.GetPixTTT(170);
		BottomButton.Y = GUIset.GetPixTTT(40);
		BottomButton.Width = GUIset.GetPixTTT(64);
		BottomButton.MyMouseDown += new MyButton.deleMouseDown( BottomButtonMouseDown );
		this.AddControl( BottomButton );
		
		if( !owner.isCode ) {
			float StartX = GUIset.GetPixTTT(5);
			float StartY = GUIset.GetPixTTT(75);
			MyButton b;
			
			b = new MyButton( this );
			SetButtonColor( b, owner.BackColor );
			b.Text = "背景颜色";
			b.X = StartX;
			b.Y = StartY;
			b.Width = GUIset.GetPixTTT(64);
			//b.Height = GUIset.GetPixTTT(25);
			b.MyMouseDown = new MyButton.deleMouseDown( BackColorButtonMouseDown );
			this.AddControl( b );
			StartX += b.Width + GUIset.GetPixTTT(4);
			
			b = new MyButton( this );
			SetButtonColor( b, owner.EdgeColor );
			b.Text = "边框颜色";
			b.X = StartX;
			b.Y = StartY;
			b.Width = GUIset.GetPixTTT(64);
			//b.Height = GUIset.GetPixTTT(25);
			b.MyMouseDown = new MyButton.deleMouseDown( FrameColorButtonMouseDown );
			this.AddControl( b );
			StartX += b.Width + GUIset.GetPixTTT(4);
			
			b = new MyButton( this );
			SetButtonColor( b, owner.ForeColor );
			b.Text = "文字颜色";
			b.X = StartX;
			b.Y = StartY;
			b.Width = GUIset.GetPixTTT(64);
			//b.Height = GUIset.GetPixTTT(25);
			b.MyMouseDown = new MyButton.deleMouseDown( ForeColorButtonMouseDown );
			this.AddControl( b );
			StartX += b.Width + GUIset.GetPixTTT(4);
		}
		
		this.Width = GUIset.GetPixTTT(250);
		this.Height = GUIset.GetPixTTT(110);
	}
	
	//文字改变事件
	void NameTextBoxTextChanged( object sender )
	{
		if( this.NameTextBox.Text != "" ) {
			owner.Name = this.NameTextBox.Text;
		}
	}
	
	//文字改变事件
	void TypeBoxTextChanged( object sender )
	{
		if( this.TypeBox.Text != "" ) {
			owner.CodeType = int.Parse( this.TypeBox.Text );
		}
	}
	
	//删除键鼠标按下事件
	void DeleteButtonMouseDown( object sender )
	{
		if( G.SimulateMode ) {
			n_Debug.Warning.WarningMessage = "仿真模式下无法删除模块, 请先结束仿真";
			return;
		}
		if( MessageBox.Show( "您确定删除当前组件吗?", "组件删除确认", MessageBoxButtons.OKCancel ) == DialogResult.OK ) {
			owner.Remove();
		}
	}
	
	//编辑键鼠标按下事件
	void EditButtonMouseDown( object sender )
	{
		if( owner.isCode ) {
			
			if( owner.CodeType == n_GNote.GNote.T_CD ) {
				if( G.LIDEBox == null ) {
					G.LIDEBox = new n_LIDEForm.LIDEForm();
				}
				string ss = G.LIDEBox.Run( owner, owner.Value );
				if( ss != null ) {
					owner.Value = ss;
					((GNote)owner).RefreshHeight();
				}
			}
			else if( owner.CodeType == n_GNote.GNote.T_CX ) {
				if( G.UserCodeBox == null ) {
					G.UserCodeBox = new n_UserCodeForm.UserCodeForm();
				}
				string gname = owner.Name;
				string s = G.UserCodeBox.Run( owner, owner.Value );
				if( s != null ) {
					owner.Value = s;
					((GNote)owner).RefreshHeight();
				}
			}
			else {
				MessageBox.Show( "未知的代码类型: " + owner.CodeType );
			}
		}
		else {
			if( G.NoteBox == null ) {
				G.NoteBox = new n_NoteForm.NoteForm();
			}
			string s = G.NoteBox.Run( owner, owner.Value );
			if( s != null ) {
				owner.Value = s;
				((GNote)owner).RefreshHeight();
			}
		}
	}
	
	//顶层键鼠标按下事件
	void TopButtonMouseDown( object sender )
	{
		int tar_index = G.CGPanel.myModuleList.FindLastNote();
		ReplaceObj( tar_index );
	}
	
	//底层键鼠标按下事件
	void BottomButtonMouseDown( object sender )
	{
		ReplaceObj( 0 );
	}
	
	void ReplaceObj( int tar_index )
	{
		if( owner.Index == tar_index ) {
			return;
		}
		GNote tar = (GNote)G.CGPanel.myModuleList.ModuleList[tar_index];
		
		int ti = tar.Index;
		tar.Index = owner.Index;
		owner.Index = ti;
		
		G.CGPanel.myModuleList.ModuleList[tar.Index] = tar;
		G.CGPanel.myModuleList.ModuleList[owner.Index] = owner;
	}
	
	//------------------------------------------------------
	
	//背景颜色按钮单击事件
	void BackColorButtonMouseDown( object sender )
	{
		if( n_wlibCom.ColorDlg.ShowColorDlgOk( owner.BackColor ) ) {
			SetButtonColor( (MyButton)sender, n_wlibCom.ColorDlg.SelectColor );
			owner.BackColor = n_wlibCom.ColorDlg.SelectColor;
			owner.RefreshPen();
		}
	}
	
	//边框颜色按钮单击事件
	void FrameColorButtonMouseDown( object sender )
	{
		if( n_wlibCom.ColorDlg.ShowColorDlgOk( owner.EdgeColor ) ) {
			SetButtonColor( (MyButton)sender, n_wlibCom.ColorDlg.SelectColor );
			owner.EdgeColor = n_wlibCom.ColorDlg.SelectColor;
			owner.RefreshPen();
		}
	}
	
	//文字颜色按钮单击事件
	void ForeColorButtonMouseDown( object sender )
	{
		if( n_wlibCom.ColorDlg.ShowColorDlgOk( owner.ForeColor ) ) {
			SetButtonColor( (MyButton)sender, n_wlibCom.ColorDlg.SelectColor );
			owner.ForeColor = n_wlibCom.ColorDlg.SelectColor;
			owner.RefreshPen();
		}
	}
	
	//设置按钮颜色属性
	void SetButtonColor( MyButton b, Color bc )
	{
		Color fc;
		if( (bc.R + bc.G + bc.B) / 3 > 120 ) {
			fc = Color.Black;
		}
		else {
			fc = Color.White;
		}
		b.BackColor = bc;
		b.hBackColor = bc;
		b.ForeColor = fc;
		b.hForeColor = fc;
	}
}
}



