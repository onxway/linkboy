﻿
namespace n_MyControl
{
using System;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Windows.Forms;
using n_MyPanel;
using n_GUIset;

//*****************************************************
//控件类
public abstract class MyControl
{
	public static MyControl FocusObject;	
	
	public float X, Y;
	public float Width, Height;
	
	public bool AutoSize;
	public bool ignoreTextChanged;
	string vvText;
	public string Text {
		get { return vvText; }
		set {
			vvText = value;
			if( AutoSize ) {
				this.Width = (int)GUIset.mg.MeasureString( vvText, n_GUIset.GUIset.ExpFont ).Width;
			}
			if( ignoreTextChanged ) {
				ignoreTextChanged = false;
			}
			else {
				TextChanged();
			}
		}
	}
	
	public bool Visible;
	
	public int iMes;
	public string sMes;
	public bool bMes;
	
	//边框颜色
	Color vvFrameColor;	
	protected Pen FramePen;
	public Color FrameColor {
		set {
			vvFrameColor = Color.FromArgb( value.R, value.G, value.B );
			FramePen = new Pen( vvFrameColor );
		}
	}
	Color vvhFrameColor;
	protected Pen hFramePen;
	public Color hFrameColor {
		set {
			vvhFrameColor = Color.FromArgb( value.R, value.G, value.B );
			hFramePen = new Pen( vvhFrameColor );
		}
	}
	
	//背景颜色
	Color vvBackColor;
	protected SolidBrush BackBrush;
	public Color BackColor {
		set {
			vvBackColor = Color.FromArgb( Opacity, value.R, value.G, value.B );
			BackBrush = new SolidBrush( vvBackColor );
		}
	}
	Color vvhBackColor;
	protected SolidBrush hBackBrush;
	public Color hBackColor {
		set {
			vvhBackColor = Color.FromArgb( Opacity, value.R, value.G, value.B );
			hBackBrush = new SolidBrush( vvhBackColor );
		}
	}
	
	//前景颜色
	Color vvForeColor;
	protected SolidBrush ForeBrush;
	public Color ForeColor {
		set {
			vvForeColor = Color.FromArgb( Opacity, value.R, value.G, value.B );
			ForeBrush = new SolidBrush( vvForeColor );
		}
	}
	Color vvhForeColor;
	protected SolidBrush hForeBrush;
	public Color hForeColor {
		set {
			vvhForeColor = Color.FromArgb( Opacity, value.R, value.G, value.B );
			hForeBrush = new SolidBrush( vvhForeColor );
		}
	}
	
	protected MyPanel owner;
	
	bool vvisMouseOn;	
	public bool isMouseOn {
		get { return vvisMouseOn; }
		set {
			if( vvisMouseOn && !value ) {
				if( MyMouseLeave != null ) {
					MyMouseLeave( this );
				}
			}
			if( !vvisMouseOn && value ) {
				if( MyMouseEnter != null ) {
					MyMouseEnter( this );
				}
			}
			vvisMouseOn = value;
		}
	}
	
	int Opacity;
	
	public delegate void deleMouseEnter( object sender );
	public deleMouseEnter MyMouseEnter;
	public delegate void deleMouseLeave( object sender );
	public deleMouseLeave MyMouseLeave;
	
	//构造函数
	public MyControl( MyPanel vowner )
	{
		owner = vowner;
		Opacity = 190;
		Visible = true;
		Text = "";
		X = 0;
		Y = 0;
		Width = GUIset.GetPixTTT(50);
		Height = GUIset.GetPixTTT(25);
		
		iMes = 0;
		sMes = null;
		bMes = false;
		
		AutoSize = false;
		ignoreTextChanged = false;
		
		isMouseOn = false;
		
		FrameColor = Color.Gray;
		hFrameColor = Color.White;
		
		BackColor = Color.Gray;
		hBackColor = Color.DarkGray;
		
		ForeColor = Color.Black;
		hForeColor = Color.DarkBlue;
	}
	
	//判断鼠标是否在当前组件上
	public bool MouseIsInside( float mX, float mY )
	{
		if( mX >= X && mX <= X + Width && mY >= Y && mY <= Y + Height ) {
			return true;
		}
		else {
			return false;
		}
	}
	
	//文字改变事件
	public abstract void TextChanged();
	
	//文字输入事件
	public abstract void KeyPress( char c );
	
	//组件绘制工作
	public abstract void Draw( Graphics g );
	
	//鼠标按下事件
	public abstract void MouseDown( float mX, float mY );
	
	//鼠标松开事件
	public abstract void MouseUp( float mX, float mY );
	
	//鼠标移动事件
	public abstract void MouseMove( float mX, float mY );
}
}



