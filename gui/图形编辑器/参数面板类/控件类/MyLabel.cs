﻿
namespace n_MyLabel
{
using System;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Windows.Forms;
using n_MyPanel;
using n_MyControl;
using n_Shape;

//*****************************************************
//按钮控件类
public class MyLabel : MyControl
{
	//构造函数
	public MyLabel( MyPanel vowner, string vText ) : base( vowner )
	{
		this.BackColor = Color.Green;
		this.Text = vText;
	}
	
	//文字改变事件
	public override void TextChanged()
	{
		
	}
	
	//文字输入事件
	public override void KeyPress( char c )
	{
		
	}
	
	//组件绘制工作
	public override void Draw( Graphics g )
	{
		float StartX = owner.X;
		float StartY = owner.Y;
		
		Brush backBrush;
		Brush foreBrush;
		Pen framePen;
		if( isMouseOn ) {
			backBrush = hBackBrush;
			foreBrush = hForeBrush;
			framePen = hFramePen;
		}
		else {
			backBrush = BackBrush;
			foreBrush = ForeBrush;
			framePen = FramePen;
		}
		
		backBrush = BackBrush;
		foreBrush = ForeBrush;
		framePen = FramePen;
		
		RectangleF r = new RectangleF( StartX + X, StartY + Y, Width, Height );
		GraphicsPath gp = Shape.CreateRoundedRectanglePath( r );
		//g.FillPath( backBrush, gp );
		//g.DrawPath( framePen, gp );
		//g.FillRectangle( backBrush, StartX + X, StartY + Y, Width, Height );
		//g.DrawRectangle( framePen, StartX + X, StartY + Y, Width, Height );
		
		//Rectangle r = new Rectangle( StartX + X, StartY + Y, Width, Height );
		//StringFormat sf = new StringFormat();
		//sf.Alignment = StringAlignment.Center;
		//sf.LineAlignment = StringAlignment.Center;
		//g.DrawString( Text, font, foreBrush, r, sf );
		
		g.DrawString( Text, n_GUIset.GUIset.ExpFont, foreBrush, StartX + X, StartY + Y );
	}
	
	//鼠标按下事件
	public override void MouseDown( float mX, float mY )
	{
		if( !MouseIsInside( mX, mY ) ) {
			return;
		}
		//...
	}
	
	//鼠标松开事件
	public override void MouseUp( float mX, float mY )
	{
		//...
	}
	
	//鼠标移动事件
	public override void MouseMove( float mX, float mY )
	{
		if( MouseIsInside( mX, mY ) ) {
			isMouseOn = true;
		}
		else {
			isMouseOn = false;
		}
	}
}
}



