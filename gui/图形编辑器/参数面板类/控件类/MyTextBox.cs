﻿
namespace n_MyTextBox
{
using System;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Windows.Forms;
using n_MyPanel;
using n_MyControl;
using n_Shape;
using n_ImagePanel;

//*****************************************************
//按钮控件类
public class MyTextBox : MyControl
{
	public delegate void deleTextChanged( object sender );
	public deleTextChanged MyTextChanged;
	
	System.Windows.Forms.Timer t;
	
	int SelectStart;
	int SelectLength;
	
	bool isFocusHighLight;
	
	Font MessageTextFont;
	
	public bool isName;
	public bool isNumber;
	
	//构造函数
	public MyTextBox( MyPanel vowner, string vText ) : base( vowner )
	{
		MessageTextFont = new Font( "宋体", 15, FontStyle.Bold );
		
		ForeColor = Color.Black;
		hForeColor = Color.Black;
		FrameColor = Color.Gray;
		hFrameColor = Color.OrangeRed;
		hBackColor = Color.White;
		BackColor = Color.LightGray;
		
		AutoSize = false;
		ignoreTextChanged = true;
		Text = vText;
		ignoreTextChanged = false;
		isName = false;
		isNumber = false;
		
		SelectStart = 0;
		SelectLength = 0;
		
		isFocusHighLight = false;
		t = new Timer();
		t.Interval = 300;
		t.Enabled = false;
		t.Tick += new EventHandler( TimerTick );
	}
	
	void TimerTick( object sender, EventArgs e )
	{
		if( MyControl.FocusObject == this ) {
			isFocusHighLight = !isFocusHighLight;
			G.CGPanel.MyRefresh();
		}
		else {
			isFocusHighLight = false;
			t.Enabled = false;
		}
	}
	
	//文字改变事件
	public override void TextChanged()
	{
		if( MyTextChanged != null ) {
			if( ignoreTextChanged ) {
				return;
			}
			MyTextChanged( this );
		}
	}
	
	//文字输入事件
	public override void KeyPress( char c )
	{
		string S1;
		string S2;
		if( SelectStart == this.Text.Length ) {
			S1 = this.Text;
			S2 = "";
		}
		else {
			S1 = this.Text.Remove( SelectStart );
			S2 = this.Text.Remove( 0, SelectStart + SelectLength );
		}
		if( c == (int)Keys.Back ) {
			if( S1.Length != 0 ) {
				S1 = S1.Remove( S1.Length - 1, 1 );
			}
		}
		else if( c == (int)Keys.Enter ) {
			//MyControl.FocusObject = null;
		}
		else if( (int)c < ' ' ) {
			//...
		}
		else {
			if( isName ) {
				if( !n_CharType.CharType.isLetterOrNumber( c ) ) {
					return;
				}
			}
			if( isNumber ) {
				if( !n_CharType.CharType.isLetterOrNumber( c ) && c != '-' && c != '.' ) {
					return;
				}
			}
			S1 += c;
		}
		
		this.Text = S1 + S2;
		
		SelectStart = this.Text.Length;
		SelectLength = 0;
	}
	
	//组件绘制工作
	public override void Draw( Graphics g )
	{
		float StartX = owner.X;
		float StartY = owner.Y;
		
		Brush backBrush;
		Brush foreBrush;
		Pen framePen;
		if( isMouseOn ) {
			backBrush = hBackBrush;
			foreBrush = hForeBrush;
			framePen = hFramePen;
		}
		else {
			backBrush = BackBrush;
			foreBrush = ForeBrush;
			framePen = FramePen;
		}
		
		if( MyControl.FocusObject == this ) {
			backBrush = Brushes.Gray;
		}
		if( isFocusHighLight ) {
			//framePen = Pens.Black;
			//backBrush = Brushes.White;
			//framePen = Pens.Black;
			
			//显示提示信息
			//g.DrawString( "请用键盘输入参数", MessageTextFont, Brushes.OrangeRed, StartX + X + Width + 10, StartY + Y );
		}
		RectangleF r = new RectangleF( X, Y, Width, Height );
		//GraphicsPath gp = Shape.CreateRoundedRectanglePath( r );
		//g.FillPath( backBrush, gp );
		//g.DrawPath( framePen, gp );
		g.FillRectangle( backBrush, StartX + X, StartY + Y, Width, Height );
		g.DrawRectangle( framePen, StartX + X, StartY + Y, Width, Height );
		
		//Rectangle r = new Rectangle( StartX + X, StartY + Y, Width, Height );
		//StringFormat sf = new StringFormat();
		//sf.Alignment = StringAlignment.Center;
		//sf.LineAlignment = StringAlignment.Center;
		//g.DrawString( Text, font, foreBrush, r, sf );
		
		if( SelectStart == 0 && MyControl.FocusObject == this ) {
			
			/*
			if( isFocusHighLight ) {
				//g.DrawString( Text, font, Brushes.OrangeRed, StartX + X, StartY + Y + 5 );
				
				//显示键盘输入提示信息
				g.FillRectangle( Brushes.PeachPuff, StartX + X + Width + 30, StartY + Y - Height, 150, Height * 3 );
				g.DrawRectangle( Pens.OrangeRed, StartX + X + Width + 30, StartY + Y - Height, 150, Height * 3 );
				g.DrawString( "请通过键盘输入信息", font, Brushes.OrangeRed, StartX + X + Width + 40, StartY + Y + 1 );
			}
			//else {
			//	g.DrawString( Text, font, Brushes.WhiteSmoke, StartX + X, StartY + Y + 5 );
			//}
			
			*/
		}
		else {
			g.DrawString( Text, n_GUIset.GUIset.ExpFont, foreBrush, StartX + X, StartY + Y );
		}
	}
	
	//鼠标按下事件
	public override void MouseDown( float mX, float mY )
	{
		if( !MouseIsInside( mX, mY ) ) {
			return;
		}
		isMouseOn = false;
		
		MyControl.FocusObject = this;
		
		float bx = (G.CGPanel.StartX + X + owner.X+1) * ImagePanel.AScale/ImagePanel.AScaleMid;
		float by = (G.CGPanel.StartY + Y + owner.Y+1) * ImagePanel.AScale/ImagePanel.AScaleMid;
		
		ImagePanel.MyTextBox.Font = n_SG.SG.MString.font[ (int)(11*n_GUIset.GUIset.pt) * ImagePanel.AScale/ImagePanel.AScaleMid];
		ImagePanel.MyTextBox.Width = (int)((Width-1) * ImagePanel.AScale/ImagePanel.AScaleMid);
		ImagePanel.MyTextBox.Height = (int)((Height-1) * ImagePanel.AScale/ImagePanel.AScaleMid);
		ImagePanel.MyTextBox.Location = new Point( (int)bx, (int)by );
		ImagePanel.MyTextBox.Text = Text;
		ImagePanel.MyTextBox.Visible = true;
		ImagePanel.MyTextBox.Focus();
		ImagePanel.MyTextBox.SelectAll();
		
		SelectStart = 0;
		SelectLength = this.Text.Length;
		//SelectStart = this.Text.Length;
		//SelectLength = 0;
		
		t.Enabled = true;
//		string s = A.TextBox.Run( this.Text );
//		if( s != null ) {
//			this.Text = s;
//			if( MyTextChanged != null ) {
//				MyTextChanged( this );
//			}
//		}
	}
	
	//鼠标松开事件
	public override void MouseUp( float mX, float mY )
	{
		//...
	}
	
	//鼠标移动事件
	public override void MouseMove( float mX, float mY )
	{
		if( MouseIsInside( mX, mY ) ) {
			isMouseOn = true;
		}
		else {
			isMouseOn = false;
		}
	}
}
}



