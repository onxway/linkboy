﻿
namespace n_MyButton
{
using System;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Windows.Forms;
using n_MyPanel;
using n_MyControl;
using n_Shape;
using n_GUIset;

//*****************************************************
//按钮控件类
public class MyButton : MyControl
{
	public delegate void deleMouseDown( object sender );
	public deleMouseDown MyMouseDown;
	public delegate void deleMouseUp( object sender );
	public deleMouseUp MyMouseUp;
	
	//构造函数
	public MyButton( MyPanel vowner ) : base( vowner )
	{
		Width = GUIset.GetPixTTT(40);
		Height = GUIset.GetPixTTT(25);
		
		FrameColor = Color.DarkBlue;
		hFrameColor = Color.Blue;
		
		BackColor = Color.LightGray;
		hBackColor = Color.White;
		
		ForeColor = Color.Black;
		hForeColor = Color.Blue;
	}
	
	//文字改变事件
	public override void TextChanged()
	{
		
	}
	
	//文字输入事件
	public override void KeyPress( char c )
	{
		
	}
	
	//组件绘制工作
	public override void Draw( Graphics g )
	{
		float StartX = owner.X;
		float StartY = owner.Y;
		
		Brush backBrush;
		Brush foreBrush;
		Pen framePen;
		if( isMouseOn ) {
			backBrush = hBackBrush;
			foreBrush = hForeBrush;
			framePen = hFramePen;
		}
		else {
			backBrush = BackBrush;
			foreBrush = ForeBrush;
			framePen = FramePen;
		}
		RectangleF r = new RectangleF( StartX + X, StartY + Y, Width, Height );
		GraphicsPath gp = Shape.CreateRoundedRectanglePath( r );
		g.FillPath( backBrush, gp );
		g.DrawPath( framePen, gp );
		//g.FillRectangle( backBrush, StartX + X, StartY + Y, Width, Height );
		//g.DrawRectangle( framePen, StartX + X, StartY + Y, Width, Height );
		
		//文本显示排版
		RectangleF rr = new RectangleF( StartX + X - 10, StartY + Y - 10, Width + 20, Height + 20 );
		StringFormat sf = new StringFormat();
		sf.Alignment = StringAlignment.Center;
		sf.LineAlignment = StringAlignment.Center;
		g.DrawString( Text, n_GUIset.GUIset.ExpFont, foreBrush, rr, sf );
		
		//普通显示
		//g.DrawString( Text, font, foreBrush, StartX + X, StartY + Y );
	}
	
	//鼠标按下事件
	public override void MouseDown( float mX, float mY )
	{
		if( !MouseIsInside( mX, mY ) ) {
			return;
		}
		if( MyMouseDown != null ) {
			MyMouseDown( this );
		}
	}
	
	//鼠标松开事件
	public override void MouseUp( float mX, float mY )
	{
		if( !MouseIsInside( mX, mY ) ) {
			return;
		}
		if( MyMouseUp != null ) {
			MyMouseUp( this );
		}
	}
	
	//鼠标移动事件
	public override void MouseMove( float mX, float mY )
	{
		if( MouseIsInside( mX, mY ) ) {
			isMouseOn = true;
		}
		else {
			isMouseOn = false;
		}
	}
}
}



