﻿
namespace n_GroupPanel
{
using System;
using System.Drawing;
using System.Windows.Forms;

using c_ControlType;
using c_MyObjectSet;
using n_EventLink;
using n_GUIcoder;
using n_GUIset;
using n_HardModule;
using n_MyButton;
using n_MyFileObject;
using n_MyLabel;
using n_MyPanel;
using n_MyTextBox;
using n_UIModule;

using n_EventIns;
using n_CondiEndIns;
using n_GUICommon;
using n_GroupList;
using n_UserFunctionIns;
using n_GUIset;

//*****************************************************
//硬件组件面板类
public class GroupPanel : MyPanel
{
	public Group owner;
	
	public MyTextBox NameTextBox;
	public MyButton DeleteButton;
	
	UserFunctionIns[] InsList;
	
	//构造函数
	public GroupPanel( Group h ) : base()
	{
		owner = h;
		
		BackBrush = new SolidBrush( Color.FromArgb( 200, 160, 180, 190 ) );
		
		/*
		NameTextBox = new MyTextBox( this, owner.Name );
		NameTextBox.isName = true;
		NameTextBox.X = 95;
		NameTextBox.Y = 5;
		NameTextBox.Width = 110;
		NameTextBox.MyTextChanged += new MyTextBox.deleTextChanged( NameTextBoxTextChanged );
		this.AddControl( NameTextBox );
		
		DeleteButton = new MyButton( this );
		DeleteButton.Text = "删除";
		DeleteButton.X = 5;
		DeleteButton.Y = 40;
		DeleteButton.ForeColor = Color.OrangeRed;
		//DeleteButton.BackColor = Color.LightPink;
		DeleteButton.FrameColor = Color.OrangeRed;
		DeleteButton.hForeColor = Color.OrangeRed;
		DeleteButton.hBackColor = Color.LightPink;
		DeleteButton.hFrameColor = Color.OrangeRed;
		DeleteButton.MyMouseDown += new MyButton.deleMouseDown( DeleteButtonMouseDown );
		this.AddControl( DeleteButton );
		*/
		
		this.Width = GUIset.GetPixTTT(210);
		this.Height = GUIset.GetPixTTT(50);
		
		InsList = new UserFunctionIns[10];
	}
	
	//添加事件列表
	public void RefreshEventList()
	{
		int StartHeight = GUIset.GetPixTTT(90);
		
		int ILength = 0;
		
		MyControlListLength = 0;
		
		foreach( n_MyObject.MyObject m in G.CGPanel.myModuleList ) {
			if( m.GroupMes != owner.GMes ) {
				continue;
			}
			if( !(m is n_UserFunctionIns.UserFunctionIns) ) {
				continue;
			}
			
			n_UserFunctionIns.UserFunctionIns ui = (n_UserFunctionIns.UserFunctionIns)m;
			if( !ui.isEvent ) {
				continue;
			}
			
			MyButton EventButton = new MyButton( this );
			EventButton.iMes = ILength;
			
			EventButton.Text = ui.UserName.Replace( "?void ", "" ).Replace( "?int32 ", "" ).Replace( "?bool ", "" ).Replace( "?Cstring ", "" ).Replace( "?fix ", "" );
			
			EventButton.X = GUIset.GetPixTTT(10);
			EventButton.Y = StartHeight;
			EventButton.Width = Width - GUIset.GetPixTTT(20);
			EventButton.iMes = ILength;
			EventButton.MyMouseDown = new MyButton.deleMouseDown( EventButtonMouseDown );
			this.AddControl( EventButton );
			
			InsList[ILength] = ui;
			ILength++;
			
			StartHeight += GUIset.GetPixTTT(25);
		}
		this.Height = StartHeight;
	}
	
	//==========================================================
	
	//事件键鼠标按下事件
	void EventButtonMouseDown( object sender )
	{
		UserFunctionIns source = InsList[((MyButton)sender).iMes];
		
		if( source.Target != null ) {
			return;
		}
		
		UserFunctionIns ui = G.CGPanel.GHead.myInsPanel.NewUserFunctionIns( n_GUICommon.GUICommon.SearchName( G.CGPanel ), 0, 0 );
		ui.UserName = n_GroupList.Group.GetFullName( owner.GMes, source.UserName );
		ui.isNewTick = n_MyObject.MyObject.MaxNewTick;
		G.CGPanel.myModuleList.Add( ui );
		ui.InsEnd.Name = n_GUICommon.GUICommon.SearchName( G.CGPanel );
		ui.InsEnd.isNewTick = n_MyObject.MyObject.MaxNewTick;
		G.CGPanel.myModuleList.Add( ui.InsEnd );
		
		if( source.Target != null ) {
			source.Target.Target = null;
		}
		
		source.Target = ui;
		ui.Target = source;
		
		Visible = false;
	}
	
	//名称编辑框文字改变事件
	void NameTextBoxTextChanged( object sender )
	{
		
	}
	
	//删除键鼠标按下事件
	void DeleteButtonMouseDown( object sender )
	{
		
	}
}
}



