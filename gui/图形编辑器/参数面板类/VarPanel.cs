﻿
namespace n_VarPanel
{
using System;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Windows.Forms;
using n_GUIcoder;
using n_MyPanel;
using n_GVar;
using n_MyButton;
using n_MyLabel;
using n_MyTextBox;
using n_GUIset;

//*****************************************************
//变量面板类
public class VarPanel : MyPanel
{
	GVar owner;
	
	public MyTextBox NameTextBox;
	public MyButton NameButton;
	public MyButton DeleteButton;
	public MyButton EditButton;
	public MyButton StoreTypeButton;
	public MyLabel MyLabelType;
	
	//构造函数
	public VarPanel( GVar v ) : base()
	{
		owner = v;
		
		BackBrush = new SolidBrush( Color.FromArgb( 220, Color.Silver ) );
		
		NameTextBox = new MyTextBox( this, owner.Name );
		NameTextBox.isName = true;
		NameTextBox.X = GUIset.GetPixTTT(5);
		NameTextBox.Y = GUIset.GetPixTTT(5);
		NameTextBox.Width = GUIset.GetPixTTT(100);
		NameTextBox.BackColor = Color.WhiteSmoke;
		NameTextBox.MyTextChanged += new MyTextBox.deleTextChanged( NameTextBoxTextChanged );
		this.AddControl( NameTextBox );
		
		DeleteButton = new MyButton( this );
		DeleteButton.AutoSize = true;
		DeleteButton.Text = "删除";
		DeleteButton.X = GUIset.GetPixTTT(5);
		DeleteButton.Y = GUIset.GetPixTTT(40);
		DeleteButton.MyMouseDown += new MyButton.deleMouseDown( DeleteButtonMouseDown );
		this.AddControl( DeleteButton );
		
		StoreTypeButton = new MyButton( this );
		StoreTypeButton.AutoSize = true;
		StoreTypeButton.Text = "存放位置";
		StoreTypeButton.X = GUIset.GetPixTTT(50);
		StoreTypeButton.Y = GUIset.GetPixTTT(40);
		StoreTypeButton.MyMouseDown += new MyButton.deleMouseDown( StoreTypeButtonMouseDown );
		this.AddControl( StoreTypeButton );
		if( owner.VarType == GType.g_int32 || owner.VarType == GType.g_fix || owner.VarType == GType.g_bool ) {
			StoreTypeButton.Visible = true;
		}
		else {
			StoreTypeButton.Visible = false;
		}
		EditButton = new MyButton( this );
		EditButton.AutoSize = true;
		EditButton.Text = "编辑";
		EditButton.X = GUIset.GetPixTTT(110);
		EditButton.Y = GUIset.GetPixTTT(40);
		EditButton.MyMouseDown += new MyButton.deleMouseDown( EditButtonMouseDown );
		this.AddControl( EditButton );
		EditButton.Visible = owner.isConst;
		
		MyLabelType = new MyLabel( this, "类型:" + owner.VarType );
		MyLabelType.X = GUIset.GetPixTTT(110);
		MyLabelType.Y = GUIset.GetPixTTT(5);
		this.AddControl( MyLabelType );
		
		this.Width = GUIset.GetPixTTT(250);
		this.Height = GUIset.GetPixTTT(100);
	}
	
	//文字改变事件
	void NameTextBoxTextChanged( object sender )
	{
		if( this.NameTextBox.Text != null ) {
			
			string OldName = owner.Name;
			string NewName = this.NameTextBox.Text;
			
			if( isNameError( OldName, NewName ) ) {
				this.NameTextBox.ignoreTextChanged = true;
				this.NameTextBox.Text = OldName;
				this.NameTextBox.ignoreTextChanged = false;
				return;
			}
			owner.Name = NewName;
			
			G.CGPanel.RenameModule( owner, true, OldName, NewName );
		}
	}
	
	//删除键鼠标按下事件
	void DeleteButtonMouseDown( object sender )
	{
		if( G.SimulateMode ) {
			n_Debug.Warning.WarningMessage = "仿真模式下无法删除模块, 请先结束仿真";
			return;
		}
		if( MessageBox.Show( "您确定删除当前组件吗?", "组件删除确认", MessageBoxButtons.OKCancel ) == DialogResult.OK ) {
			owner.Remove();
		}
	}
	
	//存放位置键鼠标按下事件
	void StoreTypeButtonMouseDown( object sender )
	{
		string StoreType = G.extendEXPBox.Run( "?memory" );
		if( StoreType != null ) {
			StoreType = StoreType.Remove( 0, StoreType.IndexOf( ' ' ) + 1 );
			if( StoreType.IndexOf( "默认" ) != -1 ) {
				owner.StoreType = "";
			}
			else {
				owner.StoreType = StoreType.Replace( ' ', '.' );
			}
		}
	}
	
	//编辑键鼠标按下事件
	void EditButtonMouseDown( object sender )
	{
		string s = null;
		if( owner.VarType == GType.g_int32 || owner.VarType == GType.g_fix || owner.VarType == GType.g_bool || owner.VarType == GType.g_Astring ) {
			if( G.MyTextInputBox == null ) {
				G.MyTextInputBox = new n_TextForm.TextForm();
			}
			s = G.MyTextInputBox.Run( owner.Value );
		}
		//else if( owner.VarType == GType.g_Rstring ) {
		//	s = G.LatticeBox.RunRString( owner.Value );
		//}
		else if( owner.VarType == GType.g_music ) {
			if( G.MusicBox == null ) {
				G.MusicBox = new n_MusicForm.MusicForm( null, false );
			}
			s = G.MusicBox.Run( owner.Value );
		}
		else {
			MessageBox.Show( "<EditButtonMouseDown> 未处理的类型: " + owner.VarType );
		}
		if( s != null ) {
			owner.Value = s;
		}
	}
}
}



