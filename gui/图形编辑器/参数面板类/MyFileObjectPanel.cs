﻿
namespace n_MyFileObjectPanel
{
using System;
using System.Drawing;
using System.Windows.Forms;

using c_ControlType;
using c_MyObjectSet;
using n_EventLink;
using n_GUIcoder;
using n_GUIset;
using n_HardModule;
using n_MyButton;
using n_MyFileObject;
using n_MyLabel;
using n_MyPanel;
using n_MyTextBox;
using n_UIModule;

using n_EventIns;
using n_CondiEndIns;
using n_GUICommon;
using n_GUIset;

//*****************************************************
//硬件组件面板类
public class MyFileObjectPanel : MyPanel
{
	public MyFileObject owner;
	
	public MyTextBox NameTextBox;
	public MyButton DeleteButton;
	public MyButton RolLeftButton;
	public MyButton RolRightButton;
	public MyButton SwapButton;
	public MyButton MesButton;
	public MyButton ExampleButton;
	
	MyButton[] EventButtonList;
	MyButton[] EventDeleteList;
	
	MyLabel[] VarLabelList;
	MyTextBox[] VarTextBoxList;
	
	//构造函数
	public MyFileObjectPanel( MyFileObject h ) : base()
	{
		owner = h;
		
		BackBrush = new SolidBrush( Color.FromArgb( 220, Color.Silver ) );
		
		NameTextBox = new MyTextBox( this, owner.Name );
		NameTextBox.isName = true;
		NameTextBox.X = GUIset.GetPixTTT(5);
		NameTextBox.Y = GUIset.GetPixTTT(5);
		NameTextBox.Width = GUIset.GetPixTTT(110);
		NameTextBox.BackColor = Color.WhiteSmoke;
		NameTextBox.MyTextChanged += new MyTextBox.deleTextChanged( NameTextBoxTextChanged );
		this.AddControl( NameTextBox );
		
		DeleteButton = new MyButton( this );
		DeleteButton.Text = n_Language.Language.MFP_Delete;
		DeleteButton.X = GUIset.GetPixTTT(5);
		DeleteButton.Y = GUIset.GetPixTTT(40);
		DeleteButton.ForeColor = Color.OrangeRed;
		//DeleteButton.BackColor = Color.LightPink;
		DeleteButton.FrameColor = Color.OrangeRed;
		DeleteButton.hForeColor = Color.OrangeRed;
		//DeleteButton.hBackColor = Color.WhiteSmoke;
		DeleteButton.hFrameColor = Color.OrangeRed;
		DeleteButton.MyMouseDown += new MyButton.deleMouseDown( DeleteButtonMouseDown );
		this.AddControl( DeleteButton );
		
		RolLeftButton = new MyButton( this );
		RolLeftButton.Text = n_Language.Language.MFP_Left;
		RolLeftButton.X = GUIset.GetPixTTT(55);
		RolLeftButton.Y = GUIset.GetPixTTT(40);
		//RolLeftButton.ForeColor = Color.Blue;
		//RolLeftButton.BackColor = Color.SlateBlue;
		RolLeftButton.FrameColor = Color.RoyalBlue;
		RolLeftButton.hForeColor = Color.RoyalBlue;
		//RolLeftButton.hBackColor = Color.SlateBlue;
		RolLeftButton.hFrameColor = Color.RoyalBlue;
		RolLeftButton.MyMouseDown += new MyButton.deleMouseDown( RolLeftButtonMouseDown );
		if( this.owner is HardModule ) {
			this.AddControl( RolLeftButton );
		}
		
		RolRightButton = new MyButton( this );
		RolRightButton.Text = n_Language.Language.MFP_Right;
		RolRightButton.X = GUIset.GetPixTTT(105);
		RolRightButton.Y = GUIset.GetPixTTT(40);
		//RolRightButton.ForeColor = Color.Blue;
		//RolRightButton.BackColor = Color.SlateBlue;
		RolRightButton.FrameColor = Color.RoyalBlue;
		RolRightButton.hForeColor = Color.RoyalBlue;
		//RolRightButton.hBackColor = Color.SlateBlue;
		RolRightButton.hFrameColor = Color.RoyalBlue;
		RolRightButton.MyMouseDown += new MyButton.deleMouseDown( RolRightButtonMouseDown );
		if( this.owner is HardModule ) {
			this.AddControl( RolRightButton );
		}
		
		if( owner.CanSwap ) {
			SwapButton = new MyButton( this );
			SwapButton.Text = n_Language.Language.MFP_Swap;
			SwapButton.X = GUIset.GetPixTTT(155);
			SwapButton.Y = GUIset.GetPixTTT(40);
			//SwapButton.ForeColor = Color.Blue;
			//SwapButton.BackColor = Color.SlateBlue;
			SwapButton.FrameColor = Color.RoyalBlue;
			SwapButton.hForeColor = Color.RoyalBlue;
			//SwapButton.hBackColor = Color.SlateBlue;
			SwapButton.hFrameColor = Color.RoyalBlue;
			SwapButton.MyMouseDown += new MyButton.deleMouseDown( SwapButtonMouseDown );
			if( this.owner is HardModule ) {
				this.AddControl( SwapButton );
			}
		}
		
		MesButton = new MyButton( this );
		MesButton.Text = n_Language.Language.MFP_Mes;
		MesButton.X = GUIset.GetPixTTT(120);
		MesButton.Y = GUIset.GetPixTTT(5);
		MesButton.Height = GUIset.GetPixTTT(25);
		//MesButton.ForeColor = Color.LightGreen;
		//MesButton.BackColor = Color.LightGreen;
		MesButton.FrameColor = Color.Green;
		MesButton.hForeColor = Color.Green;
		MesButton.hBackColor = Color.White;
		MesButton.hFrameColor = Color.Green;
		MesButton.MyMouseDown += new MyButton.deleMouseDown( MesButtonMouseDown );
		//MesButton.MyMouseEnter += new MyButton.deleMouseEnter( MesButtonMouseDown );
		this.AddControl( MesButton );
		
		ExampleButton = new MyButton( this );
		ExampleButton.Text = n_Language.Language.MFP_Example;
		ExampleButton.X = GUIset.GetPixTTT(165);
		ExampleButton.Y = GUIset.GetPixTTT(5);
		ExampleButton.Height = GUIset.GetPixTTT(25);
		//ExampleButton.ForeColor = Color.LightGreen;
		//ExampleButton.BackColor = Color.LightGreen;
		ExampleButton.FrameColor = Color.Green;
		ExampleButton.hForeColor = Color.Green;
		ExampleButton.hBackColor = Color.White;
		ExampleButton.hFrameColor = Color.Green;
		ExampleButton.MyMouseDown += new MyButton.deleMouseDown( ExampleButtonMouseDown );
		this.AddControl( ExampleButton );
		
		int StartHeight = GUIset.GetPixTTT(80);
		if( owner is UIModule ) {
			AddUIModuleEXList( ref StartHeight );
		}
		else {
			AddHardModuleEXList( ref StartHeight );
		}
		AddVarList( ref StartHeight );
		AddEventList( ref StartHeight );
		
		this.Width = GUIset.GetPixTTT(210);
		this.Height = StartHeight;
	}
	
	//添加参数列表
	void AddVarList( ref int StartHeight )
	{
		int ListNumber = owner.VarList.Length;
		VarLabelList = new MyLabel[ ListNumber ];
		VarTextBoxList = new MyTextBox[ ListNumber ];
		for( int i = 0; i < ListNumber; ++i ) {
			string VarName = owner.VarList[ i ][ owner.GetLC() ];
			
			VarLabelList[ i ] = new MyLabel( this, VarName );
			VarLabelList[ i ].X = GUIset.GetPixTTT(5);
			VarLabelList[ i ].Y = StartHeight;
			VarLabelList[ i ].Width = GUIset.GetPixTTT(140);
			VarLabelList[ i ].iMes = i;
			this.AddControl( VarLabelList[ i ] );
			
			string[] VarTypeList = owner.VarList[ i ][ 1 ].Split( '_' );
			string VarType = VarTypeList[ 1 ];
			if( VarType == GType.g_int8 ||
			    VarType == GType.g_int16 ||
			    VarType == GType.g_int32 ||
			    VarType == GType.g_fix ||
			    VarType == GType.g_uint8 ||
			    VarType == GType.g_uint16 ||
			    VarType == GType.g_uint32 ||
			    VarType == GType.g_bit
			   ) {
				VarTextBoxList[ i ] = new MyTextBox( this, owner.VarList[ i ][ 0 ] );
				VarTextBoxList[ i ].isNumber = true;
				VarTextBoxList[ i ].X = GUIset.GetPixTTT(120);
				VarTextBoxList[ i ].Y = StartHeight;
				VarTextBoxList[ i ].Width = GUIset.GetPixTTT(80);
				VarTextBoxList[ i ].iMes = i;
				VarTextBoxList[ i ].BackColor = Color.WhiteSmoke;
				VarTextBoxList[ i ].MyTextChanged = new MyTextBox.deleTextChanged( VarTextBoxTextChanged );
				this.AddControl( VarTextBoxList[ i ] );
				
				//设置默认值
				if( owner.VarList[ i ][ 0 ] == "" ) {
					owner.VarList[ i ][ 0 ] = VarTypeList[ 2 ];
					VarTextBoxList[ i ].Text = owner.VarList[ i ][ 0 ];
				}
			}
			else {
				MessageBox.Show( "<AddVarList> 未处理的类型: " + VarType );
			}
			
			//设置备注信息
			if( VarTypeList.Length >= 4 ) {
				VarLabelList[ i ].Text += "(" + VarTypeList[3] + ")";
			}
			
			StartHeight += GUIset.GetPixTTT(30);
		}
	}
	
	//添加事件列表
	void AddEventList( ref int StartHeight )
	{
		int ListNumber = owner.EventList.Length;
		EventButtonList = new MyButton[ ListNumber ];
		EventDeleteList = new MyButton[ ListNumber ];
		
		int W = GUIset.GetPixTTT(200);
		bool Cm = false;
		if( ListNumber > 8 ) {
			Cm = true;
			W = GUIset.GetPixTTT(98);
		}
		int sx = GUIset.GetPixTTT(5);
		int yinc = 0;
		for( int i = 0; i < ListNumber; ++i ) {
			
			string EventName = owner.EventList[ i ][ owner.GetLC() ];
			
			EventButtonList[ i ] = new MyButton( this );
			EventButtonList[ i ].Text = EventName;
			EventButtonList[ i ].X = sx;
			EventButtonList[ i ].Y = StartHeight;
			EventButtonList[ i ].Width = W;
			EventButtonList[ i ].iMes = i;
			EventButtonList[ i ].MyMouseDown = new MyButton.deleMouseDown( EventButtonMouseDown );
			this.AddControl( EventButtonList[ i ] );
			
			yinc = (int)EventButtonList[ i ].Height + GUIset.GetPixTTT(5);
			
			if( Cm ) {
				if( sx == GUIset.GetPixTTT(5) ) {
					sx = GUIset.GetPixTTT(108);
				}
				else {
					sx = GUIset.GetPixTTT(5);
					StartHeight += yinc;
				}
			}
			else {
				StartHeight += yinc;
			}
		}
		if( Cm && sx != GUIset.GetPixTTT(5) ) {
			StartHeight += yinc;
		}
	}
	
	//添加图形控件属性列表
	void AddUIModuleEXList( ref int StartHeight )
	{
		UIModule u = (UIModule)owner;
		string[] exList = u.UIModuleEXList;
		if( exList == null ) {
			return;
		}
		float StartX = GUIset.GetPixTTT(5);
		for( int i = 0; i < exList.Length; ++i ) {
			if( exList[ i ] == "背景颜色" ) {
				MyButton b = new MyButton( this );
				SetButtonColor( b, u.BackColor );
				b.Text = "背景颜色";
				b.X = StartX;
				b.Y = StartHeight;
				b.Width = GUIset.GetPixTTT(64);
				//b.Height = GUIset.GetPixTTT(25);
				b.MyMouseDown = new MyButton.deleMouseDown( BackColorButtonMouseDown );
				this.AddControl( b );
				StartX += b.Width + GUIset.GetPixTTT(4);
			}
			else if( exList[ i ] == "边框颜色" ) {
				MyButton b = new MyButton( this );
				SetButtonColor( b, u.EdgeColor );
				b.Text = "边框颜色";
				b.X = StartX;
				b.Y = StartHeight;
				b.Width = GUIset.GetPixTTT(64);
				//b.Height = GUIset.GetPixTTT(25);
				b.MyMouseDown = new MyButton.deleMouseDown( FrameColorButtonMouseDown );
				this.AddControl( b );
				StartX += b.Width + GUIset.GetPixTTT(4);
			}
			else if( exList[ i ] == "文字颜色" ) {
				MyButton b = new MyButton( this );
				SetButtonColor( b, u.ForeColor );
				b.Text = "文字颜色";
				b.X = StartX;
				b.Y = StartHeight;
				b.Width = GUIset.GetPixTTT(64);
				//b.Height = GUIset.GetPixTTT(25);
				b.MyMouseDown = new MyButton.deleMouseDown( ForeColorButtonMouseDown );
				this.AddControl( b );
				StartX += b.Width + GUIset.GetPixTTT(4);
			}
			else if( exList[ i ] == "字体" ) {
				MyButton b = new MyButton( this );
				//b.BackColor = u.ForeColor;
				//b.hBackColor = u.ForeColor;
				b.Text = "字体";
				b.X = StartX;
				b.Y = StartHeight;
				//b.Width = GUIset.GetPixTTT(80);
				b.Height = GUIset.GetPixTTT(25);
				b.MyMouseDown = new MyButton.deleMouseDown( FontButtonMouseDown );
				this.AddControl( b );
				StartX += b.Width + GUIset.GetPixTTT(5);
				
				StartHeight += GUIset.GetPixTTT(30);
				StartX = GUIset.GetPixTTT(5);
			}
			else if( exList[ i ] == "文字" ) {
				MyTextBox t = new MyTextBox( this, this.owner.Text );
				t.X = StartX;
				t.Y = StartHeight;
				t.Width = GUIset.GetPixTTT(155);
				//t.Height = GUIset.GetPixTTT(25);
				t.MyTextChanged = new MyTextBox.deleTextChanged( EXTextBoxTextChanged );
				this.AddControl( t );
				StartX += t.Width + GUIset.GetPixTTT(5);
			}
			else if( exList[ i ].StartsWith( "button+" ) ) {
				string[] cut = exList[ i ].Split( '+' );
				MyButton b = new MyButton( this );
				b.BackColor = Color.LightGreen;
				StartHeight += GUIset.GetPixTTT(30);
				b.X = GUIset.GetPixTTT(5);
				b.Y = StartHeight;
				b.Width = GUIset.GetPixTTT(200);
				//b.Height = 25;
				b.Text = cut[1];
				b.MyMouseDown = ExtendButtonMouseDown;
				this.AddControl( b );
			}
			else {
				n_Debug.Warning.BUG( "未处理的控件属性: " + exList[ i ] );
			}
		}
		StartHeight += GUIset.GetPixTTT(30);
	}
	
	//添加硬件属性列表
	void AddHardModuleEXList( ref int StartHeight )
	{
		HardModule u = (HardModule)owner;
		string[] exList = u.UIModuleEXList;
		if( exList == null ) {
			return;
		}
		for( int i = 0; i < exList.Length; ++i ) {
			if( exList[ i ].StartsWith( "button+" ) ) {
				string[] cut = exList[ i ].Split( '+' );
				MyButton b = new MyButton( this );
				b.BackColor = Color.LightGreen;
				StartHeight += 30;
				b.X = 5;
				b.Y = StartHeight;
				b.Width = 200;
				b.Height = 25;
				b.Text = cut[1];
				b.MyMouseDown = ExtendButtonMouseDown;
				this.AddControl( b );
			}
			//else {
			//	G.FlashBox.Run( "未处理的控件属性: " + exList[ i ] );
			//}
		}
		StartHeight += 30;
	}
	
	//==========================================================
	
	//文字框文字改变事件
	void EXTextBoxTextChanged( object sender )
	{
		MyTextBox tBox = (MyTextBox)sender;
		owner.Text = tBox.Text;
	}
	
	//背景颜色按钮单击事件
	void BackColorButtonMouseDown( object sender )
	{
		if( n_wlibCom.ColorDlg.ShowColorDlgOk( owner.BackColor ) ) {
			SetButtonColor( (MyButton)sender, n_wlibCom.ColorDlg.SelectColor );
			((UIModule)owner).BackColor = n_wlibCom.ColorDlg.SelectColor;
		}
	}
	
	//边框颜色按钮单击事件
	void FrameColorButtonMouseDown( object sender )
	{
		if( n_wlibCom.ColorDlg.ShowColorDlgOk( owner.EdgeColor ) ) {
			SetButtonColor( (MyButton)sender, n_wlibCom.ColorDlg.SelectColor );
			((UIModule)owner).EdgeColor = n_wlibCom.ColorDlg.SelectColor;
		}
	}
	
	//文字颜色按钮单击事件
	void ForeColorButtonMouseDown( object sender )
	{
		if( n_wlibCom.ColorDlg.ShowColorDlgOk( owner.ForeColor ) ) {
			SetButtonColor( (MyButton)sender, n_wlibCom.ColorDlg.SelectColor );
			((UIModule)owner).ForeColor = n_wlibCom.ColorDlg.SelectColor;
		}
	}
	
	//字体按钮单击事件
	void FontButtonMouseDown( object sender )
	{
		FontDialog fontBox = new FontDialog();
		fontBox.Font = ((UIModule)owner).TextFont;
		DialogResult d = fontBox.ShowDialog();
		if( d == DialogResult.OK ) {
			((UIModule)owner).TextFont = fontBox.Font;
			//if( ((UIModule)owner).GetControlType() == ControlType.NumberBox ) {
			//	((UIModule)owner).Height = (int)GUIset.mg.MeasureString( "0", fontBox.Font ).Height;
			//}
		}
	}
	
	//扩展按钮按下事件
	void ExtendButtonMouseDown( object sender )
	{
		MyButton eb = (MyButton)sender;
		
		if( this.owner.ImageName == SPMoudleName.SYS_Keyboard ) {
			if( G.KeyboardBox == null ) {
				G.KeyboardBox = new n_KeyboardForm.KeyboardForm();
			}
			string s = G.KeyboardBox.Run( owner.ExtendValue );
			if( s != null ) {
				owner.ExtendValue = s;
			}
		}
		else if( this.owner.ImageName == ControlType.MIDI ) {
			if( G.YSelectBox == null ) {
				G.YSelectBox = new n_YSelectForm.YSelectForm();
			}
			string s = G.YSelectBox.Run();
			if( s != null ) {
				owner.ExtendValue = s;
			}
		}
		else if( this.owner.ImageName == ControlType.Sound ) {
			string s = OpenFile();
			if( s != null ) {
				owner.ExtendValue += s + "*";
			}
			else {
				owner.ExtendValue = "";
			}
		}
		else if( this.owner.ImageName == ControlType.ControlPad ) {
			if( G.ControlSetBox == null ) {
				G.ControlSetBox = new n_ControlSetForm.ControlSetForm();
			}
			string s = G.ControlSetBox.Run( owner.ExtendValue );
			if( s != null ) {
				owner.ExtendValue = s;
			}
		}
		else if( this.owner.ImageName == ControlType.ImagePanel ) {
			if( G.ImagePanelSetBox == null ) {
				G.ImagePanelSetBox = new n_ImagePanelSetForm.ImagePanelSetForm();
			}
			string s = G.ImagePanelSetBox.Run( owner.ExtendValue );
			if( s != null ) {
				owner.ExtendValue = s;
			}
		}
		else if( this.owner.ImageName == ControlType.OpenCV ) {
			//string s = G.VideoBox.Run();
			//if( s != null ) {
			//	owner.ExtendValue = s;
			//}
		}
		else if( this.owner.ImageName == ControlType.Sprite ) {
			if( G.SpriteSetBox == null ) {
				G.SpriteSetBox = new n_SpriteSetForm.SpriteSetForm();
			}
			string s = G.SpriteSetBox.Run( (n_SG_MySprite.MySprite)(((UIModule)owner).myControl), owner.ExtendValue );
			if( s != null ) {
				owner.ExtendValue = s;
			}
		}
		//-------------------------------------------------------------
		else if( this.owner.ImageName == "Date" ) {
			if( eb.Text == "设置存储位置" ) {
				string StoreType = G.extendEXPBox.Run( "?memory" );
				if( StoreType != null ) {
					StoreType = StoreType.Remove( 0, StoreType.IndexOf( ' ' ) + 1 );
					if( StoreType.IndexOf( "默认" ) != -1 ) {
						owner.ExtendValue = "";
					}
					else {
						owner.ExtendValue = StoreType.Replace( ' ', '.' );
					}
				}
			}
			if( eb.Text == "设置当前时间" ) {
				if( G.timeEXPBox == null ) {
					G.timeEXPBox = new n_timeEXPForm.timeEXPForm();
				}
				string ConstTime = G.timeEXPBox.Run( false, null );
				if( ConstTime != null ) {
					if( ConstTime == null ) {
						owner.ExtendValue1 = "";
					}
					else {
						owner.ExtendValue1 = ConstTime;
					}
				}
			}
		}
		else if( eb.Text == "装载ROM数据" ) {
			n_CCEngine.CCEngine.LoadROM();
		}
		else if( eb.Text == "绑定芯片引脚" ) {
			if( G.PinBox == null ) {
				G.PinBox = new n_PinForm.PinForm();
			}
			string s = G.PinBox.Run( owner.ExtendValue );
			if( s != null ) {
				owner.ExtendValue = s;
			}
		}
		else {
			MessageBox.Show( "未知的对象类型: " + this.owner.ImageName );
		}
	}
	
	//添加图片
	string OpenFile()
	{
		//打开文件对话框
		OpenFileDialog OpenFileDlg = new OpenFileDialog();
		OpenFileDlg.Filter = "所有类型文件 | *.*";
		OpenFileDlg.Title = "请选择文件";
		
		if( !G.ccode.isStartFile ) {
			OpenFileDlg.InitialDirectory = G.ccode.FilePath;
		}
		else {
			string Mes = "您的这个文件尚未保存到电脑硬盘上, 所以无法设置图片资源的默认文件夹,\n";
			Mes += "强烈建议您先保存一下这个文件到电脑上, 再选择图片路径, 这样方便系统记录您的默认文件夹.\n";
			Mes += "否则系统将会使用全局文件路径记录, 这样会导致您的vex程序复制到其他目录后运行异常.\n";
			MessageBox.Show( Mes );
		}
		
		DialogResult dlgResult = OpenFileDlg.ShowDialog();
		if(dlgResult == DialogResult.OK) {
			if( OpenFileDlg.FileName.StartsWith( G.ccode.FilePath ) ) {
				return OpenFileDlg.FileName.Remove( 0, G.ccode.FilePath.Length );
			}
			else {
				return OpenFileDlg.FileName;
			}
		}
		return null;
	}
	
	//设置按钮颜色属性
	void SetButtonColor( MyButton b, Color bc )
	{
		Color fc;
		if( (bc.R + bc.G + bc.B) / 3 > 120 ) {
			fc = Color.Black;
		}
		else {
			fc = Color.White;
		}
		b.BackColor = bc;
		b.hBackColor = bc;
		b.ForeColor = fc;
		b.hForeColor = fc;
	}
	
	//==========================================================
	
	//变量参数文字改变事件
	void VarTextBoxTextChanged( object sender )
	{
		MyTextBox m = (MyTextBox)sender;
		if( m.Text != null ) {
			
			string OldText = owner.VarList[ m.iMes ][ 0 ];
			string NewText = m.Text;
			
			if( isNumberError( OldText, NewText ) ) {
				m.ignoreTextChanged = true;
				m.Text = OldText;
				m.ignoreTextChanged = false;
				return;
			}
			
			owner.VarList[ m.iMes ][ 0 ] = m.Text;
		}
	}
	
	//事件键鼠标按下事件
	void EventButtonMouseDown( object sender )
	{
		MyButton mb = (MyButton)sender;
		int NameIndex = mb.iMes;
		string EventName = owner.EventList[ NameIndex ][ owner.GetLC() ];
		
		if( owner.EventList[ NameIndex ][ 0 ] == "" ) {
			owner.EventList[ NameIndex ][ 0 ] = owner.Name + "_" + EventName;
			
			EventIns m = new EventIns();
			m.isNewTick = n_MyObject.MyObject.MaxNewTick;
			
			m.SetUserValue( GUICommon.SearchName( G.CGPanel ), null, owner.EventList[ NameIndex ][ 0 ], 0, 0, 0, true );
			owner.myObjectList.Add( m );
			
			n_CondiEndIns.CondiEndIns ee = new n_CondiEndIns.CondiEndIns();
			ee.SetUserValue( GUICommon.SearchName( G.CGPanel ), null, 200, 280, 0 );
			ee.isNewTick = n_MyObject.MyObject.MaxNewTick;
			ee.CanLinked = false;
			ee.InsStart = m;
			owner.myObjectList.Add( ee );
			
			m.InsEnd = ee;
			m.NextIns = ee;
			ee.PreIns = m;
			
			//添加事件列表
			ELNote e = new ELNote( owner.Name, owner.EventList[ NameIndex ][ 2 ], owner.EventList[ NameIndex ][ 0 ] );
			owner.myObjectList.GPanel.EL.Add( e );
			
			if( G.isCruxEXE ) {
				m.ShowFollow = false;
				m.SetFollowVisible( false );
			}
			
			GUIcoder.EventFuncCode = "task void " + owner.Name + "_" + EventName + "()\n{\n\t//这里补充事件处理代码\n}";
		}
		owner.isHighLight = false;
		owner.myObjectList.MouseOnObject = null;
		
		//...搜索对应的事件标签并移动
	}
	
	//事件删除键鼠标按下事件
	void EventDeleteMouseDown( object sender )
	{
		if( G.SimulateMode ) {
			n_Debug.Warning.WarningMessage = "仿真模式下无法删除模块, 请先结束仿真";
			return;
		}
		
		MyButton m = (MyButton)sender;
		int NameIndex = m.iMes;
		string EventEntry = owner.EventList[ NameIndex ][ 0 ];
		
		for( int i = 0; i < owner.myObjectList.MLength; ++i ) {
			if( owner.myObjectList.ModuleList[ i ] == null ) {
				continue;
			}
			if( owner.myObjectList.ModuleList[ i ] is EventIns ) {
				EventIns el = (EventIns)owner.myObjectList.ModuleList[ i ];
				if( el.EventEntry == EventEntry ) {
					el.Remove();
					break;
				}
			}
		}
		owner.EventList[ NameIndex ][ 0 ] = "";
		EventDeleteList[ NameIndex ].Visible = false;
		owner.myObjectList.GPanel.EL.Delete( owner, owner.EventList[ NameIndex ][ 2 ] );
	}
	
	//名称编辑框文字改变事件
	void NameTextBoxTextChanged( object sender )
	{
		if( this.NameTextBox.Text != null ) {
			
			string OldName = owner.Name;
			string NewName = this.NameTextBox.Text;
			
			if( isNameError( OldName, NewName ) ) {
				this.NameTextBox.ignoreTextChanged = true;
				this.NameTextBox.Text = OldName;
				this.NameTextBox.ignoreTextChanged = false;
				return;
			}
			owner.Name = NewName;
			
			G.CGPanel.RenameModule( owner, false, OldName, NewName );
			
			//修改事件列表
			for( int ei = 0; ei < owner.EventList.Length; ++ei ) {
				string EventEntry = owner.EventList[ ei ][ 0 ];
				if( EventEntry == "" ) {
					continue;
				}
				owner.EventList[ ei ][ 0 ] = owner.Name + "_" + owner.EventList[ ei ][ owner.GetLC() ];
			}
		}
	}
	
	//删除键鼠标按下事件
	void DeleteButtonMouseDown( object sender )
	{
		if( G.SimulateMode ) {
			n_Debug.Warning.WarningMessage = "仿真模式下无法删除模块, 请先结束仿真";
			return;
		}
		if( MessageBox.Show( "您确定删除当前组件吗?", "组件删除确认", MessageBoxButtons.OKCancel ) == DialogResult.OK ) {
			owner.Remove();
			G.CGPanel.RefreshResourceError();
		}
	}
	
	//左旋键鼠标按下事件
	void RolLeftButtonMouseDown( object sender )
	{
		HardModule hm = (HardModule)owner;
		if( hm.PackOwner != null ) {
			MessageBox.Show( "组合类模块不支持旋转", "提示" );
			return;
		}
		hm.Rol( -1 );
	}
	
	//右旋键鼠标按下事件
	void RolRightButtonMouseDown( object sender )
	{
		HardModule hm = (HardModule)owner;
		if( hm.PackOwner != null ) {
			MessageBox.Show( "组合类模块不支持旋转", "提示" );
			return;
		}
		hm.Rol( 1 );
	}
	
	void SwapButtonMouseDown( object sender )
	{
		((HardModule)owner).Swap();
	}
	
	//信息键鼠标按下事件
	void MesButtonMouseDown( object sender )
	{
		owner.isHighLight = false;
		owner.myObjectList.MouseOnObject = null;
		
		G.DescriptionBox.Run( owner );
	}
	
	//示例键鼠标按下事件
	void ExampleButtonMouseDown( object sender )
	{
		owner.isHighLight = false;
		owner.myObjectList.MouseOnObject = null;
		
		if( G.ExampleBox == null ) {
			G.ExampleBox = new n_ExampleForm.ExampleForm();
		}
		G.ExampleBox.Run( owner );
	}
}
}



