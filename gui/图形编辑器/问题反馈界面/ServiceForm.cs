﻿
namespace n_ServiceForm
{
using System;
using System.Drawing;
using System.Windows.Forms;
using n_Email;
using n_Web;

/// <summary>
/// Description of WorkForm.
/// </summary>
public partial class ServiceForm : Form
{
	string mailService;
	string mailAddr;
	string PassCode;
	
	bool isError;
	
	//主窗口
	public ServiceForm()
	{
		InitializeComponent();
		
		string t = n_OS.VIO.OpenTextFileGB2312( n_OS.OS.SystemRoot + "Resource" + n_OS.OS.PATH_S + "email.txt" );
		string[] c = t.Split( '\n' );
		mailService = c[0];
		mailAddr = c[1];
		PassCode = c[2];
		
		isError = false;
	}
	
	//运行
	public void Run()
	{
		isError = false;
		labelMes.Text = "如果您的程序无法工作或者遇到一些难以解决的问题，请在问题描述栏详细填写您遇到的问题，点击发送按钮后将会发送给我们。";
		
		if( n_Debug.Warning.BugMessage != null ) {
			isError = true;
			labelMes.Text = "已记录软件遇到的问题，点击发送按钮后将会发送给我们，以便改进软件，谢谢！";
			richTextBoxMes.Text = "问题记录:\n" + n_Debug.Warning.BugMessage;
			richTextBoxMes.ForeColor = Color.Black;
		}
		this.Visible = true;
	}
	
	//窗体关闭事件
	void UnitLibFormFormClosing(object sender, FormClosingEventArgs e)
	{
		e.Cancel = true;
		this.Visible = false;
	}
	
	void ButtonSendClick(object sender, EventArgs e)
	{
		if( !isError && MessageBox.Show( "为了防止部分用户恶意提交无效的反馈信息, 系统会上传您的IP地址，如后台审核到同一IP多次提交无效信息，会封禁您的IP，可能会导致后续软件使用出现问题。您确定要提交吗？",
		                     "IP上传提示", MessageBoxButtons.YesNo ) == DialogResult.No ) {
			return;
		}
		
		/*
		string mailService = "smtp.qq.com";
		string mailAddr = "xbd2048@qq.com";
		string PassCode = "ofxgaepddzovbbcg";
		*/
		
		string At = null;
		if( checkBoxAt.Checked ) {
			At = G.ccode.PathAndName;
		}
		
		string Mes = textBoxMes.Text + "\n";
		Mes += "版本: " + G.VersionShow + " " + G.VersionMessage + "\n";
		Mes += richTextBoxMes.Text;
		
		//发送邮件
		if( Email.SendMailSelf( mailService, mailAddr, PassCode, "linkboy客户问题反馈", Mes, At ) ) {
			MessageBox.Show( "我们已收到您的信息, 重要问题建议通过官方的QQ群或者客服电话沟通（本页面功能类似于单向的问卷调查形式）" );
		}
		else {
			MessageBox.Show( "问题发送失败, 请您尝试重新提交, 或者改用邮件方式将问题信息发送给客服人员邮箱: " + mailAddr );
		}
	}
	
	void ButtonFTPClick(object sender, EventArgs e)
	{
		string[] c = richTextBoxFTP.Text.Split( '\n' );
		
		string Addr = "FTP://byu2623270001.my3w.com/htdocs";
		string UserName = "byu2623270001";
		string Password = "net19870304";
		
		Addr = c[0];
		UserName = c[1];
		Password = c[2];
		
		string s = n_Web.FTP.UploadFile( G.ccode.PathAndName, Addr, UserName, Password );
		
		if( s != null ) {
			MessageBox.Show( s );
		}
		else {
			MessageBox.Show( "文件上传成功" );
		}
	}
	
	void RichTextBoxMesClick(object sender, EventArgs e)
	{
		if( richTextBoxMes.Text == "请在这里输入您遇到的问题" ) {
			richTextBoxMes.Text = "";
			richTextBoxMes.ForeColor = Color.Black;
		}
	}
}
}



