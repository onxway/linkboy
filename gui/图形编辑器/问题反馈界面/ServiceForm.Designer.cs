﻿/*
 * 由 SharpDevelop 创建。
 * 用户: bbb_2
 * 日期: 2009-11-17
 * 时间: 11:15
 * 
 * 如果你想要更改该模板，那么请使用“工具 | 选项 | 编辑 | 编辑标准标题”。
 */
namespace n_ServiceForm
{
	partial class ServiceForm
	{
		/// <summary>
		/// Designer variable used to keep track of non-visual components.
		/// </summary>
		private System.ComponentModel.IContainer components = null;
		
		/// <summary>
		/// Disposes resources used by the form.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing) {
				if (components != null) {
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}
		
		/// <summary>
		/// This method is required for Windows Forms designer support.
		/// Do not change the method contents inside the source code editor. The Forms designer might
		/// not be able to load this method if it was changed manually.
		/// </summary>
		private void InitializeComponent()
		{
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ServiceForm));
			this.buttonSend = new System.Windows.Forms.Button();
			this.labelMes = new System.Windows.Forms.Label();
			this.richTextBoxMes = new System.Windows.Forms.RichTextBox();
			this.checkBoxAt = new System.Windows.Forms.CheckBox();
			this.textBoxMes = new System.Windows.Forms.TextBox();
			this.buttonFTP = new System.Windows.Forms.Button();
			this.richTextBoxFTP = new System.Windows.Forms.RichTextBox();
			this.label3 = new System.Windows.Forms.Label();
			this.SuspendLayout();
			// 
			// buttonSend
			// 
			this.buttonSend.BackColor = System.Drawing.Color.SandyBrown;
			resources.ApplyResources(this.buttonSend, "buttonSend");
			this.buttonSend.Name = "buttonSend";
			this.buttonSend.UseVisualStyleBackColor = false;
			this.buttonSend.Click += new System.EventHandler(this.ButtonSendClick);
			// 
			// labelMes
			// 
			resources.ApplyResources(this.labelMes, "labelMes");
			this.labelMes.ForeColor = System.Drawing.Color.Black;
			this.labelMes.Name = "labelMes";
			// 
			// richTextBoxMes
			// 
			this.richTextBoxMes.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
			resources.ApplyResources(this.richTextBoxMes, "richTextBoxMes");
			this.richTextBoxMes.ForeColor = System.Drawing.Color.DarkGray;
			this.richTextBoxMes.Name = "richTextBoxMes";
			this.richTextBoxMes.Click += new System.EventHandler(this.RichTextBoxMesClick);
			// 
			// checkBoxAt
			// 
			this.checkBoxAt.Checked = true;
			this.checkBoxAt.CheckState = System.Windows.Forms.CheckState.Checked;
			resources.ApplyResources(this.checkBoxAt, "checkBoxAt");
			this.checkBoxAt.ForeColor = System.Drawing.Color.Black;
			this.checkBoxAt.Name = "checkBoxAt";
			this.checkBoxAt.UseVisualStyleBackColor = true;
			// 
			// textBoxMes
			// 
			resources.ApplyResources(this.textBoxMes, "textBoxMes");
			this.textBoxMes.Name = "textBoxMes";
			// 
			// buttonFTP
			// 
			this.buttonFTP.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
			resources.ApplyResources(this.buttonFTP, "buttonFTP");
			this.buttonFTP.Name = "buttonFTP";
			this.buttonFTP.UseVisualStyleBackColor = false;
			this.buttonFTP.Click += new System.EventHandler(this.ButtonFTPClick);
			// 
			// richTextBoxFTP
			// 
			this.richTextBoxFTP.BorderStyle = System.Windows.Forms.BorderStyle.None;
			resources.ApplyResources(this.richTextBoxFTP, "richTextBoxFTP");
			this.richTextBoxFTP.Name = "richTextBoxFTP";
			// 
			// label3
			// 
			resources.ApplyResources(this.label3, "label3");
			this.label3.ForeColor = System.Drawing.Color.DarkGray;
			this.label3.Name = "label3";
			// 
			// ServiceForm
			// 
			resources.ApplyResources(this, "$this");
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.BackColor = System.Drawing.Color.WhiteSmoke;
			this.Controls.Add(this.label3);
			this.Controls.Add(this.richTextBoxFTP);
			this.Controls.Add(this.buttonFTP);
			this.Controls.Add(this.textBoxMes);
			this.Controls.Add(this.checkBoxAt);
			this.Controls.Add(this.richTextBoxMes);
			this.Controls.Add(this.buttonSend);
			this.Controls.Add(this.labelMes);
			this.MaximizeBox = false;
			this.MinimizeBox = false;
			this.Name = "ServiceForm";
			this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.UnitLibFormFormClosing);
			this.ResumeLayout(false);
			this.PerformLayout();
		}
		private System.Windows.Forms.Label label3;
		private System.Windows.Forms.RichTextBox richTextBoxFTP;
		private System.Windows.Forms.Button buttonFTP;
		private System.Windows.Forms.TextBox textBoxMes;
		private System.Windows.Forms.CheckBox checkBoxAt;
		private System.Windows.Forms.RichTextBox richTextBoxMes;
		private System.Windows.Forms.Label labelMes;
		private System.Windows.Forms.Button buttonSend;
	}
}
