﻿
namespace n_EventLink
{
using System;
using System.Drawing;
using System.Drawing.Drawing2D;

using n_MyObject;
using n_MyFileObject;
using n_Shape;
using n_GUIset;
using n_EventIns;
using n_MainSystemData;
using n_MyObjectList;
using n_GroupList;


//*****************************************************
//事件链接记录器
public class EventLink
{
	MyObjectList mModuleList;
	GroupList mGroupList;
	
	ELNote[] EventList;
	int Length;
	
	//构造函数
	public EventLink( MyObjectList vModuleList, GroupList vGroupList )
	{
		mModuleList = vModuleList;
		mGroupList = vGroupList;
		
		EventList = new ELNote[ 100 ];
		Length = 0;
	}
	
	//清空
	public void Clear()
	{
		Length = 0;
	}
	
	//获取一个模块事件所对应的事件节点
	public EventIns GetEvent( MyFileObject m, string EventMes )
	{
		for( int i = 0; i < Length; ++i ) {
			if( EventList[ i ] == null ) {
				continue;
			}
			if( EventList[ i ].EventSource == m ) {
				EventIns ei = (EventIns)EventList[ i ].Target;
				if( ei.EventEntry == EventMes ) {
					return ei;
				}
			}
		}
		return null;
	}
	
	//添加节点
	public void Add( ELNote n )
	{
		for( int i = 0; i < Length; ++i ) {
			if( EventList[ i ] == null ) {
				EventList[ i ] = n;
				return;
			}
		}
		EventList[ Length ] = n;
		++Length;
	}
	
	//删除节点
	public void Delete( MyFileObject on, string InnerName )
	{
		for( int i = 0; i < Length; ++i ) {
			if( EventList[ i ] == null ) {
				continue;
			}
			if( EventList[ i ].Target == null ) {
				
				//正常不会出现这里 (当复制一个带有事件的模块到新文件, 会到这里)
				n_Debug.Warning.BUG( "事件删除异常: " + EventList[ i ].isOK + " " + EventList[ i ].InnerName + " " + EventList[ i ].ModuleName + " " + EventList[ i ].TargetName );
				//EventList[ i ] = null;
				continue;
			}
			if( ((EventIns)EventList[ i ].Target).owner == on && EventList[ i ].InnerName == InnerName ) {
				((EventIns)EventList[ i ].Target).owner = null;
				EventList[ i ] = null;
			}
		}
	}
	
	//组件绘制工作
	public void Draw( Graphics g )
	{
		for( int i = 0; i < Length; ++i ) {
			if( EventList[ i ] == null ) {
				continue;
			}
			if( !EventList[ i ].isOK ) {
				SearchNoteData( EventList[ i ] );
				if( !EventList[ i ].isOK ) {
					n_Debug.Warning.WarningMessage += "\n由于找不到目标事件框, 以下事件被删除, 如需要请重新添加此事件:\n" +
						EventList[ i ].ModuleName + "." + EventList[ i ].TargetName + " (" + EventList[ i ].InnerName + ")\n";
					
					//补充: 删除模块的无效事件
					MyFileObject Source = (MyFileObject)EventList[ i ].EventSource;
					for( int n = 0; n < EventList[ i ].EventSource.EventList.Length; ++n ) {
						if( Source.EventList[n][2] == EventList[ i ].InnerName ) {
							Source.EventList[n][0] = "";
						}
					}
					
					EventList[ i ] = null;
					continue;
				}
			}
			if( EventList[ i ].Target is EventIns ) {
				
				EventIns Target = (EventIns)EventList[ i ].Target;
				MyFileObject Source = (MyFileObject)EventList[ i ].EventSource;
				
				//判断名字是否修改过
				if( Target.EventSource != Source.Name ) {
					Target.EventEntry = Source.Name + "_" + Target.EventType;
				}
				//判断目标是否已被删除
				if( Target.isDeleted ) {
					
					for( int n = 0; n < EventList[ i ].EventSource.EventList.Length; ++n ) {
						if( Source.EventList[n][2] == EventList[ i ].InnerName ) {
							Source.EventList[n][0] = "";
						}
					}
					
					//这里简化了... 2021.5.17
					//Delete( EventList[ i ].EventSource, EventList[ i ].InnerName );
					EventList[ i ] = null;
					
					continue;
				}
				
				//判断事件和对象是否同组且隐藏
				if( Target.GroupMes != null && Source.GroupMes == Target.GroupMes && !Target.GroupVisible && !Source.GroupVisible ) {
					continue;
				}
				
				Shape.DrawEventLine( g, mGroupList, Source, Target );
			}
		}
	}
	
	//完善一个节点
	void SearchNoteData( ELNote e )
	{
		foreach( MyObject mo in mModuleList ) {
			if( !( mo is MyFileObject ) ) {
				continue;
			}
			MyFileObject m = (MyFileObject)mo;
			if( m.Name != e.ModuleName ) {
				continue;
			}
			e.EventSource = m;
			break;
		}
		foreach( MyObject mo in mModuleList ) {
			if( mo is EventIns ) {
				EventIns m = (EventIns)mo;
				if( m.EventEntry != e.TargetName ) {
					continue;
				}
				/*
				if( m.EventEntry.IndexOf( "_系统空闲时" ) != -1 ) {
					string s = m.EventEntry.Replace( "_系统空闲时", "_反复执行" );
					m.EventEntry = s;
					e.TargetName = s;
				}
				if( m.EventEntry.IndexOf( "_系统启动时" ) != -1 ) {
					string s = m.EventEntry.Replace( "_系统启动时", "_初始化" );
					m.EventEntry = s;
					e.TargetName = s;
				}
				*/
				m.owner = e.EventSource;
				e.Target = m;
				e.isOK = true;
				break;
			}
		}
	}
}
//事件链接节点
public class ELNote
{
	public string ModuleName;
	public string InnerName;
	public string TargetName;
	
	public bool isOK;
	public MyFileObject EventSource;
	public MyObject Target;
	
	//构造函数
	public ELNote( string mN, string eN, string tN )
	{
		ModuleName = mN;
		InnerName = eN;
		TargetName = tN;
		
		EventSource = null;
		Target = null;
		
		isOK = false;
	}
}
}



