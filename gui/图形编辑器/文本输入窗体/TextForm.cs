﻿
namespace n_TextForm
{
using System;
using System.Windows.Forms;
using n_ImagePanel;
using c_FormMover;

/// <summary>
/// Description of WorkForm.
/// </summary>
public partial class TextForm : Form
{
	bool isOK;
	
	FormMover fm;
	
	//主窗口
	public TextForm()
	{
		InitializeComponent();
		fm = new FormMover( this );
		isOK = false;
	}
	
	//运行
	public string Run( string Mes )
	{
		if( this.Visible ) {
			return null;
		}
		//注意这里是要保持组件为选中状态, 防止窗体关闭后组件取消高亮造成的闪烁
		//ImagePanel TargetGPanel = ( (FileTabPage)A.WorkBox.TexttabControl.SelectedTab ).GPanel;
		//TargetGPanel.SelPanel.SelectState = 3;
		
		this.textBox1.Text = Mes;
		this.textBox1.SelectionStart = Mes.Length;
		
		this.Visible = true;
		//this.ShowDialog();
		
		while( this.Visible ) {
			System.Windows.Forms.Application.DoEvents();
		}
		if( isOK ) {
			return this.textBox1.Text;
		}
		return null;
	}
	
	void Button确定Click(object sender, EventArgs e)
	{
		this.textBox1.Focus();
		isOK = true;
		this.Visible = false;
	}
	
	void Button取消Click(object sender, EventArgs e)
	{
		this.textBox1.Focus();
		isOK = false;
		this.Visible = false;
	}
}
}



