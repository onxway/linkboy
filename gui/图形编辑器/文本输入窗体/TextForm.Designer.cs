﻿/*
 * 由 SharpDevelop 创建。
 * 用户: bbb_2
 * 日期: 2009-11-17
 * 时间: 11:15
 * 
 * 如果你想要更改该模板，那么请使用“工具 | 选项 | 编辑 | 编辑标准标题”。
 */
namespace n_TextForm
{
	partial class TextForm
	{
		/// <summary>
		/// Designer variable used to keep track of non-visual components.
		/// </summary>
		private System.ComponentModel.IContainer components = null;
		
		/// <summary>
		/// Disposes resources used by the form.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing) {
				if (components != null) {
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}
		
		/// <summary>
		/// This method is required for Windows Forms designer support.
		/// Do not change the method contents inside the source code editor. The Forms designer might
		/// not be able to load this method if it was changed manually.
		/// </summary>
		private void InitializeComponent()
		{
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(TextForm));
			this.button确定 = new System.Windows.Forms.Button();
			this.button取消 = new System.Windows.Forms.Button();
			this.textBox1 = new System.Windows.Forms.TextBox();
			this.SuspendLayout();
			// 
			// button确定
			// 
			this.button确定.AccessibleDescription = null;
			this.button确定.AccessibleName = null;
			this.button确定.Anchor = ((System.Windows.Forms.AnchorStyles)(resources.GetObject("button确定.Anchor")));
			this.button确定.AutoSize = ((bool)(resources.GetObject("button确定.AutoSize")));
			this.button确定.AutoSizeMode = ((System.Windows.Forms.AutoSizeMode)(resources.GetObject("button确定.AutoSizeMode")));
			this.button确定.BackColor = System.Drawing.Color.Green;
			this.button确定.BackgroundImage = null;
			this.button确定.BackgroundImageLayout = ((System.Windows.Forms.ImageLayout)(resources.GetObject("button确定.BackgroundImageLayout")));
			this.button确定.Dock = ((System.Windows.Forms.DockStyle)(resources.GetObject("button确定.Dock")));
			this.button确定.FlatStyle = ((System.Windows.Forms.FlatStyle)(resources.GetObject("button确定.FlatStyle")));
			this.button确定.Font = ((System.Drawing.Font)(resources.GetObject("button确定.Font")));
			this.button确定.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
			this.button确定.ImageAlign = ((System.Drawing.ContentAlignment)(resources.GetObject("button确定.ImageAlign")));
			this.button确定.ImageIndex = ((int)(resources.GetObject("button确定.ImageIndex")));
			this.button确定.ImageKey = resources.GetString("button确定.ImageKey");
			this.button确定.ImeMode = ((System.Windows.Forms.ImeMode)(resources.GetObject("button确定.ImeMode")));
			this.button确定.Location = ((System.Drawing.Point)(resources.GetObject("button确定.Location")));
			this.button确定.Name = "button确定";
			this.button确定.RightToLeft = ((System.Windows.Forms.RightToLeft)(resources.GetObject("button确定.RightToLeft")));
			this.button确定.Size = ((System.Drawing.Size)(resources.GetObject("button确定.Size")));
			this.button确定.TabIndex = ((int)(resources.GetObject("button确定.TabIndex")));
			this.button确定.Text = resources.GetString("button确定.Text");
			this.button确定.TextAlign = ((System.Drawing.ContentAlignment)(resources.GetObject("button确定.TextAlign")));
			this.button确定.TextImageRelation = ((System.Windows.Forms.TextImageRelation)(resources.GetObject("button确定.TextImageRelation")));
			this.button确定.UseVisualStyleBackColor = false;
			this.button确定.Click += new System.EventHandler(this.Button确定Click);
			// 
			// button取消
			// 
			this.button取消.AccessibleDescription = null;
			this.button取消.AccessibleName = null;
			this.button取消.Anchor = ((System.Windows.Forms.AnchorStyles)(resources.GetObject("button取消.Anchor")));
			this.button取消.AutoSize = ((bool)(resources.GetObject("button取消.AutoSize")));
			this.button取消.AutoSizeMode = ((System.Windows.Forms.AutoSizeMode)(resources.GetObject("button取消.AutoSizeMode")));
			this.button取消.BackColor = System.Drawing.Color.Maroon;
			this.button取消.BackgroundImage = null;
			this.button取消.BackgroundImageLayout = ((System.Windows.Forms.ImageLayout)(resources.GetObject("button取消.BackgroundImageLayout")));
			this.button取消.Dock = ((System.Windows.Forms.DockStyle)(resources.GetObject("button取消.Dock")));
			this.button取消.FlatStyle = ((System.Windows.Forms.FlatStyle)(resources.GetObject("button取消.FlatStyle")));
			this.button取消.Font = ((System.Drawing.Font)(resources.GetObject("button取消.Font")));
			this.button取消.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(192)))));
			this.button取消.ImageAlign = ((System.Drawing.ContentAlignment)(resources.GetObject("button取消.ImageAlign")));
			this.button取消.ImageIndex = ((int)(resources.GetObject("button取消.ImageIndex")));
			this.button取消.ImageKey = resources.GetString("button取消.ImageKey");
			this.button取消.ImeMode = ((System.Windows.Forms.ImeMode)(resources.GetObject("button取消.ImeMode")));
			this.button取消.Location = ((System.Drawing.Point)(resources.GetObject("button取消.Location")));
			this.button取消.Name = "button取消";
			this.button取消.RightToLeft = ((System.Windows.Forms.RightToLeft)(resources.GetObject("button取消.RightToLeft")));
			this.button取消.Size = ((System.Drawing.Size)(resources.GetObject("button取消.Size")));
			this.button取消.TabIndex = ((int)(resources.GetObject("button取消.TabIndex")));
			this.button取消.Text = resources.GetString("button取消.Text");
			this.button取消.TextAlign = ((System.Drawing.ContentAlignment)(resources.GetObject("button取消.TextAlign")));
			this.button取消.TextImageRelation = ((System.Windows.Forms.TextImageRelation)(resources.GetObject("button取消.TextImageRelation")));
			this.button取消.UseVisualStyleBackColor = false;
			this.button取消.Click += new System.EventHandler(this.Button取消Click);
			// 
			// textBox1
			// 
			this.textBox1.AccessibleDescription = null;
			this.textBox1.AccessibleName = null;
			this.textBox1.Anchor = ((System.Windows.Forms.AnchorStyles)(resources.GetObject("textBox1.Anchor")));
			this.textBox1.BackgroundImage = null;
			this.textBox1.BackgroundImageLayout = ((System.Windows.Forms.ImageLayout)(resources.GetObject("textBox1.BackgroundImageLayout")));
			this.textBox1.Dock = ((System.Windows.Forms.DockStyle)(resources.GetObject("textBox1.Dock")));
			this.textBox1.Font = ((System.Drawing.Font)(resources.GetObject("textBox1.Font")));
			this.textBox1.ImeMode = ((System.Windows.Forms.ImeMode)(resources.GetObject("textBox1.ImeMode")));
			this.textBox1.Location = ((System.Drawing.Point)(resources.GetObject("textBox1.Location")));
			this.textBox1.MaxLength = ((int)(resources.GetObject("textBox1.MaxLength")));
			this.textBox1.Multiline = ((bool)(resources.GetObject("textBox1.Multiline")));
			this.textBox1.Name = "textBox1";
			this.textBox1.PasswordChar = ((char)(resources.GetObject("textBox1.PasswordChar")));
			this.textBox1.RightToLeft = ((System.Windows.Forms.RightToLeft)(resources.GetObject("textBox1.RightToLeft")));
			this.textBox1.ScrollBars = ((System.Windows.Forms.ScrollBars)(resources.GetObject("textBox1.ScrollBars")));
			this.textBox1.Size = ((System.Drawing.Size)(resources.GetObject("textBox1.Size")));
			this.textBox1.TabIndex = ((int)(resources.GetObject("textBox1.TabIndex")));
			this.textBox1.TextAlign = ((System.Windows.Forms.HorizontalAlignment)(resources.GetObject("textBox1.TextAlign")));
			this.textBox1.WordWrap = ((bool)(resources.GetObject("textBox1.WordWrap")));
			// 
			// TextForm
			// 
			this.AccessibleDescription = null;
			this.AccessibleName = null;
			this.AutoScaleDimensions = ((System.Drawing.SizeF)(resources.GetObject("$this.AutoScaleDimensions")));
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.AutoScroll = ((bool)(resources.GetObject("$this.AutoScroll")));
			this.AutoSize = ((bool)(resources.GetObject("$this.AutoSize")));
			this.AutoSizeMode = ((System.Windows.Forms.AutoSizeMode)(resources.GetObject("$this.AutoSizeMode")));
			this.BackColor = System.Drawing.Color.Silver;
			this.BackgroundImage = null;
			this.BackgroundImageLayout = ((System.Windows.Forms.ImageLayout)(resources.GetObject("$this.BackgroundImageLayout")));
			this.ClientSize = ((System.Drawing.Size)(resources.GetObject("$this.ClientSize")));
			this.Controls.Add(this.textBox1);
			this.Controls.Add(this.button取消);
			this.Controls.Add(this.button确定);
			this.Font = null;
			this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
			this.Icon = null;
			this.ImeMode = ((System.Windows.Forms.ImeMode)(resources.GetObject("$this.ImeMode")));
			this.Location = ((System.Drawing.Point)(resources.GetObject("$this.Location")));
			this.MaximumSize = ((System.Drawing.Size)(resources.GetObject("$this.MaximumSize")));
			this.Name = "TextForm";
			this.RightToLeft = ((System.Windows.Forms.RightToLeft)(resources.GetObject("$this.RightToLeft")));
			this.RightToLeftLayout = ((bool)(resources.GetObject("$this.RightToLeftLayout")));
			this.ShowInTaskbar = false;
			this.StartPosition = ((System.Windows.Forms.FormStartPosition)(resources.GetObject("$this.StartPosition")));
			this.Text = resources.GetString("$this.Text");
			this.TopMost = true;
			this.ResumeLayout(false);
			this.PerformLayout();
		}
		private System.Windows.Forms.TextBox textBox1;
		private System.Windows.Forms.Button button取消;
		private System.Windows.Forms.Button button确定;
	}
}
