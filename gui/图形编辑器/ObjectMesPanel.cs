﻿
using System;
using System.Drawing;
using System.Drawing.Drawing2D;
using n_MyIns;

namespace n_ObjectMesPanel
{
public class ObjectMesPanel
{
	public n_MyObject.MyObject TargetIns;
	public bool Visible;
	public string Mes;
	public int WarningLeavel;
	public bool AnyClickHide;
	
	int MidX;
	int MidY;
	int Width;
	int Height;
	
	const int MaxWidth = 300;
	
	Brush b1;
	Brush b2;
	Brush b3;
	
	Pen p1;
	Pen p2;
	Pen p3;
	
	int Index;
	
	public int ClickNumber;
	
	//构造函数
	public ObjectMesPanel( int i )
	{
		Index = i;
		
		Visible = false;
		TargetIns = null;
		AnyClickHide = false;
		
		ClickNumber = 0;
		
		Width = 250;
		Height = 100;
		Mes = "";
		WarningLeavel = 0;
		
		
		if( n_MainSystemData.SystemData.isBlack ) {
			b1 = new SolidBrush( Color.WhiteSmoke );
			b2 = new SolidBrush( Color.WhiteSmoke );
			b3 = new SolidBrush( Color.WhiteSmoke );
			
			p1 = new Pen( Color.Green, 1 );
			p2 = new Pen( Color.DarkGoldenrod, 1 );
			p3 = new Pen( Color.OrangeRed, 1 );
		}
		else {
			b1 = new SolidBrush( Color.FromArgb(192, 226, 192) );
			b2 = new SolidBrush( Color.FromArgb(221, 221, 155) );
			b3 = new SolidBrush( Color.FromArgb(236, 192, 189) );
			
			p1 = new Pen( Color.Black, 1 );
			p2 = new Pen( Color.Black, 1 );
			p3 = new Pen( Color.Black, 1 );
		}
	}
	
	//鼠标按下事件, 在组件中返回 true,不在组件中返回 false.
	public bool MouseDown( int mX, int mY )
	{
		if( ClickNumber == 0 ) {
			ClickNumber++;
			return false;
		}
		
		//只要点击鼠标即可隐藏
		//if( AnyClickHide ) {
			Visible = false;
		//}
		
		if( !Visible ) {
			return false;
		}
		//判断是否在组件上
		if( mX >= MidX - Width / 2 && mX <= MidX + Width / 2 && mY >= MidY - Height / 2 && mY <= MidY + Height / 2 ) {
			Visible = false;
			return true;
		}
		return false;
	}
	
	//显示
	public void Draw( Graphics g )
	{
		if( !Visible ) {
			return;
		}
		string mes = n_Language.Language.MesLevel1 + WarningLeavel + n_Language.Language.MesLevel2 + Mes;
		
		n_SG.SG.MString.MeasureSize( mes, n_GUIset.GUIset.ExpFont.Size, MaxWidth, out this.Width, out this.Height );
		
		int w = Width + 10;
		int h = Height + 10;
		
		MidX = TargetIns.SX + TargetIns.Width + 20 + w/2;
		MidY = TargetIns.MidY;
		int x = MidX;
		int y = MidY;
		
		int rx = MidX - w/2;
		int ry = MidY - h/2;
		Point[] pt = new Point[ 7 ];
		pt[ 0 ] = new Point( rx - 20, ry + h/2 );
		pt[ 1 ] = new Point( rx, ry + h/2 + 20 );
		pt[ 2 ] = new Point( rx, ry + h );
		pt[ 3 ] = new Point( rx + w, ry + h );
		pt[ 4 ] = new Point( rx + w, ry );
		pt[ 5 ] = new Point( rx, ry );
		pt[ 6 ] = new Point( rx, ry + h/2 - 20 );
		
		
		Pen p = null;
		Brush b = null;
		if( WarningLeavel == 1 ) {
			b = b1;
			p = p1;
		}
		if( WarningLeavel == 2 ) {
			b = b2;
			p = p2;
		}
		if( WarningLeavel == 3 ) {
			b = b3;
			p = p3;
		}
		
		g.FillClosedCurve( b, pt, FillMode.Winding, 0.0F );
		g.DrawClosedCurve( p, pt, 0.0F, FillMode.Winding );
		
		//g.FillRectangle( Brushes.Silver, StartX + MidX - Width/2 - 5, StartY + MidY - Height/2 - 5, Width + 10, Height + 10 );
		//g.DrawRectangle( Pens.Green, StartX + MidX - Width/2 - 5, StartY + MidY - Height/2 - 5, Width + 10, Height + 10 );
		
		if( n_MainSystemData.SystemData.isBlack ) {
			n_SG.SG.MString.DrawAtRectangle( mes, p.Color, n_GUIset.GUIset.ExpFont.Size, rx + 5, ry + 5, Width, Height );
		}
		else {
			n_SG.SG.MString.DrawAtRectangle( mes, Color.Black, n_GUIset.GUIset.ExpFont.Size, rx + 5, ry + 5, Width, Height );
		}
	}
}
}

