﻿/*
 * 由 SharpDevelop 创建。
 * 用户: bbb_2
 * 日期: 2009-11-17
 * 时间: 11:15
 * 
 * 如果你想要更改该模板，那么请使用“工具 | 选项 | 编辑 | 编辑标准标题”。
 */
namespace n_NoteForm
{
	partial class NoteForm
	{
		/// <summary>
		/// Designer variable used to keep track of non-visual components.
		/// </summary>
		private System.ComponentModel.IContainer components = null;
		
		/// <summary>
		/// Disposes resources used by the form.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing) {
				if (components != null) {
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}
		
		/// <summary>
		/// This method is required for Windows Forms designer support.
		/// Do not change the method contents inside the source code editor. The Forms designer might
		/// not be able to load this method if it was changed manually.
		/// </summary>
		private void InitializeComponent()
		{
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(NoteForm));
			this.button确定 = new System.Windows.Forms.Button();
			this.richTextBox1 = new System.Windows.Forms.RichTextBox();
			this.panel1 = new System.Windows.Forms.Panel();
			this.panel2 = new System.Windows.Forms.Panel();
			this.comboBox1 = new System.Windows.Forms.ComboBox();
			this.buttonSetStart = new System.Windows.Forms.Button();
			this.buttonImage = new System.Windows.Forms.Button();
			this.buttonAuto = new System.Windows.Forms.Button();
			this.buttonMid = new System.Windows.Forms.Button();
			this.panel1.SuspendLayout();
			this.panel2.SuspendLayout();
			this.SuspendLayout();
			// 
			// button确定
			// 
			this.button确定.BackColor = System.Drawing.Color.CornflowerBlue;
			resources.ApplyResources(this.button确定, "button确定");
			this.button确定.ForeColor = System.Drawing.Color.White;
			this.button确定.Name = "button确定";
			this.button确定.UseVisualStyleBackColor = false;
			this.button确定.Click += new System.EventHandler(this.Button确定Click);
			this.button确定.MouseDown += new System.Windows.Forms.MouseEventHandler(this.Button确定MouseDown);
			// 
			// richTextBox1
			// 
			this.richTextBox1.AcceptsTab = true;
			this.richTextBox1.BorderStyle = System.Windows.Forms.BorderStyle.None;
			resources.ApplyResources(this.richTextBox1, "richTextBox1");
			this.richTextBox1.Name = "richTextBox1";
			this.richTextBox1.MouseDown += new System.Windows.Forms.MouseEventHandler(this.RichTextBox1MouseDown);
			// 
			// panel1
			// 
			this.panel1.Controls.Add(this.button确定);
			resources.ApplyResources(this.panel1, "panel1");
			this.panel1.Name = "panel1";
			// 
			// panel2
			// 
			this.panel2.Controls.Add(this.buttonMid);
			this.panel2.Controls.Add(this.comboBox1);
			this.panel2.Controls.Add(this.buttonSetStart);
			this.panel2.Controls.Add(this.buttonImage);
			this.panel2.Controls.Add(this.buttonAuto);
			resources.ApplyResources(this.panel2, "panel2");
			this.panel2.Name = "panel2";
			// 
			// comboBox1
			// 
			this.comboBox1.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
			resources.ApplyResources(this.comboBox1, "comboBox1");
			this.comboBox1.FormattingEnabled = true;
			this.comboBox1.Items.AddRange(new object[] {
									resources.GetString("comboBox1.Items")});
			this.comboBox1.Name = "comboBox1";
			this.comboBox1.SelectedIndexChanged += new System.EventHandler(this.ComboBox1SelectedIndexChanged);
			// 
			// buttonSetStart
			// 
			this.buttonSetStart.BackColor = System.Drawing.Color.CornflowerBlue;
			resources.ApplyResources(this.buttonSetStart, "buttonSetStart");
			this.buttonSetStart.ForeColor = System.Drawing.Color.White;
			this.buttonSetStart.Name = "buttonSetStart";
			this.buttonSetStart.UseVisualStyleBackColor = false;
			this.buttonSetStart.Click += new System.EventHandler(this.ButtonSetStartClick);
			// 
			// buttonImage
			// 
			this.buttonImage.BackColor = System.Drawing.Color.CornflowerBlue;
			resources.ApplyResources(this.buttonImage, "buttonImage");
			this.buttonImage.ForeColor = System.Drawing.Color.White;
			this.buttonImage.Name = "buttonImage";
			this.buttonImage.UseVisualStyleBackColor = false;
			this.buttonImage.Click += new System.EventHandler(this.ButtonImageClick);
			// 
			// buttonAuto
			// 
			this.buttonAuto.BackColor = System.Drawing.Color.CornflowerBlue;
			resources.ApplyResources(this.buttonAuto, "buttonAuto");
			this.buttonAuto.ForeColor = System.Drawing.Color.White;
			this.buttonAuto.Name = "buttonAuto";
			this.buttonAuto.UseVisualStyleBackColor = false;
			this.buttonAuto.Click += new System.EventHandler(this.ButtonAutoClick);
			// 
			// buttonMid
			// 
			this.buttonMid.BackColor = System.Drawing.Color.CornflowerBlue;
			resources.ApplyResources(this.buttonMid, "buttonMid");
			this.buttonMid.ForeColor = System.Drawing.Color.White;
			this.buttonMid.Name = "buttonMid";
			this.buttonMid.UseVisualStyleBackColor = false;
			this.buttonMid.Click += new System.EventHandler(this.ButtonMidClick);
			// 
			// NoteForm
			// 
			resources.ApplyResources(this, "$this");
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
			this.Controls.Add(this.richTextBox1);
			this.Controls.Add(this.panel2);
			this.Controls.Add(this.panel1);
			this.MaximizeBox = false;
			this.MinimizeBox = false;
			this.Name = "NoteForm";
			this.TopMost = true;
			this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.NoteFormFormClosing);
			this.panel1.ResumeLayout(false);
			this.panel2.ResumeLayout(false);
			this.ResumeLayout(false);
		}
		private System.Windows.Forms.Button buttonMid;
		private System.Windows.Forms.ComboBox comboBox1;
		private System.Windows.Forms.Button buttonSetStart;
		private System.Windows.Forms.Button buttonImage;
		private System.Windows.Forms.Panel panel2;
		private System.Windows.Forms.Button buttonAuto;
		private System.Windows.Forms.Panel panel1;
		private System.Windows.Forms.RichTextBox richTextBox1;
		private System.Windows.Forms.Button button确定;
	}
}
