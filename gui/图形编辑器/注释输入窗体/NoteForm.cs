﻿
namespace n_NoteForm
{
using System;
using System.Windows.Forms;
using n_ImagePanel;
using System.Drawing;
using System.Drawing.Drawing2D;
using c_FormMover;
using n_MyObject;
using n_MyFileObject;
using n_GUIcoder;

/// <summary>
/// Description of WorkForm.
/// </summary>
public partial class NoteForm : Form
{
	bool isOK;
	FormMover fm;
	
	n_GNote.GNote myOwner;
	
	//主窗口
	public NoteForm()
	{
		InitializeComponent();
		this.ImeMode = ImeMode.Off;
		isOK = false;
		
		fm = new FormMover( this );
	}
	
	//运行
	public string Run( n_GNote.GNote o, string Mes )
	{
		myOwner = o;
		
		//查看py接口临时隐藏
		if( o == null ) {
			panel2.Visible = false;
		}
		else {
			panel2.Visible = true;
		}
		
		if( this.Visible ) {
			return null;
		}
		//注意这里是要保持组件为选中状态, 防止窗体关闭后组件取消高亮造成的闪烁
		//ImagePanel TargetGPanel = ( (FileTabPage)A.WorkBox.TexttabControl.SelectedTab ).GPanel;
		//TargetGPanel.SelPanel.SelectState = 3;
		
		if( Mes == "请在此输入您的注释信息" ) {
			this.richTextBox1.ForeColor = Color.Gray;
		}
		else {
			this.richTextBox1.ForeColor = Color.Black;
		}
		
		this.richTextBox1.Text = Mes;
		this.richTextBox1.SelectionStart = Mes.Length;
		
		this.Visible = true;
		//this.ShowDialog();
		
		while( this.Visible ) {
			System.Windows.Forms.Application.DoEvents();
		}
		if( isOK ) {
			return this.richTextBox1.Text;
		}
		return null;
	}
	
	void Button确定Click(object sender, EventArgs e)
	{
		this.richTextBox1.Focus();
		isOK = true;
		this.Visible = false;
	}
	
	void RichTextBox1MouseDown(object sender, MouseEventArgs e)
	{
		if( this.richTextBox1.Text == "请在此输入您的注释信息" ) {
			this.richTextBox1.Text = "";
			this.richTextBox1.ForeColor = Color.Black;
		}
	}
	
	void NoteFormFormClosing(object sender, FormClosingEventArgs e)
	{
		e.Cancel = true;
		this.richTextBox1.Focus();
		isOK = false;
		this.Visible = false;
	}
	
	void ButtonAutoClick(object sender, EventArgs e)
	{
		string Result = "";
		
		if( myOwner.GroupMes == null ) {
			Result += "<name> # #,\n";
		}
		else {
			Result += "<name> " + myOwner.GroupMes + " " + myOwner.GroupMes + ",\n";
		}
		Result += "未找到此框架:" + myOwner.GroupMes + "的描述信息\n";
		
		Result += "</name>\n";
		
		foreach( n_MyObject.MyObject mo in G.CGPanel.myModuleList ) {
			
			if( myOwner.GroupMes == null && mo.GroupMes == null || myOwner.GroupMes == mo.GroupMes ) {
				
				if( mo is n_GVar.GVar ) {
					
					Result += "<member> temp " + mo.Name + " " + mo.Name + ",\n";
					Result += "这是用户的自定义变量:" + mo.Name + ", 未找到此变量的描述信息\n";
					Result += "</member>\n";
				}
				
			}
		}
		
		foreach( n_MyObject.MyObject mo in G.CGPanel.myModuleList ) {
			
			if( myOwner.GroupMes == null && mo.GroupMes == null || myOwner.GroupMes == mo.GroupMes ) {
				
				if( mo is n_UserFunctionIns.UserFunctionIns ) {
					
					n_UserFunctionIns.UserFunctionIns ui = (n_UserFunctionIns.UserFunctionIns)mo;
					
					Result += "<member> temp " + ui.GetFuncName() + " " + ui.GetFuncName() + ",\n";
					Result += "这是用户的自定义函数:" + ui.GetFuncName() + ", 未找到此函数的描述信息\n";
					Result += "</member>\n";
				}
				
			}
		}
		
		this.richTextBox1.Text = Result;
		this.richTextBox1.ForeColor = Color.Black;
	}
	
	void ButtonImageClick(object sender, EventArgs e)
	{
		//打开文件对话框
		OpenFileDialog OpenFileDlg = new OpenFileDialog();
		OpenFileDlg.Filter = "所有类型文件 | *.*";
		OpenFileDlg.Title = "请选择文件";
		OpenFileDlg.InitialDirectory = G.ccode.FilePath;
		
		DialogResult dlgResult = OpenFileDlg.ShowDialog();
		if(dlgResult == DialogResult.OK) {
			
			if( !OpenFileDlg.FileName.StartsWith( G.ccode.FilePath ) ) {
				MessageBox.Show( "建议先把图片放到和源文件同一个目录下, 再打开" );
				return;
			}
			string FileName = OpenFileDlg.FileName.Remove( 0, G.ccode.FilePath.Length );
			//string FileName = OpenFileDlg.FileName;
			
			myOwner.SetImage( FileName );
		}
	}
	
	void Button确定MouseDown(object sender, MouseEventArgs e)
	{
		buttonAuto.Visible = true;
		//buttonAPI.Visible = true;
	}
	
	void ButtonSetStartClick(object sender, EventArgs e)
	{
		string t = richTextBox1.Text;
		if( t.StartsWith( n_GUIcoder.NoteFlag.StartMes ) ) {
			t = t.Remove( 0, t.IndexOf( '\n' ) + 1 );
		}
		G.CGPanel.UserMes = t;
	}
	
	void ComboBox1SelectedIndexChanged(object sender, EventArgs e)
	{
		richTextBox1.Text += comboBox1.Text;
	}
	
	void ButtonMidClick(object sender, EventArgs e)
	{
		richTextBox1.Text = "<居中>" + richTextBox1.Text;
	}
}
}



