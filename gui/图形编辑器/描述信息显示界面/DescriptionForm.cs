﻿
namespace n_DescriptionForm
{
using System;
using System.Drawing;
using System.Windows.Forms;

using n_GUIset;
using n_MyFileObject;
using n_HardModule;
using n_UIModule;
using n_MyObject;
using n_GUIcoder;
using n_MesPanel;
using System.Diagnostics;

/// <summary>
/// Description of WorkForm.
/// </summary>
public partial class DescriptionForm : Form
{
	MesPanel GPanel;
	
	const int GWidth = 900;
	const int MesWidth = GWidth - 20 - 20;
	
	SolidBrush backb;
	
	MyFileObject cm;
	ModuleMessage MM;
	//ModuleMessage MM0;
	//ModuleMessage MM1;
	
	//主窗口
	public DescriptionForm()
	{
		InitializeComponent();
		
		//添加图形编程界面
		GPanel = new MesPanel();
		
		//GPanel.Location = new Point( 0, 0 );
		GPanel.Visible = true;
		this.Controls.Add( GPanel );
		GPanel.BringToFront();
		
		backb = new SolidBrush( Color.WhiteSmoke );
		
		buttonCopy.Text = n_Language.Language.Module_CopyButton;
	}
	
	//运行
	public void Run( MyFileObject c )
	{
		cm = c;
		MM = null;
		if( cm is HardModule ) {
			int LIndex = cm.GetLIndex();
			MM = cm.mm[ LIndex ];
			
			//临时读取两种描述
			GPanel.MM0 = cm.mm[ 0 ];
			GPanel.MM1 = cm.mm[ 1 ];
		}
		if( cm is UIModule ) {
			int LIndex = cm.GetLIndex();
			MM = cm.mm[ LIndex ];
		}
		if( MM == null ) {
			return;
		}
		if( cm.MesFilePath != null ) {
			buttonFile.Visible = true;
			label1.Visible = false;
			buttonFile.Text = "打开 " + cm.MesFilePath;
		}
		else {
			buttonFile.Visible = false;
			//label1.Visible = true;
		}
		GPanel.StartY = 0;
		
		GPanel.SetImageList( cm.BasePath, cm.ModuleImageList );
		
		//TextHeight = SearchHeight( MM, MesWidth );
		GPanel.GWidth = this.Width;
		
		buttonOpenDriver.Name = "Lib\\" + cm.IncPackFilePath.Remove( 0, cm.IncPackFilePath.IndexOf( ' ' ) + 1 );
		buttonOpenDriver.Text = "打开模块驱动程序文件夹: " + buttonOpenDriver.Name;
		
		labelPath.Text = "UI模型文件夹: " + cm.BasePath;
		
		GPanel.Invalidate();
		this.Visible = true;
	}
	
	public static int SearchHeight( ModuleMessage MM, int MesWidth )
	{
		//搜索文字显示区域
		int TextHeight = 40;
		SizeF sz00 = GUIset.mg.MeasureString( MM.ModuleMes, MesPanel.extendFont, MesWidth - 20 );
		TextHeight += (int)sz00.Height;
		for( int i = 0; i < MM.MemberMesList.Length; ++i ) {
			if( MM.MemberMesList[ i ].UserName.StartsWith( "OS_" ) ) {
				continue;
			}
			TextHeight += 5;
			TextHeight += 20;
			
			if( !MM.MemberMesList[ i ].Mes.StartsWith( ":" ) ) {
				SizeF sz = GUIset.mg.MeasureString( MM.MemberMesList[ i ].Mes, MesPanel.extendFont, MesWidth - 20 );
				TextHeight += (int)sz.Height;
			}
		}
		return TextHeight + 20;
	}
	
	//窗体关闭事件
	void UnitLibFormFormClosing(object sender, FormClosingEventArgs e)
	{
		e.Cancel = true;
		this.Visible = false;
	}
	
	void DescriptionFormSizeChanged(object sender, EventArgs e)
	{
		if( GPanel != null ) {
			GPanel.GWidth = this.Width;
			GPanel.Invalidate();
		}
	}
	
	void ButtonFileClick(object sender, EventArgs e)
	{
		Process Proc = new Process();
		Proc.StartInfo.WindowStyle = ProcessWindowStyle.Normal;
		//Proc.StartInfo.UseShellExecute = false;
		//Proc.StartInfo.CreateNoWindow = true;
		//Proc.StartInfo.RedirectStandardOutput = true;
		
		string path = cm.MesFilePath;
		
		//打开文档
		if( path.StartsWith( ":" ) ) {
			path = path.Remove( 0, 1 );
			Proc.StartInfo.WorkingDirectory = AppDomain.CurrentDomain.BaseDirectory;
			Proc.StartInfo.FileName = path;
			
			try {
				Proc.Start();
			}
			catch {
				MessageBox.Show( "无法打开文档，可以进入文件夹自行打开查看: " + AppDomain.CurrentDomain.BaseDirectory + "Lib\\" + path );
			}
		}
		//打开文件夹
		if( path.StartsWith( "+" ) ) {
			path = path.Remove( 0, 1 );
			
			string fileToSelect = n_OS.OS.SystemRoot + path;
			
			string args = string.Format("/Select, {0}", fileToSelect);
			try {
				System.Diagnostics.Process.Start( new System.Diagnostics.ProcessStartInfo("Explorer.exe", args ) );
			}
			catch {
				MessageBox.Show( "无法打开文件夹，请自行打开查看: " + fileToSelect );
			}
		}
	}
	
	void ButtonOpenDriverClick(object sender, EventArgs e)
	{
		string fileToSelect = n_OS.OS.SystemRoot + buttonOpenDriver.Name;
		
		string args = string.Format("/Select, {0}", fileToSelect);
		try {
			System.Diagnostics.Process.Start( new System.Diagnostics.ProcessStartInfo("Explorer.exe", args ) );
		}
		catch {
			MessageBox.Show( "无法打开文件夹，请自行打开查看: " + fileToSelect );
		}
	}
	
	void CheckBoxView1CheckedChanged(object sender, EventArgs e)
	{
		//GPanel.Show1 = checkBoxView1.Checked;
		GPanel.Invalidate();
	}
		
	void ButtonCopyClick(object sender, EventArgs e)
	{
		try {
			System.Windows.Forms.Clipboard.SetText( GPanel.ModText );
		}
		catch (Exception ee) {
			n_OS.VIO.Show( "复制失败/Copy Failed: " + ee );
		}
		MessageBox.Show( n_Language.Language.Module_Copy );
	}
}
}



