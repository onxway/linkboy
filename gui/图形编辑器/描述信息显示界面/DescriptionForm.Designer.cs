﻿/*
 * 由 SharpDevelop 创建。
 * 用户: bbb_2
 * 日期: 2009-11-17
 * 时间: 11:15
 * 
 * 如果你想要更改该模板，那么请使用“工具 | 选项 | 编辑 | 编辑标准标题”。
 */
namespace n_DescriptionForm
{
	partial class DescriptionForm
	{
		/// <summary>
		/// Designer variable used to keep track of non-visual components.
		/// </summary>
		private System.ComponentModel.IContainer components = null;
		
		/// <summary>
		/// Disposes resources used by the form.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing) {
				if (components != null) {
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}
		
		/// <summary>
		/// This method is required for Windows Forms designer support.
		/// Do not change the method contents inside the source code editor. The Forms designer might
		/// not be able to load this method if it was changed manually.
		/// </summary>
		private void InitializeComponent()
		{
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(DescriptionForm));
			this.panel1 = new System.Windows.Forms.Panel();
			this.buttonCopy = new System.Windows.Forms.Button();
			this.labelPath = new System.Windows.Forms.Label();
			this.buttonOpenDriver = new System.Windows.Forms.Button();
			this.label1 = new System.Windows.Forms.Label();
			this.buttonFile = new System.Windows.Forms.Button();
			this.panel1.SuspendLayout();
			this.SuspendLayout();
			// 
			// panel1
			// 
			this.panel1.BackColor = System.Drawing.Color.WhiteSmoke;
			this.panel1.Controls.Add(this.buttonCopy);
			this.panel1.Controls.Add(this.labelPath);
			this.panel1.Controls.Add(this.buttonOpenDriver);
			this.panel1.Controls.Add(this.label1);
			this.panel1.Controls.Add(this.buttonFile);
			resources.ApplyResources(this.panel1, "panel1");
			this.panel1.Name = "panel1";
			// 
			// buttonCopy
			// 
			this.buttonCopy.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(126)))), ((int)(((byte)(163)))), ((int)(((byte)(231)))));
			resources.ApplyResources(this.buttonCopy, "buttonCopy");
			this.buttonCopy.Name = "buttonCopy";
			this.buttonCopy.UseVisualStyleBackColor = false;
			this.buttonCopy.Click += new System.EventHandler(this.ButtonCopyClick);
			// 
			// labelPath
			// 
			resources.ApplyResources(this.labelPath, "labelPath");
			this.labelPath.ForeColor = System.Drawing.Color.DarkSlateGray;
			this.labelPath.Name = "labelPath";
			// 
			// buttonOpenDriver
			// 
			this.buttonOpenDriver.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(200)))), ((int)(((byte)(216)))), ((int)(((byte)(247)))));
			resources.ApplyResources(this.buttonOpenDriver, "buttonOpenDriver");
			this.buttonOpenDriver.Name = "buttonOpenDriver";
			this.buttonOpenDriver.UseVisualStyleBackColor = false;
			this.buttonOpenDriver.Click += new System.EventHandler(this.ButtonOpenDriverClick);
			// 
			// label1
			// 
			resources.ApplyResources(this.label1, "label1");
			this.label1.ForeColor = System.Drawing.Color.DarkSlateGray;
			this.label1.Name = "label1";
			// 
			// buttonFile
			// 
			this.buttonFile.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(126)))), ((int)(((byte)(163)))), ((int)(((byte)(231)))));
			resources.ApplyResources(this.buttonFile, "buttonFile");
			this.buttonFile.Name = "buttonFile";
			this.buttonFile.UseVisualStyleBackColor = false;
			this.buttonFile.Click += new System.EventHandler(this.ButtonFileClick);
			// 
			// DescriptionForm
			// 
			resources.ApplyResources(this, "$this");
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.BackColor = System.Drawing.Color.White;
			this.Controls.Add(this.panel1);
			this.Name = "DescriptionForm";
			this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.UnitLibFormFormClosing);
			this.SizeChanged += new System.EventHandler(this.DescriptionFormSizeChanged);
			this.panel1.ResumeLayout(false);
			this.ResumeLayout(false);
		}
		private System.Windows.Forms.Button buttonCopy;
		private System.Windows.Forms.Label labelPath;
		private System.Windows.Forms.Button buttonOpenDriver;
		private System.Windows.Forms.Button buttonFile;
		private System.Windows.Forms.Label label1;
		private System.Windows.Forms.Panel panel1;
	}
}
