﻿
using System;
using System.Drawing;
using System.Windows.Forms;

using n_GUIcoder;
using n_GUIset;

namespace n_MesPanel
{
public class MesPanel : Panel
{
	public int StartY;
	public ModuleMessage MM0;
	public ModuleMessage MM1;
	public bool Show1;
	
	public static Font extendFont;
	public static Font extendHeadFont;
	public static Font extendHeadFont0;
	
	bool LeftPress;
	//bool RightPress;
	
	int LastY;
	
	public string ModText;
	
	public int GWidth;
	
	Image[] ModuleImageList;
	
	//构造函数
	public MesPanel() : base()
	{
		this.BorderStyle = BorderStyle.None;
		this.BackColor = Color.Black;
		
		SetStyle(ControlStyles.UserPaint |
		         ControlStyles.AllPaintingInWmPaint |
		         ControlStyles.OptimizedDoubleBuffer |
		         ControlStyles.ResizeRedraw |
		         ControlStyles.SupportsTransparentBackColor,true);
		
		this.Dock = DockStyle.Fill;
		this.ImeMode = ImeMode.NoControl;
		
		this.MouseMove += new MouseEventHandler( UserMouseMove );
		this.MouseUp += new MouseEventHandler( UserMouseUp );
		this.MouseDown += new MouseEventHandler( UserMouseDown );
		this.MouseWheel += new MouseEventHandler( MyMouseWheel );
		
		//extendFont = new Font( "微软雅黑", 11 );
		//extendHeadFont = new Font( "微软雅黑", 11, FontStyle.Bold );
		//extendHeadFont0 = new Font( "微软雅黑", 13, FontStyle.Bold );
		
		extendFont = GUIset.UIFont;
		extendHeadFont = GUIset.UIFont;
		extendHeadFont0 = GUIset.UIFont;
		
		Show1 = true;
	}
	
	//重绘事件
	protected override void OnPaint(PaintEventArgs e)
	{
		ModuleMessage MM = MM1;
		/*
		if( Show1 ) {
			MM = MM1;
		}
		else {
			MM = MM0;
		}
		*/
		if( MM == null ) {
			return;
		}
		//文字起点为下方 20, 宽度间距为 20
		Graphics g = e.Graphics;
		
		g.TranslateTransform( 0, StartY );
		
		//填充背景
		g.Clear( Color.White );
		
		int CurrentHeight = 20;
		int MesWidth = GWidth - 20 - 20;
		
		//绘制模块图片
		if( ModuleImageList != null ) {
			for( int i = 0; i < ModuleImageList.Length; ++i ) {
				int nw = ModuleImageList[i].Width;
				int nh = ModuleImageList[i].Height;
				if( nw > this.Width - 40 ) {
					nw = this.Width - 40;
					nh = ModuleImageList[i].Height * nw / ModuleImageList[i].Width;
				}
				g.DrawImage( ModuleImageList[i], (this.Width - nw)/2, CurrentHeight, nw, nh );
				CurrentHeight += nh + 10;
			}
			CurrentHeight += 20;
		}
		
		ModText = MM1.UserName + " (" + MM0.UserName + ")\n";
		ModText += MM.ModuleMes + "\n";
		
		//绘制组件信息
		g.DrawString( MM1.UserName + "      (" + MM0.UserName + ")", extendHeadFont0, Brushes.ForestGreen, 20, CurrentHeight );
		CurrentHeight += 30;
		SizeF sz0 = GUIset.mg.MeasureString( MM.ModuleMes, extendFont, MesWidth - 20 );
		RectangleF rf0 = new RectangleF( 40, CurrentHeight, sz0.Width, sz0.Height );
		g.DrawString( MM.ModuleMes, extendFont, Brushes.DimGray, rf0 );
		CurrentHeight += (int)sz0.Height;
		CurrentHeight += 15;
		g.DrawLine( Pens.Silver, 100, CurrentHeight, Width - 200, CurrentHeight );
		CurrentHeight += 10;
		
		//绘制组件成员信息
		for( int i = 0; i < MM.MemberMesList.Length; ++i ) {
			if( MM.MemberMesList[ i ].UserName.StartsWith( "OS_" ) ) {
				continue;
			}
			
			string[] tempTypeList = MM.MemberMesList[ i ].Type.Split( '_' );
			if( tempTypeList.Length >= 3 && tempTypeList[2] == FuncExtType.Old ) {
				continue;
			}
			
			CurrentHeight += 5;
			
			ModText += MM.MemberMesList[ i ].UserName + " (" + MM0.MemberMesList[ i ].UserName + ")\n";
			
			//判断Split
			if( MM.MemberMesList[ i ].InnerName != InterfaceType.Split ) {
				g.DrawString( MM.MemberMesList[ i ].UserName.Replace( "_s_", "= " ).Replace( "_ad_", "+= " ) + "      (" + MM0.MemberMesList[ i ].UserName + ")", extendHeadFont, Brushes.Black, 20, CurrentHeight );
			}
			
			SizeF sz = GUIset.mg.MeasureString( MM.MemberMesList[ i ].Mes, extendFont, MesWidth - 20 );
			
			RectangleF rf;
			string MemberMes;
			
			SizeF sf = GUIset.mg.MeasureString( MM.MemberMesList[ i ].UserName + ":", extendHeadFont );
			
			if( MM.MemberMesList[ i ].Mes.StartsWith( ":" ) ) {
				int HeadWidth = (int)sf.Width;
				rf = new RectangleF( 20 + HeadWidth, CurrentHeight, sz.Width, sz.Height );
				CurrentHeight += (int)sf.Height + 5;
				MemberMes = MM.MemberMesList[ i ].Mes.Remove( 0, 1 );
			}
			else {
				CurrentHeight += (int)sf.Height + 5;
				rf = new RectangleF( 40, CurrentHeight, sz.Width, sz.Height );
				MemberMes = MM.MemberMesList[ i ].Mes;
				CurrentHeight += (int)sz.Height;
			}
			
			ModText += MemberMes;
			
			if( MM.MemberMesList[ i ].InnerName != InterfaceType.Split ) {
				g.DrawString( MemberMes, extendFont, Brushes.DimGray, rf );
			}
			else {
				g.DrawString( MemberMes, extendFont, Brushes.ForestGreen, rf );
				CurrentHeight += (int)sf.Height + 5;
			}
		}
	}
	
	//设置图片列表
	public void SetImageList( string BaseName, string FileList )
	{
		if( FileList != "" ) {
			string[] flist = FileList.Split( ',' );
			ModuleImageList = new Image[ flist.Length ];
			for( int i = 0; i < flist.Length; ++i ) {
				ModuleImageList[i] = Image.FromFile( BaseName + flist[i] );
			}
		}
		else {
			ModuleImageList = null;
		}
	}
	
	void MyMouseWheel( object sender, MouseEventArgs e)
	{
		if( e.Delta > 0 ) {
			StartY += 50;
		}
		else {
			StartY -= 50;
		}
		this.Invalidate();
	}
	
	//鼠标按下事件
	void UserMouseDown( object sender, MouseEventArgs e )
	{
		if( e.Button == MouseButtons.Left ) {
			LastY = e.Y;
			LeftPress = true;
		}
		if( e.Button == MouseButtons.Right ) {
			//RightPress = true;
		}
		this.Invalidate();
		
		this.Focus();
	}
	
	//鼠标松开事件
	void UserMouseUp( object sender, MouseEventArgs e )
	{
		if( e.Button == MouseButtons.Left ) {
			LeftPress = false;
		}
		if( e.Button == MouseButtons.Right ) {
			//RightPress = false;
		}
		this.Invalidate();
	}
	
	//鼠标移动事件
	void UserMouseMove( object sender, MouseEventArgs e )
	{
		if( LeftPress ) {
			StartY += e.Y - LastY;
			LastY = e.Y;
			this.Invalidate();
		}
	}
}
}


