﻿
namespace n_HModCommon
{
using System;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Windows.Forms;


public static class HModCommon
{
	public static string GetMes( string[] ConfigList, string Mes )
	{
		for( int i = 0; i < ConfigList.Length; ++i ) {
			
			if( ConfigList[i].StartsWith( "<B>" ) ) {
				string s = ConfigList[i].Remove( 0, 3 );
				
				string[] deflist = s.Split( ';' );
				for( int id = 0; id < deflist.Length; ++id ) {
					string[] ss = deflist[id].Split( ',' );
					Mes = Mes.Replace( ss[0], ss[1] );
				}
			}
		}
		return Mes;
	}
	public static string GetDefine( string[] ConfigList )
	{
		string def = "";
		for( int i = 0; i < ConfigList.Length; ++i ) {
			
			if( ConfigList[i].StartsWith( "<B>" ) ) {
				//string s = cut[i].Remove( 0, 3 );
			}
			else if( ConfigList[i].StartsWith( "*" ) ) {
				//...
			}
			else {
				string[] deflist = ConfigList[i].Split( ';' );
				for( int id = 0; id < deflist.Length; ++id ) {
					string[] ss = deflist[id].Split( ',' );
					def += "#define $" + ss[0] + "$ " + ss[1] + "\n";
				}
			}
		}
		return def;
	}
	public static string GetName( string[] ConfigList, string Name )
	{
		if( ConfigList == null ) {
			return null;
		}
		for( int i = 0; i < ConfigList.Length; ++i ) {
			
			if( !ConfigList[i].StartsWith( "*" ) ) {
				continue;
			}
			string[] deflist = ConfigList[i].Remove( 0, 1 ).Split( ';' );
			for( int id = 0; id < deflist.Length; ++id ) {
				string[] ss = deflist[id].Split( ',' );
				if( Name == ss[0] ) {
					return ss[1];
				}
			}
		}
		return null;
	}
}
}



