﻿
namespace n_UIModule
{
using System;
using System.Drawing;

using c_ControlType;
using n_HardModule;
using n_MyFileObject;
using n_UserModule;
using n_MyFileObjectPanel;
using n_GUIcoder;
using n_GUIset;
using c_MyObjectSet;
using n_MainSystemData;
using n_SG_MyControl;
using n_SG_MyButton;
using n_SG_MyPanel;
using n_SG_MyNumberBox;
using n_SG_MyLabel;
using n_SG_MyTrackBar;
using n_SG_MyCheckBox;
using n_SG_MyMIDI;
using n_SG_MyKeyBoard;
using n_SG_MySprite;
using n_SG_MyWaveBox;
using n_SG_MySound;
using n_SG_MyBitmapBox;
using n_SG_MySerialPort;
using n_SG_MyClock;
using n_SG_MyKeyTrig;
using n_SG_MyBle20;
using n_SG_MyBle40;
using n_SG_MyTelephone;
using n_SG_MyVHTest;
using n_SG_MyOpenCV;
using n_SG_MyCamera;
using n_SG_MyImagePanel;

//*****************************************************
//控件类
public class UIModule: MyFileObject
{
	public int BaseX;
	public int BaseY;
	
	public int BSX;
	public int BSY;
	public int BaseWidth;
	public int BaseHeight;
	
	Image BImage;
	
	Pen 组件高亮边框_Pen1;
	Pen 组件高亮边框_Pen2;
	Pen 组件选中边框_Pen1;
	Pen 组件选中边框_Pen2;
	Font f;
	
	SolidBrush SelectModulebackBrush;
	SolidBrush MouseOnModulebackBrush;
	
	const int SizePadding = 9;
	int MouseOnSizePort;
	int MouseSelectSizePort;
	
	public bool isClientControlPad;
	
	//控件静态ID
	public int VUI_ID;
	
	public MyControl myControl;
	
	//构造函数,根据文件名加载组件,文件名为全路径
	public UIModule( HardModule m ): base()
	{
		HardModule.ScaleChanged -= m.MyScaleChanged;
		
		//无效赋值
		BaseX = 0;
		BaseY = 0;
		
		BSX = 0;
		BSY = 0;
		BaseWidth = 0;
		BaseHeight = 0;
		
		this.CanFloat = true;
		
		MouseOnSizePort = -1;
		MouseSelectSizePort = -1;
		ExXPadding = SizePadding;
		ExYLPadding = SizePadding;
		ExYHPadding = SizePadding;
		
		//初始化GDI对象
		Color mColor;
		if( SystemData.isBlack ) {
			mColor = Color.White;
		}
		else {
			mColor = Color.Black;
		}
		组件高亮边框_Pen1 = new Pen( mColor, 1 );
		组件高亮边框_Pen2 = new Pen( Color.Green, 3 );
		组件选中边框_Pen1 = new Pen( mColor, 2 );
		组件选中边框_Pen2 = new Pen( Color.Pink, 5 );
		
		Color mLineColor;
		if( SystemData.isBlack ) {
			mLineColor = Color.White;
		}
		else {
			mLineColor = Color.Black;
		}
		MouseOnModulebackBrush = new SolidBrush( Color.FromArgb( 90, Color.White ) );
		SelectModulebackBrush = new SolidBrush( Color.FromArgb( 80, Color.Red ) );
		
		f = GUIset.ExpFont;
		
		//拷贝现有模块
		this.isNewTick = m.isNewTick;
		
		this.Name = m.Name;
		this.Language = m.Language;
		this.Angle = m.Angle;
		this.ElementList = m.ElementList;
		this.EventList = m.EventList;
		this.VarList = m.VarList;
		this.InterfaceList = m.InterfaceList;
		this.LinkInterfaceList = m.LinkInterfaceList;
		this.mm = m.mm;
		this.IncPackFilePath = m.IncPackFilePath;
		this.BasePath = m.BasePath;
		this.IncBasePath = m.IncBasePath;
		this.NameList = m.NameList;
		this.LanguageList = m.LanguageList;
		this.ResourceList = m.ResourceList;
		this.UIModuleEXList = m.UIModuleEXList;
		this.BackColor = m.BackColor;
		this.EdgeColor = m.EdgeColor;
		this.ForeColor = m.ForeColor;
		this.Text = m.Text;
		this.TextFont = m.TextFont;
		this.ExtendValue = m.ExtendValue;
		this.MacroFilePath = m.MacroFilePath;
		this.SimulateType = m.SimulateType;
		this.ModuleImageList = m.ModuleImageList;
		this.ChipType = m.ChipType;
		this.GroupMes = m.GroupMes;
		//this.VPanel = m.VPanel;
		//this.myObjectList = m.myObjectList;
		
		//基本模块
		this.ImageName = m.ImageName;
		this.BImage = m.SourceImage;
		this.Width = m.Width;
		this.Height = m.Height;
		this.SX = m.SX;
		this.SY = m.SY;
		this.isClientControlPad = m.isClientControlPad;
		this.isControlModule = m.isControlModule;
		this.isClientChannel = m.isClientChannel;
		this.IPortList = m.IPortList;
		this.SetIPortListOwner( this );
		
		VPanel = new MyFileObjectPanel( this );
		
		VPanel.MouseOnPen = Pens.Blue;
		this.RefreshPanelLocation();
		
		//创建内置控件
		if( this.GetControlType() == ControlType.MachineArm ) {
			//MyControl mc = new MyControlData( 0, ControlType.N_MachineArm );
			//myControl = new MyMachineArm( mc );
		}
		if( this.GetControlType() == ControlType.ControlPad ) {
			myControl = new MyPanel( SX, SY, Width, Height );
		}
		if( this.GetControlType() == ControlType.Panel ) {
			myControl = new MyPanel( SX, SY, Width, Height );
		}
		if( this.GetControlType() == ControlType.Button ) {
			myControl = new MyButton( BackColor, Text, (int)TextFont.Size, ForeColor, SX, SY, Width, Height );
		}
		if( this.GetControlType() == ControlType.Panel ) {
			myControl = new MyPanel( SX, SY, Width, Height );
		}
		if( this.GetControlType() == ControlType.Label ) {
			myControl = new MyLabel( BackColor, Text, (int)TextFont.Size, ForeColor, SX, SY, Width, Height );
		}
		if( this.GetControlType() == ControlType.NumberBox ) {
			myControl = new MyNumberBox( BackColor, 0, (int)TextFont.Size, ForeColor, SX, SY, Width, Height );
		}
		if( this.GetControlType() == ControlType.TrackBar ) {
			myControl = new MyTrackBar( BackColor, ForeColor, SX, SY, Width, Height );
		}
		if( this.GetControlType() == ControlType.CheckBox ) {
			myControl = new MyCheckBox( BackColor, Text, (int)TextFont.Size, ForeColor, SX, SY, Width, Height );
		}
		if( this.GetControlType() == ControlType.WaveBox ) {
			myControl = new MyWaveBox( BackColor, (int)TextFont.Size, ForeColor, SX, SY, Width, Height );
		}
		if( this.GetControlType() == ControlType.BitmapBox ) {
			myControl = new MyBitmapBox( BackColor, ForeColor, SX, SY, Width, Height );
		}
		if( this.GetControlType() == ControlType.ImagePanel ) {
			myControl = new MyImagePanel( SX, SY, Width, Height );
		}
		if( this.GetControlType() == ControlType.MIDI ) {
			myControl = new MyMIDI();
		}
		if( this.GetControlType() == ControlType.KeyBoard ) {
			myControl = new MyKeyBoard();
		}
		if( this.GetControlType() == ControlType.Sound ) {
			myControl = new MySound();
		}
		if( this.GetControlType() == ControlType.SerialPort ) {
			myControl = new MySerialPort( BackColor, (int)TextFont.Size, ForeColor, SX, SY, Width, Height );
		}
		if( this.GetControlType() == ControlType.Clock ) {
			myControl = new MyClock( BackColor, (int)TextFont.Size, ForeColor, SX, SY, Width, Height );
		}
		if( this.GetControlType() == ControlType.KeyTrig ) {
			myControl = new MyKeyTrig();
		}
		if( this.GetControlType() == ControlType.Sprite ) {
			myControl = new MySprite( SX, SY, Width, Height );
		}
		if( this.GetControlType() == ControlType.Ble20 ) {
			myControl = new MyBle20( BackColor, (int)TextFont.Size, ForeColor, SX, SY, Width, Height );
		}
		if( this.GetControlType() == ControlType.Ble40 ) {
			myControl = new MyBle40( BackColor, (int)TextFont.Size, ForeColor, SX, SY, Width, Height );
		}
		if( this.GetControlType() == ControlType.Telephone ) {
			myControl = new MyTelephone( BackColor, (int)TextFont.Size, ForeColor, SX, SY, Width, Height );
		}
		if( this.GetControlType() == ControlType.VHTest ) {
			myControl = new MyVHTest( BackColor, (int)TextFont.Size, ForeColor, SX, SY, Width, Height );
		}
		if( this.GetControlType() == ControlType.OpenCV ) {
			myControl = new MyOpenCV( BackColor, (int)TextFont.Size, ForeColor, SX, SY, Width, Height );
		}
		if( this.GetControlType() == ControlType.Camera ) {
			myControl = new MyCamera( BackColor, Text, (int)TextFont.Size, ForeColor, SX, SY, Width, Height );
		}
		this.RefreshIPortList();
	}
	
	//组件删除
	public override void Remove()
	{
		CommonRemove();
	}
	
	//组件扩展函数
	public override bool MouseIsInEX( int mX, int mY )
	{
		if( mX < this.SX + this.Width + SizePadding && mX > this.SX - SizePadding &&
		    mY < this.SY + this.Height + SizePadding && mY > this.SY - SizePadding ) {
			return true;
		}
		return false;
	}
	
	//获取控件的类型
	public string GetControlType()
	{
		return this.ImageName;
	}
	
	//判断是否为图形控件
	public bool isGUIControl()
	{
		if( GetControlType() == ControlType.ControlPad ||
		    GetControlType() == ControlType.Panel ||
		    GetControlType() == ControlType.Button ||
		    GetControlType() == ControlType.Panel ||
		    GetControlType() == ControlType.NumberBox ||
		    GetControlType() == ControlType.Label ||
		    GetControlType() == ControlType.TrackBar ||
		    GetControlType() == ControlType.CheckBox ||
		    GetControlType() == ControlType.ImagePanel ||
		    GetControlType() == ControlType.MachineArm ||
		    GetControlType() == ControlType.WaveBox ||
		    GetControlType() == ControlType.BitmapBox ||
		    GetControlType() == ControlType.Sprite ||
		    GetControlType() == ControlType.SerialPort ||
		    GetControlType() == ControlType.Ble20 ||
		    GetControlType() == ControlType.Ble40 ||
		    GetControlType() == ControlType.Clock ||
		    GetControlType() == ControlType.VHTest ||
		    GetControlType() == ControlType.OpenCV ||
		    GetControlType() == ControlType.Camera
		  ) {
			return true;
		}
		else {
			return false;
		}
	}
	
	//组件绘制工作1
	//绘制组件的外形等,基础绘制工作
	public override void Draw1( Graphics g )
	{
		if( isNewTick != 0 ) {
			return;
		}
		//判断是否控制板
		if( GetControlType() == ControlType.ControlPad ) {
			MyPanel c = (MyPanel)myControl;
			c.V_StaX = this.SX;
			c.V_StaY = this.SY;
			c.DL_Width = this.Width;
			c.DL_Height = this.Height;
			if( this.ExtendValue != "" ) {
				c.ExtendValue = this.ExtendValue;
			}
			else {
				c.BackColor = this.BackColor;
			}
			c.EdgeColor = this.EdgeColor;
			c.Draw();
			n_SG.SG.MRectangle.Fill( c.BackColor, c.V_StaX, c.V_StaY, c.DL_Width, c.DL_Height );
			n_SG.SG.MRectangle.Draw( c.EdgeColor, 1, c.V_StaX, c.V_StaY, c.DL_Width, c.DL_Height );
			
			/*
			//绘制参考坐标系
			int r = 75;
			int ccX = c.V_StaX;
			int ccY = c.V_StaY + c.DL_Height;
			
			n_SG.SG.MLine.Draw( Color.SlateGray, 1, ccX, ccY, ccX + c.DL_Width, ccY );
			n_SG.SG.MLine.Draw( Color.SlateGray, 1, ccX, ccY, ccX, ccY - c.DL_Height );
			
			//绘制X方向的标尺
			for( int i = 0; i <= c.DL_Width / 10; ++i ) {
				if( i % 10 == 0 ) {
					n_SG.SG.MLine.Draw( Color.SlateGray, 1, ccX + i*10, ccY, ccX + i*10, ccY + 3 );
					n_SG.SG.MString.Draw( (i * 10).ToString(), Color.SlateGray, 9, 1, 0, ccX + i*10, ccY + 8 );
				}
				else {
					n_SG.SG.MLine.Draw( Color.SlateGray, 1, ccX + i*10, ccY + 1, ccX + i*10, ccY + 2 );
				}
			}
			//绘制Y方向的标尺
			for( int i = 0; i <= c.DL_Height / 10; ++i ) {
				if( i % 10 == 0 ) {
					n_SG.SG.MLine.Draw( Color.SlateGray, 1, ccX, ccY - i*10, ccX - 3, ccY - i*10 );
					n_SG.SG.MString.Draw( (i * 10).ToString(), Color.SlateGray, 9, 2, 1, ccX, ccY - i*10 );
				}
				else {
					n_SG.SG.MLine.Draw( Color.SlateGray, 1, ccX - 1, ccY - i*10, ccX - 2, ccY - i*10 );
				}
			}
			*/
			
			/*
			ccX -= 20;
			ccY += 20;
			n_SG.SG.MLine.Draw( Color.Black, 1, ccX - r + 20, ccY, ccX + r - 20, ccY );
			n_SG.SG.MLine.Draw( Color.Black, 1, ccX, ccY - r + 20, ccX, ccY + r - 20 );
			n_SG.SG.MCircle.Draw( Color.Yellow, 1, ccX, ccY, 10 );
			n_SG.SG.MString.DrawAtCenter( "0°", Color.Yellow, 9, ccX + r - 20, ccY - 8 );
			n_SG.SG.MString.DrawAtCenter( "90°", Color.Yellow, 9, ccX, ccY - r + 20 - 8 );
			n_SG.SG.MString.DrawAtCenter( "180°", Color.Yellow, 9, ccX - r + 20, ccY - 8 );
			n_SG.SG.MString.DrawAtCenter( "270°", Color.Yellow, 9, ccX, ccY + r - 20 + 8 );
			*/
		}
		//判断是否面板
		else if( GetControlType() == ControlType.Panel ) {
			MyPanel c = (MyPanel)myControl;
			c.V_StaX = this.SX;
			c.V_StaY = this.SY;
			c.DL_Width = this.Width;
			c.DL_Height = this.Height;
			if( this.ExtendValue != "" ) {
				c.ExtendValue = this.ExtendValue;
			}
			else {
				c.BackColor = this.BackColor;
			}
			c.EdgeColor = this.EdgeColor;
			c.Draw();
		}
		//判断是否为按钮
		else if( GetControlType() == ControlType.Button ) {
			MyButton c = (MyButton)myControl;
			c.Text = this.Text;
			c.V_StaX = this.SX;
			c.V_StaY = this.SY;
			c.DL_Width = this.Width;
			c.DL_Height = this.Height;
			c.BackColor = this.BackColor;
			c.EdgeColor = this.EdgeColor;
			c.ForeColor = this.ForeColor;
			c.FontSize = (int)this.TextFont.Size;
			
			c.Draw();
		}
		//判断是否为标签
		else if( GetControlType() == ControlType.Label ) {
			MyLabel c = (MyLabel)myControl;
			c.Text = this.Text;
			c.V_StaX = this.SX;
			c.V_StaY = this.SY;
			c.DL_Width = this.Width;
			c.DL_Height = this.Height;
			c.BackColor = this.BackColor;
			c.EdgeColor = this.EdgeColor;
			c.ForeColor = this.ForeColor;
			c.FontSize = (int)this.TextFont.Size;
			c.Draw();
		}
		//判断是否为数字框
		else if( GetControlType() == ControlType.NumberBox ) {
			MyNumberBox c = (MyNumberBox)myControl;
			try {
			c.DL_Value = int.Parse( this.Text );
			} catch {
			c.DL_Value = 0;
			}
			c.V_StaX = this.SX;
			c.V_StaY = this.SY;
			c.DL_Width = this.Width;
			c.DL_Height = this.Height;
			c.BackColor = this.BackColor;
			c.ForeColor = this.ForeColor;
			c.FontSize = (int)this.TextFont.Size;
			c.Draw();
		}
		//判断是否为滑动条
		else if( GetControlType() == ControlType.TrackBar ) {
			MyTrackBar c = (MyTrackBar)myControl;
			c.V_StaX = this.SX;
			c.V_StaY = this.SY;
			c.DL_Width = this.Width;
			c.DL_Height = this.Height;
			c.BackColor = this.BackColor;
			c.ForeColor = this.ForeColor;
			c.Draw();
		}
		//判断是否为检查框
		else if( GetControlType() == ControlType.CheckBox ) {
			MyCheckBox c = (MyCheckBox)myControl;
			c.Text = this.Text;
			c.V_StaX = this.SX;
			c.V_StaY = this.SY;
			c.DL_Width = this.Width;
			c.DL_Height = this.Height;
			c.BackColor = this.BackColor;
			c.ForeColor = this.ForeColor;
			c.FontSize = (int)this.TextFont.Size;
			c.Draw();
		}
		//判断是否为图片面板
		else if( GetControlType() == ControlType.ImagePanel ) {
			MyImagePanel c = (MyImagePanel)myControl;
			//c.Text = this.Text;
			c.V_StaX = this.SX;
			c.V_StaY = this.SY;
			c.DL_Width = this.Width;
			c.DL_Height = this.Height;
			if( this.ExtendValue != "" ) {
				c.ExtendValue = this.ExtendValue;
			}
			//c.BackColor = this.BackColor;
			//c.ForeColor = this.ForeColor;
			//c.FontSize = (int)this.TextFont.Size;
			c.Draw();
		}
		else if( GetControlType() == ControlType.KeyBoard ) {
			MyKeyBoard c = (MyKeyBoard)myControl;
			c.KeyList = this.ExtendValue;
			c.V_StaX = this.SX;
			c.V_StaY = this.SY;
			c.DL_Width = this.Width;
			c.DL_Height = this.Height;
			c.Draw();
		}
		else if( GetControlType() == ControlType.MachineArm ) {
			//MyMachineArm mc = (MyMachineArm)myControl;
			//mc.MidX = this.MidX;
			//mc.MidY = this.MidY;
			//mc.Draw( g, StartX, StartY );
			g.DrawRectangle( Pens.White, SX, SY, Width - 1, Height - 1 );
		}
		//判断是否为数字框
		else if( GetControlType() == ControlType.WaveBox ) {
			MyWaveBox c = (MyWaveBox)myControl;
			c.V_StaX = this.SX;
			c.V_StaY = this.SY;
			if( c.DL_Width != this.Width || c.DL_Height != this.Height ) {
				c.DL_Width = this.Width;
				c.DL_Height = this.Height;
				c.RefreshBaseData();
			}
			c.BackColor = this.BackColor;
			c.ForeColor = this.ForeColor;
			c.FontSize = (int)this.TextFont.Size;
			c.Draw();
		}
		//判断是否为点阵框
		else if( GetControlType() == ControlType.BitmapBox ) {
			MyBitmapBox c = (MyBitmapBox)myControl;
			c.V_StaX = this.SX;
			c.V_StaY = this.SY;
			if( c.DL_Width != this.Width || c.DL_Height != this.Height ) {
				c.DL_Width = this.Width;
				c.DL_Height = this.Height;
				c.RefreshBaseData();
			}
			if( c.BackColor != this.BackColor || c.ForeColor != this.ForeColor ) {
				c.BackColor = this.BackColor;
				c.ForeColor = this.ForeColor;
				c.RefreshBaseData();
			}
			c.BackColor = this.BackColor;
			c.ForeColor = this.ForeColor;
			c.Draw();
		}
		else if( GetControlType() == ControlType.MIDI ) {
			g.DrawString( Name, f, Brushes.White, SX, SY - 30 );
			g.DrawImage( BImage, SX, SY );
			if( ExtendValue != "" ) {
				string[] cut = ExtendValue.Split( '*' );
				if( cut.Length >= 1 && cut[0] != "" ) {
					g.DrawString( cut[0], f, Brushes.White, SX, SY + Height );
				}
				if( cut.Length >= 2 && cut[1] != "" ) {
					g.DrawString( cut[1], f, Brushes.White, SX, SY + Height + 20 );
				}
			}
		}
		else if( GetControlType() == ControlType.Sound ) {
			g.DrawString( Name, f, Brushes.White, SX, SY - 30 );
			g.DrawImage( BImage, SX, SY );
			if( ExtendValue != "" ) {
				string[] cut = ExtendValue.Split( '*' );
				for( int i = 0; i < cut.Length; ++i ) {
					if( cut[i] != "" ) {
						g.DrawString( (i+1) + ":  " + cut[i], f, Brushes.LightGray, SX, SY + Height + i*30 );
					}
				}
			}
		}
		else if( GetControlType() == ControlType.SerialPort ) {
			MySerialPort c = (MySerialPort)myControl;
			c.V_StaX = this.SX;
			c.V_StaY = this.SY;
			c.DL_Width = this.Width;
			c.DL_Height = this.Height;
			c.BackColor = this.BackColor;
			c.ForeColor = this.ForeColor;
			c.FontSize = (int)this.TextFont.Size;
			c.Draw();
			//g.DrawString( Name, f, Brushes.White, StartX + SX, StartY + SY - 30 );
			//g.DrawImage( BImage, StartX + SX, StartY + SY );
			if( ExtendValue != "" ) {
				g.DrawString( ExtendValue, f, Brushes.LightGray, SX, SY + Height );
			}
		}
		else if( GetControlType() == ControlType.Clock ) {
			MyClock c = (MyClock)myControl;
			c.V_StaX = this.SX;
			c.V_StaY = this.SY;
			c.DL_Width = this.Width;
			c.DL_Height = this.Height;
			c.BackColor = this.BackColor;
			c.ForeColor = this.ForeColor;
			c.FontSize = (int)this.TextFont.Size;
			c.Draw();
		}
		else if( GetControlType() == ControlType.KeyTrig ) {
			g.DrawString( Name, f, Brushes.White, SX, SY - 30 );
			g.DrawImage( BImage, SX, SY );
		}
		else if( GetControlType() == ControlType.Sprite ) {
			MySprite c = (MySprite)myControl;
			
			//这两个需要同步
			c.ImageNumber = 1;
			c.ExtendValue = this.ExtendValue;
			
			/*
			n_ValueSet.ValueSet.SetValue( c.ExtendValue );
			string RolX = n_ValueSet.ValueSet.GetValue( "RolX" );
			string RolY = n_ValueSet.ValueSet.GetValue( "RolY" );
			if( RolX != null && RolY != null ) {
				c.DL_RolOffsetX = (int)(c.DL_Width * double.Parse( RolX ));
				c.DL_RolOffsetY = (int)(c.DL_Height * double.Parse( RolY ));
			}
			*/
			c.Text = this.Text;
			c.V_StaX = this.SX;
			c.V_StaY = this.SY;
			c.DL_Width = this.Width;
			c.DL_Height = this.Height;
			c.BackColor = this.BackColor;
			c.ForeColor = this.ForeColor;
			c.FontSize = (int)this.TextFont.Size;
			c.Draw();
		}
		else if( GetControlType() == ControlType.Ble20 ) {
			
			MyBle20 c = (MyBle20)myControl;
			c.V_StaX = this.SX;
			c.V_StaY = this.SY;
			c.DL_Width = this.Width;
			c.DL_Height = this.Height;
			c.BackColor = this.BackColor;
			c.ForeColor = this.ForeColor;
			c.FontSize = (int)this.TextFont.Size;
			c.Draw();
			
			//g.DrawString( Name, f, Brushes.White, StartX + SX, StartY + SY - 30 );
			//g.DrawImage( BImage, StartX + SX, StartY + SY );
			if( ExtendValue != "" ) {
				g.DrawString( ExtendValue, f, Brushes.LightGray, SX, SY + Height );
			}
		}
		else if( GetControlType() == ControlType.Ble40 ) {
			
			MyBle40 c = (MyBle40)myControl;
			c.V_StaX = this.SX;
			c.V_StaY = this.SY;
			c.DL_Width = this.Width;
			c.DL_Height = this.Height;
			c.BackColor = this.BackColor;
			c.ForeColor = this.ForeColor;
			c.FontSize = (int)this.TextFont.Size;
			c.Draw();
			
			//g.DrawString( Name, f, Brushes.White, StartX + SX, StartY + SY - 30 );
			//g.DrawImage( BImage, StartX + SX, StartY + SY );
			if( ExtendValue != "" ) {
				g.DrawString( ExtendValue, f, Brushes.LightGray, SX, SY + Height );
			}
		}
		else if( GetControlType() == ControlType.Ble20 ) {
			
			g.DrawString( Name, f, Brushes.White, SX, SY - 30 );
			g.DrawImage( BImage, SX, SY );
		}
		else if( GetControlType() == ControlType.VHTest ) {
			MyVHTest c = (MyVHTest)myControl;
			c.V_StaX = this.SX;
			c.V_StaY = this.SY;
			c.DL_Width = this.Width;
			c.DL_Height = this.Height;
			c.BackColor = this.BackColor;
			c.ForeColor = this.ForeColor;
			c.FontSize = (int)this.TextFont.Size;
			c.Draw();
			//g.DrawString( Name, f, Brushes.White, StartX + SX, StartY + SY - 30 );
			//g.DrawImage( BImage, StartX + SX, StartY + SY );
			if( ExtendValue != "" ) {
				g.DrawString( ExtendValue, f, Brushes.LightGray, SX, SY + Height );
			}
		}
		else if( GetControlType() == ControlType.OpenCV ) {
			MyOpenCV c = (MyOpenCV)myControl;
			c.V_StaX = this.SX;
			c.V_StaY = this.SY;
			c.DL_Width = this.Width;
			c.DL_Height = this.Height;
			c.BackColor = this.BackColor;
			c.ForeColor = this.ForeColor;
			c.FontSize = (int)this.TextFont.Size;
			c.Draw();
			//g.DrawString( Name, f, Brushes.White, StartX + SX, StartY + SY - 30 );
			//g.DrawImage( BImage, StartX + SX, StartY + SY );
			if( ExtendValue != "" ) {
				g.DrawString( ExtendValue, f, Brushes.LightGray, SX, SY + Height );
			}
		}
		//判断是否为系统摄像机
		else if( GetControlType() == ControlType.Camera ) {
			MyCamera c = (MyCamera)myControl;
			c.Text = this.Text;
			c.V_StaX =  this.SX;
			c.V_StaY = this.SY;
			c.DL_Width = this.Width;
			c.DL_Height = this.Height;
			c.BackColor = this.BackColor;
			c.ForeColor = this.ForeColor;
			c.FontSize = (int)this.TextFont.Size;
			c.Draw();
		}
		//到这里为其他的图形控件
		else {
			g.DrawString( Name, f, Brushes.White, SX, SY - 30 );
			g.DrawImage( BImage, SX, SY );
		}
		if( isSelect ) {
			g.DrawRectangle( Pens.Red, this.SX, this.SY, this.Width, this.Height );
			g.DrawRectangle( Pens.Green, this.SX + 1, this.SY + 1, this.Width - 2, this.Height - 2 );
		}
		//绘制常量信息
		if( this.myObjectList.isModuleLib ) {
			return;
		}
		int hh = SY + 25;
		int ListNumber = VarList.Length;
		for( int i = 0; i < ListNumber; ++i ) {
			string VarName = VarList[ i ][ GetLC() ];
			
			string Value = VarList[ i ][ 0 ];
			
			string[] VarTypeList = VarList[ i ][ 1 ].Split( '_' );
			//string VarType = VarTypeList[ 1 ];
			if( VarTypeList.Length >= 4 ) {
				g.DrawString( VarName + ": " + Value + " " + VarTypeList[3], GUIset.ExpFont, Brushes.LightBlue, SX + Width + 5, hh );
			}
			else {
				g.DrawString( VarName + ": " + Value, GUIset.ExpFont, Brushes.LightBlue, SX + Width + 5, hh );
			}
			hh += 20;
		}
	}
	
	//组件绘制工作2
	//绘制组件的端口链接导线,需要在所有组件基础绘制完成之后进行
	//绘制端口的方框背景
	public override void Draw2( Graphics g )
	{
		if( isNewTick != 0 ) {
			return;
		}
		if( GetControlType() == ControlType.Sprite ) {
			((MySprite)myControl).DrawEage();
		}
	}
	
	//组件绘制工作3
	//绘制组件的端口和名称,需要在所有链接导线绘制完成之后进行
	public override void Draw3( Graphics g )
	{
		if( isNewTick != 0 ) {
			return;
		}
		//绘制接口
		
		this.DrawInterface( g );
		
		//if( myObjectList.MouseOnObject != this ) {
		//	return;
		//}
	}
	
	public override void DrawSim( Graphics g )
	{
		
	}
	
	//组件绘制工作4
	//绘制组件高亮选中
	public override void DrawHighLight( Graphics g )
	{
		if( isNewTick != 0 ) {
			return;
		}
		
		//g.DrawRectangle( Pens.White, X, Y, Width, Height );
		if( (!isMousePress || (isMousePress && MouseSelectSizePort != -1 ) ) && this.isGUIControl() ) {
			
			//绘制控件的坐标和尺寸等
			/*
			if( this.myObjectList.ClientPad != null ) {
				int hh = StartY + SY;
				int Perhh = 30;
				Brush tb = Brushes.Yellow;
				g.DrawString( "中心X: " + (this.MidX - this.myObjectList.ClientPad.SX), GUIset.ModuleNameFont, tb, StartX + SX + Width + 15, hh + 0*Perhh );
				g.DrawString( "中心Y: " + (this.MidY - this.myObjectList.ClientPad.SY), GUIset.ModuleNameFont, tb, StartX + SX + Width + 15, hh + 1*Perhh );
				g.DrawString( "左边X: " + (this.SX - this.myObjectList.ClientPad.SX), GUIset.ModuleNameFont, tb, StartX + SX + Width + 15, hh + 2*Perhh );
				g.DrawString( "右边X: " + (this.SX + this.Width - this.myObjectList.ClientPad.SX), GUIset.ModuleNameFont, tb, StartX + SX + Width + 15, hh + 3*Perhh );
				g.DrawString( "上边Y: " + (this.SY - this.myObjectList.ClientPad.SY), GUIset.ModuleNameFont, tb, StartX + SX + Width + 15, hh + 4*Perhh );
				g.DrawString( "下边Y: " + (this.SY + this.Height - this.myObjectList.ClientPad.SY), GUIset.ModuleNameFont, tb, StartX + SX + Width + 15, hh + 5*Perhh );
				g.DrawString( "宽度: " + this.Width, GUIset.ModuleNameFont, tb, StartX + SX + Width + 15, hh + 6*Perhh );
				g.DrawString( "高度: " + this.Height, GUIset.ModuleNameFont, tb, StartX + SX + Width + 15, hh + 7*Perhh );
			}
			*/
			if( this.GetControlType() != ControlType.MachineArm ) {
				for( int i = 0; i < 8; ++i ) {
					//if( //this.GetControlType() == ControlType.NumberBox ||
					//   this.GetControlType() == ControlType.TrackBar
					//  ) {
					//	if( i != 3 && i != 7 ) {
					//		continue;
					//	}
					//}
					Rectangle r = GetRectangleOfSizePort( i );
					if( MouseOnSizePort == i ) {
						g.FillRectangle( Brushes.Orange, r.X, r.Y, r.Width - 1, r.Height - 1 );
						g.DrawRectangle( Pens.OrangeRed, r.X, r.Y, r.Width - 1, r.Height - 1 );
					}
					else {
						g.FillRectangle( Brushes.LightGreen, r.X, r.Y, r.Width - 1, r.Height - 1 );
						g.DrawRectangle( Pens.Green, r.X, r.Y, r.Width - 1, r.Height - 1 );
					}
				}
			}
		}
		if( isMousePress ) {
			return;
		}
		int x = SX - 4;
		int y = SY - 4;
		int w = Width - 1 + 8;
		int h = Height - 1 + 8;
		//g.DrawRectangle( Pens.Red, x, y, w, h );
		this.VPanel.Draw( g );
	}
	
	//鼠标按下事件, 在组件中返回 true,不在组件中返回 false.
	public override bool MouseDown1( int mX, int mY )
	{
		if( !MouseIsInside( mX, mY ) ) {
			if( isHighLight ) {
				isHighLight = false;
			}
			return false;
		}
		if( this.myObjectList.MouseOnObject == this ) {
			
			RefreshFloatMessage();
			
			//注意,还有鼠标移动事件中也有在这段代码,注意同步
			MouseSelectSizePort = MouseOnSizePort;
			
			if( isMouseOnBase ) {
				isMousePress = true;
				Last_mX = mX;
				Last_mY = mY;
			}
			BaseX = SX;
			BaseY = SY;
			
			BSX = SX;
			BSY = SY;
			BaseWidth = Width;
			BaseHeight = Height;
			
			//判断需要搜索位置还是搜索尺寸
			if( MouseSelectSizePort == -1 ) {
				this.myObjectList.ScanLocation( this );
			}
			else {
				this.myObjectList.ScanSize( this );
			}
			//更新常用控件索引,用于新控件的参数设定
			if( this.isGUIControl() && !this.isClientControlPad ) {
				this.myObjectList.LastUIModule = this;
			}
		}
		//接口鼠标点击事件
		IPortListMouseDown( mX, mY );
		
		return true;
	}
	
	//鼠标松开事件
	public override void MouseUp1( int mX, int mY )
	{
		if( isNewTick == 0 ) {
			isMousePress = false;
			ignoreHit = false;
		}
	}
	
	//鼠标移动事件,当组件被拖动时返回 true
	public override int MouseMove1( int mX, int mY )
	{
		if( isNewTick == MaxNewTick ) {
			isNewTick--;
			//注意,还有鼠标按下事件中也有在这段代码,注意同步
			isMousePress = true;
			//this.SX = mX - this.SX;
			//this.SY = mY - this.SY;
			this.SX = mX - this.Width / 2;
			this.SY = mY - this.Height / 2;
			BaseX = this.SX;
			BaseY = this.SY;
			this.RefreshPanelLocation();
			Last_mX = mX;
			Last_mY = mY;
			this.myObjectList.ScanLocation( this );
			return 2;
		}
		if( isNewTick > 0 ) {
			isNewTick--;
		}
		bool isMouseIn = MouseIsInside( mX, mY );
		if( !isMouseIn && !isMousePress ) {
			return 0;
		}
		
		//接口鼠标悬停事件
		for( int i = 0; i < IPortList.Length; ++i ) {
			if( IPortList[ i ].isMouseInPort( mX, mY  ) ) {
				myObjectList.MouseOnIPort = IPortList[ i ];
			}
		}
		
		if( this.isGUIControl() ) {
			MouseOnSizePort = GetNumberOfSizePort( mX, mY );
		}
		
		//如果没有鼠标按下,直接返回
		if( !isMousePress ) {
			return 1;
		}
		
		RefreshFloatMessage();
		
		//如果按下尺寸调节方框
		if( MouseSelectSizePort == -1 ) {
			
			//如果按下的是面板,同时拖动所有的控件, 但是在刚添加模块时则不移动其他模块
			if( this.isClientControlPad && n_ImagePanel.ImagePanel.NewObject == null ) {
				foreach( n_MyObject.MyObject mo in myObjectList ) {
					if( mo is UIModule ) {
						UIModule um = (UIModule)mo;
						if( um.isGUIControl() ) {
							um.SX += mX - Last_mX;
							um.SY += mY - Last_mY;
							um.RefreshPanelLocation();
						}
					}
				}
			}
			else {
				//面板在上边的循环中已经拖动过了
				//SX += mX - Last_mX;
				//SY += mY - Last_mY;
				
				//自动调节位置
				BaseX += mX - Last_mX;
				BaseY += mY - Last_mY;
				int x = 0;
				int y = 0;
				this.myObjectList.AutoSetLocation( this, ref x, ref y );
				SX = x;
				SY = y;
			}
			Last_mX = mX;
			Last_mY = mY;
		}
		else {
			//BSX = this.SX;
			//BSY = this.SY;
			int BWidth = this.BaseWidth;
			int BHeight = this.BaseHeight;
			int bsx = BSX;
			int bsy = BSY;
			switch( MouseSelectSizePort ) {
				case 0: bsy += mY - Last_mY; BHeight -= mY - Last_mY; bsx += mX - Last_mX; BWidth -= mX - Last_mX; break;
				case 1: bsy += mY - Last_mY; BHeight -= mY - Last_mY; break;
				case 2: bsy += mY - Last_mY; BHeight -= mY - Last_mY; BWidth += mX - Last_mX; break;
				case 3: BWidth += mX - Last_mX; break;
				
				case 4: BWidth += mX - Last_mX; BHeight += mY - Last_mY; break;
				case 5: BHeight += mY - Last_mY; break;
				case 6: BHeight += mY - Last_mY; bsx += mX - Last_mX; BWidth -= mX - Last_mX; break;
				case 7: bsx += mX - Last_mX; BWidth -= mX - Last_mX; break;
				default: break;
			}
			if( BWidth >= SizePadding + 2 ) {
				//this.SX = BSX;
				this.BaseWidth = BWidth;
				Last_mX = mX;
				BSX = bsx;
			}
			if( BHeight >= SizePadding + 2 ) {
				//this.SY = BSY;
				this.BaseHeight = BHeight;
				Last_mY = mY;
				BSY = bsy;
			}
			//设置尺寸
			int w = 0;
			int h = 0;
			this.myObjectList.AutoSetSize( this, ref w, ref h );
			this.Width = w;
			this.Height = h;
			switch( MouseSelectSizePort ) {
				case 0: this.SX = BSX - (this.Width - this.BaseWidth); this.SY = BSY - (this.Height - this.BaseHeight); break;
				case 1: this.SX = BSX; this.SY = BSY - (this.Height - this.BaseHeight); break;
				case 2: this.SX = BSX; this.SY = BSY - (this.Height - this.BaseHeight); break;
				case 3: this.SX = BSX; this.SY = BSY; break;
				
				case 4: this.SX = BSX; this.SY = BSY; break;
				case 5: this.SX = BSX; this.SY = BSY; break;
				case 6: this.SX = BSX - (this.Width - this.BaseWidth); this.SY = BSY; break;
				case 7: this.SX = BSX - (this.Width - this.BaseWidth); this.SY = BSY; break;
				default: break;
			}
			this.RefreshIPortList();
		}
		this.RefreshPanelLocation();
		return 2;
	}
	
	//根据方向信息转换一个坐标的方向坐标
	public static Point GetDirPoint( int Power, Point p, E_DirType dir )
	{
		switch( dir ) {
			case E_DirType.UP:		return new Point( p.X, p.Y - Power );
			case E_DirType.DOWN:	return new Point( p.X, p.Y + Power );
			case E_DirType.LEFT:	return new Point( p.X - Power, p.Y );
			case E_DirType.RIGHT:	return new Point( p.X + Power, p.Y );
			default:				return Point.Empty;
		}
	}
	
	//判断坐标在哪个端口上, 从左上角开始顺时针依次为 0 1 2 3 4 5 6 7, 不在任何端口则返回-1
	int GetNumberOfSizePort( int mX, int mY )
	{
		if( this.GetControlType() == ControlType.MachineArm ) {
			return -1;
		}
		for( int i = 0; i < 8; ++i ) {
			//if( //this.GetControlType() == ControlType.NumberBox ||
			//    this.GetControlType() == ControlType.TrackBar
			//  ) {
			//	if( i != 3 && i != 7 ) {
			//		continue;
			//	}
			//}
			if( GetRectangleOfSizePort( i ).Contains( mX, mY ) ) {
				return i;
			}
		}
		return -1;
	}
	
	//获取指定索引的尺寸端口
	Rectangle GetRectangleOfSizePort( int Index )
	{
		switch ( Index ) {
			case 0: return new Rectangle( this.SX - SizePadding, this.SY - SizePadding, SizePadding, SizePadding );
			case 1: return new Rectangle( this.MidX - SizePadding / 2, this.SY - SizePadding, SizePadding, SizePadding );
			case 2: return new Rectangle( this.SX + this.Width, this.SY - SizePadding, SizePadding, SizePadding );
			case 3: return new Rectangle( this.SX + this.Width, this.MidY - SizePadding / 2, SizePadding, SizePadding );
			case 4: return new Rectangle( this.SX + this.Width, this.SY + this.Height, SizePadding, SizePadding );
			case 5: return new Rectangle( this.MidX - SizePadding / 2, this.SY + this.Height, SizePadding, SizePadding );
			case 6: return new Rectangle( this.SX - SizePadding, this.SY + this.Height, SizePadding, SizePadding );
			case 7: return new Rectangle( this.SX - SizePadding, this.MidY - SizePadding / 2, SizePadding, SizePadding );
			default: return Rectangle.Empty;
		}
	}
	
	void RefreshFloatMessage()
	{
		if( this.myObjectList.ClientPad == null ) {
			return;
		}
		/*
		//绘制控件的坐标和尺寸等
		if( MyControl.Y_Default ) {
			string Mes = 
			"中心X: " + (this.MidX - this.myObjectList.ClientPad.SX) + "\n" +
			"中心Y: " + (this.MidY - this.myObjectList.ClientPad.SY) + "\n" +
			"左边X: " + (this.SX - this.myObjectList.ClientPad.SX) + "\n" +
			"上方Y: " + (this.SY - this.myObjectList.ClientPad.SY) + "\n" +
			"右边X: " + (this.SX + this.Width - this.myObjectList.ClientPad.SX) + "\n" +
			"下边Y: " + (this.SY + this.Height - this.myObjectList.ClientPad.SY) + "\n" +
			"宽度: " + this.Width + "\n" +
			"高度: " + this.Height;
			G.CGPanel.GHead.myFloatPanel.SetMessage( this.Name, Mes );
		}
		else {
			string Mes = 
			"中心X: " + (this.MidX - this.myObjectList.ClientPad.SX) + "\n" +
			"中心Y: " + (this.myObjectList.ClientPad.SY + this.myObjectList.ClientPad.Height - this.MidY) + "\n" +
			"左边X: " + (this.SX - this.myObjectList.ClientPad.SX) + "\n" +
			"上边Y: " + (this.myObjectList.ClientPad.SY + this.myObjectList.ClientPad.Height - this.SY) + "\n" +
			"右边X: " + (this.SX + this.Width - this.myObjectList.ClientPad.SX) + "\n" +
			"下边Y: " + (this.myObjectList.ClientPad.SY + this.myObjectList.ClientPad.Height - this.SY - this.Height) + "\n" +
			"宽度: " + this.Width + "\n" +
			"高度: " + this.Height;
			G.CGPanel.GHead.myFloatPanel.SetMessage( this.Name, Mes );
		}
		*/
	}
}
}



