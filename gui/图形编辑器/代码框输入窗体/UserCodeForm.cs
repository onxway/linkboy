﻿
namespace n_UserCodeForm
{
using System;
using System.Windows.Forms;
using n_ImagePanel;
using System.Drawing;
using System.Drawing.Drawing2D;
using c_FormMover;

/// <summary>
/// Description of WorkForm.
/// </summary>
public partial class UserCodeForm : Form
{
	bool isOK;
	FormMover fm;
	
	n_GNote.GNote myOwner;
	
	//主窗口
	public UserCodeForm()
	{
		InitializeComponent();
		this.ImeMode = ImeMode.Off;
		isOK = false;
		
		fm = new FormMover( this );
	}
	
	//运行
	public string Run( n_GNote.GNote o, string Mes )
	{
		myOwner = o;
		
		if( this.Visible ) {
			return null;
		}
		//注意这里是要保持组件为选中状态, 防止窗体关闭后组件取消高亮造成的闪烁
		//ImagePanel TargetGPanel = ( (FileTabPage)A.WorkBox.TexttabControl.SelectedTab ).GPanel;
		//TargetGPanel.SelPanel.SelectState = 3;
		
		if( Mes == "请在此输入您的程序代码" ) {
			this.richTextBox1.ForeColor = Color.Gray;
		}
		else {
			this.richTextBox1.ForeColor = Color.Black;
		}
		
		this.richTextBox1.Text = Mes;
		n_wlibCom.wlibCom.SetFont( richTextBox1 );
		
		this.Visible = true;
		//this.ShowDialog();
		
		while( this.Visible ) {
			System.Windows.Forms.Application.DoEvents();
		}
		if( isOK ) {
			return this.richTextBox1.Text;
		}
		return null;
	}
	
	void Button确定Click(object sender, EventArgs e)
	{
		this.richTextBox1.Focus();
		isOK = true;
		this.Visible = false;
	}
	
	void RichTextBox1MouseDown(object sender, MouseEventArgs e)
	{
		if( this.richTextBox1.Text == "请在此输入您的程序代码" ) {
			this.richTextBox1.Text = "";
			this.richTextBox1.ForeColor = Color.Black;
		}
	}
	
	void NoteFormFormClosing(object sender, FormClosingEventArgs e)
	{
		e.Cancel = true;
		this.richTextBox1.Focus();
		isOK = false;
		this.Visible = false;
	}
	
	void ButtonAutoClick(object sender, EventArgs e)
	{
		string Result = "";
		
		if( myOwner.GroupMes == null ) {
			Result += "<name> # #,\n";
		}
		else {
			Result += "<name> " + myOwner.GroupMes + " " + myOwner.GroupMes + ",\n";
		}
		Result += "未找到此框架:" + myOwner.GroupMes + "的描述信息\n";
		
		Result += "</name>\n";
		
		foreach( n_MyObject.MyObject mo in G.CGPanel.myModuleList ) {
			
			if( myOwner.GroupMes == null && mo.GroupMes == null || myOwner.GroupMes == mo.GroupMes ) {
				
				if( mo is n_GVar.GVar ) {
					
					Result += "<member> temp " + mo.Name + " " + mo.Name + ",\n";
					Result += "这是用户的自定义变量:" + mo.Name + ", 未找到此变量的描述信息\n";
					Result += "</member>\n";
				}
				
			}
		}
		
		foreach( n_MyObject.MyObject mo in G.CGPanel.myModuleList ) {
			
			if( myOwner.GroupMes == null && mo.GroupMes == null || myOwner.GroupMes == mo.GroupMes ) {
				
				if( mo is n_UserFunctionIns.UserFunctionIns ) {
					
					n_UserFunctionIns.UserFunctionIns ui = (n_UserFunctionIns.UserFunctionIns)mo;
					
					Result += "<member> temp " + ui.GetFuncName() + " " + ui.GetFuncName() + ",\n";
					Result += "这是用户的自定义函数:" + ui.GetFuncName() + ", 未找到此函数的描述信息\n";
					Result += "</member>\n";
				}
				
			}
		}
		
		this.richTextBox1.Text = Result;
		this.richTextBox1.ForeColor = Color.Black;
	}
}
}



