﻿
namespace n_EnginePair
{
using System;
using System.Drawing;
using System.Drawing.Drawing2D;

using n_DescriptionForm;
using n_GUIcoder;
using n_GUIset;
using n_MainSystemData;
using n_GVar;
using n_HardModule;
using n_ModuleLibDecoder;
using n_MyObject;
using n_MyObjectList;
using n_OS;
using n_Shape;
using n_UIModule;
using n_GNote;
using n_MesPanel;

//*****************************************************
//
public static class EnginePair
{
	static bool isTest;
	
	public static bool ShowDetail;
	
	public static int SX;
	public static int SY;
	
	public static bool isMouseOn;
	
	static float MinX = 0, MaxX = 0;
	static float MinY = 0, MaxY = 0;
	
	public static MyObjectList myModuleList;
	
	static Brush ModuleMesBackBrush;
	
	public const int Padding = 70;
	
	//组件选择事件委托
	public delegate void myModuleSelectEventHandler( MyObject m, int x, int y );
	public static myModuleSelectEventHandler myModuleSelect;
	
	//初始化
	public static void Init()
	{
		isTest = false;
		
		ShowDetail = true;
		
		SX = 20;
		SY = -600;
		
		MinX = 0;
		MaxX = 0;
		MinY = 0;
		MaxY = 0;
		isMouseOn = false;
		
		myModuleList = null;
		
		ModuleMesBackBrush = new SolidBrush( GUIset.GetBlueColor( 110 ) );
		
		ModuleLibDecoder.deleAddNode += AddNode;
	}
	
	//添加条目
	static void AddNode( string Name, string Path, int st, int end )
	{
		if( Name == "引擎类" ) {
			myModuleList = LoadItemModule( Path );
		}
	}
	
	//-------------------------------------
	//鼠标按下事件
	public static bool UserMouseDown( int mX, int mY )
	{
		/*
		//分发组件库树列表鼠标事件
		ModuleTree.UserMouseDown( mX - SX, mY - SY );
		
		//如果拖动单个组件,则删除其他组件
		if( ModuleDetail.myModuleList.MouseOnObject != null ) {
			ModuleDetail.myModuleList.Clear();
			ModuleDetail.myModuleList.Add( ModuleDetail.myModuleList.MouseOnObject );
		}
		 */
		return false;
	}
	
	//鼠标松开事件
	public static void UserMouseUp( int mX, int mY )
	{
		if( !isTest ) {
			return;
		}
		
		//分发组件库树列表鼠标事件
		//ModuleTree.UserMouseUp( mX - SX, mY - SY );
		
		if( ShowDetail && myModuleList.MouseOnObject != null ) {
			myModuleSelect( myModuleList.MouseOnObject, myModuleList.MouseOnBaseX, myModuleList.MouseOnBaseY );
			
			ShowDetail = false;
			myModuleList.MouseOnObject = null;
		}
	}
	
	//鼠标移动事件
	public static void UserMouseMove( int mX, int mY )
	{
		if( myModuleList == null ) {
			return;
		}
		//获取图形界面的边界
		//Shape.GetArea( null, myModuleList, ref MinX, ref MinY, ref MaxX, ref MaxY );
		float C_MinX = 0;
		float C_MaxX = 0;
		float C_MinY = 0;
		float C_MaxY = 0;
		Shape.GetArea( null, myModuleList, ref MinX, ref MinY, ref MaxX, ref MaxY, ref C_MinX, ref C_MinY, ref C_MaxX, ref C_MaxY );
		
		MinX -= Padding;
		MaxX += Padding;
		MinY -= Padding;
		MaxY += Padding;
		
		//组件列表鼠标事件
		if( mX >= SX + MinX && mX <= SX + MaxX &&
		    mY >= SY + MinY && mY <= SY + MaxY ) {
			myModuleList.UserMouseMove( mX - SX, mY - SY );
		}
	}
	
	//-------------------------------------
	//装载指定目录下的模块
	static MyObjectList LoadItemModule( string Path )
	{
		if( Path == null ) {
			return null;
		}
		MyObjectList myModuleList = new MyObjectList( null );
		//myModuleList.isShowWarning = false;
		myModuleList.isModuleLib = true;
		
		if( Path == "var," ) {
			return null;
		}
		else {
			int X = Padding;
			string[] PathList = Path.Trim( ',' ).Split( ',' );
			
			HardModule[] temList = new HardModule[PathList.Length];
			
			for( int i = 0; i < PathList.Length; ++i ) {
				
				int Y = Padding;
				int MaxW = 0;
				
				int tempLength = 0;
				
				//根据文件名列表建立对应的模块列表
				for( ; i < PathList.Length; i++ ) {
					if( PathList[i] == "<nextcolumn>" ) {
						break;
					}
					HardModule m = GUIcoder.GetModuleFromMacroFile(  OS.ModuleLibPath + PathList[i] );
					temList[tempLength] = m;
					tempLength++;
					
					//计算最大宽度
					if( MaxW < m.Width ) {
						MaxW = m.Width;
					}
					//设置默认语言
					int LanguageIndex = 0;
					for( int li = 0; li < m.LanguageList.Length; ++li ) {
						if( m.LanguageList[ li ] == SystemData.ModuleLanguage ) {
							LanguageIndex = li;
							break;
						}
					}
					//设置名字
					string NewName = m.NameList[ 2 + LanguageIndex ];
					
					//m.SetUserValue( NewName, null, width / 2, height / 2, 0 );
					m.SetUserValue( NewName, null, m.LanguageList[ LanguageIndex ], 0, Y, 0, false );
					
					//CurrentModule = m;
					myModuleList.Add( m );
					Y += m.Height + 50;
				}
				//重新调整X坐标, 确保垂直对齐
				for( int n = 0; n < tempLength; n++ ) {
					temList[n].SX = X + MaxW/2 - temList[n].Width/2;
				}
				X += MaxW + 20;
			}
			//G.CGPanel.ShowImage();
			return myModuleList;
		}
	}
	
	//绘图
	public static void Draw( Graphics g )
	{
		if( !isTest ) {
			return;
		}
		
		if( ShowDetail ) {
			RectangleF r = new RectangleF( SX + MinX, SY + MinY, MaxX - MinX, MaxY - MinY );
			//g.FillRectangle( GUIset.BackBrush, r );
			GraphicsPath gp = Shape.CreateRoundedRectanglePath( r );
			//g.DrawPath( Pens.Black, gp );
			g.FillPath( GUIset.BackBrush, gp );
			
			myModuleList.DrawImage( g );
			
			if( myModuleList.MouseOnObject != null ) {
				DrawExtendMes( g, SX, SY );
			}
		}
	}
	
	//绘制扩展信息
	public static void DrawExtendMes( Graphics g, int StartX, int StartY )
	{
		MyObject cm = myModuleList.MouseOnObject;
		ModuleMessage MM = null;
		
		//绘制组件高亮状态
		int x = StartX + cm.SX - 4;
		int y = StartY + cm.SY - 4;
		int w = cm.Width - 1 + 8;
		int h = cm.Height - 1 + 8;
		g.DrawRectangle( GUIset.组件高亮边框_Pen2, x, y, w, h );
		g.DrawRectangle( GUIset.组件高亮边框_Pen1, x, y, w, h );
		
		if( cm is HardModule ) {
			HardModule m = (HardModule)cm;
			int LIndex = m.GetLIndex();
			MM = m.mm[ LIndex ];
		}
		if( cm is UIModule ) {
			UIModule m = (UIModule)cm;
			int LIndex = m.GetLIndex();
			MM = m.mm[ LIndex ];
		}
		if( MM == null ) {
			return;
		}
		//绘制背景半透明效果
		//g.FillRectangle( GUIset.HideBackBrush, 0, 0, Width, Height );
		
		//获取屏幕下方空白区域
		int H = StartY + cm.SY + cm.Height;
		
		//绘制边框,间距为 15
		//Rectangle r0 = new Rectangle( 15, H + 15, this.Width - 15 - 15, this.Height - H - 15 - 15 );
		//GraphicsPath gp = Shape.CreateRoundedRectanglePath( r0 );
		//g.FillPath( GUIset.HideBackBrush, gp );
		//g.DrawPath( Pens.LightGray, gp );
		
		//文字起点为下方 20, 宽度间距为 20
		int MesWidth = 700;
		int MesX = StartX + cm.MidX - MesWidth / 2;
		int CurrentHeight = H + 20;
		int MesPadding = 10;
		int ModuleNamePadding = MesPadding + 10;
		int ModuleMesPadding = MesPadding + 40;
		int ItemNamePadding = MesPadding + 10;
		int ItemMesPadding = MesPadding + 40;
		
		//绘制三角形
		Point P0 = new Point( StartX + cm.MidX, CurrentHeight - 15 );
		Point P1 = new Point( StartX + cm.MidX - 10, CurrentHeight + 5 );
		Point P2 = new Point( StartX + cm.MidX + 10, CurrentHeight + 5 );
		Point[] PA = new Point[]{P0, P1, P2};
		g.FillPolygon( ModuleMesBackBrush, PA );
		
		int AllHeight = DescriptionForm.SearchHeight( MM, MesWidth );
		Rectangle r = new Rectangle( MesX, CurrentHeight, MesWidth, AllHeight );
		GraphicsPath gp = Shape.CreateRoundedRectanglePath( r );
		g.FillPath( ModuleMesBackBrush, gp );
		
		CurrentHeight += 5;
		
		//绘制组件信息
		g.DrawString( MM.UserName + ":", MesPanel.extendHeadFont, Brushes.Orange, MesX + ModuleNamePadding, CurrentHeight );
		CurrentHeight += 20;
		SizeF sz0 = GUIset.mg.MeasureString( MM.ModuleMes, MesPanel.extendFont, MesWidth - ModuleMesPadding );
		RectangleF rf0 = new RectangleF( MesX + ModuleMesPadding, CurrentHeight, sz0.Width, sz0.Height );
		g.DrawString( MM.ModuleMes, MesPanel.extendFont, Brushes.LightGray, rf0 );
		CurrentHeight += (int)sz0.Height;
		
		//绘制组件成员信息
		for( int i = 0; i < MM.MemberMesList.Length; ++i ) {
			if( MM.MemberMesList[ i ].UserName.StartsWith( "OS_" ) ) {
				continue;
			}
			CurrentHeight += 5;
			g.DrawString( MM.MemberMesList[ i ].UserName + ":", MesPanel.extendHeadFont, Brushes.WhiteSmoke, MesX + ItemNamePadding, CurrentHeight );
			CurrentHeight += 20;
			SizeF sz = GUIset.mg.MeasureString( MM.MemberMesList[ i ].Mes, MesPanel.extendFont, MesWidth - ItemMesPadding );
			RectangleF rf = new RectangleF( MesX + ItemMesPadding, CurrentHeight, sz.Width, sz.Height );
			g.DrawString( MM.MemberMesList[ i ].Mes, MesPanel.extendFont, Brushes.LightGray, rf );
			CurrentHeight += (int)sz.Height;
		}
	}
}
}



