﻿
namespace n_GroupList
{
using System;
using System.Collections;
using System.Drawing;
using System.Drawing.Drawing2D;

using n_MyObject;
using n_MyFileObject;
using n_Shape;
using n_GUIset;
using n_GroupPanel;

//*****************************************************
//分组列表
public class GroupList
{
	Group[] mGroupList;
	int Length;
	
	const int MaxNumber = 50;
	
	//初始化
	public static void Init()
	{
		Group.Init();
	}
	
	//构造函数
	public GroupList()
	{
		mGroupList = new Group[MaxNumber];
		
		Length = 0;
	}
	
	//迭代器
	public IEnumerator GetEnumerator()
	{
		for (int index = 0; index < Length; index++) {
			if( mGroupList[index] == null ) {
				continue;
			}
			yield return mGroupList[index];
		}
	}
	
	//获取所有的分组属性
	public string GetConfig()
	{
		string r = null;
		foreach( Group gr in this ) {
			if( !gr.Expand ) {
				r += gr.GMes + ",";
			}
		}
		return r;
	}
	
	//显示
	public void Draw( Graphics g )
	{
		foreach( Group gr in this ) {
			gr.Draw( g );
		}
		/*
		n_Debug.Debug.Message = "";
		foreach( Group gr in this ) {
			n_Debug.Debug.Message += gr.Name + "  (" + gr.SX + "," + gr.SY + "," + gr.EX + "," + gr.EY + ")\n";
		}
		*/
	}
	
	//=========================================================
	
	//清除所有的组
	public void Reset()
	{
		foreach( Group gr in this ) {
			gr.Reset();
		}
	}
	
	//删除失效的组
	public void DeleteNull()
	{
		for( int i = 0; i < Length; ++i ) {
			if( mGroupList[i] == null ) {
				continue;
			}
			if( !mGroupList[i].Inited ) {
				mGroupList[i] = null;
			}
		}
	}
	
	//更新一个指定名字的组尺寸坐标
	public void Refresh( MyObject mo )
	{
		Group g = FindGroup( mo.GroupMes );
		
		if( g == null ) {
			n_Debug.Warning.BUG( "<GroupList.Refresh> 分组为空: " + mo.GroupMes );
			return;
		}
		g.Refresh( mo );
	}
	
	//查找一个分组
	public Group FindGroup( string Name )
	{
		foreach( Group gr in this ) {
			if( gr.GMes == Name ) {
				return gr;
			}
		}
		return null;
	}
	
	//查找一个分组 (通过名字)
	public Group FindGroupByName( string Name )
	{
		foreach( Group gr in this ) {
			if( gr.Name == Name ) {
				return gr;
			}
		}
		return null;
	}
	
	//添加一个新的分组
	public void AddGroup( string Name )
	{
		for( int i = 0; i < MaxNumber; ++i ) {
			if( mGroupList[i] == null ) {
				mGroupList[i] = new Group( Name );
				if( Length <= i ) {
					Length = i + 1;
				}
				return;
			}
		}
	}
	
	//=========================================================
	
	//鼠标移动事件
	public void MouseMove( int mX, int mY )
	{
		foreach( Group gr in this ) {
			gr.MouseMove( mX, mY );
		}
	}
	
	//鼠标左键按下事件
	public bool MouseDown( int mX, int mY )
	{
		bool on = false;
		
		foreach( Group gr in this ) {
			on |= gr.MouseDown( mX, mY );
		}
		return on;
	}
	
	//鼠标左键松开事件
	public void MouseUp( int mX, int mY )
	{
		foreach( Group gr in this ) {
			gr.MouseUp( mX, mY );
		}
	}
}
//分组
public class Group
{
	public string GMes;
	
	public string Name;
	public string Version;
	public const char SPLIT = '+';
	
	public float SX;
	public float SY;
	
	public float Width;
	public float Height;
	
	public float EX;
	public float EY;
	
	public bool Inited;
	
	int ClickTime;
	const int MaxClickTimes = 4;
	
	const float pad = 30;
	
	public const float HeadSize = 40;
	
	bool OnMove;
	bool IsMove;
	
	bool OnExp;
	bool IsExp;
	
	public bool Expand;
	
	bool AllSelect;
	
	float lastX, lastY;
	
	GroupPanel vPanel;
	
	public n_GUIcoder.ModuleMessage MM;
	
	static Pen Selectpen;
	static Image ExpImage;
	static Image ClsImage;
	static Brush LabelColorBR;
	static Brush AreaColorBR;
	static Pen LabelColorPen;
	
	//初始化
	public static void Init()
	{
		Selectpen = new Pen( Color.Orange, 3 );
		
		ExpImage = new Bitmap( n_OS.OS.SystemRoot + @"Resource\gui\GFormControls\Exp.png" );
		ClsImage = new Bitmap( n_OS.OS.SystemRoot + @"Resource\gui\GFormControls\Cls.png" );
		
		LabelColorBR = new SolidBrush( Color.FromArgb( 220, 240, 240 ) );
		LabelColorPen = new Pen( Color.FromArgb( 200, 220, 230 ) );
		
		//AreaColorBR = new SolidBrush( Color.FromArgb( 250, 250, 250 ) );
		AreaColorBR = GUIset.BackBrush;
	}
	
	//构造函数
	public Group( string n )
	{
		GMes = n;
		
		string[] cc = GMes.Split( SPLIT );
		Name = cc[0];
		if( cc.Length >= 2 ) {
			Version = cc[1];
		}
		else {
			Version = "";
		}
		Inited = false;
		Expand = true;
		
		OnMove = false;
		IsMove = false;
		OnExp = false;
		IsExp = false;
		
		ClickTime = 0;
		
		vPanel = new GroupPanel( this );
		vPanel.Visible = false;
	}
	
	//获取分组的前缀替换名
	public static string GetFullName( string Head, string old )
	{
		if( Head == null ) {
			return old;
		}
		int sp = Head.IndexOf( SPLIT );
		if( sp != -1 ) {
			Head = Head.Remove( sp );
		}
		return old.Replace( "$", Head + "_" );
	}
	
	//获取X中心坐标
	public float GetMidX()
	{
		return SX + Width / 2;
	}
	
	//获取Y中心坐标
	public float GetMidY()
	{
		return SY + HeadSize / 2;
	}
	
	//设置收起和展开标志
	public void RefreshExpandStatus( n_ImagePanel.ImagePanel GPanel )
	{
		foreach( MyObject mo in GPanel.myModuleList ) {
			
			if( mo.GroupMes == this.GMes ) {
				mo.GroupVisible = Expand;
			}
		}
	}
	
	//绘制
	public void Draw( Graphics g )
	{
		Width = EX - SX;
		Height = EY - SY;
		if( !Expand ) {
			//Width /= 4;
			Width = HeadSize * 5;
			Height = HeadSize;
		}
		
		//更新面板位置
		vPanel.X = SX + Width + 10;
		vPanel.Y = SY;
		
		//绘制框架体
		if( Expand ) {
			g.FillRectangle( AreaColorBR, SX, SY, Width, Height );
			g.DrawRectangle( LabelColorPen, SX, SY, Width, Height );
		}
		
		//else {
			if( AllSelect ) {
				g.DrawRectangle( Selectpen, SX - 4, SY - 4, Width + 8, Height + 8 );
			}
		//}
		
		//绘制标题栏
		g.FillRectangle( LabelColorBR, SX, SY, Width, HeadSize );
		if( IsMove ) {
			g.DrawRectangle( Pens.Black, SX, SY, Width, HeadSize );
		}
		else if( OnMove ) {
			g.DrawRectangle( Pens.Black, SX, SY, Width, HeadSize );
		}
		else {
			g.DrawRectangle( LabelColorPen, SX, SY, Width, HeadSize );
		}
		//绘制收起/展开按钮
		if( IsExp ) {
			g.FillRectangle( Brushes.OrangeRed, SX, SY, HeadSize, HeadSize );
		}
		else if( OnExp ) {
			g.FillRectangle( Brushes.Orange, SX, SY, HeadSize, HeadSize );
		}
		else {
			if( !Expand ) {
				//g.FillRectangle( Brushes.ForestGreen, SX, SY, HeadSize, HeadSize );
			}
			else {
				//g.FillRectangle( Brushes.Gray, SX, SY, HeadSize, HeadSize );
			}
		}
		
		//绘制收起展开图标
		if( Expand ) {
			g.DrawImage( ClsImage, SX, SY, HeadSize, HeadSize );
		}
		else {
			g.DrawImage( ExpImage, SX, SY, HeadSize, HeadSize );
		}
		
		//绘制字符串信息
		g.DrawString( Name, GUIset.ExpFont, Brushes.Black, SX + HeadSize + 5, SY + 7 );
		if( Version != "" ) {
			g.DrawString( "版本: " + Version, GUIset.ExpFont, Brushes.SlateGray, SX + HeadSize + 5, SY - 25 );
		}
		
		//绘制属性面板
		vPanel.Draw( g );
	}
	
	//鼠标按下时
	public bool MouseDown( float mx, float my )
	{
		bool d = vPanel.MouseDown( mx, my );
		
		ClickTime = 0;
		
		if( OnMove ) {
			IsMove = true;
			lastX = mx;
			lastY = my;
		}
		else {
			IsMove = false;
		}
		
		if( OnExp ) {
			IsExp = true;
		}
		else {
			IsExp = false;
		}
		
		if( !d && !OnMove ) {
			vPanel.Visible =false;
		}
		
		return d || IsMove || IsExp;
	}
	
	//鼠标松开时
	public void MouseUp( float mx, float my )
	{
		vPanel.MouseUp( mx, my );
		
		if( IsMove ) {
			IsMove = false;
			
			if( !IsExp ) {
				//触发控件的点击事件
				if( ClickTime < MaxClickTimes ) {
					
					vPanel.RefreshEventList();
					
					vPanel.Visible = true;
				}
				else {
					vPanel.Visible = false;
				}
			}
			
			//判断是否移动到垃圾桶中
			float ScreenX = (G.CGPanel.StartX + mx) * n_ImagePanel.ImagePanel.AScale / n_ImagePanel.ImagePanel.AScaleMid;
			float ScreenY = (G.CGPanel.StartY + my) * n_ImagePanel.ImagePanel.AScale / n_ImagePanel.ImagePanel.AScaleMid;
			
			if( G.CGPanel.myRecycler.isInside( ScreenX, ScreenY ) ) {
				
				foreach( MyObject mo in G.CGPanel.myModuleList ) {
					if( mo.GroupMes == this.GMes ) {
					
						if( mo is n_MyFileObject.MyFileObject ) {
							mo.Remove();
							G.CGPanel.RefreshResourceError();
						}
						else {
							mo.Remove();
						}
					}
				}
				
				return;
			}
		}
		if( IsExp && OnExp ) {
			
			//收起和展开
			Expand = !Expand;
			
			RefreshExpandStatus( G.CGPanel );
		}
		if( IsExp ) {
			IsExp = false;
		}
	}
	
	//鼠标移动时
	public void MouseMove( float mx, float my )
	{
		bool m = vPanel.MouseMove( mx, my );
		
		if( mx >= SX && mx < EX && my >= SY && my < SY + HeadSize ) {
			if( !Expand && mx > SX + Width ) {
				OnMove = false;
			}
			else {
				OnMove = true;
			}
		}
		else {
			OnMove = false;
		}
		if( mx >= SX && mx < SX + HeadSize && my >= SY && my < SY + HeadSize ) {
			OnExp = true;
		}
		else {
			OnExp = false;
		}
		
		
		//判断是否需要移动整体
		if( IsMove ) {
			
			ClickTime++;
			
			SX += (mx - lastX);
			EX += (mx - lastX);
			SY += (my - lastY);
			EY += (my - lastY);
			
			foreach( MyObject mo in G.CGPanel.myModuleList ) {
				if( mo.GroupMes == this.GMes ) {
					mo.SX += (int)(mx - lastX);
					mo.SY += (int)(my - lastY);
					mo.RefreshPanelLocation();
				}
			}
			
			lastX = mx;
			lastY = my;
		}
	}
	
	//复位
	public void Reset()
	{
		Inited = false;
		AllSelect = true;
	}
	
	//刷新组尺寸
	public void Refresh( MyObject o )
	{
		//判断是否需要提取信息
		if( MM == null ) {
			if( o is n_GNote.GNote ) {
				string mes = ((n_GNote.GNote)o).Value;
				
				MM = new n_GUIcoder.ModuleMessage();
				
				n_GUIcoder.GUIcoder.ParseExtendMes( mes, MM );
			}
		}
		
		if( !o.isSelect ) {
			AllSelect = false;
		}
		
		if( !o.Visible ) {
			return;
		}
		if( !Inited ) {
			SX = o.SX - pad;
			SY = o.SY - pad - HeadSize;
			EX = o.SX + o.Width + pad;
			EY = o.SY + o.Height + pad;
			Inited = true;
		}
		else {
			if( SX > o.SX - pad ) {
				SX = o.SX - pad;
			}
			if( SY > o.SY - pad - HeadSize ) {
				SY = o.SY - pad - HeadSize;
			}
			if( EX < o.SX + o.Width + pad ) {
				EX = o.SX + o.Width + pad;
			}
			if( EY < o.SY + o.Height + pad ) {
				EY = o.SY + o.Height + pad;
			}
		}
	}
}
}



