﻿
namespace n_GVar
{
using System;
using System.Drawing;
using System.Drawing.Drawing2D;
using n_HardModule;
using n_MyObject;
using n_Config;
using n_Shape;
using n_VarPanel;
using n_GUIset;
using n_GUIcoder;

//*****************************************************
//控件类
public class GVar: MyObject
{
	public string VarType;
	public string StoreType;
	
	public string Value;
	public bool isConst;
	
	Bitmap SL_Image;
	Bitmap v_myImage;
	public Bitmap myImage {
		get { return v_myImage; }
		set {
			v_myImage = value;
			
			int w = v_myImage.Width;
			int h = v_myImage.Height;
			if( h > MaxHeight ) {
				w = v_myImage.Width * MaxHeight / v_myImage.Height;
				h = MaxHeight;
			}
			SL_Image = new Bitmap( v_myImage, w, h );
		}
	}
	
	public int BitmapWidth;
	public int BitmapHeight;
	
	static Pen 组件高亮边框_Pen1;
	static Pen 组件高亮边框_Pen2;
	static Pen 组件选中边框_Pen1;
	static Pen 组件选中边框_Pen2;
	
	static SolidBrush SelectModulebackBrush;
	static SolidBrush MouseOnModulebackBrush;
	static SolidBrush GreenBrush;
	
	static SolidBrush Int32Brush;
	
	//仿真时指向的地址
	int Addr;
	
	int Start_mX;
	int Start_mY;
	
	int ImgScale;
	const int MaxHeight = 130;
	
	//初始化
	public static void Init()
	{
		MouseOnModulebackBrush = new SolidBrush( Color.FromArgb( 90, Color.White ) );
		SelectModulebackBrush = new SolidBrush( Color.FromArgb( 80, Color.Red ) );
		GreenBrush = new SolidBrush( Color.FromArgb( 150, 220, 180 ) );
		Int32Brush = new SolidBrush( Color.FromArgb( 160, 200, 255 ) );
		
		组件高亮边框_Pen1 = new Pen( Color.White, 1 );
		组件高亮边框_Pen2 = new Pen( Color.Green, 3 );
		组件选中边框_Pen1 = new Pen( Color.White, 2 );
		组件选中边框_Pen2 = new Pen( Color.Red, 5 );
	}
	
	
	//构造函数,根据文件名加载组件,文件名为全路径
	public GVar(): base()
	{
		Value = "";
		VarType = "";
		StoreType = "";
		this.CanFloat = false;
		
		this.Width = GUIset.GetPixTTT(130);
		this.Height = GUIset.GetPixTTT(35);
	}
	
	//移除
	public override void Remove()
	{
		//遍历组件列表,查找目标组件
		for( int i = 0; i < myObjectList.MLength; ++i ) {
			if( myObjectList.ModuleList[ i ] == null ) {
				continue;
			}
			if( myObjectList.ModuleList[ i ] == this ) {
				myObjectList.ModuleList[ i ] = null;
				return;
			}
		}
	}
	
	//自动调整尺寸(仅限于图片类型)
	public void AutoResize()
	{
		if( Height > MaxHeight ) {
			ImgScale = MaxHeight * 100 / Height;
			Width = Width * MaxHeight / Height;
			Height = MaxHeight;
		}
		else {
			ImgScale = 100;
		}
	}
	
	//仿真启动时初始化
	public void ResetSim()
	{
		n_MyObject.MyObject.GroupReplace = true;
		int i = n_VarList.VarList.GetStaticIndex( "#." + Name );
		n_MyObject.MyObject.GroupReplace = false;
		string addr = n_VarList.VarList.GetAddr( i );
		Addr = int.Parse( addr );
		
	}
	
	//组件扩展函数
	public override bool MouseIsInEX( int mX, int mY )
	{
		return false;
	}
	
	//组件绘制工作1
	//绘制组件的外形等,基础绘制工作
	public override void Draw1( Graphics g )
	{
		if( isNewTick != 0 ) {
			return;
		}
		//显示文本信息
		SizeF size = g.MeasureString( Name, GUIset.ExpFont );
		
		//绘制组件名称
		if( myImage == null ) {
			
			Pen EagePen = Pens.Black;
			int HeadLen = GUIset.GetPix(45);
			
			string TypeMes = null;
			if( VarType == GType.g_int32 ) {
				
				Rectangle sr = new Rectangle( SX - 4, SY - 4, Width + 8, Height + 8 );
				Rectangle r = new Rectangle( SX + HeadLen, SY, Width - HeadLen, Height );
				
				GraphicsPath sgp = Shape.CreateRoundedRectanglePath( sr );
				GraphicsPath gp = Shape.CreateRoundedRectanglePath( r );
				
				if( !isMouseOnBase ) {
					EagePen = Pens.CornflowerBlue;
				}
				g.FillPath( Int32Brush, sgp );
				g.DrawPath( EagePen, sgp );
				g.FillPath( Brushes.WhiteSmoke, gp );
				
				//g.DrawPath( Pens.LightSteelBlue, gp );
				TypeMes = n_Language.Language.IntType;
				
				g.DrawString( TypeMes, GUIset.ExpFont, Brushes.Black, sr, GUIset.LeftFormat );
				g.DrawString( Name, GUIset.ExpFont, Brushes.Black, r, GUIset.MidFormat );
			}
			else if( VarType == GType.g_fix ) {
				
				Rectangle sr = new Rectangle( SX - 4, SY - 4, Width + 8, Height + 8 );
				Rectangle r = new Rectangle( SX + HeadLen, SY, Width - HeadLen, Height );
				
				GraphicsPath sgp = Shape.CreateRoundedRectanglePath( sr );
				GraphicsPath gp = Shape.CreateRoundedRectanglePath( r );
				
				if( !isMouseOnBase ) {
					EagePen = Pens.MediumSeaGreen;
				}
				g.FillPath( GreenBrush, sgp );
				g.DrawPath( EagePen, sgp );
				g.FillPath( Brushes.WhiteSmoke, gp );
				
				//g.DrawPath( Pens.LightSteelBlue, gp );
				TypeMes = n_Language.Language.FixType;
				
				g.DrawString( TypeMes, GUIset.ExpFont, Brushes.Black, sr, GUIset.LeftFormat );
				g.DrawString( Name, GUIset.ExpFont, Brushes.Black, r, GUIset.MidFormat );
			}
			else if( VarType == GType.g_bool ) {
				
				Rectangle sr = new Rectangle( SX - 4, SY - 4, Width + 8, Height + 8 );
				Rectangle r = new Rectangle( SX + HeadLen, SY, Width - HeadLen, Height );
				
				//保持和其他元素一致
				//GraphicsPath Rsgp = Shape.CreateRoundedRectanglePath1( sr );
				//GraphicsPath Rgp = Shape.CreateRoundedRectanglePath1( r );
				GraphicsPath Rsgp = Shape.CreateRoundedRectanglePath( sr );
				GraphicsPath Rgp = Shape.CreateRoundedRectanglePath( r );
				
				if( !isMouseOnBase ) {
					EagePen = Pens.Goldenrod;
				}
				g.FillPath( Brushes.PaleGoldenrod, Rsgp );
				g.DrawPath( EagePen, Rsgp );
				g.FillPath( Brushes.WhiteSmoke, Rgp );
				
				//g.DrawPath( Pens.SpringGreen, gp );
				TypeMes = n_Language.Language.BoolType;
				
				g.DrawString( TypeMes, GUIset.ExpFont, Brushes.Black, sr, GUIset.LeftFormat );
				g.DrawString( Name, GUIset.ExpFont, Brushes.Black, r, GUIset.MidFormat );
			}
			else if(VarType == GType.g_music ) {
				
				if( this.Height != this.Width ) {
					this.Width = GUIset.GetPixTTT(80);
					this.Height = this.Width;
				}
				
				Rectangle sr = new Rectangle( SX - 4, SY - 4, Width + 8, Height + 8 );
				Rectangle rr = new Rectangle( SX + 22, SY + 11, Width - 22, Width - 22 );
				
				if( !isMouseOnBase ) {
					EagePen = Pens.Orchid;
				}
				g.FillEllipse( Brushes.Plum, sr );
				//g.DrawEllipse( Pens.MediumOrchid, sr );
				g.DrawEllipse( EagePen, sr );
				g.FillEllipse( Brushes.WhiteSmoke, rr );
				
				TypeMes = "♬♩";
				
				g.DrawString( TypeMes, GUIset.ExpFont, Brushes.Black, sr, GUIset.LeftFormat );
				g.DrawString( Name, GUIset.ExpFont, Brushes.Black, rr, GUIset.MidFormat );
				
			}
			else {
				//...
			}
			
			DrawDebug( g, SX, SY );
			
			if( G.SimulateMode ) {
				if( VarType == GType.g_int32 && Addr >= 0 ) {
					long data = n_UserModule.UserModule.BASE[ Addr ];
					data += n_UserModule.UserModule.BASE[ Addr + 1 ] * 256u;
					data += n_UserModule.UserModule.BASE[ Addr + 2 ] * 256u * 256u;
					data += n_UserModule.UserModule.BASE[ Addr + 3 ] * 256u * 256u * 256u;
					if( ( data & 0x80000000 ) != 0 ) {
						data = -( ( data ^ 0xffffffff ) + 1 );
					}
					
					g.DrawString( data.ToString(), GUIset.SimFont, Brushes.Black, SX, SY + GUIset.GetPix(40) );
					if( data >= ' ' && data < 128 ) {
						g.DrawString( "( " + ((char)data) + " )", GUIset.SimFont, Brushes.Green, SX + GUIset.GetPix(50), SY + GUIset.GetPix(40) );
					}
				}
				if( VarType == GType.g_fix && Addr >= 0 ) {
					long data = n_UserModule.UserModule.BASE[ Addr ];
					data += n_UserModule.UserModule.BASE[ Addr + 1 ] * 256u;
					data += n_UserModule.UserModule.BASE[ Addr + 2 ] * 256u * 256u;
					data += n_UserModule.UserModule.BASE[ Addr + 3 ] * 256u * 256u * 256u;
					if( ( data & 0x80000000 ) != 0 ) {
						data = -( ( data ^ 0xffffffff ) + 1 );
					}
					
					//添加波形数据
					AddDebugData( (int)data );
					DebugOpen = true;
					
					
					g.DrawString( (data / n_ConstString.ConstString.FixScale).ToString( "0.000" ), GUIset.SimFont, Brushes.Black, SX, SY + GUIset.GetPix(40) );
				}
				else if( VarType == GType.g_bool ) {
					if( n_UserModule.UserModule.BASE[ Addr ] == 0 ) {
						g.DrawString( n_Language.Language.SIM_False, GUIset.SimFont, Brushes.Black, SX, SY + GUIset.GetPix(40) );
					}
					else {
						g.DrawString( n_Language.Language.SIM_True, GUIset.SimFont, Brushes.Black, SX, SY + GUIset.GetPix(40) );
					}
				}
				else {
					
				}
			}
		}
		else {
			//显示缩略图, 加快速度
			//g.DrawImage( myImage, SX, SY, Width, Height );
			g.DrawImage( SL_Image, SX, SY );
			
			if( VarType == GType.g_cbitmap ) {
				g.DrawRectangle( Pens.ForestGreen, SX, SY, SL_Image.Width, SL_Image.Height );
			}
			
			if( isMouseOnBase ) {
				g.DrawRectangle( Pens.Black, SX, SY, SL_Image.Width, SL_Image.Height );
			}
			
			if( n_MainSystemData.SystemData.isBlack ) {
				g.DrawString( Name, GUIset.ExpFont, Brushes.WhiteSmoke, SX, SY - 20 );
			}
			else {
				g.DrawString( Name, GUIset.ExpFont, Brushes.Black, SX, SY - GUIset.GetPix(20) );
				
				if( ImgScale != 100 ) {
					g.DrawString( "W:" + BitmapWidth + ", " + "H:" + BitmapHeight + " (" + ImgScale + "%)", GUIset.ExpFont, Brushes.SlateGray, SX, SY + Height );
				}
				else {
					g.DrawString( "W:" + BitmapWidth + ", " + "H:" + BitmapHeight, GUIset.ExpFont, Brushes.SlateGray, SX, SY + Height );
				}
			}
		}
		
		//绘制是否选中
		if( isSelect ) {
			g.DrawRectangle( 组件高亮边框_Pen2, SX, SY, Width, Height );
		}
		
		//显示存储位置
		if( StoreType != "" ) {
			g.DrawString( "变量数据存放在", GUIset.ExpFont, Brushes.SlateGray, SX, SY + Height + 5 );
			g.DrawString( StoreType, GUIset.ExpFont, Brushes.DimGray, SX, SY + Height + 25 );
		}
		//获取音乐名称
		int fnI = Value.IndexOf( '\n' );
		if( VarType == GType.g_music && fnI != -1 ) {
			string MusicTitle = n_VersionConvert.VersionConvert.GetTitle( Value.Remove( fnI ) );
			
			if( n_MainSystemData.SystemData.isBlack ) {
				g.DrawString( "音乐名称: " + MusicTitle, GUIset.ExpFont, Brushes.WhiteSmoke, SX, SY + Height + 5 );
			}
			else {
				g.DrawString( "音乐名称: " + MusicTitle, GUIset.ExpFont, Brushes.Purple, SX, SY + Height + 5 );
			}
		}
		int x = SX - 4;
		int y = SY - 4;
		int w = Width - 1 + 8;
		int h = Height - 1 + 8;
	}
	
	//组件绘制工作2
	//绘制组件的端口链接导线,需要在所有组件基础绘制完成之后进行
	//绘制端口的方框背景
	public override void Draw2( Graphics g )
	{
		if( isNewTick != 0 ) {
			return;
		}
	}
	
	//组件绘制工作3
	//绘制组件的端口和名称,需要在所有链接导线绘制完成之后进行
	public override void Draw3( Graphics g )
	{
		if( isNewTick != 0 ) {
			return;
		}
	}
	
	public override void DrawSim( Graphics g )
	{
		
	}
	
	//组件绘制工作4
	//绘制组件高亮选中
	public override void DrawHighLight( Graphics g )
	{
		if( isNewTick != 0 ) {
			return;
		}
		if( isMousePress ) {
			return;
		}
		/*
		int x = SX - 4;
		int y = SY - 4;
		int w = Width - 1 + 8;
		int h = Height - 1 + 8;
		g.DrawRectangle( 组件高亮边框_Pen2, x, y, w, h );
		g.DrawRectangle( 组件高亮边框_Pen1, x, y, w, h );
		*/
		
		this.VPanel.Draw( g );
	}
	
	//鼠标按下事件, 在组件中返回 true,不在组件中返回 false.
	public override bool MouseDown1( int mX, int mY )
	{
		if( !MouseIsInside( mX, mY ) ) {
			return false;
		}
		if( isMouseOnBase ) {
			isMousePress = true;
			Last_mX = mX;
			Last_mY = mY;
			
			Start_mX = this.SX;
			Start_mY = this.SY;
		}
		return true;
	}
	
	//鼠标松开事件
	public override void MouseUp1( int mX, int mY )
	{
		if( isNewTick == 0 ) {
			isMousePress = false;
			ignoreHit = false;
		}
	}
	
	//鼠标移动事件, 当组件被拖动时返回 true
	public override int MouseMove1( int mX, int mY )
	{
		if( isNewTick == MaxNewTick ) {
			isNewTick--;
			isMousePress = true;
			this.SX = mX - this.Width / 2;
			this.SY = mY - this.Height / 2;
			this.RefreshPanelLocation();
			Start_mX = this.SX;
			Start_mY = this.SY;
			Last_mX = mX;
			Last_mY = mY;
			return 2;
		}
		if( isNewTick > 0 ) {
			isNewTick--;
		}
		bool isMouseIn = MouseIsInside( mX, mY );
		if( !isMouseIn && !isMousePress ) {
			return 0;
		}
		if( !isMousePress ) {
			return 1;
		}
		//SX += mX - Last_mX;
		//SY += mY - Last_mY;
		
		SX = Start_mX + (mX - Last_mX);
		SY = Start_mY + (mY - Last_mY);
		
		SX = n_Common.Common.GetCrossValue( SX );
		SY = n_Common.Common.GetCrossValue( SY );
		
		this.RefreshPanelLocation();
		//Last_mX = mX;
		//Last_mY = mY;
		
		return 2;
	}
	
	//设置用户参数
	public void SetUserValue( string vName, string vGroupMes, string vType, string vStoreType, string vvalue, bool visConst, int vX, int vY )
	{
		Name = vName;
		GroupMes = vGroupMes;
		VarType = vType;
		StoreType = vStoreType;
		Value = vvalue;
		isConst = visConst;
		SX = vX;
		SY = vY;
		
		if( VarType == GType.g_bitmap ) {
			BitmapWidth = 8;
			BitmapHeight = 8;
			VPanel = new VarBitmapPanel( this );
			this.Width = myImage.Width;
			this.Height = myImage.Height;
			
			AutoResize();
		}
		else if( VarType == GType.g_font ) {
			BitmapWidth = 80;
			BitmapHeight = 50;
			VPanel = new VarBitmapPanel( this );
			this.Width = myImage.Width;
			this.Height = myImage.Height;
			
			AutoResize();
		}
		else if( VarType == GType.g_cbitmap ) {
			BitmapWidth = 32;
			BitmapHeight = 32;
			VPanel = new VarBitmapPanel( this );
			this.Width = myImage.Width;
			this.Height = myImage.Height;
			
			AutoResize();
		}
		else {
			VPanel = new VarPanel( this );
		}
		this.RefreshPanelLocation();
	}
	
	//设置用户参数
	public void SetUserValue( string vName, string vGroupMes, string vType, string vStoreType, string vvalue, bool visConst, int vX, int vY, int Width, int Height )
	{
		Name = vName;
		GroupMes = vGroupMes;
		VarType = vType;
		StoreType = vStoreType;
		Value = vvalue;
		isConst = visConst;
		SX = vX;
		SY = vY;
		BitmapWidth = Width;
		BitmapHeight = Height;
		if( VarType == GType.g_bitmap ) {
			
			
			//转换旧版本的点阵数据到新版本格式
			if( !Value.StartsWith( n_LatticeToCode.LatticeToCode.L_V1_RightDown ) && !Value.StartsWith( n_LatticeToCode.LatticeToCode.L_V1_DownRight ) && Value != "" ) {
				string[] Line = Value.Split( '\n' );
				string[] cut = Line[0].Split( ',' );
				
				int ww;
				int hh;
				if( cut[0] != n_LatticeToCode.LatticeToCode.L_RightDown ) {
					ww = int.Parse( cut[0] );
					hh = int.Parse( cut[1] );
				}
				else {
					ww = int.Parse( cut[1] );
					hh = int.Parse( cut[2] );
				}
				
				string s = n_LatticeToCode.LatticeToCode.L_V1_RightDown + ",0,0," + (ww%256) + "," + (ww/256) + "," + hh + ",0,0,0,0,\n";
				for( int i = 1; i < Line.Length; ++i ) {
					if( i == Line.Length - 1 ) {
						s += Line[i];
					}
					else {
						s += Line[i] + "\n";
					}
				}
				Value = s;
			}
			
			VPanel = new VarBitmapPanel( this );
			this.Width = myImage.Width;
			this.Height = myImage.Height;
			
			AutoResize();
		}
		else if( VarType == GType.g_font ) {
			VPanel = new VarBitmapPanel( this );
			this.Width = myImage.Width;
			this.Height = myImage.Height;
			
			AutoResize();
		}
		else if( VarType == GType.g_cbitmap ) {
			VPanel = new VarBitmapPanel( this );
			this.Width = myImage.Width;
			this.Height = myImage.Height;
			Bitmap t = new Bitmap( myImage.Width, myImage.Height );
			string[] cut = Value.Split( ' ' );
			int i = 0;
			for( int y = 0; y < t.Height; ++y ) {
				for( int x = 0; x < t.Width; ++x ) {
					string[] cc = cut[i].Split( ',' );
					int r = int.Parse( cc[0] );
					int g = int.Parse( cc[1] );
					int b = int.Parse( cc[2] );
					t.SetPixel( x, y, Color.FromArgb( r, g, b ) );
					i++;
				}
			}
			myImage = t;
			AutoResize();
		}
		else {
			VPanel = new VarPanel( this );
		}
		this.RefreshPanelLocation();
	}
}
}



