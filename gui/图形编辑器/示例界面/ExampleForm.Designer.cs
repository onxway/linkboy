﻿/*
 * 由 SharpDevelop 创建。
 * 用户: bbb_2
 * 日期: 2009-11-17
 * 时间: 11:15
 * 
 * 如果你想要更改该模板，那么请使用“工具 | 选项 | 编辑 | 编辑标准标题”。
 */
namespace n_ExampleForm
{
	partial class ExampleForm
	{
		/// <summary>
		/// Designer variable used to keep track of non-visual components.
		/// </summary>
		private System.ComponentModel.IContainer components = null;
		
		/// <summary>
		/// Disposes resources used by the form.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing) {
				if (components != null) {
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}
		
		/// <summary>
		/// This method is required for Windows Forms designer support.
		/// Do not change the method contents inside the source code editor. The Forms designer might
		/// not be able to load this method if it was changed manually.
		/// </summary>
		private void InitializeComponent()
		{
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ExampleForm));
			this.panel1 = new System.Windows.Forms.Panel();
			this.linkLabel1 = new System.Windows.Forms.LinkLabel();
			this.label1 = new System.Windows.Forms.Label();
			this.button1 = new System.Windows.Forms.Button();
			this.radioButtonClose = new System.Windows.Forms.RadioButton();
			this.radioButtonNew = new System.Windows.Forms.RadioButton();
			this.buttonOpenLibFolder = new System.Windows.Forms.Button();
			this.panel2 = new System.Windows.Forms.Panel();
			this.panel6 = new System.Windows.Forms.Panel();
			this.panel5 = new System.Windows.Forms.Panel();
			this.panel3 = new System.Windows.Forms.Panel();
			this.treeView1 = new System.Windows.Forms.TreeView();
			this.panel4 = new System.Windows.Forms.Panel();
			this.panel1.SuspendLayout();
			this.panel2.SuspendLayout();
			this.panel3.SuspendLayout();
			this.panel4.SuspendLayout();
			this.SuspendLayout();
			// 
			// panel1
			// 
			this.panel1.BackColor = System.Drawing.Color.White;
			this.panel1.Controls.Add(this.linkLabel1);
			this.panel1.Controls.Add(this.label1);
			this.panel1.Controls.Add(this.button1);
			resources.ApplyResources(this.panel1, "panel1");
			this.panel1.Name = "panel1";
			// 
			// linkLabel1
			// 
			resources.ApplyResources(this.linkLabel1, "linkLabel1");
			this.linkLabel1.Name = "linkLabel1";
			this.linkLabel1.TabStop = true;
			// 
			// label1
			// 
			resources.ApplyResources(this.label1, "label1");
			this.label1.Name = "label1";
			// 
			// button1
			// 
			resources.ApplyResources(this.button1, "button1");
			this.button1.Name = "button1";
			this.button1.UseVisualStyleBackColor = true;
			// 
			// radioButtonClose
			// 
			resources.ApplyResources(this.radioButtonClose, "radioButtonClose");
			this.radioButtonClose.ForeColor = System.Drawing.Color.SlateGray;
			this.radioButtonClose.Name = "radioButtonClose";
			this.radioButtonClose.UseVisualStyleBackColor = true;
			// 
			// radioButtonNew
			// 
			this.radioButtonNew.Checked = true;
			resources.ApplyResources(this.radioButtonNew, "radioButtonNew");
			this.radioButtonNew.ForeColor = System.Drawing.Color.SlateGray;
			this.radioButtonNew.Name = "radioButtonNew";
			this.radioButtonNew.TabStop = true;
			this.radioButtonNew.UseVisualStyleBackColor = true;
			this.radioButtonNew.CheckedChanged += new System.EventHandler(this.RadioButtonNewCheckedChanged);
			// 
			// buttonOpenLibFolder
			// 
			this.buttonOpenLibFolder.BackColor = System.Drawing.Color.WhiteSmoke;
			resources.ApplyResources(this.buttonOpenLibFolder, "buttonOpenLibFolder");
			this.buttonOpenLibFolder.ForeColor = System.Drawing.Color.Black;
			this.buttonOpenLibFolder.Name = "buttonOpenLibFolder";
			this.buttonOpenLibFolder.UseVisualStyleBackColor = false;
			this.buttonOpenLibFolder.Click += new System.EventHandler(this.ButtonOpenLibFolderClick);
			// 
			// panel2
			// 
			this.panel2.BackColor = System.Drawing.Color.White;
			this.panel2.Controls.Add(this.panel6);
			this.panel2.Controls.Add(this.panel5);
			this.panel2.Controls.Add(this.panel3);
			resources.ApplyResources(this.panel2, "panel2");
			this.panel2.Name = "panel2";
			// 
			// panel6
			// 
			resources.ApplyResources(this.panel6, "panel6");
			this.panel6.Name = "panel6";
			// 
			// panel5
			// 
			this.panel5.BackColor = System.Drawing.Color.LightGray;
			resources.ApplyResources(this.panel5, "panel5");
			this.panel5.Name = "panel5";
			// 
			// panel3
			// 
			this.panel3.BackColor = System.Drawing.Color.White;
			this.panel3.Controls.Add(this.treeView1);
			this.panel3.Controls.Add(this.panel4);
			resources.ApplyResources(this.panel3, "panel3");
			this.panel3.Name = "panel3";
			// 
			// treeView1
			// 
			this.treeView1.BorderStyle = System.Windows.Forms.BorderStyle.None;
			resources.ApplyResources(this.treeView1, "treeView1");
			this.treeView1.Name = "treeView1";
			this.treeView1.AfterSelect += new System.Windows.Forms.TreeViewEventHandler(this.TreeView1AfterSelect);
			// 
			// panel4
			// 
			this.panel4.Controls.Add(this.radioButtonClose);
			this.panel4.Controls.Add(this.buttonOpenLibFolder);
			this.panel4.Controls.Add(this.radioButtonNew);
			resources.ApplyResources(this.panel4, "panel4");
			this.panel4.Name = "panel4";
			// 
			// ExampleForm
			// 
			resources.ApplyResources(this, "$this");
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.BackColor = System.Drawing.Color.White;
			this.Controls.Add(this.panel2);
			this.Controls.Add(this.panel1);
			this.Name = "ExampleForm";
			this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.UnitLibFormFormClosing);
			this.SizeChanged += new System.EventHandler(this.DescriptionFormSizeChanged);
			this.panel1.ResumeLayout(false);
			this.panel2.ResumeLayout(false);
			this.panel3.ResumeLayout(false);
			this.panel4.ResumeLayout(false);
			this.ResumeLayout(false);
		}
		private System.Windows.Forms.Label label1;
		private System.Windows.Forms.LinkLabel linkLabel1;
		private System.Windows.Forms.Button button1;
		private System.Windows.Forms.Panel panel5;
		private System.Windows.Forms.Panel panel6;
		private System.Windows.Forms.TreeView treeView1;
		private System.Windows.Forms.Panel panel4;
		private System.Windows.Forms.Panel panel3;
		private System.Windows.Forms.RadioButton radioButtonNew;
		private System.Windows.Forms.RadioButton radioButtonClose;
		private System.Windows.Forms.Panel panel2;
		private System.Windows.Forms.Button buttonOpenLibFolder;
		private System.Windows.Forms.Panel panel1;
	}
}
