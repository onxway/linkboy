﻿
using System;
using System.Drawing;
using System.Windows.Forms;
using System.Diagnostics;

using n_GUIcoder;
using n_GUIset;

namespace n_ExMesPanel
{
public class ExMesPanel : Panel
{
	public static bool Use = true;
	
	public delegate void D_OpenFile( bool IgnoreSave, string FileName );
	public D_OpenFile OpenFile;
	
	public bool NewFormOpen;
	
	public int StartY;
	
	public static Font extendFont;
	public static Font extendHeadFont;
	
	bool LeftPress;
	//bool RightPress;
	
	int LastY;
	public static int GWidth;
	int MoveTick;
	bool MyClick;
	
	public string[] LibList;
	
	public ModItem[] ModList;
	public static ModItem MouseOnObj;
	
	public string PrePath;
	
	//构造函数
	public ExMesPanel() : base()
	{
		this.BorderStyle = BorderStyle.None;
		this.BackColor = Color.Black;
		
		SetStyle(ControlStyles.UserPaint |
		         ControlStyles.AllPaintingInWmPaint |
		         ControlStyles.OptimizedDoubleBuffer |
		         ControlStyles.ResizeRedraw |
		         ControlStyles.SupportsTransparentBackColor,true);
		
		this.Dock = DockStyle.Fill;
		this.ImeMode = ImeMode.NoControl;
		
		this.MouseMove += new MouseEventHandler( UserMouseMove );
		this.MouseUp += new MouseEventHandler( UserMouseUp );
		this.MouseDown += new MouseEventHandler( UserMouseDown );
		this.MouseWheel += new MouseEventHandler( MyMouseWheel );
		
		//extendFont = new Font( "微软雅黑", 11 );
		//extendHeadFont = new Font( "微软雅黑", 12, FontStyle.Bold );
		
		extendFont = GUIset.UIFont;
		extendHeadFont = GUIset.UIFont;
		
		MyClick = false;
		MoveTick = 0;
		
		NewFormOpen = true;
		
		PrePath = "example";
	}
	
	//重绘事件
	protected override void OnPaint(PaintEventArgs e)
	{
		//文字起点为下方 20, 宽度间距为 20
		Graphics g = e.Graphics;
		
		g.TranslateTransform( 0, StartY );
		
		//填充背景
		g.Clear( Color.White );
		
		if( ModList != null ) {
			for( int i = 0; i < ModList.Length; ++i ) {
				
				if( ModList[i].SY + Height < -StartY || ModList[i].SY > -StartY + Height ) {
					continue;
				}
				ModList[i].Draw( g );
			}
		}
	}
	
	//设置模块信息
	public string[] SetMes( string Path )
	{
		string newpath = VersionSwitch.Switch_GetOld( Path );
		
		//MessageBox.Show( "<" + Path + "-" + newpath + ">" );
		
		ModList = null;
		string r = "";
		for( int i = 0; i < LibList.Length; ++i ) {
			
			if( LibList[i] == Path || LibList[i] == newpath ) {
				
				for( i = i + 1; i < LibList.Length; ++i ) {
					if( !LibList[i].StartsWith( "\t" ) ) {
						break;
					}
					//这里还有一个路径前缀也要删去 Tab + /
					r += LibList[i].Remove( 0, 2 ) + "\n";
				}
				break;
			}
		}
		r = r.TrimEnd( '\n' );
		if( r == "" ) {
			return null;
		}
		string[] rr = r.Split( '\n' );
		
		if( Use ) {
			ModList = new ModItem[rr.Length];
			for( int i = 0; i < ModList.Length; ++i ) {
				ModList[i] = new ModItem( i + 1, rr[i], 0, 20 + i*50 );
			}
		}
		return rr;
	}
	
	void MyMouseWheel( object sender, MouseEventArgs e)
	{
		if( e.Delta > 0 ) {
			StartY += 50;
		}
		else {
			StartY -= 50;
		}
		this.Invalidate();
	}
	
	//鼠标按下事件
	void UserMouseDown( object sender, MouseEventArgs e )
	{
		if( e.Button == MouseButtons.Left ) {
			LastY = e.Y;
			LeftPress = true;
			
			MoveTick = 0;
			MyClick = true;
		}
		if( e.Button == MouseButtons.Right ) {
			//RightPress = true;
		}
		this.Invalidate();
		
		this.Focus();
	}
	
	//鼠标松开事件
	void UserMouseUp( object sender, MouseEventArgs e )
	{
		if( e.Button == MouseButtons.Left ) {
			LeftPress = false;
			
			//if( MyClick ) {
				if( ModList != null ) {
					for( int i = 0; i < ModList.Length; ++i ) {
						if( ModList[i].MouseOpenOn ) {
							string Path = n_OS.OS.SystemRoot + PrePath + "\\" + ModList[i].FilePath;
							
							if( !System.IO.File.Exists( Path ) ) {
								MessageBox.Show( "文件未找到, 请反馈给linkboy开发团队或者到官网下载最新版本" );
								return;
							}
							
							if( NewFormOpen ) {
								Process Proc = new Process();
								Proc.StartInfo.WindowStyle = ProcessWindowStyle.Normal;
								Proc.StartInfo.WorkingDirectory = AppDomain.CurrentDomain.BaseDirectory;
								Proc.StartInfo.FileName = @"linkboy.exe";
								Proc.StartInfo.Arguments = Path;
								Proc.Start();
							}
							else {
								if( OpenFile != null ) {
									OpenFile( false, Path );
								}
							}
							return;
						}
					}
				}
			//}
		}
		if( e.Button == MouseButtons.Right ) {
			//RightPress = false;
		}
		this.Invalidate();
	}
	
	//鼠标移动事件
	void UserMouseMove( object sender, MouseEventArgs e )
	{
		if( LeftPress ) {
			
			MoveTick++;
			if( MoveTick > 3 ) {
				MyClick = false;
			}
			
			StartY += e.Y - LastY;
			LastY = e.Y;
		}
		else {
			if( ModList != null ) {
				int mX = e.X;
				int mY = e.Y - StartY;
				MouseOnObj = null;
				for( int i = 0; i < ModList.Length; ++i ) {
					if( mY >= ModList[i].SY && mY <= ModList[i].SY + ModList[i].Height ) {
						ModList[i].MouseOn = true;
						MouseOnObj = ModList[i];
						if( mX <= ModItem.OpenWidth ) {
							ModList[i].MouseOpenOn = true;
						}
						else {
							ModList[i].MouseOpenOn = false;
						}
					}
					else {
						ModList[i].MouseOn = false;
						ModList[i].MouseOpenOn = false;
					}
				}
			}
		}
		this.Invalidate();
	}
}
public class ModItem
{
	public int Index;
	
	public int SX;
	public int SY;
	
	public int Width;
	public int Height;
	
	public bool MouseOn;
	public bool MouseOpenOn;
	
	public string FilePath;
	string Name;
	string Type;
	
	public const int OpenWidth = 50;
	
	//构造函数
	public ModItem( int i, string fp, int sx, int sy )
	{
		Index = i;
		
		FilePath = fp;
		Name = FilePath.Remove( 0, FilePath.LastIndexOf( '\\' ) + 1 );
		Name = Name.Remove( Name.Length - 4 );
		
		Type = "";
		int id = FilePath.LastIndexOf( '\\' );
		if( id != -1 ) {
			Type = FilePath.Remove( id );
		}
		
		SX = sx;
		SY = sy;
		
		Width = 40;
		Height = 40;
		
		MouseOn = false;
		MouseOpenOn = false;
	}
	
	//绘制
	public void Draw( Graphics g )
	{
		int MesWidth = ExMesPanel.GWidth - 5 - 5;
		
		if( MouseOn ) {
			g.FillRectangle( Brushes.WhiteSmoke, 0, SY - 5, MesWidth, Height + 10 );
		}
		//g.FillRectangle( Brushes.CornflowerBlue, SX, SY, Width, Height );
		//g.DrawRectangle( Pens.Black, SX, SY, Width, Height );
		g.DrawString( Index.ToString(), ExMesPanel.extendHeadFont, Brushes.Black, SX + 20, SY + 9 );
		
		g.DrawString( Name, ExMesPanel.extendHeadFont, Brushes.Blue, SX + OpenWidth + 10, SY );
		g.DrawString( Type, ExMesPanel.extendFont, Brushes.Gray, SX + OpenWidth + 10, SY + 20 );
		
		if( MouseOn ) {
			if( MouseOpenOn ) {
				g.FillRectangle( Brushes.CornflowerBlue, SX, SY, OpenWidth, Height );
			}
			else {
				g.FillRectangle( Brushes.Silver, SX, SY, OpenWidth, Height );
			}
			g.DrawString( "打开", ExMesPanel.extendHeadFont, Brushes.Black, 10, SY + 10 );
		}
	}
}
}


