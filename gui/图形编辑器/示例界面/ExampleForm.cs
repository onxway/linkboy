﻿
namespace n_ExampleForm
{
using System;
using System.Drawing;
using System.Windows.Forms;

using n_GUIset;
using n_MyFileObject;
using n_HardModule;
using n_UIModule;
using n_MyObject;
using n_GUIcoder;
using n_ExMesPanel;
using System.IO;
using n_OS;

/// <summary>
/// Description of WorkForm.
/// </summary>
public partial class ExampleForm : Form
{
	public ExMesPanel GPanel;
	
	const int GWidth = 900;
	const int MesWidth = GWidth - 20 - 20;
	
	SolidBrush backb;
	
	//主窗口
	public ExampleForm()
	{
		InitializeComponent();
		
		//添加图形选择界面
		GPanel = new ExMesPanel();
		//GPanel.Location = new Point( 0, 0 );
		GPanel.Visible = true;
		//this.panel6.Controls.Add( GPanel );
		
		backb = new SolidBrush( Color.WhiteSmoke );
		
		GPanel.LibList = n_OS.VIO.OpenTextFileGB2312( n_OS.OS.SystemRoot + "example\\example.txt" ).Split( '\n' );
		
		button1.LostFocus += new EventHandler( ButtonLost );
		
		//在指定目录及子目录下查找文件,在list中列出子目录及文件
		DirectoryInfo Dir = new DirectoryInfo( OS.SystemRoot + "example_new" );
		DirectoryInfo[] dirsub = Dir.GetDirectories("*.*", SearchOption.TopDirectoryOnly);
		Array.Sort(dirsub, delegate(DirectoryInfo x, DirectoryInfo y) { return x.Name.CompareTo(y.Name); });  
		
		//遍历当前文件夹中的所有文件
		foreach( DirectoryInfo d in dirsub ) {
			
			string uName = d.ToString();
			int idx = uName.IndexOf( '-' );
			uName = uName.Remove( 0, idx + 1 );
			
			TreeNode tn = treeView1.Nodes.Add( uName );
			
			
			//在指定目录及子目录下查找文件,在list中列出子目录及文件
			DirectoryInfo[] dirsub1 = d.GetDirectories("*.*", SearchOption.TopDirectoryOnly);
			Array.Sort(dirsub1, delegate(DirectoryInfo x, DirectoryInfo y) { return x.Name.CompareTo(y.Name); });  
			
			//遍历当前文件夹中的所有文件
			foreach( DirectoryInfo d1 in dirsub1 ) {
				
				string uName1 = d1.ToString();
				int idx1 = uName1.IndexOf( '-' );
				uName1 = uName1.Remove( 0, idx + 1 );
				
				TreeNode tn1 = tn.Nodes.Add( uName1 );
				tn1.Name = d1.FullName;
			}
		}
	}
	
	void ButtonLost(object sender, EventArgs e)
	{
		button1.Select();
		button1.Focus();
	}
	
	//设置打开方式检查位
	public void ShowOpenCheck()
	{
		radioButtonNew.Visible = true;
		radioButtonClose.Visible = true;
	}
	
	//重新设置示例列表
	public void ResetLib( string lib, string PrePath )
	{
		GPanel.LibList = lib.Split( '\n' );
		GPanel.PrePath = PrePath;
	}
	
	//运行
	public void Run( MyFileObject cm )
	{
		
		Run( cm.MacroFilePath.Remove( 0, n_OS.OS.ModuleLibPath.Length ) );
	}
	
	//运行
	public void Run( string flag )
	{
		this.Visible = true;
		
		treeView1.SelectedNode = null;
		this.panel6.Controls.Clear();
		int idx = 0;
		string[] list = GPanel.SetMes( flag );
		
		if( list == null ) {
			this.Text = "此模块暂无示例";
			return;
		}
		
		this.Text = "模块用法示例 (一共" + list.Length + "个示例)";
		
		if( !ExMesPanel.Use ) {
			//遍历当前文件夹中的所有文件
			foreach( string f in list ) {
				
				string LinkName = f.Remove( 0, f.LastIndexOf( '/' ) + 1 );
				string note = f;
				AddItem( idx, LinkName, f, note );
				idx++;
			}
		}
		else {
			this.panel6.Controls.Add( GPanel );
			
			GPanel.StartY = 0;
			
			//TextHeight = SearchHeight( MM, MesWidth );
			ExMesPanel.GWidth = this.panel6.Width;
			
			GPanel.Invalidate();
			GPanel.Focus();
		}
	}
	
	//窗体关闭事件
	void UnitLibFormFormClosing(object sender, FormClosingEventArgs e)
	{
		e.Cancel = true;
		this.Visible = false;
	}
	
	void DescriptionFormSizeChanged(object sender, EventArgs e)
	{
		if( GPanel != null ) {
			ExMesPanel.GWidth = this.panel6.Width;
			GPanel.Invalidate();
		}
	}
	
	void ButtonOpenLibFolderClick(object sender, EventArgs e)
	{
		string fileToSelect = n_OS.OS.SystemRoot + "example\\应用类示例";
        string args = string.Format("/Select, {0}", fileToSelect);
        System.Diagnostics.Process.Start( new System.Diagnostics.ProcessStartInfo("Explorer.exe", args ) );
	}
	
	void RadioButtonNewCheckedChanged(object sender, EventArgs e)
	{
		GPanel.NewFormOpen = radioButtonNew.Checked;
	}
	
	void TreeView1AfterSelect(object sender, TreeViewEventArgs e)
	{
		string path = treeView1.SelectedNode.Name;
		if( path == null || path == "" ) {
			return;
		}
		
		//在指定目录及子目录下查找文件,在list中列出子目录及文件
		DirectoryInfo Dir = new DirectoryInfo( path );
		FileInfo[] filesub = Dir.GetFiles("*.*", SearchOption.TopDirectoryOnly);
		Array.Sort(filesub, delegate(FileInfo x, FileInfo y) { return x.Name.CompareTo(y.Name); });
		
		this.panel6.Controls.Clear();
		int idx = 0;
		
		//遍历当前文件夹中的所有文件
		foreach( FileInfo f in filesub ) {
			
			string LinkName = f.ToString();
			LinkName = LinkName.Remove( LinkName.Length - 4 );
			string note = "暂无说明";
			int nid = LinkName.IndexOf( "+" );
			if( nid != -1 ) {
				note = LinkName.Remove( 0, nid + 1 );
				LinkName = LinkName.Remove( nid );
			}
			AddItem( idx, LinkName, path, note );
			idx++;
		}
	}
	
	void AddItem( int idx, string name, string path, string note )
	{
		Panel p = new Panel();
		p.Dock = DockStyle.Top;
		p.Height = 100;
		
		Label lidx = new Label();
		lidx.Font = button1.Font;
		lidx.ForeColor = Color.LightSlateGray;
		lidx.Text = idx.ToString();
		lidx.Location = new Point( 5, 5 );
		lidx.Height = 30;
		lidx.Width = 20;
		
		LinkLabel lk = new LinkLabel();
		lk.LinkBehavior = LinkBehavior.NeverUnderline;
		lk.LinkColor = Color.DodgerBlue;
		lk.ActiveLinkColor = Color.OrangeRed;
		lk.Font = button1.Font;
		lk.Text = name;
		lk.Name = path;
		lk.Location = new Point( 30, 5 );
		lk.Height = 30;
		lk.Width = 300;
		
		Label l = new Label();
		l.Font = treeView1.Font;
		l.ForeColor = Color.LightSlateGray;
		l.Text = note;
		l.Location = new Point( 10, 35 );
		l.Height = 20;
		l.Width = 400;
		
		p.Controls.Add( lidx );
		p.Controls.Add( lk );
		p.Controls.Add( l );
		
		this.panel6.Controls.Add( p );
		p.BringToFront();
	}
}
}



