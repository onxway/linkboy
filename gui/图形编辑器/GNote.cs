﻿
namespace n_GNote
{
using System;
using System.Drawing;
using System.Drawing.Drawing2D;
using n_HardModule;
using n_MyObject;
using n_Config;
using n_Shape;
using n_VarPanel;
using n_GUIset;
using n_NotePanel;

//*****************************************************
//控件类
public class GNote: MyObject
{
	public string Value;
	Font f;
	
	const int SizePadding = 9;
	int MouseOnSizePort;
	int MouseSelectSizePort;
	int BSX;
	int BSY;
	int BaseWidth;
	int BaseHeight;
	int BaseX;
	int BaseY;
	
	public bool isCode; //这里设置是否为代码或者注释
	public int CodeType; //这里为代码类型 0:python 1:CX 2:C--
	public const int T_Python = 0;
	public const int T_CX = 1;
	public const int T_CD = 2;
	
	public Color BackColor;
	public Color EdgeColor;
	public Color ForeColor;
	
	Brush BackBrush;
	Pen EdgePen;
	Brush TextBrush;
	
	const int NotePadding = 10;
	
	static Pen 组件高亮边框_Pen2;
	
	public Image BackImage;
	public string ImagePath;
	
	public Bitmap NdkImage;
	
	//初始化
	public static void Init()
	{
		组件高亮边框_Pen2 = new Pen( Color.ForestGreen, 3 );
	}
	
	//构造函数,根据文件名加载组件,文件名为全路径
	public GNote(): base()
	{
		this.CanFloat = true;
		
		//f = new Font( "Courier New", 12 );
		f = GUIset.ExpFont;
		
		MouseOnSizePort = -1;
		MouseSelectSizePort = -1;
		BSX = 0;
		BSY = 0;
		BaseWidth = 0;
		BaseHeight = 0;
		BaseX = 0;
		BaseY = 0;
		
		this.Width = 120;
		this.Height = 30;
		Value = "";
		
		isCode = false;
		CodeType = T_CX;
		
		this.ExXPadding = SizePadding + 1;
		this.ExYHPadding = SizePadding + 1;
		this.ExYLPadding = SizePadding + 1;
		
		BackImage = null;
		ImagePath = null;
		
		NdkImage = new Bitmap( Width, Height );
		
		BackColor = Color.WhiteSmoke;
		EdgeColor = Color.ForestGreen;
		ForeColor = Color.ForestGreen;
		RefreshPen();
	}
	
	//设置图片
	public void SetImage( string path )
	{
		if( path != null ) {
			ImagePath = path;
			try {
				BackImage = Image.FromFile( G.ccode.FilePath + ImagePath );
			}
			catch {
				n_Debug.Warning.WarningMessage = "未找到图片文件: " + ImagePath;
			}
		}
		else {
			ImagePath = null;
			BackImage = null;
		}
	}
	
	//刷新画笔
	public void RefreshPen()
	{
		BackBrush = new SolidBrush( BackColor );
		EdgePen = new Pen( EdgeColor );
		TextBrush = new SolidBrush( ForeColor );
	}
	
	//获取用户代码, 移除原生代码标记区间
	public string GetCxCode( ref string ucl )
	{
		if( Value == null ) {
			return null;
		}
		bool is_c = false;
		string[] line = Value.Split( '\n' );
		for( int i = 0; i < line.Length; ++i ) {
			string temp = line[i].TrimStart( " \t".ToCharArray() );
			bool st = temp.StartsWith( "<start>" );
			bool ed = temp.StartsWith( "<end>" );
			if( st ) {
				is_c = true;
			}
			if( is_c ) {
				
				if( !st && !ed ) {
					ucl += line[i] + "\n";
				}
				
				line[i] = "//" + line[i];
			}
			if( ed ) {
				is_c = false;
			}
		}
		return String.Join( "\n", line );
	}
	
	//移除
	public override void Remove()
	{
		//遍历组件列表,查找目标组件
		for( int i = 0; i < myObjectList.MLength; ++i ) {
			if( myObjectList.ModuleList[ i ] == null ) {
				continue;
			}
			if( myObjectList.ModuleList[ i ] == this ) {
				myObjectList.ModuleList[ i ] = null;
				return;
			}
		}
	}
	
	//组件扩展函数
	public override bool MouseIsInEX( int mX, int mY )
	{
		if( mX < this.SX + this.Width + SizePadding && mX > this.SX - SizePadding &&
		    mY < this.SY + this.Height + SizePadding && mY > this.SY - SizePadding ) {
			return true;
		}
		return false;
	}
	
	//组件绘制工作1
	//绘制组件的外形等,基础绘制工作
	public override void Draw1( Graphics g )
	{
		if( isNewTick != 0 ) {
			return;
		}
		//显示文本信息
		SizeF size = g.MeasureString( Name, f );
		
		//绘制组件名称
		Rectangle r = new Rectangle( SX, SY, Width, Height );
		//GraphicsPath gp = Shape.CreateRoundedRectanglePath( r );
		
		if( isCode ) {
			g.FillRectangle( Brushes.White, r );
			g.DrawRectangle( Pens.SlateGray, r );
			
			if( G.SimulateMode ) {
				Rectangle sr = new Rectangle( SX + NotePadding, SY + NotePadding, Width - NotePadding*2, Height - NotePadding*2 );
				g.DrawImage( NdkImage, r );
			}
			else {
				Rectangle sr = new Rectangle( SX + NotePadding, SY + NotePadding, Width - NotePadding*2, Height - NotePadding*2 );
				g.DrawString( Value, f, Brushes.DarkSlateGray, sr );
			}
			int sy = GUIset.GetPix(20);
			if( CodeType == T_Python ) {
				g.DrawString( Name + " [Python 编程语言]", f, Brushes.Black, SX, SY - sy );
			}
			if( CodeType == T_CX ) {
				g.DrawString( Name, f, Brushes.Black, SX, SY - sy );
			}
			if( CodeType == T_CD ) {
				g.DrawString( Name + " [协处理器程序]", f, Brushes.Black, SX, SY - sy );
			}
		}
		else {
			if( n_MainSystemData.SystemData.isBlack ) {
				//g.FillPath( Brushes.SlateGray, gp );
				//g.DrawPath( Pens.DarkSlateGray, gp );
				g.FillRectangle( Brushes.SlateGray, r );
				g.DrawRectangle( Pens.DarkSlateGray, r );
				
				Rectangle sr = new Rectangle( SX + NotePadding, SY + NotePadding, Width - NotePadding*2, Height - NotePadding*2 );
				g.DrawString( Value, f, Brushes.LightGray, sr );
			}
			else {
				//g.FillPath( Brushes.LightBlue, gp );
				//g.DrawPath( Pens.CadetBlue, gp );
				
				if( BackImage == null ) {
					g.FillRectangle( BackBrush, r );
				}
				else {
					g.DrawImage( BackImage, r );
				}
				g.DrawRectangle( EdgePen, r );
				
				if( Value.IndexOf( "<居中>" ) != -1 ) {
					n_SG.SG.MString.DrawAtCenter( Value.Remove( 0, 4 ), ForeColor, f.Size, SX + Width/2, SY + Height/2 );
				}
				else {
					Rectangle sr = new Rectangle( SX + NotePadding, SY + NotePadding, Width - NotePadding*2, Height - NotePadding*2 );
					g.DrawString( Value, f, TextBrush, sr );
				}
			}
		}
		
		
		//绘制是否选中
		if( isSelect ) {
			g.DrawRectangle( 组件高亮边框_Pen2, SX, SY, Width, Height );
		}
	}
	
	//组件绘制工作2
	//绘制组件的端口链接导线,需要在所有组件基础绘制完成之后进行
	//绘制端口的方框背景
	public override void Draw2( Graphics g )
	{
		if( isNewTick != 0 ) {
			return;
		}
	}
	
	//组件绘制工作3
	//绘制组件的端口和名称,需要在所有链接导线绘制完成之后进行
	public override void Draw3( Graphics g )
	{
		if( isNewTick != 0 ) {
			return;
		}
	}
	
	public void ResetSim()
	{
		if( NdkImage.Width != Width || NdkImage.Height != Height ) {
			NdkImage = new Bitmap( Width, Height );
		}
		n_CruxSim.CruxSim.SetGObj( NdkImage );
	}
	
	public override void DrawSim( Graphics g )
	{
		
	}
	
	//组件绘制工作4
	//绘制组件高亮选中
	public override void DrawHighLight( Graphics g )
	{
		if( isNewTick != 0 ) {
			return;
		}
		if( !isMousePress || (isMousePress && MouseSelectSizePort != -1 ) ) {
			for( int i = 0; i < 8; ++i ) {
				if( i != 3 && i != 7 ) {
					//continue;
				}
				Rectangle r = GetRectangleOfSizePort( i );
				if( MouseOnSizePort == i ) {
					g.FillRectangle( Brushes.Orange, r.X, r.Y, r.Width - 1, r.Height - 1 );
					g.DrawRectangle( Pens.OrangeRed, r.X, r.Y, r.Width - 1, r.Height - 1 );
				}
				else {
					g.FillRectangle( Brushes.LightGreen, r.X, r.Y, r.Width - 1, r.Height - 1 );
					g.DrawRectangle( Pens.Green, r.X, r.Y, r.Width - 1, r.Height - 1 );
				}
			}
		}
		if( isMousePress ) {
			return;
		}
		this.VPanel.Draw( g );
	}
	
	//鼠标按下事件, 在组件中返回 true,不在组件中返回 false.
	public override bool MouseDown1( int mX, int mY )
	{
		if( !MouseIsInside( mX, mY ) ) {
			return false;
		}
		isMousePress = true;
		
		if( this.myObjectList.MouseOnObject == this ) {
			
			//注意,还有鼠标移动事件中也有在这段代码,注意同步
			MouseSelectSizePort = MouseOnSizePort;
			Last_mX = mX;
			Last_mY = mY;
			BaseX = SX;
			BaseY = SY;
			
			BSX = SX;
			BSY = SY;
			BaseWidth = Width;
			BaseHeight = Height;
		}
		return true;
	}
	
	//鼠标松开事件
	public override void MouseUp1( int mX, int mY )
	{
		if( isNewTick == 0 ) {
			isMousePress = false;
			ignoreHit = false;
		}
		
		//进行网格对齐
		SX = n_Common.Common.GetCrossValue( SX );
		SY = n_Common.Common.GetCrossValue( SY );
		Width = n_Common.Common.GetCrossValue( Width );
		Height = n_Common.Common.GetCrossValue( Height );
		if( Width < n_Common.Common.BSIZE ) {
			Width = n_Common.Common.BSIZE;
		}
		if( Height < n_Common.Common.BSIZE ) {
			Height = n_Common.Common.BSIZE;
		}
	}
	
	//鼠标移动事件,当组件被拖动时返回 true
	public override int MouseMove1( int mX, int mY )
	{
		if( isNewTick == MaxNewTick ) {
			isNewTick--;
			isMousePress = true;
			this.SX = mX - this.Width / 2;
			this.SY = mY - this.Height / 2;
			this.RefreshPanelLocation();
			Last_mX = mX;
			Last_mY = mY;
			return 2;
		}
		if( isNewTick > 0 ) {
			isNewTick--;
		}
		bool isMouseIn = MouseIsInside( mX, mY );
		if( !isMouseIn && !isMousePress ) {
			return 0;
		}
		MouseOnSizePort = GetNumberOfSizePort( mX, mY );
		
		//如果没有鼠标按下,直接返回
		if( !isMousePress ) {
			return 1;
		}
		//如果按下尺寸调节方框
		if( MouseSelectSizePort == -1 ) {
			//自动调节位置
			SX += mX - Last_mX;
			SY += mY - Last_mY;
			
			Last_mX = mX;
			Last_mY = mY;
			this.RefreshPanelLocation();
		}
		else {
			//BSX = this.SX;
			//BSY = this.SY;
			int BWidth = this.BaseWidth;
			int BHeight = this.BaseHeight;
			switch( MouseSelectSizePort ) {
				case 0: SX += mX - Last_mX; BWidth -= mX - Last_mX; SY += mY - Last_mY; BHeight -= mY - Last_mY; break;
				case 1: SY += mY - Last_mY; BHeight -= mY - Last_mY; break;
				case 2: SY += mY - Last_mY; BHeight -= mY - Last_mY; BWidth += mX - Last_mX; break;
				case 3: BWidth += mX - Last_mX; break;
				case 4: BWidth += mX - Last_mX; BHeight += mY - Last_mY; break;
				case 5: BHeight += mY - Last_mY; break;
				case 6: BHeight += mY - Last_mY; SX += mX - Last_mX; BWidth -= mX - Last_mX; break;
				case 7: SX += mX - Last_mX; BWidth -= mX - Last_mX; break;
				default: break;
			}
			if( BWidth >= SizePadding + 2 ) {
				//this.SX = BSX;
				this.BaseWidth = BWidth;
			}
			if( BHeight >= SizePadding + 2 ) {
				//this.SY = BSY;
				this.BaseHeight = BHeight;
			}
			Last_mX = mX;
			Last_mY = mY;
			
			//设置尺寸
			Width = BaseWidth;
			Height = BaseHeight;
			RefreshHeight();
		}
		return 2;
	}
	
	//设置用户参数
	public void SetUserValue( string vName, string vGroupMes, string vValue, int vX, int vY, int vWidth, int vHeight )
	{
		Name = vName;
		GroupMes = vGroupMes;
		Value = vValue;
		SX = vX;
		SY = vY;
		Width = vWidth;
		Height = vHeight;
		VPanel = new NotePanel( this );
		
		RefreshHeight();
		//this.RefreshPanelLocation();
		
		RefreshPen();
	}
	
	//刷新高度
	public void RefreshHeight()
	{
		if( Height == 0 ) {
			Height = (int)GUIset.mg.MeasureString( Value, f, Width - NotePadding*2 ).Height + NotePadding*2;
		}
		this.RefreshPanelLocation();
	}
	
	//判断坐标在哪个端口上, 从左上角开始顺时针依次为 0 1 2 3 4 5 6 7, 不在任何端口则返回-1
	int GetNumberOfSizePort( int mX, int mY )
	{
		for( int i = 0; i < 8; ++i ) {
			if( i != 3 && i != 7 ) {
				//continue;
			}
			if( GetRectangleOfSizePort( i ).Contains( mX, mY ) ) {
				return i;
			}
		}
		return -1;
	}
	
	//获取指定索引的尺寸端口
	Rectangle GetRectangleOfSizePort( int Index )
	{
		switch ( Index ) {
			case 0: return new Rectangle( this.SX - SizePadding, this.SY - SizePadding, SizePadding, SizePadding );
			case 1: return new Rectangle( this.MidX - SizePadding / 2, this.SY - SizePadding, SizePadding, SizePadding );
			case 2: return new Rectangle( this.SX + this.Width, this.SY - SizePadding, SizePadding, SizePadding );
			case 3: return new Rectangle( this.SX + this.Width, this.MidY - SizePadding / 2, SizePadding, SizePadding );
			case 4: return new Rectangle( this.SX + this.Width, this.SY + this.Height, SizePadding, SizePadding );
			case 5: return new Rectangle( this.MidX - SizePadding / 2, this.SY + this.Height, SizePadding, SizePadding );
			case 6: return new Rectangle( this.SX - SizePadding, this.SY + this.Height, SizePadding, SizePadding );
			case 7: return new Rectangle( this.SX - SizePadding, this.MidY - SizePadding / 2, SizePadding, SizePadding );
			default: return Rectangle.Empty;
		}
	}
}
}



