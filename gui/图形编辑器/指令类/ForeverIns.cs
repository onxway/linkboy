﻿
namespace n_ForeverIns
{
using System;
using System.Drawing;
using System.Drawing.Drawing2D;

using n_MyIns;
using n_Shape;
using n_EXP;
using n_GUIset;
using n_CondiEndIns;

//*****************************************************
//逻辑流程组件类
public class ForeverIns: MyIns
{
	public CondiEndIns InsEnd;
	
	GraphicsPath BackPath;
	
	//构造函数
	public ForeverIns(): base()
	{
		float w = GUIset.GetStringWidth( n_Language.Language.Forever );
		this.Width = (int)w + PadOffset*2;
		
		this.Height = GUIset.ExpFont.Height + 4;
		
		BackPath = new GraphicsPath();
	}
	
	//设置用户参数
	public void SetUserValue( string vName, string vGroupMes, int vX, int vY, int vAngle )
	{
		Name = vName;
		GroupMes = vGroupMes;
		SX = vX;
		SY = vY;
	}
	
	public override void UserMouseClick( int mX, int mY )
	{
		
	}
	
	public override void UserMouseMove( bool isMouseOn, int mX, int mY )
	{
		
	}
	
	//组件绘制工作1
	//绘制组件的外形等,基础绘制工作
	public override void Draw1( Graphics g )
	{
		if( isNewTick != 0 ) {
			return;
		}
		Brush b = FlowBackColor;
		Pen p = Pens.Sienna;
		if( isNote ) {
			b = brNoteColor;
			p = pnNoteColor;
		}
		Rectangle r = new Rectangle( SX, SY + HeadRectYOffset, this.Width, GUIset.ExpFont.Height + 4 );
		
		SIns.SetInsShape( BackPath, r, this.InsEnd );
		g.FillPath( b, BackPath );
		g.DrawPath( p, BackPath );
		g.DrawString( n_Language.Language.Forever, GUIset.ExpFont, FlowForeColor, SX + PadOffset, SY + HeadStringYOffset );
	}
}
}


