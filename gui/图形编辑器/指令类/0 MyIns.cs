﻿
namespace n_MyIns
{
using System;
using System.Drawing;

using n_MyObject;
using n_CondiEndIns;
using n_WhileIns;
using n_FuncIns;
using n_IfElseIns;
using n_ElseIns;
using n_EventIns;
using n_ForeverIns;
using n_LoopIns;
using n_UserFunctionIns;
using n_OS;
using n_MainSystemData;
using n_EXP;

//*****************************************************
//逻辑流程组件类
public abstract class MyIns: MyObject
{
	protected const int CompInsYOffset = 0;
	protected const int CompInsYStart = 0;
	
	protected const int LineOffset = 5;
	protected static int PadOffset;
	
	protected static Brush brNoteColor;
	protected static Pen pnNoteColor;
	
	static Pen RefB1;
	static Brush RefBB1;
	
	static Pen RefB2;
	static Brush RefBB2;
	
	static Pen RefB3;
	static Brush RefBB3;
	
	protected static Brush FlowBackColor;
	protected static Brush FlowForeColor;
	
	protected static Brush EventBackColor;
	protected static Brush EventNameBackColor;
	protected static Brush EventForeColor;
	
	public static Brush UserBackColor;
	
	
	public EXP MyEXP;
	
	//这两项用来调节头部方框和文字
	protected const int HeadRectYOffset = 0;
	protected const int HeadStringYOffset = 2;
	
	public bool ShowFollow;
	
	static SolidBrush SelectBrush;
	
	int ClickTime;
	public bool ShowTip;
	
	public bool isMoving;
	
	public MyIns PreIns;
	public MyIns NextIns;
	public MyIns HeadIns;
	//只有事件和函数会用到这个属性
	public MyIns SimCIns;
	
	public bool isMouseOn;
	
	public bool isNote;
	
	static Pen MoveLine;
	static Pen TipLine;
	
	const int FindX = 10;
	const int FindY = -20;
	
	public static bool MidMoveAll = false;
	
	const int MaxClickTimes = 4;
	
	public bool AddNear;
	public bool AddOn;
	
	static Image BackImage0;
	static Image BackImage1;
	const int AddWidth = 20;
	
	//单步执行箭头
	static Image StepImage;
	
	//初始化
	public static void Init()
	{
		string BaseDir = OS.SystemRoot + "Resource" + OS.PATH_S + "gui" + OS.PATH_S + "GFormControls" + OS.PATH_S;
		
		BackImage0 = new Bitmap( BaseDir + "Add0.png" );
		BackImage1 = new Bitmap( BaseDir + "Add1.png" );
		StepImage = new Bitmap( BaseDir + "step.png" );
		
		brNoteColor = new SolidBrush( Color.Gainsboro );
		pnNoteColor = new Pen( Color.Silver );
		
		//RefB.DashStyle = System.Drawing.Drawing2D.DashStyle.Dot;
		
		RefB1 = new Pen( Color.FromArgb( 70, Color.Green ), 3 );
		RefB1.DashStyle = System.Drawing.Drawing2D.DashStyle.Dash;
		RefBB1 = new SolidBrush( Color.FromArgb( 160, Color.Green ) );
		
		RefB2 = new Pen( Color.FromArgb( 80, Color.CornflowerBlue ), 3 );
		RefB2.DashStyle = System.Drawing.Drawing2D.DashStyle.Dash;
		RefBB2 = new SolidBrush( Color.FromArgb( 160, Color.CornflowerBlue ) );
		
		RefB3 = new Pen( Color.FromArgb( 80, Color.OrangeRed ), 3 );
		RefB3.DashStyle = System.Drawing.Drawing2D.DashStyle.Dash;
		RefBB3 = new SolidBrush( Color.FromArgb( 160, Color.OrangeRed ) );
		
		UserBackColor = new SolidBrush( Color.FromArgb(190, 150, 255) );
		
		SelectBrush = new SolidBrush( Color.FromArgb( 200, Color.OrangeRed ) );
		
		EventBackColor = new SolidBrush( Color.FromArgb(230, 190, 50) );
		EventNameBackColor = new SolidBrush( Color.FromArgb(130, 190, 140) );
		EventForeColor = Brushes.Black;
		
		MoveLine = new Pen( Color.SlateGray, 1 );
		TipLine = new Pen( Color.Blue, 4 );
		
		FlowBackColor = new SolidBrush( Color.FromArgb(230, 190, 50) );
		
		FlowForeColor = Brushes.Black;
		
		PadOffset = (int)(n_GUIset.GUIset.ExpFont.Height / 5);
	}
	
	//构造函数
	public MyIns(): base()
	{
		ignoreHit = true;
		
		ShowFollow = true;
		
		isNote = false;
		
		WidthPadding = 0;
		
		this.ExYLPadding = 0;
		
		AddNear = false;
		AddOn = false;
		
		Width = 150;
		Height = 20;
		
		ClickTime = 0;
		
		ShowTip = false;
		PreIns = null;
		NextIns = null;
		isMouseOn = false;
	}
	
	//组件扩展函数
	public override bool MouseIsInEX( int mX, int mY )
	{
		return false;
	}
	
	void Refresh0()
	{
		int ox = myX - targetSX;
		int oy = myY - targetSY;
		
		int of = 5;
		
		if( AutoMove ) {
			if( ox < 0 ) {
				int oox = -ox / of;
				if( oox == 0 ) oox = 1;
				myX += oox;
			}
			if( ox > 0 ) {
				int oox = ox / of;
				if( oox == 0 ) oox = 1;
				myX -= oox;
			}
			if( oy < 0 ) {
				int ooy = -oy / of;
				if( ooy == 0 ) ooy = 1;
				myY += ooy;
			}
			if( oy > 0 ) {
				int ooy = oy / of;
				if( ooy == 0 ) ooy = 1;
				myY -= ooy;
			}
			if( myX == targetSX && myY == targetSY ) {
				AutoMove = false;
			}
			G.CGPanel.MyRefresh();
		}
	}
	
	//跳过并结束自动移动
	public void EndAutoMove()
	{
		if( AutoMove ) {
			AutoMove = false;
			myX = targetSX;
			myY = targetSY;
		}
	}
	
	//组件绘制工作2
	//绘制组件的端口链接导线,需要在所有组件基础绘制完成之后进行
	//绘制端口的方框背景
	public override void Draw2( Graphics g )
	{
		if( isNewTick != 0 ) {
			return;
		}
		
		//自动移动
		Refresh0();
		
		//如果被选中状态, 显示一个框
		if( isSelect ) {
			//g.FillRectangle( SelectBrush, StartX + this.SX - 3, StartY + this.SY - 3, Width + 6, Height + 6 );
			//g.DrawRectangle( Pens.Purple, StartX + this.SX - 3, StartY + this.SY - 3, Width + 6, Height + 6 );
			
			g.FillRectangle( SelectBrush, this.SX - 10, this.SY - 3, 5, Height + 6 );
		}
		
		//判断指令添加点
		if( ShowFollow && !(this is CondiEndIns && !((CondiEndIns)this).CanLinked ) ) {
			if( AddOn ) {
				//g.FillRectangle( Brushes.OrangeRed, StartX + SX - 18, StartY + SY + (Height - 15)/2, 15, 15 );
				g.DrawImage( BackImage1, SX - AddWidth, SY + Height - AddWidth - 2, AddWidth, AddWidth );
			}
			else if( AddNear ) {
				//g.FillRectangle( Brushes.LightGray, StartX + SX - 18, StartY + SY + (Height - 15)/2, 15, 15 );
				g.DrawImage( BackImage0, SX - AddWidth, SY + Height - AddWidth - 2, AddWidth, AddWidth );
			}
			else {
				//...
			}
		}
		
		if( this is ElseIns || this is CondiEndIns || this is EventIns || this is UserFunctionIns ) {
			return;
		}
		//绘制一个小圆棒, 指向其他的模块
		if( isMousePress ) {
			
			if( SystemData.isBlack ) {
				g.FillEllipse( Brushes.WhiteSmoke, this.SX + FindX - 5, this.SY + FindY - 5, 10, 10 );
				g.DrawLine( MoveLine, this.SX + FindX, this.SY + FindY, this.SX + FindX, this.SY );
				g.FillEllipse( Brushes.LightGreen, this.SX + FindX - 4, this.SY + FindY - 4, 8, 8 );
			}
			else {
				g.FillEllipse( Brushes.Green, this.SX + FindX - 5, this.SY + FindY - 5, 10, 10 );
				g.DrawLine( MoveLine, this.SX + FindX, this.SY + FindY, this.SX + FindX, this.SY );
				g.FillEllipse( Brushes.LightGreen, this.SX + FindX - 4, this.SY + FindY - 4, 8, 8 );
			}
		}
		
		/*
		//绘制变量指向矢量
		if( !G.SimulateMode && G.CGPanel.myModuleList.MouseOnObject != null ) {
			
			if( MyEXP != null ) {
				int tt = MyEXP.UsedName( G.CGPanel.myModuleList.MouseOnObject );
				if( tt == 1 ) {
					g.DrawLine( RefB1, this.SX + Width/2, this.SY + Height/2,
				           G.CGPanel.myModuleList.MouseOnObject.SX + G.CGPanel.myModuleList.MouseOnObject.Width/2,
				           G.CGPanel.myModuleList.MouseOnObject.SY + G.CGPanel.myModuleList.MouseOnObject.Height/2 );
					//g.DrawLine( RefB, this.SX + Width/2, this.SY + Height/2, mg.SX + mg.Width/2, mg.SY + mg.Height/2 );
					g.FillEllipse( RefBB1, this.SX + Width/2 - Height/2, this.SY, Height, Height );
					g.DrawEllipse( Pens.Black, this.SX + Width/2 - Height/2, this.SY, Height, Height );
				}
				if( tt == 2 ) {
					g.DrawLine( RefB2, this.SX + Width/2, this.SY + Height/2,
				           G.CGPanel.myModuleList.MouseOnObject.SX + G.CGPanel.myModuleList.MouseOnObject.Width/2,
				           G.CGPanel.myModuleList.MouseOnObject.SY + G.CGPanel.myModuleList.MouseOnObject.Height/2 );
					//g.DrawLine( RefB, this.SX + Width/2, this.SY + Height/2, mg.SX + mg.Width/2, mg.SY + mg.Height/2 );
					g.FillEllipse( RefBB2, this.SX + Width/2 - Height/2, this.SY, Height, Height );
					g.DrawEllipse( Pens.Black, this.SX + Width/2 - Height/2, this.SY, Height, Height );
				}
				if( tt == 3 ) {
					g.DrawLine( RefB3, this.SX + Width/2, this.SY + Height/2,
				           G.CGPanel.myModuleList.MouseOnObject.SX + G.CGPanel.myModuleList.MouseOnObject.Width/2,
				           G.CGPanel.myModuleList.MouseOnObject.SY + G.CGPanel.myModuleList.MouseOnObject.Height/2 );
					//g.DrawLine( RefB, this.SX + Width/2, this.SY + Height/2, mg.SX + mg.Width/2, mg.SY + mg.Height/2 );
					g.FillEllipse( RefBB3, this.SX + Width/2 - Height/2, this.SY, Height, Height );
					g.DrawEllipse( Pens.Black, this.SX + Width/2 - Height/2, this.SY, Height, Height );
				}
			}
		}
		*/
		
//		bool Test = true;
//		if( Test ) {
//			g.DrawString( this.Name, PortFont, Brushes.Black, StartX + this.SX + this.Width + 10, StartY + this.SY );
//			if( this.PreIns != null ) {
//				g.DrawString( "P:" + this.PreIns.Name, PortFont, Brushes.White, StartX + this.SX + this.Width + 60, StartY + this.SY );
//			}
//			if( this.NextIns != null ) {
//				g.DrawString( "N:" + this.NextIns.Name, PortFont, Brushes.White, StartX + this.SX + this.Width + 110, StartY + this.SY );
//			}
//		}
	}
	
	//组件绘制工作3
	//绘制组件的端口和名称,需要在所有链接导线绘制完成之后进行
	public override void Draw3( Graphics g )
	{
		if( isNewTick != 0 ) {
			return;
		}
		if( n_ImagePanel.ImagePanel.DebugMode ) {
			g.DrawString( Name, n_GUIset.GUIset.ExpFont, Brushes.Black, this.SX - 50, this.SY + 5 );
			
			string es = "(";
			if( PreIns != null ) {
				es += "P:" + PreIns.Name + " - ";
			}
			if( NextIns != null ) {
				es += "N:" + NextIns.Name;
			}
			es += ")";
			g.DrawString( es, n_GUIset.GUIset.ExpFont, Brushes.OrangeRed, this.SX + Width + 5, this.SY + 5 );
		}
	}
	
	public void DrawRef( Graphics g )
	{
		if( isNewTick != 0 ) {
			return;
		}
		//绘制变量指向矢量
		if( !G.SimulateMode && G.CGPanel.myModuleList.MouseOnObject != null ) {
			
			if( MyEXP != null ) {
				int tt = MyEXP.UsedName( G.CGPanel.myModuleList.MouseOnObject );
				if( tt == 1 ) {
					g.DrawLine( RefB1, this.SX + Width/2, this.SY + Height/2,
				           G.CGPanel.myModuleList.MouseOnObject.SX + G.CGPanel.myModuleList.MouseOnObject.Width/2,
				           G.CGPanel.myModuleList.MouseOnObject.SY + G.CGPanel.myModuleList.MouseOnObject.Height/2 );
					//g.DrawLine( RefB, this.SX + Width/2, this.SY + Height/2, mg.SX + mg.Width/2, mg.SY + mg.Height/2 );
					g.FillEllipse( RefBB1, this.SX + Width/2 - Height/2, this.SY, Height, Height );
					g.DrawEllipse( Pens.Black, this.SX + Width/2 - Height/2, this.SY, Height, Height );
				}
				if( tt == 2 ) {
					g.DrawLine( RefB2, this.SX + Width/2, this.SY + Height/2,
				           G.CGPanel.myModuleList.MouseOnObject.SX + G.CGPanel.myModuleList.MouseOnObject.Width/2,
				           G.CGPanel.myModuleList.MouseOnObject.SY + G.CGPanel.myModuleList.MouseOnObject.Height/2 );
					//g.DrawLine( RefB, this.SX + Width/2, this.SY + Height/2, mg.SX + mg.Width/2, mg.SY + mg.Height/2 );
					g.FillEllipse( RefBB2, this.SX + Width/2 - Height/2, this.SY, Height, Height );
					g.DrawEllipse( Pens.Black, this.SX + Width/2 - Height/2, this.SY, Height, Height );
				}
				if( tt == 3 ) {
					g.DrawLine( RefB3, this.SX + Width/2, this.SY + Height/2,
				           G.CGPanel.myModuleList.MouseOnObject.SX + G.CGPanel.myModuleList.MouseOnObject.Width/2,
				           G.CGPanel.myModuleList.MouseOnObject.SY + G.CGPanel.myModuleList.MouseOnObject.Height/2 );
					//g.DrawLine( RefB, this.SX + Width/2, this.SY + Height/2, mg.SX + mg.Width/2, mg.SY + mg.Height/2 );
					g.FillEllipse( RefBB3, this.SX + Width/2 - Height/2, this.SY, Height, Height );
					g.DrawEllipse( Pens.Black, this.SX + Width/2 - Height/2, this.SY, Height, Height );
				}
			}
		}
	}
	
	public override void DrawSim( Graphics g )
	{
		
	}
	
	//组件绘制工作4
	//绘制组件高亮选中 (对于指令则是绘制TipIns(外围方框)
	public override void DrawHighLight( Graphics g )
	{
		if( isNewTick != 0 ) {
			return;
		}
		if( ShowTip ) {
			g.DrawRectangle( TipLine, this.SX - 4, this.SY - 4, this.Width + 8, this.Height + 8 );
		}
	}
	
	public void DrawCSim( Graphics g )
	{
		if( !G.SimulateMode ) {
			return;
		}
		if( isNewTick != 0 ) {
			return;
		}
		if( (this is CondiEndIns) && !((CondiEndIns)this).CanLinked ) {
			return;
		}
		//g.DrawEllipse( Pens.Red, this.SX - this.Height, this.SY, this.Height, this.Height );
		g.DrawImage( StepImage, SX - AddWidth, SY + Height - AddWidth - 2, AddWidth, AddWidth );
	}
	
	//删除
	public override void Remove()
	{
		if( this is EventIns ) {
			((EventIns)this).isDeleted = true;
		}
		//遍历组件列表,查找目标组件
		for( int i = 0; i < myObjectList.MLength; ++i ) {
			if( myObjectList.ModuleList[ i ] == null ) {
				continue;
			}
			if( myObjectList.ModuleList[ i ] == this ) {
				
				if( myObjectList.ModuleList[ i ] is UserFunctionIns ) {
					UserFunctionIns ui = (UserFunctionIns)myObjectList.ModuleList[ i ];
					if( ui.Target != null ) {
						if( ui.Target.Target != null ) {
							ui.Target.Target = null;
						}
						ui.Target = null;
					}
				}
				
				myObjectList.ModuleList[ i ] = null;
				return;
			}
		}
	}
	
	//鼠标按下事件, 在组件中返回 true,不在组件中返回 false.
	public override bool MouseDown1( int mX, int mY )
	{
		if( ShowFollow && AddOn && !(this is CondiEndIns && !((CondiEndIns)this).CanLinked ) ) {
			n_FuncIns.FuncIns funci = new n_FuncIns.FuncIns();
			if( this is FuncIns ) {
				funci.SetUserValue( n_GUICommon.GUICommon.SearchName( G.CGPanel ), this.GroupMes, ((FuncIns)this).MyEXP.Get(), this.SX - funci.Width, this.SY + Height, 0 );
			}
			else {
				funci.SetUserValue( n_GUICommon.GUICommon.SearchName( G.CGPanel ), this.GroupMes, n_Language.Language.ModuleFunc, this.SX - funci.Width, this.SY + Height, 0 );
			}
			G.CGPanel.myModuleList.Add( funci );
			
			MyIns temp = this.NextIns;
			
			this.NextIns = funci;
			funci.PreIns = this;
			funci.NextIns = temp;
			if( temp != null ) {
				temp.PreIns = funci;
			}
			NeedMove = true;
			FormatLocation( this );
			NeedMove = false;
			
			G.CGPanel.SelPanel.CancelSelect();
			
			c_MIDI.Music.PlaySound( 5, 93, 100 );
			return true;
		}
		
		if( !MouseIsInside( mX, mY ) ) {
			return false;
		}
		ClickTime = 0;
		if( isMousePress ) {
			ClickTime = MaxClickTimes;
		}
		isMousePress = true;
		Last_mX = mX;
		Last_mY = mY;
		
		return true;
	}
	
	//鼠标松开事件
	public override void MouseUp1( int mX, int mY )
	{
		if( G.CGPanel.myRecycler.ShowMessage ) {
			G.CGPanel.myRecycler.ShowMessage = false;
			if( G.CGPanel.myRecycler.Showed < 4 ) {
				G.CGPanel.myRecycler.Showed++;
			}
		}
		
		if( isMousePress ) {
			
			//触发控件的点击事件
			if( ClickTime < MaxClickTimes ) {
				UserMouseClick( mX, mY );
			}
			else {
				//判断是否移动到垃圾桶中
				float ScreenX = (G.CGPanel.StartX + mX) * n_ImagePanel.ImagePanel.AScale / n_ImagePanel.ImagePanel.AScaleMid;
				float ScreenY = (G.CGPanel.StartY + mY) * n_ImagePanel.ImagePanel.AScale / n_ImagePanel.ImagePanel.AScaleMid;
				
				if( G.CGPanel.myRecycler.isInside( ScreenX, ScreenY ) ) {
					
					if( G.SimulateMode ) {
						n_Debug.Warning.WarningMessage = "仿真模式下无法删除模块, 请先结束仿真";
						return;
					}
					
					MyIns m = this;
					while( m != null ) {
						m.Remove();
						m = m.NextIns;
					}
					if( this.myObjectList.TipIns != null ) {
						this.myObjectList.TipIns = null;
					}
					c_MIDI.Music.PlaySound( 6, 60, 100 );
				}
				//判断是否需要连接到目标指令上
				else if( this.myObjectList.TipIns != null ) {
					
					MyIns temp = this.myObjectList.TipIns.NextIns;
					
					this.myObjectList.TipIns.NextIns = this;
					this.PreIns = this.myObjectList.TipIns;
					
					if( temp != null ) {
						
						MyIns t = this;
						while( t.NextIns != null ) {
							t = t.NextIns;
						}
						t.NextIns = temp;
						temp.PreIns = t;
					}
					
					NeedMove = true;
					FormatLocation( this.myObjectList.TipIns );
					NeedMove = false;
					
					this.myObjectList.TipIns.ShowTip = false;
					this.myObjectList.TipIns = null;
					
					G.CGPanel.SelPanel.CancelSelect();
					
					c_MIDI.Music.PlaySound( 5, 93, 100 );
				}
				else {
					//...
				}
			}
		}
		if( isNewTick == 0 ) {
			isMousePress = false;
		}
	}
	
	//-------------------------------------------------------------------------------
	
	//DataList[0]:		总的延时时间
	//DataList[1]:		当前延时时间
	//DataList[2]:		指令高亮状态
	
	//初始化仿真
	public void ResetSim()
	{
		for( int i = 0; i < 10; i++ ) {
			DataList[i] = 0;
		}
	}
	
	//判断是否为激活状态
	public bool isActive()
	{
		return DataList[2] != 0;
	}
	
	//读取指令的当前延时时间
	public int GetTime()
	{
		return DataList[1];
	}
	
	//读取指令的总的延时时间
	public int GetTotalTime()
	{
		return DataList[0];
	}
	
	//-------------------------------------------------------------------------------
	
	public MyIns GetHeadIns()
	{
		MyIns mi = this;
		while( mi.PreIns != null ) {
			mi = mi.PreIns;
		}
		return mi;
	}
	
	//-------------------------------------------------------------------------------
	
	//设置当前指令后续指令的可见状态
	public void SetFollowVisible( bool b )
	{
		MyIns Next = this;
		while( Next.NextIns != null ) {
			
			Next = Next.NextIns;
			Next.Visible = b;
		}
	}
	
	bool NeedMove = false;
	
	//对齐所有指令
	protected void FormatLocation( MyIns StartIns )
	{
		MyIns s = StartIns;
		while( StartIns.NextIns != null ) {
			
			MyIns Next = StartIns.NextIns;
			
			Next.GroupMes = s.GroupMes;
			
			int sx = 0;
			//int PerHeight = StartIns.Height + 4;
			
			//int TabWidth = EXPStartX;
			int TabWidth = 25;
			
			if( StartIns is FuncIns ) {
				//PerHeight = n_GUIset.GUIset.ExpFont.Height + 4 + 6;
				sx = 0;
			}
			if( StartIns is WhileIns ||
			    StartIns is IfElseIns ||
			    StartIns is ElseIns ||
			    StartIns is ForeverIns ||
			    StartIns is LoopIns ||
			    StartIns is n_ExtIns.ExtIns ||
				StartIns is EventIns ||
				StartIns is UserFunctionIns ) {
				sx = TabWidth;
			}
			if( Next is CondiEndIns ) {
				sx -= TabWidth;
			}
			if( Next is ElseIns ) {
				sx -= TabWidth;
			}
			
			if( NeedMove ) {
				Next.targetSX = StartIns.targetSX + sx;
				int sy = StartIns.targetSY + StartIns.Height + 10 - (Next.Height - n_GUIset.GUIset.ExpFont.Height);
				Next.targetSY = sy;
				if( sy < StartIns.targetSY + StartIns.Height ) {
					Next.targetSY = StartIns.targetSY + StartIns.Height;
				}
				Next.AutoMove = true;
			}
			else {
				Next.SX = StartIns.SX + sx;
				int sy = StartIns.SY + StartIns.Height + 10 - (Next.Height - n_GUIset.GUIset.ExpFont.Height);
				Next.SY = sy;
				if( sy < StartIns.SY + StartIns.Height ) {
					Next.SY = StartIns.SY + StartIns.Height;
				}
			}
			
			StartIns = Next;
		}
	}
	
	//鼠标移动事件,当鼠标在组件上时返回1,组件被拖动时返回2
	public override int MouseMove1( int mX, int mY )
	{
		if( isNewTick == MaxNewTick ) {
			isNewTick--;
			if( this is ElseIns || this is CondiEndIns ) {
				return 2;
			}
			
			isMousePress = true;
			this.SX = mX - this.Width / 2;
			this.SY = mY - this.Height / 2;
			Last_mX = mX;
			Last_mY = mY;
			
			if( this is IfElseIns || this is WhileIns || this is ForeverIns || this is LoopIns || this is n_ExtIns.ExtIns || this is EventIns || this is UserFunctionIns ) {
				FormatLocation( this );
			}
			return 2;
		}
		if( isNewTick > 0 ) {
			isNewTick--;
		}
		isMouseOn = MouseIsInside( mX, mY );
		
		MouseAddNear( mX, mY );
		MouseAddOn( mX, mY );
		
		//结束和否则指令不允许拖动
		if( this is CondiEndIns || this is ElseIns ) {
			return 0;
		}
		
		UserMouseMove( isMouseOn, mX, mY );
		if( !isMouseOn && !isMousePress ) {
			return 0;
		}
		if( !isMousePress ) {
			return 1;
		}
		
		G.CGPanel.myRecycler.ShowMessage = true;
		
		//断开当前指令和前一个指令的连接
		ClickTime++;
		if( ClickTime >= MaxClickTimes ) {
			
			//G.CGPanel.myRecycler.Visible = true;
			
			//if( this.PreIns != null ) {
				
				MyIns temp = this;
				int Level = 0;
				int ElseLevel = 0;
				
				bool EnableSelect = temp.isSelect;
				
				while(true ) {
					if( temp is EventIns ) {
						Level++;
					}
					if( temp is UserFunctionIns ) {
						Level++;
					}
					if( temp is LoopIns ) {
						Level++;
					}
					if( temp is n_ExtIns.ExtIns ) {
						Level++;
					}
					if( temp is ForeverIns ) {
						Level++;
					}
					if( temp is WhileIns ) {
						Level++;
					}
					if( temp is IfElseIns ) {
						ElseLevel++;
						Level++;
					}
					if( temp is CondiEndIns ) {
						Level--;
					}
					if( temp is ElseIns ) {
						if( ElseLevel == 0 ) {
							Level--;
						}
						else {
							ElseLevel--;
						}
					}
					
					if( !EnableSelect ) {
						if( Level == 0 ) {
							break;
						}
					}
					else {
						if( Level < 0 ) {
							temp = temp.PreIns;
							break;
						}
						//
						if( !temp.isSelect ) {
							temp = temp.PreIns;
							break;
						}
					}
					
					if( temp.NextIns == null ) {
						break;
					}
					temp =  temp.NextIns;
				}
				if( temp.NextIns == null ) {
					if( this.PreIns != null ) {
						this.PreIns.NextIns = null;
					}
				}
				else {
					if( this.PreIns != null ) {
						this.PreIns.NextIns = temp.NextIns;
					}
					temp.NextIns.PreIns = this.PreIns;
				}
				if( this.PreIns != null ) {
					
					NeedMove = true;
					FormatLocation( this.PreIns );
					NeedMove = false;
					
					c_MIDI.Music.PlaySound( 5, 85, 100 );
				}
				
				this.PreIns = null;
				temp.NextIns = null;
			//}
			//SX += mX - Last_mX;
			//SY += mY - Last_mY;
			//Last_mX += mX - Last_mX;
			//Last_mY += mY - Last_mY;
			AllMove( mX - Last_mX, mY - Last_mY );
		}
		
		//事件指令不允许连接到其他指令上
		if( this is EventIns || this is UserFunctionIns ) {
			return 2;
		}
		if( ClickTime >= MaxClickTimes ) {
			//搜索需要连接的目标指令
			this.myObjectList.TipIns = null;
			int ThisX = this.SX + FindX;
			int ThisY = this.SY + FindY;
			
			foreach( MyObject mo in myObjectList ) {
				if( !(mo is MyIns) ) {
					continue;
				}
				if( !mo.isVisible() ) {
					continue;
				}
				MyIns mi = (MyIns)mo;
				if( !mi.ShowFollow ) {
					continue;
				}
				if( mi is CondiEndIns && !((CondiEndIns)mi).CanLinked ) {
					continue;
				}
				
				if( ThisX >= mi.SX && ThisX < mi.SX + mi.Width &&
				   ThisY >= mi.SY && ThisY < mi.SY + mi.Height ) {
					mi.ShowTip = true;
					if( this.myObjectList.TipIns != mi ) {
						this.myObjectList.TipIns = mi;
					}
				}
				else {
					mi.ShowTip = false;
				}
			}
		}
		return 2;
	}
	
	//带着下一个关联模块一块儿移动
	public void AllMove( int offmX, int offmY )
	{
		this.isMoving = true;
		
		SX += offmX;
		SY += offmY;
		Last_mX += offmX;
		Last_mY += offmY;
		
		if( this.NextIns != null ) {
			this.NextIns.AllMove( offmX, offmY );
		}
	}
	
	public abstract void UserMouseClick( int mX, int mY );
	
	public abstract void UserMouseMove( bool isMouseOn, int mX, int mY );
	
	//判断鼠标是否在当前添加点上
	void MouseAddNear( int mX, int mY )
	{
		AddNear = false;
		
		//判断是否在组件上
		if( mX >= SX - AddWidth && mX <= SX + Width && mY >= SY + (Height - AddWidth)/2 && mY <= SY + Height - (Height - AddWidth)/2 ) {
			AddNear = true;
		}
	}
	
	//判断鼠标是否在当前添加点上
	void MouseAddOn( int mX, int mY )
	{
		AddOn = false;
		
		//判断是否在组件上
		if( mX >= SX - AddWidth && mX <= SX && mY >= SY + Height - AddWidth - 2 && mY <= SY + Height - (Height - AddWidth)/2 ) {
			AddOn = true;
		}
	}
}
}



