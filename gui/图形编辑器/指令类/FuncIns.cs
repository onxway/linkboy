﻿
namespace n_FuncIns
{
using System;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Windows.Forms;

using n_MyIns;
using n_GUIcoder;
using n_MyObject;
using n_UserModule;
using n_EXP;

//*****************************************************
//逻辑流程组件类
public class FuncIns: MyIns
{
	public string Mes;
	
	public bool isExpEditor;
	
	//构造函数
	public FuncIns(): base()
	{
		MyEXP = new EXP( this, 0, 0 );
		MyEXP.ExpChanged += new EXP.deleExpChanged( ExpChanged );
		isExpEditor = false;
	}
	
	//设置用户参数
	public void SetUserValue( string vName, string vGroupMes, string vMes, int vX, int vY, int vAngle )
	{
		Name = vName;
		GroupMes = vGroupMes;
		Mes = vMes;
		MyEXP.Set( vMes );
		
		
		SX = vX;
		SY = vY;
	}
	
	//刷新组件尺寸
	public void ExpChanged()
	{
		//int w = this.Width;
		this.Width = this.MyEXP.Width;
		this.Height = this.MyEXP.Height;
		//this.SX += (w - this.Width) / 2;
		
		if( this.PreIns != null ) {
			FormatLocation( this.PreIns );
		}
	}
	
	public override void UserMouseClick( int mX, int mY )
	{
		MyEXP.MouseClick( mX -  this.SX, mY - this.SY );
	}
	
	public override void UserMouseMove( bool isMouseOn, int mX, int mY )
	{
		MyEXP.MouseMove( isMouseOn, mX -  this.SX, mY - this.SY );
	}
	
	//组件绘制工作1
	//绘制组件的外形等,基础绘制工作
	public override void Draw1( Graphics g )
	{
		//双击时添加指令, 应立刻显示出来, 而不需要移动一下
		if( isNewTick != 0 ) {
			//return;
		}
		MyEXP.Draw( g, -this.Width / 2, - this.Height / 2 );
	}
}
}

