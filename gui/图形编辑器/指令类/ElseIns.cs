﻿
namespace n_ElseIns
{
using System;
using System.Drawing;
using System.Drawing.Drawing2D;

using n_MyIns;
using n_Shape;
using n_GUIset;
using n_CondiEndIns;
using n_IfElseIns;

//*****************************************************
//逻辑流程组件类
public class ElseIns: MyIns
{
	public IfElseIns InsStart;
	public CondiEndIns InsEnd;
	
	const int W = 49;
	
	//构造函数
	public ElseIns(): base()
	{
		this.Width = GUIset.GetPix(W);
		this.Height = GUIset.ExpFont.Height + 4;
	}
	
	//设置用户参数
	public void SetUserValue( string vName, string vGroupMes, int vX, int vY, int vAngle )
	{
		Name = vName;
		GroupMes = vGroupMes;
		SX = vX;
		SY = vY;
	}
	
	public override void UserMouseClick( int mX, int mY )
	{
		InsStart.DisableElse();
	}
	
	public override void UserMouseMove( bool isMouseOn, int mX, int mY )
	{
		
	}
	
	//组件绘制工作1
	//绘制组件的外形等,基础绘制工作
	public override void Draw1( Graphics g )
	{
		if( isNewTick != 0 ) {
			return;
		}
		//绘制竖直连线
		//g.FillRectangle( Brushes.Orange, StartX + SX, StartY + SY + LineOffset, 5, this.InsEnd.SY - this.SY );
		
		/*
		Rectangle r;
		if( G.InsPath ) {
			r = new Rectangle( SX + 2, SY, W-2, this.Height );
		}
		else {
			r = new Rectangle( SX, SY, W, this.Height );
		}
		//绘制组件名称
		//g.DrawRectangle( Pens.White, r );
		GraphicsPath gp = Shape.CreateRoundedRectanglePath( r );
		g.FillPath( FlowBackColor, gp );
		//g.DrawPath( Pens.Orange, gp );
		*/
		
		if( isMouseOn ) {
			g.DrawString( n_Language.Language.DelElse, GUIset.ExpFont, FlowForeColor, SX + 3, SY + HeadStringYOffset );
		}
		else {
			g.DrawString( n_Language.Language.Else, GUIset.ExpFont, FlowForeColor, SX + 3, SY + HeadStringYOffset );
		}
	}
}
}


