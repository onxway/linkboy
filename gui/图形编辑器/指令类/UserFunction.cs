﻿
namespace n_UserFunctionIns
{
using System;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Windows.Forms;
using System.Collections.Generic;

using n_MyIns;
using n_GUIcoder;
using n_MyObject;
using n_UserModule;
using n_GUIset;
using n_CondiEndIns;
using n_Shape;
using n_EXP;
using n_MainSystemData;

//*****************************************************
//逻辑流程组件类
public class UserFunctionIns: MyIns
{
	const int OffetWidth = 5;
	
	string vvUserName;
	public string UserName {
		get { return vvUserName; }
		set {
			vvUserName = value;
			RType = vvUserName.Remove( vvUserName.IndexOf( ' ' ) );
			FName = vvUserName.Remove( 0, vvUserName.IndexOf( ' ' ) + 1 );
			Width = HeadWidth + (int)GUIset.mg.MeasureString( FName, GUIset.ExpFont ).Width + 8 + OffetWidth;
			
			FNameCut = FName.Split( ' ' );
		}
	}
	public string RType;
	public string FName;
	public string[] FNameCut;
	
	int HeadWidth;
	
	public bool isEvent;
	public UserFunctionIns Target;
	public string tempTargetName;
	
	public CondiEndIns InsEnd;
	
	GraphicsPath BackPath;
	
	//构造函数
	public UserFunctionIns(): base()
	{
		this.Width = 200;
		this.Height = 35;
		isEvent = false;
		tempTargetName = null;
		HeadWidth = (int)GUIset.mg.MeasureString( "?int32", GUIset.ExpFont ).Width;
		
		BackPath = new GraphicsPath();
	}
	
	//获取形参列表
	public string[] GetVarList( string NeedType, List<string> TypeList, List<string> VarTypeList )
	{
		string R = null;
		for( int i = 0; i < FNameCut.Length; i++ ) {
			if( FNameCut[i].StartsWith( "?" ) ) {
				
				if( FNameCut[i] == NeedType ||
				    (NeedType == EXP.ENote.v_fix && FNameCut[i] == EXP.ENote.v_int32) ||
				    (NeedType == EXP.ENote.v_void && (FNameCut[i] == EXP.ENote.v_bool || FNameCut[i] == EXP.ENote.v_int32 || FNameCut[i] == EXP.ENote.v_fix) ) ||
					NeedType == EXP.ENote.v_bool && FNameCut[i] == EXP.ENote.v_int32 || FNameCut[i] == EXP.ENote.v_fix || FNameCut[i] == EXP.ENote.v_Cstring ) {
					R += FNameCut[i+1] + " ";
					TypeList.Add( EXP.ENote.t_VAR );
					VarTypeList.Add( FNameCut[i] );
				}
				i++;
			}
		}
		if( R == null ) {
			return null;
		}
		return R.TrimEnd( ' ' ).Split( ' ' );
	}
	
	//获取所有形参列表
	public string[] GetVarList()
	{
		string R = null;
		for( int i = 0; i < FNameCut.Length; i++ ) {
			if( FNameCut[i].StartsWith( "?" ) ) {
				R += FNameCut[i+1] + " ";
				i++;
			}
		}
		if( R == null ) {
			return null;
		}
		return R.TrimEnd( ' ' ).Split( ' ' );
	}
	
	//获取函数类型
	public string GetUserType()
	{
		return RType.Remove( 0, 1 );
	}
	
	//获取函数名字
	public string GetFuncName()
	{
		string[] cut = FName.Split( ' ' );
		string Name = "";
		string VarList = "";
		for( int i = 0; i < cut.Length; ++i ) {
			if( cut[i].StartsWith( "?" ) ) {
				Name += "_";
				
				string vtype = cut[i].Remove( 0, 1 );
				if( vtype == GType.g_Cstring ) {
					vtype = GType.c_array;
				}
				
				VarList += " " + vtype + " " + cut[i+1] + ",";
				i++;
			}
			else {
				Name += cut[i];
			}
		}
		return Name;
	}
	
	//获取函数遥控名字
	public string GetRemoName()
	{
		string[] cut = FName.Split( ' ' );
		string Name = "";
		for( int i = 0; i < cut.Length; ++i ) {
			if( cut[i].StartsWith( "?" ) ) {
				Name += "#";
				i++;
			}
			else {
				Name += cut[i];
			}
		}
		return Name;
	}
	
	//获取函数定义
	public string GetFuncDefine( ref string FuncName )
	{
		string[] cut = FName.Split( ' ' );
		string Name = "";
		string VarList = null;
		for( int i = 0; i < cut.Length; ++i ) {
			if( cut[i].StartsWith( "?" ) ) {
				Name += "_";
				
				string vtype = cut[i].Remove( 0, 1 );
				if( vtype == GType.g_Cstring ) {
					vtype = GType.c_array;
				}
				if( VarList == null ) {
					VarList = " " + vtype + " " + cut[i+1];
				}
				else {
					VarList += ", " + vtype + " " + cut[i+1] + " ";
				}
				i++;
			}
			else {
				Name += cut[i];
			}
		}
		FuncName = Name;
		if( VarList == null ) {
			VarList = "void";
		}
		return GetUserType() + " " + Name + "(" + VarList + ")";
	}
	
	//获取函数定义
	public string GetFuncDefinePython()
	{
		string[] cut = FName.Split( ' ' );
		string Name = "";
		string VarList = "";
		for( int i = 0; i < cut.Length; ++i ) {
			if( cut[i].StartsWith( "?" ) ) {
				Name += "_";
				VarList += " " + cut[i+1] + ",";
				i++;
			}
			else {
				Name += cut[i];
			}
		}
		return "def " + GUIcoder.GetPinYin( Name ) + "(" + VarList.TrimEnd( ',' ) + " ):";
	}
	
	//获取函数调用
	public string GetFuncCall()
	{
		string[] cut = FName.Split( ' ' );
		string Name = "";
		for( int i = 0; i < cut.Length; ++i ) {
			if( cut[i].StartsWith( "?" ) ) {
				Name += " ( " + cut[i] + " ) ";
				i++;
			}
			else {
				Name += cut[i];
			}
		}
		return Name;
	}
	
	//设置用户参数
	public void SetUserValue( string vName, string vGroupMes, string vMes, int vX, int vY, int vAngle, bool sf )
	{
		Name = vName;
		GroupMes = vGroupMes;
		UserName = vMes;
		SX = vX;
		SY = vY;
		
		Height = GUIset.ExpFont.Height + 4;
		ShowFollow = sf;
		//if( !ShowFollow ) {
		//	SetFollowVisible( ShowFollow );
		//}
	}
	
	//点击时设置函数名称
	public override void UserMouseClick( int mX, int mY )
	{
		if( !isEvent && Target != null ) {
			MessageBox.Show( "框架事件响应函数暂不允许自行修改" );
			return;
		}
		
		if( mX - SX < HeadWidth ) {
			
			if( G.UserFuncEXPBox == null ) {
				G.UserFuncEXPBox = new n_UserFuncEXPForm.UserFuncEXPForm();
			}
			//string Mes = G.MessageInputEXPBox.Run( this.UserName );
			string Mes = G.UserFuncEXPBox.Run( this.UserName );
			//string Mes = G.UserFuncV1EXPBox.Run( this.UserName );
			
			if( Mes != null && Mes.StartsWith( "*" ) ) {
				Mes = Mes.Remove( 0, 1 );
				isEvent = true;
				if( ShowFollow ) {
					ShowFollow = false;
					SetFollowVisible( ShowFollow );
				}
			}
			else {
				isEvent = false;
			}
			
			if( Mes != null ) {
				this.UserName = Mes;
			}
		}
		else {
			if( !isEvent ) {
				ShowFollow = !ShowFollow;
				SetFollowVisible( ShowFollow );
			}
		}
	}
	
	public override void UserMouseMove( bool isMouseOn, int mX, int mY )
	{
		
	}
	
	//组件绘制工作1
	//绘制组件的外形等,基础绘制工作
	public override void Draw1( Graphics g )
	{
		if( isNewTick != 0 ) {
			return;
		}
		
		Brush b = UserBackColor;
		Pen p = Pens.Purple;
		if( isNote ) {
			b = brNoteColor;
			p = pnNoteColor;
		}
		if( isEvent ) {
			b = Brushes.Peru;
			p = Pens.SaddleBrown;
		}
		
		//绘制竖直连线
		if( !ShowFollow ) {
			int Hei = Height * 2 / 3;
			
			/*
			if( SystemData.isBlack ) {
				g.FillEllipse( Brushes.Yellow, StartX + SX - 2 - Hei, StartY + SY + ( + Height - Hei)/2, Hei, Hei );
			}
			else {
				g.FillEllipse( Brushes.Green, StartX + SX - 2 - Hei, StartY + SY + ( + Height - Hei)/2, Hei, Hei );
			}
			*/
			if( !isEvent ) {
				g.DrawString( "点击下方指令框可展开", GUIset.ExpFont, Brushes.DarkSlateGray, SX, SY - Height - 6 );
			}
		}
		else {
			if( isMouseOn ) {
				g.DrawString( "点击下方指令框可折叠", GUIset.ExpFont, Brushes.DarkSlateGray, SX, SY - Height - 6 );
			}
		}
		
		//绘制表达式位图
		Rectangle r = new Rectangle( SX, SY, this.Width, this.Height );
		
		if( !ShowFollow ) {
			GraphicsPath gp = Shape.CreateRoundedRectanglePath( r );
			g.FillPath( b, gp );
			g.DrawPath( p, gp );
			
			if( isEvent ) {
				DrawEventLine( g );
			}
		}
		else {
			SIns.SetInsShape( BackPath, r, this.InsEnd );
			g.FillPath( b, BackPath );
			g.DrawPath( p, BackPath );
		}
		
		//g.DrawString( EventEntry, GUIset.ExpFont, Brushes.Black, StartX + X - this.Width / 2 + 4, StartY + Y - this.Height / 2 + 2 );
		
		string CRType = "";
		if( RType == "?void" ) {
			CRType = n_Language.Language.Void;
		}
		else if( RType == "?bool" ) {
			CRType = n_Language.Language.Bool;
		}
		else if( RType == "?int32" ) {
			CRType = n_Language.Language.Int32;
		}
		else if( RType == "?fix" ) {
			CRType = n_Language.Language.Fix;
		}
		else {
			CRType = "Error";
		}
		HeadWidth = (int)GUIset.mg.MeasureString( CRType, GUIset.ExpFont ).Width + 1;
		
		//绘制返回类型
		if( isNote ) {
			g.DrawString( CRType, GUIset.ExpFont, Brushes.Black, SX + 2 + OffetWidth, SY + 2 );
		}
		else {
			//这里切换设置信息提示
			Rectangle rr = new Rectangle( SX + 1 + OffetWidth, SY + 1, HeadWidth + 2, this.Height - 2 );
			GraphicsPath gp1 = Shape.CreateRoundedRectanglePath( rr );
			if( isMouseOn ) {
				g.FillPath( Brushes.Orange, gp1 );
				g.DrawPath( Pens.Green, gp1 );
				g.DrawString( n_Language.Language.Setting, GUIset.ExpFont, Brushes.Black, SX + 2 + OffetWidth, SY + 2 );
			}
			else {
				g.FillPath( Brushes.WhiteSmoke, gp1 );
				g.DrawPath( Pens.Green, gp1 );
				g.DrawString( CRType, GUIset.ExpFont, Brushes.Black, SX + 2 + OffetWidth, SY + 2 );
			}
		}
		
		//g.DrawString( FName, GUIset.ExpFont, Brushes.Black, StartX + SX + HeadWidth + 4 + OffetWidth, StartY + SY + 3 );
		
		int sx = SX + HeadWidth + 4 + OffetWidth;
		for( int i = 0; i < FNameCut.Length; ++i ) {
			if( FNameCut[i].StartsWith( "?" ) ) {
				
				string CType = FNameCut[i];
				Brush VBrush = Brushes.Red;
				if( CType == "?bool" ) {
					VBrush = Brushes.Goldenrod;
				}
				else if( CType == "?int32" ) {
					VBrush = Brushes.CornflowerBlue;
				}
				else if( CType == "?fix" ) {
					VBrush = Brushes.DarkSeaGreen;
				}
				else if( CType == "?Cstring" ) {
					VBrush = Brushes.DarkTurquoise;
				}
				else {
					//...
				}
				
				i++;
				//绘制表达式位图
				int vl = (int)GUIset.mg.MeasureString( FNameCut[i], GUIset.ExpFont ).Width + 4;
				int bc = 0;
				if( vl < 20 ) {
					bc = (20 - vl)/2;
					vl = 20;
				}
				Rectangle rv = new Rectangle( sx + 1, SY - 3, vl - 2, this.Height );
				GraphicsPath gpv = Shape.CreateRoundedRectanglePath( rv );
				
				if( isNote ) {
					g.FillPath( Brushes.Silver, gpv );
					g.DrawPath( Pens.Gray, gpv );
				}
				else {
					g.FillPath( VBrush, gpv );
					g.DrawPath( Pens.WhiteSmoke, gpv );
				}
				g.DrawString( FNameCut[i], GUIset.ExpFont, Brushes.WhiteSmoke, sx + 2 + bc, SY - 2 );
				
				//if( FNameCut[i-1] == "?bool" ) {
				//	g.DrawString( "?", GUIset.ExpFont, Brushes.WhiteSmoke, sx + 2, StartY + SY - 20 );
				//}
				//else {
				//	g.DrawString( "#", GUIset.ExpFont, Brushes.WhiteSmoke, sx + 2, StartY + SY - 20 );
				//}
				
				sx += vl + 2;
			}
			else {
				if( SystemData.isBlack ) {
					g.DrawString( FNameCut[i], GUIset.ExpFont, Brushes.Black, sx, SY + 1 );
				}
				else {
					g.DrawString( FNameCut[i], GUIset.ExpFont, Brushes.Black, sx, SY + 1 );
				}
				
				sx += (int)GUIset.mg.MeasureString( FNameCut[i], GUIset.ExpFont ).Width + 2;
			}
		}
		
		if( SimCIns != null ) {
			SimCIns.DrawCSim( g );
		}
		
		if( this.Width != sx - SX ) {
			this.Width = sx - SX;
		}
	}
	
	//绘制连接目标
	public void DrawEventLine( Graphics g )
	{
		if( isEvent && Target != null ) {
			
			float midx;
			float midy;
			
			if( GroupMes != null && !GroupVisible ) {
				n_GroupList.Group gr = G.CGPanel.mGroupList.FindGroup( GroupMes );
				midx = gr.GetMidX();
				midy = gr.GetMidY();
			}
			else {
				midx = MidX;
				midy = MidY;
			}
			
			Shape.DrawEventLine( g, G.CGPanel.mGroupList, this, Target );
		}
	}
}
}


