﻿
namespace n_IfElseIns
{
using System;
using System.Drawing;
using System.Drawing.Drawing2D;

using n_MyIns;
using n_Shape;
using n_EXP;
using n_GUIset;
using n_ElseIns;
using n_CondiEndIns;

//*****************************************************
//逻辑流程组件类
public class IfElseIns: MyIns
{
	public ElseIns ElseIns;
	public CondiEndIns InsEnd;
	GraphicsPath BackPath;
	
	//构造函数
	public IfElseIns(): base()
	{
		float w = GUIset.GetStringWidth( n_Language.Language.If );
		MyEXP = new EXP( this, (int)w + PadOffset*2, CompInsYOffset );
		
		MyEXP.ExpChanged += new EXP.deleExpChanged( ExpChanged );
		
		BackPath = new GraphicsPath();
	}
	
	//设置用户参数
	public void SetUserValue( string vName, string vGroupMes, string vMes, int vX, int vY, int vAngle )
	{
		Name = vName;
		GroupMes = vGroupMes;
		MyEXP.Set( vMes );
		
		SX = vX;
		SY = vY;
	}
	
	//刷新组件尺寸
	public void ExpChanged()
	{
		//int w = this.Width;
		this.Width = 6 + MyEXP.X + MyEXP.Width;
		this.Height = this.MyEXP.Height - CompInsYStart;
		//this.SX += (w - this.Width) / 2;
		
		if( this.PreIns != null ) {
			FormatLocation( this.PreIns );
		}
	}
	
	//切换到带有else语句模式
	public void EnableElse()
	{
		ElseIns = new ElseIns();
		
		ElseIns.SetUserValue( n_GUICommon.GUICommon.SearchName( G.CGPanel ), this.GroupMes, 0, 0, 0 );
		ElseIns.InsEnd = InsEnd;
		//ElseIns.InsStart = this;
		ElseIns.NextIns = InsEnd;
		ElseIns.PreIns = InsEnd.PreIns;
		ElseIns.InsStart = (IfElseIns)InsEnd.InsStart;
		InsEnd.PreIns.NextIns = ElseIns;
		InsEnd.PreIns = ElseIns;
		G.CGPanel.myModuleList.Add( ElseIns );
		
		//这里以前不加, 导致选中后添加else再拖动, 出现一堆异常散架指令
		if( ElseIns.InsStart.isSelect ) {
			ElseIns.isSelect = true;
		}
		
		FormatLocation( this );
	}
	
	//切换到不带有else语句模式
	public void DisableElse()
	{
		ElseIns.InsEnd.PreIns = ElseIns.PreIns;
		ElseIns.PreIns.NextIns = ElseIns.InsEnd;
		
		MyIns m = ElseIns;
		MyIns e = ElseIns.InsEnd;
		while( m != e ) {
			m.Remove();
			m = m.NextIns;
		}
		ElseIns = null;
		
		FormatLocation( this );
	}
	
	public override void UserMouseClick( int mX, int mY )
	{
		MyEXP.MouseClick( mX -  this.SX, mY - this.SY );
	}
	
	public override void UserMouseMove( bool isMouseOn, int mX, int mY )
	{
		MyEXP.MouseMove( isMouseOn, mX - this.SX, mY - this.SY );
	}
	
	//组件绘制工作1
	//绘制组件的外形等,基础绘制工作
	public override void Draw1( Graphics g )
	{
		if( isNewTick != 0 ) {
			return;
		}
		
		int LevelOffet = MyEXP.Height - (GUIset.ExpFont.Height + 4);
		
		Brush b = FlowBackColor;
		Pen p = Pens.Sienna;
		if( isNote ) {
			b = brNoteColor;
			p = pnNoteColor;
		}
		Rectangle r = new Rectangle( SX, SY + HeadRectYOffset + LevelOffet, Width, GUIset.ExpFont.Height + 4 );
		
		//绘制竖直连线
		g.FillRectangle( b, SX, SY + LineOffset + LevelOffet, 5, this.InsEnd.SY - this.SY - LevelOffet );
		
		if( ElseIns == null ) {
			SIns.SetInsShape( BackPath, r, this.InsEnd );
		}
		else {
			SIns.SetElseInsShape( BackPath, r, this.ElseIns, this.InsEnd );
		}
		
		g.FillPath( b, BackPath );
		g.DrawPath( p, BackPath );
		
		g.DrawString( n_Language.Language.If, GUIset.ExpFont, FlowForeColor, SX + PadOffset, SY + HeadStringYOffset + LevelOffet );
		
		MyEXP.Draw( g, -this.Width / 2, - this.Height / 2 );
	}
}
}


