﻿
namespace n_WhileIns
{
using System;
using System.Drawing;
using System.Drawing.Drawing2D;

using n_MyIns;
using n_Shape;
using n_EXP;
using n_GUIset;
using n_CondiEndIns;

//*****************************************************
//逻辑流程组件类
public class WhileIns: MyIns
{
	public CondiEndIns InsEnd;
	GraphicsPath BackPath;
	
	//构造函数
	public WhileIns(): base()
	{
		float w = GUIset.GetStringWidth( n_Language.Language.I_While1 );
		MyEXP = new EXP( this, (int)w + PadOffset*2, CompInsYOffset );
		
		MyEXP.ExpChanged += new EXP.deleExpChanged( ExpChanged );
		
		BackPath = new GraphicsPath();
	}
	
	//设置用户参数
	public void SetUserValue( string vName, string vGroupMes, string vMes, int vX, int vY, int vAngle )
	{
		Name = vName;
		GroupMes = vGroupMes;
		MyEXP.Set( vMes );
		
		SX = vX;
		SY = vY;
	}
	
	//刷新组件尺寸
	public void ExpChanged()
	{
		float w = GUIset.GetStringWidth( n_Language.Language.I_While2 );
		this.Width = MyEXP.X + MyEXP.Width + (int)w + PadOffset*2;
		this.Height = this.MyEXP.Height - CompInsYStart;
		//this.SX += (w - this.Width) / 2;
		
		if( this.PreIns != null ) {
			FormatLocation( this.PreIns );
		}
	}
	
	public override void UserMouseClick( int mX, int mY )
	{
		MyEXP.MouseClick( mX -  this.SX, mY - this.SY );
	}
	
	public override void UserMouseMove( bool isMouseOn, int mX, int mY )
	{
		MyEXP.MouseMove( isMouseOn, mX - this.SX, mY - this.SY );
	}
	
	//组件绘制工作1
	//绘制组件的外形等,基础绘制工作
	public override void Draw1( Graphics g )
	{
		if( isNewTick != 0 ) {
			return;
		}
		
		int LevelOffet = MyEXP.Height - (GUIset.ExpFont.Height + 4);
		
		Brush b = FlowBackColor;
		Pen p = Pens.Sienna;
		if( isNote ) {
			b = brNoteColor;
			p = pnNoteColor;
		}
		Rectangle r = new Rectangle( SX, SY + HeadRectYOffset + LevelOffet, this.Width, GUIset.ExpFont.Height + 4 );
		
		SIns.SetInsShape( BackPath, r, this.InsEnd );
		g.FillPath( b, BackPath );
		g.DrawPath( p, BackPath );
		g.DrawString( n_Language.Language.I_While1, GUIset.ExpFont, FlowForeColor, SX + PadOffset, SY + HeadStringYOffset + LevelOffet );
		g.DrawString( n_Language.Language.I_While2, GUIset.ExpFont, FlowForeColor, SX + MyEXP.X + MyEXP.Width + PadOffset, SY + HeadStringYOffset + LevelOffet );
		
		MyEXP.Draw( g, - this.Width / 2, - this.Height / 2 );
	}
}
}


