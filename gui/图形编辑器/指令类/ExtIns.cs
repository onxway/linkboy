﻿
namespace n_ExtIns
{
using System;
using System.Drawing;
using System.Drawing.Drawing2D;

using n_MyIns;
using n_Shape;
using n_EXP;
using n_GUIset;
using n_CondiEndIns;

//*****************************************************
//逻辑流程组件类
public class ExtIns: MyIns
{
	public CondiEndIns InsEnd;
	
	public bool ShunXu;
	
	//构造函数
	public ExtIns(): base()
	{
		float w = GUIset.GetStringWidth( n_Language.Language.I_SHunXu );
		MyEXP = new EXP( this, (int)w + PadOffset*2, CompInsYOffset );
		
		MyEXP.ExpChanged += new EXP.deleExpChanged( ExpChanged );
		
		ShunXu = true;
	}
	
	//设置用户参数
	public void SetUserValue( string vName, string vGroupMes, string vMes, int vX, int vY, int vAngle )
	{
		Name = vName;
		GroupMes = vGroupMes;
		MyEXP.Set( vMes );
		
		SX = vX;
		SY = vY;
	}
	
	//刷新组件尺寸
	public void ExpChanged()
	{
		//int w = this.Width;
		this.Width = 35 + MyEXP.X + MyEXP.Width;
		this.Height = this.MyEXP.Height - CompInsYStart;
		//this.SX += (w - this.Width) / 2;
		
		if( this.PreIns != null ) {
			FormatLocation( this.PreIns );
		}
	}
	
	public override void UserMouseClick( int mX, int mY )
	{
		bool mo = MyEXP.MouseClick( mX -  this.SX, mY - this.SY );
		if( !mo ) {
			
			System.Windows.Forms.DialogResult dr = System.Windows.Forms.MessageBox.Show( n_Language.Language.I_IterSetMes1, n_Language.Language.I_IterSetMes2, System.Windows.Forms.MessageBoxButtons.YesNoCancel );
			if( dr == System.Windows.Forms.DialogResult.Yes ) {
				ShunXu = true;
			}
			else if( dr == System.Windows.Forms.DialogResult.No ) {
				ShunXu = false;
			}
			else {
				//...
			}
		}
	}
	
	public override void UserMouseMove( bool isMouseOn, int mX, int mY )
	{
		MyEXP.MouseMove( isMouseOn, mX - this.SX, mY - this.SY );
	}
	
	//组件绘制工作1
	//绘制组件的外形等,基础绘制工作
	public override void Draw1( Graphics g )
	{
		if( isNewTick != 0 ) {
			return;
		}
		
		int LevelOffet = MyEXP.Height - (GUIset.ExpFont.Height + 4);
		
		Brush b = FlowBackColor;
		if( isNote ) {
			b = brNoteColor;
		}
		
		//绘制竖直连线
		g.FillRectangle( b, SX, SY + LineOffset + LevelOffet, 5, this.InsEnd.SY - this.SY );
		
		//绘制组件名称
		Rectangle r = new Rectangle( SX, SY + HeadRectYOffset + LevelOffet, Width, GUIset.ExpFont.Height + 4 );
		//g.DrawRectangle( Pens.White, r );
		GraphicsPath gp = Shape.CreateRoundedRectanglePath( r );
		g.FillPath( b, gp );
		//g.DrawPath( Pens.Orange, gp );
		if( ShunXu ) {
			g.DrawString( n_Language.Language.I_SHunXu, GUIset.ExpFont, FlowForeColor, SX + PadOffset, SY + HeadStringYOffset + LevelOffet );
		}
		else {
			g.DrawString( n_Language.Language.I_FanXu, GUIset.ExpFont, FlowForeColor, SX + PadOffset, SY + HeadStringYOffset + LevelOffet );
		}
		
		g.DrawString( n_Language.Language.I_List, GUIset.ExpFont, FlowForeColor, SX + MyEXP.X + MyEXP.Width + 3, SY + HeadStringYOffset + LevelOffet );
		
		MyEXP.Draw( g, - this.Width / 2, - this.Height / 2 );
	}
}
}


