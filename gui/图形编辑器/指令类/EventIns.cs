﻿
namespace n_EventIns
{
using System;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Windows.Forms;

using n_MyIns;
using n_GUIcoder;
using n_MyObject;
using n_UserModule;
using n_GUIset;
using n_CondiEndIns;
using n_Shape;
using n_MyFileObject;
using n_MainSystemData;

//*****************************************************
//逻辑流程组件类
public class EventIns: MyIns
{
	const int OffetWidth = 5;
	
	string vvEventEntry;
	public string EventEntry {
		get { return vvEventEntry; }
		set {
			vvEventEntry = value;
			
			/*
			string[] c = vvEventEntry.Split( '_' );
			EventSource = c[ 0 ];
			EventType = c[ 1 ];
			*/
			
			int LastSp = vvEventEntry.LastIndexOf( "_" );
			EventSource = vvEventEntry.Remove( LastSp );
			EventType = vvEventEntry.Remove( 0, LastSp + 1 );
			
			HeadWidth = (int)GUIset.mg.MeasureString( EventSource, GUIset.ExpFont ).Width;
			Width = (int)GUIset.mg.MeasureString( vvEventEntry, GUIset.ExpFont ).Width + 8 + OffetWidth;
		}
	}
	
	int HeadWidth;
	
	//注意这两个是只读的
	public string EventSource;
	public string EventType;
	public MyFileObject owner;
	
	public int GoIndex;
	
	public bool isDeleted;
	
	public CondiEndIns InsEnd;
	GraphicsPath BackPath;
	
	//构造函数
	public EventIns(): base()
	{
		this.Width = 200;
		this.Height = 35;
		
		isDeleted = false;
		GoIndex = -1;
		
		BackPath = new GraphicsPath();
	}
	
	//设置用户参数
	public void SetUserValue( string vName, string vGroupMes, string vMes, int vX, int vY, int vAngle, bool sf )
	{
		Name = vName;
		GroupMes = vGroupMes;
		EventEntry = vMes;
		SX = vX;
		SY = vY;
		
		Height = GUIset.ExpFont.Height + 4;
		ExYHPadding = Height / 2;
		ShowFollow = sf;
		
		//if( !ShowFollow ) {
		//	SetFollowVisible( ShowFollow );
		//}
		
		if( G.isCruxEXE ) {
			SetFollowVisible( ShowFollow );
		}
	}
	
	public override void UserMouseClick( int mX, int mY )
	{
		if( G.isCruxEXE ) {
			return;
		}
		ShowFollow = !ShowFollow;
		SetFollowVisible( ShowFollow );
	}
	
	public override void UserMouseMove( bool isMouseOn, int mX, int mY )
	{
		
	}
	
	//组件绘制工作1
	//绘制组件的外形等,基础绘制工作
	public override void Draw1( Graphics g )
	{
		if( isNewTick != 0 ) {
			return;
		}
		//绘制竖直连线
		if( !ShowFollow ) {
			int Hei = Height * 2 / 3;
			
			/*
			if( SystemData.isBlack ) {
				g.FillEllipse( Brushes.Yellow, StartX + SX - 2 - Hei, StartY + SY + ( + Height - Hei)/2, Hei, Hei );
			}
			else {
				g.FillEllipse( Brushes.Green, StartX + SX - 2 - Hei, StartY + SY + ( + Height - Hei)/2, Hei, Hei );
			}
			*/
			
			if( G.isCruxEXE ) {
				//g.DrawString( "点击下方事件框可展开", GUIset.ExpFont, Brushes.DarkSlateGray, SX, SY - Height*3/2 - 6 );
			}
			else {
				g.DrawString( "点击下方事件框可展开", GUIset.ExpFont, Brushes.DarkSlateGray, SX, SY - Height*3/2 - 6 );
			}
		}
		else {
			if( isMouseOn ) {
				g.DrawString( "点击下方事件框可折叠", GUIset.ExpFont, Brushes.DarkSlateGray, SX, SY - Height*3/2 - 6 );
			}
		}
		
		//绘制表达式位图
		Rectangle r = new Rectangle( SX, SY, this.Width, this.Height );
		int HWidth = HeadWidth + OffetWidth*2 + 1;
		
		SIns.SetEventShape( BackPath, r, HWidth, this.InsEnd, !ShowFollow );
		
		if( owner == null || isNote ) {
			g.FillPath( brNoteColor, BackPath );
			g.DrawPath( pnNoteColor, BackPath );
		}
		else {
			g.FillPath( Brushes.SandyBrown, BackPath );
			g.DrawPath( Pens.SaddleBrown, BackPath );
			
			//g.FillPath( FlowBackColor, BackPath );
			//g.DrawPath( Pens.Sienna, BackPath );
		}
		int hx = SX + HeadWidth + OffetWidth + 2;
		g.DrawLine( Pens.WhiteSmoke, hx, SY + 2, hx, SY + Height - 2 );
		
		/*
		//绘制顶端效果线段
		g.FillRectangle( Brushes.PeachPuff, StartX + SX + 1 + OffetWidth + 3, StartY + SY + 3 - eh, HeadWidth + 2 - 6, 2 );
		*/
		
		g.DrawString( EventSource, GUIset.ExpFont, EventForeColor, SX + OffetWidth, SY + 2 );
		g.DrawString( EventType, GUIset.ExpFont, EventForeColor, SX + HeadWidth + 4 + OffetWidth, SY + 2 );
		
		if( SimCIns != null ) {
			SimCIns.DrawCSim( g );
		}
		
		//g.DrawString( GoIndex.ToString(), GUIset.ExpFont, EventForeColor, SX + OffetWidth, SY - 42 );
	}
}
}


