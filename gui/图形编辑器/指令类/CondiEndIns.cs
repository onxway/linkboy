﻿
namespace n_CondiEndIns
{
using System;
using System.Drawing;
using System.Drawing.Drawing2D;

using n_MyIns;
using n_Shape;
using n_GUIset;

//*****************************************************
//逻辑流程组件类
public class CondiEndIns: MyIns
{
	const int W = 49;
	//const int W = 70;
	
	public bool CanLinked;
	
	public MyIns InsStart;
	
	//构造函数
	public CondiEndIns(): base()
	{
		this.Width = W;
		this.Height = 20;//35;
		CanLinked = true;
	}
	
	//设置用户参数
	public void SetUserValue( string vName, string vGroupMes, int vX, int vY, int vAngle )
	{
		Name = vName;
		GroupMes = vGroupMes;
		SX = vX;
		SY = vY;
		
		Height = GUIset.ExpFont.Height + 4;
		//this.Height = 10;
	}
	
	public override void UserMouseClick( int mX, int mY )
	{
		if( InsStart is n_IfElseIns.IfElseIns ) {
			n_IfElseIns.IfElseIns t = (n_IfElseIns.IfElseIns)InsStart;
			if( t.ElseIns == null ) {
				t.EnableElse();
			}
		}
	}
	
	public override void UserMouseMove( bool isMouseOn, int mX, int mY )
	{
		
	}
	
	//组件绘制工作1
	//绘制组件的外形等,基础绘制工作
	public override void Draw1( Graphics g )
	{
		if( isNewTick != 0 ) {
			return;
		}
		//绘制组件名称
		Rectangle r = new Rectangle( SX, SY, W, this.Height );
		GraphicsPath gp = Shape.CreateRoundedRectanglePath( r );
		
		if( CanLinked ) {
			if( isMouseOn && InsStart is n_IfElseIns.IfElseIns && ((n_IfElseIns.IfElseIns)InsStart).ElseIns == null ) {
				g.DrawString( n_Language.Language.AddElse, GUIset.ExpFont, FlowForeColor, SX + 3, SY + HeadStringYOffset );
			}
			else if( InsStart is n_IfElseIns.IfElseIns ) {
				//绘制继续箭头
				g.DrawString( "↓", GUIset.ExpFont, Brushes.WhiteSmoke, SX + 3, SY + HeadStringYOffset );
			}
			else {
				//绘制循环箭头
				g.DrawString( "↖", GUIset.ExpFont, Brushes.WhiteSmoke, SX + 3, SY + HeadStringYOffset );
			}
		}
		else {
			g.DrawString( n_Language.Language.End, GUIset.ExpFont, EventForeColor, SX + 3, SY + HeadStringYOffset );
		}
	}
}
}


