﻿
namespace n_InsPanel
{
using System;
using System.Drawing;
using System.Drawing.Drawing2D;

using n_CondiEndIns;
using n_ElseIns;
using n_ForeverIns;
using n_FuncIns;
using n_GUICommon;
using n_IfElseIns;
using n_WhileIns;
using n_LoopIns;
using n_ExtIns;
using n_MyIns;
using n_Shape;
using n_UserFunctionIns;
using n_WaitIns;
using n_GUIset;

//逻辑模块面板类
public class InsPanel
{
	public delegate void D_HideEvent();
	public D_HideEvent HideEvent;
	
	public Color BackColor;
	
	SolidBrush EageBrush;
	
	bool isMouseOn;
	Brush BackBrush;
	
	MyIns[] C_MyInsList;
	int C_MyInsListLength;
	int SysInsLength;
	int SysY;
	int CurrentY;
	
	bool v_ShowLogicPanel;
	public bool ShowLogicPanel {
		set {
			v_ShowLogicPanel = value;
			if( HideEvent != null ) {
				HideEvent();
			}
		}
		get {
			return v_ShowLogicPanel;
		}
	}
	
	public int SX;
	public int SY;
	
	public int Width;
	public int Height;
	
	SolidBrush alphab;
	SolidBrush onb;
	
	MyIns MouseOnIns;
	
	static int StX = 10;//(210 - 130) / 2;
	const int InsPadding0 = 5;
	const int InsPadding1 = 15;
	
	const int MPad = 5;
	
	bool isMouseDown;
	int LastX;
	int lastY;
	
	bool NeedShow;
	
	bool FirstAddFuncIns = true;
	bool FirstAddIfcIns = true;
	bool FirstAddExtIns = true;
	bool FirstAddWhileIns = true;
	
	FuncIns funci;
	
	//构造函数
	public InsPanel( int x, int y )
	{
		GUIset.ExpFont = GUIset.UIFont;
		
		StX = n_GUIset.GUIset.GetPix(StX);
		
		//BackBrush = new SolidBrush( Color.FromArgb( 100, GUIset.GetRedColor( 150 ) ) );
		//alphab = new SolidBrush( Color.FromArgb( 150, GUIset.GetRedColor( 150 ) ) );
		//onb = new SolidBrush( Color.FromArgb( 100, GUIset.GetRedColor( 150 ) ) );
		
		//BackColor = n_GUIset.GUIset.GUIbackColor;
		BackColor = Color.FromArgb( 250, 245, 230 );
		
		BackColor = Color.FromArgb( 238, 242, 246 );
		
		if( G.LightUI ) {
			BackColor = Color.FromArgb( 255, 255, 255 );
		}
		
		BackBrush = new SolidBrush( BackColor );
		alphab = new SolidBrush( Color.FromArgb( 120, Color.LightGray ) );
		onb = new SolidBrush( Color.FromArgb( 120, Color.LightGray ) );
		
		EageBrush = new SolidBrush( n_Head.Head.InsButtonActiveColor );
		
		C_MyInsList = new MyIns[ 100 ];
		C_MyInsListLength = 0;
		
		ShowLogicPanel = false;
		
		isMouseDown = false;
		isMouseOn = false;
		
		int CurrentY = n_Head.Head.HeadHeight + InsPadding1;
		
		NeedShow = false;
		int PerHeight = n_GUIset.GUIset.ExpFont.Height + 4;
		
		//添加如果否则指令
		IfElseIns ifelsei = NewIfElseIns( "", StX, CurrentY );
		ifelsei.UserMouseMove( true, StX + ifelsei.Width / 4 * 1, CurrentY + ifelsei.Height / 2 );
		C_MyInsList[ C_MyInsListLength ] = ifelsei;
		++C_MyInsListLength;
		CurrentY += PerHeight + InsPadding0;
		
		C_MyInsList[ C_MyInsListLength ] = ifelsei.InsEnd;
		++C_MyInsListLength;
		CurrentY += PerHeight + InsPadding1;
		
		//添加反复执行指令
		ForeverIns foreveri = NewForeverIns( "", StX, CurrentY );
		foreveri.UserMouseMove( true, StX + foreveri.Width / 4 * 1, CurrentY + foreveri.Height / 2 );
		C_MyInsList[ C_MyInsListLength ] = foreveri;
		++C_MyInsListLength;
		CurrentY += PerHeight + InsPadding0;
		
		C_MyInsList[ C_MyInsListLength ] = foreveri.InsEnd;
		++C_MyInsListLength;
		CurrentY += PerHeight + InsPadding1;
		
		
		//添加反复次数指令
		LoopIns loopi = NewLoopIns( "", StX, CurrentY );
		loopi.UserMouseMove( true, StX + loopi.Width / 4 * 1, CurrentY + loopi.Height / 2 );
		C_MyInsList[ C_MyInsListLength ] = loopi;
		++C_MyInsListLength;
		CurrentY += PerHeight + InsPadding0;
		
		C_MyInsList[ C_MyInsListLength ] = loopi.InsEnd;
		++C_MyInsListLength;
		CurrentY += PerHeight + InsPadding1;
		
		//添加条件循环指令
		WhileIns ifi = NewWhileIns( "", StX, CurrentY );
		ifi.UserMouseMove( true, StX + ifi.Width / 4 * 1, CurrentY + ifi.Height / 2 );
		C_MyInsList[ C_MyInsListLength ] = ifi;
		++C_MyInsListLength;
		CurrentY += PerHeight + InsPadding0;
		
		C_MyInsList[ C_MyInsListLength ] = ifi.InsEnd;
		++C_MyInsListLength;
		CurrentY += PerHeight + InsPadding1;
		
		/*
		//添加遍历指令
		ExtIns extins = NewExtIns( "", StX, CurrentY );
		extins.UserMouseMove( true, StX + extins.Width / 4 * 1, CurrentY + extins.Height / 2 );
		C_MyInsList[ C_MyInsListLength ] = extins;
		++C_MyInsListLength;
		CurrentY += PerHeight + InsPadding0;
		
		C_MyInsList[ C_MyInsListLength ] = extins.InsEnd;
		++C_MyInsListLength;
		CurrentY += PerHeight + InsPadding1;
		*/
		
		//添加等待指令
		WaitIns waiti = NewWaitIns( "", StX, CurrentY );
		waiti.UserMouseMove( true, StX + waiti.Width / 4 * 1, CurrentY + waiti.Height / 2 );
		C_MyInsList[ C_MyInsListLength ] = waiti;
		++C_MyInsListLength;
		CurrentY += PerHeight + InsPadding1;
		
		//添加系统指令
		FuncIns operi6 = NewFuncIns( "", "?void @FLOW " + n_Language.Language.I_Return, StX, CurrentY );
		operi6.UserMouseMove( true, StX + operi6.Width / 4 * 1, CurrentY + operi6.Height / 2 );
		C_MyInsList[ C_MyInsListLength ] = operi6;
		++C_MyInsListLength;
		CurrentY += operi6.Height + InsPadding1;
		
		//添加系统指令
		FuncIns operi7 = NewFuncIns( "", "?void @FLOW " + n_Language.Language.I_Return + " ( ?int32 )", StX, CurrentY );
		operi7.UserMouseMove( true, StX + operi7.Width / 4 * 1, CurrentY + operi7.Height / 2 );
		C_MyInsList[ C_MyInsListLength ] = operi7;
		++C_MyInsListLength;
		CurrentY += operi7.Height + InsPadding1;
		
		//添加系统指令
		FuncIns operi7fix = NewFuncIns( "", "?void @FLOW " + n_Language.Language.I_Return + " ( ?fix )", StX, CurrentY );
		operi7fix.UserMouseMove( true, StX + operi7fix.Width / 4 * 1, CurrentY + operi7fix.Height / 2 );
		C_MyInsList[ C_MyInsListLength ] = operi7fix;
		++C_MyInsListLength;
		CurrentY += operi7fix.Height + InsPadding1;
		
		//添加系统指令
		FuncIns operi8 = NewFuncIns( "", "?void @FLOW " + n_Language.Language.I_Return + " ( ?bool )", StX, CurrentY );
		operi8.UserMouseMove( true, StX + operi8.Width / 4 * 1, CurrentY + operi8.Height / 2 );
		C_MyInsList[ C_MyInsListLength ] = operi8;
		++C_MyInsListLength;
		CurrentY += operi8.Height + InsPadding1;
		
		//添加系统指令
		FuncIns operi9 = NewFuncIns( "", "?void @FLOW " + n_Language.Language.I_Break, StX, CurrentY );
		operi9.UserMouseMove( true, StX + operi9.Width / 4 * 1, CurrentY + operi9.Height / 2 );
		C_MyInsList[ C_MyInsListLength ] = operi9;
		++C_MyInsListLength;
		CurrentY += operi9.Height + InsPadding1;
		
		//添加系统指令
		FuncIns operi10 = NewFuncIns( "", "?void @FLOW " + n_Language.Language.I_Continue, StX, CurrentY );
		operi10.UserMouseMove( true, StX + operi10.Width / 4 * 1, CurrentY + operi10.Height / 2 );
		C_MyInsList[ C_MyInsListLength ] = operi10;
		++C_MyInsListLength;
		CurrentY += operi10.Height + InsPadding1;
		
		//添加用户指令
		UserFunctionIns useri = NewUserFunctionIns( "", StX, CurrentY );
		useri.UserName = "?void " + n_Language.Language.UserDefine;
		
		C_MyInsList[ C_MyInsListLength ] = useri;
		++C_MyInsListLength;
		CurrentY += PerHeight + InsPadding0;
		
		C_MyInsList[ C_MyInsListLength ] = useri.InsEnd;
		++C_MyInsListLength;
		CurrentY += PerHeight + InsPadding1;
		
		//添加函数指令
		funci = NewFuncIns( "", n_Language.Language.ModuleFunc, StX, CurrentY );
		C_MyInsList[ C_MyInsListLength ] = funci;
		++C_MyInsListLength;
		CurrentY += funci.Height + InsPadding1;
		
		
		SysInsLength = C_MyInsListLength;
		SysY = CurrentY;
		
		SX = x;
		SY = y;
		Width = n_ModuleLibPanel.ModuleTree.CWidth;
		Height = CurrentY;
		
		GUIset.ExpFont = GUIset.ExpFontBack;
	}
	
	//组件绘制工作
	public void Draw( Graphics g )
	{
		if( !ShowLogicPanel ) {
			return;
		}
		//绘制背景边框
		Brush b;
		if( isMouseOn ) {
			b = BackBrush;
		}
		else {
			b = BackBrush;
		}
		
		Rectangle r = new Rectangle( n_Head.Head.ShowX, 0, Width, Height );
		//GraphicsPath path = Shape.CreateRoundedRectanglePath( r );
		g.FillRectangle( b, r );
		
		//g.FillRectangle( EageBrush, n_Head.Head.ShowX, 105 - 13, 1, Height );
		//g.FillRectangle( EageBrush, n_Head.Head.ShowX + Width - 1, 105 - 13, 1, Height );
		//g.FillRectangle( EageBrush, n_Head.Head.ShowX, 105 - 13, Width - 1, 1 );
		
		GraphicsState gs = g.Save();
		g.TranslateTransform( n_Head.Head.ShowX, SY );
		
		//绘制鼠标悬停指令
		if( isMouseOn ) {
			if( MouseOnIns != null ) {
				
				if( MouseOnIns is CondiEndIns ) {
					MouseOnIns = MouseOnIns.PreIns;
				}
				if( MouseOnIns is ElseIns ) {
					MouseOnIns = MouseOnIns.PreIns;
				}
				
				if( MouseOnIns.NextIns != null ) {
					int hh = (MouseOnIns.NextIns.SY - MouseOnIns.SY) + MouseOnIns.NextIns.Height + MPad*2;
					g.FillRectangle( Brushes.WhiteSmoke, n_Head.Head.ShowX + 1, MouseOnIns.SY - MPad, Width - 2, hh );
					g.DrawRectangle( Pens.Gainsboro, n_Head.Head.ShowX + 1, MouseOnIns.SY - MPad, Width - 2, hh );
				}
				else {
					g.FillRectangle( Brushes.WhiteSmoke, n_Head.Head.ShowX + 1, MouseOnIns.SY - MPad, Width - 2, MouseOnIns.Height + MPad*2 );
					g.DrawRectangle( Pens.Gainsboro, n_Head.Head.ShowX + 1, MouseOnIns.SY - MPad, Width - 2, MouseOnIns.Height + MPad*2 );
				}
			}
		}
		else {
			//g.FillPath( alphab, path );
		}
		
		GUIset.ExpFont = GUIset.UIFont;
		
		//绘制按钮列表
		for( int i = 0; i < C_MyInsListLength; ++i ) {
			C_MyInsList[ i ].Draw1( g );
		}
		
		GUIset.ExpFont = GUIset.ExpFontBack;
		
		g.Restore( gs );
	}
	
	//鼠标左键按下事件
	public bool LeftMouseDown( int mX, int mY )
	{
		if( !ShowLogicPanel ) {
			return false;
		}
		if( isMouseOn ) {
			
			isMouseDown = true;
			LastX = mX;
			lastY = mY;
			
			/*
			//判断鼠标点击到了哪个按钮上
			MyIns mo = null;
			for( int i = 0; i < C_MyInsListLength; ++i ) {
				if( C_MyInsList[ i ].MouseIsInside( mX - SX, mY - SY ) ) {
					mo = C_MyInsList[ i ];
					break;
				}
			}
			*/
			
			MyIns mo = MouseOnIns;
			
			if( mo != null ) {
				CloneNewIns( mo );
				
				//这里是特例, 因为不希望触发隐藏事件
				//if( n_Head.Head.AutoHide ) {
					v_ShowLogicPanel = false;
					NeedShow = true;
				//}
				isMouseDown = false;
			}
			
			return true;
		}
		return false;
	}
	
	//鼠标左键松开事件
	public void LeftMouseUp( int mX, int mY )
	{
		isMouseDown = false;
		
//		//轮询按钮列表
//		for( int i = 0; i < C_MyInsListLength; ++i ) {
//			C_MyInsList[ i ].MouseUp( mX - midX, mY - midY );
//		}
	}
	
	//鼠标移动事件,当鼠标在组件上时返回1,组件被拖动时返回2
	public bool MouseMove( int mX, int mY )
	{
		MouseOnIns = null;
		
		//鼠标离开面板后应该重新显示出来
		if( NeedShow && mX > this.Width && !n_Head.Head.AutoHide ) {
			v_ShowLogicPanel = true;
			NeedShow = false;
		}
		if( !n_Head.Head.HeadShow ) {
			return false;
		}
		if( !ShowLogicPanel ) {
			return false;
		}
		if( isMouseDown ) {
			this.SY += (mY - lastY );
			LastX = mX;
			lastY = mY;
		}
		
		//isMouseOn = isMouseInSide( mX, mY );
		isMouseOn = mX < this.Width;
		
		if( isMouseOn ) {
			//判断鼠标点击到了哪个按钮上
			
			for( int i = 0; i < C_MyInsListLength; ++i ) {
				
				if( mY - SY > C_MyInsList[ i ].SY - MPad && mY - SY < C_MyInsList[ i ].SY + C_MyInsList[ i ].Height + MPad ) {
					MouseOnIns = C_MyInsList[ i ];
				}
				
				/*
				if( C_MyInsList[ i ].MouseIsInside( mX - SX, mY - SY ) ) {
					MouseOnIns = C_MyInsList[ i ];
					break;
				}
				*/
			}
		}
		else {
			if( n_Head.Head.AutoHide ) {
				ShowLogicPanel = false;
			}
		}
		
//		//控件按钮鼠标移动事件, 轮询按钮列表
//		for( int i = 0; i < C_MyInsListLength; ++i ) {
//			C_MyInsList[ i ].MouseMove( mX - midX, mY - midY );
//		}
		return isMouseOn;
	}
	
	//滚动条移动
	public bool Scroll( int mX, int mY, int Delta )
	{
		if( !n_Head.Head.HeadShow ) {
			return false;
		}
		if( !ShowLogicPanel ) {
			return false;
		}
		if( mX > Width ) {
			return false;
		}
		SY += Delta;
		
		/*
		if( SY < BottomY ) {
			SY = BottomY;
		}
		if( SY > TopY ) {
			SY = TopY;
		}
		*/
		MouseMove( mX, mY );
		return true;
	}
	
	//刷新指令列表
	public void ClearModuleIns()
	{
		C_MyInsListLength = SysInsLength;
		CurrentY = SysY;
	}
	
	//刷新指令列表
	public void AddModuleIns( string Name )
	{
		//添加函数指令
		funci = NewFuncIns( "", Name, StX, CurrentY );
		C_MyInsList[ C_MyInsListLength ] = funci;
		++C_MyInsListLength;
		CurrentY += funci.Height + InsPadding1;
	}
	
	//判断指定点是否在内部
	bool isMouseInSide( int mX, int mY )
	{
		if( mX >= SX && mX < SX + Width && mY >= SY && mY < SY + Height  ) {
			return true;
		}
		return false;
	}
	
	//=====================================================
	//指令处理类
	
	//复制一个指令并添加到用户界面中
	void CloneNewIns( MyIns mi )
	{
		if( mi is CondiEndIns ) {
			mi = mi.PreIns;
		}
		if( mi is ElseIns ) {
			mi = mi.PreIns;
		}
		
		if( mi is FuncIns ) {
			FuncIns ui;
			
			ui = NewFuncIns( GUICommon.SearchName( G.CGPanel ), ((FuncIns)mi).Mes, 0, 0 );
			
			ui.isNewTick = n_MyObject.MyObject.MaxNewTick;
			
			if( mi == funci && FirstAddFuncIns ) {
				FirstAddFuncIns = false;
				string InsMes = n_Language.Language.ModuleFuncMes;
				G.CGPanel.AddMesPanel( InsMes, 1, ui );
			}
			
			G.CGPanel.myModuleList.Add( ui );
			
			n_NL.NL.CNode.USER_funcins( ui.Mes );
			
			return;
		}
		if( mi is WaitIns ) {
			WaitIns ui = NewWaitIns( GUICommon.SearchName( G.CGPanel ), 0, 0 );
			ui.isNewTick = n_MyObject.MyObject.MaxNewTick;
			G.CGPanel.myModuleList.Add( ui );
			
			n_NL.NL.CNode.fLOW_wait++;
			return;
		}
		if( mi is IfElseIns ) {
			IfElseIns ui = NewIfElseIns( GUICommon.SearchName( G.CGPanel ), 0, 0 );
			ui.isNewTick = n_MyObject.MyObject.MaxNewTick;
			G.CGPanel.myModuleList.Add( ui );
			//ui.ElseIns.Name = GUICommon.SearchName( G.CGPanel );
			//ui.ElseIns.isNew = true;
			//G.CGPanel.myModuleList.Add( ui.ElseIns );
			ui.InsEnd.Name = GUICommon.SearchName( G.CGPanel );
			ui.InsEnd.isNewTick = n_MyObject.MyObject.MaxNewTick;
			
			if( FirstAddIfcIns ) {
				FirstAddIfcIns = false;
				string InsMes = n_Language.Language.IfMes;
				G.CGPanel.AddMesPanel( InsMes, 1, ui );
			}
			
			G.CGPanel.myModuleList.Add( ui.InsEnd );
			
			n_NL.NL.CNode.fLOW_if++;
			return;
		}
		if( mi is ExtIns ) {
			ExtIns ui = NewExtIns( GUICommon.SearchName( G.CGPanel ), 0, 0 );
			ui.isNewTick = n_MyObject.MyObject.MaxNewTick;
			G.CGPanel.myModuleList.Add( ui );
			//ui.ElseIns.Name = GUICommon.SearchName( G.CGPanel );
			//ui.ElseIns.isNew = true;
			//G.CGPanel.myModuleList.Add( ui.ElseIns );
			ui.InsEnd.Name = GUICommon.SearchName( G.CGPanel );
			ui.InsEnd.isNewTick = n_MyObject.MyObject.MaxNewTick;
			
			if( FirstAddExtIns ) {
				FirstAddExtIns = false;
				string InsMes = n_Language.Language.I_IterMes;
				G.CGPanel.AddMesPanel( InsMes, 1, ui );
			}
			
			G.CGPanel.myModuleList.Add( ui.InsEnd );
			return;
		}
		if( mi is ForeverIns ) {
			ForeverIns ui = NewForeverIns( GUICommon.SearchName( G.CGPanel ), 0, 0 );
			ui.isNewTick = n_MyObject.MyObject.MaxNewTick;
			G.CGPanel.myModuleList.Add( ui );
			ui.InsEnd.Name = GUICommon.SearchName( G.CGPanel );
			ui.InsEnd.isNewTick = n_MyObject.MyObject.MaxNewTick;
			
			
			//不再提示
			//string InsMes = n_Language.Language.ForeverMes;
			//G.CGPanel.AddMesPanel( InsMes, 3, ui );
			
			
			G.CGPanel.myModuleList.Add( ui.InsEnd );
			
			n_NL.NL.CNode.fLOW_forever++;
			return;
		}
		if( mi is LoopIns ) {
			LoopIns ui = NewLoopIns( GUICommon.SearchName( G.CGPanel ), 0, 0 );
			ui.isNewTick = n_MyObject.MyObject.MaxNewTick;
			G.CGPanel.myModuleList.Add( ui );
			ui.InsEnd.Name = GUICommon.SearchName( G.CGPanel );
			ui.InsEnd.isNewTick = n_MyObject.MyObject.MaxNewTick;
			G.CGPanel.myModuleList.Add( ui.InsEnd );
			
			n_NL.NL.CNode.fLOW_loop++;
			return;
		}
		if( mi is WhileIns ) {
			WhileIns ui = NewWhileIns( GUICommon.SearchName( G.CGPanel ), 0, 0 );
			ui.isNewTick = n_MyObject.MyObject.MaxNewTick;
			G.CGPanel.myModuleList.Add( ui );
			ui.InsEnd.Name = GUICommon.SearchName( G.CGPanel );
			ui.InsEnd.isNewTick = n_MyObject.MyObject.MaxNewTick;
			
			if( FirstAddWhileIns ) {
				FirstAddWhileIns = false;
				string InsMes = n_Language.Language.I_WhileMes;
				G.CGPanel.AddMesPanel( InsMes, 1, ui );
			}
			
			G.CGPanel.myModuleList.Add( ui.InsEnd );
			
			n_NL.NL.CNode.fLOW_while++;
			return;
		}
		if( mi is UserFunctionIns ) {
			UserFunctionIns ui = NewUserFunctionIns( GUICommon.SearchName( G.CGPanel ), 0, 0 );
			ui.isNewTick = n_MyObject.MyObject.MaxNewTick;
			G.CGPanel.myModuleList.Add( ui );
			ui.InsEnd.Name = GUICommon.SearchName( G.CGPanel );
			ui.InsEnd.isNewTick = n_MyObject.MyObject.MaxNewTick;
			G.CGPanel.myModuleList.Add( ui.InsEnd );
			return;
		}
		n_Debug.Warning.BUG( "未知的指令类型: " + mi.GetType() );
	}
	
	//创建用户功能指令
	public UserFunctionIns NewUserFunctionIns( string Name, int x, int y )
	{
		UserFunctionIns m = new UserFunctionIns();
		
		m.SetUserValue( Name, null, n_Language.Language.FlashTimes, x, y, 0, true );
		
		CondiEndIns ee = new n_CondiEndIns.CondiEndIns();
		ee.SetUserValue( Name, null, x, y + m.Height + InsPadding0, 0 );
		ee.CanLinked = false;
		ee.InsStart = m;
		
		m.InsEnd = ee;
		m.NextIns = ee;
		ee.PreIns = m;
		
		return m;
	}
	
	//创建功能指令
	FuncIns NewFuncIns( string Name, string Mes, int x, int y )
	{
		n_FuncIns.FuncIns funci = new n_FuncIns.FuncIns();
		funci.SetUserValue( Name, null, Mes, x, y, 0 );
		return funci;
	}
	
	//创建等待指令
	WaitIns NewWaitIns( string Name, int x, int y )
	{
		n_WaitIns.WaitIns waiti = new n_WaitIns.WaitIns();
		waiti.SetUserValue( Name, null, "?bool", x, y, 0 );
		return waiti;
	}
	
	//创建如果指令
	WhileIns NewWhileIns( string Name, int x, int y )
	{
		n_WhileIns.WhileIns ifi = new n_WhileIns.WhileIns();
		ifi.SetUserValue( Name, null, "?bool", x, y, 0 );
		
		n_CondiEndIns.CondiEndIns ifiend = new n_CondiEndIns.CondiEndIns();
		ifiend.SetUserValue( Name, null, x, y + ifi.Height + InsPadding0, 0 );
		
		ifi.InsEnd = ifiend;
		ifi.NextIns = ifiend;
		ifiend.PreIns = ifi;
		ifiend.InsStart = ifi;
		return ifi;
	}
	
	//创建如果否则指令
	IfElseIns NewIfElseIns0( string Name, int x, int y )
	{
		//添加如果否则指令
		n_IfElseIns.IfElseIns ifelsei = new n_IfElseIns.IfElseIns();
		ifelsei.SetUserValue( Name, null, "?bool", x, y, 0 );
		
		n_ElseIns.ElseIns elsei = new n_ElseIns.ElseIns();
		elsei.SetUserValue( Name, null, x, y + ifelsei.Height + InsPadding0, 0 );
		
		n_CondiEndIns.CondiEndIns ifelseiend = new n_CondiEndIns.CondiEndIns();
		ifelseiend.SetUserValue( Name, null, x, y + 2*ifelsei.Height + 2*InsPadding0, 0 );
		
		ifelsei.InsEnd = ifelseiend;
		ifelseiend.InsStart = ifelsei;
		
		//添加ELSE结构
		ifelsei.ElseIns = elsei;
		elsei.InsStart = ifelsei;
		elsei.InsEnd = ifelseiend;
		
		ifelsei.NextIns = elsei;
		elsei.PreIns = ifelsei;
		elsei.NextIns = ifelseiend;
		ifelseiend.PreIns = elsei;
		
		return ifelsei;
	}
	
	//创建如果否则指令
	IfElseIns NewIfElseIns( string Name, int x, int y )
	{
		//添加如果否则指令
		n_IfElseIns.IfElseIns ifelsei = new n_IfElseIns.IfElseIns();
		ifelsei.SetUserValue( Name, null, "?bool", x, y, 0 );
		
		n_CondiEndIns.CondiEndIns ifelseiend = new n_CondiEndIns.CondiEndIns();
		ifelseiend.SetUserValue( Name, null, x, y + ifelsei.Height + InsPadding0, 0 );
		
		ifelsei.InsEnd = ifelseiend;
		ifelseiend.InsStart = ifelsei;
		
		//忽略ELSE结构
		ifelsei.ElseIns = null;
		ifelsei.NextIns = ifelseiend;
		ifelseiend.PreIns = ifelsei;
		
		return ifelsei;
	}
	
	//创建反复执行指令
	ForeverIns NewForeverIns( string Name, int x, int y )
	{
		n_ForeverIns.ForeverIns foreveri = new n_ForeverIns.ForeverIns();
		foreveri.SetUserValue( Name, null, x, y, 0 );
		
		n_CondiEndIns.CondiEndIns foreveriend = new n_CondiEndIns.CondiEndIns();
		foreveriend.SetUserValue( Name, null, x, y + foreveri.Height + InsPadding0, 0 );
		
		foreveri.InsEnd = foreveriend;
		foreveri.NextIns = foreveriend;
		foreveriend.PreIns = foreveri;
		foreveriend.InsStart = foreveri;
		
		return foreveri;
	}
	
	//创建反复次数指令
	LoopIns NewLoopIns( string Name, int x, int y )
	{
		n_LoopIns.LoopIns loopi = new n_LoopIns.LoopIns();
		loopi.SetUserValue( Name, null, "?int32", x, y, 0 );
		
		n_CondiEndIns.CondiEndIns loopiend = new n_CondiEndIns.CondiEndIns();
		loopiend.SetUserValue( Name, null, x, y + loopi.Height + InsPadding0, 0 );
		
		loopi.InsEnd = loopiend;
		loopi.NextIns = loopiend;
		loopiend.PreIns = loopi;
		loopiend.InsStart = loopi;
		
		return loopi;
	}
	
	//创建遍历指令
	ExtIns NewExtIns( string Name, int x, int y )
	{
		ExtIns ext = new ExtIns();
		ext.SetUserValue( Name, null, "?module", x, y, 0 );
		
		n_CondiEndIns.CondiEndIns iend = new n_CondiEndIns.CondiEndIns();
		iend.SetUserValue( Name, null, x, y + ext.Height + InsPadding0, 0 );
		
		ext.InsEnd = iend;
		ext.NextIns = iend;
		iend.PreIns = ext;
		iend.InsStart = ext;
		
		return ext;
	}
}
}


