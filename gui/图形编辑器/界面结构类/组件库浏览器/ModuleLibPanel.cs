﻿
namespace n_ModuleLibPanel
{
using System;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.IO;
using System.Windows.Forms;

using n_DescriptionForm;
using n_GUIcoder;
using n_GUIset;
using n_MainSystemData;
using n_GVar;
using n_HardModule;
using n_ModuleLibDecoder;
using n_MyObject;
using n_MyObjectList;
using n_OS;
using n_Shape;
using n_UIModule;
using n_GNote;
using n_UserModule;
using n_MesPanel;
using n_MyColor;

//*****************************************************
//组件库类
public static class MyModuleLibPanel
{
	public delegate void D_HideEvent();
	public static D_HideEvent HideEvent;
	
	public delegate void D_OpenLibMng();
	public static D_OpenLibMng d_OpenLibMng;
	
	public static int SX;
	public static int SY;
	
	//组件选择事件委托
	public delegate void myModuleSelectEventHandler( MyObject m, int x, int y );
	public static myModuleSelectEventHandler myModuleSelect;
	
	public static bool isMouseDown;
	static int Times;
	
	public static bool Visible;
	
	static int LastX;
	static int LastY;
	
	public static int TopY;
	public static int BottomY;
	
	static Brush BackBrush;
	static Pen ScrollPen;
	
	static bool isMouseOn;
	
	//鼠标移动时是否自动显示控件
	public static bool AutoShow;
	
	static bool Active;
	
	static bool tempSwitch;
	
	public static bool FloatButton = true;
	static SolidBrush floatBrush;
	
	public static char LabType = n_GUIcoder.labType.Default;
	
	//构造函数
	public static void Init( char typeindex )
	{
		LabType = typeindex;
		Visible = false;
		tempSwitch = !SystemData.ModuleLibClick;
		isMouseOn = false;
		isMouseDown = false;
		Times = 0;
		
		AutoShow = false;
		
		TopY = GUIset.GetPix(114 - 13);
		BottomY = GUIset.GetPix(-200);
		
		BackBrush = new SolidBrush( GUIset.GetBlueColor( 100 ) );//  GUIset.BackBrush;
		
		floatBrush = new SolidBrush( Color.FromArgb( 200, Color.WhiteSmoke ) );
		
		//ScrollPen = new Pen( Color.FromArgb(75, 120, 160), 7 );
		ScrollPen = new Pen( Color.CornflowerBlue, 3 );
		//ScrollPen.CustomEndCap = LineCap.Round;
		//ScrollPen.CustomStartCap = LineCap.Round;
		
		ModuleDetail.Init();
		
		ModuleTree.Init( typeindex );
		
		Active = false;
		
		SX = 0;
		SY = TopY;
		
		FloatButton = false;
		n_ImagePanel.ImagePanel.ShowMessage = false;
		
//		if( GUIset.isDefault ) {
//			SX = -ModuleTree.Width / 2;
//			SY = -ModuleTree.Height / 2;
//		}
//		else {
//			SX = 20;
//			SY = 40;
//		}
	}
	
	//临时鼠标按下事件
	public static void TempUserMouseDown( bool hshow )
	{
		if( FloatButton ) {
			return;
		}
		if( !hshow ) {
			CanelHilight();
			if( isMouseDown ) {
				isMouseDown = false;
			}
		}
	}
	
	//鼠标按下事件
	public static bool UserMouseDown( int mX, int mY )
	{
		if( FloatButton ) {
			return false;
		}
		if( !Visible ) {
			return false;
		}
		
		if( mX < ModuleTree.Width ) {
			Active = true;
			isMouseOn = true;
		}
		if( SystemData.ModuleLibClick ) {
			
			
			if( ModuleDetail.isMouseOn ) {
				ModuleDetail.isMouseOn = false;
			}
			
			tempSwitch = true;
			UserMouseMove( mX, mY );
			tempSwitch = false;
		}
		
		if( !isMouseOn ) {
			
			return false;
		}
		isMouseDown = true;
		LastX = mX;
		LastY = mY;
		
		Times = 0;
		
		/*
		//分发组件库树列表鼠标事件
		ModuleTree.UserMouseDown( mX - SX, mY - SY );
		
		//如果拖动单个组件,则删除其他组件
		if( ModuleDetail.myModuleList.MouseOnObject != null ) {
			ModuleDetail.myModuleList.Clear();
			ModuleDetail.myModuleList.Add( ModuleDetail.myModuleList.MouseOnObject );
		}
		*/
		
		//这里如果删除, 可以实现鼠标松开时才添加模块
		if( ModuleDetail.ShowDetail && ModuleDetail.myModuleList.MouseOnObject != null ) {
			
			myModuleSelect( ModuleDetail.myModuleList.MouseOnObject, ModuleDetail.myModuleList.MouseOnBaseX, ModuleDetail.myModuleList.MouseOnBaseY );
			CanelHilight();
		}
		return true;
	}
	
	//鼠标松开事件
	public static void UserMouseUp( int mX, int mY )
	{
		if( FloatButton ) {
			return;
		}
		isMouseDown = false;
		if( !Visible ) {
			return;
		}
		
		//分发组件库树列表鼠标事件
		//ModuleTree.UserMouseUp( mX - SX, mY - SY );
		
		
		/*
		if( ModuleDetail.ShowDetail && ModuleDetail.myModuleList.MouseOnObject != null ) {
			
			myModuleSelect( ModuleDetail.myModuleList.MouseOnObject, ModuleDetail.myModuleList.MouseOnBaseX, ModuleDetail.myModuleList.MouseOnBaseY );
			ModuleDetail.isMouseOn = false;
			ModuleDetail.ShowDetail = false;
			ModuleTree.CurrentItemView.isMouseOn = false;
			ModuleTree.CurrentItemView = null;
			ModuleDetail.myModuleList.MouseOnObject.isHighLight = false;
			ModuleDetail.myModuleList.MouseOnObject.isMouseOnBase = false;
			ModuleDetail.myModuleList.MouseOnObject = null;
			ModuleDetail.myModuleList = null;
		}
		*/
	}
	
	//鼠标移动事件
	public static bool UserMouseMove( int mX, int mY )
	{
		if( FloatButton ) {
			return false;
		}
		if( !Visible ) {
			return false;
		}
		if( !Active ) {
			return false;
		}
		
		//组件详情区域鼠标事件
		if( SystemData.ModuleLibClick && !tempSwitch ) {
			ModuleDetail.UserMouseMove( mX - SX, mY - SY );
		}
		if( !tempSwitch && !isMouseDown ) {
			return false;
		}
		if( isMouseDown ) {
			SY += ( mY - LastY );
			if( SY < BottomY ) {
				SY = BottomY;
			}
			if( SY > TopY ) {
				SY = TopY;
			}
			if( mX > SX + ModuleDetail.SX && ModuleDetail.ShowDetail ) {
				/*
				SX += ( mX - LastX );
				if( SX > 0 ) {
					SX = 0;
				}
				*/
				ModuleTree.Width = SX + ModuleTree.CWidth;
			}
			LastX = mX;
			LastY = mY;
		}
		
		//分发组件库树列表鼠标事件
		if( !isMouseDown && !ModuleDetail.isMouseOn ) {
			
			//if( !GUISystemData.ModuleLibClick ) {
				ModuleTree.UserMouseMove( mX, mX - SX, mY - SY );
			//}
		}
		//组件详情区域鼠标事件
		ModuleDetail.UserMouseMove( mX - SX, mY - SY );
		
		if( ModuleDetail.ShowDetail && isMouseDown ) {
			if( Times < 3 ) {
				Times++;
			}
			else {
				ModuleDetail.myModuleList.MouseOnObject = null;
			}
		}
		if( mX > ModuleTree.Width && !ModuleDetail.ShowDetail ) {
			
			if( n_Head.Head.AutoHide ) {
				Visible = false;
				if( HideEvent != null ) {
					HideEvent();
				}
			}
			SX = 0;
			ModuleTree.Width = ModuleTree.CWidth;
			isMouseOn = false;
			
			Active = false;
		}
		else {
			isMouseOn = true;
		}
		return Visible && isMouseOn;
	}
	
	//取消已有的高亮显示
	public static void CanelHilight()
	{
		if( ModuleTree.CurrentItemView != null ) {
			ModuleDetail.isMouseOn = false;
			ModuleDetail.ShowDetail = false;
			
			ModuleTree.CurrentItemView.isMouseOn = false;
			ModuleTree.CurrentItemView = null;
			
			if( ModuleDetail.myModuleList != null && ModuleDetail.myModuleList.MouseOnObject != null ) {
				ModuleDetail.myModuleList.MouseOnObject.isHighLight = false;
				ModuleDetail.myModuleList.MouseOnObject.isMouseOnBase = false;
				ModuleDetail.myModuleList.MouseOnObject = null;
			}
			ModuleDetail.myModuleList = null;
		}
	}
	
	//滚动条移动
	public static bool Scroll( int mX, int mY, int Delta )
	{
		if( FloatButton ) {
			return false;
		}
		if( !n_Head.Head.HeadShow ) {
			return false;
		}
		if( !Visible ) {
			return false;
		}
		if( mX > ModuleTree.Width ) {
			return false;
		}
		SY += Delta;
		if( SY < BottomY ) {
			SY = BottomY;
		}
		if( SY > TopY ) {
			SY = TopY;
		}
		UserMouseMove( mX, mY );
		return true;
	}
	
	//重新设置滚动条
	public static void ResetScroll()
	{
		//加上10为了让最后的条目能够上移一点点
		MyModuleLibPanel.BottomY = -(ModuleTree.CurrentLine*ModuleTree.ItemView.PerHeight - n_Head.Head.MaxHeight + 10 + n_Head.Head.BotScrollHeight);
	}
	
	public static int time;
	
	//显示图像
	public static void Draw( Graphics g )
	{
		if( FloatButton ) {
			return;
		}
		if( !Visible ) {
			return;
		}
		//g.DrawString( time.ToString(), DescriptionForm.DefaultFont, Brushes.Red, 5, 100 );
		
//		if( GUIset.isDefault ) {
//			StartX += SX;
//			StartY += SY;
//		}
//		else {
//			StartX = SX;
//			StartY = SY;
//		}
		
		//绘制组件侧边栏
		if( ModuleDetail.ShowDetail ) {
			ModuleTree.Draw( g, SX, SY );
		}
		else {
			ModuleTree.Draw( g, SX + n_Head.Head.ShowX, SY );
		}
		
		if( ModuleTree.ItemView.NeedRefresh ) {
			//ModuleTree.RefreshLoc();
			G.CGPanel.MyRefresh();
		}
	}
	
	//显示图像
	public static void Draw1( Graphics g )
	{
		if( FloatButton ) {
			return;
		}
		if( !Visible ) {
			return;
		}
		//g.DrawString( time.ToString(), DescriptionForm.DefaultFont, Brushes.Red, 5, 100 );
		
//		if( GUIset.isDefault ) {
//			StartX += SX;
//			StartY += SY;
//		}
//		else {
//			StartX = SX;
//			StartY = SY;
//		}
		
		//组件详情绘制
		ModuleDetail.Draw( g, SX, SY );
		
		int tx = 0;
		
		//绘制组件侧边栏
		if( ModuleDetail.ShowDetail ) {
			ModuleTree.Draw1( g, SX, SY );
		}
		else {
			ModuleTree.Draw1( g, SX + n_Head.Head.ShowX, SY );
			tx = n_Head.Head.ShowX;
		}
		
		int MH = ModuleTree.CurrentLine * ModuleTree.ItemView.PerHeight;
		int BH = n_Head.Head.MaxHeight - n_Head.R_HeadLabel.MaxHeight - n_Head.Head.BotScrollHeight;
		
		if( MH != 0 && TopY - BottomY != 0 ) {
			int Percent = (TopY - SY) * 10000 / (TopY - BottomY);
			int sy = (BH - BH*BH/MH) * Percent / 10000;
			
			//g.DrawLine( Pens.Blue, 5, n_Head.C_HeadLabel.MaxHeight + 10, 5, n_Head.C_HeadLabel.MaxHeight + 10 + BH - 20 );
			g.DrawLine( ScrollPen, tx + 5, TopY + 2 + sy, tx + 5, n_Head.R_HeadLabel.MaxHeight + 2 + BH*BH/MH - 4 + sy );
		}
		
		if( FloatButton ) {
			Rectangle r = new Rectangle( 0, 0, ModuleTree.Width - 2, ModuleTree.Height );
			g.FillRectangle( floatBrush, r );
		}
	}
}
//组件库条目详情
public static class ModuleDetail
{
	public static bool ShowDetail;
	
	public static float SX;
	public static float SY;
	
	public static float Width;
	public static float Height;
	
	public static int HTopX;
	public static int HTopY;
	public static int HBottomX;
	public static int HBottomY;
	
	public static bool isMouseOn;
	
	public static float MinX = 0, MaxX = 0;
	public static float MinY = 0, MaxY = 0;
	
	public static MyObjectList myModuleList;
	
	public static MyObjectList[] myModuleListSet;
	
	static Brush ModuleMesBackBrush;
	
	public static Brush BackPanelBrush;
	
	static GraphicsPath TrigPath;
	
	public static Pen AreaPen;
	
	static MyObject LastMouseOnObject;
	static Image[] ModuleImageList;
	
	static Pen EageLinePen0;
	static Pen EageLinePen1;
	
	//static Bitmap GT_Img;
	//static Bitmap COM_Img;
	//static Bitmap SL_Img;
	//static Bitmap UART_Img;
	
	//初始化
	public static void Init()
	{
		ShowDetail = false;
		
		SX = 20;
		SY = 100;
		
		MinX = 0;
		MaxX = 0;
		MinY = 0;
		MaxY = 0;
		isMouseOn = false;
		
		myModuleListSet = new MyObjectList[150];
		myModuleList = null;
		
		TrigPath = null;
		
		ModuleMesBackBrush = new SolidBrush( Color.FromArgb( 240, 255, 255, 255 ) );
		
		//BackPanelBrush = new SolidBrush( Color.FromArgb(75, 120, 160) );
		//BackPanelBrush = new SolidBrush( Color.FromArgb(60 * 70/100, 90 * 70/100, 120 * 70/100 ) );
		
		AreaPen = new Pen( Color.FromArgb( 0, 0, 0, 0 ), 6 );
		
		EageLinePen0 = new Pen( Color.Silver, 2 );
		//EageLinePen0 = new Pen( Color.FromArgb( 255, 178, 0 ), 2 );
		EageLinePen1 = new Pen( Color.FromArgb( 255, 255, 255 ), 1 );
		
		if( SystemData.isBlack ) {
			BackPanelBrush = new SolidBrush( Color.FromArgb(50, 65, 80 ) );
		}
		else {
			//BackPanelBrush = new SolidBrush( Color.FromArgb(180, 185, 190) );
			BackPanelBrush = new SolidBrush( Color.FromArgb(255, 255, 255) );
		}
		//GT_Img = new Bitmap( n_OS.OS.SystemRoot + @"Resource\gui\Lib\GT.png" );
		//COM_Img = new Bitmap( n_OS.OS.SystemRoot + @"Resource\gui\Lib\COM.png" );
		//SL_Img = new Bitmap( n_OS.OS.SystemRoot + @"Resource\gui\Lib\SL.png" );
		//UART_Img = new Bitmap( n_OS.OS.SystemRoot + @"Resource\gui\Lib\UART.png" );
	}
	
	//更新边界
	public static void RefreshEage()
	{
		//获取图形界面的边界
		//Shape.GetArea( null, myModuleList, ref MinX, ref MinY, ref MaxX, ref MaxY );
		
		float C_MinX = 0;
		float C_MaxX = 0;
		float C_MinY = 0;
		float C_MaxY = 0;
		Shape.GetArea( null, myModuleList, ref MinX, ref MinY, ref MaxX, ref MaxY, ref C_MinX, ref C_MinY, ref C_MaxX, ref C_MaxY );
		
		MinX -= ModuleTree.XPadding;
		MaxX += 2 * ModuleTree.XPadding;
		MinY -= ModuleTree.YPadding;
		MaxY += ModuleTree.YPadding;
		
		/*
		//判断是否需要显示背景图片的类别, 并设置对应的高度
		if( ModuleTree.CurrentItemView.BG_ImgIndex == 1 ) {
			MaxY = MinY + GT_Img.Height;
		}
		if( ModuleTree.CurrentItemView.BG_ImgIndex == 2 ) {
			MaxY = MinY + COM_Img.Height;
		}
		if( ModuleTree.CurrentItemView.BG_ImgIndex == 3 ) {
			MaxY = MinY + SL_Img.Height;
		}
		if( ModuleTree.CurrentItemView.BG_ImgIndex == 4 ) {
			MaxY = MinY + UART_Img.Height;
		}
		*/
		
		Width = MaxX - MinX;
		Height = MaxY - MinY;
	}
	
	//鼠标移动事件
	public static void UserMouseMove( int mX, int mY )
	{
		if( myModuleList == null ) {
			return;
		}
		//int Offset = 60;
		
		float cminy = HTopY - 40;
		if( cminy < SY + MinY ) {
			cminy = SY + MinY;
		}
		float cmaxy = HBottomY + 40;
		if( cmaxy > SY + MaxY ) {
			cmaxy = SY + MaxY;
		}
		
		//---------------------------------------------------------
		//绘制局部的贝塞尔连接区域
//		Point point1 = new Point( MyModuleLibPanel.SX + HTopX, MyModuleLibPanel.SY + HTopY );
//		Point p1 = new Point( MyModuleLibPanel.SX + HTopX + Offset, MyModuleLibPanel.SY + HTopY );
//		Point point2 = new Point( MyModuleLibPanel.SX + HBottomX, MyModuleLibPanel.SY + HBottomY );
//		Point p2 = new Point( MyModuleLibPanel.SX + HBottomX + Offset, MyModuleLibPanel.SY + HBottomY );
//		Point point3 = new Point( MyModuleLibPanel.SX + SX + MinX + 4, MyModuleLibPanel.SY + cminy );
//		Point p3 = new Point( MyModuleLibPanel.SX + SX + MinX + 4, MyModuleLibPanel.SY + cminy + Offset );
//		Point point4 = new Point( MyModuleLibPanel.SX + SX + MinX + 4, MyModuleLibPanel.SY + cmaxy );
//		Point p4 = new Point( MyModuleLibPanel.SX + SX + MinX + 4, MyModuleLibPanel.SY + cmaxy - Offset );
		
		//---------------------------------------------------------
//		//绘制整体的贝塞尔连接区域
//		Point point1 = new Point( MyModuleLibPanel.SX + HTopX, MyModuleLibPanel.SY + HTopY );
//		Point p1 = new Point( MyModuleLibPanel.SX + HTopX + Offset, MyModuleLibPanel.SY + HTopY );
//		Point point2 = new Point( MyModuleLibPanel.SX + HBottomX, MyModuleLibPanel.SY + HBottomY );
//		Point p2 = new Point( MyModuleLibPanel.SX + HBottomX + Offset, MyModuleLibPanel.SY + HBottomY );
//		Point point3 = new Point( MyModuleLibPanel.SX + SX + MinX + 4, MyModuleLibPanel.SY + SY + MinY );
//		Point p3 = new Point( MyModuleLibPanel.SX + SX + MinX + 4 - Offset/2, MyModuleLibPanel.SY + SY + MinY );
//		Point point4 = new Point( MyModuleLibPanel.SX + SX + MinX + 4, MyModuleLibPanel.SY + SY + MaxY );
//		Point p4 = new Point( MyModuleLibPanel.SX + SX + MinX + 4 - Offset/2, MyModuleLibPanel.SY + SY + MaxY );
		
//		TrigPath = new GraphicsPath();
//		TrigPath.Reset();
//		TrigPath.AddBezier( point1, p1, p3, point3 );
//		TrigPath.AddBezier( point4, p4, p2, point2 );
		
		//---------------------------------------------------------
		//绘制折线连接区域
		
		/*
		PointF point1 = new PointF( MyModuleLibPanel.SX + HTopX, MyModuleLibPanel.SY + HTopY );
		PointF point2 = new PointF( MyModuleLibPanel.SX + HBottomX, MyModuleLibPanel.SY + HTopY );
		PointF point3 = new PointF( MyModuleLibPanel.SX + SX + MinX, MyModuleLibPanel.SY + cminy );
		
		PointF point4 = new PointF( MyModuleLibPanel.SX + SX + MinX, MyModuleLibPanel.SY + SY + MinY );
		PointF point5 = new PointF( MyModuleLibPanel.SX + SX + MaxX, MyModuleLibPanel.SY + SY + MinY );
		PointF point6 = new PointF( MyModuleLibPanel.SX + SX + MaxX, MyModuleLibPanel.SY + SY + MaxY );
		PointF point7 = new PointF( MyModuleLibPanel.SX + SX + MinX, MyModuleLibPanel.SY + SY + MaxY );
		
		PointF point8 = new PointF( MyModuleLibPanel.SX + SX + MinX, MyModuleLibPanel.SY + cmaxy );
		PointF point9 = new PointF( MyModuleLibPanel.SX + HBottomX, MyModuleLibPanel.SY + HBottomY );
		PointF point10 = new PointF( MyModuleLibPanel.SX + HTopX, MyModuleLibPanel.SY + HBottomY );
		
		PointF[] pntArr = { point1, point2, point3, point4, point5, point6, point7, point8, point9, point10 };
		TrigPath = new GraphicsPath();
		TrigPath.Reset();
		TrigPath.AddPolygon( pntArr );
		*/
		
		int R = 4;
		int R2 = R * 2;
		
		TrigPath = new GraphicsPath();
		TrigPath.Reset();
		TrigPath.AddArc( MyModuleLibPanel.SX + HTopX, MyModuleLibPanel.SY + HTopY, R2, R2, 180, 90 );
		TrigPath.AddLine( MyModuleLibPanel.SX + HTopX + R, MyModuleLibPanel.SY + HTopY, MyModuleLibPanel.SX + HBottomX - R, MyModuleLibPanel.SY + HTopY );
		TrigPath.AddLine( MyModuleLibPanel.SX + HBottomX, MyModuleLibPanel.SY + HTopY, MyModuleLibPanel.SX + SX + MinX, MyModuleLibPanel.SY + cminy );
		TrigPath.AddLine( MyModuleLibPanel.SX + SX + MinX, MyModuleLibPanel.SY + cminy, MyModuleLibPanel.SX + SX + MinX, MyModuleLibPanel.SY + SY + MinY + R );
		TrigPath.AddArc( MyModuleLibPanel.SX + SX + MinX, MyModuleLibPanel.SY + SY + MinY, R2, R2, 180, 90 );
		TrigPath.AddLine( MyModuleLibPanel.SX + SX + MinX + R, MyModuleLibPanel.SY + SY + MinY, MyModuleLibPanel.SX + SX + MaxX - R, MyModuleLibPanel.SY + SY + MinY );
		TrigPath.AddArc( MyModuleLibPanel.SX + SX + MaxX - R2, MyModuleLibPanel.SY + SY + MinY, R2, R2, 270, 90 );
		TrigPath.AddLine( MyModuleLibPanel.SX + SX + MaxX, MyModuleLibPanel.SY + SY + MinY + R, MyModuleLibPanel.SX + SX + MaxX, MyModuleLibPanel.SY + SY + MaxY - R );
		TrigPath.AddArc( MyModuleLibPanel.SX + SX + MaxX - R2, MyModuleLibPanel.SY + SY + MaxY - R2, R2, R2, 0, 90 );
		TrigPath.AddLine( MyModuleLibPanel.SX + SX + MaxX - R, MyModuleLibPanel.SY + SY + MaxY, MyModuleLibPanel.SX + SX + MinX + R, MyModuleLibPanel.SY + SY + MaxY );
		TrigPath.AddArc( MyModuleLibPanel.SX + SX + MinX, MyModuleLibPanel.SY + SY + MaxY - R2, R2, R2, 90, 90 );
		TrigPath.AddLine( MyModuleLibPanel.SX + SX + MinX, MyModuleLibPanel.SY + SY + MaxY - R, MyModuleLibPanel.SX + SX + MinX, MyModuleLibPanel.SY + cmaxy );
		TrigPath.AddLine( MyModuleLibPanel.SX + SX + MinX, MyModuleLibPanel.SY + cmaxy, MyModuleLibPanel.SX + HBottomX, MyModuleLibPanel.SY + HBottomY );
		TrigPath.AddLine( MyModuleLibPanel.SX + HBottomX, MyModuleLibPanel.SY + HBottomY, MyModuleLibPanel.SX + HTopX + R, MyModuleLibPanel.SY + HBottomY );
		TrigPath.AddArc( MyModuleLibPanel.SX + HTopX, MyModuleLibPanel.SY + HBottomY  - R2, R2, R2, 90, 90 );
		TrigPath.AddLine( MyModuleLibPanel.SX + HTopX, MyModuleLibPanel.SY + HBottomY - R, MyModuleLibPanel.SX + HTopX, MyModuleLibPanel.SY + HTopY +R );
		TrigPath.CloseFigure();
		
		//---------------------------------------------------------
		
		
		Region BackRegion = new Region();
		BackRegion.MakeEmpty();
		BackRegion.Union(TrigPath);
		
		//组件列表鼠标事件
		if( BackRegion.IsVisible( MyModuleLibPanel.SX + mX, MyModuleLibPanel.SY + mY ) || mX >= SX + MinX && mX <= SX + MaxX &&
		    mY >= SY + MinY && mY <= SY + MaxY ) {
			isMouseOn = true;
			
			//if( MyModuleLibPanel.isMouseDown ) {
				ShowDetail = true;
				
				ModuleTree.CurrentItemView.isMouseOn = true;
			//}
		}
		else {
			if( !SystemData.ModuleLibClick ) {
				isMouseOn = false;
				if( ModuleTree.CurrentItemView != null ) {
					ModuleTree.CurrentItemView.isMouseOn = false;
					ModuleTree.CurrentItemView = null;
				}
				//if( !ShowDetail ) {
					ModuleDetail.myModuleList = null;
				//}
			}
			
			
		}
		if( isMouseOn ) {
			myModuleList.UserMouseMove( (int)(mX - SX), (int)(mY - SY) );
		}
		if( !ShowDetail ) {
			//复位组件库的水平位置
			//MyModuleLibPanel.SX = 0;
		}
		
	}
	
	//绘图
	public static void Draw( Graphics g, float StartX, float StartY )
	{
		// ModuleTree.CurrentItemView != null 点击第三方模块库会为null 所以需要判断
		if( ShowDetail && ModuleTree.CurrentItemView != null ) {
			RectangleF r = new RectangleF( StartX + SX + MinX, StartY + SY + MinY, MaxX - MinX, MaxY - MinY );
			//g.FillRectangle( GUIset.BackBrush, r );
			GraphicsPath gp = Shape.CreateRoundedRectanglePath( r );
			
			//g.DrawPath( AreaPen, gp );
			//g.FillPath( BackPanelBrush, gp );
			
			//g.FillPath( Brushes.Gray, gp );
			
			//绘制放大区域
			if(TrigPath != null ) {
				g.FillPath( BackPanelBrush, TrigPath );
				g.DrawPath( EageLinePen0, TrigPath );
				//g.DrawPath( EageLinePen1, TrigPath );
			}
			
			//============================================================================
			//这里的每个更新都需要记录到软件架构里边
			if( ModuleTree.CurrentItemView.Text == "辅助元件" ) {
				g.DrawString( "此类别模块不生成代码, 只用于导线连接或指示", GUIset.ExpFont, Brushes.DimGray, StartX + SX + MinX + 10, StartY + SY + MinY + 5 );
			}
			//============================================================================
			
			/*
			if( ModuleTree.CurrentItemView.BG_ImgIndex == 1 ) {
				g.DrawImage( GT_Img, StartX + SX + MinX, StartY + SY + MinY );
			}
			if( ModuleTree.CurrentItemView.BG_ImgIndex == 2 ) {
				g.DrawImage( COM_Img, StartX + SX + MinX, StartY + SY + MinY );
			}
			if( ModuleTree.CurrentItemView.BG_ImgIndex == 3 ) {
				g.DrawImage( SL_Img, StartX + SX + MinX, StartY + SY + MinY );
			}
			if( ModuleTree.CurrentItemView.BG_ImgIndex == 4 ) {
				g.DrawImage( UART_Img, StartX + SX + MinX, StartY + SY + MinY );
			}
			*/
			
			GraphicsState gs = g.Save();
			g.TranslateTransform( StartX + SX, StartY + SY );
			myModuleList.DrawImage( g );
			g.Restore( gs );
			
			if( myModuleList.MouseOnObject != null ) {
				DrawExtendMesImage( g, StartX + SX, StartY + SY );
			}
		}
	}
	
	//绘制扩展信息
	public static void DrawExtendMes( Graphics g, float StartX, float StartY )
	{
		MyObject cm = myModuleList.MouseOnObject;
		ModuleMessage MM = null;
		
		//绘制组件高亮状态
		float x = StartX + cm.SX - 4;
		float y = StartY + cm.SY - 4;
		int w = cm.Width - 1 + 8;
		int h = cm.Height - 1 + 8;
		//g.DrawRectangle( GUIset.组件高亮边框_Pen2, x, y, w, h );
		g.DrawRectangle( GUIset.组件高亮边框_Pen1, x, y, w, h );
		
		if( cm is HardModule ) {
			HardModule m = (HardModule)cm;
			int LIndex = m.GetLIndex();
			MM = m.mm[ LIndex ];
		}
		if( cm is UIModule ) {
			UIModule m = (UIModule)cm;
			int LIndex = m.GetLIndex();
			MM = m.mm[ LIndex ];
		}
		if( MM == null ) {
			return;
		}
		//绘制背景半透明效果
		//g.FillRectangle( GUIset.HideBackBrush, 0, 0, Width, Height );
		
		//获取屏幕下方空白区域
		float H = StartY + cm.SY + cm.Height;
		
		//绘制边框,间距为 15
		//Rectangle r0 = new Rectangle( 15, H + 15, this.Width - 15 - 15, this.Height - H - 15 - 15 );
		//GraphicsPath gp = Shape.CreateRoundedRectanglePath( r0 );
		//g.FillPath( GUIset.HideBackBrush, gp );
		//g.DrawPath( Pens.LightGray, gp );
		
		//文字起点为下方 20, 宽度间距为 20
		int MesWidth = G.CGPanel.Width - ModuleTree.Width - 10;
		int MesX = ModuleTree.Width; //StartX + cm.SX;
		float CurrentHeight = H + 20;
		int MesPadding = 10;
		int ModuleNamePadding = MesPadding + 10;
		int ModuleMesPadding = MesPadding + 40;
		int ItemNamePadding = MesPadding + 10;
		int ItemMesPadding = MesPadding + 40;
		
		/*
		//绘制三角形
		Point P0 = new Point( StartX + cm.MidX, CurrentHeight - 15 );
		Point P1 = new Point( StartX + cm.MidX - 10, CurrentHeight + 5 );
		Point P2 = new Point( StartX + cm.MidX + 10, CurrentHeight + 5 );
		Point[] PA = new Point[]{P0, P1, P2};
		g.FillPolygon( ModuleMesBackBrush, PA );
		*/
		
		int AllHeight = DescriptionForm.SearchHeight( MM, MesWidth );
		RectangleF r = new RectangleF( MesX, CurrentHeight, MesWidth, AllHeight );
		GraphicsPath gp = Shape.CreateRoundedRectanglePath( r );
		g.FillPath( ModuleMesBackBrush, gp );
		
		CurrentHeight += 5;
		
		//绘制组件信息
		g.DrawString( MM.UserName + ":", n_MesPanel.MesPanel.extendHeadFont, Brushes.DarkOrange, MesX + ModuleNamePadding, CurrentHeight );
		CurrentHeight += 20;
		SizeF sz0 = GUIset.mg.MeasureString( MM.ModuleMes, n_MesPanel.MesPanel.extendFont, MesWidth - ModuleMesPadding );
		RectangleF rf0 = new RectangleF( MesX + ModuleMesPadding, CurrentHeight, sz0.Width, sz0.Height );
		g.DrawString( MM.ModuleMes, n_MesPanel.MesPanel.extendFont, Brushes.DimGray, rf0 );
		CurrentHeight += (int)sz0.Height;
		
		//绘制组件成员信息
		for( int i = 0; i < MM.MemberMesList.Length; ++i ) {
			if( MM.MemberMesList[ i ].UserName.StartsWith( "OS_" ) ) {
				continue;
			}
			CurrentHeight += 5;
			g.DrawString( MM.MemberMesList[ i ].UserName + ":", n_MesPanel.MesPanel.extendHeadFont, Brushes.SlateBlue, MesX + ItemNamePadding, CurrentHeight );
			CurrentHeight += 20;
			SizeF sz = GUIset.mg.MeasureString( MM.MemberMesList[ i ].Mes, n_MesPanel.MesPanel.extendFont, MesWidth - ItemMesPadding );
			RectangleF rf = new RectangleF( MesX + ItemMesPadding, CurrentHeight, sz.Width, sz.Height );
			g.DrawString( MM.MemberMesList[ i ].Mes, n_MesPanel.MesPanel.extendFont, Brushes.DimGray, rf );
			CurrentHeight += (int)sz.Height;
		}
	}
	
	//绘制带有图片的扩展信息
	public static void DrawExtendMesImage( Graphics g, float StartX, float StartY )
	{
		MyObject cm = myModuleList.MouseOnObject;
		ModuleMessage MM = null;
		
		//绘制组件高亮状态
		float x = StartX + cm.SX - 4;
		float y = StartY + cm.SY - 4;
		int w = cm.Width - 1 + 8;
		int h = cm.Height - 1 + 8;
		//g.DrawRectangle( GUIset.组件高亮边框_Pen2, x, y, w, h );
		g.DrawRectangle( GUIset.组件高亮边框_Pen1, x, y, w, h );
		
		if( cm is HardModule ) {
			HardModule m = (HardModule)cm;
			int LIndex = m.GetLIndex();
			MM = m.mm[ LIndex ];
		}
		if( cm is UIModule ) {
			UIModule m = (UIModule)cm;
			int LIndex = m.GetLIndex();
			MM = m.mm[ LIndex ];
		}
		if( MM == null ) {
			return;
		}
		//绘制背景半透明效果
		//g.FillRectangle( GUIset.HideBackBrush, 0, 0, Width, Height );
		
		//获取屏幕下方空白区域
		float H = StartY + cm.SY + cm.Height;
		
		//绘制边框,间距为 15
		//Rectangle r0 = new Rectangle( 15, H + 15, this.Width - 15 - 15, this.Height - H - 15 - 15 );
		//GraphicsPath gp = Shape.CreateRoundedRectanglePath( r0 );
		//g.FillPath( GUIset.HideBackBrush, gp );
		//g.DrawPath( Pens.LightGray, gp );
		
		//文字起点为下方 20, 宽度间距为 20
		int MesWidth = 340;
		int MesX = G.CGPanel.Width - MesWidth; //StartX + cm.SX;
		int CurrentHeight = 0;
		int MesPadding = 2;
		int ModuleNamePadding = MesPadding + 10;
		int ModuleMesPadding = MesPadding + 40;
		int ItemNamePadding = MesPadding + 10;
		int ItemMesPadding = MesPadding + 40;
		
		/*
		//绘制三角形
		Point P0 = new Point( StartX + cm.MidX, CurrentHeight - 15 );
		Point P1 = new Point( StartX + cm.MidX - 10, CurrentHeight + 5 );
		Point P2 = new Point( StartX + cm.MidX + 10, CurrentHeight + 5 );
		Point[] PA = new Point[]{P0, P1, P2};
		g.FillPolygon( ModuleMesBackBrush, PA );
		*/
		
		int AllHeight = DescriptionForm.SearchHeight( MM, MesWidth );
		AllHeight = G.CGPanel.Height;
		Rectangle r = new Rectangle( MesX, CurrentHeight, MesWidth, AllHeight );
		g.FillRectangle( ModuleMesBackBrush, r );
		g.DrawLine( Pens.CornflowerBlue, MesX, CurrentHeight, MesX, AllHeight );
		
		CurrentHeight += 100;
		
		
		//判断是否需要重新加载图像
		if( LastMouseOnObject != cm ) {
			LastMouseOnObject = cm;
			//加载图像
			string FileList = ((HardModule)cm).ModuleImageList;
			ModuleImageList = null;
			if( FileList != "" ) {
				string[] flist = FileList.Split( ',' );
				ModuleImageList = new Image[ flist.Length ];
				for( int i = 0; i < flist.Length; ++i ) {
					ModuleImageList[i] = Image.FromFile( ((HardModule)cm).BasePath + flist[i] );
				}
			}
		}
		//如果图像非空则显示
		if( ModuleImageList != null ) {
			
			for( int i = 0; i < ModuleImageList.Length; ++i ) {
				int nw = ModuleImageList[i].Width;
				int nh = ModuleImageList[i].Height;
				if( nw > MesWidth - 4 ) {
					nw = MesWidth - 4;
					nh = ModuleImageList[i].Height * nw / ModuleImageList[i].Width;
					}
				g.DrawImage( ModuleImageList[i], MesX + (MesWidth-nw)/2, CurrentHeight, nw, nh );
				CurrentHeight += nh;
			}
		}
		
		
		//绘制组件信息
		g.DrawString( MM.UserName + ":", n_MesPanel.MesPanel.extendHeadFont, Brushes.DarkOrange, MesX + ModuleNamePadding, CurrentHeight );
		CurrentHeight += 20;
		SizeF sz0 = GUIset.mg.MeasureString( MM.ModuleMes, n_MesPanel.MesPanel.extendFont, MesWidth - ModuleMesPadding );
		RectangleF rf0 = new RectangleF( MesX + ModuleMesPadding, CurrentHeight, sz0.Width, sz0.Height );
		g.DrawString( MM.ModuleMes, n_MesPanel.MesPanel.extendFont, Brushes.DimGray, rf0 );
		CurrentHeight += (int)sz0.Height;
		
		//绘制组件成员信息
		for( int i = 0; i < MM.MemberMesList.Length; ++i ) {
			if( MM.MemberMesList[ i ].UserName.StartsWith( "OS_" ) ) {
				continue;
			}
			CurrentHeight += 5;
			g.DrawString( MM.MemberMesList[ i ].UserName + ":", n_MesPanel.MesPanel.extendHeadFont, Brushes.SlateBlue, MesX + ItemNamePadding, CurrentHeight );
			CurrentHeight += 20;
			SizeF sz = GUIset.mg.MeasureString( MM.MemberMesList[ i ].Mes, n_MesPanel.MesPanel.extendFont, MesWidth - ItemMesPadding );
			RectangleF rf = new RectangleF( MesX + ItemMesPadding, CurrentHeight, sz.Width, sz.Height );
			g.DrawString( MM.MemberMesList[ i ].Mes, n_MesPanel.MesPanel.extendFont, Brushes.DimGray, rf );
			CurrentHeight += (int)sz.Height;
		}
	}
}
//组件树类
public static class ModuleTree
{
	public delegate void D_ItemViewChanged( int i );
	public static D_ItemViewChanged ItemViewChanged;
	
	static ItemView[] List;
	static int ListLength;
	public static int CurrentLine;
	static int CurrentLevel;
	
	public static int SX;
	public static int SY;
	public static int Width;
	public static int Height;
	public static int CWidth = 210;
	
	public static ItemView CurrentItemView;
	public static int CurrentItemIndex;
	
	public static SolidBrush TreeBackBrush;
	
	static SolidBrush EageBrush;
	
	public static int XPadding = 20;
	
	//2020.6.26 这个参数也会影响上下对齐, 引擎类模块的起点最好设置为从0开始, 处理时会加上 YPadding
	public static int YPadding = 40;
	
	//组件面板距离界面上端的最小距离
	static int MPanelTopY;
	
	//构造函数
	public static void Init( char tindex )
	{
		List = new ItemView[ 350 ];
		ListLength = 0;
		CurrentLine = 0;
		CurrentLevel = 0;
		
		MPanelTopY = GUIset.GetPix(90);
		
		ItemView.Init();
		
		ModuleLibDecoder.deleEnterFolder = EnterFolder;
		ModuleLibDecoder.deleLeaveFolder = LeaveFolder;
		ModuleLibDecoder.deleAddNode += AddNode;
		
		ModuleLibDecoder.Load( tindex );
		
		RefreshLoc();
		
		SX = 0;
		SY = 0;
		Width = CWidth;
		
		CurrentItemIndex = -1;
		CurrentItemView = null;
		ItemViewChanged = null;
		
		EageBrush = new SolidBrush( n_Head.Head.ModButtonActiveColor );
		
		
		//TreeBackBrush = new SolidBrush( n_GUIset.GUIset.GUIbackColor );
		//TreeBackBrush = new SolidBrush( Color.FromArgb( 230, 235, 250 ) );
		
		TreeBackBrush = new SolidBrush( Color.FromArgb( 238, 242, 246 ) );
		
		if( G.LightUI ) {
			TreeBackBrush = new SolidBrush( Color.FromArgb( 255, 255, 255 ) );
		}
	}
	
	//鼠标按下事件
	public static bool UserMouseDown( int mX, int mY )
	{
		for( int i = 0; i < ListLength; ++i ) {
			List[ i ].UserMouseDown( mX, mY );
		}
		return false;
	}
	
	//鼠标松开事件
	public static void UserMouseUp( int mX, int mY )
	{
		for( int i = 0; i < ListLength; ++i ) {
			List[ i ].UserMouseUp( mX, mY );
		}
	}
	
	//鼠标移动事件
	public static void UserMouseMove( int x, int mX, int mY )
	{
		ItemView temp = CurrentItemView;
		CurrentItemView = null;
		
		for( int i = 0; i < ListLength; ++i ) {
			bool isin = List[ i ].UserMouseMove( mX, mY );
			
			//判断是否需要折叠和展开列表
			if( isin && List[ i ].Path == null ) {
				
				List[ i ].Expand = !List[ i ].Expand;
				
				SetVisible( List[i].Text, List[i], List[i].Expand );
				
				for( int j = i + 1; j < ListLength; ++j ) {
					if( List[ j ].Path == null ) {
						break;
					}
					//List[ j ].Visible = List[ i ].Expand;
					List[ j ].tempVisible = List[ i ].Expand;
					
					SetVisible( List[i].Text, List[j], List[j].tempVisible );
				}
				
				//n_Debug.Debug.Message = SystemData.TreeExpandFlag;
				
				//重新计算各个条目的坐标
				//RefreshLoc();
				
				//2019.4.21 折叠时需要复位模块库
				if( temp != null ) {
					CurrentItemView = temp;
					MyModuleLibPanel.CanelHilight();
				}
			}
			
			if( CurrentItemView == List[ i ] ) {
				CurrentItemIndex = i;
			}
		}
		//一直在条目外移动, 返回
		if( CurrentItemView == null && temp == null ) {
			return;
		}
		//一直在条目内移动, 返回
		if( CurrentItemView != null && CurrentItemView == temp ) {
			return;
		}
		//刚刚移出了条目
		if( CurrentItemView == null && temp != null ) {
			
			//这个是一个防御性设置, 在接下来的Detail 鼠标移动检测中如果移出Detail, 那么需要设置为null
			CurrentItemView = temp;
			
			CurrentItemView.isMouseOn = false;
			
			ModuleDetail.ShowDetail = false;
			
			if( !ModuleDetail.isMouseOn ) {
				//MyModuleLibPanel.CanelHilight();
			}
			
			return;
		}
		
		SelectCurrent();
		
		return;
	}
	
	//显示图像
	public static void Draw( Graphics g, int StartX, int StartY )
	{
		int sx = StartX;
		if( ModuleDetail.ShowDetail ) {
			sx = 0;
		}
		
		Rectangle r = new Rectangle( sx, 0, Width, Height );
		g.FillRectangle( TreeBackBrush, r );
		
		//g.FillRectangle( EageBrush, sx, 105 - 13, 1, Height );
		//g.FillRectangle( EageBrush, sx + Width - 1, 105 - 13, 1, Height );
		//g.FillRectangle( EageBrush, sx, 105 - 13, Width - 1, 1 );
		
		ItemView.NeedRefresh = false;
		
		for( int i = 0; i < ListLength; ++i ) {
			List[ i ].Draw( g, StartX, StartY );
		}
	}
	
	//显示图像
	public static void Draw1( Graphics g, int StartX, int StartY )
	{
		if( CurrentItemView != null ) {
			CurrentItemView.Draw( g, StartX, StartY );
		}
	}
	
	//设置当前条目为选中状态
	public static void SelectCurrent()
	{
		if( CurrentItemView.TypeIndex == 2 ) {
			HardModule.MLIB_SIZE = GUIset.GetPix(120);
		}
		else {
			HardModule.MLIB_SIZE = GUIset.GetPix(60);
		}
		
		//判断是否需要装载模块库
		if( ModuleDetail.myModuleListSet[ CurrentItemIndex ] == null ) {
			
			try {
			
			/*
			if( CurrentItemView.TypeIndex == 2 ) {
				HardModule.MLIB_SIZE = 120;
			}
			else {
				HardModule.MLIB_SIZE = 60;
			}
			*/
			
			//n_Debug.Debug.Message = "装载中, 请稍等...";
			//G.CGPanel.MyRefresh();
			HardModule.isLibTmp = true;
			ModuleDetail.myModuleListSet[ CurrentItemIndex ] = LoadItemModule( List[CurrentItemIndex].Path );
			HardModule.isLibTmp = false;
			//n_Debug.Debug.Message = "";
			}
			catch( Exception e ) {
				n_Debug.Warning.BUG( "无法展开模块列表! 请联系我们改正此问题:\n" + e.ToString() );
				return;
			}
		}
		//刚刚移动到条目内
		ModuleDetail.myModuleList = ModuleDetail.myModuleListSet[ CurrentItemIndex ];
		ModuleDetail.myModuleList.MouseOnObject = null;
		ModuleDetail.RefreshEage();
		
		ModuleDetail.HTopX = CurrentItemView.X;
		ModuleDetail.HTopY = CurrentItemView.Y;
		ModuleDetail.HBottomX = CurrentItemView.X + CurrentItemView.Width*3/4;
		ModuleDetail.HBottomY = CurrentItemView.Y + CurrentItemView.Height;
		
		ModuleDetail.SX = CurrentItemView.X + CurrentItemView.Width;
		ModuleDetail.SY = CurrentItemView.Y + CurrentItemView.Height/2 - ModuleDetail.Height/2;
		
		if( MyModuleLibPanel.SY + ModuleDetail.SY < MPanelTopY ) {
			ModuleDetail.SY = MPanelTopY - MyModuleLibPanel.SY;
		}
		if( MyModuleLibPanel.SY + ModuleDetail.SY + ModuleDetail.Height > n_Head.Head.MaxHeight - 10 ) {
			ModuleDetail.SY = n_Head.Head.MaxHeight - 10 - ModuleDetail.Height - MyModuleLibPanel.SY;
		}
		
		Color c = ((SolidBrush)CurrentItemView.ItemBackBrush).Color;
		
		ModuleDetail.AreaPen.Color = Color.FromArgb( 150, c.R - 30, c.G - 30, c.B - 30 );
		ModuleDetail.AreaPen.Color = Color.FromArgb( 255, 178, 0 );
		
		ModuleDetail.ShowDetail = true;
	}
	
	//判断指定点是否在内部
	static bool isMouseInSide( int mX, int mY )
	{
		if( mX >= SX && mX < SX + Width && mY >= SY && mY < SY + Height  ) {
			return true;
		}
		return false;
	}
	
	//重新计算各个条目的坐标
	public static void RefreshLoc()
	{
		CurrentLine = 0;
		for( int i = 0; i < ListLength; ++i ) {
			
			if( List[i].Path == null || List[i].Visible ) {
				List[i].RefreshLoc( CurrentLine );
				CurrentLine++;
			}
		}
		MyModuleLibPanel.ResetScroll();
	}
	
	static void SetVisible( string GroupText, ItemView l, bool v )
	{
		if( v ) {
			if( SystemData.TreeExpandFlag.IndexOf( "\n" + GroupText + ":" + l.Text + "\n" ) == -1 ) {
				SystemData.TreeExpandFlag += GroupText + ":" + l.Text + "\n";
				SystemData.isChanged = true;
			}
		}
		else {
			SystemData.TreeExpandFlag = SystemData.TreeExpandFlag.Replace( "\n" + GroupText + ":" + l.Text + "\n", "\n" );
			SystemData.isChanged = true;
		}
		if( !SystemData.TreeExpandFlag.StartsWith( "\n" ) ) {
			SystemData.TreeExpandFlag = "\n" + SystemData.TreeExpandFlag;
		}
	}
	
	//-------------------------------------
	
	//进入目录
	static void EnterFolder( string Name, Color c )
	{
		ItemView.backcolor = c;
		
		List[ ListLength ] = new ItemView( Name, null, CurrentLine, CurrentLevel, 0, 0 );
		++ListLength;
		
		++CurrentLevel;
		++CurrentLine;
	}
	
	//退出目录
	static void LeaveFolder()
	{
		--CurrentLevel;
	}
	
	//添加条目
	static void AddNode( string Name, string Path, int st, int end )
	{
		//改为用户点击时候才装载
		//ModuleDetail.myModuleListSet[ ListLength ] = LoadItemModule( Path );
		
		List[ ListLength ] = new ItemView( Name, Path, CurrentLine, CurrentLevel, st, end );
		
		++ListLength;
		++CurrentLine;
	}
	
	//-------------------------------------
	//装载指定目录下的模块
	static MyObjectList LoadItemModule( string Path )
	{
		if( Path == null ) {
			return null;
		}
		MyObjectList myModuleList = new MyObjectList( null );
		//myModuleList.isShowWarning = false;
		myModuleList.isModuleLib = true;
		
		
		string[] PathList = Path.Trim( ',' ).Split( ',' );
		
		int idx = 0;
		
		int X = XPadding;
		int Y = YPadding;
		
		for( int i = 0; i < PathList.Length; ++i ) {
			
			string ExtValue = null;
			
			//根据文件名列表建立对应的模块列表
			
			if( PathList[i] == "<nextcolumn>" ) {
				continue;
			}
			
			if( idx != 0 && idx % 4 == 0 ) {
				
				X = XPadding;
				Y += HardModule.MLIB_SIZE + GUIset.GetPix(45);
				
				//Y = YPadding;
				//X += HardModule.MLIB_SIZE + 40;
			}
			idx++;
			
			if( PathList[i].EndsWith( "=90" ) ) {
				PathList[i] = PathList[i].Remove( PathList[i].Length - 3 );
				ExtValue = "90";
			}
			else {
				ExtValue = null;
			}
			int uX = 0;
			int uY = 0;
			if( PathList[i].EndsWith( ")" ) ) {
				
				int locidnex = PathList[i].LastIndexOf( "(" );
				string loc = PathList[i].Remove( 0, locidnex + 1 );
				loc = loc.Remove( loc.Length - 1 );
				PathList[i] = PathList[i].Remove( locidnex );
				
				string[] cut = loc.Split( '\'' );
				
				uX = int.Parse( cut[0] );
				uY = int.Parse( cut[1] );
			}
			
			int angle = 0;
			MyObject m0 = null;
			
			if( PathList[i].EndsWith( ".lab" ) || PathList[i].EndsWith( ".cx" ) ) {
				m0 = new GNote();
				m0.Width = 100;
				m0.Height = 60;
				m0.myObjectList = myModuleList;
			}
			else {
				m0 = GUIcoder.GetModuleFromMacroFile(  OS.ModuleLibPath + PathList[i] );
				angle = 0;
				if( ExtValue == "90" ) {
					//m.Rol( 1 );
					angle = 90;
				}
				m0.myObjectList = myModuleList;
			}
			
			//设置默认语言
			if( m0 is HardModule ) {
				HardModule m = (HardModule)m0;
				int LanguageIndex = 0;
				for( int li = 0; li < m.LanguageList.Length; ++li ) {
					if( m.LanguageList[ li ] == SystemData.ModuleLanguage ) {
						LanguageIndex = li;
						break;
					}
				}
				//设置名字
				string NewName = m.NameList[ 2 + LanguageIndex ];
				
				//m.SetUserValue( NewName, null, width / 2, height / 2, 0 );
				m.SetUserValue( NewName, null, m.LanguageList[ LanguageIndex ], X, Y, angle, false );
				
				if( m.Width < HardModule.MLIB_SIZE ) {
					m.SX += (HardModule.MLIB_SIZE - m.Width) / 2;
				}
				if( m.Height < HardModule.MLIB_SIZE ) {
					m.SY += (HardModule.MLIB_SIZE - m.Height) / 2;
				}
			}
			if( m0 is GNote ) {
				GNote m = (GNote)m0;
				string showtext = PathList[i].Remove( 0, PathList[i].LastIndexOf( "\\" ) + 1 );
				showtext = "<居中>" + showtext.Remove( showtext.LastIndexOf( '.' ) );
				m.SetUserValue( PathList[i], null, showtext, 0, 0, m.Width, m.Height );
				m.SX = X;
				m.SY = Y;
			}
			//CurrentModule = m;
			myModuleList.Add( m0 );
			
			//Y += HardModule.MLIB_SIZE + 50;
			X += HardModule.MLIB_SIZE + GUIset.GetPix(50);
		}
		
		//G.CGPanel.ShowImage();
		return myModuleList;
	}
	
	public class ItemView
	{
		public string Text;
		public string Path;
		
		public int X;
		public int Y;
		public int Width;
		public int Height;
		public bool isMouseOn;
		
		public bool tempVisible;
		public bool Visible;
		public bool Expand;
		
		public int TypeIndex;
		
		//public int BG_ImgIndex;
		
		public static Font f;
		public static Font fb;
		
		public static Color backcolor;
		public Brush ItemBackBrush;
		
		public static int PerHeight;//35; //30
		
		static Icon ExtImg;
		static Icon ClsImg;
		
		public static bool NeedRefresh;
		
		public static string LastGroupName;
		
		//初始化
		public static void Init()
		{
			NeedRefresh = false;
			
			//这个不能放到后边, 因为需要计算尺寸
			
			// C#获取系统默认字体 System.Drawing.SystemFonts.DefaultFont.Name
    		//f = new System.Drawing.Font(System.Drawing.SystemFonts.DefaultFont.Name, System.Drawing.SystemFonts.DefaultFont.Size);
    		//fb = new System.Drawing.Font(System.Drawing.SystemFonts.DefaultFont.Name, System.Drawing.SystemFonts.DefaultFont.Size, FontStyle.Bold);
    		//MessageBox.Show( System.Drawing.SystemFonts.DefaultFont.Name + " - " + System.Drawing.SystemFonts.DefaultFont.Size );
    		
			SizeF sf = GUIset.mg.MeasureString( "你好", n_GUIset.GUIset.UIFont );
			
			if( SystemData.StartMode == 0 || SystemData.StartMode == 1 ) {
				PerHeight = (int)sf.Height + 28;
			}
			else {
				PerHeight = (int)sf.Height + 10;
			}
			ExtImg = new Icon( n_OS.OS.SystemRoot + @"Resource\gui\GFormControls\Exp.ico" );
			ClsImg = new Icon( n_OS.OS.SystemRoot + @"Resource\gui\GFormControls\Cls.ico" );
		}
		
		//构造函数
		public ItemView( string text, string path, int line, int column, int st_idx, int end_idx )
		{
			Text = text;
			Path = path;
			
			X = 2 + column * 10;
			Y = 2 + line * PerHeight;
			
			Width = ModuleTree.CWidth - X*2;
			Height = PerHeight - 3;
			
			isMouseOn = false;
			
			TypeIndex = st_idx;
			if( path == null ) {
				LastGroupName = text;
			}
			/*
			BG_ImgIndex = 0;
			if( Text == "图文显示功能扩展" ) {
				BG_ImgIndex = 1;
			}
			if( Text == "上位机串口通信" ) {
				BG_ImgIndex = 2;
			}
			if( Text == "声音/灯光功能扩展" ) {
				BG_ImgIndex = 3;
			}
			if( Text == "串口协议功能扩展" ) {
				BG_ImgIndex = 4;
			}
			*/
			
			if( SystemData.TreeExpandFlag.IndexOf( "\n" + LastGroupName + ":" + Text + "\n" ) != -1 ) {
				Expand = true;
				Visible = true;
			}
			else {
				Expand = false;
				Visible = path == null;
			}
			tempVisible = Visible;
			
			//ItemBackBrush = new SolidBrush( Color.FromArgb(140, 165, 190) );
			ItemBackBrush = new SolidBrush( backcolor );
		}
		
		//根据Line刷新坐标
		public void RefreshLoc( int line )
		{
			Y = 2 + line * PerHeight;
		}
		
		//鼠标按下事件
		public bool UserMouseDown( int mX, int mY )
		{
			//if( CurrentItemView == this ) {
			//	ModuleTree.SelectCurrent();
			//}
			return false;
		}
		
		//鼠标松开事件
		public void UserMouseUp( int mX, int mY )
		{
			
		}
		
		//鼠标移动事件
		public bool UserMouseMove( int mX, int mY )
		{
			bool isin = MouseIsInside( mX, mY );
			
			if( Path != null && isin && Visible ) {
				isMouseOn = true;
				CurrentItemView = this;
			}
			else {
				isMouseOn = false;
				
				if( isin && Text.StartsWith( "<" ) ) {
					
					MyModuleLibPanel.CanelHilight();
					
					
					if( G.ModMngBox == null ) {
						G.ModMngBox = new n_ModMngForm.ModMngForm();
					}
					G.ModMngBox.Run();
				}
			}
			return isin;
		}
		
		//绘制条目
		public void Draw( Graphics g, int StartX, int StartY )
		{
			if( Visible != tempVisible ) {
				if( !NeedRefresh ) {
					Visible = tempVisible;
					NeedRefresh = true;
					ModuleTree.RefreshLoc();
				}
			}
			
			if( Path != null && !Visible ) {
				return;
			}
			int sx = StartX + X;
			Rectangle r = new Rectangle( sx, StartY + Y, Width, Height );
			GraphicsPath gp = Shape.CreateRoundedRectanglePath( r );
			
			//判断是否是子条目
			if( Path != null ) {
				Brush TextB = Brushes.Black;
				Brush b = null;
				if( isMouseOn ) {
					
					b = ModuleDetail.BackPanelBrush;
					
					b = Brushes.Yellow;
					
					if( SystemData.isBlack ) {
						TextB = Brushes.WhiteSmoke;
					}
					else {
						//TextB = Brushes.Blue;
					}
				}
				else {
					b = ItemBackBrush;
					TextB = Brushes.Black;
					
					g.FillPath( b, gp );
				}
				
				
				
				//g.FillEllipse( b, sx - Height/2, StartY + Y, Height, Height );
				//g.FillEllipse( b, sx + W - Height/2, StartY + Y, Height, Height );
				//g.DrawRectangle( new Pen( GUIset.myBackColor ), StartX + X, StartY + Y, Width, Height );
				
				//绘制说明
				if( SystemData.StartMode == 0 || SystemData.StartMode == 1 ) {
					g.DrawString( Text, n_GUIset.GUIset.UIFont, TextB, StartX + X + 10 + 30, StartY + Y + 3 + 9 );
				}
				else {
					g.DrawString( Text, n_GUIset.GUIset.UIFont, TextB, StartX + X + 10, StartY + Y + 3 );
				}
			}
			//判断是否为分组名字
			else {
				Brush TextB = Brushes.Black;
				Font ft = f;
				if( Text.StartsWith( "——" ) ) {
					TextB = Brushes.LightSteelBlue;
				}
				else if( Text.StartsWith( "<" ) ) {
					
					TextB = Brushes.LightSlateGray;
					g.FillPath( Brushes.LightSteelBlue, gp );
				}
				else {
					GraphicsState gs = g.Save();
					g.InterpolationMode = System.Drawing.Drawing2D.InterpolationMode.Default;
					g.PixelOffsetMode = System.Drawing.Drawing2D.PixelOffsetMode.Half;
					//g.SmoothingMode = SmoothingMode.HighQuality;
					if( Expand ) {
						g.DrawIcon( ClsImg, StartX + X + 10, StartY + Y + 5 );
						TextB = Brushes.Black;
						ft = fb;
					}
					else {
						g.DrawIcon( ExtImg, StartX + X + 10, StartY + Y + 5 );
					}
					g.Restore( gs );
				}
				
				//绘制说明
				if( SystemData.StartMode == 0 || SystemData.StartMode == 1 ) {
					g.DrawString( Text, n_GUIset.GUIset.UIFont, TextB, StartX + X + 30 + 30, StartY + Y + 3 + 9 );
				}
				else {
					g.DrawString( Text, n_GUIset.GUIset.UIFont, TextB, StartX + X + 30, StartY + Y + 3 );
				}
			}
		}
		
		//判断鼠标是否在当前组件上
		bool MouseIsInside( int mX, int mY )
		{
			if( mX >= X && mX <= X + Width && mY >= Y && mY <= Y + Height ) {
				return true;
			}
			else {
				return false;
			}
		}
	}
}
}


