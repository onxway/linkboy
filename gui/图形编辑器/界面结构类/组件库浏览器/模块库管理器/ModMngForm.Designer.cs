﻿/*
 * 由 SharpDevelop 创建。
 * 用户: bbb_2
 * 日期: 2009-11-17
 * 时间: 11:15
 * 
 * 如果你想要更改该模板，那么请使用“工具 | 选项 | 编辑 | 编辑标准标题”。
 */
namespace n_ModMngForm
{
	partial class ModMngForm
	{
		/// <summary>
		/// Designer variable used to keep track of non-visual components.
		/// </summary>
		private System.ComponentModel.IContainer components = null;
		
		/// <summary>
		/// Disposes resources used by the form.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing) {
				if (components != null) {
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}
		
		/// <summary>
		/// This method is required for Windows Forms designer support.
		/// Do not change the method contents inside the source code editor. The Forms designer might
		/// not be able to load this method if it was changed manually.
		/// </summary>
		private void InitializeComponent()
		{
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ModMngForm));
			this.buttonOk = new System.Windows.Forms.Button();
			this.labelMes = new System.Windows.Forms.Label();
			this.buttonOther = new System.Windows.Forms.Button();
			this.panel1 = new System.Windows.Forms.Panel();
			this.SuspendLayout();
			// 
			// buttonOk
			// 
			this.buttonOk.BackColor = System.Drawing.Color.CornflowerBlue;
			resources.ApplyResources(this.buttonOk, "buttonOk");
			this.buttonOk.Name = "buttonOk";
			this.buttonOk.UseVisualStyleBackColor = false;
			this.buttonOk.Click += new System.EventHandler(this.ButtonOkClick);
			// 
			// labelMes
			// 
			resources.ApplyResources(this.labelMes, "labelMes");
			this.labelMes.ForeColor = System.Drawing.Color.LightSlateGray;
			this.labelMes.Name = "labelMes";
			// 
			// buttonOther
			// 
			this.buttonOther.BackColor = System.Drawing.Color.Silver;
			resources.ApplyResources(this.buttonOther, "buttonOther");
			this.buttonOther.Name = "buttonOther";
			this.buttonOther.UseVisualStyleBackColor = false;
			this.buttonOther.Click += new System.EventHandler(this.ButtonOtherClick);
			// 
			// panel1
			// 
			resources.ApplyResources(this.panel1, "panel1");
			this.panel1.Name = "panel1";
			// 
			// ModMngForm
			// 
			resources.ApplyResources(this, "$this");
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.BackColor = System.Drawing.Color.WhiteSmoke;
			this.Controls.Add(this.panel1);
			this.Controls.Add(this.buttonOther);
			this.Controls.Add(this.buttonOk);
			this.Controls.Add(this.labelMes);
			this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
			this.MaximizeBox = false;
			this.MinimizeBox = false;
			this.Name = "ModMngForm";
			this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.UnitLibFormFormClosing);
			this.ResumeLayout(false);
		}
		private System.Windows.Forms.Panel panel1;
		private System.Windows.Forms.Button buttonOther;
		private System.Windows.Forms.Label labelMes;
		private System.Windows.Forms.Button buttonOk;
	}
}
