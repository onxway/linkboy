﻿
namespace n_ModMngForm
{
using System;
using System.Drawing;
using System.Windows.Forms;
using n_MainSystemData;

/// <summary>
/// Description of WorkForm.
/// </summary>
public partial class ModMngForm : Form
{
	//主窗口
	public ModMngForm()
	{
		InitializeComponent();
	}
	
	//运行
	public void Run()
	{
		//labelTest.Text = "<" + SystemData.LibOtherShow + ">\n<" + n_ModuleLibDecoder.ModuleLibDecoder.OtherPath + ">";
		
		panel1.Controls.Clear();
		string[] List = n_ModuleLibDecoder.ModuleLibDecoder.OtherPath.TrimEnd( ';' ).Split( ';' );
		for( int i = 0; i < List.Length; ++i ) {
			CheckBox c = new CheckBox();
			c.Font = panel1.Font;
			c.Location = new Point( 20, 20 + i * 35 );
			c.Width = panel1.Width;
			c.Text = List[i];
			c.Visible = true;
			c.Checked = n_MainSystemData.SystemData.LibOtherShow.IndexOf( ";" + List[i] + ";" ) != -1;
			panel1.Controls.Add( c );
		}
		
		this.Visible = true;
	}
	
	//窗体关闭事件
	void UnitLibFormFormClosing(object sender, FormClosingEventArgs e)
	{
		e.Cancel = true;
		this.Visible = false;
	}
	
	void ButtonOkClick(object sender, EventArgs e)
	{
		string temp = ";";
		foreach( Control c in panel1.Controls ) {
			CheckBox cb = (CheckBox)c;
			if( cb.Checked ) {
				temp += cb.Text + ";";
			}
		}
		SystemData.LibOtherShow = temp;
		SystemData.isChanged = true;
		
		Visible = false;
	}
		
	void ButtonOtherClick(object sender, EventArgs e)
	{
		MessageBox.Show( "系统将打开一个指定文件夹, 请自行把第三方厂商库文件夹拷贝到此目录, 并重启linkboy软件即可使用。\n如果第三方厂商添加模块到linkboy中遇到问题或者需要请我们帮忙添加，可以到官网联系我们或者邮箱 910360201@qq.com", "添加第三方厂商库提示" );
		string fileToSelect = n_OS.OS.SystemRoot + "Lib\\第三方模块库\\说明.txt";
		string args = string.Format("/Select, {0}", fileToSelect);
		System.Diagnostics.Process.Start( new System.Diagnostics.ProcessStartInfo("Explorer.exe", args ) );
	}
}
}



