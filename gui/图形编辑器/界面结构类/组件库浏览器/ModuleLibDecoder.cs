﻿
namespace n_ModuleLibDecoder
{
//命名空间
using System;
using System.Drawing;
using System.Windows.Forms;
using System.IO;
using n_Config;
using n_OS;

//组件库类
public static class ModuleLibDecoder
{
	public delegate void DeleEnterFolder( string Name, Color c ); public static DeleEnterFolder deleEnterFolder;
	public delegate void DeleLeaveFolder(); public static DeleLeaveFolder deleLeaveFolder;
	public delegate void DeleAddNode( string Name, string Path, int st, int end ); public static DeleAddNode deleAddNode;
	
	public static string CurrentFolderName;
	static string LibConfig;
	static bool ignoreFold;
	
	//进度条处理
	static int LastPecent;
	static int MaxLength;
	static void DealProgress( int i )
	{
		int CPencent = i * 11 / MaxLength;
		if( CPencent != LastPecent ) {
			LastPecent = CPencent;
			if( G.InitMes != null ) {
				//G.InitMes( "■" );
			}
		}
	}
	
	public static string TempLibName = null;
	
	public static bool Sale = false;
	
	//创客加速器不显示套装
	public static bool NotShowPack = false;
	
	static string LibList;
	static string tempName = null;
	
	static string PrePath = null;
	
	public static bool TestFilePathMode = false;
	
	public static string OtherPath = null;
	
	static char LabIndex;
	
	static int TypeIndex;
	
	public static string[] LibLine;
	
	//系统初始化
	public static void Init()
	{
		//读取库配置文件
		if( TempLibName == null ) {
			TempLibName = OS.USER;
		}
		PrePath = null;
		string LibConfigPath = OS.SystemRoot + "RV" + OS.PATH_S + OS.USER + OS.PATH_S + "libconfig.txt";
		
		if( Sale ) {
			LibConfigPath = OS.SystemRoot + "sercenter" + OS.PATH_S + "libconfig_linkboy.txt";
		}
		
		string LibPath = OS.ModuleLibPath + TempLibName + ".txt";
		
		if( G.GuangZhouTest ) {
			LibPath = OS.ModuleLibPath + "linkboy_gz.txt";
		}
		
		LibConfig = VIO.OpenTextFileGB2312( LibConfigPath );
		//LibConfig = LibConfig.Split( '\n' )[0];
		
		LibList = VIO.OpenTextFileGB2312( LibPath );
		
		
		//读取第三方模块库列表
		string other = ReadOther();
		if( Sale ) {
			other = "";
		}
		LibList = LibList.Replace( "<<other>>", other );
	}
	
	//初始化, 加载元件库列表
	public static void Load( char tindex )
	{
		LabIndex = tindex;
		
		string[] Line = LibList.Split( '\n' );
		LibLine = Line;
		
		for( int i = 0; i < Line.Length; ++i ) {
			Line[ i ] = Line[ i ].Trim( " \t".ToCharArray() );
			if( Line[ i ].StartsWith( "//" ) ) {
				Line[ i ] = "";
			}
		}
		LastPecent = 0;
		MaxLength = Line.Length;
		
		TypeIndex = 0;
		int Index = 0;
		ReadFolderToNode( Line, ref Index );
	}
	
	//读取第三方模块列表
	static string ReadOther()
	{
		string Result = "";
		
		string sSourcePath = n_OS.OS.SystemRoot + @"Lib\第三方模块库";
		
		//在指定目录及子目录下查找文件,在list中列出子目录及文件
		DirectoryInfo Dir = new DirectoryInfo( sSourcePath );
		DirectoryInfo[] DirSub = Dir.GetDirectories();
		
		OtherPath = "";
		
		//遍历所有的子文件夹
		foreach( DirectoryInfo d in DirSub ) {
			
			OtherPath += d.Name + ";";
			
			if( n_MainSystemData.SystemData.LibOtherShow.IndexOf( ";" + d.Name + ";" ) == -1 ) {
				continue;
			}
			
			string path = Dir + "\\" + d + "\\list.txt";
			
			if( !File.Exists( path ) ) {
				continue;
			}
			
			
			string s = n_OS.VIO.OpenTextFileGB2312( path );
			
			Result += "<setpath>第三方模块库" + "\\" + d + "\\\n";
			Result += s;
		}
		return Result;
	}
	
	//测试路径
	public static void TestPath()
	{
		TestFilePathMode = true;
		deleEnterFolder = null;
		deleLeaveFolder = null;
		deleAddNode = null;
		Load( n_GUIcoder.labType.Default );
	}
	
	//判断指定的模块是否为过时模块
	public static bool isOld( string path )
	{
		return LibList.IndexOf( path ) == -1 && !path.StartsWith( OS.UserLibName );
	}
	
	//从指定行开始添加元素到指定的节点上,遇到目录结束时返回
	static void ReadFolderToNode( string[] Line, ref int Index )
	{
		for( ;Index < Line.Length; ++Index ) {
			
			
			DealProgress( Index );
			
			
			string LineData = Line[ Index ];
			
			
			//如果是空项,跳过
			if( LineData == "" ) {
				continue;
			}
			//如果是设置目录, 记录目录名字
			if( LineData.StartsWith( "<setpath>" ) ) {
				LineData = LineData.Remove( 0, 9 );
				PrePath = LineData;
				continue;
			}
			//如果是目录结束,返回
			if( LineData.StartsWith( "</fold>" ) ) {
				if( !ignoreFold ) {
					if( deleLeaveFolder != null ) {
						deleLeaveFolder();
					}
				}
				break;
			}
			//如果是新目录,则创建节点并递归调用自身
			if( LineData.StartsWith( "<fold>" ) ) {
				
				DealProgress( Index );
				
				string L_Type = null;
				int Linx = LineData.LastIndexOf( ';' );
				if( Linx != -1 ) {
					L_Type = LineData.Remove( 0, Linx + 1 );
					LineData = LineData.Remove( Linx );
				}
				
				int ld = LineData.LastIndexOf( ',' );
				string fcolor = LineData.Remove( 0, ld + 1 );
				string FoldName = LineData.Remove( ld ).Remove( 0, 6 );
				string[] ss = fcolor.Split( '-' );
				
				Color c = Color.FromArgb( int.Parse( ss[0] ), int.Parse( ss[1] ), int.Parse( ss[2] ) );
				
				ignoreFold = LibConfig.IndexOf( "\n" + FoldName + "\n" ) != -1;
				
				//如果是创客加速器中启动的, 或者是linkboy启动, 则不显示temp
				if( (NotShowPack || !Sale) && FoldName == "temp,temp" ) {
					ignoreFold = true;
				}
				if( LabIndex != n_GUIcoder.labType.Default && L_Type != null && L_Type.IndexOf( LabIndex ) == -1 ) {
					ignoreFold = true;
				}
				
				if( FoldName.StartsWith( "—" ) ) {
					TypeIndex++;
				}
				
				tempName = FoldName;
				
				//选择语言版本
				string[] s0 = FoldName.Split( ',' );
				
				if( n_Language.Language.isChinese ) {
					FoldName = s0[0];
				}
				else {
					FoldName = s0[1];
				}
				
				CurrentFolderName = FoldName;
				
				//模块库中可能有纯销售类的模块, 需要隐藏
				//if( !Sale && CurrentFolderName.StartsWith( "temp" ) ) {
				//	ignoreFold = true;
				//}
				
				if( !ignoreFold ) {
					if( deleEnterFolder != null ) {
						deleEnterFolder( FoldName, c );
					}
				}
				++Index;
				ReadFolderToNode( Line, ref Index );
				continue;
			}
			//到这里说明是条目项
			if( LineData.StartsWith( "<item>" ) ) {
				
				DealProgress( Index );
				
				if( !LineData.EndsWith( "," ) ) {
					MessageBox.Show( "<item> 缺少结尾逗号: " + LineData );
					continue;
				}
				string ItemName = LineData.Remove( LineData.Length - 1 ).Remove( 0, 6 );
				bool ignoreItem = LibConfig.IndexOf( "\n" + tempName + ":" + ItemName + "\n" ) != -1;
				
				bool ignoreOld = !n_MainSystemData.SystemData.ShowOldModule && ItemName == "过时模块";
				
				//选择语言版本
				string[] s = ItemName.Split( ',' );
				if( n_Language.Language.isChinese ) {
					ItemName = s[0];
				}
				else {
					ItemName = s[1];
				}
				
				string Path = null;
				int st = Index;
				for( int i = Index + 1; i < Line.Length; ++i ) {
					if( Line[ i ] == "" ) {
						continue;
					}
					if( Line[ i ] == "</item>" ) {
						Index = i;
						break;
					}
					if( ignoreFold || ignoreItem || ignoreOld ) {
						continue;
					}
					if( Line[ i ] != "<nextcolumn>" ) {
						string sPath = Line[ i ].Remove( 0, 1 + Line[ i ].IndexOf( ' ' ) );
						if( !sPath.EndsWith( "," ) ) {
							MessageBox.Show( "路径项缺少结尾逗号: " + sPath );
							continue;
						}
						sPath = sPath.Remove( sPath.Length - 1 );
						
						//判断是否为服务中心不显示的模块
						if( sPath.EndsWith( ",SS:N" ) ) {
							if( Sale ) {
								continue;
							}
							sPath = sPath.Remove( sPath.Length - 5 );
						}
						//判断是否为只在服务中心才显示的模块
						if( sPath.EndsWith( ",SS:O" ) ) {
							if( !Sale ) {
								continue;
							}
							sPath = sPath.Remove( sPath.Length - 5 );
						}
						//判断是否需要旋转
						string ExtValue = null;
						if( sPath.EndsWith( ",90" ) ) {
							sPath = sPath.Remove( sPath.Length - 3 );
							ExtValue = "=90";
						}
						else {
							ExtValue = null;
						}
						
						
						//感觉一般不需要判断文件是否存在
						if( TestFilePathMode && sPath != "var" && !sPath.StartsWith( ":" ) && !sPath.StartsWith( "s:" ) && !File.Exists( OS.ModuleLibPath + PrePath + sPath ) ) {
							MessageBox.Show( "不存在的库文件: " + OS.ModuleLibPath + sPath );
							continue;
						}
						//判断是否为重定位为系统库目录
						if( sPath.StartsWith( "s:" ) ) {
							sPath = sPath.Remove( 0, 2 );
							Path += sPath + ExtValue + ",";
						}
						else {
							Path += PrePath + sPath + ExtValue + ",";
						}
					}
					else {
						Path += Line[ i ] + ",";
					}
				}
				if( Path != null ) {
					if( deleAddNode != null ) {
						deleAddNode( ItemName, Path, TypeIndex, 0 );
					}
				}
				continue;
			}
			
			//到这里为未知项
			MessageBox.Show( "装载组件库时遇到未知的类型: [" + LineData + "]" + (LineData.Length.ToString() + " - " + (int)LineData[0]).ToString() );
		}
	}
}
}


