﻿
namespace n_Head_MtButton
{
using System;
using System.Drawing;
using n_MyObjectList;
using System.Windows.Forms;
using n_GUIset;
using n_Shape;
using System.Drawing.Drawing2D;

public class C_MyButton
{
	public delegate void D_MouseDownEvent();
	public D_MouseDownEvent MouseDownEvent;
	public delegate void D_MouseUpEvent();
	public D_MouseUpEvent MouseUpEvent;
	
	public bool isMouseOn;
	public Brush TipBrush;
	
	
	public Image tempBackImage0;
	public Image tempBackImage;
	public Image tempBackImage1;
	public Image tempBackImage2;
	public string tempValue;
	
	public Image BackImage;
	public Image BackImage1;
	public Image BackImage2;
	public SolidBrush BackBrush0;
	public SolidBrush BackBrush;
	
	public Pen BackPen0;
	
	public string Text;
	public string TipText;
	
	public Image D2Image;
	
	public bool isPress;
	
	int ChangeIndex = 0;
	
	public bool Visible;
	
	public int X;
	public int Y;
	public int Width;
	public int Height;
	
	public Brush TextBrush;
	
	public E_ButtonType ButtonType;
	public enum E_ButtonType {
		BackColor, ScaleImage, Opacity, EageLineImage, SwitchImage, Text, UText
	}
	
	public static Image tempGameImg;
	static SolidBrush buttonbr;
	
	//初始化
	public static void Init( string p )
	{
		tempGameImg = new Bitmap( p );
		buttonbr = new SolidBrush( Color.FromArgb( 228, 228, 228) );
	}
	
	//构造函数
	public C_MyButton( E_ButtonType bt, string path, int width, int height, SolidBrush backBrush )
	{
		ButtonType = bt;
		
		Width = width;
		Height = height;
		
		BackBrush = backBrush;
		
		if( bt == E_ButtonType.EageLineImage ) {
			BackPen0 = new Pen( Color.SlateGray, 2 );
		}
		
		if( path != null ) {
			BackImage = new Bitmap( path );
		}
		isPress = false;
		isMouseOn = false;
		
		Visible = true;
		Text = null;
		
		if( Width == 0 && Height == 0 ) {
			Width = BackImage.Width;
			Height = BackImage.Height;
		}
		TextBrush = Brushes.WhiteSmoke;
	}
	
	//构造函数
	public C_MyButton( E_ButtonType bt, string path, string path1, string path2, int width, int height, SolidBrush backBrush )
	{
		ButtonType = bt;
		
		Width = width;
		Height = height;
		
		BackBrush = backBrush;
		
		if( path != null ) {
			BackImage = new Bitmap( path );
		}
		if( path1 != null ) {
			BackImage1 = new Bitmap( path1 );
		}
		if( path2 != null ) {
			BackImage2 = new Bitmap( path2 );
		}
		isMouseOn = false;
		Visible = true;
		Text = null;
		
		if( Width == 0 && Height == 0 ) {
			Width = BackImage.Width;
			Height = BackImage.Height;
		}
	}
	
	//构造函数
	public C_MyButton( E_ButtonType bt, int width, int height, SolidBrush backBrush )
	{
		ButtonType = bt;
		
		Width = width;
		Height = height;
		
		BackBrush = backBrush;
		BackBrush0 = new SolidBrush( Color.Black );
		
		Visible = true;
		isMouseOn = false;
		
		Text = null;
	}
	
	//构造函数
	public C_MyButton( E_ButtonType bt, string path, SolidBrush backBrush )
	{
		ButtonType = bt;
		
		BackBrush = backBrush;
		
		BackImage = new Bitmap( path );
		
		Width = BackImage.Width;
		Height = BackImage.Height;
		
		isMouseOn = false;
		Visible = true;
		Text = null;
		
		if( Width == 0 && Height == 0 ) {
			Width = BackImage.Width;
			Height = BackImage.Height;
		}
	}
	
	//设置坐标
	public void SetLocation( Point p )
	{
		X = p.X;
		Y = p.Y;
	}
	
	//绘制按钮样式
	public void Draw( Graphics g )
	{
		if( !Visible ) {
			return;
		}
		//填充背景颜色按钮类型
		if( ButtonType == E_ButtonType.BackColor ) {
			if( isMouseOn ){
				if( BackBrush != null ) {
					g.FillRectangle( BackBrush, X, Y, Width - 1, Height - 1 );
				}
			}
			else {
				if( BackBrush0 != null ) {
					g.FillRectangle( BackBrush0, X, Y, Width - 1, Height - 1 );
				}
			}
			if( BackImage != null ) {
				g.DrawImage( BackImage, X , Y , Width, Height );
			}
		}
		//图片放缩按钮类型
		else if( ButtonType == E_ButtonType.Opacity ) {
			/*
			if( isMouseOn ) {
				n_SG.SG.MBitmap.Draw( BackImage, X, Y, Width, Height, 100 );
			}
			else {
				n_SG.SG.MBitmap.Draw( BackImage, X, Y, Width, Height, 50 );
			}
			*/
		}
		else if( ButtonType == E_ButtonType.EageLineImage ) {
			if( isMouseOn ) {
				g.DrawRectangle( BackPen0, X - 3, Y - 3, Width + 6, Height + 6 );
			}
			g.DrawImage( BackImage, X, Y, Width, Height );
		}
		//图片放缩按钮类型
		else if( ButtonType == E_ButtonType.ScaleImage ) {
			
			Image img = BackImage;
			if( tempValue != null ) {
				img = tempBackImage;
				if( tempBackImage2 != null && G.SimulateMode && n_GUIcoder.GUIcoder.isStep ) {
					img = tempBackImage2;
				}
			}
			else {
				if( tempBackImage1 != null && !G.SimulateMode && n_GUIcoder.GUIcoder.isStep ) {
					img = tempBackImage1;
				}
			}
			float ox = 0;
			
			if( isMouseOn ) {
				
				//正方形按钮
				if( Width == Height ) {
					g.FillEllipse( buttonbr, X, Y, Width, Height );
					g.DrawEllipse( Pens.Silver, X, Y, Width, Height );
				}
				//长方形按钮
				else {
					Rectangle r = new Rectangle( X, Y, Width, Height );
					GraphicsPath gp = Shape.CreateCircleRectanglePath( r );
					g.FillPath( buttonbr, gp );
					g.DrawPath( Pens.Silver, gp );
				}
			}
			else {
				//正方形按钮
				if( Width == Height ) {
					//g.FillEllipse( Brushes.WhiteSmoke, X, Y, Width, Height );
					//g.DrawEllipse( Pens.Silver, X, Y, Width, Height );
				}
				//长方形按钮
				else {
					//Rectangle r = new Rectangle( X, Y, Width, Height );
					//GraphicsPath gp = Shape.CreateCircleRectanglePath( r );
					//g.FillPath( Brushes.WhiteSmoke, gp );
					//g.DrawPath( Pens.Silver, gp );
				}
			}
			
			g.DrawImage( img, X - ox, Y - ox, Width + ox*2, Height + ox*2 );
			if( ChangeIndex != 0 && ChangeIndex != 360 ) {
				//g.Restore( gs );
			}
			//g.DrawEllipse( Pens.WhiteSmoke, X - ox, Y - ox, Width + ox*2 - 1, Height + ox*2 - 1 );
		}
		//图片切换按钮类型
		else if( ButtonType == E_ButtonType.SwitchImage ) {
			
			if( tempValue != null ) {
				if( isPress ) {
					g.DrawImage( tempBackImage0, X, Y, Width, Height );
				}
				else if( isMouseOn ) {
					g.DrawImage( tempBackImage, X, Y, Width, Height );
				}
				else {
					g.DrawImage( tempBackImage0, X, Y, Width, Height );
				}
			}
			else {
				if( isPress ) {
					g.DrawImage( BackImage2, X, Y, Width, Height );
				}
				else if( isMouseOn ) {
					g.DrawImage( BackImage1, X, Y, Width, Height );
				}
				else {
					g.DrawImage( BackImage, X, Y, Width, Height );
				}
			}
			if( Text != null && isMouseOn ) {
				
				string tempText = Text;
				if( tempValue != null ) {
					tempText = tempValue;
				}
				/*
				int W = (int)GUIset.mg.MeasureString( tempText, GUIset.ExpFont ).Width + 10;
				int H = (int)GUIset.mg.MeasureString( tempText, GUIset.ExpFont ).Height + 10;
				
				Rectangle r = new Rectangle( X + Width/2, Y + Height + 20, W, H );
				StringFormat sf = new StringFormat();
				sf.Alignment = StringAlignment.Center;
				sf.LineAlignment = StringAlignment.Center;
				
				//绘制三角形
				Point P0 = new Point( X + Width/2, Y + Height + 10 );
				Point P1 = new Point( X + Width/2, Y + Height + 25 );
				Point P2 = new Point( X + Width/2 + 15, Y + Height + 25 );
				Point[] PA = new Point[]{P0, P1, P2};
				g.FillPolygon( TipBrush, PA );
				
				if( D2Image != null ) {
					H += D2Image.Height * W / D2Image.Width + 20;
					Rectangle rr = new Rectangle( X + Width/2, Y + Height + 20, W, H );
					GraphicsPath gp = Shape.CreateRoundedRectanglePath( rr );
					g.FillPath( TipBrush, gp );
					g.DrawString( Text, GUIset.ExpFont, Brushes.WhiteSmoke, r, sf );
					g.DrawImage( D2Image, X + Width/2 + 1, Y + Height + 20 + 50, W - 1, D2Image.Height * W / D2Image.Width );
				}
				else {
					GraphicsPath gp = Shape.CreateRoundedRectanglePath( r );
					g.FillPath( TipBrush, gp );
					g.DrawString( tempText, GUIset.ExpFont, Brushes.WhiteSmoke, r, sf );
				}
				*/
				
				g.DrawString( tempText, GUIset.UIFont, Brushes.Black, n_Head.Head.FreeStartX, 15 );
			}
		}
		//填充背景颜色文本按钮类型
		else if( ButtonType == E_ButtonType.Text ) {
			
			g.FillRectangle( BackBrush, X, Y, Width, Height );
			
			g.FillRectangle( BackBrush0, X, Y, Width, GUIset.GetPix( 5 ) );
			//g.FillRectangle( BackBrush0, X, Y, 1, Height );
			//g.FillRectangle( BackBrush0, X + Width - 1, Y, 1, Height );
			
			if( n_Language.Language.isChinese ) {
				g.DrawString( Text, GUIset.UIFont, TextBrush, X +  GUIset.GetPix(18), Y +  GUIset.GetPix(8) );
			}
			else {
				g.DrawString( Text, GUIset.UIFont, TextBrush, X +  GUIset.GetPix(1), Y +  GUIset.GetPix(8) );
			}
			
		}
		//图片切换按钮类型
		else if( ButtonType == E_ButtonType.UText ) {
			
			if( isPress ) {
				g.FillRectangle( Brushes.LightSlateGray, X, Y, Width, Height );
				g.DrawRectangle( Pens.Blue, X, Y, Width, Height );
			}
			else if( isMouseOn ) {
				g.FillRectangle( BackBrush, X, Y, Width, Height );
				g.DrawRectangle( Pens.Black, X, Y, Width, Height );
			}
			else {
				g.FillRectangle( BackBrush, X, Y, Width, Height );
			}
			
			g.DrawString( Text, GUIset.ExpFont, Brushes.Black, X + 3, Y + 13 );
		}
		//未知类型
		else {
			
		}
		
		if( TipText != null && isMouseOn ) {
			//G.CGPanel.GHead.ShowTip = true;
			
			/*
			g.FillRectangle( Brushes.SlateGray, 0, n_Head.Head.HeadLabelHeight - 2, n_Head.Head.MaxWidth, 24 );
			if( GUIset.BlackHead ) {
				g.DrawString( TipText, GUIset.Font12, Brushes.WhiteSmoke, 30, n_Head.Head.HeadLabelHeight - 2 );
			}
			else {
				g.DrawString( TipText, GUIset.Font12, Brushes.Black, 40, n_Head.Head.HeadLabelHeight - 2 );
			}
			*/
			
			int ii = n_Head.Head.COnIndex - n_Head.Head.ExtStartIndex;
			if( ii == 0 || ii == 1 ) {
				int sx = n_Head.Head.MaxWidth - 245;
				g.DrawString( TipText, GUIset.UIFont, Brushes.SlateGray, sx, n_Head.Head.HeadLabelHeight + 40 - 4 );
			}
			else {
				int sx = n_Head.Head.HeadWidth + n_Head.Head.ShowX + 10;
				g.DrawString( TipText, GUIset.UIFont, Brushes.SlateGray, sx, n_Head.Head.HeadLabelHeight - 2 );
			}
		}
	}
	
	//鼠标按下事件
	public bool MouseDown( int mX, int mY )
	{
		if( !Visible ) {
			return false;
		}
		if( isMouseOn && MouseDownEvent != null ) {
			MouseDownEvent();
			isPress = true;
			return true;
		}
		return false;
	}
	
	//鼠标松开事件
	public void MouseUp( int mX, int mY )
	{
		if( !Visible ) {
			return;
		}
		if( isMouseOn && MouseUpEvent != null ) {
			MouseUpEvent();
		}
		isPress = false;
	}
	
	//鼠标移动事件
	public bool MouseMove( int mX, int mY )
	{
		if( !Visible ) {
			return false;
		}
		isMouseOn = MouseIsInside( mX, mY );
		return isMouseOn;
	}
	
	//判断鼠标是否在当前组件上
	bool MouseIsInside( int mX, int mY )
	{
		if( mX >= X && mX <= X + Width && mY >= Y && mY <= Y + Height ) {
			return true;
		}
		else {
			return false;
		}
	}
}
}



