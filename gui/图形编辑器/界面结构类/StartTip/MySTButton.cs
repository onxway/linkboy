﻿
namespace n_MySTButton
{
using System;
using System.Drawing;
using n_MyObjectList;
using System.Windows.Forms;
using n_GUIset;
using n_Shape;
using System.Drawing.Drawing2D;

public class MySTButton
{
	public delegate void D_MouseDownEvent( MySTButton sender );
	public D_MouseDownEvent MouseDownEvent;
	public delegate void D_MouseUpEvent( MySTButton sender );
	public D_MouseUpEvent MouseUpEvent;
	
	public bool isMouseOn;
	public Brush TipBrush;
	
	public Image BackImage;
	
	public string Text;
	
	public bool Visible;
	
	public int SX;
	public int SY;
	public int Width;
	public int Height;
	
	public string FilePath;
	
	bool isPress;
	
	//构造函数
	public MySTButton( string path, string fpath, int x, int y, int h )
	{
		//TipBrush = tb;
		
		FilePath = fpath;
		
		if( path != null ) {
			BackImage = new Bitmap( path );
		}
		isPress = false;
		isMouseOn = false;
		
		Visible = true;
		Text = null;
		
		SX = x;
		SY = y;
		
		//if( Width == 0 && Height == 0 ) {
			Height = h;
			Width = BackImage.Width * h / BackImage.Height;
		//}
	}
	
	//绘制按钮样式
	public void Draw( Graphics g, int StartY )
	{
		if( !Visible ) {
			return;
		}
		
		if( isPress ) {
			g.DrawRectangle( Pens.Blue, SX - 4, SY - 4, Width + 8, Height + 8 );
		}
		else if( isMouseOn ) {
			g.DrawRectangle( Pens.OrangeRed, SX - 4, SY - 4, Width + 8, Height + 8 );
		}
		else {
			//...
		}
		g.DrawImage( BackImage, SX, SY, Width, Height );
		
		if( Text != null && isMouseOn ) {
			
			int W = (int)GUIset.mg.MeasureString( Text, GUIset.ExpFont ).Width + 10;
			int H = (int)GUIset.mg.MeasureString( Text, GUIset.ExpFont ).Height + 10;
			
			Rectangle r = new Rectangle( SX + Width/2, SY + Height + 20, W, H );
			StringFormat sf = new StringFormat();
			sf.Alignment = StringAlignment.Center;
			sf.LineAlignment = StringAlignment.Center;
			
			//绘制三角形
			Point P0 = new Point( SX + Width/2, SY + Height + 10 );
			Point P1 = new Point( SX + Width/2, SY + Height + 25 );
			Point P2 = new Point( SX + Width/2 + 15, SY + Height + 25 );
			Point[] PA = new Point[]{P0, P1, P2};
			g.FillPolygon( TipBrush, PA );
			
			GraphicsPath gp = Shape.CreateRoundedRectanglePath( r );
			g.FillPath( TipBrush, gp );
			g.DrawString( Text, GUIset.ExpFont, Brushes.WhiteSmoke, r, sf );
		}
	}
	
	//鼠标按下事件
	public bool MouseDown( int mX, int mY )
	{
		if( !Visible ) {
			return false;
		}
		if( isMouseOn ) {
			if( MouseDownEvent != null ) {
				MouseDownEvent( this );
			}
			isPress = true;
			return true;
		}
		return false;
	}
	
	//鼠标松开事件
	public void MouseUp( int mX, int mY )
	{
		if( !Visible ) {
			return;
		}
		if( isPress ) {
			if( MouseUpEvent != null ) {
				MouseUpEvent( this );
			}
			isPress = false;
		}
	}
	
	//鼠标移动事件
	public void MouseMove( int mX, int mY )
	{
		if( !Visible ) {
			return;
		}
		isMouseOn = MouseIsInside( mX, mY );
	}
	
	//判断鼠标是否在当前组件上
	bool MouseIsInside( int mX, int mY )
	{
		if( mX >= SX && mX <= SX + Width && mY >= SY && mY <= SY + Height ) {
			return true;
		}
		else {
			return false;
		}
	}
}
}



