﻿
namespace n_StartTip
{
using System;
using System.Drawing;
using System.Drawing.Imaging;
using System.Drawing.Drawing2D;
using System.Windows.Forms;

using n_Compiler;
using n_GUIset;
using n_Head_MtButton;
using n_ImagePanel;
using n_MyObjectList;
using n_OS;
using n_ISP;
using n_MySTButton;

//*****************************************************
//标题栏类
public class StartTip
{
	public delegate void D_OpenFile( bool ig, string Path );
	//public D_OpenFile OpenFile;
	
	MyObjectList myModuleList;
	
	Font f;
	
	Image NoteImageStep;
	Image NoteImageMember;
	Image NoteImageOper;
	Image NoteImageConti;
	
	MySTButton[] C_MyButtonList;
	int C_MyButtonListLength;
	
	int StartY;
	
	float o1;
	float o2;
	float o3;
	float o4;
	
	int xx1;
	int xx2;
	int xx3;
	int xx4;
	int yy1;
	int yy2;
	int yy3;
	int yy4;
	int x1;
	int x2;
	int x3;
	int x4;
	int y1;
	int y2;
	int y3;
	int y4;
	
	//构造函数
	public StartTip( MyObjectList vmyModuleList )
	{
		myModuleList = vmyModuleList;
		
		f = new Font( "微软雅黑", 14 );
		
		NoteImageStep = Image.FromFile( OS.SystemRoot + "Resource" + OS.PATH_S + "tip" + OS.PATH_S + n_Language.Language.UILanguage + OS.PATH_S + "步骤.png" );
		NoteImageMember = Image.FromFile( OS.SystemRoot + "Resource" + OS.PATH_S + "tip" + OS.PATH_S + n_Language.Language.UILanguage + OS.PATH_S + "牢记.png" );
		NoteImageOper = Image.FromFile( OS.SystemRoot + "Resource" + OS.PATH_S + "tip" + OS.PATH_S + n_Language.Language.UILanguage + OS.PATH_S + "操作.png" );
		NoteImageConti = Image.FromFile( OS.SystemRoot + "Resource" + OS.PATH_S + "tip" + OS.PATH_S + n_Language.Language.UILanguage + OS.PATH_S + "进阶.png" );
		
		
		Random r = new Random( System.DateTime.Now.Second );
		o1 = r.Next( -100, 0 ) / 100f;
		o2 = r.Next( -100, 0 ) / 100f;
		o3 = r.Next( -100, 0 ) / 100f;
		o4 = r.Next( -100, 0 ) / 100f;
		
		//o1 = o2 = o3 = o4 = 0;
		
		
		xx1 = 0;
		yy1 = 0;
		
		xx2 = 0;
		yy2 = NoteImageStep.Height + 10;
		
		xx3 = 0;
		yy3 = yy2 + NoteImageMember.Height + 10;
		
		xx4 = 0;
		yy4 = yy3 + NoteImageOper.Height + 10;
		
		x1 = xx1 + r.Next( -30, +30 );
		y1 = yy1 + r.Next( -8, +8 );
		
		x2 = xx2 + r.Next( -30, +30 );
		y2 = yy2 + r.Next( -8, +8 );
		
		x3 = xx3 + r.Next( -30, +30 );
		y3 = yy3 + r.Next( -8, +8 );
		
		x4 = xx4 + r.Next( -30, +30 );
		y4 = yy4 + r.Next( -8, +8 );
		
		
		
		/*
		xx1 = 0;
		yy1 = 0;
		
		xx2 = NoteImageMember.Width + 30;
		yy2 = yy1;
		
		xx3 = 0;
		yy3 = yy1 + NoteImageMember.Height + 250;
		
		xx4 = NoteImageMember.Width + 30;
		yy4 = yy3;
		
		x1 = xx1;// + r.Next( -30, +30 );
		y1 = yy1;// + r.Next( -12, +12 );
		
		x2 = xx2;// + r.Next( -30, +30 );
		y2 = yy2;// + r.Next( -12, +12 );
		
		x3 = xx3;// + r.Next( -30, +30 );
		y3 = yy3;// + r.Next( -12, +12 );
		
		x4 = xx4;// + r.Next( -30, +30 );
		y4 = yy4;// + r.Next( -12, +12 );
		*/
		
		
		C_MyButtonList = new MySTButton[ 11 ];
		C_MyButtonListLength = 0;
		
		int CStartX = 240;
		int StartX = CStartX;
		string BaseDir = OS.SystemRoot + "ext" + OS.PATH_S;
		
		StartY = 150;
		
		
		/*
		//mini型号
		string STminiPath = BaseDir + "mini" + OS.PATH_S + "0.lab";
		MySTButton STmini = new MySTButton( BaseDir + "mini" + OS.PATH_S + "0.png", STminiPath, StartX, StartY + 20, 50 );
		STmini.MouseUpEvent = ST_MouseDownEvent;
		C_MyButtonList[ C_MyButtonListLength ] = STmini;
		++C_MyButtonListLength;
		
		
		//nano型号
		string STnanoPath = BaseDir + "nano" + OS.PATH_S + "0.lab";
		MySTButton STnano = new MySTButton( BaseDir + "nano" + OS.PATH_S + "0.png", STnanoPath, StartX + 150, StartY + 20, 50 );
		STnano.MouseUpEvent = ST_MouseDownEvent;
		C_MyButtonList[ C_MyButtonListLength ] = STnano;
		++C_MyButtonListLength;
		
		//uno型号
		string STunoPath = BaseDir + "uno" + OS.PATH_S + "0.lab";
		MySTButton STuno = new MySTButton( BaseDir + "uno" + OS.PATH_S + "0.png", STunoPath, StartX + 350, StartY, 80 );
		STuno.MouseUpEvent = ST_MouseDownEvent;
		C_MyButtonList[ C_MyButtonListLength ] = STuno;
		++C_MyButtonListLength;
		
		//2560型号
		string ST2560Path = BaseDir + "2560" + OS.PATH_S + "0.lab";
		MySTButton ST2560 = new MySTButton( BaseDir + "2560" + OS.PATH_S + "0.png", ST2560Path, StartX + 550, StartY, 80 );
		ST2560.MouseUpEvent = ST_MouseDownEvent;
		C_MyButtonList[ C_MyButtonListLength ] = ST2560;
		++C_MyButtonListLength;
		
		StartY += 200;
		int PadX = 60;
		
		//UNO转接板
		string STUnoPath = BaseDir + "nano-uno1" + OS.PATH_S + "0.lab";
		MySTButton STUno = new MySTButton( BaseDir + "nano-uno1" + OS.PATH_S + "0.png", STUnoPath, StartX, StartY, 80 );
		STUno.MouseUpEvent = ST_MouseDownEvent;
		C_MyButtonList[ C_MyButtonListLength ] = STUno;
		++C_MyButtonListLength;
		StartX += PadX + STUno.Width;
		
		*/
		
		/*
		
		//UNO转接板
		string STUnoPath = BaseDir + "nano-uno" + OS.PATH_S + "0.lab";
		MySTButton STUno = new MySTButton( BaseDir + "nano-uno" + OS.PATH_S + "0.png", STUnoPath, StartX, StartY, 80 );
		STUno.MouseUpEvent = ST_MouseDownEvent;
		C_MyButtonList[ C_MyButtonListLength ] = STUno;
		++C_MyButtonListLength;
		StartX += PadX + STUno.Width;
		
//		//学习板
//		string STstudyPath = BaseDir + "nanostudy" + OS.PATH_S + "0.lab";
//		MySTButton STstudy = new MySTButton( BaseDir + "nanostudy" + OS.PATH_S + "0.png", STstudyPath, StartX, StartY, 80 );
//		STstudy.MouseUpEvent = ST_MouseDownEvent;
//		C_MyButtonList[ C_MyButtonListLength ] = STstudy;
//		++C_MyButtonListLength;
//		StartX += PadX + STstudy.Width;
		
		//马达驱动板
		string STmotorPath = BaseDir + "motor" + OS.PATH_S + "0.lab";
		MySTButton STmotor = new MySTButton( BaseDir + "motor" + OS.PATH_S + "0.png", STmotorPath, StartX, StartY, 80 );
		STmotor.MouseUpEvent = ST_MouseDownEvent;
		C_MyButtonList[ C_MyButtonListLength ] = STmotor;
		++C_MyButtonListLength;
		StartX += PadX + STmotor.Width;
		
		//4位数码管
		string STseg4Path = BaseDir + "seg4" + OS.PATH_S + "0.lab";
		MySTButton STseg4 = new MySTButton( BaseDir + "seg4" + OS.PATH_S + "0.png", STseg4Path, StartX, StartY, 80 );
		STseg4.MouseUpEvent = ST_MouseDownEvent;
		C_MyButtonList[ C_MyButtonListLength ] = STseg4;
		++C_MyButtonListLength;
		StartX += PadX + STseg4.Width;
		
		//液晶屏
		string STlcd1602Path = BaseDir + "lcd1602" + OS.PATH_S + "0.lab";
		MySTButton STlcd1602 = new MySTButton( BaseDir + "lcd1602" + OS.PATH_S + "0.png", STlcd1602Path, StartX, StartY, 80 );
		STlcd1602.MouseUpEvent = ST_MouseDownEvent;
		C_MyButtonList[ C_MyButtonListLength ] = STlcd1602;
		++C_MyButtonListLength;
		StartX += PadX + STlcd1602.Width;
		
		//点阵屏幕
		string STLED8X8Path = BaseDir + "LED8X8" + OS.PATH_S + "0.lab";
		MySTButton STLED8X8 = new MySTButton( BaseDir + "LED8X8" + OS.PATH_S + "0.png", STLED8X8Path, StartX, StartY, 80 );
		STLED8X8.MouseUpEvent = ST_MouseDownEvent;
		C_MyButtonList[ C_MyButtonListLength ] = STLED8X8;
		++C_MyButtonListLength;
		StartX += PadX + STLED8X8.Width;
		
		StartX = CStartX;
		StartY += 200;
		
		//刷卡板
		string STrc522Path = BaseDir + "rc522" + OS.PATH_S + "0.lab";
		MySTButton STrc522 = new MySTButton( BaseDir + "rc522" + OS.PATH_S + "0.png", STrc522Path, StartX, StartY, 80 );
		STrc522.MouseUpEvent = ST_MouseDownEvent;
		C_MyButtonList[ C_MyButtonListLength ] = STrc522;
		++C_MyButtonListLength;
		StartX += PadX + STrc522.Width;
		
		//无线通信
		string STnrf2401Path = BaseDir + "nrf2401" + OS.PATH_S + "0.lab";
		MySTButton STnrf2401 = new MySTButton( BaseDir + "nrf2401" + OS.PATH_S + "0.png", STnrf2401Path, StartX, StartY, 80 );
		STnrf2401.MouseUpEvent = ST_MouseDownEvent;
		C_MyButtonList[ C_MyButtonListLength ] = STnrf2401;
		++C_MyButtonListLength;
		StartX += PadX + STnrf2401.Width;
		
		*/
		
		StartY = 0;
	}
	
	//mini
	void ST_MouseDownEvent( MySTButton sender )
	{
		//OpenFile( true, sender.FilePath );
	}
	
	//组件绘制工作
	public void Draw( Graphics g, int sx, int sy )
	{
		if( n_ModuleLibPanel.MyModuleLibPanel.FloatButton ) {
			return;
		}
//		if( X == 0 ) {
//			X = 30;
//			Y = 50;
//			ClearButton.SetLocation( new Point( X + 37, Y + 160 ) );
//		}
		//绘制提示信息
		
		//int x = G.CGPanel.Width - NoteImage.Width - 5;
		//int y = G.CGPanel.Height - NoteImage.Height - 5;
		
		int x = G.CGPanel.Width/2 - NoteImageStep.Width/2 + 100;
		int y = G.CGPanel.Height/2 - (NoteImageStep.Height + NoteImageMember.Height + NoteImageOper.Height + NoteImageConti.Height)/2 - 15;
		
		//int x = G.CGPanel.Width/2 - NoteImageStep.Width - 50;
		
		
		if( x < 230 ) {
			x = 215;
		}
		if( y < 60 ) {
			y = 60;
		}
		
		
		DrawImage( g, NoteImageStep, x + xx1 + (int)((x1-xx1)*o1), y + yy1 + (int)((y1-yy1)*o1), o1 );
		DrawImage( g, NoteImageMember, x + xx2 + (int)((x2-xx2)*o2), y + yy2 + (int)((y2-yy2)*o2), o2 );
		DrawImage( g, NoteImageOper, x + xx3 + (int)((x3-xx3)*o3), y + yy3 + (int)((y3-yy3)*o3), o3 );
		DrawImage( g, NoteImageConti, x + xx4 + (int)((x4-xx4)*o4), y + yy4 + (int)((y4-yy4)*o4), o4 );
		
		//g.DrawString( "按空格键可以切换导线连接的折线模式和曲线模式", f, Brushes.RoyalBlue, x + xx4 + (int)((x4-xx4)*o4) + 50, y + yy4 + (int)((y4-yy4)*o4) + 120 );
		
		float of = 0.02f;
		
		bool needRefresh = false;
		if( o1 <= 1 ) {
			o1 += of;
			needRefresh = true;
		}
		if( o2 <= 1 ) {
			o2 += of;
			needRefresh = true;
		}
		if( o3 <= 1 ) {
			o3 += of;
			needRefresh = true;
		}
		if( o4 <= 1 ) {
			o4 += of;
			needRefresh = true;
		}
		if( needRefresh ) {
			G.CGPanel.Invalidate();
		}
		
		//绘制按钮
		for( int i = 0; i < C_MyButtonListLength; ++i ) {
			C_MyButtonList[ i ].Draw( g, StartY );
		}
	}
	
	//绘制一个图片
	void DrawImage( Graphics g, Image img, int x, int y, float o )
	{
		if( o < 0 ) {
			return;
		}
		if( o >= 1 ) {
			g.DrawImage( img, x, y );
		}
		else {
			ImageAttributes attr = new ImageAttributes();
			ColorMatrix cm = new ColorMatrix();
			cm.Matrix33 = o;
			attr.SetColorMatrix( cm );
			attr.SetColorMatrix( cm );
			g.DrawImage( img, new Rectangle( x, y, img.Width, img.Height),
                0,0, img.Width, img.Height,
                GraphicsUnit.Pixel, attr);
		}
	}
	
	//鼠标左键按下事件
	public bool LeftMouseDown( int mX, int mY )
	{
		bool d = false;
		
		//SignButton.MouseDown( mX, mY );
		
		for( int i = 0; i < C_MyButtonListLength; ++i ) {
			C_MyButtonList[ i ].MouseDown( mX, mY );
		}
		return d;
	}
	
	//鼠标左键松开事件
	public void LeftMouseUp( int mX, int mY )
	{
		//SignButton.MouseUp( mX, mY );
		
		for( int i = 0; i < C_MyButtonListLength; ++i ) {
			C_MyButtonList[ i ].MouseUp( mX, mY );
		}
	}
	
	//鼠标移动事件,当鼠标在组件上时返回1,组件被拖动时返回2
	public void MouseMove( int mX, int mY )
	{
		//SignButton.MouseMove( mX, mY );
		
		//控件按钮鼠标移动事件
		for( int i = 0; i < C_MyButtonListLength; ++i ) {
			C_MyButtonList[ i ].MouseMove( mX, mY );
		}
	}
}
}



