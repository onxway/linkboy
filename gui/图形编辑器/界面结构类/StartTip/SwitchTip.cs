﻿
namespace n_SwitchTip
{
using System;
using System.Drawing;
using System.Drawing.Imaging;
using System.Drawing.Drawing2D;
using System.Windows.Forms;

using n_Compiler;
using n_GUIset;
using n_Head_MtButton;
using n_ImagePanel;
using n_MyObjectList;
using n_OS;
using n_ISP;
using n_MySTButton;

//*****************************************************
//标题栏类
public class SwitchTip
{
	public delegate void D_OpenFile( bool IgnoreSave, string FileName );
	public static D_OpenFile deleOpenFile;
	
	C_MyButton CodeButton;
	C_MyButton CircuitButton;
	C_MyButton GameButton;
	
	bool isNew = false;
	
	static Bitmap GT_Img;
	static Brush BR;
	
	//构造函数
	public SwitchTip()
	{
		GT_Img = new Bitmap( n_OS.OS.SystemRoot + @"Resource\gui\GT.png" );
		
		//窗体控件
		string code = OS.SystemRoot + "Resource" + OS.PATH_S + "tip" + OS.PATH_S + "Hardware1.png";
		string code1 = OS.SystemRoot + "Resource" + OS.PATH_S + "tip" + OS.PATH_S + "Hardware.png";
		CodeButton = new C_MyButton( C_MyButton.E_ButtonType.SwitchImage, code, code1, code, 0, 0, null );
		CodeButton.SetLocation( new Point( 440, 250 ) );
		CodeButton.MouseDownEvent += CodeButton_MouseDownEvent;
		
		string circuit = OS.SystemRoot + "Resource" + OS.PATH_S + "tip" + OS.PATH_S + "Circuit1.png";
		string circuit1 = OS.SystemRoot + "Resource" + OS.PATH_S + "tip" + OS.PATH_S + "Circuit.png";
		CircuitButton = new C_MyButton( C_MyButton.E_ButtonType.SwitchImage, circuit, circuit1, circuit, 0, 0, null );
		CircuitButton.SetLocation( new Point( 440, 400 ) );
		CircuitButton.MouseDownEvent += CircuitButton_MouseDownEvent;
		
		string game = OS.SystemRoot + "Resource" + OS.PATH_S + "tip" + OS.PATH_S + "Game1.png";
		string game1 = OS.SystemRoot + "Resource" + OS.PATH_S + "tip" + OS.PATH_S + "Game.png";
		GameButton = new C_MyButton( C_MyButton.E_ButtonType.SwitchImage, game, game1, game, 0, 0, null );
		GameButton.SetLocation( new Point( 440, 550 ) );
		GameButton.MouseDownEvent += GameButton_MouseDownEvent;
		
		BR = new SolidBrush( Color.FromArgb( 246, 246, 246 ) );
	}
	
	//组件绘制工作
	public void Draw( Graphics g, int sx, int sy )
	{
		//绘制按钮
		if( n_ModuleLibPanel.MyModuleLibPanel.FloatButton ) {
			
			int w = G.CGPanel.Width;
			int h = G.CGPanel.Height;
			int bw = CodeButton.Width;
			int nw = w / 4;
			int pady = CodeButton.Height/2;
			
			if( isNew ) {
				isNew  = false;
				
				CodeButton.Width = nw;
				CodeButton.Height = CodeButton.Height * CodeButton.Width / bw;
				CircuitButton.Width = nw;
				CircuitButton.Height = CircuitButton.Height * CircuitButton.Width / bw;
				GameButton.Width = nw;
				GameButton.Height = GameButton.Height * GameButton.Width / bw;
			}
			
			//水平居中
			CircuitButton.X = w/2;// - CircuitButton.Width/2;
			CircuitButton.Y = h/2 - CodeButton.Height/2;
			
			CodeButton.X = w/2;// - CodeButton.Width/2;
			CodeButton.Y = CircuitButton.Y - CodeButton.Height - pady;
			
			if( !G.TanMode ) {
				CircuitButton.Y = h/2 + CodeButton.Height/2;
				CodeButton.Y = h/2 - CodeButton.Height/2 - CodeButton.Height;
			}
			
			GameButton.X = w/2;// - CircuitButton.Width/2;
			GameButton.Y = CircuitButton.Y + CircuitButton.Height + pady;
			
			int x = CodeButton.X - CodeButton.Height * 2;
			int y = CodeButton.Y - CodeButton.Height;
			int ww = (w/2-x) * 2;
			int hh = (h/2-y) * 2;
			
			//整体背景
			
			g.FillRectangle( BR, 0, 0, GT_Img.Width, h );
			g.DrawImage( GT_Img, 0, 0 );
			
			
			//g.DrawString( "点击任意空白处将进入默认的 \"开源硬件编程实验室\"", GUIset.Font12, Brushes.SlateGray, x, y );
			g.DrawString( "（默认）", GUIset.ExpFont, Brushes.LightSteelBlue, CodeButton.X - 80, CodeButton.Y + CodeButton.Height/2 - 10 );
			
			/*
			g.DrawString( "当前版本为linkboy5.0内测版, 遇到问题请及时反馈给我们, 以便在5.0正式版中改进, 谢谢", GUIset.Font14, Brushes.LightGoldenrodYellow, GT_Img.Width + 10, 10 );
			g.DrawString( "一些新增主板(如microbit,8266,树莓派Pico等)暂不支持扬声器, 舵机, 马达调速, 红外遥控解码", GUIset.Font14, Brushes.LightSteelBlue, GT_Img.Width + 10, 40 );
			g.DrawString( "近期内测完成后, 将会解决这些问题, 并发布linkboy5.0正式版.", GUIset.Font14, Brushes.LightSteelBlue, GT_Img.Width + 10, 70 );
			*/
			
			CodeButton.Draw( g );
			CircuitButton.Draw( g );
			
			if( G.TanMode ) {
				GameButton.Draw( g );
			}
		}
	}
	
	//鼠标左键按下事件
	public bool LeftMouseDown( int mX, int mY )
	{
		bool d = false;
		if( n_ModuleLibPanel.MyModuleLibPanel.FloatButton ) {
			d |= CodeButton.MouseDown( mX, mY );
			d |= CircuitButton.MouseDown( mX, mY );
			if( G.TanMode ) {
				d |= GameButton.MouseDown( mX, mY );
			}
			
		}
		return d;
	}
	
	//鼠标左键松开事件
	public void LeftMouseUp( int mX, int mY )
	{
		if( n_ModuleLibPanel.MyModuleLibPanel.FloatButton ) {
			CodeButton.MouseUp( mX, mY );
			CircuitButton.MouseUp( mX, mY );
			if( G.TanMode ) {
				GameButton.MouseUp( mX, mY );
			}
		}
	}
	
	//鼠标移动事件,当鼠标在组件上时返回1,组件被拖动时返回2
	public void MouseMove( int mX, int mY )
	{
		//控件按钮鼠标移动事件
		if( n_ModuleLibPanel.MyModuleLibPanel.FloatButton ) {
			CodeButton.MouseMove( mX, mY );
			CircuitButton.MouseMove( mX, mY );
			if( G.TanMode ) {
				GameButton.MouseMove( mX, mY );
			}
		}
	}
	
	//控件点击事件
	public void CodeButton_MouseDownEvent()
	{
		//n_ModuleLibDecoder.ModuleLibDecoder.TempLibName = "tech";
		//n_ModuleLibPanel.ModuleTree.Reload( 0 );
		
		n_ModuleLibPanel.MyModuleLibPanel.Init( n_GUIcoder.labType.Code );
		G.CGPanel.GHead.MoButton_MouseDownEvent();
	}
	
	//控件点击事件
	public void CircuitButton_MouseDownEvent()
	{
		//n_ModuleLibDecoder.ModuleLibDecoder.TempLibName = "mod0";
		//n_ModuleLibPanel.ModuleTree.Reload( 1 );
		
		n_ModuleLibPanel.MyModuleLibPanel.Init( n_GUIcoder.labType.Circuit );
		G.CGPanel.GHead.MoButton_MouseDownEvent();
		
		if( deleOpenFile != null ) {
			deleOpenFile( true, n_StartFile.StartFile.CircuitFile );
		}
	}
	
	//控件点击事件
	public void GameButton_MouseDownEvent()
	{
		if( !G.TanMode ) {
			return;
		}
		//n_ModuleLibDecoder.ModuleLibDecoder.TempLibName = "mod0";
		//n_ModuleLibPanel.ModuleTree.Reload( 1 );
		
		n_ModuleLibPanel.MyModuleLibPanel.Init( n_GUIcoder.labType.Game );
		G.CGPanel.GHead.MoButton_MouseDownEvent();
		
		if( deleOpenFile != null ) {
			deleOpenFile( true, n_StartFile.StartFile.GameFile );
		}
	}
}
}



