﻿

namespace n_LineColorBox
{
using System;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Windows.Forms;
using n_HardModule;

//导线颜色类
public class LineColorBox
{
	int SX;
	int SY;
	int Width;
	int PerHeight;
	
	public bool Visible;
	
	SolidBrush Back;
	
	//构造函数
	public LineColorBox()
	{
		SX = 200;
		SY = 230;
		Width = 30;
		PerHeight = 20;
		
		Back = new SolidBrush( Color.Black );
		Visible = true;
	}
	
	//更新坐标
	public void ResetLocation()
	{
		SX = G.CGPanel.Width - Width - n_ImagePanel.ImagePanel.RHidWidth - 4;
		
		//SY = G.CGPanel.Height - 260 - 110;
	}
	
	//鼠标移动
	public bool MouseMove( int mX, int mY )
	{
		return false;
	}
	
	//绘制
	public void Draw( Graphics g )
	{
		if( Visible ) {
			
			//g.DrawRectangle( Pens.SlateGray, SX, SY, Width, (PerHeight + 6 ) * PortPenList.Number - 4 );
			
			for( int i = 0; i < PortPenList.Number; ++i ) {
				Color c = PortPenList.BackLinePen[i].Color;
				Back.Color = c;
				
				g.FillRectangle( Back, SX + 2, SY + 2 + i * (PerHeight + 6), Width - 4, PerHeight );
			}
			int ci = G.CGPanel.myModuleList.SelectPort.ColorIndex;
			g.DrawRectangle( Pens.Black, SX - 2, SY + ci * (PerHeight + 6), Width + 4, PerHeight + 4 );
			
			if( n_HardModule.Port.C_Rol == 0 ) {
				n_SG.SG.MString.DrawAtRight( "当前为折线模式", Color.OrangeRed, 12, SX + Width, SY - 30 );
				//n_SG.SG.MString.DrawAtRight( "空格键切换颜色", Color.SlateGray, 12, SX + Width, SY - 30 );
			}
			else {
				n_SG.SG.MString.DrawAtRight( "当前为曲线模式", Color.OrangeRed, 12, SX + Width, SY - 30 );
				//n_SG.SG.MString.DrawAtRight( "空格键切换颜色", Color.SlateGray, 12, SX + Width, SY - 30 );
			}
			n_SG.SG.MString.DrawAtRight( "空格键切换导线颜色", Color.SlateGray, 12, SX + Width, SY - 10 );
		}
	}
}
}

