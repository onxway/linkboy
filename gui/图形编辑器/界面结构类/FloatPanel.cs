﻿
namespace n_FloatPanel
{
using System;
using System.Drawing;
using System.Drawing.Drawing2D;

using n_CondiEndIns;
using n_ElseIns;
using n_ForeverIns;
using n_FuncIns;
using n_GUICommon;
using n_IfElseIns;
using n_WhileIns;
using n_LoopIns;
using n_MyIns;
using n_Shape;
using n_UserFunctionIns;
using n_WaitIns;

//逻辑模块面板类
public class FloatPanel
{
	public Color BackColor;
	Brush BackBrush;
	
	Font f;
	
	public int SX;
	public int SY;
	
	public int Width;
	public int Height;
	
	public bool Visible;
	string Message1;
	string Message2;
	
	//构造函数
	public FloatPanel()
	{
		f = new Font( "宋体", 11 );
		//BackBrush = new SolidBrush( Color.FromArgb( 100, GUIset.GetRedColor( 150 ) ) );
		//alphab = new SolidBrush( Color.FromArgb( 150, GUIset.GetRedColor( 150 ) ) );
		//onb = new SolidBrush( Color.FromArgb( 100, GUIset.GetRedColor( 150 ) ) );
		BackColor = Color.FromArgb( 100, 200, 200, 200 );
		BackBrush = new SolidBrush( BackColor );
		
		Width = 90;//210;//155;
		Height = 160;
		
		SY = 70;
		
		Visible = false;
	}
	
	//设置显示信息并显示
	public void SetMessage( string s1, string s2 )
	{
		Message1 = s1;
		Message2 = s2;
		Visible = true;
	}
	
	//组件绘制工作
	public void Draw( Graphics g )
	{
		if( !Visible ) {
			return;
		}
		Rectangle r = new Rectangle( SX, SY, Width, Height );
		GraphicsPath path = Shape.CreateRoundedRectanglePath( r );
		
		//绘制背景边框
		g.FillPath( BackBrush, path );
		g.DrawString( Message1, f, Brushes.Yellow, SX + 5, SY + 5 );
		g.DrawString( Message2, f, Brushes.White, SX + 5, SY + 23 );
	}
}
}


