﻿
namespace n_VarListPanel
{
using System;
using System.Drawing;
using System.Drawing.Drawing2D;

using n_CondiEndIns;
using n_ElseIns;
using n_ForeverIns;
using n_FuncIns;
using n_GUICommon;
using n_IfElseIns;
using n_WhileIns;
using n_LoopIns;
using n_MyIns;
using n_Shape;
using n_UserFunctionIns;
using n_WaitIns;
using n_Head_MtButton;
using n_OS;
using n_GNote;
using n_GVar;
using n_GUIcoder;
using n_GUIset;

//变量面板类
public class VarListPanel
{
	public delegate void D_HideEvent();
	public D_HideEvent HideEvent;
	
	public Color BackColor;
	
	SolidBrush EageBrush;
	
	bool isMouseOn;
	Brush BackBrush;
	
	Font f;
	
	bool v_ShowLogicPanel;
	public bool ShowLogicPanel {
		set {
			v_ShowLogicPanel = value;
			if( HideEvent != null ) {
				HideEvent();
			}
		}
		get {
			return v_ShowLogicPanel;
		}
	}
	
	public int SX;
	public int SY;
	
	public int Width;
	public int Height;
	
	SolidBrush alphab;
	SolidBrush onb;
	
	const int InsPadding0 = 5;
	const int InsPadding1 = 20;
	
	bool isMouseDown;
	int LastX;
	int lastY;
	
	C_MyButton[] C_MyButtonList;
	int C_MyButtonListLength;
	bool FirstAddMusic;
	bool NeedShow;
	bool FirstAddFix;
	
	n_MyObject.MyObject m_String;
	
	
	//构造函数
	public VarListPanel( int x, int y )
	{
		f = new Font( "宋体", 11 );
		//BackBrush = new SolidBrush( Color.FromArgb( 100, GUIset.GetRedColor( 150 ) ) );
		//alphab = new SolidBrush( Color.FromArgb( 150, GUIset.GetRedColor( 150 ) ) );
		//onb = new SolidBrush( Color.FromArgb( 100, GUIset.GetRedColor( 150 ) ) );
		
		//BackColor = n_GUIset.GUIset.GUIbackColor;
		BackColor = Color.FromArgb( 240, 250, 240 );
		
		BackColor = Color.FromArgb( 238, 242, 246 );
		
		if( G.LightUI ) {
			BackColor = Color.FromArgb( 255, 255, 255 );
		}
		
		
		BackBrush = new SolidBrush( BackColor );
		alphab = new SolidBrush( Color.FromArgb( 120, Color.LightGray ) );
		onb = new SolidBrush( Color.FromArgb( 120, Color.LightGray ) );
		
		EageBrush = new SolidBrush( n_Head.Head.VarButtonActiveColor );
		
		ShowLogicPanel = false;
		NeedShow = false;
		isMouseDown = false;
		isMouseOn = false;
		
		FirstAddMusic = true;
		FirstAddFix = true;
		
		//int CurrentY = 70 + InsPadding1;
		//int PerHeight = 20;
		
		SX = x;
		SY = y;
		Width = n_ModuleLibPanel.ModuleTree.CWidth;
		Height = 200;
		
		int StX = (210 - 160) / 2;
		
		C_MyButtonList = new C_MyButton[ 10 ];
		C_MyButtonListLength = 0;
		
		int StartY = n_GUIset.GUIset.GetPix(120);
		int offset = -5;
		int Pad = 15;
		
		int YStep = GUIset.GetPix(45 + Pad + offset);
		
		//添加数值量
		string ValueImage0 = OS.SystemRoot + "Resource" + OS.PATH_S + "gui" + OS.PATH_S + "GFormControls" + OS.PATH_S + "var-" + n_Language.Language.UILanguage + OS.PATH_S + "int.png";
		string ValueImage = OS.SystemRoot + "Resource" + OS.PATH_S + "gui" + OS.PATH_S + "GFormControls" + OS.PATH_S + "var-" + n_Language.Language.UILanguage + OS.PATH_S + "int1.png";
		C_MyButtonList[ C_MyButtonListLength ] = new C_MyButton( C_MyButton.E_ButtonType.SwitchImage, ValueImage0, ValueImage, ValueImage, GUIset.GetPix(140), GUIset.GetPix(45), null );
		C_MyButtonList[ C_MyButtonListLength ].MouseDownEvent += ValueButton_MouseDownEvent;
		C_MyButtonList[ C_MyButtonListLength ].SetLocation( new Point( StX + 10, StartY ) );
		//C_MyButtonList[ C_MyButtonListLength ].Text = "添加整数";
		C_MyButtonList[ C_MyButtonListLength ].TipBrush = Brushes.ForestGreen;
		++C_MyButtonListLength;
		StartY += YStep;
		
		//添加定点小数
		string FixValueImage0 = OS.SystemRoot + "Resource" + OS.PATH_S + "gui" + OS.PATH_S + "GFormControls" + OS.PATH_S + "var-" + n_Language.Language.UILanguage + OS.PATH_S + "Fix.png";
		string FixValueImage = OS.SystemRoot + "Resource" + OS.PATH_S + "gui" + OS.PATH_S + "GFormControls" + OS.PATH_S + "var-" + n_Language.Language.UILanguage + OS.PATH_S + "Fix1.png";
		C_MyButtonList[ C_MyButtonListLength ] = new C_MyButton( C_MyButton.E_ButtonType.SwitchImage, FixValueImage0, FixValueImage, FixValueImage, GUIset.GetPix(140), GUIset.GetPix(45), null );
		C_MyButtonList[ C_MyButtonListLength ].MouseDownEvent += FixValueButton_MouseDownEvent;
		C_MyButtonList[ C_MyButtonListLength ].SetLocation( new Point( StX + 10, StartY ) );
		//C_MyButtonList[ C_MyButtonListLength ].Text = "添加小数";
		C_MyButtonList[ C_MyButtonListLength ].TipBrush = Brushes.ForestGreen;
		++C_MyButtonListLength;
		StartY += YStep;
		
		//添加条件量
		string BoolImage0 = OS.SystemRoot + "Resource" + OS.PATH_S + "gui" + OS.PATH_S + "GFormControls" + OS.PATH_S + "var-" + n_Language.Language.UILanguage + OS.PATH_S + "bool.png";
		string BoolImage = OS.SystemRoot + "Resource" + OS.PATH_S + "gui" + OS.PATH_S + "GFormControls" + OS.PATH_S + "var-" + n_Language.Language.UILanguage + OS.PATH_S + "bool1.png";
		C_MyButtonList[ C_MyButtonListLength ] = new C_MyButton( C_MyButton.E_ButtonType.SwitchImage, BoolImage0, BoolImage, BoolImage, GUIset.GetPix(140), GUIset.GetPix(45), null );
		C_MyButtonList[ C_MyButtonListLength ].MouseDownEvent += BoolButton_MouseDownEvent;
		C_MyButtonList[ C_MyButtonListLength ].SetLocation( new Point( StX + 10, StartY ) );
		//C_MyButtonList[ C_MyButtonListLength ].Text = "添加条件量";
		C_MyButtonList[ C_MyButtonListLength ].TipBrush = Brushes.ForestGreen;
		++C_MyButtonListLength;
		StartY += YStep;
		
		//添加字符串
		m_String = GUIcoder.GetModuleFromMacroFile(  OS.ModuleLibPath + "software\\string_d\\Pack.B" );
		
		string StringImage0 = OS.SystemRoot + "Resource" + OS.PATH_S + "gui" + OS.PATH_S + "GFormControls" + OS.PATH_S + "var-" + n_Language.Language.UILanguage + OS.PATH_S + "string.png";
		string StringImage = OS.SystemRoot + "Resource" + OS.PATH_S + "gui" + OS.PATH_S + "GFormControls" + OS.PATH_S + "var-" + n_Language.Language.UILanguage + OS.PATH_S + "string1.png";
		C_MyButtonList[ C_MyButtonListLength ] = new C_MyButton( C_MyButton.E_ButtonType.SwitchImage, StringImage0, StringImage, StringImage, GUIset.GetPix(140), GUIset.GetPix(45), null );
		C_MyButtonList[ C_MyButtonListLength ].MouseDownEvent += StringButton_MouseDownEvent;
		C_MyButtonList[ C_MyButtonListLength ].SetLocation( new Point( StX + 10, StartY ) );
		//C_MyButtonList[ C_MyButtonListLength ].Text = "添加条件量";
		C_MyButtonList[ C_MyButtonListLength ].TipBrush = Brushes.ForestGreen;
		++C_MyButtonListLength;
		StartY += YStep;
		
		//添加彩色图片
		string CImageImage0 = OS.SystemRoot + "Resource" + OS.PATH_S + "gui" + OS.PATH_S + "GFormControls" + OS.PATH_S + "var-" + n_Language.Language.UILanguage + OS.PATH_S + "cimage.png";
		string CImageImage = OS.SystemRoot + "Resource" + OS.PATH_S + "gui" + OS.PATH_S + "GFormControls" + OS.PATH_S + "var-" + n_Language.Language.UILanguage + OS.PATH_S + "cimage1.png";
		C_MyButtonList[ C_MyButtonListLength ] = new C_MyButton( C_MyButton.E_ButtonType.SwitchImage, CImageImage0, CImageImage, CImageImage, GUIset.GetPix(73), GUIset.GetPix(73), null );
		C_MyButtonList[ C_MyButtonListLength ].MouseDownEvent += CImageButton_MouseDownEvent;
		C_MyButtonList[ C_MyButtonListLength ].SetLocation( new Point( StX + 43, StartY ) );
		//C_MyButtonList[ C_MyButtonListLength ].Text = "添加图片";
		C_MyButtonList[ C_MyButtonListLength ].TipBrush = Brushes.ForestGreen;
		++C_MyButtonListLength;
		StartY += GUIset.GetPix(73 + Pad + offset);
		
		//添加图片
		string ImageImage0 = OS.SystemRoot + "Resource" + OS.PATH_S + "gui" + OS.PATH_S + "GFormControls" + OS.PATH_S + "var-" + n_Language.Language.UILanguage + OS.PATH_S + "image.png";
		string ImageImage = OS.SystemRoot + "Resource" + OS.PATH_S + "gui" + OS.PATH_S + "GFormControls" + OS.PATH_S + "var-" + n_Language.Language.UILanguage + OS.PATH_S + "image1.png";
		C_MyButtonList[ C_MyButtonListLength ] = new C_MyButton( C_MyButton.E_ButtonType.SwitchImage, ImageImage0, ImageImage, ImageImage, GUIset.GetPix(73), GUIset.GetPix(73), null );
		C_MyButtonList[ C_MyButtonListLength ].MouseDownEvent += ImageButton_MouseDownEvent;
		C_MyButtonList[ C_MyButtonListLength ].SetLocation( new Point( StX + 43, StartY ) );
		//C_MyButtonList[ C_MyButtonListLength ].Text = "添加图片";
		C_MyButtonList[ C_MyButtonListLength ].TipBrush = Brushes.ForestGreen;
		++C_MyButtonListLength;
		StartY += GUIset.GetPix(73 + Pad + offset);
		
		//添加字体
		string FontImage0 = OS.SystemRoot + "Resource" + OS.PATH_S + "gui" + OS.PATH_S + "GFormControls" + OS.PATH_S + "var-" + n_Language.Language.UILanguage + OS.PATH_S + "font.png";
		string FontImage = OS.SystemRoot + "Resource" + OS.PATH_S + "gui" + OS.PATH_S + "GFormControls" + OS.PATH_S + "var-" + n_Language.Language.UILanguage + OS.PATH_S + "font1.png";
		C_MyButtonList[ C_MyButtonListLength ] = new C_MyButton( C_MyButton.E_ButtonType.SwitchImage, FontImage0, FontImage, FontImage, GUIset.GetPix(140), GUIset.GetPix(54), null );
		C_MyButtonList[ C_MyButtonListLength ].MouseDownEvent += FontButton_MouseDownEvent;
		C_MyButtonList[ C_MyButtonListLength ].SetLocation( new Point( StX + 10, StartY ) );
		//C_MyButtonList[ C_MyButtonListLength ].Text = "添加图片";
		C_MyButtonList[ C_MyButtonListLength ].TipBrush = Brushes.ForestGreen;
		++C_MyButtonListLength;
		StartY += GUIset.GetPix(54 + Pad + offset);
		
		//添加音乐
		string MusicImage0 = OS.SystemRoot + "Resource" + OS.PATH_S + "gui" + OS.PATH_S + "GFormControls" + OS.PATH_S + "var-" + n_Language.Language.UILanguage + OS.PATH_S + "music.png";
		string MusicImage = OS.SystemRoot + "Resource" + OS.PATH_S + "gui" + OS.PATH_S + "GFormControls" + OS.PATH_S + "var-" + n_Language.Language.UILanguage + OS.PATH_S + "music1.png";
		C_MyButtonList[ C_MyButtonListLength ] = new C_MyButton( C_MyButton.E_ButtonType.SwitchImage, MusicImage0, MusicImage, MusicImage, GUIset.GetPix(90), GUIset.GetPix(90), null );
		C_MyButtonList[ C_MyButtonListLength ].MouseDownEvent += MusicButton_MouseDownEvent;
		C_MyButtonList[ C_MyButtonListLength ].SetLocation( new Point( StX + 35, StartY ) );
		//C_MyButtonList[ C_MyButtonListLength ].Text = "添加音乐";
		C_MyButtonList[ C_MyButtonListLength ].TipBrush = Brushes.ForestGreen;
		++C_MyButtonListLength;
		StartY += GUIset.GetPix(90 + Pad + offset);
		
		//添加代码块
		string CodeImage0 = OS.SystemRoot + "Resource" + OS.PATH_S + "gui" + OS.PATH_S + "GFormControls" + OS.PATH_S + "var-" + n_Language.Language.UILanguage + OS.PATH_S + "code.png";
		string CodeImage = OS.SystemRoot + "Resource" + OS.PATH_S + "gui" + OS.PATH_S + "GFormControls" + OS.PATH_S + "var-" + n_Language.Language.UILanguage + OS.PATH_S + "code1.png";
		C_MyButtonList[ C_MyButtonListLength ] = new C_MyButton( C_MyButton.E_ButtonType.SwitchImage, CodeImage0, CodeImage, CodeImage, GUIset.GetPix(140), GUIset.GetPix(93), null );
		C_MyButtonList[ C_MyButtonListLength ].MouseDownEvent += CodeButton_MouseDownEvent;
		C_MyButtonList[ C_MyButtonListLength ].SetLocation( new Point( StX + 8, StartY ) );
		//C_MyButtonList[ C_MyButtonListLength ].Text = "添加注释";
		C_MyButtonList[ C_MyButtonListLength ].TipBrush = Brushes.ForestGreen;
		++C_MyButtonListLength;
		StartY += GUIset.GetPix(93 + Pad + offset);
		
		//=========================================================================
		
		//添加注释
		string NoteImage0 = OS.SystemRoot + "Resource" + OS.PATH_S + "gui" + OS.PATH_S + "GFormControls" + OS.PATH_S + "var-" + n_Language.Language.UILanguage + OS.PATH_S + "mes.png";
		string NoteImage = OS.SystemRoot + "Resource" + OS.PATH_S + "gui" + OS.PATH_S + "GFormControls" + OS.PATH_S + "var-" + n_Language.Language.UILanguage + OS.PATH_S + "mes1.png";
		C_MyButtonList[ C_MyButtonListLength ] = new C_MyButton( C_MyButton.E_ButtonType.SwitchImage, NoteImage0, NoteImage, NoteImage, GUIset.GetPix(140), GUIset.GetPix(41), null );
		C_MyButtonList[ C_MyButtonListLength ].MouseDownEvent += NoteButton_MouseDownEvent;
		C_MyButtonList[ C_MyButtonListLength ].SetLocation( new Point( StX + 8, StartY ) );
		//C_MyButtonList[ C_MyButtonListLength ].Text = "添加注释";
		C_MyButtonList[ C_MyButtonListLength ].TipBrush = Brushes.ForestGreen;
		++C_MyButtonListLength;
		StartY += GUIset.GetPix(41 + Pad + offset);
	}
	
	//组件绘制工作
	public void Draw( Graphics g )
	{
		if( !ShowLogicPanel ) {
			return;
		}
		//绘制背景边框
		Brush b;
		if( isMouseOn ) {
			b = BackBrush;
		}
		else {
			b = BackBrush;
		}
		
		Rectangle r = new Rectangle( n_Head.Head.ShowX, 0, Width, Height );
		//GraphicsPath path = Shape.CreateRoundedRectanglePath( r );
		g.FillRectangle( b, r );
		
		//g.FillRectangle( EageBrush, n_Head.Head.ShowX, 105 - 13, 1, Height );
		//g.FillRectangle( EageBrush, n_Head.Head.ShowX + Width - 1, 105 - 13, 1, Height );
		//g.FillRectangle( EageBrush, n_Head.Head.ShowX, 105 - 13, Width - 1, 1 );
		
		GraphicsState gs = g.Save();
		g.TranslateTransform( n_Head.Head.ShowX, SY );
		
		//绘制按钮列表
		for( int i = 0; i < C_MyButtonListLength; ++i ) {
			int tx = C_MyButtonList[ i ].X;
			//C_MyButtonList[ i ].X += n_Head.Head.ShowX;
			C_MyButtonList[ i ].Draw( g );
			//C_MyButtonList[ i ].X = tx;
		}
		g.Restore( gs );
	}
	
	//鼠标左键按下事件
	public bool LeftMouseDown( int mX, int mY )
	{
		if( !ShowLogicPanel ) {
			return false;
		}
		if( isMouseOn ) {
			
			//轮询按钮列表
			for( int i = 0; i < C_MyButtonListLength; ++i ) {
				if( C_MyButtonList[ i ].MouseDown( mX - SX, mY - SY ) ) {
					
					C_MyButtonList[ i ].isPress = false;
					
					//if( n_Head.Head.AutoHide ) {
						v_ShowLogicPanel = false;
						NeedShow = true;
					//}
					return true;
				}
			}
			
			isMouseDown = true;
			LastX = mX;
			lastY = mY;
			
			return true;
		}
		return false;
	}
	
	//鼠标左键松开事件
	public void LeftMouseUp( int mX, int mY )
	{
		isMouseDown = false;
		
		//轮询按钮列表
		for( int i = 0; i < C_MyButtonListLength; ++i ) {
			C_MyButtonList[ i ].MouseUp( mX, mY );
		}
	}
	
	//鼠标移动事件, 当鼠标在组件上时返回1,组件被拖动时返回2
	public bool MouseMove( int mX, int mY )
	{
		//鼠标离开面板后应该重新显示出来
		if( NeedShow && mX > this.Width && !n_Head.Head.AutoHide ) {
			v_ShowLogicPanel = true;
			NeedShow = false;
		}
		if( !n_Head.Head.HeadShow ) {
			return false;
		}
		if( !ShowLogicPanel ) {
			return false;
		}
		if( isMouseDown ) {
			this.SY += (mY - lastY );
			LastX = mX;
			lastY = mY;
		}
		
		//isMouseOn = isMouseInSide( mX, mY );
		isMouseOn = isMouseDown || mX < this.Width;
		
		if( isMouseOn ) {
			//控件按钮鼠标移动事件, 轮询按钮列表
			for( int i = 0; i < C_MyButtonListLength; ++i ) {
				bool on = C_MyButtonList[ i ].MouseMove( mX - SX, mY - SY );
			}
		}
		else {
			if( n_Head.Head.AutoHide ) {
				ShowLogicPanel = false;
			}
		}
		
//		//控件按钮鼠标移动事件, 轮询按钮列表
//		for( int i = 0; i < C_MyInsListLength; ++i ) {
//			C_MyInsList[ i ].MouseMove( mX - midX, mY - midY );
//		}
		return isMouseOn;
	}
	
	//滚动条移动
	public bool Scroll( int mX, int mY, int Delta )
	{
		if( !n_Head.Head.HeadShow ) {
			return false;
		}
		if( !ShowLogicPanel ) {
			return false;
		}
		if( mX > Width ) {
			return false;
		}
		SY += Delta;
		
		/*
		if( SY < BottomY ) {
			SY = BottomY;
		}
		if( SY > TopY ) {
			SY = TopY;
		}
		*/
		MouseMove( mX, mY );
		return true;
	}
	
	//=====================================================
	//指令处理类
	
	//添加注释
	void NoteButton_MouseDownEvent()
	{
		GNote gm = new GNote();
		gm.SetUserValue( "描述", null, "请在此输入您的注释信息", 200, 200, GUIset.GetPix(140), GUIset.GetPix(70) );
		gm.ignoreHit = true;
		gm.isNewTick = n_MyObject.MyObject.MaxNewTick;
		G.CGPanel.myModuleList.AddNote( gm );
	}
	
	//添加代码块
	void CodeButton_MouseDownEvent()
	{
		GNote gm = new GNote();
		gm.isCode = true;
		gm.ignoreHit = true;
		gm.SetUserValue( GetNewName( "代码" ), null, "void Test()\n{\n\t//这里输入代码...\n}\n", 200, 200, GUIset.GetPix(200), GUIset.GetPix(130) );
		gm.isNewTick = n_MyObject.MyObject.MaxNewTick;
		G.CGPanel.myModuleList.AddNote( gm );
	}
	
	//添加整数
	void ValueButton_MouseDownEvent()
	{
		GVar m = new GVar();
		m.SetUserValue( GetNewName( "N" ), null, GType.g_int32, "", "",  false, 0, 0 );
		m.ignoreHit = true;
		m.isNewTick = n_MyObject.MyObject.MaxNewTick;
		G.CGPanel.myModuleList.Add( m );
		
		n_NL.NL.CNode.vALUE_int++;
	}
	
	//添加小数
	void FixValueButton_MouseDownEvent()
	{
		GVar m = new GVar();
		m.SetUserValue( GetNewName( "F" ), null, GType.g_fix, "", "",  false, 0, 0 );
		m.ignoreHit = true;
		m.isNewTick = n_MyObject.MyObject.MaxNewTick;
		
		
		if( FirstAddFix ) {
			FirstAddFix = false;
			string Mes = n_Language.Language.GFixMes;
			G.CGPanel.AddMesPanel( Mes, 1, m );
		}
		
		G.CGPanel.myModuleList.Add( m );
		
		n_NL.NL.CNode.vALUE_fix++;
	}
	
	//添加条件量
	void BoolButton_MouseDownEvent()
	{
		GVar m = new GVar();
		m.SetUserValue( GetNewName( "B" ), null, GType.g_bool, "", "",  false, 0, 0 );
		m.ignoreHit = true;
		m.isNewTick = n_MyObject.MyObject.MaxNewTick;
		G.CGPanel.myModuleList.Add( m );
		
		n_NL.NL.CNode.vALUE_bool++;
	}
	
	//添加字符串
	void StringButton_MouseDownEvent()
	{
		G.CGPanel.AddNewModule( m_String, 70, 20 );
	}
	
	//添加音乐
	void MusicButton_MouseDownEvent()
	{
		GVar m = new GVar();
		m.SetUserValue( GetNewName( "音乐" ), null, GType.g_music, "", "",  true, 0, 0 );
		m.ignoreHit = true;
		m.isNewTick = n_MyObject.MyObject.MaxNewTick;
		G.CGPanel.myModuleList.Add( m );
		
		if( FirstAddMusic ) {
			FirstAddMusic = false;
			string InsMes = n_Language.Language.MusicVarMes;
			G.CGPanel.AddMesPanel( InsMes, 1, m );
		}
		n_NL.NL.CNode.vALUE_music++;
	}
	
	//添加彩色图片
	void CImageButton_MouseDownEvent()
	{
		GVar m = new GVar();
		m.SetUserValue( GetNewName( "彩色图片" ), null, GType.g_cbitmap, "", "",  true, 0, 0 );
		m.ignoreHit = true;
		m.isNewTick = n_MyObject.MyObject.MaxNewTick;
		G.CGPanel.myModuleList.Add( m );
		
		n_NL.NL.CNode.vALUE_img++;
	}
	
	//添加图片
	void ImageButton_MouseDownEvent()
	{
		GVar m = new GVar();
		m.SetUserValue( GetNewName( "图片" ), null, GType.g_bitmap, "", "",  true, 0, 0 );
		m.ignoreHit = true;
		m.isNewTick = n_MyObject.MyObject.MaxNewTick;
		G.CGPanel.myModuleList.Add( m );
		
		n_NL.NL.CNode.vALUE_img++;
	}
	
	//添加字体
	void FontButton_MouseDownEvent()
	{
		GVar m = new GVar();
		m.SetUserValue( GetNewName( "字体" ), null, GType.g_font, "", "",  true, 0, 0 );
		m.ignoreHit = true;
		m.isNewTick = n_MyObject.MyObject.MaxNewTick;
		G.CGPanel.myModuleList.Add( m );
		
		n_NL.NL.CNode.vALUE_font++;
	}
	
	//获取一个新名字
	string GetNewName( string Name )
	{
		int Vari = 1;
		string VarNewName = Name;
		while( true ) {
			if( !G.CGPanel.myModuleList.NameisUsed( VarNewName ) ) {
				break;
			}
			VarNewName = Name + Vari;
			++Vari;
		}
		return VarNewName;
	}
}
}


