﻿using n_Head;

namespace n_Head
{
using System;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Windows.Forms;

using n_Compiler;
using n_GNote;
using n_GUICommon;
using n_GUIset;
using n_MainSystemData;
using n_Head_MtButton;
using n_ImagePanel;
using n_ISP;
using n_MyObjectList;
using n_OS;
using n_Shape;
using n_InsPanel;
using n_VarListPanel;
using n_ModuleLibPanel;
using n_AVRdude;
using n_FloatPanel;
using n_MyObject;
using System.Diagnostics;

//*****************************************************
//标题栏类
public class Head
{
	public delegate void D_Python();
	public D_Python Python;
	
	public delegate void deleAuto();
	public deleAuto DeleAuto;
	
	public delegate void deleDebug();
	public deleDebug DeleDebug;
	
	public delegate void deleSimulate();
	public deleSimulate DeleSimulate;
	
	public delegate void delePy();
	public delePy DelePy;
	
	public delegate void deleUserCode();
	public deleUserCode DeleUserCode;
	
	//界面放缩事件
	public delegate void deleScale( int Delta );
	public deleScale DeleScale;
	
	public delegate void deleNewFile();
	public deleNewFile DeleNewFile;
	public delegate void deleOpenFile();
	public deleOpenFile DeleOpenFile;
	public delegate void deleSaveAsFile();
	public deleSaveAsFile DeleSaveAsFile;
	public delegate void deleToolBox();
	public deleToolBox DeleToolBox;
	
	
	//卡兔 KATU CATU KATO CATO  carto
	
	public Form TargetForm;
	
	MyObjectList myModuleList;
	
	public InsPanel myInsPanel;
	public VarListPanel myVarListPanel;
	
	//浮动信息面板
	public FloatPanel myFloatPanel;
	
	public R_HeadLabel NewHeadLabel;
	
	BotScroll MyBotScroll;
	LeftSlider MyLeftSlider;
	RightSlider MyRightSlider;
	
	public C_MyButton CompButton;
	public C_MyButton ExportButton;
	public C_MyButton SimButton;
	public C_MyButton SimuButton;
	public C_MyButton DebugButton;
	
	public C_MyButton SaveButton;
	public C_MyButton SaveAsButton;
	public C_MyButton NewButton;
	public C_MyButton OpenButton;
	public C_MyButton ToolButton;
	public C_MyButton WebButton;
	
	C_MyButton[] C_MyButtonList;
	int C_MyButtonListLength;
	
	Font f;
	
	public bool isDownLoad;
	public bool isCompile;
	
	public static int ControlWidth = 23;
	public static int ControlHeight = 23;
	
	public static int HeadWidth = 210;
	public static int HidWidth;
	public static int ShowX;
	public static bool HeadShow;
	
	//底边栏高度
	public const int BotScrollHeight = 0; //15;
	
	static int Height = ControlHeight;
	
	public static int MaxWidth;
	public static int MaxHeight;
	
	public static int HeadHeight = 100;
	
	public static int PAD = 5;
	
	public static int HeadLabelHeight = 13 + 40;
	public static int CY = 6;
	
	enum ButtonType {
		Small, Large, Setting, Min, Max, Close
	}
	
	int cmX;
	int cmY;
	
	public static bool AutoHide;
	
	C_MyButton InsButton;
	C_MyButton VarButton;
	C_MyButton ModuleButton;
	
	//C_MyButton VosButton;
	C_MyButton RemoButton;
	C_MyButton PyButton;
	C_MyButton CodeButton;
	
	public C_MyButton UndoButton;
	public C_MyButton RedoButton;
	
	public C_MyButton BeifenButton;
	public C_MyButton CopyButton;
	public C_MyButton PasteButton;
	
	public C_MyButton ScreenButton;
	
	public C_MyButton ComMesButton;
	
	public C_MyButton FTPButton;
	
	public int tempNumber;
	
	Color InsButtonBackColor;
	Color VarButtonBackColor;
	Color ModButtonBackColor;
	
	public static Color InsButtonActiveColor;
	public static Color VarButtonActiveColor;
	public static Color ModButtonActiveColor;
	
	static Brush RightBrush;
	
	//public const int XO = 35;
	public const int XO = 58;
	
	static int TabHeight = 40;
	
	//补充按钮数量偏移
	public const int ButtonNumber = 0;
	
	public static int FreeStartX;
	
	public static int ExtStartIndex;
	public static int COnIndex;
	
	//初始化
	public static void Init()
	{
		//235, 235, 235
		
		//InsButtonActiveColor = Color.FromArgb( 250, 150, 50 );
		//VarButtonActiveColor = Color.FromArgb( 80, 230, 80 );
		//ModButtonActiveColor = Color.FromArgb( 50, 150, 250 );
		
		TabHeight = GUIset.GetPix(TabHeight);
		
		InsButtonActiveColor = Color.Silver;
		VarButtonActiveColor = Color.Silver;
		ModButtonActiveColor = Color.Silver;
		
		RightBrush = new SolidBrush( Color.FromArgb( 247, 251, 255 ) );
		//RightBrush = new SolidBrush( Color.FromArgb( 230, 250, 245 ) );
		
		//InsButtonActiveColor = Color.SlateGray;
		//VarButtonActiveColor = Color.SlateGray;
		//ModButtonActiveColor = Color.SlateGray;
		
		R_HeadLabel.Init();
	}
	
	//构造函数
	public Head( MyObjectList vmyModuleList )
	{
		CodeButton = null;
		
		myModuleList = vmyModuleList;
		
		DeleNewFile = null;
		DeleOpenFile = null;
		
		myInsPanel = new InsPanel( 0, 0 );
		myInsPanel.HideEvent = HideEvent;
		
		myVarListPanel = new VarListPanel( 0, 0 );
		myVarListPanel.HideEvent = HideEvent;
		
		MyModuleLibPanel.HideEvent = HideEvent;
		
		MyBotScroll = new BotScroll();
		MyLeftSlider = new LeftSlider();
		MyRightSlider = new RightSlider();
		
		isDownLoad = false;
		isCompile = false;
		
		HidWidth = SystemData.LeftBarValue;
		ShowX = HidWidth - HeadWidth;
		HeadShow = false;
		
		
		
		InsButtonBackColor = GUIset.GUIbackColor;
		VarButtonBackColor = GUIset.GUIbackColor;
		ModButtonBackColor = GUIset.GUIbackColor;
		
		/*
			InsButtonBackColor = IMVBackColor;
			VarButtonBackColor = IMVBackColor;
			ModButtonBackColor = IMVBackColor;
		 */
		
		NewHeadLabel = new R_HeadLabel( this );
		
		myFloatPanel = new FloatPanel();
		
		f = new Font( "宋体", 11 );
		
		C_MyButtonList = new C_MyButton[ 100 ];
		C_MyButtonListLength = 0;
		
		AutoHide = false;
		
		
		try {
			InitDefault();
		}
		catch(Exception e) {
			MessageBox.Show( e.ToString() );
		}
		
		if( n_ModuleLibPanel.MyModuleLibPanel.FloatButton ) {
			InButton_MouseDownEvent();
		}
		else {
			MoButton_MouseDownEvent();
		}
	}
	
	void InitDefault()
	{
		//窗体控件
		string BaseDir = OS.SystemRoot + "Resource" + OS.PATH_S + "gui" + OS.PATH_S + "GFormControls" + OS.PATH_S;
		
		/*
		Brush LargeB = new SolidBrush( GUIset.GetBlueColor( 255 ) );
		LargeButton = new C_MyButton( C_MyButton.E_ButtonType.BackColor, BaseDir + "放大.png", ControlWidth, ControlHeight, LargeB );
		LargeButton.MouseUpEvent += LargeButton_MouseDownEvent;
		C_MyButtonList[ C_MyButtonListLength ] = LargeButton;
		++C_MyButtonListLength;
		
		Brush SmallB = new SolidBrush( GUIset.GetBlueColor( 255 ) );
		SmallButton = new C_MyButton( C_MyButton.E_ButtonType.BackColor, BaseDir + "缩小.png", ControlWidth, ControlHeight, SmallB );
		SmallButton.MouseUpEvent += SmallButton_MouseDownEvent;
		C_MyButtonList[ C_MyButtonListLength ] = SmallButton;
		++C_MyButtonListLength;
		*/
		
		string BasePath = "ButtonT0" + OS.PATH_S;
		if( G.LightUI ) {
			BasePath = "ButtonT2" + OS.PATH_S;
		}
		BaseDir += BasePath;
		
		string FileExt = ".ico";
		if( G.LightUI ) {
			FileExt = ".png";
		}
		
		int PerW = HeadWidth / 3;
		
		//添加指令
		C_MyButtonList[ C_MyButtonListLength ] = new C_MyButton( C_MyButton.E_ButtonType.Text, PerW, TabHeight, new SolidBrush( GUIset.GUIbackColor ) );
		C_MyButtonList[ C_MyButtonListLength ].MouseDownEvent += InButton_MouseDownEvent;
		C_MyButtonList[ C_MyButtonListLength ].SetLocation( new Point( 0, HeadLabelHeight - 1 ) );
		C_MyButtonList[ C_MyButtonListLength ].Text = n_Language.Language.L_Ins;
		InsButton = C_MyButtonList[ C_MyButtonListLength ];
		++C_MyButtonListLength;
		
		//添加变量
		C_MyButtonList[ C_MyButtonListLength ] = new C_MyButton( C_MyButton.E_ButtonType.Text, PerW, TabHeight, new SolidBrush( GUIset.GUIbackColor ) );
		C_MyButtonList[ C_MyButtonListLength ].MouseDownEvent += ZsButton_MouseDownEvent;
		C_MyButtonList[ C_MyButtonListLength ].SetLocation( new Point( PerW, HeadLabelHeight - 1 ) );
		C_MyButtonList[ C_MyButtonListLength ].Text = n_Language.Language.L_Element;
		VarButton = C_MyButtonList[ C_MyButtonListLength ];
		++C_MyButtonListLength;
		
		//添加模块
		C_MyButtonList[ C_MyButtonListLength ] = new C_MyButton( C_MyButton.E_ButtonType.Text, PerW, TabHeight, new SolidBrush( GUIset.GUIbackColor ) );
		C_MyButtonList[ C_MyButtonListLength ].MouseDownEvent += MoButton_MouseDownEvent;
		C_MyButtonList[ C_MyButtonListLength ].SetLocation( new Point( PerW*2, HeadLabelHeight - 1 ) );
		C_MyButtonList[ C_MyButtonListLength ].Text = n_Language.Language.L_Module;
		ModuleButton = C_MyButtonList[ C_MyButtonListLength ];
		++C_MyButtonListLength;
		
		FreeStartX = 4;
		
		int ButtonWidth = GUIset.GetPix( 60 );
		int ButtonWidth2 = GUIset.GetPix( 90 );
		int ButtonWidth_l = GUIset.GetPix( 160 );
		int ButtonWidth_lc = GUIset.GetPix( 130 );
		int ButtonHeight = GUIset.GetPix( 40 );
		
		if( !G.LightUI ) {
			ButtonWidth = 40;
		}
		n_Head_MtButton.C_MyButton.Init( OS.SystemRoot + "Resource" + OS.PATH_S + "gui" + OS.PATH_S + "GFormControls" + OS.PATH_S + "logo" + OS.PATH_S + "game.png" );
		
		//编译下载 / 切换界面
		int cw = ButtonWidth_l;
		string CompileImage = OS.SystemRoot + "RV" + OS.PATH_S + OS.USER + OS.PATH_S + "logo.png";
		if( G.isCruxEXE ) {
			CompileImage = OS.SystemRoot + "Resource" + OS.PATH_S + "gui" + OS.PATH_S + "GFormControls" + OS.PATH_S + "logo" + OS.PATH_S + "crux.png";
			cw = ButtonWidth_lc;
		}
		if( G.GuangZhouTest ) {
			CompileImage = OS.SystemRoot + "Resource" + OS.PATH_S + "gui" + OS.PATH_S + "GFormControls" + OS.PATH_S + "logo" + OS.PATH_S + OS.USER + "_gz.png";
		}
		C_MyButtonList[ C_MyButtonListLength ] = new C_MyButton( C_MyButton.E_ButtonType.ScaleImage, CompileImage, cw, ButtonHeight, null );
		
		if( G.isCruxEXE ) {
			C_MyButtonList[ C_MyButtonListLength ].MouseDownEvent += UserCodeButton_MouseDownEvent;
			C_MyButtonList[ C_MyButtonListLength ].TipText = n_Language.Language.SwitchPanel;
		}
		else {
			C_MyButtonList[ C_MyButtonListLength ].MouseDownEvent += DownLoadButton_MouseDownEvent;
			C_MyButtonList[ C_MyButtonListLength ].TipText = n_Language.Language.DownLoad;
		}
		
		C_MyButtonList[ C_MyButtonListLength ].SetLocation( new Point( 4, CY ) );
		C_MyButtonList[ C_MyButtonListLength ].TipBrush = Brushes.CornflowerBlue;
		++C_MyButtonListLength;
		FreeStartX += cw + PAD;
		
		//编译
		if( G.isCruxEXE ) {
			string CompImage = BaseDir + "Comp" + FileExt;
			CompButton = new C_MyButton( C_MyButton.E_ButtonType.ScaleImage, CompImage, ButtonWidth2, ButtonHeight, null );
			C_MyButtonList[ C_MyButtonListLength ] = CompButton;
			C_MyButtonList[ C_MyButtonListLength ].MouseDownEvent += DownLoadButton_MouseDownEvent;
			C_MyButtonList[ C_MyButtonListLength ].SetLocation( new Point( FreeStartX, CY ) );
			C_MyButtonList[ C_MyButtonListLength ].TipText = n_Language.Language.DownLoad;
			C_MyButtonList[ C_MyButtonListLength ].TipBrush = new SolidBrush( Color.FromArgb( 60, 180, 40 ) );
			++C_MyButtonListLength;
			FreeStartX += ButtonWidth2 + PAD;
		}
		
		//if( n_ModuleLibPanel.MyModuleLibPanel.LabType == n_GUIcoder.labType.Default || n_ModuleLibPanel.MyModuleLibPanel.LabType == n_GUIcoder.labType.Code ) {
			string ExportImage = BaseDir + "Export" + FileExt;
			ExportButton = new C_MyButton( C_MyButton.E_ButtonType.ScaleImage, ExportImage, ButtonWidth2, ButtonHeight, null );
			C_MyButtonList[ C_MyButtonListLength ] = ExportButton;
			C_MyButtonList[ C_MyButtonListLength ].MouseDownEvent += ExportButton_MouseDownEvent;
			C_MyButtonList[ C_MyButtonListLength ].SetLocation( new Point( FreeStartX, CY ) );
			C_MyButtonList[ C_MyButtonListLength ].TipText = n_Language.Language.Export;
			C_MyButtonList[ C_MyButtonListLength ].TipBrush = new SolidBrush( Color.FromArgb( 60, 180, 40 ) );
			++C_MyButtonListLength;
			FreeStartX += ButtonWidth2 + PAD;
		//}
		
		//仿真
		string SimImage = BaseDir + "Sim" + FileExt;
		string SimStopImage = BaseDir + "Stop" + FileExt;
		string SimImage1 = BaseDir + "Sim1" + FileExt;
		string SimStopImage1 = BaseDir + "Stop1" + FileExt;
		SimButton = new C_MyButton( C_MyButton.E_ButtonType.ScaleImage, SimImage, ButtonWidth2, ButtonHeight, null );
		C_MyButtonList[ C_MyButtonListLength ] = SimButton;
		C_MyButtonList[ C_MyButtonListLength ].tempBackImage = new Bitmap( SimStopImage );
		C_MyButtonList[ C_MyButtonListLength ].tempBackImage1 = new Bitmap( SimImage1 );
		C_MyButtonList[ C_MyButtonListLength ].tempBackImage2 = new Bitmap( SimStopImage1 );
		C_MyButtonList[ C_MyButtonListLength ].MouseDownEvent += SimButton_MouseDownEvent;
		C_MyButtonList[ C_MyButtonListLength ].SetLocation( new Point( FreeStartX, CY ) );
		C_MyButtonList[ C_MyButtonListLength ].TipText = n_Language.Language.SimMode;
		C_MyButtonList[ C_MyButtonListLength ].TipBrush = new SolidBrush( Color.FromArgb( 60, 180, 40 ) );
		
		++C_MyButtonListLength;
		FreeStartX += ButtonWidth2 + PAD;
		
		/*
		if( G.TanMode ) {
		//拟物仿真
		if( !G.Donsok ) {
		string SimuImage = OS.SystemRoot + "Resource" + OS.PATH_S + "gui" + OS.PATH_S + "GFormControls" + OS.PATH_S + @"Simu\Simu.ico";
		SimuButton = new C_MyButton( C_MyButton.E_ButtonType.ScaleImage, SimuImage, ButtonWidth, ButtonWidth, null );
		C_MyButtonList[ C_MyButtonListLength ] = SimuButton;
		C_MyButtonList[ C_MyButtonListLength ].MouseDownEvent += SimuButton_MouseDownEvent;
		C_MyButtonList[ C_MyButtonListLength ].SetLocation( new Point( CX, CY ) );
		C_MyButtonList[ C_MyButtonListLength ].TipText = n_Language.Language.WorldMode;
		C_MyButtonList[ C_MyButtonListLength ].TipBrush = new SolidBrush( Color.FromArgb( 4, 174, 218 ) );
		C_MyButtonList[ C_MyButtonListLength ].Visible = true;
		++C_MyButtonListLength;
		CX += XO;
		}
		}
		*/
		
		//保存
		string SaveImage = BaseDir + "Save" + FileExt;
		SaveButton = new C_MyButton( C_MyButton.E_ButtonType.ScaleImage, SaveImage, ButtonWidth, ButtonHeight, null );
		C_MyButtonList[ C_MyButtonListLength ] = SaveButton;
		C_MyButtonList[ C_MyButtonListLength ].MouseDownEvent += SaveButton_MouseDownEvent;
		C_MyButtonList[ C_MyButtonListLength ].SetLocation( new Point( FreeStartX, CY ) );
		C_MyButtonList[ C_MyButtonListLength ].TipText = n_Language.Language.Save;
		C_MyButtonList[ C_MyButtonListLength ].TipBrush = Brushes.DarkOrange;
		++C_MyButtonListLength;
		FreeStartX += ButtonWidth + PAD;
		
		//打开
		string OpenImage = BaseDir + "Open" + FileExt;
		OpenButton = new C_MyButton( C_MyButton.E_ButtonType.ScaleImage, OpenImage, ButtonWidth, ButtonHeight, null );
		C_MyButtonList[ C_MyButtonListLength ] = OpenButton;
		C_MyButtonList[ C_MyButtonListLength ].MouseDownEvent += OpenButton_MouseDownEvent;
		C_MyButtonList[ C_MyButtonListLength ].SetLocation( new Point( FreeStartX, CY ) );
		C_MyButtonList[ C_MyButtonListLength ].TipText = n_Language.Language.Open;
		C_MyButtonList[ C_MyButtonListLength ].TipBrush = Brushes.DarkOrange;
		++C_MyButtonListLength;
		FreeStartX += ButtonWidth + PAD;
		
		//新建
		string NewImage = BaseDir + "New" + FileExt;
		NewButton = new C_MyButton( C_MyButton.E_ButtonType.ScaleImage, NewImage, ButtonWidth, ButtonHeight, null );
		C_MyButtonList[ C_MyButtonListLength ] = NewButton;
		C_MyButtonList[ C_MyButtonListLength ].MouseDownEvent += NewButton_MouseDownEvent;
		C_MyButtonList[ C_MyButtonListLength ].SetLocation( new Point( FreeStartX, CY ) );
		C_MyButtonList[ C_MyButtonListLength ].TipText = n_Language.Language.New;
		C_MyButtonList[ C_MyButtonListLength ].TipBrush = Brushes.DarkOrange;
		++C_MyButtonListLength;
		FreeStartX += ButtonWidth + PAD;
		
		//另存为
		string SaveAsImage = BaseDir + "SaveAs" + FileExt;
		SaveAsButton = new C_MyButton( C_MyButton.E_ButtonType.ScaleImage, SaveAsImage, ButtonWidth, ButtonHeight, null );
		C_MyButtonList[ C_MyButtonListLength ] = SaveAsButton;
		C_MyButtonList[ C_MyButtonListLength ].MouseDownEvent += SaveAsButton_MouseDownEvent;
		C_MyButtonList[ C_MyButtonListLength ].SetLocation( new Point( FreeStartX, CY ) );
		C_MyButtonList[ C_MyButtonListLength ].TipText = n_Language.Language.SaveAs;
		C_MyButtonList[ C_MyButtonListLength ].TipBrush = Brushes.DarkOrange;
		++C_MyButtonListLength;
		FreeStartX += ButtonWidth + PAD;
		
		//工具箱
		string BBSImage = BaseDir + "Tool" + FileExt;
		ToolButton = new C_MyButton( C_MyButton.E_ButtonType.ScaleImage, BBSImage, ButtonWidth, ButtonHeight, null );
		C_MyButtonList[ C_MyButtonListLength ] = ToolButton;
		C_MyButtonList[ C_MyButtonListLength ].MouseDownEvent += BBSButton_MouseDownEvent;
		C_MyButtonList[ C_MyButtonListLength ].SetLocation( new Point( FreeStartX, CY ) );
		C_MyButtonList[ C_MyButtonListLength ].TipText = n_Language.Language.Tool;
		C_MyButtonList[ C_MyButtonListLength ].TipBrush = Brushes.DodgerBlue;
		++C_MyButtonListLength;
		FreeStartX += ButtonWidth + PAD;
		
		//官网
		if( OS.USER != "hoperun" ) {
		string LinkImage = BaseDir + "Web" + FileExt;
		WebButton = new C_MyButton( C_MyButton.E_ButtonType.ScaleImage, LinkImage, ButtonWidth, ButtonHeight, null );
		C_MyButtonList[ C_MyButtonListLength ] = WebButton;
		C_MyButtonList[ C_MyButtonListLength ].MouseDownEvent += LinkButton_MouseDownEvent;
		C_MyButtonList[ C_MyButtonListLength ].SetLocation( new Point( FreeStartX, CY ) );
		C_MyButtonList[ C_MyButtonListLength ].TipText = n_Language.Language.CurrentVersion1 + G.VersionShow + "(" + G.VersionMessage + ")";
		C_MyButtonList[ C_MyButtonListLength ].TipBrush = Brushes.LimeGreen;
		
		string D2Image = OS.SystemRoot + "Resource" + OS.PATH_S + "gui" + OS.PATH_S + "GFormControls" + OS.PATH_S + "D2.jpg";
		C_MyButtonList[ C_MyButtonListLength ].D2Image = new Bitmap( D2Image );
		++C_MyButtonListLength;
		FreeStartX += ButtonWidth + PAD;
		}
		
		
		
		/*
		if( false ) {
			//NOC定制
			string NocImage = OS.SystemRoot + "Resource" + OS.PATH_S + "gui" + OS.PATH_S + "GFormControls" + OS.PATH_S + "noc1.png";
			C_MyButtonList[ C_MyButtonListLength ] = new C_MyButton( C_MyButton.E_ButtonType.ScaleImage, NocImage, 300, 66, null );
			C_MyButtonList[ C_MyButtonListLength ].MouseDownEvent += NocButton_MouseDownEvent;
			C_MyButtonList[ C_MyButtonListLength ].SetLocation( new Point( CX, 0 ) );
			C_MyButtonList[ C_MyButtonListLength ].TipText = "点击进入noc官网(www.noc.net.cn)";
			C_MyButtonList[ C_MyButtonListLength ].TipBrush = Brushes.ForestGreen;
			++C_MyButtonListLength;
			CX += C_MyButtonList[ C_MyButtonListLength-1 ].Width + 20;
		}
		*/
		
		SolidBrush bb = new SolidBrush( Color.Gainsboro );
		
		int tempX = 0;
		if( G.LightUI ) {
			
			/*
			//外挂神器
			string VosImage = BaseDir + @"vos" + FileExt;
			VosButton = new C_MyButton( C_MyButton.E_ButtonType.ScaleImage, VosImage, ButtonWidth2, ButtonHeight, null );
			C_MyButtonList[ C_MyButtonListLength ] = VosButton;
			C_MyButtonList[ C_MyButtonListLength ].MouseDownEvent += VosButton_MouseDownEvent;
			C_MyButtonList[ C_MyButtonListLength ].SetLocation( new Point( tempX, CY ) );
			C_MyButtonList[ C_MyButtonListLength ].TipText = n_Language.Language.VosPad;
			C_MyButtonList[ C_MyButtonListLength ].TipBrush = Brushes.SteelBlue;
			++C_MyButtonListLength;
			tempX += XO;
			*/
			
			//遥控器
			string RemoImage = BaseDir + @"r" + FileExt;
			RemoButton = new C_MyButton( C_MyButton.E_ButtonType.ScaleImage, RemoImage, ButtonWidth2, ButtonHeight, null );
			C_MyButtonList[ C_MyButtonListLength ] = RemoButton;
			C_MyButtonList[ C_MyButtonListLength ].MouseDownEvent += RemoButton_MouseDownEvent;
			C_MyButtonList[ C_MyButtonListLength ].SetLocation( new Point( tempX, CY ) );
			C_MyButtonList[ C_MyButtonListLength ].TipText = n_Language.Language.RemoPad;
			C_MyButtonList[ C_MyButtonListLength ].TipBrush = Brushes.SteelBlue;
			++C_MyButtonListLength;
			tempX += XO;
			
			/*
			if( G.isCruxEXE ) {
				//查看代码
				string CodeImage = BaseDir + "Code.ico";
				CodeButton = new C_MyButton( C_MyButton.E_ButtonType.ScaleImage, CodeImage, ButtonHeight, ButtonHeight, bb );
				C_MyButtonList[ C_MyButtonListLength ] = CodeButton;
				C_MyButtonList[ C_MyButtonListLength ].MouseDownEvent += UserCodeButton_MouseDownEvent;
				C_MyButtonList[ C_MyButtonListLength ].SetLocation( new Point( tempX, CY ) );
				C_MyButtonList[ C_MyButtonListLength ].TipText = n_Language.Language.SysCode;
				C_MyButtonList[ C_MyButtonListLength ].TipBrush = Brushes.SteelBlue;
				++C_MyButtonListLength;
				tempX += XO;
			}
			*/
		}
		else {
			
			//遥控器
			string RemoImage = BaseDir + @"r.ico";
			string RemoImage1 = BaseDir + @"r1.ico";
			RemoButton = new C_MyButton( C_MyButton.E_ButtonType.SwitchImage, RemoImage, RemoImage1, RemoImage, ButtonWidth * 2, ButtonWidth, null );
			C_MyButtonList[ C_MyButtonListLength ] = RemoButton;
			C_MyButtonList[ C_MyButtonListLength ].MouseDownEvent += RemoButton_MouseDownEvent;
			C_MyButtonList[ C_MyButtonListLength ].SetLocation( new Point( tempX, CY ) );
			C_MyButtonList[ C_MyButtonListLength ].TipText = n_Language.Language.RemoPad;
			C_MyButtonList[ C_MyButtonListLength ].TipBrush = Brushes.SteelBlue;
			++C_MyButtonListLength;
			tempX += XO;
			
			//查看代码
			string CodeImage = BaseDir + "Code.ico";
			string CodeImage1 = BaseDir + "Code1.ico";
			CodeButton = new C_MyButton( C_MyButton.E_ButtonType.SwitchImage, CodeImage, CodeImage1, CodeImage, ButtonWidth, ButtonHeight, null );
			C_MyButtonList[ C_MyButtonListLength ] = CodeButton;
			C_MyButtonList[ C_MyButtonListLength ].MouseDownEvent += UserCodeButton_MouseDownEvent;
			C_MyButtonList[ C_MyButtonListLength ].SetLocation( new Point( tempX, CY ) );
			C_MyButtonList[ C_MyButtonListLength ].TipText = n_Language.Language.SysCode;
			C_MyButtonList[ C_MyButtonListLength ].TipBrush = Brushes.SteelBlue;
			++C_MyButtonListLength;
			tempX += XO;
		}
		
		
		//查看python
		if( G.GuangZhouTest ) {
			string PyImage = OS.SystemRoot + "Resource" + OS.PATH_S + "gui" + OS.PATH_S + "GFormControls" + OS.PATH_S + "py" + OS.PATH_S + @"code.png";
			string PyImage1 = OS.SystemRoot + "Resource" + OS.PATH_S + "gui" + OS.PATH_S + "GFormControls" + OS.PATH_S + "py" + OS.PATH_S + @"code1.png";
			PyButton = new C_MyButton( C_MyButton.E_ButtonType.SwitchImage, PyImage, PyImage1, PyImage, 100, 48, null );
			C_MyButtonList[ C_MyButtonListLength ] = PyButton;
			//C_MyButtonList[ C_MyButtonListLength ].MouseDownEvent += CodeButton_MouseDownEvent;
			C_MyButtonList[ C_MyButtonListLength ].SetLocation( new Point( CodeButton.X - PyButton.Width - 10, 1 ) );
			C_MyButtonList[ C_MyButtonListLength ].TipText = n_Language.Language.Python;
			C_MyButtonList[ C_MyButtonListLength ].TipBrush = Brushes.SteelBlue;
			++C_MyButtonListLength;
		}
		
		
		//右边栏控件
		//=======================================
		int BWidth = 32;
		ExtStartIndex = C_MyButtonListLength;
		
		string redoPath = "undo-redo1" + OS.PATH_S;
		
		//撤消操作
		string UndoImage0 = OS.SystemRoot + "Resource" + OS.PATH_S + "gui" + OS.PATH_S + "GFormControls" + OS.PATH_S + redoPath + "Undo0.png";
		UndoButton = new C_MyButton( C_MyButton.E_ButtonType.BackColor, UndoImage0, BWidth, BWidth, bb );
		C_MyButtonList[ C_MyButtonListLength ] = UndoButton;
		C_MyButtonList[ C_MyButtonListLength ].MouseDownEvent += UndoButton_MouseDownEvent;
		C_MyButtonList[ C_MyButtonListLength ].SetLocation( new Point( FreeStartX, HeadLabelHeight + 4 ) );
		C_MyButtonList[ C_MyButtonListLength ].TipText = n_Language.Language.UndoTip1 + " 0 " + n_Language.Language.UndoTip2;
		C_MyButtonList[ C_MyButtonListLength ].TipBrush = Brushes.Purple;
		++C_MyButtonListLength;
		
		//重做操作
		string RedoImage0 = OS.SystemRoot + "Resource" + OS.PATH_S + "gui" + OS.PATH_S + "GFormControls" + OS.PATH_S + redoPath + "Redo0.png";
		RedoButton = new C_MyButton( C_MyButton.E_ButtonType.BackColor, RedoImage0, BWidth, BWidth, bb );
		C_MyButtonList[ C_MyButtonListLength ] = RedoButton;
		C_MyButtonList[ C_MyButtonListLength ].MouseDownEvent += RedoButton_MouseDownEvent;
		C_MyButtonList[ C_MyButtonListLength ].SetLocation( new Point( FreeStartX, HeadLabelHeight + 4 ) );
		C_MyButtonList[ C_MyButtonListLength ].TipText = n_Language.Language.RedoTip1 + " 0 " + n_Language.Language.RedoTip2;
		C_MyButtonList[ C_MyButtonListLength ].TipBrush = Brushes.Purple;
		++C_MyButtonListLength;
		
		//高级设置
		string HSetImage0 = OS.SystemRoot + "Resource" + OS.PATH_S + "gui" + OS.PATH_S + "GFormControls" + OS.PATH_S + redoPath + "Setting0.png";
		string HSetImage = OS.SystemRoot + "Resource" + OS.PATH_S + "gui" + OS.PATH_S + "GFormControls" + OS.PATH_S + redoPath + "Setting.png";
		ComMesButton = new C_MyButton( C_MyButton.E_ButtonType.BackColor, HSetImage0, BWidth, BWidth, bb );
		C_MyButtonList[ C_MyButtonListLength ] = ComMesButton;
		C_MyButtonList[ C_MyButtonListLength ].MouseDownEvent += ComMesButton_MouseDownEvent;
		C_MyButtonList[ C_MyButtonListLength ].SetLocation( new Point( 350 + XO, HeadLabelHeight + 4 ) );
		//C_MyButtonList[ C_MyButtonListLength ].Text = n_Language.Language.UserValueSet;
		C_MyButtonList[ C_MyButtonListLength ].TipText = n_Language.Language.UserValueSetTip;
		C_MyButtonList[ C_MyButtonListLength ].TipBrush = Brushes.IndianRed;
		++C_MyButtonListLength;
		
		//复制
		string CopyImage0 = OS.SystemRoot + "Resource" + OS.PATH_S + "gui" + OS.PATH_S + "GFormControls" + OS.PATH_S + redoPath + "Copy0.png";
		string CopyImage = OS.SystemRoot + "Resource" + OS.PATH_S + "gui" + OS.PATH_S + "GFormControls" + OS.PATH_S + redoPath + "Copy.png";
		CopyButton = new C_MyButton( C_MyButton.E_ButtonType.BackColor, CopyImage0, BWidth, BWidth, bb );
		C_MyButtonList[ C_MyButtonListLength ] = CopyButton;
		C_MyButtonList[ C_MyButtonListLength ].MouseDownEvent += CopyButton_MouseDownEvent;
		C_MyButtonList[ C_MyButtonListLength ].SetLocation( new Point( 350 + XO, HeadLabelHeight + 4 ) );
		//C_MyButtonList[ C_MyButtonListLength ].Text = n_Language.Language.CopyClip;
		C_MyButtonList[ C_MyButtonListLength ].TipText = n_Language.Language.CopyClipTip;
		++C_MyButtonListLength;
		
		//粘贴
		string PasteImage0 = OS.SystemRoot + "Resource" + OS.PATH_S + "gui" + OS.PATH_S + "GFormControls" + OS.PATH_S + redoPath + "Paste0.png";
		string PasteImage = OS.SystemRoot + "Resource" + OS.PATH_S + "gui" + OS.PATH_S + "GFormControls" + OS.PATH_S + redoPath + "Paste.png";
		PasteButton = new C_MyButton( C_MyButton.E_ButtonType.BackColor, PasteImage0, BWidth, BWidth, bb );
		C_MyButtonList[ C_MyButtonListLength ] = PasteButton;
		C_MyButtonList[ C_MyButtonListLength ].MouseDownEvent += PasteButton_MouseDownEvent;
		C_MyButtonList[ C_MyButtonListLength ].SetLocation( new Point( 380 + XO, HeadLabelHeight + 4 ) );
		//C_MyButtonList[ C_MyButtonListLength ].Text = n_Language.Language.Paste;
		C_MyButtonList[ C_MyButtonListLength ].TipText = n_Language.Language.PasteTip;
		++C_MyButtonListLength;
		
		//截图
		string PtrImage0 = OS.SystemRoot + "Resource" + OS.PATH_S + "gui" + OS.PATH_S + "GFormControls" + OS.PATH_S + redoPath + "Ptr0.png";
		ScreenButton = new C_MyButton( C_MyButton.E_ButtonType.BackColor, PtrImage0, BWidth, BWidth, bb );
		C_MyButtonList[ C_MyButtonListLength ] = ScreenButton;
		C_MyButtonList[ C_MyButtonListLength ].MouseDownEvent += ScreenButton_MouseDownEvent;
		C_MyButtonList[ C_MyButtonListLength ].SetLocation( new Point( 380 + XO, HeadLabelHeight + 4 ) );
		//C_MyButtonList[ C_MyButtonListLength ].Text = n_Language.Language.Prt + (n_MainSystemData.SystemData.ScreenKey+1);
		C_MyButtonList[ C_MyButtonListLength ].TipText = n_Language.Language.PrtTip + (n_MainSystemData.SystemData.ScreenKey+1) + ")";
		++C_MyButtonListLength;
		
		//添加测试指令
		//AddTest();
	}
	
	//--------------------------------------------------------------------------------------------------------------------
	
	//撤销
	void UndoButton_MouseDownEvent()
	{
		if( G.SimulateMode ) {
			n_Debug.Warning.WarningMessage = "仿真模式下无法撤销操作, 请先结束仿真";
			return;
		}
		G.CGPanel.HistoryList_Undo();
	}
	
	//重做
	void RedoButton_MouseDownEvent()
	{
		if( G.SimulateMode ) {
			n_Debug.Warning.WarningMessage = "仿真模式下无法重做操作, 请先结束仿真";
			return;
		}
		G.CGPanel.HistoryList_Redo();
	}
	
	//高级设置
	void ComMesButton_MouseDownEvent()
	{
		if( G.USetBox == null ) {
			G.USetBox = new n_USetForm.USetForm();
		}
		G.USetBox.Run();
	}
	
	//遥控器
	void RemoButton_MouseDownEvent()
	{
		if( G.RemoBox == null ) {
			G.RemoBox = new n_RemoForm.RemoForm();
		}
		G.RemoBox.Run();
	}
	
	//查看用户代码
	void UserCodeButton_MouseDownEvent()
	{
		//G.CodeViewBox.Run();
		if( DeleUserCode != null ) {
			DeleUserCode();
		}
	}
	
	//--------------------------------------------------------------------------------------------------------------------
	
	public void MoButton_MouseDownEvent()
	{
		if( G.isCruxEXE && G.SimulateMode ) {
			return;
		}
		MyModuleLibPanel.SX = 0;
		//保持上次的位置
		//MyModuleLibPanel.SY = MyModuleLibPanel.TopY;
		ModuleTree.Width = ModuleTree.CWidth;
		if( MyModuleLibPanel.isMouseDown ) {
			MyModuleLibPanel.isMouseDown = false;
		}
		MyModuleLibPanel.Visible = true;
		
		ModuleButton.Height = TabHeight + 1;
		ModuleButton.BackBrush0.Color = ModButtonActiveColor;
		ModuleButton.BackBrush.Color = ModuleTree.TreeBackBrush.Color;
		ModuleButton.TextBrush = Brushes.Black;
		
		InsButton.Height = TabHeight;
		InsButton.BackBrush0.Color = InsButtonBackColor;
		InsButton.BackBrush.Color = GUIset.GUIbackColor;
		InsButton.TextBrush = Brushes.SlateGray;
		
		VarButton.Height = TabHeight;
		VarButton.BackBrush0.Color = VarButtonBackColor;
		VarButton.BackBrush.Color = GUIset.GUIbackColor;
		VarButton.TextBrush = Brushes.SlateGray;
		
		myInsPanel.ShowLogicPanel = false;
		myVarListPanel.ShowLogicPanel = false;
	}
	
	void ZsButton_MouseDownEvent()
	{
		if( G.isCruxEXE && G.SimulateMode ) {
			return;
		}
		myVarListPanel.SY = 0;
		myVarListPanel.ShowLogicPanel = true;
		
		VarButton.Height = TabHeight + 1;
		VarButton.BackBrush0.Color = VarButtonActiveColor;
		VarButton.BackBrush.Color = myVarListPanel.BackColor;
		VarButton.TextBrush = Brushes.Black;
		
		InsButton.Height = TabHeight;
		InsButton.BackBrush0.Color = InsButtonBackColor;
		InsButton.BackBrush.Color = GUIset.GUIbackColor;
		InsButton.TextBrush = Brushes.SlateGray;
		
		ModuleButton.Height = TabHeight;
		ModuleButton.BackBrush0.Color = ModButtonBackColor;
		ModuleButton.BackBrush.Color = GUIset.GUIbackColor;
		ModuleButton.TextBrush = Brushes.SlateGray;
		
		
		MyModuleLibPanel.Visible = false;
		myInsPanel.ShowLogicPanel = false;
		
		MyModuleLibPanel.CanelHilight();
	}
	
	void InButton_MouseDownEvent()
	{
		if( G.isCruxEXE && G.SimulateMode ) {
			return;
		}
		myInsPanel.SY = 0;
		myInsPanel.ShowLogicPanel = true;
		
		ModuleButton.Height = TabHeight;
		ModuleButton.BackBrush0.Color = ModButtonBackColor;
		ModuleButton.BackBrush.Color = GUIset.GUIbackColor;
		ModuleButton.TextBrush = Brushes.SlateGray;
		
		VarButton.Height = TabHeight;
		VarButton.BackBrush0.Color = VarButtonBackColor;
		VarButton.BackBrush.Color = GUIset.GUIbackColor;
		VarButton.TextBrush = Brushes.SlateGray;
		
		InsButton.Height = TabHeight + 1;
		InsButton.BackBrush0.Color = InsButtonActiveColor;
		InsButton.BackBrush.Color = myInsPanel.BackColor;
		InsButton.TextBrush = Brushes.Black;
		
		myVarListPanel.ShowLogicPanel = false;
		MyModuleLibPanel.Visible = false;
		
		MyModuleLibPanel.CanelHilight();
	}
	
	//--------------------------------------------------------------------------------------------------------------------
	
	//组件绘制工作
	public void Draw( Graphics g )
	{
		//禁用图像模糊效果
		//g.InterpolationMode = System.Drawing.Drawing2D.InterpolationMode.NearestNeighbor;
		//g.PixelOffsetMode = System.Drawing.Drawing2D.PixelOffsetMode.Half;
		
		/*
		//更新缩小图标按钮坐标
		SmallButton.SetLocation( GetLocation( ButtonType.Small ) );
		//更新放大图标按钮坐标
		LargeButton.SetLocation( GetLocation( ButtonType.Large ) );
		*/
		
		MyLeftSlider.Draw( g );
		//MyRightSlider.Draw( g ); //暂时隐藏右边栏
		
		//绘制逻辑模块面板
		//LogicPanel.Draw( g, StartX, StartY );
		
		//绘制分页栏面板
		//InsTabPanel.Draw( g );
		//VarTabPanel.Draw( g );
		//ModuleTabPanel.Draw( g );
		
		
		
		//临时设置按钮可见属性
		if( n_ModuleLibPanel.MyModuleLibPanel.LabType == n_GUIcoder.labType.Game ) {
			
			if( C_MyButtonList[3].BackImage1 != n_Head_MtButton.C_MyButton.tempGameImg ) {
				//C_MyButtonList[3].BackImage = n_Head_MtButton.C_MyButton.tempGameImg;
				C_MyButtonList[3].TipText = "打开动画场景窗口";
				C_MyButtonList[3].BackImage1 = n_Head_MtButton.C_MyButton.tempGameImg;
				//C_MyButtonList[3].BackImage2 = n_Head_MtButton.C_MyButton.tempGameImg;
				
				//隐藏仿真按钮
				SimButton.Visible = false;
				RemoButton.Visible = false;
				
				SaveAsButton.X -= XO * 1;
				SaveButton.X -= XO * 1;
				NewButton.X -= XO * 1;
				OpenButton.X -= XO * 1;
				ToolButton.X -= XO * 1;
				WebButton.X -= XO * 1;
			}
		}
		
		//绘制指令面板
		myInsPanel.Draw( g );
		//绘制变量面板
		myVarListPanel.Draw( g );
		//绘制组件库
		MyModuleLibPanel.Draw( g );
		
		//绘制右边栏的背景
		//g.FillRectangle( RightBrush, G.CGPanel.Width - ImagePanel.RShowX, 0, ImagePanel.RShowX, G.CGPanel.Height );
		//g.DrawLine( Pens.Gray, G.CGPanel.Width - ImagePanel.RShowX, 0, G.CGPanel.Width - ImagePanel.RShowX, G.CGPanel.Height );
		
		
		//指令按钮边缘有点模糊, 所以不用模糊效果
		
		int tx4 = C_MyButtonList[ 0 ].X;
		int tx5 = C_MyButtonList[ 1 ].X;
		int tx6 = C_MyButtonList[ 2 ].X;
		
		if( !ModuleDetail.ShowDetail || !MyModuleLibPanel.Visible ) {
			C_MyButtonList[ 2 ].X += n_Head.Head.ShowX;
			C_MyButtonList[ 0 ].X += n_Head.Head.ShowX;
			C_MyButtonList[ 1 ].X += n_Head.Head.ShowX;
		}
		
		C_MyButtonList[ 2 ].Draw( g );
		C_MyButtonList[ 0 ].Draw( g );
		C_MyButtonList[ 1 ].Draw( g );
		
		C_MyButtonList[ 0 ].X = tx4;
		C_MyButtonList[ 1 ].X = tx5;
		C_MyButtonList[ 2 ].X = tx6;
		
		//禁用图像模糊效果
		//g.InterpolationMode = System.Drawing.Drawing2D.InterpolationMode.NearestNeighbor;
		//g.PixelOffsetMode = System.Drawing.Drawing2D.PixelOffsetMode.Half;
		MyModuleLibPanel.Draw1( g );
		
		//绘制标题
		NewHeadLabel.Draw( g );
		
		
		//启用图像模糊效果
		//g.InterpolationMode = System.Drawing.Drawing2D.InterpolationMode.Default;
		//g.PixelOffsetMode = System.Drawing.Drawing2D.PixelOffsetMode.Default;
		
		//辅助按钮背景
		if( !n_ModuleLibPanel.ModuleDetail.ShowDetail ) {
			g.FillRectangle( GUIset.BackBrush, UndoButton.X - 5, UndoButton.Y - 5, UndoButton.Width * 2 + 15, (ComMesButton.Y + ComMesButton.Height - UndoButton.Y) + 10 );
			g.DrawRectangle( Pens.Gainsboro, UndoButton.X - 5, UndoButton.Y - 5, UndoButton.Width * 2 + 14, (ComMesButton.Y + ComMesButton.Height - UndoButton.Y) + 10 );
		}
		
		
		//撤销重做背景
		//g.FillEllipse( Brushes.WhiteSmoke, X - oox, Y - oox, Height + oox*2 - 1, Height + oox*2 - 1 );
		//g.FillEllipse( Brushes.WhiteSmoke, X + Width - Height - oox, Y - oox, Height + oox*2 - 1, Height + oox*2 - 1 );
		//g.FillRectangle( Brushes.WhiteSmoke, X + Height/2 - oox, Y - oox, Height + oox*2 - 1, Height + oox*2 - 1 );
		
		//绘制按钮
		for( int i = 3; i < C_MyButtonListLength; ++i ) {
			if( C_MyButtonList[ i ] == WebButton && n_Debug.Warning.BugMessage != null && ImagePanel.Flash ) {
				continue;
			}
			if( n_ModuleLibPanel.ModuleDetail.ShowDetail && i >= ExtStartIndex ) {
				break;
			}
			C_MyButtonList[ i ].Draw( g );
		}
		
		//绘制版本号
		//g.DrawString( G.VersionShow, GUIset.ExpFont, Brushes.White, 180, -4 );
		
		
		g.DrawString( "X: " + cmX, GUIset.ExpFont, Brushes.SlateGray, MaxWidth - 80, MaxHeight - 135 );
		g.DrawString( "Y: " + cmY, GUIset.ExpFont, Brushes.SlateGray, MaxWidth - 80, MaxHeight - 115 );
		
		myFloatPanel.Draw( g );
		MyBotScroll.Draw( g );
	}
	
	//鼠标左键按下事件
	public bool LeftMouseDown( int mX, int mY )
	{
		if( MyBotScroll.MouseDown( mX, mY ) ) {
			return true;
		}
		if( MyLeftSlider.MouseDown( mX, mY ) ) {
			return true;
		}
		if( MyRightSlider.MouseDown( mX, mY ) ) {
			return true;
		}
		//轮询按钮列表
		for( int i = 0; i < C_MyButtonListLength; ++i ) {
			if( C_MyButtonList[ i ].Visible && C_MyButtonList[ i ].MouseDown( mX, mY ) ) {
				return true;
			}
		}
		//标签头部, 用于窗体移动
		if( NewHeadLabel.MouseDown( mX, mY ) ) {
			return true;
		}
		
		//MyModuleLibPanel.TempUserMouseDown( HeadShow );
		
		if( !HeadShow ) {
			return false;
		}
		
		//分页栏面板
		//if( InsTabPanel.LeftMouseDown( mX, mY ) ) {
		//	return true;
		//}
		//分页栏面板
		//if( VarTabPanel.LeftMouseDown( mX, mY ) ) {
		//	return true;
		//}
		//分页栏面板
		//if( ModuleTabPanel.LeftMouseDown( mX, mY ) ) {
		//	return true;
		//}
		
		if( !HeadShow ) {
			return false;
		}
		
		if( G.isCruxEXE && G.SimulateMode ) {
			return false;
		}
		
		//指令面板
		if( myInsPanel.LeftMouseDown( mX, mY ) ) {
			return true;
		}
		//变量面板
		if( myVarListPanel.LeftMouseDown( mX, mY ) ) {
			return true;
		}
		//模块库
		if( MyModuleLibPanel.UserMouseDown( mX, mY ) ) {
			return true;
		}
		return false;
	}
	
	//鼠标左键松开事件
	public void LeftMouseUp( int mX, int mY )
	{
		MyBotScroll.MouseUp( mX, mY );
		MyLeftSlider.MouseUp( mX, mY );
		MyRightSlider.MouseUp( mX, mY );
		
		//轮询按钮列表
		for( int i = 0; i < C_MyButtonListLength; ++i ) {
			if( C_MyButtonList[ i ].Visible ) {
				C_MyButtonList[ i ].MouseUp( mX, mY );
			}
		}
		
		NewHeadLabel.MouseUp( mX, mY );
		
		if( !n_Head.Head.HeadShow ) {
			return;
		}
		
		if( G.isCruxEXE && G.SimulateMode ) {
			return;
		}
		
		myInsPanel.LeftMouseUp( mX, mY );
		myVarListPanel.LeftMouseUp( mX, mY );
		MyModuleLibPanel.UserMouseUp( mX, mY );
	}
	
	//鼠标移动事件,当鼠标在组件上时返回1,组件被拖动时返回2
	public bool MouseMove( int mX, int mY )
	{
		cmX = mX;
		cmY = mY;
		bool isMouseOn = false;
		
		isMouseOn |= MyBotScroll.MouseMove( mX, mY );
		isMouseOn |= MyLeftSlider.MouseMove( mX, mY );
		isMouseOn |= MyRightSlider.MouseMove( mX, mY );
		
		//控件按钮鼠标移动事件, 轮询按钮列表
		COnIndex = -1;
		for( int i = 0; i < C_MyButtonListLength; ++i ) {
			if( !C_MyButtonList[ i ].Visible ) {
				continue;
			}
			bool on = C_MyButtonList[ i ].MouseMove( mX, mY );
			if( on ) {
				COnIndex = i;
			}
		}
		//标题栏路径类
		isMouseOn |= NewHeadLabel.MouseMove( mX, mY );
		
		if( !(G.isCruxEXE && G.SimulateMode) && !isMouseOn ) {
			//指令面板类
			isMouseOn |= myInsPanel.MouseMove( mX, mY );
			//变量面板类
			isMouseOn |= myVarListPanel.MouseMove( mX, mY );
			//组件库面板
			isMouseOn |= MyModuleLibPanel.UserMouseMove( mX, mY );
			
			if( !isMouseOn ) {
				//分页栏面板类
				//isMouseOn |= InsTabPanel.MouseMove( mX, mY );
				//isMouseOn |= VarTabPanel.MouseMove( mX, mY );
				//isMouseOn |= ModuleTabPanel.MouseMove( mX, mY );
			}
		}
		
		RXChanged();
		
		isMouseOn |= mX < ShowX + 210 || mX > G.CGPanel.Width - ImagePanel.RShowX || ModuleDetail.ShowDetail;
		
		
		
		//这里临时加, 鼠标移动即取消未完成操作
		MyModuleLibPanel.TempUserMouseDown( HeadShow );
		
		
		
		return isMouseOn;
	}
	
	public void RXChanged()
	{
		int tx = G.CGPanel.Width - (RedoButton.Width) * 2 - 10;
		int th = HeadLabelHeight + 4;
		
		UndoButton.X = tx;
		RedoButton.X = UndoButton.X + UndoButton.Width + 5;
		RedoButton.Y = th;
		UndoButton.Y = th;
		th += UndoButton.Height + 5;
		
		CopyButton.X = tx;
		PasteButton.X = CopyButton.X + CopyButton.Width + 5;
		CopyButton.Y = th;
		PasteButton.Y = th;
		th += PasteButton.Height + 5;
		
		ScreenButton.X = tx;
		ComMesButton.X = ScreenButton.X + ScreenButton.Width + 5;
		ScreenButton.Y = th;
		ComMesButton.Y = th;
		th += ComMesButton.Height + 5;
		
		RecycleChange();
	}
	
	public void RecycleChange()
	{
		G.CGPanel.myRecycler.SX = G.CGPanel.Width - G.CGPanel.myRecycler.Width - 5 - ImagePanel.RHidWidth;
		G.CGPanel.myLineColorBox.ResetLocation();
	}
	
	//尺寸改变事件
	public void SizeChanged( object sender, EventArgs e )
	{
		MaxWidth = G.CGPanel.Width;
		MaxHeight = G.CGPanel.Height;
		
		int mh = MaxHeight - R_HeadLabel.Height;
		
		int PH = (mh- 10) / 3 + 1;
		int He = (mh - 40) / 3;
		
		myInsPanel.Height = G.CGPanel.Height - BotScrollHeight;
		myVarListPanel.Height = G.CGPanel.Height - BotScrollHeight;
		ModuleTree.Height = G.CGPanel.Height - BotScrollHeight;
		
		MyModuleLibPanel.ResetScroll();
		
		myFloatPanel.SX = MaxWidth - myFloatPanel.Width - 5;
		
		RXChanged();
		
		RemoButton.X = G.CGPanel.Width - RemoButton.Width - 10; //170;
		//RemoButton.X += 100;
		
		if( PyButton != null ) {
			PyButton.X = CodeButton.X - PyButton.Width - 10;
		}
		
		if( CodeButton != null ) {
			CodeButton.X = ImagePanel.FormWidth - CodeButton.Width - 23;
		}
		
		//BeifenButton.Y = HeadLabelHeight + 10 + 40 + 10 + 40;
		//CopyButton.Y = HeadLabelHeight + 10 + 40 + 10;
		//PasteButton.Y = CopyButton.Y + CopyButton.Height + 10;
		//ComMesButton.Y = PasteButton.Y + PasteButton.Height + 10;
		//ScreenButton.Y = ComMesButton.Y + ComMesButton.Height + 10;
		
		NewHeadLabel.SizeChanged();
		MyBotScroll.SizeChanged();
		MyLeftSlider.SizeChanged();
		MyRightSlider.SizeChanged();
	}
	
	//鼠标离开
	public void MouseLeave()
	{
		for( int i = 0; i < C_MyButtonListLength; ++i ) {
			if( C_MyButtonList[ i ].isMouseOn ) {
				C_MyButtonList[ i ].isMouseOn = false;
			}
		}
	}
	
	void HideEvent()
	{
		//InsTabPanel.Hide = false;
		//VarTabPanel.Hide = false;
		//ModuleTabPanel.Hide = false;
	}
	
	void MouseInInsTabPanelEvent()
	{
		myInsPanel.SY = 0;
		myInsPanel.ShowLogicPanel = true;
	}
	
	void MouseInVarTabPanelEvent()
	{
		myVarListPanel.SY = 0;
		myVarListPanel.ShowLogicPanel = true;
	}
	
	void MouseInModuleTabPanelEvent()
	{
		MyModuleLibPanel.SX = 0;
		//保持上次的位置
		//MyModuleLibPanel.SY = MyModuleLibPanel.TopY;
		ModuleTree.Width = ModuleTree.CWidth;
		if( MyModuleLibPanel.isMouseDown ) {
			MyModuleLibPanel.isMouseDown = false;
		}
		MyModuleLibPanel.Visible = true;
	}
	
	//----------------------------------------------------------
	//控件点击事件
	
	//放大按钮
	void LargeButton_MouseDownEvent()
	{
		if( DeleScale != null ) {
			DeleScale( 1 );
		}
	}
	
	//缩小按钮
	void SmallButton_MouseDownEvent()
	{
		if( DeleScale != null ) {
			DeleScale( -1 );
		}
	}
	
	//最小化
	void MinButton_MouseDownEvent()
	{
		TargetForm.WindowState = FormWindowState.Minimized;
	}
	
	//最大化
	void MaxButton_MouseDownEvent()
	{
		if( TargetForm.WindowState == FormWindowState.Maximized ) {
			TargetForm.WindowState = FormWindowState.Normal;
		}
		else {
			TargetForm.WindowState = FormWindowState.Maximized;
		}
	}
	
	//关闭
	void CloseButton_MouseDownEvent()
	{
		TargetForm.Close();
	}
	
	//导出代码
	void ExportButton_MouseDownEvent()
	{
		n_GUIcoder.GUIcoder.cmd_export = true;
		DeleAuto();
	}
	
	//仿真
	void SimButton_MouseDownEvent()
	{
		DeleSimulate();
	}
	
	//拟物化仿真
	void SimuButton_MouseDownEvent()
	{
		G.SimBox.Run();
	}
	
	//保存
	void SaveButton_MouseDownEvent()
	{
		if( G.SimulateMode ) {
			n_Debug.Warning.WarningMessage = "仿真时候无法保存程序, 请先结束仿真";
			return;
		}
		G.ccode.Save();
	}
	
	//新建
	void NewButton_MouseDownEvent()
	{
		if( DeleNewFile != null ) {
			DeleNewFile();
		}
	}
	
	//另存为 / 打开所在目录
	public void SaveAsButton_MouseDownEvent()
	{
		/*
		if( G.ccode.isStartFile ) {
			MessageBox.Show( "当前没有打开文件， 所以无法定位到所在的文件夹" );
			return;
		}
		
		string fileToSelect = G.ccode.PathAndName;
        string args = string.Format("/Select, {0}", fileToSelect);
        System.Diagnostics.Process.Start( new System.Diagnostics.ProcessStartInfo("Explorer.exe", args ) );
		*/
        
		//另存为
		if( DeleSaveAsFile != null ) {
			if( G.ccode.Saved != null ) {
				G.ccode.Saved( null, null );
			}
			DeleSaveAsFile();
		}
	}
	
	//工具箱
	void BBSButton_MouseDownEvent()
	{
		if( DeleToolBox != null ) {
			DeleToolBox();
		}
	}
	
	//进入官网
	void LinkButton_MouseDownEvent()
	{
		G.PushBox.Run();
	}
	
	//进入NOC官网
	void NocButton_MouseDownEvent()
	{
		//调用系统默认的浏览器
		string Web = "www.noc.net.cn";
		System.Diagnostics.Process.Start( "http://" + Web );
	}
	
	//打开
	void OpenButton_MouseDownEvent()
	{
		if( DeleOpenFile != null ) {
			DeleOpenFile();
		}
	}
	
	//编译下载
	void DownLoadButton_MouseDownEvent()
	{
		if( n_ModuleLibPanel.MyModuleLibPanel.LabType == n_GUIcoder.labType.Game ) {
			G.SimBox.Run();
		}
		else {
			n_GUIcoder.GUIcoder.cmd_download = true;
			DeleAuto();
		}
	}
	
	//================================================================
	bool Exit = false;
	
	//备份按钮按下时
	void BeifenButton_MouseDownEvent()
	{
		CopyButton_MouseDownEvent();
		
		if( Exit ) {
			PasteButton_MouseDownEvent1();
		}
	}
	
	//复制按钮按下时
	void CopyButton_MouseDownEvent()
	{
		string Result = "//[配置信息开始],\n";
		
		Result += n_GUIcoder.GUIcoder.GetCommonMes();
		
		string LinkResult = null;
		
		Exit = false;
		foreach( MyObject m in myModuleList ) {
			if( !m.isSelect ) {
				continue;
			}
			if( m is n_HardModule.HardModule && ((n_HardModule.HardModule)m).PackHid ) {
				continue;
			}
			Result += n_GUIcoder.GUIcoder.SaveConfig( m );
			
			if( m is n_MyIns.MyIns ) {
				LinkResult += n_GUIcoder.GUIcoder.SaveInsLink( (n_MyIns.MyIns)m );
			}
			Exit = true;
		}
		Result += LinkResult + "//[配置信息结束],";
		
		if( !Exit ) {
			MessageBox.Show( "请先用鼠标右键框选需要复制的多个元素, 再点击复制按钮" );
			return;
		}
		
		try {
			System.Windows.Forms.Clipboard.SetText( Result );
			n_Debug.Warning.WarningMessage = "已复制到剪贴板";
		}
		catch( Exception e ) {
			MessageBox.Show( "复制时出现一个小问题, 估计不会影响操作\n" + e.ToString() );
		}
	}
	
	//粘贴按钮按下时
	void PasteButton_MouseDownEvent()
	{
		if( System.Windows.Forms.Clipboard.GetDataObject().GetFormats().Length != 0 ) {
			
			string Result = System.Windows.Forms.Clipboard.GetText();
			
			G.CGPanel.Pause = true;
			
			n_MyObject.MyObject mo = n_GUIcoder.GUIcoder.AddTextToGPanel( G.CGPanel, Result );
			//MessageBox.Show( "{" + Result + "}" );
			
			if( mo != null ) {
				n_GUIcoder.GUIcoder.ErrorObject = mo;
				G.CGPanel.AddMesPanel( "刚刚粘贴的元素在这里", 1, mo );
				n_GUIcoder.GUIcoder.AutoMove = true;
			}
			
			G.CGPanel.Pause = false;
		}
		else {
			MessageBox.Show( "剪贴板内容为空" );
		}
	}
	
	//================================================================
	
	//截屏
	void ScreenButton_MouseDownEvent()
	{
		if( G.ScreenSetBox == null ) {
			G.ScreenSetBox = new n_ScreenSetForm.ScreenSetForm();
		}
		G.ScreenSetBox.Run();
	}
	
	//粘贴按钮按下时
	void PasteButton_MouseDownEvent1()
	{
		if( System.Windows.Forms.Clipboard.GetDataObject().GetFormats().Length != 0 ) {
			
			string Result = System.Windows.Forms.Clipboard.GetText();
			
			G.CGPanel.Pause = true;
			
			n_MyObject.MyObject mo = n_GUIcoder.GUIcoder.AddTextToGPanel( G.CGPanel, Result );
			//MessageBox.Show( "{" + Result + "}" );
			
			if( mo != null ) {
				n_GUIcoder.GUIcoder.ErrorObject = mo;
				G.CGPanel.AddMesPanel( "刚刚粘贴的指令在这里", 1, mo );
			}
			
			G.CGPanel.Pause = false;
		}
		else {
			MessageBox.Show( "剪贴板内容为空" );
		}
	}
	
	//获取控件位置
	Point GetLocation( ButtonType t )
	{
		if( t == ButtonType.Small ) {
			return new Point( G.CGPanel.Width - 1 - (ControlHeight + 1) * 65/10 + 1, 1 );
		}
		if( t == ButtonType.Large ) {
			return new Point( G.CGPanel.Width - 1 - (ControlHeight + 1) * 55/10, 1 );
		}
		if( t == ButtonType.Setting ) {
			return new Point( G.CGPanel.Width - 1 - (ControlHeight + 1) * 4, 1 );
		}
		if( t == ButtonType.Min ) {
			return new Point( G.CGPanel.Width - 1 - (ControlHeight + 1) * 3, 1 );
		}
		if( t == ButtonType.Max ) {
			return new Point( G.CGPanel.Width - 1 - (ControlHeight + 1) * 2, 1 );
		}
		if( t == ButtonType.Close ) {
			return new Point( G.CGPanel.Width - 1 - (ControlHeight + 1) * 1, 1 );
		}
		return Point.Empty;
	}
}
//左边栏调节器
public class BotScroll
{
	int SX;
	int SY;
	int Width;
	int Height;
	
	bool isMousePress;
	bool isMouseOn;
	
	//构造函数
	public BotScroll()
	{
		SX = 0;
		Width = 210;
		Height = n_Head.Head.BotScrollHeight;
		
		isMousePress = false;
		isMouseOn = false;
	}
	
	//绘制
	public void Draw( Graphics g )
	{
		/*
		int w = n_Head.Head.HeadWidth + n_Head.Head.ShowX - 1;
		if( ModuleDetail.ShowDetail ) {
			w = n_Head.Head.HeadWidth - 1;
		}
		if( isMouseOn || isMousePress ) {
			g.FillRectangle(Brushes.WhiteSmoke, SX, SY, w, Height );
			g.FillRectangle(Brushes.Orange, SX + 2, SY + 2, n_Head.Head.HidWidth - 4, Height - 4 );
		}
		else {
			g.FillRectangle(Brushes.WhiteSmoke, SX, SY, w, Height );
			g.FillRectangle(Brushes.SlateGray, SX + 2, SY + 2, n_Head.Head.HidWidth - 4, Height - 4 );
		}
		*/
	}
	
	//鼠标按下事件
	public bool MouseDown( int mX, int mY )
	{
		return false;
		
		/*
		if( !isMouseOn ) {
			return false;
		}
		isMousePress = true;
		
		n_Head.Head.HidWidth = mX;
		
		return true;
		*/
	}
	
	//鼠标松开事件
	public void MouseUp( int mX, int mY )
	{
		isMousePress = false;
	}
	
	//鼠标移动事件
	public bool MouseMove( int mX, int mY )
	{
		isMouseOn = isMouseInSide( mX, mY );
		
		//如果按下鼠标键, 移动窗体
		if( isMousePress ) {
			
			n_Head.Head.HidWidth = mX;
			
			if( n_Head.Head.HidWidth < 2 ) {
				n_Head.Head.HidWidth = 2;
			}
			if( n_Head.Head.HidWidth > Head.HeadWidth ) {
				n_Head.Head.HidWidth = Head.HeadWidth;
			}
			SystemData.LeftBarValue = n_Head.Head.HidWidth;
			SystemData.isChanged = true;
		}
		return isMouseOn;
	}
	
	//判断指定点是否在内部
	public bool isMouseInSide( int mX, int mY )
	{
		if( mX > SX && mX < SX + Width && mY > SY && mY < SY + Height ) {
			return true;
		}
		return false;
	}
	
	//尺寸改变事件
	public void SizeChanged()
	{
		SY = G.CGPanel.Height - Height;
	}
}
//左边栏调节器
public class LeftSlider
{
	int SX;
	int SY;
	int Width;
	int Height;
	
	bool isMousePress;
	bool isMouseOn;
	
	const int SlideWidth = 8;
	
	//构造函数
	public LeftSlider()
	{
		SX = 0;
		SY = Head.HeadLabelHeight - 1;
		
		Width = SlideWidth;
		
		isMousePress = false;
		isMouseOn = false;
	}
	
	//绘制
	public void Draw( Graphics g )
	{
		if( G.isCruxEXE && G.SimulateMode ) {
			return;
		}
		if( ModuleDetail.ShowDetail ) {
			//return;
		}
		SX = n_Head.Head.HeadWidth + n_Head.Head.ShowX;
		
		if( G.LightUI ) {
			g.FillRectangle( R_HeadLabel.BackBrush, SX, SY, Width, Height );
		}
		else {
			g.FillRectangle( GUIset.BackBrush, SX, SY, Width, Height );
			g.DrawRectangle( Pens.Silver, SX, SY, Width, Height );
		}
		
		if( isMousePress ) {
			g.FillRectangle( Brushes.Chocolate, SX, SY, Width, Height );
			//g.DrawRectangle( Pens.Black, SX, SY, Width, Height );
		}
		else if( isMouseOn ) {
			g.FillRectangle( Brushes.Silver, SX, SY, Width, Height );
			//g.DrawRectangle( Pens.OrangeRed, SX, SY, Width, Height );
			
			int sx = n_Head.Head.HeadWidth + n_Head.Head.ShowX + 10;
			g.DrawString( "<- 鼠标拖动左侧的竖条, 可调节左边栏的默认位置", n_GUIset.GUIset.ExpFont, Brushes.SlateGray, sx, Height + SY - 30 );
		}
		else {
			//...
		}
		
		//临时显示仿真调试状态
		if( G.SimulateMode ) {
			int sx = n_Head.Head.HeadWidth + n_Head.Head.ShowX + 10;
			if( n_GUIcoder.GUIcoder.isStep ) {
				g.DrawString( n_Language.Language.RunMode_Step, n_GUIset.GUIset.ExpFont, Brushes.Black, sx, Head.HeadLabelHeight + 15 );
				g.DrawString( n_Language.Language.RunMode_Full, n_GUIset.GUIset.ExpFont, Brushes.Silver, sx, Head.HeadLabelHeight + 40 );
			}
			else {
				g.DrawString( n_Language.Language.RunMode_Step, n_GUIset.GUIset.ExpFont, Brushes.Silver, sx, Head.HeadLabelHeight + 15 );
				g.DrawString( n_Language.Language.RunMode_Full, n_GUIset.GUIset.ExpFont, Brushes.Black, sx, Head.HeadLabelHeight + 40 );
			}
		}
		else {
			/*
			int sx = n_Head.Head.HeadWidth + n_Head.Head.ShowX + 10;
			string mes = null;
			if( n_GUIcoder.GUIcoder.isStep ) {
				mes = "单步";
			}
			else {
				mes = "全速";
			}
			g.DrawString( mes + " (空格键切换单步/全速)", n_GUIset.GUIset.Font12, Brushes.Silver, sx, Head.HeadLabelHeight + 5 );
			*/
		}
	}
	
	//鼠标按下事件
	public bool MouseDown( int mX, int mY )
	{
		if( G.isCruxEXE && G.SimulateMode ) {
			return false;
		}
		if( ModuleDetail.ShowDetail ) {
			return false;
		}
		if( !isMouseOn ) {
			return false;
		}
		isMousePress = true;
		
		return true;
	}
	
	//鼠标松开事件
	public void MouseUp( int mX, int mY )
	{
		if( G.isCruxEXE && G.SimulateMode ) {
			return;
		}
		isMousePress = false;
	}
	
	//鼠标移动事件
	public bool MouseMove( int mX, int mY )
	{
		//多分辨率切换时进行溢出保护
		if( n_Head.Head.HidWidth > n_Head.Head.HeadWidth ) {
			n_Head.Head.HidWidth = n_Head.Head.HeadWidth;
			n_Head.Head.ShowX = -( n_Head.Head.HeadWidth - n_Head.Head.HidWidth );
			SystemData.LeftBarValue = n_Head.Head.HidWidth;
			SystemData.isChanged = true;
		}
		
		
		isMouseOn = isMouseInSide( mX, mY );
		
		//如果按下鼠标键, 移动窗体
		if( isMousePress ) {
			
			n_Head.Head.HidWidth = mX - SlideWidth/2;
			if( n_Head.Head.HidWidth < 2 ) {
				n_Head.Head.HidWidth = 2;
			}
			if( n_Head.Head.HidWidth > n_Head.Head.HeadWidth ) {
				n_Head.Head.HidWidth = n_Head.Head.HeadWidth;
			}
			n_Head.Head.ShowX = -( n_Head.Head.HeadWidth - n_Head.Head.HidWidth );
			
			SystemData.LeftBarValue = n_Head.Head.HidWidth;
			SystemData.isChanged = true;
		}
		return isMouseOn;
	}
	
	//判断指定点是否在内部
	public bool isMouseInSide( int mX, int mY )
	{
		if( mX > SX && mX < SX + Width && mY > SY && mY < SY + Height ) {
			return true;
		}
		return false;
	}
	
	//尺寸改变事件
	public void SizeChanged()
	{
		Height = G.CGPanel.Height - Head.HeadLabelHeight + 1;
	}
}
//右边栏调节器
public class RightSlider
{
	int SX;
	int SY;
	int Width;
	int Height;
	
	bool isMousePress;
	bool isMouseOn;
	
	const int SlideWidth = 8;
	const int MinWidth = 0; //正常等于2
	
	//构造函数
	public RightSlider()
	{
		SX = 0;
		SY = Head.HeadLabelHeight - 1;
		Width = SlideWidth;
		
		isMousePress = false;
		isMouseOn = false;
	}
	
	//绘制
	public void Draw( Graphics g )
	{
		return;
		
		/*
		SX = G.CGPanel.Width - ImagePanel.RShowX - SlideWidth - 1;
		
		if( isMousePress ) {
			g.FillRectangle( Brushes.Chocolate, SX, SY, Width, Height );
			g.DrawRectangle( Pens.Black, SX, SY, Width, Height );
		}
		else if( G.StartStatus || isMouseOn ) {
			g.FillRectangle( Brushes.SandyBrown, SX, SY, Width, Height );
			g.DrawRectangle( Pens.OrangeRed, SX, SY, Width, Height );
		}
		else {
			g.FillRectangle( GUIset.BackBrush, SX, SY, Width, Height );
			g.DrawRectangle(Pens.Silver, SX, SY, Width, Height );
		}
		*/
	}
	
	//鼠标按下事件
	public bool MouseDown( int mX, int mY )
	{
		if( !isMouseOn ) {
			return false;
		}
		//isMousePress = true;
		
		return true;
	}
	
	//鼠标松开事件
	public void MouseUp( int mX, int mY )
	{
		isMousePress = false;
	}
	
	//鼠标移动事件
	public bool MouseMove( int mX, int mY )
	{
		isMouseOn = isMouseInSide( mX, mY );
		
		//如果按下鼠标键
		if( isMousePress ) {
			
			ImagePanel.RHidWidth = G.CGPanel.Width - (mX + SlideWidth/2);
			if( ImagePanel.RHidWidth < MinWidth ) {
				ImagePanel.RHidWidth = MinWidth;
			}
			if( ImagePanel.RHidWidth > ImagePanel.MaxRShowX ) {
				ImagePanel.RHidWidth = ImagePanel.MaxRShowX;
			}
			ImagePanel.RShowX = ImagePanel.RHidWidth;
			
			G.CGPanel.GHead.RecycleChange();
			
			SystemData.RightBarValue = ImagePanel.RHidWidth;
			SystemData.isChanged = true;
		}
		return false; //isMouseOn;
	}
	
	//判断指定点是否在内部
	public bool isMouseInSide( int mX, int mY )
	{
		if( mX > SX && mX < SX + Width && mY > SY && mY < SY + Height ) {
			return false; //true;
		}
		return false;
	}
	
	//尺寸改变事件
	public void SizeChanged()
	{
		Height = G.CGPanel.Height - Head.HeadLabelHeight + 1;
	}
}
//垃圾桶类
public class Recycler
{
	public int SX;
	int SY;
	public int Width;
	int Height;
	
	public bool Visible;
	public bool isHighLight;
	
	Bitmap RImage;
	Bitmap HRImage;
	//Bitmap BigImage;
	
	public bool ShowMessage;
	public int Showed;
	
	//构造函数
	public Recycler()
	{
		RImage = new Bitmap( OS.SystemRoot + "Resource" + OS.PATH_S + "gui" + OS.PATH_S + "GFormControls" + OS.PATH_S + "Recycler" + OS.PATH_S + "Recycler.png" );
		HRImage = new Bitmap( OS.SystemRoot + "Resource" + OS.PATH_S + "gui" + OS.PATH_S + "GFormControls" + OS.PATH_S + "Recycler" + OS.PATH_S + "HRecycler.png" );
		//BigImage = new Bitmap( OS.SystemRoot + "Resource" + OS.PATH_S + "gui" + OS.PATH_S + "GFormControls" + OS.PATH_S + "Recycler" + OS.PATH_S + "BRecycler.png" );
		
		SX = 10;
		SY = 50;
		Width = 100;
		Height = 100;
		
		Visible = true;
		isHighLight = false;
		
		ShowMessage = false;
		Showed = 0;
		
		//指令拖动 true  ImagePanel Up 
	}
	
	//更新坐标
	public void ResetLocation()
	{
		//SX = G.CGPanel.Width - Width + 8;
		SY = G.CGPanel.Height - Height;
	}
	
	//鼠标移动
	public bool MouseMove( int mX, int mY )
	{
		if( isInside( mX, mY ) ) {
			isHighLight = true;
		}
		else {
			isHighLight = false;
		}
		return isHighLight;
	}
	
	//判断指定点是否在模块上
	public bool isInside( float x, float y )
	{
		if( x >= SX && x < SX + Width && y >= SY && y < SY + Height ) {
			return true;
		}
		return false;
	}
	
	//绘制
	public void Draw( Graphics g )
	{
		if( Visible ) {
			//g.FillEllipse( GUIset.BackBrush, SX - 20, SY - 20, Width + 40, Height + 40 );
			
			if( Showed < 4 && ShowMessage ) {
				n_SG.SG.MRectangle.FillRound( Color.SandyBrown, 10, 10, SX - 220, SY + 25, 200, 50 );
				n_SG.SG.MRectangle.DrawRound( Color.SaddleBrown, 1, 10, 10, SX - 220, SY + 25, 200, 50 );
				n_SG.SG.MString.DrawAtCenter( n_Language.Language.Head_DeleteIns, Color.WhiteSmoke, 12, SX - 120, SY + 25 + 25 );
			}
			
			if( !isHighLight ) {
				//g.DrawImage( BigImage, SX, SY, Width, Height );
				
				g.DrawImage( HRImage, SX, SY, Width, Height );
			}
			else {
				//if(n_ImagePanel.ImagePanel.Flash ) {
				//	//g.DrawImage( RImage, SX, SY, Width, Height );
				//	n_SG.SG.MBitmap.Draw( RImage, SX, SY, Width, Height, 50 );
				//}
				//else {
					//g.DrawImage( HRImage, SX, SY, Width, Height );
					
					g.DrawImage( RImage, SX, SY, Width, Height );
				//}
			}
		}
	}
}
//新版标题栏路径类
public class R_HeadLabel
{
	Head owner;
	
	bool isMouseOn;
	bool isMousePress;
	int Last_mX;
	int Last_mY;
	
	public static int Height;
	
	public int Width;
	
	public static int MaxHeight;
	
	public int TotalHeight;
	
	const int CD_MesPadding = 0; //140;
	const int CD_RightPadding = 0; //170;
	int DownHeight = 12;
	
	public string SoftText;
	
	//TextureBrush texture;
	
	static Brush UserMessageBackBrush;
	public static Brush BackBrush;
	
	//初始化
	public static void Init()
	{
		//Bitmap image1 = (Bitmap)Image.FromFile(@"D:/2.png", true);
		//texture = new TextureBrush(image1);
		//texture.WrapMode = System.Drawing.Drawing2D.WrapMode.TileFlipX;
		
		BackBrush = new SolidBrush( GUIset.HeadBackColor );
		
		if( G.LightUI ) {
			BackBrush = new SolidBrush( Color.FromArgb( 241, 241, 241 ) );
		}
		
		//BackBrush = new SolidBrush( Color.FromArgb( 87, 165, 254 ) );
		//BackBrush1 = new SolidBrush( Color.FromArgb( 87, 165, 254 ) ); //65, 81, 90
		
		
		//BackBrush1 = new SolidBrush( Color.FromArgb( 210, 217, 222 ) ); //65, 81, 90
		//BackBrush = new SolidBrush( Color.FromArgb( 210, 217, 222 ) ); //65, 81, 90
		
		
		UserMessageBackBrush = new SolidBrush( GUIset.HeadBackColor );
		
		//蓝色
		//BackBrush = new SolidBrush( Color.FromArgb( 90, 159, 255 ) );
		//BackBrush1 = new SolidBrush( Color.FromArgb( 76, 151, 255 ) );
		//浅色
		//BackBrush = Brushes.SlateGray;
		//BackBrush1 = Brushes.LightSlateGray;
	}
	
	//构造函数
	public R_HeadLabel( Head o )
	{
		owner = o;
		
		Width = 0;
		
		MaxHeight = Height + DownHeight + Height + Height;
		
		isMouseOn = false;
		
		isMousePress = false;
		Last_mX = 0;
		Last_mY = 0;
		
		SoftText = "";
		
		//BackLinePen = new Pen( Color.FromArgb( 150, 100, 130, 180 ), 6 );
		
		TotalHeight = Height + DownHeight + Height;
		
		AutoShowInit();
		
		//SizeChanged( null, null );
	}
	
	//尺寸改变事件
	public void SizeChanged()
	{
		Width = G.CGPanel.Width;
		
		G.CGPanel.myRecycler.ResetLocation();
		G.CGPanel.myLineColorBox.ResetLocation();
	}
	
	//绘制函数
	public void Draw( Graphics g )
	{
		Brush b = BackBrush;
		//Brush b = GUIset.BackBrush;
		//Brush b = GUIset.FormBrush;
		
		g.FillRectangle( b, 0, 0, Width, Height - 1 );
		
		int tempStart = 0;
		
		if( G.CGPanel.GHead.isDownLoad ) {
			//g.SmoothingMode = SmoothingMode.HighSpeed;
			//g.FillPath( GUIset.BackBrush, BackPath );
			//绘制编译的进度
			int w = Width - tempStart - CD_RightPadding;
			int StartY = Height / 2;
			int CurrentX = tempStart + w / 2 + w / 2 * CIndex / Number;
			DrawCompile( g, Compiler.STEP_Number, tempStart );
			//g.DrawLine( BackLinePen3, tempStart + Padding + w / 2, StartY, StartX + Padding + w, StartY );
			//g.DrawLine( BackLinePen2, tempStart + Padding + w / 2, StartY, StartX + Padding + w, StartY );
			//g.DrawLine( BackLinePen1, tempStart + Padding + w / 2, StartY, StartX + Padding + w, StartY );
			//g.DrawLine( ForeLinePenB3, tempStart + Padding + w / 2, StartY, CurrentX, StartY );
			g.DrawLine( ForeLinePen, tempStart + w / 2, StartY, CurrentX, StartY );
			
			//g.DrawLine( ForeLinePenB1, tempStart + Padding + w / 2, StartY, CurrentX, StartY );
			//g.DrawString( ISPMes,  GUIset.FileDirFont, Brushes.WhiteSmoke, tempStart + 10, 4 );
			//g.SmoothingMode = SmoothingMode.HighQuality;
		}
		else if( G.CGPanel.GHead.isCompile ) {
			//g.SmoothingMode = SmoothingMode.HighSpeed;
			//g.FillPath( GUIset.BackBrush, BackPath );
			DrawCompile( g, Step, tempStart );
			//g.SmoothingMode = SmoothingMode.HighQuality;
		}
		else {
			
			SoftText = GUIset.SoftLabel + "   ";
			if( G.isCruxEXE ) {
				SoftText = "crux-IDE v2.0 - 2022.9.21   ";
			}
			
			if( G.ccode.isStartFile ) {
				
				//SoftText += "您的设计图还没有存放到电脑硬盘上";
				
				if( !G.ccode.isChanged ) {
					//...
				}
				else {
					SoftText += n_Language.Language.NotSave;
				}
			}
			else {
				string FileName = G.ccode.FileName + (G.ccode.isChanged? "*" : " ");
				SoftText += FileName;
				SoftText += "(" + G.ccode.PathAndName + ")";
			}
		}
	}
	
	//鼠标按下事件
	public bool MouseDown( int mX, int mY )
	{
		if( !isMouseInSide( mX, mY ) ) {
			return false;
		}
		isMousePress = true;
		Last_mX = mX;
		Last_mY = mY;
		return true;
	}
	
	//鼠标松开事件
	public void MouseUp( int mX, int mY )
	{
		isMousePress = false;
	}
	
	//鼠标移动事件
	public bool MouseMove( int mX, int mY )
	{
		isMouseOn = isMouseInSide( mX, mY );
		
		//如果按下鼠标键, 移动窗体
		if( isMousePress ) {
			int X = owner.TargetForm.Location.X;
			int Y = owner.TargetForm.Location.Y;
			X += mX - Last_mX;
			Y += mY - Last_mY;
			owner.TargetForm.Location = new Point( X, Y );
			//Last_mX = mX;
			//Last_mY = mY;
		}
		return isMouseOn;
	}
	
	//判断指定点是否在内部
	public bool isMouseInSide( int mX, int mY )
	{
		if( mY < Height ) {
			return true;
		}
		return false;
	}
	
	//=======================================
	
	Pen ForeLinePen;
	
	Pen BackLinePen1;
	Pen BackLinePen2;
	Pen BackLinePen3;
	Pen ForeLinePenR1;
	Pen ForeLinePenR2;
	Pen ForeLinePenR3;
	Pen ForeLinePenB1;
	Pen ForeLinePenB2;
	Pen ForeLinePenB3;
	
	public int Step;
	int Number;
	int CIndex;
	string ISPMes;
	
	//自动显示进度条初始化
	void AutoShowInit()
	{
		ISP.ISPStep += ShowISPStep;
		ISP.ProgressStep += ShowProgressStep;
		AVRdude.ISPStep += ShowISPStep;
		AVRdude.ProgressStep += ShowProgressStep;
		n_ExportForm.ExportForm.ProgressStep += ShowProgressStep;
		
		Compiler.CompilingStep += ShowCompileStep;
		Compiler.CheckStep += ShowCheckStep;
		
//		BackLinePen1 = new Pen( Color.White, 1 );
//		//BackLinePen1.StartCap = LineCap.Round;
//		//BackLinePen1.EndCap = LineCap.Round;
//		BackLinePen2 = new Pen( Color.Black, 5 );
//		//BackLinePen2.StartCap = LineCap.Round;
//		//BackLinePen2.EndCap = LineCap.Round;
//		BackLinePen3 = new Pen( Color.White, 7 );
//		//BackLinePen3.StartCap = LineCap.Round;
//		//BackLinePen3.EndCap = LineCap.Round;
//		
//		ForeLinePenR1 = new Pen( Color.White, 1 );
//		//ForeLinePenR1.StartCap = LineCap.Round;
//		//ForeLinePenR1.EndCap = LineCap.Round;
//		ForeLinePenR2 = new Pen( Color.Blue, 5 );
//		//ForeLinePenR2.StartCap = LineCap.Round;
//		//ForeLinePenR2.EndCap = LineCap.Round;
//		ForeLinePenR3 = new Pen( Color.Red, 7 );
//		//ForeLinePenR3.StartCap = LineCap.Round;
//		//ForeLinePenR3.EndCap = LineCap.Round;
//		
//		ForeLinePenB1 = new Pen( Color.White, 1 );
//		//ForeLinePenB1.StartCap = LineCap.Round;
//		//ForeLinePenB1.EndCap = LineCap.Round;
//		ForeLinePenB2 = new Pen( Color.SlateBlue, 5 );
//		//ForeLinePenB2.StartCap = LineCap.Round;
//		//ForeLinePenB2.EndCap = LineCap.Round;
//		ForeLinePenB3 = new Pen( Color.Blue, 7 );
//		//ForeLinePenB3.StartCap = LineCap.Round;
//		//ForeLinePenB3.EndCap = LineCap.Round;
		
		
		ForeLinePen = new Pen( Color.FromArgb( 190, 220, 250 ), Height - 4 );
		ForeLinePen.EndCap = LineCap.Round;
		
		BackLinePen1 = new Pen( Color.WhiteSmoke, 1 );
		BackLinePen2 = new Pen( GUIset.FormBrush, 5 );
		BackLinePen3 = new Pen( Color.WhiteSmoke, 7 );
		ForeLinePenR1 = new Pen( GUIset.GetRedColor( 250 ), 1 );
		ForeLinePenR2 = new Pen( GUIset.GetRedColor( 250 ), 5 );
		ForeLinePenR3 = new Pen( GUIset.GetRedColor( 250 ), 7 );
		ForeLinePenB1 = new Pen( GUIset.GetBlueColor( 250 ), 1 );
		ForeLinePenB2 = new Pen( GUIset.GetBlueColor( 250 ), 5 );
		ForeLinePenB3 = new Pen( GUIset.GetBlueColor( 250 ), 7 );
		
		BackLinePen3 = new Pen( Color.WhiteSmoke, 7 );
		ForeLinePenR2 = new Pen( Color.Orange, 5 );
		ForeLinePenB2 = new Pen( Color.Orange, 5 );
	}
	
	//显示检查情况信息
	void ShowCheckStep( int Step )
	{
		//Graphics g = G.CGPanel.g;
		//g.FillPath( GUIset.BackBrush, BackPath );
		//绘制检查描述字符
		//string Des = Compiler.GetCheckDescribe( Step );
		//g.DrawString( Des, DescriptFont, Brushes.White, StartX + 10, 4 );
	}
	
	//显示编译进度条
	void ShowCompileStep( int vStep )
	{
		Step = vStep;
		G.CGPanel.GHead.isCompile = true;
		
		System.Windows.Forms.Application.DoEvents();
	}
	
	//显示下载进度条
	void ShowProgressStep( int vNumber, int vIndex )
	{
		Number = vNumber;
		CIndex = vIndex;
		G.CGPanel.GHead.isDownLoad = true;
		G.CGPanel.MyRefresh();
	}
	
	//显示下载信息
	void ShowISPStep( string Mes )
	{
		Number = 10;
		CIndex = 0;
		ISPMes = Mes;
		G.CGPanel.GHead.isDownLoad = true;
		G.CGPanel.MyRefresh();
	}
	
	//绘制编译进度条
	void DrawCompile( Graphics g, int Step, int tempStart )
	{
		//绘制编译的进度
		int w = Width - tempStart - CD_RightPadding;
		
		int StartY = Height / 2;
		int CurrentX = tempStart + w / 2 * Step / Compiler.STEP_Number;
		
		//g.DrawLine( BackLinePen3, tempStart + CD_MesPadding, StartY, tempStart + CD_MesPadding + w, StartY );
		//g.DrawLine( BackLinePen2, tempStart + Padding, StartY, tempStart + Padding + w, StartY );
		//g.DrawLine( BackLinePen1, tempStart + Padding, StartY, StartX + Padding + w, StartY );
		//g.DrawLine( ForeLinePenR3, tempStart + Padding, StartY, CurrentX, StartY );
		g.DrawLine( ForeLinePen, tempStart + 1, StartY, CurrentX, StartY );
		//g.DrawLine( ForeLinePenR1, tempStart + Padding, StartY, CurrentX, StartY );
		
		//string Des = Compiler.GetDescribe( Step );
		//g.DrawString( Des,  GUIset.FileDirFont, Brushes.WhiteSmoke, tempStart + 10, 4 );
	}
}

}

