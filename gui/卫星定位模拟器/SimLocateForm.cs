﻿
using System;
using System.Drawing;
using System.Windows.Forms;
using n_LocatePanel;

namespace n_SimLocateForm
{
/// <summary>
/// Description of LocateForm.
/// </summary>
public partial class SimLocateForm : Form
{
	public int v_e;
	public int E {
		set {
			GPanel.AX = (value - (116321060 + int.Parse( textBoxOE.Text ))) * 10000 / 88892 + BIWidth/2;
		}
		get {
			return (116321060 + int.Parse( textBoxOE.Text )) + (GPanel.AX - BIWidth/2) * 88892 / 10000;
		}
	}
	
	int BIWidth;
	int BIHeight;
	
	public int v_w;
	public int W {
		set {
			GPanel.AY = (39980291 + int.Parse( textBoxOW.Text ) - value) * 10000 / 88892 + BIHeight/2;
		}
		get {
			return 39980291 + int.Parse( textBoxOW.Text ) - (GPanel.AY - BIHeight/2) * 88892 / 10000;
		}
	}
	
	LocatePanel GPanel;
	
	public SimLocateForm()
	{
		//
		// The InitializeComponent() call is required for Windows Forms designer support.
		//
		InitializeComponent();
		
		GPanel = new LocatePanel();
		this.panel2.Controls.Add( GPanel );
		
		GPanel.StartX = (GPanel.Width - GPanel.BackImage.Width) / 2;
		GPanel.StartY = (GPanel.Height - GPanel.BackImage.Height) / 2;
		
		GPanel.d_DataChanged = DataChanged;
		BIWidth = GPanel.BackImage.Width;
		BIHeight = GPanel.BackImage.Height;
	}
	
	//运行
	public void Run()
	{
		this.Visible = true;
	}
	
	void DataChanged()
	{
		textBoxAE.Text = E.ToString();
		textBoxAW.Text = W.ToString();
	}
	
	void LocateFormFormClosing(object sender, FormClosingEventArgs e)
	{
		this.Visible = false;
		e.Cancel = true;
	}
	
	void Button1Click(object sender, EventArgs e)
	{
		E = int.Parse( textBoxAE.Text );
		W = int.Parse( textBoxAW.Text );
		GPanel.Invalidate();
	}
}
}

