﻿
using System;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Windows.Forms;

using n_GUIcoder;
using n_GUIset;

namespace n_LocatePanel
{
public class LocatePanel : Panel
{
	public delegate void D_DataChanged();
	public D_DataChanged d_DataChanged;
	
	public int StartX;
	public int StartY;
	
	public static Font extendFont;
	public static Font extendHeadFont;
	
	bool LeftPress;
	bool RightPress;
	
	int LastX;
	int LastY;
	
	public int GWidth;
	
	public Image BackImage;
	
	public int AX;
	public int AY;
	bool APress;
	
	Brush ABrush;
	
	//构造函数
	public LocatePanel() : base()
	{
		this.BorderStyle = BorderStyle.None;
		this.BackColor = Color.Black;
		
		SetStyle(ControlStyles.UserPaint |
		         ControlStyles.AllPaintingInWmPaint |
		         ControlStyles.OptimizedDoubleBuffer |
		         ControlStyles.ResizeRedraw |
		         ControlStyles.SupportsTransparentBackColor,true);
		
		this.Dock = DockStyle.Fill;
		this.ImeMode = ImeMode.NoControl;
		
		this.MouseMove += new MouseEventHandler( UserMouseMove );
		this.MouseUp += new MouseEventHandler( UserMouseUp );
		this.MouseDown += new MouseEventHandler( UserMouseDown );
		this.MouseWheel += new MouseEventHandler( MyMouseWheel );
		
		extendFont = new Font( "宋体", 11 );
		extendHeadFont = new Font( "宋体", 12, FontStyle.Bold );
		
		BackImage = new Bitmap( n_OS.OS.SystemRoot + @"Resource\gui\tools\locate\dt.png" );
		
		AX = BackImage.Width/2;
		AY = BackImage.Height/2;
		APress = false;
		ABrush = new SolidBrush( Color.FromArgb( 100, 100, 100, 200 ) );
	}
	
	//重绘事件
	protected override void OnPaint(PaintEventArgs e)
	{
		//文字起点为下方 20, 宽度间距为 20
		Graphics g = e.Graphics;
		
		//绘制组件信息
		//g.DrawString( "12345", extendHeadFont, Brushes.DarkRed, 20, 10 );
		
		g.SmoothingMode = SmoothingMode.HighSpeed;
		
		g.TranslateTransform( StartX, StartY );
		
		//填充背景
		g.Clear( Color.WhiteSmoke );
		
		g.DrawImage( BackImage, 0, 0 );
		
		g.SmoothingMode = SmoothingMode.HighQuality;
		
		g.FillEllipse( ABrush, AX - 30, AY - 30, 60, 60 );
		g.FillEllipse( Brushes.Blue, AX - 5, AY - 5, 10, 10 );
	}
	
	void MyMouseWheel( object sender, MouseEventArgs e)
	{
		if( e.Delta > 0 ) {
			//StartY += 50;
		}
		else {
			//StartY -= 50;
		}
		this.Invalidate();
	}
	
	//鼠标按下事件
	void UserMouseDown( object sender, MouseEventArgs e )
	{
		if( e.Button == MouseButtons.Left ) {
			LeftPress = true;
			APress = true;
			AX = e.X - StartX;
			AY = e.Y - StartY;
			if( d_DataChanged != null ) {
				d_DataChanged();
			}
		}
		if( e.Button == MouseButtons.Right ) {
			LastX = e.X;
			LastY = e.Y;
			RightPress = true;
		}
		this.Invalidate();
		
		this.Focus();
	}
	
	//鼠标松开事件
	void UserMouseUp( object sender, MouseEventArgs e )
	{
		if( e.Button == MouseButtons.Left ) {
			LeftPress = false;
			APress = false;
		}
		if( e.Button == MouseButtons.Right ) {
			RightPress = false;
		}
		this.Invalidate();
	}
	
	//鼠标移动事件
	void UserMouseMove( object sender, MouseEventArgs e )
	{
		if( LeftPress ) {
			if( APress ) {
				AX = e.X - StartX;
				AY = e.Y - StartY;
				if( d_DataChanged != null ) {
					d_DataChanged();
				}
			}
		}
		if( RightPress ) {
			StartX += e.X - LastX;
			StartY += e.Y - LastY;
			
			if( StartX > 0 ) {
				StartX = 0;
			}
			if( StartX < this.Width - BackImage.Width ) {
				StartX = this.Width - BackImage.Width;
			}
			if( StartY > 0 ) {
				StartY = 0;
			}
			if( StartY < this.Height - BackImage.Height ) {
				StartY = this.Height - BackImage.Height;
			}
			LastX = e.X;
			LastY = e.Y;
		}
		this.Invalidate();
	}
}
}


