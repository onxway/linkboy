﻿/*
 * 由SharpDevelop创建。
 * 用户： cap_gpu
 * 日期: 2017/4/9
 * 时间: 8:03
 * 
 * 要改变这种模板请点击 工具|选项|代码编写|编辑标准头文件
 */
namespace n_SimLocateForm
{
	partial class SimLocateForm
	{
		/// <summary>
		/// Designer variable used to keep track of non-visual components.
		/// </summary>
		private System.ComponentModel.IContainer components = null;
		
		/// <summary>
		/// Disposes resources used by the form.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing) {
				if (components != null) {
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}
		
		/// <summary>
		/// This method is required for Windows Forms designer support.
		/// Do not change the method contents inside the source code editor. The Forms designer might
		/// not be able to load this method if it was changed manually.
		/// </summary>
		private void InitializeComponent()
		{
			this.label1 = new System.Windows.Forms.Label();
			this.label2 = new System.Windows.Forms.Label();
			this.textBoxAE = new System.Windows.Forms.TextBox();
			this.textBoxAW = new System.Windows.Forms.TextBox();
			this.button1 = new System.Windows.Forms.Button();
			this.panel1 = new System.Windows.Forms.Panel();
			this.label3 = new System.Windows.Forms.Label();
			this.label4 = new System.Windows.Forms.Label();
			this.textBoxOW = new System.Windows.Forms.TextBox();
			this.textBoxOE = new System.Windows.Forms.TextBox();
			this.panel2 = new System.Windows.Forms.Panel();
			this.panel1.SuspendLayout();
			this.SuspendLayout();
			// 
			// label1
			// 
			this.label1.Font = new System.Drawing.Font("微软雅黑", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
			this.label1.Location = new System.Drawing.Point(275, 9);
			this.label1.Name = "label1";
			this.label1.Size = new System.Drawing.Size(94, 26);
			this.label1.TabIndex = 0;
			this.label1.Text = "经度：";
			// 
			// label2
			// 
			this.label2.Font = new System.Drawing.Font("微软雅黑", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
			this.label2.Location = new System.Drawing.Point(275, 38);
			this.label2.Name = "label2";
			this.label2.Size = new System.Drawing.Size(94, 26);
			this.label2.TabIndex = 1;
			this.label2.Text = "纬度：";
			// 
			// textBoxAE
			// 
			this.textBoxAE.Font = new System.Drawing.Font("微软雅黑", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
			this.textBoxAE.Location = new System.Drawing.Point(330, 6);
			this.textBoxAE.Name = "textBoxAE";
			this.textBoxAE.Size = new System.Drawing.Size(120, 29);
			this.textBoxAE.TabIndex = 2;
			this.textBoxAE.Text = "116183412";
			this.textBoxAE.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
			// 
			// textBoxAW
			// 
			this.textBoxAW.Font = new System.Drawing.Font("微软雅黑", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
			this.textBoxAW.Location = new System.Drawing.Point(330, 35);
			this.textBoxAW.Name = "textBoxAW";
			this.textBoxAW.Size = new System.Drawing.Size(120, 29);
			this.textBoxAW.TabIndex = 3;
			this.textBoxAW.Text = "39505136";
			this.textBoxAW.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
			// 
			// button1
			// 
			this.button1.BackColor = System.Drawing.Color.DarkKhaki;
			this.button1.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
			this.button1.Font = new System.Drawing.Font("微软雅黑", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
			this.button1.Location = new System.Drawing.Point(456, 6);
			this.button1.Name = "button1";
			this.button1.Size = new System.Drawing.Size(65, 58);
			this.button1.TabIndex = 4;
			this.button1.Text = "设置";
			this.button1.UseVisualStyleBackColor = false;
			this.button1.Click += new System.EventHandler(this.Button1Click);
			// 
			// panel1
			// 
			this.panel1.Controls.Add(this.textBoxOE);
			this.panel1.Controls.Add(this.textBoxOW);
			this.panel1.Controls.Add(this.label3);
			this.panel1.Controls.Add(this.label4);
			this.panel1.Controls.Add(this.textBoxAW);
			this.panel1.Controls.Add(this.textBoxAE);
			this.panel1.Controls.Add(this.label1);
			this.panel1.Controls.Add(this.button1);
			this.panel1.Controls.Add(this.label2);
			this.panel1.Dock = System.Windows.Forms.DockStyle.Top;
			this.panel1.Location = new System.Drawing.Point(0, 0);
			this.panel1.Name = "panel1";
			this.panel1.Size = new System.Drawing.Size(656, 74);
			this.panel1.TabIndex = 5;
			// 
			// label3
			// 
			this.label3.Font = new System.Drawing.Font("微软雅黑", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
			this.label3.Location = new System.Drawing.Point(3, 9);
			this.label3.Name = "label3";
			this.label3.Size = new System.Drawing.Size(122, 26);
			this.label3.TabIndex = 7;
			this.label3.Text = "调试偏移经度";
			// 
			// label4
			// 
			this.label4.Font = new System.Drawing.Font("微软雅黑", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
			this.label4.Location = new System.Drawing.Point(3, 38);
			this.label4.Name = "label4";
			this.label4.Size = new System.Drawing.Size(122, 26);
			this.label4.TabIndex = 8;
			this.label4.Text = "调试偏移纬度";
			// 
			// textBoxOW
			// 
			this.textBoxOW.Font = new System.Drawing.Font("微软雅黑", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
			this.textBoxOW.Location = new System.Drawing.Point(113, 35);
			this.textBoxOW.Name = "textBoxOW";
			this.textBoxOW.Size = new System.Drawing.Size(116, 29);
			this.textBoxOW.TabIndex = 6;
			this.textBoxOW.Text = "0";
			this.textBoxOW.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
			// 
			// textBoxOE
			// 
			this.textBoxOE.Font = new System.Drawing.Font("微软雅黑", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
			this.textBoxOE.Location = new System.Drawing.Point(113, 6);
			this.textBoxOE.Name = "textBoxOE";
			this.textBoxOE.Size = new System.Drawing.Size(116, 29);
			this.textBoxOE.TabIndex = 5;
			this.textBoxOE.Text = "0";
			this.textBoxOE.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
			// 
			// panel2
			// 
			this.panel2.Dock = System.Windows.Forms.DockStyle.Fill;
			this.panel2.Location = new System.Drawing.Point(0, 74);
			this.panel2.Name = "panel2";
			this.panel2.Size = new System.Drawing.Size(656, 461);
			this.panel2.TabIndex = 6;
			// 
			// SimLocateForm
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.BackColor = System.Drawing.Color.Silver;
			this.ClientSize = new System.Drawing.Size(656, 535);
			this.Controls.Add(this.panel2);
			this.Controls.Add(this.panel1);
			this.Name = "SimLocateForm";
			this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
			this.Text = "卫星定位模拟器";
			this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.LocateFormFormClosing);
			this.panel1.ResumeLayout(false);
			this.panel1.PerformLayout();
			this.ResumeLayout(false);
		}
		private System.Windows.Forms.TextBox textBoxOE;
		private System.Windows.Forms.TextBox textBoxOW;
		private System.Windows.Forms.Label label4;
		private System.Windows.Forms.Label label3;
		private System.Windows.Forms.Panel panel2;
		private System.Windows.Forms.Panel panel1;
		private System.Windows.Forms.Button button1;
		private System.Windows.Forms.TextBox textBoxAW;
		private System.Windows.Forms.TextBox textBoxAE;
		private System.Windows.Forms.Label label2;
		private System.Windows.Forms.Label label1;
	}
}
