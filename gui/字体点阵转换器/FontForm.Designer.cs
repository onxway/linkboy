﻿/*
 * 由 SharpDevelop 创建。
 * 用户: bbb_2
 * 日期: 2009-11-17
 * 时间: 11:15
 * 
 * 如果你想要更改该模板，那么请使用“工具 | 选项 | 编辑 | 编辑标准标题”。
 */
namespace n_FontForm
{
	partial class FontForm
	{
		/// <summary>
		/// Designer variable used to keep track of non-visual components.
		/// </summary>
		private System.ComponentModel.IContainer components = null;
		
		/// <summary>
		/// Disposes resources used by the form.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing) {
				if (components != null) {
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}
		
		/// <summary>
		/// This method is required for Windows Forms designer support.
		/// Do not change the method contents inside the source code editor. The Forms designer might
		/// not be able to load this method if it was changed manually.
		/// </summary>
		private void InitializeComponent()
		{
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FontForm));
			this.labelPixelW = new System.Windows.Forms.Label();
			this.labelPixelH = new System.Windows.Forms.Label();
			this.textBox宽度 = new System.Windows.Forms.TextBox();
			this.textBox高度 = new System.Windows.Forms.TextBox();
			this.panel1 = new System.Windows.Forms.Panel();
			this.checkBoxUseOld = new System.Windows.Forms.CheckBox();
			this.buttonSaveAs = new System.Windows.Forms.Button();
			this.buttonOpenFont = new System.Windows.Forms.Button();
			this.panel3 = new System.Windows.Forms.Panel();
			this.button字体 = new System.Windows.Forms.Button();
			this.richTextBox文字 = new System.Windows.Forms.RichTextBox();
			this.groupBox1 = new System.Windows.Forms.GroupBox();
			this.radioButtonChinese = new System.Windows.Forms.RadioButton();
			this.radioButtonNumber = new System.Windows.Forms.RadioButton();
			this.radioButtonAscii = new System.Windows.Forms.RadioButton();
			this.buttonMoveLeft = new System.Windows.Forms.Button();
			this.buttonMoveRight = new System.Windows.Forms.Button();
			this.buttonMoveDown = new System.Windows.Forms.Button();
			this.buttonMoveUp = new System.Windows.Forms.Button();
			this.button确定 = new System.Windows.Forms.Button();
			this.button反色点阵 = new System.Windows.Forms.Button();
			this.button清空点阵 = new System.Windows.Forms.Button();
			this.panel2 = new System.Windows.Forms.Panel();
			this.label1 = new System.Windows.Forms.Label();
			this.panel1.SuspendLayout();
			this.panel3.SuspendLayout();
			this.groupBox1.SuspendLayout();
			this.SuspendLayout();
			// 
			// labelPixelW
			// 
			resources.ApplyResources(this.labelPixelW, "labelPixelW");
			this.labelPixelW.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
			this.labelPixelW.Name = "labelPixelW";
			// 
			// labelPixelH
			// 
			resources.ApplyResources(this.labelPixelH, "labelPixelH");
			this.labelPixelH.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
			this.labelPixelH.Name = "labelPixelH";
			// 
			// textBox宽度
			// 
			this.textBox宽度.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
			resources.ApplyResources(this.textBox宽度, "textBox宽度");
			this.textBox宽度.Name = "textBox宽度";
			this.textBox宽度.TextChanged += new System.EventHandler(this.TextBox宽度TextChanged);
			// 
			// textBox高度
			// 
			this.textBox高度.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
			resources.ApplyResources(this.textBox高度, "textBox高度");
			this.textBox高度.Name = "textBox高度";
			this.textBox高度.TextChanged += new System.EventHandler(this.TextBox高度TextChanged);
			// 
			// panel1
			// 
			this.panel1.BackColor = System.Drawing.Color.White;
			this.panel1.Controls.Add(this.label1);
			this.panel1.Controls.Add(this.checkBoxUseOld);
			this.panel1.Controls.Add(this.buttonSaveAs);
			this.panel1.Controls.Add(this.buttonOpenFont);
			this.panel1.Controls.Add(this.panel3);
			this.panel1.Controls.Add(this.groupBox1);
			this.panel1.Controls.Add(this.buttonMoveLeft);
			this.panel1.Controls.Add(this.buttonMoveRight);
			this.panel1.Controls.Add(this.buttonMoveDown);
			this.panel1.Controls.Add(this.buttonMoveUp);
			this.panel1.Controls.Add(this.button确定);
			this.panel1.Controls.Add(this.button反色点阵);
			this.panel1.Controls.Add(this.button清空点阵);
			this.panel1.Controls.Add(this.labelPixelW);
			this.panel1.Controls.Add(this.textBox高度);
			this.panel1.Controls.Add(this.labelPixelH);
			this.panel1.Controls.Add(this.textBox宽度);
			resources.ApplyResources(this.panel1, "panel1");
			this.panel1.Name = "panel1";
			// 
			// checkBoxUseOld
			// 
			resources.ApplyResources(this.checkBoxUseOld, "checkBoxUseOld");
			this.checkBoxUseOld.ForeColor = System.Drawing.SystemColors.ButtonShadow;
			this.checkBoxUseOld.Name = "checkBoxUseOld";
			this.checkBoxUseOld.UseVisualStyleBackColor = true;
			// 
			// buttonSaveAs
			// 
			this.buttonSaveAs.BackColor = System.Drawing.Color.CornflowerBlue;
			resources.ApplyResources(this.buttonSaveAs, "buttonSaveAs");
			this.buttonSaveAs.ForeColor = System.Drawing.Color.White;
			this.buttonSaveAs.Name = "buttonSaveAs";
			this.buttonSaveAs.UseVisualStyleBackColor = false;
			this.buttonSaveAs.Click += new System.EventHandler(this.ButtonSaveAsClick);
			// 
			// buttonOpenFont
			// 
			this.buttonOpenFont.BackColor = System.Drawing.Color.CornflowerBlue;
			resources.ApplyResources(this.buttonOpenFont, "buttonOpenFont");
			this.buttonOpenFont.ForeColor = System.Drawing.Color.White;
			this.buttonOpenFont.Name = "buttonOpenFont";
			this.buttonOpenFont.UseVisualStyleBackColor = false;
			this.buttonOpenFont.Click += new System.EventHandler(this.ButtonOpenFontClick);
			// 
			// panel3
			// 
			resources.ApplyResources(this.panel3, "panel3");
			this.panel3.Controls.Add(this.button字体);
			this.panel3.Controls.Add(this.richTextBox文字);
			this.panel3.Name = "panel3";
			// 
			// button字体
			// 
			this.button字体.BackColor = System.Drawing.Color.CornflowerBlue;
			resources.ApplyResources(this.button字体, "button字体");
			this.button字体.ForeColor = System.Drawing.Color.White;
			this.button字体.Name = "button字体";
			this.button字体.UseVisualStyleBackColor = false;
			this.button字体.Click += new System.EventHandler(this.Button字体Click);
			// 
			// richTextBox文字
			// 
			resources.ApplyResources(this.richTextBox文字, "richTextBox文字");
			this.richTextBox文字.BackColor = System.Drawing.Color.White;
			this.richTextBox文字.BorderStyle = System.Windows.Forms.BorderStyle.None;
			this.richTextBox文字.Name = "richTextBox文字";
			this.richTextBox文字.TextChanged += new System.EventHandler(this.RichTextBox文字TextChanged);
			// 
			// groupBox1
			// 
			this.groupBox1.Controls.Add(this.radioButtonChinese);
			this.groupBox1.Controls.Add(this.radioButtonNumber);
			this.groupBox1.Controls.Add(this.radioButtonAscii);
			resources.ApplyResources(this.groupBox1, "groupBox1");
			this.groupBox1.Name = "groupBox1";
			this.groupBox1.TabStop = false;
			// 
			// radioButtonChinese
			// 
			this.radioButtonChinese.ForeColor = System.Drawing.Color.Black;
			resources.ApplyResources(this.radioButtonChinese, "radioButtonChinese");
			this.radioButtonChinese.Name = "radioButtonChinese";
			this.radioButtonChinese.UseVisualStyleBackColor = true;
			this.radioButtonChinese.CheckedChanged += new System.EventHandler(this.RadioButtonChineseCheckedChanged);
			// 
			// radioButtonNumber
			// 
			this.radioButtonNumber.ForeColor = System.Drawing.Color.Black;
			resources.ApplyResources(this.radioButtonNumber, "radioButtonNumber");
			this.radioButtonNumber.Name = "radioButtonNumber";
			this.radioButtonNumber.UseVisualStyleBackColor = true;
			this.radioButtonNumber.CheckedChanged += new System.EventHandler(this.RadioButtonNumberCheckedChanged);
			// 
			// radioButtonAscii
			// 
			this.radioButtonAscii.ForeColor = System.Drawing.Color.Sienna;
			resources.ApplyResources(this.radioButtonAscii, "radioButtonAscii");
			this.radioButtonAscii.Name = "radioButtonAscii";
			this.radioButtonAscii.UseVisualStyleBackColor = true;
			this.radioButtonAscii.CheckedChanged += new System.EventHandler(this.RadioButtonAsciiCheckedChanged);
			// 
			// buttonMoveLeft
			// 
			this.buttonMoveLeft.BackColor = System.Drawing.Color.CornflowerBlue;
			resources.ApplyResources(this.buttonMoveLeft, "buttonMoveLeft");
			this.buttonMoveLeft.ForeColor = System.Drawing.Color.White;
			this.buttonMoveLeft.Name = "buttonMoveLeft";
			this.buttonMoveLeft.UseVisualStyleBackColor = false;
			this.buttonMoveLeft.Click += new System.EventHandler(this.ButtonMoveLeftClick);
			// 
			// buttonMoveRight
			// 
			this.buttonMoveRight.BackColor = System.Drawing.Color.CornflowerBlue;
			resources.ApplyResources(this.buttonMoveRight, "buttonMoveRight");
			this.buttonMoveRight.ForeColor = System.Drawing.Color.White;
			this.buttonMoveRight.Name = "buttonMoveRight";
			this.buttonMoveRight.UseVisualStyleBackColor = false;
			this.buttonMoveRight.Click += new System.EventHandler(this.ButtonMoveRightClick);
			// 
			// buttonMoveDown
			// 
			this.buttonMoveDown.BackColor = System.Drawing.Color.CornflowerBlue;
			resources.ApplyResources(this.buttonMoveDown, "buttonMoveDown");
			this.buttonMoveDown.ForeColor = System.Drawing.Color.White;
			this.buttonMoveDown.Name = "buttonMoveDown";
			this.buttonMoveDown.UseVisualStyleBackColor = false;
			this.buttonMoveDown.Click += new System.EventHandler(this.ButtonMoveDownClick);
			// 
			// buttonMoveUp
			// 
			this.buttonMoveUp.BackColor = System.Drawing.Color.CornflowerBlue;
			resources.ApplyResources(this.buttonMoveUp, "buttonMoveUp");
			this.buttonMoveUp.ForeColor = System.Drawing.Color.White;
			this.buttonMoveUp.Name = "buttonMoveUp";
			this.buttonMoveUp.UseVisualStyleBackColor = false;
			this.buttonMoveUp.Click += new System.EventHandler(this.ButtonMoveUpClick);
			// 
			// button确定
			// 
			resources.ApplyResources(this.button确定, "button确定");
			this.button确定.BackColor = System.Drawing.Color.CornflowerBlue;
			this.button确定.ForeColor = System.Drawing.Color.White;
			this.button确定.Name = "button确定";
			this.button确定.UseVisualStyleBackColor = false;
			this.button确定.Click += new System.EventHandler(this.Button确定Click);
			// 
			// button反色点阵
			// 
			this.button反色点阵.BackColor = System.Drawing.Color.CornflowerBlue;
			resources.ApplyResources(this.button反色点阵, "button反色点阵");
			this.button反色点阵.ForeColor = System.Drawing.Color.White;
			this.button反色点阵.Name = "button反色点阵";
			this.button反色点阵.UseVisualStyleBackColor = false;
			this.button反色点阵.Click += new System.EventHandler(this.Button反色点阵Click);
			// 
			// button清空点阵
			// 
			this.button清空点阵.BackColor = System.Drawing.Color.CornflowerBlue;
			resources.ApplyResources(this.button清空点阵, "button清空点阵");
			this.button清空点阵.ForeColor = System.Drawing.Color.White;
			this.button清空点阵.Name = "button清空点阵";
			this.button清空点阵.UseVisualStyleBackColor = false;
			this.button清空点阵.Click += new System.EventHandler(this.Button清空点阵Click);
			// 
			// panel2
			// 
			resources.ApplyResources(this.panel2, "panel2");
			this.panel2.BackColor = System.Drawing.Color.White;
			this.panel2.Name = "panel2";
			// 
			// label1
			// 
			this.label1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(241)))), ((int)(((byte)(241)))), ((int)(((byte)(241)))));
			resources.ApplyResources(this.label1, "label1");
			this.label1.Name = "label1";
			// 
			// FontForm
			// 
			resources.ApplyResources(this, "$this");
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.BackColor = System.Drawing.Color.White;
			this.Controls.Add(this.panel2);
			this.Controls.Add(this.panel1);
			this.Name = "FontForm";
			this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.ModuleFormClosing);
			this.panel1.ResumeLayout(false);
			this.panel1.PerformLayout();
			this.panel3.ResumeLayout(false);
			this.groupBox1.ResumeLayout(false);
			this.ResumeLayout(false);
		}
		private System.Windows.Forms.Label label1;
		private System.Windows.Forms.CheckBox checkBoxUseOld;
		private System.Windows.Forms.Button buttonOpenFont;
		private System.Windows.Forms.Button buttonSaveAs;
		private System.Windows.Forms.Panel panel3;
		private System.Windows.Forms.RadioButton radioButtonAscii;
		private System.Windows.Forms.RadioButton radioButtonNumber;
		private System.Windows.Forms.RadioButton radioButtonChinese;
		private System.Windows.Forms.GroupBox groupBox1;
		private System.Windows.Forms.TextBox textBox宽度;
		private System.Windows.Forms.Label labelPixelW;
		private System.Windows.Forms.Label labelPixelH;
		private System.Windows.Forms.Button button确定;
		private System.Windows.Forms.Button button反色点阵;
		private System.Windows.Forms.Button button清空点阵;
		private System.Windows.Forms.RichTextBox richTextBox文字;
		private System.Windows.Forms.Button button字体;
		private System.Windows.Forms.TextBox textBox高度;
		private System.Windows.Forms.Panel panel2;
		private System.Windows.Forms.Panel panel1;
		private System.Windows.Forms.Button buttonMoveLeft;
		private System.Windows.Forms.Button buttonMoveRight;
		private System.Windows.Forms.Button buttonMoveDown;
		private System.Windows.Forms.Button buttonMoveUp;
	}
}
