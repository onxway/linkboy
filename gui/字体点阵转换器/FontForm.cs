﻿
namespace n_FontForm
{
using System;
using System.IO;
using System.Net;
using System.Text;
using System.Windows.Forms;
using System.Drawing;
using System.Drawing.Drawing2D;
using n_PointPanel;
using n_LatticeToCode;
using n_GUIcoder;
using c_MyObjectSet;

/// <summary>
/// Description of WorkForm.
/// </summary>
public partial class FontForm : Form
{
	public PointPanel MPanel;
	
	Font TextFont;
	
	int WidthN;
	int HeightN;
	
	bool ignoreRefresh;
	bool isOK;
	
	bool ignoreTextChange;
	
	string BitType;
	
	int FontType;
	
	int Xoffset;
	int Yoffset;
	
	string Filter = "linkboy字体文件(*.font)|*.font";
	
	//主窗口
	public FontForm()
	{
		InitializeComponent();
		
		isOK = false;
		ignoreRefresh = false;
		WidthN = 16;
		HeightN = 16;
		
		ignoreTextChange = false;
		
		BitType = LatticeToCode.L_V1_RightDown;
		
		Xoffset = 0;
		Yoffset = 0;
		
		TextFont = this.richTextBox文字.Font;
		this.richTextBox文字.Font = TextFont;
		this.richTextBox文字.Select();
		this.richTextBox文字.SelectionFont = TextFont;
		
		MPanel = new PointPanel( 1, 1, null );
		MPanel.DotBrush = Brushes.SandyBrown;
		MPanel.isFont = true;
		this.panel2.Controls.Add( MPanel );
	}
	
	//获取图片
	public Bitmap GetBitmap( int W, int H, string text )
	{
		if( text == null || text == "" ) {
			
			MPanel.Per = 1;
			MPanel.SetSize( W, H, null );
			MPanel.ShowImage();
			MPanel.Per = PointPanel.ConstPer;
		}
		else {
			FontType = 0;
			int CW = 0;
			int h = 0;
			string Mes = "";
			bool[][] PointSet = LatticeToCode.GetLatticeFromArray( text, ref FontType, ref CW, ref h, ref Mes );
			
			MPanel.Per = 1;
			MPanel.SetSize( PointSet.Length, PointSet[ 0 ].Length, PointSet );
			MPanel.SetTip( CW, PointSet.Length );
			MPanel.ShowImage();
			MPanel.Per = PointPanel.ConstPer;
		}
		return MPanel.back;
	}
	
	//运行
	public bool RunBitmap( string text, ref int W, ref int H, ref string Value )
	{
		isOK = false;
		SetPoint( text );
		
		ShowDialog();
		/*
		this.Visible = true;
		this.Activate();
		while( this.Visible ) {
			System.Windows.Forms.Application.DoEvents();
		}
		*/
		if( isOK ) {
			W = WidthN;
			H = HeightN;
			
			bool[][] LatticeList = MPanel.PointSet;
			Value = LatticeToCode.GetCode( LatticeList, BitType, FontType, int.Parse( this.textBox宽度.Text ), this.richTextBox文字.Text );
			return true;
		}
		return false;
	}
	
	void SetPoint( string text )
	{
		if( text == null || text == "" ) {
			
			this.radioButtonAscii.Checked = true;
			
			WidthN = int.Parse( this.textBox宽度.Text ) * LatticeToCode.CharNumber;
			HeightN = int.Parse( this.textBox高度.Text );
			
			RefreshLattice( null );
		}
		else {
			if( text.StartsWith( LatticeToCode.L_DownRight ) ) {
				BitType = LatticeToCode.L_DownRight;
			}
			else if( text.StartsWith( LatticeToCode.L_RightDown ) ) {
				BitType = LatticeToCode.L_RightDown;
			}
			else if( text.StartsWith( LatticeToCode.L_V1_RightDown ) ) {
				BitType = LatticeToCode.L_V1_RightDown;
			}
			else {
				BitType = null;
			}
			FontType = 0;
			int CW = 0;
			string Mes = "";
			bool[][] PointSet = LatticeToCode.GetLatticeFromArray( text, ref FontType, ref CW, ref HeightN, ref Mes );
			
			ignoreTextChange = true;
			if( FontType == LatticeToCode.T_ascii ) {
				radioButtonAscii.Checked = true;
			}
			else if( FontType == LatticeToCode.T_part_ascii ) {
				radioButtonNumber.Checked = true;
			}
			else if( FontType == LatticeToCode.T_part_chinese ) {
				radioButtonChinese.Checked = true;
			}
			else {
				
			}
			if( FontType == LatticeToCode.T_part_ascii || FontType == LatticeToCode.T_part_chinese ) {
				this.richTextBox文字.Text = Mes;
			}
			ignoreTextChange = false;
			
			WidthN = PointSet.Length;
			
			ignoreRefresh = true;
			this.textBox宽度.Text = CW.ToString();
			this.textBox高度.Text = HeightN.ToString();
			ignoreRefresh = false;
			
			RefreshLattice( PointSet );
		}
		MPanel.ResetScale();
	}
	
	//更新点阵尺寸
	void RefreshLattice( bool[][] Set )
	{
		MPanel.SetSize( WidthN, HeightN, Set );
		MPanel.SetTip( int.Parse( this.textBox宽度.Text ), 8 );
		
		MPanel.Invalidate();
	}
	
	//更新文字点阵
	void RefreshTextLattice()
	{
		string s = this.richTextBox文字.Text;
		if( String.IsNullOrEmpty( s ) ) {
			return;
		}
		LatticeToCode.CharNumber = s.Length;
		
		WidthN = int.Parse( this.textBox宽度.Text ) * LatticeToCode.CharNumber;
		HeightN = int.Parse( this.textBox高度.Text );
		
		bool[][] PointSet = LatticeToCode.GetLatticeFont( s, TextFont, false, Xoffset, Yoffset, int.Parse( this.textBox宽度.Text ), HeightN );
		
		RefreshLattice( PointSet );
	}
	
	//关闭事件
	void ModuleFormClosing(object sender, FormClosingEventArgs e)
	{
		//isOK = false;
		this.Visible = false;
		//e.Cancel = true;
	}
	
	void 打开图片文件buttonClick(object sender, EventArgs e)
	{
		//打开文件对话框
		OpenFileDialog OpenFileDlg = new OpenFileDialog();
		OpenFileDlg.Filter = "图片文件 | *.*";
		OpenFileDlg.Title = "请选择一个图片文件";
		DialogResult dlgResult = OpenFileDlg.ShowDialog();
		if(dlgResult == DialogResult.OK) {
			string FilePath = OpenFileDlg.FileName;
			MessageBox.Show( FilePath );
		}
	}
	
	void TextBox宽度TextChanged(object sender, EventArgs e)
	{
		if( ignoreRefresh ) {
			return;
		}
		try {
			WidthN = int.Parse( this.textBox宽度.Text ) * LatticeToCode.CharNumber;
			if( WidthN <= 0 ) {
				WidthN = 1;
			}
		}
		catch {
			MessageBox.Show( "点阵尺寸更新失败" );
			ignoreRefresh = true;
			this.textBox宽度.Text = WidthN.ToString();
			ignoreRefresh = false;
		}
		RefreshTextLattice();
	}
	
	void TextBox高度TextChanged(object sender, EventArgs e)
	{
		if( ignoreRefresh ) {
			return;
		}
		try {
			HeightN = int.Parse( this.textBox高度.Text );
			if( HeightN <= 0 ) {
				HeightN = 1;
			}
		}
		catch {
			MessageBox.Show( "点阵尺寸更新失败" );
			ignoreRefresh = true;
			this.textBox高度.Text = HeightN.ToString();
			ignoreRefresh = false;
		}
		RefreshTextLattice();
	}
	
	void RichTextBox文字TextChanged(object sender, EventArgs e)
	{
		if( ignoreTextChange ) {
			return;
		}
		MPanel.UseOld = checkBoxUseOld.Checked;
		RefreshTextLattice();
		MPanel.UseOld = false;
	}
	
	void Button字体Click(object sender, EventArgs e)
	{
		FontDialog f = new FontDialog();
		f.Font = TextFont;
		if( f.ShowDialog() == DialogResult.OK ) {
			TextFont = f.Font;
			
			this.richTextBox文字.Font = TextFont;
			this.richTextBox文字.Select();
			this.richTextBox文字.SelectionFont = TextFont;
			
			RefreshTextLattice();
		}
	}
	
	void Button清空点阵Click(object sender, EventArgs e)
	{
		MPanel.Clear();
		MPanel.Invalidate();
	}
	
	void Button反色点阵Click(object sender, EventArgs e)
	{
		MPanel.Reverse();
		MPanel.Invalidate();
	}
	
	void Button确定Click(object sender, EventArgs e)
	{
		isOK = true;
		this.Visible = false;
	}
	
	//==================================================
	
	void ButtonMoveUpClick(object sender, EventArgs e)
	{
		Yoffset -= 1;
		RefreshTextLattice();
	}
	
	void ButtonMoveDownClick(object sender, EventArgs e)
	{
		Yoffset += 1;
		RefreshTextLattice();
	}
	
	void ButtonMoveLeftClick(object sender, EventArgs e)
	{
		Xoffset -= 1;
		RefreshTextLattice();
	}
	
	void ButtonMoveRightClick(object sender, EventArgs e)
	{
		Xoffset += 1;
		RefreshTextLattice();
	}
	
	void RadioButtonAsciiCheckedChanged(object sender, EventArgs e)
	{
		FontType = LatticeToCode.T_ascii;
		this.richTextBox文字.Text = " !\"#$&%'()*+,-./0123456789:;<=>?@ABCDEFGHIJKLMNOPQRSTUVWXYZ[\\]^_`abcdefghijklmnopqrstuvwxyz{|}~";
		this.richTextBox文字.ReadOnly = true;
		this.richTextBox文字.BackColor = Color.Gainsboro;
	}
	
	void RadioButtonNumberCheckedChanged(object sender, EventArgs e)
	{
		FontType = LatticeToCode.T_part_ascii;
		this.richTextBox文字.Text = "0123456789.+-*/%=: ";
		this.richTextBox文字.ReadOnly = false;
		this.richTextBox文字.BackColor = Color.White;
	}
	
	void RadioButtonChineseCheckedChanged(object sender, EventArgs e)
	{
		FontType = LatticeToCode.T_part_chinese;
		this.richTextBox文字.Text = "输入中文转为点阵";
		this.richTextBox文字.ReadOnly = false;
		this.richTextBox文字.BackColor = Color.White;
	}
	
	void ButtonOpenFontClick(object sender, EventArgs e)
	{
		//打开文件对话框
		OpenFileDialog OpenFileDlg = new OpenFileDialog();
		OpenFileDlg.Filter = Filter;
		OpenFileDlg.Title = "请选择linkboy字体文件";
		OpenFileDlg.InitialDirectory = n_OS.OS.SystemRoot + @"user\font";
		
		DialogResult dlgResult = OpenFileDlg.ShowDialog();
		if(dlgResult == DialogResult.OK) {
			
			string m = n_OS.VIO.OpenTextFileGB2312( OpenFileDlg.FileName );
			SetPoint( m );
		}
	}
	
	void ButtonSaveAsClick(object sender, EventArgs e)
	{
		//保存文件对话框
		SaveFileDialog SaveFileDlg = new SaveFileDialog();
		SaveFileDlg.Filter = Filter;
		SaveFileDlg.Title = "linkboy字体文件另存为...";
		SaveFileDlg.FileName = "我的字体.font";
		
		DialogResult dlgResult = SaveFileDlg.ShowDialog();
		if(dlgResult == DialogResult.OK) {
			string FilePath = SaveFileDlg.FileName;
			if( !FilePath.ToLower().EndsWith( ".font" ) ) {
				FilePath += ".font";
			}
			string r = LatticeToCode.GetCode( MPanel.PointSet, BitType, FontType, int.Parse( this.textBox宽度.Text ), this.richTextBox文字.Text );
			n_OS.VIO.SaveTextFileGB2312( FilePath, r );
		}
	}
}
}





