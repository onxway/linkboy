﻿
using System;
using System.Drawing;
using System.Windows.Forms;
using n_LocatePanel;
using n_MyObject;
using n_MyFileObject;
using n_GUIcoder;
using n_RemoPad;

namespace n_RemoForm
{
/// <summary>
/// Description of LocateForm.
/// </summary>
public partial class RemoForm : Form
{
	const int MaxValueNumber = 9;
	
	int LastIndex;
	string ret_type;
	
	Control[][] CList;
	TextBox[] RetList;
	
	string pad = "      ";
	
	public RemoForm()
	{
		//
		// The InitializeComponent() call is required for Windows Forms designer support.
		//
		InitializeComponent();
	}
	
	//运行
	public void Run()
	{
		if( !n_GUIcoder.GUIcoder.DebugOpenCode ) {
			n_Debug.Warning.WarningMessage = ( "调试功能已禁用, 可能是代码量/RAM过多或者用户占用了串口" );
		}
		buttonDebug.Enabled = n_GUIcoder.GUIcoder.DebugOpen;
		
		RefreshList();
		
		checkBoxEnable.Checked = GUIcoder.DebugOpenSet;
		
		LastIndex = -1;
		ret_type = null;
		panelList.Focus();
		panelList.Select();
		this.Visible = true;
	}
	
	//设置函数返回值
	public void SetRet( int data )
	{
		if( ret_type == n_EXP.EXP.ENote.v_void ) {
			return;
		}
		TextBox textBoxRet = (TextBox)RetList[LastIndex];
		
		if( textBoxRet.BackColor == Color.Khaki ) {
			textBoxRet.BackColor = Color.LightGreen;
		}
		else {
			textBoxRet.BackColor = Color.Khaki;
		}
		if( ret_type == n_EXP.EXP.ENote.v_int32 ) {
			textBoxRet.Text = data.ToString();
		}
		else if( ret_type == n_EXP.EXP.ENote.v_bool ) {
			textBoxRet.Text = data.ToString();
		}
		else if( ret_type == n_EXP.EXP.ENote.v_fix ) {
			textBoxRet.Text = (data / 1024.0).ToString( "f2" );
		}
		else {
			//...
		}
	}
	
	void RefreshList()
	{
		panelList.Controls.Clear();
		string list = GUIcoder.GetAPI( G.CGPanel.myModuleList, false ).Trim( '\n' );
		if( list != "" ) {
			
			CList = new Control[list.Length][];
			RetList = new TextBox[list.Length];
			
			for( int i = 0 ; i < MaxValueNumber; ++i ) {
				list = list.Replace( "#" + i, pad + i + pad );
			}
			
			int MaxWidth = 0;
			int H = 30;
			string[] cut = list.Split( '\n' );
			for( int i = cut.Length - 1; i >= 0; i-- ) {
			//for( int i = 0; i < cut.Length; i++ ) {
				CList[i] = new Control[MaxValueNumber];
				
				Panel p = new Panel();
				p.Dock = DockStyle.Top;
				p.Height = H;
				//p.Width = panelList.Width;
				
				p.Location = new Point( 0, i * H );
				
				Label lb = new Label();
				lb.Name = i.ToString();
				lb.Font = panelList.Font;
				//lb.AutoSize = true;
				lb.Width = p.Width;
				lb.BackColor = Color.Transparent;
				lb.ForeColor = Color.Black;
				lb.Location = new Point( 0, 0 );
				lb.Height = H;
				
				//以!开头表示字符串等暂不支持的参数传递, @符号表示暂不支持带有参数的用户自定义函数
				if( cut[i].StartsWith( "!" ) || cut[i].StartsWith( "@" ) ) {
					lb.ForeColor = Color.Gray;
					lb.Name = cut[i][0].ToString();
					cut[i] = cut[i].Remove( 0, 1 );
				}
				string ttt = cut[i];
				for( int j = 0 ; j < MaxValueNumber; ++j ) {
					ttt = ttt.Replace( pad + j + pad, pad + " " + pad );
				}
				lb.Text = ttt;
				
				lb.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
				lb.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
				//lb.Click += new EventHandler(LabelClick);
				lb.MouseEnter += new EventHandler(LabelMouseEnter);
				lb.MouseLeave += new EventHandler(LabelMouseLeave);
				lb.MouseDown += new MouseEventHandler(LabelMouseDown);
				lb.MouseUp += new MouseEventHandler(LabelMouseUp);
				p.Controls.Add( lb );
				
				Graphics g = Graphics.FromHwnd(lb.Handle);
				int w = (int)( g.MeasureString( pad + "0" + pad, lb.Font ).Width );
				int www = (int)( g.MeasureString( cut[i], lb.Font ).Width );
				if( MaxWidth < www ) {
					MaxWidth = www;
				}
				//添加返回值控件
				int retw = (int)( g.MeasureString( cut[i].Remove( cut[i].IndexOf( ' ' ) ), lb.Font ).Width );
				if( !cut[i].StartsWith( "void" ) ) {
					TextBox t = new TextBox();
					t.Font = lb.Font;
					t.Text = "0";
					t.ForeColor = Color.Black;
					t.BackColor = Color.WhiteSmoke;
					t.Location = new Point( 0, 0 );
					t.Width = retw;
					t.Height = H;
					p.Controls.Add( t );
					RetList[i] = t;
				}
				//添加函数参数
				for( int j = 0; j < MaxValueNumber; ++j ) {
					
					int st = 0;
					int idx = cut[i].IndexOf( pad + (j) + pad );
					if( idx != -1 ) {
						st = (int)( g.MeasureString( cut[i].Remove( idx ), lb.Font ).Width );
					}
					
					TextBox t = new TextBox();
					t.Font = lb.Font;
					t.Text = "0";
					t.ForeColor = Color.Black;
					t.BackColor = Color.WhiteSmoke;
					t.Location = new Point( st, 0 );
					t.Width = w;
					t.Height = H;
					t.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
					t.Click += new EventHandler( TextBoxClick );
					t.TextChanged += new EventHandler( TextBoxTextChanged );
					if( idx != -1 ) {
						p.Controls.Add( t );
						t.BringToFront();
					}
					CList[i][j] = t;
				}
				lb.SendToBack();
				panelList.Controls.Add( p );
			}
			if( Width < MaxWidth + 40 ) {
				Width = MaxWidth + 40;
			}
		}
	}
	
	int GetValue( string text )
	{
		if( text.StartsWith( "'" ) && text.EndsWith( "'" ) && text.Length == 3 ) {
			char c = text[1];
			return (int)c;
		}
		return n_Data.Const.Parse( text );
	}
	
	void LabelMouseDown(object sender, MouseEventArgs e)
	{
		Label lb = (Label)sender;
		
		if( lb.Name == "!" ) {
			MessageBox.Show( "遥控器无法发送此指令(目前只支持参数为bool和int32的指令). 请等待软件升级." );
			return;
		}
		if( lb.Name == "@" ) {
			MessageBox.Show( "遥控器无法发送此指令(目前只支持不含参数的用户自定义指令). 请等待软件升级." );
			return;
		}
		lb.ForeColor = Color.White;
		lb.BackColor = Color.DarkOrange;
		labelMes.Text = "";
		
		try {
			ret_type = "?" + lb.Text.Split( ' ' )[0];
			int sindex = int.Parse( lb.Name );
			LastIndex = sindex;
			int v1 = GetValue( ((TextBox)CList[sindex][0]).Text );
			int v2 = GetValue( ((TextBox)CList[sindex][1]).Text );
			int v3 = GetValue( ((TextBox)CList[sindex][2]).Text );
			int v4 = GetValue( ((TextBox)CList[sindex][3]).Text );
			int v5 = GetValue( ((TextBox)CList[sindex][4]).Text );
			int v6 = GetValue( ((TextBox)CList[sindex][5]).Text );
			bool ok = RemoPad.SendCmdOk( RemoPad.CMD_REMO, (byte)sindex, v1, v2, v3, v4, v5, v6 );
			if( !ok ) {
				lb.ForeColor = Color.Black;
			lb.BackColor = Color.Gainsboro;
			}
		}
		catch {
			labelMes.Text = ( "指令的参数应该均为数字" );
		}
		panelList.Select();
	}
	
	void LabelMouseUp(object sender, MouseEventArgs e)
	{
		Label lb = (Label)sender;
		lb.ForeColor = Color.Black;
		lb.BackColor = Color.Gainsboro;
	}
	
	void LabelMouseEnter(object sender, EventArgs e)
	{
		Label lb = (Label)sender;
		lb.BackColor = Color.Gainsboro;
	}
	
	void LabelMouseLeave(object sender, EventArgs e)
	{
		Label lb = (Label)sender;
		lb.BackColor = Color.Transparent;
	}
	
	void TextBoxClick(object sender, EventArgs e)
	{
		TextBox t = (TextBox)sender;
		//t.SelectAll();
	}
	
	void TextBoxTextChanged(object sender, EventArgs e)
	{
		TextBox t = (TextBox)sender;
		
		Graphics g = Graphics.FromHwnd( t.Handle );
		int w1 = (int)( g.MeasureString( pad + "0" + pad, t.Font ).Width );
		int w2 = (int)( g.MeasureString( t.Text, t.Font ).Width );
		
		if( w2 > w1 ) {
			t.Width = w2;
		}
		else {
			t.Width = w1;
		}
	}
	
	void LocateFormFormClosing(object sender, FormClosingEventArgs e)
	{
		RemoPad.CloseCmd();
		this.Visible = false;
		e.Cancel = true;
	}
	
	void ButtonDebugClick(object sender, EventArgs e)
	{
		G.CGPanel.GHead.DeleDebug();
		if( G.DebugMode ) {
			buttonDebug.Text = "关闭示波器";
		}
		else {
			buttonDebug.Text = "打开示波器";
		}
	}
	
	bool StopUser;
	void ButtonStopUserClick(object sender, EventArgs e)
	{
		StopUser = !StopUser;
		if( StopUser ) {
			buttonStopUser.Text = "开启程序";
			RemoPad.SendCmdOk( RemoPad.CMD_USER, 1, 0, 0, 0, 0, 0, 0 );
		}
		else {
			buttonStopUser.Text = "暂停程序";
			RemoPad.SendCmdOk( RemoPad.CMD_USER, 0, 0, 0, 0, 0, 0, 0 );
		}
	}
	
	void CheckBoxDisableCheckedChanged(object sender, EventArgs e)
	{
		GUIcoder.DebugOpenSet = checkBoxEnable.Checked;
	}
}
}





