﻿/*
 * 由SharpDevelop创建。
 * 用户： cap_gpu
 * 日期: 2017/4/9
 * 时间: 8:03
 * 
 * 要改变这种模板请点击 工具|选项|代码编写|编辑标准头文件
 */
namespace n_RemoForm
{
	partial class RemoForm
	{
		/// <summary>
		/// Designer variable used to keep track of non-visual components.
		/// </summary>
		private System.ComponentModel.IContainer components = null;
		
		/// <summary>
		/// Disposes resources used by the form.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing) {
				if (components != null) {
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}
		
		/// <summary>
		/// This method is required for Windows Forms designer support.
		/// Do not change the method contents inside the source code editor. The Forms designer might
		/// not be able to load this method if it was changed manually.
		/// </summary>
		private void InitializeComponent()
		{
			this.buttonDebug = new System.Windows.Forms.Button();
			this.panel1 = new System.Windows.Forms.Panel();
			this.label4 = new System.Windows.Forms.Label();
			this.checkBoxEnable = new System.Windows.Forms.CheckBox();
			this.label1 = new System.Windows.Forms.Label();
			this.buttonStopUser = new System.Windows.Forms.Button();
			this.label3 = new System.Windows.Forms.Label();
			this.label2 = new System.Windows.Forms.Label();
			this.label5 = new System.Windows.Forms.Label();
			this.labelMes = new System.Windows.Forms.Label();
			this.panelList = new System.Windows.Forms.Panel();
			this.panel1.SuspendLayout();
			this.SuspendLayout();
			// 
			// buttonDebug
			// 
			this.buttonDebug.BackColor = System.Drawing.Color.CornflowerBlue;
			this.buttonDebug.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
			this.buttonDebug.Font = new System.Drawing.Font("微软雅黑", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
			this.buttonDebug.ForeColor = System.Drawing.Color.White;
			this.buttonDebug.Location = new System.Drawing.Point(9, 36);
			this.buttonDebug.Name = "buttonDebug";
			this.buttonDebug.Size = new System.Drawing.Size(123, 42);
			this.buttonDebug.TabIndex = 4;
			this.buttonDebug.Text = "打开示波器";
			this.buttonDebug.UseVisualStyleBackColor = false;
			this.buttonDebug.Click += new System.EventHandler(this.ButtonDebugClick);
			// 
			// panel1
			// 
			this.panel1.BackColor = System.Drawing.Color.White;
			this.panel1.Controls.Add(this.label4);
			this.panel1.Controls.Add(this.checkBoxEnable);
			this.panel1.Controls.Add(this.label1);
			this.panel1.Controls.Add(this.buttonStopUser);
			this.panel1.Controls.Add(this.label3);
			this.panel1.Controls.Add(this.buttonDebug);
			this.panel1.Controls.Add(this.labelMes);
			this.panel1.Controls.Add(this.label5);
			this.panel1.Controls.Add(this.label2);
			this.panel1.Dock = System.Windows.Forms.DockStyle.Top;
			this.panel1.Location = new System.Drawing.Point(0, 0);
			this.panel1.Name = "panel1";
			this.panel1.Size = new System.Drawing.Size(469, 186);
			this.panel1.TabIndex = 11;
			// 
			// label4
			// 
			this.label4.Font = new System.Drawing.Font("微软雅黑", 10.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
			this.label4.ForeColor = System.Drawing.Color.LightSlateGray;
			this.label4.Location = new System.Drawing.Point(165, 1);
			this.label4.Name = "label4";
			this.label4.Size = new System.Drawing.Size(291, 26);
			this.label4.TabIndex = 25;
			this.label4.Text = "禁用后不支持调试，不生成代码，减小体积";
			this.label4.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// checkBoxEnable
			// 
			this.checkBoxEnable.Checked = true;
			this.checkBoxEnable.CheckState = System.Windows.Forms.CheckState.Checked;
			this.checkBoxEnable.Font = new System.Drawing.Font("微软雅黑", 10.5F);
			this.checkBoxEnable.Location = new System.Drawing.Point(12, 3);
			this.checkBoxEnable.Name = "checkBoxEnable";
			this.checkBoxEnable.Size = new System.Drawing.Size(162, 24);
			this.checkBoxEnable.TabIndex = 24;
			this.checkBoxEnable.Text = "启用示波器和遥控器";
			this.checkBoxEnable.UseVisualStyleBackColor = true;
			this.checkBoxEnable.CheckedChanged += new System.EventHandler(this.CheckBoxDisableCheckedChanged);
			// 
			// label1
			// 
			this.label1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
									| System.Windows.Forms.AnchorStyles.Right)));
			this.label1.Font = new System.Drawing.Font("微软雅黑", 10.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
			this.label1.ForeColor = System.Drawing.Color.LightSlateGray;
			this.label1.Location = new System.Drawing.Point(138, 84);
			this.label1.Name = "label1";
			this.label1.Size = new System.Drawing.Size(328, 42);
			this.label1.TabIndex = 23;
			this.label1.Text = "使用遥控器前建议先暂停用户程序以防干扰";
			this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// buttonStopUser
			// 
			this.buttonStopUser.BackColor = System.Drawing.Color.CornflowerBlue;
			this.buttonStopUser.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
			this.buttonStopUser.Font = new System.Drawing.Font("微软雅黑", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
			this.buttonStopUser.ForeColor = System.Drawing.Color.White;
			this.buttonStopUser.Location = new System.Drawing.Point(9, 84);
			this.buttonStopUser.Name = "buttonStopUser";
			this.buttonStopUser.Size = new System.Drawing.Size(123, 42);
			this.buttonStopUser.TabIndex = 22;
			this.buttonStopUser.Text = "暂停程序";
			this.buttonStopUser.UseVisualStyleBackColor = false;
			this.buttonStopUser.Click += new System.EventHandler(this.ButtonStopUserClick);
			// 
			// label3
			// 
			this.label3.Font = new System.Drawing.Font("微软雅黑", 10.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
			this.label3.ForeColor = System.Drawing.Color.LightSlateGray;
			this.label3.Location = new System.Drawing.Point(138, 36);
			this.label3.Name = "label3";
			this.label3.Size = new System.Drawing.Size(331, 42);
			this.label3.TabIndex = 12;
			this.label3.Text = "示波器用来查看传感器和变量的值（曲线图显示）";
			this.label3.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// label2
			// 
			this.label2.Dock = System.Windows.Forms.DockStyle.Bottom;
			this.label2.Font = new System.Drawing.Font("微软雅黑", 10.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
			this.label2.ForeColor = System.Drawing.Color.LightSlateGray;
			this.label2.Location = new System.Drawing.Point(0, 157);
			this.label2.Name = "label2";
			this.label2.Size = new System.Drawing.Size(469, 29);
			this.label2.TabIndex = 21;
			this.label2.Text = "遥控器用来控制外设（点击列表执行对应指令）";
			this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// label5
			// 
			this.label5.BackColor = System.Drawing.Color.Gainsboro;
			this.label5.Dock = System.Windows.Forms.DockStyle.Bottom;
			this.label5.Location = new System.Drawing.Point(0, 155);
			this.label5.Name = "label5";
			this.label5.Size = new System.Drawing.Size(469, 2);
			this.label5.TabIndex = 26;
			// 
			// labelMes
			// 
			this.labelMes.Dock = System.Windows.Forms.DockStyle.Bottom;
			this.labelMes.Font = new System.Drawing.Font("微软雅黑", 10.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
			this.labelMes.ForeColor = System.Drawing.Color.OrangeRed;
			this.labelMes.Location = new System.Drawing.Point(0, 128);
			this.labelMes.Name = "labelMes";
			this.labelMes.Size = new System.Drawing.Size(469, 27);
			this.labelMes.TabIndex = 19;
			this.labelMes.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// panelList
			// 
			this.panelList.AutoScroll = true;
			this.panelList.BackColor = System.Drawing.Color.White;
			this.panelList.Dock = System.Windows.Forms.DockStyle.Fill;
			this.panelList.Font = new System.Drawing.Font("微软雅黑", 12F);
			this.panelList.Location = new System.Drawing.Point(0, 186);
			this.panelList.Name = "panelList";
			this.panelList.Size = new System.Drawing.Size(469, 442);
			this.panelList.TabIndex = 12;
			// 
			// RemoForm
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(241)))), ((int)(((byte)(241)))), ((int)(((byte)(241)))));
			this.ClientSize = new System.Drawing.Size(469, 628);
			this.Controls.Add(this.panelList);
			this.Controls.Add(this.panel1);
			this.MaximizeBox = false;
			this.MinimizeBox = false;
			this.Name = "RemoForm";
			this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
			this.Text = "示波器 / 遥控器";
			this.TopMost = true;
			this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.LocateFormFormClosing);
			this.panel1.ResumeLayout(false);
			this.ResumeLayout(false);
		}
		private System.Windows.Forms.Label label5;
		private System.Windows.Forms.CheckBox checkBoxEnable;
		private System.Windows.Forms.Label label4;
		private System.Windows.Forms.Label label1;
		private System.Windows.Forms.Button buttonStopUser;
		private System.Windows.Forms.Label label2;
		private System.Windows.Forms.Panel panelList;
		private System.Windows.Forms.Label labelMes;
		private System.Windows.Forms.Label label3;
		private System.Windows.Forms.Panel panel1;
		private System.Windows.Forms.Button buttonDebug;
	}
}
