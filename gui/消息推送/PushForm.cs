﻿
namespace n_PushForm
{
using System;
using System.Drawing;
using System.Windows.Forms;
using System.IO;
using n_MainSystemData;
using System.Web;
using System.Diagnostics;

using n_OS;
using System.Threading;

/// <summary>
/// Description of WorkForm.
/// </summary>
public partial class PushForm : Form
{
	public delegate void D_ShowVersion();
	public static D_ShowVersion d_ShowVersion;
	
	public delegate void D_ShowCMap();
	public static D_ShowCMap d_ShowCMap;
	
	bool Dealed = false;
	
	bool Receive = false;
	
	Thread t;
	
	
	public int TickNumber;
	
	string TempPath;
	int CIndex;
	
	bool UserView;
	
	
	bool DebugMode = false;
	
	//主窗口
	public PushForm()
	{
		//
		// The InitializeComponent() call is required for Windows Forms designer support.
		//
		InitializeComponent();
		//this.MinimumSize = this.Size;
		//this.MaximumSize = this.Size;
		
		//this.webBrowser1.Visible = false;
		this.webBrowser1.Dock = DockStyle.Fill;
		
		TickNumber = 0;
		UserView = false;
		
		TempPath = OS.SystemRoot + "Resource" + OS.PATH_S + "mes" + OS.PATH_S + "temp" + OS.PATH_S;
		
		//读取历史消息列表
		String[] files = Directory.GetFileSystemEntries( OS.SystemRoot + "Resource" + OS.PATH_S + "mes" + OS.PATH_S );
		
		CIndex = 1;
		string ss = null;
		foreach(string element in files) {
			
			string s = Path.GetFileName(element);
			ss = s + ";" + ss;
		}
		if( ss != null ) {
			string[] cc = ss.TrimEnd( ';' ).Split( ';' );
			for( int i = 0; i < cc.Length; ++i ) {
				if( cc[i] != "temp" ) {
					
					try {
						AddLink( cc[i] );
					}
					catch {
						MessageBox.Show( "出现一个小小的问题, 建议报告给linbkoy开发团队 - 读取历史消息出错:" + cc[i] );
					}
				}
			}
		}
		
		if( DebugMode ) {
			Check();
		}
		
		//如果距离上次日期很近则不检查更新
		TimeSpan span = DateTime.Now - UserData.LastSignDate;
		if( span.Days > 7 ) {
			//Check();
		}
		//每天都会检查
		if( DateTime.Now.Day != UserData.LastSignDate.Day ) {
			Check();
		}
	}
	
	//显示
	public void Run()
	{
		if( n_Debug.Warning.BugMessage != null ) {
			timerBug.Enabled = true;
		}
		else {
			timerBug.Enabled = false;
		}
		//上次的消息也显示出来
		if( UserData.LastDirName != null ) {
			string path = OS.SystemRoot + "Resource" + OS.PATH_S + "mes" + OS.PATH_S + UserData.LastDirName + OS.PATH_S + "main.html";
			if( File.Exists( path ) ) {
				webBrowser1.Navigate( path );
				webBrowser1.Visible = true;
			}
		}
		
		Visible = true;
	}
	
	//窗体关闭事件
	void FindFormFormClosing(object sender, FormClosingEventArgs e)
	{
		//if( this.checkBoxIgnore.Checked ) {
			//SystemData.LastCheckDate = DateTime.Now;
			//SystemData.isChanged = true;
		//}
		
		if( timerBug.Enabled ) {
			timerBug.Enabled = false;
		}
		
		this.Visible = false;
		e.Cancel = true;
	}
	
	void Timer1Tick(object sender, EventArgs e)
	{
		TickNumber++;
		
		if( TickNumber > 10 ) {
			TickNumber = 0;
			timer1.Enabled = false;
			
			if( t != null && t.IsAlive ) {
				try {
					t.Abort();
				}
				catch {}
			}
			return;
		}
		
		if( Receive ) {
			timer1.Enabled = false;
			string path = TempPath + "main.html";
			webBrowser1.Navigate( path );
		}
	}
	
	void Check()
	{
		//if( MessageBox.Show( "建议点击确定按钮检查软件更新", "检查更新提示", MessageBoxButtons.YesNo ) == DialogResult.No ) {
		//	return;
		//}
		
		TickNumber = 1;
		
		timer1.Enabled = true;
		
		//设置当前时间为最后检查时间
		UserData.LastSignDate = DateTime.Now;
		
		if( UserData.LastDirName == null ) {
			UserData.LastDirName = "null";
		}
		
		//2019.5.9 今天发现的奇怪问题, 界面卡卡住, 解压软件也是一样, 然后重启就好了..
		
		//UserData.isChanged = true;
		//防止万一界面卡住, 当天就不再提示更新
		UserData.Save();
		
		
		
		
		
		//==================================
		
		t = new Thread( HttpDown );
		t.Priority = ThreadPriority.Lowest;
		t.Start();
	}
	
	void AddLink( string name )
	{
		string[] names = name.Split( ',' );
		
		LinkLabel l = new LinkLabel();
		l.Font = new System.Drawing.Font( "微软雅黑", 12 );
		l.Text = names[1];
		l.Name = name;
		l.Visible = true;
		l.Location = new System.Drawing.Point( 0, 10 + CIndex * 50 );
		l.Size = new System.Drawing.Size( panel2.Width,  20 );
		l.LinkClicked += new LinkLabelLinkClickedEventHandler(l_LinkClicked);
		panel2.Controls.Add( l );
		
		Label la = new Label();
		la.Font = new System.Drawing.Font( "微软雅黑", 12 );
		la.ForeColor = System.Drawing.Color.Gray;
		la.Text = names[0];
		la.Visible = true;
		la.Location = new System.Drawing.Point( 0, 10 + CIndex * 50 + 20 );
		la.Size = new System.Drawing.Size( panel2.Width,  20 );
		panel2.Controls.Add( la );
		
		CIndex++;
	}
	
	void l_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
	{
		UserView = true;
		
		string n = ((LinkLabel)sender).Name;
		string path = OS.SystemRoot + "Resource" + OS.PATH_S + "mes" + OS.PATH_S + n + OS.PATH_S + "main.html";
		webBrowser1.Navigate( path );
	}
	
	//--------------------------------------------------
	
	//HTTP 下载
	void HttpDown()
	{
		if( !Directory.Exists( TempPath ) ) {
			Directory.CreateDirectory( TempPath );
		}
		try {
		string path = TempPath + "main.html";
		
		n_Web.HTTP.HttpDownloadFile( "http://linkboy.cc/mes/main.html", path );
		
		
		//分析下载的文件, 判断是否有其他文件需要下载
		string s = VIO.OpenTextFileGB2312( path );
		
		GetFilenameFromFlag( s, "<img src=" );
		GetFilenameFromFlag( s, "<a href=" );
		
		Receive = true;
		}
		catch {
			//this.label1.Text = "消息查找失败";
		}
		if( t != null && t.IsAlive ) {
			try {
				t.Abort();
			}
			catch {
				//...
			}
		}
	}
	
	//提取指定标记的文件名
	void GetFilenameFromFlag( string s, string flag )
	{
		string filename = null;
		
		int Index = 0;
		while( Index < s.Length ) {
			Index = s.IndexOf( flag, Index );
			if( Index == -1 ) {
				break;
			}
			Index += flag.Length + 1;
			
			while( Index < s.Length ) {
				if( s[Index] == '"' ) {
					break;
				}
				filename += s[Index];
				Index++;
			}
			if( filename != null ) {
				string path = TempPath + filename;
				n_Web.HTTP.HttpDownloadFile( "http://linkboy.cc/mes/" + filename, path );
			}
		}
		//return filename;
	}
	
	//文档接收完毕事件
	void WebBrowser1DocumentCompleted(object sender, WebBrowserDocumentCompletedEventArgs e)
	{
		if( UserView ) {
			return;
		}
		if( webBrowser1.ReadyState != WebBrowserReadyState.Complete ) {
    		return;
		}
		webBrowser1.DocumentCompleted -= WebBrowser1DocumentCompleted;
		
		if( Dealed ) {
			return;
		}
		
		Dealed = true;
		
		timer1.Enabled = false;
		TickNumber = 0;
		
		string[] c = webBrowser1.DocumentTitle.Split( ',' );
		
		if( c[0] != "linkboy" ) {
			//没找到信息页面
			return;
		}
		
		//判断是否有新版本
		if( !G.isCruxEXE && c[2] != G.VersionMessage ) {
			n_Debug.Warning.WarningMessage = "检测到新版本: " + c[2] + ", 可以在官方QQ群下载到此版本\n(或者到官网 www.linkboy.cc 下载, 不过官网的版本更新可能会滞后一些)";
		}
		
		string LastTitle = UserData.LastWebMes;
		if( !DebugMode && c[1] == LastTitle ) {
			//信息页面已查看过
			return;
		}
		
		//复制信息文件夹
		string DirName = c[1] + "," + c[4];
		string SavePath = OS.SystemRoot + "Resource" + OS.PATH_S + "mes" + OS.PATH_S + DirName + OS.PATH_S;
		if( !Directory.Exists( SavePath ) ) {
			Directory.CreateDirectory( SavePath );
		}
		String[] files = Directory.GetFileSystemEntries( TempPath );
		foreach(string element in files) {
			File.Copy(element, SavePath + Path.GetFileName(element), true);
		}
		CIndex = 0;
		AddLink( DirName );
		
		Visible = true;
		
		webBrowser1.Visible = true;
		
		UserData.LastWebMes = c[1];
		UserData.LastDirName = DirName;
		UserData.isChanged = true;
		
		try {
			string[] c1 = LastTitle.Split( '-' );
			string[] c2 = c[1].Split( '-' );
			int n1 = int.Parse( c1[1] );
			int n2 = int.Parse( c2[1] );
			if( n1 < n2 - 1 ) {
				MessageBox.Show( "\n您好久没有打开linbkoy软件了, 已经错过了 " + (n2 - n1 - 1) + " 个消息, 建议经常使用linkboy呦~" );
			}
		}
		catch {
			n_Debug.Warning.BUG( "系统预存出现一点小问题, 不影响您的使用" );
		}
		webBrowser1.Select();
		webBrowser1.Focus();
	}
	
	//访问官网
	void LinkLabel1LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
	{
		System.Diagnostics.Process.Start("http://www.linkboy.cc");
	}
	
	//问题反馈
	void ButtonQuesClick(object sender, EventArgs e)
	{
		if( G.ServiceBox == null ) {
			G.ServiceBox = new n_ServiceForm.ServiceForm();
		}
		G.ServiceBox.Run();
	}
	
	//--------------------------------------------------
	
	void ButtonMapClick(object sender, EventArgs e)
	{
		if( d_ShowCMap != null ) {
			d_ShowCMap();
		}
	}
	
	void ButtonVersionClick(object sender, EventArgs e)
	{
		if( d_ShowVersion != null ) {
			d_ShowVersion();
		}
	}
	
	void TimerBugTick(object sender, EventArgs e)
	{
		if( buttonQues.BackColor == Color.SandyBrown ) {
			buttonQues.BackColor = Color.LightSteelBlue;
		}
		else {
			buttonQues.BackColor = Color.SandyBrown;
		}
	}
	
	void ButtonStudyClick(object sender, EventArgs e)
	{
		Process Proc = new Process();
		Proc.StartInfo.WindowStyle = ProcessWindowStyle.Normal;
		//Proc.StartInfo.UseShellExecute = false;
		//Proc.StartInfo.CreateNoWindow = true;
		//Proc.StartInfo.RedirectStandardOutput = true;
		
		Proc.StartInfo.WorkingDirectory = AppDomain.CurrentDomain.BaseDirectory;
		
		Proc.StartInfo.FileName = @"学习中心.exe";
		
		string StartMes = "wq";
		Proc.StartInfo.Arguments = StartMes;
		
		Proc.Start();
	}
	
	void ButtonBugClick(object sender, EventArgs e)
	{
		Process Proc = new Process();
		Proc.StartInfo.WindowStyle = ProcessWindowStyle.Normal;
		//Proc.StartInfo.UseShellExecute = false;
		//Proc.StartInfo.CreateNoWindow = true;
		//Proc.StartInfo.RedirectStandardOutput = true;
		
		Proc.StartInfo.WorkingDirectory = AppDomain.CurrentDomain.BaseDirectory;
		Proc.StartInfo.FileName = "study\\备忘录.doc";
		
		try {
			Proc.Start();
		}
		catch {
			MessageBox.Show( "无法打开文件，您需要先在电脑上安装任意一款能编辑查看word的软件，并确保双击.doc文件能打开: " + Proc.StartInfo.FileName );
		}
	}
}
}


