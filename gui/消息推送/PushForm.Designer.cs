﻿/*
 * 由 SharpDevelop 创建。
 * 用户: bbb_2
 * 日期: 2009-11-17
 * 时间: 11:15
 * 
 * 如果你想要更改该模板，那么请使用“工具 | 选项 | 编辑 | 编辑标准标题”。
 */
namespace n_PushForm
{
	partial class PushForm
	{
		/// <summary>
		/// Designer variable used to keep track of non-visual components.
		/// </summary>
		private System.ComponentModel.IContainer components = null;
		
		/// <summary>
		/// Disposes resources used by the form.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing) {
				if (components != null) {
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}
		
		/// <summary>
		/// This method is required for Windows Forms designer support.
		/// Do not change the method contents inside the source code editor. The Forms designer might
		/// not be able to load this method if it was changed manually.
		/// </summary>
		private void InitializeComponent()
		{
			this.components = new System.ComponentModel.Container();
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(PushForm));
			this.timer1 = new System.Windows.Forms.Timer(this.components);
			this.panel1 = new System.Windows.Forms.Panel();
			this.buttonBug = new System.Windows.Forms.Button();
			this.buttonStudy = new System.Windows.Forms.Button();
			this.buttonVersion = new System.Windows.Forms.Button();
			this.buttonMap = new System.Windows.Forms.Button();
			this.buttonQues = new System.Windows.Forms.Button();
			this.linkLabel1 = new System.Windows.Forms.LinkLabel();
			this.webBrowser1 = new System.Windows.Forms.WebBrowser();
			this.panel2 = new System.Windows.Forms.Panel();
			this.timerBug = new System.Windows.Forms.Timer(this.components);
			this.panel1.SuspendLayout();
			this.SuspendLayout();
			// 
			// timer1
			// 
			this.timer1.Interval = 1000;
			this.timer1.Tick += new System.EventHandler(this.Timer1Tick);
			// 
			// panel1
			// 
			this.panel1.BackColor = System.Drawing.Color.White;
			this.panel1.Controls.Add(this.buttonBug);
			this.panel1.Controls.Add(this.buttonStudy);
			this.panel1.Controls.Add(this.buttonVersion);
			this.panel1.Controls.Add(this.buttonMap);
			this.panel1.Controls.Add(this.buttonQues);
			resources.ApplyResources(this.panel1, "panel1");
			this.panel1.Name = "panel1";
			// 
			// buttonBug
			// 
			this.buttonBug.BackColor = System.Drawing.Color.WhiteSmoke;
			resources.ApplyResources(this.buttonBug, "buttonBug");
			this.buttonBug.ForeColor = System.Drawing.Color.Black;
			this.buttonBug.Name = "buttonBug";
			this.buttonBug.UseVisualStyleBackColor = false;
			this.buttonBug.Click += new System.EventHandler(this.ButtonBugClick);
			// 
			// buttonStudy
			// 
			this.buttonStudy.BackColor = System.Drawing.Color.WhiteSmoke;
			resources.ApplyResources(this.buttonStudy, "buttonStudy");
			this.buttonStudy.ForeColor = System.Drawing.Color.Black;
			this.buttonStudy.Name = "buttonStudy";
			this.buttonStudy.UseVisualStyleBackColor = false;
			this.buttonStudy.Click += new System.EventHandler(this.ButtonStudyClick);
			// 
			// buttonVersion
			// 
			this.buttonVersion.BackColor = System.Drawing.Color.WhiteSmoke;
			resources.ApplyResources(this.buttonVersion, "buttonVersion");
			this.buttonVersion.ForeColor = System.Drawing.Color.Black;
			this.buttonVersion.Name = "buttonVersion";
			this.buttonVersion.UseVisualStyleBackColor = false;
			this.buttonVersion.Click += new System.EventHandler(this.ButtonVersionClick);
			// 
			// buttonMap
			// 
			this.buttonMap.BackColor = System.Drawing.Color.WhiteSmoke;
			resources.ApplyResources(this.buttonMap, "buttonMap");
			this.buttonMap.ForeColor = System.Drawing.Color.Black;
			this.buttonMap.Name = "buttonMap";
			this.buttonMap.UseVisualStyleBackColor = false;
			this.buttonMap.Click += new System.EventHandler(this.ButtonMapClick);
			// 
			// buttonQues
			// 
			this.buttonQues.BackColor = System.Drawing.Color.WhiteSmoke;
			resources.ApplyResources(this.buttonQues, "buttonQues");
			this.buttonQues.ForeColor = System.Drawing.Color.Black;
			this.buttonQues.Name = "buttonQues";
			this.buttonQues.UseVisualStyleBackColor = false;
			this.buttonQues.Click += new System.EventHandler(this.ButtonQuesClick);
			// 
			// linkLabel1
			// 
			this.linkLabel1.ActiveLinkColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(128)))), ((int)(((byte)(255)))));
			this.linkLabel1.BackColor = System.Drawing.Color.White;
			this.linkLabel1.DisabledLinkColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(128)))), ((int)(((byte)(255)))));
			resources.ApplyResources(this.linkLabel1, "linkLabel1");
			this.linkLabel1.LinkColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(128)))), ((int)(((byte)(255)))));
			this.linkLabel1.Name = "linkLabel1";
			this.linkLabel1.TabStop = true;
			this.linkLabel1.VisitedLinkColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(128)))), ((int)(((byte)(255)))));
			this.linkLabel1.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.LinkLabel1LinkClicked);
			// 
			// webBrowser1
			// 
			resources.ApplyResources(this.webBrowser1, "webBrowser1");
			this.webBrowser1.Name = "webBrowser1";
			this.webBrowser1.DocumentCompleted += new System.Windows.Forms.WebBrowserDocumentCompletedEventHandler(this.WebBrowser1DocumentCompleted);
			// 
			// panel2
			// 
			resources.ApplyResources(this.panel2, "panel2");
			this.panel2.BackColor = System.Drawing.Color.White;
			this.panel2.Name = "panel2";
			// 
			// timerBug
			// 
			this.timerBug.Interval = 300;
			this.timerBug.Tick += new System.EventHandler(this.TimerBugTick);
			// 
			// PushForm
			// 
			resources.ApplyResources(this, "$this");
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.BackColor = System.Drawing.Color.WhiteSmoke;
			this.Controls.Add(this.webBrowser1);
			this.Controls.Add(this.panel2);
			this.Controls.Add(this.panel1);
			this.Controls.Add(this.linkLabel1);
			this.Name = "PushForm";
			this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.FindFormFormClosing);
			this.panel1.ResumeLayout(false);
			this.ResumeLayout(false);
		}
		private System.Windows.Forms.Button buttonBug;
		private System.Windows.Forms.Button buttonStudy;
		private System.Windows.Forms.Timer timerBug;
		private System.Windows.Forms.Button buttonVersion;
		private System.Windows.Forms.Button buttonMap;
		private System.Windows.Forms.Panel panel2;
		private System.Windows.Forms.Button buttonQues;
		private System.Windows.Forms.LinkLabel linkLabel1;
		private System.Windows.Forms.WebBrowser webBrowser1;
		private System.Windows.Forms.Panel panel1;
		private System.Windows.Forms.Timer timer1;
	}
}
