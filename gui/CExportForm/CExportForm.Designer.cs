﻿/*
 * 由 SharpDevelop 创建。
 * 用户: bbb_2
 * 日期: 2009-11-17
 * 时间: 11:15
 * 
 * 如果你想要更改该模板，那么请使用“工具 | 选项 | 编辑 | 编辑标准标题”。
 */
namespace n_CExportForm
{
	partial class CExportForm
	{
		/// <summary>
		/// Designer variable used to keep track of non-visual components.
		/// </summary>
		private System.ComponentModel.IContainer components = null;
		
		/// <summary>
		/// Disposes resources used by the form.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing) {
				if (components != null) {
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}
		
		/// <summary>
		/// This method is required for Windows Forms designer support.
		/// Do not change the method contents inside the source code editor. The Forms designer might
		/// not be able to load this method if it was changed manually.
		/// </summary>
		private void InitializeComponent()
		{
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(CExportForm));
			this.richTextBoxCode = new System.Windows.Forms.RichTextBox();
			this.panel2 = new System.Windows.Forms.Panel();
			this.labelPath = new System.Windows.Forms.Label();
			this.buttonSave = new System.Windows.Forms.Button();
			this.checkBoxAutoCopy = new System.Windows.Forms.CheckBox();
			this.buttonCopy = new System.Windows.Forms.Button();
			this.panel1 = new System.Windows.Forms.Panel();
			this.label2 = new System.Windows.Forms.Label();
			this.textBoxPre_t = new System.Windows.Forms.TextBox();
			this.textBoxLcPre = new System.Windows.Forms.TextBox();
			this.buttonOpenPlatform = new System.Windows.Forms.Button();
			this.label4 = new System.Windows.Forms.Label();
			this.comboBoxPlatform = new System.Windows.Forms.ComboBox();
			this.label3 = new System.Windows.Forms.Label();
			this.textBoxPre = new System.Windows.Forms.TextBox();
			this.labelChange = new System.Windows.Forms.Label();
			this.checkBoxC99 = new System.Windows.Forms.CheckBox();
			this.label1 = new System.Windows.Forms.Label();
			this.panel2.SuspendLayout();
			this.panel1.SuspendLayout();
			this.SuspendLayout();
			// 
			// richTextBoxCode
			// 
			this.richTextBoxCode.BorderStyle = System.Windows.Forms.BorderStyle.None;
			resources.ApplyResources(this.richTextBoxCode, "richTextBoxCode");
			this.richTextBoxCode.Name = "richTextBoxCode";
			// 
			// panel2
			// 
			this.panel2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(251)))), ((int)(((byte)(251)))), ((int)(((byte)(251)))));
			this.panel2.Controls.Add(this.labelPath);
			this.panel2.Controls.Add(this.buttonSave);
			this.panel2.Controls.Add(this.checkBoxAutoCopy);
			this.panel2.Controls.Add(this.buttonCopy);
			resources.ApplyResources(this.panel2, "panel2");
			this.panel2.Name = "panel2";
			// 
			// labelPath
			// 
			resources.ApplyResources(this.labelPath, "labelPath");
			this.labelPath.Name = "labelPath";
			// 
			// buttonSave
			// 
			this.buttonSave.BackColor = System.Drawing.Color.Gainsboro;
			resources.ApplyResources(this.buttonSave, "buttonSave");
			this.buttonSave.Name = "buttonSave";
			this.buttonSave.UseVisualStyleBackColor = false;
			this.buttonSave.Click += new System.EventHandler(this.ButtonSaveClick);
			// 
			// checkBoxAutoCopy
			// 
			resources.ApplyResources(this.checkBoxAutoCopy, "checkBoxAutoCopy");
			this.checkBoxAutoCopy.ForeColor = System.Drawing.Color.SlateGray;
			this.checkBoxAutoCopy.Name = "checkBoxAutoCopy";
			this.checkBoxAutoCopy.UseVisualStyleBackColor = true;
			// 
			// buttonCopy
			// 
			this.buttonCopy.BackColor = System.Drawing.Color.Gainsboro;
			resources.ApplyResources(this.buttonCopy, "buttonCopy");
			this.buttonCopy.Name = "buttonCopy";
			this.buttonCopy.UseVisualStyleBackColor = false;
			this.buttonCopy.Click += new System.EventHandler(this.ButtonCopyClick);
			// 
			// panel1
			// 
			this.panel1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(251)))), ((int)(((byte)(251)))), ((int)(((byte)(251)))));
			this.panel1.Controls.Add(this.label2);
			this.panel1.Controls.Add(this.textBoxPre_t);
			this.panel1.Controls.Add(this.textBoxLcPre);
			this.panel1.Controls.Add(this.buttonOpenPlatform);
			this.panel1.Controls.Add(this.label4);
			this.panel1.Controls.Add(this.comboBoxPlatform);
			this.panel1.Controls.Add(this.label3);
			this.panel1.Controls.Add(this.textBoxPre);
			this.panel1.Controls.Add(this.labelChange);
			this.panel1.Controls.Add(this.checkBoxC99);
			this.panel1.Controls.Add(this.label1);
			resources.ApplyResources(this.panel1, "panel1");
			this.panel1.Name = "panel1";
			// 
			// label2
			// 
			resources.ApplyResources(this.label2, "label2");
			this.label2.ForeColor = System.Drawing.Color.Black;
			this.label2.Name = "label2";
			// 
			// textBoxPre_t
			// 
			resources.ApplyResources(this.textBoxPre_t, "textBoxPre_t");
			this.textBoxPre_t.Name = "textBoxPre_t";
			this.textBoxPre_t.TextChanged += new System.EventHandler(this.TextBoxPre_tTextChanged);
			// 
			// textBoxLcPre
			// 
			resources.ApplyResources(this.textBoxLcPre, "textBoxLcPre");
			this.textBoxLcPre.Name = "textBoxLcPre";
			this.textBoxLcPre.TextChanged += new System.EventHandler(this.TextBoxLcPreTextChanged);
			// 
			// buttonOpenPlatform
			// 
			this.buttonOpenPlatform.BackColor = System.Drawing.Color.Gainsboro;
			resources.ApplyResources(this.buttonOpenPlatform, "buttonOpenPlatform");
			this.buttonOpenPlatform.Name = "buttonOpenPlatform";
			this.buttonOpenPlatform.UseVisualStyleBackColor = false;
			this.buttonOpenPlatform.Click += new System.EventHandler(this.ButtonOpenPlatformClick);
			// 
			// label4
			// 
			resources.ApplyResources(this.label4, "label4");
			this.label4.ForeColor = System.Drawing.Color.Black;
			this.label4.Name = "label4";
			// 
			// comboBoxPlatform
			// 
			this.comboBoxPlatform.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
			resources.ApplyResources(this.comboBoxPlatform, "comboBoxPlatform");
			this.comboBoxPlatform.FormattingEnabled = true;
			this.comboBoxPlatform.Items.AddRange(new object[] {
									resources.GetString("comboBoxPlatform.Items"),
									resources.GetString("comboBoxPlatform.Items1"),
									resources.GetString("comboBoxPlatform.Items2"),
									resources.GetString("comboBoxPlatform.Items3"),
									resources.GetString("comboBoxPlatform.Items4")});
			this.comboBoxPlatform.Name = "comboBoxPlatform";
			this.comboBoxPlatform.SelectedIndexChanged += new System.EventHandler(this.ComboBoxPlatformSelectedIndexChanged);
			// 
			// label3
			// 
			resources.ApplyResources(this.label3, "label3");
			this.label3.ForeColor = System.Drawing.Color.Gray;
			this.label3.Name = "label3";
			// 
			// textBoxPre
			// 
			resources.ApplyResources(this.textBoxPre, "textBoxPre");
			this.textBoxPre.Name = "textBoxPre";
			this.textBoxPre.TextChanged += new System.EventHandler(this.TextBoxPreTextChanged);
			// 
			// labelChange
			// 
			resources.ApplyResources(this.labelChange, "labelChange");
			this.labelChange.ForeColor = System.Drawing.Color.OrangeRed;
			this.labelChange.Name = "labelChange";
			// 
			// checkBoxC99
			// 
			this.checkBoxC99.Checked = true;
			this.checkBoxC99.CheckState = System.Windows.Forms.CheckState.Checked;
			resources.ApplyResources(this.checkBoxC99, "checkBoxC99");
			this.checkBoxC99.Name = "checkBoxC99";
			this.checkBoxC99.UseVisualStyleBackColor = true;
			this.checkBoxC99.CheckedChanged += new System.EventHandler(this.CheckBoxC99CheckedChanged);
			// 
			// label1
			// 
			resources.ApplyResources(this.label1, "label1");
			this.label1.ForeColor = System.Drawing.Color.Gray;
			this.label1.Name = "label1";
			// 
			// CExportForm
			// 
			resources.ApplyResources(this, "$this");
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.BackColor = System.Drawing.Color.White;
			this.Controls.Add(this.richTextBoxCode);
			this.Controls.Add(this.panel2);
			this.Controls.Add(this.panel1);
			this.Name = "CExportForm";
			this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.CodeFormFormClosing);
			this.panel2.ResumeLayout(false);
			this.panel1.ResumeLayout(false);
			this.panel1.PerformLayout();
			this.ResumeLayout(false);
		}
		private System.Windows.Forms.Label label2;
		private System.Windows.Forms.TextBox textBoxPre_t;
		private System.Windows.Forms.Label labelPath;
		private System.Windows.Forms.TextBox textBoxLcPre;
		private System.Windows.Forms.Button buttonOpenPlatform;
		private System.Windows.Forms.Label label4;
		private System.Windows.Forms.ComboBox comboBoxPlatform;
		private System.Windows.Forms.Label label3;
		private System.Windows.Forms.TextBox textBoxPre;
		private System.Windows.Forms.Label labelChange;
		private System.Windows.Forms.Label label1;
		private System.Windows.Forms.CheckBox checkBoxC99;
		private System.Windows.Forms.Button buttonSave;
		private System.Windows.Forms.Panel panel2;
		private System.Windows.Forms.Button buttonCopy;
		private System.Windows.Forms.RichTextBox richTextBoxCode;
		private System.Windows.Forms.CheckBox checkBoxAutoCopy;
		private System.Windows.Forms.Panel panel1;
	}
}
