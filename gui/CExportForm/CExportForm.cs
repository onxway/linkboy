﻿
namespace n_CExportForm
{
using System;
using System.Windows.Forms;
using System.Drawing;
using System.Drawing.Drawing2D;

/// <summary>
/// Description of WorkForm.
/// </summary>
public partial class CExportForm : Form
{
	bool firstMes = true;
	bool first = true;
	bool isChanged = false;
	
	//主窗口
	public CExportForm()
	{
		InitializeComponent();
		
		comboBoxPlatform.SelectedIndex = -1;
	}
	
	//运行
	public void Run()
	{
		string code = n_SST.SST.GetCode();
		richTextBoxCode.Text = code;
		SetFont();
		
		if( !first && checkBoxAutoCopy.Checked ) {
			MyCopy( code );
			return;
		}
		labelChange.Visible = false;
		isChanged = false;
		
		this.Visible = true;
		this.Activate();
	}
	
	void SetFont()
	{
		Font f = null;
		try {
			f = new Font( "Courier New", 12 );
			f = new Font( "Consolas", 12 );
		}
		catch {
			
		}
		if( f != null ) {
			richTextBoxCode.SelectAll();
			
			richTextBoxCode.Font = f;
			richTextBoxCode.SelectionFont = f;
			
			richTextBoxCode.SelectionLength = 0;
			richTextBoxCode.SelectionStart = richTextBoxCode.TextLength - 1;
			richTextBoxCode.ScrollToCaret();
		}
	}
	
	//配置修改
	void ConfigChanged()
	{
		isChanged = true;
		labelChange.Visible = true;
	}
	
	void MyCopy( string text )
	{
		try {
			System.Windows.Forms.Clipboard.SetText( text );
		}
		catch (Exception ee) {
			n_OS.VIO.Show( "复制失败: " + ee );
		}
	}
	
	void CodeFormFormClosing(object sender, FormClosingEventArgs e)
	{
		first = false;
		e.Cancel = true;
		this.Visible = false;
	}
	
	void ButtonCopyClick(object sender, EventArgs e)
	{
		if( !G.isCV && comboBoxPlatform.SelectedIndex == -1 ) {
			MessageBox.Show( "请选择目标平台" );
			return;
		}
		if( isChanged ) {
			MessageBox.Show( "配置已更改, 建议再次点击导出按钮, 将根据新的配置重新生成代码" );
		}
		
		MyCopy( richTextBoxCode.Text );
		
		if( firstMes ) {
			firstMes = false;
			MessageBox.Show( "已复制C程序到剪贴板. 目前导出C语言功能未经全面测试, 如果粘贴到arduino IDE等环境编译报错或者运行异常, 请联系我修复问题." );
		}
		
		this.Visible = false;
	}
	
	void ButtonSaveClick(object sender, EventArgs e)
	{
		if( !G.isCV && comboBoxPlatform.SelectedIndex == -1 ) {
			MessageBox.Show( "请选择目标平台" );
			return;
		}
		if( isChanged ) {
			MessageBox.Show( "配置已更改, 建议再次点击导出按钮, 将根据新的配置重新生成代码" );
		}
		string t = richTextBoxCode.Text;
		
		if( labelPath.Text == "" ) {
			
			//保存文件对话框
			SaveFileDialog SaveFileDlg = new SaveFileDialog();
			SaveFileDlg.Filter = "源码文件 | *.c";
			SaveFileDlg.Title = "保存文件...";
			
			SaveFileDlg.InitialDirectory = G.ccode.FilePath;
			SaveFileDlg.FileName = G.ccode.FileName;
			
			DialogResult dlgResult = SaveFileDlg.ShowDialog();
			if(dlgResult == DialogResult.OK) {
				labelPath.Text = SaveFileDlg.FileName;
			}
		}
		if( labelPath.Text == "" ) {
			return;
		}
		n_OS.VIO.SaveTextFileUTF8( labelPath.Text, t );
		
		if( firstMes ) {
			firstMes = false;
			MessageBox.Show( "已保存C程序. 目前导出C语言功能未经全面测试, 如果粘贴到arduino IDE等环境编译报错或者运行异常, 请联系我修复问题." );
		}
	}
	
	void CheckBoxC99CheckedChanged(object sender, EventArgs e)
	{
		ConfigChanged();
		
		n_SST.SST.Mode_C90 = !checkBoxC99.Checked;
	}
	
	void TextBoxPreTextChanged(object sender, EventArgs e)
	{
		ConfigChanged();
		
		n_SST.SST.PRE_g = textBoxPre.Text;
	}
	
	void ComboBoxPlatformSelectedIndexChanged(object sender, EventArgs e)
	{
		ConfigChanged();
		
		if( comboBoxPlatform.SelectedIndex == 4 ) {
			checkBoxC99.Checked = false;
		}
		else {
			checkBoxC99.Checked = true;
		}
		n_SST.SST.ModPlatform = comboBoxPlatform.SelectedIndex.ToString();
		n_SST.SST.RefreshModPlatform();
	}
	
	void ButtonOpenPlatformClick(object sender, EventArgs e)
	{
		string fileToSelect = n_OS.OS.SystemRoot + "扩展包" + n_OS.OS.PATH_S + "Default.txt";
		
		string args = string.Format("/Select, {0}", fileToSelect);
		System.Diagnostics.Process.Start( new System.Diagnostics.ProcessStartInfo("Explorer.exe", args ) );
	}
	
	void TextBoxLcPreTextChanged(object sender, EventArgs e)
	{
		ConfigChanged();
		
		n_SST.SST.PRE_l = textBoxLcPre.Text;
	}
	
	void TextBoxPre_tTextChanged(object sender, EventArgs e)
	{
		ConfigChanged();
		
		n_SST.SST.PRE_t = textBoxPre_t.Text;
	}
}
}



