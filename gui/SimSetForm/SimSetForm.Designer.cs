﻿/*
 * 由SharpDevelop创建。
 * 用户： cap_gpu
 * 日期: 2017/4/9
 * 时间: 8:03
 * 
 * 要改变这种模板请点击 工具|选项|代码编写|编辑标准头文件
 */
namespace n_SimSetForm
{
	partial class SimSetForm
	{
		/// <summary>
		/// Designer variable used to keep track of non-visual components.
		/// </summary>
		private System.ComponentModel.IContainer components = null;
		
		/// <summary>
		/// Disposes resources used by the form.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing) {
				if (components != null) {
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}
		
		/// <summary>
		/// This method is required for Windows Forms designer support.
		/// Do not change the method contents inside the source code editor. The Forms designer might
		/// not be able to load this method if it was changed manually.
		/// </summary>
		private void InitializeComponent()
		{
			this.checkBoxMainRefresh = new System.Windows.Forms.CheckBox();
			this.label15 = new System.Windows.Forms.Label();
			this.label16 = new System.Windows.Forms.Label();
			this.checkBoxShowCross = new System.Windows.Forms.CheckBox();
			this.SuspendLayout();
			// 
			// checkBoxMainRefresh
			// 
			this.checkBoxMainRefresh.Font = new System.Drawing.Font("微软雅黑", 12F);
			this.checkBoxMainRefresh.ForeColor = System.Drawing.Color.Blue;
			this.checkBoxMainRefresh.Location = new System.Drawing.Point(25, 71);
			this.checkBoxMainRefresh.Name = "checkBoxMainRefresh";
			this.checkBoxMainRefresh.Size = new System.Drawing.Size(312, 24);
			this.checkBoxMainRefresh.TabIndex = 23;
			this.checkBoxMainRefresh.Text = "游戏界面运行时主程序界面也同步刷新";
			this.checkBoxMainRefresh.UseVisualStyleBackColor = true;
			this.checkBoxMainRefresh.CheckedChanged += new System.EventHandler(this.CheckBoxDisableHitCheckedChanged);
			// 
			// label15
			// 
			this.label15.Font = new System.Drawing.Font("微软雅黑", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
			this.label15.ForeColor = System.Drawing.Color.SlateGray;
			this.label15.Location = new System.Drawing.Point(12, 7);
			this.label15.Name = "label15";
			this.label15.Size = new System.Drawing.Size(654, 34);
			this.label15.TabIndex = 24;
			this.label15.Text = "提示：以下所有设置，重启软件后将重新设置为默认值";
			this.label15.TextAlign = System.Drawing.ContentAlignment.TopCenter;
			// 
			// label16
			// 
			this.label16.Font = new System.Drawing.Font("微软雅黑", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
			this.label16.ForeColor = System.Drawing.Color.SlateGray;
			this.label16.Location = new System.Drawing.Point(25, 107);
			this.label16.Name = "label16";
			this.label16.Size = new System.Drawing.Size(641, 70);
			this.label16.TabIndex = 12;
			this.label16.Text = "默认不刷新，也就是游戏界面运行时，主程序界面看不到指令和变量的动态变化。如果需要看到游戏运行过程各个指令的实时执行情况，可以勾选此项。但会稍微降低游戏画面流畅度。" +
			"";
			// 
			// checkBoxShowCross
			// 
			this.checkBoxShowCross.Font = new System.Drawing.Font("微软雅黑", 12F);
			this.checkBoxShowCross.ForeColor = System.Drawing.Color.Blue;
			this.checkBoxShowCross.Location = new System.Drawing.Point(25, 200);
			this.checkBoxShowCross.Name = "checkBoxShowCross";
			this.checkBoxShowCross.Size = new System.Drawing.Size(208, 24);
			this.checkBoxShowCross.TabIndex = 26;
			this.checkBoxShowCross.Text = "场景显示坐标系和原点";
			this.checkBoxShowCross.UseVisualStyleBackColor = true;
			this.checkBoxShowCross.CheckedChanged += new System.EventHandler(this.CheckBoxShowCrossCheckedChanged);
			// 
			// SimSetForm
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.BackColor = System.Drawing.Color.WhiteSmoke;
			this.ClientSize = new System.Drawing.Size(682, 253);
			this.Controls.Add(this.checkBoxShowCross);
			this.Controls.Add(this.label15);
			this.Controls.Add(this.label16);
			this.Controls.Add(this.checkBoxMainRefresh);
			this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
			this.MaximizeBox = false;
			this.MinimizeBox = false;
			this.Name = "SimSetForm";
			this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
			this.Text = "参数设置";
			this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.LocateFormFormClosing);
			this.ResumeLayout(false);
		}
		public System.Windows.Forms.CheckBox checkBoxShowCross;
		private System.Windows.Forms.Label label16;
		private System.Windows.Forms.Label label15;
		public System.Windows.Forms.CheckBox checkBoxMainRefresh;
	}
}
