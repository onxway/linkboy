﻿
using System;
using System.Drawing;
using System.Windows.Forms;
using n_LocatePanel;

namespace n_SimSetForm
{
/// <summary>
/// Description of LocateForm.
/// </summary>
public partial class SimSetForm : Form
{
	public SimSetForm()
	{
		//
		// The InitializeComponent() call is required for Windows Forms designer support.
		//
		InitializeComponent();
	}
	
	//运行
	public void Run()
	{
		this.Visible = true;
	}
	
	void LocateFormFormClosing(object sender, FormClosingEventArgs e)
	{
		this.Visible = false;
		e.Cancel = true;
	}
	
	void CheckBoxDisableHitCheckedChanged(object sender, EventArgs e)
	{
		G.SimBox.isAct = !checkBoxMainRefresh.Checked;
	}
	
	void CheckBoxShowCrossCheckedChanged(object sender, EventArgs e)
	{
		G.SimBox.SPanel.ShowCross = checkBoxShowCross.Checked;
	}
}
}

