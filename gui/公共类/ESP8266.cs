﻿
using System;
using System.Net.Sockets; 
using System.Net; 
using System.IO;
using System.Threading;
using System.Windows.Forms;

namespace n_ESP8266
{
public static class ESP8266
{
	public delegate void D_SendByte( byte b );
	public static D_SendByte d_SendByte;
	
	//ESP8266是否启用, 需要每次仿真编译时遍历模块列表， 有联网模块则开启， 否则关闭
	public static bool Enable;
	
	static TcpClient tcpClient;//声明
    static NetworkStream networkStream;//网络流
	
    static byte[] ReadBuffer;
	
	static byte[] NetBuffer;
	static int SendLen;
	static int NetBufferLen;
	
	static bool NeedSend;
	public static bool Connected;
	
	static Thread t;
	
	public static string Result;
	
	static string Web;
	static int Port;
	
	static int CMD;
	const int CMD_NONE = 0;
	const int CMD_CreatTCP = 1;
	
	static bool SimStatue;
	static bool TCPOK;
	
	//初始化
	public static void Init()
	{
		ReadBuffer = new byte[ 500 ];
		
		NetBuffer = new byte[ 500 ];
		NetBufferLen = 0;
		
		Connected = false;
		
		SimStatue = false;
		Enable = true;
		
		t = new Thread( new ThreadStart(ThreadProc) );
		t.Start();
	}
	
	//结束
	public static void MyClose()
	{
		try {
		if( t != null && t.IsAlive ) {
			t.Abort();
			t.Join();
			
			//MessageBox.Show( "已结束socket调试线程" );
		}
		}
		catch(Exception e) {
			MessageBox.Show( e.ToString() );
		}
		if( tcpClient != null && tcpClient.Connected ) {
			tcpClient.Close();
		}
	}
	
	//复位仿真模拟
	public static void DisableSim()
	{
		Enable = false;
	}
	
	//开启仿真模拟
	public static void OpenSim()
	{
		if( !Enable ) {
			return;
		}
		SimStatue = true;
		TCPOK = false;
	}
	
	//关闭仿真模拟
	public static void CloseSim()
	{
		if( !Enable ) {
			return;
		}
		SimStatue = false;
		CloseTCP();
		TCPOK = false;
	}
	
	//----------------------------------------------------------------
	
	//创建TCP连接
	public static void CreatTCP( string web, int port )
	{
		Web = web;
		Port = port;
		
		CMD = CMD_CreatTCP;
	}
	
	//关闭TCP连接
	public static void CloseTCP()
	{
		if( tcpClient != null ) {
			try {
				tcpClient.Close();
			}
			catch {
				
			}
			tcpClient = null;
		}
	}
	
	//用户发送数据到云平台
	public static void SentString( string ss )
	{
		byte[] byteArray = System.Text.Encoding.Default.GetBytes ( ss );
		for( int i = 0; i < byteArray.Length; ++i ) {
			NetBuffer[i] = byteArray[i];
		}
		SendLen = byteArray.Length;
		
		NeedSend = true;
	}
	
	//仿真时系统添加数据到云平台
	public static void AddByte( byte b )
	{
		NetBuffer[NetBufferLen] = b;
		NetBufferLen++;
		if( NetBufferLen >= NetBuffer.Length ) {
			NetBufferLen = 0;
		}
		
		if( NetBufferLen >= 2 ) {
			if( NetBuffer[NetBufferLen - 2] == '\r' && NetBuffer[NetBufferLen - 1] == '\n' ) {
				
				string haha = "";
				for( int i = 0; i < NetBufferLen; ++i ) {
					haha += ((char)NetBuffer[i]).ToString();
				}
				
				//这里是AT指令
				if( NetBuffer[0] == 'A' ) {
					
					if( haha.StartsWith( "AT+CIPSTART=" ) ) {
						CreatTCP( "www.bigiot.net", 8181 );
						
						//MessageBox.Show( "TCP" );
					}
				}
				else if( NetBuffer[0] == '+' && NetBuffer[1] == '+' && NetBuffer[2] == '+' ) {
					//...
				}
				else {
					
					//MessageBox.Show( "T" + haha );
					
					if( TCPOK ) {
						SendLen = NetBufferLen;
						NeedSend = true;
					}
				}
				NetBufferLen = 0;
			}
		}
	}
	
	//----------------------------------------------------------------
	
	//执行线程 - 数据收发
	static void ThreadProc()
	{
		while( true ) {
			try {
			if( !SimStatue ) {
				System.Threading.Thread.Sleep( 1000 );
				continue;
			}
			
			//判断是否需要创建套接字
			if( CMD == CMD_CreatTCP ) {
				CMD = CMD_NONE;
				
				if( tcpClient != null ) {
					try {
						tcpClient.Close();
					}
					catch {
						
					}
				}
				
				//if( tcpClient == null || !tcpClient.Connected ) {
					
					tcpClient = new TcpClient( Web, Port );
					
					if( tcpClient != null ) {
						networkStream = tcpClient.GetStream();//得到网络流
						Connected = true;
						TCPOK = true;
						//MessageBox.Show( "TCPOK" );
					}
					else {
						Connected = false;
						MessageBox.Show( "连接失败!" );
					}
				//}
			}
			
			//判断是否有需要发送的数据
			if( Connected && NeedSend ) {
				
				try {
					networkStream.Write( NetBuffer, 0, SendLen );
					
					Result += " >>> send: ";
					for( int i = 0; i < SendLen; i+=1 ) {
						Result += ((char)NetBuffer[i]).ToString();
					}
					//Result += "\n";
				}
				catch( Exception e ) {
					MessageBox.Show( "ESP8266 网络数据发送失败!\n" + e );
				}
				NeedSend = false;
			}
			//判断是否有需要接收的数据
			if( Connected && networkStream.DataAvailable ) {
				
				byte b = (byte)networkStream.ReadByte();
				
				if( d_SendByte != null ) {
					d_SendByte( b );
				}
				
				string rcMsg = ((char)b).ToString();
				
				if( rcMsg != null ) {
					Result += rcMsg;
				}
			}
			}
			catch {
				//MessageBox.Show( e.ToString() );
			}
		}
	}
}
}



