﻿
using System;
using System.Net.Sockets; 
using System.Net; 
using System.IO;
using System.Threading;
using System.Windows.Forms;

namespace n_LAPI
{
public static class LAPI
{
	public delegate void D_SendByte( byte b );
	public static D_SendByte d_SendByte;
	
	static TcpClient tcpClient;//声明
    static NetworkStream networkStream;//网络流
	public static bool Connected;
    
	//初始化
	public static void Init()
	{
		Connected = false;
	}
	
	//开始监听网络
	public static void Connect( string IP, int port )
	{
		//tcpClient = new TcpClient( "10.3.150.49", 8234 );
		//tcpClient = new TcpClient( "192.168.0.100", 8234 );
		
		tcpClient = new TcpClient( IP, port );
		if( tcpClient != null ) {
			networkStream = tcpClient.GetStream();//得到网络流
			networkStream.Flush();
			Connected = true;
		}
	}
	
	//开启仿真模拟
	public static void OpenSim()
	{
		
	}
	
	//关闭仿真模拟
	public static void CloseSim()
	{
		
	}
	
	//仿真时系统添加数据到云平台
	public static void AddByte( byte b )
	{
		if( Connected ) {
			networkStream.WriteByte( b );
			networkStream.Flush();
		}
	}
	
	//接收数据
	public static void ReceiveData()
	{
		if( Connected  ) {
			while( networkStream.DataAvailable ) {
				byte b = (byte)networkStream.ReadByte();
				if( d_SendByte != null ) {
					d_SendByte( b );
				}
			}
		}
	}
}
}



