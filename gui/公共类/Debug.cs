﻿
using System;

namespace n_Debug
{
//临时调试用的
public static class Debug
{
	public static string Message;
}

public static class Warning
{
	//始终存在的提示信息
	//用户需要进行处理才行, 但点击Delete键可以清空信息内容
	public static string ErrMessage;
	
	//弹出提示信息后, 点击鼠标左键可以关闭
	//阻止了用户的操作, 后续不需要处理也可
	public static string WarningMessage;
	
	//放置图形代码转化过程的错误提示信息, 每次转换会清零
	//用户需要进行处理才能解决此问题, 按下Delete键不会清空(系统会反复提示)
	public static string ClashMessage;
	
	//-----------------------------------------
	
	//软件BUG反馈信息, 将会提示用户进行反馈
	public static string BugMessage;
	
	//添加一个永久的错误信息
	public static void AddErrMessage( string mes )
	{
		if( ErrMessage == null ) {
			ErrMessage = "按下 Delete 键可删除本消息, 如需技术支持请到官网联系我们: www.linkboy.cc (版本：" + G.Version + ")\n";
		}
		ErrMessage += mes + "\n";
	}
	
	//添加一个必须要解决的冲突信息
	public static void AddClashMessage( string mes )
	{
		ClashMessage += mes + "\n";
	}
	
	//添加一个bug信息
	public static void BUG( string mes )
	{
		BugMessage += "-------------------------------\n";
		BugMessage += mes + "\n";
	}
}
}


