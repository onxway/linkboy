﻿
using System;

namespace n_CodeFormat
{
public static class CodeFormat
{
	public static string[] GetFunc( string s )
	{
		string line = s.Replace( '\t', ' ' );
		line = line.Replace( "(", " ( " );
		line = line.Replace( ")", " ) " );
		line = line.Replace( ";", " ; " );
		line = line.Replace( ",", " , " );
		line = line.Trim( ' ' );
		string[] word = line.Split( ' ' );
		string r = "";
		for( int i = 0; i < word.Length; ++i ) {
			if( word[i] != "" ) {
				r += word[i] + " ";
			}
		}
		return r.Trim( ' ' ).Split( ' ' );
	}
}
}





