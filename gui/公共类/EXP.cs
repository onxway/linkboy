﻿
//说明
//表达式中的词语以 @ 开始时表示组件名 高亮,生成表达式时候会自动去掉

namespace n_EXP
{
using System;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Windows.Forms;

using n_GUIset;
using n_MyObject;
using n_Shape;
using n_OS;
using n_MainSystemData;
using n_GUIcoder;
using n_ObjectMesPanel;

//表达式解析器
public class EXP
{
	public delegate void deleExpChanged();
	public deleExpChanged ExpChanged;
	
	public int X;
	public int Y;
	public int Width;
	public int Height;
	
	MyObject owner;
	Bitmap EXPImage;
	
	
	string v_strEXP;
	string strEXP {
		get {
			return v_strEXP;
		}
		set {
			v_strEXP = value;
			strEXP_Temp = value + " ";
		}
	}
	string strEXP_Temp;
	
	
	Graphics g;
	public ENote[] ExpTree;
	int Length;
	
	int MaxLevel;
	
	//模块名称两侧的空白
	const int ExpModuleWPadding = 0;
	
	//模块指令的指令第一个字符距离模块名称的空白
	const int ExpModuleInsWPadding = 5;
	
	const int ExpModuleHPadding = 1;
	const int ExpAllHPadding = 2;
	
	const int WordPadding = 0;
	
	//const int PerLevelHeight = 7;
	const int PerLevelHeight = 4;//3;
	
	//处于高亮状态, 鼠标移出后需要重新刷新一次(例如重新执行MouseMove)
	bool isHighted;
	
	int RefreshTick;
	
	int LastScale;
	bool LastFlash;
	
	bool ExistNullIns;
	
	
	//静态全局变量 - 是否需要翻译到英文
	public static bool Translate = false;
	public bool TranslateOK = false;
	
	//自由绘图背景 (用于颜色类的自动设置背景色)
	static SolidBrush ColorBrush;
	
	public static Brush WordBrush;
	static Pen WordPen;
	
	static Brush CModuleBrush;
	static Pen CModuleEagePen;
	
	static Brush VoidOperBrush;
	static Brush VoidOperBrush1;
	static Pen VoidOperPen;
	static Pen VoidOperPen1;
	
	static Brush ValueOperBrush;
	static Pen ValueOperPen;
	
	static Pen MouseOnPen;
	
	static Brush FillBrush0;
	static Pen FillPen0;
	static Brush FillBrush1;
	static Pen FillPen1;
	
	//当前指令所在的事件或者函数名
	public static string EFName;
	
	public const char U_SP = (char)1;
	
	//初始化
	public static void Init()
	{
		MouseOnPen = new Pen( Color.OrangeRed, 2 );
		
		ColorBrush = new SolidBrush( Color.Black );

		//CModuleBrush = new SolidBrush( Color.FromArgb(90, 110, 250) );
		CModuleBrush = new SolidBrush( Color.FromArgb(160, 200, 255) );
		CModuleEagePen = new Pen( Color.White );
		FillBrush0 = new SolidBrush( Color.White );
		FillPen0 = new Pen( Color.FromArgb(30, 60, 200) );
		//FillPen0 = new Pen( Color.CornflowerBlue );
		FillBrush1 = new SolidBrush( Color.White );
		FillPen1 = new Pen( Color.FromArgb(30, 80, 240) );
		
		//CModuleBrush = new SolidBrush( Color.FromArgb(55, 70, 190) );
		//CModuleEagePen = new Pen( Color.FromArgb(160, 200, 255) );
		//FillBrush0 = new SolidBrush( Color.FromArgb(90, 110, 250) );
		//FillPen0 = new Pen( Color.FromArgb(0, 0, 255) );
		//FillBrush1 = new SolidBrush( Color.FromArgb(90, 110, 250) );
		//FillPen1 = new Pen( Color.FromArgb(0, 0, 255) );
		
		//紫色运算符
		//VoidOperBrush = new SolidBrush( Color.FromArgb(190, 160, 230) );
		//VoidOperBrush1 = new SolidBrush( Color.FromArgb(170, 140, 210) );
		//VoidOperPen = new Pen( Color.FromArgb(150, 80, 140) );
		//VoidOperPen1 = new Pen( Color.FromArgb(120, 65, 120) );
		
		VoidOperBrush = new SolidBrush( Color.FromArgb(100, 200, 100) );
		VoidOperBrush1 = new SolidBrush( Color.FromArgb(125, 225, 125) );
		VoidOperPen = new Pen( Color.DarkGreen );
		VoidOperPen1 = new Pen( Color.DarkGreen );
		
		ValueOperBrush = new SolidBrush( Color.FromArgb(210, 120, 210) );
		ValueOperPen = new Pen( Color.FromArgb(150, 80, 40) );
		
		//WordBrush = new SolidBrush( Color.FromArgb(60, 170, 120) );
		WordBrush = new SolidBrush( Color.WhiteSmoke );
		WordPen = Pens.Black;
	}
	
	//构造函数
	public EXP( MyObject o, int x, int y )
	{
		owner = o;
		
		ExpTree = new ENote[ 80 ];
		Length = 0;
		isHighted = false;
		X = x;
		Y = y;
		Width = 0;
		Height = 0;
		
		ExistNullIns = false;
		
		RefreshTick = 0;
		
		LastScale = n_ImagePanel.ImagePanel.AScaleMid;
		LastFlash = false;
		
		//ValueMessage = null;
	}
	
	//绘图
	public void Draw( Graphics g, int StartX, int StartY )
	{
		if( owner.myObjectList != null ) {
			if( G.CGPanel != null && (LastFlash != n_ImagePanel.ImagePanel.Flash) && ExistNullIns ) {
				RefreshBitmap( isHighted );
				LastFlash = n_ImagePanel.ImagePanel.Flash;
			}
			if( LastScale != n_ImagePanel.ImagePanel.AScale ) {
				//ScaleAndRefresh();
				LastScale = n_ImagePanel.ImagePanel.AScale;
			}
			if( RefreshTick != n_ImagePanel.ImagePanel.RefreshTick ) {
				RefreshBitmap( false );
				RefreshTick = n_ImagePanel.ImagePanel.RefreshTick;
			}
		}
		g.DrawImage( EXPImage, StartX + owner.MidX + X, StartY + owner.MidY + Y, Width, Height );
		//g.DrawImage( EXPImage, StartX + owner.MidX + X, StartY + owner.MidY + Y );
		
		/*
		if( ValueMessage != null ) {
			n_SG.SG.MString.DrawAtLeft( ValueMessage, Color.White, 13, StartX + owner.MidX + X + MesENote.X, StartY + owner.MidY + Y + MesENote.Y - 20 );
		}
		*/
	}
	
	//鼠标按下事件
	public bool MouseClick( int mX, int mY )
	{
		mX = mX - this.X;
		mY = this.Y + this.Height - mY;
		
		string source = strEXP;
		bool isChanged = false;
		bool MouseOn = false;
		try {
		for( int i = Length - 1; i >= 0; --i ) {
			if( isMouseIn( mX, mY, ExpTree[ i ] ) ) {
				MouseOn = true;
				if( (ExpTree[ i ].GetExpType() == ENote.t_FLOW || ExpTree[ i ].GetExpType() == ENote.t_OPER || ExpTree[ i ].GetExpType() == ENote.t_OPERS) && ExpTree[ i ].GetOperType() == ENote.v_void ) {
					//return;
				}
				if( SystemData.isTouchMode ) {
					return MouseOn;
				}
				string CValue = null;
				if( ExpTree[ i ].WordList.Length >= 3 ) {
					for( int x = 2; x < ExpTree[ i ].WordList.Length; ++x ) {
						CValue += ExpTree[ i ].WordList[x] + " ";
					}
					CValue = CValue.Remove( CValue.Length - 1 );
				}
				//查找所在函数的返回值
				string ReturnType = null;
				n_MyIns.MyIns mi = (n_MyIns.MyIns)owner;
				mi = mi.GetHeadIns();
				if( mi is n_UserFunctionIns.UserFunctionIns ) {
					ReturnType = ((n_UserFunctionIns.UserFunctionIns)mi).RType;
				}
				if( mi is n_EventIns.EventIns ) {
					ReturnType = EXP.ENote.v_void;
				}
				
				G.commonEXPBox.cExp = this;
				G.commonEXPBox.cExpIndex = i;
				
				string s = ShowEXPEditor( ReturnType, ExpTree[ i ].GetOperType(), CValue, ExpTree[ i ].GetExpType() );
				
				
				/*//设置初始表达式
				string s = "";
				if( ExpTree[ i ].GetExpType() != ENote.t_NULL ) {
					s = JionEXP( ExpTree[ i ] );
				}
				else {
					s = "";
				}
				//显示表达式设置界面
				s = A.EXPBox.Run( s );
				*/
				
				//处理结果
				if( s != null ) {
					if( s == "" ) {
						s = ExpTree[ i ].GetOperType();
					}
					else {
						s = ExpTree[ i ].GetOperType() + " " + s;
					}
					ExpTree[ i ].WordList = new string[] { s };
					isChanged = true;
				}
				break;
			}
		}
		if( isChanged ) {
			strEXP = JionEXP( ExpTree[ 0 ] );
			Set( strEXP );
		}
		}catch(Exception e) {
			isChanged = false;
			strEXP = source;
			Set( strEXP );
			MessageBox.Show( "表达式错误: " + e.ToString() );
		}
		return MouseOn;
	}
	
	//鼠标移动事件
	public void MouseMove( bool visMouseOn, int mX, int mY )
	{
		if( Translate && !TranslateOK ) {
			TranslateOK = true;
			TranslateToEnglish();
		}
		
		
		//ValueMessage = null;
		
		if( !visMouseOn && !isHighted) {
			return;
		}
		mX = mX - this.X;
		mY = this.Y + this.Height - mY;
		
		for( int i = 0; i < Length; ++i ) {
			ExpTree[ i ].isMouseOn = false;
		}
		isHighted = false;
		for( int i = Length - 1; i >= 0; --i ) {
			if( isMouseIn( mX, mY, ExpTree[ i ] ) ) {
				isHighted = true;
				ExpTree[ i ].isMouseOn = true;
				
				/*
				if( i > 0 ) {
					MesENote = ExpTree[ i - 1 ];
					if( MesENote.WordList.Length > 2 && MesENote.WordList[1] == "@MFUNC" ) {
						string MName = MesENote.WordList[2];
						
					}
					ValueMessage = MName;
				}
				*/
				
				break;
			}
		}
		//很重要, isHighted 表示组件需要被刷新才能回到正常显示状态,
		//而 visMouseOn 的时候,显示的不是正常状态,需要刷新
		isHighted |= visMouseOn;
		
		RefreshBitmap( visMouseOn );
	}
	
	//设置表达式
	public void Set( string Exp )
	{
		isHighted = false;
		strEXP = Exp;
		Paser( Exp );
		
		ScaleAndRefresh();
		
		if( ExpChanged != null ) {
			ExpChanged();
		}
	}
	
	//判断是否引用了指定名字的变量
	public int UsedName( MyObject m )
	{
		if( m is n_GVar.GVar ) {
			if( strEXP_Temp.IndexOf( "@VAR " + m.Name + " " ) != -1 || strEXP_Temp.IndexOf( "@WORD " + m.Name + " " ) != -1 ) {
				return 1;
			}
		}
		if( m is n_HardModule.HardModule ) {
			if( strEXP_Temp.IndexOf( "@MFUNC " + m.Name + " " ) != -1 || strEXP_Temp.IndexOf( "@MVAR " + m.Name + " " ) != -1 ) {
				return 2;
			}
		}
		if( m is n_UserFunctionIns.UserFunctionIns ) {
			n_UserFunctionIns.UserFunctionIns ui = (n_UserFunctionIns.UserFunctionIns)m;
			bool isOK = true;
			//n_Debug.Debug.Message += "\n" + strEXP + "\n";
			for( int i = 0; i < ui.FNameCut.Length; ++i ) {
				//n_Debug.Debug.Message += ui.FNameCut[i] + " ";
				if( ui.FNameCut[i].StartsWith( "?" ) ) {
					i += 1;
					continue;
				}
				if( strEXP_Temp.IndexOf( ui.FNameCut[i] ) == -1 ) {
					isOK = false;
					break;
				}
			}
			if( isOK ) {
				return 3;
			}
		}
		return 0;
	}
	
	//===========================================================================
	
	//表达式从中文翻译到英文
	public void TranslateToEnglish()
	{
		strEXP = TranslateOneNote( ExpTree[ 0 ] );
		
		Set( strEXP );
	}
	
	//表达式从中文翻译到英文
	public string TranslateOneNote( ENote e )
	{
		if( e.isNull() ) {
			return e.GetOperType();
		}
		string Result = "";
		int L = e.WordList.Length;
		
		if( e.GetExpType() == ENote.t_MFUNC || e.GetExpType() == ENote.t_MVAR ) {
			
			n_MyFileObject.MyFileObject mf = (n_MyFileObject.MyFileObject)G.CGPanel.myModuleList.GetModuleFromName( e.WordList[ 2 ] );
			
			if( mf == null ) {
				MessageBox.Show( "NULL:" + e.WordList[ 2 ] );
				return e.GetOperType();
			}
			
			string insname = "";
			for( int i = 3; i < L; ++i ) {
				string w = e.WordList[ i ];
				if( w.StartsWith( "#" ) ) {
					insname += "_";
				}
				else {
					insname += w;
				}
			}
			string iname = mf.GetInnerName( insname );
			
			if( iname != null ) {
				for( int n = 0; n < mf.ElementList.Length; ++n ) {
					if( mf.ElementList[ n ][ 2 ] == iname ) {
						
						//收集形参信息
						string val = null;
						for( int i = 0; i < L; ++i ) {
							string w = e.WordList[ i ];
							if( w.StartsWith( "#" ) ) {
								val += w + " ";
								continue;
							}
						}
						string[] valist = null;
						if( val != null ) {
							valist = val.TrimEnd( ' ' ).Split( ' ' );
						}
						
						//分析目标指令结构, 进行形参替换
						Result = e.WordList[0] + " " + e.WordList[1] + " " + e.WordList[2] + " ";
						string[] cut = mf.ElementList[ n ][ 3 ].Replace( '+', ' ' ).Split( ' ' );
						
						int Index = 0;
						for( int i = 0; i < cut.Length; ++i ) {
							string w = cut[ i ];
							
							//如果是一个子项指针,显示子项
							if( w == "#" ) {
								int TargetIndex = int.Parse( valist[Index].Remove( 0, 1 ) );
								string SubEXP = TranslateOneNote( ExpTree[ TargetIndex ] );
								Result += "( " + SubEXP + " ) ";
								Index++;
								continue;
							}
							Result += w + " ";
						}
						break;
					}
				}
			}
			else {
				MessageBox.Show( "ERROR:" + insname );
			}
		}
		else {
			for( int i = 0; i < L; ++i ) {
				string w = e.WordList[ i ];
				
				//如果是一个子项指针,显示子项
				if( w.StartsWith( "#" ) ) {
					int TargetIndex = int.Parse( w.Remove( 0, 1 ) );
					string SubEXP = TranslateOneNote( ExpTree[ TargetIndex ] );
					Result += "( " + SubEXP + " ) ";
					continue;
				}
				Result += w + " ";
			}
		}
		
		return Result.TrimEnd( ' ' );
	}
	
	//===========================================================================
	
	public string Get()
	{
		return strEXP;
	}
	
	public static bool ExistUserFunc;
	public static string UserFuncName;
	public string GetExpCode()
	{
		ExistUserFunc = false;
		
		if( ExistNullIns ) {
			GUIcoder.SetErrorMes( owner, "指令或者表达式不能为空" );
		}
		string e = GetExpCodeForENote( ExpTree[ 0 ] );
		
		if( GUIcoder.CodePython ) {
			e = GUIcoder.GetPinYin( e );
		}
		return e;
	}
	
	string Tab;
	public string Py_GetExpCode( string tab )
	{
		if( ExistNullIns ) {
			GUIcoder.SetErrorMes( owner, "指令或者表达式不能为空" );
		}
		Tab = tab;
		return Py_GetExpCodeForENote( ExpTree[ 0 ] );
	}
	
	string GetExpCodeForENote( ENote e )
	{
		string Result = null;
		
		int L = e.WordList.Length;
		
		//判断是否用户指令调用
		if( e.GetExpType() == ENote.t_USER || e.GetExpType() == ENote.t_CODE ) {
			
			ExistUserFunc = true;
			UserFuncName = "";
			
			string VarList = "";
			for( int i = 2; i < L; ++i ) {
				string w = e.WordList[ i ];
				if( w.StartsWith( "#" ) ) {
					int TargetIndex = int.Parse( w.Remove( 0, 1 ) );
					string SubEXP = GetExpCodeForENote( ExpTree[ TargetIndex ] );
					string r = "(" + SubEXP + ")";
					VarList += r + ",";
					if( e.GetExpType() == ENote.t_USER ) {
						Result += "_";
						UserFuncName += "_";
					}
				}
				else {
					Result += w;
					UserFuncName += w;
				}
			}
			Result += "(" + VarList.TrimEnd( ',' ) + ")";
		}
		//判断是否流程控制
		else if( e.GetExpType() == ENote.t_FLOW ) {
			if( e.WordList[2] == "返回" || e.WordList[2] == "return" ) {
				Result += "return";
			}
			else if( e.WordList[2] == "退出循环"|| e.WordList[2] == "break"  ) {
				Result += "break";
			}
			else if( e.WordList[2] == "继续循环"|| e.WordList[2] == "continue"  ) {
				Result += "continue";
			}
			else {
				Result += "<" + e.WordList[2] + ">";
			}
			if( L > 3 ) {
				string w = e.WordList[ 3 ];
				if( w.StartsWith( "#" ) ) {
					int TargetIndex = int.Parse( w.Remove( 0, 1 ) );
					string SubEXP = GetExpCodeForENote( ExpTree[ TargetIndex ] );
					Result += " " + SubEXP;
				}
			}
		}
		//判断是否模块指令调用
		else if( e.GetExpType() == ENote.t_MFUNC ) {
			string VarList = "";
			Result += e.WordList[ 2 ] + ".";
			for( int i = 3; i < L; ++i ) {
				string w = e.WordList[ i ];
				if( w.StartsWith( "#" ) ) {
					int TargetIndex = int.Parse( w.Remove( 0, 1 ) );
					string SubEXP = GetExpCodeForENote( ExpTree[ TargetIndex ] );
					string r = "(" + SubEXP + ")";
					VarList += r + ",";
					
					if( G.CGPanel != null ) {
						
						string MName = e.WordList[ 2 ];
						if( MName.StartsWith( "$" ) ) {
							MName = n_GroupList.Group.GetFullName( owner.GroupMes, MName );
						}
						MyObject mo = G.CGPanel.myModuleList.GetModuleFromName( MName );
						if( mo != null ) {
							if( mo is n_MyFileObject.MyFileObject ) {
								n_MyFileObject.MyFileObject mf = (n_MyFileObject.MyFileObject)mo;
								if( mf.Language != n_Language.Language.Mod_c ) {
									Result += "_";
								}
							}
							else {
								string err = "程序错误: 变量 [" + MName + "] 被当成模块来使用 (可能是变量命名和模块一样导致的), 重新编辑当前指令即可";
								n_Debug.Warning.AddClashMessage( err );
								
								GUIcoder.SetErrorMes( owner, err );
							}
						}
					}
					else {
						Result += "_";
					}
				}
				else {
					Result += w;
				}
			}
			Result += "(" + VarList.TrimEnd( ',' ) + ")";
		}
		//判断是否运算指令
		else if( e.GetExpType() == ENote.t_OPER ) {
			for( int i = 2; i < L; ++i ) {
				string w = e.WordList[ i ];
				if( w.StartsWith( "#" ) ) {
					int TargetIndex = int.Parse( w.Remove( 0, 1 ) );
					string SubEXP = GetExpCodeForENote( ExpTree[ TargetIndex ] );
					w = "(" + SubEXP + ")";
				}
				else {
					w = w.Replace( "绝对值", "+" );
					w = w.Replace( "取负数", "-" );
					
					//w = w.Replace( "+=", "+" );
					//w = w.Replace( "-=", "-" );
					//w = w.Replace( "*=", "*" );
					//w = w.Replace( "/=", "/" );
					//w = w.Replace( "%=", "%" );
					
					if( e.GetOperType() == "?bool" && w == "=" ) {
						w = "==";
					}
					if( w == "≠" ) {
						w = "!=";
					}
					if( w == "≤" ) {
						w = "<=";
					}
					if( w == "≥" ) {
						w = ">=";
					}
					if( n_GUIcoder.GUIcoder.CodePython ) {
						w = w.Replace( "不成立", "not" );
						w = w.Replace( "非", "not" );
						w = w.Replace( "并且", "and" );
						w = w.Replace( "或者", "or" );
					}
					else {
						w = w.Replace( "不成立", "!" );
						w = w.Replace( "非", "!" );
						w = w.Replace( "并且", "&&" );
						w = w.Replace( "或者", "||" );
					}
				}
				Result += w;
			}
		}
		//判断是否特殊运算指令
		else if( e.GetExpType() == ENote.t_OPERS ) {
			
			string w1 = e.WordList[ 2 ];
			int TargetIndex1 = int.Parse( w1.Remove( 0, 1 ) );
			string SubEXP1 = GetExpCodeForENote( ExpTree[ TargetIndex1 ] );
			
			string w2 = e.WordList[ 4 ];
			int TargetIndex2 = int.Parse( w2.Remove( 0, 1 ) );
			string SubEXP2 = GetExpCodeForENote( ExpTree[ TargetIndex2 ] );
			
			string op = e.WordList[ 3 ];
			if( op == "==" ) {
				Result += "sys_MySString.equal((" + SubEXP1 + "),(" + SubEXP2 + "))";
			}
			else if( op == "!=" ) {
				Result += "(!sys_MySString.equal((" + SubEXP1 + "),(" + SubEXP2 + ")))";
			}
			else {
				MessageBox.Show( "<GetExpCodeForENote> 异常:" + op );
			}
			GUIcoder.LoadStringLib = true;
		}
		//判断是否模块变量, 注意这里暂时未考虑数组下标的判断
		else if( e.GetExpType() == ENote.t_MVAR ) {
			Result += e.WordList[ 2 ] + ".";
			for( int i = 3; i < L; ++i ) {
				string w = e.WordList[ i ];
				//注意这里需要根据类型,可能改为数组下标
				Result += w + ".";
			}
			Result = Result.TrimEnd( '.' );
		}
		//判断是否终结词列表等, 注意这里暂时未考虑数组下标的判断
		else if( e.GetExpType() == ENote.t_VAR ) {
			Result = e.WordList[ 2 ];
		}
		//判断是否为系统指令
		else if( e.GetExpType() == ENote.t_SYS ) {
			
			string s = e.WordList[ 2 ];
			
			if( s == "启用" || s == "禁用" || s == "触发" || s == "结束" ) {
				if( s == "启用" ) {
					Result = e.WordList[ 3 ] + n_CoroCompiler.CoroCompiler.E_En + " = true";
				}
				else if( s == "禁用" ) {
					Result = e.WordList[ 3 ] + n_CoroCompiler.CoroCompiler.E_En + " = false";
				}
				else if( s == "触发" ) {
					Result = "run_mode1 " + e.WordList[ 3 ] + n_CoroCompiler.CoroCompiler.E_Flag + " = 1";
					//run语句会增加函数调用, 不好
					//Result = "run " + e.WordList[ 3 ] + "()";
				}
				//这里是强制结束事件
				else {
					//带有调度器版本
					//Result = "OS0.DeleteTask( &" + e.WordList[ 3 ] + " )";
					Result = e.WordList[ 3 ] + n_CoroCompiler.CoroCompiler.E_Flag + " = 0";
				}
			}
			else {
				MessageBox.Show( "<GetExpCodeForENote> 异常类型" );
			}
		}
		//判断是否终结词列表等, 注意这里暂时未考虑数组下标的判断
		else if( e.GetExpType() == ENote.t_WORD ) {
			
			string s = e.WordList[ 2 ];
			
			//判断是否为协议解析器
			if( s.StartsWith( "[" ) ) {
				Result += "v_Protocol" + GUIcoder.ProtocalNumber + " ";
				GUIcoder.DefineCode += "[#.code uint32*?] v_Protocol" + GUIcoder.ProtocalNumber + " = " + s.Replace( "[", "{" ).Replace( "]", "}" ) + ";\n";
				GUIcoder.ProtocalNumber++;
			}
			//判断是否为颜色
			else if( s.StartsWith( "{" ) ) {
				string aaa = s.Substring( 1, 2 );
				string rrr = s.Substring( 4, 2 );
				string ggg = s.Substring( 6, 2 );
				string bbb = s.Substring( 8, 2 );
				long aa = int.Parse( aaa, System.Globalization.NumberStyles.HexNumber );
				long rr = int.Parse( rrr, System.Globalization.NumberStyles.HexNumber );
				long gg = int.Parse( ggg, System.Globalization.NumberStyles.HexNumber );
				long bb = int.Parse( bbb, System.Globalization.NumberStyles.HexNumber );
				long d = aa * 256 * 256 * 256 + rr * 256 * 256 + gg * 256 + bb;
				if( d > 0x7FFFFFFF ) {
					d -= 0x100000000;
				}
				Result = d.ToString();
			}
			else {
				//判断是否为字符串 - 普通
				if( s.StartsWith( "@" ) ) {
					s = s.Remove( 0, 1 );
				}
				if( n_GUIcoder.GUIcoder.CodePython ) {
					if( s == "是" || s == "真" ) {
						s = "True";
					}
					if( s == "否" || s == "假" ) {
						s = "False";
					}
				}
				else {
					if( s == "是" || s == "真" ) {
						s = "true";
					}
					if( s == "否" || s == "假" ) {
						s = "false";
					}
				}
				//这里为何要加上s??? 暂时不明白
				//明白了, 这里是由于目前不支持 int8/16 隐式转换到 fix (延时器延时秒函数), 所以先设置为int32类型
				if( s.Length > 0 && (s[0] == '-' || s[0] >= '0' && s[0] <= '9' ) && s.IndexOf( "." ) == -1 ) {
					Result += s + "s ";
				}
				else {
					Result += s + " ";
				}
			}
			
			Result = Result.Replace( U_SP, ' ' );
			
			for( int i = 3; i < L; ++i ) {
				string w = e.WordList[ i ];
				
				//注意这里需要根据类型,可能改为数组下标
				Result += w + " ";
			}
			Result = Result.TrimEnd( ' ' );
		}
		else if( e.GetExpType() == ENote.t_NULL ) {
			//n_Debug.Warning.AddClashMessage( "请将表达式编辑完整, 补全所有参数" );
			Result += e.GetExpType() + " ";
		}
		else {
			n_Debug.Warning.BUG( "表达式格式不正确, 未知的表达式类型: " + e.GetExpType() );
			Result += e.GetExpType() + " ";
		}
		return Result;
	}
	
	string Py_GetExpCodeForENote( ENote e )
	{
		string Result = null;
		
		int L = e.WordList.Length;
		
		//判断是否用户指令调用
		if( e.GetExpType() == ENote.t_USER || e.GetExpType() == ENote.t_CODE ) {
			string VarList = "";
			for( int i = 2; i < L; ++i ) {
				string w = e.WordList[ i ];
				if( w.StartsWith( "#" ) ) {
					int TargetIndex = int.Parse( w.Remove( 0, 1 ) );
					string SubEXP = Py_GetExpCodeForENote( ExpTree[ TargetIndex ] );
					string r = "(" + SubEXP + ")";
					VarList += r + ",";
					Result += "_";
				}
				else {
					Result += w;
				}
			}
			Result += "(" + VarList.TrimEnd( ',' ) + ")";
		}
		//判断是否流程控制
		else if( e.GetExpType() == ENote.t_FLOW ) {
			if( e.WordList[2] == "返回" || e.WordList[2] == "return" ) {
				Result += "return";
			}
			else if( e.WordList[2] == "退出循环"|| e.WordList[2] == "break"  ) {
				Result += "break";
			}
			else if( e.WordList[2] == "继续循环"|| e.WordList[2] == "continue"  ) {
				Result += "continue";
			}
			else {
				Result += "<" + e.WordList[2] + ">";
			}
			if( L > 3 ) {
				string w = e.WordList[ 3 ];
				if( w.StartsWith( "#" ) ) {
					int TargetIndex = int.Parse( w.Remove( 0, 1 ) );
					string SubEXP = Py_GetExpCodeForENote( ExpTree[ TargetIndex ] );
					Result += " " + SubEXP;
				}
			}
		}
		//判断是否模块指令调用
		else if( e.GetExpType() == ENote.t_MFUNC ) {
			string VarList = "";
			Result += e.WordList[ 2 ] + "_";
			for( int i = 3; i < L; ++i ) {
				string w = e.WordList[ i ];
				if( w.StartsWith( "#" ) ) {
					int TargetIndex = int.Parse( w.Remove( 0, 1 ) );
					string SubEXP = Py_GetExpCodeForENote( ExpTree[ TargetIndex ] );
					string r = "(" + SubEXP + ")";
					VarList += r + ",";
					
					/*
					if( G.CGPanel != null ) {
						
						string MName = e.WordList[ 2 ];
						if( MName.StartsWith( "$" ) ) {
							MName = n_GroupList.Group.GetFullName( owner.GroupMes, MName );
						}
						MyObject mo = G.CGPanel.myModuleList.GetModuleFromName( MName );
						if( mo != null ) {
							n_MyFileObject.MyFileObject mf = (n_MyFileObject.MyFileObject)mo;
							if( mf.Language != n_Language.Language.Mod_c ) {
								Result += "_";
							}
						}
					}
					else {
						Result += "_";
					}
					*/
				}
				else {
					Result += w;
				}
			}
			Result += "(" + VarList.TrimEnd( ',' ) + ")";
			
			if( Result.IndexOf( "_延时" ) != -1 ) {
				Result += "\n" + Tab + EFName + "_sys_time = " + e.WordList[ 2 ] + "_time\n"+ Tab + "data = yield";
			}
		}
		//判断是否运算指令
		else if( e.GetExpType() == ENote.t_OPER ) {
			for( int i = 2; i < L; ++i ) {
				string w = e.WordList[ i ];
				if( w.StartsWith( "#" ) ) {
					int TargetIndex = int.Parse( w.Remove( 0, 1 ) );
					string SubEXP = Py_GetExpCodeForENote( ExpTree[ TargetIndex ] );
					w = "(" + SubEXP + ")";
				}
				else {
					w = w.Replace( "绝对值", "+" );
					w = w.Replace( "取负数", "-" );
					
					//w = w.Replace( "+=", "+" );
					//w = w.Replace( "-=", "-" );
					//w = w.Replace( "*=", "*" );
					//w = w.Replace( "/=", "/" );
					//w = w.Replace( "%=", "%" );
					
					if( e.GetOperType() == "?bool" && w == "=" ) {
						w = "==";
					}
					if( w == "≠" ) {
						w = "!=";
					}
					if( w == "≤" ) {
						w = "<=";
					}
					if( w == "≥" ) {
						w = ">=";
					}
					if( n_GUIcoder.GUIcoder.CodePython ) {
						w = w.Replace( "不成立", "not" );
						w = w.Replace( "非", "not" );
						w = w.Replace( "并且", "and" );
						w = w.Replace( "或者", "or" );
					}
					else {
						w = w.Replace( "不成立", "!" );
						w = w.Replace( "非", "!" );
						w = w.Replace( "并且", "&&" );
						w = w.Replace( "或者", "||" );
					}
				}
				Result += w;
			}
		}
		//判断是否特殊运算指令
		else if( e.GetExpType() == ENote.t_OPERS ) {
			
			string w1 = e.WordList[ 2 ];
			int TargetIndex1 = int.Parse( w1.Remove( 0, 1 ) );
			string SubEXP1 = Py_GetExpCodeForENote( ExpTree[ TargetIndex1 ] );
			
			string w2 = e.WordList[ 4 ];
			int TargetIndex2 = int.Parse( w2.Remove( 0, 1 ) );
			string SubEXP2 = Py_GetExpCodeForENote( ExpTree[ TargetIndex2 ] );
			
			string op = e.WordList[ 3 ];
			if( op == "==" ) {
				Result += "sys_string_lib.equal((" + SubEXP1 + "),(" + SubEXP2 + "))";
			}
			else if( op == "!=" ) {
				Result += "(!sys_string_lib.equal((" + SubEXP1 + "),(" + SubEXP2 + ")))";
			}
			else {
				MessageBox.Show( "<GetExpCodeForENote> 异常:" + op );
			}
			GUIcoder.LoadStringLib = true;
		}
		//判断是否模块变量, 注意这里暂时未考虑数组下标的判断
		else if( e.GetExpType() == ENote.t_MVAR ) {
			Result += e.WordList[ 2 ] + ".";
			for( int i = 3; i < L; ++i ) {
				string w = e.WordList[ i ];
				//注意这里需要根据类型,可能改为数组下标
				Result += w + ".";
			}
			Result = Result.TrimEnd( '.' );
		}
		//判断是否终结词列表等, 注意这里暂时未考虑数组下标的判断
		else if( e.GetExpType() == ENote.t_VAR ) {
			Result = e.WordList[ 2 ];
		}
		//判断是否为系统指令
		else if( e.GetExpType() == ENote.t_SYS ) {
			
			string s = e.WordList[ 2 ];
			
			if( s == "启用" || s == "禁用" || s == "触发" || s == "结束" ) {
				if( s == "启用" ) {
					Result = e.WordList[ 3 ] + n_CoroCompiler.CoroCompiler.E_En + " = true";
				}
				else if( s == "禁用" ) {
					Result = e.WordList[ 3 ] + n_CoroCompiler.CoroCompiler.E_En + " = false";
				}
				else if( s == "触发" ) {
					Result = e.WordList[ 3 ] + n_CoroCompiler.CoroCompiler.E_Flag + " = 1";
				}
				//这里是强制结束事件
				else {
					//带有调度器版本
					//Result = "OS0.DeleteTask( &" + e.WordList[ 3 ] + " )";
					Result = e.WordList[ 3 ] + n_CoroCompiler.CoroCompiler.E_Flag + " = 0";
				}
			}
			else {
				MessageBox.Show( "<GetExpCodeForENote> 异常类型" );
			}
		}
		//判断是否终结词列表等, 注意这里暂时未考虑数组下标的判断
		else if( e.GetExpType() == ENote.t_WORD ) {
			
			string s = e.WordList[ 2 ];
			
			if( s.StartsWith( "[" ) ) {
				Result += "v_Protocol" + GUIcoder.ProtocalNumber + " ";
				GUIcoder.DefineCode += "[#.code uint32*?] v_Protocol" + GUIcoder.ProtocalNumber + " = " + s + ";\n";
				GUIcoder.ProtocalNumber++;
			}
			else {
				
				if( n_GUIcoder.GUIcoder.CodePython ) {
					if( s == "是" || s == "真" ) {
						s = "True";
					}
					if( s == "否" || s == "假" ) {
						s = "False";
					}
				}
				else {
					if( s == "是" || s == "真" ) {
						s = "true";
					}
					if( s == "否" || s == "假" ) {
						s = "false";
					}
				}
				//这里为何要加上s??? 暂时不明白
				//明白了, 这里是由于目前不支持 int8/16 隐式转换到 fix (延时器延时秒函数), 所以先设置为int32类型
				if( s.Length > 0 && (s[0] == '-' || s[0] >= '0' && s[0] <= '9' ) && s.IndexOf( "." ) == -1 ) {
					Result += s + " "; //"s "
				}
				else {
					Result += s + " ";
				}
			}
			for( int i = 3; i < L; ++i ) {
				string w = e.WordList[ i ];
				
				//注意这里需要根据类型,可能改为数组下标
				Result += w + " ";
			}
			Result = Result.TrimEnd( ' ' );
		}
		else if( e.GetExpType() == ENote.t_NULL ) {
			//n_Debug.Warning.AddClashMessage( "请将表达式编辑完整, 补全所有参数" );
			Result += e.GetExpType() + " ";
		}
		else {
			n_Debug.Warning.BUG( "表达式格式不正确, 未知的表达式类型: " + e.GetExpType() );
			Result += e.GetExpType() + " ";
		}
		return Result;
	}
	
	//更新重命名信息
	public void RefreshRename()
	{
		strEXP = JionEXP( ExpTree[ 0 ] );
		Set( strEXP );
	}
	
	bool isMouseIn( int mX, int mY, ENote e )
	{
		mX -= 1;
		mY -= 1;
		if( mX >= e.X && mX < e.X + e.Width && mY <= e.Y + e.Height && mY > e.Y ) {
			return true;
		}
		return false;
	}
	
	//更新图片分辨率并重新刷新
	void ScaleAndRefresh()
	{
		//根据当前放缩设置图片 - 过时
		int tA = n_ImagePanel.ImagePanel.AScale;
		
		//按照稍微大一些的尺寸 (表达式编辑中则按照原尺寸)
		if( !(owner is n_FuncIns.FuncIns && ((n_FuncIns.FuncIns)owner).isExpEditor ) ) {
			tA = n_ImagePanel.ImagePanel.AScaleMid * 20 / 10;
		}
		
		int w = (Width)*tA/n_ImagePanel.ImagePanel.AScaleMid;
		int h = (Height)*tA/n_ImagePanel.ImagePanel.AScaleMid;
		//int w = Width + 1;
		//int h = Height + 1;
		
		EXPImage = new Bitmap( w, h );
		g = Graphics.FromImage( EXPImage );
		g.SmoothingMode = SmoothingMode.HighQuality;
		
		g.ScaleTransform( (float)tA/n_ImagePanel.ImagePanel.AScaleMid, (float)tA/n_ImagePanel.ImagePanel.AScaleMid );
		
		RefreshBitmap( false );
	}
	
	//刷新一次图片
	void RefreshBitmap( bool visMouseOn )
	{
		g.Clear( Color.Transparent );
		ExistNullIns = false;
		ShowOne( visMouseOn, ExpTree[ 0 ], 1, Height-1 );
	}
	
	void ShowOne( bool visMouseOn, ENote e, int x, int y )
	{
		//x += 1;
		//y += 1;
		
		//绘制底色
		Rectangle r = new Rectangle( x + e.X, y - e.Y - e.Height, e.Width, e.Height );
		GraphicsPath gp = Shape.CreateRoundedRectanglePath( r );
		
		Pen ModuleEagePen = CModuleEagePen;
		
		Brush ModtextBrush;
		Brush textBrush;
		Brush FillBrush;
		Pen DrawPen;
		Brush ModuleBrush = CModuleBrush;
		//Brush ModuleBrush = Brushes.DarkGreen;
		
		if( SystemData.isBlack ) {
			textBrush = Brushes.Black;
			ModtextBrush = Brushes.Black;
		}
		else {
			textBrush = Brushes.Black;
			ModtextBrush = Brushes.Black;
		}
		
		//分级显示
		if( e.Level % 2 == 0 ) {
			FillBrush = FillBrush0;
			DrawPen = FillPen0;
		}
		else {
			FillBrush = FillBrush1;
			DrawPen = FillPen1;
		}
		//if( !GUISystemData.isBlack ) {
		//	DrawPen = Pens.DimGray;
		//}
		//根据项目类型设置颜色
		
		if( !SystemData.isBlack ) {
			if( e.GetExpType() == ENote.t_OPER || e.GetExpType() == ENote.t_OPERS ) {
				if( e.Level % 2 != 0 ) {
					DrawPen = VoidOperPen;
					FillBrush = VoidOperBrush;
				}
				else {
					DrawPen = VoidOperPen1;
					FillBrush = VoidOperBrush1;
				}
			}
		}
		if( e.GetExpType() == ENote.t_WORD ) {
			//textBrush = Brushes.DarkBlue;
			//FillBrush = GUIset.WordBrush;
			//DrawPen = GUIset.WordPen;
			
			DrawPen = WordPen;
			FillBrush = WordBrush;
			
			string word = e.WordList[ 2 ];
			
			//判断是否为颜色
			if( word.StartsWith( "{" ) ) {
				string aaa = word.Substring( 1, 2 );
				string rrr = word.Substring( 4, 2 );
				string ggg = word.Substring( 6, 2 );
				string bbb = word.Substring( 8, 2 );
				int aa = int.Parse( aaa, System.Globalization.NumberStyles.HexNumber );
				int rr = int.Parse( rrr, System.Globalization.NumberStyles.HexNumber );
				int gg = int.Parse( ggg, System.Globalization.NumberStyles.HexNumber );
				int bb = int.Parse( bbb, System.Globalization.NumberStyles.HexNumber );
				if( aa == 0xFF ) {
					ColorBrush.Color = Color.WhiteSmoke;
				}
				else {
					ColorBrush.Color = Color.FromArgb( rr, gg ,bb );
					if( rr + gg + bb < 128 * 3 ) {
						textBrush = Brushes.WhiteSmoke;
					}
				}
				FillBrush = ColorBrush;
			}
			else if( word.StartsWith( "\"" ) ) {
				//DrawPen = Pens.DarkTurquoise;
				//FillBrush = WordBrush;
				
				textBrush = Brushes.DarkCyan;
				DrawPen = Pens.DarkCyan;
				FillBrush = WordBrush;
			}
			else {
				//...
			}
		}
		else if( e.GetExpType() == ENote.t_VAR ) {
			//textBrush = Brushes.DarkBlue;
			//FillBrush = GUIset.WordBrush;
			//DrawPen = GUIset.WordPen;
			
			DrawPen = Pens.Green;
			if( e.GetOperType() == ENote.v_int32 || e.GetOperType() == "*" + GType.g_int32 ) {
				FillBrush = Brushes.YellowGreen;
			}
			else if( e.GetOperType() == ENote.v_fix || e.GetOperType() == "*" + GType.g_fix ) {
				FillBrush = Brushes.PaleTurquoise;
				DrawPen = Pens.Teal;
			}
			else if( e.GetOperType() == ENote.v_bool || e.GetOperType() == "*" + GType.g_bool ) {
				FillBrush = Brushes.DarkKhaki;
				DrawPen = Pens.Olive;
			}
			else {
				FillBrush = WordBrush;
			}
			DrawPen = WordPen;
			FillBrush = WordBrush;
		}
		else if( e.GetExpType() == ENote.t_SYS ) {
			//textBrush = Brushes.DarkBlue;
			//FillBrush = GUIset.WordBrush;
			//DrawPen = GUIset.WordPen;
			FillBrush = Brushes.BurlyWood;
			DrawPen = Pens.Peru;
		}
		else if( (e.GetExpType() == ENote.t_MFUNC || e.GetExpType() == ENote.t_MVAR) ) {
			FillBrush = ModuleBrush;
			DrawPen = FillPen0;
			//FillBrush = Brushes.WhiteSmoke;
		}
		else if( e.GetExpType() == ENote.t_USER ) {
			DrawPen = Pens.Teal;
			if( SystemData.isBlack ) {
				FillBrush = Brushes.DarkSeaGreen;
			}
			else {
				FillBrush = n_MyIns.MyIns.UserBackColor;
				DrawPen = Pens.Purple;
				textBrush = Brushes.Black;
			}
		}
		else if( e.GetExpType() == ENote.t_CODE ) {
			DrawPen = Pens.Teal;
			if( SystemData.isBlack ) {
				FillBrush = Brushes.DarkSeaGreen;
			}
			else {
				FillBrush = Brushes.PaleTurquoise;
				DrawPen = Pens.LightSeaGreen;
				textBrush = Brushes.Black;
			}
		}
		//这里不要加else, 需要独立附加判断是否改变颜色
		if( (e.isNull() || strEXP == n_Language.Language.ModuleFunc ) && !e.isMouseOn ) {
			
			//这里可能是判断当前指令是否位于指令列表选择面板中
			if( owner.myObjectList != null ) {
				if( n_ImagePanel.ImagePanel.Flash ) {
					textBrush = Brushes.Black;
					DrawPen = Pens.Red;
					FillBrush = Brushes.Orange;
					
					//这是一个临时加的, 设置空指令的模块背景颜色
					//ModuleBrush = Brushes.Gray;
					//ModuleEagePen = Pens.Red;
					//ModtextBrush = Brushes.White;
				}
			}
			else {
				textBrush = Brushes.Black;
				DrawPen = Pens.Gray;
				FillBrush = Brushes.WhiteSmoke;
			}
		}
		else {
			//...
		}
		
		//如果是不可更改的类型, 则不响应鼠标位置
		if( e.GetExpType() == ENote.t_FLOW ) {
			
			//如果为高亮
			if( owner.myObjectList != null && e.isMouseOn ) {
				FillBrush = Brushes.Gainsboro;
				DrawPen = Pens.Black;
				textBrush = Brushes.White;
			}
			else {
				FillBrush = Brushes.PaleTurquoise;
				DrawPen = Pens.Teal;
				textBrush = Brushes.Black;
			}
		}
		else {
			//如果为高亮
			if( owner.myObjectList != null && visMouseOn ) {
				textBrush = Brushes.Gray;
				ModtextBrush = Brushes.Gray;
				FillBrush = Brushes.White;
				DrawPen = Pens.DimGray;
				ModuleBrush = Brushes.White;
				ModuleEagePen = Pens.Silver;
			}
			//高亮显示选中的元素
			if( owner.myObjectList != null && e.isMouseOn ) {
				textBrush = Brushes.Black;
				ModtextBrush = Brushes.Black;
				ModuleBrush = Brushes.MediumSeaGreen;
				ModuleEagePen = Pens.MediumSeaGreen;
				FillBrush = Brushes.Gainsboro;
				DrawPen = Pens.Black;
			}
		}
		//判断是否为无效模式
		bool isNote = false;
		if( owner is n_MyIns.MyIns && ((n_MyIns.MyIns)owner).isNote ) {
			isNote = true;
		}
		
		if( isNote || owner.myObjectList == null && owner.Name == "(无效)" ) {
			/*
			FillBrush = Brushes.Silver;
			DrawPen = Pens.Gray;
			textBrush = Brushes.DimGray;
			ModuleBrush = Brushes.Silver;
			ModuleEagePen = Pens.Gainsboro;
			ModtextBrush = Brushes.DimGray;
			*/
			FillBrush = Brushes.WhiteSmoke;
			DrawPen = Pens.Gainsboro;
			textBrush = Brushes.Silver;
			ModuleBrush = Brushes.Gainsboro;
			ModuleEagePen = Pens.Gainsboro;
			ModtextBrush = Brushes.Silver;
		}
		
		//注意这里临时调节文字的位置, 相对于边框向下平移若干微小单位
		int OffY = 0;
		
		g.FillPath( FillBrush, gp );
		g.DrawPath( DrawPen, gp );
		if( strEXP == n_Language.Language.ModuleFunc ) {
			ExistNullIns = true;
		}
		if( e.isNull() ) {
			string mes = e.WordList[ 0 ];
			if( mes == "?void" ) {
				mes = n_Language.Language.NewIns;
			}
			else if( mes == "?base" ) {
				mes = n_Language.Language.BaseType;
			}
			else if( mes == "?bool" || mes == "*bool" ) {
				mes = n_Language.Language.BoolType;
			}
			else if( mes == "?int32" || mes == "*int32" ) {
				mes = n_Language.Language.IntType;
			}
			else if( mes == "?fix" || mes == "*fix" ) {
				mes = n_Language.Language.FixType;
			}
			else if( mes == "?float" ) {
				mes = n_Language.Language.FloatType;
			}
			else if( mes == "?module" ) {
				mes = n_Language.Language.RoleType;
			}
			else {
				//...
			}
			g.DrawString( mes, GUIset.ExpFont, textBrush, x + e.X, y - e.Y - e.Height + ExpAllHPadding + OffY );
			ExistNullIns = true;
			return;
		}
		//绘制短语列表
		int L = e.WordList.Length;
		int vWidth = 0;
		for( int i = 2; i < L; ++i ) {
			string word = e.WordList[ i ];
			//如果是一个子项指针,显示子项
			if( word.StartsWith( "#" ) ) {
				int TargetIndex = int.Parse( word.Remove( 0, 1 ) );
				ShowOne( visMouseOn, ExpTree[ TargetIndex ], x, y );
				vWidth += ExpTree[ TargetIndex ].Width;
				continue;
			}
			//如果是组件开头,则特殊显示
			int w = 0;
			if( i == 2 && ( e.GetExpType() == ENote.t_MFUNC || e.GetExpType() == ENote.t_MVAR ) ) {
				
				int MWidth = (int)GUIset.mg.MeasureString( word, GUIset.ExpFont ).Width + 2 * ExpModuleWPadding + ExpModuleInsWPadding;
				w = (ExpAllHPadding - ExpModuleHPadding) + MWidth;
				
				int ExpHeight = GUIset.ExpFont.Height;
				Rectangle rr = new Rectangle( x + e.X + ExpAllHPadding - ExpModuleHPadding, y - e.Y - e.Height + ExpAllHPadding - ExpModuleHPadding, MWidth, ExpHeight + 2 * ExpModuleHPadding );
				GraphicsPath gp1 = Shape.CreateRoundedRectanglePath( rr );
				//g.FillPath( ModuleBrush, gp1 );
				//g.DrawPath( Pens.White, gp1 );
				
				int shx = x + e.X + ExpAllHPadding - ExpModuleHPadding + MWidth - (2 * ExpModuleWPadding + ExpModuleInsWPadding)/2 - 1;
				int shy0 = y - e.Y - e.Height + ExpAllHPadding - ExpModuleHPadding + 2;
				int shy1 = y - e.Y - e.Height + ExpAllHPadding - ExpModuleHPadding + ExpHeight + 2 * ExpModuleHPadding - 2;
				if( e.Level == 0 ) {
					g.DrawLine( Pens.WhiteSmoke, shx, shy0, shx, shy1 );
				}
				else {
					g.DrawLine( Pens.WhiteSmoke, shx, shy0, shx, shy1 );
				}
				
				g.DrawString( word, GUIset.ExpFont, ModtextBrush, x + e.X + vWidth + (ExpAllHPadding - ExpModuleHPadding) + ExpModuleWPadding, y - e.Y - e.Height + ExpAllHPadding + OffY );
			}
			else {
				w = (int)GUIset.mg.MeasureString( word, GUIset.ExpFont ).Width + WordPadding * 2;
				string tmp = word;
				if( tmp.IndexOf( U_SP ) != -1) {
					tmp = tmp.Replace( U_SP, ' ' );
				}
				if( tmp.IndexOf( "_s_" ) != -1 ) {
					tmp = tmp.Replace( "_s_", "=" );
				}
				if( tmp.IndexOf( "_ad_" ) != -1 ) {
					tmp = tmp.Replace( "_ad_", "+= " );
				}
				g.DrawString( tmp, GUIset.ExpFont, textBrush, x + e.X + vWidth + WordPadding + e.XOffset, y - e.Y - e.Height + ExpAllHPadding + OffY );
			}
			vWidth += w;
		}
	}
	
	//------------------------------------------
	//解析表达式到树形结构中
	void Paser( string exp )
	{
		Width = 0;
		MaxLevel = 0;
		Length = 1;
		ExpTree[ 0 ] = new ENote( exp );
		for( int i = 0; i < Length; ++i ) {
			PaserOneNote( ExpTree[ i ] );
		}
		Scan( 0, 0, 0 );
		
		int ExpHeight = GUIset.ExpFont.Height;
		Height = ExpHeight + 2 * ExpAllHPadding + MaxLevel * PerLevelHeight;
		
		Width += 2;
		Height += 2;
		
		//这里是做一个防护, 保证最底层的始终是一个 void/bool 类型的返回
		if( ExpTree[0].GetOperType() != "?void" && ExpTree[0].GetOperType() != "?bool" && ExpTree[0].GetOperType() != "?int32" && ExpTree[0].GetOperType() != "?" + GType.g_module ) {
			ExpTree[0].WordList[0] = "?void";
			
			n_Debug.Warning.BUG( "表达式解析出错: EXP.cs - Paser: " + strEXP );
			//MessageBox.Show( strEXP );
			
			strEXP = "?void " + strEXP.Remove( 0, 1 + strEXP.IndexOf( ' ' ) );
		}
	}
	
	//解析一个子表达式,提取第一层括号为指针,剩下表达式主干
	//指针为行号索引
	void PaserOneNote( ENote exp )
	{
		string[] Cut = exp.WordList[ 0 ].Split( ' ' );
		string result = "";
		for( int i = 0; i < Cut.Length; ++i ) {
			if( Cut[ i ] == "(" ) {
				string SubExp = "";
				int PadLevel = 0;
				for( int p = i + 1; p < Cut.Length; ++p ) {
					if( Cut[ p ] == "(" ) {
						++PadLevel;
					}
					if( Cut[ p ] == ")" || p == Cut.Length - 1 ) {
						--PadLevel;
						if( PadLevel < 0 ) {
							SubExp = SubExp.TrimEnd( ' ' );
							ExpTree[ Length ] = new ENote( SubExp );
							++Length;
							i = p;
							break;
						}
					}
					SubExp += Cut[ p ] + " ";
				}
				result += "#" + (Length - 1) + " ";
				continue;
			}
			result += Cut[ i ] + " ";
		}
		exp.WordList = result.TrimEnd( ' ' ).Split( ' ' );
		
		//这里升级旧版的格式, 启用 禁用 结束 类型为 SYS
		if( exp.GetOperType() == ENote.v_void && exp.GetExpType() == ENote.t_WORD &&
		    (exp.WordList[2] == "启用" || exp.WordList[2] == "禁用" || exp.WordList[2] == "结束" ) ) {
			exp.WordList[1] = ENote.t_SYS;
		}
	}
	
	//扫描指定的节点并添加位置信息, 这个函数为递归调用
	void Scan( int index, int NewLevel, int LastStartX )
	{
		ExpTree[ index ].Level = NewLevel;
		ExpTree[ index ].X = LastStartX;
		
		int vWidth = 0;
		int L = ExpTree[ index ].WordList.Length;
		
		//扫描计算成员尺寸宽度
		bool isword = ExpTree[ index ].GetExpType() == ENote.t_WORD || ExpTree[ index ].GetExpType() == ENote.t_VAR;
		if( !ExpTree[ index ].isNull() ) {
			for( int i = 2; i < L; ++i ) {
				string word = ExpTree[ index ].WordList[ i ];
				if( word.StartsWith( "#" ) ) {
					int TargetIndex = int.Parse( word.Remove( 0, 1 ) );
					Scan( TargetIndex, NewLevel + 1, LastStartX + vWidth );
					vWidth += ExpTree[ TargetIndex ].Width;
					continue;
				}
				int w = 0;
				if( i == 2 && ( ExpTree[ index ].GetExpType() == ENote.t_MFUNC || ExpTree[ index ].GetExpType() == ENote.t_MVAR ) ) {
					w = (ExpAllHPadding - ExpModuleHPadding);
					w += (int)GUIset.mg.MeasureString( word, GUIset.ExpFont ).Width + 2 * ExpModuleWPadding + ExpModuleInsWPadding;
				}
				else {
					w = (int)GUIset.mg.MeasureString( word.Replace( U_SP, ' ' ), GUIset.ExpFont ).Width + WordPadding * 2;
					ExpTree[ index ].XOffset = 0;
					if( isword && w < 20 ) {
						ExpTree[ index ].XOffset = (20 - w)/2;
						w = 20;
					}
				}
				vWidth += w;
			}
		}
		else {
			string word = ExpTree[ index ].WordList[ 0 ];
			if( word == "?void" ) {
				word = "新指令";
			}
			else if( word == "?base" ) {
				word = "基本量";
			}
			else if( word == "?bool" ) {
				word = "条件量";
			}
			else if( word == "?int32" ) {
				word = "整数值";
			}
			else if( word == "?float" ) {
				word = "浮点数";
			}
			else if( word == "?fix" ) {
				word = "小数值";
			}
			else if( word == "?module" ) {
				word = "角色";
			}
			else {
				//...
			}
			vWidth = (int)GUIset.mg.MeasureString( word, GUIset.ExpFont ).Width + WordPadding * 2;
		}
		ExpTree[ index ].Width = vWidth;
		
		//更新最大级别计数
		if( MaxLevel < NewLevel ) {
			MaxLevel = NewLevel;
		}
		//更新最大宽度
		if( Width < vWidth ) {
			Width = vWidth;
		}
	}
	
	//重新合成某个节点的文字表达式
	public string JionEXP( ENote e )
	{
		if( e.isNull() ) {
			return e.GetOperType();
		}
		string Result = "";
		int L = e.WordList.Length;
		for( int i = 0; i < L; ++i ) {
			string w = e.WordList[ i ];
			
			//如果是一个子项指针,显示子项
			if( w.StartsWith( "#" ) ) {
				int TargetIndex = int.Parse( w.Remove( 0, 1 ) );
				string SubEXP = JionEXP( ExpTree[ TargetIndex ] );
				Result += "( " + SubEXP + " ) ";
				continue;
			}
			Result += w + " ";
		}
		return Result.TrimEnd( ' ' );
	}
	
	string ShowEXPEditor( string ReturnType, string NeedType, string Value, string ExpType )
	{
		//结束插入指令时的自动移动
		((n_MyIns.MyIns)owner).EndAutoMove();
		
		if( NeedType == ENote.v_void ||
		    NeedType == ENote.v_bool ||
		    NeedType == ENote.v_mbool ||
		    NeedType == ENote.v_int32 ||
		    NeedType == ENote.v_mint32 ||
		    NeedType == ENote.v_Achar ||
		    NeedType == ENote.v_Astring ||
		    NeedType == ENote.v_Cstring ) {
			return G.commonEXPBox.Run( ReturnType, NeedType, ExpType, this.owner, Value, owner.GroupMes, false );
		}
		if( NeedType == ENote.v_fix ||
		    NeedType == ENote.v_mfix ||
		    NeedType == ENote.v_float ) {
			return G.commonEXPBox.Run( ReturnType, ENote.v_fix, ExpType, this.owner, Value, owner.GroupMes, true );
		}
		//if( NeedType == ENote.v_Astring || NeedType == ENote.v_Cstring ) {
		//	return G.AstringEXPBox.Run( Value );
		//}
		if( NeedType == ENote.v_music ||
		    NeedType == ENote.v_cbitmap ||
		    NeedType == ENote.v_bitmap ||
		    NeedType == ENote.v_m_bitmap ||
		    NeedType == ENote.v_font ||
		    NeedType == ENote.v_Time ||
		    NeedType == ENote.v_Module ) {
			return G.extendEXPBox.Run( NeedType );
		}
		if( NeedType == ENote.v_time ) {
			if( G.timeEXPBox == null ) {
				G.timeEXPBox = new n_timeEXPForm.timeEXPForm();
			}
			return G.timeEXPBox.Run( true, Value );
		}
		if( NeedType == ENote.v_protocol ) {
			if(G.protocolEXPBox == null ) {
				G.protocolEXPBox = new n_protocolEXPForm.protocolEXPForm();
			}
			return G.protocolEXPBox.Run( Value );
		}
		MessageBox.Show( "<ShowEXPEditor> 未知的类型: " + NeedType );
		return null;
	}
	
	//表达式节点类
	public class ENote
	{
		//2020.6.24格式定义: ?前缀表示指定类型即可; *前缀表示必须要可变的指定类型
		
		//注意暂时未用到开始的 "@" 符号
		public const string t_NULL = "@NULL";
		public const string t_USER = "@USER";
		public const string t_CODE = "@CODE";
		public const string t_FLOW = "@FLOW";
		public const string t_MFUNC = "@MFUNC";
		public const string t_MVAR = "@MVAR";
		public const string t_OPER = "@OPER";
		public const string t_OPERS = "@OPERS";
		public const string t_WORD = "@WORD";
		public const string t_VAR = "@VAR";
		public const string t_SYS = "@SYS";
		
		public const string t_SPLIT = "-";
		
		public const string v_void = "?void";
		public const string v_bool = "?bool";
		public const string v_int32 = "?int32";
		public const string v_fix = "?fix";
		
		//以下表示需要可变类型
		public const string v_mbool = "*bool";
		public const string v_mint32 = "*int32";
		public const string v_mfix = "*fix";
		
		public const string v_float = "?float";
		
		public const string v_Achar = "?" + GType.g_Achar;
		public const string v_Astring = "?" + GType.g_Astring;
		public const string v_Cstring = "?" + GType.g_Cstring;
		public const string v_music = "?" + GType.g_music;
		public const string v_bitmap = "?" + GType.g_bitmap;
		public const string v_cbitmap = "?" + GType.g_cbitmap;
		public const string v_m_bitmap = "*" + GType.g_bitmap;
		public const string v_font = "?" + GType.g_font;
		public const string v_time = "?" + GType.g_time;
		public const string v_Time = "?" + GType.g_Time;
		public const string v_Module = "?" + GType.g_module;
		public const string v_protocol = "?" + GType.g_protocol;
		
		int vvLevel;
		public int Level {
			get { return vvLevel; }
			set {
				vvLevel = value;
				Y = vvLevel * PerLevelHeight;
			}
		}
		public int X;
		public int Y;
		public int Width;
		public int Height;
		public bool isWord;
		public string[] WordList;
		
		public int XOffset;
		
		public bool isMouseOn;
		
		//构造函数
		public ENote( string exp )
		{
			Level = 0;
			X = 0;
			XOffset = 0;
			//Y = 0; 不需要赋值
			Width = 0;
			
			int ExpHeight = GUIset.ExpFont.Height;
			Height = ExpHeight + 2 * ExpAllHPadding;
			isWord = false;
			isMouseOn= false;
			if( exp != null ) {
				WordList = new string[ 1 ];
				WordList[ 0 ] = exp;
			}
			else {
				WordList = null;
			}
		}
		
		//获取节点类型
		public bool isNull()
		{
			if( WordList.Length == 1 && WordList[ 0 ].IndexOf( ' ' ) == -1 ) {
				return true;
			}
			else {
				return false;
			}
		}
		
		//获取节点类型
		public string GetExpType()
		{
			if( isNull() ) {
				return t_NULL;
			}
			return WordList[ 1 ];
		}
		
		//获取表达式运算类型
		public string GetOperType()
		{
			return WordList[ 0 ];
		}
		
		//显示
		public string Show()
		{
			string r = "S" + X + ", W" + Width + ", L" + Level + " - ";
			for( int i = 0; i < WordList.Length; ++i ) {
				r += WordList[ i ] + "+";
			}
			return r.TrimEnd( '+' );
		}
	}
}
}

