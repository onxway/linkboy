﻿

namespace n_CoroCompiler
{
using System;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Windows.Forms;

using n_Compiler;
using n_GUIset;
using n_Head_MtButton;
using n_ImagePanel;
using n_ISP;
using n_MyObjectList;
using n_OS;
using n_Shape;
using n_GNote;
using n_HardModule;
using n_GUIcoder;
using n_MyFileObject;
using n_LoopIns;
using n_UserFunctionIns;
using n_ET;
using n_SST;

public static class CoroCompiler
{
	//协程后台调度代码
	public static string BackLoop;
	
	//协程初始化代码
	public static string InitCode;
	
	public const string E_Flag = "_flg_";
	public const string E_En = "_en_";
	
	static string[] CoroList;
	public static int CoroLen;
	
	static int FileIndex;
	
	//查找一个协程的ID
	static int FindCoroID( string name )
	{
		for( int i = 0; i < CoroLen; ++i ) {
			if( CoroList[i] == name ) {
				return i;
			}
		}
		return -1;
	}
	
	//提取任务名称创建ID
	static void BuildCoroList( string s )
	{
		string[] cut = s.Split( '\n' );
		
		CoroList = new string[cut.Length];
		CoroLen = 0;
		
		for( int i = 0; i < cut.Length; i++ ) {
			string[] word_o = cut[i].Replace( '\t', ' ' ).Split( ' ' );
			
			string[] word = new string[word_o.Length];
			int len = 0;
			for( int j = 0; j < word.Length; ++j ) {
				if( word_o[j] != "" ) {
					word[len] = word_o[j];
					len++;
				}
			}
			//判断是否为任务定义
			if( word[0] == "task" ) {
				if( len > 2 ) {
					string task_name = word[2];
					int id = task_name.IndexOf( "(" );
					task_name = task_name.Remove( id );
					
					//BackLoop += "\tif(" + task_name + E_Flag + "!=0){OS0.CurrentTaskIndex=" + CoroLen + ";" + task_name + "();}\n";
					BackLoop += "\tif(" + task_name + E_Flag + "!=0){" + task_name + "();}\n";
					
					//添加一个协程记录
					CoroList[CoroLen] = task_name;
					CoroLen++;
				}
			}
		}
	}
	
	//预编译
	public static string Compile( string s )
	{
		FileIndex = 0;
		int Level = 0;
		int TaskLevel = -1;
		int funci = 0;
		
		//当前语句所在的任务名称
		string task_name = null;
		
		string r = "";
		string[] cut = s.Split( '\n' );
		
		InitCode = "";
		BackLoop = "";
		
		BuildCoroList( s );
		
		for( int i = 0; i < cut.Length; i++ ) {
			
			string code = cut[i];
			string note = "";
			int nid = code.IndexOf( "//" );
			if( nid != -1 ) {
				note = code.Remove( 0, nid );
				code = code.Remove( nid );
			}
			string code_tmp = code.Trim( " \t".ToCharArray() );
			string[] word_o = code.Replace( '\t', ' ' ).Trim( ' ' ).Split( ' ' ); //code 之前是 cut[i] 需要测一下兼容性
			string templine = String.Join( "", word_o );
			string[] word = new string[word_o.Length];
			int len = 0;
			for( int j = 0; j < word.Length; ++j ) {
				if( word_o[j] != "" ) {
					word[len] = word_o[j];
					len++;
				}
			}
			//空语句
			if( len == 0 ) {
				r += code;
			}
			//判断是否为任务定义
			else if( word[0] == "task" ) {
				TaskLevel = 0;
				
				if( len > 1 ) {
					if( word[1] != "void" ) {
						ET.WriteLineError( FileIndex, i, "任务函数的返回类型只能为 void -> 第" + i + "行: " + cut[i] );
					}
				}
				if( len > 2 ) {
					task_name = word[2];
					int id = task_name.IndexOf( "(" );
					task_name = task_name.Remove( id );
					r += "bool " + task_name + E_En + "; ";
					r += "uint8 " + task_name + E_Flag + "; ";
					InitCode += "\t" + task_name + E_Flag + " = 0;\n";
					InitCode += "\t" + task_name + E_En + " = true;\n";
				}
				r += cut[i];
			}
			//情况分类: 函数开始和结束, 等待语句{}, 条件判断/循环/次数等...
			else if( code_tmp.EndsWith( "{" ) ) {
				
				if( code.IndexOf( "}" ) == -1 ) {
					Level++;
				}
				
				//这里表示任务开始
				if( TaskLevel == 0 ) {
					TaskLevel = Level;
					funci = 1;
					r += code + " goto " + task_name + "_end;" + task_name + "_1:";
				}
				else {
					r += code;
				}
			}
			//判断是否任务结束
			else if( code_tmp.EndsWith( "}" ) ) {
				
				//这里表示任务结束
				if( TaskLevel != -1 && TaskLevel == Level ) {
					TaskLevel = -1;
					
					r += task_name + E_Flag + "=0;return;";
					r += task_name + "_end:";
					
					//if语句实现
					for( int n = 1; n <= funci; ++n ) {
						r += "if(" + task_name + E_Flag + "==" + n + "){goto " + task_name + "_" + n + ";}";
					}
					
					/*
					//switch实现, 代码量要多一些
					r += "switch(" + task_name + E_Flag + "){";
					for( int n = 1; n <= funci; ++n ) {
						r += "case " + n + ":goto " + task_name + "_" + n + ";";
					}
					r += "default: break;}";
					*/
					r += code;
				}
				else {
					r += code;
				}
				Level--;
			}
			//判断是否为协程触发
			else if( word[0] == "run" ) {
				
				if( TaskLevel == -1 ) {
					//MessageBox.Show( "只能在任务中触发其他任务 -> 第" + i + "行: " + cut[i] );
					//return null;
				}
				word[0] = "";
				string function = string.Join( " ", word );
				
				string tar_name = null;
				if( len > 1 ) {
					tar_name = word[1];
					int id = tar_name.IndexOf( "(" );
					if( id != -1 ) {
						tar_name = tar_name.Remove( id );
					}
				}
				if( tar_name == null ) {
					ET.WriteLineError( FileIndex, i, "触发任务语法错误 -> 第" + i + "行: " + cut[i] );
				}
				int id1 = FindCoroID( tar_name );
				if( id1 != -1 ) {
					//r += "\tOS0.CurrentTaskIndex=" + id1 + ";" + tar_name + E_Flag + "=1; " + function + "\n";
					r += "\t" + tar_name + E_Flag + "=1; " + function;
				}
				else {
					ET.WriteLineError( FileIndex, i, "未找到任务<" + tar_name + "> -> 第" + i + "行: " + cut[i] );
				}
			}
			//判断是否为临时调度语句触发
			else if( word[0] == "run_mode1" ) {
				funci++;
				r += "\t" + task_name + E_Flag + "=" + funci + ";" + code.Remove( 0, "run_mode1".Length + 1 ) + "return; " + task_name + "_" + funci + ":";
			}
			//判断是否返回指令, 需要加上标志位清零 2022.11.26 BUG
			else if( TaskLevel != -1 && (code_tmp.StartsWith( "return " ) || code_tmp.StartsWith( "return;" )) ) {
				r += task_name + E_Flag + "=0; " + code;
			}
			//判断是否为延时函数 - 当存在高精度延时器时 过滤掉遥控器的函数列表
			else if( Level != 0 && !word[0].StartsWith( "&#." ) &&  (
				word[0].IndexOf( "DelayMillisecond" ) != -1 ||
				word[0].IndexOf( "DelaySecond" ) != -1 ||
				word[0].IndexOf( "DelayMinute" ) != -1 ||
				word[0].IndexOf( "DelayHour" ) != -1 ||
				word[0].IndexOf( "DelayDay" ) != -1 ||
				word[0].IndexOf( "延时_毫秒" ) != -1 ||
				word[0].IndexOf( "延时_秒" ) != -1 ||
				word[0].IndexOf( "延时_分" ) != -1 ||
				word[0].IndexOf( "延时_小时" ) != -1 ||
				word[0].IndexOf( "延时_天" ) != -1
				) ) {
				
				if( Level != 0 && TaskLevel == -1 ) {
					ET.WriteLineError( FileIndex, i, "只能在任务中调用延时函数 -> 第" + i + "行: " + cut[i] );
				}
				funci++;
				int id2 = FindCoroID( task_name );
				
				//这里不对... 不断调用 Running 之前也需要不断设置 CurrentTaskIndex 的值!!!!
				//r += "\tOS0.CurrentTaskIndex=" + id2 + "; " + code + " " + task_name + E_Flag + "=" + funci + ";" + task_name + "_" + funci + ":if(OS0.Delayer_Running()){return;}";
				
				//改为调用函数, 这样可以处理延时器的允许调度标志
				r += "\tOS0.CurrentTaskIndex=" + id2 + "; " + code + " " + task_name + E_Flag + "=" + funci + ";" + task_name + "_" + funci + ":if(OS0.Delayer_TarRunning(" + id2 + ")){return;}";
			}
			//判断是否为调度函数
			else if( Level != 0 && word[0].StartsWith( "OS0.Schedule();" ) ) {
				if( Level != 0 && TaskLevel == -1 ) {
					ET.WriteLineError( FileIndex, i, "只能在任务中调用系统调度函数 -> 第" + i + "行: " + cut[i] );
				}
				funci++;
				int id2 = FindCoroID( task_name );
				
				//这里不对... 不断调用 Running 之前也需要不断设置 CurrentTaskIndex 的值!!!!
				//r += "\tOS0.CurrentTaskIndex=" + id2 + "; " + code + " " + task_name + E_Flag + "=" + funci + ";" + task_name + "_" + funci + ":if(OS0.Delayer_Running()){return;}";
				
				//改为调用函数, 这样可以处理延时器的允许调度标志
				r += "\t\t" + task_name + E_Flag + "=" + funci + ";return;" + task_name + "_" + funci + ":";
			}
			else {
				string defaultcode = code;
				
				//这里判断是否存在将协程按普通函数调用
				int padidx = templine.IndexOf( "(" );
				if( padidx != -1 ) {
					string tar_name = templine.Remove( padidx );
					int id1 = FindCoroID( tar_name );
					if( id1 != -1 ) {
						funci++;
						defaultcode = "\t" + task_name + E_Flag + "=" + funci + ";" + tar_name + E_Flag + "=1; " + templine + " " + task_name + "_" + funci + ": " + tar_name + "(); if(" + tar_name + E_Flag + "!=0){return;}";
					}
				}
				r += defaultcode;
			}
			r += note + "\n";
		}
		
		//System.Windows.Forms.Clipboard.SetText( r );
		
		return r;
	}
}
public static class cxvComp
{
	public const string E_PC = "_pc_";
	public const string E_Time = "_time_";
	
	static int FileIndex;
	static string InitCode;
	
	//crux.v -> verilog 编译器
	public static string Compile( string s )
	{
		InitCode = "";
		FileIndex = 0;
		
		int Level = 0;
		int TaskLevel = -1;
		int func_pc = 0;
		
		//当前语句所在的任务名称
		string task_name = null;
		
		s = s.Replace( "=", "<=" );
		
		string r = "\n";
		r += "module led (input wire CLK_IN, input wire RST_N, input wire KEY, output wire [2:0]RGB_LED);";
		r += "reg [2:0]rledout;\n";
		r += "assign RGB_LED = ~rledout;\n";
		r += "\n";
		
		string[] cut = s.Split( '\n' );
		
		for( int i = 0; i < cut.Length; i++ ) {
			
			string code = cut[i];
			string note = "";
			int nid = code.IndexOf( "//" );
			if( nid != -1 ) {
				note = code.Remove( 0, nid );
				code = code.Remove( nid );
			}
			string[] word_o = code.Replace( '\t', ' ' ).Trim( ' ' ).Split( ' ' ); //code 之前是 cut[i] 需要测一下兼容性
			string templine = String.Join( "", word_o );
			string[] word = new string[word_o.Length];
			int len = 0;
			for( int j = 0; j < word.Length; ++j ) {
				if( word_o[j] != "" ) {
					word[len] = word_o[j];
					len++;
				}
			}
			//空语句
			if( len == 0 ) {
				r += code;
			}
			//判断是否为任务定义
			else if( word[0] == "task" ) {
				TaskLevel = 0;
				
				if( len > 1 ) {
					if( word[1] != "void" ) {
						ET.WriteLineError( FileIndex, i, "任务函数的返回类型只能为 void -> 第" + i + "行: " + cut[i] );
					}
				}
				if( len > 2 ) {
					task_name = word[2];
					int id = task_name.IndexOf( "(" );
					task_name = task_name.Remove( id );
					
					r += "reg [31:0] " + task_name + E_PC + ";\n";
					r += "reg [31:0] " + task_name + E_Time + ";\n";
				}
				r += "always @(posedge " + SST.V_ClockName + ") begin\n";
				r += "\tif(" + SST.V_ResetName + "==0) begin\n";
				r += "\t\t" + task_name + E_PC + " <= 1;\n";
				r += "\t\t" + task_name + E_Time + " <= 1;\n";
				r += "\tend\n";
				r += "\telse";
			}
			else if( code == "{" ) {
				Level++;
				
				//这里表示任务开始
				if( TaskLevel == 0 ) {
					TaskLevel = Level;
					func_pc = 1;
					r += "\tcase( " + task_name + E_PC + " )\n";
					r += "\t0: begin " + task_name + E_PC + " <= 0; end";
				}
			}
			//判断是否任务结束
			else if( code == "}" ) {
				
				//这里表示任务结束
				if( TaskLevel == Level ) {
					TaskLevel = -1;
				}
				r += "\tdefault: " + task_name + E_PC + " <= 1;\n";
				r += "\tendcase\n";
				r += "end";
				Level--;
			}
			//判断是否为延时函数
			else if( Level != 0 && ( word[0].IndexOf( "delay" ) != -1 ) ) {
				
				
				if( Level != 0 &&TaskLevel == -1 ) {
					ET.WriteLineError( FileIndex, i, "只能在任务中调用延时函数 -> 第" + i + "行: " + cut[i] );
				}
				r += "\t" + func_pc + ": begin " + task_name + E_Time + " <= 10000000; " + task_name + E_PC + "<=" + (func_pc+1) + "; end\n";
				func_pc++;
				
				r += "\t" + func_pc + ": " + "if( " + task_name + E_Time + " != 0 )\n";
				r += "\t\t" + task_name + E_Time + " <= " + task_name + E_Time + " - 1;\n";
				r += "\telse\n";
				r += "\t\t" + task_name + E_PC + " <= " + (func_pc+1) + ";\n";
				func_pc++;
			}
			else {
				if( Level == 0 ) {
					r += code;
				}
				else {
					r += "\t" + func_pc + ": begin " + code + " " + task_name + E_PC + "<=" + (func_pc+1) + "; end";
					func_pc++;
				}
			}
			r += note + "\n";
		}
		
		r += "endmodule\n";
		
		if( GUIcoder.cmd_export ) {
			System.Windows.Forms.Clipboard.SetText( r );
		}
		
		return r;
	}
}
}


