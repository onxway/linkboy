﻿
//命名空间
namespace n_GUIcoder
{
using System;
using System.Drawing;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using System.Text;
using System.Windows.Forms;
using c_ControlType;

using n_HardModule;
using c_MyObjectSet;
using n_Config;
using n_EventLink;
using n_GNote;
using n_GUIset;
using n_GVar;
using n_ImagePanel;
using n_MyFileObject;
using n_MyObject;
using n_MyObjectList;
using n_OS;
using n_Shape;
using n_UIModule;
using n_UserModule;
using n_VarType;
using n_MainSystemData;
using n_CPUType;

using n_MyIns;
using n_WhileIns;
using n_EventIns;
using n_IfElseIns;
using n_ElseIns;
using n_CondiEndIns;
using n_WaitIns;
using n_FuncIns;
using n_ForeverIns;
using n_LoopIns;
using n_ExtIns;
using n_UserFunctionIns;
using n_MidPortList;
using NPinyin;
using n_SST;

using n_Py_GUIcoder;

//GUI界面解析引擎
public static class GUIcoder
{
	public delegate void SetLoadProgress( int n, int i );
	public static SetLoadProgress mySetLoadProgress;
	
	public static bool isVmProgram;
	
	public static int CodeSysLine;
	public static string CodeSys;
	public static string CodeAll;
	
	public static string CodeAll0_Temp;
	public static string CodeAll1_Temp;
	public static string InitCode_Temp;
	public static string BackLoop_Temp;
	
	public static bool isMakerLib = false;
	
	public static bool ExistCircuit;
	
	public static bool ExistLAPI;
	public static bool DebugOpen;
	public static bool DebugOpenCode;
	public static bool DebugOpenSet;
	static string DebugUART_ID;
	
	//AVR下载时候为 AVR, 仿真和其他处理器均为 VM
	public static string cputype1;
	
	public static bool isVCC3_3;
	
	static MyObject[] ControlList;
	static int ControlNumber;
	
	public static string DefineCode;
	static string InitCode;
	
	//是否需要导入字符串操作库
	public static bool LoadStringLib;
	
	//是否导入动态字符串和动态内存
	public static bool LoadDoStringLib = false;
	static bool HasShowDoStringMes = false;
	
	//忽略连线错误
	public static bool IgnoreError = false;
	
	public static int ProtocalNumber;
	
	//是否有信息显示器显示数字指令
	public static bool ExistShowNumberIns;
	public static bool ExistClearNumberIns;
	
	static string CommonRefLoad;
	static string CommonRefFunc1;
	static string CommonRefFunc2;
	static string UsedCommonRefCode;
	static int SYS_ServoID;
	
	static string  UsedMasterPort;
	
	static bool HaseServo;
	
	//当前是否是点击编译按钮事件
	public static bool cmd_download;
	//判断是否点击导出按钮
	public static bool cmd_export;
	//当前是否点击仿真事件
	public static bool cmd_simulate;
	
	public static bool AutoMove = false;
	public static bool ExistError;
	public static string ErrorMessage;
	public static MyObject ErrorObject;
	
	//查看图形界面的py代码, 仅供查看
	public static bool CodePython = false;
	
	public static bool FPGA_CPU = false;
	
	static ImagePanel t_GPanel;
	
	//当前打开程序的参考距离
	static float Cur_H;
	
	static bool GOTO = true;
	//static string[] GoNameList;
	static int TotalGoNumber;
	
	public static string EventFuncCode;
	
	//------------
	
	//是否开启单步调试模式 (是的话每个语句后边插入一个断点指令)
	public static bool TempOpenStep = true;
	
	//0-全速执行  1-暂停  2-继续(从上次位置)
	public static int isPause;
	//是否为全速执行状态
	public static bool isStep;
	
	//-------------
	
	//仿真定时器间隔
	public const int SIM_Tick = 20;
	
	//初始化
	public static void Init()
	{
		DebugOpenSet = true;
		EventFuncCode = null;
		
		cmd_download = false;
		cmd_export = false;
		cmd_simulate = false;
		
		isPause = 0;
		isStep = false;
	}
	
	//代码分割
	public static void CodeSplit( string Source, ref string UserText, ref string GCode )
	{
		bool isText = true;
		StringBuilder GUICode = new StringBuilder( "" );
		StringBuilder TextCode = new StringBuilder( "" );
		string[] Lines = Source.Split( '\n' );
		for( int i = 0; i < Lines.Length; ++i ) {
			if( Lines[ i ].StartsWith( "//[文字隐藏]," ) || Lines[ i ].StartsWith( "//[图形界面]," ) ) {
				isText = false;
			}
			if( isText ) {
				TextCode.Append( Lines[ i ] );
				TextCode.Append( "\n" );
			}
			else {
				GUICode.Append( Lines[ i ] );
				if( i != Lines.Length - 1 ) {
					GUICode.Append( "\n" );
				}
			}
		}
		if( !isText ) {
			GCode = GUICode.ToString();
		}
		UserText = TextCode.ToString();
	}
	
	//从文件中提取组件信息,包括背景图片,组件名,端口列表等
	//文件名为全路径格式
	public static void LoadModule(
		string FullPath,
		ref BasePort[] tPortList,
		ref BasePort[] tZWireList,
		ref int PORTLength,
		ref int ZWireLength,
		ref string BaseName,
		ref string BackFilePath,
		ref Image BackImage,
		ref string UserName,
		ref string DeviceType,
		ref string ImageList,
		ref string DriverFilePath,
		ref string Config,
		ref string MesFilePath,
		ref string EXMes,
		ref Size sz,
		ref string InitDataList,
		ref string SimValueList,
		ref bool isControlModule,
		ref bool isClientModule,
		ref bool isClientControlPad,
		ref bool isClientChannel,
		ref bool isVHRemoModule,
		ref bool isYuanJian,
		ref bool CanSwap,
		ref string Version
	)
	{
		Stream stream = File.Open( FullPath, FileMode.Open );
		BinaryFormatter bin = new BinaryFormatter();
		string Mes = (string)bin.Deserialize( stream );
		
		BackImage = (Image)bin.Deserialize( stream );
		stream.Close();
		
		//替换图片背景颜色(黑色替换成系统背景颜色)
//		for( int x = 0; x < BackImage.Width; ++x ) {
//			for( int y = 0; y < BackImage.Height; ++y ) {
//				
//			}
//		}
		//提取组件信息并创建组件对象
		string[][] SubList = GetConfigFromText( Mes );
		tPortList = new BasePort[ 1000 ];
		tZWireList = new BasePort[ 1000 ];
		PORTLength = 0;
		ZWireLength = 0;
		Version = "0";
		SimValueList = null;
		BackFilePath = null;
		
		for( int i = 0; i < SubList.Length; ++i ) {
			string[] ConfigCmdCut = SubList[ i ];
			
			if( ConfigCmdCut[ 0 ] == "[Version]" ) {
				Version = ConfigCmdCut[ 1 ];
			}
			else if( ConfigCmdCut[ 0 ] == "[基本名称]" ) {
				BaseName = ConfigCmdCut[ 1 ];
			}
			else if( ConfigCmdCut[ 0 ] == "[驱动文件路径]" ) {
				//...已过时的属性
			}
			else if( ConfigCmdCut[ 0 ] == "[背景图片路径]" ) {
				BackFilePath = ConfigCmdCut[ 1 ];
			}
			else if( ConfigCmdCut[ 0 ] == "[UserName]" ) {
				UserName = ConfigCmdCut[ 1 ];
			}
			else if( ConfigCmdCut[ 0 ] == "[DeviceType]" ) {
				DeviceType = ConfigCmdCut[ 1 ];
			}
			else if( ConfigCmdCut[ 0 ] == "[ImageList]" ) {
				ImageList = ConfigCmdCut[ 1 ];
				ImageList = ImageList.Replace( ".png", ".jpg" );
				if( ImageList != "" && !ImageList.StartsWith( "a" ) ) {
					ImageList = "a" + ImageList.Replace( ",", ",a" );
				}
			}
			else if( ConfigCmdCut[ 0 ] == "[DriverFilePath]" ) {
				DriverFilePath = ConfigCmdCut[ 1 ];
			}
			else if( ConfigCmdCut[ 0 ] == "[Config]" ) {
				Config = ConfigCmdCut[ 1 ];
			}
			else if( ConfigCmdCut[ 0 ] == "[MesFilePath]" ) {
				MesFilePath = ConfigCmdCut[ 1 ];
			}
			else if( ConfigCmdCut[ 0 ] == "[EXMes]" ) {
				EXMes = ConfigCmdCut[ 1 ];
			}
			else if( ConfigCmdCut[ 0 ] == "[Init]" ) {
				InitDataList += ConfigCmdCut[ 1 ];
			}
			else if( ConfigCmdCut[ 0 ] == "[Simulate]" ) {
				SimValueList += ConfigCmdCut[ 1 ] + "\n";
			}
			else if( ConfigCmdCut[ 0 ] == "[isControlModule]" ) {
				if( ConfigCmdCut[ 1 ] == "True" ) {
					isControlModule = true;
				}
				else {
					isControlModule = false;
				}
			}
			else if( ConfigCmdCut[ 0 ] == "[isClientModule]" ) {
				if( ConfigCmdCut[ 1 ] == "True" ) {
					isClientModule = true;
				}
				else {
					isClientModule = false;
				}
			}
			else if( ConfigCmdCut[ 0 ] == "[isClientControlPad]" ) {
				if( ConfigCmdCut[ 1 ] == "True" ) {
					isClientControlPad = true;
				}
				else {
					isClientControlPad = false;
				}
			}
			else if( ConfigCmdCut[ 0 ] == "[isClientChannel]" ) {
				if( ConfigCmdCut[ 1 ] == "True" ) {
					isClientChannel = true;
				}
				else {
					isClientChannel = false;
				}
			}
			else if( ConfigCmdCut[ 0 ] == "[isVHRemoModule]" ) {
				if( ConfigCmdCut[ 1 ] == "True" ) {
					isVHRemoModule = true;
				}
				else {
					isVHRemoModule = false;
				}
			}
			else if( ConfigCmdCut[ 0 ] == "[isYuanJian]" ) {
				if( ConfigCmdCut[ 1 ] == "True" ) {
					isYuanJian = true;
				}
				else {
					isYuanJian = false;
				}
			}
			else if( ConfigCmdCut[ 0 ] == "[CanSwap]" ) {
				if( ConfigCmdCut[ 1 ] == "True" ) {
					CanSwap = true;
				}
				else {
					CanSwap = false;
				}
			}
			//这个参数已经过时
			else if( ConfigCmdCut[ 0 ] == "[坐标]" ) {
				Point p = new Point( int.Parse( ConfigCmdCut[ 1 ] ), int.Parse( ConfigCmdCut[ 2 ] ) );
			}
			//这个参数已经过时
			else if( ConfigCmdCut[ 0 ] == "[Size]" ) {
				sz.Width = int.Parse( ConfigCmdCut[ 1 ] );
				sz.Height = int.Parse( ConfigCmdCut[ 2 ] );
			}
			else if( ConfigCmdCut[ 0 ] == "[端口]" ) {
				E_DirType DirType = E_DirType.NONE;
				switch( ConfigCmdCut[ 4 ] ) {
					case "UP":		DirType = E_DirType.UP; break;
					case "DOWN":	DirType = E_DirType.DOWN; break;
					case "LEFT":	DirType = E_DirType.LEFT; break;
					case "RIGHT":	DirType = E_DirType.RIGHT; break;
					case "NONE":	DirType = E_DirType.NONE; break;
					default:		n_Debug.Warning.BUG( "GUIcoder: 未知的端口方向枚举类型: " + ConfigCmdCut[ 4 ] ); break;
				}
				//需要添加头尾闭合包, 方便判断是否包含指定功能
				ConfigCmdCut[ 2 ] = "," + ConfigCmdCut[ 2 ] + ",";
				
				float PortX;
				float PortY;
				if( Version == "0" ) {
					PortX = int.Parse( ConfigCmdCut[ 5 ] );
					PortY = int.Parse( ConfigCmdCut[ 6 ] );
				}
				else {
					PortX = float.Parse( ConfigCmdCut[ 5 ] );
					PortY = float.Parse( ConfigCmdCut[ 6 ] );
				}
				string ExtValue = "";
				if( ConfigCmdCut.Length >= 8 ) {
					ExtValue = ConfigCmdCut[7];
				}
				ExtValue = ExtValue.PadRight( 2, Port.ExtNull );
				
				BasePort cPort = new BasePort( ConfigCmdCut[ 3 ], ConfigCmdCut[ 1 ], ConfigCmdCut[ 2 ], null, null, DirType, PortX, PortY, ExtValue );
				tPortList[ PORTLength ] = cPort;
				++PORTLength;
			}
			else if( ConfigCmdCut[ 0 ] == "[总线]" ) {
				E_DirType DirType = E_DirType.UP;
				switch( ConfigCmdCut[ 4 ] ) {
					case "UP":		DirType = E_DirType.UP; break;
					case "DOWN":	DirType = E_DirType.DOWN; break;
					case "LEFT":	DirType = E_DirType.LEFT; break;
					case "RIGHT":	DirType = E_DirType.RIGHT; break;
					default:		n_Debug.Warning.BUG( "GUIcoder: 未知的总线方向枚举类型: " + ConfigCmdCut[ 4 ] ); break;
				}
				//需要添加头尾闭合包, 方便判断是否包含指定功能
				ConfigCmdCut[ 2 ] = "," + ConfigCmdCut[ 2 ] + ",";
				
				string Style = null;
				if( ConfigCmdCut.Length > 7 ) {
					Style = ConfigCmdCut[7];
				}
				string SubPortConfig = null;
				if( ConfigCmdCut.Length > 8 ) {
					SubPortConfig = ConfigCmdCut[8];
				}
				float PortX;
				float PortY;
				if( Version == "0" ) {
					PortX = int.Parse( ConfigCmdCut[ 5 ] );
					PortY = int.Parse( ConfigCmdCut[ 6 ] );
				}
				else {
					PortX = float.Parse( ConfigCmdCut[ 5 ] );
					PortY = float.Parse( ConfigCmdCut[ 6 ] );
				}
				BasePort cPort = new BasePort(
					ConfigCmdCut[ 3 ], ConfigCmdCut[ 1 ], ConfigCmdCut[ 2 ], Style, SubPortConfig, DirType, PortX, PortY, "" );
				tZWireList[ ZWireLength ] = cPort;
				++ZWireLength;
			}
			else {
				n_Debug.Warning.BUG( "<组件配置> 未知的配置项: " + ConfigCmdCut[ 0 ] );
			}
		}
		if( SimValueList != null ) {
			SimValueList = SimValueList.TrimEnd( '\n' );
		}
	}
	
	//规范化图形界面描述表
	public static string[][] GetConfigFromText( string Text )
	{
		string[] ConfigList = Text.Split( '\n' );
		string[][] CfgList = new string[ ConfigList.Length ][];
		int cLength = 0;
		bool Start = false;
		for( int i = 0; i < ConfigList.Length; ++i ) {
			
			if( ConfigList[ i ] == "//[配置信息开始]," ) {
				Start = true;
				continue;
			}
			if( !Start ) {
				continue;
			}
			if( ConfigList[ i ] == "//[配置信息结束]," ) {
				break;
			}
			if( !ConfigList[ i ].StartsWith( "//" ) || !ConfigList[ i ].EndsWith( "," ) ) {
				n_Debug.Warning.BUG( "<GetConfigFromText> 系统错误, 图形界面语法解析异常: " + ConfigList[ i ] );
			}
			string ConfigCmd = ConfigList[ i ].Remove( ConfigList[ i ].Length - 1 ).Remove( 0, 2 );
			CfgList[ cLength ] = ConfigCmd.Split( ' ' );
			++cLength;
		}
		string[][] rList = new string[ cLength ][];
		for( int i = 0; i < cLength; ++i ) {
			rList[ i ] = CfgList[ i ];
		}
		return rList;
	}
	
	//更新图形界面参数
	public static string VarToText( ImagePanel GPanel )
	{
		t_GPanel = GPanel;
		
		ExistError = false;
		ErrorMessage = null;
		//ErrorObject = null; //这里临时去掉
		
		n_Debug.Warning.ClashMessage = null;
		
		LoadStringLib = false;
		LoadDoStringLib = false;
		
		n_SST.SST.GUI_Clear();
		
		DebugOpenCode = !n_ExportForm.ExportForm.isv && DebugOpenSet && n_ModuleLibPanel.MyModuleLibPanel.LabType == n_GUIcoder.labType.Code;
		DebugOpen = DebugOpenCode && cmd_download;
		
		DebugUART_ID = "0";
		
		
		/*
		if( G.CGPanel != null ) {
			n_Debug.Debug.Message = G.CGPanel.EventNumber + "  " + DebugOpen + "  " + DebugOpenSet + "  " + isDownload + " ";
		}
		*/
		
		ClearControlList( GPanel );
		
		//处理RAM优化 - 这里里判断是否是旧版的程序文件
		if( GPanel.RAM_Save < ImagePanel.RAM_Save_Start && GPanel.EventNumber < ImagePanel.DefaultEventNumber ) {
			
			GPanel.RAM_Save = ImagePanel.RAM_Save_Start;
			
			GPanel.EventNumber = ImagePanel.DefaultEventNumber;
			
			//n_Debug.Warning.WarningMessage = "已采用升级后的优化方案对RAM占用进行优化";
		}
		
		//转换工作台配置信息
		string GUItext = "";
		
		//if( !isPyMode || SimulateMode ) {
		
		//注意这里, 多处理器情况下也应该只包含一次文件, 这里尚未处理
		GUItext += AddPreDefine();
		
		//生成配置代码
		string ClientChannelName = null;
		for( int i = 0; i < ControlNumber; ++i ) {
			
			if( !GOTO ) {
				DefineCode = "uint8 x_TempAddEvent;\n";
			}
			else {
				DefineCode = "";
			}
			InitCode = "void OS_VarInit(void)\n{\n<<coroinit>>\n";
			
			MyObject.GroupReplace = true;
			GUItext += AddBoxCode( i,GPanel.myModuleList );
			GUItext += ConvertCode( i, GPanel.myModuleList, ref ClientChannelName );
			
			GUItext += AutoFlowCode( i, GPanel.myModuleList, ClientChannelName );
			
			if( cmd_download || cmd_simulate ) {
				
				//if( n_ModuleLibPanel.MyModuleLibPanel.LabType == n_GUIcoder.labType.Circuit ) {
					GUItext += GPanel.myCLineList.GetNet();
				//}
				if( GPanel.myCLineList.Exist ) {
					InitCode += "\t#.SYS_CTCOM.Init();\n";
					InitCode += "\tC_Init();\n";
				}
			}
			GUItext += AutoDefineCode( i, GPanel.myModuleList );
			MyObject.GroupReplace = false;
			
			GUItext += DefineCode;
			
			GUItext += "\n";
			
			GUItext += ConvertUserCode( i, GPanel.myModuleList );
			
			//添加初始化代码
			GUItext += InitCode;
			
			GUItext += "}\n";
			
			if( cputype1 == CPU_Type.AVR ) {
				if( GPanel.RAM_Save > 0 ) {
					n_Interrupt.Interrupt.MaxNumber = n_Interrupt.Interrupt.C_MaxNumber;//150;
				}
				else {
					n_Interrupt.Interrupt.MaxNumber = n_Interrupt.Interrupt.C_MaxNumber;
				}
			}
			else {
				n_Interrupt.Interrupt.MaxNumber = n_Interrupt.Interrupt.C_MaxNumber;
			}
		}
		
		SST.EN = !(G.isCruxEXE && G.isCV);
		
		//判断是否需要导入字符串操作库
		if( LoadStringLib ) {
			GUItext += "#include <system\\CommonRef\\MyString\\MySString.txt>\n\n";
		}
		
		//这里始终装载字符串库
		if( LoadDoStringLib ) {
			GUItext += "const uint16 sys_DString_heap_HIDLength = " + GPanel.DRAM_Block + ";\n";
			GUItext += "#.sys_DString.heap.HIDLength = sys_DString_heap_HIDLength;\n";
			GUItext += "#include <system\\CommonRef\\MyString\\MyDString.txt>\n\n";
		}
		
		//}
		//else {
		//	GUItext += Py_GUIcoder.Py_GetSysCode( GPanel );
		//}
		
		string ucode = GetUserProgram();
		
		CodeSys = GUItext + "\n//<<SYS_USER_START>>\n";
		//CodeSys = n_Param.Param.S_Param + " " + n_Param.Param.S_UserspaceOff + "\n";
		
		CodeAll = CodeSys + ucode + n_Param.Param.S_Param + " " + n_Param.Param.S_UserspaceOff + "\n";
		
		n_ET.ET.Clear();
		
		bool iscmd = cmd_simulate || cmd_download || cmd_export;
		
		if( iscmd ) {
			CodeAll0_Temp = CodeAll;
		}
		else {
			n_ET.ET.Open = false;
		}
		//调用预编译器编译协程代码
		CodeAll = n_CoroCompiler.CoroCompiler.Compile( CodeAll );
		
		//string ttt = n_CoroCompiler.cxvComp.Compile( ucode );
		
		CodeAll = CodeAll.Replace( "<<coroinit>>", n_CoroCompiler.CoroCompiler.InitCode );
		CodeAll = CodeAll.Replace( "<<coroback>>", n_CoroCompiler.CoroCompiler.BackLoop );
		CodeAll = CodeAll.Replace( "<<TotalGoNumber>>", n_CoroCompiler.CoroCompiler.CoroLen.ToString() );
		int id = CodeAll.IndexOf( "//<<SYS_USER_START>>" );
		
		if( iscmd ) {
			CodeAll1_Temp = CodeAll;
			InitCode_Temp = n_CoroCompiler.CoroCompiler.InitCode;
			BackLoop_Temp = n_CoroCompiler.CoroCompiler.BackLoop;
			CodeSysLine = CodeAll.Remove( id ).Split( '\n' ).Length;
		}
		
		if( G.isCruxEXE ) {
			n_ET.ET.SysLine = CodeSysLine;
		}
		
		if( iscmd && n_ET.ET.isErrors() && !G.isCruxEXE ) {
			n_Debug.Warning.AddErrMessage( "协程预编译错误, 请检查程序:\n" + n_ET.ET.Show() );
		}
		
		n_ET.ET.Open = true;
		
		//System.Windows.Forms.Clipboard.SetText( CodeAll );
		
		
		//转换工作台配置信息
		GUItext = "";
		
		GUItext += "//[图形界面],\n";
		GUItext += "//以下配置信息和程序代码由linkboy图形界面自动生成,请勿手动修改\n";
		GUItext += "//可在www.linkboy.cc下载最新版软件打开此文件\n";
		GUItext += "//[配置信息开始],\n";
		GUItext += "//[工作台],\n";
		GUItext += "//[语言] " + GPanel.myModuleList.Language + ",\n";
		//GUItext += "//[模块放缩] " + HardModule.Scale + ",\n";
		GUItext += "//[软件加速] " + GPanel.voffset + ",\n";
		GUItext += "//[VEX驱动周期] " + GPanel.VEX_Tick + ",\n";
		GUItext += "//[动态内存] " + GPanel.DRAM_Block + ",\n";
		GUItext += "//[RAM节约] 0,\n"; //GPanel.RAM_Save + ",\n"; //这里注释后就是每次编译都优化
		GUItext += "//[隐藏导线] " + GPanel.HideLine + ",\n";
		GUItext += "//[隐藏名称] " + GPanel.HideName + ",\n";
		GUItext += "//[连线拟合] " + n_HardModule.Port.C_Rol + ",\n";
		GUItext += "//[事件池] " + GPanel.EventNumber + ",\n";
		GUItext += "//[EventVarStack] " + GPanel.EventVarStack + ",\n";
		GUItext += "//[EventFuncStack] " + GPanel.EventFuncStack + ",\n";
		GUItext += "//[界面类型] " + n_ModuleLibPanel.MyModuleLibPanel.LabType + ",\n";
		string grmes = GPanel.mGroupList.GetConfig();
		if( grmes != null ) {
			GUItext += "//[分组] " + grmes + "\n";
		}
		if( GPanel.OSPath != null ) {
			GUItext += "//[操作系统] " + GPanel.OSPath + ",\n";
		}
		if( GPanel.SysMes != null ) {
			GUItext += "//[系统信息] " + GPanel.SysMes + ",\n";
		}
		if( GPanel.UserMes != null ) {
			GUItext += "//[用户信息] " + GPanel.UserMes.Replace( "\n", "\\n" ).Replace( " ", "\\s" ) + ",\n";
		}
		//GUItext += "//[DPI] " + GUIset.SYS_H + ",\n";
		GUItext += "//[版本号] " + G.Version + ",\n";
		GUItext += "//[工作台结束],\n";
		
		
		//保存UI信息
		GUItext += "//[地图],\n";
		
		if( n_SimPanel.SimPanel.MapImgFile != null ) {
			GUItext += "//[路径] " + n_SimPanel.SimPanel.MapImgFile + ",\n";
		}
		if( n_SimPanel.SimPanel.BackIamgePath != null ) {
			GUItext += "//[背景图] " + n_SimPanel.SimPanel.BackIamgePath + ",\n";
		}
		GUItext += "//[背景图属性] " + n_SimPanel.SimPanel.BackImgX + " " + n_SimPanel.SimPanel.BackImgY + " " + n_SimPanel.SimPanel.BackImgWidth + " " + n_SimPanel.SimPanel.BackImgHeight + ",\n";
		
		GUItext += "//[放大] " + n_SimPanel.SimPanel.MapDotNumber + ",\n";
		GUItext += "//[颜色] " + n_SimPanel.SimPanel.myBackColor.R + " " + n_SimPanel.SimPanel.myBackColor.G + " " + n_SimPanel.SimPanel.myBackColor.B + ",\n";
		GUItext += "//[CamMidX] " + n_SimPanel.SimPanel.BackCamMidX + ",\n";
		GUItext += "//[CamMidY] " + n_SimPanel.SimPanel.BackCamMidY + ",\n";
		GUItext += "//[CamAngle] " + n_SimPanel.SimPanel.BackCamAngle + ",\n";
		GUItext += "//[CamScale] " + n_SimPanel.SimPanel.BackCamZ + ",\n";
		GUItext += "//[宽度] " + n_SimPanel.SimPanel.UWidth + ",\n";
		GUItext += "//[高度] " + n_SimPanel.SimPanel.UHeight + ",\n";
		GUItext += "//[地图结束],\n";
		
		
		//转换组件列表配置信息
		foreach( MyObject mo in GPanel.myModuleList ) {
			
			string s = SaveConfig( mo );
			if( s != null ) {
				GUItext += s;
				continue;
			}
			
			n_Debug.Warning.BUG( "<VarToText>解析图形界面时发现未知的类型: " + mo.GetType() );
		}
		if( GPanel != null ) {
			GPanel.myMidPortList.StartSwitch();
		}
		
		//转换链接配置信息
		foreach( MyObject mo in GPanel.myModuleList ) {
			if( (mo is HardModule) ) {
				HardModule cM = (HardModule)mo;
				for( int n = 0; n < cM.PORTList.Length; ++n ) {
					if( cM.PORTList[ n ].TargetPort != null ) {
						
						string Name1 = cM.PORTList[ n ].Name;
						string Name2 = cM.PORTList[ n ].TargetPort.Name;
						
						bool SaveNumber = cM.PORTList[ n ].ExtValue[0] == Port.Ext0_DigitalSave;
						bool SaveNumber1 = cM.PORTList[ n ].TargetPort.ExtValue[0] == Port.Ext0_DigitalSave;
						
						if( cM.PORTList[ n ].CanChange || cM.PORTList[ n ].FuncName == PortFuncName.VCC || cM.PORTList[ n ].FuncName == PortFuncName.GND || SaveNumber ) {
							Name1 = "#" + cM.PORTList[ n ].Index;
						}
						if( cM.PORTList[ n ].TargetPort.CanChange || cM.PORTList[ n ].TargetPort.FuncName == PortFuncName.VCC || cM.PORTList[ n ].TargetPort.FuncName == PortFuncName.GND || SaveNumber1 ) {
							Name2 = "#" + cM.PORTList[ n ].TargetPort.Index;
						}
						string Target = cM.PORTList[ n ].TargetPort.Owner.Name + " " + Name2;
						GUItext += "//[链接] " + cM.Name + " " + Name1 + " ->#" + cM.PORTList[ n ].ColorIndex + " " + Target;
						
						if( cM.PORTList[ n ].MidPortIndex != -1 ) {
							GUItext += " " + cM.PORTList[ n ].MidPortIndex;
							int L = GPanel.myMidPortList.GetLength( cM.PORTList[ n ].MidPortIndex );
							for( int mi = 0; mi < L; ++ mi ) {
								n_MidPortList.MidPort mp = GPanel.myMidPortList.GetPoint( cM.PORTList[ n ].MidPortIndex, mi );
								if( mp.MultPort > 1 ) {
									if( mp.isFirstMult ) {
										mp.isFirstMult = false;
										mp.ListIndex = cM.PORTList[ n ].MidPortIndex;
										mp.Index = mi;
										GUItext += " " + mp.X + "," + mp.Y;
									}
									else {
										GUItext += " &" + mp.ListIndex + "," + mp.Index;
									}
								}
								else {
									GUItext += " " + mp.X + "," + mp.Y;
								}
							}
						}
						GUItext += ",\n";
					}
					else if( cM.PORTList[ n ].MidPortIndex != -1 ) {
						
						string Name1 = cM.PORTList[ n ].Name;
						
						bool SaveNumber = cM.PORTList[ n ].ExtValue[0] == Port.Ext0_DigitalSave;
						
						if( cM.PORTList[ n ].CanChange || cM.PORTList[ n ].FuncName == PortFuncName.VCC || cM.PORTList[ n ].FuncName == PortFuncName.GND || SaveNumber ) {
							Name1 = "#" + cM.PORTList[ n ].Index;
						}
						
						string Target = "* *";
						GUItext += "//[链接] " + cM.Name + " " + Name1 + " ->#" + cM.PORTList[ n ].ColorIndex + " " + Target;
						
						if( cM.PORTList[ n ].MidPortIndex != -1 ) {
							GUItext += " " + cM.PORTList[ n ].MidPortIndex;
							int L = GPanel.myMidPortList.GetLength( cM.PORTList[ n ].MidPortIndex );
							for( int mi = 0; mi < L; ++ mi ) {
								n_MidPortList.MidPort mp = GPanel.myMidPortList.GetPoint( cM.PORTList[ n ].MidPortIndex, mi );
								if( mp.MultPort > 1 ) {
									if( mp.isFirstMult ) {
										mp.isFirstMult = false;
										mp.ListIndex = cM.PORTList[ n ].MidPortIndex;
										mp.Index = mi;
										GUItext += " " + mp.X + "," + mp.Y;
									}
									else {
										GUItext += " &" + mp.ListIndex + "," + mp.Index;
									}
								}
								else {
									GUItext += " " + mp.X + "," + mp.Y;
								}
							}
						}
						GUItext += ",\n";
					}
					if( cM.PORTList[ n ].TargetModule != null ) {
						string Target = cM.PORTList[ n ].TargetModule.Name;
						GUItext += "//[链接] " + cM.Name + " " + cM.PORTList[ n ].Name + " -> " + Target + ",\n";
					}
				}
				for( int n = 0; n < cM.ZWireList.Length; ++n ) {
					if( cM.ZWireList[ n ].TargetPort != null ) {
						string Target =
							cM.ZWireList[ n ].TargetPort.Owner.Name + " " +
							cM.ZWireList[ n ].TargetPort.Name;
						GUItext += "//[链接] " + cM.Name + " " + cM.ZWireList[ n ].Name + " -> " + Target + ",\n";
					}
					if( cM.ZWireList[ n ].TargetModule != null ) {
						string Target = cM.ZWireList[ n ].TargetModule.Name;
						GUItext += "//[链接] " + cM.Name + " " + cM.ZWireList[ n ].Name + " -> " + Target + ",\n";
					}
				}
			}
			if( mo is MyIns ) {
				MyIns mi = (MyIns)mo;
				
				GUItext += SaveInsLink( mi );
				
				//把 Else 合并到 IfElse 中, 防止先解析 Else 时出错
				//if( ModuleList[ i ] is ElseIns ) {
				//	ElseIns ii = (ElseIns)ModuleList[ i ];
				//	GUItext += "//[Else结构链接] " + mi.Name + " CondiEnd -> " + ii.InsEnd.Name + ",\n";
				//}
			}
		}
		
		//保存电路线列表
		for( int i = 0; i < GPanel.myCLineList.Length; ++i ) {
			if( GPanel.myCLineList.CL_List[i] == null ) {
				continue;
			}
			n_CLineList.CPort CP0 = GPanel.myCLineList.CL_List[i][0];
			n_CLineList.CPort CP1 = GPanel.myCLineList.CL_List[i][1];
			GUItext += "//[CLine] " + CP0.X + "," + CP0.Y + " " + CP1.X + "," + CP1.Y + ",\n";
		}
		
		GUItext += "//[配置信息结束],\n";
		
		return GUItext;
	}
	
	//状态为收起的块自动执行收起动作
	public static void SetFollowVisible( MyObjectList mlist, ImagePanel CGPanel )
	{
		foreach( MyObject mo in mlist ) {
			
			//判断事件是否需要折叠
			if( mo is EventIns ) {
				EventIns ei = (EventIns)mo;
				if( !ei.ShowFollow ) {
					ei.SetFollowVisible( ei.ShowFollow );
				}
			}
			//判断用户函数是否需要折叠
			if( mo is UserFunctionIns ) {
				UserFunctionIns ui = (UserFunctionIns)mo;
				if( !ui.ShowFollow ) {
					ui.SetFollowVisible( ui.ShowFollow );
				}
				//获取用户函数的映射目标对象
				if( ui.isEvent ) {
					foreach( MyObject mo1 in mlist ) {
						
						if( mo1 is UserFunctionIns ) {
							if( mo1.Name == ui.tempTargetName ) {
								ui.Target = (UserFunctionIns)mo1;
								((UserFunctionIns)mo1).Target = ui;
								break;
							}
						}
					}
				}
			}
			//判断是否需要建立相关的分组
			if( mo.GroupMes != null ) {
				if( CGPanel.mGroupList.FindGroup( mo.GroupMes ) == null ) {
					CGPanel.mGroupList.AddGroup( mo.GroupMes );
				}
			}
		}
		//遍历各个分组根据标志判断是否需要折叠
		if( CGPanel.tempGroupMes != null ) {
			foreach( n_GroupList.Group gr in CGPanel.mGroupList ) {
				
				string[] cut = CGPanel.tempGroupMes.Split( ',' );
				
				foreach( string c in cut ) {
					//c = c.Split( '+' )[0];
					if( gr.GMes == c ) {
						gr.Expand = false;
						gr.RefreshExpandStatus( CGPanel );
						
						break;
					}
				}
			}
		}
	}
	
	//提取图形界面参数
	public static void TextToVar( ImagePanel vGPanel, MyObjectList myModuleList, MidPortList myMidPortList, string GUItext )
	{
		if( GUItext == null ) {
			return;
		}
		Cur_H = GUIset.CK_H;
		
		string[][] ConfigList = GetConfigFromText( GUItext );
		
		for( int i = 0; i < ConfigList.Length; ++i ) {
			
			/*
			if( mySetLoadProgress != null ) {
				mySetLoadProgress( ConfigList.Length, i );
			}
			*/
			
			switch( ConfigList[ i ][ 0 ] ) {
				case "[工作台]":	i = TableConfig( vGPanel, ConfigList, i ); break;
				case "[地图]":		i = MapConfig( myModuleList, ConfigList, i ); break;
				case "[描述]":		i = NoteConfig( myModuleList, ConfigList, i ); break;
				case "[组件]":		i = ModuleConfig( vGPanel, myModuleList, ConfigList, i ); break;
				case "[变量]":		i = VarConfig( myModuleList, ConfigList, i ); break;
				
				case "[EventIns]":	i = EventInsConfig( myModuleList, ConfigList, i ); break;
				case "[ForeverIns]":i = ForeverInsConfig( myModuleList, ConfigList, i ); break;
				case "[LoopIns]":	i = LoopInsConfig( myModuleList, ConfigList, i ); break;
				case "[ExtIns]":	i = ExtInsConfig( myModuleList, ConfigList, i ); break;
				case "[IfIns]":		i = WhileInsConfig( myModuleList, ConfigList, i ); break;
				case "[IfElseIns]":	i = IfElseInsConfig( myModuleList, ConfigList, i ); break;
				case "[ElseIns]":	i = ElseInsConfig( myModuleList, ConfigList, i ); break;
				case "[CondiEndIns]":i = CondiEndInsConfig( myModuleList, ConfigList, i ); break;
				case "[WaitIns]":	i = WaitInsConfig( myModuleList, ConfigList, i ); break;
				case "[FuncIns]":	i = FuncInsConfig( myModuleList, ConfigList, i ); break;
				case "[UserFunctionIns]":	i = UserFunctionInsConfig( myModuleList, ConfigList, i ); break;
				
				case "[链接]":		i = LinkConfig( myModuleList, myMidPortList, ConfigList, i ); break;
				case "[CLine]":		i = CLineConfig( vGPanel, ConfigList, i ); break;
				
				case "[指令链接]":				i = LinkInsConfig( myModuleList, ConfigList, i ); break;
				case "[UserFunction结构链接]":	i = LinkUserFunctionConfig( myModuleList, ConfigList, i ); break;
				case "[Event结构链接]":			i = LinkEventConfig( myModuleList, ConfigList, i ); break;
				case "[Loop结构链接]":			i = LinkLoopConfig( myModuleList, ConfigList, i ); break;
				case "[Ext结构链接]":			i = LinkExtConfig( myModuleList, ConfigList, i ); break;
				case "[Forever结构链接]":		i = LinkForeverConfig( myModuleList, ConfigList, i ); break;
				case "[While结构链接]":			i = LinkWhileConfig( myModuleList, ConfigList, i ); break;
				case "[IfElse结构链接]":			i = LinkIfElseConfig( myModuleList, ConfigList, i ); break;
				case "[Else结构链接]":			i = LinkElseConfig( myModuleList, ConfigList, i ); break;
				case "[IfElseEnd结构链接]":		i = LinkIfElseEndConfig( myModuleList, ConfigList, i ); break;
				
				default:	ConfigError( "<配置类型解析> 未知的配置项: " + ConfigList[ i ][ 0 ] ); break;
			}
		}
		SetFollowVisible( vGPanel.myModuleList, vGPanel );
		
		bool e = vGPanel.myCLineList.GetCrossOK();
		
		//处理特例问题
		//1-接口文本连接的对象化
		//2-事件文本链接的对象化(未处理)
		foreach( MyObject mo1 in vGPanel.myModuleList ) {
			if( !( mo1 is MyFileObject ) ) {
				continue;
			}
			MyFileObject m = (MyFileObject)mo1;
			
			//遍历接口列表
			for( int j = 0; j < m.LinkInterfaceList.Length; ++j ) {
				
				//设置已经连接的接口
				if( m.LinkInterfaceList[ j ][ 0 ] != "" ) {
					string TargetName = m.LinkInterfaceList[ j ][ 0 ];
					string TargetModuleName = TargetName.Split( '.' )[ 0 ];
					string TargetInterfaceName = TargetName.Split( '.' )[ 1 ];
					
					foreach( MyObject mo in vGPanel.myModuleList ) {
						if( !( mo is MyFileObject ) ) {
							continue;
						}
						MyFileObject tm = (MyFileObject)mo;
						if( tm.Name != TargetModuleName ) {
							continue;
						}
						for( int n = 0; n < tm.InterfaceList.Length; ++n ) {
							if( tm.InterfaceList[ n ][ 2 ] != TargetInterfaceName ) {
								continue;
							}
							try{
								m.IPortList[ m.InterfaceList.Length + j ].TargetIPort = tm.IPortList[n];
							}catch {
								n_Debug.Warning.BUG( "<TextToVar> 出现异常: + " + m.InterfaceList.Length + "," + j + "," + m.IPortList.Length );
							}
							goto next;
						}
					}
					next:;
				}
			}
		}
		//获取图形界面的边界, 自动布局全局图
		float MinX = 0, MaxX = 0;
		float MinY = 0, MaxY = 0;
		float C_MinX = 0, C_MaxX = 0;
		float C_MinY = 0, C_MaxY = 0;
		Shape.GetArea( vGPanel, vGPanel.myModuleList, ref MinX, ref MinY, ref MaxX, ref MaxY, ref C_MinX, ref C_MinY, ref C_MaxX, ref C_MaxY );
		
		int LeftX = n_Head.Head.HeadWidth + n_Head.Head.ShowX;
		int pW = vGPanel.Width - LeftX;
		int pH = vGPanel.Height - n_Head.Head.HeadLabelHeight;
		
		vGPanel.StartX = (int)(-( (MaxX+MinX)/2 - pW/2 - LeftX));
		vGPanel.StartY = (int)(-( (MaxY+MinY)/2 - pH/2 - n_Head.Head.HeadLabelHeight ));
	}
	
	//提取参数到当前的页面中
	static bool PasteMode;
	public static MyObject AddTextToGPanel( ImagePanel vGPanel, string GUItext )
	{
		PasteMode = true;
		
		string[][] ConfigList = GetConfigFromText( GUItext );
		
		MyObjectList myModuleList = new MyObjectList( vGPanel );
		
		for( int i = 0; i < ConfigList.Length; ++i ) {
			switch( ConfigList[ i ][ 0 ] ) {
				case "[工作台]":		i = TableConfig( vGPanel, ConfigList, i ); break;
				case "[描述]":		i = NoteConfig( myModuleList, ConfigList, i ); break;
				case "[组件]":		i = ModuleConfig( vGPanel, myModuleList, ConfigList, i ); break;
				case "[变量]":		i = VarConfig( myModuleList, ConfigList, i ); break;
				
				case "[EventIns]":	i = EventInsConfig( myModuleList, ConfigList, i ); break;
				case "[ForeverIns]":i = ForeverInsConfig( myModuleList, ConfigList, i ); break;
				case "[LoopIns]":	i = LoopInsConfig( myModuleList, ConfigList, i ); break;
				case "[IfIns]":		i = WhileInsConfig( myModuleList, ConfigList, i ); break;
				case "[IfElseIns]":	i = IfElseInsConfig( myModuleList, ConfigList, i ); break;
				case "[ElseIns]":	i = ElseInsConfig( myModuleList, ConfigList, i ); break;
				case "[CondiEndIns]":i = CondiEndInsConfig( myModuleList, ConfigList, i ); break;
				case "[WaitIns]":	i = WaitInsConfig( myModuleList, ConfigList, i ); break;
				case "[FuncIns]":	i = FuncInsConfig( myModuleList, ConfigList, i ); break;
				case "[ExtIns]":	i = ExtInsConfig( myModuleList, ConfigList, i ); break;
				case "[UserFunctionIns]":	i = UserFunctionInsConfig( myModuleList, ConfigList, i ); break;
				
				//case "[链接]":		i = LinkConfig( myModuleList, myMidPortList, ConfigList, i ); break;
				
				case "[指令链接]":				i = LinkInsConfig( myModuleList, ConfigList, i ); break;
				case "[UserFunction结构链接]":	i = LinkUserFunctionConfig( myModuleList, ConfigList, i ); break;
				case "[Event结构链接]":			i = LinkEventConfig( myModuleList, ConfigList, i ); break;
				case "[Loop结构链接]":			i = LinkLoopConfig( myModuleList, ConfigList, i ); break;
				case "[Ext结构链接]":			i = LinkExtConfig( myModuleList, ConfigList, i ); break;
				case "[Forever结构链接]":		i = LinkForeverConfig( myModuleList, ConfigList, i ); break;
				case "[While结构链接]":			i = LinkWhileConfig( myModuleList, ConfigList, i ); break;
				case "[IfElse结构链接]":		i = LinkIfElseConfig( myModuleList, ConfigList, i ); break;
				case "[Else结构链接]":			i = LinkElseConfig( myModuleList, ConfigList, i ); break;
				case "[IfElseEnd结构链接]":		i = LinkIfElseEndConfig( myModuleList, ConfigList, i ); break;
				
				default:	n_Debug.Warning.BUG( "<配置类型解析> 未知的配置项: " + ConfigList[ i ][ 0 ] ); break;
			}
		}
		//设置指令名字
		foreach( MyObject mo in myModuleList ) {
			if( mo is MyIns ) {
				mo.Name = n_GUICommon.GUICommon.SearchName( vGPanel );
			}
			
			mo.isSelect = false;
			mo.NeedPasteMove = true;
			vGPanel.myModuleList.Add( mo );
		}
		
		SetFollowVisible( myModuleList, vGPanel );
		
		MyObject.PasteMoveTime = 100;
		//G.CGPanel.SelPanel.SelectState = 2;
		//G.CGPanel.SelPanel.ExistSelect = true;
		
		PasteMode = false;
		
		if( myModuleList.MLength > 0 && myModuleList.ModuleList[0] != null ) {
			return myModuleList.ModuleList[0];
		}
		return null;
	}
	
	//获取用户代码(python自动编译并返回机器码结果)
	public static string GetUserProgram()
	{
		//如果是C语言, 直接返回
		return G.ccode.UserText;
		
		
		n_PyCompiler.Compiler.SimTempSource = G.ccode.UserText + "\n<sys> noline\n"+ ConvertUserPythonCode( 0, G.CGPanel.myModuleList );
		
		//这里提取cx语言
		string cxcode = GetCxCode( ref n_PyCompiler.Compiler.SimTempSource );
		
		string r = n_PyCompiler.Compiler.Compile( G.ccode.PathAndName );
		if( n_PyET.ET.isErrors() ) {
			return null;
		}
		
		//G.CodeViewBox.SetSysText( r );
		G.CodeBox.SetByteCode( r );
		
		return cxcode + r;
	}
	
	//获取cx代码
	static string GetCxCode( ref string code )
	{
		string py = null;
		string cx = null;
		string[] cut = code.Split( '\n' );
		bool isPy = true;
		for( int i = 0; i < cut.Length; ++i ) {
			if( cut[i].Trim( " \t".ToCharArray() ) == "@linkboy" ) {
				isPy = false;
				py += "\n";
				//cx += "\n";
				continue;
			}
			if( cut[i].Trim( " \t".ToCharArray() ) == "@python" ) {
				isPy = true;
				py += "\n";
				//cx += "\n";
				continue;
			}
			if( isPy ) {
				py += cut[i] + "\n";
				//cx += "\n";
			}
			else {
				cx += cut[i] + "\n";
				py += "\n";
			}
		}
		code = py;
		return cx;
	}
	
	//保存公共信息
	public static string GetCommonMes()
	{
		string GUItext = "//[工作台],\n";
		
		string grmes = G.CGPanel.mGroupList.GetConfig();
		if( grmes != null ) {
			GUItext += "//[分组] " + grmes + "\n";
		}
		GUItext += "//[工作台结束],\n";
		return GUItext;
	}
	
	public static string SaveConfig( MyObject o )
	{
		string Result = "";
		
		if( o is GNote ) {
			GNote cM = (GNote)o;
			
			Result += "//[描述],\n";
			Result += "//[名称] " + cM.Name + ",\n";
			Result += "//[代码] " + cM.isCode + ",\n";
			if( cM.ImagePath != null ) {
				Result += "//[图片] " + cM.ImagePath + ",\n";
			}
			Result += "//[颜色] " +
				cM.BackColor.R + "," + cM.BackColor.G + "," + cM.BackColor.B + "," +
				cM.EdgeColor.R + "," + cM.EdgeColor.G + "," + cM.EdgeColor.B + "," + 
				cM.ForeColor.R + "," + cM.ForeColor.G + "," + cM.ForeColor.B + ",\n";
			if( cM.CodeType == 0 ) {
				Result += "//[cx] False,\n";
			}
			else if( cM.CodeType == 1 ) {
				Result += "//[cx] True,\n";
			}
			else {
				Result += "//[cx] " + cM.CodeType + ",\n";
			}
			
			if( o.GroupMes != null ) {
				Result += "//[分组] " + o.GroupMes + ",\n";
			}
			Result += "//[坐标] X = " + cM.SX + ",\n";
			Result += "//[坐标] Y = " + cM.SY + ",\n";
			Result += "//[尺寸] Width = " + cM.Width + ",\n";
			Result += "//[尺寸] Height = " + cM.Height + ",\n";
			Result += "//[参数],\n";
			if( cM.Value != "" ) {
				string[] cut = cM.Value.Split( '\n' );
				for( int vi = 0; vi < cut.Length; ++vi ) {
					Result += "//" + cut[ vi ] + ",\n";
				}
			}
			Result += "//[参数结束],\n";
			Result += "//[描述结束],\n";
			return Result;
		}
		if( o is UIModule ) {
			UIModule cM = (UIModule)o;
			
			//添加事件链接
			string EventList = null;
			for( int k = 0; k < cM.EventList.Length; ++k ) {
				if( cM.EventList[ k ][ 0 ] != "" ) {
					EventList += "//[事件] " +
						cM.EventList[ k ][ 2 ] + " -> " +
						cM.EventList[ k ][ 0 ] + ",\n";
				}
			}
			//添加参数设置
			string VarList = null;
			for( int k = 0; k < cM.VarList.Length; ++k ) {
				if( cM.VarList[ k ][ 0 ] != "" ) {
					VarList += "//[参数] " + cM.VarList[ k ][ 2 ] + " = " + cM.VarList[ k ][ 0 ] + ",\n";
				}
			}
			//添加接口链接
			string LinkInterfaceList = null;
			for( int k = 0; k < cM.LinkInterfaceList.Length; ++k ) {
				if( cM.LinkInterfaceList[ k ][ 0 ] != "" ) {
					LinkInterfaceList += "//[接口] " +
						cM.LinkInterfaceList[ k ][ 2 ] + " -> " +
						cM.LinkInterfaceList[ k ][ 0 ] + ",\n";
				}
			}
			string UIModuleEXList = null;
			if( cM.UIModuleEXList != null ) {
				string[] exList = cM.UIModuleEXList;
				for( int exi = 0; exi < exList.Length; ++exi ) {
					if( exList[ exi ] == "背景颜色" ) {
						UIModuleEXList += "//[背景颜色] " + cM.BackColor.ToArgb() + ",\n";
					}
					else if( exList[ exi ] == "边框颜色" ) {
						UIModuleEXList += "//[边框颜色] " + cM.EdgeColor.ToArgb() + ",\n";
					}
					else if( exList[ exi ] == "文字颜色" ) {
						UIModuleEXList += "//[文字颜色] " + cM.ForeColor.ToArgb() + ",\n";
					}
					else if( exList[ exi ] == "文字" ) {
						UIModuleEXList += "//[文字] " + cM.Text + ",\n";
					}
					else if( exList[ exi ].StartsWith( "button+" ) ) {
						UIModuleEXList += "//[扩展参数] " + cM.ExtendValue + ",\n";
					}
					else if( exList[ exi ] == "字体" ) {
						UIModuleEXList += "//[字体] " + Common.GetStringFromFont( cM.TextFont ) + ",\n";
					}
					else {
						n_Debug.Warning.BUG( "<VarToText> 未处理的控件属性: " + exList[ exi ] );
					}
				}
			}
			Result += "//[组件],\n";
			Result += "//[组件路径] " + cM.IncPackFilePath + ",\n";
			Result += "//[名称] " + cM.Name + ",\n";
			if( o.GroupMes != null ) {
				Result += "//[分组] " + o.GroupMes + ",\n";
			}
			Result += "//[语言] " + cM.Language + ",\n";
			Result += EventList;
			Result += VarList;
			Result += LinkInterfaceList;
			Result += UIModuleEXList;
			Result += "//[坐标] X = " + cM.SX + ",\n";
			Result += "//[坐标] Y = " + cM.SY + ",\n";
			if( cM.isGUIControl() && cM.GetControlType() != ControlType.MachineArm ) {
				Result += "//[尺寸] Width = " + cM.Width + ",\n";
				//if( //cM.GetControlType() != ControlType.NumberBox &&
				//    cM.GetControlType() != ControlType.TrackBar
				//  ) {
				Result += "//[尺寸] Height = " + cM.Height + ",\n";
				//}
			}
			Result += "//[角度] " + cM.Angle + ",\n";
			Result += "//[组件结束],\n";
			return Result;
		}
		if( o is HardModule ) {
			HardModule cM = (HardModule)o;
			
			//添加事件链接
			string EventList = null;
			for( int k = 0; k < cM.EventList.Length; ++k ) {
				if( cM.EventList[ k ][ 0 ] != "" ) {
					EventList += "//[事件] " +
						cM.EventList[ k ][ 2 ] + " -> " +
						cM.EventList[ k ][ 0 ] + ",\n";
				}
			}
			//添加参数设置
			string VarList = null;
			for( int k = 0; k < cM.VarList.Length; ++k ) {
				if( cM.VarList[ k ][ 0 ] != "" ) {
					VarList += "//[参数] " + cM.VarList[ k ][ 2 ] + " = " + cM.VarList[ k ][ 0 ] + ",\n";
				}
			}
			//添加接口链接
			string LinkInterfaceList = null;
			for( int k = 0; k < cM.LinkInterfaceList.Length; ++k ) {
				if( cM.LinkInterfaceList[ k ][ 0 ] != "" ) {
					LinkInterfaceList += "//[接口] " +
						cM.LinkInterfaceList[ k ][ 2 ] + " -> " +
						cM.LinkInterfaceList[ k ][ 0 ] + ",\n";
				}
			}
			Result += "//[组件],\n";
			Result += "//[组件路径] " + cM.MacroFilePathPart + ",\n";
			Result += "//[名称] " + cM.Name + ",\n";
			if( o.GroupMes != null ) {
				Result += "//[分组] " + o.GroupMes + ",\n";
			}
			Result += "//[语言] " + cM.Language + ",\n";
			if( cM.PackOwner != null ) {
				Result += "//[封包] " + cM.PackOwner + ",\n";
			}
			Result += EventList;
			Result += VarList;
			Result += LinkInterfaceList;
			if( cM.ExtendValue != "" ) {
				Result += "//[扩展参数] " + cM.ExtendValue + ",\n";
			}
			if( cM.ExtendValue1 != "" ) {
				Result += "//[扩展参数1] " + cM.ExtendValue1 + ",\n";
			}
			Result += "//[坐标] X = " + cM.SX + ",\n";
			Result += "//[坐标] Y = " + cM.SY + ",\n";
			if( cM.ImageName == SPMoudleName.SYS_PADSHAP ) {
				Result += "//[尺寸] Width = " + cM.Width + ",\n";
				Result += "//[尺寸] Height = " + cM.Height + ",\n";
				Result += "//[颜色] " + cM.PadBackBrush.Color.R + " " + cM.PadBackBrush.Color.G + " " + cM.PadBackBrush.Color.B + ",\n";
			}
			Result += "//[角度] " + cM.Angle + ",\n";
			Result += "//[翻转] " + cM.SwapPort + ",\n";
			
			if( cM.InitTargetSObject != null) {
				
				Result += "//[仿真] " + cM.InitTargetSObject.MidX + "," + cM.InitTargetSObject.MidY + "," + cM.InitTargetSObject.Angle + "," + cM.InitTargetSObject.Visible + "," +
							cM.InitTargetSObject.Width + "," + cM.InitTargetSObject.Height + ",\n";
				
				if( cM.InitTargetSObject is n_Sprite.Sprite ) {
					n_Sprite.Sprite sp = (n_Sprite.Sprite)cM.InitTargetSObject;
					Result += "//[S_Image] " + sp.GetImagePathList() + ",\n";
					Result += "//[S_Value] " + sp.RolX + "," + sp.RolY + "," + sp.imgAngle + "," + sp.IIndex + ",\n";
					Result += "//[S_NotHit] " + sp.NotHitList + ",\n";
				}
				if( cM.TargetSObject is n_SimModule.SimModule ) {
					n_SimModule.SimModule sm = (n_SimModule.SimModule)cM.TargetSObject;
					Result += "//[仿真M] " + sm.SwapPin + "," + sm.StartPower + "," + sm.SpeedDown + "," + sm.Inertia + ",\n";
				}
			}
			Result += "//[键值] " + cM.GetBindKeyList() + ",\n";
			
			Result += "//[组件结束],\n";
			
			return Result;
		}
		if( o is GVar ) {
			GVar cM = (GVar)o;
			
			Result += "//[变量],\n";
			Result += "//[名称] " + cM.Name + ",\n";
			if( o.GroupMes != null ) {
				Result += "//[分组] " + o.GroupMes + ",\n";
			}
			Result += "//[常量] " + cM.isConst + ",\n";
			Result += "//[类型] " + cM.VarType + ",\n";
			Result += "//[存储] " + cM.StoreType + ",\n";
			Result += "//[参数],\n";
			if( cM.Value != "" ) {
				string[] cut = cM.Value.Split( '\n' );
				for( int vi = 0; vi < cut.Length; ++vi ) {
					Result += "//" + cut[ vi ] + ",\n";
				}
			}
			Result += "//[参数结束],\n";
			Result += "//[坐标] X = " + cM.SX + ",\n";
			Result += "//[坐标] Y = " + cM.SY + ",\n";
			if( cM.VarType == GType.g_bitmap || cM.VarType == GType.g_cbitmap ) {
				Result += "//[尺寸] Width = " + cM.BitmapWidth + ",\n";
				Result += "//[尺寸] Height = " + cM.BitmapHeight + ",\n";
			}
			if( cM.VarType == GType.g_font ) {
				Result += "//[尺寸] Width = " + cM.BitmapWidth + ",\n";
				Result += "//[尺寸] Height = " + cM.BitmapHeight + ",\n";
			}
			Result += "//[变量结束],\n";
			return Result;
		}
		if( !(o is MyIns) ) return null;
		
		MyIns ins = (MyIns)o;
		
		if( ins is ForeverIns ) {
			ForeverIns cM = (ForeverIns)ins;
			
			Result += "//[ForeverIns],\n";
			Result += "//[名称] " + cM.Name + ",\n";
			if( o.GroupMes != null ) {
				Result += "//[分组] " + o.GroupMes + ",\n";
			}
			Result += "//[禁用] " + cM.isNote + ",\n";
			Result += "//[坐标] X = " + cM.SX + ",\n";
			Result += "//[坐标] Y = " + cM.SY + ",\n";
			Result += "//[角度] " + cM.Angle + ",\n";
			Result += "//[/ForeverIns],\n";
			return Result;
		}
		if( ins is LoopIns ) {
			LoopIns cM = (LoopIns)ins;
			
			Result += "//[LoopIns],\n";
			Result += "//[名称] " + cM.Name + ",\n";
			if( o.GroupMes != null ) {
				Result += "//[分组] " + o.GroupMes + ",\n";
			}
			Result += "//[禁用] " + cM.isNote + ",\n";
			Result += "//[参数] " + cM.MyEXP.Get() + ",\n";
			Result += "//[坐标] X = " + cM.SX + ",\n";
			Result += "//[坐标] Y = " + cM.SY + ",\n";
			Result += "//[角度] " + cM.Angle + ",\n";
			Result += "//[/LoopIns],\n";
			return Result;
		}
		if( ins is ExtIns ) {
			ExtIns cM = (ExtIns)ins;
			
			Result += "//[ExtIns],\n";
			Result += "//[名称] " + cM.Name + ",\n";
			if( o.GroupMes != null ) {
				Result += "//[分组] " + o.GroupMes + ",\n";
			}
			Result += "//[禁用] " + cM.isNote + ",\n";
			Result += "//[参数] " + cM.MyEXP.Get() + ",\n";
			Result += "//[顺序] " + cM.ShunXu + ",\n";
			Result += "//[坐标] X = " + cM.SX + ",\n";
			Result += "//[坐标] Y = " + cM.SY + ",\n";
			Result += "//[角度] " + cM.Angle + ",\n";
			Result += "//[/ExtIns],\n";
			return Result;
		}
		if( ins is WhileIns ) {
			WhileIns cM = (WhileIns)ins;
			
			Result += "//[IfIns],\n";
			Result += "//[名称] " + cM.Name + ",\n";
			if( o.GroupMes != null ) {
				Result += "//[分组] " + o.GroupMes + ",\n";
			}
			Result += "//[禁用] " + cM.isNote + ",\n";
			Result += "//[参数] " + cM.MyEXP.Get() + ",\n";
			Result += "//[坐标] X = " + cM.SX + ",\n";
			Result += "//[坐标] Y = " + cM.SY + ",\n";
			Result += "//[角度] " + cM.Angle + ",\n";
			Result += "//[/IfIns],\n";
			return Result;
		}
		if( ins is IfElseIns ) {
			IfElseIns cM = (IfElseIns)ins;
			
			Result += "//[IfElseIns],\n";
			Result += "//[名称] " + cM.Name + ",\n";
			if( o.GroupMes != null ) {
				Result += "//[分组] " + o.GroupMes + ",\n";
			}
			Result += "//[禁用] " + cM.isNote + ",\n";
			Result += "//[参数] " + cM.MyEXP.Get() + ",\n";
			Result += "//[坐标] X = " + cM.SX + ",\n";
			Result += "//[坐标] Y = " + cM.SY + ",\n";
			Result += "//[角度] " + cM.Angle + ",\n";
			Result += "//[/IfElseIns],\n";
			return Result;
		}
		if( ins is ElseIns ) {
			ElseIns cM = (ElseIns)ins;
			
			Result += "//[ElseIns],\n";
			Result += "//[名称] " + cM.Name + ",\n";
			if( o.GroupMes != null ) {
				Result += "//[分组] " + o.GroupMes + ",\n";
			}
			Result += "//[禁用] " + cM.isNote + ",\n";
			Result += "//[坐标] X = " + cM.SX + ",\n";
			Result += "//[坐标] Y = " + cM.SY + ",\n";
			Result += "//[角度] " + cM.Angle + ",\n";
			Result += "//[/ElseIns],\n";
			return Result;
		}
		if( ins is CondiEndIns ) {
			CondiEndIns cM = (CondiEndIns)ins;
			
			Result += "//[CondiEndIns],\n";
			Result += "//[名称] " + cM.Name + ",\n";
			if( o.GroupMes != null ) {
				Result += "//[分组] " + o.GroupMes + ",\n";
			}
			Result += "//[禁用] " + cM.isNote + ",\n";
			Result += "//[坐标] X = " + cM.SX + ",\n";
			Result += "//[坐标] Y = " + cM.SY + ",\n";
			Result += "//[角度] " + cM.Angle + ",\n";
			Result += "//[/CondiEndIns],\n";
			return Result;
		}
		if( ins is WaitIns ) {
			WaitIns cM = (WaitIns)ins;
			
			Result += "//[WaitIns],\n";
			Result += "//[名称] " + cM.Name + ",\n";
			if( o.GroupMes != null ) {
				Result += "//[分组] " + o.GroupMes + ",\n";
			}
			Result += "//[禁用] " + cM.isNote + ",\n";
			Result += "//[参数] " + cM.MyEXP.Get() + ",\n";
			Result += "//[坐标] X = " + cM.SX + ",\n";
			Result += "//[坐标] Y = " + cM.SY + ",\n";
			Result += "//[角度] " + cM.Angle + ",\n";
			Result += "//[/WaitIns],\n";
			return Result;
		}
		if( ins is EventIns ) {
			EventIns cM = (EventIns)ins;
			
			Result += "//[EventIns],\n";
			Result += "//[名称] " + cM.Name + ",\n";
			if( o.GroupMes != null ) {
				Result += "//[分组] " + o.GroupMes + ",\n";
			}
			Result += "//[禁用] " + cM.isNote + ",\n";
			Result += "//[参数] " + cM.EventEntry + ",\n";
			Result += "//[坐标] X = " + cM.SX + ",\n";
			Result += "//[坐标] Y = " + cM.SY + ",\n";
			Result += "//[角度] " + cM.Angle + ",\n";
			Result += "//[展开] " + cM.ShowFollow + ",\n";
			Result += "//[/EventIns],\n";
			return Result;
		}
		if( ins is UserFunctionIns ) {
			UserFunctionIns cM = (UserFunctionIns)ins;
			
			Result += "//[UserFunctionIns],\n";
			Result += "//[名称] " + cM.Name + ",\n";
			if( o.GroupMes != null ) {
				Result += "//[分组] " + o.GroupMes + ",\n";
			}
			Result += "//[禁用] " + cM.isNote + ",\n";
			if( cM.isEvent ) {
				if( cM.Target != null ) {
					Result += "//[映射] " + cM.Target.Name + ",\n";
				}
				else {
					Result += "//[映射] null,\n";
				}
			}
			Result += "//[参数] " + cM.UserName + ",\n";
			Result += "//[坐标] X = " + cM.SX + ",\n";
			Result += "//[坐标] Y = " + cM.SY + ",\n";
			Result += "//[角度] " + cM.Angle + ",\n";
			Result += "//[展开] " + cM.ShowFollow + ",\n";
			Result += "//[/UserFunctionIns],\n";
			return Result;
		}
		if( ins is FuncIns ) {
			FuncIns cM = (FuncIns)ins;
			
			Result += "//[FuncIns],\n";
			Result += "//[名称] " + cM.Name + ",\n";
			if( o.GroupMes != null ) {
				Result += "//[分组] " + o.GroupMes + ",\n";
			}
			Result += "//[禁用] " + cM.isNote + ",\n";
			Result += "//[参数] " + cM.MyEXP.Get() + ",\n";
			Result += "//[坐标] X = " + cM.SX + ",\n";
			Result += "//[坐标] Y = " + cM.SY + ",\n";
			Result += "//[角度] " + cM.Angle + ",\n";
			Result += "//[/FuncIns],\n";
			return Result;
		}
		return null;
	}
	
	public static string SaveInsLink( MyIns mi )
	{
		string s = null;
		try {
		if( mi.PreIns != null ) {
			s += "//[指令链接] " + mi.Name + " PreIns -> " + mi.PreIns.Name + ",\n";
		}
		if( mi.NextIns != null ) {
			s += "//[指令链接] " + mi.Name + " NextIns -> " + mi.NextIns.Name + ",\n";
		}
		if( mi is EventIns ) {
			EventIns ii = (EventIns)mi;
			s += "//[Event结构链接] " + mi.Name + " CondiEnd -> " + ii.InsEnd.Name + ",\n";
		}
		if( mi is UserFunctionIns ) {
			UserFunctionIns ii = (UserFunctionIns)mi;
			s += "//[UserFunction结构链接] " + mi.Name + " CondiEnd -> " + ii.InsEnd.Name + ",\n";
		}
		if( mi is ForeverIns ) {
			ForeverIns ii = (ForeverIns)mi;
			s += "//[Forever结构链接] " + mi.Name + " CondiEnd -> " + ii.InsEnd.Name + ",\n";
		}
		if( mi is LoopIns ) {
			LoopIns ii = (LoopIns)mi;
			s += "//[Loop结构链接] " + mi.Name + " CondiEnd -> " + ii.InsEnd.Name + ",\n";
		}
		if( mi is ExtIns ) {
			ExtIns ii = (ExtIns)mi;
			s += "//[Ext结构链接] " + mi.Name + " CondiEnd -> " + ii.InsEnd.Name + ",\n";
		}
		if( mi is WhileIns ) {
			WhileIns ii = (WhileIns)mi;
			s += "//[While结构链接] " + mi.Name + " CondiEnd -> " + ii.InsEnd.Name + ",\n";
		}
		if( mi is IfElseIns ) {
			IfElseIns ii = (IfElseIns)mi;
			if( ii.ElseIns != null ) {
				s += "//[IfElse结构链接] " + mi.Name + " ElseIns -> " + ii.ElseIns.Name + ",\n";
				s += "//[Else结构链接] " + ii.ElseIns.Name + " CondiEnd -> " + ii.ElseIns.InsEnd.Name + ",\n";
			}
			else {
				s += "//[IfElseEnd结构链接] " + mi.Name + " InsEnd -> " + ii.InsEnd.Name + ",\n";
			}
		}
		}catch(Exception e) {
			n_Debug.Warning.BUG( "指令序列化出错: " + e.ToString() );
		}
		return s;
	}
	
	//初始化控制器列表
	static void ClearControlList( ImagePanel GPanel )
	{
		isVmProgram = false;
		
		ControlList = new MyObject[ 100 ];
		ControlNumber = 0;
		
		//搜索全部控制器
		foreach( MyObject mo in GPanel.myModuleList ) {
			mo.ChipIndex = 0;
			if( mo is HardModule ) {
				HardModule m = (HardModule)mo;
				if( m.isControlModule ) {
					ControlList[ ControlNumber ] = m;
					m.ChipIndex = ControlNumber;
					++ControlNumber;
					
					if( m.PyValue.IndexOf( ";py;" ) != -1 ) {
						//isPyMode = true;
					}
				}
			}
		}
		//搜索全部控制器
		if( ControlNumber == 0 ) {
			isVmProgram = true;
			foreach( MyObject mo in GPanel.myModuleList ) {
				mo.ChipIndex = 0;
				if( mo is MyFileObject ) {
					MyFileObject m = (MyFileObject)mo;
					if( m.isControlModule ) {
						ControlList[ ControlNumber ] = m;
						m.ChipIndex = ControlNumber;
						++ControlNumber;
					}
				}
			}
		}
		//当没有硬件主控板时, 默认处理为纯动画编程
		if( ControlNumber == 0 ) {
			ControlNumber = 1;
		}
		
		//根据端口连接设置组件所在的芯片序号
		foreach( MyObject mo in GPanel.myModuleList ) {
			if( mo is HardModule ) {
				HardModule h = (HardModule)mo;
				if( h.isControlModule ) {
					continue;
				}
				if( h.PORTList.Length == 0 ) {
					continue;
				}
				Port p = h.PORTList[ 0 ];
				if( p.ClientType == PortClientType.MASTER || p.TargetPort == null ) {
					continue;
				}
				h.ChipIndex = p.TargetPort.Owner.ChipIndex;
			}
		}
		//根据接口连接设置组件所在的芯片序号
		foreach( MyObject mo in GPanel.myModuleList ) {
			if( mo is HardModule ) {
				HardModule h = (HardModule)mo;
				if( h.LinkInterfaceList.Length == 0 ) {
					continue;
				}
				string TargetName = h.LinkInterfaceList[ 0 ][ 0 ].Split( '.' )[ 0 ];
				if( TargetName == "" ) {
					continue;
				}
				MyObject m = GPanel.myModuleList.GetModuleFromName( TargetName );
				
				//h.ChipIndex = m.ChipIndex;
			}
		}
		//这里暂时不允许多个主控板
		if( ControlNumber > 1 ) {
			n_Debug.Warning.AddClashMessage( "每个程序中最多只能有一个主控模块" );
		}
	}
	
	//设置出错信息和出错点
	public static void SetErrorMes( MyObject m, string mes )
	{
		ExistError = true;
		ErrorMessage = mes;
		ErrorObject = m;
	}
	
	//获取放缩值
	static int GetPix( int w )
	{
		return w;
		//return (int)(w * GUIset.SYS_H / Cur_H);
	}
	
	//---------------------------------------------------------------
	public static bool UseBindKey;
	public static int UseBindKeyIndex;
	
	//桌面参数配置
	static int TableConfig( ImagePanel GPanel, string[][] ConfigList, int i )
	{
		UseBindKey = false;
		string Version = null;
		
		while( i < ConfigList.Length ) {
			
			string[] ConfigCmdCut = ConfigList[ i ];
			
			if( ConfigCmdCut[ 0 ] == "[工作台]" ) {
				//无操作,忽略
			}
			else if( ConfigCmdCut[ 0 ] == "[工作台结束]" ) {
				break;
			}
			else if( ConfigCmdCut[ 0 ] == "[坐标]" ) {
				if( ConfigCmdCut[ 1 ] == "X" ) {
					GPanel.StartX = int.Parse( ConfigCmdCut[ 3 ] );
				}
				else if( ConfigCmdCut[ 1 ] == "Y" ) {
					GPanel.StartY = int.Parse( ConfigCmdCut[ 3 ] );
				}
				else {
					n_Debug.Warning.BUG( "<工作台配置> 未知的配置项: " + ConfigCmdCut[ 1 ] );
				}
			}
			else if( ConfigCmdCut[ 0 ] == "[自动布局]" ) {
				//GPanel.myModuleList.AutoLocate = ConfigCmdCut[ 1 ] == "True";
			}
			else if( ConfigCmdCut[ 0 ] == "[语言]" ) {
				GPanel.myModuleList.Language = ConfigCmdCut[ 1 ];
			}
			else if( ConfigCmdCut[ 0 ] == "[模块放缩]" ) {
				//HardModule.Scale = float.Parse( ConfigCmdCut[ 1 ] );
			}
			else if( ConfigCmdCut[ 0 ] == "[软件加速]" ) {
				GPanel.voffset = int.Parse( ConfigCmdCut[ 1 ] );
			}
			else if( ConfigCmdCut[ 0 ] == "[RAM节约]" ) {
				GPanel.RAM_Save = int.Parse( ConfigCmdCut[ 1 ] );
			}
			else if( ConfigCmdCut[ 0 ] == "[VEX驱动周期]" ) {
				GPanel.VEX_Tick = int.Parse( ConfigCmdCut[ 1 ] );
			}
			else if( ConfigCmdCut[ 0 ] == "[动态内存]" ) {
				GPanel.DRAM_Block = int.Parse( ConfigCmdCut[ 1 ] );
			}
			else if( ConfigCmdCut[ 0 ] == "[隐藏导线]" ) {
				GPanel.HideLine = int.Parse( ConfigCmdCut[ 1 ] );
			}
			else if( ConfigCmdCut[ 0 ] == "[隐藏名称]" ) {
				GPanel.HideName = ConfigCmdCut[ 1 ] == "True";
			}
			else if( ConfigCmdCut[ 0 ] == "[连线拟合]" ) {
				
				if( !PasteMode ) {
					Port.C_Rol = int.Parse( ConfigCmdCut[ 1 ] );
				}
			}
			else if( ConfigCmdCut[ 0 ] == "[事件池]" ) {
				GPanel.EventNumber = int.Parse( ConfigCmdCut[ 1 ] );
			}
			else if( ConfigCmdCut[ 0 ] == "[EventVarStack]" ) {
				GPanel.EventVarStack = int.Parse( ConfigCmdCut[ 1 ] );
			}
			else if( ConfigCmdCut[ 0 ] == "[EventFuncStack]" ) {
				GPanel.EventFuncStack = int.Parse( ConfigCmdCut[ 1 ] );
			}
			else if( ConfigCmdCut[ 0 ] == "[界面类型]" ) {
				char c = ConfigCmdCut[ 1 ][ 0 ];
				if( n_ModuleLibPanel.MyModuleLibPanel.LabType == n_GUIcoder.labType.Default ) {
					n_ModuleLibPanel.MyModuleLibPanel.LabType = c;
				}
				else if( c != n_ModuleLibPanel.MyModuleLibPanel.LabType ) {
					n_Debug.Warning.AddErrMessage( "文件类型和当前界面类型不同, 建议此刻关闭软件, 再双击当前文件打开: " + c + "/" + n_ModuleLibPanel.MyModuleLibPanel.LabType );
				}
			}
			else if( ConfigCmdCut[ 0 ] == "[分组]" ) {
				GPanel.tempGroupMes = ConfigCmdCut[ 1 ];
			}
			else if( ConfigCmdCut[ 0 ] == "[操作系统]" ) {
				GPanel.OSPath = ConfigCmdCut[ 1 ];
			}
			else if( ConfigCmdCut[ 0 ] == "[系统信息]" ) {
				GPanel.SysMes = ConfigCmdCut[ 1 ];
			}
			else if( ConfigCmdCut[ 0 ] == "[用户信息]" ) {
				GPanel.UserMes = ConfigCmdCut[ 1 ].Replace( "\\n", "\n" ).Replace( "\\s", " " );
			}
			//else if( ConfigCmdCut[ 0 ] == "[DPI]" ) {
			//	Cur_H = float.Parse( ConfigCmdCut[ 1 ] );
			//}
			else if( ConfigCmdCut[ 0 ] == "[版本号]" ) {
				Version = ConfigCmdCut[ 1 ];
			}
			else {
				ConfigError( "<工作台配置> 未知的配置项: " + ConfigCmdCut[ 0 ] );
			}
			++i;
		}
		//检查版本号
		if( !G.isCruxEXE && Version != null && !G.ccode.isStartFile && G.Version != Version ) {
			string[] sc = G.Version.Split( '.' );
			string[] cc = Version.Split( '.' );
			
			int igc0 = int.Parse( sc[0] );
			int igc1 = int.Parse( sc[1] );
			int igc2 = int.Parse( sc[2] );
			
			int icc0 = int.Parse( cc[0] );
			int icc1 = int.Parse( cc[1] );
			int icc2 = int.Parse( cc[2] );
			
			if( igc0 < icc0 || (igc0 == icc0) && igc1 < icc1 ||  (igc0 == icc0) && (igc1 == icc1) && igc2 < icc2 ) {
				n_Debug.Warning.WarningMessage = n_Language.Language.SYS_VersionMesA1 + n_Language.Language.SYS_VersionMes2 + G.Version + n_Language.Language.SYS_VersionMes3 + Version + n_Language.Language.SYS_VersionMesA2;
			}
			else {
				n_Debug.Warning.WarningMessage = n_Language.Language.SYS_VersionMesB1 + n_Language.Language.SYS_VersionMes2 + G.Version + n_Language.Language.SYS_VersionMes3 + Version + n_Language.Language.SYS_VersionMesB2;
			}
		}
		return i;
	}
	
	//地图界面配置
	static int MapConfig( MyObjectList myModuleList, string[][] ConfigList, int i )
	{
		string IFile = null;
		string BackImg = null;
		int StartX = 0;
		int StartY = 0;
		int gui_Width = 0;
		int gui_Height = 0;
		int DotNumber = 0;
		
		float CamMidX = 0;
		float CamMidY = 0;
		float CamScale = 0;
		float CamAngle = 0;
		int UWidth = 0;
		int UHeight = 0;
		Color BackColor = Color.White;
		bool ExistCam = false;
		Rectangle sz = new Rectangle();
		
		while( i < ConfigList.Length ) {
			
			string[] ConfigCmdCut = ConfigList[ i ];
			
			if( ConfigCmdCut[ 0 ] == "[地图]" ) {
				//...
			}
			else if( ConfigCmdCut[ 0 ] == "[地图结束]" ) {
				break;
			}
			else if( ConfigCmdCut[ 0 ] == "[路径]" ) {
				ConfigCmdCut[ 0 ] = "";
				IFile = string.Join( " ", ConfigCmdCut ).Trim( ' ' );
			}
			else if( ConfigCmdCut[ 0 ] == "[背景图]" ) {
				ConfigCmdCut[ 0 ] = "";
				BackImg = string.Join( " ", ConfigCmdCut ).Trim( ' ' );
			}
			else if( ConfigCmdCut[ 0 ] == "[背景图属性]" ) {
				int x = int.Parse( ConfigCmdCut[ 1 ] );
				int y = int.Parse( ConfigCmdCut[ 2 ] );
				int w = int.Parse( ConfigCmdCut[ 3 ] );
				int h = int.Parse( ConfigCmdCut[ 4 ] );
				sz.X = x;
				sz.Y = y;
				sz.Width = w;
				sz.Height = h;
			}
			else if( ConfigCmdCut[ 0 ] == "[坐标]" ) {
				if( ConfigCmdCut[ 1 ] == "X" ) {
					StartX = int.Parse( ConfigCmdCut[ 3 ] );
				}
				else if( ConfigCmdCut[ 1 ] == "Y" ) {
					StartY = int.Parse( ConfigCmdCut[ 3 ] );
				}
				else {
					n_Debug.Warning.BUG( "<MapConfig> 未知的坐标配置项: " + ConfigCmdCut[ 1 ] );
				}
			}
			else if( ConfigCmdCut[ 0 ] == "[尺寸]" ) {
				if( ConfigCmdCut[ 1 ] == "Width" ) {
					gui_Width = int.Parse( ConfigCmdCut[ 3 ] );
				}
				else if( ConfigCmdCut[ 1 ] == "height" ) {
					gui_Height = int.Parse( ConfigCmdCut[ 3 ] );
				}
				else {
					n_Debug.Warning.BUG( "<MapConfig> 未知的尺寸配置项: " + ConfigCmdCut[ 1 ] );
				}
			}
			else if( ConfigCmdCut[ 0 ] == "[放大]" ) {
				DotNumber = int.Parse( ConfigCmdCut[ 1 ] );
			}
			else if( ConfigCmdCut[ 0 ] == "[颜色]" ) {
				int rr = int.Parse( ConfigCmdCut[ 1 ] );
				int gg = int.Parse( ConfigCmdCut[ 2 ] );
				int bb = int.Parse( ConfigCmdCut[ 3 ] );
				BackColor = Color.FromArgb( rr, gg, bb );
			}
			else if( ConfigCmdCut[ 0 ] == "[CamMidX]" ) {
				CamMidX = float.Parse( ConfigCmdCut[ 1 ] );
				ExistCam = true;
			}
			else if( ConfigCmdCut[ 0 ] == "[CamMidY]" ) {
				CamMidY = float.Parse( ConfigCmdCut[ 1 ] );
			}
			else if( ConfigCmdCut[ 0 ] == "[CamScale]" ) {
				CamScale = float.Parse( ConfigCmdCut[ 1 ] );
			}
			else if( ConfigCmdCut[ 0 ] == "[CamAngle]" ) {
				CamAngle = float.Parse( ConfigCmdCut[ 1 ] );
			}
			else if( ConfigCmdCut[ 0 ] == "[宽度]" ) {
				UWidth = int.Parse( ConfigCmdCut[ 1 ] );
			}
			else if( ConfigCmdCut[ 0 ] == "[高度]" ) {
				UHeight = int.Parse( ConfigCmdCut[ 1 ] );
			}
			else {
				ConfigError( "<MapConfig> 未知的配置项: " + ConfigCmdCut[ 0 ] );
			}
			++i;
		}
		
		G.SimBox.SetInitScale( DotNumber, IFile, BackImg, sz, ExistCam, CamMidX, CamMidY, CamScale, CamAngle, BackColor, UWidth, UHeight );
		return i;
	}
	
	//描述配置
	static int NoteConfig( MyObjectList myModuleList, string[][] ConfigList, int i )
	{
		string Name = null;
		string GroupMes = null;
		string Value = "";
		int StartX = 0;
		int StartY = 0;
		int gui_Width = 0;
		int gui_Height = 0;
		bool isCode = false;
		int isCX = 0;
		string IFile = null;
		GNote cVar = new GNote();
		while( i < ConfigList.Length ) {
			
			string[] ConfigCmdCut = ConfigList[ i ];
			
			if( ConfigCmdCut[ 0 ] == "[描述]" ) {
				//...
			}
			else if( ConfigCmdCut[ 0 ] == "[描述结束]" ) {
				break;
			}
			else if( ConfigCmdCut[ 0 ] == "[名称]" ) {
				Name = ConfigCmdCut[ 1 ];
			}
			else if( ConfigCmdCut[ 0 ] == "[图片]" ) {
				ConfigCmdCut[ 0 ] = "";
				IFile = string.Join( " ", ConfigCmdCut ).Trim( ' ' );
			}
			else if( ConfigCmdCut[ 0 ] == "[颜色]" ) {
				string[] list = ConfigCmdCut[ 1 ].Split( ',' );
				cVar.BackColor = Color.FromArgb( int.Parse( list[0] ), int.Parse( list[1] ), int.Parse( list[2] ) );
				cVar.EdgeColor = Color.FromArgb( int.Parse( list[3] ), int.Parse( list[4] ), int.Parse( list[5] ) );
				cVar.ForeColor = Color.FromArgb( int.Parse( list[6] ), int.Parse( list[7] ), int.Parse( list[8] ) );
			}
			else if( ConfigCmdCut[ 0 ] == "[代码]" ) {
				if( ConfigCmdCut[ 1 ] == "True" ) {
					isCode = true;
				}
				else {
					isCode = false;
				}
			}
			else if( ConfigCmdCut[ 0 ] == "[cx]" ) {
				if( ConfigCmdCut[ 1 ] == "False" ) {
					isCX = 0;
				}
				else if( ConfigCmdCut[ 1 ] == "True" ) {
					isCX = 1;
				}
				else {
					isCX = int.Parse( ConfigCmdCut[ 1 ] );
				}
			}
			else if( ConfigCmdCut[ 0 ] == "[分组]" ) {
				GroupMes = ConfigCmdCut[ 1 ];
			}
			else if( ConfigCmdCut[ 0 ] == "[坐标]" ) {
				if( ConfigCmdCut[ 1 ] == "X" ) {
					StartX = int.Parse( ConfigCmdCut[ 3 ] );
				}
				else if( ConfigCmdCut[ 1 ] == "Y" ) {
					StartY = int.Parse( ConfigCmdCut[ 3 ] );
				}
				else {
					n_Debug.Warning.BUG( "<ModuleConfig> 未知的坐标配置项: " + ConfigCmdCut[ 1 ] );
				}
			}
			else if( ConfigCmdCut[ 0 ] == "[尺寸]" ) {
				if( ConfigCmdCut[ 1 ] == "Width" ) {
					gui_Width = int.Parse( ConfigCmdCut[ 3 ] );
				}
				else if( ConfigCmdCut[ 1 ] == "Height" ) {
					gui_Height = int.Parse( ConfigCmdCut[ 3 ] );
				}
				else {
					n_Debug.Warning.BUG( "<ModuleConfig> 未知的尺寸配置项: " + ConfigCmdCut[ 1 ] );
				}
			}
			else if( ConfigCmdCut[ 0 ] == "[参数]" ) {
				for( int n = i + 1; n < ConfigList.Length; ++n ) {
					if( ConfigList[ n ][ 0 ] == "[参数结束]" ) {
						i = n;
						Value = Value .TrimEnd( '\n' );
						break;
					}
					Value += string.Join( " ", ConfigList[ n ] ) + "\n";
				}
			}
			else {
				ConfigError( "<ModuleConfig> 未知的配置项: " + ConfigCmdCut[ 0 ] );
			}
			++i;
		}
		cVar.isCode = isCode;
		cVar.CodeType = isCX;
		cVar.SetUserValue( Name, GroupMes, Value, GetPix(StartX), GetPix(StartY), gui_Width, gui_Height );
		cVar.SetImage( IFile );
		myModuleList.Add( cVar );
		return i;
	}
	
	//变量配置
	static int VarConfig( MyObjectList myModuleList, string[][] ConfigList, int i )
	{
		string Name = null;
		string GroupMes = null;
		string Value = "";
		string VarType = null;
		string StoreType = "";
		bool isConst = false;
		int StartX = 0;
		int StartY = 0;
		int Width = 0;
		int Height = 0;
		GVar cVar = new GVar();
		while( i < ConfigList.Length ) {
			
			string[] ConfigCmdCut = ConfigList[ i ];
			
			if( ConfigCmdCut[ 0 ] == "[变量]" ) {
				//...
			}
			else if( ConfigCmdCut[ 0 ] == "[变量结束]" ) {
				break;
			}
			else if( ConfigCmdCut[ 0 ] == "[名称]" ) {
				Name = ConfigCmdCut[ 1 ];
			}
			else if( ConfigCmdCut[ 0 ] == "[分组]" ) {
				GroupMes = ConfigCmdCut[ 1 ];
			}
			else if( ConfigCmdCut[ 0 ] == "[常量]" ) {
				if( ConfigCmdCut[ 1 ] == "True" ) {
					isConst = true;
				}
				else {
					isConst = false;
				}
			}
			else if( ConfigCmdCut[ 0 ] == "[类型]" ) {
				VarType = ConfigCmdCut[ 1 ];
			}
			else if( ConfigCmdCut[ 0 ] == "[存储]" ) {
				StoreType = ConfigCmdCut[ 1 ];
			}
			else if( ConfigCmdCut[ 0 ] == "[坐标]" ) {
				if( ConfigCmdCut[ 1 ] == "X" ) {
					StartX = int.Parse( ConfigCmdCut[ 3 ] );
				}
				else if( ConfigCmdCut[ 1 ] == "Y" ) {
					StartY = int.Parse( ConfigCmdCut[ 3 ] );
				}
				else {
					n_Debug.Warning.BUG( "<ModuleConfig> 未知的坐标配置项: " + ConfigCmdCut[ 1 ] );
				}
			}
			else if( ConfigCmdCut[ 0 ] == "[尺寸]" ) {
				if( ConfigCmdCut[ 1 ] == "Width" ) {
					Width = int.Parse( ConfigCmdCut[ 3 ] );
				}
				else if( ConfigCmdCut[ 1 ] == "Height" ) {
					Height = int.Parse( ConfigCmdCut[ 3 ] );
				}
				else {
					n_Debug.Warning.BUG( "<ModuleConfig> 未知的坐标配置项: " + ConfigCmdCut[ 1 ] );
				}
			}
			else if( ConfigCmdCut[ 0 ] == "[参数]" ) {
				for( int n = i + 1; n < ConfigList.Length; ++n ) {
					if( ConfigList[ n ][ 0 ] == "[参数结束]" ) {
						i = n;
						Value = Value .TrimEnd( '\n' );
						break;
					}
					Value += string.Join( " ", ConfigList[ n ] ) + "\n";
				}
			}
			else {
				ConfigError( "<ModuleConfig> 未知的配置项: " + ConfigCmdCut[ 0 ] );
			}
			++i;
		}
		cVar.SetUserValue( Name, GroupMes, VarType, StoreType, Value, isConst, GetPix(StartX), GetPix(StartY), Width, Height );
		myModuleList.Add( cVar );
		return i;
	}
	
	//---------------------------------------------------------------
	
	//UserFunctionIns配置
	static int UserFunctionInsConfig( MyObjectList myModuleList, string[][] ConfigList, int i )
	{
		string Name = null;
		string GroupMes = null;
		string Mes = null;
		int StartX = 0;
		int StartY = 0;
		int Angle = 0;
		bool ShowFollow = true;
		bool isEvent = false;
		bool isNote = false;
		string tempTargetName = null;
		UserFunctionIns lm = null;
		while( i < ConfigList.Length ) {
			
			string[] ConfigCmdCut = ConfigList[ i ];
			
			if( ConfigCmdCut[ 0 ] == "[UserFunctionIns]" ) {
				lm = new UserFunctionIns();
			}
			else if( ConfigCmdCut[ 0 ] == "[/UserFunctionIns]" ) {
				break;
			}
			else if( ConfigCmdCut[ 0 ] == "[名称]" ) {
				Name = ConfigCmdCut[ 1 ];
			}
			else if( ConfigCmdCut[ 0 ] == "[分组]" ) {
				GroupMes = ConfigCmdCut[ 1 ];
			}
			else if( ConfigCmdCut[ 0 ] == "[禁用]" ) {
				if( ConfigCmdCut[ 1 ] == "True" ) {
					isNote = true;
				}
				else {
					isNote = false;
				}
			}
			else if( ConfigCmdCut[ 0 ] == "[映射]" ) {
				isEvent = true;
				tempTargetName = ConfigCmdCut[ 1 ];
			}
			else if( ConfigCmdCut[ 0 ] == "[参数]" ) {
				for( int j = 1; j < ConfigCmdCut.Length; ++j ) {
					Mes += ConfigCmdCut[ j ];
					if( j != ConfigCmdCut.Length - 1 ) {
						Mes += " ";
					}
				}
			}
			else if( ConfigCmdCut[ 0 ] == "[角度]" ) {
				Angle = int.Parse( ConfigCmdCut[ 1 ] );
			}
			else if( ConfigCmdCut[ 0 ] == "[坐标]" ) {
				if( ConfigCmdCut[ 1 ] == "X" ) {
					StartX = int.Parse( ConfigCmdCut[ 3 ] );
				}
				else if( ConfigCmdCut[ 1 ] == "Y" ) {
					StartY = int.Parse( ConfigCmdCut[ 3 ] );
				}
				else {
					n_Debug.Warning.BUG( "<UserFunctionInsConfig> 未知的坐标配置项: " + ConfigCmdCut[ 1 ] );
				}
			}
			else if( ConfigCmdCut[ 0 ] == "[展开]" ) {
				ShowFollow = ConfigCmdCut[ 1 ] == "True";
			}
			else {
				ConfigError( "<UserFunctionInsConfig> 未知的配置项: " + ConfigCmdCut[ 0 ] );
			}
			++i;
		}
		lm.isNote = isNote;
		lm.SetUserValue( Name, GroupMes, Mes, GetPix(StartX), GetPix(StartY), Angle, ShowFollow );
		lm.tempTargetName = tempTargetName;
		lm.isEvent = isEvent;
		myModuleList.Add( lm );
		return i;
	}
	
	//EventIns配置
	static int EventInsConfig( MyObjectList myModuleList, string[][] ConfigList, int i )
	{
		string Name = null;
		string GroupMes = null;
		string Mes = null;
		int StartX = 0;
		int StartY = 0;
		int Angle = 0;
		bool isNote = false;
		bool ShowFollow = true;
		EventIns lm = null;
		while( i < ConfigList.Length ) {
			
			string[] ConfigCmdCut = ConfigList[ i ];
			
			if( ConfigCmdCut[ 0 ] == "[EventIns]" ) {
				lm = new EventIns();
			}
			else if( ConfigCmdCut[ 0 ] == "[/EventIns]" ) {
				break;
			}
			else if( ConfigCmdCut[ 0 ] == "[名称]" ) {
				Name = ConfigCmdCut[ 1 ];
			}
			else if( ConfigCmdCut[ 0 ] == "[分组]" ) {
				GroupMes = ConfigCmdCut[ 1 ];
			}
			else if( ConfigCmdCut[ 0 ] == "[禁用]" ) {
				if( ConfigCmdCut[ 1 ] == "True" ) {
					isNote = true;
				}
				else {
					isNote = false;
				}
			}
			else if( ConfigCmdCut[ 0 ] == "[参数]" ) {
				for( int j = 1; j < ConfigCmdCut.Length; ++j ) {
					Mes += ConfigCmdCut[ j ];
					if( j != ConfigCmdCut.Length - 1 ) {
						Mes += " ";
					}
				}
				//转换旧版本事件名称
				if( Mes.IndexOf( "_系统空闲时" ) != -1 ) {
					string s = Mes.Replace( "_系统空闲时", "_反复执行" );
					Mes = s;
				}
				if( Mes.IndexOf( "_系统启动时" ) != -1 ) {
					string s = Mes.Replace( "_系统启动时", "_初始化" );
					Mes = s;
				}
			}
			else if( ConfigCmdCut[ 0 ] == "[角度]" ) {
				Angle = int.Parse( ConfigCmdCut[ 1 ] );
			}
			else if( ConfigCmdCut[ 0 ] == "[坐标]" ) {
				if( ConfigCmdCut[ 1 ] == "X" ) {
					StartX = int.Parse( ConfigCmdCut[ 3 ] );
				}
				else if( ConfigCmdCut[ 1 ] == "Y" ) {
					StartY = int.Parse( ConfigCmdCut[ 3 ] );
				}
				else {
					n_Debug.Warning.BUG( "<EventInsConfig> 未知的坐标配置项: " + ConfigCmdCut[ 1 ] );
				}
			}
			else if( ConfigCmdCut[ 0 ] == "[展开]" ) {
				ShowFollow = ConfigCmdCut[ 1 ] == "True";
			}
			else {
				ConfigError( "<EventInsConfig> 未知的配置项: " + ConfigCmdCut[ 0 ] );
			}
			++i;
		}
		lm.isNote = isNote;
		lm.SetUserValue( Name, GroupMes, Mes, GetPix(StartX), GetPix(StartY), Angle, ShowFollow );
		myModuleList.Add( lm );
		return i;
	}
	
	//WaitIns配置
	static int WaitInsConfig( MyObjectList myModuleList, string[][] ConfigList, int i )
	{
		string Name = null;
		string GroupMes = null;
		string Mes = null;
		int StartX = 0;
		int StartY = 0;
		int Angle = 0;
		bool isNote = false;
		WaitIns lm = null;
		while( i < ConfigList.Length ) {
			
			string[] ConfigCmdCut = ConfigList[ i ];
			
			if( ConfigCmdCut[ 0 ] == "[WaitIns]" ) {
				lm = new WaitIns();
			}
			else if( ConfigCmdCut[ 0 ] == "[/WaitIns]" ) {
				break;
			}
			else if( ConfigCmdCut[ 0 ] == "[名称]" ) {
				Name = ConfigCmdCut[ 1 ];
			}
			else if( ConfigCmdCut[ 0 ] == "[分组]" ) {
				GroupMes = ConfigCmdCut[ 1 ];
			}
			else if( ConfigCmdCut[ 0 ] == "[禁用]" ) {
				if( ConfigCmdCut[ 1 ] == "True" ) {
					isNote = true;
				}
				else {
					isNote = false;
				}
			}
			else if( ConfigCmdCut[ 0 ] == "[参数]" ) {
				for( int j = 1; j < ConfigCmdCut.Length; ++j ) {
					Mes += ConfigCmdCut[ j ];
					if( j != ConfigCmdCut.Length - 1 ) {
						Mes += " ";
					}
				}
			}
			else if( ConfigCmdCut[ 0 ] == "[角度]" ) {
				Angle = int.Parse( ConfigCmdCut[ 1 ] );
			}
			else if( ConfigCmdCut[ 0 ] == "[坐标]" ) {
				if( ConfigCmdCut[ 1 ] == "X" ) {
					StartX = int.Parse( ConfigCmdCut[ 3 ] );
				}
				else if( ConfigCmdCut[ 1 ] == "Y" ) {
					StartY = int.Parse( ConfigCmdCut[ 3 ] );
				}
				else {
					n_Debug.Warning.BUG( "<WaitIns> 未知的坐标配置项: " + ConfigCmdCut[ 1 ] );
				}
			}
			else {
				ConfigError( "<WaitIns> 未知的配置项: " + ConfigCmdCut[ 0 ] );
			}
			++i;
		}
		lm.isNote = isNote;
		lm.SetUserValue( Name, GroupMes, Mes, GetPix(StartX), GetPix(StartY), Angle );
		myModuleList.Add( lm );
		return i;
	}
	
	//FuncIns配置
	static int FuncInsConfig( MyObjectList myModuleList, string[][] ConfigList, int i )
	{
		string Name = null;
		string GroupMes = null;
		string Mes = null;
		int StartX = 0;
		int StartY = 0;
		int Angle = 0;
		bool isNote = false;
		FuncIns lm = null;
		while( i < ConfigList.Length ) {
			
			string[] ConfigCmdCut = ConfigList[ i ];
			
			if( ConfigCmdCut[ 0 ] == "[FuncIns]" ) {
				lm = new FuncIns();
			}
			else if( ConfigCmdCut[ 0 ] == "[/FuncIns]" ) {
				break;
			}
			else if( ConfigCmdCut[ 0 ] == "[名称]" ) {
				Name = ConfigCmdCut[ 1 ];
			}
			else if( ConfigCmdCut[ 0 ] == "[分组]" ) {
				GroupMes = ConfigCmdCut[ 1 ];
			}
			else if( ConfigCmdCut[ 0 ] == "[禁用]" ) {
				if( ConfigCmdCut[ 1 ] == "True" ) {
					isNote = true;
				}
				else {
					isNote = false;
				}
			}
			else if( ConfigCmdCut[ 0 ] == "[参数]" ) {
				for( int j = 1; j < ConfigCmdCut.Length; ++j ) {
					Mes += ConfigCmdCut[ j ];
					if( j != ConfigCmdCut.Length - 1 ) {
						Mes += " ";
					}
				}
			}
			else if( ConfigCmdCut[ 0 ] == "[角度]" ) {
				Angle = int.Parse( ConfigCmdCut[ 1 ] );
			}
			else if( ConfigCmdCut[ 0 ] == "[坐标]" ) {
				if( ConfigCmdCut[ 1 ] == "X" ) {
					StartX = int.Parse( ConfigCmdCut[ 3 ] );
				}
				else if( ConfigCmdCut[ 1 ] == "Y" ) {
					StartY = int.Parse( ConfigCmdCut[ 3 ] );
				}
				else {
					ConfigError( "<FuncIns> 未知的坐标配置项: " + ConfigCmdCut[ 1 ] );
				}
			}
			else {
				ConfigError( "<FuncIns> 未知的配置项: " + ConfigCmdCut[ 0 ] );
			}
			++i;
		}
		lm.isNote = isNote;
		lm.SetUserValue( Name, GroupMes, Mes, GetPix(StartX), GetPix(StartY), Angle );
		myModuleList.Add( lm );
		return i;
	}
	
	//If配置
	static int ForeverInsConfig( MyObjectList myModuleList, string[][] ConfigList, int i )
	{
		string Name = null;
		string GroupMes = null;
		int StartX = 0;
		int StartY = 0;
		int Angle = 0;
		bool isNote = false;
		ForeverIns lm = null;
		while( i < ConfigList.Length ) {
			
			string[] ConfigCmdCut = ConfigList[ i ];
			
			if( ConfigCmdCut[ 0 ] == "[ForeverIns]" ) {
				lm = new ForeverIns();
			}
			else if( ConfigCmdCut[ 0 ] == "[/ForeverIns]" ) {
				break;
			}
			else if( ConfigCmdCut[ 0 ] == "[名称]" ) {
				Name = ConfigCmdCut[ 1 ];
			}
			else if( ConfigCmdCut[ 0 ] == "[分组]" ) {
				GroupMes = ConfigCmdCut[ 1 ];
			}
			else if( ConfigCmdCut[ 0 ] == "[禁用]" ) {
				if( ConfigCmdCut[ 1 ] == "True" ) {
					isNote = true;
				}
				else {
					isNote = false;
				}
			}
			else if( ConfigCmdCut[ 0 ] == "[角度]" ) {
				Angle = int.Parse( ConfigCmdCut[ 1 ] );
			}
			else if( ConfigCmdCut[ 0 ] == "[坐标]" ) {
				if( ConfigCmdCut[ 1 ] == "X" ) {
					StartX = int.Parse( ConfigCmdCut[ 3 ] );
				}
				else if( ConfigCmdCut[ 1 ] == "Y" ) {
					StartY = int.Parse( ConfigCmdCut[ 3 ] );
				}
				else {
					ConfigError( "<ForeverInsConfig> 未知的坐标配置项: " + ConfigCmdCut[ 1 ] );
				}
			}
			else {
				ConfigError( "<ForeverInsConfig> 未知的配置项: " + ConfigCmdCut[ 0 ] );
			}
			++i;
		}
		lm.isNote = isNote;
		lm.SetUserValue( Name, GroupMes, GetPix(StartX), GetPix(StartY), Angle );
		myModuleList.Add( lm );
		return i;
	}
	
	//Loop配置
	static int LoopInsConfig( MyObjectList myModuleList, string[][] ConfigList, int i )
	{
		string Name = null;
		string GroupMes = null;
		string Mes = null;
		int StartX = 0;
		int StartY = 0;
		int Angle = 0;
		bool isNote = false;
		LoopIns lm = null;
		while( i < ConfigList.Length ) {
			
			string[] ConfigCmdCut = ConfigList[ i ];
			
			if( ConfigCmdCut[ 0 ] == "[LoopIns]" ) {
				lm = new LoopIns();
			}
			else if( ConfigCmdCut[ 0 ] == "[/LoopIns]" ) {
				break;
			}
			else if( ConfigCmdCut[ 0 ] == "[名称]" ) {
				Name = ConfigCmdCut[ 1 ];
			}
			else if( ConfigCmdCut[ 0 ] == "[分组]" ) {
				GroupMes = ConfigCmdCut[ 1 ];
			}
			else if( ConfigCmdCut[ 0 ] == "[禁用]" ) {
				if( ConfigCmdCut[ 1 ] == "True" ) {
					isNote = true;
				}
				else {
					isNote = false;
				}
			}
			else if( ConfigCmdCut[ 0 ] == "[参数]" ) {
				for( int j = 1; j < ConfigCmdCut.Length; ++j ) {
					Mes += ConfigCmdCut[ j ];
					if( j != ConfigCmdCut.Length - 1 ) {
						Mes += " ";
					}
				}
			}
			else if( ConfigCmdCut[ 0 ] == "[角度]" ) {
				Angle = int.Parse( ConfigCmdCut[ 1 ] );
			}
			else if( ConfigCmdCut[ 0 ] == "[坐标]" ) {
				if( ConfigCmdCut[ 1 ] == "X" ) {
					StartX = int.Parse( ConfigCmdCut[ 3 ] );
				}
				else if( ConfigCmdCut[ 1 ] == "Y" ) {
					StartY = int.Parse( ConfigCmdCut[ 3 ] );
				}
				else {
					ConfigError( "<LoopInsConfig> 未知的坐标配置项: " + ConfigCmdCut[ 1 ] );
				}
			}
			else {
				ConfigError( "<LoopInsConfig> 未知的配置项: " + ConfigCmdCut[ 0 ] );
			}
			++i;
		}
		lm.isNote = isNote;
		lm.SetUserValue( Name, GroupMes, Mes, GetPix(StartX), GetPix(StartY), Angle );
		myModuleList.Add( lm );
		return i;
	}
	
	//Ext配置
	static int ExtInsConfig( MyObjectList myModuleList, string[][] ConfigList, int i )
	{
		string Name = null;
		string GroupMes = null;
		string Mes = null;
		int StartX = 0;
		int StartY = 0;
		int Angle = 0;
		bool isNote = false;
		bool ShunXu = true;
		ExtIns lm = null;
		while( i < ConfigList.Length ) {
			
			string[] ConfigCmdCut = ConfigList[ i ];
			
			if( ConfigCmdCut[ 0 ] == "[ExtIns]" ) {
				lm = new ExtIns();
			}
			else if( ConfigCmdCut[ 0 ] == "[/ExtIns]" ) {
				break;
			}
			else if( ConfigCmdCut[ 0 ] == "[名称]" ) {
				Name = ConfigCmdCut[ 1 ];
			}
			else if( ConfigCmdCut[ 0 ] == "[分组]" ) {
				GroupMes = ConfigCmdCut[ 1 ];
			}
			else if( ConfigCmdCut[ 0 ] == "[禁用]" ) {
				if( ConfigCmdCut[ 1 ] == "True" ) {
					isNote = true;
				}
				else {
					isNote = false;
				}
			}
			else if( ConfigCmdCut[ 0 ] == "[顺序]" ) {
				ShunXu = ConfigCmdCut[ 1 ] == "True";
			}
			else if( ConfigCmdCut[ 0 ] == "[参数]" ) {
				for( int j = 1; j < ConfigCmdCut.Length; ++j ) {
					Mes += ConfigCmdCut[ j ];
					if( j != ConfigCmdCut.Length - 1 ) {
						Mes += " ";
					}
				}
			}
			else if( ConfigCmdCut[ 0 ] == "[角度]" ) {
				Angle = int.Parse( ConfigCmdCut[ 1 ] );
			}
			else if( ConfigCmdCut[ 0 ] == "[坐标]" ) {
				if( ConfigCmdCut[ 1 ] == "X" ) {
					StartX = int.Parse( ConfigCmdCut[ 3 ] );
				}
				else if( ConfigCmdCut[ 1 ] == "Y" ) {
					StartY = int.Parse( ConfigCmdCut[ 3 ] );
				}
				else {
					ConfigError( "<LoopInsConfig> 未知的坐标配置项: " + ConfigCmdCut[ 1 ] );
				}
			}
			else {
				ConfigError( "<LoopInsConfig> 未知的配置项: " + ConfigCmdCut[ 0 ] );
			}
			++i;
		}
		lm.isNote = isNote;
		lm.SetUserValue( Name, GroupMes, Mes, GetPix(StartX), GetPix(StartY), Angle );
		lm.ShunXu = ShunXu;
		myModuleList.Add( lm );
		return i;
	}
	
	//While配置
	static int WhileInsConfig( MyObjectList myModuleList, string[][] ConfigList, int i )
	{
		string Name = null;
		string GroupMes = null;
		string Mes = null;
		int StartX = 0;
		int StartY = 0;
		int Angle = 0;
		bool isNote = false;
		WhileIns lm = null;
		while( i < ConfigList.Length ) {
			
			string[] ConfigCmdCut = ConfigList[ i ];
			
			if( ConfigCmdCut[ 0 ] == "[IfIns]" ) {
				lm = new WhileIns();
			}
			else if( ConfigCmdCut[ 0 ] == "[/IfIns]" ) {
				break;
			}
			else if( ConfigCmdCut[ 0 ] == "[名称]" ) {
				Name = ConfigCmdCut[ 1 ];
			}
			else if( ConfigCmdCut[ 0 ] == "[分组]" ) {
				GroupMes = ConfigCmdCut[ 1 ];
			}
			else if( ConfigCmdCut[ 0 ] == "[禁用]" ) {
				if( ConfigCmdCut[ 1 ] == "True" ) {
					isNote = true;
				}
				else {
					isNote = false;
				}
			}
			else if( ConfigCmdCut[ 0 ] == "[参数]" ) {
				for( int j = 1; j < ConfigCmdCut.Length; ++j ) {
					Mes += ConfigCmdCut[ j ];
					if( j != ConfigCmdCut.Length - 1 ) {
						Mes += " ";
					}
				}
			}
			else if( ConfigCmdCut[ 0 ] == "[角度]" ) {
				Angle = int.Parse( ConfigCmdCut[ 1 ] );
			}
			else if( ConfigCmdCut[ 0 ] == "[坐标]" ) {
				if( ConfigCmdCut[ 1 ] == "X" ) {
					StartX = int.Parse( ConfigCmdCut[ 3 ] );
				}
				else if( ConfigCmdCut[ 1 ] == "Y" ) {
					StartY = int.Parse( ConfigCmdCut[ 3 ] );
				}
				else {
					ConfigError( "<IfInsConfig> 未知的坐标配置项: " + ConfigCmdCut[ 1 ] );
				}
			}
			else {
				ConfigError( "<IfInsConfig> 未知的配置项: " + ConfigCmdCut[ 0 ] );
			}
			++i;
		}
		lm.isNote = isNote;
		lm.SetUserValue( Name, GroupMes, Mes, GetPix(StartX), GetPix(StartY), Angle );
		myModuleList.Add( lm );
		return i;
	}
	
	//IfElse配置
	static int IfElseInsConfig( MyObjectList myModuleList, string[][] ConfigList, int i )
	{
		string Name = null;
		string GroupMes = null;
		string Mes = null;
		int StartX = 0;
		int StartY = 0;
		int Angle = 0;
		bool isNote = false;
		IfElseIns lm = null;
		while( i < ConfigList.Length ) {
			
			string[] ConfigCmdCut = ConfigList[ i ];
			
			if( ConfigCmdCut[ 0 ] == "[IfElseIns]" ) {
				lm = new IfElseIns();
			}
			else if( ConfigCmdCut[ 0 ] == "[/IfElseIns]" ) {
				break;
			}
			else if( ConfigCmdCut[ 0 ] == "[名称]" ) {
				Name = ConfigCmdCut[ 1 ];
			}
			else if( ConfigCmdCut[ 0 ] == "[分组]" ) {
				GroupMes = ConfigCmdCut[ 1 ];
			}
			else if( ConfigCmdCut[ 0 ] == "[禁用]" ) {
				if( ConfigCmdCut[ 1 ] == "True" ) {
					isNote = true;
				}
				else {
					isNote = false;
				}
			}
			else if( ConfigCmdCut[ 0 ] == "[参数]" ) {
				for( int j = 1; j < ConfigCmdCut.Length; ++j ) {
					Mes += ConfigCmdCut[ j ];
					if( j != ConfigCmdCut.Length - 1 ) {
						Mes += " ";
					}
				}
			}
			else if( ConfigCmdCut[ 0 ] == "[角度]" ) {
				Angle = int.Parse( ConfigCmdCut[ 1 ] );
			}
			else if( ConfigCmdCut[ 0 ] == "[坐标]" ) {
				if( ConfigCmdCut[ 1 ] == "X" ) {
					StartX = int.Parse( ConfigCmdCut[ 3 ] );
				}
				else if( ConfigCmdCut[ 1 ] == "Y" ) {
					StartY = int.Parse( ConfigCmdCut[ 3 ] );
				}
				else {
					ConfigError( "<IfElseIns> 未知的坐标配置项: " + ConfigCmdCut[ 1 ] );
				}
			}
			else {
				ConfigError( "<IfElseIns> 未知的配置项: " + ConfigCmdCut[ 0 ] );
			}
			++i;
		}
		lm.isNote = isNote;
		lm.SetUserValue( Name, GroupMes, Mes, GetPix(StartX), GetPix(StartY), Angle );
		myModuleList.Add( lm );
		return i;
	}
	
	//Else配置
	static int ElseInsConfig( MyObjectList myModuleList, string[][] ConfigList, int i )
	{
		string Name = null;
		string GroupMes = null;
		int StartX = 0;
		int StartY = 0;
		int Angle = 0;
		bool isNote = false;
		ElseIns lm = null;
		while( i < ConfigList.Length ) {
			
			string[] ConfigCmdCut = ConfigList[ i ];
			
			if( ConfigCmdCut[ 0 ] == "[ElseIns]" ) {
				lm = new ElseIns();
			}
			else if( ConfigCmdCut[ 0 ] == "[/ElseIns]" ) {
				break;
			}
			else if( ConfigCmdCut[ 0 ] == "[名称]" ) {
				Name = ConfigCmdCut[ 1 ];
			}
			else if( ConfigCmdCut[ 0 ] == "[分组]" ) {
				GroupMes = ConfigCmdCut[ 1 ];
			}
			else if( ConfigCmdCut[ 0 ] == "[禁用]" ) {
				if( ConfigCmdCut[ 1 ] == "True" ) {
					isNote = true;
				}
				else {
					isNote = false;
				}
			}
			else if( ConfigCmdCut[ 0 ] == "[角度]" ) {
				Angle = int.Parse( ConfigCmdCut[ 1 ] );
			}
			else if( ConfigCmdCut[ 0 ] == "[坐标]" ) {
				if( ConfigCmdCut[ 1 ] == "X" ) {
					StartX = int.Parse( ConfigCmdCut[ 3 ] );
				}
				else if( ConfigCmdCut[ 1 ] == "Y" ) {
					StartY = int.Parse( ConfigCmdCut[ 3 ] );
				}
				else {
					ConfigError( "<ElseIns> 未知的坐标配置项: " + ConfigCmdCut[ 1 ] );
				}
			}
			else {
				ConfigError( "<ElseIns> 未知的配置项: " + ConfigCmdCut[ 0 ] );
			}
			++i;
		}
		lm.isNote = isNote;
		lm.SetUserValue( Name, GroupMes, GetPix(StartX), GetPix(StartY), Angle );
		myModuleList.Add( lm );
		return i;
	}
	
	//CondiEndIns配置
	static int CondiEndInsConfig( MyObjectList myModuleList, string[][] ConfigList, int i )
	{
		string Name = null;
		string GroupMes = null;
		int StartX = 0;
		int StartY = 0;
		int Angle = 0;
		bool isNote = false;
		CondiEndIns lm = null;
		while( i < ConfigList.Length ) {
			
			string[] ConfigCmdCut = ConfigList[ i ];
			
			if( ConfigCmdCut[ 0 ] == "[CondiEndIns]" ) {
				lm = new CondiEndIns();
			}
			else if( ConfigCmdCut[ 0 ] == "[/CondiEndIns]" ) {
				break;
			}
			else if( ConfigCmdCut[ 0 ] == "[名称]" ) {
				Name = ConfigCmdCut[ 1 ];
			}
			else if( ConfigCmdCut[ 0 ] == "[分组]" ) {
				GroupMes = ConfigCmdCut[ 1 ];
			}
			else if( ConfigCmdCut[ 0 ] == "[禁用]" ) {
				if( ConfigCmdCut[ 1 ] == "True" ) {
					isNote = true;
				}
				else {
					isNote = false;
				}
			}
			else if( ConfigCmdCut[ 0 ] == "[角度]" ) {
				Angle = int.Parse( ConfigCmdCut[ 1 ] );
			}
			else if( ConfigCmdCut[ 0 ] == "[坐标]" ) {
				if( ConfigCmdCut[ 1 ] == "X" ) {
					StartX = int.Parse( ConfigCmdCut[ 3 ] );
				}
				else if( ConfigCmdCut[ 1 ] == "Y" ) {
					StartY = int.Parse( ConfigCmdCut[ 3 ] );
				}
				else {
					ConfigError( "<CondiEndIns> 未知的坐标配置项: " + ConfigCmdCut[ 1 ] );
				}
			}
			else {
				ConfigError( "<CondiEndIns> 未知的配置项: " + ConfigCmdCut[ 0 ] );
			}
			++i;
		}
		lm.isNote = isNote;
		lm.SetUserValue( Name, GroupMes, GetPix(StartX), GetPix(StartY), Angle );
		myModuleList.Add( lm );
		return i;
	}
	
	//---------------------------------------------------------------
	
	//组件配置
	static int ModuleConfig( ImagePanel vGPanel, MyObjectList myModuleList, string[][] ConfigList, int i )
	{
		string ModuleName = null;
		string GroupMes = null;
		string Language = null;
		string PackOwner = null;
		int StartX = 0;
		int StartY = 0;
		int gui_Width = -1;
		int gui_Height = -1;
		int Angle = 0;
		HardModule cModule = null;
		string ImageModulePath = null;
		Color UIMBackColor = Color.Empty;
		Color UIMEdgeColor = Color.Empty;
		Color UIMForeColor = Color.Empty;
		Font TextFont = null;
		string UIText = null;
		string ExtendValue = null;
		string ExtendValue1 = null;
		bool SwapPort = false;
		Color MBackColor = Color.Gainsboro;
		
		while( i < ConfigList.Length ) {
			
			string[] ConfigCmdCut = ConfigList[ i ];
			
			if( ConfigCmdCut[ 0 ] == "[组件]" ) {
				//无操作,忽略
			}
			else if( ConfigCmdCut[ 0 ] == "[组件结束]" ) {
				break;
			}
			else if( ConfigCmdCut[ 0 ] == "[组件路径]" ) {
				string noFullPath = null;
				for( int k = 2; k < ConfigCmdCut.Length; ++k ) {
					noFullPath += ConfigCmdCut[ k ];
					if( k != ConfigCmdCut.Length - 1 ) {
						noFullPath += " ";
					}
				}
				bool isuser = false;
				if( ConfigCmdCut[ 1 ] == ModuleFlag.UserPath ) {
					isuser = true;
				}
				//强制进行版本更新
				//if( !File.Exists( ImageModulePath ) ) {
					ImageModulePath = VersionSwitch.Switch( noFullPath, isuser );
					
					if( ImageModulePath == null || !File.Exists( ImageModulePath ) ) {
						n_Debug.Warning.BUG( "<ModuleConfig> 不存在的文件: " + ImageModulePath + "\n系统打开文件时遇到了一个问题, 文件中用到了一个过时或不存在的模块" +
						                                    " 一般每种过时模块都会有对应的新版模块, 可直接删除替换即可." );
						++i;
						
						while( i < ConfigList.Length ) {
							ConfigCmdCut = ConfigList[ i ];
							if( ConfigCmdCut[ 0 ] == "[组件结束]" ) {
								return i;
							}
							++i;
						}
					}
				//}
				
				cModule = (HardModule)GetModuleFromMacroFileLoad( ImageModulePath, isuser );
				cModule.isUserPathMod = isuser;
			}
			else if( ConfigCmdCut[ 0 ] == "[名称]" ) {
				ModuleName = ConfigCmdCut[ 1 ];
			}
			else if( ConfigCmdCut[ 0 ] == "[分组]" ) {
				GroupMes = ConfigCmdCut[ 1 ];
			}
			else if( ConfigCmdCut[ 0 ] == "[语言]" ) {
				Language = ConfigCmdCut[ 1 ];
			}
			else if( ConfigCmdCut[ 0 ] == "[封包]" ) {
				PackOwner = ConfigCmdCut[ 1 ];
			}
			else if( ConfigCmdCut[ 0 ] == "[文字]" ) {
				ConfigCmdCut[ 0 ] = "";
				UIText = string.Join( " ", ConfigCmdCut ).Remove( 0, 1 );
			}
			else if( ConfigCmdCut[ 0 ] == "[扩展参数]" ) {
				ConfigCmdCut[ 0 ] = "";
				ExtendValue = string.Join( " ", ConfigCmdCut ).Remove( 0, 1 );
			}
			else if( ConfigCmdCut[ 0 ] == "[扩展参数1]" ) {
				ConfigCmdCut[ 0 ] = "";
				ExtendValue1 = string.Join( " ", ConfigCmdCut ).Remove( 0, 1 );
			}
			else if( ConfigCmdCut[ 0 ] == "[背景颜色]" ) {
				UIMBackColor = Color.FromArgb( int.Parse( ConfigCmdCut[ 1 ] ) );
			}
			else if( ConfigCmdCut[ 0 ] == "[边框颜色]" ) {
				UIMEdgeColor = Color.FromArgb( int.Parse( ConfigCmdCut[ 1 ] ) );
			}
			else if( ConfigCmdCut[ 0 ] == "[文字颜色]" ) {
				UIMForeColor = Color.FromArgb( int.Parse( ConfigCmdCut[ 1 ] ) );
			}
			else if( ConfigCmdCut[ 0 ] == "[字体]" ) {
				ConfigCmdCut[ 0 ] = "";
				string FontDesc = string.Join( " ", ConfigCmdCut ).Remove( 0, 1 );
				TextFont = Common.GetFontFromString( FontDesc );
				gui_Height = (int)GUIset.mg.MeasureString( "0", TextFont ).Height;
			}
			else if( ConfigCmdCut[ 0 ] == "[坐标]" ) {
				if( ConfigCmdCut[ 1 ] == "X" ) {
					StartX = int.Parse( ConfigCmdCut[ 3 ] );
				}
				else if( ConfigCmdCut[ 1 ] == "Y" ) {
					StartY = int.Parse( ConfigCmdCut[ 3 ] );
				}
				else {
					n_Debug.Warning.BUG( "<ModuleConfig> 未知的坐标配置项: " + ConfigCmdCut[ 1 ] );
				}
			}
			else if( ConfigCmdCut[ 0 ] == "[尺寸]" ) {
				if( ConfigCmdCut[ 1 ] == "Width" ) {
					gui_Width = int.Parse( ConfigCmdCut[ 3 ] );
				}
				else if( ConfigCmdCut[ 1 ] == "Height" ) {
					gui_Height = int.Parse( ConfigCmdCut[ 3 ] );
				}
				else {
					n_Debug.Warning.BUG( "<ModuleConfig> 未知的尺寸配置项: " + ConfigCmdCut[ 1 ] );
				}
			}
			else if( ConfigCmdCut[ 0 ] == "[颜色]" ) {
				int rr = int.Parse( ConfigCmdCut[ 1 ] );
				int gg = int.Parse( ConfigCmdCut[ 2 ] );
				int bb = int.Parse( ConfigCmdCut[ 3 ] );
				MBackColor = Color.FromArgb( rr, gg, bb );
			}
			else if( ConfigCmdCut[ 0 ] == "[角度]" ) {
				Angle = int.Parse( ConfigCmdCut[ 1 ] );
			}
			else if( ConfigCmdCut[ 0 ] == "[翻转]" ) {
				SwapPort = ConfigCmdCut[ 1 ] == "True";
			}
			else if( ConfigCmdCut[ 0 ] == "[事件]" ) {
				
				string InnerName = ConfigCmdCut[ 1 ];
				string UserName = ConfigCmdCut[ 3 ];
				
				//转换旧版本事件名称
				if( UserName.IndexOf( "_系统空闲时" ) != -1 ) {
					string s = UserName.Replace( "_系统空闲时", "_反复执行" );
					UserName = s;
				}
				if( UserName.IndexOf( "_系统启动时" ) != -1 ) {
					string s = UserName.Replace( "_系统启动时", "_初始化" );
					UserName = s;
				}
				for( int index = 0; index < cModule.EventList.Length; ++index ) {
					if( cModule.EventList[ index ][ 2 ] == InnerName ) {
						cModule.EventList[ index ][ 0 ] = UserName;
						break;
					}
				}
				//添加到事件连接列表, 用于在图形界面显示虚线的连接关系
				ELNote e = new ELNote( ModuleName, InnerName, UserName );
				vGPanel.EL.Add( e );
			}
			else if( ConfigCmdCut[ 0 ] == "[接口]" ) {
				
				string InnerName = ConfigCmdCut[ 1 ];
				for( int index = 0; index < cModule.LinkInterfaceList.Length; ++index ) {
					if( cModule.LinkInterfaceList[ index ][ 2 ] == InnerName ) {
						cModule.LinkInterfaceList[ index ][ 0 ] = ConfigCmdCut[ 3 ];
						break;
					}
				}
			}
			else if( ConfigCmdCut[ 0 ] == "[参数]" ) {
				string InnerName = ConfigCmdCut[ 1 ];
				string UserName = ConfigCmdCut[ 3 ];
				for( int index = 0; index < cModule.VarList.Length; ++index ) {
					if( cModule.VarList[ index ][ 2 ] == InnerName ) {
						cModule.VarList[ index ][ 0 ] = UserName;
					}
				}
			}
			else if( ConfigCmdCut[ 0 ] == "[仿真]" ) {
				string SimValue = ConfigCmdCut[ 1 ];
				if( SimValue != null ) {
					cModule.ExistSimValue = true;
					string[] ss = SimValue.Split( ',' );
					cModule.SimMidX = float.Parse( ss[0] );
					cModule.SimMidY = float.Parse( ss[1] );
					cModule.SimAngle = float.Parse( ss[2] );
					if( ss.Length >= 4 ) {
						cModule.SimVisible = ss[3] == "True";
					}
					if( ss.Length >= 5 ) {
						cModule.SimWidth = float.Parse( ss[4] );
						cModule.SimHeight = float.Parse( ss[5] );
					}
				}
			}
			else if( ConfigCmdCut[ 0 ] == "[S_Image]" ) {
				cModule.ExistSimValue = true;
				string SpriteFilePath = null;
				for( int k = 1; k < ConfigCmdCut.Length; ++k ) {
					SpriteFilePath += ConfigCmdCut[ k ];
					if( k != ConfigCmdCut.Length - 1 ) {
						SpriteFilePath += " ";
					}
				}
				cModule.SpriteFilePath = SpriteFilePath;
			}
			else if( ConfigCmdCut[ 0 ] == "[S_Value]" ) {
				cModule.ExistSimValue = true;
				string[] ss = ConfigCmdCut[ 1 ].Split( ',' );
				cModule.SimRolX = float.Parse( ss[0] );
				cModule.SimRolY = float.Parse( ss[1] );
				cModule.SimimgAngle = float.Parse( ss[2] );
				if( ss.Length > 3 ) {
					cModule.ImgIndex = int.Parse( ss[3] );
				}
			}
			else if( ConfigCmdCut[ 0 ] == "[S_NotHit]" ) {
				cModule.ExistSimValue = true;
				cModule.NotHitList = ConfigCmdCut[ 1 ];
			}
			else if( ConfigCmdCut[ 0 ] == "[仿真M]" ) {
				cModule.ExistSimValue = true;
				string[] ss = ConfigCmdCut[ 1 ].Split( ',' );
				cModule.SimSwapPin = ss[0] == "True";
				cModule.SimStartPower = int.Parse( ss[1] );
				cModule.SimSpeedDown = int.Parse( ss[2] );
				cModule.SimInertia = int.Parse( ss[3] );
			}
			else if( ConfigCmdCut[ 0 ] == "[键值]" ) {
				string[] ss = ConfigCmdCut[ 1 ].Split( ',' );
				if( cModule != null ) {
					cModule.SetBindKeyList( ss );
				}
			}
			else {
				ConfigError( "<ModuleConfig> 未知的配置项: " + ConfigCmdCut[ 0 ] );
			}
			++i;
		}
		if( cModule == null ) {
			return i;
		}
		cModule.SetUserValue( ModuleName, GroupMes, Language, GetPix(StartX), GetPix(StartY), Angle, SwapPort );
		
		cModule.PadBackBrush = new SolidBrush( MBackColor );
		if( gui_Width != -1 ) {
			cModule.Width = gui_Width;
		}
		if( gui_Height != -1 ) {
			cModule.Height = gui_Height;
		}
		if( PackOwner != null ) {
			cModule.PackOwner = PackOwner;
			string[] pss = PackOwner.Split( ',' );
			if( pss[0] == PackHidType.hid ) {
				cModule.PackHid = true;
				cModule.CanFloat = true;
			}
			if( pss.Length >= 2 ) {
				if( pss[1] == PackHidType.SYS_OPEN_MPORT ) {
					cModule.PackOpenMPort = true;
				}
				if( pss[1] == PackHidType.SYS_HIDE_MPORT ) {
					cModule.PackHideMPort = true;
				}
			}
		}
		if( cModule.ImageName == SPMoudleName.SYS_PADSHAP ) {
			cModule.CanFloat = true;
		}
		if( UIMBackColor != Color.Empty ) {
			cModule.BackColor = UIMBackColor;
		}
		if( UIMEdgeColor != Color.Empty ) {
			cModule.EdgeColor = UIMEdgeColor;
		}
		if( UIMForeColor != Color.Empty ) {
			cModule.ForeColor = UIMForeColor;
		}
		if( TextFont != null ) {
			cModule.TextFont = TextFont;
		}
		if( UIText != null ) {
			cModule.Text = UIText;
		}
		if( ExtendValue != null ) {
			cModule.ExtendValue = ExtendValue;
		}
		if( ExtendValue1 != null ) {
			cModule.ExtendValue1 = ExtendValue1;
		}
		if( cModule.isClientModule ) {
			UIModule c = new UIModule( cModule );
			myModuleList.Add( c );
		}
		else {
			if( cModule.ImageName == n_GUIcoder.SPMoudleName.SYS_Arduino ) {
				//cModule.PackHid = true;
				//cModule.CanFloat = true;
				myModuleList.Addmbb( cModule );
			}
			else {
				myModuleList.Add( cModule );
			}
		}
		return i;
	}
	
	//配置读取失败提示
	static void ConfigError( string Mes )
	{
		n_Debug.Warning.BUG( "遇到未知条目，建议您在官网下载最新版软件打开此文件：" + Mes );
	}
	
	//---------------------------------------------------------------
	
	//从组件宏配置文件中提取组件对象
	public static HardModule GetModuleFromMacroFileLoad( string FilePath, bool isuser )
	{
		try{
		if( FilePath.EndsWith( ".B" ) ) {
			return GetModuleFromMacroFileBase( null, FilePath, null, isuser );
		}
		else {
			return GetModuleFromModuleFile( FilePath, isuser );
		}
		}catch(Exception e){
			n_Debug.Warning.BUG( "<GetModuleFromMacroFileLoad> 解析异常: " + e.ToString() );
			return null;
		}
	}
	public static HardModule GetModuleFromMacroFile( string FilePath )
	{
		try{
		if( FilePath.EndsWith( ".B" ) ) {
			return GetModuleFromMacroFileBase( null, FilePath, null, false );
		}
		else {
			return GetModuleFromModuleFile( FilePath, false );
		}
		}catch(Exception e){
			n_Debug.Warning.BUG( "<GetModuleFromMacroFile> 解析异常: " + e.ToString() );
			return null;
		}
	}
	
	static HardModule GetModuleFromMacroFileBase( string RealPath, string FilePath, HardModule cModule, bool isuser )
	{
		string[] ModuleNameList = null;
		string[] LanguageList = null;
		string incPackFileName = null;
		if( isuser ) {
			incPackFileName = Config.PathPre_CurrentDir + " " + FilePath.Remove( 0, G.ccode.FilePath.Length );
		}
		else {
			incPackFileName = Config.PathPre_ModuleLib + " " + FilePath.Remove( 0, OS.ModuleLibPath.Length );
		}
		
		string Mes = null;
		try {
			Mes = VIO.OpenTextFileGB2312( FilePath );
		}
		catch {
			n_Debug.Warning.BUG( RealPath + " - 驱动路径不存在:" + FilePath );
		}
		//根据M文件配置信息进行驱动配置
		if( cModule != null && cModule.ConfigList != null ) {
			Mes = n_HModCommon.HModCommon.GetMes( cModule.ConfigList, Mes );
		}
		
		//成员列表
		int ElementIndex = 0;
		string[][] ElementList = null;
		string[] ResourceList = null;
		string[] UIModuleEXList = null;
		ModuleMessage[] mm = null;
		string CommonRefFile = null;
		int iSimulateType = 0;
		string ModuleImageList = "";
		string ChipType = "";
		string PyValue = "";
		string[][] ModuleMesList = GetConfigFromText( Mes );
		for( int i = 0; i < ModuleMesList.Length; ++i ) {
			switch( ModuleMesList[ i ][ 0 ] ) {
					case "[组件文件]":
						string Folder = FilePath.Remove( FilePath.LastIndexOf( "\\" ) + 1 );
						cModule = new HardModule( Folder + ModuleMesList[ i ][ 1 ] );
						break;
					case "[公共引用]":
						CommonRefFile = ModuleMesList[ i ][ 1 ];
						break;
					case "[实物图片]":
						ModuleImageList = ModuleMesList[ i ][ 1 ];
						ModuleImageList = ModuleImageList.Replace( ".png", ".jpg" );
						
						//如果不是以a开始, 表示是旧版的组件配置, 则进行替换
						if( ModuleImageList != "" && !ModuleImageList.StartsWith( "a" ) ) {
							ModuleImageList = "a" + ModuleImageList.Replace( ",", ",a" );
						}
						break;
					case "[组件名称]":
						ModuleNameList = ModuleMesList[ i ];
						
						
						
						if( LanguageList.Length != 2 ) {
							n_Debug.Debug.Message += string.Join( " ", ModuleNameList ) + "  ->  " + string.Join( " ", LanguageList ) + "\n";
						}
						
						
						
						
						break;
					case "[模块型号]":
						ChipType = ModuleMesList[ i ][ 1 ];
						break;
					case "[资源占用]":
						ModuleMesList[ i ][ 0 ] = "";
						ResourceList = string.Join( ",", ModuleMesList[ i ] ).Trim( ',' ).Split( ',' );
						break;
					case "[py参数]":
						PyValue = ModuleMesList[ i ][ 1 ];
						break;
					case "[控件属性]":
						UIModuleEXList = ModuleMesList[ i ][ 1 ].Split( ',' );
						break;
					case "[仿真类型]":
						iSimulateType = int.Parse( ModuleMesList[ i ][ 1 ] );
						break;
					//case "[处理器]":
					//	break;
					case "[语言列表]":
						string folder = FilePath.Remove( FilePath.LastIndexOf( "." ) );
						mm = GUIcoder.LoadExtendFile( folder, ModuleMesList[ i ] );
						ModuleMesList[ i ][ 0 ] = null;
						LanguageList = string.Join( " ", ModuleMesList[ i ] ).TrimStart( ' ' ).Split( ' ' );
						ElementList = new string[ ModuleMesList.Length - 1 ][];
						break;
					case "[元素子项]":
						string[] TempConfig = string.Join( " ", ModuleMesList[ i ] ).Split( ' ' );
						ElementList[ ElementIndex ] = TempConfig;
						++ElementIndex;
						break;
					default:
						n_Debug.Warning.BUG( "<ModuleConfig> 未知的配置项: " + ModuleMesList[ i ][ 0 ] );
						break;
			}
		}
		//组件占用的资源列表
		cModule.ResourceList = ResourceList;
		cModule.UIModuleEXList = UIModuleEXList;
		cModule.CommonRefFile = CommonRefFile;
		//M文件的ChipType会覆盖B文件的ChipType
		if( cModule.ChipType == "" ) {
			cModule.ChipType = ChipType;
		}
		
		cModule.PyValue = PyValue;
		
		//提取用户信息更新默认信息
		if( cModule.ImageList == null || cModule.ImageList == "" ) {
			cModule.ModuleImageList = ModuleImageList;
		}
		else{
			cModule.ModuleImageList = cModule.ImageList;
		}
		
		if( cModule.UserName != null && cModule.UserName != "" ) {
			
			//这里只处理中文, 应加上英文处理 (判断不为 Pack 则进行覆盖 2022.8.31)
			ModuleNameList[3] = cModule.UserName.Split( ',' )[1];
			mm[1].UserName = ModuleNameList[3];
		}
		
		//仿真类型
		cModule.SimulateType = iSimulateType;
		
		//格式化成员列表
		cModule.ElementList = new string[ ElementIndex ][];
		for( int i = 0; i < ElementIndex; ++i ) {
			cModule.ElementList[ i ] = ElementList[ i ];
		}
		//----------------------------------
		//提取接口列表
		int InterfaceIndex = 0;
		string[][] InterfaceList = new string[ ElementIndex ][];
		for( int  i = 0; i < ElementIndex; ++i ) {
			if( ElementList[ i ][ 1 ].StartsWith( InterfaceType.Interface_ ) ) {
				InterfaceList[ InterfaceIndex ] = ElementList[ i ];
				++InterfaceIndex;
			}
		}
		//格式化接口列表
		cModule.InterfaceList = new string[ InterfaceIndex ][];
		for( int i = 0; i < InterfaceIndex; ++i ) {
			cModule.InterfaceList[ i ] = InterfaceList[ i ];
		}
		//----------------------------------
		//提取需求接口列表
		int LinkInterfaceIndex = 0;
		string[][] LinkInterfaceList = new string[ ElementIndex ][];
		for( int  i = 0; i < ElementIndex; ++i ) {
			if( ElementList[ i ][ 1 ].StartsWith( InterfaceType.LinkInterface_ ) ) {
				LinkInterfaceList[ LinkInterfaceIndex ] = ElementList[ i ];
				LinkInterfaceList[ LinkInterfaceIndex ][ 0 ] = "";
				++LinkInterfaceIndex;
			}
		}
		//格式化需求接口列表
		cModule.LinkInterfaceList = new string[ LinkInterfaceIndex ][];
		for( int i = 0; i < LinkInterfaceIndex; ++i ) {
			cModule.LinkInterfaceList[ i ] = LinkInterfaceList[ i ];
		}
		//---------------------------------
		//提取事件列表
		int eventIndex = 0;
		string[][] eventList = new string[ ElementIndex ][];
		for( int  i = 0; i < ElementIndex; ++i ) {
			if( ElementList[ i ][ 1 ] == InterfaceType.Event ) {
				eventList[ eventIndex ] = ElementList[ i ];
				eventList[ eventIndex ][ 0 ] = "";
				++eventIndex;
			}
		}
		//格式化事件列表
		cModule.EventList = new string[ eventIndex ][];
		for( int i = 0; i < eventIndex; ++i ) {
			cModule.EventList[ i ] = eventList[ i ];
		}
		//----------------------------------
		//提取参数列表
		int VarIndex = 0;
		string[][] VarList = new string[ ElementIndex ][];
		for( int  i = 0; i < ElementIndex; ++i ) {
			if( ElementList[ i ][ 1 ].StartsWith( InterfaceType.LinkConst_ ) ||
			    ElementList[ i ][ 1 ].StartsWith( InterfaceType.LinkVar_ ) ) {
				VarList[ VarIndex ] = ElementList[ i ];
				VarList[ VarIndex ][ 0 ] = "";
				++VarIndex;
			}
		}
		//格式化参数列表
		cModule.VarList = new string[ VarIndex ][];
		for( int i = 0; i < VarIndex; ++i ) {
			cModule.VarList[ i ] = VarList[ i ];
		}
		//组件其他属性设置
		cModule.mm = mm;
		cModule.IncPackFilePath = incPackFileName;
		cModule.NameList = ModuleNameList;
		cModule.LanguageList = LanguageList;
		
		if( RealPath != null ) {
			cModule.MacroFilePath = RealPath;
			cModule.BasePath = RealPath.Remove( RealPath.LastIndexOf( OS.PATH_S ) + 1 );
		}
		else {
			cModule.MacroFilePath = FilePath;
			cModule.BasePath = FilePath.Remove( FilePath.LastIndexOf( OS.PATH_S ) + 1 );
		}
		
		cModule.PyFilePath = cModule.BasePath + "driver\\Pack.py";
		
		//用于保存到用户的模块配置文件中
		if( isuser ) {
			cModule.MacroFilePathPart = Config.PathPre_CurrentDir + " " + cModule.MacroFilePath.Remove( 0, G.ccode.FilePath.Length );
		}
		else {
			cModule.MacroFilePathPart = Config.PathPre_ModuleLib + " " + cModule.MacroFilePath.Remove( 0, OS.ModuleLibPath.Length );
		}
		
		cModule.IncBasePath = incPackFileName.Remove( incPackFileName.LastIndexOf( OS.PATH_S ) + 1 );
		
		//创建接口列表
		cModule.CreateIPortList();
		
		if( ModuleNameList == null ) {
			n_Debug.Warning.BUG( "组件包封装的格式不对,未发现组件名称项:\n" + FilePath );
		}
		return cModule;
	}
	
	//从组件设计文件中提取组件对象
	static HardModule GetModuleFromModuleFile( string FilePath, bool isuser )
	{
		HardModule cModule = new HardModule( FilePath );
		if( isuser ) {
			GetModuleFromMacroFileBase( FilePath, G.ccode.FilePath + cModule.DriverFilePath, cModule, isuser );
		}
		else {
			GetModuleFromMacroFileBase( FilePath, OS.ModuleLibPath + cModule.DriverFilePath, cModule, isuser );
		}
		
		return cModule;
	}
	
	//加载并解析说明文件
	public static ModuleMessage[] LoadExtendFile( string Folder, string[] LanguageList )
	{
		string[] Result = new string[ LanguageList.Length - 1 ];
		ModuleMessage[] ModuleMessageList = new ModuleMessage[ LanguageList.Length - 1 ];
		
		for( int i = 1; i < LanguageList.Length; ++i ) {
			string FileName = Folder + OS.PATH_S + "extend_" + LanguageList[ i ] + ".txt";
			if( !File.Exists( FileName ) ) {
				continue;
			}
			string text = VIO.OpenTextFileGB2312( FileName );
			
			ModuleMessageList[ i - 1 ] = new ModuleMessage();
			ModuleMessageList[ i - 1 ].Language = LanguageList[ i ];
			
			ParseExtendMes( text, ModuleMessageList[ i - 1 ] );
		}
		return ModuleMessageList;
	}
	
	//解析扩展信息
	public static void ParseExtendMes( string Mes, ModuleMessage mm )
	{
		string[] Lines = Mes.Split( '\n' );
		string ModuleMes = "";
		string ModuleMes1 = "";
		MemberMes[] MemberMesList = new MemberMes[ 100 ];
		int MemberNumber = 0;
		for( int i = 0; i < Lines.Length; ++i ) {
			
			//如果是组件信息,则进入提取组件信息
			if( Lines[ i ].StartsWith( "<module>" ) ) {
				for( int j = i + 1; j < Lines.Length; ++j ) {
					if( Lines[ j ].StartsWith( "</module>" ) ) {
						i = j;
						ModuleMes = ModuleMes.TrimEnd( '\n' );
						break;
					}
					ModuleMes += Lines[ j ] + "\n";
				}
			}
			//如果是名称信息,则进入提取组件名称
			if( Lines[ i ].StartsWith( "<name>" ) ) {
				string MemberHead = Lines[ i ].Remove( Lines[ i ].Length - 1 );
				string[] MemberHeadList = MemberHead.Split( ' ' );
				string MemberInnerName = MemberHeadList[ 1 ];
				string MemberUserName = MemberHeadList[ 2 ];
				mm.InnerName = MemberInnerName;
				mm.UserName = MemberUserName;
				for( int j = i + 1; j < Lines.Length; ++j ) {
					if( Lines[ j ].StartsWith( "</name>" ) ) {
						i = j;
						ModuleMes1 = ModuleMes1.TrimEnd( '\n' );
						
						if( ModuleMes1 != "" ) {
							ModuleMes = ModuleMes1;
						}
						
						break;
					}
					ModuleMes1 += Lines[ j ] + "\n";
				}
			}
			//如果是成员信息,则进入提取成员信息
			else if( Lines[ i ].StartsWith( "<member>" ) ) {
				string MemberHead = Lines[ i ].Remove( Lines[ i ].Length - 1 );
				string[] MemberHeadList = MemberHead.Split( ' ' );
				
				string MemberType = "";
				string MemberInnerName = "";
				string MemberUserName = "";
				if( MemberHeadList.Length == 2 ) {
					MemberInnerName = MemberHeadList[ 1 ];
				}
				else {
					MemberType = MemberHeadList[ 1 ];
					MemberInnerName = MemberHeadList[ 2 ];
					for( int mi = 3; mi < MemberHeadList.Length; ++mi ) {
						MemberUserName += MemberHeadList[ mi ] + " ";
					}
					MemberUserName = MemberUserName.TrimEnd( ' ' );
				}
				MemberMesList[ MemberNumber ] = new MemberMes();
				MemberMesList[ MemberNumber ].InnerName = MemberInnerName;
				MemberMesList[ MemberNumber ].UserName = MemberUserName;
				MemberMesList[ MemberNumber ].Type = MemberType;
				string MemberMes = "";
				for( int j = i + 1; j < Lines.Length; ++j ) {
					if( Lines[ j ].StartsWith( "</member>" ) ) {
						i = j;
						MemberMes = MemberMes.TrimEnd( '\n' );
						break;
					}
					MemberMes += Lines[ j ] + "\n";
				}
				MemberMesList[ MemberNumber ].Mes = MemberMes;
				++MemberNumber;
			}
			else {
				//n_Debug.Warning.AddErrMessage( "未知的扩展信息提取标志: " + Lines[ i ] );
			}
		}
		mm.MemberMesList = new MemberMes[ MemberNumber ];
		for( int i = 0; i < MemberNumber; ++i ) {
			mm.MemberMesList[ i ] = MemberMesList[ i ];
		}
		mm.ModuleMes = ModuleMes;
	}
	
	//---------------------------------------------------------------
	
	//硬件连接器配置
	static int LinkConfig( MyObjectList myModuleList, MidPortList myMidPortList, string[][] ConfigList, int i )
	{
		string[] ConfigCmdCut = ConfigList[ i ];
		HardModule CM = (HardModule)myModuleList.GetModuleFromName( ConfigCmdCut[ 1 ] );
		if( CM == null ) {
			n_Debug.Warning.BUG( "<连线配置> 未知的模块: " + ConfigCmdCut[ 1 ] );
			return i;
		}
		Port p = CM.GetPort( ConfigCmdCut[ 2 ] );
		if( p == null ) {
			n_Debug.Warning.BUG( "<连线配置> 未知的模块针脚: " + ConfigCmdCut[ 2 ] );
			return i;
		}
		if( p.FuncName == PortFuncName.INTERFACE ) {
			p.TargetModule = (HardModule)myModuleList.GetModuleFromName( ConfigCmdCut[ 4 ] );
		}
		else if( ConfigCmdCut[ 4 ] != "*" ) {
			try {
			p.TargetPort = ((HardModule)myModuleList.GetModuleFromName( ConfigCmdCut[ 4 ] )).GetPort( ConfigCmdCut[ 5 ] );
			}catch {
				n_Debug.Warning.BUG( "模块引脚连接异常: " + ConfigCmdCut[ 4 ] + "." + ConfigCmdCut[ 5 ] );
			}
			if( p.TargetPort == null ) {
				n_Debug.Warning.BUG( "不存在的模块引脚: " + ConfigCmdCut[ 4 ] + "." + ConfigCmdCut[ 5 ] );
			}
		}
		else {
			//...
		}
		//判断是否有导线颜色配置
		int ci = ConfigCmdCut[ 3 ].IndexOf( '#' );
		if( ci != -1 ) {
			string SNumber = ConfigCmdCut[ 3 ].Remove( 0, ci + 1 );
			p.ColorIndex = int.Parse( SNumber );
		}
		//判断是否有过渡节点
		if( ConfigCmdCut.Length > 6 ) {
			int Index = int.Parse( ConfigCmdCut[ 6 ] );
			p.MidPortIndex = Index;
			myMidPortList.SetList( Index );
			for( int n = 7; n < ConfigCmdCut.Length; ++n ) {
				
				if( ConfigCmdCut[n].StartsWith( "&" ) ) {
					string[] cut = ConfigCmdCut[n].Remove( 0, 1 ).Split( ',' );
					int listi = int.Parse( cut[0] );
					int idx = int.Parse( cut[1] );
					myMidPortList.AddExistPort( Index, listi, idx );
				}
				else {
					string[] cut = ConfigCmdCut[n].Split( ',' );
					float x = float.Parse( cut[0] );
					float y = float.Parse( cut[1] );
					myMidPortList.AddPort( Index, x, y );
				}
			}
		}
		return i;
	}
	
	//电路线列表配置
	//桌面参数配置
	static int CLineConfig( ImagePanel GPanel, string[][] ConfigList, int i )
	{
		string[] ConfigCmdCut = ConfigList[i];
		
		string[] cut0 = ConfigCmdCut[1].Split( ',' );
		string[] cut1 = ConfigCmdCut[2].Split( ',' );
		
		int CP0X = int.Parse( cut0[0] );
		int CP0Y = int.Parse( cut0[1] );
		
		int CP1X = int.Parse( cut1[0] );
		int CP1Y = int.Parse( cut1[1] );
		
		GPanel.myCLineList.AddList( false, CP0X, CP0Y, CP1X, CP1Y );
		
		return i;
	}
	
	//---------------------------------------------------------------
	
	//指令连接器配置
	static int LinkInsConfig( MyObjectList myModuleList, string[][] ConfigList, int i )
	{
		string[] ConfigCmdCut = ConfigList[ i ];
		MyIns CM = (MyIns)myModuleList.GetModuleFromName( ConfigCmdCut[ 1 ] );
		MyIns TM = (MyIns)myModuleList.GetModuleFromName( ConfigCmdCut[ 4 ] );
		if( ConfigCmdCut[ 2 ] == "PreIns" ) {
			CM.PreIns = TM;
		}
		else if( ConfigCmdCut[ 2 ] == "NextIns" ) {
			CM.NextIns = TM;
		}
		else {
			n_Debug.Warning.BUG( "<LinkInsConfig> 未知的配置项: " + ConfigCmdCut[ 2 ] );
		}
		return i;
	}
	
	//UserFunction连接器配置
	static int LinkUserFunctionConfig( MyObjectList myModuleList, string[][] ConfigList, int i )
	{
		string[] ConfigCmdCut = ConfigList[ i ];
		UserFunctionIns CM = (UserFunctionIns)myModuleList.GetModuleFromName( ConfigCmdCut[ 1 ] );
		CondiEndIns TM = (CondiEndIns)myModuleList.GetModuleFromName( ConfigCmdCut[ 4 ] );
		CM.InsEnd = TM;
		TM.InsStart = CM;
		TM.CanLinked = false;
		return i;
	}
	
	//Event连接器配置
	static int LinkEventConfig( MyObjectList myModuleList, string[][] ConfigList, int i )
	{
		string[] ConfigCmdCut = ConfigList[ i ];
		EventIns CM = (EventIns)myModuleList.GetModuleFromName( ConfigCmdCut[ 1 ] );
		CondiEndIns TM = (CondiEndIns)myModuleList.GetModuleFromName( ConfigCmdCut[ 4 ] );
		CM.InsEnd = TM;
		TM.InsStart = CM;
		TM.CanLinked = false;
		return i;
	}
	
	//Loop连接器配置
	static int LinkLoopConfig( MyObjectList myModuleList, string[][] ConfigList, int i )
	{
		string[] ConfigCmdCut = ConfigList[ i ];
		LoopIns CM = (LoopIns)myModuleList.GetModuleFromName( ConfigCmdCut[ 1 ] );
		CondiEndIns TM = (CondiEndIns)myModuleList.GetModuleFromName( ConfigCmdCut[ 4 ] );
		CM.InsEnd = TM;
		TM.InsStart = CM;
		return i;
	}
	
	//Ext连接器配置
	static int LinkExtConfig( MyObjectList myModuleList, string[][] ConfigList, int i )
	{
		string[] ConfigCmdCut = ConfigList[ i ];
		ExtIns CM = (ExtIns)myModuleList.GetModuleFromName( ConfigCmdCut[ 1 ] );
		CondiEndIns TM = (CondiEndIns)myModuleList.GetModuleFromName( ConfigCmdCut[ 4 ] );
		CM.InsEnd = TM;
		TM.InsStart = CM;
		return i;
	}
	
	//Forever连接器配置
	static int LinkForeverConfig( MyObjectList myModuleList, string[][] ConfigList, int i )
	{
		string[] ConfigCmdCut = ConfigList[ i ];
		ForeverIns CM = (ForeverIns)myModuleList.GetModuleFromName( ConfigCmdCut[ 1 ] );
		CondiEndIns TM = (CondiEndIns)myModuleList.GetModuleFromName( ConfigCmdCut[ 4 ] );
		CM.InsEnd = TM;
		TM.InsStart = CM;
		return i;
	}
	
	//While连接器配置
	static int LinkWhileConfig( MyObjectList myModuleList, string[][] ConfigList, int i )
	{
		string[] ConfigCmdCut = ConfigList[ i ];
		WhileIns CM = (WhileIns)myModuleList.GetModuleFromName( ConfigCmdCut[ 1 ] );
		CondiEndIns TM = (CondiEndIns)myModuleList.GetModuleFromName( ConfigCmdCut[ 4 ] );
		CM.InsEnd = TM;
		TM.InsStart = CM;
		return i;
	}
	
	//IfElse连接器配置
	static int LinkIfElseConfig( MyObjectList myModuleList, string[][] ConfigList, int i )
	{
		string[] ConfigCmdCut = ConfigList[ i ];
		IfElseIns CM = (IfElseIns)myModuleList.GetModuleFromName( ConfigCmdCut[ 1 ] );
		ElseIns TM = (ElseIns)myModuleList.GetModuleFromName( ConfigCmdCut[ 4 ] );
		CM.ElseIns = TM;
		TM.InsStart = CM;
		return i;
	}
	
	//IfElseEnd连接器配置
	static int LinkIfElseEndConfig( MyObjectList myModuleList, string[][] ConfigList, int i )
	{
		string[] ConfigCmdCut = ConfigList[ i ];
		IfElseIns CM = (IfElseIns)myModuleList.GetModuleFromName( ConfigCmdCut[ 1 ] );
		CondiEndIns TM = (CondiEndIns)myModuleList.GetModuleFromName( ConfigCmdCut[ 4 ] );
		CM.ElseIns = null;
		CM.InsEnd = TM;
		TM.InsStart = CM;
		return i;
	}
	
	//Else连接器配置
	static int LinkElseConfig( MyObjectList myModuleList, string[][] ConfigList, int i )
	{
		string[] ConfigCmdCut = ConfigList[ i ];
		ElseIns CM = (ElseIns)myModuleList.GetModuleFromName( ConfigCmdCut[ 1 ] );
		CondiEndIns TM = (CondiEndIns)myModuleList.GetModuleFromName( ConfigCmdCut[ 4 ] );
		CM.InsEnd = TM;
		CM.InsStart.InsEnd = TM;
		TM.InsStart = CM.InsStart;
		return i;
	}
	
	//---------------------------------------------------------------
	
	//生成变量定义代码
	static string AutoDefineCode( int ChipIndex, MyObjectList myModuleList )
	{
		string GUItext = null;
		
		int BitmapBufferSize = 0;
		
		//转换变量定义代码
		foreach( MyObject mo in myModuleList ) {
			if( mo is GVar ) {
				GVar gv = (GVar)mo;
				if( gv.ChipIndex != ChipIndex ) {
					continue;
				}
				if( gv.StoreType != "" ) {
					GUItext += gv.StoreType + " ";
				}
				if( !gv.isConst ) {
					
					GUItext += gv.VarType + " " + gv.Name + ";\n";
					
					//带有存储类型的变量就不要初始化了
					if( gv.StoreType == "" ) {
						if( gv.VarType == GType.g_int32 ) {
							InitCode += "\t" + gv.Name + " = 0;\n";
						}
						if( gv.VarType == GType.g_fix ) {
							InitCode += "\t" + gv.Name + " = 0.0;\n";
						}
						if( gv.VarType == GType.g_bool ) {
							InitCode += "\t" + gv.Name + " = false;\n";
						}
					}
					
					continue;
				}
				if( gv.VarType == GType.g_int32 ) {
					GUItext += "const " + gv.VarType + " " + gv.Name + " = " + gv.Value + ";\n";
					continue;
				}
				if( gv.VarType == GType.g_fix ) {
					GUItext += "const " + gv.VarType + " " + gv.Name + " = " + gv.Value + ";\n";
					continue;
				}
				if( gv.VarType == GType.g_Astring ) {
					GUItext += "[#.code int8*?] " + gv.Name + " = " + gv.Value + ";\n";
					continue;
				}
				if( gv.VarType == GType.g_Rstring ) {
					GUItext += "[#.code uint8*?] " + gv.Name + " = " + gv.Value + ";\n";
					continue;
				}
				if( gv.VarType == GType.g_cbitmap ) {
					if( gv.Value != "" ) {
						string s = gv.Value;
						GUItext += "[]#.code uint16 " + gv.Name + " = {";
						GUItext += gv.BitmapWidth + "," + gv.BitmapHeight + ",\n";
						StringBuilder sbd = new StringBuilder( "" );
						string[] cut = gv.Value.Trim( ' ' ).Split( ' ' );
						int len = cut.Length;
						for( int i = 0; i < len; ++i ) {
							string[] cc = cut[i].Split( ',' );
							int r = int.Parse( cc[0] );
							int g = int.Parse( cc[1] );
							int b = int.Parse( cc[2] );
							int v = ((r>>3)<<11) + ((g>>2)<<5) + (b>>3);
							sbd.Append( "0x" + v.ToString( "X" ).PadLeft( 4, '0' ) );
							if( i != len - 1 ) {
								sbd.Append( "," );
							}
						}
						GUItext += sbd.ToString() + "};\n";
					}
					else {
						GUItext += "[]#.code uint16 " + gv.Name + " = { 0 };\n";
					}
					continue;
				}
				if( gv.VarType == GType.g_bitmap || gv.VarType == GType.g_font ) {
					if( gv.Value != "" ) {
						string s = gv.Value;
						if( s.StartsWith( n_LatticeToCode.LatticeToCode.L_V1_RightDown ) ) {
							s = s.Remove( 0, 13 );
						}
						GUItext += "[#.code uint8*?] " + gv.Name + " =\n{\n" + s + "\n};\n";
					}
					else {
						GUItext += "->struct GUI.icon " + gv.Name + ";\n";
						int bWidth = gv.BitmapWidth;
						int bHeight = gv.BitmapHeight;
						InitCode += "\t" + gv.Name + " -> GUI.OS_CreateIcon( " + bWidth + ", " + bHeight + " );\n";
						
						//这里以及GUI库是对应的, 必须同步更改
						BitmapBufferSize += 9 + bWidth * (bHeight / 8);
					}
					continue;
				}
				if( gv.VarType == GType.g_music ) {
					string mvalue = gv.Value;
					if( mvalue == "" ) {
						mvalue = "\n\n";
					}
					string[] mud = mvalue.Split( '\n' );
					mud[1] = mud[1] + "255,";
					mud[2] = mud[2] + "255,";
					int number = 6;
					for( int x = 0; x < mud[1].Length; ++x ) {
						if( mud[1][x] == ',' ) number++;
					}
					GUItext += "[#.code uint8*?] " + gv.Name + " =\n{\n";
					GUItext += "2,0,6,0," + (number%256) + "," + (number/256) + ",\n";
					GUItext += mud[1] + "\n" + mud[2] + "\n" + "};\n";
					continue;
				}
				n_Debug.Warning.BUG( "<AutoDefineCode> 未处理的变量类型: " + gv.VarType );
			}
		}
		if( BitmapBufferSize != 0 ) {
			GUItext += "GUI.OS_BufferLength = GUI_OS_BufferLength;\n";
			GUItext += "const int16 GUI_OS_BufferLength = " + BitmapBufferSize + ";\n";
		}
		//不添加系统定义
		//GUItext += LocalVarDefine() + "\n";
		
		return GUItext;
	}
	
	//生成代码
	public static string GetUserCode( bool c, int ChipIndex, MyObjectList myModuleList )
	{
		//转换新版指令流程图代码
		
		string code = "";
		foreach( MyObject mo in myModuleList ) {
			if( mo is EventIns || mo is UserFunctionIns ) {
				
				if( mo is UserFunctionIns ) {
					UserFunctionIns ui = (UserFunctionIns)mo;
					if( ui.isEvent && ui.Target != null ) {
						continue;
					}
				}
				
				if( CodePython ) {
					code += ConvertOneMyInsForPython( ChipIndex, (MyIns)mo );
				}
				else {
					code += ConvertOneMyIns( ChipIndex, (MyIns)mo );
				}
			}
		}
		return code;
	}
	
	//---------------------------------------------------------------
	//生成流程图代码
	static string AutoFlowCode( int ChipIndex, MyObjectList myModuleList, string ClientChannelName )
	{
		//SwitchFunction = false;
		
		string GUItext = SwitchLoopCode( ChipIndex, myModuleList, ClientChannelName );
		
		if( ChipIndex != 0 ) {
			return GUItext + "}\n";
		}
		//转换新版指令流程图代码
		GUItext += GetUserCode( true, ChipIndex, myModuleList );
		
		
		//转换新版用户自定义指令
		//SwitchFunction = true;
//		UserInsList = "";
//		for( int i = 0; i < myModuleList.ModuleList.Length; ++i ) {
//			if( myModuleList.ModuleList[ i ] == null ) {
//				continue;
//			}
//			if( myModuleList.ModuleList[ i ] is UserFunctionIns ) {
//				UserInsList += ConvertOneMyIns( ChipIndex, (MyIns)myModuleList.ModuleList[ i ] );
//				//UserInsList += "}\n";
//			}
//		}
		//返回结果
		return GUItext;
	}
	
	//转换扫描代码
	static string SwitchLoopCode( int ChipIndex, MyObjectList myModuleList, string ClientChannelName )
	{
		string code = null;
		string ControlName = null;
		
		//转换链接配置信息
		foreach( MyObject mo in myModuleList ) {
			if( mo.ChipIndex != ChipIndex ) {
				continue;
			}
			if( mo is MyFileObject ) {
				MyFileObject m = (MyFileObject)mo;
				
				if( m is MyFileObject && ((MyFileObject)m).isControlModule ) {
					ControlName = m.Name;
				}
				
				string EventScan = "";
				for( int ei = 0; ei < m.EventList.Length; ++ei ) {
					
					if( m.EventList[ ei ][ 0 ] == "" ) {
						continue;
					}
					//0:保留事件标志  1:忽略事件标志  2:强制重启
					int EventTrigType = 1;
					
					string event_name = n_GroupList.Group.GetFullName( m.GroupMes, m.EventList[ ei ][ 0 ] );
					
					if( !GOTO ) {
						DefineCode += "uint8 x_" + m.Name + "_" + ei + "_ResCount;\n";
						DefineCode += "bool " + event_name + n_CoroCompiler.CoroCompiler.E_En + ";\n";
						InitCode += "\t" + event_name + n_CoroCompiler.CoroCompiler.E_En + " = true;\n";
					}
					
					string evflag = m.Name + ".OS_EventFlag";
					int ttei = ei;
					if( ei >= 32 ) {
						evflag = m.Name + ".OS_EventFlag1";
						ttei = ei - 32;
					}
					//读取控件事件
					if( m is UIModule ) {
						EventScan += "\t" + m.Name + ".OS_EventFlag = zui_OList[" + m.Name + ".driver.VUI_ID].Event;\n";
					}
					
					EventScan += "\tif( " + evflag + "." + ttei + "(bit) == 1 ) {\n";
					
					if( EventTrigType == 0 ) {
						
					}
					else {
						EventScan += "\t\t" + evflag + "." + ttei + "(bit) = 0;\n";
						//清除控件事件
						if( m is UIModule ) {
							EventScan += "\t\tzui_OList[" + m.Name + ".driver.VUI_ID].Event = " + m.Name + ".OS_EventFlag;\n";
						}
					}
					
					string EE = event_name + n_CoroCompiler.CoroCompiler.E_En;
					
					if( DebugOpen ) {
						EE = "!SYS_Debug.StopUser && " + EE;
					}
					
					if( GOTO ) {
						EventScan += "\t\tif( " + EE + " && " + event_name + n_CoroCompiler.CoroCompiler.E_Flag + " == 0 ) {\n";
					}
					else {
						EventScan += "\t\tif( " + EE + " && x_" + m.Name + "_" + ei + "_ResCount == 0 ) {\n";
					}
					//清除事件标志
					if( EventTrigType == 0 ) {
						EventScan += "\t\t" + evflag + "." + ttei + "(bit) = 0;\n";
						//清除控件事件
						if( m is UIModule ) {
							EventScan += "\t\tzui_OList[" + m.Name + ".driver.VUI_ID].Event = " + m.Name + ".OS_EventFlag;\n";
						}
					}
					else {
						
					}
					
					//触发执行事件
					if( !GOTO ) {
						EventScan += "\t\t\tOS0.AppTCB[OS0.CurrentTaskIndex].e_res_count -> x_" + m.Name + "_" + ei + "_ResCount;\n";
					}
					
					//正常linkboy程序
					if( m.EventList[ ei ][ 0 ] != "" ) {
						
						string temp = "";
						if( m.ImageName == SPMoudleName.SYS_Sprite ) {
							temp = m.Name + ".设置事件ID_( x_TempAddEvent ); ";
						}
						if( GOTO ) {
							EventScan += "\t\t\trun " + event_name + "();\n";
						}
						else {
							EventScan += "\t\t\tx_TempAddEvent = OS" + ChipIndex + ".CreateTask( &" + event_name + " ); " + temp +
								"OS" + ChipIndex + ".TrigEvent( x_TempAddEvent );\n";
						}
					}
					//如果为空表示在 MakerLib 中运行
					else {
						n_Debug.Warning.BUG( "事件名称为空" );
					}
					EventScan += "\t\t}\n";
					EventScan += "\t}\n";
					
					/*
					if( GOTO ) {
						EventScan += "\tif( " + event_name + "_flg != 0 ) {\n";
						
						int eidx = -1;
						for( int n = 0; n < GoNameList.Length; ++n ) {
							if( GoNameList[n] == event_name ) {
								eidx = n;
								break;
							}
						}
						EventScan += "\t\tOS0.CurrentTaskIndex = " + eidx + ";\n";
						
						EventScan += "\t\t" + event_name + "();\n";
						EventScan += "\t}\n";
					}
					*/
				}
				//判断是否需要调整扫描顺序
				if( m is MyFileObject && ((MyFileObject)m).isControlModule ) {
					code = EventScan + code;
				}
				else {
					code += EventScan;
				}
			}
		}
		code = "\tif( OS0.EnableSchedule ) {\n" + code + "\t}\n";
		
		if( !GOTO ) {
			code = "\tdispatch:\n" +
				"\tloop {\n" +
				code +
				"\t}\n";
		}
		
		//不再适用以前的查询事件方式
		//if( ClientChannelName != null ) {
		//	code += "\t\t" + ClientChannelName + ".GetEventFlag();\n";
		//}
		
		if( G.ccode.ExistPython() ) {
			//code += "\t\tPYVM.Run();\n";
			
			/*
			code += "\tif( x_SYS_PYVM_ResCount == 0 ) {\n";
			code += "\t\tOS0.AppTCB[OS0.CurrentTaskIndex].e_res_count -> x_SYS_PYVM_ResCount;\n";
			code += "\t\tx_TempAddEvent = OS" + ChipIndex + ".CreateTask( &PYVM.Run );\n";
			code += "\t\tOS" + ChipIndex + ".TrigEvent( x_TempAddEvent );\n";
			code += "\t}\n";
			*/
		}
		
		//转换线程信息
		foreach( MyObject mo in myModuleList ) {
			if( mo.ChipIndex != ChipIndex ) {
				continue;
			}
			if( mo is MyFileObject ) {
				MyFileObject CM = (MyFileObject)mo;
				//注入 内核线程 到调度器
				if( Contain( CM, InterfaceType.Function_, AIsuaUseName.OS_thread ) ) {
					code += "\t" + mo.Name + ".OS_thread();\n";
				}
			}
		}
		//事件扫描结束部分, 运行看门狗清零
		if( !GOTO ) {
			code += "\tOS" + ChipIndex + ".Schedule();\n";
		}
		
		if( DebugOpen ) {
			code += "\tSYS_Debug.Debug_loop();\n";
			code += "\tif( SYS_Debug.Open && OS0.DebugTick > 100 ) {\n";
			code += "\t\tOS0.DebugTick = 0;\n";
			code += GetModuleMember( myModuleList );
			code += "\t}\n";
		}
		
		if( cmd_simulate ) {
			code += "\tSim.OS_thread();\n";
		}
		
		code += "<<coroback>>\n";
		
		if( ControlName != null ) {
			//code += "\t" + ControlName + ".OS_ClearWatchDog();\n";
		}
		//增加仿真软中断指令
		
		
		/*
		//if( G.SimulateMode ) { //5.0发布时候改为下边的, 同时增加版本校验
		if( cputype1 == CPU_Type.VM ) {
			//code += "\t\t#asm \"" + n_VCODE.C.SYS + "\"\n";
			code += "\tSYS_Loop = 1;\n";
		}
		*/
		
		//难道不应该是所有MCU都应加上这个吗????
		code += "\tSYS_Loop = 1;\n";
		
		
		code += "}\n";
		//code += "#param userspaceon\n";
		//code += "uint8 x_SYS_PYVM_ResCount;\n";
		//code += "void Test()\n{\n";
		//code += "\tPYVM.Run();\n";
		//code += "}\n";
		
		return code;
	}
	
	//遍历模块列表, 添加调试函数和调试变量
	static string GetModuleMember( MyObjectList myModuleList )
	{
		string c = "";
		
		foreach( MyObject mo in myModuleList ) {
			
			if( ( mo is MyFileObject ) ) {
				
				MyFileObject m = (MyFileObject)mo;
				
				if( cmd_download ) {
					m.DebugOpen = false;
				}
				
				for( int n = 0; n < m.ElementList.Length; ++n ) {
				
					//扫描函数成员
					if( m.ElementList[ n ][ 1 ].StartsWith( InterfaceType.Function_ ) && !m.ElementList[ n ][ 2 ].StartsWith( "OS_" ) ) {
						string[] tempTypeList = m.ElementList[ n ][ 1 ].Split( '_' );
						
						if( tempTypeList.Length >= 3 && tempTypeList[2] == FuncExtType.Debug ) {
							c += AddValue( mo.Index, m.Name + "." + m.ElementList[ n ][ m.GetLC() ] + "()" );
						}
					}
					//扫描变量成员
					if( (m.ElementList[ n ][ 1 ].StartsWith( InterfaceType.Var_ ) || m.ElementList[ n ][ 1 ].StartsWith( InterfaceType.LinkVar_ )) && !m.ElementList[ n ][ 2 ].StartsWith( "OS_" ) ) {
						string[] tempTypeList = m.ElementList[ n ][ 1 ].Split( '_' );
						if( tempTypeList.Length >= 3 && tempTypeList[2] == FuncExtType.Debug ) {
							c += AddValue( mo.Index, m.Name + "." + m.ElementList[ n ][ m.GetLC() ] );
						}
					}
				}
			}
			if( mo is GVar ) {
				
				GVar gv = (GVar)mo;
				
				if( cmd_download ) {
					gv.DebugOpen = false;
				}
				
				if( "?" + gv.VarType == n_EXP.EXP.ENote.v_bool ) {
					//c += "\t\tSYS_Debug.send_byte( 0xAA );\n";
					//c += "\t\tSYS_Debug.send_byte( " + i + " );\n";
					//c += AddValue( gv.Name );
				}
				if( "?" + gv.VarType == n_EXP.EXP.ENote.v_int32 ) {
					c += AddValue( mo.Index, gv.Name );
				}
			}
		}
		return c;
	}
	
	//添加一个数据
	static string AddValue( int i, string Data )
	{
		string c = "\t\t\tSYS_Debug.send_data( " + i + ", " + Data + " );\n";
		return c;
	}
	
	static string ModName;
	static string EGO_FuncName; //当前的事件或者函数名
	
	//转换一个流程图代码
	static string ConvertOneMyIns( int ChipIndex, MyIns m )
	{
		MyIns first = m;
		
		string Res = null;
		string Tab = "\t";
		while( m != null ) {
			
			if( m is EventIns ) {
				EventIns myi = (EventIns)m;
				
				string ename = myi.EventEntry;
				
				//如果是代码实验室, 则创建临时事件函数
				bool nottask =  G.isCruxEXE && myi.NextIns is CondiEndIns;
				if( nottask ) {
					ename += "_hide";
				}
				
				if( cmd_download && myi.owner == null ) {
					n_Debug.Warning.WarningMessage = "此事件<" + myi.EventEntry + ">为无效事件, 其中的程序永远都不会执行! 请删除此事件或者点击对应模块的事件按钮重新连接激活";
				}
				//InitCode += "\t" + myi.EventEntry + "_flg = 0;\n";
				//Res += "uint8 " + myi.EventEntry + "_flg;\n";
				
				if( nottask ) {
					Res += "void " + ename + "(void)\n";
				}
				else {
					Res += "task void " + ename + "(void)\n";
				}
				
				Res += "{\n";
				
				/*
				if( GOTO ) {
					EGO_FuncName = myi.EventEntry;
					EGO_Index = 1;
					Res += Tab + "goto " + EGO_FuncName + "_label_end;\n";
					Res += Tab + EGO_FuncName + "_label" + EGO_Index + ":\n";
					EGO_Index++;
				}
				*/
				
				//Res += LocalVarDefine() + "\n";
				Tab = "\t";
				Res += AddInsSim( Tab, ChipIndex, m );
				m = m.NextIns;
				continue;
			}
			if( (m is CondiEndIns && !m.isNote) || (m is CondiEndIns && ((CondiEndIns)m).InsStart is EventIns ) ) {
				//if( ((CondiEndIns)m).CanLinked || SwitchFunction ) {
					Res += AddInsSim( Tab, ChipIndex, m );
					
					if( ((CondiEndIns)m).InsStart is ExtIns ) {
						ExtIns ei = (ExtIns)((CondiEndIns)m).InsStart;
						if( ei.ShunXu ) {
							Res += Tab + ModName + ".设置为下一个();\n";
						}
						else {
							Res += Tab + ModName + ".设置为上一个();\n";
						}
					}
					/*
					if( GOTO && ((CondiEndIns)m).InsStart is EventIns || ((CondiEndIns)m).InsStart is UserFunctionIns ) {
						Res += Tab + EGO_FuncName + "_flg = 0;\n";
						Res += Tab + "return;\n";
						Res += Tab + EGO_FuncName + "_label_end:\n";
						
						for( int i = 1; i < EGO_Index; ++i ) {
							Res += Tab + "if( " + EGO_FuncName + "_flg == " + i + " ) { goto " + EGO_FuncName + "_label" + i + "; }\n";
						}
					}
					*/
					//这里极个别情况下会异常, 不知道为什么
					if( Tab.Length != 0 ) {
						Tab = Tab.Remove( 0, 1 );
					}
					else{
						n_Debug.Warning.BUG( "<ConvertOneMyIns> [Start] 刚刚捕获了一个小小的软件异常, 应该不会影响您的后续操作." );
					}
					//仿真模式下需要添加结尾的返回语句合规
					if( cmd_simulate && (m is CondiEndIns && ((CondiEndIns)m).InsStart is UserFunctionIns ) ) {
						UserFunctionIns myi = (UserFunctionIns)(((CondiEndIns)m).InsStart);
						string rtype = myi.GetUserType();
						if( rtype == VarType.BaseType.Sint32 ) {
							Res += Tab + "return 0;\n";
						}
						if( rtype == VarType.BaseType.Bool ) {
							Res += Tab + "return false;\n";
						}
					}
					
					Res += Tab + "}\n";
				//}
				//else {
				//	Res += Tab + "return;\n";
				//}
				
				m = m.NextIns;
				continue;
			}
			if( m.isNote ) {
				m = m.NextIns;
				continue;
			}
			if( m is UserFunctionIns ) {
				UserFunctionIns myi = (UserFunctionIns)m;
				
				string funcdef = myi.GetFuncDefine( ref EGO_FuncName );
				
				string rtype = myi.GetUserType();
				if( rtype == VarType.Void ) {
					Res += "task ";
				}
				
				//InitCode += "\t" + EGO_FuncName + "_flg = 0;\n";
				//Res += "uint8 " + EGO_FuncName + "_flg;\n";
				Res += funcdef + AddNoteForIns( m ) + "\n";
				Res += "{\n";
				
				/*
				if( GOTO ) {
					EGO_Index = 1;
					Res += Tab + "goto " + EGO_FuncName + "_label_end;\n";
					Res += Tab + EGO_FuncName + "_label" + EGO_Index + ":\n";
					EGO_Index++;
				}
				*/
				//Res += LocalVarDefine() + "\n";
				Tab = "\t";
				Res += AddInsSim( Tab, ChipIndex, m );
				m = m.NextIns;
				continue;
			}
			if( m is FuncIns ) {
				
				FuncIns myi = (FuncIns)m;
				
				//调用此函数后更新 ExistUserFunc
				string exp = myi.MyEXP.GetExpCode();
				/*
				if( GOTO ) {
					if( n_EXP.EXP.ExistUserFunc ) {
						Res += Tab + EGO_FuncName + "_flg = " + EGO_Index + ";\n";
						Res += Tab + n_EXP.EXP.UserFuncName + "_flg = 1;\n";
						Res += Tab + EGO_FuncName + "_label" + EGO_Index + ":\n";
						EGO_Index++;
					}
				}
				*/
				Res += AddInsSim( Tab, ChipIndex, m );
				Res += Tab + exp + ";" + AddNoteForIns( m ) + "\n";
				/*
				if( GOTO ) {
					if( myi.MyEXP.GetExpCode().IndexOf( "延时器" ) != -1 ) {
						Res += Tab + EGO_FuncName + "_flg = " + EGO_Index + ";\n";
						Res += Tab + EGO_FuncName + "_label" + EGO_Index + ":\n";
						Res += Tab + "if( OS0.enable_list[OS0.CurrentTaskIndex] ) return;\n";
						EGO_Index++;
					}
					if( n_EXP.EXP.ExistUserFunc ) {
						Res += Tab + "if( " + n_EXP.EXP.UserFuncName + "_flg != 0 ) return;\n";
					}
				}
				*/
				m = m.NextIns;
				continue;
			}
			string schd = " OS" + ChipIndex + ".Schedule(); ";
			if( GOTO ) {
				schd = "\n\t\tOS" + ChipIndex + ".Schedule(); ";
			}
			if( m is WaitIns ) {
				WaitIns myi = (WaitIns)m;
				Res += AddInsSim( Tab, ChipIndex, m );
				Res += Tab + "while( !(" + myi.MyEXP.GetExpCode() + ") ) {" + schd + AddNoteForIns( m ) + "\n";
				Res += Tab + "}\n";
				m = m.NextIns;
				continue;
			}
			if( m is ForeverIns ) {
				ForeverIns myi = (ForeverIns)m;
				Res += Tab + "loop {\n";
				Tab += "\t";
				Res += AddInsSim( Tab, ChipIndex, m );
				//Res += Tab + schd + "\n";
				m = m.NextIns;
				continue;
			}
			if( m is LoopIns ) {
				LoopIns myi = (LoopIns)m;
				//Res += Tab + "loop( " + myi.MyEXP.GetExpCode() + " ) {" + schd + AddNoteForIns( m ) + "\n";
				Res += Tab + "loop( " + myi.MyEXP.GetExpCode() + " ) {" + AddNoteForIns( m ) + "\n";
				Tab += "\t";
				Res += AddInsSim( Tab, ChipIndex, m );
				m = m.NextIns;
				continue;
			}
			if( m is WhileIns ) {
				WhileIns myi = (WhileIns)m;
				//Res += Tab + "while( " + myi.MyEXP.GetExpCode() + " ) {" + schd + AddNoteForIns( m ) + "\n";
				Res += Tab + "while( " + myi.MyEXP.GetExpCode() + " ) {" + AddNoteForIns( m ) + "\n";
				Tab += "\t";
				Res += AddInsSim( Tab, ChipIndex, m );
				m = m.NextIns;
				continue;
			}
			if( m is ExtIns ) {
				ExtIns myi = (ExtIns)m;
				ModName = myi.MyEXP.GetExpCode();
				if( myi.ShunXu ) {
					Res += Tab + ModName + ".设置为第一个();" + AddNoteForIns( m ) + "\n";
				}
				else {
					Res += Tab + ModName + ".设置为最后一个();" + AddNoteForIns( m ) + "\n";
				}
				
				Res += Tab + "while( " + ModName + ".非空() ) {" + AddNoteForIns( m ) + "\n";
				Tab += "\t";
				Res += AddInsSim( Tab, ChipIndex, m );
				m = m.NextIns;
				continue;
			}
			if( m is IfElseIns ) {
				IfElseIns myi = (IfElseIns)m;
				Res += Tab + "if( " + myi.MyEXP.GetExpCode() + " ) {" + AddNoteForIns( m ) + "\n";
				Tab += "\t";
				Res += AddInsSim( Tab, ChipIndex, m );
				m = m.NextIns;
				continue;
			}
			if( m is ElseIns ) {
				Tab = Tab.Remove( 0, 1 );
				Res += Tab + "}\n";
				Res += Tab + "else {\n";
				Tab += "\t";
				Res += AddInsSim( Tab, ChipIndex, m );
				m = m.NextIns;
				continue;
			}
			n_Debug.Warning.BUG( "<ConvertOneMyIns> 未知的指令: " + m.GetType() );
			return null;
		}
		
		
		if( first.GroupMes != null ) {
			Res = n_GroupList.Group.GetFullName( first.GroupMes, Res );
		}
		
		return Res;
	}
	
	//添加一个指令索引注释
	static string AddNoteForIns( MyIns m )
	{
		return " //#" + m.Index + " ";
	}
	
	//转换一个流程图代码
	public static string ConvertOneMyInsForRealPython( int ChipIndex, MyIns m )
	{
		string Res = null;
		string Tab = "\t";
		while( m != null ) {
			
			if( m is UserFunctionIns ) {
				UserFunctionIns myi = (UserFunctionIns)m;
				
				n_EXP.EXP.EFName = myi.GetFuncName();
				
				Res += myi.GetFuncDefinePython() + "\n";
				//Res += LocalVarDefine() + "\n";
				Tab = "\t";
				Res += AddInsSim( Tab, ChipIndex, m );
				m = m.NextIns;
				continue;
			}
			if( m is EventIns ) {
				EventIns myi = (EventIns)m;
				//Res += "# 当事件：" + myi.EventEntry + " 成立则执行以下代码\n";
				Res += "def " + myi.EventEntry + "():\n";
				
				//Res += LocalVarDefine() + "\n";
				Tab = "\t";
				
				n_EXP.EXP.EFName = myi.EventEntry;
				
				Res += Tab + "global " + myi.EventEntry + "_sys_time, 延时器_time\n";
				Res += Tab + "#<global>\n";
				
				Res += Tab + n_EXP.EXP.EFName + "_sys_time = 1\n"+ Tab + "data = yield\n";
				
				Res += AddInsSim( Tab, ChipIndex, m );
				m = m.NextIns;
				continue;
			}
			if( m is FuncIns ) {
				FuncIns myi = (FuncIns)m;
				Res += AddInsSim( Tab, ChipIndex, m );
				Res += Tab + myi.MyEXP.Py_GetExpCode( Tab ) + "\n";
				m = m.NextIns;
				continue;
			}
			if( m is WaitIns ) {
				WaitIns myi = (WaitIns)m;
				Res += AddInsSim( Tab, ChipIndex, m );
				Res += Tab + "while !(" + myi.MyEXP.Py_GetExpCode( Tab ) + "): pass\n";
				m = m.NextIns;
				continue;
			}
			if( m is ForeverIns ) {
				ForeverIns myi = (ForeverIns)m;
				Res += Tab + "while True:\n";
				Tab += "\t";
				Res += AddInsSim( Tab, ChipIndex, m );
				//Res += Tab + "OS" + ChipIndex + ".Schedule()\n";
				m = m.NextIns;
				continue;
			}
			if( m is LoopIns ) {
				LoopIns myi = (LoopIns)m;
				Res += Tab + "for n in range(" + myi.MyEXP.Py_GetExpCode( Tab ) + "):\n";
				Tab += "\t";
				Res += AddInsSim( Tab, ChipIndex, m );
				m = m.NextIns;
				continue;
			}
			if( m is ExtIns ) {
				ExtIns myi = (ExtIns)m;
				Res += Tab + "foreach(" + myi.MyEXP.Py_GetExpCode( Tab ) + "):\n";
				Tab += "\t";
				Res += AddInsSim( Tab, ChipIndex, m );
				m = m.NextIns;
				continue;
			}
			if( m is WhileIns ) {
				WhileIns myi = (WhileIns)m;
				Res += Tab + "while " + myi.MyEXP.Py_GetExpCode( Tab ) + ":\n";
				Tab += "\t";
				Res += AddInsSim( Tab, ChipIndex, m );
				m = m.NextIns;
				continue;
			}
			if( m is IfElseIns ) {
				IfElseIns myi = (IfElseIns)m;
				Res += Tab + "if " + myi.MyEXP.Py_GetExpCode( Tab ) + ":\n";
				Tab += "\t";
				Res += AddInsSim( Tab, ChipIndex, m );
				m = m.NextIns;
				continue;
			}
			if( m is CondiEndIns ) {
				//if( ((CondiEndIns)m).CanLinked || SwitchFunction ) {
					Res += AddInsSim( Tab, ChipIndex, m );
					
					//这里极个别情况下会异常, 不知道为什么
					if( Tab.Length != 0 ) {
						Tab = Tab.Remove( 0, 1 );
					}
					else {
						n_Debug.Warning.BUG( "<ConvertOneMyInsForRealPython> 刚刚捕获了一个小小的软件异常, 应该不会影响您的后续操作." );
					}
					
					Res += Tab + "\n";
				//}
				//else {
				//	Res += Tab + "return;\n";
				//}
				
				m = m.NextIns;
				continue;
			}
			if( m is ElseIns ) {
				Tab = Tab.Remove( 0, 1 );
				Res += Tab + "else :\n";
				Tab += "\t";
				Res += AddInsSim( Tab, ChipIndex, m );
				m = m.NextIns;
				continue;
			}
			n_Debug.Warning.BUG( "<ConvertOneMyIns> 未知的指令: " + m.GetType() );
			return null;
		}
		return Res;
	}
	
	//转换一个流程图代码
	static string ConvertOneMyInsForPython( int ChipIndex, MyIns m )
	{
		string Res = null;
		string Tab = "\t";
		while( m != null ) {
			
			if( m is UserFunctionIns ) {
				UserFunctionIns myi = (UserFunctionIns)m;
				Res += myi.GetFuncDefinePython() + "\n";
				//Res += LocalVarDefine() + "\n";
				Tab = "\t";
				Res += AddInsSim( Tab, ChipIndex, m );
				m = m.NextIns;
				continue;
			}
			if( m is EventIns ) {
				EventIns myi = (EventIns)m;
				Res += "# 当事件：" + myi.EventEntry + " 成立则执行以下代码\n";
				Res += "def " + GetPinYin( myi.EventEntry ) + "():\n";
				//Res += LocalVarDefine() + "\n";
				Tab = "\t";
				Res += AddInsSim( Tab, ChipIndex, m );
				m = m.NextIns;
				continue;
			}
			if( m is FuncIns ) {
				FuncIns myi = (FuncIns)m;
				Res += AddInsSim( Tab, ChipIndex, m );
				Res += Tab + myi.MyEXP.GetExpCode() + "\n";
				m = m.NextIns;
				continue;
			}
			if( m is WaitIns ) {
				WaitIns myi = (WaitIns)m;
				Res += AddInsSim( Tab, ChipIndex, m );
				Res += Tab + "while !(" + myi.MyEXP.GetExpCode() + "): pass\n";
				m = m.NextIns;
				continue;
			}
			if( m is ForeverIns ) {
				ForeverIns myi = (ForeverIns)m;
				Res += Tab + "while True:\n";
				Tab += "\t";
				Res += AddInsSim( Tab, ChipIndex, m );
				//Res += Tab + "OS" + ChipIndex + ".Schedule()\n";
				m = m.NextIns;
				continue;
			}
			if( m is LoopIns ) {
				LoopIns myi = (LoopIns)m;
				Res += Tab + "for n in range(" + myi.MyEXP.GetExpCode() + "):\n";
				Tab += "\t";
				Res += AddInsSim( Tab, ChipIndex, m );
				m = m.NextIns;
				continue;
			}
			if( m is ExtIns ) {
				ExtIns myi = (ExtIns)m;
				Res += Tab + "foreach(" + myi.MyEXP.GetExpCode() + "):\n";
				Tab += "\t";
				Res += AddInsSim( Tab, ChipIndex, m );
				m = m.NextIns;
				continue;
			}
			if( m is WhileIns ) {
				WhileIns myi = (WhileIns)m;
				Res += Tab + "while " + myi.MyEXP.GetExpCode() + ":\n";
				Tab += "\t";
				Res += AddInsSim( Tab, ChipIndex, m );
				m = m.NextIns;
				continue;
			}
			if( m is IfElseIns ) {
				IfElseIns myi = (IfElseIns)m;
				Res += Tab + "if " + myi.MyEXP.GetExpCode() + ":\n";
				Tab += "\t";
				Res += AddInsSim( Tab, ChipIndex, m );
				m = m.NextIns;
				continue;
			}
			if( m is CondiEndIns ) {
				//if( ((CondiEndIns)m).CanLinked || SwitchFunction ) {
					Res += AddInsSim( Tab, ChipIndex, m );
					
					//这里极个别情况下会异常, 不知道为什么
					if( Tab.Length != 0 ) {
						Tab = Tab.Remove( 0, 1 );
					}
					else{
						n_Debug.Warning.BUG( "<ConvertOneMyInsForPython> 刚刚捕获了一个小小的软件异常, 应该不会影响您的后续操作." );
					}
					
					Res += Tab + "\n";
				//}
				//else {
				//	Res += Tab + "return;\n";
				//}
				
				m = m.NextIns;
				continue;
			}
			if( m is ElseIns ) {
				Tab = Tab.Remove( 0, 1 );
				Res += Tab + "else :\n";
				Tab += "\t";
				Res += AddInsSim( Tab, ChipIndex, m );
				m = m.NextIns;
				continue;
			}
			n_Debug.Warning.BUG( "<ConvertOneMyIns> 未知的指令: " + m.GetType() );
			return null;
		}
		return Res;
	}
	
	static string AddInsSim( string Tab, int ChipIndex, MyIns m )
	{
		if( cmd_simulate ) {
			string tt = Tab + "OS" + ChipIndex + ".REMO_ModuleWrite( " + (m.Index * 0x00010000 + 2) + ", 2 );\n" + Tab + "#.CInsIndex = " + m.Index + ";\n";
			if( TempOpenStep ) {
				tt += Tab + "#asm \"step\"\n";
			}
			return tt;
		}
		else {
			return null;
		}
	}
	
	//---------------------------------------------------------------
	//添加包代码
	static string AddBoxCode( int ChipIndex, MyObjectList myModuleList )
	{
		string BoardType = null;
		string DefaultChip = null;
		n_AVRdude.AVRdude.BoardType = null;
		
		DefaultChip = CPU_Type.VM;
		
		n_AVRdude.AVRdude.myAVRdudeSetForm.SetWarningImage( null );
		
		
		//所有驱动的MaxTick也需要处理 可以在OS中统一处理  改成动态读取并进行设置(仅限虚拟机字节码)
		//前10个静态变量可用于存放系统配置信息, 当某个外设的Tick==1或者取余后==0时, 需要根据情况进行补偿处理(如Timer Delayer)
		//其他普通外设直接在 OS 添加driver时 MaxTick / Tick 即可
		
		int MsPerTick = 0; //0表示默认tick
		FPGA_CPU = false;
		HardModule boardm = null;
		
		//转换链接配置信息
		foreach( MyObject mo in myModuleList ) {
			if( mo.ChipIndex != ChipIndex ) {
				continue;
			}
			if( mo is MyFileObject ) {
				MyFileObject m = (MyFileObject)mo;
				if( m is MyFileObject && ((MyFileObject)m).isControlModule ) {
					
					//这里临时设置调试串口序号
					if( m.MacroFilePath.IndexOf( "nucleo411re" ) != -1 ) {
						DebugUART_ID = "2";
					}
					else if( m.MacroFilePath.IndexOf( "STM32" ) != -1 ) {
						DebugUART_ID = "1";
					}
					else if( m.MacroFilePath.IndexOf( "epy-lite" ) != -1 ) {
						DebugUART_ID = "-1";
					}
					else {
						DebugUART_ID = "0";
					}
					
					//如果是W515型号, 临时将Tick设置为50
					if( m.MacroFilePath.IndexOf( "GD32\\W515T1" ) != -1 || m.MacroFilePath.IndexOf( "GD32\\W515T2" ) != -1 ) {
						MsPerTick = 50;
					}
					if( m.MacroFilePath.IndexOf( "FPGA\\TANG\\" ) != -1 ) {
						FPGA_CPU = true;
						DebugOpen = false;
						DebugOpenCode = false;
						n_ExportForm.ExportForm.isv = true;
						n_Decode.Decode.Zip = false;
					}
					//如果是PC运行模式, 改为50
					if( m.MacroFilePath.IndexOf( "vos_pc\\pc" ) != -1 ) {
						MsPerTick = 50;
					}
					
					DefaultChip = m.ImageName;
					if( DefaultChip.IndexOf( "-" ) != -1 ) {
						string[] cut = DefaultChip.Split( '-' );
						BoardType = cut[0];
						DefaultChip = cut[1];
						
						if( n_AVRdude.AVRdude.tempISP ) {
							n_AVRdude.AVRdude.BoardType = "ISP";
						}
						else {
							n_AVRdude.AVRdude.BoardType = BoardType;
						}
					}
					HardModule hm = (HardModule)m;
					boardm = hm;
					
					//这里可能暂时没有用到
					if( hm.ModuleImageList.EndsWith( ",w.png" ) ) {
						string[] c = hm.ModuleImageList.Split( ',' );
						string path = hm.BasePath + c[c.Length - 1];
						n_AVRdude.AVRdude.myAVRdudeSetForm.SetWarningImage( path );
					}
					else {
						if( G.CGPanel != null && G.CGPanel.SysMes != null && G.CGPanel.SysMes.StartsWith( ImagePanel.SYSHEAD_DownloadMes ) ) {
							string path = OS.SystemRoot + G.CGPanel.SysMes.Remove( 0, ImagePanel.SYSHEAD_DownloadMes.Length );
							n_AVRdude.AVRdude.myAVRdudeSetForm.SetWarningImage( path );
						}
					}
				}
			}
		}
		
		//这里用于电路版图下载, 动画类和其他类型点击下载按钮也会下载导致运行报错, 后续需要拦截
		if( BoardType == null ) {
			BoardType = "Nucleo";
			n_AVRdude.AVRdude.BoardType = "Nucleo";
		}
		
		//如果是下载模式且主板为EXPORT, 报错
		if( cmd_download && BoardType == "EXPORT" ) {
			SetErrorMes( boardm, "此主板: [" + boardm.Name + "] 目前仅支持导出源码模式, 暂不支持通过内置编译器下载程序." );
		}
		
		//如果是导出模式, 临时设置为 MEGA328
		if( cmd_export && DefaultChip == CPU_Type.VM ) {
			DefaultChip = CPU_Type.MEGA328;
		}
		
		if( DefaultChip == CPU_Type.FPGA ) {
			DefaultChip = CPU_Type.VM;
			G.isCV = true;
		}
		else {
			G.isCV = false;
		}
		SST.ModeV = G.isCV;
		SST.ModeC = !G.isCV;
		
		
		string cputype = DefaultChip;
		
		isVCC3_3 = false;
		cputype1 = DefaultChip;
		if( cputype1 != CPU_Type.VM ) {
			cputype1 = CPU_Type.AVR;
		}
		if( DefaultChip == "ControlPad" || cmd_simulate ) {
			DefaultChip = CPU_Type.VM;
		}
		string RUNNoteType = "//";
		string SIMNoteType = " ";
		string RunType = "run";
		if( cmd_simulate ) {
			RunType = "sim";
			RUNNoteType = " ";
			SIMNoteType = "//";
		}
		string code = "";
		string freq = "16000000";
		if( BoardType == "ISP" ) {
			freq = "16000000";
		}
		if( BoardType == "MINI8M" ) {
			freq = "8000000";
		}
		if( BoardType == "ISP1" ) {
			freq = "11059200";
		}
		if( BoardType == "ISP2" ) {
			freq = "8000000";
		}
		/*
		if( !SimulateMode && BoardType == "Curie" ) {
			MsPerTick = "50";
		}
		*/
		
		
		if( !cmd_simulate && BoardType == "Nucleo" ) {
			
			if( MsPerTick == 0 ) {
				MsPerTick = myModuleList.GPanel.VEX_Tick;
			}
			
			isVCC3_3 = true;
		}
		
		if( MsPerTick == 0 ) {
			MsPerTick = 1;
		}
		if( cmd_simulate ) {
			MsPerTick = SIM_Tick;
		}
		//导出模式设置为1
		if( cmd_export ) {
			MsPerTick = 1;
		}
		
		InitCode += "\t#.SYS_Tick = " + MsPerTick + ";\n";
		//如果是非图形化界面 则保持这个数值, 并忽略, 图形界面则会先设置为当前指令, 再使用其值, 因此初始值不影响
		//此变量仅用于仿真指令跟踪显示
		InitCode += "\t#.CInsIndex = 0x2000;\n";
		
		string t_chip = DefaultChip;
		//这里, 仿真也作为下载模式, 比如AD公共驱动等简化封装, 检测到default就丢给之前的驱动包处理
		if( !cmd_export ) {
			code += "#define " + GetSysDefine( "tool" ) + " default\n";
		}
		else {
			code += "#define " + GetSysDefine( "tool" ) + " export\n";
			t_chip = "export";
		}
		code += "#define " + GetSysDefine( "run" ) + " " + RunType + "\n";
		code += "#define " + GetSysDefine( "board" ) + " " + BoardType + "\n";
		code += "#define " + GetSysDefine( "freq" ) + " " + freq + "\n";
		code += "#define " + GetSysDefine( "chip" ) + " " + DefaultChip + "\n";
		code += "#define " + GetSysDefine( "chip1" ) + " " + t_chip + "\n";
		code += "#define " + GetSysDefine( "cpu" ) + " " + cputype + "\n";
		code += "#define " + GetSysDefine( "cpu1" ) + " " + cputype1 + "\n";
		code += "#define " + GetSysDefine( "RUN_NOTE" ) + " " + RUNNoteType + "\n";
		code += "#define " + GetSysDefine( "SIM_NOTE" ) + " " + SIMNoteType + "\n";
		
		if( FPGA_CPU ) {
			code += "#define " + GetSysDefine( "cpu2" ) + " FPGA\n";
		}
		else {
			code += "#define " + GetSysDefine( "cpu2" ) + " VOS\n";
		}
		//code += "#define " + GetSysDefine( "tick" ) + " " + MsPerTick + "\n";
		code += "#define " + GetSysDefine( "voffset" ) + " " + myModuleList.GPanel.voffset + "\n";
		code += "#setcpu " + DefaultChip + "\n";
		code += "#." + GetSysDefine( "chip" ) + " = #.COM_MCU;\n";
		
		//SMOS = false;
		if( DefaultChip.StartsWith( "MCS" ) ) {
			//SMOS = true;
		}
		//根据芯片类型设置OS类型
		if( DefaultChip == "MEGA8" ) {
			//os_type = OS_Type.DRIVER;
		}
		else {
			//os_type = OS_Type.DRIVER_TASK;
		}
		
		if( ChipIndex == 0 ) {
			return code;
		}
		code += "#resetcompiler\n";
		return code;
	}
	
	//添加包代码, 注意这个只会添加一次
	static string AddPreDefine()
	{
		return "#include <system" + OS.PATH_S + "predefine.txt>\n";
	}
	
	//转换配置代码
	static string ConvertCode( int ChipIndex, MyObjectList myModuleList, ref string ClientChannelName )
	{
		string GUItext = null;
		string VarInit = null;
		
		CommonRefLoad = "";
		CommonRefFunc1 = "";
		CommonRefFunc2 = "";
		UsedCommonRefCode = ";";
		SYS_ServoID = 0;
		HaseServo = false;
		ProtocalNumber = 0;
		
		UsedMasterPort = ",";
		
		myModuleList.myResourceList.Clear();
		
		//GoNameList = new string[myModuleList.MLength];
		TotalGoNumber = 0;
		
		//转换链接配置信息
		foreach( MyObject mo in myModuleList ) {
			if( mo.ChipIndex != ChipIndex ) {
				continue;
			}
			if( mo is HardModule ) {
				try{
				GUItext += ConvertOneModule( myModuleList, mo.Index, ref VarInit );
				}catch(Exception e) {
					n_Debug.Warning.BUG( "<ConvertCode> 模块转换异常: " + mo.Name + ", " + myModuleList.ModuleList[mo.Index].Name + " " + e.ToString() );
				}
			}
			if( mo is UIModule ) {
				GUItext += ConvertOneControl( myModuleList, mo.Index, ref VarInit );
			}
			if( mo is EventIns ) {
				EventIns ei = (EventIns)mo;
				ei.GoIndex = TotalGoNumber;
				//GoNameList[TotalGoNumber] = ei.EventEntry;
				TotalGoNumber++;
			}
		}
		
		if( DebugOpen ) {
			GUItext += "#define UART_NUMBER " + DebugUART_ID + "\n";
			GUItext += "SYS_Debug =\n";
			GUItext += "#include <system\\debug_v1\\Pack.txt>\n";
			GUItext += "SYS_Debug.SYS_FuncList = SYS_FuncList;\n";
			GUItext += GetAPI( myModuleList, true );
		}
		
		//添加公共引用代码
		if( CommonRefLoad != "" ) {
			if( HaseServo ) {
				CommonRefFunc1 += "}\n";
				CommonRefFunc2 += "}\n";
				GUItext += CommonRefLoad + CommonRefFunc1 + CommonRefFunc2;
			}
			else {
				GUItext += CommonRefLoad;
			}
		}
		
		//转换客户端GUI代码
		string ClientCode = null;
		string ClientGUICode = ConvertClientGUI( ChipIndex, myModuleList, ref ClientCode, ref ClientChannelName );
		ClientGUICode = "";
		
		GUItext += ConvertOSCode( ChipIndex, myModuleList, VarInit, ClientGUICode, ClientCode );
		
		return GUItext;
	}
	
	//转换用户代码框
	static string ConvertUserCode( int ChipIndex, MyObjectList myModuleList )
	{
		string r = null;

		//转换链接配置信息
		foreach( MyObject mo in myModuleList ) {
			if( mo.ChipIndex != ChipIndex ) {
				continue;
			}
			if( mo is GNote ) {
				GNote go = (GNote)mo;
				if( go.isCode && go.CodeType == n_GNote.GNote.T_CX ) {
					string ucl = null;
					string cx_code = go.GetCxCode( ref ucl );
					if( ucl != null ) {
						n_SST.SST.UserCL += ucl + "\n";
					}
					r += n_GroupList.Group.GetFullName( go.GroupMes, cx_code ) + "\n";
				}
				if( go.isCode && go.CodeType == n_GNote.GNote.T_CD ) {
					
					if( G.LIDEBox == null ) {
						G.LIDEBox = new n_LIDEForm.LIDEForm();
					}
					string err = G.LIDEBox.Compile( go.Value );
					if( err != null ) {
						n_Debug.Warning.AddClashMessage( "协处理器程序编译出错, 请检查程序:\n" + err );
						r += "[]#.code uint8 CodeList = {0};\n";
					}
					else {
						r += n_GroupList.Group.GetFullName( go.GroupMes, n_LCC.LCC.HexResult ) + "\n";
					}
				}
			}
		}
		return r;
	}
	
	//转换用户的python代码框
	public static string ConvertUserPythonCode( int ChipIndex, MyObjectList myModuleList )
	{
		string r = null;
		
		//转换链接配置信息
		foreach( MyObject mo in myModuleList ) {
			if( mo.ChipIndex != ChipIndex ) {
				continue;
			}
			if( mo is GNote ) {
				GNote go = (GNote)mo;
				if( go.isCode && go.CodeType == n_GNote.GNote.T_Python ) {
					//r += n_GroupList.Group.GetFullName( go.GroupMes, go.Value ) + "\n";
					r += go.Value + "\n";
				}
			}
		}
		return r;
	}
	
	//转换单个组件的配置代码
	static string ConvertOneModule(  MyObjectList ml, int Index, ref string VarInit )
	{
		HardModule m = (HardModule)ml.ModuleList[ Index ];
		ResourceList myResourceList = ml.myResourceList;
		string GUItext = null;
		string FullPortName = null;
		
		//虚拟端口配置代码,如果为null表示没有虚拟端口配置
		string V_ConfigCode = null;
		
		//用于直接并行端口配置
		string D_ConfigPortBaseName = null;
		string D_TargetModuleName = null;
		string D_TargetPortBaseName = null;
		bool SameNumber = true;
		int NumberSum = 0;
		bool SameModuleAndSamePort = true;
		
		string UartDefine = null;
		
		//遍历端口列表处理每个端口的配置代码
		for( int n = 0; n < m.PORTList.Length; ++n ) {
			
			//如果是空端口或VCC和GND,跳过
			if( m.PORTList[ n ].FuncName == PortFuncName.GND ||
			    m.PORTList[ n ].FuncName == PortFuncName.VCC ||
				m.PORTList[ n ].FuncName == PortFuncName.VCC3_3V ||
				m.PORTList[ n ].FuncName == PortFuncName.VCC6t9V ||
				m.PORTList[ n ].FuncName == PortFuncName.VCCA ||
				m.PORTList[ n ].FuncName == PortFuncName.UNUSED ||
				m.PORTList[ n ].FuncName == PortFuncName.RESET ||
				m.PORTList[ n ].FuncName == PortFuncName.AREF ||
				m.PORTList[ n ].FuncName == PortFuncName.PULL ) {
				continue;
			}
			if( m.PORTList[ n ].ClientType == PortClientType.MASTER ) {
				continue;
			}
			if( m.PORTList[ n ].TargetPort == null && m.PORTList[ n ].TargetModule == null ) {
				
				if( m.ImageName != "mbb" &&
				    m.PORTList[ n ].FuncName != PortFuncName.LINE &&
				    m.PORTList[ n ].FuncName != PortFuncName.PULL &&
				    m.PORTList[ n ].FuncName != PortFuncName.C_Analog &&
				    m.PORTList[ n ].FuncName != PortFuncName.C_Digital &&
					m.PORTList[ n ].FuncName != PortFuncName.UNUSED ) {
					
					SetErrorMes( m, "模块 [" + m.Name + "] 的这个端口 [" + m.PORTList[ n ].Name + "] 需要连接到控制板的端口上." );
				}
				
				continue;
			}
			string PortName = m.PORTList[ n ].Name;
			
			//处理接口链接
			if( m.PORTList[ n ].FuncName == PortFuncName.INTERFACE ) {
				GUItext += m.Name + "." + PortName + " = " + m.PORTList[ n ].TargetModule.Name + ";\n";
				continue;
			}
			string Target = m.PORTList[ n ].TargetPort.Owner.Name + "." + m.PORTList[ n ].TargetPort.Name;
			
			
			//记录主端口使用情况, 是否有重复
			HardModule main = m.PORTList[ n ].TargetPort.Owner;
			if( main.isControlModule ) {
				
				string AddName = m.PORTList[ n ].TargetPort.Name;
				
				if( !IgnoreError && UsedMasterPort.IndexOf( "," + AddName + "," ) != -1 ) {
					SetErrorMes( m, "模块 [" + m.Name + "] 的这个端口 [" + m.PORTList[ n ].Name + "] 所连接到的主板端口 [" + AddName + "] 已经被占用, 请更换为其他空闲的端口." );
				}
				else {
					UsedMasterPort += AddName + ",";
				}
			}
			//判断是否为字节端口链接
			bool temp = false;
			if( temp && PortName.IndexOf( "_" ) != -1 ) {
				
				//目标端口的参数
				string TargetModuleName = m.PORTList[ n ].TargetPort.Owner.Name;
				string TargetPortName = m.PORTList[ n ].TargetPort.Name;
				int TargetUnderLineIndex = TargetPortName.LastIndexOf( "_" );
				string TargetPortNumber = TargetPortName.Remove( 0, TargetUnderLineIndex + 1 );
				string TargetPortBaseName = TargetPortName.Remove( TargetUnderLineIndex );
				//检查目标组件名是否一致
				if( D_TargetModuleName == null ) {
					D_TargetModuleName = TargetModuleName;
				}
				else if( D_TargetModuleName != TargetModuleName ) {
					SameModuleAndSamePort = false;
				}
				//检查目标端口名是否一致
				if( D_TargetPortBaseName == null ) {
					D_TargetPortBaseName = TargetPortBaseName;
				}
				else if( D_TargetPortBaseName != TargetPortBaseName ) {
					SameModuleAndSamePort = false;
				}
				//链接端口的参数
				int UnderLineIndex = PortName.LastIndexOf( "_" );
				string PortNumber = PortName.Remove( 0, UnderLineIndex + 1 );
				string PortBaseName = PortName.Remove( UnderLineIndex );
				D_ConfigPortBaseName = PortBaseName;
				//检查端口序号是否一致
				if( PortNumber != TargetPortNumber ) {
					SameNumber = false;
				}
				else {
					NumberSum += 1 + int.Parse( PortNumber );
				}
				string ConfigUnitName = "F_" + m.Name + "_" + PortBaseName;
				if( FullPortName == null ) {
					FullPortName = PortBaseName;
					V_ConfigCode += ConfigUnitName + "_DIR =\n";
					V_ConfigCode += Config.LOAD + " <system" + OS.PATH_S + "vport.txt>\n";
					V_ConfigCode += m.Name + ".io." + PortBaseName + "_DIR = " + ConfigUnitName + "_DIR DATA;\n";
					V_ConfigCode += ConfigUnitName + "_IN =\n";
					V_ConfigCode += Config.LOAD + " <system" + OS.PATH_S + "vport.txt>\n";
					V_ConfigCode += m.Name + ".io." + PortBaseName + "_IN = " + ConfigUnitName + "_IN DATA;\n";
					V_ConfigCode += ConfigUnitName + "_OUT =\n";
					V_ConfigCode += Config.LOAD + " <system" + OS.PATH_S + "vport.txt>\n";
					V_ConfigCode += m.Name + ".io." + PortBaseName + "_OUT = " + ConfigUnitName + "_OUT DATA;\n";
				}
				V_ConfigCode += ConfigUnitName + "_IN." + PortName + " = " + Target + "_IN; ";
				V_ConfigCode += ConfigUnitName + "_OUT." + PortName + " = " + Target + "_OUT; ";
				V_ConfigCode += ConfigUnitName + "_DIR." + PortName + " = " + Target + "_DIR;\n";
			}
			else {
				string tr_name = null;
				int Rxi = m.PORTList[ n ].TargetPort.FuncName.IndexOf( PortFuncName.RxDn );
				if( Rxi != -1 ) {
					tr_name = m.PORTList[ n ].TargetPort.FuncName.Remove( 0, Rxi + 4 );
					Rxi = m.PORTList[ n ].FuncName.IndexOf( PortFuncName.RxD );
					if( Rxi == -1 ) {
						tr_name = null;
					}
				}
				int Txi = m.PORTList[ n ].TargetPort.FuncName.IndexOf( PortFuncName.TxDn );
				if( Txi != -1 ) {
					tr_name = m.PORTList[ n ].TargetPort.FuncName.Remove( 0, Txi + 4 );
					Txi = m.PORTList[ n ].FuncName.IndexOf( PortFuncName.TxD );
					if( Txi == -1 ) {
						tr_name = null;
					}
				}
				int Uxi = m.PORTList[ n ].TargetPort.FuncName.IndexOf( PortFuncName.UART );
				if( Uxi != -1 ) {
					tr_name = m.PORTList[ n ].TargetPort.Name.Remove( 0, 4 );
					if( tr_name == "" ) {
						tr_name = "0";
					}
				}
				if( tr_name != null ) {
					string tempUartDefine = "";
					char c = tr_name[0];
					switch( c ) {
						case '0':	tempUartDefine = "0"; break;
						case '1':	tempUartDefine = "1"; break;
						case '2':	tempUartDefine = "2"; break;
						case '3':	tempUartDefine = "3"; break;
						case '4':	tempUartDefine = "4"; break;
						case '5':	tempUartDefine = "5"; break;
						case '6':	tempUartDefine = "6"; break;
						case '7':	tempUartDefine = "7"; break;
						case '8':	tempUartDefine = "8"; break;
						case '9':	tempUartDefine = "9"; break;
						default:	tempUartDefine = "0"; break;
					}
					if( UartDefine == null ) {
						UartDefine = tempUartDefine;
						GUItext += "#define UART_NUMBER " +UartDefine + "\n";
					}
					else {
						if( UartDefine != tempUartDefine ) {
							SetErrorMes( m, "模块 [" + m.Name + "] 的RX和TX引脚需要连接到主板同序号(同组)的串口引脚上, 不允许分开, 例如可连接到主板的 RX0和TX0, RX1和TX1... 但不允许分别连接到RX0和TX1" );
						}
					}
					//这里应该根据不同的主板进行判断, 获取真正的下载串口序号, 不一定是0
					if( cmd_download && !m.PORTList[ n ].CanChange && Uxi == -1 && tempUartDefine == "0" ) {
						if( !n_AVRdude.AVRdude.myAVRdudeSetForm.ExistWarning() ) {
							n_Debug.Warning.WarningMessage = "请注意, 此模块：" + m.Name +
								"占用了主板的串口针脚, 由于主板下载程序也是通过串口通信, 两者会冲突. \n建议先拔掉此模块和实物主板的杜邦线, 等下载完成后, 再把它连接到实物主控板上.(程序界面的连线不需要断开)\n";
						}
					}
					if( !m.PORTList[ n ].CanChange && tempUartDefine == "0" ) {
						if( DebugOpen ) {
							DebugOpen = false;
							DebugOpenCode = false;
						}
					}
				}
				//判断是否为总线型端口
				if( m.PORTList[ n ].TargetPort.FuncName.IndexOf( PortFuncName.ZX ) == -1 ) {
					GUItext += m.Name + ".driver." + PortName + " = " + Target + ";\n";
				}
				else {
					string[] NameList = PortName.Split( ',' );
					for( int i = 0; i < NameList.Length; ++i ) {
						GUItext += m.Name + ".driver." + NameList[i] + " = " + Target + i + ";\n";
					}
				}
			}
		}
		//遍历总线列表处理每个端口的配置代码
		for( int n = 0; n < m.ZWireList.Length; ++n ) {
			if( m.ZWireList[ n ].ClientType == PortClientType.MASTER ) {
				continue;
			}
			if( m.ZWireList[ n ].FuncName == ZWireFuncName.POWER ) {
				continue;
			}
			if( m.ZWireList[ n ].TargetPort == null && m.ZWireList[ n ].TargetModule == null ) {
				SetErrorMes( m, "模块 [" + m.Name + "] 的这个针脚 [" + m.ZWireList[ n ].Name + "] 需要连接到控制板的针脚上." );
				continue;
			}
			if( m.ZWireList[ n ].FuncName == ZWireFuncName.POWER ) {
				continue;
			}
			string PortName = m.ZWireList[ n ].Name;
			string Target = m.ZWireList[ n ].TargetPort.Owner.Name + "." + m.ZWireList[ n ].TargetPort.Name;
			GUItext += m.Name + ".driver." + PortName + " = " + Target + ";\n";
		}
		if( V_ConfigCode != null ) {
			if( SameNumber && NumberSum == 36 && SameModuleAndSamePort ) {
				GUItext +=	m.Name + ".io." + D_ConfigPortBaseName + "_DIR = " +
							D_TargetModuleName + "." + D_TargetPortBaseName + "_DIR; ";
				GUItext +=	m.Name + ".io." + D_ConfigPortBaseName + "_IN = " +
							D_TargetModuleName + "." + D_TargetPortBaseName + "_IN; ";
				GUItext +=	m.Name + ".io." + D_ConfigPortBaseName + "_OUT = " +
							D_TargetModuleName + "." + D_TargetPortBaseName + "_OUT;\n";
			}
			else {
				n_Debug.Warning.WarningMessage = ( "这个端口可以按照顺序连接,程序会执行地更快:\n" +
					m.Name + "." + D_ConfigPortBaseName + " = " +
					D_TargetModuleName + "." + D_TargetPortBaseName );
				GUItext += V_ConfigCode + "\n";
			}
		}
		//配置组件参数
		for( int i = 0; i < m.VarList.Length; ++i ) {
			if( m.VarList[ i ][ 0 ] != "" ) {
				string[] TypeMes = m.VarList[ i ][ 1 ].Split( '_' );
				if( TypeMes[ 0 ] + "_" == InterfaceType.LinkConst_ ) {
					GUItext += m.Name + "." + m.VarList[ i ][ m.GetLC() ] + " = " +
						m.Name + "_" + m.VarList[ i ][ m.GetLC() ] + ";\n";
					
					if( TypeMes.Length >= 4 && TypeMes[3] == "秒" ) {
						string Value = null;
						
						try {
						Value = ((int)(float.Parse( m.VarList[ i ][ 0 ] ) * n_ConstString.ConstString.FixScale)).ToString();
						}
						catch {
							n_Debug.Warning.AddClashMessage( "C: 模块[" + m.Name + "]的参数[" +  m.VarList[ i ][ m.GetLC() ] + "]数据格式不正确: " + m.VarList[ i ][ 0 ] );
						}
						GUItext += "const " + TypeMes[ 1 ] + " " + m.Name + "_" + m.VarList[ i ][ m.GetLC() ] +
							" = " + Value + ";\n";
					}
					else if( TypeMes[ 1 ] == VarType.BaseType.fix && m.VarList[ i ][ 0 ].IndexOf( "." ) == -1 ) {
						string Value = m.VarList[ i ][ 0 ] + ".0";
						GUItext += "const " + TypeMes[ 1 ] + " " + m.Name + "_" + m.VarList[ i ][ m.GetLC() ] +
							" = " + Value + ";\n";
					}
					else {
						GUItext += "const " + TypeMes[ 1 ] + " " + m.Name + "_" + m.VarList[ i ][ m.GetLC() ] +
							" = " + m.VarList[ i ][ 0 ] + ";\n";
					}
				}
				else if( TypeMes[ 0 ] + "_" == InterfaceType.LinkVar_ ) {
					if( TypeMes.Length >= 4 && TypeMes[3] == "秒" ) {
						string Value = null;
						try {
						Value = ((int)(float.Parse( m.VarList[ i ][ 0 ] ) * n_ConstString.ConstString.FixScale)).ToString();
						}
						catch {
							n_Debug.Warning.AddClashMessage( "V: 模块[" + m.Name + "]的参数[" +  m.VarList[ i ][ m.GetLC() ] + "]数据格式不正确: " + m.VarList[ i ][ 0 ] );
						}
						VarInit += "\t" + m.Name + "." + m.VarList[ i ][ m.GetLC() ] + " = " +Value + ";\n";
					}
					else {
						VarInit += "\t" + m.Name + "." + m.VarList[ i ][ m.GetLC() ] + " = " +
							m.VarList[ i ][ 0 ] + ";\n";
					}
				}
				else {
					n_Debug.Warning.BUG( "未知的配置参数类型：" + TypeMes[ 0 ] );
				}
			}
			else {
				//G.FlashBox.Run( "这个组件需要您设置参数: " + m.Name );
				n_Debug.Warning.AddClashMessage( "模块 [" + m.Name + "] 的参数 [" +  m.VarList[ i ][ m.GetLC() ] + "] 需要进行设置" );
			}
		}
		//配置组件接口
		for( int i = 0; i < m.LinkInterfaceList.Length; ++i ) {
			string TargetName = m.LinkInterfaceList[ i ][ 0 ];
			if( TargetName != "" ) {
				
				
				//注意这里需要处理下语言索引, 否则直接用内置名称(目前接口的内置名称和外部名称相同,所以暂时没问题)
				//TargetName需要解析成实际的语言接口名称
				
				
				GUItext += m.Name + "." + m.LinkInterfaceList[ i ][ m.GetLC() ] + " = " + TargetName + ";\n";
			}
			else {
				n_Debug.Warning.AddClashMessage( "模块 [" + m.Name + "] 需要连接到目标设备才能运行" );
			}
		}
		//提取组件占用的资源
		if( !(m.ImageName == "servo" || m.ResourceList == null || m.ResourceList.Length == 1 && m.ResourceList[ 0 ] == "") ) {
			for( int i = 0; i < m.ResourceList.Length; ++i ) {
				string ResName = m.ResourceList[ i ];
				int ResIndex = myResourceList.GetIndex( ResName );
				if( ResIndex == -1 ) {
					n_Debug.Warning.BUG( "未知的资源名称: " + ResName );
					continue;
				}
				//判断是否有开关中断相互冲突的模块 主要是DS18B20类 和 红外接收类
				if( ResName == ResourceList.EA ) {
					if( cmd_download && myResourceList.ExistResUser != null ) {
						n_Debug.Warning.WarningMessage += "[" + m.Name + "] 读取传感器数值的时候会反复关闭中断, 可能会影响 [" + myResourceList.ExistResUser + "] 使用的资源 <" + myResourceList.ExistRes  + ">, \n所以您应该降低传感器参数读取频率, 例如十秒钟读取一次. \n";
					}
				}
				else {
					if( cmd_download && myResourceList.ExistEAUser != null && ResName != ResourceList.TIMER0 && m.ImageName == "NOT_EA" ) {
						n_Debug.Warning.WarningMessage += "[" + myResourceList.ExistEAUser + "] 读取传感器数值的时候会反复关闭中断, 可能会影响 [" + m.Name + "] 使用的资源 <" + ResName  + ">, \n所以您应该降低传感器参数读取频率, 例如十秒钟读取一次. \n";
					}
					string UserName = myResourceList.GetUser( ResIndex );
					if( UserName != null && ResName != "USART" ) {
						n_Debug.Warning.AddClashMessage( "[" + m.Name + "] 和 [" + UserName + "] 不能同时使用, 因为它们需要相同的资源 <" + ResName  + ">" );
						continue;
					}
				}
				//判断是否有重复的资源使用
				if( ResName != ResourceList.EA ) {
					string UserName = myResourceList.GetUser( ResIndex );
					if( UserName != null && ResName != "USART" ) {
						n_Debug.Warning.AddClashMessage( "[" + m.Name + "] 和 [" + UserName + "] 不能同时使用, 因为它们需要相同的资源 <" + ResName  + ">" );
						continue;
					}
				}
				myResourceList.SetUser( ResIndex, m.Name, m.ImageName );
			}
		}
		if( cputype1 == CPU_Type.VM ) {
			for( int i = 0; i < m.ResourceList.Length; ++i ) {
				string ResName = m.ResourceList[ i ];
				if( ResName == ResourceList.AVR ) {
					n_Debug.Warning.AddClashMessage( "[" + m.Name + "] 不能用于32位主板, 请删除此模块, 并在模块列表中找到附近的同类模块 (如超声波/扬声器等都有用于arduino和32位主板的对应版本)" );
					continue;
				}
			}
		}
		
		//判断是否需要设置组件的存储位置
		if( m.ExtendValue != "" ) {
			if( m.ImageName == SPMoudleName.SYS_Keyboard ) {
				//...
			}
			else if( m.ImageName == SPMoudleName.SYS_PinInput ) {
				if( cmd_download ) {
					GUItext += m.Name + ".driver.D0_DIR = #.SYS_CTCOM.IO." + m.ExtendValue + ".D0_DIR;\n";
					GUItext += m.Name + ".driver.D0_PUL = #.SYS_CTCOM.IO." + m.ExtendValue + ".D0_PUL;\n";
					GUItext += m.Name + ".driver.D0_IN = #.SYS_CTCOM.IO." + m.ExtendValue + ".D0_IN;\n";
				}
			}
			else if( m.ImageName == SPMoudleName.SYS_PinOutput ) {
				if( cmd_download ) {
					GUItext += m.Name + ".driver.D0_DIR = #.SYS_CTCOM.IO." + m.ExtendValue + ".D0_DIR;\n";
					GUItext += m.Name + ".driver.D0_OUT = #.SYS_CTCOM.IO." + m.ExtendValue + ".D0_OUT;\n";
				}
			}
			else {
				GUItext += m.Name + ".driver.Mem = " + m.ExtendValue + ";\n";
			}
		}
		else {
			if(  (m.ImageName == SPMoudleName.SYS_PinInput || m.ImageName == SPMoudleName.SYS_PinOutput) && cmd_download ) {
				SetErrorMes( m, "模块 [" + m.Name + "] 需要绑定目标芯片针脚, 请点击设置" );
			}
		}
		//判断是否需要设置组件的存储位置
		if( m.ExtendValue1 != "" ) {
			VarInit += "\t" + m.Name + ".设置时间为_( " + m.ExtendValue1 + " );\n";
		}
		//驱动库进行配置
		if( m.ConfigList != null ) {
			GUItext += n_HModCommon.HModCommon.GetDefine( m.ConfigList );
		}
		
		//导入组件驱动代码
		
		//如果代码编程模式下始终设置为英文语言
		string mod_lang = m.Language;
		if( G.isCruxEXE ) {
			mod_lang = "c";
		}
		GUItext += "#define " + GetSysDefine( "language" ) + " " + mod_lang + "\n";
		GUItext += "public " + m.Name + " =\n";
		if( m.isUserPathMod ) {
			GUItext += Config.LOAD + " \"" + m.IncPackFilePath.Remove( 0, m.IncPackFilePath.IndexOf( ' ' ) + 1 ) + "\"\n";
		}
		else {
			GUItext += Config.LOAD + " <" + m.IncPackFilePath.Remove( 0, m.IncPackFilePath.IndexOf( ' ' ) + 1 ) + ">\n";
		}
		
		GUItext += "const uint16 " + m.Name + "_simsurport_ID = " + Index + ";\n";
		GUItext += m.Name + ".driver.ID = " + m.Name + "_simsurport_ID;\n";
		
		//判断是否设置电路仿真元件类ID
		if( m.isCircuit ) {
			GUItext += "const int16 " + m.Name + "_circuit_ID = " + m.CC_ID + ";\n";
			GUItext += m.Name + ".driver.CC_ID = " + m.Name + "_circuit_ID;\n";
		}
		if( cmd_simulate ) {
			
			if( m.SimulateType == SimulateType.SurportLib ) {
				//GUItext += "const uint16 " + m.Name + "_simsurport_ID = " + Index + ";\n";
				GUItext += m.Name + "_simsurport.ID = " + m.Name + "_simsurport_ID;\n";
				GUItext += m.Name + "_simsurport.driver = " + m.Name + ".driver;\n";
				GUItext += "public " + m.Name + "_simsurport =\n";
				string PackName = m.IncPackFilePath.Remove( 0, m.IncPackFilePath.LastIndexOf( '\\' ) + 1 );
				PackName = PackName.Remove( PackName.Length - 2 );
				GUItext += Config.LOAD + " <" + m.IncBasePath.Remove( 0, m.IncBasePath.IndexOf( ' ' ) + 1 ) + "driver\\" + PackName + "_surport.txt>\n";
			}
			else if( m.SimulateType == SimulateType.UseLib ) {
				//这里就不需要处理了
			}
			else {
				//这里需要给当前模块重新生成一个完整的仿真代码包
				//... 很困难...
				
				SetErrorMes( m, "很抱歉通知您, 这个模块暂时还不支持仿真: " + m.Name );
			}
		}
		
		//临时判断是否为导入动态字符串
		if( m != null && m.IncPackFilePath != null && m.IncPackFilePath.EndsWith( "software\\string_d\\Pack.B" ) ) {
			LoadDoStringLib = true;
			if( !HasShowDoStringMes ) {
				HasShowDoStringMes = true;
				n_Debug.Warning.WarningMessage = "使用 [字符串] 模块会自动开启动态内存分配, 默认分配500字节空间. 如提示系统资源空间不够或者要改变动态内存大小, 可点击界面右侧高级设置按钮进入设置";
			}
		}
		
		//判断是否需要导入公共引用文件
		if( !cmd_simulate && m.CommonRefFile != null ) {
			
			if( UsedCommonRefCode.IndexOf( m.CommonRefFile ) == -1 ) {
				
				if( cputype1 == "AVR" ) {
					CommonRefLoad += Config.LOAD + " <" + m.CommonRefFile + ">\n";
				}
				
				UsedCommonRefCode += m.CommonRefFile + ";";
				
				if( m.CommonRefFile == @"system\CommonRef\Servo.txt" && cputype1 == "AVR" ) {
					
					HaseServo = true;
					
					//此处需要注意, 自动加入了公共舵机驱动的初始化
					VarInit += "\tSYS_Servo.OS_init();\n";
					
					CommonRefFunc1 += "#param FUNCTION_USED\n";
					CommonRefFunc1 += "void SYS_Servo_Clear()\n";
					CommonRefFunc1 += "{\n";
					
					CommonRefFunc2 += "#param FUNCTION_USED\n";
					CommonRefFunc2 += "void SYS_Servo_SetPin()\n";
					CommonRefFunc2 += "{\n";
				}
			}
			if( m.CommonRefFile == @"system\CommonRef\Servo.txt" ) {
				
				if( cputype1 == "AVR" ) {
					//此处需要注意, 自动加入了舵机的两个函数, 以后增加其他公共引用模块需要进行判断(现在是直接加入)
					CommonRefFunc1 += "\t#." + m.Name + ".driver.D.D0_OUT = 0;\n";
					CommonRefFunc2 += "\t#asm \"sbrc r31," + SYS_ServoID + "\"\n";
					CommonRefFunc2 += "\t#." + m.Name + ".driver.D.D0_OUT = 1;\n";
					CommonRefFunc2 += "\t#asm \"sbrs r31," + SYS_ServoID + "\"\n";
					CommonRefFunc2 += "\t#." + m.Name + ".driver.D.D0_OUT = 0;\n";
				}
				GUItext += m.Name + ".driver.SERVO_ID = SYS_ServoID" + SYS_ServoID + ";\n";
				GUItext += "const uint8 SYS_ServoID" + SYS_ServoID + " = " + SYS_ServoID + ";\n";
				SYS_ServoID++;
			}
		}
		return GUItext;
	}
	
	//转换单个控件的配置代码
	static string ConvertOneControl(  MyObjectList ml, int Index, ref string VarInit )
	{
		UIModule m = (UIModule)ml.ModuleList[ Index ];
		ResourceList myResourceList = ml.myResourceList;
		string GUItext = null;
		
		//配置组件参数
		for( int i = 0; i < m.VarList.Length; ++i ) {
			if( m.VarList[ i ][ 0 ] != "" ) {
				string[] TypeMes = m.VarList[ i ][ 1 ].Split( '_' );
				if( TypeMes[ 0 ] + "_" == InterfaceType.LinkConst_ ) {
					
					GUItext += m.Name + "." + m.VarList[ i ][ m.GetLC() ] + " = " +
						m.Name + "_" + m.VarList[ i ][ m.GetLC() ] + ";\n";
					
					if( TypeMes.Length >= 4 && TypeMes[3] == "秒" ) {
						string Value = ((int)(float.Parse( m.VarList[ i ][ 0 ] ) * n_ConstString.ConstString.FixScale)).ToString();
						GUItext += "const " + TypeMes[ 1 ] + " " + m.Name + "_" + m.VarList[ i ][ m.GetLC() ] +
							" = " + Value + ";\n";
					}
					else if( TypeMes[ 1 ] == VarType.BaseType.fix && m.VarList[ i ][ 0 ].IndexOf( "." ) == -1 ) {
						string Value = m.VarList[ i ][ 0 ] + ".0";
						GUItext += "const aaa " + TypeMes[ 1 ] + " " + m.Name + "_" + m.VarList[ i ][ m.GetLC() ] +
							" = " + Value + ";\n";
					}
					else {
						GUItext += "const bbb " + TypeMes[ 1 ] + " " + m.Name + "_" + m.VarList[ i ][ m.GetLC() ] +
							" = " + m.VarList[ i ][ 0 ] + ";\n";
					}
				}
				else if( TypeMes[ 0 ] + "_" == InterfaceType.LinkVar_ ) {
					VarInit += "\t" + m.Name + "." + m.VarList[ i ][ m.GetLC() ] + " = " +
						m.VarList[ i ][ 0 ] + ";\n";
				}
				else {
					n_Debug.Warning.BUG( "未知的配置参数类型：" + TypeMes[ 0 ] );
				}
			}
			else {
				n_Debug.Warning.AddClashMessage( "C: 模块 [" + m.Name + "] 的参数 [" +  m.VarList[ i ][ m.GetLC() ] + "] 需要进行设置" );
			}
		}
		//提取组件占用的资源
		if( !(m.ResourceList == null || m.ResourceList.Length == 1 && m.ResourceList[ 0 ] == "") ) {
			for( int i = 0; i < m.ResourceList.Length; ++i ) {
				string ResName = m.ResourceList[ i ];
				int ResIndex = myResourceList.GetIndex( ResName );
				if( ResIndex == -1 ) {
					n_Debug.Warning.BUG( "未知的资源名称: " + ResName );
					continue;
				}
				string UserName = myResourceList.GetUser( ResIndex );
				if( UserName != null ) {
					n_Debug.Warning.AddClashMessage( "模块[" + m.Name + "]需要的资源<" + ResName  + ">已被<" + UserName + ">占用" );
					continue;
				}
				myResourceList.SetUser( ResIndex, m.Name, m.ImageName );
			}
		}
		//配置组件接口
		for( int i = 0; i < m.LinkInterfaceList.Length; ++i ) {
			string TargetName = m.LinkInterfaceList[ i ][ 0 ];
			if( TargetName != "" ) {
				GUItext += m.Name + "." + m.LinkInterfaceList[ i ][ m.GetLC() ] + " -> " + TargetName + ";\n";
			}
			else {
				n_Debug.Warning.AddClashMessage( "模块 [" + m.Name + "] 需要连接到目标设备才能运行" );
			}
		}
		//导入组件驱动代码
		GUItext += "#define " + GetSysDefine( "language" ) + " " + m.Language + "\n";
		
		GUItext += "const int16 " + m.Name + "_ctsurport_ID = " + m.VUI_ID + ";\n";
		GUItext += m.Name + ".driver.VUI_ID = " + m.Name + "_ctsurport_ID;\n";
		
		GUItext += "public " + m.Name + " =\n";
		GUItext += Config.LOAD + " <" + m.IncPackFilePath.Remove( 0, m.IncPackFilePath.IndexOf( ' ' ) + 1 ) + ">\n";
		return GUItext;
	}
	
	//转换嵌入式操作系统代码
	static string ConvertOSCode( int ChipIndex, MyObjectList ml, string VarInit, string ClientGUI, string ClientCode )
	{
		string OS_init = n_Param.Param.S_Param + " " + n_Param.Param.S_UserspaceOn + "\n" + "void main" + ChipIndex + "(void)\n{\n";
		
		//OS_init += "VVVV();\n";
		
		if( GOTO ) {
			OS_init += "\tSetup();\n\tloop {\n\t\tLoop();\n\t}\n}\nvoid Setup(void)\n{\n";
		}
		
		OS_init += "\tOS_VarInit();\n" + "\tOS" + ChipIndex + ".OS_init();\n";
		if( LoadDoStringLib ) {
			OS_init += "\t#.sys_DString.Init();\n";
		}
		
		string OS_add_driver = null;
		
		int RunNumber = 0;
		int Run100usNumber = 0;
		
		string OS_add_init = null;
		string OS_AddInitControl = null;
		string OS_AddInitMem = null;
		string OS_AddInitModule = null;
		
		bool isVHRemoModule = false;
		
		foreach( MyObject mo in ml ) {
			if( mo.ChipIndex != ChipIndex ) {
				continue;
			}
			if( mo is MyFileObject ) {
				MyFileObject CM = (MyFileObject)mo;
				
				//判断是否包含远程板模块
				if( CM is HardModule ) {
					HardModule HCM = (HardModule)CM;
					if( HCM.isVHRemoModule ) {
						isVHRemoModule = true;
					}
				}
				
				//组件初始化
				if( Contain( CM, InterfaceType.Function_, AIsuaUseName.OS_init ) ) {
					
					if( CM is MyFileObject && ((MyFileObject)CM).isControlModule ) {
						OS_AddInitControl += "\t" + CM.Name + "." + AIsuaUseName.OS_init + "();\n";
					}
					else if( CM.ImageName.StartsWith( "MEM_" ) ) {
						OS_AddInitMem += "\t" + CM.Name + "." + AIsuaUseName.OS_init + "();\n";
					}
					else {
						OS_AddInitModule += "\t" + CM.Name + "." + AIsuaUseName.OS_init + "();\n";
					}
				}
				//注入 内核驱动 到调度器
				if( Contain( CM, InterfaceType.Function_, AIsuaUseName.OS_run ) ) {
					++RunNumber;
					OS_add_driver += "\tOS" + ChipIndex + ".CreateDriver( " + "&" + CM.Name + ".OS_run, " + CM.Name + ".OS_time );\n";
				}
				//注入 快速内核驱动 到调度器
				if( Contain( CM, InterfaceType.Function_, AIsuaUseName.OS_run100us ) ) {
					++Run100usNumber;
					
					OS_add_driver += "\tOS" + ChipIndex + ".CreateDriver100us( &" + CM.Name + ".OS_run100us );\n";
				}
//				continue;
			}
//			if( ModuleList[ i ] is UIModule ) {
//				UIModule CM = (UIModule)ModuleList[ i ];
//				
//				//组件初始化
//				if( Contain( CM, InterfaceType.Function, AIsuaUseName.OS_init ) ) {
//					OS_add_init += "\t" + CM.Name + "." + AIsuaUseName.OS_init + "();\n";
//				}
//				//注入 内核驱动 到调度器
//				if( Contain( CM, InterfaceType.Function, AIsuaUseName.OS_run ) ) {
//					++RunNumber;
//					
//					OS_add_driver += "\tOS" + ChipIndex + ".CreateDriver( " +
//						"(&" + ModuleList[ i ].Name + ".OS_run), " + ModuleList[ i ].Name + ".OS_time );\n";
//				}
//				continue;
//			}
		}
		OS_add_init = OS_AddInitControl + OS_AddInitMem + OS_AddInitModule;
		
		if( G.ccode.ExistPython() ) {
			//OS_add_init += "\tPYVM.Init();\n";
		}
		
		//初始化通信管道
		if( isVHRemoModule ) {
			OS_init += "\t#.OS.REMO_DataChannelInit();\n";
		}
		
		OS_init += OS_add_init;
		OS_init += OS_add_driver;
		
		if( !GOTO ) {
			OS_init += "\tx_TempAddEvent = OS" + ChipIndex + ".CreateTask( &dispatch );\n";
		}
		
		if( DebugOpen ) {
			OS_init += "\tSYS_Debug.Debug_init();\n";
		}
		
		if( cmd_simulate ) {
			OS_init += "\tSim.OS_init();\n";
		}
		
		
		OS_init += ClientGUI + VarInit;
		
		//操作系统开始运行
		OS_init += "\tOS" + ChipIndex + ".Start();\n";
		
		if( G.isCruxEXE ) {
			if( !G.isCV ) {
				OS_init += "\trun Start();\n";
			}
			else {
				OS_init += "\tV_Sche();\n";
			}
		}
		
		if( GOTO ) {
			OS_init += "}\n";
			OS_init += "void Loop(void)\n{\n";
			
			//临时
			if( cmd_download && n_ExportForm.ExportForm.isv ) {
				OS_init += "\tOS0.Tmp_SysRun();\n";
			}
		}
		
		
		string Pack = "\n//*******************************************************\n";
		
		
		string OSPath = "<system" + OS.PATH_S;
		if( GOTO ) {
			OSPath += "os" + OS.PATH_S + "os.txt>";
		}
		else {
			OSPath += "os-old" + OS.PATH_S + "os.txt>";
		}
		if( !cmd_simulate && ml.GPanel.OSPath != null ) {
			OSPath = ml.GPanel.OSPath;
		}
		Pack += "OS" + ChipIndex + " =\n#include " + OSPath + "\n";
		Pack += "const uint8 x" + ChipIndex + "_OS_R0 = " + RunNumber + "; OS" + ChipIndex + ".RUN_NUMBER = x" + ChipIndex + "_OS_R0;\n";
		Pack += "const uint8 x" + ChipIndex + "_OS_R1 = " + Run100usNumber + "; OS" + ChipIndex + ".RUN100us_NUMBER = x" + ChipIndex + "_OS_R1;\n";
		
		if( !GOTO ) {
			Pack += "const uint8 x" + ChipIndex + "_OS_R2 = " + ml.GPanel.EventNumber + "; OS" + ChipIndex + ".TASK_NUMBER = x" + ChipIndex + "_OS_R2;\n";
			Pack += "const uint16 x" + ChipIndex + "_OS_R3 = " + ml.GPanel.EventVarStack + "; OS" + ChipIndex + ".EVENT_VAR_STACK = x" + ChipIndex + "_OS_R3;\n";
			Pack += "const uint16 x" + ChipIndex + "_OS_R4 = " +  ml.GPanel.EventFuncStack + "; OS" + ChipIndex + ".EVENT_FUNC_STACK = x" + ChipIndex + "_OS_R4;\n";
		}
		else {
			Pack += "const uint8 x" + ChipIndex + "_OS_R2 = <<TotalGoNumber>>; OS" + ChipIndex + ".TASK_NUMBER = x" + ChipIndex + "_OS_R2;\n";
		}
		
		//仿真时导入crux仿真包
		//if( G.isCruxEXE && cmd_simulate ) {
			Pack += "Sim =\n#include <system" + OS.PATH_S + "Sim" + OS.PATH_S + "Pack.txt>\n";
		//}
		
		if( G.ccode.ExistPython() ) {
			//Pack += "const uint16 x" + ChipIndex + "_OS_R3 = 450; OS" + ChipIndex + ".ComStackLength = x" + ChipIndex + "_OS_R3;\n";
		}
		else {
			//判断是否需要优化OS
			if( cputype1 == CPU_Type.AVR && t_GPanel.RAM_Save > 0 ) {
				//Pack += "const uint8 x" + ChipIndex + "_OS_R4 = 200; OS" + ChipIndex + ".ComStackLength = x" + ChipIndex + "_OS_R4;\n";
			}
		}
		
		Pack += ClientCode;
		Pack += "#._start_ = main" + ChipIndex + ";\n";
		Pack += "#.OS = OS" + ChipIndex + ";\n";
		Pack += OS_init;
		
		ml.RunNumber = RunNumber;
		
		return Pack;
	}
	
	//转换客户端GUI代码
	static string ConvertClientGUI( int ChipIndex, MyObjectList ml, ref string ClientCode, ref string ClientChannelName )
	{
		UIModule ClientPad = null;
		
		foreach( MyObject mo in ml ) {
			if( mo is UIModule ) {
				UIModule CM = (UIModule)mo;
				if( CM.isClientControlPad ) {
					if( ClientPad != null ) {
						n_Debug.Warning.AddClashMessage( "注意: 最多只能有一个控制面板" );
					}
					ClientPad = CM;
				}
			}
		}
		if( ClientPad == null ) {
			return null;
		}
		string ClientTextCode = "";
		ClientCode = "";
		string Result = null;
		int ID = 0;
		foreach( MyObject mo in ml ) {
			if( !(mo is UIModule) ) {
				continue;
			}
			UIModule CM = (UIModule)mo;
			CM.VUI_ID = ID;
			string controlType = CM.GetControlType();
			
			int X = CM.SX - ClientPad.SX;
			int Y = CM.SY - ClientPad.SY;
			Y = n_SG_MyControl.MyControl.Y_Default? Y: ClientPad.Height - Y;
			
			if( controlType == ControlType.ControlPad ) {
				ClientCode += GetVuiObjConfig( CM, 1, X, Y, ref ClientTextCode );
			}
			if( controlType == ControlType.Panel ) {
				
			}
			if( controlType == ControlType.Button ) {
				ClientCode += GetVuiObjConfig( CM, 2, X, Y, ref ClientTextCode );
			}
			if( controlType == ControlType.Label ) {
				ClientCode += GetVuiObjConfig( CM, 3, X, Y, ref ClientTextCode );
			}
			if( controlType == ControlType.NumberBox ) {
				
			}
			if( controlType == ControlType.TrackBar ) {
				ClientCode += GetVuiObjConfig( CM, 4, X, Y, ref ClientTextCode );
			}
			if( controlType == ControlType.CheckBox ) {
				
			}
			
			//if( Contain( CM, InterfaceType.Var_, AIsuaUseName.OS_EventFlag ) ) {
			//	Result += "\t" + ClientChannel.Name + ".FlagList[" + ID + "] -> " + CM.Name + ".OS_EventFlag;\n";
			//}
			++ID;
		}
		ClientCode = ClientCode.TrimEnd( ',' );
		ClientCode = ClientTextCode + "[]#.code int32 vui_ObjList = {" + (ID) + ", " + ClientCode + "};\n";
		//ClientCode = SetIDList;
		//ClientCode += "#.linker = " + ClientChannel.Name + ";\n";
		
		return Result;
	}
	
	static string GetVuiObjConfig( UIModule CM, int type, int x, int y, ref string ClientTextCode )
	{
		string text_name = CM.Name + "_text";
		ClientTextCode += "[]#.code uint8 " + text_name + " = \""+ CM.Text + "\";\n";
		
		string code = type + ",";
		code += x + ",";
		code += y + ",";
		code += CM.Width + ",";
		code += CM.Height + ",";
		code += n_MyColor.MyColor.GetValue( CM.BackColor ) + ",";
		code += n_MyColor.MyColor.GetValue( CM.EdgeColor ) + ",";
		code += n_MyColor.MyColor.GetValue( CM.ForeColor ) + ",";
		code += "#addr " + text_name + ",";
		
		return code;
	}
	
	//封装一个预定义词
	public static string GetSysDefine( string s )
	{
		return "$" + s + "$";
	}
	
	//查找某个组件中是否包含指定类型和名称的成员
	public static bool Contain( MyFileObject m, string Type, string Name )
	{
		for( int i = 0; i < m.ElementList.Length; ++i ) {
			if( m.ElementList[ i ][ 1 ].StartsWith( Type ) && m.ElementList[ i ][ 2 ] == Name ) {
				return true;
			}
		}
		return false;
	}
	
	//获取当前文件的文件名描述
	public static string GetUserFileName( int ChipIndex, MyObjectList myModuleList )
	{
		string Name = "";
		int n = 0;
		
		//转换链接配置信息
		foreach( MyObject mo in myModuleList ) {
			if( mo.ChipIndex != ChipIndex ) {
				continue;
			}
			if( mo is MyFileObject ) {
				MyFileObject m = (MyFileObject)mo;
				if( m.ImageName == SPMoudleName.SYS_Arduino ) {
					continue;
				}
				if( m is MyFileObject && ((MyFileObject)m).isControlModule ) {
					string DefaultChip = m.ImageName;
					if( DefaultChip.IndexOf( "-" ) != -1 ) {
						string[] cut = DefaultChip.Split( '-' );
						if( cut[0] == "Nucleo" ) {
							Name += m.ChipType;
						}
						else {
							Name += cut[0];
						}
					}
				}
				else {
					Name += m.Name;
				}
				n += 1;
				if( n > 5 ) {
					break;
				}
				Name += "_";
			}
		}
		if( Name == "" ) {
			Name = "空文件";
		}
		return Name;
	}
	
	public static string GetAPI( MyObjectList myModuleList, bool codemode )
	{
		string FuncList = "[]#.code uint32 SYS_FuncList = {\n";
		string CruxCode = "";
		string Common = "LB_value = 0\n";
		string ButtonList = "";
		int Index = 0;
		
		Common += "def Serial_Write1( id, fid, d1 ):\n\t#这里调用本地系统的串口发送函数\n";
		Common += "def Serial_Write2( id, fid, d1, d2 ):\n\t#这里调用本地系统的串口发送函数\n";
		Common += "def Serial_Write3( id, fid, d1, d2, d3 ):\n\t#这里调用本地系统的串口发送函数\n";
		Common += "def Serial_Write4( id, fid, d1, d2, d3, d4 ):\n\t#这里调用本地系统的串口发送函数\n\n";
		
		string Result = "";
		
		//扫描模块类指令
		foreach( MyObject mo in myModuleList ) {
			if( !( mo is MyFileObject ) ) {
				continue;
			}
			MyFileObject m = (MyFileObject)mo;
			if( m.ImageName == n_GUIcoder.SPMoudleName.SYS_delayer ) {
				continue;
			}
			string[] eList = new string[ m.ElementList.Length + 128 ];
			
			CruxCode += "if( ID == " + m.Index + " ) {\n";
			
			for( int n = 0; n < m.ElementList.Length; ++n ) {
				
				//扫描分割线
				if( m.ElementList[ n ][ 1 ].StartsWith( InterfaceType.Split ) ) {
					Result += "#---------------------------------------";
				}
				//扫描函数成员
				if( m.ElementList[ n ][ 1 ].StartsWith( InterfaceType.Function_ ) && !m.ElementList[ n ][ 2 ].StartsWith( "OS_" ) ) {
					string[] tempTypeList = m.ElementList[ n ][ 1 ].Split( '_' );
					if( tempTypeList.Length >= 3 && tempTypeList[2] == FuncExtType.Old ) {
						continue;
					}
					//获取英文指令
					string[] temp = m.ElementList[ n ][ 3 ].Split( '+' );
					
					int lindex = m.GetLC();
					if( G.isCruxEXE  ) {
						lindex = 3;
					}
					string funcname = m.Name + "." + m.ElementList[ n ][ lindex ].Replace( "+", "" );
					
					if( m.GroupMes != null ) {
						funcname = n_GroupList.Group.GetFullName( m.GroupMes, funcname );
					}
					if( G.isCruxEXE  ) {
						FuncList += "\t&#." + funcname.Replace( "#", "" ) + ",\n";
					}
					else {
						FuncList += "\t&#." + funcname.Replace( "#", "_" ) + ",\n";
					}
					Result += "#功能描述: " + m.Name + " " + m.ElementList[ n ][ m.GetLC() ] + "\n";
					
					string[] cut = funcname.Split( '#' );
					string rr = cut[0];
					bool ignor = false;
					for( int i = 1; i < cut.Length; ++i ) {
						
						string tp = "?" + tempTypeList[2 - 1 + i];
						if( tp != n_EXP.EXP.ENote.v_int32 && tp != n_EXP.EXP.ENote.v_bool ) {
							ignor = true;
						}
						
						rr += "#" + (i-1) + cut[i];
					}
					if( ignor ) {
						ButtonList += "!" + tempTypeList[1] + "  " + rr + "\n";
					}
					else {
						ButtonList += tempTypeList[1] + "  " + rr + "\n";
					}
					
					Result += "def " + m.NameList[2] + "_" + temp[0] + "(";
					char v = 'a';
					for( int i = 1; i < temp.Length; ++i ) {
						if( temp[i] != "#" ) {
							continue;
						}
						Result += " " + v;
						
						v = (char)(v+1);
						if( i == temp.Length - 1 ) {
							Result += " ";
						}
						else {
							Result += ", ";
						}
					}
					CruxCode += "\tif( n == " + n + " ) {";
					CruxCode += " #." + m.Name + "." + m.ElementList[ n ][ m.GetLC() ].Replace( "+", "" ).Replace( "#", "_" ) + "(";
					
					if( v - 'a' == 0 ) {
						Result += "):\n\tSerial_Write1( " + m.Index + ", " + n + " )\n\treturn LB_value\n";
						CruxCode += "";
					}
					if( v - 'a' == 1 ) {
						Result += "):\n\tSerial_Write1( " + m.Index + ", " + n + ", a )\n\treturn LB_value\n";
						CruxCode += " a ";
					}
					if( v - 'a' == 2 ) {
						Result += "):\n\tSerial_Write1( " + m.Index + ", " + n + ", a, b )\n\treturn LB_value\n";
						CruxCode += " a, b ";
					}
					if( v - 'a' == 3 ) {
						Result += "):\n\tSerial_Write1( " + m.Index + ", " + n + ", a, b, c )\n\treturn LB_value\n";
						CruxCode += " a, b, c ";
					}
					if( v - 'a' == 4 ) {
						Result += "):\n\tSerial_Write1( " + m.Index + ", " + n + ", a, b, c, d )\n\treturn LB_value\n";
						CruxCode += " a, b, c, d ";
					}
					CruxCode += "); }\n";
					Index++;
				}
			}
			CruxCode += "}\n";
		}
		//扫描用户指令
		foreach( MyObject mo in myModuleList ) {
			if( mo is UserFunctionIns ) {
				UserFunctionIns m = (UserFunctionIns)mo;
				string UserType = m.GetUserType();
				string funcname = m.GetRemoName();
				
				if( mo.GroupMes != null ) {
					funcname = n_GroupList.Group.GetFullName( mo.GroupMes, funcname );
				}
				bool ignor = false;
				if( funcname.IndexOf( "#" ) != -1 ) {
					ignor = true;
					//continue;
				}
				string[] cut = funcname.Split( '#' );
				string rr = cut[0];
				for( int i = 1; i < cut.Length; ++i ) {
					rr += "#" + (i-1) + cut[i];
				}
				if( ignor ) {
					ButtonList += "@" + UserType + "  " + rr + "\n";
				}
				else {
					ButtonList += UserType + "  " + rr + "\n";
				}
				
				string fname = m.GetFuncName();
				if( mo.GroupMes != null ) {
					fname = n_GroupList.Group.GetFullName( mo.GroupMes, fname );
				}
				FuncList += "\t&#." + fname + ",\n";
			}
			if( mo is EventIns ) {
				EventIns em = (EventIns)mo;
				string UserType = "void";
				string ecode = em.EventEntry;
				if( em.GroupMes != null ) {
					ecode = n_GroupList.Group.GetFullName( em.GroupMes, ecode );
				}
				ButtonList += UserType + "  " + ecode + "\n";
				FuncList += "\t&#." + ecode + ",\n";
			}
		}
		
		if( codemode ) {
			FuncList += "\t&#.SYS_NullFunc,\n";
			return FuncList + "};\n";// + CruxCode + Common + Result;
		}
		else {
			return ButtonList;
		}
	}
	
	//获取字符串的拼音格式
	public static string GetPinYin( string mes )
	{
		mes = Pinyin.GetPinyin( mes );
		
		return mes.Replace( " ", "" );
		
		/*
		string s = "";
		for( int i = 0; i < mes.Length; ++i ) {
			if( mes[i] == ' ' && i < mes.Length - 1 ) {
				i++;
				if( mes[i] != '_' ) {
					s += ((char)((int)mes[i] - ('a' - 'A' ))).ToString();
				}
				else {
					s += mes[i].ToString();
				}
			}
			else {
				s += mes[i].ToString();
			}
		}
		return s;
		*/
	}
}
//版本转换
public static class VersionSwitch
{
	static string[][] LibVersion;
	
	//初始化
	public static void Init()
	{
		//读取配置文件
		string LibConfig = VIO.OpenTextFileGB2312( OS.ModuleLibPath + "versionswitch" + OS.PATH_S + "2017.10.8.txt" );
		
		string[] ss = LibConfig.Trim( '\n' ).Split( '\n' );
		LibVersion = new string[ss.Length][];
		for( int i = 0; i < LibVersion.Length; ++i ) {
			LibVersion[i] = ss[i].Split( ',' );
		}
	}
	
	//版本转换
	public static string Switch( string FileName, bool isuser )
	{
		if( isuser ) {
			return G.ccode.FilePath + Switch_nofull( FileName );
		}
		else {
			return OS.ModuleLibPath + Switch_nofull( FileName );
		}
	}
	
	//版本转换1
	public static string Switch_nofull( string FileName )
	{
		if( FileName.IndexOf( "育松电子" ) != -1 ) {
			FileName = FileName.Replace( "育松电子", "group1" );
		}
		if( FileName.IndexOf( "电子信息等级考试器材" ) != -1 ) {
			FileName = FileName.Replace( "电子信息等级考试器材", "电子学会等级考试器材" );
		}
		if( FileName.IndexOf( "等级考试普惠课程器材" ) != -1 ) {
			FileName = FileName.Replace( "等级考试普惠课程器材", "创客教育普惠课程器材" );
		}
		for( int i = 0; i < LibVersion.Length; ++i ) {
			if( FileName == LibVersion[i][0] ) {
				FileName = LibVersion[i][1];
				break;
			}
		}
		return FileName;
	}
	
	//获取旧版本的路径, 用于示例文件汇总
	public static string Switch_GetOld( string FileName )
	{
		for( int i = 0; i < LibVersion.Length; ++i ) {
			if( FileName == LibVersion[i][1] ) {
				FileName = LibVersion[i][0];
				break;
			}
		}
		return FileName;
	}
}
}



