﻿
using System;
using System.IO;
using System.Windows.Forms;
using n_OS;

namespace n_Backup
{
public static class Backup
{
	public static string BackupPath;
	
	//初始化
	public static void Init()
	{
		BackupPath = OS.SystemRoot + "备份历史";
		if( !Directory.Exists( BackupPath ) ) {
			Directory.CreateDirectory( BackupPath );
		}
	}
	
	//保存当前文件到历史文件夹
	public static void CopyFile()
	{
		if( !n_OS.OS.isGForm ) {
			return;
		}
		if( G.ccode.isStartFile || G.ccode.isSystemFile ) {
			return;
		}
		string source_path = G.ccode.PathAndName;
		string tar_path = GetBPath();
		try {
			//这里打开一个文件后一秒钟内点击下载或者仿真, 会导致存在同名的文件, 所以需要判断
			if( !File.Exists( tar_path ) ) {
				File.Copy( source_path, tar_path );
			}
		}
		catch( Exception e ) {
			n_Debug.Warning.BUG( "备份文件时遇到错误: \n" + e.ToString() );
		}
	}
	
	//保存新的程序文本到备份文件夹
	public static void SaveText()
	{
		if( !n_OS.OS.isGForm ) {
			return;
		}
		if( G.ccode.Saved != null ) {
			G.ccode.Saved( null, EventArgs.Empty );
		}
		string tar_path = GetBPath();
		VIO.SaveTextFileGB2312( tar_path, G.ccode.Text );
	}
	
	//---------------------------------------------
	
	//获取备份文件名
	static string GetBPath()
	{
		string tar_path = BackupPath + OS.PATH_S + n_MyFile.MyFile.GetDateTimeNow();
		tar_path += " " + G.ccode.FileName + "." + G.ccode.FileType;
		return tar_path;
	}
}
}




