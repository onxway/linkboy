﻿
using System;
using System.Drawing;

namespace n_MyColor
{
public static class MyColor
{
	//获取指定色调和亮度的颜色
	public static Color GetColor( int an, int l )
	{
		Color c = GetColor( an );
		if( l >= 128 ) {
			int r = c.R + (255 - c.R) * (l - 128) / 128;
			int g = c.G + (255 - c.G) * (l - 128) / 128;
			int b = c.B + (255 - c.B) * (l - 128) / 128;
			c = Color.FromArgb( r, g, b );
		}
		else {
			int r = c.R - c.R * (128 - l) / 128;
			int g = c.G - c.G * (128 - l) / 128;
			int b = c.B - c.B * (128 - l) / 128;
			c = Color.FromArgb( r, g, b );
		}
		return c;
	}
	
	//获取指定色调的颜色
	public static Color GetColor( int an )
	{
		int R, G, B;
		
		if( an > 0 ) {
			an %= 360;
		}
		if( an < 0 ) {
			an = -an;
			an %= 360;
			an = 359 - an;
		}
		
		an = an * 512 / 120;
		
		if( an < 256 ) {
			R = 255;
			G = an;
			B = 0;
		}
		else if( an < 512 ) {
			R = 511 - an;
			G = 255;
			B = 0;
		}
		else if( an < 768 ) {
			R = 0;
			G = 255;
			B = an - 512;
		}
		else if( an < 1024 ) {
			R = 0;
			G = 1023 - an;
			B = 255;
		}
		else if( an < 1280 ) {
			R = an - 1024;
			G = 0;
			B = 255;
		}
		else {
			
			R = 255;
			G = 0;
			B = 1535 - an;
		}
		return Color.FromArgb( R, G, B );
	}
	
	public static Color GetColor( byte low, byte gigh )
	{
		int c = low;
		c += 256 * gigh;
		int rr = ((c >> 11) << 3);
		int gg = (((c >> 5) & 0x003F) << 2);
		int bb = ((c & 0x001F) << 3);
		
		return Color.FromArgb( rr, gg, bb );
	}
	
	//获取一个颜色的数值, 忽略透明度
	public static int GetValue( Color c )
	{
		return (c.R << 16) + (c.G << 8) + c.B;
	}
}
}


