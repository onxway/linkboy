﻿
//命名空间
namespace n_Py_GUIcoder
{
using System;
using System.Drawing;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using System.Text;
using System.Windows.Forms;
using c_ControlType;

using c_MyObjectSet;
using n_Config;
using n_EventLink;
using n_GNote;
using n_GUIset;
using n_GVar;
using n_HardModule;
using n_ImagePanel;
using n_MyFileObject;
using n_MyObject;
using n_MyObjectList;
using n_OS;
using n_Shape;
using n_UIModule;
using n_UserModule;
using n_VarType;
using n_MainSystemData;
using n_CPUType;

using n_MyIns;
using n_WhileIns;
using n_EventIns;
using n_IfElseIns;
using n_ElseIns;
using n_CondiEndIns;
using n_WaitIns;
using n_FuncIns;
using n_ForeverIns;
using n_LoopIns;
using n_ExtIns;
using n_UserFunctionIns;
using n_MidPortList;
using NPinyin;
using n_GUIcoder;

//GUI界面解析引擎
public static class Py_GUIcoder
{
	static string ModFile;
	static string VarInit;
	static string EventList;
	static string ModInit;
	static string ModLoop;
	
	static string GlobalVar;
	
	public static string CSource;
	
	//获取py代码
	public static string Py_GetSysCode( ImagePanel GPanel )
	{
		//ModFile = "from microbit import *\n";
		ModFile = "from mpython import *\n";
		ModFile += "import music\n";
		
		ModInit = null;
		VarInit = null;
		ModLoop = null;
		GlobalVar = null;
		
		EventList = GetUserCode( GPanel.myModuleList );
		
		if( GlobalVar != null ) {
			GlobalVar = "global " + GlobalVar.Remove( GlobalVar.Length - 1 );
		}
		
		//转换链接配置信息
		foreach( MyObject mo in GPanel.myModuleList ) {
			
			if( mo.ChipIndex != 0 ) {
				continue;
			}
			if( mo is HardModule ) {
				Py_ConvertOneModule( GPanel.myModuleList, mo.Index );
			}
		}
		
		/*
		ModLoop += "\tdisplay.set_pixel(4, 4, 4)\n";
		ModLoop += "\tsleep(5)\n";
		ModLoop += "\tdisplay.set_pixel(4, 4, 0)\n";
		ModLoop += "\tsleep(5)\n";
		*/
		//ModLoop += "\trgb[0] = (int(255/30), int(0/30), int(0/30))\n";
		//ModLoop += "\trgb.write()\n";
		//ModLoop += "\tsleep_ms(5)\n";
		//ModLoop += "\trgb.fill( (0, 0, 0) )\n";
		//ModLoop += "\trgb.write()\n";
		ModLoop += "\tsleep_ms( <<sys_tick>> )\n";
		ModLoop = "while True:\n" + ModLoop;
		
		string source = ModFile + VarInit + EventList + ModInit + ModLoop;
		
		//进行全局变量声明替换
		if( GlobalVar != null ) {
			source = source.Replace( "#<global>", GlobalVar );
		}
		
		//-------------------------------------------------------------------
		//格式化处理
		source = source.Replace( "\t", "    " );
		
		string[] c = source.Split( '\n' );
		string r = "";
		for( int i = 0; i < c.Length; ++i ) {
			if( c[i].TrimStart( ' ' ).StartsWith( "#" ) ) {
				continue;
			}
			if( c[i].TrimStart( ' ' ) == "" ) {
				continue;
			}
			r += c[i] + "\n";
		}
		//转换到英文
		CSource = r;
		
		r = toEnglish( r );
		
		return r;
	}
	
	//转换到英文
	static string toEnglish( string r )
	{
		string ret = "";
		for( int i = 0; i < r.Length; ++i ) {
			if( r[i] > 127 ) {
				ret += "H";
				int d = (int)r[i];
				ret += d.ToString();
			}
			else {
				ret += r[i];
			}
		}
		return ret;
	}
	
	//转换单个组件的配置代码
	static void Py_ConvertOneModule(  MyObjectList ml, int Index )
	{
		HardModule m = (HardModule)ml.ModuleList[ Index ];
		
		if( m.PyFile == null ) {
			
			try {
			m.PyFile = n_OS.VIO.OpenTextFileGB2312( m.PyFilePath );
			} catch {
				m.PyFile = "=============无效的文件=============";
			}
		}
		
		ModFile += m.PyFile.Replace( "$", m.Name + "_" ) + "\n";
		
		ModInit += m.Name + "_OS_init()\n";
		
		ModLoop += "\t" + m.Name + "_OS_thread()\n";
		
		if( m is MyFileObject ) {
			SwitchLoop( (MyFileObject)m );
		}
	}
	
	//转换事件调度
	static void SwitchLoop( MyFileObject m )
	{
		string EventScan = "";
		for( int ei = 0; ei < m.EventList.Length; ++ei ) {
			
			string ename = m.EventList[ ei ][ 0 ];
			if( ename == "" ) {
				continue;
			}
			
			ModInit += ename + "_sys_thread = None\n";
			ModInit += ename + "_sys_run = False\n";
			ModInit += ename + "_sys_time = 0\n";
			ModInit += ename + "_sys_tick = 0\n";
			
			//添加延时循环代码
			EventScan += "\tif " + ename + "_sys_time != 0:\n";
			EventScan += "\t\t" + ename + "_sys_tick += 1\n";
			EventScan += "\t\tif " + ename + "_sys_tick == " + ename + "_sys_time:\n";
			EventScan += "\t\t\t" + ename + "_sys_tick = 0\n";
			EventScan += "\t\t\t" + ename + "_sys_time = 0\n";
			EventScan += "\t\t\ttry:\n";
			EventScan += "\t\t\t\t" + ename + "_sys_thread.send(0)\n";
			EventScan += "\t\t\texcept Exception:\n";
			EventScan += "\t\t\t\t" + ename + "_sys_run = False\n";
			
			EventScan += "\tif (not "+ ename + "_sys_run) and (" + m.Name + "_OS_EventFlag & " + (0x01<<ei) + ") != 0:\n";
			EventScan += "\t\t" + m.Name + "_OS_EventFlag &= " + ~(0x01<<ei) + "\n";
			EventScan += "\t\t" + ename + "_sys_run = True\n";
			EventScan += "\t\t" + ename + "_sys_thread = " + m.EventList[ ei ][ 0 ] + "()\n";
			EventScan += "\t\t" + ename + "_sys_thread.send(None)\n";
		}
		//判断是否需要调整扫描顺序
		if( m.isControlModule ) {
			ModLoop = EventScan + ModLoop;
		}
		else {
			ModLoop += EventScan;
		}
	}
	
	//生成代码
	public static string GetUserCode( MyObjectList myModuleList )
	{
		//转换新版指令流程图代码
		string UserCode = "";
		
		foreach( MyObject mo in myModuleList ) {
			if( mo is EventIns || mo is UserFunctionIns ) {
				
				if( mo is UserFunctionIns ) {
					UserFunctionIns ui = (UserFunctionIns)mo;
					if( ui.isEvent && ui.Target != null ) {
						continue;
					}
				}
				UserCode += n_GUIcoder.GUIcoder.ConvertOneMyInsForRealPython( 0, (MyIns)mo );
			}
			if( mo is n_GVar.GVar ) {
				
				n_GVar.GVar gv = (GVar)mo;
				if( gv.VarType == GType.g_int32 ) {
					
					GlobalVar += gv.Name + ",";
					
					VarInit += gv.Name + " = 0\n";
				}
				if( gv.VarType == GType.g_bitmap || gv.VarType == GType.g_font ) {
					
					GlobalVar += gv.Name + ",";
					
					if( gv.Value != "" ) {
						string s = gv.Value;
						if( s.StartsWith( n_LatticeToCode.LatticeToCode.L_V1_RightDown ) ) {
							s = s.Remove( 0, 13 );
						}
						VarInit += gv.Name + " =\n[\\\n" + s + "\n]\n";
					}
					else {
						VarInit += gv.Name + " = None\n";
					}
					continue;
				}
			}
		}
		return UserCode;
	}
}
}



