﻿
//系统参数
namespace n_MainSystemData
{
using System;
using System.Drawing;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using n_OS;
using n_CPUType;

public static class SystemData
{
	//初始化
	public static void Init()
	{
		isChanged = false;
		
		UserData.Init();
	}
	
	//加载对象, 从:  系统数据.ser
	public static void Load()
	{
		stream = File.Open( OS.SystemRoot + "RV" + OS.PATH_S + OS.USER + OS.PATH_S + "Setting.ser", FileMode.Open );
		
		bin = new BinaryFormatter();
		
		try {
			SourceTextFont = (Font)bin.Deserialize( stream );
		} catch {
			SourceTextFont = new Font( "Curriew", 12 );
		}
		
		try {
		NoteColor = (Color)bin.Deserialize( stream );
		CharColor = (Color)bin.Deserialize( stream );
		StringColor = (Color)bin.Deserialize( stream );
		OperColor = (Color)bin.Deserialize( stream );
		NumberColor = (Color)bin.Deserialize( stream );
		MemberValueColor = (Color)bin.Deserialize( stream );
		FunctionTypeColor = (Color)bin.Deserialize( stream );
		InsetConstColor = (Color)bin.Deserialize( stream );
		MemberTypeColor = (Color)bin.Deserialize( stream );
		VarTypeColor = (Color)bin.Deserialize( stream );
		FlowColor = (Color)bin.Deserialize( stream );
		OtherColor = (Color)bin.Deserialize( stream );
		IdentColor = (Color)bin.Deserialize( stream );
		SplitColor = (Color)bin.Deserialize( stream );
		SysInsColor = (Color)bin.Deserialize( stream );
		TextBackColor = (Color)bin.Deserialize( stream );
		
		GUIbackColorBlack = (Color)bin.Deserialize( stream );//界面背景颜色
		GUIbackColorWhite = (Color)bin.Deserialize( stream );//界面背景颜色
		FunctionListBackColor = (Color)bin.Deserialize( stream );//函数列表框背景颜色
		UnitLibBackColor = (Color)bin.Deserialize( stream );//元件库列表背景颜色
		HelpBoxBackColor = (Color)bin.Deserialize( stream );//协助文档框背景颜色
		HeadMessageBackColor = (Color)bin.Deserialize( stream );//信息输出框文字颜色
		FormColor = (Color)bin.Deserialize( stream );//窗体颜色
		
		isSuperUser = (bool)bin.Deserialize( stream );
		FunctionTreeWidth = (int)bin.Deserialize( stream );
		MessageShowerHeight = (int)bin.Deserialize( stream );
		
		isShowLeftBox = (bool)bin.Deserialize( stream ); //是否显示左边框
		isShowLineNumber = (bool)bin.Deserialize( stream ); //是否显示行号
		isShowCurrentIcon = (bool)bin.Deserialize( stream ); //是否显示当前光标位置提示
		isShowCompileResult = (bool)bin.Deserialize( stream ); //是否显示编译器的编译结果
		ShowLaterFile = (bool)bin.Deserialize( stream ); //是否显示近期文件列表
		
		FormLocationList = (Point[])bin.Deserialize( stream ); //窗体位置列表
		FormSizeList = (Size[])bin.Deserialize( stream ); //窗体尺寸列表
		
		TableLanguage_temp = (string)bin.Deserialize( stream ); //桌面语言
		ModuleLanguage = (string)bin.Deserialize( stream ); //组件语言
		isBlack = (bool)bin.Deserialize( stream ); //背景颜色是否为黑色
		CreateHEXFile = (bool)bin.Deserialize( stream ); //创建HEX文件
		OptimizeFlag = (int)bin.Deserialize( stream ); //读取优化标志
		
		isTouchMode = (bool)bin.Deserialize( stream ); //读取是否为触屏模式
		
		UserMessage = (string)bin.Deserialize( stream ); //读取用户显示信息
		WebMessage = (string)bin.Deserialize( stream ); //读取网站信息
		
		SwitchOldFile = (bool)bin.Deserialize( stream ); //自动转换旧文件
		ShowOldModule = (bool)bin.Deserialize( stream ); //是否显示过时的模块
		
		DefaultBoardType = (string)bin.Deserialize( stream ); //默认板子型号
		
		CreatTempSimFile = (bool)bin.Deserialize( stream ); //是否创建临时仿真文件
		
		ShowNewVersion = (bool)bin.Deserialize( stream ); //是否显示新功能提示页面
		
		LastCheckDate = (DateTime)bin.Deserialize( stream ); //上次检查更新时间
		
		Mohu = (bool)bin.Deserialize( stream ); //界面是否模糊虚化
		ModuleLibClick = (bool)bin.Deserialize( stream ); //模块库单击切换操作
		UIModeSimple = (bool)bin.Deserialize( stream ); //界面操作模式
		
		ExtMes = (string)bin.Deserialize( stream ); //比赛专用信息
		
		StartMode = (int)bin.Deserialize( stream ); //启动模式
		
		ColorType = (int)bin.Deserialize( stream ); //颜色模式
		
		ScreenKey = (int)bin.Deserialize( stream ); //截屏热键
		
		UILanguage = (string)bin.Deserialize( stream ); //界面语言
		n_Language.Language.SetLanguage( UILanguage );
		
		LeftBarValue = (int)bin.Deserialize( stream ); //左侧边栏位置
		TreeExpandFlag = (string)bin.Deserialize( stream ); //模块列表状态
		RightBarValue = (int)bin.Deserialize( stream ); //右侧边栏位置
		
		LAPI_IP = (string)bin.Deserialize( stream );
		LAPI_PORT = (string)bin.Deserialize( stream );
		
		LibOtherShow = (string)bin.Deserialize( stream );
		DPI_Val = (int)bin.Deserialize( stream );
		
		//n_OS.VIO.Show( TreeExpandFlag );
		
		}
		catch {
			isChanged = true;
			//System.Windows.Forms.MessageBox.Show( "注意: 您的配置文件比较旧, 有些新增设置项将会设置为默认值! 若要去掉这个提示框, 请点击软件右上角的设置图标, 然后点击保存按钮即可.\n" + e.ToString() );
		}
		
		
		if( isBlack ) {
			n_GUIset.GUIset.GUIbackColor = GUIbackColorBlack;
		}
		else {
			n_GUIset.GUIset.GUIbackColor = GUIbackColorWhite;
		}
		n_GUIset.GUIset.SoftLabel = UserMessage + G.VersionShow + " (" + G.VersionMessage + ")"; //G.VersionShow + 
		
		stream.Close();
		
		//这里是临时设置,防止用户无意间修改语言
		//TableLanguage = "english_UL";
		//ModuleLanguage = "chinese";
		
		UserData.Load();
	}
	
	//串行化对象,保存到:  系统数据.ser
	public static void Save()
	{
		/*
		if( OS.DefaultUser != null ) {
			stream = File.Open( OS.SystemPrePath + OS.DefaultUser + OS.PATH_S + "setting.ser", FileMode.Open );
		}
		else {
			stream = File.Open( OS.SystemRoot + "Resource" + OS.PATH_S + "setting" + OS.PATH_S + OS.USER + ".ser", FileMode.Open );
		}
		*/
		
		stream = File.Open( OS.SystemRoot + "RV" + OS.PATH_S + OS.USER + OS.PATH_S + "Setting.ser", FileMode.Open );
		
		bin = new BinaryFormatter();
		bin.Serialize( stream, SourceTextFont );
		bin.Serialize( stream, NoteColor );
		bin.Serialize( stream, CharColor );
		bin.Serialize( stream, StringColor );
		bin.Serialize( stream, OperColor );
		bin.Serialize( stream, NumberColor );
		bin.Serialize( stream, MemberValueColor );
		bin.Serialize( stream, FunctionTypeColor );
		bin.Serialize( stream, InsetConstColor );
		bin.Serialize( stream, MemberTypeColor );
		bin.Serialize( stream, VarTypeColor );
		bin.Serialize( stream, FlowColor );
		bin.Serialize( stream, OtherColor );
		bin.Serialize( stream, IdentColor );
		bin.Serialize( stream, SplitColor );
		bin.Serialize( stream, SysInsColor );
		bin.Serialize( stream, TextBackColor );  //文本框背景颜色
		
		bin.Serialize( stream, GUIbackColorBlack );  //界面背景颜色
		bin.Serialize( stream, GUIbackColorWhite );  //界面背景颜色
		bin.Serialize( stream, FunctionListBackColor );  //函数列表框背景颜色
		bin.Serialize( stream, UnitLibBackColor );  //元件库列表背景颜色
		bin.Serialize( stream, HelpBoxBackColor );  //协助文档框背景颜色
		bin.Serialize( stream, HeadMessageBackColor );  //信息输出框文字颜色
		bin.Serialize( stream, FormColor );  //窗体颜色
		
		bin.Serialize( stream, isSuperUser );
		bin.Serialize( stream, FunctionTreeWidth );
		bin.Serialize( stream, MessageShowerHeight );
		
		bin.Serialize( stream, isShowLeftBox ); //是否显示左边框
		bin.Serialize( stream, isShowLineNumber ); //是否显示行号
		bin.Serialize( stream, isShowCurrentIcon ); //是否显示当前光标位置提示
		bin.Serialize( stream, isShowCompileResult ); //是否显示编译器的编译结果
		bin.Serialize( stream, ShowLaterFile ); //是否显示近期文件列表
		
		bin.Serialize( stream, FormLocationList );
		bin.Serialize( stream, FormSizeList );
		
		bin.Serialize( stream, TableLanguage_temp );
		bin.Serialize( stream, ModuleLanguage );
		bin.Serialize( stream, isBlack );
		bin.Serialize( stream, CreateHEXFile );
		bin.Serialize( stream, OptimizeFlag );
		
		bin.Serialize( stream, isTouchMode );
		
		bin.Serialize( stream, UserMessage );
		bin.Serialize( stream, WebMessage );
		
		bin.Serialize( stream, SwitchOldFile );
		bin.Serialize( stream, ShowOldModule );
		
		bin.Serialize( stream, DefaultBoardType );
		
		bin.Serialize( stream, CreatTempSimFile );
		
		bin.Serialize( stream, ShowNewVersion );
		
		bin.Serialize( stream, LastCheckDate );
		
		bin.Serialize( stream, Mohu );
		bin.Serialize( stream, ModuleLibClick );
		bin.Serialize( stream, UIModeSimple );
		
		bin.Serialize( stream, ExtMes );
		bin.Serialize( stream, StartMode );
		
		bin.Serialize( stream, ColorType );
		bin.Serialize( stream, ScreenKey );
		
		bin.Serialize( stream, UILanguage );
		bin.Serialize( stream, LeftBarValue );
		bin.Serialize( stream, TreeExpandFlag );
		bin.Serialize( stream, RightBarValue );
		
		bin.Serialize( stream, LAPI_IP );
		bin.Serialize( stream, LAPI_PORT );
		
		bin.Serialize( stream, LibOtherShow );
		bin.Serialize( stream, DPI_Val );
		
		stream.Close();
	}
	
	
	
	//输出文件流对象
	static Stream stream;
	
	//串行化对象
	static BinaryFormatter bin;
	
	//是否改变
	public static bool isChanged;
	
	//+++++++++++++++++++++++++++++++++++++++++++++++==
	//以下为配置保存项
	
	//程序文本字体和颜色
	public static Font SourceTextFont;
	public static Color NoteColor;
	public static Color CharColor;
	public static Color StringColor;
	public static Color OperColor;
	public static Color NumberColor;
	public static Color MemberValueColor;
	public static Color FunctionTypeColor;
	public static Color InsetConstColor;
	public static Color MemberTypeColor;
	public static Color VarTypeColor;
	public static Color FlowColor;
	public static Color OtherColor;
	public static Color IdentColor;
	public static Color SplitColor;
	public static Color SysInsColor;
	
	public static Color TextBackColor; //文本背景颜色
	
	public static Color GUIbackColorBlack; //界面背景颜色
	public static Color GUIbackColorWhite; //界面背景颜色
	
	public static Color FunctionListBackColor; //函数列表框背景颜色
	public static Color UnitLibBackColor; //元件库列表背景颜色
	public static Color HelpBoxBackColor; //协助文档框背景颜色
	public static Color HeadMessageBackColor; //标题栏信息背景颜色
	public static Color FormColor; //窗体颜色
	
	public static bool isSuperUser;  //超级用户
	public static int FunctionTreeWidth;
	public static int MessageShowerHeight;
	public static bool isShowLeftBox; //是否显示左边框
	public static bool isShowLineNumber; //是否显示行号
	public static bool isShowCurrentIcon; //是否显示当前光标位置提示
	public static bool isShowCompileResult; //是否显示编译器的编译结果
	public static bool ShowLaterFile; //启动时是否显示近期文件列表
	
	//窗体属性列表
	public const int ControlBoxIndex = 0;
	public const int MesBoxIndex = 1;
	public const int FindBoxIndex = 2;
	public const int ErrorBoxIndex = 3;
	
	public static Point[] FormLocationList;	//窗体位置列表
	public static Size[] FormSizeList;	//窗体尺寸列表
	
	//语言项
	public static string TableLanguage_temp;
	
	public static string ModuleLanguage;
	
	//图形界面参数
	public static bool isBlack;
	
	//编译器设置
	public static bool CreateHEXFile;
	public static int OptimizeFlag;
	
	public static bool isTouchMode;
	
	//图形界面工作台的用户自定义信息
	public static string UserMessage;
	
	public static string WebMessage;
	
	public static bool SwitchOldFile;
	
	public static bool ShowOldModule;
	
	public static string DefaultBoardType;
	
	//是否生成临时仿真文件
	public static bool CreatTempSimFile;
	
	//启动软件时是否显示新功能提示页面
	public static bool ShowNewVersion;
	
	//上次联网检查更新的时间
	public static DateTime LastCheckDate;
	
	//图形界面是否模糊虚化
	public static bool Mohu;
	
	//模块库是否为点击操作
	public static bool ModuleLibClick;
	
	//图形界面操作模式
	public static bool UIModeSimple;
	
	//比赛专用定制信息
	public static string ExtMes;
	
	//是否显示启动模式对话框 0:元件模式  1:入门模块模式  2:完整模式
	public static int StartMode;
	
	//连接颜色设置
	public static int ColorType;
	
	//连接颜色设置
	public static int ScreenKey;
	
	//界面语言
	public static string UILanguage;
	
	//左侧边栏位置
	public static int LeftBarValue;
	
	//模块列表折叠状态
	public static string TreeExpandFlag;
	//第三方模块显示列表
	public static string LibOtherShow;
	
	//右侧边栏位置
	public static int RightBarValue;
	
	//python-LAPI仿真IP和端口
	public static string LAPI_IP;
	public static string LAPI_PORT;
	
	//DPI设置
	public static int DPI_Val;
}

public static class UserData
{
	//初始化
	public static void Init()
	{
		isChanged = false;
	}
	
	//加载对象, 从:  系统数据.ser
	public static void Load()
	{
		//string Path = Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData) + OS.PATH_S + "SystemTestUserV2" + OS.PATH_S;
		AppDataPath = OS.SystemRoot + "Resource" + OS.PATH_S + "setting" + OS.PATH_S + "user.ser";
		
		/*
		if( !Directory.Exists( Path ) ) {
			Directory.CreateDirectory( Path );
		}
		if( !File.Exists( AppDataPath ) ) {
			File.Create( AppDataPath ).Close();
		}
		*/
		
		stream = File.Open( AppDataPath, FileMode.Open );
		
		bin = new BinaryFormatter();
		
		try {
		StartDate = (DateTime)bin.Deserialize( stream ); //起始时间
		LastDate = (DateTime)bin.Deserialize( stream ); //上一次时间
		CPU_ID = (string)bin.Deserialize( stream ); //CPU信息
		TotalTime = (int)bin.Deserialize( stream ); //有效时间
		}
		catch {
			//System.Windows.Forms.MessageBox.Show( "注意: 您的配置文件比较旧, 有些新增设置项将会设置为默认值" );
			
			StartDate = System.DateTime.Now;
			LastDate = System.DateTime.Now;
			CPU_ID = "";
			TotalTime = 0;
			
			isChanged = true;
		}
		
		try {
		LastWebMes = (string)bin.Deserialize( stream ); //网页信息
		SignNumber = (string)bin.Deserialize( stream ); //签到信息
		LastSignDate = (DateTime)bin.Deserialize( stream ); //上一次签到时间
		}
		catch {
			LastWebMes = "0-0";
			SignNumber = "0";
			LastSignDate = System.DateTime.Now;
			isChanged = true;
		}
		try {
		NanoNew = (bool)bin.Deserialize( stream );
		}
		catch{
			NanoNew = true;
		}
		try {
		LastDirName = (string)bin.Deserialize( stream ); //网页信息
		}
		catch{
			LastDirName = null;
		}
		n_AVRdude.AVRdude.NanoNew = NanoNew;
		
		stream.Close();
		
		//这里是临时设置,防止用户无意间修改语言
		//TableLanguage = "english_UL";
		//ModuleLanguage = "chinese";
	}
	
	//串行化对象,保存到:  系统数据.ser
	public static void Save()
	{
		stream = File.Open( AppDataPath, FileMode.Open );
		
		bin.Serialize( stream, StartDate );
		bin.Serialize( stream, LastDate );
		bin.Serialize( stream, CPU_ID );
		bin.Serialize( stream, TotalTime );
		
		bin.Serialize( stream, LastWebMes );
		bin.Serialize( stream, SignNumber );
		bin.Serialize( stream, LastSignDate );
		
		bin.Serialize( stream, NanoNew );
		
		bin.Serialize( stream, LastDirName );
		
		stream.Close();
	}
	
	
	//--------------------------------------------------
	//用户文件路径
	static string AppDataPath;
	
	//输出文件流对象
	static Stream stream;
	
	//串行化对象
	static BinaryFormatter bin;
	
	//是否改变
	public static bool isChanged;
	
	//+++++++++++++++++++++++++++++++++++++++++++++++==
	//以下为配置保存项
	
	//授权开始时间
	public static DateTime StartDate;
	public static DateTime LastDate;
	public static string CPU_ID;
	public static int TotalTime;
	
	public static string LastWebMes;
	public static string SignNumber;
	public static DateTime LastSignDate;
	public static string LastDirName;
	
	public static bool NanoNew;
}
}

