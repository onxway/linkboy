﻿
namespace n_GUIset
{
using System;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Windows.Forms;
using n_MainSystemData;

//图形和形状类
public static class GUIset
{
	//软件名称
	public static string SoftLabel;
	
	//图形界面背景颜色和画刷
	public static Color myBackColor;
	public static Brush BackBrush;
	public static Pen BackPen;
	
	public static Brush BackBrushSim;
	public static Color myFreeAreaColor;
	public static Brush FreeAreaBrush;
	
	//标题栏颜色和窗体边框颜色
	public static Color FormColor;
	public static Brush FormBrush;
	
	//logo颜色
	public static Color HeadBackColor;
	public static Brush HeadBackBrush;
	
	//隐藏背景画刷
	public static Brush HideBackBrush;
	
	//中间对称文字显示格式
	public static StringFormat MidFormat;
	//左对齐文字显示格式
	public static StringFormat LeftFormat;
	//右对齐文字显示格式
	public static StringFormat RightFormat;
	
	//界面背景颜色
	public static Color GUIbackColor;
	
	//无线模块接口连接画笔
	public static Pen RF_Pen;
	//红外模块接口连接画笔
	public static Pen IR_Pen;
	//普通接口画笔
	public static Pen NOR_Pen;
	
	public const float CK_H = 21.19075f; //参考距离
	public static float SYS_H;
	public static float Scale;
	public static float pt = 9.0f;//8.0f; //标准为10
	public static float ptui = 9.0f; //此字体不需要根据分辨率和DPI修改
	
	//UI字体
	public static Font UIFont;
	//表达式字体
	public static Font ExpFont;
	//表达式字体
	public static Font ExpFontBack;
	
	//变量仿真数据字体
	public static Font SimFont;
	public static Font Font13;
	//Tip字体
	public static Font Font20;
	//调试字体
	public static Font DebugFont;
	
	
	//表达式最顶端词的边框画笔
	public static Pen WordPen;
	//表达式最顶端词的画刷
	public static Brush WordBrush;
	
	//用于调用图形化类的静态函数如字体尺寸等
	public static Graphics mg;
	
	//组件高亮的边框
	public static Pen 组件高亮边框_Pen1;
	public static Pen 组件高亮边框_Pen2;
	
	public static bool BlackHead = true;
	
	//初始化
	public static void Init()
	{
		//设置背景颜色
		//myBackColor = GetBlueColor( 50 );
		//myBackColor = GetBlueColor( 200 );
		//myBackColor = GetBlueColor( 180 );
		
		//ptui = 9.0f * SystemData.DPI_Val / 100;
		
		mg = Graphics.FromHwnd( System.IntPtr.Zero );
		myBackColor = GUIbackColor;
		
		//myBackColor = Color.FromArgb( 70, 70, 70 );
		//myBackColor = Color.SlateGray;
		//myBackColor = Color.FromArgb(35, 46, 40);
		//myBackColor = Color.FromArgb(45, 65, 95);
		BackBrush = new SolidBrush( myBackColor );
		
		BackBrushSim = new SolidBrush( Color.FromArgb( 95, 90, 85 ) );
		
		//LogoColor = Color.FromArgb(60, 130, 210);
		HeadBackColor = SystemData.HeadMessageBackColor;
		HeadBackBrush = new SolidBrush( HeadBackColor );
		
		myFreeAreaColor = Color.FromArgb( 240, 240, 240 );
		FreeAreaBrush = new SolidBrush( myFreeAreaColor );
		
		if( SystemData.isBlack ) {
			FormColor = GetBlueColor( 80 );
		}
		else {
			//FormColor = GetBlueColor1( 200 );//90
			//FormColor = GetBlueColor2( 255 );//90
			
			FormColor = SystemData.FormColor;
		}
		//FormBrush = new SolidBrush( GetBlueColor( 180 ) );
		FormBrush = new SolidBrush( FormColor );
		BackPen = new Pen( FormColor );
		//BackPen = new Pen( Color.Black );
		
		/*
		//根据DPI比例, 26应进行放大缩小
		int FH = 21 * SystemData.DPI_Val / 100;
		int EH = 19; //这个不要改, 应和模块图片比例保持固定, 但是高分辨率下, 会导致 左侧指令列表/表达式编辑器 里的字体偏小
		
		//自动计算表达式的字体 H:18.2, UI字体:26
		bool pt_ok = false;
		bool ptui_ok = false;
		for( float i = 1.0f; i < 35.0f; i += 0.25f ) {
			Font f = new Font( "微软雅黑", i, FontStyle.Regular );
			float h = mg.MeasureString( "你好", f ).Height;
			if( !pt_ok && h > EH ) {
				pt = i;
				pt_ok = true;
			}
			if( !ptui_ok && h > FH ) {
				ptui = i;
				ptui_ok = true;
			}
			f.Dispose();
			
			if( pt_ok && ptui_ok ) {
				break;
			}
		}
		*/
		
		float minvalue = 0;
		bool first = true;
		for( float i = 1.0f; i < 35.0f; i += 0.25f ) {
			Font f1 = new Font( "微软雅黑", i, FontStyle.Regular );
			float h = mg.MeasureString( "你好", f1 ).Height;
			float cd = Math.Abs( h - CK_H );
			if( first || minvalue > cd ) {
				first = false;
				minvalue = cd;
				pt = i;
			}
		}
		
		//ptui = 10; //笔记本10，台式机11
		ptui = SystemData.DPI_Val;
		if( ptui == 0 ) {
			ptui = 11;
		}
		Font f = new Font( "微软雅黑", ptui, FontStyle.Regular );
		SYS_H = mg.MeasureString( "你好", f ).Height;
		Scale = SYS_H / CK_H;
		
		//MessageBox.Show( "字号: " + ptui + " 高度: " + h + " 放缩: " + Scale );
		
		UIFont = new Font( "微软雅黑", ptui, FontStyle.Regular );
		ExpFont = new Font( "微软雅黑", pt, FontStyle.Regular );
		ExpFontBack = ExpFont;
		
		DebugFont = new Font( "微软雅黑", 35, FontStyle.Bold );
		SimFont = new Font( "微软雅黑", 15, FontStyle.Regular );
		Font13 = new Font( "微软雅黑", 13, FontStyle.Regular );
		Font20 = new Font( "微软雅黑", 20, FontStyle.Regular );
		
		
		WordPen = new Pen( Color.Blue, 1 );
		WordBrush = new SolidBrush( Color.FromArgb( 210, 210, 240 ) );
		
		MidFormat = new StringFormat();
		MidFormat.Alignment = StringAlignment.Center;
		MidFormat.LineAlignment = StringAlignment.Center;
		
		LeftFormat = new StringFormat();
		LeftFormat.Alignment = StringAlignment.Near;
		LeftFormat.LineAlignment = StringAlignment.Center;
		
		RightFormat = new StringFormat();
		RightFormat.Alignment = StringAlignment.Far;
		RightFormat.LineAlignment = StringAlignment.Center;
		
		if( SystemData.isBlack ) {
			组件高亮边框_Pen1 = new Pen( Color.White, 1 );
		}
		else {
			组件高亮边框_Pen1 = new Pen( Color.LightSlateGray, 1 );
		}
		组件高亮边框_Pen2 = new Pen( Color.OrangeRed, 3 );
		
		HideBackBrush = new SolidBrush(
			Color.FromArgb(
				210,
			    GUIset.myBackColor.R,
			    GUIset.myBackColor.G,
			    GUIset.myBackColor.B
			    ) );
		
		System.Drawing.Drawing2D.AdjustableArrowCap AC = new AdjustableArrowCap( 3, 2, true );
		
		if( SystemData.isBlack ) {
			RF_Pen = new Pen( Color.FromArgb( 200, 0, 100, 255 ), 10 );
			RF_Pen.CustomEndCap = AC;
			IR_Pen = new Pen( Color.FromArgb( 200, 255, 100, 0 ), 10 );
			IR_Pen.CustomEndCap = AC;
			NOR_Pen = new Pen( Color.FromArgb( 150, 200, 200, 200 ), 7 );
		}
		else {
			RF_Pen = new Pen( Color.FromArgb( 150, 0, 100, 200 ), 10 );
			RF_Pen.CustomEndCap = AC;
			IR_Pen = new Pen( Color.FromArgb( 150, 200, 100, 0 ), 10 );
			IR_Pen.CustomEndCap = AC;
			NOR_Pen = new Pen( Color.FromArgb( 200, 120, 140, 160 ), 5 );
		}
		NOR_Pen.DashStyle = System.Drawing.Drawing2D.DashStyle.Dot;
		NOR_Pen.CustomEndCap = AC;
		
		//公共初始化
		n_ModuleLibPanel.ModuleTree.CWidth = GetPix( n_ModuleLibPanel.ModuleTree.CWidth );
		n_Head.Head.HeadWidth = n_ModuleLibPanel.ModuleTree.CWidth;
		
		n_Head.Head.HeadLabelHeight = GetPix( n_Head.Head.HeadLabelHeight );
		n_Head.R_HeadLabel.Height = n_Head.Head.HeadLabelHeight;
		
		n_Head.Head.PAD = GetPix( n_Head.Head.PAD );
		n_Head.Head.CY = GetPix( n_Head.Head.CY );
		n_Head.Head.HeadHeight = GetPix( n_Head.Head.HeadHeight );
		
		n_EPanel.EPanel.ModWidth = GetPix( n_EPanel.EPanel.ModWidth );
		n_EPanel.EPanel.ModHeight = GetPix( n_EPanel.EPanel.ModHeight );
		n_EPanel.EPanel.HeadHeight = GetPix( n_EPanel.EPanel.HeadHeight );
		
		n_HardModule.HardModule.MLIB_SIZE = GetPix( n_HardModule.HardModule.MLIB_SIZE );
		n_ModuleLibPanel.ModuleTree.XPadding = GetPix( n_ModuleLibPanel.ModuleTree.XPadding );
		n_ModuleLibPanel.ModuleTree.YPadding = GetPix( n_ModuleLibPanel.ModuleTree.YPadding );
	}
	
	//获取变换距离
	public static int GetPix( int w )
	{
		return (int)(w * Scale);
	}
	
	public static int GetPixTTT( int w )
	{
		return w;
		//return (int)(w * Scale);
	}
	
	//获取系统字体的尺寸
	public static float GetStringWidth( string mes )
	{
		return mg.MeasureString( mes, ExpFont ).Width;
	}
	
	//获取默认颜色
	public static Color GetBlueColor( int a )
	{
		//return Color.FromArgb( a*4/9, a*2/3, a );
		
		return Color.FromArgb( a*60/100, a*63/100, a*66/100 );
		
		//return Color.FromArgb( a*75/100, a*80/100, a*85/100 );
	}
	
	//获取默认颜色
	public static Color GetBlueColor1( int a )
	{
		//return Color.FromArgb( a*4/9, a*2/3, a );
		
		return Color.FromArgb( a*60/100, a*70/100, a*80/100 );
		
		//return Color.FromArgb( a*75/100, a*80/100, a*85/100 );
	}
	
	//获取默认颜色
	public static Color GetBlueColor2( int a )
	{
		//return Color.FromArgb( a*4/9, a*2/3, a );
		
		return Color.FromArgb( a*170/255, a*185/255, a*200/255 );
		
		//return Color.FromArgb( a, a, a );
	}
	
	//获取默认颜色
	public static Color GetRedColor( int a )
	{
		return Color.FromArgb( a, a*2/3, a*4/9 );
	}
}
}

