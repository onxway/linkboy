﻿

//命名空间
namespace n_GUIcoder
{
using System;
using System.Windows.Forms;
using System.Drawing;
using System.Drawing.Drawing2D;
using n_UserModule;
using n_OS;


//==================================================
public class ResourceList
{
	static string[][] StaticResList;
	string[][] ResList;
	
	
	
	//模块配置器中, 占用的资源之间用逗号隔开
	public const string AVR = "AVR"; //这是一个特殊标记, 表明模块只能用于AVR系列
	public const string VOS = "VOS"; //这是一个特殊标记, 表明模块只能用于VOS系列
	
	public const string EA = "EA";
	public const string TIMER0 = "TIMER0";
	
	
	
	public int Number;
	
	public string ExistResUser;
	public string ExistRes;
	
	public string ExistEAUser;
	
	//初始化
	public static void Init()
	{
		//加载可用资源列表
		string FileName = OS.SystemRoot + "Lib" + OS.PATH_S + "resource" + OS.PATH_S + "ATMEGA.lst";
		string text = VIO.OpenTextFileGB2312( FileName );
		string s = text.Remove( text.IndexOf( "\n<end>" ) );
		string[] List = s.Split( '\n' );
		StaticResList = new string[ List.Length ][];
		for( int i = 0; i < List.Length; ++i ) {
			StaticResList[ i ] = new string[ 2 ];
			StaticResList[ i ][ 0 ] = List[ i ].TrimEnd( ',' );
		}
	}
	
	//构造函数
	public ResourceList()
	{
		ResList = new string[ StaticResList.Length ][];
		for( int i = 0; i < ResList.Length; ++i ) {
			ResList[ i ] = new string[ 2 ];
			ResList[ i ][ 0 ] = StaticResList[ i ][ 0 ];
		}
		Number = ResList.Length;
	}
	
	//清除占用资源信息
	public void Clear()
	{
		for( int i = 0; i < ResList.Length; ++i ) {
			ResList[ i ][ 1 ] = null;
		}
		ExistResUser = null;
		ExistRes = null;
		ExistEAUser = null;
	}
	
	//查找一个资源名称,没找到则返回 -1
	public int GetIndex( string Name )
	{
		for( int i = 0; i < ResList.Length; ++i ) {
			if( ResList[ i ][ 0 ] == Name ) {
				return i;
			}
		}
		return -1;
	}
	
	//获取一个指定索引的资源名
	public string GetName( int Index )
	{
		return ResList[ Index ][ 0 ];
	}
	
	//获取一个指定索引的资源名被哪个组件占用
	public string GetUser( int Index )
	{
		return ResList[ Index ][ 1 ];
	}
	
	//设置某个资源的用户
	public void SetUser( int Index, string UserName, string ImageName )
	{
		ResList[ Index ][ 1 ] = UserName;
		
		if( ResList[ Index ][ 0 ] != TIMER0 && ResList[ Index ][ 0 ] != EA && ImageName == "NOT_EA" ) {
			ExistResUser = UserName;
			ExistRes = ResList[ Index ][ 0 ];
		}
		if( ResList[ Index ][ 0 ] == EA ) {
			ExistEAUser = UserName;
		}
	}
}
//==================================================
//组件提示信息类
public class ModuleMessage
{
	public string Language;
	public string InnerName;
	public string UserName;
	public string ModuleMes;
	public MemberMes[] MemberMesList;
	
	//查找指定的成员信息对象
	public MemberMes GetMemberMessageFromName( string Name )
	{
		for( int i = 0; i < MemberMesList.Length; ++i ) {
			if( MemberMesList[ i ].InnerName == Name ) {
				return MemberMesList[ i ];
			}
		}
		return null;
	}
}
public static class GType
{
	public const string g_int8 = "int8";
	public const string g_int16 = "int16";
	public const string g_int32 = "int32";
	public const string g_uint8 = "uint8";
	public const string g_uint16 = "uint16";
	public const string g_uint32 = "uint32";
	public const string g_fix = "fix";
	public const string g_bool = "bool";
	public const string g_bit = "bit";
	public const string g_Achar = "Achar";
	public const string g_Astring = "Astring";
	public const string g_Cstring = "Cstring";
	public const string g_Rstring = "Rstring";
	public const string g_bitmap = "bitmap";
	public const string g_font = "font";
	public const string g_cbitmap = "cbitmap";
	public const string g_music = "music";
	public const string g_color = "color";
	public const string g_module = "module";
	
	public const string c_array = "[#.code uint8*?]";
	
	//常量时间类 (临时, 最终应和Time合并, 转化代码时把字面的字符串型时间常量转换为常量数组, 这样就能兼容 Time 了)
	public const string g_time = "time";
	public const string g_Time = "Time";
	
	public const string g_protocol = "protocol";
}
//组件成员提示信息类
public class MemberMes
{
	public string InnerName;
	public string UserName;
	public string Type;
	public string Mes;
}
//基本端口类
public class BasePort
{
	public string Name;
	public string FuncType;
	public string FuncName;
	public string Style;
	public string SubPortConfig;
	public E_DirType DirType;
	public float X, Y;
	public string ExtValue;
	
	public BasePort( string vName, string vFuncType, string vFuncName, string vStyle, string vSubPortConfig, E_DirType vDirType, float vX, float vY, string vExtValue )
	{
		Name = vName;
		FuncType = vFuncType;
		FuncName = vFuncName;
		Style = vStyle;
		SubPortConfig = vSubPortConfig;
		DirType = vDirType;
		X = vX;
		Y = vY;
		ExtValue = vExtValue;
	}
}
public static class ExtendType
{
	public const string Channel = "OS_channel";
}
public static class InterfaceType
{
	public const string Split = "split";
	
	public const string Event = "event";
	public const string Struct = "struct";
	public const string Vtype = "memory";
	public const string Unit = "unit";
	public const string LinkUnit = "linkunit";
	
	public const string Function_ = "function_";
	public const string Var_ = "var_";
	public const string Const_ = "const_";
	
	public const string LinkVar_ = "linkvar_";
	public const string LinkConst_ = "linkconst_";
	public const string LinkSysVar_ = "linksysvar_";
	public const string LinkSysConst_ = "linksysconst_";
	
	public const string Interface_ = "interface_";
	public const string LinkInterface_ = "linkinterface_";
}
public static class FuncExtType
{
	public const string Old = "old";
	public const string Debug = "debug";
}
public static class AIsuaUseName
{
	public const string OS_init = "OS_init";
	public const string OS_thread = "OS_thread";
	public const string OS_run = "OS_run";
	public const string OS_run100us = "OS_run100us";
	public const string OS_EventFlag = "OS_EventFlag";
}
public static class SimulateType
{
	public const int IgnoreLib = 0;
	public const int UseLib = 1;
	public const int SurportLib = 2;
}
public static class PortClientType
{
	public const string CLIENT = "CLIENT";
	public const string MASTER = "MASTER";
}
public static class PortStyle
{
	public const string IO6 = "IO6";
	public const string IO3 = "IO3";
}
public static class PortFuncName
{
	public const string VCC = ",VCC,";
	public const string VCC3_3V = ",VCC3.3V,";
	public const string VCC6t9V = ",VCC6V-9V,";
	public const string VCCA = ",VCCA,"; //可能是根据主板电源自动跟随电源, 如AD转换类
	public const string GND = ",GND,";
	public const string AD = ",AD,";
	public const string RxD = ",RxD,";
	public const string TxD = ",TxD,";
	public const string RxDn = ",RxD";
	public const string TxDn = ",TxD";
	public const string INT = ",INT,";
	public const string IN = ",IN,";
	public const string OUT = ",OUT,";
	public const string INTERFACE = "INTERFACE";
	public const string PULL = ",PULL,";
	public const string LINE = ",LINE,";
	public const string UNUSED = ",UNUSED,";
	public const string RESET = ",RESET,";
	public const string AREF = ",AREF,";
	public const string UART = ",UART,";
	
	public const string ZX = ",ZX,";
	
	public const string C_Analog = ",C-Analog,";
	public const string C_Digital = ",C-Digital,";
}
public static class ZWireFuncName
{
	public const string POWER = ",POWER,";
	public const string MOTOR = ",MOTOR,";
	public const string SERVO = ",SERVO,";
	
	public const string IO3 = ",IO3,";
}
public static class SPMoudleName
{
	public const string mbb = "mbb";
	public const string SYS_NULL = "SYS_NULL";
	public const string SYS_iport = "SYS_iport";		//
	public const string SYS_PADSHAP = "SYS_PADSHAP";	//板子外形模块
	
	public const string SYS_NOHIT = "SYS_NOHIT";	//不参与碰撞的模块, 可以实现堆叠
	
	public const string SYS_View = "SYS_View";		//摄像头类
	public const string SYS_Sprite = "SYS_Sprite";		//动画精灵类
	public const string SYS_Keyboard = "SYS_Keyboard";		//键盘类
	public const string SYS_G_BackColor = "SYS_G_BackColor";
	public const string SYS_G_BackImage = "SYS_G_BackImage";
	public const string SYS_G_Map = "SYS_G_Map";
	
	public const string SYS_Arduino = "SYS_Arduino";
	public const string SYS_LAPI = "SYS_LAPI";
	public const string SYS_delayer = "SYS_delayer";
	
	public const string SYS_PinInput = "SYS_PinInput";		//输入引脚
	public const string SYS_PinOutput = "SYS_PinOutput";	//输出引脚
}
public static class PackHidType
{
	//模块是否隐藏
	public const string hid = "hid";
	public const string nohid = "nohid";
	
	//--以下都不设置则主板端口可见, 不可连接; 设置第一个则可见, 可连接; 设置第二个则不可见, 不可连接;
	
	//模块设置为开放主板端口
	public const string SYS_OPEN_MPORT = "SYS_OPEN_MPORT";
	//模块设置为隐藏主板端口
	public const string SYS_HIDE_MPORT = "SYS_HIDE_MPORT";
}
public static class labType
{
	public const char Default = '0';	//未用的类型
	public const char Code = '1';		//开源硬件实验室
	public const char Circuit = '2';	//电路仿真实验室
	public const char Game = '3';		//动画游戏实验室
}
//主板类型
public static class BOARD_Type
{
	public const string EXPORT = "EXPORT";
	public const string UNO = "UNO";
	public const string NANO = "NANO";
	public const string VOS = "Nucleo";
}
public static class CPU_Type
{
	public const string AVR = "AVR";
	public const string VM = "VM";	//VOS
	
	public const string MEGA328 = "MEGA328";
	public const string FPGA = "FPGA";
}
public static class PathFlag
{
	public const string Default = "";
	public const string SYS = "$";
	public const string Global = "@";
}
public static class NoteFlag
{
	public const string StartMes = "<start-mes>";
}
public static class ModuleFlag
{
	public const string ModuleLib = n_Config.Config.PathPre_ModuleLib;
	public const string UserPath = n_Config.Config.PathPre_CurrentDir;
}
}



