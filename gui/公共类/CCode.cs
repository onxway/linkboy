﻿
namespace n_CCode
{
using System;
using System.Windows.Forms;
using i_Compiler;
using n_GUIcoder;
using n_LaterFilesManager;
using n_OS;

//高级文本类
public class CCode
{
	public delegate void D_FileMesChanged( string fn );
	public D_FileMesChanged FileMesChanged;
	
	public delegate void D_SetUserCode( string fn );
	public static D_SetUserCode SetUserCode;
	
	public static string Filter;
	
	public static string GetLinkboyFileName()
	{
		return GUIcoder.GetUserFileName( 0, G.CGPanel.myModuleList ) + n_MyFile.MyFile.GetDateTimeNow();
	}
	
	//构造函数
	public CCode( string tempPathAndName )
	{
		if( G.isCruxEXE ) {
			Filter = "crux源程序" + n_Language.Language.CC_ProFile + OS.CruxFile + ")|*." + OS.CruxFile + n_Language.Language.CC_OtherFile;
		}
		else {
			Filter = OS.USER + n_Language.Language.CC_ProFile + OS.LinkboyFile + ")|*." + OS.LinkboyFile + n_Language.Language.CC_OtherFile;
		}
		
		PathAndName = tempPathAndName;
		
		//解析文件路径/文件名/文件扩展名
		GetFileNameAndPath();
		
		if( n_StartFile.StartFile.isStartFile( this.PathAndName ) ) {
			isStartFile = true;
		}
		else {
			isStartFile = false;
		}
		
		SysFile = false;
		if( this.PathAndName == OS.SystemRoot + @"Lib\dfl\zkb\zkb.lab" ) {
			isStartFile = true;
		}
		if( this.PathAndName.StartsWith( OS.SystemRoot + "example" + OS.PATH_S ) ||
		    this.PathAndName.StartsWith( OS.ModuleLibPath ) ||
		    this.PathAndName.StartsWith( OS.SystemRoot + "arduino-ext" + OS.PATH_S ) ) {
			if( this.FileName == "0" ) {
				isStartFile = true;
			}
		}
		
		Text = Compiler.OpenProgFile( PathAndName );
		
		//图形界面代码分割
		GUItext = null;
		UserText = "";
		GUIcoder.CodeSplit( this.Text, ref UserText, ref GUItext );
		
		//判断是否需要添加 Arduino 兼容子系统
		/*
		if( GUItext != null &&
		    
		    GUItext.IndexOf( "//[界面类型] 2," ) == -1 &&
		    GUItext.IndexOf( "//[界面类型] 3," ) == -1 &&
		    
			//(GUItext.IndexOf( "//[界面类型] 1," ) != -1 ||
			//n_ModuleLibPanel.MyModuleLibPanel.LabType == n_GUIcoder.labType.Default ||
			//n_ModuleLibPanel.MyModuleLibPanel.LabType == n_GUIcoder.labType.Code) &&
		    
		    GUItext.IndexOf( "Arduino\\Arduino-base\\Pack.B" ) == -1 ) {
			string addt = "";
			addt += "//[组件],\n";
			addt += "//[组件路径] ModuleLib Arduino\\Arduino-base\\Pack.B,\n";
			addt += "//[名称] Arduino,\n";
			addt += "//[语言] chinese,\n";
			addt += "//[组件结束],\n";
			
			GUItext = GUItext.Replace( "//[配置信息结束],", addt + "//[配置信息结束]," );
		}
		*/
		
		if( G.CodeViewBox != null ) {
			G.CodeViewBox.SetUserText( UserText );
		}
		if( SetUserCode != null ) {
			SetUserCode( UserText );
		}
		
		
		//变量初始化
		isChanged = false;
		NotRefresh = false;
		isNew = true;
		isSystemFile = false;
		
		if( FileMesChanged != null ) {
			FileMesChanged( GetFileMes() );
		}
	}
	
	//设置程序代码
	public void SetCode( string code )
	{
		this.Text = code;
		isChanged = true;
		
		if( FileMesChanged != null ) {
			FileMesChanged( GetFileMes() );
		}
	}
	
	//合并设置程序代码
	public void SetCode( string c1, string c2 )
	{
		if( !c1.EndsWith( "\n" ) ) {
			c1 += "\n";
		}
		SetCode( c1 + c2 );
	}
	
	//判断是否存在合法的python代码
	public bool b_ExistPython;
	public bool ExistPython()
	{
		b_ExistPython = UserText.Trim( "\t \n".ToCharArray() ) != "";
		return b_ExistPython;
	}
	
	//获取程序代码
	public string GetCode()
	{
		return Text;
	}
	
	//获取仿真程序代码
	public string GetSimCode()
	{
		return SimText;
	}
	
	//获取文件路径信息
	public string GetFileMes()
	{
		if( G.ccode.isStartFile ) {
			if( !G.ccode.isChanged ) {
				return "^_^ 欢迎使用 linkboy 图形化编程电子积木";
			}
			return n_Language.Language.NotSave;
		}
		return G.ccode.FileName + (G.ccode.isChanged? "*" : " ") + "(" + G.ccode.PathAndName + ")";
	}
	
	//保存文件操作触发的事件
	public EventHandler Saved;
	static bool FirstOE = true;
	
	//保存文件
	public bool Save()
	{
		//判断是否为系统默认启动文件
		if( isStartFile ) {
			//保存文件对话框
			SaveFileDialog SaveFileDlg = new SaveFileDialog();
			SaveFileDlg.Filter = Filter;
			SaveFileDlg.Title = n_Language.Language.CC_SaveFile;
			
			SaveFileDlg.FileName = GetLinkboyFileName();
			DialogResult dlgResult = SaveFileDlg.ShowDialog();
			if(dlgResult == DialogResult.OK) {
				string FilePath = SaveFileDlg.FileName;
				if( !G.isCruxEXE && !FilePath.ToLower().EndsWith( "." + OS.LinkboyFile ) ) {
					FilePath += "." + OS.LinkboyFile;
				}
				else if( G.isCruxEXE && !FilePath.ToLower().EndsWith( "." + OS.CruxFile ) ) {
					FilePath += "." + OS.CruxFile;
				}
				else {
					//...
				}
				this.PathAndName = FilePath;
				isStartFile = false;
				GetFileNameAndPath();
				LaterFilesManager.AddFile( FilePath );
				SysFile = false;
			}
			else {
				n_Backup.Backup.SaveText();
				return false;
			}
		}
		
		if( SysFile ) {
			MessageBox.Show( n_Language.Language.CC_SaveAs, n_Language.Language.CC_SaveAsHead );
		}
		
		//判断是否为第三方编辑模式
		if( G.OtherEditor ) {
			if( FirstOE ) {
				MessageBox.Show( "第三方编辑器模式" );
				FirstOE = false;
			}
			
			Text = Compiler.OpenProgFile( PathAndName );
			
			//图形界面代码分割
			GUItext = null;
			UserText = "";
			GUIcoder.CodeSplit( this.Text, ref UserText, ref GUItext );
			
			if( G.CodeViewBox != null ) {
				G.CodeViewBox.SetUserText( UserText );
			}
			if( SetUserCode != null ) {
				SetUserCode( UserText );
			}
			if( Saved != null ) {
				Saved( this, EventArgs.Empty );
			}
			isChanged = false;
			return true;
		}
		
		if( Saved != null ) {
			Saved( this, EventArgs.Empty );
		}
		isChanged = false;
		
		try {
			VIO.SaveTextFileGB2312( PathAndName, Text );
		}
		catch {
			MessageBox.Show( "文件保存失败, 请确认此文件属性是否有写保护: " + PathAndName );
		}
		
		if( FileMesChanged != null ) {
			FileMesChanged( GetFileMes() );
		}
		n_Backup.Backup.CopyFile();
		return true;
	}
	
	//保存给定文本到指定文件名
	//保存文件
	public void SaveSim( string s )
	{
		VIO.SaveTextFileGB2312( PathAndName + ".txt", s );
	}
	
	//重新获取代码
	public string GetNewCode()
	{
		if( Saved != null ) {
			Saved( this, EventArgs.Empty );
		}
		SimText = Text;
		return Text;
	}
	
	//解析文件名
	public void GetFileNameAndPath()
	{
		FilePath = PathAndName.Remove( PathAndName.LastIndexOf( OS.PATH_S ) + 1 );
		FileName = PathAndName.Remove( 0, PathAndName.LastIndexOf( OS.PATH_S ) + 1 );
		
		int dotindex = FileName.LastIndexOf( '.' );
		if( dotindex != -1 ) {
			FileType = FileName.Remove( 0, dotindex + 1 ).ToLower();
			FileName = FileName.Remove( dotindex );
		}
		else {
			FileType = "";
		}
	}
	
	public string Text;
	string SimText;
	
	public bool isStartFile;	//是否为系统启动文件
	public bool SysFile;		//是否为系统文件
	public string PathAndName;	//文件完整目录,包括文件名和扩展名
	public string FilePath;	//文件目录,包含最后的"\", 不包含文件名
	public string FileName;	//文件名,不包含路径和扩展名
	public string FileType;	//文件类型,小写字母
	
	public string GUItext;
	
	public string UserText;
	
	public bool v_isChanged;	//是否修改
	public bool isChanged {
		set {
			v_isChanged = value;
			if( FileMesChanged != null ) {
				FileMesChanged( GetFileMes() );
			}
		}
		get {
			return v_isChanged;
		}
	}
	
	public bool isNew;		//新建文件
	public bool NotRefresh;	//禁止刷新
	public bool isSystemFile;  //是否系统文件
}
}
