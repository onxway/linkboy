﻿
//命名空间
namespace c_MyObjectSet
{
using System;
using System.Drawing;
using System.Drawing.Drawing2D;

public static class Common
{
	//根据文本描述返回字体
	public static Font GetFontFromString( string Font )
	{
		string[] FontList = Font.Split( ',' );
		string FontName = FontList[ 0 ].Trim( ' ' );
		float FontSize = float.Parse( FontList[ 1 ].Trim( ' ' ) );
		if( FontList.Length > 3 ) {
			return null;
		}
		FontStyle FS = (FontStyle)0;
		if( FontList.Length == 3 ) {
			string BIUS = FontList[ 2 ].Trim( ' ' );
			
			if( BIUS.IndexOf( "B" ) != -1 ) {
				FS = FontStyle.Bold;
			}
			if( BIUS.IndexOf( "I" ) != -1 ) {
				FS |= FontStyle.Italic;
			}
			if( BIUS.IndexOf( "U" ) != -1 ) {
				FS |= FontStyle.Underline;
			}
			if( BIUS.IndexOf( "S" ) != -1 ) {
				FS |= FontStyle.Strikeout;
			}
		}
		Font f = new Font( FontName, FontSize, FS );
		return f;
	}
	
	//返回一个字体的文字描述, 依次是: Name, Size, BIUS
	public static string GetStringFromFont( Font TextFont )
	{
		string BIUS = null;
		if( TextFont.Bold ) {
			BIUS += "B";
		}
		if( TextFont.Italic ) {
			BIUS += "I";
		}
		if( TextFont.Underline ) {
			BIUS += "U";
		}
		if( TextFont.Strikeout ) {
			BIUS += "S";
		}
		if( BIUS != null ) {
			BIUS = "," + BIUS;
		}
		return TextFont.Name + "," + TextFont.Size + BIUS;
	}
}
}

