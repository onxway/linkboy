﻿/*
 * 由SharpDevelop创建。
 * 用户： dell
 * 日期: 2015/12/23
 * 时间: 17:06
 * 
 * 要改变这种模板请点击 工具|选项|代码编写|编辑标准头文件
 */
using System;

namespace n_CPU
{
public static class CPU
{
	static int[] IO;
	
	//------------------------------------------------------------
	//访问接口
	
	//初始化
	public static void Init()
	{
		IO = new int[1024];
	}
	
	//初始化
	public static void Reset()
	{
		for( int i = 0; i < IO.Length; ++i ) {
			IO[i] = 0;
		}
	}
	
	public static void ChannelInit()
	{
		
	}
	
	public static void ChannelWrite( int V1, int V2 )
	{
		IO[V1] = V2;
	}
	
	public static int ChannelRead( int V1 )
	{
		return IO[V1];
	}
	
	//------------------------------------------------------------
	//寄存器接口
	
	public static int GetOut( int addr )
	{
		return IO[0x0100 + addr];
	}
	
	public static void SetIn( int addr, int data )
	{
		IO[0x0200 + addr] = data;
	}
}
}




