﻿

namespace n_GUICommon
{
using System;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Windows.Forms;

using n_Compiler;
using n_GUIset;
using n_Head_MtButton;
using n_ImagePanel;
using n_ISP;
using n_MyObjectList;
using n_OS;
using n_Shape;
using n_GNote;
using n_HardModule;
using n_GUIcoder;
using n_MyFileObject;
using n_LoopIns;
using n_UserFunctionIns;

static class GUICommon
{
	//-----------------------------
	
	//基于已有名字搜索一个未用的名字
	public static string SearchName( ImagePanel TargetGPanel, string BaseName )
	{
		string NewName = BaseName;
		int i = 1;
		while( true ) {
			if( !TargetGPanel.myModuleList.NameisUsed( NewName ) ) {
				break;
			}
			NewName = BaseName + i;
			++i;
		}
		return NewName;
	}
	
	//搜索一个未用的名字
	public static string SearchName( ImagePanel TargetGPanel )
	{
		//搜索一个未用的名字
		string NewName = null;
		int i = 1;
		while( true ) {
			NewName = "@" + i;
			++i;
			if( !TargetGPanel.myModuleList.NameisUsed( NewName ) ) {
				break;
			}
		}
		return NewName;
	}
}
}
