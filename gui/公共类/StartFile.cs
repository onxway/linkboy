﻿
using System;
using n_OS;

namespace n_StartFile
{
public static class StartFile
{
	public static string CodeFile;
	public static string CircuitFile;
	public static string GameFile;
	
	//初始化
	public static void Init()
	{
		CodeFile = OS.SystemRoot + "RV" + OS.PATH_S + OS.USER + OS.PATH_S + "startfile" + "." + OS.LinkboyFile;
		
		CircuitFile = OS.SystemRoot + "RV" + OS.PATH_S + "circuit" + OS.PATH_S + "startfile" + "." + OS.LinkboyFile;
		GameFile = OS.SystemRoot + "RV" + OS.PATH_S + "game" + OS.PATH_S + "startfile" + "." + OS.LinkboyFile;
	}
	
	//判断一个文件是否为启动文件
	public static bool isStartFile( string f )
	{
		if( f.StartsWith( OS.SystemRoot + "RV" + OS.PATH_S ) ) {
			return true;
		}
		else {
			return false;
		}
	}
}
}

