﻿

using System;
using System.Drawing;
using System.Drawing.Drawing2D;

namespace n_ALG
{
public static class ALG
{
	//队列
	public class Queue
	{
		int[] DataList;
		int ReadIdx;
		int WriteIdx;
		int MaxNum;
		int anum;	//有效数据数目
		
		//构造函数
		public Queue( int num )
		{
			MaxNum = num;
			DataList = new int[MaxNum];
			Reset();
		}
		
		//复位
		public void Reset()
		{
			ReadIdx = 0;
			WriteIdx = 0;
			anum = 0;
		}
		
		//添加一个数据
		public bool WriteOk( int data )
		{
			if( anum < MaxNum ) {
				anum++;
				DataList[WriteIdx] = data;
				WriteIdx++;
				WriteIdx %= MaxNum;
				return true;
			}
			return false;
		}
		
		//读取一个数据
		public bool ReadOk( ref int data )
		{
			if( anum > 0 ) {
				anum--;
				data = DataList[ReadIdx];
				ReadIdx++;
				ReadIdx %= MaxNum;
				return true;
			}
			return false;
		}
	}
}
}





