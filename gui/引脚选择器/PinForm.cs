﻿
namespace n_PinForm
{
using System;
using System.Windows.Forms;
using System.IO;
using System.Drawing;

using n_VarType;
using n_UnitList;
using n_FunctionList;
using n_VarList;
using n_StructList;
using n_ImagePanel;
using n_MyObjectList;
using n_MyObject;
using n_HardModule;
using n_GVar;
using n_GUIcoder;
using n_MyFileObject;
using c_FormMover;
using n_EXP;
using n_EXPcommon;

/// <summary>
/// Description of WorkForm.
/// </summary>
public partial class PinForm : Form
{
	bool isOK;
	
	string KeyList;
	Button LastButton;
	
	//FormMover fm;
	
	//主窗口
	public PinForm()
	{
		InitializeComponent();
		isOK = false;
		
		//fm = new FormMover( this );
	}
	
	//运行
	public string Run( string s )
	{
		if( s == null || s == "" ) {
			s = "";
			//this.checkBox按键阻塞.Checked = false;
		}
		KeyList = "";
		LastButton = null;
		
		foreach( Control c in this.panel1.Controls ) {
			if( c is Button && s == c.Text ) {
				c.BackColor = Color.CornflowerBlue;
				LastButton = (Button)c;
			}
			else {
				c.BackColor = Color.Gainsboro;
			}
		}
		
		//注意,直接设置Visible不能使窗体获得焦点
		//this.Visible = true;
		this.ShowDialog();
		
		while( this.Visible ) {
			System.Windows.Forms.Application.DoEvents();
		}
		if( isOK ) {
			return KeyList;
			
		}
		return null;
	}
	
	void Button确定Click(object sender, EventArgs e)
	{
		if( LastButton == null ) {
			MessageBox.Show( "请选中一个绑定针脚" );
			return;
		}
		KeyList = LastButton.Text;
		
		isOK = true;
		this.Visible = false;
	}
	
	void ButtonKClick(object sender, EventArgs e)
	{
		if( LastButton != null ) {
			LastButton.BackColor = Color.Gainsboro;
		}
		LastButton = (Button)sender;
		LastButton.BackColor = Color.CornflowerBlue;
	}
	
	void PinFormFormClosing(object sender, FormClosingEventArgs e)
	{
		e.Cancel = true;
		this.Visible = false;
	}
}
}




