﻿
namespace n_SpriteSetForm
{
using System;
using System.Windows.Forms;
using System.IO;
using System.Drawing;

using n_VarType;
using n_UnitList;
using n_FunctionList;
using n_VarList;
using n_StructList;
using n_ImagePanel;
using n_MyObjectList;
using n_MyObject;
using n_HardModule;
using n_GVar;
using n_GUIcoder;
using n_MyFileObject;
using c_FormMover;
using n_EXP;
using n_EXPcommon;
using n_SG_MySprite;
using n_ValueSet;
using n_SpriteEditPanel;

/// <summary>
/// Description of WorkForm.
/// </summary>
public partial class SpriteSetForm : Form
{
	bool isOK;
	FormMover fm;
	
	MySprite myOwner;
	
	SpriteEditPanel mySpriteEditPanel;
	
	//主窗口
	public SpriteSetForm()
	{
		InitializeComponent();
		
		fm = new FormMover( this );
		
		mySpriteEditPanel = new SpriteEditPanel( 200, 200 );
		mySpriteEditPanel.Location = new Point( 10, 10 );
		this.panel1.Controls.Add( mySpriteEditPanel );
		
		this.MouseWheel += UserMouseWheel;
	}
	
	//运行
	public string Run( MySprite o, string s )
	{
		if( s == null ) {
			s = "";
		}
		myOwner = o;
		
		ValueSet.SetValue( s );
		
		this.checkBoxHitStop.Checked = false;
		if( ValueSet.GetValue( "Bounce" ) == "0" ) {
			this.checkBoxHitStop.Checked = true;
		}
		int SelectX = myOwner.DL_Width/2;
		int SelectY = myOwner.DL_Height/2;
		if( ValueSet.GetValue( "RolX" ) != null ) {
			SelectX = (int)(myOwner.DL_Width * double.Parse( ValueSet.GetValue( "RolX" ) ) );
		}
		if( ValueSet.GetValue( "RolY" ) != null ) {
			SelectY = (int)(myOwner.DL_Height * double.Parse( ValueSet.GetValue( "RolY" ) ) );
		}
		if( ValueSet.GetValue( "FowardAngle" ) != null ) {
			this.textBoxFowardAngle.Text = (int.Parse( ValueSet.GetValue( "FowardAngle" ) ) ).ToString();
		}
		this.listBox1.Items.Clear();
		for( int i = 0; ValueSet.FileList != null && i < ValueSet.FileList.Length; ++i ) {
			this.listBox1.Items.Add( ValueSet.FileList[i] );
		}
		//打开图片
		mySpriteEditPanel.SetImage( null, myOwner.DL_Width, myOwner.DL_Height, SelectX, SelectY );
		if( ValueSet.FileList != null && ValueSet.FileList.Length > 0 ) {
			string fp = n_CommonData.CommonData.GetFullPath( ValueSet.FileList[0] );
			if( fp != null ) {
				Image ti = Image.FromFile( fp );
				mySpriteEditPanel.SetImage( new Bitmap( ti ), myOwner.DL_Width, myOwner.DL_Height, SelectX, SelectY );
				ti.Dispose();
			}
		}
		//注意,直接设置Visible不能使窗体获得焦点
		//this.Visible = true;
		this.ShowDialog();
		
		while( this.Visible ) {
			System.Windows.Forms.Application.DoEvents();
		}
		if( isOK ) {
			string r = "E=" +
				"Bounce:" + (this.checkBoxHitStop.Checked? "0" : "-1" )+ "," +
				"RolX:" + (double)mySpriteEditPanel.SelectX/myOwner.DL_Width + "," +
				"RolY:" + (double)mySpriteEditPanel.SelectY/myOwner.DL_Height + "," +
				"FowardAngle:" + this.textBoxFowardAngle.Text + ",";
				
			for( int j = 0; j < this.listBox1.Items.Count; ++j ) {
				r += "*" + this.listBox1.Items[j].ToString();
			}
			return r;
		}
		return null;
	}
	
	void UserMouseWheel( object sender, MouseEventArgs e )
	{
		mySpriteEditPanel.UserMouseWheel( e.X - panel1.Location.X, e.Y - panel1.Location.Y, e.Delta );
	}
	
	void Button确定Click(object sender, EventArgs e)
	{
		isOK = true;
		this.Visible = false;
	}
	
	void Button取消Click(object sender, EventArgs e)
	{
		isOK = false;
		this.Visible = false;
	}
	
	//添加图片
	void Button1Click(object sender, EventArgs e)
	{
		//打开文件对话框
		OpenFileDialog OpenFileDlg = new OpenFileDialog();
		OpenFileDlg.Filter = "所有类型文件 | *.*";
		OpenFileDlg.Title = "请选择文件";
		
		if( !G.ccode.isStartFile ) {
			OpenFileDlg.InitialDirectory = G.ccode.FilePath;
		}
		else {
			string Mes = "您的这个文件尚未保存到电脑硬盘上, 所以无法设置图片资源的默认文件夹,\n";
			Mes += "强烈建议您先保存一下这个文件到电脑上, 再选择图片路径, 这样方便系统记录您的默认文件夹.\n";
			Mes += "否则系统将会使用全局文件路径记录, 这样会导致您的vex程序复制到其他目录后运行异常.\n";
			MessageBox.Show( Mes );
		}
		
		DialogResult dlgResult = OpenFileDlg.ShowDialog();
		if(dlgResult == DialogResult.OK) {
			if( OpenFileDlg.FileName.StartsWith( G.ccode.FilePath ) ) {
				string FileName = OpenFileDlg.FileName.Remove( 0, G.ccode.FilePath.Length );
				this.listBox1.Items.Add( FileName );
				string fp = n_CommonData.CommonData.GetFullPath( FileName );
				if( fp != null ) {
					Image ti = Image.FromFile( fp );
					mySpriteEditPanel.SetImage( new Bitmap( ti ), myOwner.DL_Width, myOwner.DL_Height, mySpriteEditPanel.SelectX, mySpriteEditPanel.SelectY );
					ti.Dispose();
				}
			}
			else {
				
				MessageBox.Show( "图片文件需要放到您的程序文件同目录下, 目前暂不支持引用外部路径的图片" );
				return;
				
				//this.listBox1.Items.Add( OpenFileDlg.FileName );
			}
		}
	}
	
	void ButtonClearClick(object sender, EventArgs e)
	{
		this.listBox1.Items.Clear();
	}
	
	void TextBoxFowardAngleTextChanged(object sender, EventArgs e)
	{
		try {
			mySpriteEditPanel.FowardAngle = int.Parse( this.textBoxFowardAngle.Text );
			mySpriteEditPanel.t.Enabled = true;
		}
		catch {
			this.textBoxFowardAngle.Text = "0";
		}
	}
	
	void ButtonCenterClick(object sender, EventArgs e)
	{
		mySpriteEditPanel.SelectX = mySpriteEditPanel.BWidth / 2;
		mySpriteEditPanel.SelectY = mySpriteEditPanel.BHeight / 2;
		
		mySpriteEditPanel.t.Enabled = true;
	}
}
}

