﻿

namespace n_SpriteSetForm
{
	partial class SpriteSetForm
	{
		/// <summary>
		/// Designer variable used to keep track of non-visual components.
		/// </summary>
		private System.ComponentModel.IContainer components = null;
		
		/// <summary>
		/// Disposes resources used by the form.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing) {
				if (components != null) {
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}
		
		/// <summary>
		/// This method is required for Windows Forms designer support.
		/// Do not change the method contents inside the source code editor. The Forms designer might
		/// not be able to load this method if it was changed manually.
		/// </summary>
		private void InitializeComponent()
		{
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(SpriteSetForm));
			this.button确定 = new System.Windows.Forms.Button();
			this.button取消 = new System.Windows.Forms.Button();
			this.button1 = new System.Windows.Forms.Button();
			this.buttonClear = new System.Windows.Forms.Button();
			this.checkBoxHitStop = new System.Windows.Forms.CheckBox();
			this.listBox1 = new System.Windows.Forms.ListBox();
			this.panel1 = new System.Windows.Forms.Panel();
			this.label1 = new System.Windows.Forms.Label();
			this.textBoxFowardAngle = new System.Windows.Forms.TextBox();
			this.label2 = new System.Windows.Forms.Label();
			this.buttonCenter = new System.Windows.Forms.Button();
			this.SuspendLayout();
			// 
			// button确定
			// 
			this.button确定.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(192)))), ((int)(((byte)(255)))));
			resources.ApplyResources(this.button确定, "button确定");
			this.button确定.ForeColor = System.Drawing.Color.White;
			this.button确定.Name = "button确定";
			this.button确定.UseVisualStyleBackColor = false;
			this.button确定.Click += new System.EventHandler(this.Button确定Click);
			// 
			// button取消
			// 
			this.button取消.BackColor = System.Drawing.Color.Coral;
			resources.ApplyResources(this.button取消, "button取消");
			this.button取消.ForeColor = System.Drawing.Color.White;
			this.button取消.Name = "button取消";
			this.button取消.UseVisualStyleBackColor = false;
			this.button取消.Click += new System.EventHandler(this.Button取消Click);
			// 
			// button1
			// 
			this.button1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
			resources.ApplyResources(this.button1, "button1");
			this.button1.ForeColor = System.Drawing.Color.White;
			this.button1.Name = "button1";
			this.button1.UseVisualStyleBackColor = false;
			this.button1.Click += new System.EventHandler(this.Button1Click);
			// 
			// buttonClear
			// 
			this.buttonClear.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
			resources.ApplyResources(this.buttonClear, "buttonClear");
			this.buttonClear.ForeColor = System.Drawing.Color.White;
			this.buttonClear.Name = "buttonClear";
			this.buttonClear.UseVisualStyleBackColor = false;
			this.buttonClear.Click += new System.EventHandler(this.ButtonClearClick);
			// 
			// checkBoxHitStop
			// 
			resources.ApplyResources(this.checkBoxHitStop, "checkBoxHitStop");
			this.checkBoxHitStop.Name = "checkBoxHitStop";
			this.checkBoxHitStop.UseVisualStyleBackColor = true;
			// 
			// listBox1
			// 
			this.listBox1.FormattingEnabled = true;
			resources.ApplyResources(this.listBox1, "listBox1");
			this.listBox1.Name = "listBox1";
			// 
			// panel1
			// 
			resources.ApplyResources(this.panel1, "panel1");
			this.panel1.Name = "panel1";
			// 
			// label1
			// 
			resources.ApplyResources(this.label1, "label1");
			this.label1.Name = "label1";
			// 
			// textBoxFowardAngle
			// 
			resources.ApplyResources(this.textBoxFowardAngle, "textBoxFowardAngle");
			this.textBoxFowardAngle.Name = "textBoxFowardAngle";
			this.textBoxFowardAngle.TextChanged += new System.EventHandler(this.TextBoxFowardAngleTextChanged);
			// 
			// label2
			// 
			resources.ApplyResources(this.label2, "label2");
			this.label2.Name = "label2";
			// 
			// buttonCenter
			// 
			this.buttonCenter.BackColor = System.Drawing.Color.DarkSeaGreen;
			resources.ApplyResources(this.buttonCenter, "buttonCenter");
			this.buttonCenter.ForeColor = System.Drawing.Color.White;
			this.buttonCenter.Name = "buttonCenter";
			this.buttonCenter.UseVisualStyleBackColor = false;
			this.buttonCenter.Click += new System.EventHandler(this.ButtonCenterClick);
			// 
			// SpriteSetForm
			// 
			resources.ApplyResources(this, "$this");
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.BackColor = System.Drawing.Color.WhiteSmoke;
			this.Controls.Add(this.buttonCenter);
			this.Controls.Add(this.label2);
			this.Controls.Add(this.textBoxFowardAngle);
			this.Controls.Add(this.label1);
			this.Controls.Add(this.panel1);
			this.Controls.Add(this.listBox1);
			this.Controls.Add(this.checkBoxHitStop);
			this.Controls.Add(this.buttonClear);
			this.Controls.Add(this.button1);
			this.Controls.Add(this.button取消);
			this.Controls.Add(this.button确定);
			this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
			this.Name = "SpriteSetForm";
			this.ShowInTaskbar = false;
			this.TopMost = true;
			this.ResumeLayout(false);
			this.PerformLayout();
		}
		private System.Windows.Forms.Button buttonCenter;
		private System.Windows.Forms.Label label2;
		private System.Windows.Forms.TextBox textBoxFowardAngle;
		private System.Windows.Forms.Label label1;
		private System.Windows.Forms.ListBox listBox1;
		private System.Windows.Forms.CheckBox checkBoxHitStop;
		private System.Windows.Forms.Button buttonClear;
		private System.Windows.Forms.Button button取消;
		private System.Windows.Forms.Button button确定;
		private System.Windows.Forms.Button button1;
		private System.Windows.Forms.Panel panel1;
	}
}
