﻿
namespace n_SpriteEditPanel
{
using System;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Windows.Forms;

using n_OS;

//*****************************************************
//图形编辑器容器类
public class SpriteEditPanel : Panel
{
	public Timer t;
	SolidBrush BackBrush;
	
	Pen EageLine;
	
	bool LeftisPress;
	
	public Bitmap BackBitmap;
	
	public int BWidth;
	public int BHeight;
	
	int StartX;
	int StartY;
	
	int RightLastX;
	int RightLastY;
	bool RightisPress;
	
	float FlScale;
	int MouseX;
	int MouseY;
	
	public int SelectX;
	public int SelectY;
	
	public int FScale;
	
	public int FowardAngle;
	
	Pen FAPen;
	
	//构造函数
	public SpriteEditPanel( int Width, int Height ) : base()
	{
		this.BorderStyle = BorderStyle.None;
		this.BackColor = Color.Black;
		
		SetStyle(ControlStyles.UserPaint |
		         ControlStyles.AllPaintingInWmPaint |
		         ControlStyles.OptimizedDoubleBuffer |
		         ControlStyles.ResizeRedraw |
		         ControlStyles.SupportsTransparentBackColor,true);
		
		this.Dock = DockStyle.Fill;
		this.ImeMode = ImeMode.NoControl;
		
		this.Width = Width;
		this.Height = Height;
		
		FScale = 1024;
		
		Cursor Cursor1 = new Cursor( OS.SystemRoot + "Resource" + OS.PATH_S + "myPointEdit.cur" );
		//Cursor2 = new Cursor( OS.SystemRoot + "Resource" + OS.PATH_S + "myPoint2.cur" );
		
		this.Cursor = Cursor1;
		
		this.MouseMove += new MouseEventHandler( UserMouseMove );
		this.MouseUp += new MouseEventHandler( UserMouseUp );
		this.MouseDown += new MouseEventHandler( UserMouseDown );
		
		BackBrush = new SolidBrush( Color.Gray );
		EageLine = new Pen( Color.LightGray );
		EageLine.DashStyle = DashStyle.Dot;
		EageLine.DashOffset = 5;
		
		LeftisPress = false;
		
		RightisPress = false;
		RightLastX = 0;
		RightLastY = 0;
		
		StartX = 0;
		StartY = 0;
		
		FlScale = 1;
		FowardAngle = 0;
		
		FAPen = new Pen( Color.Yellow );
		FAPen.Width = 2;
		FAPen.EndCap = System.Drawing.Drawing2D.LineCap.ArrowAnchor;
		FAPen.DashStyle = System.Drawing.Drawing2D.DashStyle.Dot;
		
		t = new Timer();
		t.Interval = 1;
		t.Tick += new EventHandler( MPanel_Refresh );
		t.Enabled = false;
	}
	
	//设置图片
	public void SetImage( Bitmap b, int w, int h, int sx, int sy )
	{
		BackBitmap = b;
		
		BWidth = w;
		BHeight = h;
		StartX = (Width - BWidth)/2;
		StartY = (Height - BHeight)/2;
		FlScale = 1;
		
		SelectX = sx;
		SelectY = sy;
		
		t.Enabled = true;
	}
	
	//==========================================
	
	//重绘事件
	protected override void OnPaint(PaintEventArgs e)
	{
		e.Graphics.InterpolationMode = InterpolationMode.NearestNeighbor;
		e.Graphics.PixelOffsetMode = PixelOffsetMode.Half;
		
		ShowImage( e.Graphics );
	}
	
	//显示图像
	public void ShowImage( Graphics g )
	{
		//绘制背景
		g.FillRectangle( BackBrush, 0, 0, Width, Height );
		//g.Clear( BackBrush );
		
		int tempStartX =  StartX;
		int tempStartY =  StartY;
		
		if( BackBitmap != null ) {
			g.DrawImage( BackBitmap, tempStartX, tempStartY, (int)(BWidth * FlScale), (int)(BHeight * FlScale) );
		}
		//绘制网格
		//DrawCross( g );
		
		if( BackBitmap != null ) {
			g.DrawRectangle( EageLine, tempStartX, tempStartY, (int)(BWidth * FlScale), (int)(BHeight * FlScale) );
		}
		
		int ew = (int)FlScale;
		int eh = (int)FlScale;
		
		int ex = tempStartX + (int)(MouseX * FlScale);
		int ey = tempStartY + (int)(MouseY * FlScale);
		g.FillRectangle( Brushes.Blue, ex, ey, ew, eh  );
		
		int sx = tempStartX + (int)(SelectX * FlScale);
		int sy = tempStartY + (int)(SelectY * FlScale);
		g.FillRectangle( Brushes.OrangeRed, sx - 2, sy - 2, ew + 4, eh + 4 );
		g.FillRectangle( Brushes.Blue, sx, sy, ew, eh  );
		
		int ww = (int)FlScale * 10;
		
		g.DrawEllipse( Pens.White, sx - ww, sy - ww, ww*2 + ew, ww*2 + eh );
		g.DrawEllipse( Pens.Black, sx - ww - 1, sy - ww - 1, ww*2 + ew + 2, ww*2 + eh + 2 );
		g.DrawEllipse( Pens.Blue, sx - ww - 2, sy - ww - 2, ww*2 + ew + 4, ww*2 + eh + 4 );
		
		int MidX = sx + ew/2;
		int MidY = sy + eh/2;
		int FAX = (int)( ww * 20 / 10 * Math.Cos( Math.PI * 2 * FowardAngle / 360 ) );
		int FAY = (int)( ww * 20 / 10 * Math.Sin( Math.PI * 2 * FowardAngle / 360 ) );
		g.DrawLine( FAPen, MidX, MidY, MidX + FAX, MidY - FAY );
	}
	
	//绘制背景点阵
	void DrawCross( Graphics g )
	{
		g.DrawLine( Pens.Red, 0, Height/2, Width, Height/2 );
		g.DrawLine( Pens.Red, Width/2, 0, Width/2, Height );
		g.DrawEllipse( Pens.Red, Width/2 - 20, Height/2 - 20, 40, 40 );
	}
	
	//图形界面定时刷新事件
	void MPanel_Refresh( object sender, EventArgs e )
	{
		t.Enabled = false;
		
		//触发界面显示刷新
		this.Invalidate();
	}
	
	//鼠标滚轮事件
	public void UserMouseWheel( int px, int py, int Delta )
	{
		Point bp = GetBitmapLocation( px, py );
		
		if( Delta > 0 ) {
			FlScale *= 2;
		}
		else {
			FlScale /= 2;
		}
		StartX = px - (int)(bp.X * FlScale);
		StartY = py - (int)(bp.Y * FlScale);
		
		t.Enabled = true;
	}
	
	//鼠标按下事件
	void UserMouseDown( object sender, MouseEventArgs e )
	{
		if( e.Button == MouseButtons.Left ) {
			LeftisPress = true;
			Point p = GetBitmapLocation( e.X, e.Y );
			SelectX = p.X;
			SelectY = p.Y;
		}
		if( e.Button == MouseButtons.Right ) {
			RightisPress = true;
			RightLastX = e.X;
			RightLastY = e.Y;
		}
		t.Enabled = true;
	}
	
	//鼠标松开事件
	void UserMouseUp( object sender, MouseEventArgs e )
	{
		if( e.Button == MouseButtons.Left ) {
			LeftisPress = false;
		}
		if( e.Button == MouseButtons.Right ) {
			RightisPress = false;
		}
		t.Enabled = true;
	}
	
	//鼠标移动事件
	void UserMouseMove( object sender, MouseEventArgs e )
	{
		Point p = GetBitmapLocation( e.X, e.Y );
		MouseX = p.X;
		MouseY = p.Y;
		
		if( LeftisPress ) {
			//...
		}
		if( RightisPress ) {
			StartX += e.X - RightLastX;
			StartY += e.Y - RightLastY;
			RightLastX = e.X;
			RightLastY = e.Y;
		}
		
		t.Enabled = true;
	}
	
	//-------------------------------------------
	//根据鼠标单击位置, 计算出对应的图片位置
	Point GetBitmapLocation( int x, int y )
	{
		x = (int)((x - StartX) / FlScale);
		y = (int)((y - StartY) / FlScale);
		
		return new Point( x, y );
	}
}
}


