﻿

namespace n_ImagePanelSetForm
{
	partial class ImagePanelSetForm
	{
		/// <summary>
		/// Designer variable used to keep track of non-visual components.
		/// </summary>
		private System.ComponentModel.IContainer components = null;
		
		/// <summary>
		/// Disposes resources used by the form.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing) {
				if (components != null) {
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}
		
		/// <summary>
		/// This method is required for Windows Forms designer support.
		/// Do not change the method contents inside the source code editor. The Forms designer might
		/// not be able to load this method if it was changed manually.
		/// </summary>
		private void InitializeComponent()
		{
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ImagePanelSetForm));
			this.button确定 = new System.Windows.Forms.Button();
			this.button取消 = new System.Windows.Forms.Button();
			this.button1 = new System.Windows.Forms.Button();
			this.label1 = new System.Windows.Forms.Label();
			this.buttonClear = new System.Windows.Forms.Button();
			this.textBox1 = new System.Windows.Forms.TextBox();
			this.SuspendLayout();
			// 
			// button确定
			// 
			this.button确定.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(192)))), ((int)(((byte)(255)))));
			resources.ApplyResources(this.button确定, "button确定");
			this.button确定.ForeColor = System.Drawing.Color.White;
			this.button确定.Name = "button确定";
			this.button确定.UseVisualStyleBackColor = false;
			this.button确定.Click += new System.EventHandler(this.Button确定Click);
			// 
			// button取消
			// 
			this.button取消.BackColor = System.Drawing.Color.Coral;
			resources.ApplyResources(this.button取消, "button取消");
			this.button取消.ForeColor = System.Drawing.Color.White;
			this.button取消.Name = "button取消";
			this.button取消.UseVisualStyleBackColor = false;
			this.button取消.Click += new System.EventHandler(this.Button取消Click);
			// 
			// button1
			// 
			this.button1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
			resources.ApplyResources(this.button1, "button1");
			this.button1.ForeColor = System.Drawing.Color.White;
			this.button1.Name = "button1";
			this.button1.UseVisualStyleBackColor = false;
			this.button1.Click += new System.EventHandler(this.Button1Click);
			// 
			// label1
			// 
			resources.ApplyResources(this.label1, "label1");
			this.label1.Name = "label1";
			// 
			// buttonClear
			// 
			this.buttonClear.BackColor = System.Drawing.Color.Green;
			resources.ApplyResources(this.buttonClear, "buttonClear");
			this.buttonClear.ForeColor = System.Drawing.Color.White;
			this.buttonClear.Name = "buttonClear";
			this.buttonClear.UseVisualStyleBackColor = false;
			this.buttonClear.Click += new System.EventHandler(this.ButtonClearClick);
			// 
			// textBox1
			// 
			resources.ApplyResources(this.textBox1, "textBox1");
			this.textBox1.Name = "textBox1";
			// 
			// ImagePanelSetForm
			// 
			resources.ApplyResources(this, "$this");
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.BackColor = System.Drawing.Color.WhiteSmoke;
			this.Controls.Add(this.textBox1);
			this.Controls.Add(this.buttonClear);
			this.Controls.Add(this.label1);
			this.Controls.Add(this.button1);
			this.Controls.Add(this.button取消);
			this.Controls.Add(this.button确定);
			this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
			this.Name = "ImagePanelSetForm";
			this.ShowInTaskbar = false;
			this.TopMost = true;
			this.ResumeLayout(false);
			this.PerformLayout();
		}
		private System.Windows.Forms.TextBox textBox1;
		private System.Windows.Forms.Button buttonClear;
		private System.Windows.Forms.Button button取消;
		private System.Windows.Forms.Button button确定;
		private System.Windows.Forms.Button button1;
		private System.Windows.Forms.Label label1;
	}
}
