﻿
namespace n_ControlSetForm
{
using System;
using System.Windows.Forms;
using System.IO;
using System.Drawing;
using c_FormMover;
using n_SG_MyPanel;

/// <summary>
/// Description of WorkForm.
/// </summary>
public partial class ControlSetForm : Form
{
	bool isOK;
	FormMover fm;
	
	//主窗口
	public ControlSetForm()
	{
		InitializeComponent();
		
		fm = new FormMover( this );
	}
	
	//运行
	public string Run( string s )
	{
		this.label1.Text = s;
		
		//注意,直接设置Visible不能使窗体获得焦点
		//this.Visible = true;
		this.ShowDialog();
		
		while( this.Visible ) {
			System.Windows.Forms.Application.DoEvents();
		}
		if( isOK ) {
			return this.label1.Text;
		}
		return null;
	}
	
	void Button确定Click(object sender, EventArgs e)
	{
		isOK = true;
		this.Visible = false;
	}
	
	void Button取消Click(object sender, EventArgs e)
	{
		isOK = false;
		this.Visible = false;
	}
	
	//添加图片
	void Button1Click(object sender, EventArgs e)
	{
		//打开文件对话框
		OpenFileDialog OpenFileDlg = new OpenFileDialog();
		OpenFileDlg.Filter = "所有类型文件 | *.*";
		OpenFileDlg.Title = "请选择文件";
		
		if( !G.ccode.isStartFile ) {
			OpenFileDlg.InitialDirectory = G.ccode.FilePath;
		}
		else {
			string Mes = "您的这个文件尚未保存到电脑硬盘上, 所以无法设置图片资源的默认文件夹,\n";
			Mes += "强烈建议您先保存一下这个文件到电脑上, 再选择图片路径, 这样方便系统记录您的默认文件夹.\n";
			Mes += "否则系统将会使用全局文件路径记录, 这样会导致您的vex程序复制到其他目录后运行异常.\n";
			MessageBox.Show( Mes );
		}
		
		DialogResult dlgResult = OpenFileDlg.ShowDialog();
		if(dlgResult == DialogResult.OK) {
			if( OpenFileDlg.FileName.StartsWith( G.ccode.FilePath ) ) {
				this.label1.Text += OpenFileDlg.FileName.Remove( 0, G.ccode.FilePath.Length );
			}
			else {
				this.label1.Text += OpenFileDlg.FileName;
			}
		}
	}
	
	void ButtonClearClick(object sender, EventArgs e)
	{
		this.label1.Text = "";
	}
}
}

