﻿

namespace n_ControlSetForm
{
	partial class ControlSetForm
	{
		/// <summary>
		/// Designer variable used to keep track of non-visual components.
		/// </summary>
		private System.ComponentModel.IContainer components = null;
		
		/// <summary>
		/// Disposes resources used by the form.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing) {
				if (components != null) {
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}
		
		/// <summary>
		/// This method is required for Windows Forms designer support.
		/// Do not change the method contents inside the source code editor. The Forms designer might
		/// not be able to load this method if it was changed manually.
		/// </summary>
		private void InitializeComponent()
		{
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ControlSetForm));
			this.button确定 = new System.Windows.Forms.Button();
			this.button取消 = new System.Windows.Forms.Button();
			this.button1 = new System.Windows.Forms.Button();
			this.label1 = new System.Windows.Forms.Label();
			this.buttonClear = new System.Windows.Forms.Button();
			this.SuspendLayout();
			// 
			// button确定
			// 
			this.button确定.AccessibleDescription = resources.GetString("button确定.AccessibleDescription");
			this.button确定.AccessibleName = resources.GetString("button确定.AccessibleName");
			this.button确定.Anchor = ((System.Windows.Forms.AnchorStyles)(resources.GetObject("button确定.Anchor")));
			this.button确定.AutoSize = ((bool)(resources.GetObject("button确定.AutoSize")));
			this.button确定.AutoSizeMode = ((System.Windows.Forms.AutoSizeMode)(resources.GetObject("button确定.AutoSizeMode")));
			this.button确定.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(192)))), ((int)(((byte)(255)))));
			this.button确定.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("button确定.BackgroundImage")));
			this.button确定.BackgroundImageLayout = ((System.Windows.Forms.ImageLayout)(resources.GetObject("button确定.BackgroundImageLayout")));
			this.button确定.Dock = ((System.Windows.Forms.DockStyle)(resources.GetObject("button确定.Dock")));
			this.button确定.FlatStyle = ((System.Windows.Forms.FlatStyle)(resources.GetObject("button确定.FlatStyle")));
			this.button确定.Font = ((System.Drawing.Font)(resources.GetObject("button确定.Font")));
			this.button确定.ForeColor = System.Drawing.Color.White;
			this.button确定.ImageAlign = ((System.Drawing.ContentAlignment)(resources.GetObject("button确定.ImageAlign")));
			this.button确定.ImageIndex = ((int)(resources.GetObject("button确定.ImageIndex")));
			this.button确定.ImageKey = resources.GetString("button确定.ImageKey");
			this.button确定.ImeMode = ((System.Windows.Forms.ImeMode)(resources.GetObject("button确定.ImeMode")));
			this.button确定.Location = ((System.Drawing.Point)(resources.GetObject("button确定.Location")));
			this.button确定.MaximumSize = ((System.Drawing.Size)(resources.GetObject("button确定.MaximumSize")));
			this.button确定.Name = "button确定";
			this.button确定.RightToLeft = ((System.Windows.Forms.RightToLeft)(resources.GetObject("button确定.RightToLeft")));
			this.button确定.Size = ((System.Drawing.Size)(resources.GetObject("button确定.Size")));
			this.button确定.TabIndex = ((int)(resources.GetObject("button确定.TabIndex")));
			this.button确定.Text = resources.GetString("button确定.Text");
			this.button确定.TextAlign = ((System.Drawing.ContentAlignment)(resources.GetObject("button确定.TextAlign")));
			this.button确定.TextImageRelation = ((System.Windows.Forms.TextImageRelation)(resources.GetObject("button确定.TextImageRelation")));
			this.button确定.UseVisualStyleBackColor = false;
			this.button确定.Click += new System.EventHandler(this.Button确定Click);
			// 
			// button取消
			// 
			this.button取消.AccessibleDescription = resources.GetString("button取消.AccessibleDescription");
			this.button取消.AccessibleName = resources.GetString("button取消.AccessibleName");
			this.button取消.Anchor = ((System.Windows.Forms.AnchorStyles)(resources.GetObject("button取消.Anchor")));
			this.button取消.AutoSize = ((bool)(resources.GetObject("button取消.AutoSize")));
			this.button取消.AutoSizeMode = ((System.Windows.Forms.AutoSizeMode)(resources.GetObject("button取消.AutoSizeMode")));
			this.button取消.BackColor = System.Drawing.Color.Coral;
			this.button取消.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("button取消.BackgroundImage")));
			this.button取消.BackgroundImageLayout = ((System.Windows.Forms.ImageLayout)(resources.GetObject("button取消.BackgroundImageLayout")));
			this.button取消.Dock = ((System.Windows.Forms.DockStyle)(resources.GetObject("button取消.Dock")));
			this.button取消.FlatStyle = ((System.Windows.Forms.FlatStyle)(resources.GetObject("button取消.FlatStyle")));
			this.button取消.Font = ((System.Drawing.Font)(resources.GetObject("button取消.Font")));
			this.button取消.ForeColor = System.Drawing.Color.White;
			this.button取消.ImageAlign = ((System.Drawing.ContentAlignment)(resources.GetObject("button取消.ImageAlign")));
			this.button取消.ImageIndex = ((int)(resources.GetObject("button取消.ImageIndex")));
			this.button取消.ImageKey = resources.GetString("button取消.ImageKey");
			this.button取消.ImeMode = ((System.Windows.Forms.ImeMode)(resources.GetObject("button取消.ImeMode")));
			this.button取消.Location = ((System.Drawing.Point)(resources.GetObject("button取消.Location")));
			this.button取消.MaximumSize = ((System.Drawing.Size)(resources.GetObject("button取消.MaximumSize")));
			this.button取消.Name = "button取消";
			this.button取消.RightToLeft = ((System.Windows.Forms.RightToLeft)(resources.GetObject("button取消.RightToLeft")));
			this.button取消.Size = ((System.Drawing.Size)(resources.GetObject("button取消.Size")));
			this.button取消.TabIndex = ((int)(resources.GetObject("button取消.TabIndex")));
			this.button取消.Text = resources.GetString("button取消.Text");
			this.button取消.TextAlign = ((System.Drawing.ContentAlignment)(resources.GetObject("button取消.TextAlign")));
			this.button取消.TextImageRelation = ((System.Windows.Forms.TextImageRelation)(resources.GetObject("button取消.TextImageRelation")));
			this.button取消.UseVisualStyleBackColor = false;
			this.button取消.Click += new System.EventHandler(this.Button取消Click);
			// 
			// button1
			// 
			this.button1.AccessibleDescription = resources.GetString("button1.AccessibleDescription");
			this.button1.AccessibleName = resources.GetString("button1.AccessibleName");
			this.button1.Anchor = ((System.Windows.Forms.AnchorStyles)(resources.GetObject("button1.Anchor")));
			this.button1.AutoSize = ((bool)(resources.GetObject("button1.AutoSize")));
			this.button1.AutoSizeMode = ((System.Windows.Forms.AutoSizeMode)(resources.GetObject("button1.AutoSizeMode")));
			this.button1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
			this.button1.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("button1.BackgroundImage")));
			this.button1.BackgroundImageLayout = ((System.Windows.Forms.ImageLayout)(resources.GetObject("button1.BackgroundImageLayout")));
			this.button1.Dock = ((System.Windows.Forms.DockStyle)(resources.GetObject("button1.Dock")));
			this.button1.FlatStyle = ((System.Windows.Forms.FlatStyle)(resources.GetObject("button1.FlatStyle")));
			this.button1.Font = ((System.Drawing.Font)(resources.GetObject("button1.Font")));
			this.button1.ForeColor = System.Drawing.Color.White;
			this.button1.ImageAlign = ((System.Drawing.ContentAlignment)(resources.GetObject("button1.ImageAlign")));
			this.button1.ImageIndex = ((int)(resources.GetObject("button1.ImageIndex")));
			this.button1.ImageKey = resources.GetString("button1.ImageKey");
			this.button1.ImeMode = ((System.Windows.Forms.ImeMode)(resources.GetObject("button1.ImeMode")));
			this.button1.Location = ((System.Drawing.Point)(resources.GetObject("button1.Location")));
			this.button1.MaximumSize = ((System.Drawing.Size)(resources.GetObject("button1.MaximumSize")));
			this.button1.Name = "button1";
			this.button1.RightToLeft = ((System.Windows.Forms.RightToLeft)(resources.GetObject("button1.RightToLeft")));
			this.button1.Size = ((System.Drawing.Size)(resources.GetObject("button1.Size")));
			this.button1.TabIndex = ((int)(resources.GetObject("button1.TabIndex")));
			this.button1.Text = resources.GetString("button1.Text");
			this.button1.TextAlign = ((System.Drawing.ContentAlignment)(resources.GetObject("button1.TextAlign")));
			this.button1.TextImageRelation = ((System.Windows.Forms.TextImageRelation)(resources.GetObject("button1.TextImageRelation")));
			this.button1.UseVisualStyleBackColor = false;
			this.button1.Click += new System.EventHandler(this.Button1Click);
			// 
			// label1
			// 
			this.label1.AccessibleDescription = resources.GetString("label1.AccessibleDescription");
			this.label1.AccessibleName = resources.GetString("label1.AccessibleName");
			this.label1.Anchor = ((System.Windows.Forms.AnchorStyles)(resources.GetObject("label1.Anchor")));
			this.label1.AutoSize = ((bool)(resources.GetObject("label1.AutoSize")));
			this.label1.BackgroundImageLayout = ((System.Windows.Forms.ImageLayout)(resources.GetObject("label1.BackgroundImageLayout")));
			this.label1.Dock = ((System.Windows.Forms.DockStyle)(resources.GetObject("label1.Dock")));
			this.label1.Font = ((System.Drawing.Font)(resources.GetObject("label1.Font")));
			this.label1.ImageAlign = ((System.Drawing.ContentAlignment)(resources.GetObject("label1.ImageAlign")));
			this.label1.ImageIndex = ((int)(resources.GetObject("label1.ImageIndex")));
			this.label1.ImageKey = resources.GetString("label1.ImageKey");
			this.label1.ImeMode = ((System.Windows.Forms.ImeMode)(resources.GetObject("label1.ImeMode")));
			this.label1.Location = ((System.Drawing.Point)(resources.GetObject("label1.Location")));
			this.label1.MaximumSize = ((System.Drawing.Size)(resources.GetObject("label1.MaximumSize")));
			this.label1.Name = "label1";
			this.label1.RightToLeft = ((System.Windows.Forms.RightToLeft)(resources.GetObject("label1.RightToLeft")));
			this.label1.Size = ((System.Drawing.Size)(resources.GetObject("label1.Size")));
			this.label1.TabIndex = ((int)(resources.GetObject("label1.TabIndex")));
			this.label1.TextAlign = ((System.Drawing.ContentAlignment)(resources.GetObject("label1.TextAlign")));
			// 
			// buttonClear
			// 
			this.buttonClear.AccessibleDescription = resources.GetString("buttonClear.AccessibleDescription");
			this.buttonClear.AccessibleName = resources.GetString("buttonClear.AccessibleName");
			this.buttonClear.Anchor = ((System.Windows.Forms.AnchorStyles)(resources.GetObject("buttonClear.Anchor")));
			this.buttonClear.AutoSize = ((bool)(resources.GetObject("buttonClear.AutoSize")));
			this.buttonClear.AutoSizeMode = ((System.Windows.Forms.AutoSizeMode)(resources.GetObject("buttonClear.AutoSizeMode")));
			this.buttonClear.BackColor = System.Drawing.Color.Green;
			this.buttonClear.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("buttonClear.BackgroundImage")));
			this.buttonClear.BackgroundImageLayout = ((System.Windows.Forms.ImageLayout)(resources.GetObject("buttonClear.BackgroundImageLayout")));
			this.buttonClear.Dock = ((System.Windows.Forms.DockStyle)(resources.GetObject("buttonClear.Dock")));
			this.buttonClear.FlatStyle = ((System.Windows.Forms.FlatStyle)(resources.GetObject("buttonClear.FlatStyle")));
			this.buttonClear.Font = ((System.Drawing.Font)(resources.GetObject("buttonClear.Font")));
			this.buttonClear.ForeColor = System.Drawing.Color.White;
			this.buttonClear.ImageAlign = ((System.Drawing.ContentAlignment)(resources.GetObject("buttonClear.ImageAlign")));
			this.buttonClear.ImageIndex = ((int)(resources.GetObject("buttonClear.ImageIndex")));
			this.buttonClear.ImageKey = resources.GetString("buttonClear.ImageKey");
			this.buttonClear.ImeMode = ((System.Windows.Forms.ImeMode)(resources.GetObject("buttonClear.ImeMode")));
			this.buttonClear.Location = ((System.Drawing.Point)(resources.GetObject("buttonClear.Location")));
			this.buttonClear.MaximumSize = ((System.Drawing.Size)(resources.GetObject("buttonClear.MaximumSize")));
			this.buttonClear.Name = "buttonClear";
			this.buttonClear.RightToLeft = ((System.Windows.Forms.RightToLeft)(resources.GetObject("buttonClear.RightToLeft")));
			this.buttonClear.Size = ((System.Drawing.Size)(resources.GetObject("buttonClear.Size")));
			this.buttonClear.TabIndex = ((int)(resources.GetObject("buttonClear.TabIndex")));
			this.buttonClear.Text = resources.GetString("buttonClear.Text");
			this.buttonClear.TextAlign = ((System.Drawing.ContentAlignment)(resources.GetObject("buttonClear.TextAlign")));
			this.buttonClear.TextImageRelation = ((System.Windows.Forms.TextImageRelation)(resources.GetObject("buttonClear.TextImageRelation")));
			this.buttonClear.UseVisualStyleBackColor = false;
			this.buttonClear.Click += new System.EventHandler(this.ButtonClearClick);
			// 
			// ControlSetForm
			// 
			this.AccessibleDescription = resources.GetString("$this.AccessibleDescription");
			this.AccessibleName = resources.GetString("$this.AccessibleName");
			this.AutoScaleDimensions = ((System.Drawing.SizeF)(resources.GetObject("$this.AutoScaleDimensions")));
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.AutoScroll = ((bool)(resources.GetObject("$this.AutoScroll")));
			this.AutoSize = ((bool)(resources.GetObject("$this.AutoSize")));
			this.AutoSizeMode = ((System.Windows.Forms.AutoSizeMode)(resources.GetObject("$this.AutoSizeMode")));
			this.BackColor = System.Drawing.Color.WhiteSmoke;
			this.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImage")));
			this.BackgroundImageLayout = ((System.Windows.Forms.ImageLayout)(resources.GetObject("$this.BackgroundImageLayout")));
			this.ClientSize = ((System.Drawing.Size)(resources.GetObject("$this.ClientSize")));
			this.Controls.Add(this.buttonClear);
			this.Controls.Add(this.label1);
			this.Controls.Add(this.button1);
			this.Controls.Add(this.button取消);
			this.Controls.Add(this.button确定);
			this.Font = ((System.Drawing.Font)(resources.GetObject("$this.Font")));
			this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
			this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
			this.ImeMode = ((System.Windows.Forms.ImeMode)(resources.GetObject("$this.ImeMode")));
			this.Location = ((System.Drawing.Point)(resources.GetObject("$this.Location")));
			this.MaximumSize = ((System.Drawing.Size)(resources.GetObject("$this.MaximumSize")));
			this.Name = "ControlSetForm";
			this.RightToLeft = ((System.Windows.Forms.RightToLeft)(resources.GetObject("$this.RightToLeft")));
			this.RightToLeftLayout = ((bool)(resources.GetObject("$this.RightToLeftLayout")));
			this.ShowInTaskbar = false;
			this.StartPosition = ((System.Windows.Forms.FormStartPosition)(resources.GetObject("$this.StartPosition")));
			this.Text = resources.GetString("$this.Text");
			this.TopMost = true;
			this.ResumeLayout(false);

		}
		private System.Windows.Forms.Button buttonClear;
		private System.Windows.Forms.Button button取消;
		private System.Windows.Forms.Button button确定;
		private System.Windows.Forms.Button button1;
		private System.Windows.Forms.Label label1;
	}
}
