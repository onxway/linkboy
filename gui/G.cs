﻿
//命名空间
using System;
using System.Windows.Forms;
using n_CCode;
using n_CodeOutForm;
using n_scommonEXPForm;
using n_DescriptionForm;
using n_extendEXPForm;
using n_FlashForm;
using n_GUIset;
using n_ImagePanel;
using n_LatticeForm;
using n_ModuleLibPanel;
using n_MusicForm;
using n_NoteForm;
using n_TextForm;
using n_timeEXPForm;
using n_protocolEXPForm;
using n_EnginePair;
using n_StartFile;
using n_KeyboardForm;
using n_YSelectForm;
using n_SpriteSetForm;
using n_ControlSetForm;
using n_ImagePanelSetForm;
using n_UserFuncEXPForm;
using n_UserFuncV1EXPForm;
using n_SimLocateForm;
using n_FontForm;
using n_USetForm;
using n_ExampleForm;
using n_SimForm;
using n_CodeViewForm;
using n_ServiceForm;
using n_LD3320Form;
using n_ScreenForm;
using n_ScreenSetForm;
using n_PushForm;
using n_LIDEForm;

//主程序类
public static class G
{
	//2016.10.26
	//版本号改成了1.6, 因为加入了天眼系统
	
	//2016.11.28
	//版本号改成了1.7, 因为升级了新版的音乐编辑器
	
	//2016.12.29
	//版本号改成了1.8, 初步加上了仿真功能
	
	//2017.1.19
	//版本号改成了2.0,仿真功能基本完成了
	
	//2017.5.10 版本号改为了2.1 模块的效果图全部改完
	
	//2017.5.23 版本号改为了2.2 表达式编辑器改为了图形界面方案
	
	//2017.5.23 版本号改为了2.4 提取出了启动器，文件夹简洁了
	
	//2017.5.23 版本号改为了2.5 增加了物联网模块
	
	//2017.5.23 版本号改为了2.6 升级了马达驱动，步进电机驱动，增加了一些盾板类
	
	//2018.2.6 版本号改为了2.7 马达驱动增加了周期设置，在抖动和低启动电压之间平衡，右键删除导线
	
	//2018.6 版本号改为了2.8 增加了上帝视角, 在线调试功能
	
	//2018.7.13 版本号改为了3.0 增加虚拟地图模拟器, 界面全新升级
	
	//2018.11.19 版本号改为了3.1 增加物联网相关功能模块, 验证通过 (基于贝壳物联)
	
	//2019.1.21 版本号改为3.2 支持框架式编程
	
	//2019.3.27 版本号改为3.3 增加了机器视觉插件(摄像头)
	
	//2019.4.19 版本号改为了3.4 支持第三方厂商添加硬件, 支持通用硬件
	//						  支持STM32
	
	//2019.7.10 版本号改为了3.5 十周年 特别版
	//						  支持Arduino的python编程
	
	//2019.11.26 版本号改为了3.6 升级python编程界面, 增加RISC-V处理器
	
	//2020.3.1 版本号改为了3.7 全面升级STM32和GD32 VEX, 支持大量硬件相关模块
	//增加形状等图形相关引擎, 完成各类点阵屏幕的标准化
	
	//2020.6.1 版本号改为了3.9X(内测版) (调过了3.8) vex屏幕刷新加速, 界面调整, 增加电路仿真等
	
	//2020.7.13 版本号改为了4.0 配合兆易活动
	
	//2020.9.1 版本号改为了4.1 增加了动画编程实验室
	
	//2020.11.29 版本号改为了4.2 增加了ESP32系列
	
	//2021.3.11 版本号改为4.3, 调整了模块库分类, 合并到主类别
	
	//2021.5.1 改为了4.4内测版, 升级了波形调试器, 增加了虚拟底层IO机制;
	
	//2021.5.16 改为了4.5 (4.41)内测版, 增加遥控器功能;
	
	//2021.6.8 改为了4.51 调整了arduino子系统, 不再自动导入
	
	//2021.6.30 升级到4.60 增加了工控类几个模块, 增加了鸿蒙openHarmony(hi3861)
	
	//2021.7.29 升级到4.61 ESP32wifi修复, I2C升级
	
	//2021.10.12 升级到5.0版本, 增加外挂模式和更多主板
	
	//2022.7.24 发布5.0正式版
	
	//2022.9.19 发布5.1 十三周年特别版
	
	//2022.12.2 发布 5.2 主要改正遗留问题
	//2022.12.27 发布 5.21 主要升级ESP8266/32系列
	
	//2023.2.1 发布 5.3 正式版
	
	//2023.3.15 发布 5.33 增加自研指令集处理器
	//2023.7.30 发布 5.40 增加蜗牛操作系统
	
	//2023.9.5 发布5.50版本 开源
	
	public const string Version = "5.50.230827"; //注意整数版本号后边要带有0
	public const string VersionShow = "5.50"; //注意整数版本号后边要带有0
	public const string VersionMessage = "2023.9.5";
	
	//是否为探索开发模式
	public static bool TanMode = false;
	
	//是否为教育版
	public static bool EduVersion = false;
	
	
	public static bool LightUI = true;
	
	//注意修改 Ftp.cs -> Tls12
	public static bool GuangZhouTest = false;
	
	
	//这个版本号主要是兼容以前的旧代码, 用来识别, 凡是小于这个版本号的, 都会进行一个
	//版本转换, 包括替换库文件等, 以后尽量不改库的目录等, 保持兼容
	//public const int VersionNumberYear = 2015;
	//public const int VersionNumberMonth = 6;
	//public const int VersionNumberDay = 3;
	
	public delegate void deleInitMes( string Mes );
	public static deleInitMes InitMes;
	
	public delegate void deleSystemError( string FileName, string ErrorMes );
	public static deleSystemError SystemError;
	
	//是否为仿真自动运行模式启动
	public static bool AutoRunMode;
	
	
	//文本框窗体
	public static TextForm MyTextInputBox;
	
	//小精灵界面
	public static FlashForm FlashBox;
	
	//描述框窗体
	public static NoteForm NoteBox;
	//代码框窗体
	public static n_UserCodeForm.UserCodeForm UserCodeBox;
	
	//表达式窗体
	//public static commonEXPForm commonEXPBox;
	//表达式窗体
	public static scommonEXPForm commonEXPBox;
	//music扩展类型变量选择器窗体
	public static extendEXPForm extendEXPBox;
	//time扩展类型变量选择器窗体
	public static timeEXPForm timeEXPBox;
	//protocol扩展类型窗体
	public static protocolEXPForm protocolEXPBox;
	//用户函数设置窗体
	public static UserFuncEXPForm UserFuncEXPBox;
	//用户函数设置窗体
	public static UserFuncV1EXPForm UserFuncV1EXPBox;
	//组件描述窗体
	public static DescriptionForm DescriptionBox;
	//示例学习界面
	public static ExampleForm ExampleBox;
	
	//控件设置器
	public static ControlSetForm ControlSetBox;
	public static ImagePanelSetForm ImagePanelSetBox;
	
	//角色设置器
	public static SpriteSetForm SpriteSetBox;
	//音色选择器
	public static YSelectForm YSelectBox;
	//键盘布局器
	public static KeyboardForm KeyboardBox;
	//音乐盒
	public static MusicForm MusicBox;
	//图片编辑器
	public static n_ImageEditForm.Form1 BitmapBox;
	//彩图编辑器
	public static n_CbmpForm.CbmpForm CbmpBox;
	//点阵窗口
	public static LatticeForm LatticeBox;
	//字体编辑器
	public static FontForm FontBox;
	//代码输出窗体
	public static CodeForm CodeBox;
	//卫星导航模拟器
	public static SimLocateForm SimLocateBox;
	//语音输入器
	public static LD3320Form LD3320Box;
	//高级视图
	public static USetForm USetBox;
	//模拟演示器
	public static SimForm SimBox;
	//微型IDE
	public static LIDEForm LIDEBox;
	//引脚选择器
	public static n_PinForm.PinForm PinBox;
	
	//Vex文件串口下载器
	//public static VexLoaderForm VexLoaderBox;
	
	//Py代码复制文件下载器
	public static n_PyLoaderForm.PyLoaderForm PyLoderBox;
	
	//Export导出器
	public static n_ExportForm.ExportForm ExportBox;
	
	//Hex文件查看器
	public static n_HexForm.HexForm HexBox;
	
	//代码查看界面
	public static CodeViewForm CodeViewBox;
	
	//问题反馈界面
	public static ServiceForm ServiceBox;
	
	//截屏器
	public static ScreenForm ScreenBox;
	
	//截屏设置器
	public static ScreenSetForm ScreenSetBox;
	
	//信息界面
	public static PushForm PushBox;
	
	//编译信息界面
	public static n_CompileMessageForm.CompileMessageForm CompileMessageBox;
	
	//查找界面
	public static n_FindForm.FindForm FindBox;
	
	//知识图谱
	public static n_NGForm.NGForm NGBox;
	
	//python远程通信设置
	public static n_LAPIForm.LAPIForm LAPIBox;
	
	//遥控器
	public static n_RemoForm.RemoForm RemoBox;
	
	//模块库管理器
	public static n_ModMngForm.ModMngForm ModMngBox;
	
	//导出代码窗口
	public static n_CExportForm.CExportForm CExportBox;
	
	//全局静态成员
	public static ImagePanel CGPanel;
	
	public static RichTextBox FindTextBox;
	
	public static bool SimulateMode;
	
	public static bool ShowAllVar = false;
	
	public static bool DebugMode;
	
	public static bool ShowCruxCode;
	public static bool isCruxEXE = false; //crux编程实验室模式
	
	public static bool isCV = false; //crux.v电路设计语言
	
	public static bool StartStatus = true;
	
	//外部(第三方编辑器)模式
	public static bool OtherEditor = false;
	
	static CCode v_ccode;
	public static CCode ccode {
		get { return v_ccode; }
		set {
			v_ccode = value;
		}
	}
	
	//仿真发生变化, 需要重绘
	public delegate void D_MyRefresh();
	public static D_MyRefresh MyRefresh;
	
	//初始化
	public static void Init()
	{
		try {
		
		InitMes( "\n" );
		
		SimulateMode = false;
		DebugMode = false;
		ShowCruxCode = false;
		
		GUIset.Init();
		
		n_EXP.EXP.Init();
		
		n_Shape.Shape.Init();
		
		StartFile.Init();
		
		InitMes( "\n" );
		
		//初始化控件系统
		n_SG_MyControl.MyControl.Init();
		n_SG_MyControl.MyControl.EditEnable = true;
		
		n_ImagePanel.ImagePanel.Init();
		
		n_NL.NL.Init();
		
		n_Bitblt.Blt.Init();
		
		//InitMes( "\n" );
		
		//+++++++++++++++++++++++++++++++++++++++++++++
		//初始化其他必要插件
		
		//初始化GUIcoder
		n_GUIcoder.GUIcoder.Init();
		
		//初始化版本转换器
		n_GUIcoder.VersionSwitch.Init();
		
		//InitMes( "\n" );
		
		//注意这个需要在前边 仿真用到
		//BitmapBox.TopMost = true;
		
		//仿真模拟演示器
		SimBox = new SimForm();
		
		if( G.AutoRunMode ) {
			return;
		}
		InitMes( "\n" );
		//描述界面
		DescriptionBox = new DescriptionForm();
		InitMes( "\n" );
		//表达式窗体
		commonEXPBox = new scommonEXPForm();
		//InitMes( "\n" );
		//扩展表达式窗体
		extendEXPBox = new extendEXPForm();
		//InitMes( "\n" );
		//示例学习界面
		ExampleBox = new ExampleForm();
		InitMes( "\n" );
		//彩图编辑器
		CbmpBox = new n_CbmpForm.CbmpForm();
		//字模点阵转换器
		LatticeBox = new LatticeForm();
		//InitMes( "\n" );
		//字体编辑器
		FontBox = new FontForm();
		//InitMes( "\n" );
		//卫星导航模拟器
		SimLocateBox = new SimLocateForm();
		InitMes( "\n" );
		//代码输出窗体
		CodeBox = new CodeForm();
		//InitMes( "\n" );
		//代码查看
		CodeViewBox = new CodeViewForm();
		//InitMes( "\n" );
		//编译信息
		CompileMessageBox = new n_CompileMessageForm.CompileMessageForm();
		
		
		//==============================================
		//1 8266初始化创建线程并运行, 需要考虑下是否需要
		//2 以下待验证...
		
		
		//用户函数设置窗体
		//UserFuncEXPBox = new UserFuncEXPForm();
		
		//控件设置器
		//ControlSetBox = new ControlSetForm();
		
		//ImagePanelSetBox = new ImagePanelSetForm();
		
		//角色设置器
		//SpriteSetBox = new SpriteSetForm();
		
		//音色选择器
		//YSelectBox = new YSelectForm();
		
		//键盘布局器
		//KeyboardBox = new KeyboardForm();
		
		//语音输入器
		//LD3320Box = new LD3320Form();
		
		//初始化虚拟机串口下载器
		//VexLoaderBox = new VexLoaderForm();
		
		//初始化py代码下载器
		//PyLoderBox = new n_PyLoaderForm.PyLoaderForm();
		
		//描述框窗体
		//NoteBox = new NoteForm();
		
		//用户代码窗体
		//UserCodeBox = new n_UserCodeForm.UserCodeForm();
		
		//查找界面
		//FindBox = new n_FindForm.FindForm();
		
		//密码验证
		//PassBox = new PassForm();
		
		//截屏器
		//ScreenBox = new ScreenForm();
		
		//截屏设置器
		//ScreenSetBox = new ScreenSetForm();
		
		//知识网络
		//NGBox = new n_NGForm.NGForm();
		
		//==============================================
		
		
		//注意这个需要在前边， 因为MyModuleLibPanel中调用了Decoder初始化
		
		//初始化引擎匹配器
		EnginePair.Init();
		
		/*
		if( n_GUISystemData.GUISystemData.StartMode == 0 ) {
			if( MessageBox.Show( "是否进入教学模式(教学模式下会隐藏一些不常用模块)? 点击 [是] 进入教学模式, 点击 <否> 进入完整模式. 如不希望出现此对话框，可在系统设置中隐藏。", "模式选择", MessageBoxButtons.YesNo ) == DialogResult.Yes ) {
				n_ModuleLibDecoder.ModuleLibDecoder.TempLibName = "tech";
			}
		}
		else if( n_GUISystemData.GUISystemData.StartMode == 1 ) {
			n_ModuleLibDecoder.ModuleLibDecoder.TempLibName = "tech";
		}
		else if( n_GUISystemData.GUISystemData.StartMode == 2 ) {
			
		}
		else {
			//....
		}
		*/
		
		
		//初始化组件库
		if( InitMes != null ) {
			InitMes( "加载模块库  " );
		}
		//MyModuleLibPanel.Init();
		n_ModuleLibDecoder.ModuleLibDecoder.Init();
		
		
		InitMes( "\n" );
		
		//由于要检测消息, 所以放到软件启动后创建消息推送机
		//创建消息推送机
		PushBox = new PushForm();
		
		//网络通信初始化
		n_ESP8266.ESP8266.Init();
		
		n_LAPI.LAPI.Init();
		
		//指令链接和仿真都需要声音
		c_MIDI.Music.Open();
		c_MIDI.Music.SetTimbre( 5, 113 );
		c_MIDI.Music.SetTimbre( 6, 121 );
		
		} catch(Exception e) { MessageBox.Show( e.ToString() ); }
	}
	
	//设置为 crux 模式
	public static void SetCruxMode()
	{
		isCruxEXE = true;
		n_Config.Config.WarningAsError = true;
	}
	
	//关闭
	public static void Close()
	{
		n_NL.NL.Close();
		
		n_Bitblt.Blt.Close();
		n_ESP8266.ESP8266.MyClose();
		
		try {
		
		//组件关闭
		if( !G.AutoRunMode ) {
			if( G.MusicBox != null ) {
				G.MusicBox.Quit();
			}
		}
		
		}catch{
			
		}
	}
}





