﻿
using System;
using System.Drawing;
using System.Windows.Forms;
using n_LocatePanel;

namespace n_LD3320Form
{
/// <summary>
/// Description of LocateForm.
/// </summary>
public partial class LD3320Form : Form
{
	public static int Data = -1;
	
	public LD3320Form()
	{
		//
		// The InitializeComponent() call is required for Windows Forms designer support.
		//
		InitializeComponent();
		
		Data = -1;
	}
	
	//运行
	public void Run()
	{
		this.Visible = true;
	}
	
	void LocateFormFormClosing(object sender, FormClosingEventArgs e)
	{
		this.Visible = false;
		e.Cancel = true;
	}
	
	void Button1Click(object sender, EventArgs e)
	{
		Data = int.Parse( textBox1.Text );
	}
	
	void TextBox1MouseDown(object sender, MouseEventArgs e)
	{
		textBox1.SelectAll();
	}
}
}

