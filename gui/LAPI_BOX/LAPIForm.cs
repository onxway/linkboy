﻿
using System;
using System.Drawing;
using System.Windows.Forms;
using n_LocatePanel;
using n_MyObject;
using n_MyFileObject;
using n_GUIcoder;

namespace n_LAPIForm
{
/// <summary>
/// Description of LocateForm.
/// </summary>
public partial class LAPIForm : Form
{
	public LAPIForm()
	{
		//
		// The InitializeComponent() call is required for Windows Forms designer support.
		//
		InitializeComponent();
		
		textBoxIP.Text = n_MainSystemData.SystemData.LAPI_IP;
		textBoxPORT.Text = n_MainSystemData.SystemData.LAPI_PORT;
	}
	
	//运行
	public void Run()
	{
		//richTextBox1.Text = GetPyAPI();
		
		if( n_LAPI.LAPI.Connected ) {
			//return;
		}
		this.Visible = true;
		//while( this.Visible ) {
		//	System.Windows.Forms.Application.DoEvents();
		//}
	}
	
	void LocateFormFormClosing(object sender, FormClosingEventArgs e)
	{
		this.Visible = false;
		e.Cancel = true;
	}
	
	void Button1Click(object sender, EventArgs e)
	{
		try {
			n_LAPI.LAPI.Connect( textBoxIP.Text, int.Parse( textBoxPORT.Text ) );
		}
		catch {
			MessageBox.Show( "串口转网络数据连接失败, 请确保串口转网络软件已在后台运行!" );
			return;
		}
		if( !n_LAPI.LAPI.Connected ) {
			MessageBox.Show( "串口转网络数据连接失败, 请确保串口转网络软件已在后台运行!" );
			return;
		}
		
		n_MainSystemData.SystemData.LAPI_IP = textBoxIP.Text;
		n_MainSystemData.SystemData.LAPI_PORT = textBoxPORT.Text;
		n_MainSystemData.SystemData.isChanged = true;
		
		button1.Enabled = false;
		//this.Visible = false;
	}
}
}

