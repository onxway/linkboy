﻿
namespace n_CompileMessageForm
{
using System;
using System.Text;
using System.Windows.Forms;

using n_Compiler;
using n_FunctionCodeList;
using n_MesTabPage;
using n_ParseNet;
using n_WordList;

/// <summary>
/// Description of WorkForm.
/// </summary>
public partial class CompileMessageForm : Form
{
	MesTabPage CompListBox;
	MesTabPage SourceCodeBox0;
	MesTabPage SourceCodeBox;
	MesTabPage AcidenceBox;
	MesTabPage ParseBox;
	MesTabPage MidCode1Box;
	MesTabPage ASMBox;
	MesTabPage HEXBox;
	
	//主窗口
	public CompileMessageForm()
	{
		InitializeComponent();
		
		CompListBox = new MesTabPage( "编译信息" );
		this.tabControl2.Controls.Add( CompListBox );
		SourceCodeBox0 = new MesTabPage( "用户源代码" );
		this.tabControl2.Controls.Add( SourceCodeBox0 );
		SourceCodeBox = new MesTabPage( "全部源代码" );
		this.tabControl2.Controls.Add( SourceCodeBox );
		AcidenceBox = new MesTabPage( "词法分析" );
		this.tabControl2.Controls.Add( AcidenceBox );
		ParseBox = new MesTabPage( "语法分析" );
		this.tabControl2.Controls.Add( ParseBox );
		MidCode1Box = new MesTabPage( "中间代码" );
		this.tabControl2.Controls.Add( MidCode1Box );
		ASMBox = new MesTabPage( "汇编代码" );
		this.tabControl2.Controls.Add( ASMBox );
		HEXBox = new MesTabPage( "机器代码" );
		this.tabControl2.Controls.Add( HEXBox );
		
		this.tabControl2.Click += new EventHandler( MessageTabSelect );
	}
	
	//窗体显示
	public void PyRun()
	{
		SetPyResult();
		
		this.Visible = true;
		MessageTabSelect( null, null );
	}
	
	//窗体显示
	public void Run()
	{
		SetResult();
		
		this.Visible = true;
		MessageTabSelect( null, null );
	}
	
	//设置python编译结果
	void SetPyResult()
	{
		//设置结果
		try { SourceCodeBox.Set( n_PyAccidence.Accidence.Source ); } catch {}
		try { AcidenceBox.Set( n_PyWordList.WordList.Show() ); } catch {}
		try { ParseBox.Set( n_PyParseNet.ParseNet.Show() ); } catch {}
		try { MidCode1Box.Set( n_PyDeploy.Deploy.Result ); } catch {}
		try { ASMBox.Set( n_Compiler.Compiler.SimTempSource ); } catch {}
		
		SetPyCompList();
	}
	
	//设置编译结果
	void SetPyCompList()
	{
		StringBuilder result = new StringBuilder( "" );
		
		result.Append( "****************元件列表:****************\n" );
		try { result.Append( n_PyUnitList.UnitList.Show() ); } catch {}
		
		result.Append( "****************虚拟类型列表:****************\n" );
		try { result.Append( n_PyVdataList.VdataList.Show() ); } catch {}
		
		result.Append( "***************结构体列表:***************\n" );
		try { result.Append( n_PyStructList.StructList.Show() ); } catch {}
		
		result.Append( "****************函数列表:****************\n" );
		try { result.Append( n_PyFunctionList.FunctionList.Show() ); } catch {}
		
		result.Append( "****************变量列表:****************\n" );
		try { result.Append( n_PyVarList.VarList.Show() ); } catch {}
		
		result.Append( "****************标号列表:****************\n" );
		try { result.Append( n_PyLabelList.LabelList.Show() ); } catch {}
		
		
		CompListBox.Set( result.ToString() );
	}
	
	//设置结果
	void SetResult()
	{
		//设置结果
		
		try {
			SourceCodeBox0.Set( n_GUIcoder.GUIcoder.CodeAll0_Temp +
			                   					"\n********临时仿真源码********\n" + Compiler.SimTempSource +
												"\n********不带协程程序********\n" + n_GUIcoder.GUIcoder.CodeAll1_Temp +
												"********初始化和循环********\n" + n_GUIcoder.GUIcoder.InitCode_Temp + n_GUIcoder.GUIcoder.BackLoop_Temp +
												"CodeSysLen: " + n_GUIcoder.GUIcoder.CodeSysLine );
		} catch {}
		
		try { SourceCodeBox.Set( Compiler.Source ); } catch {}
		try { AcidenceBox.Set( WordList.Show() ); } catch {}
		try { ParseBox.Set( ParseNet.Show() ); } catch {}
		try { MidCode1Box.Set( FunctionCodeList.ShowCode() ); } catch {}
		try { ASMBox.Set( string.Join( "\n-------------------------------\n", Compiler.ASMList ) ); } catch {}
		try { HEXBox.Set( string.Join( "\n-------------------------------\n", Compiler.HEXList ) ); } catch {}
		try { SetVMCode(); } catch {}
		try { SetCompList(); } catch {}
	}
	
	void SetVMCode()
	{
		string res = "";
		for( int i = 0; i < n_Decode.Decode.BCLength; i += 4 ) {
			byte b0 = n_Decode.Decode.ByteCodeList[i];
			byte b1 = n_Decode.Decode.ByteCodeList[i+1];
			byte b2 = n_Decode.Decode.ByteCodeList[i+2];
			byte b3 = n_Decode.Decode.ByteCodeList[i+3];
			UInt64 data = b3; data <<= 8;
			data += b2; data <<= 8;
			data += b1; data <<= 8;
			data += b0;
			
			if( (i/4) % 4 == 0 ) {
				res += i.ToString( "X" ).PadLeft( 4, '0' ) + ":  ";
			}
			
			res += data.ToString( "X" ).PadLeft( 8, '0' ) + ", ";
			if( (i/4) % 4 == 3 ) {
				res += "\r\n";
			}
		}
		res += "\r\n\r\n";
		HEXBox.Set( res );
	}
	
	//设置编译结果
	void SetCompList()
	{
		StringBuilder result = new StringBuilder( "" );
		
		result.Append( "****************元件列表:****************\n" );
		try { result.Append( n_UnitList.UnitList.Show() ); } catch {}
		
		result.Append( "****************虚拟类型列表:****************\n" );
		try { result.Append( n_VdataList.VdataList.Show() ); } catch {}
		
		result.Append( "****************地址列表:****************\n" );
		try { result.Append( n_AddressList.AddressList.Show() ); } catch {}
		
		result.Append( "***************结构体列表:***************\n" );
		try { result.Append( n_StructList.StructList.Show() ); } catch {}
		
		result.Append( "****************函数列表:****************\n" );
		try { result.Append( n_FunctionList.FunctionList.Show() ); } catch {}
		
		result.Append( "****************变量列表:****************\n" );
		try { result.Append( n_VarList.VarList.Show() ); } catch {}
		
		result.Append( "****************标号列表:****************\n" );
		try { result.Append( n_LabelList.LabelList.Show() ); } catch {}
		
		result.Append( "****************中断地址列表:****************\n" );
		try { result.Append( n_Interrupt.Interrupt.Show() ); } catch {}
		
		result.Append( "****************ROM常量列表:****************\n" );
		try { result.Append( n_CodeData.CodeData.Show() ); } catch {}
		
		if( n_Config.Config.GetCPU() == n_CPUType.CPUType.VM ) {
			result.Append( "****************VM标签地址列表:****************\n" );
			try { result.Append( n_VM_Assembler.VLabelList.Show() ); } catch {}
		}
		if( n_Config.Config.GetCPU().StartsWith( n_CPUType.CPUType.MEGA_X ) ) {
			result.Append( "****************MEGA标签地址列表:****************\n" );
			try { result.Append( n_ATMEGA_Label.ATMEGA_Label.Show() ); } catch {}
		}
		
		CompListBox.Set( result.ToString() );
	}
	
	//窗体关闭事件
	void MesFormFormClosing(object sender, FormClosingEventArgs e)
	{
		this.Visible = false;
		e.Cancel = true;
	}
	
	//选中输出框事件,让文本框获得焦点
	void MessageTabSelect( object sender, EventArgs e )
	{
		this.tabControl2.SelectedTab.Controls[0].Focus();
		if( this.tabControl2.SelectedTab.Controls[0] is RichTextBox ) {
			G.FindTextBox = (RichTextBox)this.tabControl2.SelectedTab.Controls[0];
		}
	}
	
	//窗体移动事件
	void MesFormMove(object sender, EventArgs e)
	{
		//SystemData.FormLocationList[ SystemData.MesBoxIndex ] = this.Location;
		//SystemData.isChanged = true;
	}
	
	//窗体加载事件
	void MesFormLoad(object sender, EventArgs e)
	{
		//this.Size = SystemData.FormSizeList[ SystemData.MesBoxIndex ];
		//this.Location = SystemData.FormLocationList[ SystemData.MesBoxIndex ];
	}
	
	//窗体尺寸改变事件
	void MesFormResize(object sender, EventArgs e)
	{
		//SystemData.FormSizeList[ SystemData.MesBoxIndex ] = this.Size;
		//SystemData.isChanged = true;
	}
	
	//查找和替换
	void Button查找和替换Click(object sender, EventArgs e)
	{
		if( G.FindBox == null ) {
			G.FindBox = new n_FindForm.FindForm();
		}
		G.FindBox.Run();
	}
}
}



