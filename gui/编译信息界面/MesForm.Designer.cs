﻿/*
 * 由 SharpDevelop 创建。
 * 用户: bbb_2
 * 日期: 2009-11-17
 * 时间: 11:15
 * 
 * 如果你想要更改该模板，那么请使用“工具 | 选项 | 编辑 | 编辑标准标题”。
 */
namespace n_CompileMessageForm
{
	partial class CompileMessageForm
	{
		/// <summary>
		/// Designer variable used to keep track of non-visual components.
		/// </summary>
		private System.ComponentModel.IContainer components = null;
		
		/// <summary>
		/// Disposes resources used by the form.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing) {
				if (components != null) {
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}
		
		/// <summary>
		/// This method is required for Windows Forms designer support.
		/// Do not change the method contents inside the source code editor. The Forms designer might
		/// not be able to load this method if it was changed manually.
		/// </summary>
		private void InitializeComponent()
		{
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(CompileMessageForm));
			this.tabControl2 = new System.Windows.Forms.TabControl();
			this.panel1 = new System.Windows.Forms.Panel();
			this.button查找和替换 = new System.Windows.Forms.Button();
			this.panel1.SuspendLayout();
			this.SuspendLayout();
			// 
			// tabControl2
			// 
			resources.ApplyResources(this.tabControl2, "tabControl2");
			this.tabControl2.Multiline = true;
			this.tabControl2.Name = "tabControl2";
			this.tabControl2.SelectedIndex = 0;
			// 
			// panel1
			// 
			this.panel1.BackColor = System.Drawing.Color.DarkGray;
			this.panel1.Controls.Add(this.button查找和替换);
			resources.ApplyResources(this.panel1, "panel1");
			this.panel1.Name = "panel1";
			// 
			// button查找和替换
			// 
			resources.ApplyResources(this.button查找和替换, "button查找和替换");
			this.button查找和替换.Name = "button查找和替换";
			this.button查找和替换.UseVisualStyleBackColor = true;
			this.button查找和替换.Click += new System.EventHandler(this.Button查找和替换Click);
			// 
			// CompileMessageForm
			// 
			resources.ApplyResources(this, "$this");
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.BackColor = System.Drawing.Color.White;
			this.Controls.Add(this.tabControl2);
			this.Controls.Add(this.panel1);
			this.Name = "CompileMessageForm";
			this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.MesFormFormClosing);
			this.Load += new System.EventHandler(this.MesFormLoad);
			this.Move += new System.EventHandler(this.MesFormMove);
			this.Resize += new System.EventHandler(this.MesFormResize);
			this.panel1.ResumeLayout(false);
			this.ResumeLayout(false);
		}
		private System.Windows.Forms.Button button查找和替换;
		private System.Windows.Forms.Panel panel1;
		private System.Windows.Forms.TabControl tabControl2;
	}
}
