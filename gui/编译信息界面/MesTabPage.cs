﻿
//命名空间
namespace n_MesTabPage
{
using System;
using System.Drawing;
using System.Windows.Forms;

//显示信息类
public class MesTabPage : TabPage
{
	//编译器软件启动初始化
	public MesTabPage( string Name )
	{
		OText = new RichTextBox();
		OText.Dock = DockStyle.Fill;
		OText.Visible = true;
		//OText.ReadOnly = !SystemData.isSuperUser;
		OText.Font = new Font( "Consolas", 11, FontStyle.Regular );
		
		OText.BorderStyle = BorderStyle.None;
		
		this.Controls.Add( OText );
		this.BorderStyle = BorderStyle.None;
		this.Text = Name;
	}
	
	//设置显示信息
	public void Set( string Message )
	{
		OText.Text = Message;
		OText.SelectAll();
		OText.SelectionFont = OText.Font;
		OText.SelectionStart = 0;
		OText.SelectionLength = 0;
	}
	
	RichTextBox OText;		//输出框
}
}



