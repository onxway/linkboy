﻿/*
 * 由 SharpDevelop 创建。
 * 用户: bbb_2
 * 日期: 2009-11-17
 * 时间: 11:15
 * 
 * 如果你想要更改该模板，那么请使用“工具 | 选项 | 编辑 | 编辑标准标题”。
 */
namespace n_CodeViewForm
{
	partial class CodeViewForm
	{
		/// <summary>
		/// Designer variable used to keep track of non-visual components.
		/// </summary>
		private System.ComponentModel.IContainer components = null;
		
		/// <summary>
		/// Disposes resources used by the form.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing) {
				if (components != null) {
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}
		
		/// <summary>
		/// This method is required for Windows Forms designer support.
		/// Do not change the method contents inside the source code editor. The Forms designer might
		/// not be able to load this method if it was changed manually.
		/// </summary>
		private void InitializeComponent()
		{
			this.components = new System.ComponentModel.Container();
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(CodeViewForm));
			this.panel1 = new System.Windows.Forms.Panel();
			this.buttonDrawType = new System.Windows.Forms.Button();
			this.buttonSaveAs = new System.Windows.Forms.Button();
			this.buttonOpen = new System.Windows.Forms.Button();
			this.buttonPCStep = new System.Windows.Forms.Button();
			this.buttonStep = new System.Windows.Forms.Button();
			this.buttonResult = new System.Windows.Forms.Button();
			this.splitContainer1 = new System.Windows.Forms.SplitContainer();
			this.richTextBox1 = new System.Windows.Forms.RichTextBox();
			this.timer1 = new System.Windows.Forms.Timer(this.components);
			this.panel1.SuspendLayout();
			//((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).BeginInit();
			this.splitContainer1.Panel1.SuspendLayout();
			this.splitContainer1.SuspendLayout();
			this.SuspendLayout();
			// 
			// panel1
			// 
			this.panel1.BackColor = System.Drawing.Color.WhiteSmoke;
			this.panel1.Controls.Add(this.buttonDrawType);
			this.panel1.Controls.Add(this.buttonSaveAs);
			this.panel1.Controls.Add(this.buttonOpen);
			this.panel1.Controls.Add(this.buttonPCStep);
			this.panel1.Controls.Add(this.buttonStep);
			this.panel1.Controls.Add(this.buttonResult);
			resources.ApplyResources(this.panel1, "panel1");
			this.panel1.Name = "panel1";
			// 
			// buttonDrawType
			// 
			this.buttonDrawType.BackColor = System.Drawing.Color.Gainsboro;
			resources.ApplyResources(this.buttonDrawType, "buttonDrawType");
			this.buttonDrawType.Name = "buttonDrawType";
			this.buttonDrawType.UseVisualStyleBackColor = false;
			this.buttonDrawType.Click += new System.EventHandler(this.ButtonDrawTypeClick);
			// 
			// buttonSaveAs
			// 
			this.buttonSaveAs.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(192)))));
			resources.ApplyResources(this.buttonSaveAs, "buttonSaveAs");
			this.buttonSaveAs.Name = "buttonSaveAs";
			this.buttonSaveAs.UseVisualStyleBackColor = false;
			this.buttonSaveAs.Click += new System.EventHandler(this.ButtonSaveAsClick);
			// 
			// buttonOpen
			// 
			this.buttonOpen.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(192)))));
			resources.ApplyResources(this.buttonOpen, "buttonOpen");
			this.buttonOpen.Name = "buttonOpen";
			this.buttonOpen.UseVisualStyleBackColor = false;
			this.buttonOpen.Click += new System.EventHandler(this.ButtonOpenClick);
			// 
			// buttonPCStep
			// 
			this.buttonPCStep.BackColor = System.Drawing.Color.Gainsboro;
			resources.ApplyResources(this.buttonPCStep, "buttonPCStep");
			this.buttonPCStep.Name = "buttonPCStep";
			this.buttonPCStep.UseVisualStyleBackColor = false;
			this.buttonPCStep.Click += new System.EventHandler(this.ButtonPCStepClick);
			// 
			// buttonStep
			// 
			this.buttonStep.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(128)))));
			resources.ApplyResources(this.buttonStep, "buttonStep");
			this.buttonStep.Name = "buttonStep";
			this.buttonStep.UseVisualStyleBackColor = false;
			this.buttonStep.Click += new System.EventHandler(this.ButtonStepClick);
			// 
			// buttonResult
			// 
			this.buttonResult.BackColor = System.Drawing.Color.Gainsboro;
			resources.ApplyResources(this.buttonResult, "buttonResult");
			this.buttonResult.Name = "buttonResult";
			this.buttonResult.UseVisualStyleBackColor = false;
			this.buttonResult.Click += new System.EventHandler(this.ButtonResultClick);
			// 
			// splitContainer1
			// 
			this.splitContainer1.BackColor = System.Drawing.Color.Gainsboro;
			resources.ApplyResources(this.splitContainer1, "splitContainer1");
			this.splitContainer1.Name = "splitContainer1";
			// 
			// splitContainer1.Panel1
			// 
			this.splitContainer1.Panel1.Controls.Add(this.richTextBox1);
			// 
			// richTextBox1
			// 
			this.richTextBox1.AcceptsTab = true;
			this.richTextBox1.BorderStyle = System.Windows.Forms.BorderStyle.None;
			resources.ApplyResources(this.richTextBox1, "richTextBox1");
			this.richTextBox1.Name = "richTextBox1";
			this.richTextBox1.KeyDown += new System.Windows.Forms.KeyEventHandler(this.RichTextBox1KeyDown);
			// 
			// timer1
			// 
			this.timer1.Interval = 50;
			this.timer1.Tick += new System.EventHandler(this.Timer1Tick);
			// 
			// CodeViewForm
			// 
			resources.ApplyResources(this, "$this");
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.BackColor = System.Drawing.Color.White;
			this.Controls.Add(this.splitContainer1);
			this.Controls.Add(this.panel1);
			this.Name = "CodeViewForm";
			this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.FindFormFormClosing);
			this.panel1.ResumeLayout(false);
			this.splitContainer1.Panel1.ResumeLayout(false);
			//((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).EndInit();
			this.splitContainer1.ResumeLayout(false);
			this.ResumeLayout(false);
		}
		private System.Windows.Forms.Timer timer1;
		private System.Windows.Forms.Button buttonDrawType;
		private System.Windows.Forms.RichTextBox richTextBox1;
		private System.Windows.Forms.Button buttonOpen;
		private System.Windows.Forms.Button buttonSaveAs;
		private System.Windows.Forms.Button buttonPCStep;
		private System.Windows.Forms.Button buttonStep;
		private System.Windows.Forms.Button buttonResult;
		public System.Windows.Forms.SplitContainer splitContainer1;
		private System.Windows.Forms.Panel panel1;
	}
}
