﻿
using System;
using System.Drawing;
using n_GUIset;

namespace n_PyVar
{
//============================================================
public static class PyVar
{
	public static PyStack[] PyStackList;
	public static PyFrame[] PyFrameList;
	
	//虚拟机内存
	public static byte[] BASE;
	
	//---------------------------------------------------
	
	public static int CLine;
	
	public static int Loc_Start;
	
	public static int PCAddr;
	public static int PC;
	
	public static int SPAddr;
	public static int SP;
	
	public static int StackAddr;
	public static int RAMAddr;
	public static int FrameAddr;
	
	public static int HIndexAddr;
	public static int HIDAddr;
	public static int HeapAddr;
	
	public static int DrawType;
	
	//初始化
	public static void Init()
	{
		DrawType = 1;
		
		BASE = n_UserModule.UserModule.BASE;
		
		PyStackList = new PyStack[20];
		for( int i = 0; i < PyStackList.Length; ++i ) {
			PyStack pf = new PyStack( i );
			pf.SX = 0;
			pf.SY = i * 25;
			PyStackList[i] = pf;
			
			pf.Target.SX = pf.SX - 120;
			pf.Target.SY = pf.SY;
		}
		
		PyFrameList = new PyFrame[128];
		for( int i = 0; i < PyFrameList.Length; ++i ) {
			PyFrame pf = new PyFrame( i );
			pf.SX = 200;
			pf.SY = i * 50;
			PyFrameList[i] = pf;
			
			pf.Target.SX = pf.SX + 200;
			pf.Target.SY = pf.SY;
		}
	}
	
	//仿真初始化
	public static void ResetSim()
	{
		for( int i = 0; i < PyStackList.Length; ++i ) {
			PyStackList[i].ResetSim();
		}
		for( int i = 0; i < PyFrameList.Length; ++i ) {
			PyFrameList[i].ResetSim();
		}
		
		Loc_Start = n_PyVarList.VarList.StaticAddr + 1;
		
		int idx;
		string addr;
		
		idx = n_VarList.VarList.GetStaticIndex( "#.PYVM.stack" );
		addr = n_VarList.VarList.GetAddr( idx );
		StackAddr = int.Parse( addr );
		
		idx = n_VarList.VarList.GetStaticIndex( "#.PYVM.RAM" );
		addr = n_VarList.VarList.GetAddr( idx );
		RAMAddr = int.Parse( addr );
		
		idx = n_VarList.VarList.GetStaticIndex( "#.PYVM.Frame" );
		addr = n_VarList.VarList.GetAddr( idx );
		FrameAddr = int.Parse( addr );
		
		//--------------------------------------------------------------
		idx = n_VarList.VarList.GetStaticIndex( "#.PYVM.Heap.HID" );
		addr = n_VarList.VarList.GetAddr( idx );
		HIDAddr = int.Parse( addr );
		
		idx = n_VarList.VarList.GetStaticIndex( "#.PYVM.Heap.buffer" );
		addr = n_VarList.VarList.GetAddr( idx );
		HeapAddr = int.Parse( addr );
		
		idx = n_VarList.VarList.GetStaticIndex( "#.PYVM.Heap.CIndex" );
		addr = n_VarList.VarList.GetAddr( idx );
		HIndexAddr = int.Parse( addr );
		//--------------------------------------------------------------
		
		idx = n_VarList.VarList.GetStaticIndex( "#.PYVM.PC" );
		addr = n_VarList.VarList.GetAddr( idx );
		PCAddr = int.Parse( addr );
		
		idx = n_VarList.VarList.GetStaticIndex( "#.PYVM.SP" );
		addr = n_VarList.VarList.GetAddr( idx );
		SPAddr = int.Parse( addr );
		
		//复位静态变量对应的视图元素
		float SY = 10;
		float SX = 20;
		float ox = 0;
		float oy = 0;
		DRef.GetDir( 0, out ox, out oy );
		for( int i = 0; i < n_PyVarList.VarList.length; ++i ) {
			n_PyVarNode.VarNode vn = n_PyVarList.VarList.Get( i );
			if( vn.Static ) {
				PyFrameList[vn.Addr].StaticVarName = vn.Name;
				if( DrawType == 0 ) {
					PyFrameList[vn.Addr].Visible = true;
					//...
				}
				else if( !vn.isFunctionName && vn.Addr != 1 ) {
					PyFrameList[vn.Addr].Visible = true;
					PyFrameList[vn.Addr].SX = SX;
					PyFrameList[vn.Addr].SY = SY;
					PyFrameList[vn.Addr].RefLevel = 0;
					//PyFrameList[vn.Addr].Target.SX = PyFrameList[vn.Addr].SX + 200;
					//PyFrameList[vn.Addr].Target.SY = PyFrameList[vn.Addr].SY;
					
					SX += ox;
					SY += oy;
				}
				else {
					//...
				}
			}
		}
		//调试模式
		if( DrawType == 0 ) {
			for( int i = 0; i < PyFrameList.Length; ++i ) {
				PyFrameList[i].SX = 200;
				PyFrameList[i].SY = i * 30;
				PyFrameList[i].Target.SX = PyFrameList[i].SX + 140;
				PyFrameList[i].Target.SY = PyFrameList[i].SY;
			}
		}
	}
	
	//更新虚拟机状态
	public static void MyRefresh()
	{
		byte pb0 = BASE[ PCAddr + 0 ];
		byte pb1 = BASE[ PCAddr + 1 ];
		PC = pb0 + pb1 * 256;
		
		byte spb0 = BASE[ SPAddr + 0 ];
		byte spb1 = BASE[ SPAddr + 1 ];
		SP = spb0 + spb1 * 256;
		
		//G.CGPanel.MyRefresh();
	}
	
	//实时处理Frame标记
	static void DealFramFlag()
	{
		//本函数将会标记出实时状态下的各个子函数帧标记
		//假设起点为8, 根据函数帧定义, 读取出局部变量数目n, 累加后得到下一个函数的起点, 如此反复
		
		//清除帧数据的所有标记
		for( int i = 0; i < PyFrameList.Length; ++i ) {
			PyFrameList[i].Flag = 0;
			PyFrameList[i].FuncLevel = 0;
		}
		int Start = Loc_Start;
		int lev = 1;
		while( true ) {
			
			if( Start != Loc_Start && PyFrameList[Start].Data == 0 ) {
				break;
			}
			
			int preFs = PyFrameList[Start].Data; PyFrameList[Start].Flag = 1; PyFrameList[Start].FuncLevel = lev;
			int preVs = PyFrameList[Start+1].Data; PyFrameList[Start+1].Flag = 2; PyFrameList[Start+1].FuncLevel = lev;
			int LocNum = PyFrameList[Start+2].Data; PyFrameList[Start+2].Flag = 3; PyFrameList[Start+2].FuncLevel = lev;
			
			for( int i = 0; i < LocNum; ++i ) {
				PyFrameList[Start + 3 + i].Flag = 4;
				 PyFrameList[Start + 3 + i].FuncLevel = lev;
			}
			lev++;
			Start += 3 + LocNum;
		}
	}
	
	//绘制帧数据列表
	public static void Draw( Graphics g )
	{
		try {
		DealFramFlag();
		
		DrawStatus( g );
		
		if( DrawType == 0 ) {
			DrawDebug( g );
		}
		else {
			DrawUser( g );
		}
		}
		catch {
			g.Clear( Color.Orange );
		}
	}
	
	//绘制虚拟机状态 数据预处理
	static void DrawStatus( Graphics g )
	{
		string r = "";
		
		r += "SP:" + SP + "\t\t";
		r += "PC:" + PC + "\t\t";
		
		for( int i = 0; i < PyStackList.Length; ++i ) {
			
			byte b0 = BASE[ StackAddr + i * 2 + 0 ];
			byte b1 = BASE[ StackAddr + i * 2 + 1 ];
			
			PyStackList[i].Data = b0 + b1 * 256;
		}
		
		for( int i = 0; i < PyFrameList.Length; ++i ) {
			
			byte b0 = BASE[ FrameAddr + i * 2 + 0 ];
			byte b1 = BASE[ FrameAddr + i * 2 + 1 ];
			
			PyFrameList[i].Data = b0 + b1 * 256;
		}
		
		g.DrawString( r, GUIset.ExpFont, Brushes.SlateBlue, 0, -30 );
	}
	
	//-------------------------------------------------------
	
	//绘制级别1
	static void DrawDebug( Graphics g )
	{
		for( int i = 0; i < PyStackList.Length; ++i ) {
			PyStackList[i].Target.Deal( PyStackList[i].Data );
			PyStackList[i].DrawDebug( g, i < PyVar.SP );
		}
		for( int i = 0; i < PyFrameList.Length; ++i ) {
			PyFrameList[i].Target.Deal( PyFrameList[i].Data );
			PyFrameList[i].DrawDebug( g );
		}
		DrawHeap( g );
	}
	
	//绘制动态内存分配区
	static void DrawHeap( Graphics g )
	{
		int startX = 500;
		
		byte b00 = BASE[ HIndexAddr + 0 ];
		byte b01 = BASE[ HIndexAddr + 1 ];
		int index = b00 + b01 * 256;
		
		for( int i = 0; i < 2048; i += 2 ) {
			
			int x = startX + (i / 256) * 250;
			int y = (i % 256) * 10;
			
			
			int hid = i / 2;
			int mod = hid % 8;
			hid /= 8;
			byte hmask = BASE[ HIDAddr + hid ];
			bool Use = false;
			if( (hmask & (0x01 << mod)) != 0 ) {
				Use = true;
			}
			
			byte b0 = BASE[ HeapAddr + i ];
			byte b1 = BASE[ HeapAddr + i + 1 ];
			
			string bb0 = (b0 < ' '? "?": ((char)b0).ToString() );
			string bb1 = (b1 < ' '? "?": ((char)b1).ToString() );
			
			string bbb0 = b0.ToString().PadLeft( 3, '0' );
			string bbb1 = b1.ToString().PadLeft( 3, '0' );
			
			string text  =": (" + bb0 + bb1 + ") " + bbb0 + "," + bbb1 + " =" + (b0 + b1 * 256);
			
			if( Use ) {
				g.FillEllipse( Brushes.Orange, x, y + 2, 16, 16 );
				g.DrawString( i.ToString().PadLeft( 3, '0' ) + text, GUIset.ExpFont, Brushes.Black, x + 30, y );
			}
			else {
				g.FillEllipse( Brushes.Silver, x, y + 2, 16, 16 );
				g.DrawString( i.ToString().PadLeft( 3, '0' ) + text, GUIset.ExpFont, Brushes.DarkGray, x + 30, y );
			}
		}
	}
	
	//-------------------------------------------------------
	
	//绘制级别1
	static void DrawUser( Graphics g )
	{
		for( int i = 0; i < PyFrameList.Length; ++i ) {
			PyFrameList[i].Target.Deal( PyFrameList[i].Data );
			PyFrameList[i].DrawUser( g );
		}
	}
}
//============================================================
//属性绘制器
public static class DRef
{
	const float RWidth = 80;
	const float RHeight = 30;
	
	const float RX = 200;
	const float RY = 0;
	
	public static int Angle = 0;
	public static int STAngle = 0;
	
	//根据给定的引用地址绘制其值
	public static void Draw( int Addr, float MidX, float MidY, Graphics g, int OLevel )
	{
		OLevel++;
		
		//绘制引用体的方框
		float SX = MidX - RWidth/2;
		float SY = MidY - RHeight/2;
		
		if( Addr != 0 ) {
			g.FillRectangle( Brushes.LightSteelBlue, SX, SY, RWidth, RHeight );
			g.DrawRectangle( Pens.DarkSlateGray, SX, SY, RWidth, RHeight );
		}
		else {
			g.FillRectangle( Brushes.WhiteSmoke, SX, SY, RWidth, RHeight );
			g.DrawRectangle( Pens.Silver, SX, SY, RWidth, RHeight );
		}
		//绘制引用指针连线
		if( Addr == 0 ) {
			return;
		}
		float lmx = MidX;
		float lmy = MidY;
		
		MidX += RX;
		MidY += RY;
		SX += RX;
		SY += RY;
		
		Addr += PyVar.RAMAddr;
		
		int num = (int)n_Data.Data.GetUint16( PyVar.BASE, Addr + 0 );
		
		int type = (int)n_Data.Data.GetUint16( PyVar.BASE, Addr + 2 );
		type &= 0x00FF;
		int refn = (int)n_Data.Data.GetUint16( PyVar.BASE, Addr + 4 );
		
		Brush TextB = Brushes.Black;
		
		//显示None类型
		if( type == 0 ) {
			long d = n_Data.Data.GetInt32( PyVar.BASE, Addr + 6 );
			g.FillEllipse( Brushes.Red, SX, SY, RWidth, RHeight );
			g.DrawString( d.ToString(), GUIset.ExpFont, Brushes.Silver, SX + 40, SY + 4 );
		}
		//显示整数
		else if( type == 1 ) {
			long d = n_Data.Data.GetInt32( PyVar.BASE, Addr + 6 );
			g.FillRectangle( Brushes.CornflowerBlue, SX, SY, RWidth, RHeight );
			g.DrawRectangle( Pens.MidnightBlue, SX, SY, RWidth, RHeight );
			g.DrawString( d.ToString(), GUIset.ExpFont, TextB, SX + 15, SY + 4 );
		}
		//显示Bool类型
		else if( type == 2 ) {
			long d = n_Data.Data.GetInt32( PyVar.BASE, Addr + 6 );
			g.FillEllipse( Brushes.SandyBrown, SX, SY, RWidth, RHeight );
			g.DrawEllipse( Pens.SaddleBrown, SX, SY, RWidth, RHeight );
			g.DrawString( ((d!= 0)?"True":"False") + (d>1?" (" + d + ")":""), GUIset.ExpFont, TextB, SX + 15, SY + 4 );
		}
		//显示字符串
		else if( type == 3 ) {
			long bytelength = n_Data.Data.GetUint16( PyVar.BASE, Addr + 6 );
			
			long start = n_Data.Data.GetUint16( PyVar.BASE, Addr + 8 );
			
			string s0  = "长度:" + bytelength;
			string s1 = "";
			for( int i = 0; i < bytelength; ++i ) {
				byte b = PyVar.BASE[ PyVar.HeapAddr + start + 2 + i ];
				s1 += ((char)b).ToString();
			}
			g.FillRectangle( Brushes.YellowGreen, SX, SY, RWidth, RHeight );
			g.DrawRectangle( Pens.DarkGreen, SX, SY, RWidth, RHeight );
			g.DrawString( s0, GUIset.ExpFont, TextB, SX + 10, SY + 4 );
			g.DrawString( s1, GUIset.ExpFont, TextB, SX + 100, SY + 4 );
		}
		//显示列表
		else if( type == 4 ) {
			
			MidY += RY;
			SY += RY;
			
			long length = n_Data.Data.GetUint16( PyVar.BASE, Addr + 6 );
			long start = n_Data.Data.GetUint16( PyVar.BASE, Addr + 8 );
			
			string s  = "长度:" + length;
			//g.FillRectangle( Brushes.Pink, SX, SY, RWidth, RHeight );
			//g.DrawRectangle( Pens.Orchid, SX, SY, RWidth, RHeight );
			g.DrawString( s, GUIset.ExpFont, TextB, SX + 15, SY + 4 );
			
			
			float ox = 0;
			float oy = 0;
			GetDir( OLevel, out ox, out oy );
			
			int l = (int)length;
			if( l == 0 ) {
				l = 1;
				g.DrawLine( Pens.Gray, MidX, MidY, MidX + ox*(l), MidY + oy*(l) );
			}
			else {
				g.DrawLine( Pens.Black, MidX, MidY, MidX + ox*(l), MidY + oy*(l) );
			}
			
			for( int i = 0; i < length; ++i ) {
				
				int data = (int)n_Data.Data.GetUint16( PyVar.BASE, PyVar.HeapAddr + (int)start + 2 + i * 2 );
				
				//递归调用元素显示函数
				Draw( data, MidX + ox*i, MidY + oy*i, g, OLevel );
				
				//显示索引
				g.DrawString( "序号:" + i.ToString(), GUIset.ExpFont, Brushes.Black, SX + ox*i, SY + 4 + oy*i );
			}
		}
		else {
			long d = n_Data.Data.GetInt32( PyVar.BASE, Addr + 6 );
			g.DrawString( d.ToString(), GUIset.ExpFont, TextB, SX + 40, SY );
		}
		//绘制引用的导线
		g.DrawLine( Pens.SlateBlue, lmx, lmy, MidX, MidY );
	}
	
	public static void GetDir( int OLevel, out float ox, out float oy )
	{
		int L = 100;
		float a = (float)(Math.PI/2 * (STAngle + OLevel * Angle) / 90);
		
		ox = (int)(L * Math.Sin( a ));
		oy = (int)(L * Math.Cos( a ));
	}
}
//============================================================
//栈空间
public class PyStack
{
	public float Index;
	
	public float SX;
	public float SY;
	public float Width;
	public float Height;
	public int Data;
	
	public PyRAM Target;
	
	//构造函数
	public PyStack( int i )
	{
		Index = i;
		
		Width = 60;
		Height = 20;
		
		Target = new PyRAM();
		
		ResetSim();
	}
	
	//仿真初始化
	public void ResetSim()
	{
		Data = 0;
	}
	
	//绘制
	public void DrawDebug( Graphics g, bool uesd )
	{
		if( uesd ) {
			g.FillRectangle( Brushes.ForestGreen, SX, SY, Width, Height );
			
			g.DrawLine( Pens.ForestGreen, SX + Width, SY + Height / 2, Target.SX + Target.Width, Target.SY + Target.Height/2 );
		}
		else {
			g.FillRectangle( Brushes.Silver, SX, SY, Width, Height );
		}
		
		if( Data % 2 != 0 ) {
			//n_Debug.Debug.Message += "ST:" + Index + "-" + Data + " ";
		}
		
		if( Data != 0 ) {
			Target.Draw( g );
		}
		
		g.DrawRectangle( Pens.Green, SX, SY, Width, Height );
		
		g.DrawString( Data.ToString(), GUIset.ExpFont, Brushes.Black, SX + 5, SY );
	}
}
//============================================================
//帧空间
public class PyFrame
{
	public float Index;
	
	float v_sx;
	float v_sy;
	
	public float SX {
		get {
			return v_sx;
		}
		set {
			v_sx = value;
		}
	}
	public float SY {
		get {
			return v_sy;
		}
		set {
			v_sy = value;
		}
	}
	public float EX {
		get {
			return v_sx + Width;
		}
		set {
			v_sx = value - Width;
		}
	}
	public float EY {
		get {
			return v_sy + Height;
		}
		set {
			v_sy = value - Height;
		}
	}
	public float MidX {
		get {
			return v_sx + Width/2;
		}
		set {
			v_sx = value - Width/2;
		}
	}
	public float MidY {
		get {
			return v_sy + Height/2;
		}
		set {
			v_sy = value - Height/2;
		}
	}
	
	public float Width;
	public float Height;
	public int Data;
	
	//帧标记
	//0:默认		1:Pre函数FS		2:Pre函数VS		3:当前函数VN		4:局部变量
	public int Flag;
	public int FuncLevel;
	
	public string StaticVarName;
	
	public bool Visible;
	
	//所在的距离根元素的引用层级
	public int RefLevel;
	
	//----------------------------------------------------
	//DrawDebug
	public PyRAM Target;
	
	//构造函数
	public PyFrame( int i )
	{
		Index = i;
		
		Width = 60;
		Height = 20;
		
		Target = new PyRAM();
		
		ResetSim();
	}
	
	//仿真初始化
	public void ResetSim()
	{
		Data = 0;
		StaticVarName = null;
		Visible = false;
		
		 RefLevel = 0;
	}
	
	//绘制
	public void DrawDebug( Graphics g )
	{
		SX = 200;
		
		SX += FuncLevel * 10;
		
		bool Ref = false;
		
		if( StaticVarName != null ) {
			Ref = true;
			
			g.DrawString( StaticVarName, GUIset.ExpFont, Brushes.Black, SX - 120, SY );
			g.DrawRectangle( Pens.Blue, SX, SY, Width, Height );
		}
		else {
			if( Flag == 1 || Flag == 2 || Flag == 3 ) {
				g.DrawRectangle( Pens.Purple, SX, SY, Width, Height );
			}
			else if( Flag == 4 ) {
				g.DrawRectangle( Pens.Green, SX, SY, Width, Height );
				Ref = true;
			}
			else {
				g.DrawRectangle( Pens.Silver, SX, SY, Width, Height );
			}
		}
		//绘制引用指针
		if( Ref && Data != 0 ) {
			
			Pen p = Pens.Black;
			if( Index % 3 == 0 ) {
				p = Pens.SlateBlue;
			}
			if( Index % 3 == 1 ) {
				p = Pens.Orange;
			}
			if( Index % 3 == 2 ) {
				p = Pens.Green;
			}
			g.DrawLine( p, SX + Width, SY + Height / 2, Target.SX, Target.SY + Target.Height/2 );
			
			Target.Draw( g );
		}
		
		//绘制数值
		g.DrawString( Data.ToString(), GUIset.ExpFont, Brushes.Black, SX + 5, SY );
		
		SX -= FuncLevel * 10;
		
		//绘制序号
		g.DrawString( Index.ToString(), GUIset.ExpFont, Brushes.Silver, SX - 25, SY );
	}
	
	//绘制
	public void DrawUser( Graphics g )
	{
		if( !Visible ) {
			return;
		}
		
		SX += FuncLevel * 10;
		
		DRef.Draw( Data, MidX, MidY, g, RefLevel );
		
		if( StaticVarName != null ) {
			g.DrawString( StaticVarName, GUIset.ExpFont, Brushes.Black, SX + 5, SY );
		}
		
		SX -= FuncLevel * 10;
	}
}
//============================================================
//对象头部数据内存分配区
public class PyRAM
{
	public int num;
	public int type;
	public int refn;
	
	float v_sx;
	float v_sy;
	
	public float SX {
		get {
			return v_sx;
		}
		set {
			v_sx = value;
		}
	}
	public float SY {
		get {
			return v_sy;
		}
		set {
			v_sy = value;
		}
	}
	public float EX {
		get {
			return v_sx + Width;
		}
		set {
			v_sx = value - Width;
		}
	}
	public float EY {
		get {
			return v_sy + Height;
		}
		set {
			v_sy = value - Height;
		}
	}
	public float MidX {
		get {
			return v_sx + Width/2;
		}
		set {
			v_sx = value - Width/2;
		}
	}
	public float MidY {
		get {
			return v_sy + Height/2;
		}
		set {
			v_sy = value - Height/2;
		}
	}
	
	public float Width;
	public float Height;
	public byte[] DataList;
	
	public const int ClassByteNumber = 10;
	
	public bool isArray;
	
	public PyRAM()
	{
		DataList = new byte[10];
		
		Width = 120;
		Height = 20;
		isArray = false;
	}
	
	//仿真初始化
	public void ResetSim()
	{
		for( int i = 0; i < DataList.Length; ++i ) {
			DataList[i] = 0;
		}
	}
	
	//仿真预处理
	public void Deal( int Addr )
	{
		Addr += PyVar.RAMAddr;
		DataList[0] = PyVar.BASE[Addr+0];
		DataList[1] = PyVar.BASE[Addr+1];
		DataList[2] = PyVar.BASE[Addr+2];
		DataList[3] = PyVar.BASE[Addr+3];
		DataList[4] = PyVar.BASE[Addr+4];
		DataList[5] = PyVar.BASE[Addr+5];
		DataList[6] = PyVar.BASE[Addr+6];
		DataList[7] = PyVar.BASE[Addr+7];
		DataList[8] = PyVar.BASE[Addr+8];
		DataList[9] = PyVar.BASE[Addr+9];
		
		num = (int)n_Data.Data.GetUint16( DataList, 0 );
		
		type = (int)n_Data.Data.GetUint16( DataList, 2 );
		type &= 0x00FF;
		refn = (int)n_Data.Data.GetUint16( DataList, 4 );
	}
	
	//绘制
	public void Draw( Graphics g )
	{
		Brush TypeB = Brushes.OrangeRed;
		Brush RefB = Brushes.CornflowerBlue;
		Brush TextB = Brushes.Black;
		
		if( refn == 0 ) {
			g.DrawRectangle( Pens.Silver, SX, SY, Width, Height );
			TypeB = Brushes.Silver;
			RefB = Brushes.Silver;
			TextB = Brushes.Silver;
		}
		else if( refn == 0xFFFF ) {
			g.DrawRectangle( Pens.Orange, SX, SY, Width, Height );
		}
		else {
			g.DrawRectangle( Pens.Purple, SX, SY, Width, Height );
		}
		
		//绘制序号
		//g.DrawString( (Index * ClassByteNumber).ToString(), GUIset.Font12, Brushes.Silver, SX - 35, SY );
		
		g.DrawString( type.ToString(), GUIset.ExpFont, TypeB, SX, SY );
		g.DrawString( refn.ToString(), GUIset.ExpFont, RefB, SX + 15, SY );
		
		//显示None类型
		if( type == 0 ) {
			long d = n_Data.Data.GetInt32( DataList, 6 );
			g.DrawString( d.ToString(), GUIset.ExpFont, Brushes.Silver, SX + 40, SY );
		}
		//显示整数
		if( type == 1 ) {
			long d = n_Data.Data.GetInt32( DataList, 6 );
			g.DrawString( d.ToString(), GUIset.ExpFont, TextB, SX + 40, SY );
		}
		//显示Bool类型
		if( type == 2 ) {
			long d = n_Data.Data.GetInt32( DataList, 6 );
			g.DrawString( d.ToString() + "(" + ((d!= 0)?"True":"False") + ")", GUIset.ExpFont, TextB, SX + 40, SY );
		}
		//显示字符串
		if( type == 3 ) {
			long length = n_Data.Data.GetUint16( DataList, 6 );
			long start = n_Data.Data.GetUint16( DataList, 8 );
			
			//显示字符串的起点
			g.DrawString( start.ToString(), GUIset.ExpFont, TextB, SX + 40, SY );
			
			string s  = length + ":";
			for( int i = 0; i < length; ++i ) {
				byte b = PyVar.BASE[ PyVar.HeapAddr + start + 2 + i ];
				s += ((char)b).ToString();
			}
			g.DrawString( s, GUIset.ExpFont, TextB, SX + 80, SY );
		}
		//显示列表
		if( type == 4 ) {
			long length = n_Data.Data.GetUint16( DataList, 6 );
			long start = n_Data.Data.GetUint16( DataList, 8 );
			
			//显示字符串的起点
			g.DrawString( "S:" + start + " L:" + length, GUIset.ExpFont, TextB, SX + 40, SY );
		}
	}
	
	//绘制
	public void Draw1( Graphics g )
	{
		if( refn == 0 ) {
			return;
		}
		
		Brush TypeB = Brushes.OrangeRed;
		Brush RefB = Brushes.CornflowerBlue;
		Brush TextB = Brushes.Black;
		
		if( type > 3 ) {
			isArray = true;
		}
		else {
			isArray = false;
		}
		
		g.DrawRectangle( Pens.Purple, SX, SY, Width, Height );
		
		g.DrawString( "T" + type.ToString(), GUIset.ExpFont, TypeB, SX, SY );
		g.DrawString( refn.ToString(), GUIset.ExpFont, RefB, SX + 15, SY );
		
		//显示None类型
		if( type == 0 ) {
			long d = n_Data.Data.GetInt32( DataList, 6 );
			g.DrawString( d.ToString(), GUIset.ExpFont, Brushes.Silver, SX + 40, SY );
		}
		//显示整数
		else if( type == 1 ) {
			long d = n_Data.Data.GetInt32( DataList, 6 );
			g.DrawString( d.ToString(), GUIset.ExpFont, TextB, SX + 40, SY );
		}
		//显示Bool类型
		else if( type == 2 ) {
			long d = n_Data.Data.GetInt32( DataList, 6 );
			g.DrawString( d.ToString() + "(" + ((d!= 0)?"True":"False") + ")", GUIset.ExpFont, TextB, SX + 40, SY );
		}
		//显示字符串
		else if( type == 3 ) {
			long bytelength = n_Data.Data.GetUint16( DataList, 6 );
			long start = n_Data.Data.GetUint16( DataList, 8 );
			
			string s  = "长度:" + bytelength + "  ";
			for( int i = 0; i < bytelength; ++i ) {
				byte b = PyVar.BASE[ PyVar.HeapAddr + start + 2 + i ];
				s += ((char)b).ToString();
			}
			g.DrawString( s, GUIset.ExpFont, TextB, SX + 80, SY );
		}
		//显示列表
		else if( type == 4 ) {
			long length = n_Data.Data.GetUint16( DataList, 6 );
			long start = n_Data.Data.GetUint16( DataList, 8 );
			
			string s  = "长度:" + length;
			g.DrawString( s, GUIset.ExpFont, TextB, SX + 40, SY );
			
			for( int i = 0; i < length; ++i ) {
				
				long data = n_Data.Data.GetUint16( PyVar.BASE, PyVar.HeapAddr + (int)start + 2 + i * 2 );
				
				g.DrawRectangle( Pens.Black, SX + 120, SY + 2 + i * 35, 48, Height - 4 );
				g.DrawString( data.ToString(), GUIset.ExpFont, Brushes.Black, SX + 120, SY + 2 + i * 35 );
				
				//绘制引用指针
				if( data != 0 ) {
					
					//PyRAM pr = PyVar.PyRAMList[data/ClassByteNumber];
					
					//pr.Draw1( g, EX, MidY + i * 35 );
					//g.DrawLine( Pens.SlateBlue, SX + 120 + 48, MidY + i * 35, pr.SX, pr.MidY );
				}
			}
		}
		else {
			long d = n_Data.Data.GetInt32( DataList, 6 );
			g.DrawString( d.ToString(), GUIset.ExpFont, TextB, SX + 40, SY );
		}
	}
}
}














