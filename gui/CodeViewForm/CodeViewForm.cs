﻿
namespace n_CodeViewForm
{
using System;
using System.Text;
using System.Windows.Forms;
using System.IO;
using n_MainSystemData;
using n_PyPanel;
using n_PyVM;

using n_OS;

/// <summary>
/// Description of WorkForm.
/// </summary>
public partial class CodeViewForm : Form
{
	/*
	public delegate void DE_SetUserText( string s );
	public DE_SetUserText D_SetUserText;
	public delegate string DE_GetUserText();
	public DE_GetUserText D_GetUserText;
	*/
	
	PyPanel GPanel;
	
	//主窗口
	public CodeViewForm()
	{
		//
		// The InitializeComponent() call is required for Windows Forms designer support.
		//
		InitializeComponent();
		
		GPanel = new PyPanel();
		splitContainer1.Panel2.Controls.Add( GPanel );
	}
	
	//初始化仿真参数
	public void ResetSim()
	{
		GPanel.ResetSim();
	}
	
	//运行, 显示代码
	public void Run()
	{
		timer1.Enabled = true;
		
		Visible = true;
		
		//mmTimer1.Start();
	}
	
	//设置用户代码
	public void SetUserText( string s )
	{
		richTextBox1.Text = s;
		
		try {
			richTextBox1.SelectAll();
			richTextBox1.SelectionFont = new System.Drawing.Font( "Consolas", 12 );
			richTextBox1.Font = new System.Drawing.Font( "Consolas", 12 );
		}
		catch {
			//...
		}
		richTextBox1.SelectionStart = 0;
		richTextBox1.SelectionLength = 0;
	}
	
	//获取用户代码
	public string GetUserText()
	{
		return richTextBox1.Text;
	}
	
	//设置当前将要执行的代码行
	void SetCLine( int line )
	{
		Text = line.ToString();
		
		int sindex = richTextBox1.GetFirstCharIndexFromLine( line );
		richTextBox1.SelectionStart = sindex;
		richTextBox1.SelectionLength = richTextBox1.Lines[line].Length;
		
		richTextBox1.Focus();
	}
	
	//===================================================================================
	
	//窗体关闭事件
	void FindFormFormClosing(object sender, FormClosingEventArgs e)
	{
		timer1.Enabled = false;
		this.Visible = false;
		e.Cancel = true;
	}
	
	void MmTimer1Tick(object sender, EventArgs e)
	{
		if( !G.SimulateMode ) {
			return;
		}
		
		//GPanel.MyRefresh();
	}
	
	void ButtonResultClick(object sender, EventArgs e)
	{
		//获取编译字节码
		G.ccode.UserText = GetUserText();
		string s = n_GUIcoder.GUIcoder.GetUserProgram();
		
		//获取编译结果
		StringBuilder result = new StringBuilder( "" );
		
		//result.Append( "****************元件列表:****************\n" );
		//try { result.Append( n_PyUnitList.UnitList.Show() ); } catch {}
		
		//result.Append( "****************虚拟类型列表:****************\n" );
		//try { result.Append( n_PyVdataList.VdataList.Show() ); } catch {}
		
		//result.Append( "***************结构体列表:***************\n" );
		//try { result.Append( n_PyStructList.StructList.Show() ); } catch {}
		
		result.Append( "****************类列表:****************\n" );
		try { result.Append( n_PyUnitList.UnitList.Show() ); } catch {}
		
		result.Append( "****************函数列表:****************\n" );
		try { result.Append( n_PyFunctionList.FunctionList.Show() ); } catch {}
		
		result.Append( "****************变量列表:****************\n" );
		try { result.Append( n_PyVarList.VarList.Show() ); } catch {}
		
		result.Append( "****************标号列表:****************\n" );
		try { result.Append( n_PyLabelList.LabelList.Show() ); } catch {}
		
		result.Append( "****************词法列表:****************\n" );
		try { result.Append( n_PyWordList.WordList.Show() ); } catch {}
		
		result.Append( "****************语法列表:****************\n" );
		try { result.Append( n_PyParseNet.ParseNet.Show() ); } catch {}
		
		result.Append( "****************源程序:****************\n" );
		try { result.Append( n_PyAccidence.Accidence.Source ); } catch {}
		
		G.CodeBox.SetResult( result.ToString() );
		
		G.CodeBox.Run();
	}
	
	void ButtonStepClick(object sender, EventArgs e)
	{
		if( PyVM.isOk ) {
			return;
		}
		PyVM.PCStep = false;
		PyVM.isOk = true;
		
		int tick = 0;
		while( PyVM.isOk && tick < 10 ) {
			System.Threading.Thread.Sleep( 50 );
			tick++;
		}
		n_PyVar.PyVar.MyRefresh();
		GPanel.MyRefresh();
		SetCLine( n_PyVar.PyVar.CLine - 1 );
	}
	
	void ButtonPCStepClick(object sender, EventArgs e)
	{
		if( PyVM.isOk ) {
			return;
		}
		PyVM.PCStep = true;
		PyVM.isOk = true;
		
		int tick = 0;
		while( PyVM.isOk && tick < 10 ) {
			System.Threading.Thread.Sleep( 50 );
			tick++;
		}
		n_PyVar.PyVar.MyRefresh();
		GPanel.MyRefresh();
		G.CodeBox.SetLine( n_PyVar.PyVar.PC );
		G.CodeBox.Activate();
	}
	
	void ButtonOpenClick(object sender, EventArgs e)
	{
		//打开文件对话框
		OpenFileDialog OpenFileDlg = new OpenFileDialog();
		OpenFileDlg.Filter = "python程序文件|*.py|所有文件|*.*";
		OpenFileDlg.Title = "请选择文件";
		OpenFileDlg.InitialDirectory = n_OS.OS.SystemRoot + "\\python";
		
		DialogResult dlgResult = OpenFileDlg.ShowDialog();
		if(dlgResult == DialogResult.OK) {
			
			string code = VIO.OpenTextFileGB2312( OpenFileDlg.FileName );
			SetUserText( code );
		}
	}
	
	void ButtonSaveAsClick(object sender, EventArgs e)
	{
		//保存文件对话框
		SaveFileDialog SaveFileDlg = new SaveFileDialog();
		SaveFileDlg.Filter = "python程序文件|*.py|所有文件|*.*";
		SaveFileDlg.Title = "文件另存为...";
		
		SaveFileDlg.FileName = n_CCode.CCode.GetLinkboyFileName();
		DialogResult dlgResult = SaveFileDlg.ShowDialog();
		if(dlgResult == DialogResult.OK) {
			string FilePath = SaveFileDlg.FileName;
			if( !FilePath.ToLower().EndsWith( ".py" ) ) {
				FilePath += "." + OS.LinkboyFile;
			}
			
			VIO.SaveTextFileGB2312( FilePath, GetUserText() );
		}
	}
	
	void RichTextBox1KeyDown(object sender, KeyEventArgs e)
	{
		if( G.ccode != null ) {
			G.ccode.isChanged = true;
		}
	}
	
	void ButtonDrawTypeClick(object sender, EventArgs e)
	{
		n_PyVar.PyVar.DrawType++;
		if( n_PyVar.PyVar.DrawType == 2 ) {
			n_PyVar.PyVar.DrawType = 0;
		}
	}
	
	void Timer1Tick(object sender, EventArgs e)
	{
		GPanel.MyRefresh();
	}
}
}


