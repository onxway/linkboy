﻿
namespace n_LIDEForm
{
using System;
using System.Globalization;
using System.Windows.Forms;
using n_ImagePanel;
using System.Drawing;
using System.Drawing.Drawing2D;
using c_FormMover;
using n_LCC;

/// <summary>
/// Description of WorkForm.
/// </summary>
public partial class LIDEForm : Form
{
	string FileName;
	string FileNameNoExt;
	bool Changed;
	
	bool isOK;
	string code;
	n_GNote.GNote myOwner;
	
	//主窗口
	public LIDEForm()
	{
		InitializeComponent();
		//this.ImeMode = ImeMode.Off;
		
		//fm = new FormMover( this );
		
		try {
			richTextBoxSource.Font = new Font( "consolas", 11 );
			richTextBoxPTree.Font = new Font( "consolas", 11 );
			richTextBoxHEX.Font = new Font( "consolas", 11 );
			richTextBoxWord.Font = new Font( "consolas", 11 );
		}
		catch {
			
		}
		LCC.Init();
	}
	
	//运行
	public string Run( n_GNote.GNote o, string c )
	{
		myOwner = o;
		code = c;
		
		if( myOwner != null ) {
			buttonOpenFile.Visible = false;
			checkBoxHEX.Visible = false;
			buttonSave.Text = "确定";
			richTextBoxSource.Text = code;
		}
		else {
			this.Text = FileName;
			buttonOpenFile.Visible = true;
			checkBoxHEX.Visible = true;
			buttonSave.Text = "保存";
		}
		
		isOK = false;
		this.Visible = true;
		//this.ShowDialog();
		
		while( this.Visible ) {
			System.Windows.Forms.Application.DoEvents();
		}
		if( isOK ) {
			return this.richTextBoxSource.Text;
		}
		return null;
	}
	
	//窗体退出
	void NoteFormFormClosing(object sender, FormClosingEventArgs e)
	{
		if( myOwner == null && Changed ) {
			DialogResult dr = MessageBox.Show( "文件已被修改, 是否保存?", "保存提示", MessageBoxButtons.YesNoCancel );
			if( dr == DialogResult.Yes ) {
				n_OS.VIO.SaveTextFileGB2312( FileName, richTextBoxSource.Text );
				Changed = false;
			}
			else if( dr == DialogResult.Cancel ) {
				return;
			}
			else {
				Changed = false;
				//... 不保存
			}
		}
		
		e.Cancel = true;
		this.richTextBoxSource.Focus();
		this.Visible = false;
	}
	
	//设置文件名
	void SetFileName( string f )
	{
		FileName = f;
		FileNameNoExt = FileName.Remove( FileName.LastIndexOf( "." ) );
		Text = "已打开文件: " + FileName;
	}
	
	//打开程序文件
	void ButtonOpenFileClick(object sender, EventArgs e)
	{
		//打开文件对话框
		OpenFileDialog OpenFileDlg = new OpenFileDialog();
		OpenFileDlg.Filter = "文本类型文件 | *.*";
		OpenFileDlg.Title = "请选择文件";
		OpenFileDlg.InitialDirectory = G.ccode.FilePath;
		
		DialogResult dlgResult = OpenFileDlg.ShowDialog();
		if(dlgResult == DialogResult.OK) {
			
			string ssFileName = OpenFileDlg.FileName;
			
			if( ssFileName.ToLower().EndsWith( ".lab" ) || ssFileName.ToLower().EndsWith( ".hex" ) ) {
				MessageBox.Show( "请打开txt文件!" );
				return;
			}
			//设置文件名
			SetFileName( ssFileName );
			
			string code = n_OS.VIO.OpenTextFileGB2312( FileName );
			richTextBoxSource.Text = code;
			
			Changed = false;
		}
	}
	
	//生成代码
	public void Button生成Click(object sender, EventArgs e)
	{
		if( myOwner == null ) {
			if( FileName == null ) {
				
				//弹出保存对话框
				SaveFileDialog SaveFileDlg = new SaveFileDialog();
				SaveFileDlg.Filter = "文本类型文件 | *.*";
				SaveFileDlg.Title = "请选择文件";
				SaveFileDlg.InitialDirectory = G.ccode.FilePath;
				
				DialogResult dlgResult = SaveFileDlg.ShowDialog();
				if(dlgResult == DialogResult.OK) {
					string f = SaveFileDlg.FileName;
					if( !f.ToLower().EndsWith( ".txt" ) ) {
						f += ".txt";
					}
					SetFileName( f );
				}
				else {
					return;
				}
			}
			n_OS.VIO.SaveTextFileGB2312( FileName, richTextBoxSource.Text );
		}
		
		Changed = false;
		
		labelMes.Text = "";
		richTextBoxWord.Text = "";
		richTextBoxParse.Text = "";
		richTextBoxASM.Text = "";
		richTextBoxHEX.Text = "";
		richTextBoxCCMes.Text = "";
		
		string ErrMes = Compile( richTextBoxSource.Text );
		
		richTextBoxWord.Text = LCC.WordResult;
		richTextBoxParse.Text = LCC.ParseResult;
		richTextBoxASM.Text = LCC.ASMResult;
		richTextBoxHEX.Text = LCC.HexResult;
		richTextBoxCCMes.Text = LCC.CompMes;
		
		if( ErrMes != null ) {
			labelMes.ForeColor = Color.Red;
			labelMes.Text += ErrMes;
		}
		else {
			labelMes.ForeColor = Color.Black;
			
			if( checkBoxHEX.Checked ) {
				n_OS.VIO.SaveTextFileGB2312( FileNameNoExt + ".hex", LCC.HexResult );
				labelMes.Text = "完成:\n" + FileNameNoExt + ".hex";
			}
			else {
				labelMes.Text = "完成";
			}
		}
	}
	
	public string Compile( string source )
	{
		LCC.Reset();
		string rr = LCC.SetParser( richTextBoxPTree.Text );
		
		string ErrMes = null;
		
		//预处理
		string[] c = source.Split( '\n' );
		LCC.SourceResult = "";
		for( int i = 0; i < c.Length; ++i ) {
			if( c[i].StartsWith( "#include " ) ) {
				string name = c[i].Remove( 0, 9 );
				if( !name.StartsWith( "\"" ) || !name.EndsWith( "\"" ) ) {
					ErrMes += "#include语句格式不正确: " + name + "\n";
					continue;
				}
				name = name.Remove( name.Length - 1 ).Remove( 0, 1 );
				string code = n_OS.VIO.OpenTextFileGB2312( n_OS.OS.ModuleLibPath + "FPGA\\bitlib\\" + name );
				LCC.SourceResult += code + "\n";
			}
			else {
				LCC.SourceResult += c[i] + "\n";
			}
		}
		
		//语法分析
		string[] s = LCC.SourceResult.Split( '\n' );
		for( int i = 0; i < s.Length; ++i ) {
			string line = s[i];
			
			//语法分析
			string err = LCC.M_Parse( line );
			if( err != null ) {
				ErrMes += err + "\n";
				continue;
			}
		}
		//汇编器
		if( ErrMes == null ) {
			
			string code = null;
			
			//提取标签
			string err = LCC.M_GetLabel( LCC.ASMResult, ref code );
			if( err != null ) {
				ErrMes += err + "\n";
			}
			LCC.FuncIndex = -1;
			string[] ss = code.Split( '\n' );
			for( int i = 0; i < ss.Length; ++i ) {
				string line = ss[i];
				
				line = line.Trim( ' ' );
				if( line == "" ) {
					continue;
				}
				if( line == "0" ) {
					LCC.FuncIndex++;
				}
				//宏替换
				err = LCC.M_RepLabel( ref line );
				if( err != null ) {
					ErrMes += err + "\n";
					continue;
				}
				//输出到hex文件流
				err = LCC.M_ToHexFile( line );
				if( err != null ) {
					ErrMes += err + "\n";
					continue;
				}
			}
			LCC.HexResult = "[]#.code uint8 CodeList = {\n" + (LCC.hexnumer*2%256) + ", " + (LCC.hexnumer*2/256) + ",\n" + LCC.HexResult + "\n};\n";
		}
		return ErrMes;
	}
	
	//文本改变
	void RichTextBoxASMTextChanged(object sender, EventArgs e)
	{
		Changed = true;
	}
	
	//保存
	void ButtonSaveClick(object sender, EventArgs e)
	{
		if( myOwner != null ) {
			isOK = true;
			Visible = false;
			return;
		}
		if( FileName == null ) {
			
			//弹出保存对话框
			SaveFileDialog SaveFileDlg = new SaveFileDialog();
			SaveFileDlg.Filter = "文本类型文件 | *.*";
			SaveFileDlg.Title = "请选择文件";
			SaveFileDlg.InitialDirectory = G.ccode.FilePath;
			
			DialogResult dlgResult = SaveFileDlg.ShowDialog();
			if(dlgResult == DialogResult.OK) {
				string f = SaveFileDlg.FileName;
				if( !f.ToLower().EndsWith( ".txt" ) ) {
					f += ".txt";
				}
				SetFileName( f );
			}
			else {
				return;
			}
		}
		n_OS.VIO.SaveTextFileGB2312( FileName, richTextBoxSource.Text );
		Changed = false;
	}
}
}



