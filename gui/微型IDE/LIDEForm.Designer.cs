﻿/*
 * 由 SharpDevelop 创建。
 * 用户: bbb_2
 * 日期: 2009-11-17
 * 时间: 11:15
 * 
 * 如果你想要更改该模板，那么请使用“工具 | 选项 | 编辑 | 编辑标准标题”。
 */
namespace n_LIDEForm
{
	partial class LIDEForm
	{
		/// <summary>
		/// Designer variable used to keep track of non-visual components.
		/// </summary>
		private System.ComponentModel.IContainer components = null;
		
		/// <summary>
		/// Disposes resources used by the form.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing) {
				if (components != null) {
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}
		
		/// <summary>
		/// This method is required for Windows Forms designer support.
		/// Do not change the method contents inside the source code editor. The Forms designer might
		/// not be able to load this method if it was changed manually.
		/// </summary>
		private void InitializeComponent()
		{
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(LIDEForm));
			this.button确定 = new System.Windows.Forms.Button();
			this.richTextBoxSource = new System.Windows.Forms.RichTextBox();
			this.panel2 = new System.Windows.Forms.Panel();
			this.checkBoxHEX = new System.Windows.Forms.CheckBox();
			this.labelMes = new System.Windows.Forms.Label();
			this.buttonSave = new System.Windows.Forms.Button();
			this.buttonOpenFile = new System.Windows.Forms.Button();
			this.splitContainer1 = new System.Windows.Forms.SplitContainer();
			this.tabControl1 = new System.Windows.Forms.TabControl();
			this.tabPage1 = new System.Windows.Forms.TabPage();
			this.tabPage2 = new System.Windows.Forms.TabPage();
			this.richTextBoxPTree = new System.Windows.Forms.RichTextBox();
			this.label1 = new System.Windows.Forms.Label();
			this.tabControl2 = new System.Windows.Forms.TabControl();
			this.tabPage4 = new System.Windows.Forms.TabPage();
			this.richTextBoxWord = new System.Windows.Forms.RichTextBox();
			this.tabPage5 = new System.Windows.Forms.TabPage();
			this.richTextBoxParse = new System.Windows.Forms.RichTextBox();
			this.tabPage6 = new System.Windows.Forms.TabPage();
			this.richTextBoxASM = new System.Windows.Forms.RichTextBox();
			this.tabPage7 = new System.Windows.Forms.TabPage();
			this.richTextBoxHEX = new System.Windows.Forms.RichTextBox();
			this.tabPage3 = new System.Windows.Forms.TabPage();
			this.richTextBoxCCMes = new System.Windows.Forms.RichTextBox();
			this.label2 = new System.Windows.Forms.Label();
			this.panel2.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).BeginInit();
			this.splitContainer1.Panel1.SuspendLayout();
			this.splitContainer1.Panel2.SuspendLayout();
			this.splitContainer1.SuspendLayout();
			this.tabControl1.SuspendLayout();
			this.tabPage1.SuspendLayout();
			this.tabPage2.SuspendLayout();
			this.tabControl2.SuspendLayout();
			this.tabPage4.SuspendLayout();
			this.tabPage5.SuspendLayout();
			this.tabPage6.SuspendLayout();
			this.tabPage7.SuspendLayout();
			this.tabPage3.SuspendLayout();
			this.SuspendLayout();
			// 
			// button确定
			// 
			this.button确定.BackColor = System.Drawing.Color.White;
			resources.ApplyResources(this.button确定, "button确定");
			this.button确定.ForeColor = System.Drawing.Color.Black;
			this.button确定.Name = "button确定";
			this.button确定.UseVisualStyleBackColor = false;
			this.button确定.Click += new System.EventHandler(this.Button生成Click);
			// 
			// richTextBoxSource
			// 
			this.richTextBoxSource.AcceptsTab = true;
			this.richTextBoxSource.BorderStyle = System.Windows.Forms.BorderStyle.None;
			resources.ApplyResources(this.richTextBoxSource, "richTextBoxSource");
			this.richTextBoxSource.Name = "richTextBoxSource";
			this.richTextBoxSource.TextChanged += new System.EventHandler(this.RichTextBoxASMTextChanged);
			// 
			// panel2
			// 
			this.panel2.BackColor = System.Drawing.Color.WhiteSmoke;
			this.panel2.Controls.Add(this.checkBoxHEX);
			this.panel2.Controls.Add(this.labelMes);
			this.panel2.Controls.Add(this.buttonSave);
			this.panel2.Controls.Add(this.buttonOpenFile);
			this.panel2.Controls.Add(this.button确定);
			resources.ApplyResources(this.panel2, "panel2");
			this.panel2.Name = "panel2";
			// 
			// checkBoxHEX
			// 
			resources.ApplyResources(this.checkBoxHEX, "checkBoxHEX");
			this.checkBoxHEX.Name = "checkBoxHEX";
			this.checkBoxHEX.UseVisualStyleBackColor = true;
			// 
			// labelMes
			// 
			resources.ApplyResources(this.labelMes, "labelMes");
			this.labelMes.Name = "labelMes";
			// 
			// buttonSave
			// 
			this.buttonSave.BackColor = System.Drawing.Color.White;
			resources.ApplyResources(this.buttonSave, "buttonSave");
			this.buttonSave.ForeColor = System.Drawing.Color.Black;
			this.buttonSave.Name = "buttonSave";
			this.buttonSave.UseVisualStyleBackColor = false;
			this.buttonSave.Click += new System.EventHandler(this.ButtonSaveClick);
			// 
			// buttonOpenFile
			// 
			this.buttonOpenFile.BackColor = System.Drawing.Color.White;
			resources.ApplyResources(this.buttonOpenFile, "buttonOpenFile");
			this.buttonOpenFile.ForeColor = System.Drawing.Color.Black;
			this.buttonOpenFile.Name = "buttonOpenFile";
			this.buttonOpenFile.UseVisualStyleBackColor = false;
			this.buttonOpenFile.Click += new System.EventHandler(this.ButtonOpenFileClick);
			// 
			// splitContainer1
			// 
			this.splitContainer1.BackColor = System.Drawing.Color.WhiteSmoke;
			resources.ApplyResources(this.splitContainer1, "splitContainer1");
			this.splitContainer1.Name = "splitContainer1";
			// 
			// splitContainer1.Panel1
			// 
			this.splitContainer1.Panel1.Controls.Add(this.tabControl1);
			this.splitContainer1.Panel1.Controls.Add(this.label1);
			// 
			// splitContainer1.Panel2
			// 
			this.splitContainer1.Panel2.Controls.Add(this.tabControl2);
			this.splitContainer1.Panel2.Controls.Add(this.label2);
			// 
			// tabControl1
			// 
			this.tabControl1.Controls.Add(this.tabPage1);
			this.tabControl1.Controls.Add(this.tabPage2);
			resources.ApplyResources(this.tabControl1, "tabControl1");
			this.tabControl1.Name = "tabControl1";
			this.tabControl1.SelectedIndex = 0;
			// 
			// tabPage1
			// 
			this.tabPage1.Controls.Add(this.richTextBoxSource);
			resources.ApplyResources(this.tabPage1, "tabPage1");
			this.tabPage1.Name = "tabPage1";
			this.tabPage1.UseVisualStyleBackColor = true;
			// 
			// tabPage2
			// 
			this.tabPage2.Controls.Add(this.richTextBoxPTree);
			resources.ApplyResources(this.tabPage2, "tabPage2");
			this.tabPage2.Name = "tabPage2";
			this.tabPage2.UseVisualStyleBackColor = true;
			// 
			// richTextBoxPTree
			// 
			this.richTextBoxPTree.AcceptsTab = true;
			this.richTextBoxPTree.BorderStyle = System.Windows.Forms.BorderStyle.None;
			resources.ApplyResources(this.richTextBoxPTree, "richTextBoxPTree");
			this.richTextBoxPTree.Name = "richTextBoxPTree";
			// 
			// label1
			// 
			this.label1.BackColor = System.Drawing.Color.WhiteSmoke;
			resources.ApplyResources(this.label1, "label1");
			this.label1.Name = "label1";
			// 
			// tabControl2
			// 
			this.tabControl2.Controls.Add(this.tabPage4);
			this.tabControl2.Controls.Add(this.tabPage5);
			this.tabControl2.Controls.Add(this.tabPage6);
			this.tabControl2.Controls.Add(this.tabPage7);
			this.tabControl2.Controls.Add(this.tabPage3);
			resources.ApplyResources(this.tabControl2, "tabControl2");
			this.tabControl2.Name = "tabControl2";
			this.tabControl2.SelectedIndex = 0;
			// 
			// tabPage4
			// 
			this.tabPage4.Controls.Add(this.richTextBoxWord);
			resources.ApplyResources(this.tabPage4, "tabPage4");
			this.tabPage4.Name = "tabPage4";
			this.tabPage4.UseVisualStyleBackColor = true;
			// 
			// richTextBoxWord
			// 
			this.richTextBoxWord.AcceptsTab = true;
			this.richTextBoxWord.BorderStyle = System.Windows.Forms.BorderStyle.None;
			resources.ApplyResources(this.richTextBoxWord, "richTextBoxWord");
			this.richTextBoxWord.Name = "richTextBoxWord";
			// 
			// tabPage5
			// 
			this.tabPage5.Controls.Add(this.richTextBoxParse);
			resources.ApplyResources(this.tabPage5, "tabPage5");
			this.tabPage5.Name = "tabPage5";
			this.tabPage5.UseVisualStyleBackColor = true;
			// 
			// richTextBoxParse
			// 
			this.richTextBoxParse.AcceptsTab = true;
			this.richTextBoxParse.BorderStyle = System.Windows.Forms.BorderStyle.None;
			resources.ApplyResources(this.richTextBoxParse, "richTextBoxParse");
			this.richTextBoxParse.Name = "richTextBoxParse";
			// 
			// tabPage6
			// 
			this.tabPage6.Controls.Add(this.richTextBoxASM);
			resources.ApplyResources(this.tabPage6, "tabPage6");
			this.tabPage6.Name = "tabPage6";
			this.tabPage6.UseVisualStyleBackColor = true;
			// 
			// richTextBoxASM
			// 
			this.richTextBoxASM.AcceptsTab = true;
			this.richTextBoxASM.BorderStyle = System.Windows.Forms.BorderStyle.None;
			resources.ApplyResources(this.richTextBoxASM, "richTextBoxASM");
			this.richTextBoxASM.Name = "richTextBoxASM";
			// 
			// tabPage7
			// 
			this.tabPage7.Controls.Add(this.richTextBoxHEX);
			resources.ApplyResources(this.tabPage7, "tabPage7");
			this.tabPage7.Name = "tabPage7";
			this.tabPage7.UseVisualStyleBackColor = true;
			// 
			// richTextBoxHEX
			// 
			this.richTextBoxHEX.AcceptsTab = true;
			this.richTextBoxHEX.BorderStyle = System.Windows.Forms.BorderStyle.None;
			resources.ApplyResources(this.richTextBoxHEX, "richTextBoxHEX");
			this.richTextBoxHEX.Name = "richTextBoxHEX";
			// 
			// tabPage3
			// 
			this.tabPage3.Controls.Add(this.richTextBoxCCMes);
			resources.ApplyResources(this.tabPage3, "tabPage3");
			this.tabPage3.Name = "tabPage3";
			this.tabPage3.UseVisualStyleBackColor = true;
			// 
			// richTextBoxCCMes
			// 
			this.richTextBoxCCMes.AcceptsTab = true;
			this.richTextBoxCCMes.BorderStyle = System.Windows.Forms.BorderStyle.None;
			resources.ApplyResources(this.richTextBoxCCMes, "richTextBoxCCMes");
			this.richTextBoxCCMes.Name = "richTextBoxCCMes";
			// 
			// label2
			// 
			this.label2.BackColor = System.Drawing.Color.WhiteSmoke;
			resources.ApplyResources(this.label2, "label2");
			this.label2.Name = "label2";
			// 
			// LIDEForm
			// 
			resources.ApplyResources(this, "$this");
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.BackColor = System.Drawing.Color.White;
			this.Controls.Add(this.splitContainer1);
			this.Controls.Add(this.panel2);
			this.Name = "LIDEForm";
			this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.NoteFormFormClosing);
			this.panel2.ResumeLayout(false);
			this.splitContainer1.Panel1.ResumeLayout(false);
			this.splitContainer1.Panel2.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).EndInit();
			this.splitContainer1.ResumeLayout(false);
			this.tabControl1.ResumeLayout(false);
			this.tabPage1.ResumeLayout(false);
			this.tabPage2.ResumeLayout(false);
			this.tabControl2.ResumeLayout(false);
			this.tabPage4.ResumeLayout(false);
			this.tabPage5.ResumeLayout(false);
			this.tabPage6.ResumeLayout(false);
			this.tabPage7.ResumeLayout(false);
			this.tabPage3.ResumeLayout(false);
			this.ResumeLayout(false);
		}
		private System.Windows.Forms.CheckBox checkBoxHEX;
		private System.Windows.Forms.RichTextBox richTextBoxCCMes;
		private System.Windows.Forms.TabPage tabPage3;
		private System.Windows.Forms.TabPage tabPage7;
		private System.Windows.Forms.RichTextBox richTextBoxASM;
		private System.Windows.Forms.TabPage tabPage6;
		private System.Windows.Forms.RichTextBox richTextBoxParse;
		private System.Windows.Forms.TabPage tabPage5;
		private System.Windows.Forms.RichTextBox richTextBoxWord;
		private System.Windows.Forms.TabPage tabPage4;
		private System.Windows.Forms.TabControl tabControl2;
		private System.Windows.Forms.RichTextBox richTextBoxPTree;
		private System.Windows.Forms.TabPage tabPage2;
		private System.Windows.Forms.TabPage tabPage1;
		private System.Windows.Forms.TabControl tabControl1;
		private System.Windows.Forms.Label labelMes;
		private System.Windows.Forms.Button buttonSave;
		private System.Windows.Forms.Button buttonOpenFile;
		private System.Windows.Forms.Label label2;
		private System.Windows.Forms.RichTextBox richTextBoxHEX;
		private System.Windows.Forms.Label label1;
		private System.Windows.Forms.SplitContainer splitContainer1;
		private System.Windows.Forms.Panel panel2;
		private System.Windows.Forms.RichTextBox richTextBoxSource;
		private System.Windows.Forms.Button button确定;
	}
}
