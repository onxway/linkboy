﻿/*
 * 由 SharpDevelop 创建。
 * 用户: bbb_2
 * 日期: 2009-11-17
 * 时间: 11:15
 * 
 * 如果你想要更改该模板，那么请使用“工具 | 选项 | 编辑 | 编辑标准标题”。
 */
namespace n_LatticeForm
{
	partial class LatticeForm
	{
		/// <summary>
		/// Designer variable used to keep track of non-visual components.
		/// </summary>
		private System.ComponentModel.IContainer components = null;
		
		/// <summary>
		/// Disposes resources used by the form.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing) {
				if (components != null) {
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}
		
		/// <summary>
		/// This method is required for Windows Forms designer support.
		/// Do not change the method contents inside the source code editor. The Forms designer might
		/// not be able to load this method if it was changed manually.
		/// </summary>
		private void InitializeComponent()
		{
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(LatticeForm));
			this.labelPixelW = new System.Windows.Forms.Label();
			this.labelPixelH = new System.Windows.Forms.Label();
			this.textBox宽度 = new System.Windows.Forms.TextBox();
			this.textBox高度 = new System.Windows.Forms.TextBox();
			this.panel1 = new System.Windows.Forms.Panel();
			this.label4 = new System.Windows.Forms.Label();
			this.label1 = new System.Windows.Forms.Label();
			this.radioButtonVar = new System.Windows.Forms.RadioButton();
			this.radioButtonConst = new System.Windows.Forms.RadioButton();
			this.groupBox2 = new System.Windows.Forms.GroupBox();
			this.comboBoxDD = new System.Windows.Forms.ComboBox();
			this.buttonOpenBitmap = new System.Windows.Forms.Button();
			this.trackBar1 = new System.Windows.Forms.TrackBar();
			this.groupBox1 = new System.Windows.Forms.GroupBox();
			this.label3 = new System.Windows.Forms.Label();
			this.label2 = new System.Windows.Forms.Label();
			this.richTextBox文字 = new System.Windows.Forms.RichTextBox();
			this.button字体 = new System.Windows.Forms.Button();
			this.textBoxTW = new System.Windows.Forms.TextBox();
			this.textBoxTH = new System.Windows.Forms.TextBox();
			this.buttonMoveLeft = new System.Windows.Forms.Button();
			this.buttonMoveRight = new System.Windows.Forms.Button();
			this.buttonMoveDown = new System.Windows.Forms.Button();
			this.buttonMoveUp = new System.Windows.Forms.Button();
			this.button确定 = new System.Windows.Forms.Button();
			this.button反色点阵 = new System.Windows.Forms.Button();
			this.button清空点阵 = new System.Windows.Forms.Button();
			this.panel2 = new System.Windows.Forms.Panel();
			this.label5 = new System.Windows.Forms.Label();
			this.panel1.SuspendLayout();
			this.groupBox2.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.trackBar1)).BeginInit();
			this.groupBox1.SuspendLayout();
			this.SuspendLayout();
			// 
			// labelPixelW
			// 
			resources.ApplyResources(this.labelPixelW, "labelPixelW");
			this.labelPixelW.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
			this.labelPixelW.Name = "labelPixelW";
			// 
			// labelPixelH
			// 
			resources.ApplyResources(this.labelPixelH, "labelPixelH");
			this.labelPixelH.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
			this.labelPixelH.Name = "labelPixelH";
			// 
			// textBox宽度
			// 
			this.textBox宽度.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
			resources.ApplyResources(this.textBox宽度, "textBox宽度");
			this.textBox宽度.Name = "textBox宽度";
			this.textBox宽度.TextChanged += new System.EventHandler(this.TextBox宽度TextChanged);
			// 
			// textBox高度
			// 
			this.textBox高度.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
			resources.ApplyResources(this.textBox高度, "textBox高度");
			this.textBox高度.Name = "textBox高度";
			this.textBox高度.TextChanged += new System.EventHandler(this.TextBox高度TextChanged);
			// 
			// panel1
			// 
			this.panel1.BackColor = System.Drawing.Color.White;
			this.panel1.Controls.Add(this.label5);
			this.panel1.Controls.Add(this.label4);
			this.panel1.Controls.Add(this.label1);
			this.panel1.Controls.Add(this.radioButtonVar);
			this.panel1.Controls.Add(this.radioButtonConst);
			this.panel1.Controls.Add(this.groupBox2);
			this.panel1.Controls.Add(this.groupBox1);
			this.panel1.Controls.Add(this.buttonMoveLeft);
			this.panel1.Controls.Add(this.buttonMoveRight);
			this.panel1.Controls.Add(this.buttonMoveDown);
			this.panel1.Controls.Add(this.buttonMoveUp);
			this.panel1.Controls.Add(this.button确定);
			this.panel1.Controls.Add(this.button反色点阵);
			this.panel1.Controls.Add(this.button清空点阵);
			this.panel1.Controls.Add(this.labelPixelW);
			this.panel1.Controls.Add(this.textBox高度);
			this.panel1.Controls.Add(this.labelPixelH);
			this.panel1.Controls.Add(this.textBox宽度);
			resources.ApplyResources(this.panel1, "panel1");
			this.panel1.Name = "panel1";
			// 
			// label4
			// 
			resources.ApplyResources(this.label4, "label4");
			this.label4.ForeColor = System.Drawing.Color.SlateGray;
			this.label4.Name = "label4";
			// 
			// label1
			// 
			resources.ApplyResources(this.label1, "label1");
			this.label1.ForeColor = System.Drawing.Color.SlateGray;
			this.label1.Name = "label1";
			// 
			// radioButtonVar
			// 
			resources.ApplyResources(this.radioButtonVar, "radioButtonVar");
			this.radioButtonVar.Name = "radioButtonVar";
			this.radioButtonVar.UseVisualStyleBackColor = true;
			// 
			// radioButtonConst
			// 
			this.radioButtonConst.Checked = true;
			resources.ApplyResources(this.radioButtonConst, "radioButtonConst");
			this.radioButtonConst.Name = "radioButtonConst";
			this.radioButtonConst.TabStop = true;
			this.radioButtonConst.UseVisualStyleBackColor = true;
			this.radioButtonConst.CheckedChanged += new System.EventHandler(this.RadioButtonConstCheckedChanged);
			// 
			// groupBox2
			// 
			this.groupBox2.Controls.Add(this.comboBoxDD);
			this.groupBox2.Controls.Add(this.buttonOpenBitmap);
			this.groupBox2.Controls.Add(this.trackBar1);
			this.groupBox2.ForeColor = System.Drawing.Color.SlateGray;
			resources.ApplyResources(this.groupBox2, "groupBox2");
			this.groupBox2.Name = "groupBox2";
			this.groupBox2.TabStop = false;
			// 
			// comboBoxDD
			// 
			this.comboBoxDD.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
			resources.ApplyResources(this.comboBoxDD, "comboBoxDD");
			this.comboBoxDD.FormattingEnabled = true;
			this.comboBoxDD.Items.AddRange(new object[] {
									resources.GetString("comboBoxDD.Items"),
									resources.GetString("comboBoxDD.Items1"),
									resources.GetString("comboBoxDD.Items2"),
									resources.GetString("comboBoxDD.Items3"),
									resources.GetString("comboBoxDD.Items4")});
			this.comboBoxDD.Name = "comboBoxDD";
			this.comboBoxDD.SelectedIndexChanged += new System.EventHandler(this.ComboBoxDDSelectedIndexChanged);
			// 
			// buttonOpenBitmap
			// 
			this.buttonOpenBitmap.BackColor = System.Drawing.Color.CornflowerBlue;
			resources.ApplyResources(this.buttonOpenBitmap, "buttonOpenBitmap");
			this.buttonOpenBitmap.ForeColor = System.Drawing.Color.White;
			this.buttonOpenBitmap.Name = "buttonOpenBitmap";
			this.buttonOpenBitmap.UseVisualStyleBackColor = false;
			this.buttonOpenBitmap.Click += new System.EventHandler(this.ButtonOpenBitmapClick);
			// 
			// trackBar1
			// 
			resources.ApplyResources(this.trackBar1, "trackBar1");
			this.trackBar1.Maximum = 255;
			this.trackBar1.Minimum = 1;
			this.trackBar1.Name = "trackBar1";
			this.trackBar1.Value = 128;
			this.trackBar1.Scroll += new System.EventHandler(this.TrackBar1Scroll);
			// 
			// groupBox1
			// 
			this.groupBox1.Controls.Add(this.label3);
			this.groupBox1.Controls.Add(this.label2);
			this.groupBox1.Controls.Add(this.richTextBox文字);
			this.groupBox1.Controls.Add(this.button字体);
			this.groupBox1.Controls.Add(this.textBoxTW);
			this.groupBox1.Controls.Add(this.textBoxTH);
			this.groupBox1.ForeColor = System.Drawing.Color.SlateGray;
			resources.ApplyResources(this.groupBox1, "groupBox1");
			this.groupBox1.Name = "groupBox1";
			this.groupBox1.TabStop = false;
			// 
			// label3
			// 
			resources.ApplyResources(this.label3, "label3");
			this.label3.ForeColor = System.Drawing.Color.Black;
			this.label3.Name = "label3";
			// 
			// label2
			// 
			resources.ApplyResources(this.label2, "label2");
			this.label2.ForeColor = System.Drawing.Color.Black;
			this.label2.Name = "label2";
			// 
			// richTextBox文字
			// 
			this.richTextBox文字.BackColor = System.Drawing.Color.White;
			resources.ApplyResources(this.richTextBox文字, "richTextBox文字");
			this.richTextBox文字.Name = "richTextBox文字";
			this.richTextBox文字.TextChanged += new System.EventHandler(this.RichTextBox文字TextChanged);
			// 
			// button字体
			// 
			this.button字体.BackColor = System.Drawing.Color.CornflowerBlue;
			resources.ApplyResources(this.button字体, "button字体");
			this.button字体.ForeColor = System.Drawing.Color.White;
			this.button字体.Name = "button字体";
			this.button字体.UseVisualStyleBackColor = false;
			this.button字体.Click += new System.EventHandler(this.Button字体Click);
			// 
			// textBoxTW
			// 
			this.textBoxTW.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
			resources.ApplyResources(this.textBoxTW, "textBoxTW");
			this.textBoxTW.Name = "textBoxTW";
			this.textBoxTW.TextChanged += new System.EventHandler(this.TextBoxTWTextChanged);
			// 
			// textBoxTH
			// 
			this.textBoxTH.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
			resources.ApplyResources(this.textBoxTH, "textBoxTH");
			this.textBoxTH.Name = "textBoxTH";
			this.textBoxTH.TextChanged += new System.EventHandler(this.TextBoxTHTextChanged);
			// 
			// buttonMoveLeft
			// 
			this.buttonMoveLeft.BackColor = System.Drawing.Color.CornflowerBlue;
			resources.ApplyResources(this.buttonMoveLeft, "buttonMoveLeft");
			this.buttonMoveLeft.ForeColor = System.Drawing.Color.White;
			this.buttonMoveLeft.Name = "buttonMoveLeft";
			this.buttonMoveLeft.UseVisualStyleBackColor = false;
			this.buttonMoveLeft.Click += new System.EventHandler(this.ButtonMoveLeftClick);
			// 
			// buttonMoveRight
			// 
			this.buttonMoveRight.BackColor = System.Drawing.Color.CornflowerBlue;
			resources.ApplyResources(this.buttonMoveRight, "buttonMoveRight");
			this.buttonMoveRight.ForeColor = System.Drawing.Color.White;
			this.buttonMoveRight.Name = "buttonMoveRight";
			this.buttonMoveRight.UseVisualStyleBackColor = false;
			this.buttonMoveRight.Click += new System.EventHandler(this.ButtonMoveRightClick);
			// 
			// buttonMoveDown
			// 
			this.buttonMoveDown.BackColor = System.Drawing.Color.CornflowerBlue;
			resources.ApplyResources(this.buttonMoveDown, "buttonMoveDown");
			this.buttonMoveDown.ForeColor = System.Drawing.Color.White;
			this.buttonMoveDown.Name = "buttonMoveDown";
			this.buttonMoveDown.UseVisualStyleBackColor = false;
			this.buttonMoveDown.Click += new System.EventHandler(this.ButtonMoveDownClick);
			// 
			// buttonMoveUp
			// 
			this.buttonMoveUp.BackColor = System.Drawing.Color.CornflowerBlue;
			resources.ApplyResources(this.buttonMoveUp, "buttonMoveUp");
			this.buttonMoveUp.ForeColor = System.Drawing.Color.White;
			this.buttonMoveUp.Name = "buttonMoveUp";
			this.buttonMoveUp.UseVisualStyleBackColor = false;
			this.buttonMoveUp.Click += new System.EventHandler(this.ButtonMoveUpClick);
			// 
			// button确定
			// 
			this.button确定.BackColor = System.Drawing.Color.CornflowerBlue;
			resources.ApplyResources(this.button确定, "button确定");
			this.button确定.ForeColor = System.Drawing.Color.White;
			this.button确定.Name = "button确定";
			this.button确定.UseVisualStyleBackColor = false;
			this.button确定.Click += new System.EventHandler(this.Button确定Click);
			// 
			// button反色点阵
			// 
			this.button反色点阵.BackColor = System.Drawing.Color.CornflowerBlue;
			resources.ApplyResources(this.button反色点阵, "button反色点阵");
			this.button反色点阵.ForeColor = System.Drawing.Color.White;
			this.button反色点阵.Name = "button反色点阵";
			this.button反色点阵.UseVisualStyleBackColor = false;
			this.button反色点阵.Click += new System.EventHandler(this.Button反色点阵Click);
			// 
			// button清空点阵
			// 
			this.button清空点阵.BackColor = System.Drawing.Color.CornflowerBlue;
			resources.ApplyResources(this.button清空点阵, "button清空点阵");
			this.button清空点阵.ForeColor = System.Drawing.Color.White;
			this.button清空点阵.Name = "button清空点阵";
			this.button清空点阵.UseVisualStyleBackColor = false;
			this.button清空点阵.Click += new System.EventHandler(this.Button清空点阵Click);
			// 
			// panel2
			// 
			resources.ApplyResources(this.panel2, "panel2");
			this.panel2.BackColor = System.Drawing.Color.White;
			this.panel2.Name = "panel2";
			// 
			// label5
			// 
			this.label5.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(241)))), ((int)(((byte)(241)))), ((int)(((byte)(241)))));
			resources.ApplyResources(this.label5, "label5");
			this.label5.Name = "label5";
			// 
			// LatticeForm
			// 
			resources.ApplyResources(this, "$this");
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.BackColor = System.Drawing.Color.White;
			this.Controls.Add(this.panel2);
			this.Controls.Add(this.panel1);
			this.MaximizeBox = false;
			this.MinimizeBox = false;
			this.Name = "LatticeForm";
			this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.ModuleFormClosing);
			this.panel1.ResumeLayout(false);
			this.panel1.PerformLayout();
			this.groupBox2.ResumeLayout(false);
			this.groupBox2.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.trackBar1)).EndInit();
			this.groupBox1.ResumeLayout(false);
			this.groupBox1.PerformLayout();
			this.ResumeLayout(false);
		}
		private System.Windows.Forms.Label label5;
		private System.Windows.Forms.Label label1;
		private System.Windows.Forms.Label label4;
		private System.Windows.Forms.RadioButton radioButtonConst;
		private System.Windows.Forms.RadioButton radioButtonVar;
		private System.Windows.Forms.Label label2;
		private System.Windows.Forms.Label label3;
		private System.Windows.Forms.ComboBox comboBoxDD;
		private System.Windows.Forms.TrackBar trackBar1;
		private System.Windows.Forms.GroupBox groupBox1;
		private System.Windows.Forms.Button buttonOpenBitmap;
		private System.Windows.Forms.GroupBox groupBox2;
		private System.Windows.Forms.TextBox textBox宽度;
		private System.Windows.Forms.Label labelPixelW;
		private System.Windows.Forms.Label labelPixelH;
		private System.Windows.Forms.Button button确定;
		private System.Windows.Forms.Button button反色点阵;
		private System.Windows.Forms.Button button清空点阵;
		private System.Windows.Forms.RichTextBox richTextBox文字;
		private System.Windows.Forms.Button button字体;
		private System.Windows.Forms.TextBox textBox高度;
		private System.Windows.Forms.Panel panel2;
		private System.Windows.Forms.Panel panel1;
		private System.Windows.Forms.TextBox textBoxTH;
		private System.Windows.Forms.TextBox textBoxTW;
		private System.Windows.Forms.Button buttonMoveLeft;
		private System.Windows.Forms.Button buttonMoveRight;
		private System.Windows.Forms.Button buttonMoveDown;
		private System.Windows.Forms.Button buttonMoveUp;
	}
}
