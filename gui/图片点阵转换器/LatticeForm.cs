﻿
namespace n_LatticeForm
{
using System;
using System.IO;
using System.Net;
using System.Text;
using System.Windows.Forms;
using System.Drawing;
using System.Drawing.Drawing2D;
using n_PointPanel;
using n_LatticeToCode;
using n_GUIcoder;
using c_MyObjectSet;

/// <summary>
/// Description of WorkForm.
/// </summary>
public partial class LatticeForm : Form
{
	public PointPanel MPanel;
	
	Font TextFont;
	
	int WidthN;
	int HeightN;
	
	bool ignoreRefresh;
	bool ignoreChecked;
	bool isOK;
	
	string BitType;
	
	bool ClearMesShowed = false;
	
	Bitmap bmp;
	
	//主窗口
	public LatticeForm()
	{
		InitializeComponent();
		
		isOK = false;
		ignoreRefresh = false;
		ignoreChecked = false;
		WidthN = 16;
		HeightN = 16;
		
		BitType = LatticeToCode.L_V1_RightDown;
		
		TextFont = this.richTextBox文字.Font;
		this.richTextBox文字.Font = TextFont;
		this.richTextBox文字.Select();
		this.richTextBox文字.SelectionFont = TextFont;
		
		comboBoxDD.SelectedIndex = 0;
	}
	
	//获取图片
	public Bitmap GetBitmap( int W, int H, string text )
	{
		if( text == null || text == "" ) {
			MPanel = new PointPanel( W, H, null );
			MPanel.ShowImage();
		}
		else {
			int T = 0;
			int CW = 0;
			int h = 0;
			string Mes = "";
			bool[][] PointSet = LatticeToCode.GetLatticeFromArray( text, ref T, ref CW, ref h, ref Mes );
			MPanel = new PointPanel( PointSet.Length, PointSet[ 0 ].Length, PointSet );
			MPanel.ShowImage();
		}
		return MPanel.back;
	}
	
	//运行
	public bool RunBitmap( string text, ref int W, ref int H, ref string Value )
	{
		isOK = false;
		if( text == null || text == "" ) {
			ignoreChecked = true;
			SetConst( false );
			ignoreChecked = false;
			WidthN = W;
			HeightN = H;
			ignoreRefresh = true;
			this.textBox宽度.Text = (WidthN/8).ToString();
			this.textBox高度.Text = (HeightN/8).ToString();
			ignoreRefresh = false;
			RefreshLattice( null );
			MPanel.Select();
		}
		else {
			if( text.StartsWith( LatticeToCode.L_DownRight ) ) {
				BitType = LatticeToCode.L_DownRight;
			}
			else if( text.StartsWith( LatticeToCode.L_RightDown ) ) {
				BitType = LatticeToCode.L_RightDown;
			}
			else if( text.StartsWith( LatticeToCode.L_V1_RightDown ) ) {
				BitType = LatticeToCode.L_V1_RightDown;
			}
			else {
				BitType = null;
			}
			
			SetConst( true );
			
			this.richTextBox文字.Text = "";
			int T = 0;
			int CW = 0;
			string Mes = "";
			bool[][] PointSet = LatticeToCode.GetLatticeFromArray( text, ref T, ref CW, ref HeightN, ref Mes );
			WidthN = PointSet.Length;
			ignoreRefresh = true;
			this.textBox宽度.Text = (WidthN/8).ToString();
			this.textBox高度.Text = (HeightN/8).ToString();
			ignoreRefresh = false;
			RefreshLattice( PointSet );
			MPanel.Select();
		}
		this.ShowDialog();
		/*
		this.Visible = true;
		this.Activate();
		
		while( this.Visible ) {
			System.Windows.Forms.Application.DoEvents();
		}
		*/
		
		if( isOK ) {
			W = WidthN;
			H = HeightN;
			if( this.radioButtonConst.Checked ) {
				bool[][] LatticeList = MPanel.PointSet;
				Value = LatticeToCode.GetCode( LatticeList, BitType, LatticeToCode.T_bitmap, 0, null );
				
				//if( GUIcoder.isPyMode ) {
				//	Value1 = LatticeToCode.GetCode( LatticeList, LatticeToCode.L_V1_DownRight, LatticeToCode.T_bitmap, 0, null );
				//}
			}
			else {
				Value = "";
			}
			return true;
		}
		return false;
	}
	
	bool DecodeText( string Strings )
	{
		Strings = Strings.Remove( Strings.Length - 1 ).Remove( 0, 1 );
		int SPL = Strings.LastIndexOf( '(' );
		if( SPL == -1 ) {
			//ET.WriteParseError( Index, Mes );
			return false;
		}
		string Text = Strings.Remove( SPL - 1 );
		string Font = Strings.Remove( 0, SPL + 1 );
		
		Font f = Common.GetFontFromString( Font );
		if( f == null ) {
			return false;
		}
		this.richTextBox文字.Text = Text;
		
		TextFont = f;
		this.richTextBox文字.Font = TextFont;
		this.richTextBox文字.Select();
		this.richTextBox文字.SelectionFont = TextFont;
		return true;
	}
	
	void SetConst( bool c )
	{
		this.radioButtonConst.Checked = c;
		this.radioButtonVar.Checked = !c;
	}
	
	//更新点阵尺寸
	void RefreshLattice( bool[][] Set )
	{
		MPanel = new PointPanel( WidthN, HeightN, Set );
		MPanel.Location = new Point( 2, 2 );
		MPanel.Changed = ImageChanged;
		
		this.panel2.Controls.Clear();
		this.panel2.Controls.Add( MPanel );
		
		MPanel.Scale( 1, this.panel2.Width / 2, this.panel2.Height / 2 );
		MPanel.Scale( 1, this.panel2.Width / 2, this.panel2.Height / 2 );
		MPanel.Scale( 1, this.panel2.Width / 2, this.panel2.Height / 2 );
		//MPanel.Scale( 1, this.panel2.Width / 2, this.panel2.Height / 2 );
		//MPanel.Scale( 1, MPanel.Width / 2, MPanel.Height / 2 );
		
		int H = this.panel1.Height + MPanel.Height + 60;
		if( H < 700 ) {
			this.Height = H;
		}
		else {
			this.Height = 700;
		}
		MPanel.ShowImage();
	}
	
	void ImageChanged()
	{
		SetConst( true );
	}
	
	//更新文字点阵
	void RefreshTextLattice()
	{
		string s = this.richTextBox文字.Text;
		if( String.IsNullOrEmpty( s ) ) {
			return;
		}
		bool[][] PointSet = LatticeToCode.GetLatticeBitmap( s, TextFont, false, int.Parse( this.textBoxTW.Text ), int.Parse( this.textBoxTH.Text ) );
		
		//更新图形界面
		WidthN = PointSet.Length;
		HeightN = PointSet[ 0 ].Length;
		ignoreRefresh = true;
		this.textBox宽度.Text = (WidthN / 8).ToString();
		this.textBox高度.Text = (HeightN / 8).ToString();
		ignoreRefresh = false;
		RefreshLattice( PointSet );
	}
	
	//关闭事件
	void ModuleFormClosing(object sender, FormClosingEventArgs e)
	{
		//isOK = false;
		this.Visible = false;
		//e.Cancel = true;
	}
	
	void TextBox宽度TextChanged(object sender, EventArgs e)
	{
		try {
			WidthN = int.Parse( this.textBox宽度.Text );
			WidthN *= 8;
			this.labelPixelW.Text = "宽度为 " + this.textBox宽度.Text + "*8 = " + WidthN;
		}
		catch {
			MessageBox.Show( "点阵尺寸更新失败" );
			ignoreRefresh = true;
			this.textBox宽度.Text = (WidthN/8).ToString();
			ignoreRefresh = false;
		}
		if( ignoreRefresh ) {
			return;
		}
		RefreshLattice( MPanel.PointSet );
	}
	
	void TextBox高度TextChanged(object sender, EventArgs e)
	{
		try {
			HeightN = int.Parse( this.textBox高度.Text );
			HeightN *= 8;
			this.labelPixelH.Text = "高度为 " + this.textBox高度.Text + "*8 = " + HeightN;
		}
		catch {
			MessageBox.Show( "点阵尺寸更新失败" );
			ignoreRefresh = true;
			this.textBox高度.Text = (HeightN/8).ToString();
			ignoreRefresh = false;
		}
		if( ignoreRefresh ) {
			return;
		}
		RefreshLattice( MPanel.PointSet );
	}
	
	void RichTextBox文字TextChanged(object sender, EventArgs e)
	{
		if( !ClearMesShowed ) {
			if( MessageBox.Show( "文字转图片之前将会清空您已经编辑好的图片数据, 确定要继续吗?", "图片清空提示", MessageBoxButtons.YesNo ) == DialogResult.No ) {
				return;
			}
		}
		ClearMesShowed = true;
		
		RefreshTextLattice();
		SetConst( true );
	}
	
	void Button字体Click(object sender, EventArgs e)
	{
		FontDialog f = new FontDialog();
		f.Font = TextFont;
		if( f.ShowDialog() == DialogResult.OK ) {
			TextFont = f.Font;
			
			this.richTextBox文字.Font = TextFont;
			this.richTextBox文字.Select();
			this.richTextBox文字.SelectionFont = TextFont;
			
			RefreshTextLattice();
		}
	}
	
	void Button清空点阵Click(object sender, EventArgs e)
	{
		MPanel.Clear();
		MPanel.Invalidate();
	}
	
	void Button反色点阵Click(object sender, EventArgs e)
	{
		MPanel.Reverse();
		MPanel.Invalidate();
	}
	
	void Button确定Click(object sender, EventArgs e)
	{
		isOK = true;
		this.Visible = false;
	}
	
	void RadioButtonConstCheckedChanged(object sender, EventArgs e)
	{
		if( !ignoreChecked ) {
			if( !this.radioButtonConst.Checked ) {
				if( MessageBox.Show( "设置为变量图片类型时不支持编辑图片, 您的图片数据将会被清空. 确定要设置为变量图片类型吗?", "注意: 图形数据将清空", MessageBoxButtons.YesNo ) == DialogResult.Yes ) {
					RefreshLattice( null );
				}
				else {
					SetConst( true );
				}
			}
		}
	}
	
	void TextBoxTWTextChanged(object sender, EventArgs e)
	{
		if( ignoreRefresh ) {
			return;
		}
		if( this.textBoxTW.Text == "-" ) {
			return;
		}
		try {
			int i = int.Parse( this.textBoxTW.Text );
		}
		catch {
			MessageBox.Show( "点阵字符间距更新失败" );
			ignoreRefresh = true;
			this.textBoxTW.Text = "0";
			ignoreRefresh = false;
			return;
		}
		RefreshTextLattice();
		SetConst( true );
	}
	
	void TextBoxTHTextChanged(object sender, EventArgs e)
	{
		if( ignoreRefresh ) {
			return;
		}
		if( this.textBoxTH.Text == "-" ) {
			return;
		}
		try {
			int i = int.Parse( this.textBoxTH.Text );
		}
		catch {
			MessageBox.Show( "点阵字符间距更新失败" );
			ignoreRefresh = true;
			this.textBoxTH.Text = "0";
			ignoreRefresh = false;
			return;
		}
		RefreshTextLattice();
		SetConst( true );
	}
	
	//==================================================
	
	void ButtonMoveUpClick(object sender, EventArgs e)
	{
		MPanel.MoveUp();
		MPanel.Invalidate();
	}
	
	void ButtonMoveDownClick(object sender, EventArgs e)
	{
		MPanel.MoveDown();
		MPanel.Invalidate();
	}
	
	void ButtonMoveLeftClick(object sender, EventArgs e)
	{
		MPanel.MoveLeft();
		MPanel.Invalidate();
	}
	
	void ButtonMoveRightClick(object sender, EventArgs e)
	{
		MPanel.MoveRight();
		MPanel.Invalidate();
	}
	
	void ButtonOpenBitmapClick(object sender, EventArgs e)
	{
		//打开文件对话框
		OpenFileDialog OpenFileDlg = new OpenFileDialog();
		OpenFileDlg.Filter = "所有图片类型文件 | *.*";
		OpenFileDlg.Title = "请选择图片文件";
		
		DialogResult dlgResult = OpenFileDlg.ShowDialog();
		if(dlgResult == DialogResult.OK) {
			
			bmp = new Bitmap( OpenFileDlg.FileName );
			
			RefreshBitmap();
		}
	}
	
	void TrackBar1Scroll(object sender, EventArgs e)
	{
		RefreshBitmap();
	}
	
	void ComboBoxDDSelectedIndexChanged(object sender, EventArgs e)
	{
		RefreshBitmap();
	}
	
	//刷新图片
	void RefreshBitmap()
	{
		if( bmp != null ) {
			MPanel.SetImage( bmp, trackBar1.Value, comboBoxDD.SelectedIndex );
			MPanel.Invalidate();
			SetConst( true );
		}
	}
}
}





