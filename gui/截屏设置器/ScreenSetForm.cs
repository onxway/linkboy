﻿
using System;
using System.Drawing;
using System.Windows.Forms;
using n_LocatePanel;

namespace n_ScreenSetForm
{
/// <summary>
/// Description of LocateForm.
/// </summary>
public partial class ScreenSetForm : Form
{
	public int Data;
	
	public ScreenSetForm()
	{
		//
		// The InitializeComponent() call is required for Windows Forms designer support.
		//
		InitializeComponent();
		
		comboBox1.SelectedIndex = n_MainSystemData.SystemData.ScreenKey;
		trackBarScale.Value = n_ImagePanel.ImagePanel.SCALE_MID_INDEX;
	}
	
	//运行
	public void Run()
	{
		trackBarScale.Value = n_ImagePanel.ImagePanel.SCALE_MID_INDEX;
		
		TrackBarScaleScroll( null, null );
		
		this.Visible = true;
	}
	
	void LocateFormFormClosing(object sender, FormClosingEventArgs e)
	{
		this.Visible = false;
		e.Cancel = true;
	}
	
	void Button1Click(object sender, EventArgs e)
	{
		this.Visible = false;
		System.Threading.Thread.Sleep( 1000 );
		
		if( G.ScreenBox == null ) {
			G.ScreenBox = new n_ScreenForm.ScreenForm();
		}
		G.ScreenBox.Run();
	}
	
	void ComboBox1SelectedIndexChanged(object sender, EventArgs e)
	{
		n_MainSystemData.SystemData.ScreenKey = comboBox1.SelectedIndex;
		n_MainSystemData.SystemData.isChanged = true;
	}
	
	void ButtonFullClick(object sender, EventArgs e)
	{
		if( G.CGPanel.StartExportImg ) {
			MessageBox.Show( "请设置linbkoy窗口为激活状态, 以便完成截图" );
			return;
		}
		G.CGPanel.ExportImgForSave_Scale = trackBarScale.Value;
		G.CGPanel.ExportImgForSave = true;
		G.CGPanel.StartExportImg = true;
		timer1.Enabled = true;
	}
	
	void Timer1Tick(object sender, EventArgs e)
	{
		if( G.CGPanel.StartExportImg ) {
			return;
		}
		G.CGPanel.StartExportImg = false;
		G.CGPanel.ExportImgForSave = false;
		
		timer1.Enabled = false;
		
		if( n_ImagePanel.ImagePanel.ExportImg == null ) {
			MessageBox.Show( "图片导出错误" );
			return;
		}
		
		if( G.ScreenBox == null ) {
			G.ScreenBox = new n_ScreenForm.ScreenForm();
		}
		G.ScreenBox.SPanel.SaveImg( n_ImagePanel.ImagePanel.ExportImg );
	}
	
	void ButtonGifClick(object sender, EventArgs e)
	{
		G.ScreenBox.SPanel.Gif_Start();
	}
	
	void TrackBarScaleScroll(object sender, EventArgs e)
	{
		int AScale = n_ImagePanel.ImagePanel.ScaleList[trackBarScale.Value];
		
		int pad = 98;
		float minx = G.CGPanel.MinX - pad;
		float maxx = G.CGPanel.MaxX + pad;
		float miny = G.CGPanel.MinY - pad;
		float maxy = G.CGPanel.MaxY + pad;
		
		float aww = ( maxx - minx ) * AScale / n_ImagePanel.ImagePanel.AScaleMid;
		float ahh = ( maxy - miny ) * AScale / n_ImagePanel.ImagePanel.AScaleMid;
		
		labelSize.Text = "图像尺寸: " + (int)aww + "*" + (int)ahh;
		labelQ.Text = "图像质量: " + trackBarScale.Value;
	}
}
}

