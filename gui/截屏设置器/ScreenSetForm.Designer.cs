﻿/*
 * 由SharpDevelop创建。
 * 用户： cap_gpu
 * 日期: 2017/4/9
 * 时间: 8:03
 * 
 * 要改变这种模板请点击 工具|选项|代码编写|编辑标准头文件
 */
namespace n_ScreenSetForm
{
	partial class ScreenSetForm
	{
		/// <summary>
		/// Designer variable used to keep track of non-visual components.
		/// </summary>
		private System.ComponentModel.IContainer components = null;
		
		/// <summary>
		/// Disposes resources used by the form.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing) {
				if (components != null) {
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}
		
		/// <summary>
		/// This method is required for Windows Forms designer support.
		/// Do not change the method contents inside the source code editor. The Forms designer might
		/// not be able to load this method if it was changed manually.
		/// </summary>
		private void InitializeComponent()
		{
			this.components = new System.ComponentModel.Container();
			this.button1 = new System.Windows.Forms.Button();
			this.label1 = new System.Windows.Forms.Label();
			this.comboBox1 = new System.Windows.Forms.ComboBox();
			this.label2 = new System.Windows.Forms.Label();
			this.buttonFull = new System.Windows.Forms.Button();
			this.timer1 = new System.Windows.Forms.Timer(this.components);
			this.label3 = new System.Windows.Forms.Label();
			this.label4 = new System.Windows.Forms.Label();
			this.label5 = new System.Windows.Forms.Label();
			this.label6 = new System.Windows.Forms.Label();
			this.buttonGif = new System.Windows.Forms.Button();
			this.trackBarScale = new System.Windows.Forms.TrackBar();
			this.labelQ = new System.Windows.Forms.Label();
			this.labelSize = new System.Windows.Forms.Label();
			((System.ComponentModel.ISupportInitialize)(this.trackBarScale)).BeginInit();
			this.SuspendLayout();
			// 
			// button1
			// 
			this.button1.BackColor = System.Drawing.Color.CornflowerBlue;
			this.button1.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
			this.button1.Font = new System.Drawing.Font("微软雅黑", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
			this.button1.ForeColor = System.Drawing.Color.WhiteSmoke;
			this.button1.Location = new System.Drawing.Point(12, 12);
			this.button1.Name = "button1";
			this.button1.Size = new System.Drawing.Size(173, 42);
			this.button1.TabIndex = 4;
			this.button1.Text = "部分区域截图";
			this.button1.UseVisualStyleBackColor = false;
			this.button1.Click += new System.EventHandler(this.Button1Click);
			// 
			// label1
			// 
			this.label1.Font = new System.Drawing.Font("微软雅黑", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
			this.label1.Location = new System.Drawing.Point(12, 120);
			this.label1.Name = "label1";
			this.label1.Size = new System.Drawing.Size(123, 29);
			this.label1.TabIndex = 5;
			this.label1.Text = "截屏快捷键：";
			this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// comboBox1
			// 
			this.comboBox1.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
			this.comboBox1.Font = new System.Drawing.Font("微软雅黑", 12F);
			this.comboBox1.FormattingEnabled = true;
			this.comboBox1.Items.AddRange(new object[] {
									"F1",
									"F2",
									"F3",
									"F4",
									"F5",
									"F6",
									"F7",
									"F8",
									"F9",
									"F10",
									"F11",
									"F12"});
			this.comboBox1.Location = new System.Drawing.Point(116, 120);
			this.comboBox1.Name = "comboBox1";
			this.comboBox1.Size = new System.Drawing.Size(69, 29);
			this.comboBox1.TabIndex = 6;
			this.comboBox1.SelectedIndexChanged += new System.EventHandler(this.ComboBox1SelectedIndexChanged);
			// 
			// label2
			// 
			this.label2.Font = new System.Drawing.Font("微软雅黑", 10.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
			this.label2.ForeColor = System.Drawing.Color.Chocolate;
			this.label2.Location = new System.Drawing.Point(12, 57);
			this.label2.Name = "label2";
			this.label2.Size = new System.Drawing.Size(173, 60);
			this.label2.TabIndex = 7;
			this.label2.Text = "修改快捷键后需要重启软件";
			// 
			// buttonFull
			// 
			this.buttonFull.BackColor = System.Drawing.Color.CornflowerBlue;
			this.buttonFull.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
			this.buttonFull.Font = new System.Drawing.Font("微软雅黑", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
			this.buttonFull.ForeColor = System.Drawing.Color.WhiteSmoke;
			this.buttonFull.Location = new System.Drawing.Point(230, 11);
			this.buttonFull.Name = "buttonFull";
			this.buttonFull.Size = new System.Drawing.Size(173, 42);
			this.buttonFull.TabIndex = 8;
			this.buttonFull.Text = "完整区域截图";
			this.buttonFull.UseVisualStyleBackColor = false;
			this.buttonFull.Click += new System.EventHandler(this.ButtonFullClick);
			// 
			// timer1
			// 
			this.timer1.Tick += new System.EventHandler(this.Timer1Tick);
			// 
			// label3
			// 
			this.label3.Font = new System.Drawing.Font("微软雅黑", 10.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
			this.label3.ForeColor = System.Drawing.Color.Chocolate;
			this.label3.Location = new System.Drawing.Point(230, 57);
			this.label3.Name = "label3";
			this.label3.Size = new System.Drawing.Size(173, 111);
			this.label3.TabIndex = 9;
			this.label3.Text = "完整区域截图可将屏幕之外的部分也进行截图，包含程序的全部元素，此截图与当前界面可视区域大小无关";
			// 
			// label4
			// 
			this.label4.BackColor = System.Drawing.Color.LightGray;
			this.label4.Location = new System.Drawing.Point(206, 30);
			this.label4.Name = "label4";
			this.label4.Size = new System.Drawing.Size(2, 250);
			this.label4.TabIndex = 10;
			// 
			// label5
			// 
			this.label5.BackColor = System.Drawing.Color.LightGray;
			this.label5.Location = new System.Drawing.Point(421, 30);
			this.label5.Name = "label5";
			this.label5.Size = new System.Drawing.Size(2, 150);
			this.label5.TabIndex = 13;
			// 
			// label6
			// 
			this.label6.Font = new System.Drawing.Font("微软雅黑", 10.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
			this.label6.ForeColor = System.Drawing.Color.Chocolate;
			this.label6.Location = new System.Drawing.Point(445, 57);
			this.label6.Name = "label6";
			this.label6.Size = new System.Drawing.Size(173, 123);
			this.label6.TabIndex = 12;
			// 
			// buttonGif
			// 
			this.buttonGif.BackColor = System.Drawing.Color.CornflowerBlue;
			this.buttonGif.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
			this.buttonGif.Font = new System.Drawing.Font("微软雅黑", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
			this.buttonGif.ForeColor = System.Drawing.Color.WhiteSmoke;
			this.buttonGif.Location = new System.Drawing.Point(445, 11);
			this.buttonGif.Name = "buttonGif";
			this.buttonGif.Size = new System.Drawing.Size(173, 42);
			this.buttonGif.TabIndex = 11;
			this.buttonGif.Text = "导出GIF文件";
			this.buttonGif.UseVisualStyleBackColor = false;
			this.buttonGif.Click += new System.EventHandler(this.ButtonGifClick);
			// 
			// trackBarScale
			// 
			this.trackBarScale.Location = new System.Drawing.Point(230, 195);
			this.trackBarScale.Maximum = 19;
			this.trackBarScale.Minimum = 1;
			this.trackBarScale.Name = "trackBarScale";
			this.trackBarScale.Size = new System.Drawing.Size(173, 45);
			this.trackBarScale.TabIndex = 14;
			this.trackBarScale.Value = 1;
			this.trackBarScale.Scroll += new System.EventHandler(this.TrackBarScaleScroll);
			// 
			// labelQ
			// 
			this.labelQ.Font = new System.Drawing.Font("微软雅黑", 10.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
			this.labelQ.ForeColor = System.Drawing.Color.Black;
			this.labelQ.Location = new System.Drawing.Point(230, 168);
			this.labelQ.Name = "labelQ";
			this.labelQ.Size = new System.Drawing.Size(170, 28);
			this.labelQ.TabIndex = 15;
			this.labelQ.Text = "图像质量:";
			// 
			// labelSize
			// 
			this.labelSize.Font = new System.Drawing.Font("微软雅黑", 10.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
			this.labelSize.ForeColor = System.Drawing.Color.DimGray;
			this.labelSize.Location = new System.Drawing.Point(230, 243);
			this.labelSize.Name = "labelSize";
			this.labelSize.Size = new System.Drawing.Size(170, 28);
			this.labelSize.TabIndex = 16;
			this.labelSize.Text = "图像尺寸: ";
			// 
			// ScreenSetForm
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.BackColor = System.Drawing.Color.WhiteSmoke;
			this.ClientSize = new System.Drawing.Size(412, 304);
			this.Controls.Add(this.labelSize);
			this.Controls.Add(this.labelQ);
			this.Controls.Add(this.trackBarScale);
			this.Controls.Add(this.label5);
			this.Controls.Add(this.label6);
			this.Controls.Add(this.buttonGif);
			this.Controls.Add(this.label4);
			this.Controls.Add(this.label3);
			this.Controls.Add(this.buttonFull);
			this.Controls.Add(this.label2);
			this.Controls.Add(this.comboBox1);
			this.Controls.Add(this.button1);
			this.Controls.Add(this.label1);
			this.MaximizeBox = false;
			this.MinimizeBox = false;
			this.Name = "ScreenSetForm";
			this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
			this.Text = "屏幕截图";
			this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.LocateFormFormClosing);
			((System.ComponentModel.ISupportInitialize)(this.trackBarScale)).EndInit();
			this.ResumeLayout(false);
			this.PerformLayout();
		}
		private System.Windows.Forms.Label labelSize;
		private System.Windows.Forms.Label labelQ;
		private System.Windows.Forms.TrackBar trackBarScale;
		private System.Windows.Forms.Button buttonGif;
		private System.Windows.Forms.Label label6;
		private System.Windows.Forms.Label label5;
		private System.Windows.Forms.Label label4;
		private System.Windows.Forms.Label label3;
		private System.Windows.Forms.Timer timer1;
		private System.Windows.Forms.Button buttonFull;
		private System.Windows.Forms.Label label2;
		private System.Windows.Forms.ComboBox comboBox1;
		private System.Windows.Forms.Label label1;
		private System.Windows.Forms.Button button1;
	}
}
