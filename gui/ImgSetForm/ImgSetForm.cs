﻿
using System;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Windows.Forms;
using n_LocatePanel;
using n_SimPanel;
using n_SimForm;
using n_CommonData;
using n_Sprite;
using System.IO;

namespace n_ImgSetForm
{
/// <summary>
/// Description of LocateForm.
/// </summary>
public partial class ImgSetForm : Form
{
	SimPanel SPanel;
	SimForm SimBox;
	
	// 1:角色  2:背景图片  3:地图
	int SType;
	
	const int ImgWidth = 50;
	const int ImgHeight = 50;
	
	
	public ImgSetForm( SimForm sm, SimPanel sp )
	{
		InitializeComponent();
		
		SimBox = sm;
		SPanel = sp;
	}
	
	//运行
	public void Run( int t )
	{
		SType = t;
		
		string path = null;
		if( SType == 1 ) {
			path = n_OS.OS.SystemRoot + "game" + n_OS.OS.PATH_S + "精灵" + n_OS.OS.PATH_S;
		}
		else if( SType == 2 ) {
			path = n_OS.OS.SystemRoot + "game" + n_OS.OS.PATH_S + "背景" + n_OS.OS.PATH_S;
		}
		else if( SType == 3 ) {
			path = n_OS.OS.SystemRoot + "game" + n_OS.OS.PATH_S + "地图" + n_OS.OS.PATH_S;
		}
		else {
			MessageBox.Show( "<ImgSetForm> 未知类型: " + SType );
			return;
		}
		
		imageList1.ImageSize = new Size(ImgWidth, ImgHeight);
		imageList1.Images.Clear();
		this.listView1.Clear();
		this.listView1.BeginUpdate();
		
		A_FindFile( path );
		
		this.listView1.EndUpdate();
		
		this.listView1.LargeImageList = this.imageList1;
		
		this.Visible = true;
	}
	
	//遍历指定的文件夹中的全部文件
	void A_FindFile( string sSourcePath )
	{
		//在指定目录及子目录下查找文件,在list中列出子目录及文件
		DirectoryInfo Dir = new DirectoryInfo( sSourcePath );
		DirectoryInfo[] DirSub = Dir.GetDirectories();
		
		//遍历当前文件夹中的所有文件
		int i = 0;
		foreach( FileInfo f in Dir.GetFiles("*.*", SearchOption.TopDirectoryOnly) ) {
			
			string FilePath = Dir + f.ToString();
			imageList1.Images.Add( GetStdImg( new Bitmap( FilePath ) ) );
			ListViewItem lvi = new ListViewItem();
			lvi.ImageIndex = i;
			lvi.Text = f.ToString();
			lvi.Name = FilePath;
			listView1.Items.Add(lvi);
			i++;
		}
		
		/*
		//遍历所有的子文件夹
		foreach( DirectoryInfo d in DirSub ) {
			A_FindFile( USEList, Dir + d.ToString() + @"\" );
		}
		*/
	}
	
	//获取标准尺寸的图片
	Image GetStdImg( Bitmap spm )
	{
		Image n = new Bitmap( ImgWidth, ImgWidth );
		Graphics g = Graphics.FromImage( n );
		g.SmoothingMode = SmoothingMode.HighQuality;
		g.InterpolationMode = System.Drawing.Drawing2D.InterpolationMode.Default;
		g.PixelOffsetMode = System.Drawing.Drawing2D.PixelOffsetMode.Half;
		
		g.Clear( Color.FromArgb( 0, 0, 0, 0 ) );
		
		int sww = spm.Width;
		int shh = spm.Height;
		if( sww > shh ) {
			shh = shh * ImgWidth / sww;
			sww = ImgWidth;
		}
		else {
			sww = sww * ImgHeight / shh;
			shh = ImgHeight;
		}
		g.DrawImage( spm, (ImgWidth-sww)/2, (ImgHeight-shh)/2, sww, shh );
		
		return n;
	}
	
	void Deal( string path )
	{
		string FileName = path;
		
		
		//注意: 临时处理 - 这里始终切换为当前目录
		FileName = CommonData.Flag_GetCuPath( FileName );
		if( FileName != path ) {
			try {
				File.Copy( path, FileName );
			}
			catch {
				MessageBox.Show( "复制到当前文件夹失败: \n" + path + "\n" + FileName );
				return;
			}
		}
		
		string FLG_FileName = CommonData.Flag_SetPathFlag( FileName );
		
		if( SType == 1 ) {
			Sprite sp = (Sprite)SPanel.SelObj;
			sp.AddImage( FLG_FileName );
			SimBox.listBoxImage.Items.Add( FLG_FileName );
			SimBox.listBoxImage.SelectedIndex = sp.IIndex;
		}
		if( SType == 2 ) {
			Bitmap temp = new Bitmap( FileName );
			SimPanel.BackBmp = new Bitmap( temp );
			temp.Dispose();
			SimPanel.BackIamgePath = FLG_FileName;
			SimBox.textBoxBackImgWidth.Text = SimPanel.BackBmp.Width.ToString();
			SimBox.textBoxBackImgHeight.Text = SimPanel.BackBmp.Height.ToString();
		}
		if( SType == 3 ) {
			SimPanel.MapImgFile = FLG_FileName;
			Bitmap b = new Bitmap( FLG_FileName );
			SPanel.SetBitmap( new Bitmap( b ) );
			b.Dispose();
		}
		this.Visible = false;
	}
	
	//====================================================================
	
	void LocateFormFormClosing(object sender, FormClosingEventArgs e)
	{
		this.Visible = false;
		e.Cancel = true;
	}
	
	void ButtonFileClick(object sender, EventArgs e)
	{
		//打开文件对话框
		OpenFileDialog OpenFileDlg = new OpenFileDialog();
		OpenFileDlg.Filter = "所有类型文件 | *.*";
		OpenFileDlg.Title = "请选择图片文件";
		
		OpenFileDlg.InitialDirectory = G.ccode.FilePath;
		//OpenFileDlg.InitialDirectory = n_OS.OS.SystemRoot + "game" + n_OS.OS.PATH_S + "精灵";
		
		DialogResult dlgResult = OpenFileDlg.ShowDialog();
		if(dlgResult == DialogResult.OK) {
			
			if( !OpenFileDlg.FileName.StartsWith( G.ccode.FilePath ) ) {
				MessageBox.Show( "建议先把图片放到和源文件同一个目录下, 再打开" );
				//return;
			}
			Deal( OpenFileDlg.FileName );
		}
	}
	
	void ListView1SelectedIndexChanged(object sender, EventArgs e)
	{
		Deal( this.listView1.SelectedItems[0].Name );
	}
}
}

