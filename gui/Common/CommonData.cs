﻿
using System;
using System.IO;

namespace n_CommonData
{
public static class CommonData
{
	//获取当前的工作目录
	public static string GetBasePath()
	{
		return G.ccode.FilePath;
	}
	
	//尝试读取一个文件的完整路径
	public static string GetFullPath( string FileName )
	{
		if( FileName == null ) {
			return null;
		}
		if( FileName.LastIndexOf( "\\" ) == -1 || !System.IO.Directory.Exists( FileName.Remove( FileName.LastIndexOf( "\\" ) ) ) ) {
			FileName = G.ccode.FilePath + FileName;
		}
		if( System.IO.File.Exists( FileName ) ) {
			return FileName;
		}
		//else if( System.IO.File.Exists( BasePath + FileName ) ) {
		//	return BasePath + FileName;
		//}
		else {
			return null;
		}
	}
	
	//==============================================================
	
	//获取指定文件切换到当前目录
	public static string Flag_GetCuPath( string fname )
	{
		if( !fname.StartsWith( G.ccode.FilePath ) ) {
			string n = Path.GetFileName( fname );
			return G.ccode.FilePath + n;
		}
		return fname;
	}
	
	//给一个文件路径增加标记
	public static string Flag_SetPathFlag( string FileName )
	{
		if( FileName.StartsWith( G.ccode.FilePath ) ) {
			FileName = n_GUIcoder.PathFlag.Default + FileName.Remove( 0, G.ccode.FilePath.Length );
		}
		else if( FileName.StartsWith( n_OS.OS.SystemRoot ) ) {
			FileName = n_GUIcoder.PathFlag.SYS + FileName.Remove( 0, n_OS.OS.SystemRoot.Length );
		}
		else {
			FileName = n_GUIcoder.PathFlag.Global + FileName;
		}
		return FileName;
	}
	
	//从标记文件获取真实的路径
	public static string Flag_GetRealPath( string FileName )
	{
		if( FileName.StartsWith( n_GUIcoder.PathFlag.SYS ) ) {
			FileName = n_OS.OS.SystemRoot + FileName.Remove( 0, n_GUIcoder.PathFlag.SYS.Length );
		}
		else if( FileName.StartsWith( n_GUIcoder.PathFlag.Global ) ) {
			FileName = FileName.Remove( 0, n_GUIcoder.PathFlag.Global.Length );
		}
		else {
			FileName = G.ccode.FilePath + FileName;
		}
		return FileName;
	}
}
}



