﻿
using System;
using n_SG_MyControl;
using Dongzr.MidiLite;
using n_SG_MyImagePanel;

namespace n_Linker
{
public static class Linker
{
	public static MyImagePanel tMyImagePanel;
	
	public delegate void D_StartRun();
	public static D_StartRun StartRun;
	
	public delegate void D_DataChannelWrite( int addr, int data );
	public static D_DataChannelWrite DataChannelWrite;
	
	public static void SendData( int n, byte[] Buffer )
	{
		//...
	}
	
	public static void SendEventFlag( int a, object o )
	{
		
	}
}
}

namespace n_KeyBoard
{
	public class KeyBoard
	{
		public delegate void temp( int a );
		
		public static void SetKeyEvent( int a, temp o, int ki )
		{
			
		}
		
		public static void SetKeyMaxTick( int a, int t )
		{
			
		}
		
		public static void Open( int a )
		{
			
		}
		
		public static void Close( int a )
		{
			
		}
	}
}

namespace n_M
{
	public static class M
	{
		public delegate void D_AfterDraw();
		public static D_AfterDraw AfterDraw;
		
		public delegate void D_SysTick();
		public static D_SysTick SysTick;
		
		public static MmTimer SpriteTimer;
		public const float RunTime = 0.05F;
		
		public static MyControl SelectedObj;
		public static MyControl[] MyControlList;
		public static int ListLength;
		public static int CamMidX;
		public static int CamMidY;
		public static float CamScale;
		public static float CamAngle;
		public static int V_CamWidth;
		public static int V_CamHeight;
		public static bool isStart;
		
		public static int MaxWidth;
		public static int MaxHeight;
	}
}
