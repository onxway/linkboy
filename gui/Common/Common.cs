﻿
using System;

namespace n_Common
{
public static class Common
{
	public class Font
	{
		public string Name;
		public float Size;
		
		public bool Bold;
		public bool Italic;
		public bool Underline;
		public bool Strikeout;
		
		//构造函数
		public Font( string n, float s )
		{
			Name = n;
			Size = s;
			
			Bold = false;
			Italic = false;
			Underline = false;
			Strikeout = false;
		}
	}
	
	//初始化
	public static void Init()
	{
	}
	
	//根据文本描述返回字体
	public static Font GetFontFromString( string Font )
	{
		//n_SYS.SYS.AddMessage( "<" + Font + ">\n" );
		
		string[] FontList = Font.Split( ',' );
		string FontName = FontList[ 0 ].Trim( ' ' );
		float FontSize = float.Parse( FontList[ 1 ].Trim( ' ' ) );
		if( FontList.Length > 3 ) {
			return null;
		}
		
		#if OS_android
		FontSize = FontSize * 12 / 3;
		#endif
		
		Font f = new Font( FontName, FontSize );
		
		if( FontList.Length == 3 ) {
			string BIUS = FontList[ 2 ].Trim( ' ' );
			if( BIUS.IndexOf( "B" ) != -1 ) {
				f.Bold = true;
			}
			if( BIUS.IndexOf( "I" ) != -1 ) {
				f.Italic = true;
			}
			if( BIUS.IndexOf( "U" ) != -1 ) {
				f.Underline = true;
			}
			if( BIUS.IndexOf( "S" ) != -1 ) {
				f.Strikeout = true;
			}
		}
		return f;
	}
	
	public const int BSIZE = 10;
	
	//计算一个数据的网格化结果 (x = 4.99 也属于 0-4 )
	public static int GetCrossValue( float x )
	{
		x += BSIZE/2;
		int xx = (int)x;
		if( xx >= 0 ) {
			return xx / BSIZE * BSIZE;
		}
		else {
			return (xx+1) / BSIZE * BSIZE - BSIZE;
		}
	}
}
}

