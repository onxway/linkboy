﻿
namespace n_CodeOutForm
{
using System;
using System.Windows.Forms;

/// <summary>
/// Description of WorkForm.
/// </summary>
public partial class CodeForm : Form
{
	string[] cut;
	
	//主窗口
	public CodeForm()
	{
		InitializeComponent();
	}
	
	//运行
	public void Run()
	{
		this.Visible = true;
		this.Activate();
	}
	
	//设置字节码
	public void SetByteCode( string s )
	{
		richTextBoxByteCode.Text = s;
		cut = s.Split( '\n' );
	}
	
	//设置编译结果
	public void SetResult( string s )
	{
		richTextBoxResult.Text = s;
	}
	
	//设置当前要执行的指令行
	public void SetLine( int PC )
	{
		string end = "//[" + PC + "]";
		
		for( int i = 0; i < cut.Length; ++i ) {
			
			if( cut[i].EndsWith( end ) ) {
				int sindex = richTextBoxByteCode.GetFirstCharIndexFromLine( i );
				richTextBoxByteCode.SelectionStart = sindex;
				richTextBoxByteCode.SelectionLength = cut[i].Length;
				
				return;
			}
		}
	}
	
	void Button1Click(object sender, EventArgs e)
	{
		this.richTextBoxByteCode.Focus();
		this.richTextBoxByteCode.SelectAll();
		
		System.Threading.Thread.Sleep( 200 );
		
		this.richTextBoxByteCode.Copy();
		this.richTextBoxByteCode.SelectionLength = 0;
		
		//A.FlashBox.Run( "点阵编码数据已复制到剪贴板中" );
	}
	
	void CodeFormFormClosing(object sender, FormClosingEventArgs e)
	{
		e.Cancel = true;
		this.Visible = false;
	}
}
}



