﻿
//元件定义信息分析类
namespace n_RecordMacro
{
using System;
using n_ET;
using n_MemberType;
using n_Parse;
using n_ParseNet;
using n_UnitList;
using n_UnitNode;
using n_WordList;
using n_VarList;
using n_VarNode;
using n_VarType;
using n_VdataList;
using n_VdataNode;
using n_StructList;
using n_StructNode;
using n_FunctionList;
using n_FunctionNode;
using n_Config;
using n_ConstString;
using n_Interrupt;
using n_SST;

public static class RecordMacro
{
	public static string UnitName;
	static int NameIndex;
	static int ChipIndex;
	static int Level;
	
	public static bool isRecord;
	
	public static int Step;
	public const int S_Unit = 0;
	public const int S_Unit_Link = 1;
	public const int S_Const = 2;
	public const int S_Const_Link = 3;
	public const int S_VData = 4;
	public const int S_VData_Link = 5;
	public const int S_Struct = 6;
	public const int S_Struct_Link = 7;
	public const int S_FunVar = 8;
	public const int S_FunVar_Link = 9;
	public const int S_Macro = 10;
	
	//复位
	public static void Reset()
	{
		n_Param.Param.UserspaceOn = false;
		
		NameIndex = 0;
		StartFuncOK = false;
		FunctionIndex = 0;
		
		//元件列表初始化
		UnitList.Clear();
		
		//常量列表初始化
		VarList.Clear();
		
		//虚拟数据类型列表初始化
		VdataList.Clear();
		
		//结构体列表初始化
		StructList.Clear();
		
		//函数列表初始化
		FunctionList.Clear();
		
		//接口占用列表初始化
		n_InterNumberList.InterNumberList.Clear();
		
		//中断列表初始化
		Interrupt.Reset( Config.ChipNumber );
	}
	
	public static void ResetChip()
	{
		ChipIndex = 0;
		Config.SetChipIndex( ChipIndex );
	}
	
	//获取元件信息,生成: 元件列表 函数列表 变量列表
	public static void Record()
	{
		isRecord = true;
		Level = 0;
		Parse.DealWith( ref UnitName, new Parse.MemberListHandle( 成员列表 ) );
		isRecord = false;
	}
	
	static void 元件( int Index )
	{
		Parse.Unit( ref UnitName, Index, new Parse.MemberListHandle( 成员列表 ) );
	}
	
	public static void 成员列表( int Index )
	{
		for( int i = 1; i < ParseNet.NodeSet[ Index ].Length; ++i ) {
			int index = int.Parse( ParseNet.NodeSet[ Index ][ i ] );
			
			bool dealMacro = (Step == S_Macro && Level != 0);
			
			//判断是否元件定义
			if( ParseNet.NodeSet[ index ][ 0 ] == ParseNet.Node.元件 ) {
				if( Step == S_Unit || dealMacro ) {
					记录元件信息( index );
				}
				元件( index );
				continue;
			}
			//判断常量定义
			if( ParseNet.NodeSet[ index ][ 0 ] == ParseNet.Node.常量 ) {
				if( Step == S_Const || dealMacro ) {
					常量( index );
				}
				continue;
			}
			//判断是否虚拟数据定义
			if( ParseNet.NodeSet[ index ][ 0 ] == ParseNet.Node.虚拟数据定义 ) {
				if( Step == S_Const || dealMacro ) {
					VData虚拟数据定义( index );
				}
				continue;
			}
			if( ParseNet.NodeSet[ index ][ 0 ] == ParseNet.Node.结构体定义 ) {
				if( Step == S_Struct || dealMacro ) {
					结构体( index );
				}
				continue;
			}
			
			//判断函数定义
			if( ParseNet.NodeSet[ index ][ 0 ] == ParseNet.Node.函数 ) {
				if( Step == S_FunVar || dealMacro ) {
					函数( index );
					++FunctionIndex;
				}
				continue;
			}
			if( ParseNet.NodeSet[ index ][ 0 ] == ParseNet.Node.静态变量 ) {
				if( Step == S_FunVar || dealMacro ) {
					静态变量( index );
					SST.TempClose = false;
				}
				continue;
			}
			//判断Chisel函数定义
			if( ParseNet.NodeSet[ index ][ 0 ] == ParseNet.Node.Chisel函数 ) {
				if( Step == S_FunVar || dealMacro ) {
					chisel函数( index );
					++FunctionIndex;
				}
				continue;
			}
			if( ParseNet.NodeSet[ index ][ 0 ] == ParseNet.Node.Chisel静态变量 ) {
				if( Step == S_FunVar || dealMacro ) {
					chisel静态变量( index );
					SST.TempClose = false;
				}
				continue;
			}
			//判断是否为设置编译参数
			if( ParseNet.NodeSet[ index ][ 0 ] ==ParseNet.Node.设置编译参数 ) {
				//if( Step == S_FunVar || dealMacro ) {
					//获取参数信息
					int ParamIndex = int.Parse( ParseNet.NodeSet[ index ][ 2 ] );
					string param = WordList.GetWord( ParamIndex );
					if( param == n_Param.Param.FUNCTION_USED ) {
						//可能是后续会处理这个标志, 所以不需要处理
						//n_Param.Param.function_used = true;
					}
					else {
						n_Deploy.Deploy.DealParam( ParamIndex, param, "<FuncAndVar>" );
					}
				//}
				continue;
			}
			
			
			//判断是否为设置当前包
			if( ParseNet.NodeSet[ index ][ 0 ] ==ParseNet.Node.设置当前包 ) {
				if( Step == S_Struct || Step == S_FunVar || dealMacro ) {
					++ChipIndex;
					Config.SetChipIndex( ChipIndex );
				}
				continue;
			}
			//判断是否连接器定义
			if( ParseNet.NodeSet[ index ][ 0 ] == ParseNet.Node.连接器 ) {
				if( Step == S_Unit_Link || dealMacro ) {
					unit连接器( index );
				}
				if( Step == S_Const_Link || dealMacro ) {
					常量连接器( index );
				}
				if( Step == S_VData_Link || dealMacro ) {
					VData连接器( index );
				}
				if( Step == S_Struct_Link || dealMacro ) {
					Struct连接器( index );
				}
				if( Step == S_FunVar_Link || dealMacro ) {
					FuncAndVarLink连接器( index );
				}
				continue;
			}
			//-----------------------------------------------------------------
			if( Step < S_Macro ) {
				continue;
			}
			//判断宏定义
			if( ParseNet.NodeSet[ index ][ 0 ] == ParseNet.Node.宏静态如果语句 ) {
				Level++;
				n_FlowSence.FlowSence.宏if语句( index, n_FlowSence.FlowSence.MACRO );
				Level--;
				continue;
			}
			if( ParseNet.NodeSet[ index ][ 0 ] == ParseNet.Node.宏静态如果否则语句 ) {
				Level++;
				n_FlowSence.FlowSence.宏ifelse语句( index, n_FlowSence.FlowSence.MACRO );
				Level--;
				continue;
			}
			if( ParseNet.NodeSet[ index ][ 0 ] == ParseNet.Node.宏静态loop语句 ) {
				Level++;
				n_FlowSence.FlowSence.宏loop语句( index, n_FlowSence.FlowSence.MACRO );
				Level--;
				continue;
			}
			if( ParseNet.NodeSet[ index ][ 0 ] == ParseNet.Node.宏表达式语句 ) {
				
				int ExpIndex = int.Parse( ParseNet.NodeSet[ index ][ 5 ] );
				int MidVar = n_Expression.Expression.ConstExp( ExpIndex );
			}
		}
	}
	
	//------------------------------------------------------------------
	//unit元件
	
	static void 记录元件信息( int Index )
	{
		string UnitName = RecordMacro.UnitName;
		
		//获取成员类型
		string VisitType = null;
		string RealRefType = null;
		string ThisUnitName = null;
		int NameIndex = 0;
		int WordNameIndex = int.Parse( ParseNet.NodeSet[ Index ][ 3 ] );
		
		//处理重命名项
		int RenameIndex = int.Parse( ParseNet.NodeSet[ Index ][ 1 ] );
		if( RenameIndex == -1 ) {
			int MemberTypeIndex = int.Parse( ParseNet.NodeSet[ Index ][ 2 ] );
			Parse.GetMemberType( MemberTypeIndex, ref VisitType, ref RealRefType );
			NameIndex = int.Parse( ParseNet.NodeSet[ Index ][ 4 ] );
			ThisUnitName = UnitName + "." + WordList.GetWord( NameIndex );
		}
		else {
			NameIndex = Parse.GetRename( RenameIndex, UnitName, ref VisitType, ref RealRefType, ref ThisUnitName );
		}
		if( UnitList.isExist( ThisUnitName ) ) {
			ET.WriteParseError( NameIndex, "已经定义了同名元件: " + ThisUnitName );
			return;
		}
		//判断成员列表是否为空
		int MemberIndex = int.Parse( ParseNet.NodeSet[ Index ][ 7 ] );
		if( ParseNet.NodeSet[ MemberIndex ].Length >= 2 && RealRefType == MemberType.RealRefType.link ) {
			ET.WriteParseError( NameIndex, "元件类型为链接类型时不可以包含成员列表" );
		}
		UnitNode node = new UnitNode( VisitType, RealRefType, ThisUnitName, WordNameIndex );
		UnitList.Add( node );
		
		//处理链接类型
		int LinkIndex = int.Parse( ParseNet.NodeSet[ Index ][ 9 ] );
		if( LinkIndex != -1 ) {
			//提取第二个成员信息
			int SecondIndex = int.Parse( ParseNet.NodeSet[ LinkIndex ][ 2 ] );
			int ErrorIndex2 = 0;
			string SecondName = null;
			Parse.GetMemberNames( UnitName, SecondIndex, ref SecondName, ref ErrorIndex2 );
			if( RealRefType != MemberType.RealRefType.link ) {
				ET.WriteParseError( int.Parse( ParseNet.NodeSet[ Index ][ 4 ] ), "连接器的左端必须为引用类型成员:" + UnitName );
			}
			node.TargetMemberName = SecondName;
		}
	}
	
	static void unit连接器( int Index )
	{
		string UnitName = RecordMacro.UnitName;
		
		//提取当前连接器的权限
		bool Super = false;
		int SuperIndex = int.Parse( ParseNet.NodeSet[ Index ][ 1 ] );
		if( SuperIndex != -1 ) {
			Super = true;
		}
		//提取第一个成员信息
		int FirstIndex = int.Parse( ParseNet.NodeSet[ Index ][ 2 ] );
		int ErrorIndex1 = 0;
		string FirstName = null;
		Parse.GetMemberNames( UnitName, FirstIndex, ref FirstName, ref ErrorIndex1 );
		
		//提取第二个成员信息
		int SecondIndex = int.Parse( ParseNet.NodeSet[ Index ][ 4 ] );
		int ErrorIndex2 = 0;
		string SecondName = null;
		Parse.GetMemberNames( UnitName, SecondIndex, ref SecondName, ref ErrorIndex2 );
		
		//判断成员是否为元件类型
		int FirstUnitIndex = UnitList.GetDirectIndex( FirstName );
		
		//2016.12.22 新增引用置换, 当目标link所在的Lunit为link类型时, 先解析Lunit的实际元件
		if( FirstUnitIndex == -1 ) {
			string LastName = FirstName.Remove( 0, FirstName.LastIndexOf( '.' ) + 1 );
			string FName = FirstName.Remove( FirstName.LastIndexOf( '.' ) );
			int i = UnitList.GetDirectIndex( FName );
			if( i != -1 ) {
				FirstName = UnitList.Get( i ).TargetMemberName + "." + LastName;
				FirstUnitIndex = UnitList.GetDirectIndex( FirstName );
			}
		}
		if( FirstUnitIndex != -1 ) {
			if( UnitList.Get( FirstUnitIndex ).RealRefType != MemberType.RealRefType.link ) {
				if( Super ) {
					UnitList.Get( FirstUnitIndex ).RealRefType = MemberType.RealRefType.link;
				}
				else {
					ET.WriteParseError( ErrorIndex1, "连接器的左端必须为引用类型成员:" + FirstName );
					return;
				}
			}
			if( UnitList.GetDirectIndex( SecondName ) == -1 ) {
				ET.WriteParseError( ErrorIndex2, "未定义的元件: " + SecondName );
			}
			UnitList.Get( FirstUnitIndex ).TargetMemberName = SecondName;
			
			ParseNet.NodeSet[ Index ][ 3 ] = "-1";
			
			return;
		}
		//后续可能还会进行判断, 所以这里不报错, 直接退出
		//else {
		//	ET.WriteParseError( ErrorIndex1, "未定义的元素: " + FirstName );
		//}
	}
	
	//------------------------------------------------------------------
	//常量
	
	static void 常量( int Index )
	{
		string UnitName = RecordMacro.UnitName;
		
		//获取成员类型
		string VisitType = null;
		string RealRefType = null;
		int MemberTypeIndex = int.Parse( ParseNet.NodeSet[ Index ][ 1 ] );
		Parse.GetMemberType( MemberTypeIndex, ref VisitType, ref RealRefType );
		
		//获取常量名称
		int NameIndex = int.Parse( ParseNet.NodeSet[ Index ][ 4 ] );
		string Name = WordList.GetWord( NameIndex );
		if( !VarList.CanDefineStatic( UnitName + "." + Name ) ) {
			ET.WriteParseError( NameIndex, "常量名非法:已经定义了同名的成员--" + UnitName + "." + Name );
			return;
		}
		//获取常量的定义类型
		int VarTypeIndex = int.Parse( ParseNet.NodeSet[ Index ][ 3 ] );
		string vartype = Parse.GetType( UnitName, VarTypeIndex );
		if( vartype == null ) {
			return;
		}
		if( !VarType.isBase( vartype ) ) {
			ET.WriteParseError( NameIndex, "常量的类型应为基本类型: " + vartype );
			return;
		}
		//添加常量
		VarNode node = new VarNode();
		node.SetStaticValue( UnitName + "." + Name, vartype, VisitType, RealRefType, true, NameIndex, -1, NameIndex );
		node.isConst = true;
		VarList.Add( node );
		
		if( RealRefType == MemberType.RealRefType.link ) {
			ET.WriteParseError( NameIndex, "定义常量为链接类型时不可以指定常量值" );
			return;
		}
		//获取常量的实际类型
		node.Value = "I " + ParseNet.NodeSet[ Index ][ 6 ];
		
//		if( ConstType != vartype && !Operation.CanSwitch( ConstType, vartype ) ) {
//			ET.WriteParseError( ConstIndex, "常量类型与定义类型赋值不兼容: " + vartype + " <- " + ConstType );
//			return;
//		}
	}
	
	public static void 常量连接器( int Index )
	{
		string UnitName = RecordMacro.UnitName;
		
		//提取第一个成员信息
		int FirstIndex = int.Parse( ParseNet.NodeSet[ Index ][ 2 ] );
		int ErrorIndex1 = 0;
		string FirstName = null;
		Parse.GetMemberNames( UnitName, FirstIndex, ref FirstName, ref ErrorIndex1 );
		
		//提取第二个成员信息
		int SecondIndex = int.Parse( ParseNet.NodeSet[ Index ][ 4 ] );
		int ErrorIndex2 = 0;
		string SecondName = null;
		Parse.GetMemberNames( UnitName, SecondIndex, ref SecondName, ref ErrorIndex2 );
		
		//判断成员是否为静态变量类型
		int FirstConstIndex = VarList.GetStaticIndex( FirstName );
		if( FirstConstIndex != -1 ) {
			
			//这里允许对一个常量重定向
			if( VarList.Get( FirstConstIndex ).isConst ) {
				VarList.Get( FirstConstIndex ).RealRefType = MemberType.RealRefType.link;
			}
			if( VarList.Get( FirstConstIndex ).RealRefType != MemberType.RealRefType.link ) {
				ET.WriteParseError( ErrorIndex1, "连接器的左端必须为引用类型常量:" + FirstName );
				return;
			}
			VarList.Get( FirstConstIndex ).TargetMemberName = SecondName;
			return;
		}
	}
	
	public static void Complete()
	{
		//遍历常量列表,计算每个常量的值
		int time = 0;
		while( !CompleteExpressionValue() ) {
			++time;
			if( ET.isErrors() ) {
			   	break;
			}
			if( time > 500 ) {
				ET.WriteLineError( 0, 0, "常量计算循环过大" );
				break;
			}
		}
	}
	
	//遍历常量列表,计算每个常量的值
	static bool CompleteExpressionValue()
	{
		bool isOK = true;
		for( int i = 0; i < VarList.length; ++i ) {
			VarNode n = VarList.Get( i );
			if( n.isConst && n.Value.StartsWith( "I " ) ) {
				int Index = int.Parse( n.Value.Remove( 0, 2 ) );
				string UnitName = n.Name.Remove( n.Name.LastIndexOf( "." ) );
				string Result = ConstExpression.GetExpressionValue( Index, UnitName );
				if( Result == null ) {
					isOK = false;
				}
				else {
					n.Value = Result;
				}
			}
		}
		return isOK;
	}
	
	//------------------------------------------------------------------
	//VData
	
	static void VData虚拟数据定义( int Index )
	{
		string UnitName = RecordMacro.UnitName;
		
		//获取成员类型
		string VisitType = null;
		string RealRefType = null;
		int MemberTypeIndex = int.Parse( ParseNet.NodeSet[ Index ][ 1 ] );
		Parse.GetMemberType( MemberTypeIndex, ref VisitType, ref RealRefType );
		
		int VdataNameIndex = int.Parse( ParseNet.NodeSet[ Index ][ 3 ] );
		string VdataName = UnitName + "." + WordList.GetWord( VdataNameIndex );
		
		//判断虚拟类型名是否存在
		if( VdataList.isExist( VdataName ) ) {
			ET.WriteParseError( VdataNameIndex,	"已经定义了同名的虚拟数据类型: " + VdataName );
		}
		string sDataType = null;
		string sAddrType = null;
		string sUnitType = null;
		string sPreName = null;
		int Start = 0;
		int End = 0;
		bool AutoAllot = false;
		
		int MemberIndex = int.Parse( ParseNet.NodeSet[ Index ][ 5 ] );
		int Length = ParseNet.NodeSet[ MemberIndex ].Length;
		for( int i = 1; i < Length; ++i ) {
			int MemIndex = int.Parse( ParseNet.NodeSet[ MemberIndex ][ i ] );
			
			//判断是否为虚拟数据元件引用
			if( ParseNet.NodeSet[ MemIndex ][ 0 ] == ParseNet.Node.虚拟数据元件引用 ) {
				if( sUnitType != null ) {
					int UnitErrorIndex = int.Parse( ParseNet.NodeSet[ MemIndex ][ 1 ] );
					ET.WriteParseError( UnitErrorIndex, "虚拟数据类型定义中已定义元件链接目标" );
				}
				int UnitLinkIndex = int.Parse( ParseNet.NodeSet[ MemIndex ][ 3 ] );
				int eIndex = -1;
				string Name = null;
				Parse.GetMemberNames( UnitName, UnitLinkIndex, ref Name, ref eIndex );
				sUnitType = Name;
			}
			else if( ParseNet.NodeSet[ MemIndex ][ 0 ] == ParseNet.Node.虚拟数据前缀指定 ) {
				if( sPreName != null ) {
					int UnitErrorIndex = int.Parse( ParseNet.NodeSet[ MemIndex ][ 1 ] );
					ET.WriteParseError( UnitErrorIndex, "虚拟数据类型定义中已定义前缀指定" );
				}
				int VdataPreNameIndex = int.Parse( ParseNet.NodeSet[ MemIndex ][ 3 ] );
				sPreName = WordList.GetWord( VdataPreNameIndex );
			}
			//判断是否为虚拟数据地址限定
			else {
				if( sAddrType != null ) {
					int UnitErrorIndex = int.Parse( ParseNet.NodeSet[ MemIndex ][ 1 ] );
					ET.WriteParseError( UnitErrorIndex, "虚拟数据类型定义中已定义地址类型" );
				}
				//解析数据类型
				int DataTypeIndex = int.Parse( ParseNet.NodeSet[ MemIndex ][ 7 ] );
				sDataType = WordList.GetWord( DataTypeIndex );
				if( sDataType != VarType.BaseType.Bit && !VarType.BaseType.isUint( sDataType ) ) {
					ET.WriteParseError( DataTypeIndex,
					                   "虚拟数据类型中的数据类型应为无符号数类型: " + sDataType );
				}
				//获取地址类型 判断是否自动分配
				int AddrTypeIndex = int.Parse( ParseNet.NodeSet[ MemIndex ][ 4 ] );
				sAddrType = WordList.GetWord( AddrTypeIndex );
				if( !VarType.BaseType.isUint( sAddrType ) ) {
					ET.WriteParseError( Index, "虚拟数据类型中的地址类型应为无符号类型: " + sAddrType );
				}
				//读取地址类型和地址区间
				int AreaIndex = int.Parse( ParseNet.NodeSet[ MemIndex ][ 5 ] );
				if( AreaIndex == -1 ) {
					AutoAllot = false;
					continue;
				}
				AutoAllot = true;
				
				//获取地址区间
				int StartIndex = int.Parse( ParseNet.NodeSet[ AreaIndex ][ 1 ] );
				int EndIndex = int.Parse( ParseNet.NodeSet[ AreaIndex ][ 3 ] );
				
				//string SStart = WordList.GetWord( StartIndex );
				//string SEnd = WordList.GetWord( EndIndex );
				string SStart = ConstExpression.GetExpressionValue( StartIndex, UnitName );
				string SEnd = ConstExpression.GetExpressionValue( EndIndex, UnitName );
				
				string StartType = null;
				string EndType = null;
				SStart = ConstString.GetValue( ref StartType, SStart, StartIndex );
				SEnd = ConstString.GetValue( ref EndType, SEnd, EndIndex );
				Start = int.Parse( SStart );
				End = int.Parse( SEnd );
				
				if( Start > End ) {
					ET.WriteParseError( StartIndex, "虚拟数据类型中的起始地址不能大于结束地址: " + Start + " - " + End );
				}
				if( sAddrType == VarType.BaseType.Uint8 ) {
					if( End > 0xFF ) {
						ET.WriteParseError( EndIndex, "虚拟数据类型中的地址常量超出地址数据类型: " + sAddrType + " - " + End );
					}
				}
				if( sAddrType == VarType.BaseType.Uint16 ) {
					if( End > 0xFFFF ) {
						ET.WriteParseError( EndIndex, "虚拟数据类型中的地址常量超出地址数据类型: " + sAddrType + " - " + End );
					}
				}
			}
		}
		if( sDataType == null ) {
			sDataType = VarType.BaseType.Bit;
		}
		if( sUnitType == null ) {
			sUnitType = UnitName;
		}
		VdataNode node = new VdataNode(
			VisitType, RealRefType, VdataName, sDataType, sAddrType, sUnitType, AutoAllot, Start, End, UnitName, sPreName );
		VdataList.Add( node );
		
		//处理链接类型
		int LinkIndex = int.Parse( ParseNet.NodeSet[ Index ][ 7 ] );
		if( LinkIndex != -1 ) {
			//提取第二个成员信息
			int SecondIndex = int.Parse( ParseNet.NodeSet[ LinkIndex ][ 2 ] );
			int ErrorIndex2 = 0;
			string SecondName = null;
			Parse.GetMemberNames( UnitName, SecondIndex, ref SecondName, ref ErrorIndex2 );
			if( RealRefType != MemberType.RealRefType.link ) {
				ET.WriteParseError( int.Parse( ParseNet.NodeSet[ Index ][ 4 ] ), "连接器的左端必须为引用类型成员:" + UnitName );
			}
			node.TargetMemberName = SecondName;
		}
	}
	
	static void VData连接器( int Index )
	{
		string UnitName = RecordMacro.UnitName;
		
		//提取第一个成员信息
		int FirstIndex = int.Parse( ParseNet.NodeSet[ Index ][ 2 ] );
		int ErrorIndex1 = 0;
		string FirstName = null;
		Parse.GetMemberNames( UnitName, FirstIndex, ref FirstName, ref ErrorIndex1 );
		
		//提取第二个成员信息
		int SecondIndex = int.Parse( ParseNet.NodeSet[ Index ][ 4 ] );
		int ErrorIndex2 = 0;
		string SecondName = null;
		Parse.GetMemberNames( UnitName, SecondIndex, ref SecondName, ref ErrorIndex2 );
		
		//判断成员是否为虚拟数据类型
		int FirstVdataIndex = VdataList.GetDirectIndex( FirstName );
		if( FirstVdataIndex != -1 ) {
			if( VdataList.Get( FirstVdataIndex ).RealRefType != MemberType.RealRefType.link ) {
				ET.WriteParseError( ErrorIndex1, "连接器的左端必须为引用类型元件:" + FirstName );
				return;
			}
			if( VdataList.GetDirectIndex( SecondName ) == -1 ) {
				ET.WriteParseError( ErrorIndex2, "未定义的虚拟类型: " + SecondName );
			}
			VdataList.Get( FirstVdataIndex ).TargetMemberName = SecondName;
			return;
		}
	}
	
	//------------------------------------------------------------------
	//结构体
	
	static void 结构体( int Index )
	{
		string UnitName = RecordMacro.UnitName;
		
		//获取成员类型
		string VisitType = null;
		string RealRefType = null;
		int MemberTypeIndex = int.Parse( ParseNet.NodeSet[ Index ][ 1 ] );
		Parse.GetMemberType( MemberTypeIndex, ref VisitType, ref RealRefType );
		
		//获取结构体名称
		int NameIndex = int.Parse( ParseNet.NodeSet[ Index ][ 3 ] );
		string StructName = UnitName + "." + WordList.GetWord( NameIndex );
		if( StructList.GetDirectIndex( StructName ) != -1 ) {
			ET.WriteParseError( NameIndex, "当前的名称空间已经有同名的结构体类型: " + UnitName + "." + StructName );
			return;
		}
		StructNode node = null;
		
		//获取结构体成员列表
		int TypeIndex = int.Parse( ParseNet.NodeSet[ Index ][ 4 ] );
		int MemberIndex = int.Parse( ParseNet.NodeSet[ TypeIndex ][ 2 ] );
		
		int MemberNumber = ParseNet.NodeSet[ MemberIndex ].Length - 1;
		if( MemberNumber == 0 && RealRefType == MemberType.RealRefType.Real ) {
			ET.WriteParseError( NameIndex, "结构体的成员不能为空: " + UnitName + "." + StructName );
			return;
		}
		MemberNode[] MemberList = AddNewStruct( MemberIndex );
		if( MemberList == null ) {
			return;
		}
		node = new StructNode( VisitType, RealRefType, StructName, MemberList, UnitName, NameIndex );
		StructList.Add( node );
		
		//处理链接类型
		int LinkIndex = int.Parse( ParseNet.NodeSet[ Index ][ 5 ] );
		if( LinkIndex != -1 ) {
			//提取第二个成员信息
			int SecondIndex = int.Parse( ParseNet.NodeSet[ LinkIndex ][ 2 ] );
			int ErrorIndex2 = 0;
			string SecondName = null;
			Parse.GetMemberNames( UnitName, SecondIndex, ref SecondName, ref ErrorIndex2 );
			if( RealRefType != MemberType.RealRefType.link ) {
				ET.WriteParseError( int.Parse( ParseNet.NodeSet[ Index ][ 4 ] ), "连接器的左端必须为引用类型成员:" + UnitName );
			}
			node.TargetMemberName = SecondName;
		}
	}
	
	//添加并检查结构体类型
	public static string AddNewStructAndCheck( int MemberIndex, string UnitName )
	{
		int MemberNumber = ParseNet.NodeSet[ MemberIndex ].Length - 1;
		MemberNode[] MemberList = AddNewStruct( MemberIndex );
		if( MemberList == null ) {
			return null;
		}
		string StructName = NameIndex.ToString();
		++NameIndex;
		StructNode node = new StructNode( MemberType.VisitType.Private, MemberType.RealRefType.Real,
		                                 StructName, MemberList, UnitName, 0 );
		StructList.Add( node );
		StandardStructList();
		return StructName;
	}
	
	//添加新的结构体类型
	static MemberNode[] AddNewStruct( int MemberIndex )
	{
		int MemberNumber = ParseNet.NodeSet[ MemberIndex ].Length - 1;
		MemberNode[] MemberList = new MemberNode[ MemberNumber ];
		string MemberNameList = ",";
		for( int j = 1; j < ParseNet.NodeSet[ MemberIndex ].Length; ++j ) {
			int member = int.Parse( ParseNet.NodeSet[ MemberIndex ][ j ] );
			
			int MemberLocation = int.Parse( ParseNet.NodeSet[ member ][ 2 ] );
			string MemberName = WordList.GetWord( MemberLocation );
			if( MemberNameList.IndexOf( "," + MemberName + "," ) != -1 ) {
				ET.WriteParseError( MemberLocation, "结构体的这个成员已经定义过: " + MemberName );
			}
			MemberNameList += MemberName + ",";
			int TypeIndex = int.Parse( ParseNet.NodeSet[ member ][ 1 ] );
			MemberList[ j - 1 ] = new MemberNode( TypeIndex, MemberName, MemberLocation );
		}
		return MemberList;
	}
	
	static void Struct连接器( int Index )
	{
		string UnitName = RecordMacro.UnitName;
		
		//提取第一个成员信息
		int FirstIndex = int.Parse( ParseNet.NodeSet[ Index ][ 2 ] );
		int ErrorIndex1 = 0;
		string FirstName = null;
		Parse.GetMemberNames( UnitName, FirstIndex, ref FirstName, ref ErrorIndex1 );
		
		//提取第二个成员信息
		int SecondIndex = int.Parse( ParseNet.NodeSet[ Index ][ 4 ] );
		int ErrorIndex2 = 0;
		string SecondName = null;
		Parse.GetMemberNames( UnitName, SecondIndex, ref SecondName, ref ErrorIndex2 );
		
		//判断成员是否为结构体类型
		int FirstStructIndex = StructList.GetDirectIndex( FirstName );
		if( FirstStructIndex != -1 ) {
			if( StructList.Get( FirstStructIndex ).RealRefType != MemberType.RealRefType.link ) {
				ET.WriteParseError( ErrorIndex1, "连接器的左端必须为引用类型元件:" + FirstName );
				return;
			}
			StructList.Get( FirstStructIndex ).TargetMemberName = SecondName;
			return;
		}
	}
	
	//查找结构体信息
	public static void StandardStructList()
	{
		int Times = 0;
		bool HaveUnkownAddr;
		do {
			++Times;
			if( Times > 20 ) {
				ET.WriteLineError( 0, 0, "结构体定义的层数过大,或者您的程序中出现结构体的递归定义" );
				return;
			}
			HaveUnkownAddr = false;
			for( int i = 0; i < StructList.length; ++i ) {
				
				//如果当前结构体已经处理,转向下一个
				if( StructList.Get( i ).Size != -1 ) {
					continue;
				}
				int Offset = 0;
				string StoreType = null;
				MemberNode[] MemberList = StructList.Get( i ).MemberList;
				
				//搜索分量信息
				for( int j = 0; j < MemberList.Length; ++j ) {
					
					string Type = MemberList[ j ].Type;
					if( Type == null ) {
						string BaseUnitName = StructList.Get( i ).UnitName;
						Type = Parse.GetType( BaseUnitName, MemberList[ j ].TypeIndex );
						if( Type == null ) {
							return;
						}
						//创建子类型的完整类型
						if( VarType.isBase( Type ) ) {
							Type = VarType.CreatVdataType( VarType.VBase, Type );
						}
						MemberList[ j ].Type = Type;
					}
					int Size = VarType.GetSize( Type );
					
					/*
					if( Size % 4 != 0 ) {
						Size = (Size/4+1) * 4;
					}
					*/
					
					//如果引用了未处理的结构体,转向下一个
					if( Type.StartsWith( "struct " ) && Size == -1 ) {
						
						//如果未定义的结构体,报错
						string StructName = Type.Split( ' ' )[ 1 ];
						if( StructList.GetIndex( StructName ) == -1 ) {
							ET.WriteParseError( MemberList[ j ].Location,
								"未定义的结构体类型: " + StructName );
							return;
						}
						HaveUnkownAddr = true;
						continue;
					}
					string SType = VarType.GetStoreTypeonce( Type );
					
					//如果是第一个分量,直接存入元件类型
					if( j == 0 ) {
						StoreType = SType;
					}
					//比较存储类型是否和第一个一致
					else {
						if( !VarType.StoreTypeisSame( StoreType, SType ) ) {
							ET.WriteParseError( MemberList[ j ].Location,
								"结构体中的所有成员必须在同一种元件类型中: " +
								StoreType + " <-- " + SType
							);
						}
					}
					MemberList[ j ].Size = Size;
					MemberList[ j ].Offset = Offset;
					Offset += Size;
				}
				StructList.Get( i ).Size = Offset;
				StructList.Get( i ).StoreType = StoreType;
			}
		}
		while( HaveUnkownAddr );
	}
	
	//------------------------------------------------------------------
	//函数和变量
	
	static int FunctionIndex;
	static bool StartFuncOK;
	
	static string g_Name;
	static string g_RealRefType;
	static string Type;
	
	static void 函数( int Index )
	{
		string UnitName = RecordMacro.UnitName;
		
		//获取成员类型
		bool isFast = false;
		string VisitType = null;
		string RealRefType = null;
		string RefValue = null;
		int FuncPreIndex = int.Parse( ParseNet.NodeSet[ Index ][ 1 ] );
		int MemberTypeIndex = int.Parse( ParseNet.NodeSet[ FuncPreIndex ][ 1 ] );
		Parse.GetMemberType( MemberTypeIndex, ref VisitType, ref RealRefType );
		
		//获取函数类型 general task interface interrupt start
		string InterType = null;
		int InterTypeIndex = int.Parse( ParseNet.NodeSet[ FuncPreIndex ][ 2 ] );
		
		bool Used = false;
		int IntNumber = -1;
		
		if( InterTypeIndex == -1 ) {
			InterType = MemberType.FunctionType.General;
		}
		else if( ParseNet.NodeSet[ InterTypeIndex ][ 0 ] == ParseNet.Node.终结词 ) {
			InterType = WordList.GetWord( InterTypeIndex );
		}
		//中断函数定义
		else if( ParseNet.NodeSet[ InterTypeIndex ][ 0 ] == ParseNet.Node.中断 ) {
			
			//判断是否手工设置了中断函数的空间分配
			int IntSpaceIndex = int.Parse( ParseNet.NodeSet[ InterTypeIndex ][ 2 ] );
			if( IntSpaceIndex != -1 ) {
				string s = WordList.GetWord( IntSpaceIndex );
				if( !int.TryParse( s, out IntNumber ) ) {
					ET.WriteParseError( IntSpaceIndex, "中断函数指定空间尺寸需要为数字: " + s );
				}
			}
			Used = true;
			
			InterType = MemberType.FunctionType.Interrupt;
			int IntIndex = int.Parse( ParseNet.NodeSet[ InterTypeIndex ][ 4 ] );
			string Addr = ConstExpression.GetExpressionValue( IntIndex, UnitName );
			if( Addr != null ) {
				int addr = int.Parse( Addr );
				if( Interrupt.isUsed( addr ) ) {
					ET.WriteParseError( IntIndex, "这个中断地址已经被占用, 一个中断地址不能指向两个以上的函数: " + addr );
				}
				Interrupt.Add( Addr + " " + FunctionIndex + " " + IntIndex );
			}
			int isFastWord = int.Parse( ParseNet.NodeSet[ InterTypeIndex ][ 6 ] );
			if( isFastWord != -1 ) {
				//这里应该进行判断, 是否为快速模式
				isFast = true;
			}
		}
		else if( ParseNet.NodeSet[ InterTypeIndex ][ 0 ] == ParseNet.Node.接口调用 ) {
			InterType = MemberType.FunctionType.Interface;
			int RefIndex = int.Parse( ParseNet.NodeSet[ InterTypeIndex ][ 3 ] );
			RefValue = ConstExpression.GetExpressionValue( RefIndex, UnitName );
			
			if( !n_InterNumberList.InterNumberList.AddDefOK( RefValue ) ) {
				int interi = int.Parse( ParseNet.NodeSet[ InterTypeIndex ][ 1 ] );
				//ET.WriteParseError( interi, "这个接口序号已经被占用, 两个函数不能有相同的接口序号: " + RefValue );
			}
		}
		else {
			//... 不可能到达这里
		}
		
		
		
		//设置函数名为单击定位点
		int NameIndex = int.Parse( ParseNet.NodeSet[ Index ][ 3 ] );
		string FunctionName = UnitName + "." + WordList.GetWord( NameIndex );
		
		if( SST.ModeV && n_Param.Param.UserspaceOn &&
		   	FunctionName != "#" + n_SST.Verilog.F_nop &&
		   	FunctionName != "#" + n_SST.Verilog.F_delay_clk && FunctionName != "#" + n_SST.Verilog.F_delay_us && FunctionName != "#" + n_SST.Verilog.F_delay_ms ) {
			InterType = MemberType.FunctionType.Task;
		}
		
		if( FunctionName.IndexOf( "@" ) != -1 ) {
			Config.NDK_List += FunctionName + ",";
		}
		
		//判断是否可以定义函数
		if( FunctionList.GetDirectIndex( FunctionName ) != -1 ||
		    !VarList.CanDefineStatic( FunctionName ) ||
		    UnitList.isExist( FunctionName )
		  ) {
			//ET.WriteParseError( NameIndex, "函数名非法:已经定义了同名的成员: " + FunctionName );
		}
		//获取函数返回类型
		string ReturnType = null;
		int idx = int.Parse( ParseNet.NodeSet[ Index ][ 2 ] );
		if( idx == -1 || ( ParseNet.NodeSet[ idx ][ 0 ] == "@终结词" && WordList.GetWord( idx ) == VarType.Void ) ) {
			ReturnType = VarType.Void;
		}
		else {
			ReturnType = Parse.GetType( UnitName, idx );
			if( VarType.isRefer( ReturnType ) ) {
				ET.WriteParseError( NameIndex, "函数的返回类型应为基本类型: " + ReturnType );
			}
			if( !VarType.isBase( ReturnType ) ) {
				ReturnType = VarType.AddSysRefer( ReturnType );
			}
		}
		//获取函数形参列表
		int index = int.Parse( ParseNet.NodeSet[ Index ][ 5 ] );
		int index_v = int.Parse( ParseNet.NodeSet[ Index ][ 6 ] );
		
		if( index != -1 && index_v != -1 ) {
			ET.WriteParseError( index_v, "函数形参定义不正确" );
		}
		
		if( InterType == MemberType.FunctionType.Task ) {
			if( SST.ModeV ) {
				string RetPC = "#._cx_fv" + FunctionIndex + "_Sys_RetPC";
				AddV_VarDef( VarType.BaseType.Uint16, RetPC );
			}
		}
		
		
		string VarIndexList = null;
		
		if( index != -1  ) {
			for( int i = 1; i < ParseNet.NodeSet[ index ].Length; i += 2 ) {
				int j = int.Parse( ParseNet.NodeSet[ index ][ i ] );
				
				//获取变量名
				int VarNameIndex = int.Parse( ParseNet.NodeSet[ j ][ 2 ] );
				string Name = WordList.GetWord( VarNameIndex );
				
				//获取变量是否允许修改
				bool isRefer = true;
				if( int.Parse( ParseNet.NodeSet[ j ][ 3 ] ) == -1 ) {
					isRefer = false;
				}
				//判断变量名是否有效
				if( !VarList.CanDefineLocal( UnitName, FunctionIndex, Name ) ) {
					ET.WriteParseError( VarNameIndex, "已经定义了同名的变量: " + Name );
				}
				//获取变量类型
				string Type = Parse.GetType( UnitName, int.Parse( ParseNet.NodeSet[ j ][ 1 ] ) );
				if( Type == null ) {
					return;
				}
				bool isfunctype = false;
				if( Type == VarType.BaseType.Function ) {
					Type = VarType.BaseType.Uint16;
					isfunctype = true;
				}
				if( VarType.isRefer( Type ) ) {
					ET.WriteParseError( VarNameIndex, "形参不能为引用类型: " + Type );
					continue;
				}
				if( !VarType.isBase( Type ) || isRefer ) {
					Type = VarType.AddSysRefer( Type );
				}
				
				if( InterType == MemberType.FunctionType.Task ) {
					Name = "#._cx_fv" + FunctionIndex + "_" + Name;
					
					if( SST.ModeC ) {
						SST.AddVar( Type + " " + SST.GetFullName( Name ) + ";\n" );
					}
					if( SST.ModeV ) {
						AddV_VarDef( Type, Name );
					}
				}
				
				//添加变量
				VarNode v = new VarNode();
				v.SetLocalValue( Name, Type, 0, FunctionIndex, VarNameIndex, ChipIndex );
				v.isFunctionType = isfunctype;
				
				//注意下边也有一段判断
				if( InterType == MemberType.FunctionType.Task ) { //n_Param.Param.UserspaceOn && 
					v.ActiveType = MemberType.StoreType.Static;
				}
				
				VarIndexList += VarList.Add( v ) + " ";
			}
			if( VarIndexList != null ) {
				VarIndexList = VarIndexList.TrimEnd( ' ' );
			}
		}
		else {
			
			//这里判断是否用户空间, 是的话报错, 必须要加上 void
			if( Config.WarningAsError && n_Param.Param.UserspaceOn && index_v == -1 ) {
				ET.WriteParseError( NameIndex, "无参数的函数的定义格式应为: " + FunctionName + "(void) " );
			}
		}
		//判断是否中断函数的返回类型
		if( ( InterType == MemberType.FunctionType.Interrupt || InterType == MemberType.FunctionType.Task ) ) {
			if( ReturnType != VarType.Void ) {
				ET.WriteParseError( NameIndex, "中断函数和任务函数的返回类型必须是 void 类型: " + ReturnType );
			}
			if( VarIndexList != null ) {
				//ET.WriteParseError( NameIndex, "中断函数和任务函数不能有形参" );
			}
		}
		int UnitIndex = UnitList.GetDirectIndex( UnitName );
		string FuncName = FunctionName.Remove( 0, FunctionName.LastIndexOf( "." ) + 1 );
		FunctionNode node = new FunctionNode(
			VisitType, RealRefType, InterType, ReturnType, FunctionName, VarIndexList, ChipIndex, NameIndex, RefValue );
		
		//需要和上边的对应
		if( InterType == MemberType.FunctionType.Task ) { //n_Param.Param.UserspaceOn && 
			node.UserSpace = true;
		}
		
		if( IntNumber != -1 ) {
			node.InterruptSpaceNumber = IntNumber;
		}
		node.isFast = isFast;
		if( Used ) {
			node.isUsed = true;
		}
		
		if( FunctionList.GetDirectIndex( node.FunctionName ) != -1 ) {
			ET.WriteParseError( NameIndex, "已经定义了同名的函数: " + FunctionName );
		}
		else {
			FunctionList.Add( node );
		}
		
		//处理链接类型
		int LinkIndex = int.Parse( ParseNet.NodeSet[ Index ][ 11 ] );
		if( LinkIndex != -1 ) {
			//提取第二个成员信息
			int SecondIndex = int.Parse( ParseNet.NodeSet[ LinkIndex ][ 2 ] );
			int ErrorIndex2 = 0;
			string SecondName = null;
			Parse.GetMemberNames( UnitName, SecondIndex, ref SecondName, ref ErrorIndex2 );
			if( RealRefType != MemberType.RealRefType.link ) {
				ET.WriteParseError( int.Parse( ParseNet.NodeSet[ Index ][ 4 ] ), "连接器的左端必须为引用类型成员:" + UnitName );
			}
			node.TargetMemberName = SecondName;
		}
	}
	
	static void 静态变量( int Index )
	{
		string UnitName = RecordMacro.UnitName;
		
		//获取成员类型
		string VisitType = null;
		string RealRefType = null;
		int MemberTypeIndex = int.Parse( ParseNet.NodeSet[ Index ][ 1 ] );
		Parse.GetMemberType( MemberTypeIndex, ref VisitType, ref RealRefType );
		
		//获取变量类型
		int VarTypeIndex = int.Parse( ParseNet.NodeSet[ Index ][ 2 ] );
		Type = Parse.GetType( UnitName, VarTypeIndex );
		if( Type == null ) {
			return;
		}
		VarNode v = new VarNode();
		//获取变量名
		int NameIndex = int.Parse( ParseNet.NodeSet[ Index ][ 3 ] );
		string oName = WordList.GetWord( NameIndex );
		string Name = UnitName + "." + oName;
		
		int MacroIndex = int.Parse( ParseNet.NodeSet[ Index ][ 4 ] );
		if( MacroIndex != -1 ) {
			Name += Parse.GetMacroMes( MacroIndex );
		}
		
		if( Name.IndexOf( "@" ) != -1 ) {
			Config.NDK_List += Name + ",";
		}
		
		if( oName.StartsWith( "@" ) ) {
			SST.TempClose = true;
		}
		
		if( !VarList.CanDefineStatic( Name ) ||
		    FunctionList.GetIndex( Name ) != -1 ||
		    UnitList.isExist( Name ) ) {
			ET.WriteParseError( NameIndex, "变量名非法:已经定义了同名的成员:" + Name );
			return;
		}
		
		g_Name = Name;
		g_RealRefType = RealRefType;
		
		//获取变量地址
		int AddrIndex = int.Parse( ParseNet.NodeSet[ Index ][ 5 ] );
		try{
			string VarasdVdataName = VarType.GetStoreType( Type );
		}catch{
			ET.WriteParseError( NameIndex, "<单个变量> 未知的存储类型 - 类型:" + Type + "  名称:" + Name );
			return;
		}
		
		string VarVdataName = VarType.GetStoreType( Type );
		int VdataIndex = VdataList.GetIndex( VarVdataName );
		if( VdataIndex == -1 ) {
			ET.WriteParseError( NameIndex, "函数和变量信息提取时发现未定义的虚拟数据类型:" + VarVdataName );
			return;
		}
		string Address = null;
		bool isOut = false;
		bool isFunctionType = false;
		
		//有地址限定项
		if( AddrIndex != -1 ) {
			string VdataAddrType = VdataList.Get( VdataIndex ).AddrType;
			Address = 获取地址( AddrIndex, VdataAddrType );
			isOut = true;
		}
		//没有地址限定
		else {
			isOut = false;
			if( !VdataList.Get( VdataIndex ).AutoAllot && VdataList.Get( VdataIndex ).AddrType != null ) {
				ET.WriteParseError( NameIndex, "此变量应指定地址: " + Name );
				return;
			}
			if( RealRefType != MemberType.RealRefType.link ) {
				
				if( VarType.isBase( Type ) ) {
					
					if( Type == VarType.BaseType.Function ) {
						Type = VarType.BaseType.Uint16;
						isFunctionType = true;
						SST.AddVar( "void (*" + SST.GetFullName( Name ) + ")(void);\n" );
					}
					else {
						if( SST.ModeC ) {
							SST.AddVar( SST.PRE_t + Type + " " + SST.GetFullName( Name ) + ";\n" );
						}
						else {
							//...
						}
					}
				}
				else if( VarType.isArray( Type ) ) {
					int an = VarType.GetArrayNumber( Type );
					string subType = VarType.GetArrayMemberType( Type );
					if( VarType.StoreTypeisVbase( subType ) ) {
						subType = VarType.GetTypeFromVdata( subType );
					}
					if( subType == VarType.BaseType.Function ) {
						Type = Type.Remove( Type.Length - VarType.BaseType.Function.Length ) + VarType.BaseType.Uint16;
						isFunctionType = true;
						
						if( an == 0 ) {
							an = 1;
						}
						SST.AddVar( "void (*" + SST.GetFullName( Name ) + "[" + an + "])(void);\n" );
					}
					else {
						if( an == 0 ) {
							an = 1;
						}
						if( SST.ModeC ) {
							SST.AddVar( SST.PRE_t + subType + " " + SST.GetFullName( Name ) + "[" + an + "];\n" );
						}
						if( n_Param.Param.isVon && SST.ModeV ) {
							int size = VarType.GetBitSize( subType );
							SST.AddVar( "reg [" + (size-1) + ":0] " + SST.GetFullName( Name ) + "[" + (an-1) + ":0];\n" );
						}
					}
				}
				else {
					//...
				}
			}
		}
		if( n_Param.Param.isVon && SST.ModeV && RealRefType == MemberType.RealRefType.Real ) {
			DealVerilogVar( v, Type, Address, Name );
		}
		
		if( Address != null && Address.StartsWith( "123456789" ) ) {
			Address = null;
		}
		
		//处理地址
		if( Address != null ) {
			if( Address.StartsWith( "&link " ) ) {
				if( RealRefType != MemberType.RealRefType.link ) {
					ET.WriteParseError( NameIndex, "只有引用类型的变量才能指定链接目标" );
					return;
				}
				string TargetMemberName = Address.Remove( 0, 6 );
				v.TargetMemberName = TargetMemberName;
			}
			else if( Address.StartsWith( "&unitoffset " ) ) {
				
				if( RealRefType != MemberType.RealRefType.link ) {
					ET.WriteParseError( NameIndex, "只有引用类型的变量才能指定链接目标" );
					return;
				}
				//这一步体现编译器对 link 和 相对偏移引用 这两种形式的定义形式的统一
				RealRefType = MemberType.RealRefType.Real;
			}
			else if( Address.StartsWith( "&retarget " ) ) {
				
				if( RealRefType != MemberType.RealRefType.Real ) {
					ET.WriteParseError( NameIndex, "引用类型的变量不能指定其地址到目标变量" );
					return;
				}
				//这一步体现编译器对 link 和 相对偏移引用 这两种形式的定义形式的统一
				RealRefType = MemberType.RealRefType.Real;
			}
			else if( Address.StartsWith( "@code " ) ) {
				string cAddress = Address.Split( ' ' )[ 1 ];
				string[] s = cAddress.Split( ',' );
				string basetype = Type.Remove( 0, Type.LastIndexOf( ' ' ) + 1 );
				
				//如果省略定义长度则根据实际长度自动补上定义长度
				if( VarType.GetSize( Type ) == -1 ) {
					Type = VarType.GetArrayMemberType( Type );
					int Size = VarType.GetArrayNumber( Type );
					if( Size == -1 ) {
						ET.WriteParseError( NameIndex, "常量数组只能第一项省略大小: " + Type );
						return;
					}
					if( s.Length % Size != 0 ) {
						ET.WriteParseError( NameIndex, "数组的长度不是成员的整数倍: " + s.Length + " " + Size );
						return;
					}
					Size = s.Length / Size;
					Type = VarType.CreatArray( Size, Type );
					v.Type = Type;
				}
				else {
					int DefineLength = VarType.GetSize( Type ) / VarType.GetSize( basetype );
					//int DefineLength = VarType.GetSize( Type );
					
					//判断数组长度是否溢出 (超出定义长度)
					if( DefineLength != s.Length ) {
						ET.WriteParseError( NameIndex, "数组的定义长度不等于实际长度: " + DefineLength + " " + s.Length );
					}
				}
			}
		}
		v.SetStaticValue( Name, Type, VisitType, RealRefType, isOut, NameIndex, ChipIndex, NameIndex );
		v.Address = Address;
		v.isFunctionType = isFunctionType;
		v.isUserSpace = n_Param.Param.UserspaceOn;
		v.isVerilogSpace = n_Param.Param.isVon;
		VarList.Add( v );
	}
	
	static string 获取地址( int Index, string AddrType )
	{
		string UnitName = RecordMacro.UnitName;
		
		if( g_RealRefType != MemberType.RealRefType.link ) {
			
			int lres = g_Name.LastIndexOf( ".RES." );
			if( lres != -1 ) {
				SST.AddVar( "#define " + SST.GetFullName( g_Name ) + " " + g_Name.Remove( 0, lres + 5 ) + "\n" );
			}
		}
		
		//如果是静态变量引用,返回其值
		int index = int.Parse( ParseNet.NodeSet[ Index ][ 2 ] );
		if( ParseNet.NodeSet[ index ][ 0 ] == ParseNet.Node.静态变量引用 ) {
			return 静态变量引用( index );
		}
		//如果是直接地址常量,返回其值
		if( ParseNet.NodeSet[ index ][ 0 ] == ParseNet.Node.立即数地址 ) {
			int i = int.Parse( ParseNet.NodeSet[ index ][ 2 ] );
			
			string Address = ConstExpression.GetExpressionValue( i, UnitName );
			
			if( Address == null ) {
				ET.WriteParseError( int.Parse( ParseNet.NodeSet[ index ][ 1 ] ), "设置变量地址时遇到无法计算的表达式" );
			}
			return Address;
			
//			string Address = WordList.GetWord( i );
//			string Type = null;
//			string DecAddr = ConstString.GetValue( ref Type, Address, i );
////			if( AddrType != Type && !Operation.CanSwitch( Type, AddrType ) ) {
////				ET.WriteParseError( i, "地址常量超出对应元件的地址范围: " + AddrType + " <- " + Type );
////			}
//			return DecAddr;
		}
		//如果是已有变量地址,返回变量名
		if( ParseNet.NodeSet[ index ][ 0 ] == ParseNet.Node.已有变量地址 ) {
			int i = int.Parse( ParseNet.NodeSet[ index ][ 2 ] );
			
			int ErrorIndex = 0;
			string SecondName = null;
			Parse.GetMemberNames( UnitName, i, ref SecondName, ref ErrorIndex );
			
			return "&retarget " + SecondName;
		}
		//如果是数组,返回数组本身,用逗号 "," 分隔
		if( ParseNet.NodeSet[ index ][ 0 ] == ParseNet.Node.数组 ) {
			int ii = int.Parse( ParseNet.NodeSet[ index ][ 2 ] );
			return 数组元素( ii );
		}
		//如果是字符串,返回各个字符的ascii代码,用 "," 分隔,结尾加 \0
		string Strings = WordList.GetWord( index );
		if( !Strings.StartsWith( "\"" ) && !Strings.EndsWith( "\"" ) ) {
			
			//这里可以加上支持code常量的定义
			ET.WriteParseError( index, "缺少字符串引号" );
			return "0";
		}
		int Length = 0;
		int CodeType = 0;
		string Result = "@code ";
		Result += StringCoder.GetCode( Strings, ref Length, ref CodeType, index );
		return Result;
	}
	
	static string 数组元素( int Index )
	{
		string UnitName = RecordMacro.UnitName;
		
		int an = VarType.GetArrayNumber( Type );
		string subType = VarType.GetArrayMemberType( Type );
		subType = VarType.GetTypeFromVdata( subType );
		
		//常量数组前缀
		//arduinoAVR: const PROGMEM
		//其他平台: const
		SST.AddVar( SST.CodeArrayPre + " " + SST.PRE_t + subType + " " + SST.GetFullName( g_Name ) + "[] = {" );
		
		
		string Result = "@code ";
		for( int i = 1; i < ParseNet.NodeSet[ Index ].Length; i += 2 ) {
			int MemberIndex = int.Parse( ParseNet.NodeSet[ Index ][ i ] );
			string Value = ConstExpression.GetExpressionValue( MemberIndex, UnitName );
			Result += Value + ",";
			
			if( i == 1 ) {
				SST.AddVar( " " + Value );
			}
			else {
				SST.AddVar( ", " + Value );
			}
			if( i % 16 == 0 ) {
				SST.AddVar( "\n" );
			}
		}
		
		SST.AddVar( " };\n" );
		
		return Result.TrimEnd( ',' );
	}
	
	static string 静态变量引用( int Index )
	{
		string UnitName = RecordMacro.UnitName;
		
		int MemberIndex = int.Parse( ParseNet.NodeSet[ Index ][ 1 ] );
		int ErrorIndex = -1;
		string Name = null;
		Parse.GetMemberNames( UnitName, MemberIndex, ref Name, ref ErrorIndex );
		int OffsetIndex = int.Parse( ParseNet.NodeSet[ Index ][ 2 ] );
		if( OffsetIndex == -1 ) {
			return "&link " + Name;
		}
		int OffIndex = int.Parse( ParseNet.NodeSet[ OffsetIndex ][ 2 ] );
		string Offset = WordList.GetWord( OffIndex );
		string oType = null;
		Offset = ConstString.GetValue( ref oType, Offset, OffIndex );
		
		//获取后缀限定类型
		string ReturnType = VarType.BaseType.Bit;
		int LastAddIndex = int.Parse( ParseNet.NodeSet[ OffsetIndex ][ 3 ] );
		if( LastAddIndex != -1 ) {
			ReturnType = WordList.GetWord( int.Parse( ParseNet.NodeSet[ LastAddIndex ][ 2 ] ) );
		}
		if( Type != ReturnType ) {
			ET.WriteParseError( ErrorIndex, "定义变量的类型和目标变量的类型不同: " + Type + "," + ReturnType );
		}
		return "&unitoffset " + Name + " " + Offset;
	}
	
	static void DealVerilogVar( VarNode v, string Type, string Address, string Name )
	{
		if( VarType.isBase( Type ) ) {
			int size = VarType.GetBitSize( Type );
			
			//1234567895: 除法IP变量
			//1234567894: 多核访问变量
			
			//1234567893: 读取接口
			//1234567892: 三态类型
			//1234567891: 输出类型
			//1234567890: 输入类型
			
			string rtype = "reg";
			if( Address == "1234567893" ) {
				rtype = "wire";
			}
			if( Address == "1234567890" || Address == "1234567891" || Address == "1234567892" ) {
				
				//输入类型
				if( Address == "1234567890" ) {
					SST.AddVar( "assign " + SST.GetFullName( Name ) + " = O_" + SST.GetFullName( Name ) + ";\n" );
					SST.AddVInterVar( "\tinput wire [" + (size-1) + ":0] O_" + SST.GetFullName( Name ) );
					SST.AddVar( "wire [" + (size-1) + ":0] " + SST.GetFullName( Name ) + ";\n" );
				}
				//输出类型
				if( Address == "1234567891" ) {
					SST.AddVar( "assign O_" + SST.GetFullName( Name ) + " = " + SST.GetFullName( Name ) + ";\n" );
					SST.AddVInterVar( "\toutput wire [" + (size-1) + ":0] O_" + SST.GetFullName( Name ) );
					SST.AddVar( "reg [" + (size-1) + ":0] " + SST.GetFullName( Name ) + ";\n" );
				}
				//三态类型
				if( Address == "1234567892" ) {
					for( int i = 0; i < size; ++i ) {
						SST.AddVar( "assign O_" + SST.GetFullName( Name ) + "[" + i + "] = " + SST.GetFullName( Name ) + "_DIR[" + i + "] ? " + SST.GetFullName( Name ) + "[" + i + "] : 1'bz;\n" );
					}
					SST.AddVInterVar( "\tinout wire [" + (size-1) + ":0] O_" + SST.GetFullName( Name ) );
					SST.AddVar( "reg [" + (size-1) + ":0] " + SST.GetFullName( Name ) + ";\n" );
					//SST.AddVar( "wire [" + (size-1) + ":0] " + SST.GetFullName( Name ) + "_IN;\n" );
					SST.AddVar( "assign " + SST.GetFullName( Name ) + "_IN = O_" + SST.GetFullName( Name ) + ";\n" );
				}
			}
			else if( VarType.BaseType.isSint( Type ) ) {
				SST.AddVar( rtype + " signed [" + (size-1) + ":0] " + SST.GetFullName( Name ) + ";\n" );
			}
			else {
				SST.AddVar( rtype + " [" + (size-1) + ":0] " + SST.GetFullName( Name ) + ";\n" );
			}
			if( Address == "1234567894" ) {
				v.isVerilogMult = true;
				v.VeriType = 0;
			}
			if( Address == "1234567895" ) {
				v.isVerilogMult = true;
				v.VeriType = 1;
			}
		}
	}
	
	public static void AddV_VarDef( string Type, string Name )
	{
		string rtype = "reg";
		int size = VarType.GetBitSize( Type );
		if( VarType.BaseType.isSint( Type ) ) {
			SST.AddVar( rtype + " signed [" + (size-1) + ":0] " + SST.GetFullName( Name ) + ";\n" );
		}
		else {
			SST.AddVar( rtype + " [" + (size-1) + ":0] " + SST.GetFullName( Name ) + ";\n" );
		}
	}
	
	static void chisel函数( int Index )
	{
		string UnitName = RecordMacro.UnitName;
		
		//获取成员类型
		bool isFast = false;
		string VisitType = MemberType.VisitType.Public;
		string RealRefType = MemberType.RealRefType.Real;
		string RefValue = null;
		
		//获取函数类型 general task interface interrupt start
		string InterType = MemberType.FunctionType.General;
		
		bool Used = false;
		int IntNumber = -1;
		
		//设置函数名为单击定位点
		int NameIndex = int.Parse( ParseNet.NodeSet[ Index ][ 2 ] );
		string FunctionName = UnitName + "." + WordList.GetWord( NameIndex );
		
		if( SST.ModeV && n_Param.Param.UserspaceOn &&
		   	FunctionName != "#" + n_SST.Verilog.F_nop &&
		   	FunctionName != "#" + n_SST.Verilog.F_delay_clk && FunctionName != "#" + n_SST.Verilog.F_delay_us && FunctionName != "#" + n_SST.Verilog.F_delay_ms ) {
			InterType = MemberType.FunctionType.Task;
		}
		
		//判断是否可以定义函数
		if( FunctionList.GetDirectIndex( FunctionName ) != -1 ||
		    !VarList.CanDefineStatic( FunctionName ) ||
		    UnitList.isExist( FunctionName )
		  ) {
			//ET.WriteParseError( NameIndex, "函数名非法:已经定义了同名的成员: " + FunctionName );
		}
		//获取函数返回类型
		string ReturnType = null;
		int idx = int.Parse( ParseNet.NodeSet[ Index ][ 7 ] );
		if( idx == -1 || ( ParseNet.NodeSet[ idx ][ 0 ] == "@终结词" && WordList.GetWord( idx ) == VarType.Void ) ) {
			ReturnType = VarType.Void;
		}
		else {
			
			
			ReturnType = VarType.Void;
			/*
			ReturnType = Parse.GetType( UnitName, idx );
			if( VarType.isRefer( ReturnType ) ) {
				ET.WriteParseError( NameIndex, "函数的返回类型应为基本类型: " + ReturnType );
			}
			if( !VarType.isBase( ReturnType ) ) {
				ReturnType = VarType.AddSysRefer( ReturnType );
			}
			*/
			
			
		}
		
		if( InterType == MemberType.FunctionType.Task ) {
			if( SST.ModeV ) {
				string RetPC = "#._cx_fv" + FunctionIndex + "_Sys_RetPC";
				AddV_VarDef( VarType.BaseType.Uint16, RetPC );
			}
		}
		
		string VarIndexList = null;
		
		//判断是否中断函数的返回类型
		if( ( InterType == MemberType.FunctionType.Interrupt || InterType == MemberType.FunctionType.Task ) ) {
			if( ReturnType != VarType.Void ) {
				ET.WriteParseError( NameIndex, "中断函数和任务函数的返回类型必须是 void 类型: " + ReturnType );
			}
			if( VarIndexList != null ) {
				//ET.WriteParseError( NameIndex, "中断函数和任务函数不能有形参" );
			}
		}
		int UnitIndex = UnitList.GetDirectIndex( UnitName );
		string FuncName = FunctionName.Remove( 0, FunctionName.LastIndexOf( "." ) + 1 );
		FunctionNode node = new FunctionNode(
			VisitType, RealRefType, InterType, ReturnType, FunctionName, VarIndexList, ChipIndex, NameIndex, RefValue );
		
		//需要和上边的对应
		if( InterType == MemberType.FunctionType.Task ) { //n_Param.Param.UserspaceOn && 
			node.UserSpace = true;
		}
		
		if( IntNumber != -1 ) {
			node.InterruptSpaceNumber = IntNumber;
		}
		node.isFast = isFast;
		if( Used ) {
			node.isUsed = true;
		}
		
		if( FunctionList.GetDirectIndex( node.FunctionName ) != -1 ) {
			ET.WriteParseError( NameIndex, "已经定义了同名的函数: " + FunctionName );
		}
		else {
			FunctionList.Add( node );
		}
	}
	
	static void chisel静态变量( int Index )
	{
		string UnitName = RecordMacro.UnitName;
		
		//获取成员类型
		string VisitType = MemberType.VisitType.Public;
		string RealRefType = MemberType.RealRefType.Real;
		
		//获取变量类型
		//int VarTypeIndex = int.Parse( ParseNet.NodeSet[ Index ][ 2 ] );
		//Type = Parse.GetType( UnitName, VarTypeIndex );
		
		int TypeIndex = int.Parse( ParseNet.NodeSet[ Index ][ 6 ] );
		string cType = WordList.GetWord( TypeIndex );
		int WIndex = int.Parse( ParseNet.NodeSet[ Index ][ 8 ] );
		string cWidth = WordList.GetWord( WIndex );
		if( !cWidth.EndsWith( ".W" ) ) {
			ET.WriteParseError( WIndex, "变量宽度非法:应以.W结尾:" + cWidth );
			cWidth = "32.W";
		}
		cWidth = cWidth.Remove( cWidth.Length - 2 );
		if( cWidth != "8" && cWidth != "16" && cWidth != "32" ) {
			ET.WriteParseError( WIndex, "变量宽度目前支持 8 16 32:" + cWidth );
			cWidth = "32";
		}
		if( cType == "UInt" ) {
			Type = VarType.BaseType.Uint + cWidth;
		}
		else if( cType == "SInt" ) {
			Type = VarType.BaseType.Sint + cWidth;
		}
		else {
			ET.WriteParseError( TypeIndex, "变量类型未知:" + cType );
			Type =  VarType.BaseType.Uint32;
		}
		if( Type == null ) {
			return;
		}
		
		VarNode v = new VarNode();
		//获取变量名
		int NameIndex = int.Parse( ParseNet.NodeSet[ Index ][ 2 ] );
		string oName = WordList.GetWord( NameIndex );
		string Name = UnitName + "." + oName;
		
		if( !VarList.CanDefineStatic( Name ) ||
		    FunctionList.GetIndex( Name ) != -1 ||
		    UnitList.isExist( Name ) ) {
			ET.WriteParseError( NameIndex, "变量名非法:已经定义了同名的成员:" + Name );
			return;
		}
		
		string Address = null;
		bool isOut = false;
		bool isFunctionType = false;
		
		isOut = false;
		
		if( VarType.isBase( Type ) ) {
			
			if( Type == VarType.BaseType.Function ) {
				Type = VarType.BaseType.Uint16;
				isFunctionType = true;
				SST.AddVar( "void (*" + SST.GetFullName( Name ) + ")(void);\n" );
			}
			else {
				if( SST.ModeC ) {
					SST.AddVar( SST.PRE_t + Type + " " + SST.GetFullName( Name ) + ";\n" );
				}
				else {
					//...
				}
			}
		}
		else if( VarType.isArray( Type ) ) {
			int an = VarType.GetArrayNumber( Type );
			string subType = VarType.GetArrayMemberType( Type );
			if( VarType.StoreTypeisVbase( subType ) ) {
				subType = VarType.GetTypeFromVdata( subType );
			}
			if( subType == VarType.BaseType.Function ) {
				Type = Type.Remove( Type.Length - VarType.BaseType.Function.Length ) + VarType.BaseType.Uint16;
				isFunctionType = true;
				
				if( an == 0 ) {
					an = 1;
				}
				SST.AddVar( "void (*" + SST.GetFullName( Name ) + "[" + an + "])(void);\n" );
			}
			else {
				if( an == 0 ) {
					an = 1;
				}
				if( SST.ModeC ) {
					SST.AddVar( SST.PRE_t + subType + " " + SST.GetFullName( Name ) + "[" + an + "];\n" );
				}
				if( n_Param.Param.isVon && SST.ModeV ) {
					int size = VarType.GetBitSize( subType );
					SST.AddVar( "reg [" + (size-1) + ":0] " + SST.GetFullName( Name ) + "[" + (an-1) + ":0];\n" );
				}
			}
		}
		else {
			//...
		}
		
		
		if( n_Param.Param.isVon && SST.ModeV && RealRefType == MemberType.RealRefType.Real ) {
			
			DealVerilogVar( v, Type, Address, Name );
		}
		
		if( Address != null && Address.StartsWith( "123456789" ) ) {
			Address = null;
		}
		
		v.SetStaticValue( Name, Type, VisitType, RealRefType, isOut, NameIndex, ChipIndex, NameIndex );
		v.Address = Address;
		v.isFunctionType = isFunctionType;
		v.isUserSpace = n_Param.Param.UserspaceOn;
		v.isVerilogSpace = n_Param.Param.isVon;
		VarList.Add( v );
	}
	
	static void FuncAndVarLink连接器( int Index )
	{
		string UnitName = RecordMacro.UnitName;
		
		//提取当前连接器的权限
		bool Super = false;
		int SuperIndex = int.Parse( ParseNet.NodeSet[ Index ][ 1 ] );
		if( SuperIndex != -1 ) {
			Super = true;
		}
		//提取第一个成员信息
		int FirstIndex = int.Parse( ParseNet.NodeSet[ Index ][ 2 ] );
		int ErrorIndex1 = 0;
		string FirstName = null;
		Parse.GetMemberNames( UnitName, FirstIndex, ref FirstName, ref ErrorIndex1 );
		
		//提取第二个成员信息
		int SecondIndex = int.Parse( ParseNet.NodeSet[ Index ][ 4 ] );
		int ErrorIndex2 = 0;
		string SecondName = null;
		Parse.GetMemberNames( UnitName, SecondIndex, ref SecondName, ref ErrorIndex2 );
		
		int MemberIndex;
		
		//判断是否已经处理过
		if( ParseNet.NodeSet[ Index ].Length >= 3 && ParseNet.NodeSet[ Index ][ 3 ] == "-1" ) {
			return;
		}
		//判断成员是否为元件类型
		MemberIndex = UnitList.GetDirectIndex( FirstName );
		if( MemberIndex != -1 ) {
			return;
		}
		//判断成员是否为虚拟数据类型
		MemberIndex = VdataList.GetDirectIndex( FirstName );
		if( MemberIndex != -1 ) {
			return;
		}
		//判断成员是否为结构体类型
		MemberIndex = StructList.GetDirectIndex( FirstName );
		if( MemberIndex != -1 ) {
			return;
		}
		//判断成员是否为函数类型
		MemberIndex = FunctionList.GetDirectIndex( FirstName );
		
		/*
		//2016.12.22 新增引用置换, 当目标link所在的Lunit为link类型时, 先解析Lunit的实际元件
		if( MemberIndex == -1 ) {
			string LastName = FirstName.Remove( 0, FirstName.LastIndexOf( '.' ) + 1 );
			string FName = FirstName.Remove( FirstName.LastIndexOf( '.' ) );
			int i = UnitList.GetDirectIndex( FName );
			if( i != -1 ) {
				FirstName = UnitList.Get( i ).TargetMemberName + "." + LastName;
				MemberIndex = FunctionList.GetDirectIndex( FirstName );
			}
		}
		*/
		
		if( MemberIndex != -1 ) {
			if( FunctionList.Get( MemberIndex ).RealRefType != MemberType.RealRefType.link ) {
				if( Super ) {
					FunctionList.Get( MemberIndex ).RealRefType = MemberType.RealRefType.link;
				}
				else {
					ET.WriteParseError( ErrorIndex1, "连接器的左端必须为引用类型元件:" + FirstName );
					return;
				}
			}
			//如果是导出代码模式且目标为@函数, 则临时忽略, 解决:
			//Sim.onInit = @onInit;
			//Sim.onDraw = @onDraw;
			if( n_Compiler.Compiler.isExportMode && SecondName.IndexOf( "@" ) != -1 ) {
				return;
			}
			
			FunctionList.Get( MemberIndex ).TargetMemberName = SecondName;
			return;
		}
		//判断成员是否为静态变量类型
		MemberIndex = VarList.GetDirectStaticIndex( FirstName );
		
		//2016.12.22 新增引用置换, 当目标link所在的Lunit为link类型时, 先解析Lunit的实际元件
		if( MemberIndex == -1 ) {
			string LastName = FirstName.Remove( 0, FirstName.LastIndexOf( '.' ) + 1 );
			string FName = FirstName.Remove( FirstName.LastIndexOf( '.' ) );
			int i = UnitList.GetDirectIndex( FName );
			if( i != -1 ) {
				FirstName = UnitList.Get( i ).TargetMemberName + "." + LastName;
				MemberIndex = VarList.GetDirectStaticIndex( FirstName );
			}
		}
		if( MemberIndex != -1 ) {
			if( VarList.Get( MemberIndex ).RealRefType != MemberType.RealRefType.link ) {
				if( Super ) {
					VarList.Get( MemberIndex ).RealRefType = MemberType.RealRefType.link;
				}
				else {
					ET.WriteParseError( ErrorIndex1, "连接器的左端必须为引用类型属性:" + FirstName );
					return;
				}
			}
			VarList.Get( MemberIndex ).TargetMemberName = SecondName;
			return;
		}
		//判断成员是否为常量类型
		MemberIndex = VarList.GetStaticIndex( FirstName );
		if( MemberIndex != -1 ) {
			if( VarList.Get( MemberIndex ).isConst ) {
				return;
			}
		}
		//判断是否系统配置
		if( FirstName == VarType.Root + "." + VarType.Start ) {
			int FuncIndex = FunctionList.GetIndex( SecondName );
			if( FuncIndex == -1 ) {
				ET.WriteParseError( 0, "起始函数未定义: " + SecondName );
			}
			else {
				Config.AddStartFunction( SecondName );
				StartFuncOK = true;
			}
			return;
		}
		ET.WriteParseError( ErrorIndex1, "不支持的链接成员类型: " + FirstName );
	}
	
	public static void FuncAndVarLinkEnd()
	{
		if( !StartFuncOK ) {
			Config.AddStartFunction( VarType.Root + ".main" );
		}
	}
}
}


