﻿
//预定义标识符类
namespace n_spce061aLabel
{
using System;
using System.Text;

public static class spce061aLabel
{
	//初始化网络
	public static void Init()
	{
		NodeSet = new string[ 10000 ][];
	}
	
	//清除网络
	public static void Clear()
	{
		Length = 0;
	}
	
	//显示网络
	public static string Show()
	{
		string Result = "";
		for( int Index = 0; Index < Length; ++Index ) {
			Result += Index + ":\t" + string.Join( " ", NodeSet[ Index ] ) + "\n";
		}
		return Result.Trim( '\n' );
	}
	
	//添加节点
	public static void AddNode( string Name, string Address )
	{
		NodeSet[ Length ] = new string[ 2 ];
		NodeSet[ Length ][ 0 ] = Name;
		NodeSet[ Length ][ 1 ] = Address;
		++Length;
	}
	
	//替换标签
	public static string ReplaceLabel( string Source )
	{
		for( int i = 0; i < Length; ++i ) {
			Source = Source.Replace( "_" + NodeSet[ i ][ 0 ] + "_", NodeSet[ i ][ 1 ] );
		}
		return Source.Trim( '\n' );
	}
	
	public static string[][] NodeSet;	//网络集
	static int Length;			//网络长度
}
}
