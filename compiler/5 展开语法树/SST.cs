﻿
using System;
using n_FunctionList;
using n_VarNode;

namespace n_SST
{
public static class SST
{
	public static bool isError;
	
	//----------------------------------
	//源码变换器配置
	
	//C模式 or verilog模式
	public static bool ModeC = true;
	public static bool ModeV = false;
	
	public static bool isChisel = true;
	
	//代码转换总开关
	public static bool EN = true;
	
	//临时禁用代码添加
	public static bool TempClose = false;
	
	public static bool Mode_C90 = false;
	public static string PRE_g = "cx";
	public static string PRE_l = "tmp_";
	public static string PRE_t = "t_";
	
	//自动生成的配置
	public static int CodeArrayByFunc;
	public static string CodeArrayPre;
	
	//----------------------------------
	
	static string Code;
	static string Head;
	static string Var; //全局变量
	static string FuncVar; //局部变量, 仅用于C90
	static int TabNum = 0;
	
	public static string ModPlatform;
	static string ModCode;
	
	static string ModCode_Default;
	static string ModCode_Arduino8_MEGA328;
	static string ModCode_Arduino8;
	static string ModCode_Arduino32;
	static string ModCode_C51;
	
	static string ModCode_None;
	
	public static string UserCL;
	
	public const string FuncPre = "<<func_pre>>";
	
	static string V_InterVar;
	public static string V_UsedPinIndex;
	public static string V_DynModuleList;
	
	const string V_Rep_SYS_RAM = "<<SYS_RAM>>";
	public static string V_SYS_RAM;
	const string V_Rep_SYS_ROM = "<<SYS_ROM>>";
	public static string V_SYS_ROM;
	const string V_Rep_SYS_BPURAM = "<<SYS_BPURAM>>";
	public static string V_SYS_BPURAM;
	
	public static int FREQ = 25000000; //48000000; 安路为48M
	
	public const string V_ClockName = "Clk"; //cx_PLL_CLK
	public const string V_ResetName = "Rst_n"; //cx_PLL_LOCK
	
	//初始化
	public static void Init()
	{
		ModCode_Default = n_OS.VIO.OpenTextFileUTF8( n_OS.OS.SystemRoot + "扩展包" + n_OS.OS.PATH_S + "Default.txt" );
		ModCode_Arduino8_MEGA328 = n_OS.VIO.OpenTextFileUTF8( n_OS.OS.SystemRoot + "扩展包" + n_OS.OS.PATH_S + "Arduino_8_MEGA328.txt" );
		ModCode_Arduino8 = n_OS.VIO.OpenTextFileUTF8( n_OS.OS.SystemRoot + "扩展包" + n_OS.OS.PATH_S + "Arduino_8.txt" );
		ModCode_Arduino32 = n_OS.VIO.OpenTextFileUTF8( n_OS.OS.SystemRoot + "扩展包" + n_OS.OS.PATH_S + "Arduino_32.txt" );
		ModCode_C51 = n_OS.VIO.OpenTextFileUTF8( n_OS.OS.SystemRoot + "扩展包" + n_OS.OS.PATH_S + "C51.txt" );
		
		//ModCode_None = n_OS.VIO.OpenTextFileUTF8( n_OS.OS.SystemRoot + "扩展包\\V_Anlogic.txt" );
		ModCode_None = n_OS.VIO.OpenTextFileUTF8( n_OS.OS.SystemRoot + "扩展包" + n_OS.OS.PATH_S + "V_isilicontech.txt" );
		
		ModCode = ModCode_None;
		
		Clear();
		
		Verilog.Init();
	}
	
	//图形转换时清零代码
	public static void GUI_Clear()
	{
		UserCL = "";
	}
	
	//清零代码
	public static void Clear()
	{
		Code = "";
		Head = "";
		Var = "";
		FuncVar = "";
		
		V_InterVar = "";
		Verilog.Clear();
		
		isError = false;
	}
	
	//获取代码
	public static string GetCode()
	{
		if( ModeC ) {
			string r = ModCode.Replace( "#.", PRE_g + "_" ).Replace( "<pre_t>", PRE_t ).Replace( "<<linkboy>>", UserCL + Head + "\n" + Var + "\n" + Code );
			return r;
		}
		if( ModeV ) {
			string r = ModCode.Replace( "#.", PRE_g + "_" ).Replace( "<pre_t>", PRE_t );
			
			r = r.Replace( V_Rep_SYS_ROM, V_SYS_ROM );
			r = r.Replace( V_Rep_SYS_RAM, V_SYS_RAM );
			r = r.Replace( V_Rep_SYS_BPURAM, V_SYS_BPURAM );
			
			
			//创建软核访问GPIO缓冲器 (需要手工删除IP用到的端口)
			string mr = "always @(posedge " + SST.V_ClockName + ") begin\n";
			
			mr += "if(" + SST.V_ResetName + "==0) begin cx_M_PORTA_DIR <= 0; cx_M_PORTB_DIR <= 0; end else begin\n";
			
			string mr1 = "cx_PORTA_DIR <= cx_M_PORTA_DIR";
			string mr2 = "cx_PORTA <= cx_M_PORTA";
			string mr3 = "cx_PORTB_DIR <= cx_M_PORTB_DIR";
			string mr4 = "cx_PORTB <= cx_M_PORTB";
			
			//以下为临时, 调试 BitPU
			if( V_DynModuleList != "" ) {
				string[] cut = V_DynModuleList.TrimEnd( ',' ).Split( ',' );
				
				for( int i = 0; i < cut.Length; ++i ) {
					mr1 += " & cx_" + cut[i] + "_M_AND_PORTA_DIR";
					mr2 += " & cx_" + cut[i] + "_M_AND_PORTA_OUT";
					mr3 += " & cx_" + cut[i] + "_M_AND_PORTB_DIR";
					mr4 += " & cx_" + cut[i] + "_M_AND_PORTB_OUT";
				}
				for( int i = 0; i < cut.Length; ++i ) {
					mr1 += " | cx_" + cut[i] + "_M_OR_PORTA_DIR";
					mr2 += " | cx_" + cut[i] + "_M_OR_PORTA_OUT";
					mr3 += " | cx_" + cut[i] + "_M_OR_PORTB_DIR";
					mr4 += " | cx_" + cut[i] + "_M_OR_PORTB_OUT";
				}
			}
			mr += mr1 + ";\n" + mr2 + ";\n" + mr3 + ";\n" + mr4 + ";\n\n";
			mr += "end\n";
			mr += "end\n";
			/*
			for( int i = 0; i < 32; ++i ) {
				if( V_UsedPinIndex.IndexOf( " " + i + " " ) == -1 ) {
					mr += "cx_PORTA[" + i + "] <= cx_M_PORTA[" + i + "]; cx_PORTA_DIR[" + i + "] <= cx_M_PORTA_DIR[" + i + "];\n";
				}
				if( V_UsedPinIndex.IndexOf( " " + (i+32) + " " ) == -1 ) {
					mr += "cx_PORTB[" + i + "] <= cx_M_PORTB[" + i + "]; cx_PORTB_DIR[" + i + "] <= cx_M_PORTB_DIR[" + i + "];\n";
				}
			}
			*/
			
			
			r = r.Replace( "<<linkboy>>", UserCL + Head + "\n" + Var + "//$BUS$\n" + Code );
			r = n_LabelList.LabelList.V_Replace( r );
			
			string vhead = null;
			//vhead = "\nmodule led (input wire CLK_IN, input wire RST_N" + V_InterVar + " );\n\n"; //安路
			vhead = "\nmodule fpga_led (Clk, Rst_n, O_cx_PORTA, O_cx_PORTB );\n\tinput Clk;\n\tinput Rst_n;\n\tinout wire [31:0] O_cx_PORTA;\n\tinout wire [31:0] O_cx_PORTB;\n\n"; //智多晶
			
			r = vhead + r; // + "\n" +
				//"initial $readmemh(\"H:\\\\_soft\\\\FPGA\\\\code.mem\",cx_core1_ROM);\n" +
				//"\nendmodule\n";
			
				
				
			//临时设置为单always驱动
			//r = r.Replace( "end\nalways @(posedge " + SST.V_ClockName + ") begin\n", "//end\n//always @(posedge " + SST.V_ClockName + ") begin\n" );
			
			r = r.Replace( "end\nalways @(posedge " + SST.V_ClockName + ") begin", "//end\n//always @(posedge " + SST.V_ClockName + ") begin" );
			
			//提取总线交叉矩阵
			string BusCode = "";
			int CoreNumber = n_Deploy.Deploy.CoreIndex + 1;
			string[] core_reset = new string[CoreNumber];
			for( int i = 0; i < n_VarList.VarList.length; ++i ) {
				VarNode v = n_VarList.VarList.NodeSet[ i ];
				if( !v.isVerilogMult ) {
					continue;
				}
				string vName = GetFullName( v.Name );
				
				for( int j = 0; j < CoreNumber; ++j ) {
					if( v.VisitCoreList.IndexOf( " " + j + " " ) != -1 ) {
						//core_reset[j] += vName + "_t" + j + "<=0;";
						core_reset[j] += vName + "_flg" + j + "<=0; ";
					}
				}
				if( v.VeriType == 0 ) {
					BusCode += Verilog.GenBus( v );
				}
				else {
					BusCode += Verilog.GenDivBus( v );
				}
			}
			if( Verilog.ResetLen != Verilog.UserCoreNumber ) {
				n_OS.VIO.Show( "图形界面cpu核的数量(" + Verilog.ResetLen + ")应等于程序文件中定义的cup数量(" + Verilog.UserCoreNumber + ")" );
				isError = true;
				return null;
			}
			//查找每个核main函数的起始地址
			for( int i = 0; i < Verilog.ResetLen; ++i ) {
				int fnn = FunctionList.GetIndex( "#." + Verilog.ResetList[i] );
				if( fnn == -1 ) {
					n_OS.VIO.Show( "未找到指定核的复位函数: " + Verilog.ResetList[i] );
					isError = true;
					continue;
				}
				n_FunctionNode.FunctionNode fn = FunctionList.Get( fnn );
				Verilog.CoreVar cv = Verilog.Core_Get( fn.V_CoreIndex );
				if( cv.EnterPC != -1 ) {
					n_OS.VIO.Show( "此cpu核(" + fn.V_CoreIndex + ")已指定复位入口函数: " + cv.EnterFunc + ", 无法再指定新的复位入口函数: " + Verilog.ResetList[i] );
					isError = true;
				}
				cv.EnterPC = fn.V_PC;
				cv.EnterFunc = fn.FunctionName;
			}
			
			for( int j = 0; j < CoreNumber; ++j ) {
				Verilog.CoreVar cv = Verilog.Core_Get( j );
				r = r.Replace( "//$CORE_INIT_" + j + "$", core_reset[j] );
				int epc = cv.EnterPC;
				if( epc == -1 ) {
					epc = 1;
				}
				r = r.Replace( Verilog.GetRName( "MAIN_START", j.ToString() ), epc.ToString() );
			}
			r = r.Replace( "//$BUS$", BusCode );
			return r;
		}
		return null;
	}
	
	//刷新配置
	public static void RefreshModPlatform()
	{
		CodeArrayByFunc = 0;
		CodeArrayPre = "const";
		
		if( ModPlatform == "1" ) {
			ModCode = ModCode_Arduino8_MEGA328;
			
			CodeArrayByFunc = 1;
			CodeArrayPre = "const PROGMEM";
		}
		else if( ModPlatform == "2" ) {
			ModCode = ModCode_Arduino8;
			
			CodeArrayByFunc = 1;
			CodeArrayPre = "const PROGMEM";
		}
		else if( ModPlatform == "3" ) {
			ModCode = ModCode_Arduino32;
		}
		else if( ModPlatform == "4" ) {
			ModCode = ModCode_C51;
		}
		else {
			ModCode = ModCode_Default;
		}
	}
	
	//添加到头文件代码段
	public static void AddHead( string s )
	{
		if( !EN || TempClose ) {
			return;
		}
		Head += s;
	}
	
	//添加全局变量定义
	public static void AddVar( string s )
	{
		if( !EN || TempClose ) {
			return;
		}
		Var += s;
	}
	
	//添加函数起始预定义, 可用于C90模式下局部变量定义
	public static void AddFuncPre( string s )
	{
		if( !EN || TempClose ) {
			return;
		}
		FuncVar += "\t" + s;
	}
	
	//替换函数预定义
	public static void ReplaceFuncPre()
	{
		if( FuncVar != "" ) {
			Code = Code.Replace( "\t" + FuncPre + "\n", FuncVar );
			FuncVar = "";
		}
		else {
			Code = Code.Replace( "\t" + FuncPre + "\n", "" );
		}
	}
	
	//替换 verilog 参数
	public static void V_Replace( string olds, string news )
	{
		Code = Code.Replace( olds, news );
	}
	
	//添加代码段
	public static void Add( string s )
	{
		if( !EN || TempClose ) {
			return;
		}
		if( s.EndsWith( "}\n" ) ) {
			TabNum--;
		}
		Code += "".PadLeft( TabNum, '\t' ) + s;
		
		if( s.EndsWith( "{\n" ) ) {
			TabNum++;
		}
	}
	
	//添加代码段
	public static void AddVInterVar( string s )
	{
		if( !EN || TempClose ) {
			return;
		}
		if( ModeV ) {
			
			/*
			if( V_InterVar == "" ) {
				V_InterVar += ",\n" + s;
			}
			else {
				V_InterVar += ",\n" + s;
			}
			*/
			V_InterVar += ",\n" + s;
		}
	}
	
	//获取一个名字的最终c名字
	public static string GetFullName( string name )
	{
		if( name == null ) {
			return "<SST_fullname_null>";
		}
		name = name.Replace( "#.", PRE_g + "_" ).Replace( ".", "_" );
		
		name = n_OS.PFunc.PinYin.GetPinyin( name );
		
		name = name.Replace( " ", "" );
		
		return name;
	}
}
public static class Verilog
{
	public const string F_nop = ".nop";
	public const string F_delay_clk = ".delay_clk";
	public const string F_delay_us = ".delay_us";
	public const string F_delay_ms = ".delay_ms";
	
	//初始化
	public static void Init()
	{
		CoreVarList = new Verilog.CoreVar[MAX_CORE_NUM];
		ResetList = new string[MAX_CORE_NUM];
		UserCoreNumber = 0;
	}
	
	//外部清空
	public static void GUI_Clear()
	{
		//for( int i = 0; i < MAX_CORE_NUM; ++i ) {
		//	ResetList[i] = null;
		//}
		ResetLen = 0;
	}
	
	//复位
	public static void Clear()
	{
		CVLen = 0;
	}
	
	//生成一个verilog变量的总线管理器
	public static string GenBus( VarNode v )
	{
		int size = n_VarType.VarType.GetBitSize( v.Type );
		string rtype = "reg";
		string reset = "";
		string ocase = "";
		string sw = "";
		string result = "";
		int max_core = n_Deploy.Deploy.CoreIndex + 1;
		string FName = SST.GetFullName( v.Name );
		string[] corelist = v.VisitCoreList.Trim( ' ' ).Split( ' ');
		int UsedNumber = corelist.Length;
		int idx = 0;
		for( int i = 0; i < max_core; i++ ) {
			if( v.VisitCoreList.IndexOf( " " + i + " " ) == -1 ) {
				continue;
			}
			result += ( rtype + " [" + (size-1) + ":0] " + FName + "_t" + i + ";\n" );
			result += ( rtype + " [0:0] " + FName + "_flg" + i + ";\n" );
			result += ( rtype + " [0:0] " + FName + "_ack" + i + ";\n" );
			reset += FName + "_ack" + i + "<=0; ";
			
			int next = idx+1;
			if( idx == UsedNumber - 1 ) {
				next = 0;
			}
			sw += "\t" + idx + ": if(" + FName + "_flg" + i + "!=0) " + FName + "_pc<=" + (UsedNumber+idx*2) + "; else " + FName + "_pc<=" + next + ";\n";
			ocase += "\t" + (UsedNumber+idx*2) + ": begin " + FName + "<=" + FName + "_t" + i + "; " +
				FName + "_ack" + i + "<=1; " + FName + "_pc<=" + (UsedNumber+idx*2+1) + "; end\n";
			ocase += "\t" + (UsedNumber+idx*2+1) + ": if( " + FName + "_flg" + i + "==0 ) " + "begin " + FName + "_ack" + i + "<=0; " + FName + "_pc <= 0; end\n";
			idx++;
		}
		result += ( rtype + " [0:0] " + FName + "_temp" + ";\n" );
		result += ( rtype + " [3:0] " + FName + "_pc" + ";\n" );
		result += ( "always @(posedge " + SST.V_ClockName + ") begin\n" );
		result += ( "if(" + SST.V_ResetName + "==0) begin " + FName + "_pc <= 0; " + reset + "end else\n" );
		result += ( "case( " + FName + "_pc )\n" );
		
		result += ( sw );
		result += ( ocase );
		
		result += ( "\tdefault: " + FName + "_temp" + "<=0;\n" );
		result += ( "endcase\n" );
		result += ( "end\n" );
		return result;
	}
	
	//生成一个verilog除法的总线管理器
	public static string GenDivBus( VarNode v )
	{
		int size = n_VarType.VarType.GetBitSize( v.Type );
		string rtype = "reg";
		string reset = "";
		string ocase = "";
		string sw = "";
		string result = "";
		int max_core = n_Deploy.Deploy.CoreIndex + 1;
		string FName = SST.GetFullName( v.Name );
		string[] corelist = v.VisitCoreList.Trim( ' ' ).Split( ' ');
		int UsedNumber = corelist.Length;
		int idx = 0;
		for( int i = 0; i < max_core; i++ ) {
			if( v.VisitCoreList.IndexOf( " " + i + " " ) == -1 ) {
				continue;
			}
			result += ( rtype + " [31:0] " + FName + "_Numer" + i + ";\n" );
			result += ( rtype + " [31:0] " + FName + "_Denom" + i + ";\n" );
			result += ( rtype + " [0:0] " + FName + "_flg" + i + ";\n" );
			result += ( rtype + " [0:0] " + FName + "_ack" + i + ";\n" );
			reset += FName + "_ack" + i + "<=0; ";
			
			int next = idx+1;
			if( idx == UsedNumber - 1 ) {
				next = 0;
			}
			sw += "\t" + idx + ": if(" + FName + "_flg" + i + "!=0) " + FName + "_pc<=" + (UsedNumber+idx*4) + "; else " + FName + "_pc<=" + next + ";\n";
			ocase += "\t" + (UsedNumber+idx*4) + ": begin cx_DIV_Numer<=" + FName + "_Numer" + i + "; " + "cx_DIV_Denom<=" + FName + "_Denom" + i + "; cx_DIV_Start <= 1; " +
									FName + "_pc<=" + (UsedNumber+idx*4+1) + "; end\n";
			ocase += "\t" + (UsedNumber+idx*4+1) + ": begin cx_DIV_Start <= 0; " + FName + "_pc<=" + (UsedNumber+idx*4+2) + "; end\n";
			ocase += "\t" + (UsedNumber+idx*4+2) + ": if( cx_DIV_Done != 0 ) begin cx_DIV_Quotient_r <= cx_DIV_Quotient; " +
									FName + "_ack" + i + "<=1; " + FName + "_pc<=" + (UsedNumber+idx*4+3) + "; end\n";
			ocase += "\t" + (UsedNumber+idx*4+3) + ": if( " + FName + "_flg" + i + "==0 ) " + "begin " + FName + "_ack" + i + "<=0; " + FName + "_pc <= 0; end\n";
			idx++;
		}
		result += ( rtype + " [0:0] " + FName + "_temp" + ";\n" );
		result += ( rtype + " [3:0] " + FName + "_pc" + ";\n" );
		result += ( "always @(posedge " + SST.V_ClockName + ") begin\n" );
		result += ( "if(" + SST.V_ResetName + "==0) begin " + FName + "_pc <= 0; " + reset + "end else\n" );
		result += ( "case( " + FName + "_pc )\n" );
		
		result += ( sw );
		result += ( ocase );
		
		result += ( "\tdefault: " + FName + "_temp" + "<=0;\n" );
		result += ( "endcase\n" );
		result += ( "end\n" );
		return result;
	}
	
	//获取一个替换名
	public static string GetRName( string type, string name )
	{
		return "<<" + type + "+" + name + ">>";
	}
	
	//-------------------------------------------
	
	const int MAX_CORE_NUM = 200;
	public static CoreVar[] CoreVarList;
	public static int CVLen;
	
	public static string[] ResetList;
	public static int ResetLen;
	
	public static int UserCoreNumber;
	
	public static void AddReset( string enter )
	{
		ResetList[ResetLen] = enter;
		ResetLen++;
	}
	
	//创建一个核对象
	public static void Core_Add()
	{
		CoreVarList[CVLen] = new CoreVar();
		CVLen++;
	}
	
	//获取指定的核对象
	public static CoreVar Core_Get( int i )
	{
		return CoreVarList[i];
	}
	
	//-------------------------------------------
	
	public class CoreVar
	{
		public string VisitVarList;
		
		public int EnterPC;
		public string EnterFunc;
		
		public CoreVar()
		{
			VisitVarList = "";
			
			//对于库IP来说, 不设置起始函数时, 默认选择第一个函数作为main函数
			EnterPC = -1;
		}
	}
}
}


