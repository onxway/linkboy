﻿
//控制语句转化器
namespace n_FlowSence
{
using System;
using n_Code1;
using n_Deploy;
using n_ET;
using n_Expression;
using n_ParseNet;
using n_VarList;
using n_VarNode;
using n_VarType;
using n_WordList;
using n_ConstString;
using n_Config;
using n_SST;
using n_MemberType;

public static partial class FlowSence
{
	//迭代循环
	static void Chiselfor语句( int Index )
	{
		int ControlIndex = Deploy.SenceNumber;
		
		//获取主语句索引
		int MainSenceIndex = int.Parse( ParseNet.NodeSet[ Index ][ 10 ] );
		
		Expression.AddIns( Code1.语句层加 );
		
		string exp = null;
		int NumberIndex0 = int.Parse( ParseNet.NodeSet[ Index ][ 6 ] );
		int NumberIndex1 = int.Parse( ParseNet.NodeSet[ Index ][ 8 ] );
		Expression.isPad = true;
		
		int Number0 = int.Parse( ConstExpression.GetExpressionValue( NumberIndex0, Deploy.UnitName ) );
		int Number1 = int.Parse( ConstExpression.GetExpressionValue( NumberIndex1, Deploy.UnitName ) );
		
		int V_StPC = 0;
		string V_LoopVar = "LP_Var" + ControlIndex;
		if( SST.ModeV ) {
			
			SST.AddVar( "reg[31:0] " + V_LoopVar + ";\n" );
			
			SST.Add( Expression.V_Add( V_LoopVar + " <= " + Number0 + ";" ) );
			Deploy.V_PC += 1;
			
			V_StPC = Deploy.V_PC;
			
			SST.Add( Expression.V_Add( "if( " + V_LoopVar + " != " + (Number1+1) + " ) " + V_LoopVar + " <= " + V_LoopVar + " + 1; else " + Deploy.V_NamePC() + " <= $FS" + ControlIndex + "$;" ) );
			
			Deploy.V_PC += 1;
		}
		//判断是否为用户空间, 是的话需要设置为静态变量
		else if( n_FunctionList.FunctionList.Get( Deploy.FunctionIndex ).InterType == MemberType.FunctionType.Task ) { //n_Param.Param.UserspaceOn ) {
			
			string tname = SST.GetFullName( "#._cx_sc_" + Deploy.SVarTempIdx );
			Deploy.SVarTempIdx++;
			
			string tnum = SST.GetFullName( "#._cx_sc_" + Deploy.SVarTempIdx );
			Deploy.SVarTempIdx++;
			
			//应定义为全局变量
			SST.AddVar( SST.PRE_t + "uint16 " + tname + ";\n" );
			SST.AddVar( SST.PRE_t + "uint16 " + tnum + ";\n" ); //循环次数也要设置为全局的
			
			SST.Add( tnum + " = " + exp + ";\n" ); //循环次数也要设置为全局的
			SST.Add( "for( " + tname + " = 0; " + tname + " < " + tnum + "; " + tname + "++ ) {\n" );
		}
		else {
			string tname = "_cx_lc_" + Deploy.LVarTempIdx;
			Deploy.LVarTempIdx++;
			
			string tnum = "_cx_lc_" + Deploy.LVarTempIdx;
			Deploy.LVarTempIdx++;
			
			if( SST.Mode_C90 ) {
				
				SST.AddFuncPre( SST.PRE_t + "uint16 " + tname + ";\n" );
				SST.AddFuncPre( SST.PRE_t + "uint16 " + tnum + ";\n" );
				
				SST.Add( tnum + " = " + exp + ";\n" );
				SST.Add( "for( " + tname + " = 0; " + tname + " < " + tnum + "; " + tname + "++ ) {\n" );
			}
			else {
				SST.Add( SST.PRE_t + "uint16 " + tnum + " = " + exp + ";\n" );
				SST.Add( "for( " + SST.PRE_t + "uint16 " + tname + " = 0; " + tname + " < " + tnum + "; " + tname + "++ ) {\n" );
			}
		}
		
		Expression.AddIns( Code1.标号 + " " + Config.PreLabel.Start + ControlIndex );
		
		Expression.AddIns( Code1.标号 + " " + Config.PreLabel.Head + ControlIndex );
		Deploy.语句( MainSenceIndex, ControlIndex );
		Expression.AddIns( Code1.标号 + " " + Config.PreLabel.Continue + ControlIndex );
		
		Deploy.跳转( Config.PreLabel.Start + ControlIndex );
		
		Expression.AddIns( Code1.标号 + " " + Config.PreLabel.End + ControlIndex );
		
		Expression.AddIns( Code1.语句层减 );
		
		if( SST.ModeC ) {
			SST.Add( "}\n" );
		}
		else {
			SST.Add( Expression.V_Add( Deploy.V_NamePC() + " <= " + V_StPC + ";" ) );
			Deploy.V_PC += 1;
			
			int V_EndPC = Deploy.V_PC;
			
			//这里进行替换....
			SST.V_Replace( "$FS" + ControlIndex + "$", V_EndPC.ToString() );
		}
	}
}
}




