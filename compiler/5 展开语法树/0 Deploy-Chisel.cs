﻿
//输入: 语法树 --> 中间语言指令序列
//每个指令的元素都用空格分隔(用+表示空格, 用&表示字符串连接)
//变量索引是 int 类型

namespace n_Deploy
{
using System;
using n_Code1;
using n_Config;
using n_FlowSence;
using n_ET;
using n_Expression;
using n_FunctionCodeList;
using n_FunctionList;
using n_LabelList;
using n_LabelNode;
using n_MemberType;
using n_Operation;
using n_Parse;
using n_ParseNet;
using n_VarList;
using n_VarType;
using n_WordList;
using n_CallList;
using n_CDebug;
using n_Param;
using n_Opti;
using n_SST;
using n_GateTree;

public static partial class Deploy
{
	static void Chisel函数( int Index )
	{
		if( Param.function_used ) {
			Param.function_used = false;
			FunctionList.Get( FunctionIndex ).isUsed = true;
		}
		LVarTempIdx = 0;
		ExpLevel = 1;
		
		if( SST.ModeV ) {
			FunctionList.Get( FunctionIndex ).V_PC = V_PC;
			FunctionList.Get( FunctionIndex ).V_CoreIndex = CoreIndex;
		}
		
		Expression.AddIns( Code1.函数名称 + " " + n_Config.Config.PreLabel.Function + FunctionIndex );
		
		string sst_varlist = "";
		
		//函数形参列表
		string varlist = FunctionList.Get( FunctionIndex ).VarIndexList;
		int FuncVarIndex = 0;
		string[] VarCut = null;
		if( varlist != null ) {
			VarCut = varlist.Split( ' ' );
		}
		
		int SenceIndex = int.Parse( ParseNet.NodeSet[ Index ][ 10 ] );
		
		if( FunctionList.Get( FunctionIndex ).InterType != MemberType.FunctionType.Task && sst_varlist != "" ) {
			sst_varlist += " ";
		}
		else {
			sst_varlist = "void";
		}
		
		SST.TempClose = FunctionList.Get( FunctionIndex ).FunctionName.IndexOf( "@" ) != -1;
		
		if( FunctionList.Get( FunctionIndex ).RealRefType != MemberType.RealRefType.link ) {
			//这里返回类型仅限定为基本类型
			string rtype = FunctionList.Get( FunctionIndex ).ReturnType;
			if( rtype != VarType.Void ) {
				rtype = SST.PRE_t + rtype;
			}
			string head = rtype + " " + SST.GetFullName( FunctionList.Get( FunctionIndex ).FunctionName ) + "(" +  sst_varlist + ")";
			
			//函数声明
			if( SST.ModeC ) {
				SST.AddHead( head + ";\n" );
				SST.Add( head + "\n" );
				SST.Add( "{\n" );
				SST.Add( SST.FuncPre + "\n" );
			}
			if( SST.ModeV ) {
				SST.Add( "//" + head + "\n" );
				
				//SST.Add( Expression.V_Add( Deploy.V_NamePC() + "_Back_SP <= " + Deploy.V_NamePC() + "_Back_SP + 1;" ) );
				//Deploy.V_PC += 1;
			}
		}
		
		语句列表( SenceIndex, -1 );
		//判断当前函数是否包含返回,不包含则自动添加
		//if( !Result.EndsWith( "返回\n" ) ) {
		//Expression.AddIns( Code1.返回 );
		//}
		
		if( FunctionCodeList.LastIns != Code1.返回 ) {
			Expression.AddIns( Code1.返回 );
			
			//这里判断是否是有返回值的函数
			if( Config.WarningAsError && FunctionList.Get( FunctionIndex ).ReturnType != VarType.Void &&
			   FunctionList.Get( FunctionIndex ).RealRefType != MemberType.RealRefType.link &&
			   FunctionList.Get( FunctionIndex ).InterType != MemberType.FunctionType.Interface ) {
				int endIndex = int.Parse( ParseNet.NodeSet[ Index ][ 10 ] );
				ET.WriteParseError( endIndex, "有返回值的函数必须要用 return 语句返回一个合法数据: " + FunctionList.Get( FunctionIndex ).FunctionName );
			}
		}
		
		//一定要添加空语句, 因为FunctionCodeList.AddIns只是写入到缓冲区指令
		FunctionCodeList.AddCode( null );
		
		if( FunctionList.Get( FunctionIndex ).RealRefType != MemberType.RealRefType.link ) {
			
			if( SST.ModeC ) {
				SST.Add( "}\n" );
				SST.ReplaceFuncPre();
			}
			if( SST.ModeV ) {
				//SST.Add( Expression.V_Add( Deploy.V_NamePC() + "_Back_SP <= " + Deploy.V_NamePC() + "_Back_SP - 1;" ) );
				//Deploy.V_PC++;
				
				string RetPC = SST.GetFullName( "#._cx_fv" + FunctionIndex + "_Sys_RetPC" );
				SST.Add( Expression.V_Add( Deploy.V_NamePC() + " <= " + RetPC + ";" ) );
				Deploy.V_PC++;
			}
		}
		
		SST.TempClose = false;
	}
}
}



