﻿
namespace n_Code1
{
using System;

public static class Code1
{
	public const string 单目运算	= "单目运算";	// 单目运算? 变量索引 变量索引
	public const string 双目运算	= "双目运算";	// 双目运算? 变量索引 变量索引 变量索引
	public const string 地址偏移	= "地址偏移";	// 地址偏移 变量索引 变量索引 变量索引 #常量值
	public const string 结构偏移	= "结构偏移";	// 结构偏移 变量索引 变量索引 #偏移量
	public const string 读取分量	= "读取分量";	// 读取分量 变量索引 变量索引 #位置
	public const string 写入分量	= "写入分量";	// 写入分量 变量索引 变量索引 变量索引 #位置
	public const string 常量赋值	= "常量赋值";	// 常量赋值 变量索引 #常量值
	public const string 隐式转换	= "隐式转换";	// 隐式转换 变量索引 变量索引
	public const string 读取地址	= "读取地址";	// 读取地址 变量索引 #变量索引
	
	//流程标号指:end_? else_? continue_? start_? loop_? case_?
	public const string 肯定跳转	= "肯定跳转";	//肯定跳转 # 变量索引 流程标号
	public const string 否定跳转	= "否定跳转";	//否定跳转 # 变量索引 流程标号
	public const string 跳转递减	= "跳转递减";	//递减跳转 变量索引 变量索引 流程标号
	public const string 分支比较	= "分支比较";	//分支比较 # 变量索引 变量索引 #数目 流程标号
	public const string 分支散转	= "分支散转";	//分支散转 # 变量索引 流程标号
	
	public const string 定义变量	= "#定义变量";	// 定义变量 # 变量索引
	
	/*
	public const string LOAD_local_uint8		= "LOAD_local_uint8";
	public const string LOAD_local_uint16		= "LOAD_local_uint16";
	public const string LOAD_local_uint32		= "LOAD_local_uint32";
	*/
	
	public const string 加载_0		= "加载_0";		// 加载_0 变量索引
	public const string 加载_1		= "加载_1";		// 加载_1 变量索引
	public const string 保存_0		= "保存_0";		// 保存_0 变量索引
	public const string 读系统量	= "读系统量";	// 读系统量 变量索引
	public const string 写系统量	= "写系统量";	// 写系统量 # 变量索引
	public const string 设置堆栈	= "设置堆栈";	// 设置堆栈 #函数索引
	public const string 堆栈递增	= "堆栈递增";	// 堆栈递增 #函数索引
	public const string 堆栈递减	= "堆栈递减";	// 堆栈递减 #函数索引
	public const string 函数地址	= "函数地址";	// 函数地址 变量索引 #函数索引
	public const string 标签地址	= "标签地址";	// 标签地址 变量索引 #标签名称
	public const string 函数尺寸	= "函数尺寸";	// 函数尺寸 变量索引 #函数索引
	public const string 传递形参	= "传递形参";	// 传递形参 变量索引 变量索引
	public const string 设置云参数	= "设置云参数";	// 设置云参数 #芯片序号 #函数索引 #返回尺寸 #形参尺寸
	
	public const string 禁用中断	= "#禁用中断";		//
	public const string 启用中断	= "#启用中断";		//
	
	public const string 函数名称	= "#函数名称";	// #函数名称 函数索引
	public const string 保护现场	= "#保护现场";	// #保护现场    //进入中断程序
	public const string 恢复现场	= "#恢复现场";	// #恢复现场    //退出中断程序
	public const string 保护接口实参	= "#保护接口实参";	// #保护现场    //进入中断程序
	public const string 恢复接口实参	= "#恢复接口实参";	// #恢复现场    //退出中断程序
	public const string 返回		= "#返回";		// #返回
	public const string 中断返回	= "#中断返回";	// #中断返回
	public const string 域返回	= "#域返回";		// #域返回 $number
	public const string 函数调用	= "#函数调用";	// #函数调用 函数索引
	public const string 标号		= "#标号";		// #标号 s_+标号索引 s= label continue else loop start ...
	public const string 语句层加	= "#语句层加";	// #语句层加
	public const string 语句层减	= "#语句层减";	// #语句层减
	public const string 跳转		= "#跳转";		// #跳转 标号索引
	public const string 内嵌汇编	= "#内嵌汇编";	// #内嵌汇编 词法序号
	public const string 系统标志	= "#系统标志";	// #系统标志 词法序号
	public const string 转移参数	= "#转移参数";	// 转移参数
	
	//PC80X86专用
	public const string 转移形参	= "转移形参";	// 转移堆栈 #函数索引
	public const string 压栈		= "压栈";		// 压栈 # 变量索引
}
}


