﻿
using System;
using n_FunctionList;

namespace n_Opti
{
public static class Opti
{
	//初始化
	public static void Init()
	{
	}
	
	//复位
	public static void Reset()
	{
		
	}
	
	//运行优化 (未调用函数移除)
	public static void Run()
	{
		int sf = n_Config.Config.GetStartFunction();
		
		//设置起始函数使用标志为真
		FunctionList.Get( sf ).isUsed = true;
		
		//到这里使用标志为真的函数有: 1-起始函数, 2-函数地址, 3-中断函数
		
		//遍历函数调用列表, 将所有 USED 标志的子函数设置为 USED
		for( int i = 0; i < FunctionList.length; ++i ) {
			if( FunctionList.Get( i ).isUsed ) {
				SetSubUsed( i );
			}
		}
	}
	
	//设置当前函数的使用标志及调用函数列表使用标志
	public static void SetSubUsed( int FuncIndex )
	{
		if( FunctionList.Get( FuncIndex ).Dealed ) {
			return;
		}
		FunctionList.Get( FuncIndex ).Dealed = true;
		
		string s = n_CallList.CallList.GetSubFunction( FuncIndex ).Trim( ' ' );
		
		if( s != "" ) {
			string[] SubList = s.Split( ' ' );
			for( int i = 0; i < SubList.Length; ++i ) {
				int subi = int.Parse( SubList[i] );
				FunctionList.Get( subi ).isUsed = true;
				SetSubUsed( subi );
			}
		}
	}
}
}


