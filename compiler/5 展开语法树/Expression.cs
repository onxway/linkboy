﻿
//表达式转化器

namespace n_Expression
{
using System;
using n_Code1;
using n_Config;
using n_ConstString;
using n_CPUType;
using n_Deploy;
using n_ET;
using n_FunctionCodeList;
using n_FunctionList;
using n_MemberType;
using n_Operation;
using n_Parse;
using n_ParseNet;
using n_StructList;
using n_UnitList;
using n_VarList;
using n_VarNode;
using n_VarType;
using n_VdataList;
using n_WordList;
using n_CallList;
using n_SST;
using n_RecordMacro;

public static class Expression
{
	public static bool isPad;
	
	public static void Reset()
	{
		isPad = false;
	}
	
	//计算常量表达式
	public static bool isConstExpError;
	public static int ConstExp( int ExpIndex )
	{
		isConstExpError = false;
		string uname = Deploy.UnitName;
		if( RecordMacro.isRecord ) {
			uname = RecordMacro.UnitName;
		}
		string val = ConstExpression.GetExpressionValue( ExpIndex, uname );
		if( val == null ) {
			isConstExpError = true;
			return 0;
		}
		return int.Parse( val );
	}
	
	//如果返回 -1,表示解析表达式出现错误,而返回 void 类型时通过 GetType 去判断
	public static int 表达式( int Index, ref string exp )
	{
		++Deploy.ExpLevel;
		
		int Name = -1;
		string Head = ParseNet.NodeSet[ Index ][ 0 ];
		switch( Head ) {
			case ParseNet.Node.括号运算:		Name = 括号运算( Index, ref exp ); break;
			case ParseNet.Node.双目运算符:	Name = 双目运算( Index, ref exp ); break;
			case ParseNet.Node.单目运算:		Name = 单目运算( Index, ref exp ); break;
			case ParseNet.Node.函数地址:		Name = 函数地址( Index, ref exp ); break;
			case ParseNet.Node.函数变量尺寸:	Name = 函数变量尺寸( Index ); break;
			case ParseNet.Node.函数调用:		Name = 函数调用( Index, ref exp ); break;
			case ParseNet.Node.变量:			Name = 变量( Index, ref exp ); break;
			case ParseNet.Node.带宏变量:		Name = 带宏变量( Index, ref exp ); break;
			case ParseNet.Node.新变量:		Name = 新变量( Index, ref exp ); break;
			case ParseNet.Node.表达式:		Name = 表达式( int.Parse( ParseNet.NodeSet[ Index ][ 1 ] ), ref exp ); break;
			//case ParseNet.Node.终结词:		Name = 终结词( Index ); break;
			default :
				ET.ShowError( "<常量表达式> 未定义的运算项: " + Head );
				break;
		}
		isPad = false;
		return Name;
	}
	
	static int 括号运算( int Index, ref string exp )
	{
		isPad = true;
		++Deploy.ExpLevel;
		int ret = 表达式( int.Parse( ParseNet.NodeSet[ Index ][ 2 ] ), ref exp );
		
		exp = "(" + exp + ")";
		
		return ret;
	}
	
	static int 双目运算( int Index, ref string exp )
	{
		++Deploy.ExpLevel;
		
		//获取运算符号
		int OperIndex = int.Parse( ParseNet.NodeSet[ Index ][ 2 ] );
		string Oper = WordList.GetWord( OperIndex );
		
		Deploy.Line = WordList.GetLine( OperIndex );
		
		//如果是复合运算, 就拆成普通运算
		bool isComp = false;
		if( Deploy.ExpLevel == 2 ) {
			if( !Config.WarningAsError ) {
				isComp = true; //这里注释后就不再支持不带=的复合表达式了
			}
		}
		//如果是复合运算, 就拆成普通运算
		if( Oper == ":=" ) {
			Oper = "=";
		}
		if( Oper.EndsWith( "=" ) &&
					Oper != "==" && Oper != "!=" && Oper != "<=" && Oper != ">=" && Oper != "=" ) {
			Oper = Oper.Remove( Oper.Length - 1 );
			isComp = true;
			isPad = true;
		}
		
		//逻辑运算防护
		if( Oper == "&" || Oper == "|" || Oper == "^" || Oper == ">>" || Oper == "<<" ) {
			if( !isPad ) {
				ET.WriteParseError( OperIndex, "表达式中含有位运算( & | ^ >> << )时, 必须要加上括号明确表示运算次序: " + Oper );
			}
		}
		isPad = false;
		
		//解析左右两个操作数
		string exp1 = null;
		string exp2 = null;
		
		int Name1 = 表达式( int.Parse( ParseNet.NodeSet[ Index ][ 1 ] ), ref exp1 );
		
		if( Oper == "=" || isComp ) {
			isPad = true;
		}
		int Name2 = 表达式( int.Parse( ParseNet.NodeSet[ Index ][ 3 ] ), ref exp2 );
		if( Name1 == -1 || Name2 == -1 ) {
			return -1;
		}
		
		Name2 = 读取宏变量( Name2, ref exp2 );
		if( Name2 == -1 ) {
			return -1;
		}
		//如果是赋值运算,添加赋值语句返回
		if( Oper == "=" ) {
			赋值运算( Name1, Name2, ref exp, exp1, exp2 );
			VarNode vv = new VarNode();
			vv.SetTempValue( VarType.Void, Deploy.VarLevel, OperIndex );
			
			return VarList.Add( vv );
		}
		//如果是引用赋值运算,添加引用赋值语句返回
		if( Oper == "->" ) {
			
			引用运算( Name1, Name2, ref exp, exp1, exp2 );
			VarNode vv = new VarNode();
			vv.SetTempValue( VarType.Void, Deploy.VarLevel, OperIndex );
			return VarList.Add( vv );
		}
		Name1 = 读取宏变量( Name1, ref exp1 );
		if( Name1 == -1 ) {
			return -1;
		}
		int j = 双目解析运算( Oper, Name1, Name2, ref exp1, ref exp2 );
		
		
		string exp2A = exp1 + " " + Oper + " " + exp2;
		//复合运算需要加上括号
		if( isComp ) {
			
			if( SST.ModeC ) {
				exp2A = exp1 + " " + Oper + " (" + exp2 + ")";
			}
			if( SST.ModeV ) {
				
				if( Oper == "/" ) {
					Verilog_Div( exp1, exp2 );
					exp2A = "cx_DIV_Quotient_r";
				}
				else {
					exp2A = exp1 + " " + Oper + " (" + exp2 + ")";
				}
			}
		}
		
		if( j == -1 ) {
			return -1;
		}
		//处理复合运算符
		if( isComp ) {
			
			exp = exp1 + " " + Oper + "= " + exp2;
			
			VarList.Get( Name1 ).IsComp = true;
			赋值运算( Name1, j, ref exp, exp1, exp2A );
			VarNode vv = new VarNode();
			vv.SetTempValue( VarType.Void, Deploy.VarLevel, OperIndex );
			
			return VarList.Add( vv );
		}
		else {
			if( SST.ModeC ) {
				exp = exp1 + " " + Oper + " " + exp2;
			}
			if( SST.ModeV ) {
				
				if( Oper == "/" ) {
					Verilog_Div( exp1, exp2 );
					exp = "cx_DIV_Quotient_r";
				}
				else {
					exp = exp1 + " " + Oper + " " + exp2;
				}
			}
		}
		
		return j;
	}
	
	public static int 双目解析运算( string Oper, int Var1, int Var2, ref string exp1, ref string exp2 )
	{
		if( Var1 == -1 || Var2 == -1 ) {
			return -1;
		}
		Var1 = 读取方法( Var1, ref exp1, "error" );
		Var2 = 读取方法( Var2, ref exp2, "error" );
		if( Var1 == -1 || Var2 == -1 ) {
			return -1;
		}
		string Type1 = VarList.Get( Var1 ).Type;
		string Type2 = VarList.Get( Var2 ).Type;
		
		if( Type1.IndexOf( ',' ) != -1 ) {
			Type1 = Operation.GetDoubleVarType( Type1, Type2 );
			VarList.Get( Var1 ).Type = Type1;
		}
		if( Type2.IndexOf( ',' ) != -1 ) {
			if( Oper == "<<" || Oper == ">>" ) {
				Type2 = VarType.BaseType.Uint8;
			}
			else {
				Type2 = Operation.GetDoubleVarType( Type2, Type1 );
			}
			VarList.Get( Var2 ).Type = Type2;
		}
		//获取当前运算的转换方式
		string How = Operation.GetSwitchType( Oper, Type1, Type2 );
		string exp_t = null;
		if( How == "left" ) {
			Var2 = Expression.隐式转换( Type1, Var2, Type2, ref exp_t );
			if( Var2 == -1 ) {
				return -1;
			}
			Type2 = Type1;
		}
		else if( How == "right" ) {
			Var1 = Expression.隐式转换( Type2, Var1, Type1, ref exp_t );
			if( Var1 == -1 ) {
				return -1;
			}
			Type1 = Type2;
		}
		string ReturnType = Operation.GetDoubleReturnType( Oper, Type1, Type2 );
		if( ReturnType == null ) {
			ET.WriteParseError( VarList.Get( Var2 ).Location, "<双目解析运算> 运算的操作数类型不合适: " + Type1 + " " + Oper + " " + Type2 );
			return -1;
		}
		VarNode vv = new VarNode();
		vv.SetTempValue( ReturnType, Deploy.VarLevel, VarList.Get( Var2 ).Location );
		int ReturnIndex = VarList.Add( vv );
		AddIns( Oper + " " + ReturnIndex + " " + Var1 + " " + Var2 );
		
		return ReturnIndex;
	}
	
	public static void 赋值运算( int Var1, int Var2, ref string exp, string exp1, string exp2 )
	{
		//错误拦截
		if( Var1 == -1 || Var2 == -1 ) {
			return;
		}
		Var2 = 读取方法( Var2, ref exp2, exp2 );
		if( Var2 == -1 ) {
			return;
		}
		exp = exp1 + " = " + exp2;
		
		string Type1 = VarList.Get( Var1 ).Type;
		string Type2 = VarList.Get( Var2 ).Type;
		
		//如果是虚拟类型, 执行写入操作返回
		string Address = VarList.GetAddr( Var1 );
		if( Address != null && Address.StartsWith( "#macro " ) ) {
			
			//处理常量类型自动识别
			if( Type2.IndexOf( ',' ) != -1 ) {
				Type2 = Operation.GetDoubleVarType( Type2, Type1 );
				VarList.Get( Var2 ).Type = Type2;
			}
			//判断是否可以写入
			if( Type1 != Type2 ) {
				ET.WriteParseError( VarList.Get( Var1 ).Location, "<赋值运算> 无法写入虚拟变量,类型不同 " + Type1 + " <- " + Type2 );
				return;
			}
			string[] MacroAddrCut = Address.Split( ' ' );
			
			NeedDisableInt = true;
			//AddIns( Code1.禁用中断 );
			AddIns( Code1.写入分量 + " " + MacroAddrCut[ 1 ] + " " + MacroAddrCut[ 1 ] + " " + Var2 + " #" + MacroAddrCut[ 2 ] );
			NeedDisableInt = false;
			//AddIns( Code1.启用中断 );
			Var1 = int.Parse( MacroAddrCut[ 1 ] );
			string TypeO1 = VarList.Get( Var1 ).Type;
			exp1 = VarList.GetFinalName( Var1 );
			if( VarList.Get( Var1 ).ActiveType == MemberType.StoreType.Local ) {
				exp1 = SST.PRE_l + exp1;
			}
			
			string BIT1 = null;
			string BIT0 = null;
			long MAX = 0;
			int loc = int.Parse( MacroAddrCut[ 2 ] );
			
			if( TypeO1 == VarType.BaseType.Uint8 ) {
				MAX = 0xFF;
			}
			if( TypeO1 == VarType.BaseType.Uint16 ) {
				MAX = 0xFFFF;
			}
			if( TypeO1 == VarType.BaseType.Uint32 ) {
				MAX = 0xFFFFFFFF;
			}
			long sft = 0;
			if( Type1 == VarType.BaseType.Bit ) {
				sft = 0x01;
			}
			if( Type1 == VarType.BaseType.Uint8 ) {
				sft = 0xFF;
			}
			if( Type1 == VarType.BaseType.Uint16 ) {
				sft = 0xFFFF;
			}
			if( Type1 == VarType.BaseType.Uint32 ) {
				sft = 0xFFFFFFFF;
			}
			
			BIT1 = (sft << loc).ToString();
			BIT0 = (MAX - (sft << loc)).ToString();
			
			if( SST.ModeC ) {
				if( VarList.Get( Var2 ).ActiveType == "temp" && VarList.Get( Var2 ).isConst ) {
					if( VarList.Get( Var2 ).Value != "0" ) {
						exp = SST.GetFullName( exp1 ) + " |= " + BIT1;
					}
					else {
						exp = SST.GetFullName( exp1 ) + " &= " + BIT0;
					}
				}
				else {
					if( Type1 == VarType.BaseType.Bit ) {
						exp = "(" + exp2 + ") != 0 ? (";
						exp += SST.GetFullName( exp1 ) + " |= " + BIT1 + "): (";
						exp += SST.GetFullName( exp1 ) + " &= " + BIT0 + ")";
					}
					else {
						exp = SST.GetFullName( exp1 ) + " |= " + "(" + SST.PRE_t + TypeO1 + ")(" + exp2 + ") << " + loc + "; ";
						exp += SST.GetFullName( exp1 ) + " &= ((" + SST.PRE_t + TypeO1 + ")(" + exp2 + ") << " + loc + ") | " + BIT0;
					}
				}
			}
			if( SST.ModeV ) {
				
				if( n_FlowSence.FlowSence.isPara ) {
					exp = SST.GetFullName( exp1 ) + "[" + loc + ":" + loc + "] <= " + VarList.Get( Var2 ).Value + "; ";
				}
				else {
					exp = V_Add( SST.GetFullName( exp1 ) + "[" + loc + ":" + loc + "] <= " + VarList.Get( Var2 ).Value + ";" );
					Deploy.V_PC += 1;
				}
				
				if( SST.ModeV && n_Param.Param.isVon ) {
					AddIns( Code1.域返回 + " " + Deploy.Line );
				}
			}
			//exp += "   [" + Type1 + "," + TypeO1 + "]";
			return;
		}
		
		//如果是普通赋值运算, 执行返回
		if( VarType.isBase( Type1 ) ) {
			
			if( VarList.Get( Var1 ).isNDK == 1 ) {
				if( Type1.IndexOf( ',' ) != -1 ) {
					VarList.Get( Var1 ).Type = VarType.BaseType.Sint8;
				}
			}
			else {
				//常量类型规范化
				if( Type2.IndexOf( ',' ) != -1 ) {
					Type2 = Operation.GetDoubleVarType( Type2, Type1 );
					VarList.Get( Var2 ).Type = Type2;
				}
				if( Type1 != Type2 ) {
					Var2 = Expression.隐式转换( Type1, Var2, Type2, ref exp2 );
					if( Var2 == -1 ) {
						return;
					}
				}
				AddIns( "= " + Var1 + " " + Var2 );
				if( SST.ModeV && n_Param.Param.isVon ) {
					AddIns( Code1.域返回 + " " + Deploy.Line );
				}
			}
			
			
			
//			if( VarList.Get( Var2 ).ActiveType != "temp" || Config.OptimizeFlag == 0 ) {
//				AddIns( "= " + Var1 + " " + Var2 );
//				return;
//			}
//			//进行代码优化, 这段代码是优化器核心
//			//AddIns( "= " + Var1 + " " + Var2 );
//			string[] c = FunctionCodeList.LastIns.Split( ' ' );
//			c[ 1 ] = Var1.ToString();
//			FunctionCodeList.LastIns = string.Join( " ", c );
			
			if( SST.ModeC ) {
				exp = exp1 + " = " + exp2;
			}
			if( SST.ModeV ) {
				if( n_FlowSence.FlowSence.isPara ) {
					exp = exp1 + " <= " + exp2 + "; ";
				}
				else {
					//exp = "\t" + Deploy.V_PC + ": begin " + exp1 + " <= " + exp2 + "; " + Deploy.V_NamePC() + " <= " + (Deploy.V_PC+1) + "; end//";
					
					/*
					if( exp2.IndexOf( "DIV_Quotient" ) == -1 && exp2.IndexOf( "DIV_Remain" ) == -1 ) {
						exp = "\t" + Deploy.V_PC + ": begin " + exp1 + " <= " + exp2 + "; " + Deploy.V_NamePC() + " <= " + (Deploy.V_PC+1) + "; end//";
					}
					else {
						exp = "\t" + Deploy.V_PC + ": if( cx_core1_DIV_Done != 0 ) begin " + exp1 + " <= " + exp2 + "; " + Deploy.V_NamePC() + " <= " + (Deploy.V_PC+1) + "; end" +
							" else PC0 <= " + Deploy.V_PC + "; //";
					}
					*/
					
					if( VarList.Get( Var1 ).isVerilogMult ) {
						exp = V_Add( "begin " + exp1 + "_t" + Deploy.CoreIndex + " <= " + exp2 + "; " + exp1 + "_flg" + Deploy.CoreIndex + " <= 1; end" );
						Deploy.V_PC += 1;
						exp += V_Add( "if(" + exp1 + "_ack" + Deploy.CoreIndex + " != 1) " + Deploy.V_NamePC() + " <= " + (Deploy.V_PC) + "; else " + exp1 + "_flg" + Deploy.CoreIndex + " <= 0;" );
						Deploy.V_PC += 1;
						
						if( VarList.Get( Var1 ).VisitCoreList.IndexOf( " " + Deploy.CoreIndex + " " ) == -1 ) {
							VarList.Get( Var1 ).VisitCoreList += Deploy.CoreIndex + " ";
						}
					}
					else {
						exp = V_Add( exp1 + " <= " + exp2 + ";" );
						Deploy.V_PC += 1;
					}
				}
			}
			
			return;
		}
		else if( VarType.isPointer( Type1 ) && VarType.isPointer( Type2 ) ) {
			AddIns( "= " + Var1 + " " + Var2 );
		}
		else {
			
			if( SST.ModeV ) {
				
				if( n_FlowSence.FlowSence.isPara ) {
					exp = exp1 + " <= " + exp2 + "; ";
				}
				else {
					exp = V_Add( exp1 + " <= " + exp2 + ";" );
					Deploy.V_PC += 1;
				}
			}
			
			写入方法( Var1, Var2, ref exp, exp1, exp2 );
		}
		
	}
	
	public static void 引用运算( int Var1, int Var2, ref string exp, string exp1, string exp2 )
	{
		if( Var1 == -1 || Var2 == -1 ) {
			return;
		}
		int RetIndex = -1;
		
		string VarType2 = VarList.Get( Var2 ).Type;
		if( VarType.isBase( VarType2 ) ) {
			VarNode mv = new VarNode();
			mv.SetTempValue( VarType.AddSysRefer( VarType2 ), Deploy.VarLevel, VarList.Get( Var2 ).Location );
			RetIndex = VarList.Add( mv );
			AddIns( Code1.读取地址 + " " + RetIndex + " #" + Var2 );
		}
		else if( VarType.isSysReferForVdata( VarType2 ) ) {
			RetIndex = Var2;
		}
		else if( VarType.isSysReferForStruct( VarType2 ) || VarType.isSysReferForArray( VarType2 ) ) {
			RetIndex = Var2;
		}
		else if( VarType.isSysReferForRefer( VarType2 ) ) {
			string exp_error = null;
			RetIndex = 读接口( Var2, ref exp_error, "error" );
		}
		else {
			ET.ShowError( "<引用指向> 遇到未处理的引用类型赋值:" + VarType2 );
		}
		//2013.3.15
		
		//应该是这样吧
		string Type1 = VarList.Get( Var1 ).Type;
		if( VarList.Get( Var1 ).ActiveType == "temp" && VarType.isSysReferForRefer( Type1 ) ) {
			string exp_t = null;
			写接口( Var1, RetIndex, ref exp_t, null, null );
		}
		else {
			AddIns( "= " + Var1 + " " + RetIndex );
		}
		exp = exp1 + " = &(" + exp2 + ")";
		
		//这里好像不对吧???
//		string Type1 = VarList.Get( Var1 ).Type;
//		if( VarType.isSysReferForRefer( Type1 ) ) {
//			写接口( Var1, RetIndex );
//		}
//		else {
//			AddIns( "= " + Var1 + " " + RetIndex );
//		}
	}
	
	public static void 后置单目语句( int Index )
	{
		int ExpIndex = int.Parse( ParseNet.NodeSet[ Index ][ 1 ] );
		int OperIndex = int.Parse( ParseNet.NodeSet[ Index ][ 2 ] );
		
		string Oper = WordList.GetWord( OperIndex );
		
		string exp = null;
		int VarIndex = Expression.表达式( ExpIndex, ref exp );
		if( VarIndex == -1 ) {
			return;
		}
		VarIndex = 读取宏变量( VarIndex, ref exp );
		if( VarIndex == -1 ) {
			return;
		}
		//进行通常的操作
		VarIndex = 读取方法( VarIndex, ref exp, "error" );
		if( VarIndex == -1 ) {
			return;
		}
		string Rtype = VarList.Get( VarIndex ).Type;
		string Name = VarList.Get( VarIndex ).Name;
		
		//获取普通单目运算的返回类型
		string ReturnType = Operation.GetSingleReturnType( Oper, Rtype );
		if( ReturnType == null ) {
			ET.WriteParseError( VarList.Get( VarIndex ).Location, "<后置单目运算 - 普通运算> 运算的操作数类型不合适: " + Rtype + " " + Oper );
			return;
		}
		
		VarNode v = new VarNode();
		v.SetTempValue( ReturnType, Deploy.VarLevel, OperIndex );
		v.IgnoreWriteError = true;
		int i = VarList.Add( v );
		
		AddIns( Oper + " " + i + " " + VarIndex );
		
		SST.Add( exp + Oper + ";\n" );
		
		string exp_sub1 = null;
		赋值运算( VarIndex, i, ref exp_sub1, null, null );
		VarList.Get( VarIndex ).IsComp = true;
	}
	
	static int 单目运算( int Index, ref string exp )
	{
		++Deploy.ExpLevel;
		
		//如果是复合运算,就拆成普通运算,注意这里等于2,而不等于1,和双目运算不同
		bool isComp = false;
		if( Deploy.ExpLevel == 3 ) {
			//isComp = true;
		}
		//获取运算符
		int OperIndex = int.Parse( ParseNet.NodeSet[ Index ][ 1 ] );
		string Oper;
		bool isForeSign = false;
		if( ParseNet.NodeSet[ OperIndex ][ 0 ] == ParseNet.Node.前括号运算 ) {
			int OperTypeIndex = int.Parse( ParseNet.NodeSet[ OperIndex ][ 2 ] );
			if( ParseNet.NodeSet[ OperTypeIndex ][ 0 ] == ParseNet.Node.基本变量类型 ) {
				string OperType = Parse.GetType( Deploy.UnitName, OperTypeIndex );
				if( OperType == null ) {
					return -1;
				}
				Oper = "(" + OperType + ")";
			}
			else {
				Oper = "(" + WordList.GetWord( OperTypeIndex ) + ")";
				isForeSign = true;
			}
			OperIndex = OperTypeIndex;
		}
		else {
			Oper = WordList.GetWord( OperIndex );
		}
		//解析右操作数
		int Name = 表达式( int.Parse( ParseNet.NodeSet[ Index ][ 2 ] ), ref exp );
		if( Name == -1 ) {
			return -1;
		}
		Name = 读取宏变量( Name, ref exp );
		if( Name == -1 ) {
			return -1;
		}
		//进行通常的操作
		Name = 读取方法( Name, ref exp, "error" );
		if( Name == -1 ) {
			return -1;
		}
		//获取类型并判断是否强制类型转换的常量
		string Type1 = VarList.Get( Name ).Type;
		if( Type1.IndexOf( ',' ) != -1 ) {
			
			Type1 = Operation.GetSingleVarType( Oper, Type1 );
			VarList.Get( Name ).Type = Type1;
			
			//这里是后来补上的
			if( isForeSign ) {
				string ReturnType0 = Operation.GetSingleReturnType( Oper, Type1 );
				if( ReturnType0 == null ) {
					ET.WriteParseError( VarList.Get( Name ).Location, "<单目运算 - Unsigned/Signed转换> 运算的操作数类型不合适: " + Oper + " " + Type1 );
					return -1;
				}
				VarList.Get( Name ).Type = ReturnType0;
				
				if( Oper.StartsWith( "(" ) ) {
					exp = Oper.Replace( "(", "(" + SST.PRE_t ) + exp;
				}
				else {
					exp = Oper + exp;
				}
				
				return Name;
			}
		}
		//获取普通单目运算的返回类型
		string ReturnType = Operation.GetSingleReturnType( Oper, Type1 );
		if( ReturnType == null ) {
			ET.WriteParseError( VarList.Get( Name ).Location, "<单目运算 - 普通运算> 运算的操作数类型不合适: " + Oper + " " + Type1 );
			return -1;
		}
		VarNode v = new VarNode();
		v.SetTempValue( ReturnType, Deploy.VarLevel, OperIndex );
		v.IgnoreWriteError = true;
		int i = VarList.Add( v );
		
		//如果是指针运算, 生成指针类型变量
		if( Oper == "*" ) {
			AddIns( "= " + i + " " + Name );
		}
		//如果是引用(取地址), 生成一个取地址运算
		else if( Oper == "&" ) {
			string exp_error1 = null;
			引用运算( i, Name, ref exp_error1, exp_error1, exp_error1 );
		}
		else {
			AddIns( Oper + " " + i + " " + Name );
		}
		
		if( ReturnType == VarType.BaseType.Bit && Oper == "~" ) {
			exp = "(" + exp + "^1)";
		}
		else {
			int num = VarType.GetBitSize( Type1 );
			
			if( SST.ModeC ) {
				if( Oper == "(uint)" ) {
					exp = "(" + SST.PRE_t + "uint" + num + ")" + exp;
				}
				else if( Oper == "(int)" ) {
					exp = "(" + SST.PRE_t + "int" + num + ")" + exp;
				}
				else {
					if( Oper.StartsWith( "(" ) ) {
						exp = Oper.Replace( "(", "(" + SST.PRE_t ) + exp;
					}
					else {
						exp = Oper + exp;
					}
				}
			}
			if( SST.ModeV ) {
				if( Oper.StartsWith( "(" ) ) {
					//exp = Oper.Replace( "(", "(" + SST.PRE_t ) + exp;
					if( Oper == "(uint)" ) {
						exp = "$unsigned(" + exp + ")";
					}
					if( Oper == "(int)" ) {
						exp = "$signed(" + exp + ")";
					}
				}
				else {
					exp = Oper + exp;
				}
			}
		}
		if( Oper == "++" || Oper == "--" ) {
			isComp= true;
		}
		//判断是否复合运算
		if( isComp ) {
			string exp_sub1 = null;
			赋值运算( Name, i, ref exp_sub1, null, null );
			VarList.Get( Name ).IsComp = true;
			VarNode vv = new VarNode();
			vv.SetTempValue( VarType.Void, Deploy.VarLevel, OperIndex );
			return VarList.Add( vv );
		}
		return i;
	}
	
	static int 新变量( int Index, ref string exp )
	{
		++Deploy.ExpLevel;
		
		//获取变量类型
		string Type = Parse.GetType( Deploy.UnitName, int.Parse( ParseNet.NodeSet[ Index ][ 1 ] ) );
		if( Type == null ) {
			return -1;
		}
		string StoreType = VarType.GetStoreType( Type );
		int VdataIndex = VdataList.GetIndex( StoreType );
		if( VdataIndex == -1 ) {
			ET.WriteParseError( 0, "处理变量定义语句时发现未定义的虚拟数据类型:" + StoreType );
			return -1;
		}
		//获取变量名
		int NameIndex = int.Parse( ParseNet.NodeSet[ Index ][ 2 ] );
		string Name = WordList.GetWord( NameIndex );
		
		if( !VarList.CanDefineLocal( Deploy.UnitName, Deploy.FunctionIndex, Name ) ) {
			ET.WriteParseError( NameIndex, "已经定义了同名的变量: " + Name );
			return -1;
		}
		
		VarNode v = new VarNode();
		
		string sst_Name = SST.PRE_l + Name;
		bool isTask = n_FunctionList.FunctionList.Get( Deploy.FunctionIndex ).InterType == MemberType.FunctionType.Task;
		if( isTask ) {
			Name = "#._cx_fv" + Deploy.FunctionIndex + "_" + Name;
			sst_Name = Name;
		}
		
		v.SetLocalValue( Name, Type, Deploy.VarLevel, Deploy.FunctionIndex, NameIndex, Deploy.ChipIndex );
		int VarIndex = VarList.Add( v );
		
		if( isTask ) {
			v.ActiveType = MemberType.StoreType.Static;
		}
		
		if( VarType.isRefer( Type ) ) {
			
			
			string mType = VarType.GetSourceTypeFromRefer( Type );
			string OperType = VarType.GetTypeFromVdata( mType );
			
			if( isTask ) {
				SST.AddVar( SST.PRE_t + OperType + " *" + SST.GetFullName( sst_Name ) + ";\n" );
				exp = SST.GetFullName( sst_Name );
			}
			else {
				if( SST.Mode_C90 ) {
					SST.AddFuncPre( SST.PRE_t + OperType + " *" + SST.GetFullName( sst_Name ) + ";\n" );
					exp = SST.GetFullName( sst_Name );
				}
				else {
					exp = SST.PRE_t + OperType + " *" + SST.GetFullName( sst_Name );
				}
			}
			
			
			/*
			if( isTask ) {
				SST.AddVar( SST.PRE_t + VarType.BaseType.Uint32 + " " + SST.GetFullName( sst_Name ) + ";\n" );
				exp = SST.GetFullName( sst_Name );
			}
			else {
				if( SST.Mode_C90 ) {
					SST.AddFuncPre( SST.PRE_t + VarType.BaseType.Uint32 + " " + SST.GetFullName( sst_Name ) + ";\n" );
					exp = SST.GetFullName( sst_Name );
				}
				else {
					exp = SST.PRE_t + VarType.BaseType.Uint32 + " " + SST.GetFullName( sst_Name );
				}
			}
			*/
		}
		else if( VarType.isArray( Type ) ) {
			int an = VarType.GetArrayNumber( Type );
			string subType = VarType.GetArrayMemberType( Type );
			
			if( VarType.StoreTypeisVbase( subType ) ) {
				subType = VarType.GetTypeFromVdata( subType );
			}
			if( subType == VarType.BaseType.Function ) {
				Type = Type.Remove( Type.Length - VarType.BaseType.Function.Length ) + VarType.BaseType.Uint16;
				
				if( isTask ) {
					SST.AddVar( "void (*" + SST.GetFullName( sst_Name ) + "[" + an + "])(void);\n" );
					exp = SST.GetFullName( sst_Name );
				}
				else {
					if( SST.Mode_C90 ) {
						SST.AddFuncPre( "void (*" + SST.GetFullName( sst_Name ) + "[" + an + "])(void);\n" );
						exp = ( SST.GetFullName( sst_Name ) );
					}
					else {
						exp = ( "void (*" + SST.GetFullName( sst_Name ) + "[" + an + "])(void);\n" );
					}
				}
			}
			else {
				
				if( isTask ) {
					SST.AddVar( SST.PRE_t + subType + " " + SST.GetFullName( sst_Name ) + "[" + an + "];\n" );
					exp = SST.GetFullName( sst_Name );
				}
				else {
					if( SST.Mode_C90 ) {
						SST.AddFuncPre( SST.PRE_t + subType + " " + SST.GetFullName( sst_Name ) + "[" + an + "];\n" );
						exp = ( SST.GetFullName( sst_Name ) );
					}
					else {
						exp = ( SST.PRE_t + subType + " " + SST.GetFullName( sst_Name ) + "[" + an + "]" );
					}
				}
				
			}
		}
		else {
			if( isTask ) {
				if( SST.ModeC ) {
					SST.AddVar( SST.PRE_t + Type + " " + SST.GetFullName( sst_Name ) + ";\n" );
				}
				if( SST.ModeV ) {
					RecordMacro.AddV_VarDef( Type, sst_Name );
				}
				exp = SST.GetFullName( sst_Name );
			}
			else {
				if( SST.Mode_C90 ) {
					SST.AddFuncPre( SST.PRE_t + Type + " " + SST.GetFullName( sst_Name ) + ";\n" );
					exp = SST.GetFullName( sst_Name );
				}
				else {
					exp = SST.PRE_t + Type + " " + SST.GetFullName( sst_Name );
				}
			}
		}
		
		//这里说明是单独的变量定义语句
		if( Deploy.ExpLevel == 3 ) {
			
			if( SST.Mode_C90 ) {
				exp = "";
			}
			
			AddIns( Code1.定义变量 + " # " + VarIndex );
			VarNode vv = new VarNode();
			vv.SetTempValue( VarType.Void, Deploy.VarLevel, NameIndex );
			
			return VarList.Add( vv );
		}
		string ttt = null;
		int ret = 创建引用( VarIndex, ref ttt );
		
		//exp = Type + " " + exp;
		
		return ret;
	}
	
	static int 带宏变量( int Index, ref string exp )
	{
		++Deploy.ExpLevel;
		
		int AIndex = int.Parse( ParseNet.NodeSet[ Index ][ 1 ] );
		int NIndex = int.Parse( ParseNet.NodeSet[ Index ][ 2 ] );
		int MacroIndex = int.Parse( ParseNet.NodeSet[ Index ][ 3 ] );
		string Name = WordList.GetWord( NIndex );
		if( MacroIndex != -1 ) {
			Name += Parse.GetMacroMes( MacroIndex );
		}
		
		//解析局部或者全局变量
		return LocalOrStaticl( Name, NIndex, AIndex, ref exp );
	}
	
	static int 变量( int Index, ref string exp )
	{
		++Deploy.ExpLevel;
		
		int AIndex = int.Parse( ParseNet.NodeSet[ Index ][ 1 ] );
		int FirstIndex = int.Parse( ParseNet.NodeSet[ Index ][ 2 ] );
		int NextIndex = int.Parse( ParseNet.NodeSet[ Index ][ 3 ] );
		string[] Cut = ParseNet.NodeSet[ NextIndex ];
		int CurrentIndex = 1;
		int DataIndex = 0;
		
		//迭代处理元件嵌套引用
		string NextUnitName = null;
		string FirstNameWord = WordList.GetWord( FirstIndex );
		
		if( AIndex != -1 ) {
			NextUnitName = VarType.Root + "." + FirstNameWord;
		}
		else {
			NextUnitName = Deploy.UnitName + "." + FirstNameWord;
		}
		if( UnitList.GetIndex( NextUnitName ) != -1 ) {
			string UnitName = NextUnitName;
			for( CurrentIndex = 1; CurrentIndex < Cut.Length; ++CurrentIndex ) {
				int ParseIndex = int.Parse( Cut[ CurrentIndex ] );
				if( ParseNet.NodeSet[ ParseIndex ][ 0 ] != ParseNet.Node.点运算 ||
				   ParseNet.NodeSet[ ParseIndex ][ 3 ] != "-1" ) {
					ET.WriteParseError( FirstIndex, "<变量> 元件类型名进行点运算不允许有后缀修饰" );
					return -1;
				}
				string Name = 获取表达式中的单名称( int.Parse( ParseNet.NodeSet[ ParseIndex ][ 2 ] ) );
				NextUnitName += "." + Name;
				if( UnitList.GetIndex( NextUnitName ) == -1 ) {
					break;
				}
				UnitName = NextUnitName;
			}
			if( Cut.Length <= CurrentIndex ) {
				ET.WriteParseError( FirstIndex, "<变量> 变量名和元件名相同,引用错误" );
				return -1;
			}
			int VarIndex = int.Parse( Cut[ CurrentIndex ] );
			VarIndex = int.Parse( ParseNet.NodeSet[ VarIndex ][ 2 ] );
			++CurrentIndex;
			DataIndex = 元件引用( UnitName, VarIndex, ref exp );
			if( DataIndex == -1 ) {
				return -1;
			}
		}
		else {
			//判断是否为NDK交互
			if( FirstNameWord == "@" ) {
				int ParseIndex = int.Parse( Cut[ 1 ] );
				if( ParseNet.NodeSet[ ParseIndex ][ 0 ] != ParseNet.Node.点运算 ) {
					ET.WriteParseError( FirstIndex, "<变量> 访问本地C变量需要<.>运算" );
					return -1;
				}
				if( Cut.Length != 2 ) {
					ET.WriteParseError( FirstIndex, "<变量> 访问本地C变量格式为: @.Name" );
					return -1;
				}
				int CN_Index = int.Parse( ParseNet.NodeSet[ ParseIndex ][ 2 ] );
				string CName = WordList.GetWord( CN_Index );
				
				string Type = null;
				string Number = ConstString.GetValue( ref Type, "0", FirstIndex );
				if( Type == null ) {
					return -1;
				}
				VarNode v = new VarNode();
				v.SetTempValue( Type + ",bool", Deploy.VarLevel, FirstIndex );
				v.isNDK = 1;
				v.isConst = true;
				v.Value = Number;
				int i = VarList.Add( v );
				AddIns( Code1.常量赋值 + " " + i + " #" + Number );
				
				exp = CName;
				
				return i;
			}
			DataIndex = 终结词( FirstIndex, AIndex, ref exp );
			if( DataIndex == -1 ) {
				return -1;
			}
		}
		
		//迭代处理后缀
		for( int i = CurrentIndex; i < Cut.Length; ++i ) {
			
			if( DataIndex == -1 ) {
				return -1;
			}
			int ParseIndex = int.Parse( Cut[ i ] );
			
			//如果需要改回统一的点运算,包括数组下标运算,只需要去掉else语句,并且在 <分量运算> 中使能下标
			if( ParseNet.NodeSet[ ParseIndex ][ 0 ] == ParseNet.Node.点运算 ) {
				DataIndex = 分量运算( DataIndex, ParseIndex );
			}
			else {
				DataIndex = 数组分量运算( DataIndex, ParseIndex, ref exp );
			}
			if( DataIndex == -1 ) {
				return -1;
			}
		}
		return DataIndex;
	}
	
	static int 元件引用( string BaseUnitName, int VarIndex, ref string exp )
	{
		++Deploy.ExpLevel;
		
		string VarName = BaseUnitName + "." + 获取表达式中的单名称( VarIndex );
		
		//如果是静态变量, 再判断是否为常量
		int j = VarList.GetStaticIndex( VarName );
		if( j != -1 ) {
			
			exp = VarList.Get( j ).Name;
			exp = SST.GetFullName( exp );
			
			if( VarList.Get( j ).isConst ) {
				string ConstType = VarList.Get( j ).Type;
				VarNode v = new VarNode();
				v.SetTempValue( ConstType, Deploy.VarLevel, VarIndex );
				int i = VarList.Add( v );
				string ConstValue = ConstExpression.GetRealValue( VarList.Get( j ).Value, VarList.Get( j ).Type );
				if( ConstValue == null ) {
					ET.WriteParseError( VarList.Get( j ).Location, "常量数值超出范围: " + VarList.Get( j ).Value );
				}
				else {
					Expression.AddIns( Code1.常量赋值 + " " + i + " #" + ConstValue );
				}
				
				exp = ConstValue;
				
				return i;
			}
			//这里设置新的位置(当前使用点)
			VarList.Get( j ).Location = VarIndex;
			
			return 创建引用( j, ref exp );
		}
		ET.WriteParseError( VarIndex, "<元件引用> 未知操作数: " + BaseUnitName + " : " + VarName );
		return -1;
	}
	
	static int 终结词( int Index, int AIndex, ref string exp )
	{
		++Deploy.ExpLevel;
		
		string Name = WordList.GetWord( Index );
		
		//判断字符常量或数字
		if( ConstString.isValue( Name ) ) {
			string Type = null;
			string Number = ConstString.GetValue( ref Type, Name, Index );
			if( Type == null ) {
				return -1;
			}
			VarNode v = new VarNode();
			v.SetTempValue( Type, Deploy.VarLevel, Index );
			v.isConst = true;
			v.Value = Number;
			int i = VarList.Add( v );
			AddIns( Code1.常量赋值 + " " + i + " #" + Number );
			
			exp = Number;
			
			return i;
		}
		//判断字符串
		if( Name.StartsWith( "\"" ) ) { //|| Name.StartsWith( "@" )
			
			//exp = Name;
			exp = "(" + SST.PRE_t + "int8*)((" + SST.PRE_t + "uint32)" + Name + " + 0x10000)";
			
			return 字符串( Name, Index );
		}
		//解析局部或者全局变量
		return LocalOrStaticl( Name, Index, AIndex, ref exp );
	}
	
	static int LocalOrStaticl( string Name, int Index, int AIndex, ref string exp )
	{
		//判断是否为局部变量
		int j = VarList.GetLocalIndex( Deploy.FunctionIndex, Name );
		if( j == -1 ) {
			//判断是否为静态变量
			string FullName = "";
			if( AIndex != -1 ) {
				FullName = VarType.Root + "." + Name;
			}
			else {
				FullName = Deploy.UnitName + "." + Name;
			}
			j = VarList.GetStaticIndex( FullName );
			if( j == -1 ) {
				ET.WriteParseError( Index, "<终结词> 未知操作数: " + FullName + " (" + Name + ")" );
				return -1;
			}
			
			exp = VarList.Get( j ).Name;
			exp = SST.GetFullName( exp );
			
			if( VarList.Get( j ).isConst ) {
				string ConstType = VarList.Get( j ).Type;
				VarNode v = new VarNode();
				v.SetTempValue( ConstType, Deploy.VarLevel, Index );
				int i = VarList.Add( v );
				string ConstValue = ConstExpression.GetRealValue( VarList.Get( j ).Value, VarList.Get( j ).Type );
				if( ConstValue == null ) {
					ET.WriteParseError( VarList.Get( j ).Location, "<终结词> 常量数值超出范围: " + VarList.Get( j ).Value );
				}
				else {
					Expression.AddIns( Code1.常量赋值 + " " + i + " #" + ConstValue );
				}
				
				exp = SST.GetFullName( ConstValue );
				
				return i;
			}
		}
		else {
			exp = SST.GetFullName( Name );
		}
		
		//这里设置新的位置(当前使用点)
		VarList.Get( j ).Location = Index;
		
		//判断变量是否存在
		if( !VarList.Get( j ).IsExist ) {
			ET.WriteParseError( Index, "<终结词> 使用了无效的变量: " + Name );
			return -1;
		}
		
		int ri = 创建引用( j, ref exp );
		
		int nidx = exp.IndexOf( "@" );
		if( nidx != -1 ) {
			exp = exp.Remove( 0, nidx + 1 );
		}
		return ri;
	}
	
	static int 函数地址( int Index, ref string exp )
	{
		++Deploy.ExpLevel;
		
		//获取函数名
		int FunctionNameIndex = int.Parse( ParseNet.NodeSet[ Index ][ 2 ] );
		string FunctionName = null;
		int ErrorIndex = -1;
		Parse.GetMemberNames( Deploy.UnitName, FunctionNameIndex, ref FunctionName, ref ErrorIndex );
		
		//获得调用的函数索引
		int CallFunctionIndex = FunctionList.GetIndex( FunctionName );
		if( CallFunctionIndex != -1 ) {
			
			//设置函数调用标志为真
			FunctionList.Get( CallFunctionIndex ).isUsed = true;
			
			//函数指针
			string FuncPointype = Config.GetFunctionPointType();
			VarNode vv = new VarNode();
			vv.SetTempValue( FuncPointype, Deploy.VarLevel, ErrorIndex );
			int ii = VarList.Add( vv );
			Expression.AddIns( Code1.函数地址 + " " + ii + " #" + Config.PreLabel.Function + CallFunctionIndex );
			
			exp = "&" + SST.GetFullName( FunctionList.Get( CallFunctionIndex ).FunctionName );
			
			return ii;
		}
		//2023.3.1
		int varindex = VarList.GetStaticIndex( FunctionName );
		if( varindex != -1 ) {
			VarNode vv = new VarNode();
			vv.SetTempValue( VarType.BaseType.Uint32, Deploy.VarLevel, ErrorIndex );
			int ii = VarList.Add( vv );
			Expression.AddIns( Code1.常量赋值 + " " + ii + " #" + Config.PreLabel.VarAddr + varindex );
			return ii;
		}
		//标签指针
		string FuncPointype1 = Config.GetFunctionPointType();
		VarNode vv1 = new VarNode();
		vv1.SetTempValue( FuncPointype1, Deploy.VarLevel, ErrorIndex );
		int ii1 = VarList.Add( vv1 );
		string Label = FunctionName.Remove( 0, 1 + FunctionName.LastIndexOf( "." ) );
		Expression.AddIns( Code1.标签地址 + " " + ii1 + " #" + Config.PreLabel.Label + Deploy.FunctionIndex + "_" + Label );
		return ii1;
	}
	
	static int 函数变量尺寸( int Index )
	{
		++Deploy.ExpLevel;
		
		//获取函数名
		int FunctionNameIndex = int.Parse( ParseNet.NodeSet[ Index ][ 2 ] );
		string FunctionName = null;
		int ErrorIndex = -1;
		Parse.GetMemberNames( Deploy.UnitName, FunctionNameIndex, ref FunctionName, ref ErrorIndex );
		
		//获得调用的函数索引
		int CallFunctionIndex = FunctionList.GetIndex( FunctionName );
		if( CallFunctionIndex == -1 ) {
			ET.WriteParseError( ErrorIndex, "<函数变量尺寸> 未定义的函数: " + FunctionName );
			return -1;
		}
		//函数尺寸
		string FuncPointype = Config.GetFunctionSizeType();
		VarNode vv = new VarNode();
		vv.SetTempValue( FuncPointype, Deploy.VarLevel, ErrorIndex );
		int ii = VarList.Add( vv );
		Expression.AddIns( Code1.函数尺寸 + " " + ii + " #" + CallFunctionIndex );
		return ii;
	}
	
	static int 函数调用( int Index, ref string exp )
	{
		++Deploy.ExpLevel;
		
		//获取函数名
		int FunctionNameIndex = int.Parse( ParseNet.NodeSet[ Index ][ 1 ] );
		string FunctionName = null;
		int ErrorIndex = -1;
		Parse.GetMemberNames( Deploy.UnitName, FunctionNameIndex, ref FunctionName, ref ErrorIndex );
		
		//判断是否为NDK原生函数
		bool isCruxFunc = true;
		int idx = FunctionName.IndexOf( "@." );
		if( idx != -1 ) {
			isCruxFunc = false;
			FunctionName = FunctionName.Remove( 0, idx + 2 );
		}
		
		//获取实参列表
		int RealVarListIndex = int.Parse( ParseNet.NodeSet[ Index ][ 3 ] );
		
		//这里说明是调用C语言函数
		if( !isCruxFunc ) {
			
			exp = FunctionName + "(";
			
			if( RealVarListIndex == -1 ) {
				//没有实参列表
				exp += ")";
			}
			else {
				//处理实参列表
				string[] RealVarList = ParseNet.NodeSet[ RealVarListIndex ];
				
				//迭代解析实参列表,注意如果改回原先的函数调用方式,需要修改下边的 i - 1 这个表达式,
				//一共要改3个地方,上面还有一段代码也涉及到 除2 或 减1 (RealVarLength)
				for( int i = 1; i < RealVarList.Length; i += 2 ) {
					
					//解析第 i 个实参
					int PointOper = int.Parse( RealVarList[ i ] );
					isPad = true;
					string var_exp = null;
					int j = 表达式( PointOper, ref var_exp );
					
					if( j == -1 ) {
						return -1;
					}
					string exp_m = null;
					j = Expression.读取宏变量( j, ref exp_m );
					if( j == -1 ) {
						return -1;
					}
					//这里随便确定一个最终类型就行吧
					if( VarList.Get( j ).Type.IndexOf( ',' ) != -1 ) {
						VarList.Get( j ).Type = "int8";
					}
					if( VarList.Get( j ).Type == VarType.Void ) {
						ET.WriteParseError( VarList.Get( j ).Location, "<函数调用> 函数实参不能是 void 类型" );
						return -1;
					}
					
					if( i == 1 ) {
						exp += " " + var_exp;
					}
					else {
						exp += ", " + var_exp;
					}
				}
				exp += " )";
			}
			VarNode vf = new VarNode();
			vf.isNDK = 2;
			//这个类型顺序不能乱! Operation.GetDoubleVarType() 会用到
			vf.SetTempValue( "uint8,int8,bit,bool", Deploy.VarLevel, ErrorIndex );
			vf.isConst = true;
			vf.Value = "0";
			int rf = VarList.Add( vf );
			AddIns( Code1.常量赋值 + " " + rf + " #" + "0" );
			return rf;
		}
		
		//获得调用的函数索引
		int CallFunctionIndex = CallFunctionIndex = FunctionList.GetIndex( FunctionName );
		if( CallFunctionIndex == -1 ) {
			
			int varf = VarList.GetStaticIndex( FunctionName );
			if( varf != -1 && VarList.Get( varf ).isFunctionType ) {
				exp = "(*" + SST.GetFullName( FunctionName ) + ")()";
				
				//这里忽略了函数指针的代码, 需要后续完善 2022.7.29
				VarNode vf = new VarNode();
				vf.SetTempValue( VarType.Void, Deploy.VarLevel, ErrorIndex );
				int rf = VarList.Add( vf );
				return rf;
			}
			else {
				ET.WriteParseError( ErrorIndex, "<函数调用> 未定义的函数或函数指针: " + FunctionName + " " + varf );
				return -1;
			}
		}
		//判断是否为类型安全的NC调用
		string sst_name = SST.GetFullName( FunctionList.Get( CallFunctionIndex ).FunctionName );
		int nidx = sst_name.IndexOf( "@" );
		if( nidx != -1 ) {
			sst_name = sst_name.Remove( 0, nidx + 1 );
		}
		
		exp = sst_name + "(";
		string transvar = "";
		bool isTaskCall = FunctionList.Get( CallFunctionIndex ).InterType == MemberType.FunctionType.Task;
		
		//获取函数形参列表
		string FuncVarList = FunctionList.Get( CallFunctionIndex ).VarIndexList;
		
		//形参尺寸变量归零
		int VarListSize = 0;
		if( RealVarListIndex == -1 ) {
			//没有实参列表
			if( FuncVarList != null && FunctionList.Get( CallFunctionIndex ).InterType != MemberType.FunctionType.Task ) {
				ET.WriteParseError( ErrorIndex, "<函数调用> 此函数需要实参: " + FunctionName );
				return -1;
			}
			Expression.AddIns( Code1.堆栈递增 + " #" + Deploy.FunctionIndex );
			
			exp += ")";
		}
		else {
			//处理实参列表
			if( FuncVarList == null ) {
				ET.WriteParseError( ErrorIndex, "<FunctionCall> 此函数未定义实参: " + FunctionName );
				return -1;
			}
			string[] RealVarList = ParseNet.NodeSet[ RealVarListIndex ];
			
			string[] VarCut = FuncVarList.Split( ' ' );
			if( RealVarList.Length / 2 != VarCut.Length ) {
				ET.WriteParseError( ErrorIndex,
				                   "<函数调用> 函数实参数目不合适(" + FunctionName + "): " + RealVarList.Length / 2 + " " + VarCut.Length );
				return -1;
			}
			//实参变量列表
			string RealVar = "";
			
			//迭代解析实参列表,注意如果改回原先的函数调用方式,需要修改下边的 i - 1 这个表达式,
			//一共要改3个地方,上面还有一段代码也涉及到 除2 或 减1 (RealVarLength)
			int FuncVarIndex = 0;
			for( int i = 1; i < RealVarList.Length; i += 2 ) {
				
				//解析第 i 个实参
				isPad = true;
				int PointOper = int.Parse( RealVarList[ i ] );
				string var_exp = null;
				int j = 表达式( PointOper, ref var_exp );
				
				if( j == -1 ) {
					return -1;
				}
				j = Expression.读取宏变量( j, ref var_exp );
				if( j == -1 ) {
					return -1;
				}
				if( VarList.Get( j ).Type == VarType.Void ) {
					ET.WriteParseError( VarList.Get( j ).Location, "<函数调用> 函数实参不能是 void 类型" );
					return -1;
				}
				//这里是让函数调用实参中的所有表达式计算完成之后再进行参数传递,而不是计算一个传递一个;
				//而且堆栈递增后只能允许变量传送操作
				int FuncVar = int.Parse( VarCut[ FuncVarIndex ] );
				string FuncVarType = VarList.Get( FuncVar ).Type;
				VarListSize += VarType.GetSize( FuncVarType );
				
				//判断形参的传递方式
				if( VarType.isBase( FuncVarType ) ) {
					string exp_error = null;
					j = 读取方法( j, ref exp_error, "error" );
						if( j == -1 ) {
						return -1;
					}
					//判断是否为常量, 并设置最终的类型
					string Type2 = VarList.Get( j ).Type;
					if( Type2.IndexOf( ',' ) != -1 ) {
						Type2 = Operation.GetDoubleVarType( Type2, FuncVarType );
						VarList.Get( j ).Type = Type2;
					}
					if( Type2 != FuncVarType ) {
						j = Expression.隐式转换( FuncVarType, j, Type2, ref var_exp );
						if( j == -1 ) {
							return -1;
						}
						Type2 = FuncVarType;
					}
					if( isTaskCall ) {
						if( SST.ModeC ) {
							SST.Add( SST.GetFullName( VarList.Get( FuncVar ).Name ) + " = " + var_exp + ";\n" );
						}
						if( SST.ModeV ) {
							SST.Add( Expression.V_Add( SST.GetFullName( VarList.Get( FuncVar ).Name ) + " <= " + var_exp + ";" ) );
							Deploy.V_PC += 1;
						}
					}
					else {
						if( i == 1 ) {
							transvar += " " + var_exp;
						}
						else {
							transvar += ", " + var_exp;
						}
					}
				}
				else {
					string jType = VarList.Get( j ).Type;
					if( VarType.isBase( jType ) ) {
						VarNode mv = new VarNode();
						mv.SetTempValue( VarType.AddSysRefer( jType ), Deploy.VarLevel, VarList.Get( j ).Location );
						int RetIndex = VarList.Add( mv );
						AddIns( Code1.读取地址 + " " + RetIndex + " #" + j );
						j = RetIndex;
					}
					else if( VarType.isSysReferForRefer( jType ) ) {
						string exp_error = null;
						j = 读接口( j, ref exp_error, "error" );
					}
					if( i == 1 ) {
						transvar += " " + var_exp;
					}
					else {
						transvar += ", " + var_exp;
					}
				}
				//这里好像用来处理静态变量做实参的情况
//				if( VarList.Get( j ).ActiveType == "static" ) {
//					VarNode vv = new VarNode();
//					vv.IgnoreWriteError = true;
//					vv.SetTempValue( VarList.Get( j ).Type, Deploy.VarLevel, VarList.Get( j ).Location );
//					int vIndex = VarList.Add( vv );
//					AddIns( "= " + vIndex + " " + j );
//					j = vIndex;
//				}
				//添加到实参列表
				RealVar += j + " ";
				FuncVarIndex += 1;
			}
			if( !isTaskCall ) {
				exp += transvar + " )";
			}
			else {
				exp += ")";
			}
			Expression.AddIns( Code1.堆栈递增 + " #" + Deploy.FunctionIndex );
			
			//迭代处理实参赋值,依次传递到形参
			//目标代码为PC80X86时反向传递(从右向左) 和masm的传参约定保持兼容
			string[] RealVarSet = RealVar.TrimEnd( ' ' ).Split( ' ' );
			string InterType = FunctionList.Get( CallFunctionIndex ).InterType;
			if( ( InterType == MemberType.FunctionType.Interface || InterType == MemberType.FunctionType.Callback )
				&& Config.GetCPU() == CPUType.PC80X86 ) {
				for( int i = RealVarSet.Length - 1; i >= 0; --i ) {
					Expression.AddIns( Code1.压栈 + " # " + RealVarSet[ i ] );
				}
			}
			else {
				for( int i = 0; i < RealVarSet.Length; ++i ) {
					
					if( Config.GetCPU().StartsWith( CPUType.MCS_X ) ) {
						string[] CallVarList = FunctionList.Get( CallFunctionIndex ).VarIndexList.Split( ' ' );
						AddIns( Code1.传递形参 + " " + CallVarList[i] + " " + RealVarSet[ i ] );
					}
					else if( Config.GetCPU().StartsWith( CPUType.MEGA_X )  || Config.GetCPU() == CPUType.VM ) {
						
						//这里处理用户空间避免存在局部变量的问题
						if( FunctionList.Get( CallFunctionIndex ).UserSpace ) {
							string[] CallVarList = FunctionList.Get( CallFunctionIndex ).VarIndexList.Split( ' ' );
							AddIns( "= " + CallVarList[i] + " " + RealVarSet[ i ] );
						}
						else {
							AddIns( Code1.传递形参 + " # " + RealVarSet[ i ] );
						}
					}
					else {
						ET.WriteLineError( 0, 0, "传递形参遇到未知的芯片类型" );
					}
				}
			}
		}
		
		if( SST.ModeV ) {
			bool F_nop = FunctionList.Get( CallFunctionIndex ).FunctionName.IndexOf( n_SST.Verilog.F_nop ) != -1;
			bool F_delay_clk = FunctionList.Get( CallFunctionIndex ).FunctionName.IndexOf( n_SST.Verilog.F_delay_clk ) != -1;
			bool F_delay_us = FunctionList.Get( CallFunctionIndex ).FunctionName.IndexOf( n_SST.Verilog.F_delay_us ) != -1;
			bool F_delay_ms = FunctionList.Get( CallFunctionIndex ).FunctionName.IndexOf( n_SST.Verilog.F_delay_ms ) != -1;
			if( F_nop ) {
				exp = V_Add( " ;" );
				Deploy.V_PC += 1;
			}
			else if( F_delay_clk || F_delay_us || F_delay_ms ) {
				if( F_delay_us ) {
					int val = int.Parse( transvar );
					val *= SST.FREQ / 1000000;
					transvar = val.ToString();
				}
				if( F_delay_ms ) {
					int val = int.Parse( transvar );
					val *= SST.FREQ / 1000;
					transvar = val.ToString();
				}
				exp = V_Add( Deploy.V_NameTime() + " <= " + transvar + ";" );
				Deploy.V_PC += 1;
				exp += V_Add( "if( " + Deploy.V_NameTime() + " != 0 ) begin " + Deploy.V_NameTime() + " <= " + Deploy.V_NameTime() + " - 1; " + Deploy.V_NamePC() + " <= " + Deploy.V_PC + "; end" );
				Deploy.V_PC += 1;
			}
			else {
				//这里也行, 调试一下稳定性.............
				//exp = "\t" + Deploy.V_PC + ": begin " + Deploy.V_NamePC() + "_Back[" + Deploy.V_NamePC() + "_Back_SP" + "] <= " + (Deploy.V_PC + 1) + "; " + Deploy.V_NamePC() + " <= $CALL" + CallFunctionIndex + "$; " + Deploy.V_NamePC() + "_Back_SP <= " + Deploy.V_NamePC() + "_Back_SP + 1; " + "end//";
				//Deploy.V_PC += 1;
				
				//主调方提升堆栈
				//exp = "\t" + Deploy.V_PC + ": begin " + Deploy.V_NamePC() + "_Back[" + Deploy.V_NamePC() + "_Back_SP" + "] <= " + (Deploy.V_PC + 2) + "; " + Deploy.V_NamePC() + " <= " + (Deploy.V_PC+1) + "; end\n";
				//Deploy.V_PC += 1;
				//exp += "\t" + Deploy.V_PC + ": begin " + Deploy.V_NamePC() + " <= $CALL" + CallFunctionIndex + "$; " + Deploy.V_NamePC() + "_Back_SP <= " + Deploy.V_NamePC() + "_Back_SP + 1; " + "end//";
				//Deploy.V_PC += 1;
				
				//被调方提升堆栈
				//exp = V_Add( "begin " + Deploy.V_NamePC() + "_Back[" + Deploy.V_NamePC() + "_Back_SP" + "] <= " + (Deploy.V_PC + 1) + "; " +
				//      Deploy.V_NamePC() + " <= $CALL" + CallFunctionIndex + "$; end" );
				//Deploy.V_PC += 1;
				
				string RetPC = SST.GetFullName( "#._cx_fv" + CallFunctionIndex + "_Sys_RetPC" );
				exp = V_Add( "begin " + RetPC + " <= " + (Deploy.V_PC + 1) + "; " +
				             Deploy.V_NamePC() + " <= $CALL" + CallFunctionIndex + "$; end" );
				Deploy.V_PC += 1;
			}
		}
		
		//封装函数调用
		string ReturnType = FunctionList.Get( CallFunctionIndex ).ReturnType;
		VarNode v = new VarNode();
		v.SetTempValue( ReturnType, Deploy.VarLevel, ErrorIndex );
		int r = VarList.Add( v );
		
		if( FunctionList.Get( CallFunctionIndex ).ChipIndex != Deploy.ChipIndex ) {
			int ci = FunctionList.Get( CallFunctionIndex ).ChipIndex;
			int TargetFuncIndex = FunctionList.GetIndex( "#.linka" + Deploy.ChipIndex + ".test" );
			int ReturnIType = 0;
			if( ReturnType != VarType.Void ) {
				ReturnIType = VarType.GetSize( ReturnType );
			}
			Expression.AddIns(
				Code1.设置云参数 +
				" #" + ci +
				" #" + CallFunctionIndex +
				" #" + ReturnIType +
				" #" + VarListSize );
			Expression.AddIns( Code1.函数调用 + " " + TargetFuncIndex );
		}
		else {
			Expression.AddIns( Code1.函数调用 + " " + CallFunctionIndex );
		}
		
		CallList.AddSubFunction( Deploy.FunctionIndex, CallFunctionIndex.ToString() );
		
		Expression.AddIns( Code1.堆栈递减 + " #" + Deploy.FunctionIndex );
		if( ReturnType == VarType.Void ) {
			return r;
		}
		Expression.AddIns( Code1.读系统量 + " " + r );
		return r;
	}
	
	static int 字符串( string Strings, int Index )
	{
		string Result = "@code ";
		int Size = 0;
		int CodeType = 0;
		Result += StringCoder.GetCode( Strings, ref Size, ref CodeType, Index );
		
		//设置字符数组地址并返回数组变量索引
		VarNode v = new VarNode();
		string SubType = null;
		if( CodeType == 0 ) {
			SubType = VarType.CreatVdataType( VarType.VCode, VarType.BaseType.Sint8 );
		}
		else if( CodeType == 1 ) {
			SubType = VarType.CreatVdataType( VarType.VCode, VarType.BaseType.Sint8 );
		}
		else {
			SubType = VarType.CreatVdataType( VarType.VCode, VarType.BaseType.Sint16 );
		}
		string vType = VarType.CreatArray( Size, SubType );
		v.SetStaticValue( "", vType, MemberType.VisitType.Private, MemberType.RealRefType.Real, false,
		                 Index, Deploy.ChipIndex, 0 );
		v.Address = Result;
		int r = VarList.Add( v );
		
		VarNode vv = new VarNode();
		vv.SetTempValue( VarType.AddSysRefer( vType ), Deploy.VarLevel, Index );
		int ii = VarList.Add( vv );
		AddIns( Code1.读取地址 + " " + ii + " #" + r );
		return ii;
	}
	
	static int 分量运算( int VarIndex, int Index )
	{
		if( VarIndex == -1 ) {
			return -1;
		}
		string Vtype = VarList.Get( VarIndex ).Type;
		
		//判断是否整型的分量运算
		if( VarType.isBase( Vtype ) ) {
			return 整型分量运算( VarIndex, Index );
		}
		//判断是否结构体的分量运算
		if( VarType.isSysReferForStruct( Vtype ) ) {
			return 组合分量运算( VarIndex, Index );
		}
		//判断是否数组的分量运算
		if( VarType.isSysReferForArray( Vtype ) ) {
			//return 数组属性运算( VarIndex, Index );
		}
		//如果是系统引用,读取这个引用再重新进行一次分量运算
		if( VarType.isSysReferForRefer( Vtype ) ) {
			string exp_error = null;
			int RefIndex = 读接口( VarIndex, ref exp_error, "error" );
			return 分量运算( RefIndex, Index );
		}
		ET.WriteParseError( VarList.Get( VarIndex ).Location, "<分量运算> 未定义的类型: " + Vtype );
		return -1;
	}
	
	static int 数组分量运算( int Var1, int Index, ref string exp )
	{
		if( Var1 == -1 ) {
			return -1;
		}
		string exp_sub = null;
		int Var2 = 表达式( int.Parse( ParseNet.NodeSet[ Index ][ 2 ] ), ref exp_sub );
		if( Var2 == -1 ) {
			return -1;
		}
		string exp_m = null;
		Var2 = 读取宏变量( Var2, ref exp_m );
		if( Var2 == -1 ) {
			return -1;
		}
		string exp_error = null;
		Var1 = 读取方法( Var1, ref exp_error, "error" );
		if( Var1 == -1 ) {
			return -1;
		}
		Var2 = 读取方法( Var2, ref exp_error, "error" );
		if( Var2 == -1 ) {
			return -1;
		}
		string Type1 = VarList.Get( Var1 ).Type;
		string Type2 = VarList.Get( Var2 ).Type;
		
		//如果数组分量下标为常量,则根据数组的地址宽度类型设置常量类型
		string ASMType = VarType.GetReferAddressType( Type1 );
		if( Type2.IndexOf( ',' ) != -1 ) {
			Type2 = Operation.GetDoubleVarType( Type2, ASMType );
			VarList.Get( Var2 ).Type = Type2;
		}
		//这里处理下标为有符号数的情况
		if( VarType.BaseType.isSint( Type2 ) ) {
			Type2 = VarType.BaseType.GetInterfaceType( Type2 );
			VarNode v = new VarNode();
			v.SetTempValue( Type2, Deploy.VarLevel, VarList.Get( Var2 ).Location );
			int nv = VarList.Add( v );
			Expression.AddIns( "(" + VarType.BaseType.Uint + ")" + " " + nv + " " + Var2 );
			Var2 = nv;
		}
		if( ASMType != Type2 ) {
			string exp_t = null;
			Var2 = Expression.隐式转换( ASMType, Var2, Type2, ref exp_t );
			if( Var2 == -1 ) {
				return -1;
			}
		}
		//获取分量的空间大小
		string SubType = VarType.GetArrayMemberTypeFromRefer( Type1 );
		int Length = VarType.GetSize( SubType );
		
		VarNode vv = new VarNode();
		vv.SetTempValue( VarType.AddSysRefer( SubType ), Deploy.VarLevel, VarList.Get( Var1 ).Location );
		int ReturnIndex = VarList.Add( vv );
		
		AddIns( Code1.地址偏移 + " " + ReturnIndex + " " + Var1 + " " + Var2 + " #" + Length );
		
		if( SST.ModeC ) {
			if( VarType.StoreTypeisVcode( SubType ) ) {
				string btype = VarType.GetTypeFromVdata( SubType );
				int w = VarType.GetBitSize( btype );
				
				if( SST.CodeArrayByFunc == 1 ) {
					if( w <= 8 ) {
						exp = "(" + SST.PRE_t + btype + ")pgm_read_byte_near(" + exp + "+" + exp_sub + ")";
					}
					else if( w <= 16 ) {
						exp = "(" + SST.PRE_t + btype + ")pgm_read_word_near(" + exp + "+" + exp_sub + ")";
					}
					else {
						exp = "(" + SST.PRE_t + btype + ")pgm_read_dword_near(" + exp + "+" + exp_sub + ")";
					}
				}
				else {
					exp = exp + "[" + exp_sub + "]";
				}
			}
			else if( SST.CodeArrayByFunc == 1 && VarType.isSysReferForArray( Type1 ) && Type1.StartsWith( "-> [ " ) ) {
				exp = "(((" + SST.PRE_t + "uint32)(" + exp + ")&0x10000)==0? " + exp + "[" + exp_sub + "]: pgm_read_byte_near((" + SST.PRE_t + "int8*)((" + SST.PRE_t + "uint32)" + exp + "&0xFFFF)+" + exp_sub + " ))";
			}
			else {
				exp += "[" + exp_sub + "]";
			}
		}
		if( SST.ModeV ) {
			
			/*
			SST.Add( "\t" + Deploy.V_PC + ": begin ramADDR <= " + exp_sub + "; ramDI <= 23; ramWE <= 1; " + Deploy.V_NamePC() + " <= " + (Deploy.V_PC+1) + "; end" );
			Deploy.V_PC += 1;
			SST.Add( "\t" + Deploy.V_PC + ": begin ramWE <= 0; " + Deploy.V_NamePC() + " <= " + (Deploy.V_PC+1) + "; end" );
			Deploy.V_PC += 1;
			*/
			
			/*
			SST.Add( "\t" + Deploy.V_PC + ": begin ramADDR <= $ARRAY:tttt$ + " + exp_sub + "; " + Deploy.V_NamePC() + " <= " + (Deploy.V_PC+1) + "; end\n" );
			Deploy.V_PC += 1;
			SST.Add( "\t" + Deploy.V_PC + ": begin NOP <= 0; " + Deploy.V_NamePC() + " <= " + (Deploy.V_PC+1) + "; end\n" );
			Deploy.V_PC += 1;
			*/
			
			//exp = "ramDO";
			
			exp += "[" + exp_sub + "]";
		}
		
		return ReturnIndex;
	}
	
	static int 数组属性运算( int Var1, int Index )
	{
		if( Var1 == -1 ) {
			return -1;
		}
		string exp_error = null;
		Var1 = 读取方法( Var1, ref exp_error, "error" );
		if( Var1 == -1 ) {
			return -1;
		}
		string Type1 = VarList.Get( Var1 ).Type;
		
		//应该判断属性名称是否为 length
		
		//获取分量的空间大小
		string SubType = VarType.GetArrayMemberTypeFromRefer( Type1 );
		int Length = VarType.GetSize( SubType );
		
		VarNode vv = new VarNode();
		vv.SetTempValue( VarType.AddSysRefer( SubType ), Deploy.VarLevel, VarList.Get( Var1 ).Location );
		vv.IgnoreWriteError = true;
		int ReturnIndex = VarList.Add( vv );
		
		AddIns( "= " + ReturnIndex + " " + Var1 );
		
		return ReturnIndex;
	}
	
	static int 整型分量运算( int VarIndex, int Index )
	{
		if( VarIndex == -1 ) {
			return -1;
		}
		int NumberIndex = int.Parse( ParseNet.NodeSet[ Index ][ 2 ] );
		string Number = 获取表达式中的单名称( NumberIndex );
		string TypeTemp = null;
		Number = ConstString.GetValue( ref TypeTemp, Number, NumberIndex );
		int N = int.Parse( Number );
		
		//获取后缀限定类型
		int LastAddIndex = int.Parse( ParseNet.NodeSet[ Index ][ 3 ] );
		if( LastAddIndex == -1 ) {
			ET.WriteParseError( NumberIndex, "需要指定整型变量分量的类型" );
			return -1;
		}
		string ReturnType = WordList.GetWord( int.Parse( ParseNet.NodeSet[ LastAddIndex ][ 2 ] ) );
		VarNode v = new VarNode();
		v.SetTempValue( ReturnType, Deploy.VarLevel, NumberIndex );
		v.Address = "&index " + VarIndex + " " + N;
		v.IsOut = true;
		int r = VarList.Add( v );
		return r;
	}
	
	static int 组合分量运算( int VarIndex, int Index )
	{
		if( VarIndex == -1 ) {
			return -1;
		}
		string SourceType = VarList.Get( VarIndex ).Type;
		string StructName = VarType.GetStructNameFromRefer( SourceType );
		int StructIndex = StructList.GetIndex( StructName );
		
		//生成结构体的偏移运算
		string SubOffset = 获取表达式中的单名称( int.Parse( ParseNet.NodeSet[ Index ][ 2 ] ) );
		
		//获取并判断结构体分量是否存在
		string SubType = StructList.Get( StructIndex ).GetSubType( SubOffset );
		if( SubType == null ) {
			ET.WriteParseError( VarList.Get( VarIndex ).Location, "<组合分量运算> 未定义的结构体分量: " + StructName + "." + SubOffset );
			return -1;
		}
		SubType = VarType.AddSysRefer( SubType );
		VarNode v = new VarNode();
		v.SetTempValue( SubType, Deploy.VarLevel, VarList.Get( VarIndex ).Location );
		int r = VarList.Add( v );
		int Offset = StructList.Get( StructIndex ).GetOffset( SubOffset );
		Expression.AddIns( Code1.结构偏移 + " " + r + " " + VarIndex + " #" + Offset );
		return r;
	}
	
	static string 获取表达式中的单名称( int Index )
	{
		string Head = ParseNet.NodeSet[ Index ][ 0 ];
		
		if( Head == ParseNet.Node.终结词 ) {
			return WordList.GetWord( Index );
		}
		ET.ShowError( "<获取表达式中的单名称> 发现非终结词" );
		return null;
	}
	
	public static int 创建引用( int VarIndex, ref string exp )
	{
		if( VarIndex == -1 ) {
			return -1;
		}
		string Type = VarList.Get( VarIndex ).Type;
		
		string fname = SST.GetFullName( VarList.Get( VarIndex ).Name );
		
		//测试局部变量添加前缀
		if( !VarList.Get( VarIndex ).Name.StartsWith( "#." ) ) {
			fname = SST.PRE_l + fname;
		}
		exp = fname;
		
		//如果是虚拟类型数据直接返回
		string Address = VarList.GetAddr( VarIndex );
		if( Address != null && Address.StartsWith( "#macro " ) ) {
			return VarIndex;
		}
		//如果是基本存储类型的变量直接返回
		if( VarType.isBase( Type ) ) {
			return VarIndex;
		}
		//如果是系统引用类型的变量直接返回
		if( VarType.isSysRefer( Type ) ) {
			
			if( VarType.isSysReferForArray( Type ) ) {
				//函数形参定义为数组类型时, 会执行这里
				exp = fname;
				
				//string mType = VarType.GetSourceTypeFromRefer( Type );
				//string OperType = VarType.GetTypeFromVdata( mType );
				//OperType = VarType.GetTypeFromVdata( OperType );
				
				//exp = "((" + SST.PRE_t + OperType + "*)" + fname + ")";
			}
			else {
				//函数形参定义为 #.base type类型时, 会执行这里
				exp = "(*" +  fname + ")";
			}
			
			return VarIndex;
		}
		//如果是指针类型的变量直接返回
		if( VarType.isPointer( Type ) ) {
			return VarIndex;
		}
		//如果是引用类型的变量, 创建一个系统引用, 并读取目标变量的地址
		if( VarType.isRefer( Type ) || VarType.isArray( Type ) || VarType.isStruct( Type ) ) {
			
			
			if( VarType.isRefer( Type ) ) {
				exp = "(*" + fname + ")";
			}
			
			if( VarType.isArray( Type ) ) {
				
				if( SST.CodeArrayByFunc == 1 && VarType.StoreTypeisVcode( Type ) ) {
					exp = "((" + SST.PRE_t + "int8*)((" + SST.PRE_t + "uint32)" + fname + " + 0x10000))";
				}
				else {
					//exp = "((" + SST.PRE_t + "uint32)" + fname + ")";
				}
			}
			
			Type = VarType.AddSysRefer( Type );
			VarNode mv = new VarNode();
			mv.SetTempValue( Type, Deploy.VarLevel, VarList.Get( VarIndex ).Location );
			int mi = VarList.Add( mv );
			AddIns( Code1.读取地址 + " " + mi + " #" + VarIndex );
			return mi;
		}
		//到这里应是扩展类型变量, 读取其地址, 返回一个指向这个地址的引用,类型为 &+type
		VarNode externV = new VarNode();
		externV.EnableWrite = false;
		externV.SetTempValue( VarType.AddSysRefer( Type ), Deploy.VarLevel, VarList.Get( VarIndex ).Location );
		int ei = VarList.Add( externV );
		
		//对于没有地址的虚拟数据类型,一定不能有下面这个指令,会很麻烦的,还占据指令行;
		string VdataType = VarType.GetStoreType( Type );
		int VdataIndex = VdataList.GetIndex( VdataType );
		if( VdataList.Get( VdataIndex ).AddrType != null ) {
			AddIns( Code1.读取地址 + " " + ei + " #" + VarIndex );
			
			if( VarList.Get( VarIndex ).Address == null ) {
				exp = "<<addr-null>>";
			}
			else {
				exp = VarList.Get( VarIndex ).Address.ToString();
			}
		}
		else {
			exp = "($$$*" +  fname + ")";
		}
		//引用静态变量执行这里
		
		return ei;
	}
	
	public static int 读取宏变量( int VarIndex, ref string exp )
	{
		if( VarIndex == -1 ) {
			return -1;
		}
		string Type = VarList.Get( VarIndex ).Type;
		
		//如果是虚拟类型数据, 调用读取操作, 返回一个实际变量
		string Address = VarList.GetAddr( VarIndex );
		if( Address != null && Address.StartsWith( "#macro " ) ) {
			string[] MacroAddrCut = Address.Split( ' ' );
			VarNode v = new VarNode();
			v.SetTempValue( VarList.Get( VarIndex ).Type, Deploy.VarLevel, VarList.Get( VarIndex ).Location );
			int i = VarList.Add( v );
			Expression.AddIns( Code1.读取分量 + " " + i + " " + MacroAddrCut[ 1 ] + " #" + MacroAddrCut[ 2 ] );
			
			int loc = int.Parse( MacroAddrCut[ 2 ] );
			
			long sft = 0;
			if( Type == VarType.BaseType.Bit ) {
				sft = 0x01;
			}
			if( Type == VarType.BaseType.Uint8 ) {
				sft = 0xFF;
			}
			if( Type == VarType.BaseType.Uint16 ) {
				sft = 0xFFFF;
			}
			if( Type == VarType.BaseType.Uint32 ) {
				sft = 0xFFFFFFFF;
			}
			
			int varidx = int.Parse( MacroAddrCut[ 1 ] );
			exp = VarList.GetFinalName( varidx );
			if( VarList.Get( varidx ).ActiveType == MemberType.StoreType.Local ) {
				exp = SST.PRE_l + exp;
			}
			
			//string BIT0 = (MAX - (sft << loc)).ToString();
			exp = "((" + SST.GetFullName( exp ) + " >> " + loc + ") & " + sft + ")";
			
			return i;
		}
		return VarIndex;
	}
	
	public static int 隐式转换( string TargetType, int VarIndex, string myVarType, ref string exp )
	{
		if( VarType.isSysRefer( TargetType ) && VarType.isSysRefer( myVarType ) ) {
			return VarIndex;
		}
		if( !Operation.CanSwitch( myVarType, TargetType ) ) {
			
			//try {
			ET.WriteParseError( VarList.Get( VarIndex ).Location,
			                   "<Expression 隐式转换> 类型不兼容: " + TargetType + " <- " + myVarType );
			//} catch {
			//	n_OS.VIO.Show( VarList.Get( VarIndex ).ToString() );
			//}
			return -1;
		}
		VarNode v = new VarNode();
		v.SetTempValue( TargetType, Deploy.VarLevel, VarList.Get( VarIndex ).Location );
		int i = VarList.Add( v );
		Expression.AddIns( Code1.隐式转换 + " " + i + " " + VarIndex );
		
		if( TargetType == VarType.BaseType.fix ) {
			exp = "((" + SST.PRE_t + "int32)(" + exp + ") << 10)";
		}
		if( TargetType == VarType.BaseType.Sint32 && myVarType == VarType.BaseType.fix ) {
			exp = "((" + SST.PRE_t + "int32)(" + exp + ") >> 10)";
		}
		return i;
	}
	
	static void 写入方法( int Var1, int Var2, ref string exp, string exp1, string exp2 )
	{
		if( Var1 == -1 || Var2 == -1 ) {
			return;
		}
		string Type1 = VarList.Get( Var1 ).Type;
		if( VarType.isSysReferForStruct( Type1 ) || VarType.isSysReferForArray( Type1 ) ) {
			ET.WriteParseError( VarList.Get( Var2 ).Location,
							"<写入方法> 不可以向复合类型的引用赋值运算: " + Type1 );
			return;
		}
		if( VarType.isSysReferForRefer( Type1 ) ) {
			string exp_error = null;
			Var1 = 读接口( Var1, ref exp_error, "error" );
		}
		Type1 = VarList.Get( Var1 ).Type;
		if( VarType.isSysReferForStruct( Type1 ) || VarType.isSysReferForArray( Type1 ) ) {
			ET.WriteParseError( VarList.Get( Var2 ).Location,
							"<写入方法> 不可以向复合类型的引用赋值运算: " + Type1 );
			return;
		}
		写接口( Var1, Var2, ref exp, exp1, exp2 );
	}
	
	static void 写接口( int Var1, int Var2, ref string exp, string exp1, string exp2 )
	{
		string Type1 = VarList.Get( Var1 ).Type;
		string Type2 = VarList.Get( Var2 ).Type;
		
		string tttType = VarType.GetSourceTypeFromRefer( Type1 );
		bool isBase = VarType.StoreTypeisVbase( tttType );
		
		int CallFunctionIndex = GetInterface( VarList.Get( Var1 ).Location, "set_", ref Type1 );
		if( CallFunctionIndex == -1 ) {
			return;
		}
		//获取函数形参列表
		string FuncVarList = FunctionList.Get( CallFunctionIndex ).VarIndexList;
		string[] VarListCut = FuncVarList.Split( ' ' );
		
		//如果形参是常量类型,就根据实参类型进行转换
		int DataVarIndex;
		if( VarListCut.Length == 2 ) {
			DataVarIndex = int.Parse( VarListCut[ 1 ] );
		}
		else {
			DataVarIndex = int.Parse( VarListCut[ 0 ] );
		}
		if( Type2.IndexOf( ',' ) != -1 ) {
			//Type2 = Operation.GetDoubleVarType( Type2, VarList.Get( DataVarIndex ).Type );
			Type2 = Operation.GetDoubleVarType( Type2, Type1 );
			VarList.Get( Var2 ).Type = Type2;
		}
		
		if( Type1 != Type2 ) {
			//注意这里临时放过引用指向的检查...
			if( VarType.isBase( Type1 ) ) {
				string exp_t = null;
				Var2 = Expression.隐式转换( Type1, Var2, Type2, ref exp_t );
				if( Var2 == -1 ) {
					return;
				}
				Type2 = VarList.Get( Var2 ).Type;
			}
		}
		//这里是后添的,解决静态变量作为实参传递的问题
		//另外又加上了类型匹配, 把基本类型转换成无符号数类型
		Type2 = VarType.BaseType.GetInterfaceType( Type2 );
		//if( VarList.Get( Var2 ).ActiveType == "static" ) {
//			VarNode vv = new VarNode();
//			vv.SetTempValue( Type2, Deploy.VarLevel, VarList.Get( Var2 ).Location );
//			vv.IgnoreWriteError = true;
//			int ii = VarList.Add( vv );
//			AddIns( "= " + ii + " " + Var2 );
//			Var2 = ii;
		//}

		//传递实参
		if( VarListCut.Length == 2 ) {
			
			//---------------------------------
			//这一段赋值不知道为何...
			//这里不知道为何要进行一次赋值运算
//			VarNode mv = new VarNode();
//			mv.SetTempValue( VarList.Get( Var1 ).Type, Deploy.VarLevel, VarList.Get( Var1 ).Location );
//			mv.IgnoreWriteError = true;
//			int mi = VarList.Add( mv );
//			AddIns( "= " + mi + " " + Var1 );
			//---------------------------------
			
			AddIns( Code1.堆栈递增 + " " + "#" + Deploy.FunctionIndex );
			
			
			
			
			if( Config.GetCPU().StartsWith( CPUType.MCS_X ) ) {
				string[] CallVarList = FunctionList.Get( CallFunctionIndex ).VarIndexList.Split( ' ' );
				AddIns( Code1.传递形参 + " " + CallVarList[0] + " " + Var1 );
				AddIns( Code1.传递形参 + " " + CallVarList[1] + " " + Var2 );
			}
			else if( Config.GetCPU().StartsWith( CPUType.MEGA_X ) || Config.GetCPU() == CPUType.VM ) {
				AddIns( Code1.传递形参 + " # " + Var1 );
				AddIns( Code1.传递形参 + " # " + Var2 );
			}
			else {
				ET.WriteLineError( 0, 0, "传递形参遇到未知的芯片类型" );
			}
			
			
			if( !isBase ) {
				exp = SST.GetFullName( FunctionList.Get( CallFunctionIndex ).FunctionName ) + "( " + exp1 + ", " + exp2 + " )";
			}
			
			
		}
		else if( VarListCut.Length == 1 ) {
			AddIns( Code1.堆栈递增 + " " + "#" + Deploy.FunctionIndex );
			
			
			
			if( Config.GetCPU().StartsWith( CPUType.MCS_X ) ) {
				string[] CallVarList = FunctionList.Get( CallFunctionIndex ).VarIndexList.Split( ' ' );
				AddIns( Code1.传递形参 + " " + CallVarList[1] + " " + Var2 );
			}
			else if( Config.GetCPU().StartsWith( CPUType.MEGA_X ) || Config.GetCPU() == CPUType.VM ) {
				AddIns( Code1.传递形参 + " # " + Var2 );
			}
			else {
				ET.WriteLineError( 0, 0, "传递形参遇到未知的芯片类型" );
			}
			
			if( !isBase ) {
				exp = SST.GetFullName( FunctionList.Get( CallFunctionIndex ).FunctionName ) + "( " + exp2 + " )";
			}
		}
		else {
			ET.ShowError( "<写接口> 写接口函数形参数目不对" );
			return;
		}
		AddIns( Code1.函数调用 + " " + CallFunctionIndex );
		AddIns( Code1.堆栈递减 + " #" + Deploy.FunctionIndex );
		
		CallList.AddSubFunction( Deploy.FunctionIndex, CallFunctionIndex.ToString() );
	}
	
	public static int 读取方法( int VarIndex, ref string exp, string exp_var )
	{
		if( VarIndex == -1 ) {
			return -1;
		}
		if( VarList.Get( VarIndex ).ChipIndex != Deploy.ChipIndex ) {
			ET.ShowError( VarList.Get( VarIndex ).Name + "(" + VarList.Get( VarIndex ).Type + ")" );
		}
		string Type = VarList.Get( VarIndex ).Type;
		
		//如果是基本类型, 直接返回
		if( VarType.isBase( Type ) ) {
			return VarIndex;
		}
		//如果是指针类型, 直接返回
		if( VarType.isPointer( Type ) ) {
			return VarIndex;
		}
		//如果是结构体的系统引用, 直接返回
		if( VarType.isSysReferForStruct( Type ) ) {
			return VarIndex;
		}
		//如果是对数组的系统引用, 直接返回
		if( VarType.isSysReferForArray( Type ) ) {
			return VarIndex;
		}
		//如果是对指针的系统引用, 直接返回
		if( VarType.isSysReferForPointer( Type ) ) {
			return VarIndex;
		}
		//到这里肯定是系统引用
		if( !VarType.isSysRefer( Type ) ) {
			ET.ShowError( "<读取方法> 未定义的引用类型读取: " + Type );
			return -1;
		}
		int RefIndex = 读接口( VarIndex, ref exp, exp );
		return 读取方法( RefIndex, ref exp, exp );
	}
	
	static int 读接口( int VarIndex, ref string exp, string exp_var )
	{
		string Type = VarList.Get( VarIndex ).Type;
		
		string tttType = VarType.GetSourceTypeFromRefer( Type );
		bool isBase = VarType.StoreTypeisVbase( tttType ) || VarType.StoreTypeisVcode( tttType );
		
		//处理接口函数调用
		int CallFunctionIndex = GetInterface( VarList.Get( VarIndex ).Location, "get_", ref Type );
		if( CallFunctionIndex == -1 ) {
			return -1;
		}
		int RetIndex;
		
		/*
		if( CallFunctionIndex == 1 ) {
			AddIns( Code1.LOAD_local_uint8 + " # " + VarIndex );
			VarNode v = new VarNode();
			v.SetTempValue( Type, Deploy.VarLevel, VarList.Get( VarIndex ).Location );
			RetIndex = VarList.Add( v );
			AddIns( Code1.读系统量 + " " + RetIndex );
		}
		else if( CallFunctionIndex == 3 ) {
			AddIns( Code1.LOAD_local_uint16 + " # " + VarIndex );
			VarNode v = new VarNode();
			v.SetTempValue( Type, Deploy.VarLevel, VarList.Get( VarIndex ).Location );
			RetIndex = VarList.Add( v );
			AddIns( Code1.读系统量 + " " + RetIndex );
		}
		else if( CallFunctionIndex == 5 ) {
			AddIns( Code1.LOAD_local_uint32 + " # " + VarIndex );
			VarNode v = new VarNode();
			v.SetTempValue( Type, Deploy.VarLevel, VarList.Get( VarIndex ).Location );
			RetIndex = VarList.Add( v );
			AddIns( Code1.读系统量 + " " + RetIndex );
		}
		else {
		*/
		
		//获取函数形参列表
		string FuncVarList = FunctionList.Get( CallFunctionIndex ).VarIndexList;
		
		if( FuncVarList != null ) {
			
			AddIns( Code1.堆栈递增 + " " + "#" + Deploy.FunctionIndex );
			
			if( Config.GetCPU().StartsWith( CPUType.MCS_X ) ) {
				string[] CallVarList = FunctionList.Get( CallFunctionIndex ).VarIndexList.Split( ' ' );
				AddIns( Code1.传递形参 + " " + CallVarList[0] + " " + VarIndex );
			}
			else if( Config.GetCPU().StartsWith( CPUType.MEGA_X ) || Config.GetCPU() == CPUType.VM ) {
				AddIns( Code1.传递形参 + " # " + VarIndex );
			}
			else {
				ET.WriteLineError( 0, 0, "传递形参遇到未知的芯片类型" );
			}
			
			if( !isBase ) {
				exp = SST.GetFullName( FunctionList.Get( CallFunctionIndex ).FunctionName ) + "( " + exp + " )";
			}
		}
		else {
			AddIns( Code1.堆栈递增 + " " + "#" + Deploy.FunctionIndex );
			
			if( !isBase ) {
				exp = SST.GetFullName( FunctionList.Get( CallFunctionIndex ).FunctionName ) + "()";
			}
		}
		AddIns( Code1.函数调用 + " " + CallFunctionIndex );
		AddIns( Code1.堆栈递减 + " " + "#" + Deploy.FunctionIndex );
		VarNode v = new VarNode();
		v.SetTempValue( Type, Deploy.VarLevel, VarList.Get( VarIndex ).Location );
		RetIndex = VarList.Add( v );
		AddIns( Code1.读系统量 + " " + RetIndex );
		
		CallList.AddSubFunction( Deploy.FunctionIndex, CallFunctionIndex.ToString() );
		
		return RetIndex;
	}
	
	static int GetInterface( int ErrorLocation, string GetOrSet, ref string vType )
	{
		//获取引用指向的源类型
		vType = VarType.GetSourceTypeFromRefer( vType );
		string VdataName = VarType.GetStoreType( vType );
		int VdataIndex = VdataList.GetIndex( VdataName );
		
		//如果是虚拟内置类型,获取其运算类型
		if( VarType.isVdata( vType ) ) {
			vType = VarType.GetTypeFromVdata( vType );
		}
		//到这里说明是引用
		else {
			vType = VarType.SwitchToSysRefer( vType );
		}
		//获取虚拟变量的接口类型,把有符号数和判断类型转化成无符号数
		string ASMType = VarType.GetReferAddressType( vType );
		
		//===========
		//vType = ASMType;
		
		
		//对齐数据长度,任何变量最少占据一个基本单位
		string BaseType = VdataList.Get( VdataIndex ).DataType;
		
		if( VarType.GetBitSize( ASMType ) <= VarType.GetBitSize( BaseType ) ) {
			ASMType = BaseType;
		}
		//获取目标元件和接口函数
		string UnitName = VdataList.Get( VdataIndex ).TargetUnitName;
		int UnitIndex = UnitList.GetIndex( UnitName );
		if( UnitIndex == -1 ) {
			ET.WriteParseError( ErrorLocation, "<GetInterface> 此类型变量未定义目标元件接口: " + VdataName + "->" + UnitName );
			return -1;
		}
		string PreName = VdataList.Get( VdataIndex ).PreName;
		int FuncIndex = FunctionList.GetIndex( UnitName + "." + PreName + GetOrSet + ASMType );
		if( FuncIndex == -1 ) {
			ET.WriteParseError( ErrorLocation, "<GetInterface> 此类型变量的读写接口函数未定义: " +
			                    UnitName + "." + GetOrSet + ASMType + " <" + VdataName + ">" );
			return -1;
		}
		//获取目标函数形参类型列表和返回类型
		string VarTypeList = FunctionList.Get( FuncIndex ).VarTypeList;
		string ReturnType = FunctionList.Get( FuncIndex ).ReturnType;
		
		//判断函数形参和实参格式是否一致
		if( GetOrSet == "get_" ) {
			string RightAddrType = VdataList.Get( VdataIndex ).AddrType;
			string RightType = ASMType + ":" + RightAddrType;
			if( RightType != ReturnType + ":" + VarTypeList ) {
				ET.WriteParseError( ErrorLocation, "<GetInterface> 此类型的get接口函数定义不符合规则,应为: " +
				                   ASMType + " get_" + ASMType + "(" + RightAddrType + ")" );
				return -1;
			}
		}
		else {
			string RightType = VarType.Void;
			
			string RightAddrType = VdataList.Get( VdataIndex ).AddrType;
			if( RightAddrType != null ) {
				RightAddrType += "," + ASMType;
			}
			else {
				RightAddrType = ASMType;
			}
			RightType += ":" + RightAddrType;
			
			if( RightType != ReturnType + ":" + VarTypeList ) {
				ET.WriteParseError( ErrorLocation, "<GetInterface> 此类型的set接口函数定义不符合规则,应为: " +
				                   "void set_" + ASMType + "(" + RightAddrType + ") 实际为 " + VarTypeList );
				return -1;
			}
		}
		return FuncIndex;
	}
	
	//优化辅助量
	static int LastOperVarIndex = -1;
	
	static bool NeedDisableInt = false;
	static bool TempDisableInt = false;
	
	public static void WriteBack()
	{
		if( LastOperVarIndex == -1 ) {
			//n_OS.VIO.Show( "<WriteBack> 当前的临时变量已经保存" );
		}
		else {
			FunctionCodeList.AddCode( Code1.保存_0 + "_" + 读写方式( LastOperVarIndex ) + " " + LastOperVarIndex );
			LastOperVarIndex = -1;
		}
	}
	
	public static void AddIns( string instruction )
	{
		//如果是基本指令 返回
		if( !Config.isReentrant() || instruction.StartsWith( "#" ) ) {
			if( instruction != Code1.启用中断 && instruction != Code1.禁用中断 ) {
				if( LastOperVarIndex != -1 ) {
					
					//n_OS.VIO.Show( "<AddIns> 临时变量尚未保存: " + instruction + " " + LastOperVarIndex );
					
				}
				LastOperVarIndex = -1;
			}
			FunctionCodeList.AddCode( instruction );
			return;
		}
		//加载变量到寄存器中
		string[] Cut = instruction.Split( ' ' );
		
		if( Cut.Length <= 2 ) {
			if( LastOperVarIndex != -1 ) {
				FunctionCodeList.AddCode( Code1.保存_0 + "_" + 读写方式( LastOperVarIndex ) + " " + LastOperVarIndex );
			}
		}
		TempDisableInt = false;
		bool join = false;
		if( Cut.Length > 2 ) {
			if( !Cut[ 2 ].StartsWith( "#" ) ) {
				
				//这个和下边的对应, 不要使用了过时的 LastOperVarIndex
				if( LastOperVarIndex != int.Parse( Cut[ 2 ] ) ) {
					
					if( Cut.Length > 3 ) {
						if( !Cut[ 3 ].StartsWith( "#" ) && LastOperVarIndex == int.Parse( Cut[ 3 ] ) ) {
							
							
							if( !n_Param.Param.TestOn ) {
								join = true;
								FunctionCodeList.AddCode( Code1.转移参数 + " " + LastOperVarIndex );
							}
						}
					}
					if( LastOperVarIndex != -1 ) {
						if( !join ) {
							FunctionCodeList.AddCode( Code1.保存_0 + "_" + 读写方式( LastOperVarIndex ) + " " + LastOperVarIndex );
						}
						else {
							//FunctionCodeList.AddCode( "///////////////////////////////////////////" + Code1.保存_0 + "_" + 读写方式( LastOperVarIndex ) + " " + LastOperVarIndex );
						}
					}
					
					if( NeedDisableInt ) {
						FunctionCodeList.AddCode( Code1.禁用中断 );
						TempDisableInt = true;
					}
					FunctionCodeList.AddCode( Code1.加载_0 + "_" + 读写方式( int.Parse( Cut[ 2 ] ) ) + " " + Cut[ 2 ] );
				}
				//这里是优化的核心, 不需要对上一个临时变量执行保存和重新加载
				else {
					//...
				}
			}
			else {
				if( LastOperVarIndex != -1 ) {
					FunctionCodeList.AddCode( Code1.保存_0 + "_" + 读写方式( LastOperVarIndex ) + " " + LastOperVarIndex );
				}
			}
		}
		if( Cut.Length > 3 && !Cut[ 3 ].StartsWith( "#" ) ) {
			
			if( !join ) {
				FunctionCodeList.AddCode( Code1.加载_1 + "_" + 读写方式( int.Parse( Cut[ 3 ] ) ) + " " + Cut[ 3 ] );
			}
			else {
				//FunctionCodeList.AddCode( "///////////////////////////////////////////" + Code1.加载_1 + "_" + 读写方式( int.Parse( Cut[ 3 ] ) ) + " " + Cut[ 3 ] );
			}
		}
		//进行运算
		FunctionCodeList.AddCode( instruction );
		
		
		//清除上一次的标志, 不能把这个标志遗留到以后的代码, 防止该加载的变量忽略
		//刚才别的程序优化都没问题, 就是音乐播放有问题, 播放太慢了, 研究一下午, 终于发现了问题, 下边的程序优化逻辑
		//有些问题, 没有把 LastOperVarIndex 清零, 导致下一个指令忽略了加载数据(碰巧相等时)
		LastOperVarIndex = -1;
		
		//写回结果到变量中
		if( !Cut[ 1 ].StartsWith( "#" ) ) {
			
			LastOperVarIndex = int.Parse( Cut[ 1 ] );
			
			//是否启用临时变量读写优化
			bool OpenOpt = true;
			
			if( !OpenOpt || VarList.Get( LastOperVarIndex ).ActiveType != "temp" ) {
				
				FunctionCodeList.AddCode( Code1.保存_0 + "_" + 读写方式( LastOperVarIndex ) + " " + LastOperVarIndex );
				
				if( NeedDisableInt ) {
					FunctionCodeList.AddCode( Code1.启用中断 );
					if( !TempDisableInt ) {
						n_OS.VIO.Show( "禁用中断和启用中断不匹配!" );
					}
				}
				
				LastOperVarIndex = -1;
			}
		}
		
	}
	
	static string 读写方式( int VarIndex )
	{
		string ActiveType = VarList.Get( VarIndex ).ActiveType;
		if( ActiveType == "static" ) {
			return "static";
		}
		else if( ActiveType == "local" || ActiveType == "temp" ) {
			return "local";
		}
		else {
			ET.ShowError( "<读写方式> 未知的存储类型: " + ActiveType );
			return null;
		}
	}
	
	//-----------------------------------------------
	
	public static string V_Add( string exp )
	{
		return "\t" + Deploy.V_PC + ": " + exp + "\n";
	}
	
	public static string V_Add0( string exp )
	{
		return "\t" + Deploy.V_PC + ": " + exp;
	}
	
	//生成verilog除法
	static void Verilog_Div( string exp1, string exp2 )
	{
		//直接调用IP
		/*
		SST.Add( "\t" + Deploy.V_PC + ": begin cx_DIV_Numer <= " + exp1 + "; cx_DIV_Denom <= " + exp2 + "; cx_DIV_Start <= 1; " + Deploy.V_NamePC() + " <= " + (Deploy.V_PC+1) + "; end\n" );
		Deploy.V_PC += 1;
		SST.Add( "\t" + Deploy.V_PC + ": begin cx_DIV_Start <= 0; " + Deploy.V_NamePC() + " <= " + (Deploy.V_PC+1) + "; end\n" );
		Deploy.V_PC += 1;
		SST.Add( "\t" + Deploy.V_PC + ": if( cx_DIV_Done != 0 ) begin cx_DIV_Quotient_r <= cx_DIV_Quotient; " + Deploy.V_NamePC() + " <= " + (Deploy.V_PC+1) + "; end else " + Deploy.V_NamePC() + " <= " + Deploy.V_PC + ";\n" );
		Deploy.V_PC += 1;
		*/
		
		//通过总线调用
		string div_name = "#.DIV_IP";
		string div_fullname = SST.GetFullName( div_name + "." );
		int Var1 = VarList.GetStaticIndex( div_name );
		if( VarList.Get( Var1 ).VisitCoreList.IndexOf( " " + Deploy.CoreIndex + " " ) == -1 ) {
			VarList.Get( Var1 ).VisitCoreList += Deploy.CoreIndex + " ";
		}
		SST.Add( V_Add( "begin " + div_fullname + "Numer" + Deploy.CoreIndex + " <= " + exp1 + "; " + div_fullname + "Denom" + Deploy.CoreIndex + " <= " + exp2 + "; " +
		                div_fullname + "flg" + Deploy.CoreIndex + " <= 1; end" ) );
		Deploy.V_PC += 1;
		SST.Add( V_Add( "if( " + div_fullname + "ack" + Deploy.CoreIndex + " != 0 ) " + div_fullname + "flg" + Deploy.CoreIndex + " <= 0; else " + Deploy.V_NamePC() + " <= " + Deploy.V_PC + ";" ) );
		Deploy.V_PC += 1;
	}
}
}



