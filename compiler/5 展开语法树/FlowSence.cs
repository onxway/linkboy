﻿
//控制语句转化器
namespace n_FlowSence
{
using System;
using n_Code1;
using n_Deploy;
using n_ET;
using n_Expression;
using n_ParseNet;
using n_VarList;
using n_VarNode;
using n_VarType;
using n_WordList;
using n_ConstString;
using n_Config;
using n_SST;
using n_MemberType;
using n_RecordMacro;

public static partial class FlowSence
{
	public const int MACRO = -1234567;
	
	//当前是否是 para 语句块
	public static bool isPara;
	
	//复位
	public static void Reset()
	{
		isPara = false;
	}
	
	//控制语句转化器, 每个控制语句中只允许最多一个中间变量和布尔变量
	public static void 控制语句( int Index, int LastControlIndex )
	{
		++Deploy.VarLevel;
		++Deploy.SenceNumber;
		
		if( ParseNet.NodeSet[ Index ][ 0 ] == ParseNet.Node.如果语句 ) {
			if语句( Index, LastControlIndex );
		}
		else if( ParseNet.NodeSet[ Index ][ 0 ] == ParseNet.Node.如果否则语句 ) {
			ifelse语句( Index, LastControlIndex );
		}
		else if( ParseNet.NodeSet[ Index ][ 0 ] == ParseNet.Node.迭代语句 ) {
			for语句( Index );
		}
		
		
		else if( ParseNet.NodeSet[ Index ][ 0 ] == ParseNet.Node.ChiselFor ) {
			Chiselfor语句( Index );
		}
		
		
		else if( ParseNet.NodeSet[ Index ][ 0 ] == ParseNet.Node.loop语句 ) {
			loop语句( Index );
		}
		else if( ParseNet.NodeSet[ Index ][ 0 ] == ParseNet.Node.while语句 ) {
			while语句( Index );
		}
		else if( ParseNet.NodeSet[ Index ][ 0 ] == ParseNet.Node.dowhile语句 ) {
			dowhile语句( Index );
		}
		else if( ParseNet.NodeSet[ Index ][ 0 ] == ParseNet.Node.switch语句 ) {
			switch语句( Index );
		}
		//已过时
		else if( ParseNet.NodeSet[ Index ][ 0 ] == ParseNet.Node.多分支如果语句 ) {
			n_OS.VIO.Show( "注意: [多分支如果语句] 已经过时, 请尽快反馈给我们解决问题" );
			switchelse语句( Index );
		}
		//已过时
		else if( ParseNet.NodeSet[ Index ][ 0 ] == ParseNet.Node.反复执行语句 ) {
			n_OS.VIO.Show( "注意: [反复执行语句] 已经过时, 请尽快反馈给我们解决问题!" );
			repeat语句( Index );
		}
		else if( ParseNet.NodeSet[ Index ][ 0 ] == ParseNet.Node.forever语句 ) {
			forever语句( Index );
		}
		else if( ParseNet.NodeSet[ Index ][ 0 ] == ParseNet.Node.单次执行语句 ) {
			once语句( Index );
		}
		else if( ParseNet.NodeSet[ Index ][ 0 ] == ParseNet.Node.并行语句 ) {
			并行语句( Index );
		}
		else if( ParseNet.NodeSet[ Index ][ 0 ] == ParseNet.Node.用户流程语句 ) {
			用户流程语句( Index );
		}
		
		
		else if( ParseNet.NodeSet[ Index ][ 0 ] == ParseNet.Node.宏如果语句 ) {
			宏if语句( Index, LastControlIndex );
		}
		else if( ParseNet.NodeSet[ Index ][ 0 ] == ParseNet.Node.宏如果否则语句 ) {
			宏ifelse语句( Index, LastControlIndex );
		}
		else if( ParseNet.NodeSet[ Index ][ 0 ] == ParseNet.Node.宏loop语句 ) {
			宏loop语句( Index, LastControlIndex );
		}
		
		else {
			ET.ShowError( "<控制语句> 未定义的控制语句: " + ParseNet.NodeSet[ Index ][ 0 ] );
		}
		--Deploy.VarLevel;
		VarList.ClearUselessVar( Deploy.VarLevel );
	}
	
	//====================================================================
	
	static void switch语句( int Index )
	{
		int ControlIndex = Deploy.SenceNumber;
		Expression.AddIns( Code1.语句层加 );
		int ExSenceIndex = int.Parse( ParseNet.NodeSet[ Index ][ 3 ] );
		string exp = null;
		Expression.isPad = true;
		int VarIndex = 计算整型表达式( ExSenceIndex, ref exp );
		
		if( VarIndex == -1 ) {
			return;
		}
		
		string MidType = VarList.Get( VarIndex ).Type;
		if( !SST.ModeV && MidType != VarType.BaseType.Uint8 ) {
			ET.WriteParseError( VarList.Get( VarIndex ).Location, "当前版本语法限定switch的表达式应是一个uint8类型: " + MidType );
			return;
		}
		if( SST.ModeC ) {
			SST.Add( "switch( " + exp + " ) {\n" );
		}
		if( SST.ModeV ) {
			SST.Add( Expression.V_Add( "case( " + exp + " )" ) );
			Deploy.V_PC++;
			
			SST.Add( "<case_list>" );
			SST.Add( "\tendcase\n" );
		}
		
		case分支判断( Index, ControlIndex, VarIndex );
		Expression.AddIns( Code1.标号 + " " + Config.PreLabel.End + ControlIndex );
		Expression.AddIns( Code1.语句层减 );
		
		if( SST.ModeC ) {
			SST.Add( "}\n" );
		}
	}
	
	static void case分支判断( int Index, int ControlIndex, int MidVar )
	{
		string Address = null;
		string ConstList = null;
		int CodeNumber = 0;
		int DefaultNumber = 0;
		int CaseNumber = 0;
		int n = int.Parse( ParseNet.NodeSet[ Index ][ 6 ] );
		for( int i = 1; i < ParseNet.NodeSet[ n ].Length; ++i ) {
			int CaseIndex = int.Parse( ParseNet.NodeSet[ n ][ i ] );
			
			if( ParseNet.NodeSet[ CaseIndex ][ 0 ] == ParseNet.Node.default语句 ) {
				DefaultNumber++;
				if( DefaultNumber > 1 ) {
					int dfid = int.Parse( ParseNet.NodeSet[ CaseIndex ][ 1 ] );
					ET.WriteParseError( dfid, "switch语句中最多只能有1个default语句" );
				}
				continue;
			}
			CaseNumber++;
			int ConstIndex = int.Parse( ParseNet.NodeSet[ CaseIndex ][ 2 ] );
			string Const = ConstExpression.GetExpressionValue( ConstIndex, Deploy.UnitName );
			if( Const == null ) {
				Const = "0";
			}
			//int ConstValue = int.Parse( Const );
			
			string label = Config.PreLabel.Case + ControlIndex + "_" + i;
			if( Address == null ) {
				Address = "@code ";
				ConstList = "@code ";
			}
			else {
				Address += ",";
				ConstList += ",";
			}
			Address += "#" + label;
			//Address += "#sys_label6_label";
			ConstList += Const;
			CodeNumber++;
		}
		int swIndex = int.Parse( ParseNet.NodeSet[ Index ][ 1 ] );
		if( CaseNumber == 0 ) {
			ET.WriteParseError( swIndex, "switch语句中至少要有1个case语句" );
			return;
		}
		if( CaseNumber > 255 ) {
			ET.WriteParseError( swIndex, "目前版本语法switch语句中case语句的数量不能大于255个" );
			return;
		}
		//这里可以细化, AVR单片机才需要把flash数组补齐为偶数个
		int cnumber = CodeNumber;
		if( cnumber % 2 != 0 ) {
			cnumber++;
			ConstList += ",0";
		}
		string codetype = VarType.Root + "." + VarType.UserRootUnitName + "0" + ".sys.code";
		
		//创建函数地址列表
		VarNode v = new VarNode();
		string Name = Config.PreLabel.Switchlist + ControlIndex;
		string type = "[" + CodeNumber + " " + codetype + " " + n_Config.Config.GetFunctionPointType();
		v.SetStaticValue( Name, type, n_MemberType.MemberType.VisitType.Private, n_MemberType.MemberType.RealRefType.Real, true, swIndex, 0, 0 );
		v.Address = Address;
		int FuncIdx = VarList.Add( v );
		string exp_sub = null;
		FuncIdx = Expression.创建引用( FuncIdx, ref exp_sub );
		
		//如果母函数被优化, 则不分配变量的空间
		v.FunctionIndex = Deploy.FunctionIndex;
		
		//创建常量列表
		VarNode v1 = new VarNode();
		string Name1 = Config.PreLabel.Switchlist + ControlIndex;
		string type1 = "[" + cnumber + " " + codetype + " " + VarType.BaseType.Uint8;
		v1.SetStaticValue( Name1, type1, n_MemberType.MemberType.VisitType.Private, n_MemberType.MemberType.RealRefType.Real, true, swIndex, 0, 0 );
		v1.Address = ConstList;
		int ConstIdx = VarList.Add( v1 );
		string exp_sub1 = null;
		ConstIdx = Expression.创建引用( ConstIdx, ref exp_sub1 );
		
		//如果母函数被优化, 则不分配变量的空间
		v1.FunctionIndex = Deploy.FunctionIndex;
		
		string labeldefault = Config.PreLabel.Default + ControlIndex;
		if( DefaultNumber == 0 ) {
			labeldefault = Config.PreLabel.End + ControlIndex;
		}
		Expression.AddIns( Code1.分支比较 + " # " + ConstIdx + " " + MidVar + " #" + CodeNumber + " #" + labeldefault );
		Expression.AddIns( Code1.分支散转 + " # " + FuncIdx );
		
		string case_list = "";
		
		for( int i = 1; i < ParseNet.NodeSet[ n ].Length; ++i ) {
			int CaseIndex = int.Parse( ParseNet.NodeSet[ n ][ i ] );
			
			if( ParseNet.NodeSet[ CaseIndex ][ 0 ] == ParseNet.Node.default语句 ) {
				
				if( SST.ModeC ) {
					SST.Add( "default:\n" );
				}
				if( SST.ModeV ) {
					case_list += "\t\tdefault: " + Deploy.V_NamePC() + " <= " + (Deploy.V_PC) + ";\n";
				}
				string label = Config.PreLabel.Default + ControlIndex;
				Expression.AddIns( Code1.标号 + " " + label );
				int CaseSenceIndex = int.Parse( ParseNet.NodeSet[ CaseIndex ][ 3 ] );
				Deploy.语句列表( CaseSenceIndex, ControlIndex );
			}
			else {
				
				int ConstIndex = int.Parse( ParseNet.NodeSet[ CaseIndex ][ 2 ] );
				string Const = ConstExpression.GetExpressionValue( ConstIndex, Deploy.UnitName );
				
				if( SST.ModeC ) {
					SST.Add( "case " + Const + ":\n" );
				}
				if( SST.ModeV ) {
					//SST.Add( "\t\t" + Const + ":\n" );
					
					case_list += "\t\t" + Const + ": " + Deploy.V_NamePC() + " <= " + (Deploy.V_PC) + ";\n";
				}
				
				string label = Config.PreLabel.Case + ControlIndex + "_" + i;
				Expression.AddIns( Code1.标号 + " " + label );
				int CaseSenceIndex = int.Parse( ParseNet.NodeSet[ CaseIndex ][ 4 ] );
				Deploy.语句列表( CaseSenceIndex, ControlIndex );
			}
		}
		
		if( SST.ModeV ) {
			SST.V_Replace( "<case_list>", case_list );
			SST.V_Replace( "<switch_end>", Deploy.V_PC.ToString() );
		}
	}
	
	static void if语句( int Index, int CurrentControlIndex )
	{
//		语句层加<<<<
//		有值表达式 2<<<<
//		否定跳转 var? end<<<<
//		带结束语句列表 4<<<<
//		标号 end<<<<
//		语句层减<<<<
		
		int ControlNumber = Deploy.SenceNumber;
		
		Expression.AddIns( Code1.语句层加 );
		int BoolIndex = int.Parse( ParseNet.NodeSet[ Index ][ 3 ] );
		string exp = null;
		int MidVar = 计算条件表达式( BoolIndex, ref exp );
		if( MidVar == -1 ) {
			return;
		}
		
		if( SST.ModeC ) {
			SST.Add( "if( " + exp + " ) {\n" );
		}
		if( SST.ModeV ) {
			SST.Add( Expression.V_Add( "if( " + exp + " ) ; else " + Deploy.V_NamePC() + " <= $IF" + ControlNumber + "$;" ) );
			Deploy.V_PC++;
		}
		
		Expression.AddIns( Code1.否定跳转 + " # " + MidVar + " " + "#" + Config.PreLabel.End + ControlNumber );
		
		int MainSenceIndex = int.Parse( ParseNet.NodeSet[ Index ][ 5 ] );
		Deploy.语句( MainSenceIndex, CurrentControlIndex );
		Expression.AddIns( Code1.标号 + " " + Config.PreLabel.End + ControlNumber );
		Expression.AddIns( Code1.语句层减 );
		
		if( SST.ModeC ) {
			SST.Add( "}\n" );
		}
		if( SST.ModeV ) {
			SST.V_Replace( "$IF" + ControlNumber + "$", Deploy.V_PC.ToString() );
		}
	}
	
	static void ifelse语句( int Index, int CurrentControlIndex )
	{
//		语句层加<<<<
//		有值表达式 1<<<<
//		否定跳转 var? else<<<<
//		语句列表 3<<<<
//		跳转 end<<<<
//		标号 else<<<<
//		带结束语句列表 5<<<<
//		标号 end<<<<
//		语句层减<<<<
		
		int ControlNumber = Deploy.SenceNumber;
		
		string exp = null;
		Expression.AddIns( Code1.语句层加 );
		int BoolIndex = int.Parse( ParseNet.NodeSet[ Index ][ 3 ] );
		int MidVar = 计算条件表达式( BoolIndex, ref exp );
		if( MidVar == -1 ) {
			return;
		}
		if( SST.ModeC ) {
			SST.Add( "if( " + exp + " ) {\n" );
		}
		if( SST.ModeV ) {
			SST.Add( Expression.V_Add( "if( " + exp + " ) ; else " + Deploy.V_NamePC() + " <= $IF" + ControlNumber + "$;" ) );
			Deploy.V_PC++;
		}
		
		Expression.AddIns( Code1.否定跳转 + " # " + MidVar + " " + "#" + Config.PreLabel.Else + ControlNumber );
		
		//这里特殊处理局部变量, 防止在else分支中可见
		//++Deploy.VarLevel;
		int IfSenceIndex = int.Parse( ParseNet.NodeSet[ Index ][ 5 ] );
		Deploy.语句( IfSenceIndex, CurrentControlIndex );
		//--Deploy.VarLevel;
		VarList.ClearUselessVar( Deploy.VarLevel - 1 );
		
		if( SST.ModeC ) {
			SST.Add( "}\n" );
			SST.Add( "else {\n" );
		}
		if( SST.ModeV ) {
			SST.Add( Expression.V_Add( Deploy.V_NamePC() + " <= $IF" + ControlNumber + "_1$;" ) );
			Deploy.V_PC++;
			
			SST.V_Replace( "$IF" + ControlNumber + "$", Deploy.V_PC.ToString() );
		}
		
		Deploy.跳转( Config.PreLabel.End + ControlNumber );
		Expression.AddIns( Code1.标号 + " " + Config.PreLabel.Else + ControlNumber );
		int ElseSenceIndex = int.Parse( ParseNet.NodeSet[ Index ][ 7 ] );
		Deploy.语句( ElseSenceIndex, CurrentControlIndex );
		Expression.AddIns( Code1.标号 + " " + Config.PreLabel.End + ControlNumber );
		Expression.AddIns( Code1.语句层减 );
		
		if( SST.ModeC ) {
			SST.Add( "}\n" );
		}
		if( SST.ModeV ) {
			SST.V_Replace( "$IF" + ControlNumber + "_1$", Deploy.V_PC.ToString() );
		}
	}
	
	//迭代循环
	static void for语句( int Index )
	{
		int ControlIndex = Deploy.SenceNumber;
		
		//获取主语句索引
		int MainSenceIndex = int.Parse( ParseNet.NodeSet[ Index ][ 9 ] );
		
		Expression.AddIns( Code1.语句层加 );
		
		string exp_1 = null;
		int InitIndex = int.Parse( ParseNet.NodeSet[ Index ][ 3 ] );
		if( InitIndex != -1 ) {
			Deploy.无值表达式( InitIndex, ref exp_1 );
		}
		
		Expression.AddIns( Code1.标号 + " " + Config.PreLabel.Start + ControlIndex );
		
		string exp = null;
		int BoolIndex = int.Parse( ParseNet.NodeSet[ Index ][ 5 ] );
		if( BoolIndex != -1 ) {
			int MidVar = 计算条件表达式( BoolIndex, ref exp );
			if( MidVar == -1 ) {
				return;
			}
			
			SST.Add( "for( " + exp_1 + "; " + exp + "; ) {\n" );
			
			Expression.AddIns( Code1.否定跳转 + " # " + MidVar + " " + "#" + Config.PreLabel.End + ControlIndex );
		}
		Expression.AddIns( Code1.标号 + " " + Config.PreLabel.Head + ControlIndex );
		Deploy.语句( MainSenceIndex, ControlIndex );
		Expression.AddIns( Code1.标号 + " " + Config.PreLabel.Continue + ControlIndex );
		
		int ExIndex = int.Parse( ParseNet.NodeSet[ Index ][ 7 ] );
		
		if( ExIndex != -1 ) {
			
			if( ParseNet.NodeSet[ ExIndex ][ 0 ] == ParseNet.Node.后置单目表达式 ) {
				Expression.后置单目语句( ExIndex );
			}
			else {
				string exp_2 = null;
				Deploy.无值表达式( ExIndex, ref exp_2 );
				SST.Add( exp_2 + ";\n" );
			}
		}
		Deploy.跳转( Config.PreLabel.Start + ControlIndex );
		
		Expression.AddIns( Code1.标号 + " " + Config.PreLabel.End + ControlIndex );
		
		Expression.AddIns( Code1.语句层减 );
		
		SST.Add( "}\n" );
	}
	
	//循环次数
	static void loop语句( int Index )
	{
		int ControlIndex = Deploy.SenceNumber;
		
		//获取主语句索引
		int MainSenceIndex = int.Parse( ParseNet.NodeSet[ Index ][ 5 ] );
		
		Expression.AddIns( Code1.语句层加 );
		
		string exp = null;
		int NumberIndex = int.Parse( ParseNet.NodeSet[ Index ][ 3 ] );
		Expression.isPad = true;
		int MidVar = 计算整型表达式( NumberIndex, ref exp );
		if( MidVar == -1 ) {
			return;
		}
		
		int V_StPC = 0;
		string V_LoopVar = "LP_Var" + ControlIndex;
		if( SST.ModeV ) {
			
			SST.AddVar( "reg[31:0] " + V_LoopVar + ";\n" );
			
			SST.Add( Expression.V_Add( V_LoopVar + " <= " + exp + ";" ) );
			Deploy.V_PC += 1;
			
			V_StPC = Deploy.V_PC;
			
			SST.Add( Expression.V_Add( "if( " + V_LoopVar + " != 0 ) " + V_LoopVar + " <= " + V_LoopVar + " - 1; else " + Deploy.V_NamePC() + " <= $FS" + ControlIndex + "$;" ) );
			
			Deploy.V_PC += 1;
		}
		//判断是否为用户空间, 是的话需要设置为静态变量
		else if( n_FunctionList.FunctionList.Get( Deploy.FunctionIndex ).InterType == MemberType.FunctionType.Task ) { //n_Param.Param.UserspaceOn ) {
			
			string tname = SST.GetFullName( "#._cx_sc_" + Deploy.SVarTempIdx );
			Deploy.SVarTempIdx++;
			
			string tnum = SST.GetFullName( "#._cx_sc_" + Deploy.SVarTempIdx );
			Deploy.SVarTempIdx++;
			
			//应定义为全局变量
			SST.AddVar( SST.PRE_t + "uint16 " + tname + ";\n" );
			SST.AddVar( SST.PRE_t + "uint16 " + tnum + ";\n" ); //循环次数也要设置为全局的
			
			SST.Add( tnum + " = " + exp + ";\n" ); //循环次数也要设置为全局的
			SST.Add( "for( " + tname + " = 0; " + tname + " < " + tnum + "; " + tname + "++ ) {\n" );
		}
		else {
			string tname = "_cx_lc_" + Deploy.LVarTempIdx;
			Deploy.LVarTempIdx++;
			
			string tnum = "_cx_lc_" + Deploy.LVarTempIdx;
			Deploy.LVarTempIdx++;
			
			if( SST.Mode_C90 ) {
				
				SST.AddFuncPre( SST.PRE_t + "uint16 " + tname + ";\n" );
				SST.AddFuncPre( SST.PRE_t + "uint16 " + tnum + ";\n" );
				
				SST.Add( tnum + " = " + exp + ";\n" );
				SST.Add( "for( " + tname + " = 0; " + tname + " < " + tnum + "; " + tname + "++ ) {\n" );
			}
			else {
				SST.Add( SST.PRE_t + "uint16 " + tnum + " = " + exp + ";\n" );
				SST.Add( "for( " + SST.PRE_t + "uint16 " + tname + " = 0; " + tname + " < " + tnum + "; " + tname + "++ ) {\n" );
			}
		}
		
		Expression.AddIns( Code1.标号 + " " + Config.PreLabel.Start + ControlIndex );
		
		Expression.AddIns( Code1.跳转递减 + " " + MidVar + " " + MidVar + " #" + Config.PreLabel.End + ControlIndex );
		
		Expression.AddIns( Code1.标号 + " " + Config.PreLabel.Head + ControlIndex );
		Deploy.语句( MainSenceIndex, ControlIndex );
		Expression.AddIns( Code1.标号 + " " + Config.PreLabel.Continue + ControlIndex );
		
		Deploy.跳转( Config.PreLabel.Start + ControlIndex );
		
		Expression.AddIns( Code1.标号 + " " + Config.PreLabel.End + ControlIndex );
		
		Expression.AddIns( Code1.语句层减 );
		
		if( SST.ModeC ) {
			SST.Add( "}\n" );
		}
		else {
			SST.Add( Expression.V_Add( Deploy.V_NamePC() + " <= " + V_StPC + ";" ) );
			Deploy.V_PC += 1;
			
			int V_EndPC = Deploy.V_PC;
			
			//这里进行替换....
			SST.V_Replace( "$FS" + ControlIndex + "$", V_EndPC.ToString() );
		}
	}
	
	//当循环
	static void while语句( int Index )
	{
		int ControlIndex = Deploy.SenceNumber;
		
		//获取主语句索引
		int MainSenceIndex = int.Parse( ParseNet.NodeSet[ Index ][ 5 ] );
		
		Expression.AddIns( Code1.语句层加 );
		
		Expression.AddIns( Code1.标号 + " " + Config.PreLabel.Start + ControlIndex );
		
		string exp = null;
		int BoolIndex = int.Parse( ParseNet.NodeSet[ Index ][ 3 ] );
		if( BoolIndex != -1 ) {
			int MidVar = 计算条件表达式( BoolIndex, ref exp );
			if( MidVar == -1 ) {
				return;
			}
			Expression.AddIns( Code1.否定跳转 + " # " + MidVar + " " + "#" + Config.PreLabel.End + ControlIndex );
		}
		
		int StartPC = Deploy.V_PC;
		if( SST.ModeC ) {
			SST.Add( "while( " + exp + " ) {\n" );
		}
		if( SST.ModeV ) {
			SST.Add( Expression.V_Add( "if( " + exp + " ) ; else " + Deploy.V_NamePC() + " <= $WHILE" + ControlIndex + "$;" ) );
			Deploy.V_PC++;
		}
		
		Expression.AddIns( Code1.标号 + " " + Config.PreLabel.Head + ControlIndex );
		Deploy.语句( MainSenceIndex, ControlIndex );
		Expression.AddIns( Code1.标号 + " " + Config.PreLabel.Continue + ControlIndex );
		
		Deploy.跳转( Config.PreLabel.Start + ControlIndex );
		
		Expression.AddIns( Code1.标号 + " " + Config.PreLabel.End + ControlIndex );
		
		Expression.AddIns( Code1.语句层减 );
		
		if( SST.ModeC ) {
			SST.Add( "}\n" );
		}
		if( SST.ModeV ) {
			SST.Add( Expression.V_Add( Deploy.V_NamePC() + " <= " + StartPC + ";" ) );
			Deploy.V_PC++;
			
			SST.V_Replace( "$WHILE" + ControlIndex + "$", Deploy.V_PC.ToString() );
		}
	}
	
	//循环当
	static void dowhile语句( int Index )
	{
		int ControlIndex = Deploy.SenceNumber;
		
		//获取主语句索引
		int MainSenceIndex = int.Parse( ParseNet.NodeSet[ Index ][ 2 ] );
		
		if( SST.ModeC ) {
			SST.Add( "do {\n" );
		}
		int StartPC = Deploy.V_PC;
		
		Expression.AddIns( Code1.语句层加 );
		
		Expression.AddIns( Code1.标号 + " " + Config.PreLabel.Start + ControlIndex );
		
		Expression.AddIns( Code1.标号 + " " + Config.PreLabel.Head + ControlIndex );
		Deploy.语句( MainSenceIndex, ControlIndex );
		Expression.AddIns( Code1.标号 + " " + Config.PreLabel.Continue + ControlIndex );
		
		string exp = null;
		int BoolIndex = int.Parse( ParseNet.NodeSet[ Index ][ 5 ] );
		if( BoolIndex != -1 ) {
			int MidVar = 计算条件表达式( BoolIndex, ref exp );
			if( MidVar == -1 ) {
				return;
			}
			Expression.AddIns( Code1.否定跳转 + " # " + MidVar + " " + "#" + Config.PreLabel.End + ControlIndex );
		}
		Deploy.跳转( Config.PreLabel.Start + ControlIndex );
		
		if( SST.ModeC ) {
			SST.Add( "}\n" );
			SST.Add( "while( " + exp + " );\n" );
		}
		if( SST.ModeV ) {
			SST.Add( Expression.V_Add( "if( " + exp + " ) " + Deploy.V_NamePC() + " <= " + StartPC + ";" ) );
			Deploy.V_PC++;
		}
		
		Expression.AddIns( Code1.标号 + " " + Config.PreLabel.End + ControlIndex );
		
		Expression.AddIns( Code1.语句层减 );
	}
	
	//单次循环
	static void once语句( int Index )
	{
//		语句层加<<<<
//		带结束语句列表 2<<<<
//		标号 end<<<<
//		语句层减<<<<
		
		int ControlNumber = Deploy.SenceNumber;
		int ControlIndex = Deploy.SenceNumber;
		
		Expression.AddIns( Code1.语句层加 );
		
		int MainSenceIndex = int.Parse( ParseNet.NodeSet[ Index ][ 2 ] );
		
		Expression.AddIns( Code1.标号 + " " + Config.PreLabel.Head + ControlIndex );
		Deploy.语句( MainSenceIndex, ControlIndex );
		Expression.AddIns( Code1.标号 + " " + Config.PreLabel.Continue + ControlIndex );
		Expression.AddIns( Code1.标号 + " " + Config.PreLabel.End + ControlNumber );
		
		Expression.AddIns( Code1.语句层减 );
	}
	
	//并行语句流程
	static void 并行语句( int Index )
	{
//		语句层加<<<<
//		带结束语句列表 2<<<<
//		标号 end<<<<
//		语句层减<<<<
		
		int ControlNumber = Deploy.SenceNumber;
		int ControlIndex = Deploy.SenceNumber;
		
		Expression.AddIns( Code1.语句层加 );
		
		int ErrorIndex = int.Parse( ParseNet.NodeSet[ Index ][ 1 ] );
		
		//可能不需要判断-1 (不会出现这个情况)
		int MainSenceIndex = int.Parse( ParseNet.NodeSet[ Index ][ 3 ] );
		if( MainSenceIndex != -1 ) {
			
			SST.Add( Expression.V_Add0("begin " ) );
			
			isPara = true;
			
			Deploy.语句列表( MainSenceIndex, ControlIndex );
			
			isPara = false;
			
			SST.Add( "end\n" );
			Deploy.V_PC += 1;
		}
		
		Expression.AddIns( Code1.语句层减 );
	}
	
	//用户自定义流程
	static void 用户流程语句( int Index )
	{
//		语句层加<<<<
//		带结束语句列表 2<<<<
//		标号 end<<<<
//		语句层减<<<<
		
		int ControlNumber = Deploy.SenceNumber;
		int ControlIndex = Deploy.SenceNumber;
		
		Expression.AddIns( Code1.语句层加 );
		
		string Name = null;
		int ErrorIndex = -1;
		int NameIndex = int.Parse( ParseNet.NodeSet[ Index ][ 1 ] );
		if( NameIndex != -1 ) {
			n_Parse.Parse.GetMemberNames( Deploy.UnitName, NameIndex, ref Name, ref ErrorIndex );
		}
		if( Name != null ) {
			//处理接口函数调用
			int CallFunctionIndex = n_FunctionList.FunctionList.GetIndex( Name + ".enter" );
			if( CallFunctionIndex == -1 ) {
				ET.WriteParseError( ErrorIndex, "未找到自定义流程的 enter 函数: " + Name );
			}
			else {
				Expression.AddIns( Code1.堆栈递增 + " " + "#" + Deploy.FunctionIndex );
				Expression.AddIns( Code1.函数调用 + " " + CallFunctionIndex );
				Expression.AddIns( Code1.堆栈递减 + " " + "#" + Deploy.FunctionIndex );
				n_CallList.CallList.AddSubFunction( Deploy.FunctionIndex, CallFunctionIndex.ToString() );
			}
			Expression.AddIns( Code1.系统标志 + " " + n_Param.Param.Safe );
		}
		
		Expression.AddIns( Code1.标号 + " " + Config.PreLabel.Head + ControlIndex );
		
		//可能不需要判断-1 (不会出现这个情况)
		int MainSenceIndex = int.Parse( ParseNet.NodeSet[ Index ][ 3 ] );
		if( MainSenceIndex != -1 ) {
			Deploy.语句列表( MainSenceIndex, ControlIndex );
		}
		Expression.AddIns( Code1.标号 + " " + Config.PreLabel.Continue + ControlIndex );
		Expression.AddIns( Code1.标号 + " " + Config.PreLabel.End + ControlNumber );
		
		if( Name != null ) {
			//处理接口函数调用
			int CallFunctionIndex = n_FunctionList.FunctionList.GetIndex( Name + ".leave" );
			if( CallFunctionIndex == -1 ) {
				ET.WriteParseError( ErrorIndex, "未找到自定义流程的 leaver 函数: " + Name );
			}
			else {
				Expression.AddIns( Code1.堆栈递增 + " " + "#" + Deploy.FunctionIndex );
				Expression.AddIns( Code1.函数调用 + " " + CallFunctionIndex );
				Expression.AddIns( Code1.堆栈递减 + " " + "#" + Deploy.FunctionIndex );
				n_CallList.CallList.AddSubFunction( Deploy.FunctionIndex, CallFunctionIndex.ToString() );
			}
			Expression.AddIns( Code1.系统标志 + " " + n_Param.Param.Unsafe );
		}
		
		Expression.AddIns( Code1.语句层减 );
	}
	
	//无限循环
	static void forever语句( int Index )
	{	
		int ControlNumber = Deploy.SenceNumber;
		int ControlIndex = Deploy.SenceNumber;
		
		int V_StPC = Deploy.V_PC;
		if( SST.ModeC ) {
			SST.Add( "while( true ) {\n" );
		}
		
		Expression.AddIns( Code1.语句层加 );
		int MainSenceIndex = int.Parse( ParseNet.NodeSet[ Index ][ 2 ] );
		
		Expression.AddIns( Code1.标号 + " " + Config.PreLabel.Start + ControlIndex );
		Expression.AddIns( Code1.标号 + " " + Config.PreLabel.Head + ControlIndex );
		Deploy.语句( MainSenceIndex, ControlIndex );
		Expression.AddIns( Code1.标号 + " " + Config.PreLabel.Continue + ControlIndex );
		Deploy.跳转( Config.PreLabel.Start + ControlIndex );
		Expression.AddIns( Code1.标号 + " " + Config.PreLabel.End + ControlNumber );
		
		Expression.AddIns( Code1.语句层减 );
		
		if( SST.ModeC ) {
			SST.Add( "}\n" );
		}
		if( SST.ModeV ) {
			SST.Add( Expression.V_Add( Deploy.V_NamePC() + " <= " + V_StPC + ";" ) );
			Deploy.V_PC += 1;
		}
	}
	
	static int 计算整型表达式( int ExpIndex, ref string exp )
	{
		if( ExpIndex == -1 ) {
			return -1;
		}
		int MidVar = Expression.表达式( ExpIndex, ref exp );
		if( MidVar == -1 ) {
			return -1;
		}
		string exp_error = null;
		MidVar = Expression.读取宏变量( MidVar, ref exp_error );
		if( MidVar == -1 ) {
			return -1;
		}
		MidVar = Expression.读取方法( MidVar, ref exp_error, "error" );
		if( MidVar == -1 ) {
			return -1;
		}
		string MidType = VarList.Get( MidVar ).Type;
		if( MidType.IndexOf( ',' ) != -1 ) {
			MidType  = MidType.Split( ',' )[ 0 ];
			if( MidType == VarType.BaseType.Bit ) {
				MidType = VarType.BaseType.Uint8;
			}
			VarList.Get( MidVar ).Type = MidType;
		}
		if( !VarType.BaseType.isUint( MidType ) ) {
			//ET.WriteParseError( VarList.Get( MidVar ).Location, "表达式应返回无符号类型: " + MidType );
		}
		if( VarList.Get( MidVar ).ActiveType != "temp" && !SST.ModeV ) {
			VarNode v = new VarNode();
			v.SetTempValue( VarList.Get( MidVar ).Type, Deploy.VarLevel, VarList.Get( MidVar ).Location );
			int vIndex = VarList.Add( v );
			string exp_sub = null;
			Expression.赋值运算( vIndex, MidVar, ref exp_sub, null, null );
			MidVar = vIndex;
		}
		
		if( n_Param.Param.UserspaceOn ) {
			VarList.Get( MidVar ).ActiveType = n_MemberType.MemberType.StoreType.Static;
			Expression.WriteBack();
		}
		else {
			Expression.WriteBack();
			VarList.Get( MidVar ).ActiveType = "local";
		}
		
		return MidVar;
	}
	
	static int 计算条件表达式( int BoolIndex, ref string exp )
	{
		int MidVar = Expression.表达式( BoolIndex, ref exp );
		if( MidVar == -1 ) {
			return -1;
		}
		string exp_m = null;
		MidVar = Expression.读取宏变量( MidVar, ref exp_m );
		if( MidVar == -1 ) {
			return -1;
		}
		string exp_error = null;
		MidVar = Expression.读取方法( MidVar, ref exp_error, "error" );
		if( MidVar == -1 ) {
			return -1;
		}
		string MidType = VarList.Get( MidVar ).Type;
		if( MidType != VarType.BaseType.Bool ) {
			ET.WriteParseError( VarList.Get( MidVar ).Location, "条件表达式应返回判断类型: " + MidType );
			return -1;
		}
		return MidVar;
	}
	
	//--------------------------------------------------------------------
	
	public static void 宏if语句( int Index, int CurrentControlIndex )
	{
		int BoolIndex = int.Parse( ParseNet.NodeSet[ Index ][ 4 ] );
		
		int MidVar = Expression.ConstExp( BoolIndex );
		if( Expression.isConstExpError ) {
			return;
		}
		if( MidVar != 0 ) {
			int MainSenceIndex = int.Parse( ParseNet.NodeSet[ Index ][ 6 ] );
			
			if( CurrentControlIndex == MACRO ) {
				MainSenceIndex = int.Parse( ParseNet.NodeSet[ MainSenceIndex ][ 2 ] );
				RecordMacro.成员列表( MainSenceIndex );
			}
			else {
				Deploy.语句( MainSenceIndex, CurrentControlIndex );
			}
		}
	}
	
	public static void 宏ifelse语句( int Index, int CurrentControlIndex )
	{
		int BoolIndex = int.Parse( ParseNet.NodeSet[ Index ][ 4 ] );
		
		int MidVar = Expression.ConstExp( BoolIndex );
		if( Expression.isConstExpError ) {
			return;
		}
		if( MidVar != 0 ) {
			int MainSenceIndex = int.Parse( ParseNet.NodeSet[ Index ][ 6 ] );
			Deploy.语句( MainSenceIndex, CurrentControlIndex );
		}
		else {
			int MainSenceIndex = int.Parse( ParseNet.NodeSet[ Index ][ 9 ] );
			Deploy.语句( MainSenceIndex, CurrentControlIndex );
		}
	}
	
	public static void 宏loop语句( int Index, int CurrentControlIndex )
	{
		int ExpIndex = int.Parse( ParseNet.NodeSet[ Index ][ 4 ] );
		
		int MidVar = Expression.ConstExp( ExpIndex );
		if( Expression.isConstExpError ) {
			return;
		}
		int MainSenceIndex = int.Parse( ParseNet.NodeSet[ Index ][ 6 ] );
		if( CurrentControlIndex == MACRO ) {
			MainSenceIndex = int.Parse( ParseNet.NodeSet[ MainSenceIndex ][ 2 ] );
		}
		for( int i = 0; i < MidVar; i++ ) {
			
			if( CurrentControlIndex == MACRO ) {
				RecordMacro.成员列表( MainSenceIndex );
			}
			else {
				Deploy.语句( MainSenceIndex, CurrentControlIndex );
			}
		}
	}
	
	//====================================================================
	//过时的
	
	static void switchelse语句( int Index )
	{
		int ControlIndex = Deploy.SenceNumber;
		
		Expression.AddIns( Code1.语句层加 );
		int ExSenceIndex = int.Parse( ParseNet.NodeSet[ Index ][ 3 ] );
		string exp = null;
		int VarIndex = 计算整型表达式( ExSenceIndex, ref exp );
		分支判断( Index, ControlIndex, VarIndex );
		Expression.AddIns( Code1.标号 + " " + Config.PreLabel.End + ControlIndex );
		Expression.AddIns( Code1.语句层减 );
	}
	
	static void 分支判断( int Index, int ControlIndex, int MidVar )
	{
		int n = int.Parse( ParseNet.NodeSet[ Index ][ 6 ] );
		for( int i = 1; i < ParseNet.NodeSet[ n ].Length; ++i ) {
			int CaseIndex = int.Parse( ParseNet.NodeSet[ n ][ i ] );
			
			//获取运算符号
			int OperIndex = int.Parse( ParseNet.NodeSet[ CaseIndex ][ 1 ] );
			string Oper = WordList.GetWord( OperIndex );
			string exp = null;
			int Exp = Expression.表达式( int.Parse( ParseNet.NodeSet[ CaseIndex ][ 2 ] ), ref exp );
			if( Exp == -1 ) {
				return;
			}
			string exp_m1 = null;
			Exp = Expression.读取宏变量( Exp, ref exp_m1 );
			string exp_error1 = null;
			string exp_error2 = null;
			int RetIndex = Expression.双目解析运算( Oper, Exp, MidVar, ref exp_error1, ref exp_error2 );
			if( RetIndex == -1 ) {
				return;
			}
			string exp_m2 = null;
			RetIndex = Expression.读取宏变量( RetIndex, ref exp_m2 );
			if( MidVar == -1 ) {
				return;
			}
			string exp_error = null;
			RetIndex = Expression.读取方法( RetIndex, ref exp_error, "error" );
			if( MidVar == -1 ) {
				return;
			}
			string MidType = VarList.Get( RetIndex ).Type;
			if( MidType != VarType.BaseType.Bool ) {
				ET.WriteParseError( VarList.Get( RetIndex ).Location, "<分支判断> 表达式应返回判断类型: " + MidType );
			}
			Expression.AddIns( Code1.否定跳转 + " # " + RetIndex + " #" + Config.PreLabel.Case + ControlIndex + "_" + i );
			int CaseSenceIndex = int.Parse( ParseNet.NodeSet[ CaseIndex ][ 4 ] );
			Deploy.语句列表( CaseSenceIndex, ControlIndex );
			Deploy.跳转( Config.PreLabel.End + ControlIndex );
			Expression.AddIns( Code1.标号 + " " + Config.PreLabel.Case + ControlIndex + "_" + i );
		}
		int ElseIndex = int.Parse(ParseNet.NodeSet[ Index ][ 9 ] );
		Deploy.语句列表( ElseIndex, ControlIndex );
	}
	
	static void repeat语句( int Index )
	{
		int ControlIndex = Deploy.SenceNumber;
		
		int ExSenceIndex = int.Parse( ParseNet.NodeSet[ Index ][ 2 ] );
		int InitIndex = -1;
		int ExIndex = -1;
		int BoolIndex = -1;
		int TimesIndex = -1;
		获取流程辅助语句( ExSenceIndex, ref InitIndex, ref ExIndex, ref BoolIndex, ref TimesIndex );
		
		//获取主语句索引
		int MainSenceIndex = int.Parse( ParseNet.NodeSet[ Index ][ 3 ] );
		
		Expression.AddIns( Code1.语句层加 );
		string exp_n = null;
		if( InitIndex != -1 ) {
			Deploy.无值表达式( InitIndex, ref exp_n );
		}
		string exp = null;
		int VarIndex = 计算整型表达式( TimesIndex, ref exp );
		Expression.AddIns( Code1.标号 + " " + Config.PreLabel.Start + ControlIndex );
		if( BoolIndex != -1 ) {
			int MidVar = 计算条件表达式( BoolIndex, ref exp );
			if( MidVar == -1 ) {
				return;
			}
			Expression.AddIns( Code1.肯定跳转 + " # " + MidVar + " " + "#" + Config.PreLabel.End + ControlIndex );
		}
		if( VarIndex != -1 ) {
			Expression.AddIns( Code1.跳转递减 + " " + VarIndex + " " + VarIndex + " #" + Config.PreLabel.End + ControlIndex );
		}
		Expression.AddIns( Code1.标号 + " " + Config.PreLabel.Head + ControlIndex );
		Deploy.语句( MainSenceIndex, ControlIndex );
		Expression.AddIns( Code1.标号 + " " + Config.PreLabel.Continue + ControlIndex );
		string exp_n1 = null;
		if( ExIndex != -1 ) {
			Deploy.无值表达式( ExIndex, ref exp_n1 );
		}
		Deploy.跳转( Config.PreLabel.Start + ControlIndex );
		Expression.AddIns( Code1.标号 + " " + Config.PreLabel.End + ControlIndex );
		Expression.AddIns( Code1.语句层减 );
	}
	
	static void 获取流程辅助语句( int ExSenceIndex, ref int InitIndex, ref int ExIndex, ref int BoolIndex, ref int TimesIndex )
	{
		int Length = ParseNet.NodeSet[ ExSenceIndex ].Length;
		for( int i = 1; i < Length; ++i ) {
			int ExSenceSubIndex = int.Parse( ParseNet.NodeSet[ ExSenceIndex ][ i ] );
			
			if( ParseNet.NodeSet[ ExSenceSubIndex ][ 0 ] == ParseNet.Node.流程初始化 ) {
				if( InitIndex != -1 ) {
					int ErrIndex = int.Parse( ParseNet.NodeSet[ ExSenceSubIndex ][ 1 ] );
					ET.WriteParseError( ErrIndex, "<获取流程辅助语句> 当前控制语句中已经定义了初始化语句" );
				}
				InitIndex = int.Parse( ParseNet.NodeSet[ ExSenceSubIndex ][ 2 ] );
			}
			else if( ParseNet.NodeSet[ ExSenceSubIndex ][ 0 ] == ParseNet.Node.流程附加操作 ) {
				if( ExIndex != -1 ) {
					int ErrIndex = int.Parse( ParseNet.NodeSet[ ExSenceSubIndex ][ 1 ] );
					ET.WriteParseError( ErrIndex, "<获取流程辅助语句> 当前控制语句中已经定义了附加语句" );
				}
				ExIndex = int.Parse( ParseNet.NodeSet[ ExSenceSubIndex ][ 2 ] );
			}
			else if( ParseNet.NodeSet[ ExSenceSubIndex ][ 0 ] == ParseNet.Node.流程条件判断 ) {
				if( BoolIndex != -1 || TimesIndex != -1 ) {
					int ErrIndex = int.Parse( ParseNet.NodeSet[ ExSenceSubIndex ][ 1 ] );
					ET.WriteParseError( ErrIndex, "<获取流程辅助语句> 当前控制语句中已经定义了条件语句" );
				}
				BoolIndex = int.Parse( ParseNet.NodeSet[ ExSenceSubIndex ][ 2 ] );
			}
			else if( ParseNet.NodeSet[ ExSenceSubIndex ][ 0 ] == ParseNet.Node.流程次数判断 ) {
				if( BoolIndex != -1 || TimesIndex != -1 ) {
					int ErrIndex = int.Parse( ParseNet.NodeSet[ ExSenceSubIndex ][ 2 ] );
					ET.WriteParseError( ErrIndex, "<获取流程辅助语句> 当前控制语句中已经定义了条件语句" );
				}
				TimesIndex = int.Parse( ParseNet.NodeSet[ ExSenceSubIndex ][ 1 ] );
			}
			else {
				ET.ShowError( "<获取流程辅助语句> 未定义的'反复执行'语句附加项: " + ParseNet.NodeSet[ ExSenceSubIndex ][ 0 ] );
			}
		}
	}
}
}




