﻿
using System;
using n_FunctionList;

namespace n_GateTree
{
public static class GateTree
{
	public static LGate[] GateList;
	public static int Length;
	
	//初始化
	public static void Init()
	{
		GateList = new LGate[100];
		Length = 0;
	}
	
	//清空
	public static void Clear()
	{
		Length = 0;
	}
	
	//添加一个逻辑门
	public static void Add()
	{
		LGate lg = new LGate();
		lg.SX = 200;
		lg.SY = 500 + Length * 30;
		GateList[Length] = lg;
		Length++;
	}
}
public class LGate
{
	public int SX;
	public int SY;
	public int Width;
	public int Height;
	
	public LGate()
	{
		Width = 20;
		Height = 20;
	}
}
}


