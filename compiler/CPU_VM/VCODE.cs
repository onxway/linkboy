﻿
namespace n_VCODE
{
	using System;
	
	public static class COM
	{
		//2016.5.7
		//优化虚拟机, 注意跟这个数字相关的还有两个地方:
		//1是字节码转换 AddCode 中,
		// (OK 不需要了) (2是设置指令集调整地址时(行数转为字节地址)也要相应调整)
		
		//2016.6.9
		//调试了两天, 终于实现了PC 4字节到3字节的转换. 最终原因是, 中断函数是32个, 根据PC宽度的不同, CODE常量的起始地址也需要跟着设置
		
		//常量 - 占据4个字节
		public const int ConstByteNumber = 4;
		
		//接口参数 - 占据4个字节
		public const int InterfaceByteNumber = 4;
		
		//函数地址 - 占据3个字节
		public static uint GotoByteNumber = 3;
		
		//域信息 - 占据3个字节
		public static int RETYByteNumber = 3;
		
		//变量地址 - 占据2个字节
		public const int VarAddrByteNumber = 2;
		
		//读写分量 - 占据1个字节
		public const int SubValueByteNumber = 1;
		
		//无参数
		public const int None = 0;
	}
	
	public static class C
	{
		//222 隐式转换 fix int32
		//223: *_fix_fix_fix
		//224: /_fix_fix_fix
		//225: 转移参数
		//232: N_SETB_fix
		//233 隐式转换_sint32_fix   //已支持    注意STM32虚拟机中暂不支持此指令
		//public const byte N_分支比较_uint32_uint8 =	234;
		//public const byte N_分支散转_uint32 =	235;
		// ************* 取消 - public const byte N_SYS =	236;
		// 236 - 237 fix <- int8/16
		// 238: 自加
		// 239: 自减
		
		// 240: 读取PC
		//未用 241
		
		//临时 255 单步自陷指令
		
		public const byte N_STEP =		255;
			public const string 单步 =	"step";
		
		public const byte N_域返回 =		240;
			public const string 域返回 =	"域返回";
			
		public const string SYS_PAD =	"指令对齐";
		
		public const byte N_CLI =		220;
			public const string CLI =		"cli";
		public const byte N_SEI =		221;
			public const string SEI =		"sei";
		
		public const string CODE =	"CODE";
		public const string 中断未用跳转 =	"中断未用跳转";
		
		public const byte N_NULL =	0;
		
		public const byte N_接口调用 =	1;
			public const string 接口调用 =	"bind";
		
		public const byte N_设置堆栈 =	2;
			public const string 设置堆栈 =	"设置堆栈";
		
		public const byte N_跳转 =		3;
			public const string 跳转 =		"跳转";
		
		public const byte N_函数调用 =	4;
			public const string 函数调用 =	"函数调用";
		
		public const byte N_返回 =		5;
			public const string 返回 =		"返回";
		
		public const byte N_中断返回 =	6;
			public const string 中断返回 =	"中断返回";
		
		public const byte N_堆栈递增 =	7;
			public const string 堆栈递增 =	"堆栈递增";
		
		public const byte N_堆栈递减 =	8;
			public const string 堆栈递减 =	"堆栈递减";
		
		public const byte N_转移形参 =	9;
			public const string 转移形参 =	"转移形参";
		
		public const byte N_保护现场 =	10;
			public const string 保护现场 =	"保护现场";
		
		public const byte N_恢复现场 =	11;
			public const string 恢复现场 =	"恢复现场";
		
		public const byte N_空指令 =		12; //0
			public const string 空指令 =	"空指令";
			public const string 空指令1 =	"nop";
		
		public const byte N_出栈 =	13;
			public const string 出栈 =	"出栈";
		
		public const byte N_压栈 =	14;
			public const string 压栈 =	"压栈";
		
		public const byte N_转移参数 =	225;
			public const string 转移参数 =	"转移参数";
		
		public const byte N_SETA_uint8 =	15;
			public const string 常量赋值_bit =	"常量赋值_bit";
			public const string 常量赋值_bool =	"常量赋值_bool";
			public const string 常量赋值_uint8 =	"常量赋值_uint8";
		
		public const byte N_SETA_uint16 =	205;
			public const string 常量赋值_uint16 =	"常量赋值_uint16";
		public const byte N_SETA_uint32 =	206;
			public const string 常量赋值_uint32 =	"常量赋值_uint32";
		public const byte N_SETA_sint8 =	207;
			public const string 常量赋值_sint8 =	"常量赋值_sint8";
		public const byte N_SETA_sint16 =	208;
			public const string 常量赋值_sint16 =	"常量赋值_sint16";
		public const byte N_SETA_sint32 =	209;
			public const string 常量赋值_sint32 =	"常量赋值_sint32";
		public const byte N_SETA_fix =	209;
			public const string 常量赋值_fix =	"常量赋值_fix";
		
		public const byte N_SETB_uint8 =	226;
			public const string B常量赋值_bit =	"B常量赋值_bit";
			public const string B常量赋值_bool =	"B常量赋值_bool";
			public const string B常量赋值_uint8 =	"B常量赋值_uint8";
		
		public const byte N_SETB_uint16 =	227;
			public const string B常量赋值_uint16 =	"B常量赋值_uint16";
		public const byte N_SETB_uint32 =	228;
			public const string B常量赋值_uint32 =	"B常量赋值_uint32";
		public const byte N_SETB_sint8 =	229;
			public const string B常量赋值_sint8 =	"B常量赋值_sint8";
		public const byte N_SETB_sint16 =	230;
			public const string B常量赋值_sint16 =	"B常量赋值_sint16";
		public const byte N_SETB_sint32 =	231;
			public const string B常量赋值_sint32 =	"B常量赋值_sint32";
		public const byte N_SETB_fix =	232;
			public const string B常量赋值_fix =	"B常量赋值_fix";
		
			public const string 读取地址_code_uint32 =		"读取地址_code_uint32";
			public const string 读取地址_static_uint32 =		"读取地址_static_uint32";
			public const string 读取地址_vdata_uint8 =		"读取地址_vdata_uint8";
			public const string 读取地址_vdata_uint16 =		"读取地址_vdata_uint16";
			public const string 读取地址_vdata_uint32 =		"读取地址_vdata_uint32";
		
		public const byte N_读取地址_local_uint32 =		16;
			public const string 读取地址_local_uint32 =		"读取地址_local_uint32";
		
		public const byte N_地址偏移_uintx_uintx_uintx =		17;
			public const string 地址偏移_uint8_uint8_uint8 =			"地址偏移_uint8_uint8_uint8";
			public const string 地址偏移_uint16_uint16_uint16 =		"地址偏移_uint16_uint16_uint16";
			public const string 地址偏移_uint24_uint24_uint24 =		"地址偏移_uint24_uint24_uint24";
			public const string 地址偏移_uint32_uint32_uint32 =		"地址偏移_uint32_uint32_uint32";
		
		public const byte N_结构偏移_uintx_uintx =				18;
			public const string 结构偏移_uint8_uint8 =		"结构偏移_uint8_uint8";
			public const string 结构偏移_uint16_uint16 =		"结构偏移_uint16_uint16";
			public const string 结构偏移_uint24_uint24 =		"结构偏移_uint24_uint24";
			public const string 结构偏移_uint32_uint32 =		"结构偏移_uint32_uint32";
		
		//无指令
			public const string 赋值_bit_bit =		"=_bit_bit";
			public const string 赋值_bool_bool =		"=_bool_bool";
			public const string 赋值_uint8_uint8 =	"=_uint8_uint8";
			public const string 赋值_uint16_uint16 =	"=_uint16_uint16";
			public const string 赋值_uint24_uint24 =	"=_uint24_uint24";
			public const string 赋值_uint32_uint32 =	"=_uint32_uint32";
			public const string 赋值_sint8_sint8 =	"=_sint8_sint8";
			public const string 赋值_sint16_sint16 =	"=_sint16_sint16";
			public const string 赋值_sint24_sint24 =	"=_sint24_sint24";
			public const string 赋值_sint32_sint32 =	"=_sint32_sint32";
			public const string 赋值_fix_fix =	"=_fix_fix";
			
			public const string 隐式转换_uint16_uint8 =		"隐式转换_uint16_uint8";
			public const string 隐式转换_uint24_uint8 =		"隐式转换_uint24_uint8";
			public const string 隐式转换_uint32_uint8 =		"隐式转换_uint32_uint8";
			public const string 隐式转换_uint24_uint16 =	"隐式转换_uint24_uint16";
			public const string 隐式转换_uint32_uint16 =	"隐式转换_uint32_uint16";
			public const string 隐式转换_uint32_uint24 =	"隐式转换_uint32_uint24";
			
		public const byte N_强制转换_sint8_uint8 =				198;
			public const string 强制转换_sint8_uint8 =		"(sint)_sint8_uint8";
		public const byte N_强制转换_sint16_uint16 =				199;
			public const string 强制转换_sint16_uint16 =		"(sint)_sint16_uint16";
		public const byte N_强制转换_sint24_uint24 =				200;
			public const string 强制转换_sint24_uint24 =		"(sint)_sint24_uint24";
		public const byte N_强制转换_sint32_uint32 =				201;
			public const string 强制转换_sint32_uint32 =		"(sint)_sint32_uint32";
		
		public const byte N_强制转换_uint8_sint8 =				216;
			public const string 强制转换_uint8_sint8 =		"(uint)_uint8_sint8";
		public const byte N_强制转换_uint16_sint16 =				217;
			public const string 强制转换_uint16_sint16 =		"(uint)_uint16_sint16";
		public const byte N_强制转换_uint32_sint32 =				219;
			public const string 强制转换_uint32_sint32 =		"(uint)_uint32_sint32";
		public const byte N_强制转换_uint32_fix =					219;
			public const string 强制转换_uint32_fix =		"(uint)_uint32_fix";
		public const byte N_强制转换_fix_uint32 =					219;
			public const string 强制转换_fix_uint32 =		"(fix)_fix_uint32";
			
		public const byte N_隐式转换_sint16_sint8 =				202;
			public const string 隐式转换_sint16_sint8 =		"隐式转换_sint16_sint8";
			//public const string 隐式转换_sint24_sint8 =		"隐式转换_sint24_sint8";
		
		public const byte N_隐式转换_sint32_sint8 =				203;
			public const string 隐式转换_sint32_sint8 =		"隐式转换_sint32_sint8";
			//public const string 隐式转换_sint24_sint16 =		"隐式转换_sint24_sint16";
		
		public const byte N_隐式转换_sint32_sint16 =				204;
			public const string 隐式转换_sint32_sint16 =		"隐式转换_sint32_sint16";
			//public const string 隐式转换_sint32_sint24 =		"隐式转换_sint32_sint24";
		
		public const byte N_隐式转换_fix_sint32 =				222;
			public const string 隐式转换_fix_sint32 =		"隐式转换_fix_sint32";
		public const byte N_隐式转换_fix_sint8 =				236;
			public const string 隐式转换_fix_sint8 =		"隐式转换_fix_sint8";
		public const byte N_隐式转换_fix_sint16 =				237;
			public const string 隐式转换_fix_sint16 =		"隐式转换_fix_sint16";
		
		
		public const byte N_隐式转换_sint32_fix =				233;
			public const string 隐式转换_sint32_fix =		"隐式转换_sint32_fix";
			
		public const byte N_强制转换_uint8_uint16 =				192;
			public const string 强制转换_uint8_uint16 =		"(uint8)_uint8_uint16";
			//public const string 强制转换_uint8_uint24 =		"(uint8)_uint8_uint24";
		
		public const byte N_强制转换_uint8_uint32 =				193;
			public const string 强制转换_uint8_uint32 =		"(uint8)_uint8_uint32";
			//public const string 强制转换_uint16_uint24 =		"(uint8)_uint16_uint24";
		
		public const byte N_强制转换_uint16_uint32 =				194;
			public const string 强制转换_uint16_uint32 =		"(uint16)_uint16_uint32";
			//public const string 强制转换_uint24_uint32 =		"(uint16)_uint24_uint32";
			
		public const byte N_强制转换_sint8_sint16 =				195;
			public const string 强制转换_sint8_sint16 =		"(sint8)_sint8_sint16";
			//public const string 强制转换_sint8_sint24 =		"(sint8)_sint8_sint24";
		
		public const byte N_强制转换_sint8_sint32 =				196;
			public const string 强制转换_sint8_sint32 =		"(sint8)_sint8_sint32";
			//public const string 强制转换_sint16_sint24 =		"(sint8)_sint16_sint24";
		
		public const byte N_强制转换_sint16_sint32 =				197;
			public const string 强制转换_sint16_sint32 =		"(sint16)_sint16_sint32";
			//public const string 强制转换_sint24_sint32 =		"(sint16)_sint24_sint32";
			
		public const byte N_非_bool_bool =			19;
			public const string 非_bool_bool =			"!_bool_bool";
		
		public const byte N_与_bool_bool_bool =		20;
			public const string 与_bool_bool_bool =		"&&_bool_bool_bool";
		
		public const byte N_或_bool_bool_bool =		21;
			public const string 或_bool_bool_bool =		"||_bool_bool_bool";
		
		public const byte N_取负_sint8_sint8 =		22;
			public const string 取负_sint8_sint8 =		"-_sint8_sint8";
		
		public const byte N_取负_sint16_sint16 =		153;
			public const string 取负_sint16_sint16 =		"-_sint16_sint16";
		
		public const byte N_取负_sint24_sint24 =		154;
			public const string 取负_sint24_sint24 =		"-_sint24_sint24";
		
		public const byte N_取负_sint32_sint32 =		155;
			public const string 取负_sint32_sint32 =		"-_sint32_sint32";
		public const byte N_取负_sint32_fix =		155;
			public const string 取负_fix_fix =		"-_fix_fix";
			
		public const byte N_取绝对值_x_x =	23;
			public const string 取绝对值_sint8_sint8 =	"+_sint8_sint8";
			public const string 取绝对值_sint16_sint16 =	"+_sint16_sint16";
			public const string 取绝对值_sint24_sint24 =	"+_sint24_sint24";
			public const string 取绝对值_sint32_sint32 =	"+_sint32_sint32";
			public const string 取绝对值_fix_fix =	"+_fix_fix";
		
		public const byte N_自加_x_x =		238;
			public const string 自加_sint8_sint8 =		"++_sint8_sint8";
			public const string 自加_sint16_sint16 =		"++_sint16_sint16";
			public const string 自加_sint32_sint32 =		"++_sint32_sint32";
			public const string 自加_uint8_uint8 =		"++_uint8_uint8";
			public const string 自加_uint16_uint16 =		"++_uint16_uint16";
			public const string 自加_uint32_uint32 =		"++_uint32_uint32";
		
		public const byte N_自减_x_x =		239;
			public const string 自减_sint8_sint8 =		"--_sint8_sint8";
			public const string 自减_sint16_sint16 =		"--_sint16_sint16";
			public const string 自减_sint32_sint32 =		"--_sint32_sint32";
			public const string 自减_uint8_uint8 =		"--_uint8_uint8";
			public const string 自减_uint16_uint16 =		"--_uint16_uint16";
			public const string 自减_uint32_uint32 =		"--_uint32_uint32";			
		
		public const byte N_加_uint8_uint8_uint8 =		156;
			public const string 加_uint8_uint8_uint8 =		"+_uint8_uint8_uint8";
		
		public const byte N_加_uint16_uint16_uint16 =		157;
			public const string 加_uint16_uint16_uint16 =	"+_uint16_uint16_uint16";
		
		public const byte N_加_uint24_uint24_uint24 =		158;
			public const string 加_uint24_uint24_uint24 =	"+_uint24_uint24_uint24";
		
		public const byte N_加_uint32_uint32_uint32 =		159;
			public const string 加_uint32_uint32_uint32 =	"+_uint32_uint32_uint32";
		
		public const byte N_加_sint8_sint8_sint8 =		160;
			public const string 加_sint8_sint8_sint8 =		"+_sint8_sint8_sint8";
		
		public const byte N_加_sint16_sint16_sint16 =		161;
			public const string 加_sint16_sint16_sint16 =	"+_sint16_sint16_sint16";
		
		public const byte N_加_sint24_sint24_sint24 =		162;
			public const string 加_sint24_sint24_sint24 =	"+_sint24_sint24_sint24";
		
		public const byte N_加_sint32_sint32_sint32 =		163;
			public const string 加_sint32_sint32_sint32 =	"+_sint32_sint32_sint32";
		
		public const byte N_加_fix_fix_fix =		163;
			public const string 加_fix_fix_fix =	"+_fix_fix_fix";
			
		public const byte N_减_uint8_uint8_uint8 =		25;
			public const string 减_uint8_uint8_uint8 =		"-_uint8_uint8_uint8";
		
		public const byte N_减_uint16_uint16_uint16 =		164;
			public const string 减_uint16_uint16_uint16 =	"-_uint16_uint16_uint16";
		
		public const byte N_减_uint24_uint24_uint24 =		165;
			public const string 减_uint24_uint24_uint24 =	"-_uint24_uint24_uint24";
		
		public const byte N_减_uint32_uint32_uint32 =		166;
			public const string 减_uint32_uint32_uint32 =	"-_uint32_uint32_uint32";
		
		public const byte N_减_sint8_sint8_sint8 =		167;
			public const string 减_sint8_sint8_sint8 =		"-_sint8_sint8_sint8";
		
		public const byte N_减_sint16_sint16_sint16 =		168;
			public const string 减_sint16_sint16_sint16 =	"-_sint16_sint16_sint16";
		
		public const byte N_减_sint24_sint24_sint24 =		169;
			public const string 减_sint24_sint24_sint24 =	"-_sint24_sint24_sint24";
		
		public const byte N_减_sint32_sint32_sint32 =		170;
			public const string 减_sint32_sint32_sint32 =	"-_sint32_sint32_sint32";
		
		public const byte N_减_fix_fix_fix =		170;
			public const string 减_fix_fix_fix =	"-_fix_fix_fix";
		
		public const byte N_乘_uint8_uint8_uint8 =		26;
			public const string 乘_uint8_uint8_uint8 =		"*_uint8_uint8_uint8";
		
		public const byte N_乘_uint16_uint16_uint16 =		171;
			public const string 乘_uint16_uint16_uint16 =	"*_uint16_uint16_uint16";
		
		public const byte N_乘_uint24_uint24_uint24 =		172;
			public const string 乘_uint24_uint24_uint24 =	"*_uint24_uint24_uint24";
		
		public const byte N_乘_uint32_uint32_uint32 =		173;
			public const string 乘_uint32_uint32_uint32 =	"*_uint32_uint32_uint32";
		
		public const byte N_乘_sint8_sint8_sint8 =		174;
			public const string 乘_sint8_sint8_sint8 =		"*_sint8_sint8_sint8";
		
		public const byte N_乘_sint16_sint16_sint16 =		175;
			public const string 乘_sint16_sint16_sint16 =	"*_sint16_sint16_sint16";
		
		public const byte N_乘_sint24_sint24_sint24 =		176;
			public const string 乘_sint24_sint24_sint24 =	"*_sint24_sint24_sint24";
		
		public const byte N_乘_sint32_sint32_sint32 =		177;
			public const string 乘_sint32_sint32_sint32 =	"*_sint32_sint32_sint32";
		
		public const byte N_乘_fix_fix_fix =		223;
			public const string 乘_fix_fix_fix =	"*_fix_fix_fix";
		
		public const byte N_除_uint8_uint8_uint8 =		27;
			public const string 除_uint8_uint8_uint8 =		"/_uint8_uint8_uint8";
		
		public const byte N_除_uint16_uint16_uint16 =		178;
			public const string 除_uint16_uint16_uint16 =	"/_uint16_uint16_uint16";
		
		public const byte N_除_uint24_uint24_uint24 =		179;
			public const string 除_uint24_uint24_uint24 =	"/_uint24_uint24_uint24";
		
		public const byte N_除_uint32_uint32_uint32 =		180;
			public const string 除_uint32_uint32_uint32 =	"/_uint32_uint32_uint32";
		
		public const byte N_除_sint8_sint8_sint8 =		181;
			public const string 除_sint8_sint8_sint8 =		"/_sint8_sint8_sint8";
		
		public const byte N_除_sint16_sint16_sint16 =		182;
			public const string 除_sint16_sint16_sint16 =	"/_sint16_sint16_sint16";
		
		public const byte N_除_sint24_sint24_sint24 =		183;
			public const string 除_sint24_sint24_sint24 =	"/_sint24_sint24_sint24";
		
		public const byte N_除_sint32_sint32_sint32 =		184;
			public const string 除_sint32_sint32_sint32 =	"/_sint32_sint32_sint32";
		
		public const byte N_除_fix_fix_fix =		224;
			public const string 除_fix_fix_fix =	"/_fix_fix_fix";
		
		public const byte N_余_uint8_uint8_uint8 =		28;
			public const string 余_uint8_uint8_uint8 =		"%_uint8_uint8_uint8";
		
		public const byte N_余_uint16_uint16_uint16 =		185;
			public const string 余_uint16_uint16_uint16 =	"%_uint16_uint16_uint16";
		
		public const byte N_余_uint24_uint24_uint24 =		186;
			public const string 余_uint24_uint24_uint24 =	"%_uint24_uint24_uint24";
		
		public const byte N_余_uint32_uint32_uint32 =		187;
			public const string 余_uint32_uint32_uint32 =	"%_uint32_uint32_uint32";
		
		public const byte N_余_sint8_sint8_sint8 =		188;
			public const string 余_sint8_sint8_sint8 =		"%_sint8_sint8_sint8";
		
		public const byte N_余_sint16_sint16_sint16 =		189;
			public const string 余_sint16_sint16_sint16 =	"%_sint16_sint16_sint16";
		
		public const byte N_余_sint24_sint24_sint24 =		190;
			public const string 余_sint24_sint24_sint24 =	"%_sint24_sint24_sint24";
		
		public const byte N_余_sint32_sint32_sint32 =		191;
			public const string 余_sint32_sint32_sint32 =	"%_sint32_sint32_sint32";
			
		
		public const byte N_大于_bool_uint_uint =		29;
			public const string 大于_bool_uint8_uint8 =		">_bool_uint8_uint8";
			public const string 大于_bool_uint16_uint16 =	">_bool_uint16_uint16";
			public const string 大于_bool_uint24_uint24 =	">_bool_uint24_uint24";
			public const string 大于_bool_uint32_uint32 =	">_bool_uint32_uint32";
		public const byte N_大于_bool_sint_sint =		210;
			public const string 大于_bool_sint8_sint8 =		">_bool_sint8_sint8";
			public const string 大于_bool_sint16_sint16 =	">_bool_sint16_sint16";
			public const string 大于_bool_sint24_sint24 =	">_bool_sint24_sint24";
			public const string 大于_bool_sint32_sint32 =	">_bool_sint32_sint32";
			public const string 大于_bool_fix_fix =	">_bool_fix_fix";
		
		public const byte N_大于等于_bool_uint_uint =		30;
			public const string 大于等于_bool_uint8_uint8 =		">=_bool_uint8_uint8";
			public const string 大于等于_bool_uint16_uint16 =		">=_bool_uint16_uint16";
			public const string 大于等于_bool_uint24_uint24 =		">=_bool_uint24_uint24";
			public const string 大于等于_bool_uint32_uint32 =		">=_bool_uint32_uint32";
		public const byte N_大于等于_bool_sint_sint =		211;
			public const string 大于等于_bool_sint8_sint8 =		">=_bool_sint8_sint8";
			public const string 大于等于_bool_sint16_sint16 =		">=_bool_sint16_sint16";
			public const string 大于等于_bool_sint24_sint24 =		">=_bool_sint24_sint24";
			public const string 大于等于_bool_sint32_sint32 =		">=_bool_sint32_sint32";
			public const string 大于等于_bool_fix_fix =		">=_bool_fix_fix";
		
		public const byte N_小于_bool_uint_uint =		31;
			public const string 小于_bool_uint8_uint8 =		"<_bool_uint8_uint8";
			public const string 小于_bool_uint16_uint16 =	"<_bool_uint16_uint16";
			public const string 小于_bool_uint24_uint24 =	"<_bool_uint24_uint24";
			public const string 小于_bool_uint32_uint32 =	"<_bool_uint32_uint32";
		public const byte N_小于_bool_sint_sint =		212;
			public const string 小于_bool_sint8_sint8 =		"<_bool_sint8_sint8";
			public const string 小于_bool_sint16_sint16 =	"<_bool_sint16_sint16";
			public const string 小于_bool_sint24_sint24 =	"<_bool_sint24_sint24";
			public const string 小于_bool_sint32_sint32 =	"<_bool_sint32_sint32";
			public const string 小于_bool_fix_fix =	"<_bool_fix_fix";
		
		public const byte N_小于等于_bool_uint_uint =	32;
			public const string 小于等于_bool_uint8_uint8 =	"<=_bool_uint8_uint8";
			public const string 小于等于_bool_uint16_uint16 =	"<=_bool_uint16_uint16";
			public const string 小于等于_bool_uint24_uint24 =	"<=_bool_uint24_uint24";
			public const string 小于等于_bool_uint32_uint32 =	"<=_bool_uint32_uint32";
		public const byte N_小于等于_bool_sint_sint =		213;
			public const string 小于等于_bool_sint8_sint8 =	"<=_bool_sint8_sint8";
			public const string 小于等于_bool_sint16_sint16 =	"<=_bool_sint16_sint16";
			public const string 小于等于_bool_sint24_sint24 =	"<=_bool_sint24_sint24";
			public const string 小于等于_bool_sint32_sint32 =	"<=_bool_sint32_sint32";
			public const string 小于等于_bool_fix_fix =	"<=_bool_fix_fix";
		
		public const byte N_等于_bool_uint_uint =			33;
			public const string 等于_bool_bit_bit =			"==_bool_bit_bit";
			public const string 等于_bool_uint8_uint8 =		"==_bool_uint8_uint8";
			public const string 等于_bool_uint16_uint16 =	"==_bool_uint16_uint16";
			public const string 等于_bool_uint24_uint24 =	"==_bool_uint24_uint24";
			public const string 等于_bool_uint32_uint32 =	"==_bool_uint32_uint32";
		public const byte N_等于_bool_sint_sint =		214;
			public const string 等于_bool_sint8_sint8 =		"==_bool_sint8_sint8";
			public const string 等于_bool_sint16_sint16 =	"==_bool_sint16_sint16";
			public const string 等于_bool_sint24_sint24 =	"==_bool_sint24_sint24";
			public const string 等于_bool_sint32_sint32 =	"==_bool_sint32_sint32";
		
		public const byte N_不等于_bool_uint_uint =		34;
			public const string 不等于_bool_bit_bit =				"!=_bool_bit_bit";
			public const string 不等于_bool_uint8_uint8 =			"!=_bool_uint8_uint8";
			public const string 不等于_bool_uint16_uint16 =		"!=_bool_uint16_uint16";
			public const string 不等于_bool_uint24_uint24 =		"!=_bool_uint24_uint24";
			public const string 不等于_bool_uint32_uint32 =		"!=_bool_uint32_uint32";
		public const byte N_不等于_bool_sint_sint =		215;
			public const string 不等于_bool_sint8_sint8 =			"!=_bool_sint8_sint8";
			public const string 不等于_bool_sint16_sint16 =		"!=_bool_sint16_sint16";
			public const string 不等于_bool_sint24_sint24 =		"!=_bool_sint24_sint24";
			public const string 不等于_bool_sint32_sint32 =		"!=_bool_sint32_sint32";
		
		public const byte N_取反_bit_bit =		35;
			public const string 取反_bit_bit =		"~_bit_bit";
			
		public const byte N_取反_uint8_uint8 =	36;
			public const string 取反_uint8_uint8 =	"~_uint8_uint8";
			
		public const byte N_取反_uint16_uint16 = 37;
			public const string 取反_uint16_uint16 =	"~_uint16_uint16";
			
		public const byte N_取反_uint24_uint24 =	38;
			public const string 取反_uint24_uint24 =	"~_uint24_uint24";
			
		public const byte N_取反_uint32_uint32 =	39;
			public const string 取反_uint32_uint32 =	"~_uint32_uint32";
		
		public const byte N_与_x_x_x =				40;
			public const string 与_bit_bit_bit =				"&_bit_bit_bit";
			public const string 与_uint8_uint8_uint8 =		"&_uint8_uint8_uint8";
			public const string 与_uint16_uint16_uint16 =	"&_uint16_uint16_uint16";
			public const string 与_uint24_uint24_uint24 =	"&_uint24_uint24_uint24";
			public const string 与_uint32_uint32_uint32 =	"&_uint32_uint32_uint32";
		
		public const byte N_或_x_x_x =				41;
			public const string 或_bit_bit_bit =				"|_bit_bit_bit";
			public const string 或_uint8_uint8_uint8 =		"|_uint8_uint8_uint8";
			public const string 或_uint16_uint16_uint16 =	"|_uint16_uint16_uint16";
			public const string 或_uint24_uint24_uint24 =	"|_uint24_uint24_uint24";
			public const string 或_uint32_uint32_uint32 =	"|_uint32_uint32_uint32";
		
		public const byte N_异或_x_x_x =			42;
			public const string 异或_bit_bit_bit =			"^_bit_bit_bit";
			public const string 异或_uint8_uint8_uint8 =		"^_uint8_uint8_uint8";
			public const string 异或_uint16_uint16_uint16 =	"^_uint16_uint16_uint16";
			public const string 异或_uint24_uint24_uint24 =	"^_uint24_uint24_uint24";
			public const string 异或_uint32_uint32_uint32 =	"^_uint32_uint32_uint32";
		
		public const byte N_补0左移_uint8_uint8_uint8 =		43;
			public const string 补0左移_uint8_uint8_uint8 =		"<<_uint8_uint8_uint8";
		
		public const byte N_补0左移_uint16_uint16_uint8 =		44;
			public const string 补0左移_uint16_uint16_uint8 =		"<<_uint16_uint16_uint8";
			
		public const byte N_补0左移_uint24_uint24_uint8 =		45;
			public const string 补0左移_uint24_uint24_uint8 =		"<<_uint24_uint24_uint8";
			
		public const byte N_补0左移_uint32_uint32_uint8 =		46;
			public const string 补0左移_uint32_uint32_uint8 =		"<<_uint32_uint32_uint8";
		
		public const byte N_补0右移_x_x_x =		47;
			public const string 补0右移_uint8_uint8_uint8 =		">>_uint8_uint8_uint8";
			public const string 补0右移_uint16_uint16_uint8 =		">>_uint16_uint16_uint8";
			public const string 补0右移_uint24_uint24_uint8 =		">>_uint24_uint24_uint8";
			public const string 补0右移_uint32_uint32_uint8 =		">>_uint32_uint32_uint8";
		
		public const byte N_分支比较_uint32_uint8 =	234;
			public const string 分支比较_uint32_uint8 =	"分支比较_uint32_uint8";
		public const byte N_分支散转_uint32 =	235;
			public const string 分支散转_uint32 =	"分支散转_uint32";
			
		public const byte N_肯定跳转_bool =	48;
			public const string 肯定跳转_bool =	"肯定跳转_bool";
		
		public const byte N_否定跳转_bool =	49;
			public const string 否定跳转_bool =	"否定跳转_bool";
		
		public const byte N_跳转递减_x =	50;
			public const string 跳转递减_uint8 =	"跳转递减_uint8";
			public const string 跳转递减_uint16 =	"跳转递减_uint16";
			public const string 跳转递减_uint24 =	"跳转递减_uint24";
			public const string 跳转递减_uint32 =	"跳转递减_uint32";
			public const string 跳转递减_sint8 =	"跳转递减_sint8";
			public const string 跳转递减_sint16 =	"跳转递减_sint16";
			public const string 跳转递减_sint24 =	"跳转递减_sint24";	
			public const string 跳转递减_sint32 =	"跳转递减_sint32";
		
		public const byte N_读取分量_bit_uint8 =			51;
			public const string 读取分量_bit_uint8 =		"读取分量_bit_uint8";
		
		public const byte N_读取分量_bit_uint16 =		52;
			public const string 读取分量_bit_uint16 =		"读取分量_bit_uint16";
		
		public const byte N_读取分量_bit_uint32 =		53;
			public const string 读取分量_bit_uint32 =		"读取分量_bit_uint32";
		
		public const byte N_读取分量_uint8_uint32 =		54;
			public const string 读取分量_uint8_uint32 =		"读取分量_uint8_uint32";
		
		public const byte N_读取分量_uint8_uint16 =		55;
			public const string 读取分量_uint8_uint16 =		"读取分量_uint8_uint16";
		
		public const byte N_读取分量_uint16_uint32 =		56;
			public const string 读取分量_uint16_uint32 =	"读取分量_uint16_uint32";
		
		public const byte N_写入分量_uint8_uint8_bit =		57;
			public const string 写入分量_uint8_uint8_bit =		"写入分量_uint8_uint8_bit";
		
		public const byte N_写入分量_uint16_uint16_bit =		58;
			public const string 写入分量_uint16_uint16_bit =		"写入分量_uint16_uint16_bit";
		
		public const byte N_写入分量_uint32_uint32_bit =		59;
			public const string 写入分量_uint32_uint32_bit =		"写入分量_uint32_uint32_bit";
		
		public const byte N_写入分量_uint16_uint16_uint8 =	60;
			public const string 写入分量_uint16_uint16_uint8 =	"写入分量_uint16_uint16_uint8";
		
		public const byte N_写入分量_uint32_uint32_uint8 =	61;
			public const string 写入分量_uint32_uint32_uint8 =	"写入分量_uint32_uint32_uint8";
		
		public const byte N_写入分量_uint32_uint32_uint16 =	62;
			public const string 写入分量_uint32_uint32_uint16 =	"写入分量_uint32_uint32_uint16";
		
		//无指令
			public const string 读系统量_bool =	"读系统量_bool";
			public const string 读系统量_bit =	"读系统量_bit";
			public const string 读系统量_uint8 =	"读系统量_uint8";
			public const string 读系统量_uint16 =	"读系统量_uint16";
			public const string 读系统量_uint24 =	"读系统量_uint24";
			public const string 读系统量_uint32 =	"读系统量_uint32";
		
		public const byte N_读系统量_sint8 =		63;
			public const string 读系统量_sint8 =	"读系统量_sint8";
		
		public const byte N_读系统量_sint16 =	64;
			public const string 读系统量_sint16 =	"读系统量_sint16";
		
		public const byte N_读系统量_sint24 =	65;
			public const string 读系统量_sint24 =	"读系统量_sint24";
		
		public const byte N_读系统量_sint32 =	66;
			public const string 读系统量_sint32 =	"读系统量_sint32";
		
		public const byte N_读系统量_fix =	66;
			public const string 读系统量_fix =	"读系统量_fix";			
		
		//无指令
			public const string 写系统量_bool =	"写系统量_bool";
			public const string 写系统量_bit =	"写系统量_bit";
			public const string 写系统量_uint8 =	"写系统量_uint8";
			public const string 写系统量_uint16 =	"写系统量_uint16";
			public const string 写系统量_uint24 =	"写系统量_uint24";
			public const string 写系统量_uint32 =	"写系统量_uint32";
			public const string 写系统量_sint8 =	"写系统量_sint8";
			public const string 写系统量_sint16 =	"写系统量_sint16";
			public const string 写系统量_sint24 =	"写系统量_sint24";
			public const string 写系统量_sint32 =	"写系统量_sint32";
			public const string 写系统量_fix =	"写系统量_fix";
		
		
		public const byte N_传递形参_bit =			67;
			public const string 传递形参_bit =		"传递形参_bit";
		
		public const byte N_传递形参_bool =			68;
			public const string 传递形参_bool =		"传递形参_bool";
		
		public const byte N_传递形参_uint8 =			69;
			public const string 传递形参_uint8 =		"传递形参_uint8";
		
		public const byte N_传递形参_uint16 =		70;
			public const string 传递形参_uint16 =		"传递形参_uint16";
		
		public const byte N_传递形参_uint24 =		71;
			public const string 传递形参_uint24 =		"传递形参_uint24";
		
		public const byte N_传递形参_uint32 =		72;
			public const string 传递形参_uint32 =		"传递形参_uint32";
		
		public const byte N_传递形参_sint8 =			73;
			public const string 传递形参_sint8 =		"传递形参_sint8";
			
		public const byte N_传递形参_sint16 =		74;
			public const string 传递形参_sint16 =		"传递形参_sint16";
		
		public const byte N_传递形参_sint24 =		75;
			public const string 传递形参_sint24 =		"传递形参_sint24";
		
		public const byte N_传递形参_sint32 =		76;
			public const string 传递形参_sint32 =		"传递形参_sint32";
		
		public const byte N_传递形参_fix =		76;
			public const string 传递形参_fix =		"传递形参_fix";
		
		/*
		public const byte N_加载CODE_uint8 =		77;
			public const string 加载CODE_uint8 =		"加载CODE_uint8";
		
		public const byte N_加载CODE_uint16 =	78;
			public const string 加载CODE_uint16 =	"加载CODE_uint16";
		
		public const byte N_加载CODE_uint24 =	79;
			public const string 加载CODE_uint24 =	"加载CODE_uint24";
		
		public const byte N_加载CODE_uint32 =	80;
			public const string 加载CODE_uint32 =	"加载CODE_uint32";
		*/
		public const byte N_LOAD_local_uint8 =	81;
			public const string LOAD_local_uint8 =	"LOAD_local_uint8";
		
		public const byte N_LOAD_local_uint16 =	82;
			public const string LOAD_local_uint16 =	"LOAD_local_uint16";
		
		public const byte N_LOAD_local_uint24 =	83;
			public const string LOAD_local_uint24 =	"LOAD_local_uint24";
		
		public const byte N_LOAD_local_uint32 =	84;
			public const string LOAD_local_uint32 =	"LOAD_local_uint32";
		
		
		public const byte N_保存BASE_uint8 =		85;
			public const string 保存BASE_uint8 =		"保存BASE_uint8";
		
		public const byte N_保存BASE_uint16 =	86;
			public const string 保存BASE_uint16 =	"保存BASE_uint16";
		
		public const byte N_保存BASE_uint24 =	87;
			public const string 保存BASE_uint24 =	"保存BASE_uint24";
		
		public const byte N_保存BASE_uint32 =	88;
			public const string 保存BASE_uint32 =	"保存BASE_uint32";
		
		
		
		
		public const byte N_加载_0_static_bool =		89;
			public const string 加载_0_static_bool =		"加载_0_static_bool";
		
		public const byte N_加载_0_static_bit =		90;
			public const string 加载_0_static_bit =		"加载_0_static_bit";
		
		public const byte N_加载_0_static_uint8 =	91;
			public const string 加载_0_static_uint8 =	"加载_0_static_uint8";
		
		public const byte N_加载_0_static_uint16 =	92;
			public const string 加载_0_static_uint16 =	"加载_0_static_uint16";
		
		public const byte N_加载_0_static_uint24 =	93;
			public const string 加载_0_static_uint24 =	"加载_0_static_uint24";
		
		public const byte N_加载_0_static_uint32 =	94;
			public const string 加载_0_static_uint32 =	"加载_0_static_uint32";
		
		public const byte N_加载_0_static_sint8 =	95;
			public const string 加载_0_static_sint8 =	"加载_0_static_sint8";
		
		public const byte N_加载_0_static_sint16 =	96;
			public const string 加载_0_static_sint16 =	"加载_0_static_sint16";
		
		public const byte N_加载_0_static_sint24 =	97;
			public const string 加载_0_static_sint24 =	"加载_0_static_sint24";
		
		public const byte N_加载_0_static_sint32 =	98;
			public const string 加载_0_static_sint32 =	"加载_0_static_sint32";
		
		public const byte N_加载_0_static_fix =	98;
			public const string 加载_0_static_fix =	"加载_0_static_fix";
			
		public const byte N_加载_1_static_bool =		99;
			public const string 加载_1_static_bool =		"加载_1_static_bool";
		
		public const byte N_加载_1_static_bit =		100;
			public const string 加载_1_static_bit =		"加载_1_static_bit";
		
		public const byte N_加载_1_static_uint8 =	101;
			public const string 加载_1_static_uint8 =	"加载_1_static_uint8";
		
		public const byte N_加载_1_static_uint16 =	102;
			public const string 加载_1_static_uint16 =	"加载_1_static_uint16";
		
		public const byte N_加载_1_static_uint24 =	103;
			public const string 加载_1_static_uint24 =	"加载_1_static_uint24";
		
		public const byte N_加载_1_static_uint32 =	104;
			public const string 加载_1_static_uint32 =	"加载_1_static_uint32";
		
		public const byte N_加载_1_static_sint8 =	105;
			public const string 加载_1_static_sint8 =	"加载_1_static_sint8";
		
		public const byte N_加载_1_static_sint16 =	106;
			public const string 加载_1_static_sint16 =	"加载_1_static_sint16";
		
		public const byte N_加载_1_static_sint24 =	107;
			public const string 加载_1_static_sint24 =	"加载_1_static_sint24";
		
		public const byte N_加载_1_static_sint32 =	108;
			public const string 加载_1_static_sint32 =	"加载_1_static_sint32";
		
		public const byte N_加载_1_static_fix =	108;
			public const string 加载_1_static_fix =	"加载_1_static_fix";
		
		public const byte N_保存_0_static_bool =		109;
			public const string 保存_0_static_bool =		"保存_0_static_bool";
		
		public const byte N_保存_0_static_bit =		110;
			public const string 保存_0_static_bit =		"保存_0_static_bit";
		
		public const byte N_保存_0_static_uint8 =	111;
			public const string 保存_0_static_uint8 =	"保存_0_static_uint8";
		
		public const byte N_保存_0_static_uint16 =	112;
			public const string 保存_0_static_uint16 =	"保存_0_static_uint16";
		
		public const byte N_保存_0_static_uint24 =	113;
			public const string 保存_0_static_uint24 =	"保存_0_static_uint24";
		
		public const byte N_保存_0_static_uint32 =	114;
			public const string 保存_0_static_uint32 =	"保存_0_static_uint32";
		
		public const byte N_保存_0_static_sint8 =	115;
			public const string 保存_0_static_sint8 =	"保存_0_static_sint8";
		
		public const byte N_保存_0_static_sint16 =	116;
			public const string 保存_0_static_sint16 =	"保存_0_static_sint16";
		
		public const byte N_保存_0_static_sint24 =	117;
			public const string 保存_0_static_sint24 =	"保存_0_static_sint24";
		
		public const byte N_保存_0_static_sint32 =	118;
			public const string 保存_0_static_sint32 =	"保存_0_static_sint32";
		
		public const byte N_保存_0_static_fix =	118;
			public const string 保存_0_static_fix =	"保存_0_static_fix";
		
		public const byte N_加载_0_local_bool =		119;
			public const string 加载_0_local_bool =		"加载_0_local_bool";
		
		public const byte N_加载_0_local_bit =		120;
			public const string 加载_0_local_bit =		"加载_0_local_bit";
		
		public const byte N_加载_0_local_uint8 =		121;
			public const string 加载_0_local_uint8 =		"加载_0_local_uint8";
		
		public const byte N_加载_0_local_uint16 =	122;
			public const string 加载_0_local_uint16 =	"加载_0_local_uint16";
		
		public const byte N_加载_0_local_uint24 =	123;
			public const string 加载_0_local_uint24 =	"加载_0_local_uint24";
		
		public const byte N_加载_0_local_uint32 =	124;
			public const string 加载_0_local_uint32 =	"加载_0_local_uint32";
		
		public const byte N_加载_0_local_sint8 =		125;
			public const string 加载_0_local_sint8 =		"加载_0_local_sint8";
		
		public const byte N_加载_0_local_sint16 =	126;
			public const string 加载_0_local_sint16 =	"加载_0_local_sint16";
		
		public const byte N_加载_0_local_sint24 =	127;
			public const string 加载_0_local_sint24 =	"加载_0_local_sint24";
		
		public const byte N_加载_0_local_sint32 =	128;
			public const string 加载_0_local_sint32 =	"加载_0_local_sint32";
		
		public const byte N_加载_0_local_fix =	128;
			public const string 加载_0_local_fix =	"加载_0_local_fix";
		
		public const byte N_加载_1_local_bool =		129;
			public const string 加载_1_local_bool =		"加载_1_local_bool";
		
		public const byte N_加载_1_local_bit =		130;
			public const string 加载_1_local_bit =		"加载_1_local_bit";
		
		public const byte N_加载_1_local_uint8 =		131;
			public const string 加载_1_local_uint8 =		"加载_1_local_uint8";
		
		public const byte N_加载_1_local_uint16 =	132;
			public const string 加载_1_local_uint16 =	"加载_1_local_uint16";
		
		public const byte N_加载_1_local_uint24 =	133;
			public const string 加载_1_local_uint24 =	"加载_1_local_uint24";
		
		public const byte N_加载_1_local_uint32 =	134;
			public const string 加载_1_local_uint32 =	"加载_1_local_uint32";
		
		public const byte N_加载_1_local_sint8 =		135;
			public const string 加载_1_local_sint8 =		"加载_1_local_sint8";
		
		public const byte N_加载_1_local_sint16 =	136;
			public const string 加载_1_local_sint16 =	"加载_1_local_sint16";
		
		public const byte N_加载_1_local_sint24 =	137;
			public const string 加载_1_local_sint24 =	"加载_1_local_sint24";
		
		public const byte N_加载_1_local_sint32 =	138;
			public const string 加载_1_local_sint32 =	"加载_1_local_sint32";
		
		public const byte N_加载_1_local_fix =	138;
			public const string 加载_1_local_fix =	"加载_1_local_fix";
		
		public const byte N_保存_0_local_bool =		139;
			public const string 保存_0_local_bool =		"保存_0_local_bool";
		
		public const byte N_保存_0_local_bit =		140;
			public const string 保存_0_local_bit =		"保存_0_local_bit";
		
		public const byte N_保存_0_local_uint8 =		141;
			public const string 保存_0_local_uint8 =		"保存_0_local_uint8";
		
		public const byte N_保存_0_local_uint16 =	142;
			public const string 保存_0_local_uint16 =	"保存_0_local_uint16";
		
		public const byte N_保存_0_local_uint24 =	143;
			public const string 保存_0_local_uint24 =	"保存_0_local_uint24";
		
		public const byte N_保存_0_local_uint32 =	144;
			public const string 保存_0_local_uint32 =	"保存_0_local_uint32";
		
		public const byte N_保存_0_local_sint8 =		145;
			public const string 保存_0_local_sint8 =		"保存_0_local_sint8";
		
		public const byte N_保存_0_local_sint16 =	146;
			public const string 保存_0_local_sint16 =	"保存_0_local_sint16";
		
		public const byte N_保存_0_local_sint24 =	147;
			public const string 保存_0_local_sint24 =	"保存_0_local_sint24";
		
		public const byte N_保存_0_local_sint32 =	148;
			public const string 保存_0_local_sint32 =	"保存_0_local_sint32";
		
		public const byte N_保存_0_local_fix =	148;
			public const string 保存_0_local_fix =	"保存_0_local_fix";
		
		public const byte N_加载_SP_local_uint16 =	149;
			public const string 加载_SP_local_uint16 =	"加载_SP_local_uint16";
		
		public const byte N_保存_SP_local_uint16 =	150;
			public const string 保存_SP_local_uint16 =	"保存_SP_local_uint16";
		
		public const byte N_加载_PC_local_uint =	151;
			public const string 加载_PC_local_uint16 =	"加载_PC_local_uint16";
			public const string 加载_PC_local_uint24 =	"加载_PC_local_uint24";
			public const string 加载_PC_local_uint32 =	"加载_PC_local_uint32";
		
		public const byte N_指令地址 =	152;
			public const string 函数地址_uint16 =		"函数地址_uint16";
			public const string 标签地址_uint16 =		"标签地址_uint16";
			public const string 函数地址_uint24 =		"函数地址_uint24";
			public const string 标签地址_uint24 =		"标签地址_uint24";
			public const string 函数地址_uint32 =		"函数地址_uint32";
			public const string 标签地址_uint32 =		"标签地址_uint32";
	}
}
