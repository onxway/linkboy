﻿

using System;
using n_VCODE;
using n_Config;

namespace n_Decode
{
	//字节码解码转换类
	public static class Decode
	{
		//指令集合类
		public struct Ins
		{
			public byte CODE0;
			public byte CODE0EXT;
			public byte CODE0END;
			public uint CODE1;
			
			public int CodeNumber;
			
			public int InsAddr;
			public bool isAlign;
			public string[] InsCut;
			
			public bool isNone;
			
			public override string ToString()
			{
				string s = "";
				for( int i = 0; InsCut != null && i < InsCut.Length; ++i ) {
					s += InsCut[i] + " ";
				}
				return CODE0 + "," + CODE1 + " " + InsAddr + "(" + InsAddr.ToString( "X" ) + ") " + s;
			}
		}
		
		public static Ins[] InsList;
		
		static string[] FuncAddrList;
		static int FuncLength;
		
		public static byte[] ByteCodeList;
		public static int BCLength;
		
		static bool ExistEm;
		public static bool Zip = false;
		
		//初始化
		public static void Init()
		{
			FuncAddrList = new string[1000];
		}
		
		public static string SetInsList( string vCode )
		{
			string[] ROM_S = vCode.Split( '\n' );
			InsList = new Ins[ROM_S.Length];
			ByteCodeList = new byte[ROM_S.Length*20];
			BCLength = 0;
			
			FuncLength = 0;
			ExistEm = false;
			
			//生成机器码
			for( int i = 0; i < ROM_S.Length; ++i ) {
				if( ROM_S[i] != null && ROM_S[i] != "" ) {
					try{
					SwitchByteCode( i, ROM_S[i] );
					}
					catch( Exception e) {
						n_OS.VIO.Show( "<Decode.SetInsList> " + i + ":" + ROM_S[i] + "\n" + e );
					}
				}
			}
			//处理函数地址相关指令
			DealAddr( true );
			
			//生成字节码
			for( int i = 0; i < InsList.Length; ++i ) {
				if( ROM_S[i] != null && ROM_S[i] != "" ) {
					OutputByteCode( i );
				}
			}
			if( ExistEm ) {
				ExistEm = false;
				AddCode( 0 );
				AddCode( 0 );
				AddCode( 0 );
			}
			//处理函数地址相关指令
			DealAddr( false );
			
			//处理常量数组中的函数地址
			for( int i = 0; i < FuncLength; ++i ) {
				string[] c = FuncAddrList[i].Split( ',' );
				string funcname = c[0].Remove( 0, 2 );
				int byteAddr = int.Parse( c[1] );
				
				int LineIndex = n_VM_Assembler.VLabelList.GetLineNumber( funcname );
				if( LineIndex == -1 ) {
					n_OS.VIO.Show( "<Decode.SetInsList> 未知的函数名: " + funcname );
					continue;
				}
				int Addr = InsList[LineIndex].InsAddr;
				
				if( c[0].StartsWith( "&L" ) ) {
					ByteCodeList[byteAddr] = (byte)(Addr % 256 );
				}
				else if( c[0].StartsWith( "&H" ) ) {
					ByteCodeList[byteAddr] = (byte)(Addr / 256 % 256 );
				}
				//到这里说明是 &E
				else {
					ByteCodeList[byteAddr] = (byte)(Addr / 256 / 256 );
				}
			}
			
			n_CodeData.CodeData.TotalByteNumber = BCLength;
			
			//统计指令频率
			//GetInsTotalMap();
			
			//return Hex;
			return null;
			
			//显示字节码
//			string s = null;
//			for( int i = 0; i < BCLength; ++i ) {
//				s += ByteCode[i].ToString().PadLeft( 3, ' ' ) + ",";
//				if( i % 10 == 9 ) {
//					s += "\n";
//				}
//			}
//			MessageBox.Show( s );
		}
		
		static void SwitchByteCode( int index, string Ins )
		{
			InsList[index].InsCut = Ins.Split( ' ' );
			InsList[index].isAlign = false;
			InsList[index].isNone = false;
			InsList[index].CODE0END = 0;
			
			int CodeNumber = -1;
			switch( InsList[index].InsCut[ 0 ] ) {
				case C.CODE:		break;
				case C.SYS_PAD:		break;
				case C.中断未用跳转:	InsList[ index ].CODE0 = 0; CodeNumber = (int)COM.GotoByteNumber; break;
				case C.接口调用:		InsList[ index ].CODE0 = C.N_接口调用; InsList[ index ].CODE1 = uint.Parse( InsList[ index ].InsCut[1] ); CodeNumber = COM.InterfaceByteNumber; break;
				case C.设置堆栈:		InsList[ index ].CODE0 = C.N_设置堆栈; InsList[ index ].CODE1 = uint.Parse( InsList[ index ].InsCut[1] ); CodeNumber = COM.VarAddrByteNumber; break;
				case C.跳转:			InsList[ index ].CODE0 = C.N_跳转; InsList[ index ].CODE1 = uint.Parse( InsList[ index ].InsCut[1] ); CodeNumber = (int)COM.GotoByteNumber; break;
				case C.函数调用:		InsList[ index ].CODE0 = C.N_函数调用; InsList[ index ].CODE1 = uint.Parse( InsList[ index ].InsCut[1] ); CodeNumber = (int)COM.GotoByteNumber; break;
				case C.域返回:		InsList[ index ].CODE0 = C.N_域返回; InsList[ index ].CODE1 = uint.Parse( InsList[ index ].InsCut[1] ); CodeNumber = COM.RETYByteNumber; break;
				case C.返回:			InsList[ index ].CODE0 = C.N_返回; CodeNumber = COM.None; break;
				case C.中断返回:		InsList[ index ].CODE0 = C.N_中断返回; CodeNumber = COM.None; break;
				case C.堆栈递增:		InsList[ index ].CODE0 = C.N_堆栈递增; InsList[ index ].CODE1 = uint.Parse( InsList[ index ].InsCut[1] ); CodeNumber = COM.VarAddrByteNumber; break;
				case C.堆栈递减:		InsList[ index ].CODE0 = C.N_堆栈递减; InsList[ index ].CODE1 = uint.Parse( InsList[ index ].InsCut[1] ); CodeNumber = COM.VarAddrByteNumber; break;
				case C.转移形参:		InsList[ index ].CODE0 = C.N_转移形参; InsList[ index ].CODE1 = uint.Parse( InsList[ index ].InsCut[1] ); CodeNumber = COM.VarAddrByteNumber; break;
				case C.保护现场:		InsList[ index ].CODE0 = C.N_保护现场; CodeNumber = COM.None; break;
				case C.恢复现场:		InsList[ index ].CODE0 = C.N_恢复现场; CodeNumber = COM.None; break;
				case C.单步:			InsList[ index ].CODE0 = C.N_STEP; CodeNumber = COM.None; break;
				
				case C.空指令:
				case C.空指令1:		InsList[ index ].CODE0 = C.N_空指令; CodeNumber = COM.None; break;
				
				case C.出栈:		InsList[ index ].CODE0 = C.N_出栈; CodeNumber = COM.None; break;
				case C.压栈:		InsList[ index ].CODE0 = C.N_压栈; CodeNumber = COM.None; break;
				case C.CLI:		InsList[ index ].CODE0 = C.N_CLI; CodeNumber = COM.None; break;
				case C.SEI:		InsList[ index ].CODE0 = C.N_SEI; CodeNumber = COM.None; break;
				
				case C.转移参数:		InsList[ index ].CODE0 = C.N_转移参数; CodeNumber = COM.None; break;
				
				case C.函数地址_uint16:
				case C.标签地址_uint16:
				case C.函数地址_uint24:
				case C.标签地址_uint24:
				case C.函数地址_uint32:
				case C.标签地址_uint32:
					InsList[ index ].CODE0 = C.N_指令地址; InsList[ index ].CODE1 = uint.Parse( InsList[ index ].InsCut[1] ); CodeNumber = (int)COM.GotoByteNumber; break;
				
				case C.读取地址_static_uint32:
				case C.读取地址_vdata_uint8:
				case C.读取地址_vdata_uint16:
				case C.读取地址_vdata_uint32:
				
				case C.常量赋值_bit:
				case C.常量赋值_bool:
				case C.常量赋值_uint8:
				case C.常量赋值_uint16:
				case C.常量赋值_uint32:
				case C.常量赋值_sint8:
				case C.常量赋值_sint16:
				case C.常量赋值_sint32:
				case C.常量赋值_fix:
					InsList[ index ].CODE0 = C.N_SETA_sint32; InsList[ index ].CODE1 = uint.Parse( InsList[ index ].InsCut[1] ); CodeNumber = COM.ConstByteNumber;
					if( Zip && InsList[ index ].CODE1 < 0x800000 ) {
						CodeNumber = 3;
					}
					else {
						InsList[ index ].CODE0EXT = 0;
						InsList[ index ].CODE0END = 0x80;
					}
					break;
				
				case C.B常量赋值_bit:
				case C.B常量赋值_bool:
				case C.B常量赋值_uint8:
				case C.B常量赋值_uint16:
				case C.B常量赋值_uint32:
				case C.B常量赋值_sint8:
				case C.B常量赋值_sint16:
				case C.B常量赋值_sint32:
				case C.B常量赋值_fix:
					InsList[ index ].CODE0 = C.N_SETB_sint32; InsList[ index ].CODE1 = uint.Parse( InsList[ index ].InsCut[1] ); CodeNumber = COM.ConstByteNumber;
					if( Zip && InsList[ index ].CODE1 < 0x800000 ) {
						CodeNumber = 3;
					}
					else {
						InsList[ index ].CODE0EXT = 0;
						InsList[ index ].CODE0END = 0x80;
					}
					break;
				
				case C.读取地址_local_uint32: InsList[ index ].CODE0 = C.N_读取地址_local_uint32; InsList[ index ].CODE1 = uint.Parse( InsList[ index ].InsCut[1] ); CodeNumber = COM.ConstByteNumber; break;
				
				//这里是添加CODE类型标记
				case C.读取地址_code_uint32:
					InsList[ index ].CODE0 = C.N_SETA_sint32;
					InsList[ index ].CODE1 = 0x10000 + uint.Parse( InsList[ index ].InsCut[1] );
					if( Zip ) {
						CodeNumber = 3;
					}
					else {
						InsList[ index ].CODE0END = 0x80;
						CodeNumber = COM.ConstByteNumber;
					}
					break;
					
				case C.分支散转_uint32: InsList[ index ].CODE0 = C.N_分支散转_uint32; CodeNumber = COM.None; break;
				case C.分支比较_uint32_uint8:
					InsList[ index ].CODE0 = C.N_分支比较_uint32_uint8;
					InsList[ index ].CODE0EXT = byte.Parse( InsList[ index ].InsCut[1] );
					InsList[ index ].CODE0END = 0;
					InsList[ index ].CODE1 = uint.Parse( InsList[ index ].InsCut[2] );
					CodeNumber = 1 + (int)COM.GotoByteNumber;
					break;
				
				case C.肯定跳转_bool:		InsList[ index ].CODE0 = C.N_肯定跳转_bool; InsList[ index ].CODE1 = uint.Parse( InsList[ index ].InsCut[1] ); CodeNumber = (int)COM.GotoByteNumber; break;
				case C.否定跳转_bool:		InsList[ index ].CODE0 = C.N_否定跳转_bool; InsList[ index ].CODE1 = uint.Parse( InsList[ index ].InsCut[1] ); CodeNumber = (int)COM.GotoByteNumber; break;
				case C.跳转递减_uint8:
				case C.跳转递减_uint16:
				case C.跳转递减_uint24:
				case C.跳转递减_uint32:
				case C.跳转递减_sint8:
				case C.跳转递减_sint16:
				case C.跳转递减_sint24:
				case C.跳转递减_sint32:
					InsList[ index ].CODE0 = C.N_跳转递减_x; InsList[ index ].CODE1 = uint.Parse( InsList[ index ].InsCut[1] ); CodeNumber = (int)COM.GotoByteNumber; break;
				
				case C.加载_0_static_bit:	InsList[ index ].CODE0 = C.N_加载_0_static_bit; InsList[ index ].CODE1 = uint.Parse( InsList[ index ].InsCut[1] ); CodeNumber = COM.VarAddrByteNumber; break;
				case C.加载_0_static_bool:	InsList[ index ].CODE0 = C.N_加载_0_static_bool; InsList[ index ].CODE1 = uint.Parse( InsList[ index ].InsCut[1] ); CodeNumber = COM.VarAddrByteNumber; break;
				case C.加载_0_static_uint8:	InsList[ index ].CODE0 = C.N_加载_0_static_uint8; InsList[ index ].CODE1 = uint.Parse( InsList[ index ].InsCut[1] ); CodeNumber = COM.VarAddrByteNumber; break;;
				case C.加载_0_static_uint16:	InsList[ index ].CODE0 = C.N_加载_0_static_uint16; InsList[ index ].CODE1 = uint.Parse( InsList[ index ].InsCut[1] ); CodeNumber = COM.VarAddrByteNumber; break;
				case C.加载_0_static_uint24:	InsList[ index ].CODE0 = C.N_加载_0_static_uint24; InsList[ index ].CODE1 = uint.Parse( InsList[ index ].InsCut[1] ); CodeNumber = COM.VarAddrByteNumber; break;
				case C.加载_0_static_uint32:	InsList[ index ].CODE0 = C.N_加载_0_static_uint32; InsList[ index ].CODE1 = uint.Parse( InsList[ index ].InsCut[1] ); CodeNumber = COM.VarAddrByteNumber; break;
				case C.加载_0_static_sint8:	InsList[ index ].CODE0 = C.N_加载_0_static_sint8; InsList[ index ].CODE1 = uint.Parse( InsList[ index ].InsCut[1] ); CodeNumber = COM.VarAddrByteNumber; break;
				case C.加载_0_static_sint16:	InsList[ index ].CODE0 = C.N_加载_0_static_sint16; InsList[ index ].CODE1 = uint.Parse( InsList[ index ].InsCut[1] ); CodeNumber = COM.VarAddrByteNumber; break;
				case C.加载_0_static_sint24:	InsList[ index ].CODE0 = C.N_加载_0_static_sint24; InsList[ index ].CODE1 = uint.Parse( InsList[ index ].InsCut[1] ); CodeNumber = COM.VarAddrByteNumber; break;
				case C.加载_0_static_sint32:
				case C.加载_0_static_fix: 	InsList[ index ].CODE0 = C.N_加载_0_static_sint32; InsList[ index ].CODE1 = uint.Parse( InsList[ index ].InsCut[1] ); CodeNumber = COM.VarAddrByteNumber; break;
				
				case C.加载_1_static_bit:	InsList[ index ].CODE0 = C.N_加载_1_static_bit; InsList[ index ].CODE1 = uint.Parse( InsList[ index ].InsCut[1] ); CodeNumber = COM.VarAddrByteNumber; break;
				case C.加载_1_static_bool:	InsList[ index ].CODE0 = C.N_加载_1_static_bool; InsList[ index ].CODE1 = uint.Parse( InsList[ index ].InsCut[1] ); CodeNumber = COM.VarAddrByteNumber; break;
				case C.加载_1_static_uint8:	InsList[ index ].CODE0 = C.N_加载_1_static_uint8; InsList[ index ].CODE1 = uint.Parse( InsList[ index ].InsCut[1] ); CodeNumber = COM.VarAddrByteNumber; break;
				case C.加载_1_static_uint16:	InsList[ index ].CODE0 = C.N_加载_1_static_uint16; InsList[ index ].CODE1 = uint.Parse( InsList[ index ].InsCut[1] ); CodeNumber = COM.VarAddrByteNumber; break;
				case C.加载_1_static_uint24:	InsList[ index ].CODE0 = C.N_加载_1_static_uint24; InsList[ index ].CODE1 = uint.Parse( InsList[ index ].InsCut[1] ); CodeNumber = COM.VarAddrByteNumber; break;
				case C.加载_1_static_uint32:	InsList[ index ].CODE0 = C.N_加载_1_static_uint32; InsList[ index ].CODE1 = uint.Parse( InsList[ index ].InsCut[1] ); CodeNumber = COM.VarAddrByteNumber; break;
				case C.加载_1_static_sint8:	InsList[ index ].CODE0 = C.N_加载_1_static_sint8; InsList[ index ].CODE1 = uint.Parse( InsList[ index ].InsCut[1] ); CodeNumber = COM.VarAddrByteNumber; break;
				case C.加载_1_static_sint16:	InsList[ index ].CODE0 = C.N_加载_1_static_sint16; InsList[ index ].CODE1 = uint.Parse( InsList[ index ].InsCut[1] ); CodeNumber = COM.VarAddrByteNumber; break;
				case C.加载_1_static_sint24:	InsList[ index ].CODE0 = C.N_加载_1_static_sint24; InsList[ index ].CODE1 = uint.Parse( InsList[ index ].InsCut[1] ); CodeNumber = COM.VarAddrByteNumber; break;
				case C.加载_1_static_sint32:
				case C.加载_1_static_fix: InsList[ index ].CODE0 = C.N_加载_1_static_sint32; InsList[ index ].CODE1 = uint.Parse( InsList[ index ].InsCut[1] ); CodeNumber = COM.VarAddrByteNumber; break;
				
				case C.保存_0_static_bit:	InsList[ index ].CODE0 = C.N_保存_0_static_bit; InsList[ index ].CODE1 = uint.Parse( InsList[ index ].InsCut[1] ); CodeNumber = COM.VarAddrByteNumber; break;
				case C.保存_0_static_bool:	InsList[ index ].CODE0 = C.N_保存_0_static_bool; InsList[ index ].CODE1 = uint.Parse( InsList[ index ].InsCut[1] ); CodeNumber = COM.VarAddrByteNumber; break;
				case C.保存_0_static_uint8:	InsList[ index ].CODE0 = C.N_保存_0_static_uint8; InsList[ index ].CODE1 = uint.Parse( InsList[ index ].InsCut[1] ); CodeNumber = COM.VarAddrByteNumber; break;
				case C.保存_0_static_uint16:	InsList[ index ].CODE0 = C.N_保存_0_static_uint16; InsList[ index ].CODE1 = uint.Parse( InsList[ index ].InsCut[1] ); CodeNumber = COM.VarAddrByteNumber; break;
				case C.保存_0_static_uint24:	InsList[ index ].CODE0 = C.N_保存_0_static_uint24; InsList[ index ].CODE1 = uint.Parse( InsList[ index ].InsCut[1] ); CodeNumber = COM.VarAddrByteNumber; break;
				case C.保存_0_static_uint32:	InsList[ index ].CODE0 = C.N_保存_0_static_uint32; InsList[ index ].CODE1 = uint.Parse( InsList[ index ].InsCut[1] ); CodeNumber = COM.VarAddrByteNumber; break;
				case C.保存_0_static_sint8:	InsList[ index ].CODE0 = C.N_保存_0_static_sint8; InsList[ index ].CODE1 = uint.Parse( InsList[ index ].InsCut[1] ); CodeNumber = COM.VarAddrByteNumber; break;
				case C.保存_0_static_sint16:	InsList[ index ].CODE0 = C.N_保存_0_static_sint16; InsList[ index ].CODE1 = uint.Parse( InsList[ index ].InsCut[1] ); CodeNumber = COM.VarAddrByteNumber; break;
				case C.保存_0_static_sint24:	InsList[ index ].CODE0 = C.N_保存_0_static_sint24; InsList[ index ].CODE1 = uint.Parse( InsList[ index ].InsCut[1] ); CodeNumber = COM.VarAddrByteNumber; break;
				case C.保存_0_static_sint32:
				case C.保存_0_static_fix:	InsList[ index ].CODE0 = C.N_保存_0_static_sint32; InsList[ index ].CODE1 = uint.Parse( InsList[ index ].InsCut[1] ); CodeNumber = COM.VarAddrByteNumber; break;
				
				case C.加载_0_local_bit:		InsList[ index ].CODE0 = C.N_加载_0_local_bit; InsList[ index ].CODE1 = uint.Parse( InsList[ index ].InsCut[1] ); CodeNumber = COM.VarAddrByteNumber; break;
				case C.加载_0_local_bool:		InsList[ index ].CODE0 = C.N_加载_0_local_bool; InsList[ index ].CODE1 = uint.Parse( InsList[ index ].InsCut[1] ); CodeNumber = COM.VarAddrByteNumber; break;
				case C.加载_0_local_uint8:		InsList[ index ].CODE0 = C.N_加载_0_local_uint8; InsList[ index ].CODE1 = uint.Parse( InsList[ index ].InsCut[1] ); CodeNumber = COM.VarAddrByteNumber; break;
				case C.加载_0_local_uint16:		InsList[ index ].CODE0 = C.N_加载_0_local_uint16; InsList[ index ].CODE1 = uint.Parse( InsList[ index ].InsCut[1] ); CodeNumber = COM.VarAddrByteNumber; break;
				case C.加载_0_local_uint24:		InsList[ index ].CODE0 = C.N_加载_0_local_uint24; InsList[ index ].CODE1 = uint.Parse( InsList[ index ].InsCut[1] ); CodeNumber = COM.VarAddrByteNumber; break;
				case C.加载_0_local_uint32:		InsList[ index ].CODE0 = C.N_加载_0_local_uint32; InsList[ index ].CODE1 = uint.Parse( InsList[ index ].InsCut[1] ); CodeNumber = COM.VarAddrByteNumber; break;
				case C.加载_0_local_sint8:		InsList[ index ].CODE0 = C.N_加载_0_local_sint8; InsList[ index ].CODE1 = uint.Parse( InsList[ index ].InsCut[1] ); CodeNumber = COM.VarAddrByteNumber; break;
				case C.加载_0_local_sint16:		InsList[ index ].CODE0 = C.N_加载_0_local_sint16; InsList[ index ].CODE1 = uint.Parse( InsList[ index ].InsCut[1] ); CodeNumber = COM.VarAddrByteNumber; break;
				case C.加载_0_local_sint24:		InsList[ index ].CODE0 = C.N_加载_0_local_sint24; InsList[ index ].CODE1 = uint.Parse( InsList[ index ].InsCut[1] ); CodeNumber = COM.VarAddrByteNumber; break;
				case C.加载_0_local_sint32:
				case C.加载_0_local_fix:			InsList[ index ].CODE0 = C.N_加载_0_local_sint32; InsList[ index ].CODE1 = uint.Parse( InsList[ index ].InsCut[1] ); CodeNumber = COM.VarAddrByteNumber; break;
				
				case C.加载_1_local_bit:		InsList[ index ].CODE0 = C.N_加载_1_local_bit; InsList[ index ].CODE1 = uint.Parse( InsList[ index ].InsCut[1] ); CodeNumber = COM.VarAddrByteNumber; break;
				case C.加载_1_local_bool:		InsList[ index ].CODE0 = C.N_加载_1_local_bool; InsList[ index ].CODE1 = uint.Parse( InsList[ index ].InsCut[1] ); CodeNumber = COM.VarAddrByteNumber; break;
				case C.加载_1_local_uint8:		InsList[ index ].CODE0 = C.N_加载_1_local_uint8; InsList[ index ].CODE1 = uint.Parse( InsList[ index ].InsCut[1] ); CodeNumber = COM.VarAddrByteNumber; break;
				case C.加载_1_local_uint16:		InsList[ index ].CODE0 = C.N_加载_1_local_uint16; InsList[ index ].CODE1 = uint.Parse( InsList[ index ].InsCut[1] ); CodeNumber = COM.VarAddrByteNumber; break;
				case C.加载_1_local_uint24:		InsList[ index ].CODE0 = C.N_加载_1_local_uint24; InsList[ index ].CODE1 = uint.Parse( InsList[ index ].InsCut[1] ); CodeNumber = COM.VarAddrByteNumber; break;
				case C.加载_1_local_uint32:		InsList[ index ].CODE0 = C.N_加载_1_local_uint32; InsList[ index ].CODE1 = uint.Parse( InsList[ index ].InsCut[1] ); CodeNumber = COM.VarAddrByteNumber; break;
				case C.加载_1_local_sint8:		InsList[ index ].CODE0 = C.N_加载_1_local_sint8; InsList[ index ].CODE1 = uint.Parse( InsList[ index ].InsCut[1] ); CodeNumber = COM.VarAddrByteNumber; break;
				case C.加载_1_local_sint16:		InsList[ index ].CODE0 = C.N_加载_1_local_sint16; InsList[ index ].CODE1 = uint.Parse( InsList[ index ].InsCut[1] ); CodeNumber = COM.VarAddrByteNumber; break;
				case C.加载_1_local_sint24:		InsList[ index ].CODE0 = C.N_加载_1_local_sint24; InsList[ index ].CODE1 = uint.Parse( InsList[ index ].InsCut[1] ); CodeNumber = COM.VarAddrByteNumber; break;
				case C.加载_1_local_sint32:		
				case C.加载_1_local_fix:		InsList[ index ].CODE0 = C.N_加载_1_local_sint32; InsList[ index ].CODE1 = uint.Parse( InsList[ index ].InsCut[1] ); CodeNumber = COM.VarAddrByteNumber; break;
				
				case C.保存_0_local_bit:		InsList[ index ].CODE0 = C.N_保存_0_local_bit; InsList[ index ].CODE1 = uint.Parse( InsList[ index ].InsCut[1] ); CodeNumber = COM.VarAddrByteNumber; break;
				case C.保存_0_local_bool:		InsList[ index ].CODE0 = C.N_保存_0_local_bool; InsList[ index ].CODE1 = uint.Parse( InsList[ index ].InsCut[1] ); CodeNumber = COM.VarAddrByteNumber; break;
				case C.保存_0_local_uint8:		InsList[ index ].CODE0 = C.N_保存_0_local_uint8; InsList[ index ].CODE1 = uint.Parse( InsList[ index ].InsCut[1] ); CodeNumber = COM.VarAddrByteNumber; break;
				case C.保存_0_local_uint16:		InsList[ index ].CODE0 = C.N_保存_0_local_uint16; InsList[ index ].CODE1 = uint.Parse( InsList[ index ].InsCut[1] ); CodeNumber = COM.VarAddrByteNumber; break;
				case C.保存_0_local_uint24:		InsList[ index ].CODE0 = C.N_保存_0_local_uint24; InsList[ index ].CODE1 = uint.Parse( InsList[ index ].InsCut[1] ); CodeNumber = COM.VarAddrByteNumber; break;
				case C.保存_0_local_uint32:		InsList[ index ].CODE0 = C.N_保存_0_local_uint32; InsList[ index ].CODE1 = uint.Parse( InsList[ index ].InsCut[1] ); CodeNumber = COM.VarAddrByteNumber; break;
				case C.保存_0_local_sint8:		InsList[ index ].CODE0 = C.N_保存_0_local_sint8; InsList[ index ].CODE1 = uint.Parse( InsList[ index ].InsCut[1] ); CodeNumber = COM.VarAddrByteNumber; break;
				case C.保存_0_local_sint16:		InsList[ index ].CODE0 = C.N_保存_0_local_sint16; InsList[ index ].CODE1 = uint.Parse( InsList[ index ].InsCut[1] ); CodeNumber = COM.VarAddrByteNumber; break;
				case C.保存_0_local_sint24:		InsList[ index ].CODE0 = C.N_保存_0_local_sint24; InsList[ index ].CODE1 = uint.Parse( InsList[ index ].InsCut[1] ); CodeNumber = COM.VarAddrByteNumber; break;
				case C.保存_0_local_sint32:		
				case C.保存_0_local_fix:		InsList[ index ].CODE0 = C.N_保存_0_local_sint32; InsList[ index ].CODE1 = uint.Parse( InsList[ index ].InsCut[1] ); CodeNumber = COM.VarAddrByteNumber; break;
				
				case C.加载_SP_local_uint16:	InsList[ index ].CODE0 = C.N_加载_SP_local_uint16; InsList[ index ].CODE1 = uint.Parse( InsList[ index ].InsCut[1] ); CodeNumber = COM.VarAddrByteNumber; break;
				case C.保存_SP_local_uint16:	InsList[ index ].CODE0 = C.N_保存_SP_local_uint16; InsList[ index ].CODE1 = uint.Parse( InsList[ index ].InsCut[1] ); CodeNumber = COM.VarAddrByteNumber; break;
				
				case C.加载_PC_local_uint16:
				case C.加载_PC_local_uint24:
				case C.加载_PC_local_uint32:
					InsList[ index ].CODE0 = C.N_加载_PC_local_uint; InsList[ index ].CODE1 = uint.Parse( InsList[ index ].InsCut[1] ); CodeNumber = COM.VarAddrByteNumber; break;
				
				/*
				case C.加载CODE_uint8:		InsList[ index ].CODE0 = C.N_加载CODE_uint8; CodeNumber = 0; break;
				case C.加载CODE_uint16:		InsList[ index ].CODE0 = C.N_加载CODE_uint16; CodeNumber = 0; break;
				case C.加载CODE_uint24:		InsList[ index ].CODE0 = C.N_加载CODE_uint24; CodeNumber = 0; break;
				case C.加载CODE_uint32:		InsList[ index ].CODE0 = C.N_加载CODE_uint32; CodeNumber = 0; break;
				*/
				
				case C.LOAD_local_uint8:	InsList[ index ].CODE0 = C.N_LOAD_local_uint8; CodeNumber = COM.VarAddrByteNumber; break;
				case C.LOAD_local_uint16:	InsList[ index ].CODE0 = C.N_LOAD_local_uint16; CodeNumber = COM.VarAddrByteNumber; break;
				case C.LOAD_local_uint24:	InsList[ index ].CODE0 = C.N_LOAD_local_uint24; CodeNumber = COM.VarAddrByteNumber; break;
				case C.LOAD_local_uint32:	InsList[ index ].CODE0 = C.N_LOAD_local_uint32; CodeNumber = COM.VarAddrByteNumber; break;
				
				case C.保存BASE_uint8:		InsList[ index ].CODE0 = C.N_保存BASE_uint8; CodeNumber = COM.None; break;
				case C.保存BASE_uint16:		InsList[ index ].CODE0 = C.N_保存BASE_uint16; CodeNumber = COM.None; break;
				case C.保存BASE_uint24:		InsList[ index ].CODE0 = C.N_保存BASE_uint24; CodeNumber = COM.None; break;
				case C.保存BASE_uint32:		InsList[ index ].CODE0 = C.N_保存BASE_uint32; CodeNumber = COM.None; break;
				
				case C.传递形参_bit:		InsList[ index ].CODE0 = C.N_传递形参_bit; CodeNumber = COM.None; break;
				case C.传递形参_bool:		InsList[ index ].CODE0 = C.N_传递形参_bool; CodeNumber = COM.None; break;
				case C.传递形参_uint8:		InsList[ index ].CODE0 = C.N_传递形参_uint8; CodeNumber = COM.None; break;
				case C.传递形参_uint16:		InsList[ index ].CODE0 = C.N_传递形参_uint16; CodeNumber = COM.None; break;
				case C.传递形参_uint24:		InsList[ index ].CODE0 = C.N_传递形参_uint24; CodeNumber = COM.None; break;
				case C.传递形参_uint32:		InsList[ index ].CODE0 = C.N_传递形参_uint32; CodeNumber = COM.None; break;
				case C.传递形参_sint8:		InsList[ index ].CODE0 = C.N_传递形参_sint8; CodeNumber = COM.None; break;
				case C.传递形参_sint16:		InsList[ index ].CODE0 = C.N_传递形参_sint16; CodeNumber = COM.None; break;
				case C.传递形参_sint24:		InsList[ index ].CODE0 = C.N_传递形参_sint24; CodeNumber = COM.None; break;
				case C.传递形参_sint32:
				case C.传递形参_fix:			InsList[ index ].CODE0 = C.N_传递形参_sint32; CodeNumber = COM.None; break;
				
				case C.地址偏移_uint8_uint8_uint8:
				case C.地址偏移_uint16_uint16_uint16:
				case C.地址偏移_uint24_uint24_uint24:
				case C.地址偏移_uint32_uint32_uint32:
					InsList[ index ].CODE0 = C.N_地址偏移_uintx_uintx_uintx; InsList[ index ].CODE1 = uint.Parse( InsList[ index ].InsCut[1] ); CodeNumber = COM.VarAddrByteNumber; break;
				
				case C.结构偏移_uint8_uint8:
				case C.结构偏移_uint16_uint16:
				case C.结构偏移_uint24_uint24:
				case C.结构偏移_uint32_uint32:
					InsList[ index ].CODE0 = C.N_结构偏移_uintx_uintx; InsList[ index ].CODE1 = uint.Parse( InsList[ index ].InsCut[1] ); CodeNumber = COM.VarAddrByteNumber; break;
				
				case C.写系统量_bit:
				case C.写系统量_bool:
				case C.写系统量_uint8:
				case C.写系统量_uint16:
				case C.写系统量_uint24:
				case C.写系统量_uint32:
				case C.写系统量_sint8:
				case C.写系统量_sint16:
				case C.写系统量_sint24:
				case C.写系统量_sint32:
				case C.写系统量_fix:
				
				case C.读系统量_bit:
				case C.读系统量_bool:
				case C.读系统量_uint8:
				case C.读系统量_uint16:
				case C.读系统量_uint24:
				case C.读系统量_uint32:
					InsList[index].isNone = true; CodeNumber = COM.None; break;
				
				case C.读系统量_sint8:		InsList[ index ].CODE0 = C.N_读系统量_sint8; CodeNumber = COM.None; break;
				case C.读系统量_sint16:		InsList[ index ].CODE0 = C.N_读系统量_sint16; CodeNumber = COM.None; break;
				case C.读系统量_sint24:		InsList[ index ].CODE0 = C.N_读系统量_sint24; CodeNumber = COM.None; break;
				case C.读系统量_sint32:
				case C.读系统量_fix:		InsList[ index ].CODE0 = C.N_读系统量_sint32; CodeNumber = COM.None; break;
				
				case C.赋值_bit_bit:
				case C.赋值_bool_bool:
				case C.赋值_uint8_uint8:
				case C.赋值_uint16_uint16:
				case C.赋值_uint24_uint24:
				case C.赋值_uint32_uint32:
				case C.赋值_sint8_sint8:
				case C.赋值_sint16_sint16:
				case C.赋值_sint24_sint24:
				case C.赋值_sint32_sint32:
				case C.赋值_fix_fix:
				
				case C.隐式转换_uint16_uint8:
				case C.隐式转换_uint24_uint8:
				case C.隐式转换_uint32_uint8:
				case C.隐式转换_uint24_uint16:
				case C.隐式转换_uint32_uint16:
				case C.隐式转换_uint32_uint24:
					InsList[index].isNone = true; CodeNumber = COM.None; break;
				
				case C.强制转换_sint8_uint8:		InsList[ index ].CODE0 = C.N_强制转换_sint8_uint8; CodeNumber = COM.None; break;
				case C.强制转换_sint16_uint16:	InsList[ index ].CODE0 = C.N_强制转换_sint16_uint16; CodeNumber = COM.None; break;
				case C.强制转换_sint32_uint32:	InsList[ index ].CODE0 = C.N_强制转换_sint32_uint32; CodeNumber = COM.None; break;
				case C.强制转换_uint8_sint8:		InsList[ index ].CODE0 = C.N_强制转换_uint8_sint8; CodeNumber = COM.None; break;
				case C.强制转换_uint16_sint16:	InsList[ index ].CODE0 = C.N_强制转换_uint16_sint16; CodeNumber = COM.None; break;
				case C.强制转换_uint32_sint32:	
				case C.强制转换_uint32_fix:		InsList[ index ].CODE0 = C.N_强制转换_uint32_sint32; CodeNumber = COM.None; break;
				case C.强制转换_fix_uint32:		InsList[ index ].CODE0 = C.N_强制转换_sint32_uint32; CodeNumber = COM.None; break;
				
				case C.隐式转换_sint16_sint8:		InsList[ index ].CODE0 = C.N_隐式转换_sint16_sint8; CodeNumber = COM.None; break;
				//case C.隐式转换_sint24_sint8:
				case C.隐式转换_sint32_sint8:		InsList[ index ].CODE0 = C.N_隐式转换_sint32_sint8; CodeNumber = COM.None; break;
				//case C.隐式转换_sint24_sint16:
				case C.隐式转换_sint32_sint16:	InsList[ index ].CODE0 = C.N_隐式转换_sint32_sint16; CodeNumber = COM.None; break;
				//case C.隐式转换_sint32_sint24:
				
				case C.隐式转换_fix_sint8:		InsList[ index ].CODE0 = C.N_隐式转换_fix_sint8; CodeNumber = COM.None; break;
				case C.隐式转换_fix_sint16:		InsList[ index ].CODE0 = C.N_隐式转换_fix_sint16; CodeNumber = COM.None; break;
				case C.隐式转换_fix_sint32:		InsList[ index ].CODE0 = C.N_隐式转换_fix_sint32; CodeNumber = COM.None; break;
				case C.隐式转换_sint32_fix:		InsList[ index ].CODE0 = C.N_隐式转换_sint32_fix; CodeNumber = COM.None; break;
				
				case C.强制转换_sint8_sint16:		InsList[ index ].CODE0 = C.N_强制转换_sint8_sint16; CodeNumber = COM.None; break;
				//case C.强制转换_sint8_sint24:
				case C.强制转换_sint8_sint32:		InsList[ index ].CODE0 = C.N_强制转换_sint8_sint32; CodeNumber = COM.None; break;
				//case C.强制转换_sint16_sint24:
				case C.强制转换_sint16_sint32:	InsList[ index ].CODE0 = C.N_强制转换_sint16_sint32; CodeNumber = COM.None; break;
				//case C.强制转换_sint24_sint32:
				
				case C.强制转换_uint8_uint16:		InsList[ index ].CODE0 = C.N_强制转换_uint8_uint16; CodeNumber = COM.None; break;
				//case C.强制转换_uint8_uint24:
				case C.强制转换_uint8_uint32:		InsList[ index ].CODE0 = C.N_强制转换_uint8_uint32; CodeNumber = COM.None; break;
				//case C.强制转换_uint16_uint24:
				case C.强制转换_uint16_uint32:	InsList[ index ].CODE0 = C.N_强制转换_uint16_uint32; CodeNumber = COM.None; break;
				//case C.强制转换_uint24_uint32:
				
				case C.非_bool_bool:		InsList[ index ].CODE0 = C.N_非_bool_bool; CodeNumber = COM.None; break;
				case C.与_bool_bool_bool:	InsList[ index ].CODE0 = C.N_与_bool_bool_bool; CodeNumber = COM.None; break;
				case C.或_bool_bool_bool:	InsList[ index ].CODE0 = C.N_或_bool_bool_bool; CodeNumber = COM.None; break;
				
				case C.取负_sint8_sint8:		InsList[ index ].CODE0 = C.N_取负_sint8_sint8; CodeNumber = COM.None; break;
				case C.取负_sint16_sint16:	InsList[ index ].CODE0 = C.N_取负_sint16_sint16; CodeNumber = COM.None; break;
				case C.取负_sint24_sint24:	InsList[ index ].CODE0 = C.N_取负_sint24_sint24; CodeNumber = COM.None; break;
				case C.取负_sint32_sint32:	
				case C.取负_fix_fix:		InsList[ index ].CODE0 = C.N_取负_sint32_sint32; CodeNumber = COM.None; break;
				
				case C.取绝对值_sint8_sint8:
				case C.取绝对值_sint16_sint16:
				case C.取绝对值_sint24_sint24:
				case C.取绝对值_sint32_sint32:
				case C.取绝对值_fix_fix:
					InsList[ index ].CODE0 = C.N_取绝对值_x_x; CodeNumber = COM.None; break;
				
				case C.自加_sint8_sint8:
				case C.自加_sint16_sint16:
				case C.自加_sint32_sint32:
				case C.自加_uint8_uint8:
				case C.自加_uint16_uint16:
				case C.自加_uint32_uint32:				
					InsList[ index ].CODE0 = C.N_自加_x_x; CodeNumber = COM.None; break;
				
				case C.自减_sint8_sint8:
				case C.自减_sint16_sint16:
				case C.自减_sint32_sint32:
				case C.自减_uint8_uint8:
				case C.自减_uint16_uint16:
				case C.自减_uint32_uint32:				
					InsList[ index ].CODE0 = C.N_自减_x_x; CodeNumber = COM.None; break;
				
				case C.加_uint8_uint8_uint8:		InsList[ index ].CODE0 = C.N_加_uint8_uint8_uint8; CodeNumber = COM.None; break;
				case C.加_uint16_uint16_uint16:	InsList[ index ].CODE0 = C.N_加_uint16_uint16_uint16; CodeNumber = COM.None; break;
				case C.加_uint24_uint24_uint24:	InsList[ index ].CODE0 = C.N_加_uint24_uint24_uint24; CodeNumber = COM.None; break;
				case C.加_uint32_uint32_uint32:	InsList[ index ].CODE0 = C.N_加_uint32_uint32_uint32; CodeNumber = COM.None; break;
				
				case C.加_sint8_sint8_sint8:		InsList[ index ].CODE0 = C.N_加_sint8_sint8_sint8; CodeNumber = COM.None; break;
				case C.加_sint16_sint16_sint16:	InsList[ index ].CODE0 = C.N_加_sint16_sint16_sint16; CodeNumber = COM.None; break;
				case C.加_sint24_sint24_sint24:	InsList[ index ].CODE0 = C.N_加_sint24_sint24_sint24; CodeNumber = COM.None; break;
				case C.加_sint32_sint32_sint32:	
				case C.加_fix_fix_fix:			InsList[ index ].CODE0 = C.N_加_sint32_sint32_sint32; CodeNumber = COM.None; break;
				
				case C.减_uint8_uint8_uint8:		InsList[ index ].CODE0 = C.N_减_uint8_uint8_uint8; CodeNumber = COM.None; break;
				case C.减_uint16_uint16_uint16:	InsList[ index ].CODE0 = C.N_减_uint16_uint16_uint16; CodeNumber = COM.None; break;
				case C.减_uint24_uint24_uint24:	InsList[ index ].CODE0 = C.N_减_uint24_uint24_uint24; CodeNumber = COM.None; break;
				case C.减_uint32_uint32_uint32:	InsList[ index ].CODE0 = C.N_减_uint32_uint32_uint32; CodeNumber = COM.None; break;
				
				case C.减_sint8_sint8_sint8:		InsList[ index ].CODE0 = C.N_减_sint8_sint8_sint8; CodeNumber = COM.None; break;
				case C.减_sint16_sint16_sint16:	InsList[ index ].CODE0 = C.N_减_sint16_sint16_sint16; CodeNumber = COM.None; break;
				case C.减_sint24_sint24_sint24:	InsList[ index ].CODE0 = C.N_减_sint24_sint24_sint24; CodeNumber = COM.None; break;
				case C.减_sint32_sint32_sint32:	
				case C.减_fix_fix_fix:			InsList[ index ].CODE0 = C.N_减_sint32_sint32_sint32; CodeNumber = COM.None; break;
				
				case C.乘_uint8_uint8_uint8:		InsList[ index ].CODE0 = C.N_乘_uint8_uint8_uint8; CodeNumber = COM.None; break;
				case C.乘_uint16_uint16_uint16:	InsList[ index ].CODE0 = C.N_乘_uint16_uint16_uint16; CodeNumber = COM.None; break;
				case C.乘_uint24_uint24_uint24:	InsList[ index ].CODE0 = C.N_乘_uint24_uint24_uint24; CodeNumber = COM.None; break;
				case C.乘_uint32_uint32_uint32:	InsList[ index ].CODE0 = C.N_乘_uint32_uint32_uint32; CodeNumber = COM.None; break;
				
				case C.乘_sint8_sint8_sint8:		InsList[ index ].CODE0 = C.N_乘_sint8_sint8_sint8; CodeNumber = COM.None; break;
				case C.乘_sint16_sint16_sint16:	InsList[ index ].CODE0 = C.N_乘_sint16_sint16_sint16; CodeNumber = COM.None; break;
				case C.乘_sint24_sint24_sint24:	InsList[ index ].CODE0 = C.N_乘_sint24_sint24_sint24; CodeNumber = COM.None; break;
				case C.乘_sint32_sint32_sint32:	InsList[ index ].CODE0 = C.N_乘_sint32_sint32_sint32; CodeNumber = COM.None; break;
				case C.乘_fix_fix_fix:			InsList[ index ].CODE0 = C.N_乘_fix_fix_fix; CodeNumber = COM.None; break;
				
				case C.除_uint8_uint8_uint8:		InsList[ index ].CODE0 = C.N_除_uint8_uint8_uint8; CodeNumber = COM.None; break;
				case C.除_uint16_uint16_uint16:	InsList[ index ].CODE0 = C.N_除_uint16_uint16_uint16; CodeNumber = COM.None; break;
				case C.除_uint24_uint24_uint24:	InsList[ index ].CODE0 = C.N_除_uint24_uint24_uint24; CodeNumber = COM.None; break;
				case C.除_uint32_uint32_uint32:	InsList[ index ].CODE0 = C.N_除_uint32_uint32_uint32; CodeNumber = COM.None; break;
				
				case C.除_sint8_sint8_sint8:		InsList[ index ].CODE0 = C.N_除_sint8_sint8_sint8; CodeNumber = COM.None; break;
				case C.除_sint16_sint16_sint16:	InsList[ index ].CODE0 = C.N_除_sint16_sint16_sint16; CodeNumber = COM.None; break;
				case C.除_sint24_sint24_sint24:	InsList[ index ].CODE0 = C.N_除_sint24_sint24_sint24; CodeNumber = COM.None; break;
				case C.除_sint32_sint32_sint32:	InsList[ index ].CODE0 = C.N_除_sint32_sint32_sint32; CodeNumber = COM.None; break;
				case C.除_fix_fix_fix:			InsList[ index ].CODE0 = C.N_除_fix_fix_fix; CodeNumber = COM.None; break;
				
				case C.余_uint8_uint8_uint8:		InsList[ index ].CODE0 = C.N_余_uint8_uint8_uint8; CodeNumber = COM.None; break;
				case C.余_uint16_uint16_uint16:	InsList[ index ].CODE0 = C.N_余_uint16_uint16_uint16; CodeNumber = COM.None; break;
				case C.余_uint24_uint24_uint24:	InsList[ index ].CODE0 = C.N_余_uint24_uint24_uint24; CodeNumber = COM.None; break;
				case C.余_uint32_uint32_uint32:	InsList[ index ].CODE0 = C.N_余_uint32_uint32_uint32; CodeNumber = COM.None; break;
				
				case C.余_sint8_sint8_sint8:		InsList[ index ].CODE0 = C.N_余_sint8_sint8_sint8; CodeNumber = COM.None; break;
				case C.余_sint16_sint16_sint16:	InsList[ index ].CODE0 = C.N_余_sint16_sint16_sint16; CodeNumber = COM.None; break;
				case C.余_sint24_sint24_sint24:	InsList[ index ].CODE0 = C.N_余_sint24_sint24_sint24; CodeNumber = COM.None; break;
				case C.余_sint32_sint32_sint32:	InsList[ index ].CODE0 = C.N_余_sint32_sint32_sint32; CodeNumber = COM.None; break;
				
				case C.大于_bool_uint8_uint8:
				case C.大于_bool_uint16_uint16:
				case C.大于_bool_uint24_uint24:
				case C.大于_bool_uint32_uint32:
					InsList[ index ].CODE0 = C.N_大于_bool_uint_uint; CodeNumber = COM.None; break;
				case C.大于_bool_sint8_sint8:
				case C.大于_bool_sint16_sint16:
				case C.大于_bool_sint24_sint24:
				case C.大于_bool_sint32_sint32:
				case C.大于_bool_fix_fix:
					InsList[ index ].CODE0 = C.N_大于_bool_sint_sint; CodeNumber = COM.None; break;
				
				case C.小于_bool_uint8_uint8:
				case C.小于_bool_uint16_uint16:
				case C.小于_bool_uint24_uint24:
				case C.小于_bool_uint32_uint32:
					InsList[ index ].CODE0 = C.N_小于_bool_uint_uint; CodeNumber = COM.None; break;
				case C.小于_bool_sint8_sint8:
				case C.小于_bool_sint16_sint16:
				case C.小于_bool_sint24_sint24:
				case C.小于_bool_sint32_sint32:
				case C.小于_bool_fix_fix:
					InsList[ index ].CODE0 = C.N_小于_bool_sint_sint; CodeNumber = COM.None; break;
				
				case C.大于等于_bool_uint8_uint8:
				case C.大于等于_bool_uint16_uint16:
				case C.大于等于_bool_uint24_uint24:
				case C.大于等于_bool_uint32_uint32:
					InsList[ index ].CODE0 = C.N_大于等于_bool_uint_uint; CodeNumber = COM.None; break;
				case C.大于等于_bool_sint8_sint8:
				case C.大于等于_bool_sint16_sint16:
				case C.大于等于_bool_sint24_sint24:
				case C.大于等于_bool_sint32_sint32:
				case C.大于等于_bool_fix_fix:
					InsList[ index ].CODE0 = C.N_大于等于_bool_sint_sint; CodeNumber = COM.None; break;
				
				case C.小于等于_bool_uint8_uint8:
				case C.小于等于_bool_uint16_uint16:
				case C.小于等于_bool_uint24_uint24:
				case C.小于等于_bool_uint32_uint32:
					InsList[ index ].CODE0 = C.N_小于等于_bool_uint_uint; CodeNumber = COM.None; break;
				case C.小于等于_bool_sint8_sint8:
				case C.小于等于_bool_sint16_sint16:
				case C.小于等于_bool_sint24_sint24:
				case C.小于等于_bool_sint32_sint32:
				case C.小于等于_bool_fix_fix:
					InsList[ index ].CODE0 = C.N_小于等于_bool_sint_sint; CodeNumber = COM.None; break;
				
				case C.等于_bool_bit_bit:
				case C.等于_bool_uint8_uint8:
				case C.等于_bool_uint16_uint16:
				case C.等于_bool_uint24_uint24:
				case C.等于_bool_uint32_uint32:
					InsList[ index ].CODE0 = C.N_等于_bool_uint_uint; CodeNumber = COM.None; break;
				case C.等于_bool_sint8_sint8:
				case C.等于_bool_sint16_sint16:
				case C.等于_bool_sint24_sint24:
				case C.等于_bool_sint32_sint32:
					InsList[ index ].CODE0 = C.N_等于_bool_sint_sint; CodeNumber = COM.None; break;
				
				case C.不等于_bool_bit_bit:
				case C.不等于_bool_uint8_uint8:
				case C.不等于_bool_uint16_uint16:
				case C.不等于_bool_uint24_uint24:
				case C.不等于_bool_uint32_uint32:
					InsList[ index ].CODE0 = C.N_不等于_bool_uint_uint; CodeNumber = COM.None; break;
				case C.不等于_bool_sint8_sint8:
				case C.不等于_bool_sint16_sint16:
				case C.不等于_bool_sint24_sint24:
				case C.不等于_bool_sint32_sint32:
					InsList[ index ].CODE0 = C.N_不等于_bool_sint_sint; CodeNumber = COM.None; break;
				
				case C.取反_bit_bit:			InsList[ index ].CODE0 = C.N_取反_bit_bit; CodeNumber = COM.None; break;
				case C.取反_uint8_uint8:		InsList[ index ].CODE0 = C.N_取反_uint8_uint8; CodeNumber = COM.None; break;
				case C.取反_uint16_uint16:		InsList[ index ].CODE0 = C.N_取反_uint16_uint16; CodeNumber = COM.None; break;
				case C.取反_uint24_uint24:		InsList[ index ].CODE0 = C.N_取反_uint24_uint24; CodeNumber = COM.None; break;
				case C.取反_uint32_uint32:		InsList[ index ].CODE0 = C.N_取反_uint32_uint32; CodeNumber = COM.None; break;
				
				case C.与_bit_bit_bit:
				case C.与_uint8_uint8_uint8:
				case C.与_uint16_uint16_uint16:
				case C.与_uint24_uint24_uint24:
				case C.与_uint32_uint32_uint32:
					InsList[ index ].CODE0 = C.N_与_x_x_x; CodeNumber = COM.None; break;
				
				case C.或_bit_bit_bit:
				case C.或_uint8_uint8_uint8:
				case C.或_uint16_uint16_uint16:
				case C.或_uint24_uint24_uint24:
				case C.或_uint32_uint32_uint32:
					InsList[ index ].CODE0 = C.N_或_x_x_x; CodeNumber = COM.None; break;
				
				case C.异或_bit_bit_bit:
				case C.异或_uint8_uint8_uint8:
				case C.异或_uint16_uint16_uint16:
				case C.异或_uint24_uint24_uint24:
				case C.异或_uint32_uint32_uint32:
					InsList[ index ].CODE0 = C.N_异或_x_x_x; CodeNumber = COM.None; break;
				
				case C.补0左移_uint8_uint8_uint8:		InsList[ index ].CODE0 = C.N_补0左移_uint8_uint8_uint8; CodeNumber = COM.None; break;
				case C.补0左移_uint16_uint16_uint8:		InsList[ index ].CODE0 = C.N_补0左移_uint16_uint16_uint8; CodeNumber = COM.None; break;
				case C.补0左移_uint24_uint24_uint8:		InsList[ index ].CODE0 = C.N_补0左移_uint24_uint24_uint8; CodeNumber = COM.None; break;
				case C.补0左移_uint32_uint32_uint8:		InsList[ index ].CODE0 = C.N_补0左移_uint32_uint32_uint8; CodeNumber = COM.None; break;
				
				case C.补0右移_uint8_uint8_uint8:
				case C.补0右移_uint16_uint16_uint8:
				case C.补0右移_uint24_uint24_uint8:
				case C.补0右移_uint32_uint32_uint8:
					InsList[ index ].CODE0 = C.N_补0右移_x_x_x; CodeNumber = COM.None; break;
				
				case C.读取分量_bit_uint8:		InsList[ index ].CODE0 = C.N_读取分量_bit_uint8; InsList[ index ].CODE1 = uint.Parse( InsList[ index ].InsCut[1] ); CodeNumber = COM.SubValueByteNumber; break;
				case C.读取分量_bit_uint16:		InsList[ index ].CODE0 = C.N_读取分量_bit_uint16; InsList[ index ].CODE1 = uint.Parse( InsList[ index ].InsCut[1] ); CodeNumber = COM.SubValueByteNumber; break;
				case C.读取分量_bit_uint32:		InsList[ index ].CODE0 = C.N_读取分量_bit_uint32; InsList[ index ].CODE1 = uint.Parse( InsList[ index ].InsCut[1] ); CodeNumber = COM.SubValueByteNumber; break;
				case C.读取分量_uint8_uint32:	InsList[ index ].CODE0 = C.N_读取分量_uint8_uint32; InsList[ index ].CODE1 = uint.Parse( InsList[ index ].InsCut[1] ); CodeNumber = COM.SubValueByteNumber; break;
				case C.读取分量_uint8_uint16:	InsList[ index ].CODE0 = C.N_读取分量_uint8_uint16; InsList[ index ].CODE1 = uint.Parse( InsList[ index ].InsCut[1] ); CodeNumber = COM.SubValueByteNumber; break;
				case C.读取分量_uint16_uint32:	InsList[ index ].CODE0 = C.N_读取分量_uint16_uint32; InsList[ index ].CODE1 = uint.Parse( InsList[ index ].InsCut[1] ); CodeNumber = COM.SubValueByteNumber; break;
				
				case C.写入分量_uint8_uint8_bit:		InsList[ index ].CODE0 = C.N_写入分量_uint8_uint8_bit; InsList[ index ].CODE1 = uint.Parse( InsList[ index ].InsCut[1] ); CodeNumber = COM.SubValueByteNumber; break;
				case C.写入分量_uint16_uint16_bit:		InsList[ index ].CODE0 = C.N_写入分量_uint16_uint16_bit; InsList[ index ].CODE1 = uint.Parse( InsList[ index ].InsCut[1] ); CodeNumber = COM.SubValueByteNumber; break;
				case C.写入分量_uint32_uint32_bit:		InsList[ index ].CODE0 = C.N_写入分量_uint32_uint32_bit; InsList[ index ].CODE1 = uint.Parse( InsList[ index ].InsCut[1] ); CodeNumber = COM.SubValueByteNumber; break;
				case C.写入分量_uint16_uint16_uint8:	InsList[ index ].CODE0 = C.N_写入分量_uint16_uint16_uint8; InsList[ index ].CODE1 = uint.Parse( InsList[ index ].InsCut[1] ); CodeNumber = COM.SubValueByteNumber; break;
				case C.写入分量_uint32_uint32_uint16:	InsList[ index ].CODE0 = C.N_写入分量_uint32_uint32_uint16; InsList[ index ].CODE1 = uint.Parse( InsList[ index ].InsCut[1] ); CodeNumber = COM.SubValueByteNumber; break;
				case C.写入分量_uint32_uint32_uint8:	InsList[ index ].CODE0 = C.N_写入分量_uint32_uint32_uint8; InsList[ index ].CODE1 = uint.Parse( InsList[ index ].InsCut[1] ); CodeNumber = COM.SubValueByteNumber; break;
				
				default: n_OS.VIO.Show( "<转换字节码> 未知的虚拟机指令: " + InsList[index].InsCut[ 0 ] ); break;
				//default:  CodeNumber = 0; break;
			}
			InsList[ index ].CodeNumber = CodeNumber;
		}
		
		static void OutputByteCode( int index )
		{
			switch( InsList[index].InsCut[ 0 ] ) {
				
				case C.CODE:
					string[] CodeNumberList = InsList[index].InsCut[ 1 ].Split( ',' );
					for( int j = 0; j < CodeNumberList.Length; ++j ) {
						
						if( CodeNumberList[ j ].StartsWith( "&" ) ) {
							FuncAddrList[FuncLength] = CodeNumberList[ j ] + "," + BCLength;
							FuncLength++;
							AddCode( 0 );
						}
						else {
							AddCode( byte.Parse( CodeNumberList[ j ] ) );
						}
					}
					break;
				
				case C.SYS_PAD:
					int jn = 4 - BCLength % 4;
					if( jn != 4 ) {
						for( int ii = 0; ii < jn; ++ii ) {
							AddCode( 0 );
						}
					}
					break;
				default: break;
			}
			int CodeNumber = InsList[ index ].CodeNumber;
			if( CodeNumber == -1 ) {
				return;
			}
			if( InsList[index].isNone ) {
				return;
			}
			if( InsList[ index ].isAlign && ExistEm ) {
				AddCode( 0 );
				AddCode( 0 );
				AddCode( 0 );
				ExistEm = false;
			}
			InsList[index].InsAddr = BCLength;
			
			if( !ExistEm ) {
				if( CodeNumber == 0 ) {
					AddCode( InsList[ index ].CODE0 );
					
					if( Zip ) {
						ExistEm = true;
					}
					else {
						AddCode( 0 );
						AddCode( 0 );
						AddCode( 0 );
						ExistEm = false;
					}
					return;
				}
				if( CodeNumber == 1 ) {
					AddCode( InsList[ index ].CODE0 );
					AddCode( (byte)(InsList[ index ].CODE1) );
					AddCode( 0 );
					AddCode( 0 );
					return;
				}
				if( CodeNumber == 2 ) {
					AddCode( InsList[ index ].CODE0 );
					AddCode( (byte)(InsList[ index ].CODE1 % 0x100) );
					AddCode( (byte)(InsList[ index ].CODE1 / 0x100) );
					AddCode( 0 );
					return;
				}
				if( CodeNumber == 3 ) {
					AddCode( InsList[ index ].CODE0 );
					AddCode( (byte)(InsList[ index ].CODE1 % 0x100) );
					AddCode( (byte)(InsList[ index ].CODE1 / 0x100 % 0x100) );
					AddCode( (byte)(InsList[ index ].CODE1 / 0x10000 % 0x100) );
					return;
				}
				if( CodeNumber == 4 ) {
					AddCode( InsList[ index ].CODE0 );
					AddCode( InsList[ index ].CODE0EXT ); //仅用于分支比较指令
					AddCode( 0 );
					AddCode( InsList[ index ].CODE0END );
					
					AddCode( (byte)(InsList[ index ].CODE1 % 0x100) );
					AddCode( (byte)(InsList[ index ].CODE1 / 0x100 % 0x100) );
					AddCode( (byte)(InsList[ index ].CODE1 / 0x10000 % 0x100) );
					AddCode( (byte)(InsList[ index ].CODE1 / 0x1000000) );
					return;
				}
			}
			else {
				ExistEm = false;
				if( CodeNumber == 0 ) {
					AddCode( InsList[ index ].CODE0 );
					AddCode( 0 );
					AddCode( 0 );
					return;
				}
				if( CodeNumber == 1 ) {
					AddCode( InsList[ index ].CODE0 );
					AddCode( (byte)(InsList[ index ].CODE1) );
					AddCode( 0 );
					return;
				}
				if( CodeNumber == 2 ) {
					AddCode( InsList[ index ].CODE0 );
					AddCode( (byte)(InsList[ index ].CODE1 % 0x100) );
					AddCode( (byte)(InsList[ index ].CODE1 / 0x100) );
					return;
				}
				if( CodeNumber == 3 ) {
					
					if( InsList[ index ].CODE1 <= 0xFFFF ) {
						AddCode( InsList[ index ].CODE0 );
						AddCode( (byte)(InsList[ index ].CODE1 % 0x100) );
						AddCode( (byte)(InsList[ index ].CODE1 / 0x100 % 0x100) );
					}
					else {
						AddCode( 0 );
						AddCode( 0 );
						AddCode( 0 );
						
						AddCode( InsList[ index ].CODE0 );
						AddCode( (byte)(InsList[ index ].CODE1 % 0x100) );
						AddCode( (byte)(InsList[ index ].CODE1 / 0x100 % 0x100) );
						AddCode( (byte)(InsList[ index ].CODE1 / 0x10000 % 0x100) );
					}
					return;
				}
				if( CodeNumber == 4 ) {
					
					if( InsList[ index ].CODE0END == 0 ) {
						AddCode( InsList[ index ].CODE0 );
						AddCode( InsList[ index ].CODE0EXT ); //仅用于分支比较指令
						AddCode( 0 );
					}
					else {
						AddCode( 0 );
						AddCode( 0 );
						AddCode( 0 );
						
						AddCode( InsList[ index ].CODE0 );
						AddCode( InsList[ index ].CODE0EXT ); //仅用于分支比较指令
						AddCode( 0 );
						AddCode( InsList[ index ].CODE0END );
					}
					
					AddCode( (byte)(InsList[ index ].CODE1 % 0x100) );
					AddCode( (byte)(InsList[ index ].CODE1 / 0x100 % 0x100) );
					AddCode( (byte)(InsList[ index ].CODE1 / 0x10000 % 0x100) );
					AddCode( (byte)(InsList[ index ].CODE1 / 0x1000000) );
					return;
				}
			}
			n_OS.VIO.Show( "未知的指令长度: " + CodeNumber );
		}
		
		static void DealAddr( bool first )
		{
			//string Hex = "";
			for( int i = 0; i < InsList.Length; ++i ) {
				switch( InsList[i].CODE0 ) {
					case C.N_分支比较_uint32_uint8:
					case C.N_跳转:
					case C.N_函数调用:
					case C.N_肯定跳转_bool:
					case C.N_否定跳转_bool:
					case C.N_跳转递减_x:
					case C.N_指令地址:
						
						if( first ) {
							//指令本身和其指向目标都应是对齐的!!!! 2022.12.3
							InsList[i].isAlign = true;
							InsList[InsList[i].CODE1].isAlign = true;
						}
						else {
							int Start = 1;
							if( InsList[i].CODE0 == C.N_分支比较_uint32_uint8 ) {
								Start = 4;
							}
							int Addr = InsList[InsList[i].CODE1].InsAddr;
							for( int n = 0; n < COM.GotoByteNumber; ++n ) {
								ByteCodeList[InsList[i].InsAddr + n + Start] = (byte)(Addr % 256);
								Addr /= 256;
							}
						}
						break;
					default: break;
				}
				//Hex += InsList[i].ToString() + "\n";
			}
		}
		
		//----------------------------------
		
		static void AddCode( byte c )
		{
			ByteCodeList[BCLength] = c;
			BCLength++;
		}
		
		static void GetInsTotalMap()
		{
			int[] List = new int[256];
			
			for( int i = 0; i < InsList.Length; ++i ) {
				
				List[ InsList[i].CODE0 ]++;
			}
			
			string s = "";
			for( int i = 0; i < List.Length; ++i ) {
				
				string p = "";
				for( int n = 0; n < List[i]; ++n ) {
					p += "+";
				}
				
				s += i + ":\t" + List[i].ToString() + "\t"+ p + "\n";
			}
			
			n_OS.VIO.SaveTextFileGB2312( "D:\\result.txt", s );
		}
	}
}


