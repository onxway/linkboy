﻿
//函数调用信息列表
namespace n_CallList
{
using System;
using System.Text;
using n_FunctionNode;
using n_FunctionList;
using n_MemberType;

public static class CallList
{
	//清除函数信息
	public static void Clear()
	{
		FunctionCallList = new string[ FunctionList.length ];
		for( int i = 0; i < FunctionCallList.Length; ++i ) {
			FunctionCallList[ i ] = "";
		}
	}
	
	//添加被调函数索引
	public static void AddSubFunction( int FunctionIndex, string FuncIndex )
	{
		FunctionCallList[ FunctionIndex ] += FuncIndex + " ";
	}
	
	//获取函数调用的子函数列表
	public static string GetSubFunction( int Index )
	{
		return FunctionCallList[ Index ];
	}
	
	static string[] FunctionCallList;	//节点集合
}
}
