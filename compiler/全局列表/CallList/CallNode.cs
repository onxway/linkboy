﻿
//调用节点
namespace n_CallNode
{
using System;
using n_UnitList;

public class CallNode
{
	//构造函数
	public CallNode()
	{
		isMalloc = false;
		CallFuncList = "";
		UsedAddressList = new string[ UnitList.length ];
		for( int i = 0; i < UsedAddressList.Length; ++i ) {
			UsedAddressList[ i ] = "";
		}
	}
	
	//显示字符串
	public override string ToString()
	{
		string Result = null;
		Result = "已分配:" + isMalloc + "  子函数:<" + CallFuncList + ">\n已用空间:\n";
		for( int i = 0; i < UsedAddressList.Length; ++i ) {
			Result += i + ": " + UsedAddressList[ i ] + "\n";
		}
		return Result;
	}
	
	public bool isMalloc;
	public string CallFuncList;
	public string[] UsedAddressList;
}
}
