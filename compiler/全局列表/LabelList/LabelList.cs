﻿
//标号列表类
namespace n_LabelList
{
using System;
using n_LabelNode;
using System.Collections.Generic;

public static class LabelList
{
	//初始化
	public static void Init()
	{
		NodeSet = new LabelNode[ 1000 ];
		
		NodeDic = new Dictionary<string, LabelNode>();
	}
	
	//清除标号列表
	public static void Clear()
	{
		length = 0;
		NodeDic.Clear();
	}
	
	//显示
	public static string Show()
	{
		string Result = "";
		for( int i = 0; i < length; ++i ) {
			Result += NodeSet[ i ].ToString() + "\n";
		}
		return Result;
	}
	
	//标号替换 - 用于 verilog
	public static string V_Replace( string c )
	{
		for( int i = 0; i < length; ++i ) {
			if( c.IndexOf( "$" + NodeSet[ i ].LabelName + "$" ) != -1 ) {
			   	c = c.Replace( "$" + NodeSet[ i ].LabelName + "$", NodeSet[ i ].V_PC.ToString() );
			}
		}
		return c;
	}
	
	//添加标号
	public static void Add( LabelNode f )
	{
		NodeSet[ length ] = f;
		++length;
	}
	
	//判断标号是否存在
	public static bool isExist( string Name, int FunctionIndex )
	{
		for( int i = 0; i < length; ++i ) {
			if( NodeSet[ i ].Name == Name && NodeSet[ i ].FunctionIndex == FunctionIndex ) {
				return true;
			}
		}
		return false;
	}
	
	//获取底层名称
	public static string GetLabelName( string Name, int FunctionIndex )
	{
		for( int i = 0; i < length; ++i ) {
			if( NodeSet[ i ].Name == Name && NodeSet[ i ].FunctionIndex == FunctionIndex ) {
				return NodeSet[ i ].LabelName;
			}
		}
		return null;
	}
	
	static Dictionary<string, LabelNode> NodeDic;
	
	static LabelNode[] NodeSet;
	public static int length;
}
}
