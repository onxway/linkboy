﻿
//标号节点类
namespace n_LabelNode
{
using System;

public class LabelNode
{
	//构造函数
	public LabelNode( string name, string labelName, int level, int functionIndex )
	{
		this.Name = name;
		this.LabelName = labelName;
		this.Level = level;
		this.FunctionIndex = functionIndex;
	}
	
	//显示
	public override string ToString()
	{
		return "名称: " + LabelName + " 级别: " + Level + " 函数序号: " + FunctionIndex;
	}
	
	public string Name;
	public string LabelName;
	public int Level;
	public int FunctionIndex;
	
	public int V_PC;
}
}
