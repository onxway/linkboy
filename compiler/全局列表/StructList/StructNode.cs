﻿
//结构体信息节点类
namespace n_StructNode
{
using System;
using n_VarType;

public class StructNode
{
	//构造函数
	public StructNode( string vVisitType, string vRealRefType, string vName, MemberNode[] vMemberList, string vUnitName, int vWordIndex )
	{
		VisitType = vVisitType;
		RealRefType = vRealRefType;
		Name = vName;
		WordIndex = vWordIndex;
		MemberList = vMemberList;
		UnitName = vUnitName;
		Size = -1;
		StoreType = VarType.VBase;
		TargetMemberName = null;
		isStruct = true;
		ArrayNumber = -1;
	}
	
	//重载-显示字符串
	public override string ToString()
	{
		string member = "";
		for( int i = 0; i < MemberList.Length; ++i ) {
			member += "\t" + MemberList[ i ].Type + " " + MemberList[ i ].Name + "\n";
		}
		return "访问类型: " + VisitType + " 引用类型: " + RealRefType + " 名称: " + Name + " 大小:" + Size + "\n" + member.TrimEnd( '\n' );
	}
	
	//获取结构体分量的类型
	public string GetSubType( string SubName )
	{
		for( int j = 0; j < MemberList.Length; ++j ) {
			if( MemberList[ j ].Name == SubName ) {
				return MemberList[ j ].Type;
			}
		}
		return null;
	}
	
	//获取偏移量数值, 也就是占据的基本存储单元数目
	public int GetOffset( string SubName )
	{
		for( int j = 0; j < MemberList.Length; ++j ) {
			if( MemberList[ j ].Name == SubName ) {
				return MemberList[ j ].Offset;
			}
		}
		return -1;
	}
	
	public readonly string VisitType; 		//访问类型
	public readonly string RealRefType; 	//引用类型
	public readonly string Name; 		//结构体名称
	public readonly int WordIndex;		//代表定义位置的词法序号
	public string TargetMemberName;		//目标成员
	public int Size;					//结构体尺寸
	public string StoreType;			//结构体存储类型
	public MemberNode[] MemberList;		//成员列表
	public bool isStruct;				//是结构体还是数组
	public int ArrayNumber;				//是数组的话,其长度
	public string UnitName;				//这项用于保存子项类型解析时的起点
}

public class MemberNode
{
	//构造函数
	public MemberNode( int vTypeIndex, string vName, int vLocation )
	{
		TypeIndex = vTypeIndex;
		Name = vName;
		Location = vLocation;
		Size = -1;
		Offset = -1;
		Type = null; 
	}
	
	public string Type;		//类型
	public string Name;		//名称
	public int Location;	//位置
	public int Size;		//大小
	public int Offset;		//偏移量
	public int TypeIndex;	//类型在语法树中的索引,用于推迟子成员的类型判断
}
}
