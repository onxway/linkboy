﻿
//函数代码信息列表
namespace n_FunctionCodeList
{
using System;
using System.Text;
using n_FunctionList;

public static class FunctionCodeList
{
	public static string LastIns;
	
	public static string LastFactIns;
	
	//清除函数代码信息
	public static void Clear()
	{
		LastIns = null;
		NodeSet = new StringBuilder[ FunctionList.length ];
		
		for( int i = 0; i < NodeSet.Length; ++i ) {
			NodeSet[ i ] = new StringBuilder( "" );
		}
	}
	
	//设置当前函数序号
	public static void SetFunctionIndex( int FuncIndex )
	{
		FunctionIndex = FuncIndex;
	}
	
	//向函数代码添加函数
	public static void AddCode( string instruction )
	{
		if( LastIns != null ) {
			NodeSet[ FunctionIndex ].Append( LastIns );
			NodeSet[ FunctionIndex ].Append( "\n" );
		}
		LastIns = instruction;
		
		//if( instruction != n_Code1.Code1.语句层减 && !instruction.StartsWith( n_Code1.Code1.标号 ) ) {
		//	LastFactIns = instruction;
		//}
	}
	
	//获取函数代码
	public static string GetCode( int FuncIndex )
	{
		return NodeSet[ FuncIndex ].ToString().TrimEnd( '\n' );
	}
	
	//获取代码层1目标代码
	public static string ShowCode()
	{
		int length = NodeSet.Length;
		StringBuilder Result = new StringBuilder( "" );
		for( int i = 0; i < length; ++i ) {
			Result.Append( NodeSet[ i ] );
		}
		return Result.ToString();
	}
	
	static int FunctionIndex;
	static StringBuilder[] NodeSet;	//节点集合
}
}
