﻿
//函数信息列表
//	string FunctionName;	//函数名
//	string ReturnType;		//函数返回类型
//	string VarType;			//函数形参类型列表, 用逗号分隔
							//当无形参时值为 void, 其他情况: 形参1类型 , 形参2类型 , 形参3类型 ...
//	string FunctionType;	//函数类型 public  private  interface  interrupt
//	string UnitName;		//所在的元件名
//	string VarIndexList;	//形参序号列表, 用空格分隔, 每一项用变量索引表示
namespace n_FunctionList
{
using System;
using n_FunctionNode;
using n_MemberType;
using n_UnitList;
using n_WordList;
using n_ParseNet;
using n_ET;
using n_VarList;
using n_VarType;
using System.Collections.Generic;

public static class FunctionList
{
	//初始化
	public static void Init()
	{
		NodeSet = new FunctionNode[ 10000 ];
		NodeDic = new Dictionary<string, int>();
	}
	
	//清除函数信息
	public static void Clear()
	{
		length = 0;
		NodeDic.Clear();
	}
	
	//显示
	public static string Show()
	{
		string Result = "";
		for( int i = 0; i < length; ++i ) {
			Result += i + " " + NodeSet[ i ] + "\n";
		}
		return Result;
	}
	
	//添加函数
	public static void Add( FunctionNode f )
	{
		NodeDic.Add( f.FunctionName, length );
		
		NodeSet[ length ] = f;
		++length;
	}
	
	//根据元件名获取函数直接索引
	public static int GetDirectIndex( string Name )
	{
		if( NodeDic.ContainsKey( Name ) ) {
			return NodeDic[Name];
		}
		else {
			return -1;
		}
		
//		for( int i = 0; i < length; ++i ) {
//			if( NodeSet[ i ].FunctionName == Name ) {
//				return i;
//			}
//		}
//		return -1;
	}
	
	//根据函数名获取最终直接索引(进行了虚元件置换)
	public static int GetSurfaceIndex( string Name )
	{
		int FunctionIndex = GetDirectIndex( Name );
		if( FunctionIndex == -1 ) {
			int SplitIndex = Name.LastIndexOf( "." );
			
			//数组中存在标签时会出现 -1 情况
			if( SplitIndex == -1 ) {
				return -1;
			}
			
			string UnitName = Name.Remove( SplitIndex );
			string FunctionName = Name.Remove( 0, SplitIndex );
			int UnitIndex = UnitList.GetIndex( UnitName );
			if( UnitIndex == -1 ) {
				return UnitIndex;
			}
			string NewName = UnitList.Get( UnitIndex ).UnitName + FunctionName;
			FunctionIndex = GetDirectIndex( NewName );
		}
		return FunctionIndex;
	}
	
	//获取函数索引
	public static int GetIndex( string Name )
	{
		if( Name == null ) {
			return -1;
		}
		int FunctionIndex = GetSurfaceIndex( Name );
		if( FunctionIndex == -1 ) {
			return FunctionIndex;
		}
		if( NodeSet[ FunctionIndex ].RealRefType == MemberType.RealRefType.Real ) {
			return FunctionIndex;
		}
		return GetIndex( NodeSet[ FunctionIndex ].TargetMemberName );
	}
	
	//获取在某个元件中的函数名集合
	public static string GetFunctionNames( string UnitName )
	{
		string result = null;
		for( int i = 0; i < length; ++i ) {
			
			if( NodeSet[ i ].UnitName == UnitName ) {//&&
			    //NodeSet[ i ].VisitType == MemberType.VisitType.Public ) {
				
				int fi = GetIndex( NodeSet[ i ].FunctionName );
				
				string Name = NodeSet[ i ].FunctionName;
				Name = Name.Remove( 0, Name.LastIndexOf( "." ) + 1 );
				if( !Name.StartsWith( "OS_" ) && fi != -1 ) {
					result += Name + "(";
					if( NodeSet[ fi ].VarTypeList != null ) {
						
						result += " ";
						
						string[] TypeList = NodeSet[ fi ].VarTypeList.Split( ',' );
						string TypeArray = "";
						for( int n = 0; n < TypeList.Length; ++n ) {
							if( n == TypeList.Length - 1 ) {
								TypeArray += TypeList[n] + " ";
							}
							else {
								TypeArray += TypeList[n] + ", ";
							}
						}
						//TypeArray = TypeArray.TrimEnd( ' ' );
						result += TypeArray;
						
						//result += " ";
					}
					result += ");";
				}
			}
		}
		return result;
	}
	
	//获取函数属性
	public static FunctionNode Get( int Index )
	{
		return NodeSet[ Index ];
	}
	
	static Dictionary<string, int> NodeDic;
	
	static FunctionNode[] NodeSet;	//节点集合
	public static int length;			//长度
}
}




