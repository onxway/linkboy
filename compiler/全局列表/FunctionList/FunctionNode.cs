﻿
//函数信息节点类
namespace n_FunctionNode
{
using System;
using System.Text;
using n_UnitList;
using n_VarType;
using n_VarList;

//函数节点类
public class FunctionNode
{
	//构造函数
	public FunctionNode( string vVisitType, string vRealRefType, string vInterType, string vReturnType,
	                    string vFunctionName, string vVarIndexList, int vChipIndex, int vWordIndex, string vRefValue )
	{
		VisitType = vVisitType;
		RealRefType = vRealRefType;
		InterType = vInterType;
		ReturnType = vReturnType;
		FunctionName = vFunctionName;
		VarIndexList = vVarIndexList;
		ChipIndex = vChipIndex;
		WordIndex = vWordIndex;
		
		TargetMemberName = null;
		isFast = false;
		isUsed = false;
		isInter = false;
		
		RefValue = vRefValue;
		InterruptSpaceNumber = -1;
		
		UserSpace = false;
		Dealed = false;
		
		V_PC = 0;
		V_CoreIndex = 0;
	}
	
	//显示字符串到输出信息列表
	public override string ToString()
	{
		string Result = "访问类型:" + VisitType + " 函数类型:" + InterType + "\t函数原型:" +
			   ReturnType + " " + FunctionName + "(" + VarTypeList + ")\t形参索引:" + VarIndexList +
			   " 芯片: " + ChipIndex;
		
		return Result;
	}
	
	//获取函数形参类型列表
	public string VarTypeList
	{
		get
		{
			string VarTypeList = null;
			if( VarIndexList != null ) {
				string[] VarCut = VarIndexList.Split( ' ' );
				for( int i = 0; i < VarCut.Length; ++i ) {
					int FuncVarIndex = int.Parse( VarCut[ i ] );
					VarTypeList += VarList.Get( FuncVarIndex ).Type + ",";
				}
				VarTypeList = VarTypeList.TrimEnd( ',' );
			}
			return VarTypeList;
		}
	}
	
	//形参总大小
	public int VarSize {
		get
		{
			if( VarIndexList == null ) {
				return 0;
			}
			string[] VarCut = VarIndexList.Split( ' ' );
			int size = 0;
			for( int i = 0; i < VarCut.Length; ++i ) {
				int FuncVarIndex = int.Parse( VarCut[ i ] );
				string FVarType = VarList.Get( FuncVarIndex ).Type;
				size += VarType.GetSize( FVarType );
			}
			return size;
		}
	}
	
	//获取函数所在元件
	public string UnitName
	{
		get
		{
			return FunctionName.Remove( FunctionName.LastIndexOf( "." ) );
		}
	}
	
	public int InterruptSpaceNumber;			//中断函数所分配的空间, -1表示系统自动分配
	public bool isUsed;							//函数是否被调用过, 用于代码优化
	public bool isInter;						//函数是否在中断中被调用
	
	public readonly string VisitType;			//访问类型  private  public
	public string RealRefType; 					//引用类型  real  link
	public readonly string InterType;			//函数类型  general  interface  interrupt  task dll callback
	public readonly string ReturnType;			//返回类型
	public readonly string FunctionName;		//函数名
	public string TargetMemberName;				//目标成员
	public readonly int ChipIndex;				//函数所在的芯片序号
	public readonly int WordIndex;				//代表函数的词法序号
	
	public readonly string RefValue;			//接口调用类型的函数所引用的调用信息
	
	public readonly string VarIndexList;		//形参序号列表
	
	//中断函数是否保存现场
	public bool isFast;
	
	//是否为用户空间函数 (不能含有局部变量) 这个可能和函数的 task 重合, 两者含义一样, 后续可以优化
	public bool UserSpace;
	
	public bool Dealed;
	
	//verilog里边存储对应的PC指针地址
	public int V_PC;
	//所在的核编号
	public int V_CoreIndex;
}
}




