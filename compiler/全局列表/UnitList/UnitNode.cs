﻿
//元件节点,记录元件的各个属性
namespace n_UnitNode
{
using System;

public class UnitNode
{
	//构造函数
	public UnitNode( string vVisitType, string vRealRefType, string vUnitName, int vWordIndex )
	{
		this.VisitType = vVisitType;
		this.RealRefType = vRealRefType;
		this.UnitName = vUnitName;
		TargetMemberName = null;
		WordIndex = vWordIndex;
	}
	
	//显示字符串
	public override string ToString()
	{
		return "访问类型:" + VisitType + " 引用类型:" + RealRefType + " 名称:" + UnitName;
	}
	
	public readonly string VisitType;		//访问类型 public private
	public string RealRefType;		//引用类型 real ref
	public string TargetMemberName;			//目标元件(只有引用类型为ref时才有效)
	public readonly string UnitName;		//元件名称
	public readonly int WordIndex;			//代表元件位置的词法序号
}
}
