﻿
//元件列表类
namespace n_VdataList
{
using System;
using n_VdataNode;
using n_MemberType;
using n_UnitList;
using n_VarType;
using System.Collections.Generic;

public static class VdataList
{
	//初始化
	public static void Init()
	{
		NodeSet = new VdataNode[ 1000 ];
		NodeDic = new Dictionary<string, int>();
	}
	
	//清除虚拟变量类型记录
	public static void Clear()
	{
		length = 0;
		MaxVdataAddrLength = 0;
		NodeDic.Clear();
	}
	
	//显示
	public static string Show()
	{
		string Result = "";
		for( int i = 0; i < length; ++i ) {
			Result += NodeSet[ i ].ToString() + "\n";
		}
		return Result;
	}
	
	//添加虚拟变量类型定义
	public static void Add( VdataNode node )
	{
		NodeDic.Add( node.VdataName, length );
		
		NodeSet[ length ] = node;
		++length;
		
		//int cLength = VarType.GetSize( node.AddrType );
		//if( MaxVdataAddrLength < cLength ) {
		//	MaxVdataAddrLength = cLength;
		//}
	}
	
	//判断虚拟变量类型名是否存在
	public static bool isExist( string Name )
	{
		return NodeDic.ContainsKey( Name );
		
//		for( int i = 0; i < length; ++i ) {
//			if( NodeSet[ i ].VdataName == Name ) {
//				return true;
//			}
//		}
//		return false;
	}
	
	//根据元件名获取元件直接索引
	public static int GetDirectIndex( string Name )
	{
		if( NodeDic.ContainsKey( Name ) ) {
			return NodeDic[Name];
		}
		else {
			return -1;
		}
		
//		for( int i = 0; i < length; ++i ) {
//			if( NodeSet[ i ].VdataName == Name ) {
//				return i;
//			}
//		}
//		return -1;
	}
	
	//根据虚拟变量类型名获取虚拟变量类型索引
	public static int GetIndex( string Name )
	{
		if( Name == null ) {
			return -1;
		}
		int VdataIndex = GetDirectIndex( Name );
		if( VdataIndex == -1 ) {
			int SplitIndex = Name.LastIndexOf( "." );
			string UnitName = Name.Remove( SplitIndex );
			string VdataName = Name.Remove( 0, SplitIndex );
			int UnitIndex = UnitList.GetIndex( UnitName );
			if( UnitIndex == -1 ) {
				return UnitIndex;
			}
			string NewName = UnitList.Get( UnitIndex ).UnitName + VdataName;
			VdataIndex = GetDirectIndex( NewName );
		}
		if( VdataIndex == -1 ) {
			return VdataIndex;
		}
		if( NodeSet[ VdataIndex ].RealRefType == MemberType.RealRefType.Real ) {
			return VdataIndex;
		}
		return GetIndex( NodeSet[ VdataIndex ].TargetMemberName );
	}
	
	//获取虚拟变量类型信息
	public static VdataNode Get( int Index )
	{
		return NodeSet[ Index ];
	}
	
	//获取在某个元件中的组件名集合
	public static string GetVdataNames( string UnitName )
	{
		string result = null;
		for( int i = 0; i < length; ++i ) {
			if( NodeSet[ i ].UnitName == UnitName ) {//&&
			    //NodeSet[ i ].VisitType == MemberType.VisitType.Public ) {
				int fi = GetIndex( NodeSet[ i ].VdataName );
				string Name = NodeSet[ i ].VdataName;
				Name = Name.Remove( 0, Name.LastIndexOf( "." ) + 1 );
				if( !Name.StartsWith( "OS_" ) && fi != -1 ) {
					result += Name + ";";
				}
			}
		}
		return result;
	}
	
	static Dictionary<string, int> NodeDic;
	
	public static int MaxVdataAddrLength;
	
	static VdataNode[] NodeSet;
	public static int length;
}
}
