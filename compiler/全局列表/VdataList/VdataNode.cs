﻿
//元件节点,记录元件的各个属性

namespace n_VdataNode
{
using System;

public class VdataNode
{
	//构造函数
	public VdataNode( string vVisitType, string vRealRefType, string vVdataName,
	                  string vDataType, string vAddrType, string vTargetUnitName,
	                  bool vAutoAllot, int vStartAddress, int vEndAddress, string vUnitName, string sPreName )
	{
		this.UnitName = vUnitName;
		this.VisitType = vVisitType;
		this.RealRefType = vRealRefType;
		this.VdataName = vVdataName;
		this.DataType = vDataType;
		this.AddrType = vAddrType;
		TargetUnitName = vTargetUnitName;
		this.AutoAllot = vAutoAllot;
		this.StartAddress = vStartAddress;
		this.EndAddress = vEndAddress;
		TargetMemberName = null;
		PreName = sPreName;
		
		StartAddressConst = vStartAddress;
	}
	
	//显示字符串
	public override string ToString()
	{
		return "访问类型:" + VisitType + " 引用类型:" + RealRefType + " 名称:" + VdataName +
			   " 地址类型:" + AddrType + " 目标:" + TargetMemberName +
			   " 自动分配:" + AutoAllot + " 区间:[" + StartAddress + "," + EndAddress + "]";
	}
	
	public readonly string UnitName;
	public readonly string VisitType;		//访问类型 public private
	public readonly string RealRefType;		//引用类型 real ref
	public string TargetMemberName;			//目标成员(只有引用类型为ref时才有效)
	public readonly string VdataName;		//虚拟数据类型名称
	public readonly string TargetUnitName;	//指向的目标元件名称
	public readonly string PreName;			//前缀名字
	public readonly string DataType;		// bit uint8 uint16 uint24 uint32	
	public readonly string AddrType;		// void uint8 uint16 uint24 uint32
	//地址类型不为 void 时以下参数有效
	public readonly bool AutoAllot;			//自动分配,为真时以下参数有意义	
	public int StartAddress;				//存储区域起点地址
	public int StartAddressConst;			//存储区域起点地址, 常量不可改变
	public readonly int EndAddress;			//存储区域终点地址
}
}
