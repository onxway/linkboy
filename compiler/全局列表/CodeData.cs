﻿
//code类型数据表类
namespace n_CodeData
{
using System;
using System.Text;

public static class CodeData
{
	//清除数据
	public static void Reset( int ChipNumber )
	{
		CodeDataSet = new StringBuilder[ ChipNumber ];
		for( int i = 0; i < CodeDataSet.Length; ++i ) {
			CodeDataSet[ i ] = new StringBuilder( "" );
		}
		ChipIndex = -1;
		isChanged = false;
	}
	
	//设置当前CPU序号
	public static void SetChipIndex( int Index )
	{
		ChipIndex = Index;
	}
	
	//获取代码表
	public static string GetSeg()
	{
		return CodeDataSet[ ChipIndex ].ToString();
	}
	
	//添加数据
	public static void Add( string Data )
	{
		CodeDataSet[ ChipIndex ].Append( Data + "\n" );
	}
	
	//显示数据
	public static string Show()
	{
		string r = "";
		for( int i = 0; i < CodeDataSet.Length; ++i ) {
			r += CodeDataSet[ i ] + "\n---------------------------------------------\n";
		}
		return r;
	}
	
	//code类型数据集合
	static StringBuilder[] CodeDataSet;
	static int ChipIndex;
	
	//总共的机器码字节数量
	public static int TotalByteNumber = 0;
	
	//是否更新过
	public static bool isChanged;
}
}

