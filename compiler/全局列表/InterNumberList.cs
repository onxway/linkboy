﻿
namespace n_InterNumberList
{
using System;

public static class InterNumberList
{
	//初始化
	public static void Clear()
	{
		DefList = " ";
		UseList = " ";
	}
	
	//添加一个新定义的序号并判断是否有重复
	public static bool AddDefOK( string Name )
	{
		if( DefList.IndexOf( " " + Name + " " ) != -1 ) {
			return false;
		}
		DefList += Name + " ";
		return true;
	}
	
	//添加一个使用的序号并判断是否有重复
	public static void AddUse( string Name )
	{
		if( UseList.IndexOf( " " + Name + " " ) != -1 ) {
			return;
		}
		UseList += Name + " ";
	}
	
	public static string DefList;
	public static string UseList;
}
}



