﻿
//变量节点类
namespace n_VarNode
{
using System;
using n_Deploy;
using n_MemberType;

public class VarNode
{
	//构造函数
	public VarNode()
	{
		name = null;
		type = null;
		activeType = null;
		visitType = null;
		functionIndex = -1;
		location = -1;
		level = -1;
		isExist = true;
		address = null;
		isComp = false;
		ignoreWriteError = false;
		isConst = false;
		ChipIndex = -1;
		
		UsedInterFincID = -1;
		MultAccessError = false;
		isFunctionType = false;
		
		isUserSpace = false;
		
		isVerilogSpace = false;
		isVerilogMult = false;
		VisitCoreList = " ";
		VeriType = 0;
		
		isNDK = 0;
		
		TargetMemberName = null;
	}
	
	//设置静态变量属性
	public void SetStaticValue( string Name, string Type, string VisitType,
	                           string vRealRefType, bool IsOut, int Location, int ChipIndex, int vWordIndex )
	{
		this.activeType = MemberType.StoreType.Static;
		this.name = Name;
		this.type = Type;
		this.visitType = VisitType;
		this.RealRefType = vRealRefType;
		this.level = 0;
		this.isOut = IsOut;
		this.location = Location;
		this.chipIndex = ChipIndex;
		this.WordIndex = vWordIndex;
		
		EnableWrite = true;
	}
	
	//设置函数局部变量属性
	public void SetLocalValue( string Name, string Type, int Level, int FunctionIndex, int Location, int ChipIndex )
	{
		this.activeType = MemberType.StoreType.Local;
		this.name = Name;
		this.type = Type;
		this.level = Level;
		this.functionIndex = FunctionIndex;
		this.location = Location;
		this.chipIndex = ChipIndex;
		
		//需要加上这个 2022.8.26 整型分量访问 导出代码获取 GetFinalName 时候判断
		this.RealRefType = MemberType.RealRefType.Real;
	}
	
	//设置临时变量属性
	public void SetTempValue( string Type, int Level, int Location )
	{
		this.activeType = MemberType.StoreType.Temp;
		this.type = Type;
		this.level = Level;
		this.location = Location;
		this.chipIndex = Deploy.ChipIndex;
		
		//需要加上这个 2022.8.26 整型分量访问 导出代码获取 GetFinalName 时候判断
		this.RealRefType = MemberType.RealRefType.Real;
	}
	
	//显示
	public override string ToString()
	{
		return " 函数:" + functionIndex + " 可见:" + visitType +
			   " 存储:" + activeType + " 类型:" + type + " 名称:" + name +
			   " 层数:" + level + " 地址:" + address + " 链接:" + RealRefType +
			   " 芯片: " + chipIndex + " " + (isConst ? "常量值:" + Value : "") + " 用户:" + isUserSpace + " 访问函数:" + UsedInterFincID + "(" + MultAccessError + ")";
	}
	
	string name;
	public string Name {
		get { return name; }
	}
	
	//获取变量所在元件
	public string UnitName
	{
		get
		{
			return name.Remove( name.LastIndexOf( "." ) );
		}
	}
	
	public string TargetMemberName;			//目标成员(只有引用类型为ref时才有效)
	
	public bool EnableWrite;
	
	//是否在中断函数中被使用, -1:不被使用 >=0:被使用的函数ID  (-2表示多个中断使用)
	public int UsedInterFincID;
	public bool MultAccessError;
	
	//是否忽略未被使用的情况, 用于调用NDK的函数返回值
	//0: 普通变量   1: C标识符   2: C函数返回值
	public int isNDK;
	
	public bool isFunctionType;
	string type;
	public string Type {
		get { return type; }
		set { type = value; }
	}
	string activeType;
	public string ActiveType {
		get { return activeType; }
		set { activeType = value; }
	}
	string visitType;
	public string VisitType {
		get { return visitType; }
	}
	
	public string RealRefType;		//引用类型 real ref
	
	public bool isConst;	//是否为常量, 静态变量为用户值, 局部变量为数值创建的临时变量
	public string Value;	//用于存放常量类型的值
	
	int functionIndex;
	public int FunctionIndex {
		get { return functionIndex; }
		set { functionIndex = value; }
	}
	
	int location;
	public int Location {
		get { return location; }
		set { location = value; }
	}
	int level;
	public int Level {
		get { return level; }
	}
	
	//变量是否存在, 此属性在展开语法树之后就不再有用
	bool isExist;
	public bool IsExist {
		get { return isExist; }
		set { isExist = value; }
	}
	
	string address;
	public string Address {
		get { return address; }
		set { address = value; }
	}
	
	//是否复合运算
	bool isComp;
	public bool IsComp {
		get { return isComp; }
		set { isComp = value; }
	}
	
	//是否手工分配地址的变量
	bool isOut;
	public bool IsOut {
		get { return isOut; }
		set { isOut = value; }
	}
	
	//忽略临时变量写错误
	bool ignoreWriteError;
	public bool IgnoreWriteError {
		get { return ignoreWriteError; }
		set { ignoreWriteError = value; }
	}
	
	//所在的芯片编号
	int chipIndex;
	public int ChipIndex {
		get { return chipIndex; }
		set { chipIndex = value; }
	}
	
	public int WordIndex;
	
	//是否用户空间的变量
	public bool isUserSpace;
	//是否verilog空间变量
	public bool isVerilogSpace;
	
	//是否多核访问的变量
	public bool isVerilogMult;
	public string VisitCoreList;
	public int VeriType; //0 普通多核变量  1 除法器
}
}
