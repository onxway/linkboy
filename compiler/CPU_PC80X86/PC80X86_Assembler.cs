﻿
//编译器类--把源程序编译为汇编程序
namespace n_PC80X86_Assembler
{
using System;
using System.Diagnostics;
using System.IO;

using n_Compiler;
using n_Config;
using i_Compiler;
using n_ET;
using n_OS;

public static class PC80X86_Assembler
{
	//编译器启动初始化,加载汇编运算库文件
	public static void LoadFile()
	{
		//加载运算信息表
		string s = i_Compiler.Compiler.OpenCompileFile( "compiler" + OS.PATH_S + "CPU" + OS.PATH_S + "PC80X86" + OS.PATH_S + "hexcode.lst" );
		s = s.Remove( s.IndexOf( "\n<end>" ) );
		string[] Lines = s.Split( '\n' );
		HEXList = new string[ Lines.Length ][];
		for( int i = 0; i < Lines.Length; ++i ) {
			HEXList[ i ] = Lines[ i ].Remove( Lines[ i ].IndexOf( "<<<<" ) ).Split( '\\' );
		}
		Info = new ProcessStartInfo();
		Info.WindowStyle = ProcessWindowStyle.Hidden;
		Info.WorkingDirectory = OS.SystemRoot + "compiler" + OS.PATH_S + "CPU" + OS.PATH_S + "PC80X86";
	}
	
	//编译汇编文件
	public static string Assemble( string Source, int Index )
	{
		n_Compiler.Compiler.CompilingStep( n_Compiler.Compiler.STEP_InitAssemble );
		
		Process Proc;
		
		n_Compiler.Compiler.CompilingStep( n_Compiler.Compiler.STEP_Assemble );
		
		Info.FileName = @"ml.exe";//调用汇编命令
		Info.Arguments = @"/c /coff result.asm";
		Proc = Process.Start(Info);
		while( !Proc.HasExited ) {}
		
		Info.FileName = @"link.exe";//调用汇编命令
		Info.Arguments = "/subsystem:windows result.obj";
		Proc = Process.Start(Info);
		while( !Proc.HasExited ) {}
		
		string TargetPath = "";//A.ctext.FilePath + A.ctext.FileName + Config.GetChipName() + ".exe";
		if( !File.Exists( OS.SystemRoot + "compiler" + OS.PATH_S + "CPU" + OS.PATH_S + "PC80X86" + OS.PATH_S + "result.exe" ) ) {
			ET.ShowError( "汇编出错! 未生成EXE文件" );
			return null;
		}
		File.Delete( TargetPath );
		File.Move( OS.SystemRoot + "compiler" + OS.PATH_S + "CPU" + OS.PATH_S + "PC80X86" + OS.PATH_S + "result.exe", TargetPath );
		
		return null;
	}
	
	static string[][] HEXList;
	
	static ProcessStartInfo Info;	//进程信息
}
}

