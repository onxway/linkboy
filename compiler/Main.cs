﻿
using System;
using n_OS;
using n_Compiler;
using n_ET;

namespace ccc
{
	class Program
	{
		public static void Main(string[] args)
		{
			//判断是否加载文件
			string DefaultFilePath = p_Win.CommandLine.GetStartPath( args );
			
			//Console.Write( ":" + Environment.CommandLine + ":" );
			//Console.Write( "<" + DefaultFilePath +">" );
			//Console.ReadKey(true);
			
			OS.Init();
			Compiler.FileLoadInit();
			
			Compiler.isExportMode = true;
			n_SST.SST.ModeV = true;
			n_SST.SST.ModeC = false;
			
			string[] rr = Compiler.Compile( DefaultFilePath, 1 );
			
			if( ET.isErrors() ) {
				Console.Write( ET.Show() );
			}
			else {
				Console.Write( rr[0] );
				Console.WriteLine( n_SST.SST.GetCode() );
			}
			Console.ReadKey(true);
		}
	}
}


