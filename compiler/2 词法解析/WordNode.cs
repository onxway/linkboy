﻿
//词法分析结点类
//Word:			单个的词
//FileIndex:	所在的文件
//Line:			所在的行
//Column:		所在的列
//Index:		所在的索引
//TypeIndex:	词类型, 0-结束, 1-关键字, 2-标识符, 3-运算符, 4-分界符, 5-字符串, 6-字符, 7-注释, 8-数字

namespace n_WordNode
{
using System;

public class WordNode
{
	//构造函数
	public WordNode( string vWord, int vFileIndex, int vLine, int vColumn, int vIndex, int vTypeIndex, int vWordLength )
	{
		Word = vWord;
		FileIndex = vFileIndex;
		Line = vLine;
		Column = vColumn;
		Index = vIndex;
		TypeIndex = vTypeIndex;
		WordLength = vWordLength;
		ColorIndex = 0;
	}
	
	public string Word;		//词本身
	public int FileIndex;	//所在的文件索引
	public int Line;		//所在的行
	public int Column;		//所在的列
	public int Index;		//首字符在文件中的索引值
	public int TypeIndex;	//词类型
	public int ColorIndex;	//关键字颜色序号
	public int WordLength;	//词长度
	
	public const int END = 0; //程序结束
	public const int KEYWORD = 1; //关键字
	public const int IDENT = 2; //标识符
	public const int OPER = 3; //运算符号
	public const int SPLIT = 4; //分隔符标志
	public const int STRING = 5; //字符串标志
	public const int CHAR = 6; //字符标志
	public const int FRONT = 7; //前端 宏定义着色标志
	public const int NUMBER = 8; //数字标志
	public const int SYS = 9; //系统指令标志
}
}



