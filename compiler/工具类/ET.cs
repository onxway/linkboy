﻿
//编译错误输出类
namespace n_ET
{
using System;
using n_WordList;
using i_Compiler;
using n_UseFileList;

public static class ET
{
	public static int SysLine = 0;
	public static bool Open;
	
	//清空错误列表
	public static void Clear()
	{
		ErrorMessage = "";
		isError = false;
		Open = true;
	}
	
	//是否出错
	public static bool isErrors()
	{
		return isError;
	}
	
	//显示错误列表
	public static string Show()
	{
		return ErrorMessage.Trim( '\n' );
	}
	
	//向错误输出流写入一个语法错误项描述
	//格式: 词序号, 错误描述
	public static void ShowError( string Describe )
	{
		isError = true;
		Compiler.Show( Describe );
	}
	
	//向错误输出流写入一个语法错误项描述
	//格式: 词序号, 错误描述
	public static void WriteParseError( int Index, string Describe )
	{
		if( !Open ) {
			return;
		}
		isError = true;
		if( Index == -1 ) {
			Index = 0;
		}
		try {
		int Line = WordList.GetLine( Index ) + 1;
		Line -= SysLine;
		int Number = WordList.GetColumn( Index );
		int fileindex = WordList.GetFileIndex( Index );
		ErrorMessage += "词 " +fileindex + " " + Line + " " + Number + " " + 
			            WordList.GetWord( Index ).Length + " " +
			",第" + Line + "行有错: " + Describe + " (" + UseFileList.GetPath( fileindex ) + ")\n";
		}
		catch( Exception e ) {
			ET.ShowError( "输出错误时发生异常, 当前的错误描述:\n" + Describe + "\n出错点: " + Index + "\n" + e  );
		}
	}
	
	//向错误输出流写入一个语法树错误项描述
	//格式: 行号, 错误描述
	public static void WriteLineError( int FileIndex, int Line, string Describe )
	{
		if( !Open ) {
			return;
		}
		isError = true;
		++Line;
		Line -= SysLine;
		ErrorMessage += "行 " + FileIndex + " " + Line + "," + FileIndex + "-" + "第" + Line + "行有错: " + Describe + " (" + UseFileList.GetPath( FileIndex ) + ")\n";
	}
	
	static string ErrorMessage;		//编译错误信息
	static bool isError; 			//是否出错标志
}
}
