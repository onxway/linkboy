﻿
//函数类型
namespace n_MemberType
{
using System;

public static class MemberType
{
	//访问类型
	public static class VisitType
	{
		public const string Public = "public";
		public const string Private = "Private";
	}
	
	//函数类型
	public static class FunctionType
	{
		public const string General = "general";
		public const string Task = "task";
		public const string Interrupt = "interrupt";
		public const string Callback = "callback";
		public const string Interface = "interface";
	}
	
	//引用类型
	public static class RealRefType
	{
		public const string Real = "real";
		public const string link = "link";
	}
	
	//存储类型
	public static class StoreType
	{
		public const string Static = "static";
		public const string Local = "local";
		public const string Temp = "temp";
	}
}
}

