﻿
using System;

namespace n_Param
{
public static class Param
{
	public const string S_Param = "#param";
	
	
	public const string S_TestOn = "teston";
	public const string S_TestOff = "testoff";
	public static bool TestOn;
	
	public const string FUNCTION_USED = "FUNCTION_USED";
	public static bool function_used;
	
	public const string S_UserspaceOn = "userspaceon";
	public const string S_UserspaceOff = "userspaceoff";
	public static bool UserspaceOn;
	
	//中断访问状态
	public static string Safe = "safe";
	public static string Unsafe = "unsafe";
	public static bool isSafe;
	
	//verilog代码生成开关
	public const string V_On = "verilog_on";
	public const string V_Off = "verilog_off";
	public static bool isVon;
	public const string V_cpu = "cpu";
	public const string V_cpu_lib = "cpu_lib";
	public const string V_cpu_end = "cpu_end";
	
	//初始化
	public static void Clear()
	{
		TestOn = false;
		
		function_used = false;
		UserspaceOn = false;
		
		isSafe = false;
		isVon = false;
	}
}
}


