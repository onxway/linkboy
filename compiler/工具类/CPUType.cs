﻿
//CPU类型
namespace n_CPUType
{
using System;
using n_ET;
using n_VdataList;
using n_VarType;
using n_OS;

public static class CPUType
{
	public const string MCS_X = "MCS";
	
	public const string MEGA_X = "MEGA";
	
	const string MEGA8 = "MEGA8";
	const string MEGA16 = "MEGA16";
	const string MEGA32 = "MEGA32";
	const string MEGA64 = "MEGA64";
	const string MEGA128 = "MEGA128";
	
	const string MEGA48 = "MEGA48";
	public const string MEGA328 = "MEGA328";
	
	public const string MEGA644 = "MEGA644";
	
	public const string MEGA2560 = "MEGA2560";
	
	
	public const string SPCE061A = "SPCE061A";
	public const string LPC1114 = "LPC1114";
	public const string PC80X86 = "PC80X86";
	public const string VM = "VM";
	
	//判断类型是否存在
	public static bool isExist( string T_CPU )
	{
		if( T_CPU.StartsWith( MEGA_X ) ||
		    T_CPU.StartsWith( MCS_X ) ||
		    T_CPU == SPCE061A ||
		    T_CPU == LPC1114 ||
		    T_CPU == PC80X86 ||
		    T_CPU == VM ) {
			return true;
		}
		else {
			return false;
		}
	}
	
	//判断AVR芯片是否为短中断地址, 包括MEGA8, MEGA48这类的芯片
	public static bool isShortInterruptAddr( string T_CPU )
	{
		if( T_CPU == MEGA48 ||
		    T_CPU == MEGA8 ) {
			return true;
		}
		return false;
	}
}
}

