﻿
//编译错误输出类
namespace n_CDebug
{
using System;
using n_WordList;
using n_ET;

public static class Debug
{
	//清空错误列表
	public static void Clear()
	{
		ShowMessage = false;
	}
	
	//处理一个系统命令
	public static void Deal( string Command )
	{
		if( Command == "message_on" ) {
			ShowMessage = true;
		}
		else if( Command == "message_off" ) {
			ShowMessage = false;
		}
		if( Command == n_Param.Param.Safe ) {
			n_Param.Param.isSafe = true;
		}
		else if( Command == n_Param.Param.Unsafe ) {
			n_Param.Param.isSafe = false;
		}
		else {
			ET.ShowError( "<Debug> 未知的命令: " + Command );
		}
	}
	
	public static bool ShowMessage;
}
}
