﻿
//编译配置

namespace n_Config
{
using System;
using n_CodeData;
using n_CPUType;
using n_ET;
using n_FunctionList;
using n_Interrupt;
using n_VarType;
using i_Compiler;
using n_OS;

public static class Config
{
	public const string LOAD = "#include";
	public const string PathPre_ModuleLib = "ModuleLib";
	public const string PathPre_CurrentDir = "CurrentDir";
	
	public static string Path_compiler;
	
	public static class PreLabel
	{
		public static readonly string SysPre = "sys_";
		
		public static readonly string Function = SysPre + "func";
		public static readonly string FunctionInterEnd = SysPre + "funcinterend";
		
		public static readonly string Label = SysPre + "label";
		
		public static readonly string VarAddr = SysPre + "VarAddr";
		
		public static readonly string Switchlist = "#." + SysPre + "switchlist";
		public static readonly string Case = SysPre + "case";
		public static readonly string Default = SysPre + "default";
		public static readonly string Head = SysPre + "head";
		public static readonly string Start = SysPre + "start";
		public static readonly string Continue = SysPre + "continue";
		public static readonly string End = SysPre + "end";
		public static readonly string Else = SysPre + "else";
	}
	
	//编译器优化标志
	public static int OptimizeFlag;
	
	//无参函数定义, 返回值语法合规, ...
	public static bool WarningAsError = false;
	
	//=====================================================
	
	//配置类初始化
	public static void Init()
	{
		DealLockError = false;
		
		Path_compiler = @"Resource" + n_OS.OS.PATH_S + "cpl" + n_OS.OS.PATH_S;
	}
	
	//复位编译配置
	public static void Reset()
	{
		TargetCPUList = null;
		StartFunctionNameList = null;
		ChipNumber = 0;
		ChipIndex = 0;
		isReentranted = false;
		ProtectBaseCodeVarAtInt = false;
		NDK_List = null;
		
		OptimizeFlag = 0;
	}
	
	//设置当前芯片号
	public static void SetChipIndex( int Index )
	{
		ChipIndex = Index;
		
		//设置重入类型
		string[] Cut = TargetCPUList.Split( ' ' );
		string TargetCPU = Cut[ Index ];
		if( !TargetCPU.StartsWith( CPUType.MCS_X ) ) {
			isReentranted = true;
			ProtectBaseCodeVarAtInt = false;
		}
		else {
			isReentranted = false;
			ProtectBaseCodeVarAtInt = true;
		}
		//设置当前处理器的其他信息
		VarType.SetBaseType( Index );
		CodeData.SetChipIndex( Index );
		Interrupt.SetChipIndex( Index );
	}
	
	//添加目标CPU型号
	public static void AddCPU( int Line, string CPU )
	{
		if( !CPUType.isExist( CPU ) ) {
			ET.WriteLineError( 0, Line, "未知的目标cpu型号: " + CPU );
			return;
		}
		TargetCPUList += CPU + " ";
	}
	
	//添加启动函数
	public static void AddStartFunction( string StartFunction )
	{
		StartFunctionNameList += StartFunction + " ";
	}
	
	//获取起始函数名称
	public static int GetStartFunction()
	{
		if( StartFunctionNameList == null ) {
			return -1;
		}
		string[] Cut = StartFunctionNameList.Split( ' ' );
		if( ChipIndex >= Cut.Length ) {
			return -1;
		}
		string StartFunctionName = Cut[ ChipIndex ];
		int FuncIndex = FunctionList.GetIndex( StartFunctionName );
		if( FuncIndex == -1 ) {
			ET.WriteParseError( 0, "起始函数未定义: " + StartFunctionName );
		}
		return FuncIndex;
	}
	
	//获取芯片名称
	public static string GetChipName()
	{
		return ChipIndex.ToString();
	}
	
	//获取CPU类型
	public static string GetCPU()
	{
		string[] Cut = TargetCPUList.Split( ' ' );
		return Cut[ ChipIndex ];
	}
	
	//判断CPU是否有效
	public static bool CPUExist()
	{
		return TargetCPUList != null;
	}
	
	//根据CPU类型获取系统路径
	public static string GetSystemFilePathFromCPUType()
	{
		string TargetCPU = GetCPU();
		
		if( TargetCPU.StartsWith( CPUType.MEGA_X ) ) {
			return n_Config.Config.Path_compiler + "CPU" + OS.PATH_S + "ATMEGA" + OS.PATH_S + TargetCPU + "_System.txt";
		}
		if( TargetCPU.StartsWith( CPUType.MCS_X ) ) {
			return n_Config.Config.Path_compiler + "CPU" + OS.PATH_S + "MCS51" + OS.PATH_S + "System.txt";
		}
		else if( TargetCPU == CPUType.SPCE061A ) {
			return n_Config.Config.Path_compiler + "CPU" + OS.PATH_S + "SPCE061A" + OS.PATH_S + "System.txt";
		}
		else if( TargetCPU == CPUType.LPC1114 ) {
			return n_Config.Config.Path_compiler + "CPU" + OS.PATH_S + "CORTEX M0" + OS.PATH_S + "System.txt";
		}
		else if( TargetCPU == CPUType.PC80X86 ) {
			return n_Config.Config.Path_compiler + "CPU" + OS.PATH_S + "PC80X86" + OS.PATH_S + "System.txt";
		}
		else if( TargetCPU == CPUType.VM ) {
			return n_Config.Config.Path_compiler + "CPU" + OS.PATH_S + "VM" + OS.PATH_S + "System.txt";
		}
		else {
			return null;
		}
	}
	
	//获取当前CPU的函数名引用基本类型
	public static string GetFunctionPointType()
	{
		string TargetCPU = GetCPU();
		
		if( Config.GetCPU().StartsWith( CPUType.MCS_X ) ) {
			return VarType.BaseType.Uint16;
		}
		if( TargetCPU.StartsWith( CPUType.MEGA_X ) ) {
			return VarType.BaseType.Uint16;
		}
		if( TargetCPU == CPUType.SPCE061A ) {
			return VarType.BaseType.Uint16;
		}
		if( TargetCPU == CPUType.LPC1114 ) {
			return VarType.BaseType.Uint32;
		}
		if( TargetCPU == CPUType.PC80X86 ) {
			return VarType.BaseType.Uint32;
		}
		if( TargetCPU == CPUType.VM ) {
			return VarType.BaseType.Uint32;
		}
		return null;
	}
	
	//获取当前CPU的函数变量尺寸类型
	public static string GetFunctionSizeType()
	{
		string TargetCPU = GetCPU();
		
		if( Config.GetCPU().StartsWith( CPUType.MCS_X ) ) {
			return VarType.BaseType.Uint16;
		}
		if( TargetCPU.StartsWith( CPUType.MEGA_X ) ) {
			return VarType.BaseType.Uint16;
		}
		if( TargetCPU == CPUType.SPCE061A ) {
			return VarType.BaseType.Uint16;
		}
		if( TargetCPU == CPUType.LPC1114 ) {
			return VarType.BaseType.Uint32;
		}
		if( TargetCPU == CPUType.PC80X86 ) {
			return VarType.BaseType.Uint32;
		}
		if( TargetCPU == CPUType.VM ) {
			return VarType.BaseType.Uint32;
		}
		return null;
	}
	
	//获取当前CPU是否可重入
	public static bool isReentrant()
	{
		return isReentranted;
	}
	
	//当前CPU是否需要在中断中保存base code接口的实参
	public static bool isProtectBaseCodeVarAtInt()
	{
		return ProtectBaseCodeVarAtInt;
	}
	
	//----------------------------------------------
	
	//处理器数量,只在包含文件时候递增,以后为只读,用于初始化各种列表等
	public static int ChipNumber;
	
	//当前处理器序号,会反复的从零开始递增
	public static int ChipIndex;
	
	static bool ProtectBaseCodeVarAtInt;
	static bool isReentranted;
	static string TargetCPUList;
	static string StartFunctionNameList;
	
	public static bool DealLockError;
	public static string NDK_List;
}
}


