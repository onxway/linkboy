﻿
//运算符号类
namespace n_COper
{
using System;
using System.IO;
using n_OS;

public static class COper
{
	//加载运算符号列表
	public static void LoadFile()
	{
		string s = i_Compiler.Compiler.OpenCompileFile( n_Config.Config.Path_compiler + "oper.lst" );
		s = s.Remove( s.IndexOf( "\n<end>" ) );
		string[] Lines = s.Split( '\n' );
		COperList = new string[ Lines.Length ];
		for( int i = 0; i < Lines.Length; ++i ) {
			COperList[ i ] = Lines[ i ].Remove( Lines[ i ].IndexOf( "<line>" ) );
		}
	}
	
	//判断运算是否存在
	public static bool isExist( string Oper )
	{
		for( int i = 0; i < COperList.Length; ++i ) {
			if( COperList[ i ] == Oper ) {
				return true;
			}
		}
		return false;
	}
	
	public static string[] COperList;
}
}
