
//判断字符类型
namespace n_CharType
{
using System;

public static class CharType
{
	//单词: a-z A-Z _ 中文
	public static bool isLetter( char c )
	{
		if( ( c >= 'a' && c <= 'z' ) || ( c >= 'A' && c <= 'Z' ) || c == '_' || c == '#' || c == '$' || c == '@' || c > 127 ) {
			return true;
		}
		return false;
	}
	
	//非符号:  a-z A-Z _ 中文 0-9
	public static bool isLetterOrNumber( char c )
	{
		if( isLetter( c ) || ( c >= '0' && c <= '9' ) ) {
			return true;
		}
		return false;
	}
	
	//数字:  0-9
	public static bool isNumber( char c )
	{
		if(  c >= '0' && c <= '9' ) {
			return true;
		}
		return false;
	}
}
}



