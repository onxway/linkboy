﻿
//封装汇编程序
namespace n_Box
{
using System;
using System.Text;

using n_AddressList;
using n_CodeData;
using n_Config;
using n_CPUType;
using n_ET;
using n_FunctionList;
using n_Interrupt;
using n_VdataList;
using n_VarType;
using n_MemberType;
using i_Compiler;
using n_OS;
using n_VarList;

public static class Box
{
	//加载文件
	public static void LoadFile()
	{
		MCS51_Lib = Compiler.OpenCompileFile( n_Config.Config.Path_compiler + "CPU" + OS.PATH_S + "MCS51" + OS.PATH_S + "asmlink.lst" );
		MCS51_Lib = MCS51_Lib.Remove( MCS51_Lib.IndexOf( "\n<end>" ) ).Trim( '\n' );
		
		ATMEGA_Lib = Compiler.OpenCompileFile( n_Config.Config.Path_compiler + "CPU" + OS.PATH_S + "ATMEGA" + OS.PATH_S + "asmlink.lst" );
		ATMEGA_Lib = ATMEGA_Lib.Remove( ATMEGA_Lib.IndexOf( "\n<end>" ) ).Trim( '\n' );
	
		SPCE061A_Lib = Compiler.OpenCompileFile( n_Config.Config.Path_compiler + "CPU" + OS.PATH_S + "SPCE061A" + OS.PATH_S + "asmlink.lst" );
		SPCE061A_Lib = SPCE061A_Lib.Remove( SPCE061A_Lib.IndexOf( "\n<end>" ) ).Trim( '\n' );
		
		CORTEX_M0_Lib = Compiler.OpenCompileFile( n_Config.Config.Path_compiler + "CPU" + OS.PATH_S + "CORTEX M0" + OS.PATH_S + "asmlink.lst" );
		CORTEX_M0_Lib = CORTEX_M0_Lib.Remove( CORTEX_M0_Lib.IndexOf( "\n<end>" ) ).Trim( '\n' );
		
		//PC80X86_Lib = Compiler.OpenCompileFile( n_Config.Config.Path_compiler + "CPU" + OS.PATH_S + "PC80X86" + OS.PATH_S + "asmlink.lst" );
		//PC80X86_Lib = PC80X86_Lib.Remove( PC80X86_Lib.IndexOf( "\n<end>" ) ).Trim( '\n' );
		PC80X86_Lib = null;
		
		VM_Lib = Compiler.OpenCompileFile( n_Config.Config.Path_compiler + "CPU" + OS.PATH_S + "VM" + OS.PATH_S + "asmlink.lst" );
		VM_Lib = VM_Lib.Remove( VM_Lib.IndexOf( "\n<end>" ) ).Trim( '\n' );
	}
	
	//展开宏指令为目标代码
	public static string[] Pack( string[] Source )
	{
		string[] ChipList = new string[ Config.ChipNumber ];
		for( int i = 0; i < Source.Length; ++i ) {
			Config.SetChipIndex( i );
			ChipList[ i ] = OncePack( Source[ i ], i );
		}
		return ChipList;
	}
	
	//打包
	public static string OncePack( string Program, int ChipIndex )
	{
		//获取起始函数
		int StartFunctionIndex = Config.GetStartFunction();
		if( StartFunctionIndex == -1 ) {
			ET.WriteLineError( 0, 0, "在源程序工程中应定义一个连接器指向起始函数(类似C语言中的main函数)" );
		}
		string main = Config.PreLabel.Function + StartFunctionIndex;
		
		//根据芯片类型进行相应封装
		string cpu = Config.GetCPU();
		if( cpu.StartsWith( CPUType.MEGA_X ) ) {
			return ATMEGA_Pack( main, Program );
		}
		if( cpu.StartsWith( CPUType.MCS_X ) ) {
			return MCS51_Pack( main, Program );
		}
		switch( cpu ) {
			case CPUType.SPCE061A:		return SPCE061A_Pack( main, Program );
			case CPUType.LPC1114:		return CORTEX_M0_Pack( main, Program );
			case CPUType.PC80X86:		return PC80X86_Pack( main, Program );
			case CPUType.VM:			return VM_Pack( main, Program );
			default: ET.ShowError( "<OncePack> 未知的芯片类型: " + Config.GetCPU() ); return null;
		}
	}
	
	//mcs51打包
	static string MCS51_Pack( string MainLabel, string Program )
	{
		int SystemVdataIndex = VdataList.GetIndex( VarType.VBase );
		int StartAddress = VdataList.Get( SystemVdataIndex ).StartAddress;
		int Number = AddressList.Number[ SystemVdataIndex ];
		int SPAddress = StartAddress + Number - 1;
		StringBuilder result = new StringBuilder( "" );
		result.Append( "org 0000h\n" );
		result.Append( "ljmp _main\n" );
		result.Append( AddInterruptFor_MCS51() + "\n" );
		result.Append( "org 100\n" );
		result.Append( Switch_MCS51_CodeData() );
		result.Append( "_main:\n" );
		
		//清零内部RAM
		//重大问题: MOV @R0,#0 居然不行!  只能 MOV @R0,A
		result.Append( "mov r0,#0\n" );
		result.Append( "mov a,#0\n" );
		result.Append( "sysinit:\n" );
		result.Append( "mov @r0,a\n" );
		result.Append( "djnz r0,sysinit\n" );
		
		result.Append( "mov sp,#" + SPAddress + "\n" );
		result.Append( "ljmp " + MainLabel + "\n" );
		result.Append( Program );
		result.Append(  MCS51_Lib + "\n" );
		result.Append( "end\n" );
		return result.ToString();
	}
	
	//atmega打包
	static string ATMEGA_Pack( string MainLabel, string Program )
	{
		//获取软件堆栈起始地址
		int VdataIndex = VdataList.GetIndex( VarType.VBase );
		
		//中断函数不需要清空内容, 这样初始化时候被主函数main占用作为堆栈(向上增长), 一直持续到 OS.start()
		//int SoftSP_StartAddress = AddressList.StaticMaxAddr[ VdataIndex ] + Interrupt.GetStackNumber() + 1;
		int SoftSP_StartAddress = AddressList.StaticMaxAddr[ VdataIndex ] + 1;
		
		int StartAddress = VdataList.Get( VdataIndex ).StartAddressConst;
		int EndAddress = VdataList.Get( VdataIndex ).EndAddress;
		
		StringBuilder result = new StringBuilder( "" );
		
		//主要是处理MEGA8的中断入口重定位
		result.Append( AddStartCode() );
		
		result.Append( AddInterruptFor_ATMEGA() );
		
		if( Config.GetCPU() == CPUType.MEGA2560 ) {
			result.Append( ".org 0x0080\n" );
		}
		else {
			result.Append( ".org 0x0040\n" );
		}
		
		result.Append( Switch_ATMEGA_CodeData() );
		result.Append( "fmain:\n" );
		
		//初始化C环境, 清零内存区, 0x0060 - 0x0860 全部清零(实际只清零静态变量区)
		result.Append( "ldi r31," + StartAddress / 256 + "\n" );
		result.Append( "ldi r30," + StartAddress % 256 + "\n" );
		result.Append( "ldi r26,0\n" );
		result.Append( "finit0:\n" );
		result.Append( "cpi r31," + (SoftSP_StartAddress / 256) + "\n" );
		result.Append( "brne finit1\n" );
		result.Append( "cpi r30," + (SoftSP_StartAddress % 256) + "\n" );
		result.Append( "brne finit1\n" );
		result.Append( "jmp finitok\n" );
		result.Append( "finit1:\n" );
		result.Append( "st z+,r26\n" );
		result.Append( "jmp finit0\n" );
		result.Append( "finitok:\n" );
		
		//初始化C函数调用PC硬堆栈
		result.Append( "ldi r28," + EndAddress % 256 + "\n" );
		result.Append( "out 0x3d,r28\n" );
		result.Append( "ldi r28," + EndAddress / 256 + "\n" );
		result.Append( "out 0x3e,r28\n" );
		
		//这里需要留出中断函数的空间
		SoftSP_StartAddress += Interrupt.GetStackNumber() + 10;
		
		//初始化C函数调用局部变量软堆栈
		result.Append( "ldi r28," + (SoftSP_StartAddress % 256) + "\n" );
		result.Append( "ldi r29," + (SoftSP_StartAddress / 256) + "\n" );
		result.Append( "call " + MainLabel + "\n" );
		result.Append( "jmp fmain\n" );
		result.Append(  Program );
		result.Append( ATMEGA_Lib );
		return result.ToString();
	}
	
	//spce061a打包
	static string SPCE061A_Pack( string MainLabel, string Program )
	{
		StringBuilder result = new StringBuilder( "" );
		result.Append( Switch_SPCE061A_CodeData() );
		result.Append( "_main:\n" );
		result.Append( "r0 = 0x07ff\n" );
		result.Append( "goto " + MainLabel + "\n" );
		result.Append( Program + "\n" );
		result.Append( SPCE061A_Lib );
		return result.ToString();
	}
	
	//CORTEX M0打包
	static string CORTEX_M0_Pack( string MainLabel, string Program )
	{
		StringBuilder result = new StringBuilder( "" );
		result.Append( "	EXPORT __initial_sp\n" );
		result.Append( "	AREA STACK, NOINIT, READWRITE, ALIGN=3\n" );
		result.Append( "Stack_Mem SPACE 0x00000200\n" );
		result.Append( "__initial_sp\n" );
		result.Append( "	AREA RESET, DATA, READONLY\n" );
		result.Append( "__Vectors\n" );
		result.Append( "	DCD __initial_sp\n" );
		result.Append( "	DCD Reset_Handler\n" );
		result.Append( "	AREA |.text|, CODE, READONLY\n" );
		result.Append( "	ENTRY\n" );
		result.Append( "Reset_Handler\n" );
		result.Append( "	b " + Config.PreLabel.Function + MainLabel + "\n" );
		result.Append( Program + "\n" );
		result.Append( CORTEX_M0_Lib );
		return result.ToString();
	}
	
	//PC80X86打包
	static string PC80X86_Pack( string MainLabel, string Program )
	{
		//获取软件堆栈起始地址
		int VdataIndex = VdataList.GetIndex( VarType.VBase );
		int SoftSP_StartAddress = AddressList.StaticMaxAddr[ VdataIndex ] + Interrupt.GetStackNumber() + 1;
		SoftSP_StartAddress += 100000;
		StringBuilder result = new StringBuilder( "" );
		
		result.Append( ".386\n" );
		result.Append( ".model Flat,stdcall\n" );
		result.Append( "option casemap:none\n" );
		
		result.Append( ".data\n" );
		result.Append( Switch_PC80X86_CodeData() );
		
		result.Append( ".data?\n" );
		result.Append( "static_data_callback dd 1 dup(?)\n" );
		result.Append( "static_data db 200000 dup(?)\n" );
		
		result.Append( ".code\n" );
		result.Append( "start:\n" );
		result.Append( "lea edi,static_data\n" );
		result.Append( "lea esi,const_data0\n" );
		result.Append( "mov ebx,edi\n" );
		result.Append( "add ebx," + SoftSP_StartAddress + "\n" );
		result.Append( "mov eax,0\n" );
		result.Append( "mov static_data_callback,eax\n" );
		result.Append( "jmp " + MainLabel + "\n" );
		result.Append( GetFunctionLink() );
		result.Append( Program + "\n" );
		result.Append( PC80X86_Lib );
		result.Append( "end start\n" );
		return result.ToString();
	}
	
	//VM打包
	static string VM_Pack( string MainLabel, string Program )
	{
		//获取软件堆栈起始地址
		int VdataIndex = VdataList.GetIndex( VarType.VBase );
		int SoftSP_StartAddress = AddressList.StaticMaxAddr[ VdataIndex ] + Interrupt.GetStackNumber() + 1;
		//SoftSP_StartAddress += 0x1000;
		//SoftSP_StartAddress = 0x9000;
		StringBuilder result = new StringBuilder( "" );
		
		//result.Append( Switch_VM_CodeData() );
		//result.Append( "指令区\n" );
		result.Append( "跳转 _main\n" );
		result.Append( AddInterruptFor_VM() );
		result.Append( Switch_VM_CodeData() );
		result.Append( "指令对齐\n" );
		result.Append( "标号 _main\n" );
		result.Append( "设置堆栈 " + SoftSP_StartAddress + "\n" );
		result.Append( "跳转 " + MainLabel + "\n" );
		result.Append( Program + "\n" );
		result.Append( VM_Lib );
		result.Append( "结束\n" );
		return result.ToString();
	}
	
	//转换mcs51code代码表
	static string Switch_MCS51_CodeData()
	{
		string CodeDataSet =  CodeData.GetSeg();
		if( CodeDataSet == "" ) {
			return "";
		}
		StringBuilder Result = new StringBuilder( "" );
		string[] Seg = CodeDataSet.TrimEnd( '\n' ).Split( '\n' );
		for( int i = 0; i < Seg.Length; ++i ) {
			string Type = Seg[ i ].Split( ' ' )[ 0 ];
			string Data = Seg[ i ].Split( ' ' )[ 1 ];
			if( Type == VarType.BaseType.Uint8 ) {
				Result.Append( "db " );
				Result.Append( Data );
				Result.Append( "\n" );
			}
			else if( Type == VarType.BaseType.Uint16 ) {
				Result.Append( "db " );
				string[] Cut = Data.Split( ',' );
				for( int j = 0; j < Cut.Length; ++j ) {
					int N = int.Parse( Cut[ j ] );
					Result.Append( N % 256 + "," + N / 256 );
					if( j != Cut.Length - 1 ) {
						Result.Append( "," );
					}
				}
				Result.Append( "\n" );
			}
			else {
				ET.ShowError( "<Switch_MCS51_CodeData> 暂不支持此类型的常量: " + Type );
			}
		}
		return Result.ToString();
	}
	
	//转换AVR代码表
	static string Switch_ATMEGA_CodeData()
	{
		string CodeDataSet =  CodeData.GetSeg();
		if( CodeDataSet == "" ) {
			return "";
		}
		StringBuilder Result = new StringBuilder( "" );
		string[] Seg = CodeDataSet.TrimEnd( '\n' ).Split( '\n' );
		for( int i = 0; i < Seg.Length; ++i ) {
			string Type = Seg[ i ].Split( ' ' )[ 0 ];
			string Data = Seg[ i ].Split( ' ' )[ 1 ];
			if( Type == VarType.BaseType.Bool || Type == VarType.BaseType.Bit ) {
				Result.Append( ".db " );
				string[] Cut = Data.Split( ',' );
				for( int j = 0; j < Cut.Length; ++j ) {
					int N = int.Parse( Cut[ j ] );
					if( N > 1 ) {
						ET.ShowError( "<Switch_ATMEGA_CodeData> 位类型解析出错: " + N );
						N = 0;
					}
					if( N == 1 ) {
						N = 255;
					}
					Result.Append( N.ToString() );
					if( j != Cut.Length - 1 ) {
						Result.Append( "," );
					}
				}
				Result.Append( "\n" );
			}
			else if( Type == VarType.BaseType.Uint8 || Type == VarType.BaseType.Sint8 ) {
				Result.Append( ".db " );
				Result.Append( Data );
				Result.Append( "\n" );
			}
			else if( Type == VarType.BaseType.Uint16 || Type == VarType.BaseType.Sint16 ) {
				Result.Append( ".db " );
				string[] cut = Data.Split( ',' );
				Result.Append( AddUint16( cut ) );
				Result.Append( "\n" );
			}
			else if( Type == VarType.BaseType.Uint32 || Type == VarType.BaseType.Sint32 || Type == VarType.BaseType.fix ) {
				Result.Append( ".db " );
				string[] cut = Data.Split( ',' );
				Result.Append( AddUint32( cut ) );
				Result.Append( "\n" );
			}
			else {
				ET.ShowError( "<Switch_ATMEGA_CodeData> 暂不支持此类型的常量: " + Type );
			}
		}
		return Result.ToString();
	}
	
	//转换spce061a代码表
	static string Switch_SPCE061A_CodeData()
	{
		string CodeDataSet =  CodeData.GetSeg();
		if( CodeDataSet == "" ) {
			return "";
		}
		StringBuilder Result = new StringBuilder( "" );
		string[] Seg = CodeDataSet.TrimEnd( '\n' ).Split( '\n' );
		for( int i = 0; i < Seg.Length; ++i ) {
			string Type = Seg[ i ].Split( ' ' )[ 0 ];
			string Data = Seg[ i ].Split( ' ' )[ 1 ];
			Result.Append( ".dw " );
			Result.Append( Data );
			Result.Append( "\n" );
		}
		return Result.ToString();
	}
	
	//转换PC80X86代码表
	static string Switch_PC80X86_CodeData()
	{
		string CodeDataSet =  CodeData.GetSeg();
		if( CodeDataSet == "" ) {
			return "const_data0 db 0\n";
		}
		StringBuilder Result = new StringBuilder( "" );
		string[] Seg = CodeDataSet.TrimEnd( '\n' ).Split( '\n' );
		int index = 0;
		for( int i = 0; i < Seg.Length; ++i ) {
			string Type = Seg[ i ].Split( ' ' )[ 0 ];
			string Data = Seg[ i ].Split( ' ' )[ 1 ];
			
			string ASMType = null;
			switch( Type ) {
				case VarType.BaseType.Uint8:	ASMType = "db"; break;
				case VarType.BaseType.Uint16:	ASMType = "dw"; break;
				case VarType.BaseType.Uint32:	ASMType = "dd"; break;
				default:		ET.ShowError( "未知的汇编code代码类型: " + Type ); break;
			}
			string[] cut = Data.Split( ',' );
			Result.Append( "const_data" + index + " " + ASMType + " " );
			for( int j = 0; j < cut.Length; ++j ) {
				Result.Append( cut[ j ] );
				if( j % 40 == 39 ) {
					Result.Append( "\n" + ASMType + " " );
					continue;
				}
				if( j != cut.Length - 1 ) {
					Result.Append( "," );
				}
			}
			Result.Append( "\n" );
			++index;
		}
		return Result.ToString();
	}
	
	//转换VM代码表
	static string Switch_VM_CodeData()
	{
		string CodeDataSet =  CodeData.GetSeg();
		if( CodeDataSet == "" ) {
			return null;
		}
		StringBuilder Result = new StringBuilder( "" );
		string[] Seg = CodeDataSet.TrimEnd( '\n' ).Split( '\n' );
		for( int i = 0; i < Seg.Length; ++i ) {
			string Type = Seg[ i ].Split( ' ' )[ 0 ];
			string Data = Seg[ i ].Split( ' ' )[ 1 ];
			string[] cut = Data.Split( ',' );
			Result.Append( "CODE " );
			if( Type == VarType.BaseType.Uint8 || Type == VarType.BaseType.Sint8 ) {
				for( int j = 0; j < cut.Length; ++j ) {
					Result.Append( cut[ j ] );
					if( j != cut.Length - 1 ) {
						Result.Append( "," );
					}
				}
				Result.Append( "\n" );
			}
			else if( Type == VarType.BaseType.Uint16 || Type == VarType.BaseType.Sint16 ) {
				Result.Append( AddUint16( cut ) );
				Result.Append( "\n" );
			}
			else if( Type == VarType.BaseType.Uint32 || Type == VarType.BaseType.Sint32 || Type == VarType.BaseType.fix ) {
				Result.Append( AddUint32( cut ) );
				Result.Append( "\n" );
			}
			else {
				ET.ShowError( "<Switch_VM_CodeData> 暂不支持此类型的常量: " + Type );
			}
		}
		return Result.ToString();
	}
	
	static string AddUint16( string[] cut )
	{
		string Result = null;
		for( int j = 0; j < cut.Length; ++j ) {
			
			string label = null;
			long v = GetStaticVarAddr( cut[j], ref label );
			
			/*
			if( v >= 0x100000000 ) {
				v -= 0x100000000;
				Result += "&L" + ( v ).ToString();
				Result += ",";
				Result += "&H" + ( v ).ToString();
			}
			*/
			if( label != null ) {
				Result += "&L" + label;
				Result += ",";
				Result += "&H" + label;
			}
			else {
				Result += ( v % 256 ).ToString();
				Result += ",";
				Result += ( v / 256 ).ToString();
			}
			if( j != cut.Length - 1 ) {
				Result += ",";
			}
		}
		return Result;
	}
	
	static string AddUint32( string[] cut )
	{
		string Result = null;
		for( int j = 0; j < cut.Length; ++j ) {
			
			string label = null;
			long v = GetStaticVarAddr( cut[j], ref label );
			
			/*
			if( v >= 0x100000000 ) {
				v -= 0x100000000;
				Result += "&L" + ( v ).ToString();
				Result += ",";
				Result += "&H" + ( v ).ToString();
				Result += ",";
				Result += "&E" + ( v ).ToString();
				Result += ",";
				Result += "0";
			}
			*/
			if( label != null ) {
				Result += "&L" + label;
				Result += ",";
				Result += "&H" + label;
				Result += ",";
				Result += "&E" + label;
				Result += ",";
				Result += "0";
			}
			else {
				Result += ( v % 256 ).ToString();
				Result += ",";
				v /= 256;
				Result += ( v % 256 ).ToString();
				Result += ",";
				v /= 256;
				Result += ( v % 256 ).ToString();
				Result += ",";
				v /= 256;
				Result += ( v % 256 ).ToString();
			}
			if( j != cut.Length - 1 ) {
				Result += ",";
			}
		}
		return Result;
	}
	
	static long GetStaticVarAddr( string Name0, ref string label )
	{
		long N = 0;
		if( Name0.StartsWith( "#" ) ) {
			
			string Name = Name0.Remove( 0, 1 );
			
			//这里好像是没有用
			bool w0 = false;
			if( Name0.StartsWith( "#addrw0+" ) ) {
				Name = Name0.Remove( 0, 8 );
				w0 = true;
			}
			
			int vi = VarList.GetStaticIndex( Name );
			if( vi == -1 ) {
				
				vi = FunctionList.GetIndex( Name );
				if( vi == -1 ) {
					label = Name;
				}
				else {
					label = Config.PreLabel.Function + vi;
				}
				
				
				/*
				vi = FunctionList.GetIndex( Name );
				if( vi == -1 ) {
					ET.WriteParseError( 0, "<Pack>扫描数组常量时遇到未定义的静态变量(地址)或者函数地址: " + Name );
					return 0;
				}
				N = 0x100000000 + vi;
				*/
			}
			else {
				
				//根据类型判断地址是否需要增加第三字节标志位
				if( VarType.StoreTypeisVcode( VarList.Get( vi ).Type ) ) {
					N = 0x00010000 + long.Parse( VarList.Get( vi ).Address );
				}
				else {
					N = long.Parse( VarList.Get( vi ).Address );
				}
				if( w0 ) {
					N &= 0xFFFF;
				}
			}
		}
		else {
			N = long.Parse( Name0 );
		}
		return N;
	}
	
	//添加mcs51中断入口地址
	static string AddInterruptFor_MCS51()
	{
		string[] Cut = Interrupt.GetSet();
		StringBuilder InterruptGate = new StringBuilder( "" );
		if( Cut != null ) {
			for( int i = 0; i < Cut.Length; ++i ) {
				string[] c = Cut[ i ].Split( ' ' );
				InterruptGate.Append( "org " + c[ 0 ] + "\n" + "ljmp " + Config.PreLabel.Function + c[ 1 ] + "\n" );
			}
		}
		return InterruptGate.ToString();
	}
	
	//添加ATMEGA中断入口地址
	static string AddInterruptFor_ATMEGA()
	{
		string[] Cut = Interrupt.GetSet();
		StringBuilder InterruptGate = new StringBuilder( "" );
		if( Cut != null ) {
			for( int i = 0; i < Cut.Length; ++i ) {
				string[] c = Cut[ i ].Split( ' ' );
				int Addr = int.Parse( c[ 0 ] );
				if( CPUType.isShortInterruptAddr( Config.GetCPU() ) ) {
					InterruptGate.Append( ".org " + Addr + "\n" + "rjmp " + Config.PreLabel.Function + c[ 1 ] + "\n" );
				}
				else {
					InterruptGate.Append( ".org " + Addr + "\n" + "jmp " + Config.PreLabel.Function + c[ 1 ] + "\n" );
				}
			}
		}
		return InterruptGate.ToString();
	}
	
	//添加spce061a中断入口地址
	static string AddInterruptFor_SPCE061A()
	{
		return null;
	}
	
	//添加VM中断入口地址
	const int IntNumber = 31;
	static string AddInterruptFor_VM()
	{
		string[] Cut = Interrupt.GetSet();
		StringBuilder InterruptGate = new StringBuilder( "" );
		
		int NullNumber = 0;
		int Total = 0;
		for( int i = 0; i < IntNumber; ++i ) {
			if( Cut == null || i >= Cut.Length ) {
				InterruptGate.Append( "中断未用跳转\n" );
				Total++;
				if( Total == IntNumber ) {
					break;
				}
				continue;
			}
			string[] c = Cut[ i ].Split( ' ' );
			int Addr = int.Parse( c[ 0 ] );
			int Target = Addr - 1 - NullNumber;
			for( int Index = i; Index < Target; ++Index ) {
				InterruptGate.Append( "中断未用跳转\n" );
				Total++;
				++NullNumber;
			}
			
			//InterruptGate.Append( "指令地址 " + Addr + "\n" + "跳转 f" + c[ 1 ] + "\n" );
			InterruptGate.Append( "跳转 " + Config.PreLabel.Function + c[ 1 ] + "\n" );
			Total++;
			if( Total == IntNumber ) {
				break;
			}
		}
		return InterruptGate.ToString();
	}
	
	//添加AVR起始代码
	static string AddStartCode()
	{
		StringBuilder result = new StringBuilder( "" );
		if( CPUType.isShortInterruptAddr( Config.GetCPU() ) ) {
			result.Append( ".org 0\n" );
			result.Append( "rjmp fmain\n" );
		}
		else {
			result.Append( ".org 0\n" );
			result.Append( "jmp fmain\n" );
		}
		return result.ToString();
	}
	
	//获取库函数声明,用于PC80X86
	static string GetFunctionLink()
	{
		StringBuilder Result = new StringBuilder( "" );
		for( int i = 0; i < FunctionList.length; ++i ) {
			if( FunctionList.Get( i ).InterType != MemberType.FunctionType.Interface ) {
				continue;
			}
			//以下为插入DLL调用的汇编声明
			string Name = FunctionList.Get( i ).FunctionName;
			Name = Name.Remove( 0, Name.LastIndexOf( '.' ) + 1 );
			Result.Append( Name + " PROTO " );
			string VarIndexList = FunctionList.Get( i ).VarIndexList;
			if( VarIndexList != null ) {
				int Number = VarIndexList.Split( ' ' ).Length;
				for( int j = 0; j < Number; ++j ) {
					Result.Append( ":DWORD" );
					if( j != Number - 1 ) {
						Result.Append( "," );
					}
				}
			}
			Result.Append( "\n" );
		}
		return Result.ToString();
	}
	
	static string MCS51_Lib;
	static string ATMEGA_Lib;
	static string SPCE061A_Lib;
	static string CORTEX_M0_Lib;
	static string PC80X86_Lib;
	static string VM_Lib;
}
}

