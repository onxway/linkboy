﻿
//预定义标识符类
namespace n_EQU
{
using System;
using System.Text;

public static class EQU
{
	//初始化网络
	public static void Reset()
	{
		NodeSet = new string[ 1000 ][];
		Length = 0;
		AddNode( "acc.0", "0e0h" );
		AddNode( "acc.1", "0e1h" );
		AddNode( "acc.2", "0e2h" );
		AddNode( "acc.3", "0e3h" );
		AddNode( "acc.4", "0e4h" );
		AddNode( "acc.5", "0e5h" );
		AddNode( "acc.6", "0e6h" );
		AddNode( "acc.7", "0e7h" );
		AddNode( "acc", "0e0h" );
		AddNode( "b", "0f0h" );
		AddNode( "psw", "0d0h" );
		AddNode( "sp", "81h" );
		AddNode( "dpl", "82h" );
		AddNode( "dph", "83h" );
//		AddNode( "ie", "a8h" );
//		AddNode( "ip", "d8h" );
//		AddNode( "p0", "80h" );
//		AddNode( "p1", "90h" );
//		AddNode( "p2", "a0h" );
//		AddNode( "p3", "b0h" );
//		AddNode( "pcon", "87h" );
//		AddNode( "scon", "98h" );
//		AddNode( "sbuf", "99h" );
//		AddNode( "tcon", "88h" );
//		AddNode( "tmod", "89h" );
//		AddNode( "tl0", "8ah" );
//		AddNode( "tl1", "8bh" );
//		AddNode( "th0", "8ch" );
//		AddNode( "th1", "8dh" );
	}
	
	//显示网络
	public static string Show()
	{
		string Result = "";
		for( int Index = 0; Index < Length; ++Index ) {
			Result += Index + ":\t" + string.Join( " ", NodeSet[ Index ] ) + "\n";
		}
		return Result.Trim( '\n' );
	}
	
	//添加节点
	public static void AddNode( string Name, string Value )
	{
		NodeSet[ Length ] = new string[ 2 ];
		NodeSet[ Length ][ 0 ] = Name;
		NodeSet[ Length ][ 1 ] = Value;
		++Length;
	}
	
	//获取一个名称的值,没有的名称返回 null
	public static string GetValue( string Name )
	{
		for( int i = 0; i < Length; ++i ) {
			if( NodeSet[ i ][ 0 ] == Name ) {
				return NodeSet[ i ][ 1 ];
			}
		}
		return null;
	}
	
	public static string[][] NodeSet;	//网络集
	static int Length;			//网络长度
}
}
