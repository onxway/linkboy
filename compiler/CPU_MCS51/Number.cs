﻿
//常量字符串转化类
namespace n_Number
{
using System;
using n_ET;

public static class Number
{
	//计算一个字节常量的值
	public static string GetByteValue( string Const, int CLine )
	{
		ErrorLine = CLine;
		Const = ToHEX( Const );
		if( Const.Length != 2 ) {
			
			
			n_OS.VIO.Show( "EEE " + Const );
			
			ET.WriteLineError( 0, ErrorLine, "常量应为字节型: " + Const );
			return "00";
		}
		return Const;
	}
	
	//计算一个字常量的值
	public static string GetDoubleValue( string Const, int CLine )
	{
		ErrorLine = CLine;
		Const = ToHEX( Const );
		if( Const.Length == 2 ) {
			return "00" + Const;
		}
		return Const;
	}
	
	//转换为十六进制的值
	static string ToHEX( string Const )
	{
		string result = "";
		if( Const.EndsWith( "b" ) || Const.EndsWith( "B" ) ) {
			result = BINToHEX( Const );
		}
		else if( Const.EndsWith( "h" ) || Const.EndsWith( "H" ) ) {
			result = HEXToHEX( Const );
		}
		else {
			result = DECToHEX( Const );
		}
		if( result.Length == 0 ) {
			result = "00";
		}
		if( result.Length == 1 || result.Length == 3 ) {
			result = "0" + result;
		}
		if( result.Length > 4 ) {
			ET.WriteLineError( 0, ErrorLine, "常量过大: " + Const );
			return "00";
		}
		return result;
	}
	
	//十进制--十六进制
	static string DECToHEX( string Const )
	{
		if( Const.IndexOf( "+" ) != -1 ) {
			string[] Data = Const.Split( '+' );
			int V1 = int.Parse( Data[ 0 ] );
			int V2 = 0;
			int Mode = -1;
			if( Data[ 1 ].IndexOf( "%" ) != -1 ) {
				string[] Data1 = Data[ 1 ].Split( '%' );
				V2 = int.Parse( Data1[ 0 ] );
				Mode = int.Parse( Data1[ 1 ] );
			}
			else {
				V2 = int.Parse( Data[ 1 ] );
			}
			int R = V1 + V2;
			if( Mode != -1 ) {
				R %= Mode;
			}
			Const = R.ToString();
		}
		else if( Const.IndexOf( "-" ) != -1 ) {
			string[] Data = Const.Split( '-' );
			int V1 = int.Parse( Data[ 0 ] );
			int V2 = 0;
			int Mode = -1;
			if( Data[ 1 ].IndexOf( "%" ) != -1 ) {
				string[] Data1 = Data[ 1 ].Split( '%' );
				V2 = int.Parse( Data1[ 0 ] );
				Mode = int.Parse( Data1[ 1 ] );
			}
			else {
				V2 = int.Parse( Data[ 1 ] );
			}
			int R = V1 - V2;
			if( Mode != -1 ) {
				R %= Mode;
			}
			Const = R.ToString();
		}
		else if( Const.IndexOf( "*" ) != -1 ) {
			string[] Data = Const.Split( '*' );
			int V1 = int.Parse( Data[ 0 ] );
			int V2 = 0;
			int Mode = -1;
			if( Data[ 1 ].IndexOf( "%" ) != -1 ) {
				string[] Data1 = Data[ 1 ].Split( '%' );
				V2 = int.Parse( Data1[ 0 ] );
				Mode = int.Parse( Data1[ 1 ] );
			}
			else {
				V2 = int.Parse( Data[ 1 ] );
			}
			int R = V1 * V2;
			if( Mode != -1 ) {
				R %= Mode;
			}
			Const = R.ToString();
		}
		else if( Const.IndexOf( "|" ) != -1 ) {
			string[] Data = Const.Split( '|' );
			int V1 = int.Parse( Data[ 0 ] );
			int V2 = 0;
			int Mode = -1;
			if( Data[ 1 ].IndexOf( "%" ) != -1 ) {
				string[] Data1 = Data[ 1 ].Split( '%' );
				V2 = int.Parse( Data1[ 0 ] );
				Mode = int.Parse( Data1[ 1 ] );
			}
			else {
				V2 = int.Parse( Data[ 1 ] );
			}
			int R = V1 / V2;
			if( Mode != -1 ) {
				R %= Mode;
			}
			Const = R.ToString();
		}
		else if( Const.IndexOf( "%" ) != -1 ) {
			string[] Data = Const.Split( '%' );
			int V1 = int.Parse( Data[ 0 ] );
			int V2 = 0;
			int Mode = -1;
			if( Data[ 1 ].IndexOf( "%" ) != -1 ) {
				string[] Data1 = Data[ 1 ].Split( '%' );
				V2 = int.Parse( Data1[ 0 ] );
				Mode = int.Parse( Data1[ 1 ] );
			}
			else {
				V2 = int.Parse( Data[ 1 ] );
			}
			int R = V1 % V2;
			if( Mode != -1 ) {
				R %= Mode;
			}
			Const = R.ToString();
		}
		else {
			//...
		}
		
		if( Const.Length > 5 ) {
			ET.WriteLineError( 0, ErrorLine, "数字过大: " + Const );
			return "0";
		}
		for( int i = 0; i < Const.Length; ++i ) {
			if( Const[ i ] < '0' || Const[ i ] > '9' ) {
				ET.WriteLineError( 0, ErrorLine, "十进制数字中不能包含非数字符号: " + Const );
				return "0";
			}
		}
		string Number = "";
		int n = int.Parse( Const );
		do {
			Number = SwitchToHEX( n % 16 ) + Number;
			n = n / 16;
		}
		while( n != 0 );
		return Number;
	}
	
	//十六进制--十六进制
	static string HEXToHEX( string Const )
	{
		if( Const.Length > 7 ) {
			ET.WriteLineError( 0, ErrorLine, "十六进制数字过大: " + Const );
			return "0";
		}
		for( int i = 0; i < Const.Length - 1; ++i ) {
			if( !isHexNumber( Const[ i ] ) ) {
				ET.WriteLineError( 0, ErrorLine, "十六进制数字中不能包含无效符号: " + Const );
				return "0";
			}
		}
		Const = Const.Remove( Const.Length - 1, 1 );
		while( Const.StartsWith( "0" ) ) {
			Const = Const.Remove( 0, 1 );
		}
		
		return Const.ToUpper();
	}
	
	//二进制--十六进制
	static string BINToHEX( string Const )
	{
		if( Const.Length > 17 ) {
			ET.WriteLineError( 0, ErrorLine, "二进制数字过大: " + Const );
			return "0";
		}
		for( int i = 0; i < Const.Length - 1; ++i ) {
			if( Const[ i ] != '0' && Const[ i ] != '1' ) {
				ET.WriteLineError( 0, ErrorLine, "二进制数字中不能包含无效数字符号: " + Const );
				return "0";
			}
		}
		int n = 0;
		int Base = 1;
		for( int i = Const.Length - 2; i >= 0; --i ) {
			n += ( (int)Const[ i ] - 0x30 ) * Base;
			Base *= 2;
		}
		string Number = "";
		do {
			Number = SwitchToHEX( n % 16 ) + Number;
			n = n / 16;
		}
		while( n != 0 );
		return Number;
	}
	
	//查找十六进制字符
	static string SwitchToHEX( int n )
	{
		if( n > 15 ) {
			ET.ShowError( "系统错误: 数字大于15: " + n.ToString() );
			return "0";
		}
		string Table = "0123456789ABCDEF";
		return Table[ n ].ToString();
	}
	
	//判断是否为十六进制数字
	static bool isHexNumber( char c )
	{
		if( c >= '0' && c <= '9' ||
		    c >= 'a' && c <= 'f' ||
		    c >= 'A' && c <= 'F' )
		{
			return true;
		}
		else
		{
			return false;
		}
	}
	
	static int ErrorLine;
}
}

