﻿
//预定义标识符类
namespace n_ASMLabel
{
using System;
using System.Text;

public static class ASMLabel
{
	//初始化网络
	public static void Init()
	{
		NodeSet = new string[ 10000 ][];
	}
	
	//清除网络
	public static void Clear()
	{
		Length = 0;
	}
	
	//显示网络
	public static string Show()
	{
		string Result = "";
		for( int Index = 0; Index < Length; ++Index ) {
			Result += Index + ":\t" + string.Join( " ", NodeSet[ Index ] ) + "\n";
		}
		return Result.Trim( '\n' );
	}
	
	//添加节点
	public static void AddNode( string Name, string Address )
	{
		NodeSet[ Length ] = new string[ 2 ];
		NodeSet[ Length ][ 0 ] = Name;
		NodeSet[ Length ][ 1 ] = Address;
		++Length;
	}
	
	//获取一个标号的地址,没有的名称返回 -1
	public static int GetValue( string Name )
	{
		for( int i = 0; i < Length; ++i ) {
			if( NodeSet[ i ][ 0 ] == Name ) {
				return int.Parse( NodeSet[ i ][ 1 ] );
			}
		}
		return -1;
	}
	
	public static string[][] NodeSet;	//网络集
	static int Length;			//网络长度
}
}
