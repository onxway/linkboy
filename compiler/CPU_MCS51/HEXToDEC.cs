﻿
//常量字符串转化类
namespace n_HEXToDEC
{
using System;

public static class HEXToDEC
{
	//计算一个十六进制数的值
	public static int GetDecValue( string Const )
	{
		int Base = 1;
		int Number = 0;
		for( int i = Const.Length - 1; i >= 0; --i ) {
			Number += HexToInteger( Const[ i ] ) * Base;
			Base *= 16;
		}
		return Number;
	}
	
	//判断是否为十进制数字
	static bool isDecNumber( char c )
	{
		if( c >= '0' && c <= '9' )
		{
			return true;
		}
		else
		{
			return false;
		}
	}
	
	//十六进制数字转化为0-15,非数值符号返回-1
	static int HexToInteger( char c )
	{
		if( isDecNumber( c ) )
		{
			return int.Parse( c.ToString() );
		}
		if( c >= 'A' && c <= 'F' )
		{
			return (int)c - 0x41 + 10;
		}
		if( c >= 'a' && c <= 'f' )
		{
			return (int)c - 0x61 + 10;
		}
		return -1;
	}
}
}

