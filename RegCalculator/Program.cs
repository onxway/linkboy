﻿
using System;
using System.Windows.Forms;

namespace n_RegCalForm
{
internal sealed class Program
{
	[STAThread]
	private static void Main(string[] args)
	{
		Application.EnableVisualStyles();
		Application.SetCompatibleTextRenderingDefault(false);
		
		string DefaultFilePath = null;
		
		//判断是否加载文件
		if( ( args != null ) && ( args.Length > 0 ) ) {
			string filePath = "";
			int si = 0;
			for( int i = si; i < args.Length; ++i ) {
				filePath += " " + args[ i ];
			}
			DefaultFilePath = filePath.Trim( ' ' );
		}
		
		Application.Run( new Form1() );
	}
}
}


