﻿
namespace n_GPanel
{
using System;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Windows.Forms;

using n_RegCalForm;

//*****************************************************
//图形编辑器容器类
public class GPanel : Panel
{
	bool LeftisPress;
	bool RightisPress;
	
	int MouseX;
	int MouseY;
	
	Font f;
	
	//构造函数
	public GPanel( Form1 fm, int Width, int Height ) : base()
	{
		//CellBitmap = new Bitmap( AppDomain.CurrentDomain.BaseDirectory + "Resource\\CloseIconOn.ico" );
		
		this.BorderStyle = BorderStyle.None;
		this.BackColor = Color.Black;
		
		SetStyle(ControlStyles.UserPaint |
		         ControlStyles.AllPaintingInWmPaint |
		         ControlStyles.OptimizedDoubleBuffer |
		         ControlStyles.ResizeRedraw |
		         ControlStyles.SupportsTransparentBackColor,true);
		
		this.Dock = DockStyle.Fill;
		this.ImeMode = ImeMode.NoControl;
		
		this.Width = Width;
		this.Height = Height;
		
		f = new Font( "微软雅黑", 12 );
		
		//Cursor Cursor1 = new Cursor( AppDomain.CurrentDomain.BaseDirectory + "Resource\\myPointEdit.cur" );
		//Cursor2 = new Cursor( OS.SystemRoot + "Resource" + OS.PATH_S + "myPoint2.cur" );
		
		//this.Cursor = Cursor1;
		
		this.MouseMove += new MouseEventHandler( UserMouseMove );
		this.MouseUp += new MouseEventHandler( UserMouseUp );
		this.MouseDown += new MouseEventHandler( UserMouseDown );
		
		LeftisPress = false;
		RightisPress = false;
	}
	
	
	//==========================================
	
	//重绘事件
	protected override void OnPaint(PaintEventArgs e)
	{
		//禁用图像模糊效果
		e.Graphics.InterpolationMode = InterpolationMode.NearestNeighbor;
		e.Graphics.PixelOffsetMode = PixelOffsetMode.Half;
		
		ShowImage( e.Graphics );
	}
	
	//显示图像
	public void ShowImage( Graphics g )
	{
		//绘制背景
		g.FillRectangle( Brushes.Silver, 0, 0, Width, Height );
		
		g.DrawString( "本软件未开发完成, 请静待新版本!", f, Brushes.OrangeRed, 200, 200 );
	}
	
	//鼠标滚轮事件
	public void UserMouseWheel( int px, int py, int Delta )
	{
		if( !RightisPress ) {
			
		}
		else {
			
		}
		this.Invalidate();
	}
	
	//鼠标按下事件
	void UserMouseDown( object sender, MouseEventArgs e )
	{
		if( e.Button == MouseButtons.Left ) {
			LeftisPress = true;
		}
		if( e.Button == MouseButtons.Right ) {
			RightisPress = true;
		}
		this.Invalidate();
	}
	
	//鼠标松开事件
	void UserMouseUp( object sender, MouseEventArgs e )
	{
		if( e.Button == MouseButtons.Left ) {
			LeftisPress = false;
		}
		if( e.Button == MouseButtons.Right ) {
			RightisPress = false;
		}
		this.Invalidate();
	}
	
	//鼠标移动事件
	void UserMouseMove( object sender, MouseEventArgs e )
	{
		MouseX = e.X;
		MouseY = e.Y;
		
		if( LeftisPress ) {
			
		}
		if( RightisPress ) {
			
		}
		this.Invalidate();
	}
}
}


