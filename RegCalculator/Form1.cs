﻿
namespace n_RegCalForm
{
using System;
using System.Windows.Forms;
using System.Drawing;
//using c_FormMover;
using n_GPanel;
using winlib.MyColorForm;

public partial class Form1 : Form
{
	public bool SingleMode = true;
	
	GPanel IPanel;
	
	const int BackOff = 30;
	
	//主窗口
	public Form1()
	{
		InitializeComponent();
		
		//fm = new FormMover( this );
		
		IPanel = new GPanel( this, Screen.PrimaryScreen.WorkingArea.Width, Screen.PrimaryScreen.WorkingArea.Height );
		this.panel1.Controls.Add( IPanel );
		
		this.MouseWheel += UserMouseWheel;
	}
	
	//运行
	public void Run()
	{
		this.Visible = true;
	}
	
	void ImageEditFormFormClosing(object sender, FormClosingEventArgs e)
	{
		if( !SingleMode ) {
			this.Visible = false;
			e.Cancel = true;
			return;
		}
	}
	
	void UserMouseWheel( object sender, MouseEventArgs e )
	{
		IPanel.UserMouseWheel( e.X - panel1.Location.X, e.Y - panel1.Location.Y, e.Delta );
	}
}
}



