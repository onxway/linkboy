﻿
using System;
using System.Drawing;
using System.Drawing.Drawing2D;

namespace n_DPort
{
public class DPort
{
	public int Type;
	
	public float SX;
	public float SY;
	
	public float Width;
	public float Height;
	
	public int Value;
	
	public bool MouseOn;
	public bool MousePress;
	
	//构造函数
	public DPort( int t, float x, float y, float w, float h )
	{
		Type = t;
		
		SX = x;
		SY = y;
		Width = w;
		Height = h;
		
		MouseOn = false;
		MousePress = false;
	}
	
	//显示
	public void Draw( Graphics g )
	{
		Brush b = Brushes.CornflowerBlue;
		if( Value != 0 ) {
			b = Brushes.OrangeRed;
		}
		
		if( Type == 0 ) {
			g.FillEllipse( b, SX, SY, Width, Height );
			return;
		}
		
		if( Type == 1 ) {
			g.FillRectangle( b, SX, SY, Width, Height );
		}
		if( Type == 2 ) {
			g.FillRectangle( Brushes.Purple, SX, SY, Width, Height );
		}
		if( MouseOn ) {
			g.DrawRectangle( Pens.Black, SX, SY, Width, Height );
		}
	}
	
	//鼠标按下事件
	public void MouseDown( float mx, float my )
	{
		if( MouseOn ) {
			MousePress = true;
			if( Type == 1 ) {
				Value = 1;
			}
		}
	}
	
	//鼠标松开事件
	public void MouseUp( float mx, float my )
	{
		if( MousePress ) {
			MousePress = false;
			if( Type == 1 ) {
				Value = 0;
			}
		}
	}
	
	//鼠标移动事件
	public void MouseMove( float mx, float my )
	{
		if( mx >= SX && mx <= SX + Width && my >= SY && my <= SY + Height ) {
			MouseOn = true;
		}
		else {
			MouseOn = false;
		}
	}
}
}


