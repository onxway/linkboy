﻿
namespace n_ArduinoSimPanel
{
using System;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Windows.Forms;
using n_DPort;

//*****************************************************
//图形编辑器容器类
public class ArduinoSimPanel : Panel
{
	public float StartX;
	public float StartY;
	
	bool isMousePress;
	int Last_mX, Last_mY;
	
	public int ScaleIndex;
	public int[] ScaleList;
	public int AScale;
	public int AScaleMid = 1024;
	
	Font textFont;
	
	//public bool AutoShow;
	
	DPort[] DPortList;
	
	int[] temp;
	
	//构造函数
	public ArduinoSimPanel() : base()
	{
		this.BorderStyle = BorderStyle.None;
		this.BackColor = Color.Black;
		
		SetStyle(ControlStyles.UserPaint |
		         ControlStyles.AllPaintingInWmPaint |
		         ControlStyles.OptimizedDoubleBuffer |
		         ControlStyles.ResizeRedraw |
		         ControlStyles.SupportsTransparentBackColor,true);
		
		this.Dock = DockStyle.Fill;
		this.ImeMode = ImeMode.NoControl;
		
		StartX = 0;
		StartY = 0;
		isMousePress = false;
		
		//AutoShow = false;
		
		this.MouseMove += new MouseEventHandler( UserMouseMove );
		this.MouseUp += new MouseEventHandler( UserMouseUp );
		this.MouseDown += new MouseEventHandler( UserMouseDown );
		this.MouseWheel += new MouseEventHandler( UserMouseWheel );
		
		textFont = new Font( "微软雅黑", 12 );
		
		InitAScale();
		
		temp = new int[100];
		
		DPortList = new DPort[44];
		for( int i = 0; i < 22; ++i ) {
			DPortList[i] = new DPort( 0, 60, 30 + i*30, 20, 20 );
		}
		for( int i = 22; i < 44; ++i ) {
			DPortList[i] = new DPort( 1, 90, 30 + (i - 22)*30, 20, 20 );
		}
		//for( int i = 44; i < 66; ++i ) {
		//	DPortList[i] = new DPort( 2, 90, 30 + (i - 44)*30, 100, 20 );
		//}
	}
	
	public void ModuleWrite( int V1, int V2 )
	{
		int ModuleIndex = V1 / 65536;
		int Addr = V1 % 65536;
		
		if( ModuleIndex == 0 ) {
			DPortList[Addr].Value = V2;
		}
		else {
			temp[ModuleIndex*10+Addr] = V2;
		}
		
		Invalidate();
	}
	
	public int ModuleRead( int V1 )
	{
		int ModuleIndex = V1 / 65536;
		int Addr = V1 % 65536;
		
		if( ModuleIndex == 0 ) {
			return DPortList[Addr].Value;
		}
		else {
			
		}
		return temp[ModuleIndex*10+Addr];
	}
	
	//重新开始仿真
	public void ResetSim()
	{
		for( int i = 0; i < DPortList.Length; ++i ) {
			DPortList[i].Value = 0;
		}
	}
	
	//结束仿真
	public void StopSim()
	{
		ResetSim();
		Invalidate();
	}
	
	//初始化系统放缩
	void InitAScale()
	{
		AScale = AScaleMid;
		
		ScaleIndex = 10;
		ScaleList = new int[21];
		
		float f1 = 100;
		float f2 = 100;
		float ff = 0.8f;
		
		ScaleList[10] = AScaleMid * 100 / 100;
		
		f1 *= ff;
		f2 /= ff;
		ScaleList[9] = AScaleMid * (int)f1 / 100;
		ScaleList[11] = AScaleMid * (int)f2 / 100;
		f1 *= ff;
		f2 /= ff;
		ScaleList[8] = AScaleMid * (int)f1 / 100;
		ScaleList[12] = AScaleMid * (int)f2 / 100;
		f1 *= ff;
		f2 /= ff;
		ScaleList[7] = AScaleMid * (int)f1 / 100;
		ScaleList[13] = AScaleMid * (int)f2 / 100;
		f1 *= ff;
		f2 /= ff;
		ScaleList[6] = AScaleMid * (int)f1 / 100;
		ScaleList[14] = AScaleMid * (int)f2 / 100;
		f1 *= ff;
		f2 /= ff;
		ScaleList[5] = AScaleMid * (int)f1 / 100;
		ScaleList[15] = AScaleMid * (int)f2 / 100;
		f1 *= ff;
		f2 /= ff;
		ScaleList[4] = AScaleMid * (int)f1 / 100;
		ScaleList[16] = AScaleMid * (int)f2 / 100;
		f1 *= ff;
		f2 /= ff;
		ScaleList[3] = AScaleMid * (int)f1 / 100;
		ScaleList[17] = AScaleMid * (int)f2 / 100;
		f1 *= ff;
		f2 /= ff;
		ScaleList[2] = AScaleMid * (int)f1 / 100;
		ScaleList[18] = AScaleMid * (int)f2 / 100;
		f1 *= ff;
		f2 /= ff;
		ScaleList[1] = AScaleMid * (int)f1 / 100;
		ScaleList[19] = AScaleMid * (int)f2 / 100;
		f1 *= ff;
		f2 /= ff;
		ScaleList[0] = AScaleMid * (int)f1 / 100;
		ScaleList[20] = AScaleMid * (int)f2 / 100;
	}
	
	//------------------------------------------------------------
	
	//重绘事件
	protected override void OnPaint(PaintEventArgs e)
	{
		Graphics g = e.Graphics;
		
		//设置绘图目标
		//SG.SetObject( g );
		
		g.SmoothingMode = SmoothingMode.HighQuality;
		
		//启用图像模糊效果
		g.InterpolationMode = System.Drawing.Drawing2D.InterpolationMode.Default;
		g.PixelOffsetMode = System.Drawing.Drawing2D.PixelOffsetMode.Half;
		
		//开始界面放缩
		if( AScale != AScaleMid ) {
			g.ScaleTransform( (float)AScale/AScaleMid, (float)AScale/AScaleMid );
		}
		g.TranslateTransform( StartX, StartY );
		
		//填充背景
		g.Clear( Color.WhiteSmoke );
		
		
		//for( int i = 0; i < DataList.Length; ++i ) {
		
		
		for( int i = 0; i < DPortList.Length; ++i ) {
			DPortList[i].Draw( g );
			if( i < 22 ) {
				g.DrawString( "D" + i + ":", textFont, Brushes.Black, 5, DPortList[i].SY );
			}
		}
		
		g.DrawString( "延时时间(毫秒):" + temp[10+1], textFont, Brushes.Green, 50 - 35, 2 );
		
		//}
	}
	
	//触发刷新操作
	public void NeedRefresh()
	{
		this.Invalidate();
	}
	
	//------------------------------------------------------------
	
	//鼠标按下事件
	void UserMouseDown( object sender, MouseEventArgs e )
	{
		Focus();
		
		int eX = e.X;
		int eY = e.Y;
		
		//如果按下左键
		if( e.Button == MouseButtons.Left ) {
			isMousePress = true;
			Last_mX = eX;
			Last_mY = eY;
			
			for( int i = 0; i < DPortList.Length; ++i ) {
				DPortList[i].MouseDown( -StartX + eX, -StartY + eY );
			}
		}
		//判断是否按下右键
		if( e.Button == MouseButtons.Right ) {
			
		}
		//判断是否按下鼠标中键
		if( e.Button == MouseButtons.Middle ) {
			
		}
		//界面刷新
		Invalidate();
	}
	
	//鼠标松开事件
	void UserMouseUp( object sender, MouseEventArgs em )
	{
		int eX = em.X;
		int eY = em.Y;
		
		if( em.Button == MouseButtons.Left ) {
			isMousePress = false;
			
			for( int i = 0; i < DPortList.Length; ++i ) {
				DPortList[i].MouseUp( -StartX + eX, -StartY + eY );
			}
		}
		if( em.Button == MouseButtons.Right ) {
			
		}
		if( em.Button == MouseButtons.Middle ) {
			
		}
		
		//界面刷新
		Invalidate();
	}
	
	//鼠标移动事件
	void UserMouseMove( object sender, MouseEventArgs em )
	{
		int eX = em.X;
		int eY = em.Y;
		
		for( int i = 0; i < DPortList.Length; ++i ) {
			DPortList[i].MouseMove( -StartX + eX, -StartY + eY );
		}
		
		//处理背景的拖动
		if( isMousePress ) {
			float x = (eX - Last_mX) * 1024f / AScale;
			float y = (eY - Last_mY) * 1024f / AScale;
			StartX += x;
			StartY += y;
			
			Last_mX = eX;
			Last_mY = eY;
		}
		else {
			//...
		}
		Invalidate();
	}
	
	//鼠标滚轮事件
	void UserMouseWheel( object sender, MouseEventArgs e )
	{
		if( e.Delta > 0 ) {
			if( ScaleIndex < ScaleList.Length - 1 ) {
				float mapx = e.X * AScaleMid / AScale - StartX;
				float mapy = e.Y * AScaleMid / AScale - StartY;
				ScaleIndex++;
				AScale = ScaleList[ScaleIndex];
				StartX = e.X * AScaleMid / AScale - mapx;
				StartY = e.Y * AScaleMid / AScale - mapy;
			}
		}
		else {
			if( ScaleIndex > 0 ) {
				float mapx = e.X * AScaleMid / AScale - StartX;
				float mapy = e.Y * AScaleMid / AScale - StartY;
				ScaleIndex--;
				AScale = ScaleList[ScaleIndex];
				StartX = e.X * AScaleMid / AScale - mapx;
				StartY = e.Y * AScaleMid / AScale - mapy;
			}
		}
		Invalidate();
	}
	
	//关闭
	public void Close()
	{
		
	}
}
}


