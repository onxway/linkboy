﻿
namespace n_MesForm
{
using System;
using System.Windows.Forms;
using n_Compiler;
using n_ErrorListBox;
using n_ET;
using n_MainSystemData;
using n_ParseNet;
using n_Config;
using n_TargetFile;
using T;

/// <summary>
/// Description of WorkForm.
/// </summary>
public partial class MesForm : Form
{
	//主窗口
	public MesForm()
	{
		//
		// The InitializeComponent() call is required for Windows Forms designer support.
		//
		InitializeComponent();
	}
	
	//显示
	public void Run()
	{
		Visible = true;
	}
	
	void FormFormClosing(object sender, FormClosingEventArgs e)
	{
		this.Visible = false;
		e.Cancel = true;
	}
	
	void LinkLabel1LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
	{
		System.Diagnostics.Process.Start("http://www.linkboy.cc");
	}
	
	int tick = 0;	
	void PictureBox1MouseDown(object sender, MouseEventArgs e)
	{
		tick++;
		
		if( tick >= 3 ) {
			if( T.ToolBox == null ) {
				T.ToolBox = new n_TToolForm.ToolForm();
				T.ControlInit();
			}
			T.ToolBox.Run();
		}
	}
}
}




