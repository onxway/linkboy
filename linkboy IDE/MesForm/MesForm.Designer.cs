﻿/*
 * 由 SharpDevelop 创建。
 * 用户: bbb_2
 * 日期: 2009-11-17
 * 时间: 11:15
 * 
 * 如果你想要更改该模板，那么请使用“工具 | 选项 | 编辑 | 编辑标准标题”。
 */
namespace n_TToolForm
{
	partial class ToolForm
	{
		/// <summary>
		/// Designer variable used to keep track of non-visual components.
		/// </summary>
		private System.ComponentModel.IContainer components = null;
		
		/// <summary>
		/// Disposes resources used by the form.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing) {
				if (components != null) {
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}
		
		/// <summary>
		/// This method is required for Windows Forms designer support.
		/// Do not change the method contents inside the source code editor. The Forms designer might
		/// not be able to load this method if it was changed manually.
		/// </summary>
		private void InitializeComponent()
		{
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ToolForm));
			this.button重载编译文件 = new System.Windows.Forms.Button();
			this.检查语法树button = new System.Windows.Forms.Button();
			this.button查看编译结果 = new System.Windows.Forms.Button();
			this.buttonISP下载器 = new System.Windows.Forms.Button();
			this.button系统设置 = new System.Windows.Forms.Button();
			this.button编辑器设置 = new System.Windows.Forms.Button();
			this.button2 = new System.Windows.Forms.Button();
			this.SuspendLayout();
			// 
			// button重载编译文件
			// 
			this.button重载编译文件.BackColor = System.Drawing.Color.RosyBrown;
			resources.ApplyResources(this.button重载编译文件, "button重载编译文件");
			this.button重载编译文件.ForeColor = System.Drawing.Color.White;
			this.button重载编译文件.Name = "button重载编译文件";
			this.button重载编译文件.UseVisualStyleBackColor = false;
			this.button重载编译文件.Click += new System.EventHandler(this.Button重载编译文件Click);
			this.button重载编译文件.MouseDown += new System.Windows.Forms.MouseEventHandler(this.Button重载编译文件MouseDown);
			// 
			// 检查语法树button
			// 
			this.检查语法树button.BackColor = System.Drawing.Color.RosyBrown;
			resources.ApplyResources(this.检查语法树button, "检查语法树button");
			this.检查语法树button.ForeColor = System.Drawing.Color.White;
			this.检查语法树button.Name = "检查语法树button";
			this.检查语法树button.UseVisualStyleBackColor = false;
			this.检查语法树button.Click += new System.EventHandler(this.检查语法树buttonClick);
			// 
			// button查看编译结果
			// 
			this.button查看编译结果.BackColor = System.Drawing.Color.RosyBrown;
			resources.ApplyResources(this.button查看编译结果, "button查看编译结果");
			this.button查看编译结果.ForeColor = System.Drawing.Color.White;
			this.button查看编译结果.Name = "button查看编译结果";
			this.button查看编译结果.UseVisualStyleBackColor = false;
			this.button查看编译结果.Click += new System.EventHandler(this.Button查看编译结果Click);
			// 
			// buttonISP下载器
			// 
			this.buttonISP下载器.BackColor = System.Drawing.Color.SteelBlue;
			resources.ApplyResources(this.buttonISP下载器, "buttonISP下载器");
			this.buttonISP下载器.ForeColor = System.Drawing.Color.White;
			this.buttonISP下载器.Name = "buttonISP下载器";
			this.buttonISP下载器.UseVisualStyleBackColor = false;
			this.buttonISP下载器.Click += new System.EventHandler(this.ButtonISP下载器Click);
			// 
			// button系统设置
			// 
			this.button系统设置.BackColor = System.Drawing.Color.DarkKhaki;
			resources.ApplyResources(this.button系统设置, "button系统设置");
			this.button系统设置.ForeColor = System.Drawing.Color.White;
			this.button系统设置.Name = "button系统设置";
			this.button系统设置.UseVisualStyleBackColor = false;
			this.button系统设置.Click += new System.EventHandler(this.Button系统设置Click);
			// 
			// button编辑器设置
			// 
			this.button编辑器设置.BackColor = System.Drawing.Color.DarkKhaki;
			resources.ApplyResources(this.button编辑器设置, "button编辑器设置");
			this.button编辑器设置.ForeColor = System.Drawing.Color.White;
			this.button编辑器设置.Name = "button编辑器设置";
			this.button编辑器设置.UseVisualStyleBackColor = false;
			this.button编辑器设置.Click += new System.EventHandler(this.Button编辑器设置Click);
			// 
			// button2
			// 
			this.button2.BackColor = System.Drawing.Color.RosyBrown;
			resources.ApplyResources(this.button2, "button2");
			this.button2.ForeColor = System.Drawing.Color.White;
			this.button2.Name = "button2";
			this.button2.UseVisualStyleBackColor = false;
			this.button2.Click += new System.EventHandler(this.Button2Click);
			// 
			// ToolForm
			// 
			resources.ApplyResources(this, "$this");
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.BackColor = System.Drawing.Color.WhiteSmoke;
			this.Controls.Add(this.button2);
			this.Controls.Add(this.button编辑器设置);
			this.Controls.Add(this.button系统设置);
			this.Controls.Add(this.buttonISP下载器);
			this.Controls.Add(this.button查看编译结果);
			this.Controls.Add(this.检查语法树button);
			this.Controls.Add(this.button重载编译文件);
			this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
			this.MaximizeBox = false;
			this.MinimizeBox = false;
			this.Name = "ToolForm";
			this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.SetIDEFormFormClosing);
			this.ResumeLayout(false);
		}
		private System.Windows.Forms.Button button2;
		private System.Windows.Forms.Button button编辑器设置;
		private System.Windows.Forms.Button button系统设置;
		private System.Windows.Forms.Button button重载编译文件;
		private System.Windows.Forms.Button buttonISP下载器;
		private System.Windows.Forms.Button button查看编译结果;
		private System.Windows.Forms.Button 检查语法树button;
	}
}
