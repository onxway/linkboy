﻿/*
 * 由 SharpDevelop 创建。
 * 用户: bbb_2
 * 日期: 2009-11-17
 * 时间: 11:15
 * 
 * 如果你想要更改该模板，那么请使用“工具 | 选项 | 编辑 | 编辑标准标题”。
 */
namespace n_VisualForm
{
	partial class VisualForm
	{
		/// <summary>
		/// Designer variable used to keep track of non-visual components.
		/// </summary>
		private System.ComponentModel.IContainer components = null;
		
		/// <summary>
		/// Disposes resources used by the form.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing) {
				if (components != null) {
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}
		
		/// <summary>
		/// This method is required for Windows Forms designer support.
		/// Do not change the method contents inside the source code editor. The Forms designer might
		/// not be able to load this method if it was changed manually.
		/// </summary>
		private void InitializeComponent()
		{
			this.components = new System.ComponentModel.Container();
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(VisualForm));
			this.contextMenuStrip1 = new System.Windows.Forms.ContextMenuStrip(this.components);
			this.ToolStripMenuItem查找和替换 = new System.Windows.Forms.ToolStripMenuItem();
			this.ToolStripMenuItem代码注释 = new System.Windows.Forms.ToolStripMenuItem();
			this.ToolStripMenuItem对齐文字 = new System.Windows.Forms.ToolStripMenuItem();
			this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
			this.ToolStripMenuItem复制 = new System.Windows.Forms.ToolStripMenuItem();
			this.ToolStripMenuItem剪切 = new System.Windows.Forms.ToolStripMenuItem();
			this.ToolStripMenuItem粘贴 = new System.Windows.Forms.ToolStripMenuItem();
			this.ToolStripMenuItem全选 = new System.Windows.Forms.ToolStripMenuItem();
			this.toolStripSeparator2 = new System.Windows.Forms.ToolStripSeparator();
			this.ToolStripMenuItem撤销操作 = new System.Windows.Forms.ToolStripMenuItem();
			this.ToolStripMenuItem重复操作 = new System.Windows.Forms.ToolStripMenuItem();
			this.toolStripSeparator3 = new System.Windows.Forms.ToolStripSeparator();
			this.打开所在目录ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
			this.保存文件ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
			this.contextMenuStrip1.SuspendLayout();
			this.SuspendLayout();
			// 
			// contextMenuStrip1
			// 
			this.contextMenuStrip1.BackColor = System.Drawing.Color.White;
			this.contextMenuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
									this.ToolStripMenuItem查找和替换,
									this.ToolStripMenuItem代码注释,
									this.ToolStripMenuItem对齐文字,
									this.toolStripSeparator1,
									this.ToolStripMenuItem复制,
									this.ToolStripMenuItem剪切,
									this.ToolStripMenuItem粘贴,
									this.ToolStripMenuItem全选,
									this.toolStripSeparator2,
									this.ToolStripMenuItem撤销操作,
									this.ToolStripMenuItem重复操作,
									this.toolStripSeparator3,
									this.打开所在目录ToolStripMenuItem,
									this.保存文件ToolStripMenuItem});
			this.contextMenuStrip1.Name = "contextMenuStrip1";
			resources.ApplyResources(this.contextMenuStrip1, "contextMenuStrip1");
			// 
			// ToolStripMenuItem查找和替换
			// 
			resources.ApplyResources(this.ToolStripMenuItem查找和替换, "ToolStripMenuItem查找和替换");
			this.ToolStripMenuItem查找和替换.Name = "ToolStripMenuItem查找和替换";
			this.ToolStripMenuItem查找和替换.Click += new System.EventHandler(this.ToolStripMenuItem查找和替换Click);
			// 
			// ToolStripMenuItem代码注释
			// 
			this.ToolStripMenuItem代码注释.BackColor = System.Drawing.Color.White;
			resources.ApplyResources(this.ToolStripMenuItem代码注释, "ToolStripMenuItem代码注释");
			this.ToolStripMenuItem代码注释.Name = "ToolStripMenuItem代码注释";
			this.ToolStripMenuItem代码注释.Click += new System.EventHandler(this.ToolStripMenuItem代码注释Click);
			// 
			// ToolStripMenuItem对齐文字
			// 
			this.ToolStripMenuItem对齐文字.BackColor = System.Drawing.Color.White;
			resources.ApplyResources(this.ToolStripMenuItem对齐文字, "ToolStripMenuItem对齐文字");
			this.ToolStripMenuItem对齐文字.Name = "ToolStripMenuItem对齐文字";
			this.ToolStripMenuItem对齐文字.Click += new System.EventHandler(this.ToolStripMenuItem对齐文字Click);
			// 
			// toolStripSeparator1
			// 
			this.toolStripSeparator1.Name = "toolStripSeparator1";
			resources.ApplyResources(this.toolStripSeparator1, "toolStripSeparator1");
			// 
			// ToolStripMenuItem复制
			// 
			this.ToolStripMenuItem复制.BackColor = System.Drawing.Color.White;
			this.ToolStripMenuItem复制.Name = "ToolStripMenuItem复制";
			resources.ApplyResources(this.ToolStripMenuItem复制, "ToolStripMenuItem复制");
			this.ToolStripMenuItem复制.Click += new System.EventHandler(this.ToolStripMenuItem复制Click);
			// 
			// ToolStripMenuItem剪切
			// 
			this.ToolStripMenuItem剪切.BackColor = System.Drawing.Color.White;
			this.ToolStripMenuItem剪切.Name = "ToolStripMenuItem剪切";
			resources.ApplyResources(this.ToolStripMenuItem剪切, "ToolStripMenuItem剪切");
			this.ToolStripMenuItem剪切.Click += new System.EventHandler(this.ToolStripMenuItem剪切Click);
			// 
			// ToolStripMenuItem粘贴
			// 
			this.ToolStripMenuItem粘贴.BackColor = System.Drawing.Color.White;
			this.ToolStripMenuItem粘贴.Name = "ToolStripMenuItem粘贴";
			resources.ApplyResources(this.ToolStripMenuItem粘贴, "ToolStripMenuItem粘贴");
			this.ToolStripMenuItem粘贴.Click += new System.EventHandler(this.ToolStripMenuItem粘贴Click);
			// 
			// ToolStripMenuItem全选
			// 
			this.ToolStripMenuItem全选.BackColor = System.Drawing.Color.White;
			this.ToolStripMenuItem全选.Name = "ToolStripMenuItem全选";
			resources.ApplyResources(this.ToolStripMenuItem全选, "ToolStripMenuItem全选");
			this.ToolStripMenuItem全选.Click += new System.EventHandler(this.ToolStripMenuItem全选Click);
			// 
			// toolStripSeparator2
			// 
			this.toolStripSeparator2.Name = "toolStripSeparator2";
			resources.ApplyResources(this.toolStripSeparator2, "toolStripSeparator2");
			// 
			// ToolStripMenuItem撤销操作
			// 
			this.ToolStripMenuItem撤销操作.BackColor = System.Drawing.Color.White;
			resources.ApplyResources(this.ToolStripMenuItem撤销操作, "ToolStripMenuItem撤销操作");
			this.ToolStripMenuItem撤销操作.Name = "ToolStripMenuItem撤销操作";
			this.ToolStripMenuItem撤销操作.Click += new System.EventHandler(this.ToolStripMenuItem撤销操作Click);
			// 
			// ToolStripMenuItem重复操作
			// 
			this.ToolStripMenuItem重复操作.BackColor = System.Drawing.Color.White;
			resources.ApplyResources(this.ToolStripMenuItem重复操作, "ToolStripMenuItem重复操作");
			this.ToolStripMenuItem重复操作.Name = "ToolStripMenuItem重复操作";
			this.ToolStripMenuItem重复操作.Click += new System.EventHandler(this.ToolStripMenuItem重复操作Click);
			// 
			// toolStripSeparator3
			// 
			this.toolStripSeparator3.Name = "toolStripSeparator3";
			resources.ApplyResources(this.toolStripSeparator3, "toolStripSeparator3");
			// 
			// 打开所在目录ToolStripMenuItem
			// 
			this.打开所在目录ToolStripMenuItem.Name = "打开所在目录ToolStripMenuItem";
			resources.ApplyResources(this.打开所在目录ToolStripMenuItem, "打开所在目录ToolStripMenuItem");
			this.打开所在目录ToolStripMenuItem.Click += new System.EventHandler(this.打开所在目录ToolStripMenuItemClick);
			// 
			// 保存文件ToolStripMenuItem
			// 
			this.保存文件ToolStripMenuItem.Name = "保存文件ToolStripMenuItem";
			resources.ApplyResources(this.保存文件ToolStripMenuItem, "保存文件ToolStripMenuItem");
			this.保存文件ToolStripMenuItem.Click += new System.EventHandler(this.保存文件ToolStripMenuItemClick);
			// 
			// VisualForm
			// 
			resources.ApplyResources(this, "$this");
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.BackColor = System.Drawing.Color.White;
			this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.SizableToolWindow;
			this.Name = "VisualForm";
			this.contextMenuStrip1.ResumeLayout(false);
			this.ResumeLayout(false);
		}
		private System.Windows.Forms.ToolStripMenuItem 保存文件ToolStripMenuItem;
		private System.Windows.Forms.ToolStripMenuItem 打开所在目录ToolStripMenuItem;
		private System.Windows.Forms.ToolStripSeparator toolStripSeparator3;
		private System.Windows.Forms.ToolStripMenuItem ToolStripMenuItem重复操作;
		private System.Windows.Forms.ToolStripMenuItem ToolStripMenuItem撤销操作;
		private System.Windows.Forms.ToolStripSeparator toolStripSeparator2;
		private System.Windows.Forms.ToolStripMenuItem ToolStripMenuItem全选;
		private System.Windows.Forms.ToolStripMenuItem ToolStripMenuItem粘贴;
		private System.Windows.Forms.ToolStripMenuItem ToolStripMenuItem剪切;
		private System.Windows.Forms.ToolStripMenuItem ToolStripMenuItem复制;
		private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
		private System.Windows.Forms.ToolStripMenuItem ToolStripMenuItem对齐文字;
		private System.Windows.Forms.ToolStripMenuItem ToolStripMenuItem代码注释;
		private System.Windows.Forms.ToolStripMenuItem ToolStripMenuItem查找和替换;
		private System.Windows.Forms.ContextMenuStrip contextMenuStrip1;
	}
}
