﻿
namespace n_VisualForm
{
using System;
using System.Windows.Forms;
using T;

/// <summary>
/// Description of WorkForm.
/// </summary>
public partial class VisualForm : Form
{
	public ContextMenuStrip contextM;
	
	//主窗口
	public VisualForm()
	{
		InitializeComponent();
		
		contextM = this.contextMenuStrip1;
		
		if( n_OS.OS.isGForm ) {
			this.contextMenuStrip1.Items.Remove( 保存文件ToolStripMenuItem );
			
		}
	}
	
	//右键菜单
	
	void ToolStripMenuItem查找和替换Click(object sender, EventArgs e)
	{
		if( G.FindBox == null ) {
			G.FindBox = new n_FindForm.FindForm();
		}
		G.FindBox.Run();
	}
	
	void ToolStripMenuItem代码注释Click(object sender, EventArgs e)
	{
		if( T.ctext == null ) {
			A.CodeErrBox.Run( "无法注释, 因为现在没有打开的文件" );
			return;
		}
		T.ctext.TurnNoteAndCode();
	}
	
	void ToolStripMenuItem对齐文字Click(object sender, EventArgs e)
	{
		if( T.ctext == null ) {
			A.CodeErrBox.Run( "无法对齐, 因为现在没有打开的文件" );
			return;
		}
		T.ctext.FormatCode();
	}
	
	void ToolStripMenuItem复制Click(object sender, EventArgs e)
	{
		if( T.ctext == null ) {
			A.CodeErrBox.Run( "无法复制, 因为现在没有打开的文件" );
			return;
		}
		T.ctext.SCopy();
	}
	
	void ToolStripMenuItem剪切Click(object sender, EventArgs e)
	{
		if( T.ctext == null ) {
			A.CodeErrBox.Run( "无法剪切, 因为现在没有打开的文件" );
			return;
		}
		T.ctext.SCut();
	}
	
	void ToolStripMenuItem粘贴Click(object sender, EventArgs e)
	{
		if( T.ctext == null ) {
			A.CodeErrBox.Run( "无法粘贴, 因为现在没有打开的文件" );
			return;
		}
		T.ctext.SPaste();
	}
	
	void ToolStripMenuItem全选Click(object sender, EventArgs e)
	{
		if( T.ctext == null ) {
			A.CodeErrBox.Run( "无法全选, 因为现在没有打开的文件" );
			return;
		}
		T.ctext.SSelectAll();
	}
	
	void ToolStripMenuItem撤销操作Click(object sender, EventArgs e)
	{
		if( T.ctext == null ) {
			A.CodeErrBox.Run( "无法撤销, 因为现在没有打开的文件" );
			return;
		}
		T.ctext.Undone();
	}
	
	void ToolStripMenuItem重复操作Click(object sender, EventArgs e)
	{
		if( T.ctext == null ) {
			A.CodeErrBox.Run( "无法重复, 因为现在没有打开的文件" );
			return;
		}
		T.ctext.Redone();
	}
	
	void 打开所在目录ToolStripMenuItemClick(object sender, EventArgs e)
	{
		System.Diagnostics.Process.Start( G.ccode.FilePath );
	}
	
	void 保存文件ToolStripMenuItemClick(object sender, EventArgs e)
	{
		if( T.ctext == null ) {
			A.CodeErrBox.Run( "无法保存, 因为现在没有打开的文件" );
			return;
		}
		G.ccode.SetCode( T.ctext.Text );
		G.ccode.Save();
	}
}
}



