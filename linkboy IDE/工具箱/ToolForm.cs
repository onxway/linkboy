﻿
namespace n_TToolForm
{
using System;
using System.Windows.Forms;
using n_Compiler;
using n_ErrorListBox;
using n_ET;
using n_MainSystemData;
using n_ParseNet;
using n_Config;
using n_TargetFile;
using T;

/// <summary>
/// Description of WorkForm.
/// </summary>
public partial class ToolForm : Form
{
	//主窗口
	public ToolForm()
	{
		//
		// The InitializeComponent() call is required for Windows Forms designer support.
		//
		InitializeComponent();
		this.MinimumSize = this.Size;
		this.MaximumSize = this.Size;
		//初始化
		//
		// TODO: Add constructor code after the InitializeComponent() call.
		//
	}
	
	//显示
	public void Run()
	{
		this.Show();
		this.Activate();
	}
	
	void SetIDEFormFormClosing(object sender, FormClosingEventArgs e)
	{
		this.Visible = false;
		e.Cancel = true;
	}
	
	void Button重载编译文件Click(object sender, EventArgs e)
	{
		Compiler.FileLoadInit();
	}
	
	void 检查语法树buttonClick(object sender, EventArgs e)
	{
		ErrorListBox.Clear();
		ParseNet.Check();
		if( !ET.isErrors() ) {
			MessageBox.Show( "没有错误" );
		}
		else {
			//T.ErrorBox.Visible = true;
			ErrorListBox.ShowError( ET.Show().Split( '\n' ) );
		}
	}
	
	void 重新运行汇编器(object sender, EventArgs e)
	{
//		//清空信息
//		OutputBox.Clear();
//		ET.Clear();
//		
//		string Result = Compiler.Assemble( ASMBox.GetText(), 0 );
//		
//		if( ET.isErrors() ) {
//			ErrorListBox.ShowError( ET.Show().Split( '\n' ) );
//		}
//		else {
//			//保存文件
//			VIO.SaveTextFile( A.WorkBox.ctext.PathAndName.Remove( A.WorkBox.ctext.PathAndName.LastIndexOf( "." ) ) + ".hex", Result );
//		}
//		string Path = A.WorkBox.ctext.PathAndName.Remove( A.WorkBox.ctext.PathAndName.LastIndexOf( "." ) ) + ".asm";
//		MessageBox.Show( "保存汇编文件到" + Path );
//		VIO.SaveTextFile( Path, ASMBox.GetText() );
	}
	
	void Button查看编译结果Click(object sender, EventArgs e)
	{
		this.Visible = false;
		if( G.CompileMessageBox == null ) {
			G.CompileMessageBox = new n_CompileMessageForm.CompileMessageForm();
		}
		G.CompileMessageBox.Run();
	}
	
	void ButtonISP下载器Click(object sender, EventArgs e)
	{
		this.Visible = false;
		//T.ISPBox.LoadHexFile( TargetFile.GetTargetFilePath() );
		//T.ISPBox.SetChipType( Config.GetCPU() );
		T.ISPBox.Run();
	}
	
	void Button系统设置Click(object sender, EventArgs e)
	{
		this.Visible = false;
		A.SetIDEBox.Run();
	}
	
	void Button编辑器设置Click(object sender, EventArgs e)
	{
		this.Visible = false;
		if( T.SetTextBox == null ) {
			T.SetTextBox = new n_SetEditorForm.SetEditorForm();
		}
		T.SetTextBox.Run();
	}
	
	void Button2Click(object sender, EventArgs e)
	{
		ErrorListBox.Clear();
		n_PyParseNet.ParseNet.Check();
		
		if( !n_PyET.ET.isErrors() ) {
			MessageBox.Show( "没有错误" );
		}
		else {
			//T.ErrorBox.Visible = true;
			ErrorListBox.ShowError( n_PyET.ET.Show().Split( '\n' ) );
		}
	}
	
	int tick = 0;
	void Button重载编译文件MouseDown(object sender, MouseEventArgs e)
	{
		if( e.Button == MouseButtons.Right ) {
			tick++;
			if( tick > 10 ) {
				button2.Visible = true;
				button查看编译结果.Visible = true;
				检查语法树button.Visible = true;
			}
		}
	}
}
}




