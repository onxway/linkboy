﻿
namespace n_SetEditorForm
{
using System;
using System.Windows.Forms;
using n_MainSystemData;
//using n_ctext;

/// <summary>
/// Description of WorkForm.
/// </summary>
public partial class SetEditorForm : Form
{
	//static CTextBox ctext;
	
	//主窗口
	public SetEditorForm()
	{
		//
		// The InitializeComponent() call is required for Windows Forms designer support.
		//
		InitializeComponent();
		this.MinimumSize = this.Size;
		this.MaximumSize = this.Size;
		
		//装载示例程序
		//ctext = new CTextBox( OS.SystemRoot + "Resource" + OS.PATH_S + "示例文本.txt" );
		//ctext = new CTextBox( "哈哈哈" );
		//ctext.isNew = false;
		//
		// TODO: Add constructor code after the InitializeComponent() call.
		//
	}
	
	//运行
	public void Run()
	{
		RefreshSampleText();
		this.Show();
	}
	
	//刷新
	void RefreshSampleText()
	{
		//Drawer.RefreshColor();
		
		this.b0.BackColor = SystemData.NoteColor;
		this.b1.BackColor = SystemData.CharColor;
		this.b2.BackColor = SystemData.StringColor;
		this.b3.BackColor = SystemData.OperColor;
		this.b4.BackColor = SystemData.NumberColor;
		this.b5.BackColor = SystemData.MemberValueColor;
		this.b6.BackColor = SystemData.FunctionTypeColor;
		this.b7.BackColor = SystemData.InsetConstColor;
		this.b8.BackColor = SystemData.MemberTypeColor;
		this.b9.BackColor = SystemData.VarTypeColor;
		this.b10.BackColor = SystemData.FlowColor;
		this.b11.BackColor = SystemData.OtherColor;
		this.b12.BackColor = SystemData.IdentColor;
		this.b13.BackColor = SystemData.SplitColor;
		this.b14.BackColor = SystemData.TextBackColor;
		this.b15.BackColor = SystemData.SysInsColor;
		
		this.richTextBox1.BackColor = SystemData.TextBackColor;
		//ctext.Font = SystemData.SourceTextFont;
		
		//ctext.DrawAllArea();
		//this.richTextBox1.Rtf = ctext.Rtf;
	}
	
	//确定按钮
	void YesButtonClick(object sender, EventArgs e)
	{
		SystemData.Save();
		this.Visible = false;
	}
	
	//选择字体
	void Button1Click(object sender, EventArgs e)
	{
		FontDialog fontBox = new FontDialog();
		fontBox.Font = SystemData.SourceTextFont;
		DialogResult d = fontBox.ShowDialog();
		if( d == DialogResult.OK ) {
			SystemData.SourceTextFont = fontBox.Font;
			//ctext.Font = fontBox.Font;
		}
		RefreshSampleText();
	}
	
	//选择颜色对话框
	void ColorButtonClick(object sender, EventArgs e)
	{
		ColorDialog c = new ColorDialog();
		if( c.ShowDialog() == DialogResult.OK ) {
			switch( ( (Button)sender ).Name ) {
					case "b0": SystemData.NoteColor = c.Color; break;
					case "b1": SystemData.CharColor = c.Color; break;
					case "b2": SystemData.StringColor = c.Color; break;
					case "b3": SystemData.OperColor = c.Color; break;
					case "b4": SystemData.NumberColor = c.Color; break;
					case "b5": SystemData.MemberValueColor = c.Color; break;
					case "b6": SystemData.FunctionTypeColor = c.Color; break;
					case "b7": SystemData.InsetConstColor = c.Color; break;
					case "b8": SystemData.MemberTypeColor = c.Color; break;
					case "b9": SystemData.VarTypeColor = c.Color; break;
					case "b10": SystemData.FlowColor = c.Color; break;
					case "b11": SystemData.OtherColor = c.Color; break;
					case "b12": SystemData.IdentColor = c.Color; break;
					case "b13": SystemData.SplitColor = c.Color; break;
					case "b14": SystemData.TextBackColor = c.Color; break;
					case "b15": SystemData.SysInsColor = c.Color; break;
					default:   break;
			}
		}
		RefreshSampleText();
	}
	
	void SetEditorFormFormClosing(object sender, FormClosingEventArgs e)
	{
		SystemData.Load();
		this.Visible = false;
		e.Cancel = true;
	}
}
}
