﻿/*
 * 由 SharpDevelop 创建。
 * 用户: bbb_2
 * 日期: 2009-11-17
 * 时间: 11:15
 * 
 * 如果你想要更改该模板，那么请使用“工具 | 选项 | 编辑 | 编辑标准标题”。
 */
namespace n_SetEditorForm
{
	partial class SetEditorForm
	{
		/// <summary>
		/// Designer variable used to keep track of non-visual components.
		/// </summary>
		private System.ComponentModel.IContainer components = null;
		
		/// <summary>
		/// Disposes resources used by the form.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing) {
				if (components != null) {
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}
		
		/// <summary>
		/// This method is required for Windows Forms designer support.
		/// Do not change the method contents inside the source code editor. The Forms designer might
		/// not be able to load this method if it was changed manually.
		/// </summary>
		private void InitializeComponent()
		{
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(SetEditorForm));
			this.button1 = new System.Windows.Forms.Button();
			this.YesButton = new System.Windows.Forms.Button();
			this.richTextBox1 = new System.Windows.Forms.RichTextBox();
			this.label3 = new System.Windows.Forms.Label();
			this.label1 = new System.Windows.Forms.Label();
			this.label2 = new System.Windows.Forms.Label();
			this.label5 = new System.Windows.Forms.Label();
			this.label6 = new System.Windows.Forms.Label();
			this.label7 = new System.Windows.Forms.Label();
			this.label8 = new System.Windows.Forms.Label();
			this.label9 = new System.Windows.Forms.Label();
			this.label10 = new System.Windows.Forms.Label();
			this.b0 = new System.Windows.Forms.Button();
			this.b1 = new System.Windows.Forms.Button();
			this.b2 = new System.Windows.Forms.Button();
			this.b3 = new System.Windows.Forms.Button();
			this.b4 = new System.Windows.Forms.Button();
			this.b5 = new System.Windows.Forms.Button();
			this.b6 = new System.Windows.Forms.Button();
			this.label4 = new System.Windows.Forms.Label();
			this.b7 = new System.Windows.Forms.Button();
			this.b8 = new System.Windows.Forms.Button();
			this.label11 = new System.Windows.Forms.Label();
			this.b9 = new System.Windows.Forms.Button();
			this.label12 = new System.Windows.Forms.Label();
			this.label13 = new System.Windows.Forms.Label();
			this.b10 = new System.Windows.Forms.Button();
			this.b11 = new System.Windows.Forms.Button();
			this.label14 = new System.Windows.Forms.Label();
			this.b12 = new System.Windows.Forms.Button();
			this.label15 = new System.Windows.Forms.Label();
			this.label16 = new System.Windows.Forms.Label();
			this.b13 = new System.Windows.Forms.Button();
			this.label17 = new System.Windows.Forms.Label();
			this.b14 = new System.Windows.Forms.Button();
			this.label18 = new System.Windows.Forms.Label();
			this.b15 = new System.Windows.Forms.Button();
			this.SuspendLayout();
			// 
			// button1
			// 
			this.button1.AccessibleDescription = null;
			this.button1.AccessibleName = null;
			this.button1.Anchor = ((System.Windows.Forms.AnchorStyles)(resources.GetObject("button1.Anchor")));
			this.button1.AutoSize = ((bool)(resources.GetObject("button1.AutoSize")));
			this.button1.AutoSizeMode = ((System.Windows.Forms.AutoSizeMode)(resources.GetObject("button1.AutoSizeMode")));
			this.button1.BackgroundImage = null;
			this.button1.BackgroundImageLayout = ((System.Windows.Forms.ImageLayout)(resources.GetObject("button1.BackgroundImageLayout")));
			this.button1.Dock = ((System.Windows.Forms.DockStyle)(resources.GetObject("button1.Dock")));
			this.button1.FlatStyle = ((System.Windows.Forms.FlatStyle)(resources.GetObject("button1.FlatStyle")));
			this.button1.Font = null;
			this.button1.ImageAlign = ((System.Drawing.ContentAlignment)(resources.GetObject("button1.ImageAlign")));
			this.button1.ImageIndex = ((int)(resources.GetObject("button1.ImageIndex")));
			this.button1.ImageKey = resources.GetString("button1.ImageKey");
			this.button1.ImeMode = ((System.Windows.Forms.ImeMode)(resources.GetObject("button1.ImeMode")));
			this.button1.Location = ((System.Drawing.Point)(resources.GetObject("button1.Location")));
			this.button1.Name = "button1";
			this.button1.RightToLeft = ((System.Windows.Forms.RightToLeft)(resources.GetObject("button1.RightToLeft")));
			this.button1.Size = ((System.Drawing.Size)(resources.GetObject("button1.Size")));
			this.button1.TabIndex = ((int)(resources.GetObject("button1.TabIndex")));
			this.button1.Text = resources.GetString("button1.Text");
			this.button1.TextAlign = ((System.Drawing.ContentAlignment)(resources.GetObject("button1.TextAlign")));
			this.button1.TextImageRelation = ((System.Windows.Forms.TextImageRelation)(resources.GetObject("button1.TextImageRelation")));
			this.button1.UseVisualStyleBackColor = true;
			this.button1.Click += new System.EventHandler(this.Button1Click);
			// 
			// YesButton
			// 
			this.YesButton.AccessibleDescription = null;
			this.YesButton.AccessibleName = null;
			this.YesButton.Anchor = ((System.Windows.Forms.AnchorStyles)(resources.GetObject("YesButton.Anchor")));
			this.YesButton.AutoSize = ((bool)(resources.GetObject("YesButton.AutoSize")));
			this.YesButton.AutoSizeMode = ((System.Windows.Forms.AutoSizeMode)(resources.GetObject("YesButton.AutoSizeMode")));
			this.YesButton.BackgroundImage = null;
			this.YesButton.BackgroundImageLayout = ((System.Windows.Forms.ImageLayout)(resources.GetObject("YesButton.BackgroundImageLayout")));
			this.YesButton.Dock = ((System.Windows.Forms.DockStyle)(resources.GetObject("YesButton.Dock")));
			this.YesButton.FlatStyle = ((System.Windows.Forms.FlatStyle)(resources.GetObject("YesButton.FlatStyle")));
			this.YesButton.Font = null;
			this.YesButton.ImageAlign = ((System.Drawing.ContentAlignment)(resources.GetObject("YesButton.ImageAlign")));
			this.YesButton.ImageIndex = ((int)(resources.GetObject("YesButton.ImageIndex")));
			this.YesButton.ImageKey = resources.GetString("YesButton.ImageKey");
			this.YesButton.ImeMode = ((System.Windows.Forms.ImeMode)(resources.GetObject("YesButton.ImeMode")));
			this.YesButton.Location = ((System.Drawing.Point)(resources.GetObject("YesButton.Location")));
			this.YesButton.Name = "YesButton";
			this.YesButton.RightToLeft = ((System.Windows.Forms.RightToLeft)(resources.GetObject("YesButton.RightToLeft")));
			this.YesButton.Size = ((System.Drawing.Size)(resources.GetObject("YesButton.Size")));
			this.YesButton.TabIndex = ((int)(resources.GetObject("YesButton.TabIndex")));
			this.YesButton.Text = resources.GetString("YesButton.Text");
			this.YesButton.TextAlign = ((System.Drawing.ContentAlignment)(resources.GetObject("YesButton.TextAlign")));
			this.YesButton.TextImageRelation = ((System.Windows.Forms.TextImageRelation)(resources.GetObject("YesButton.TextImageRelation")));
			this.YesButton.UseVisualStyleBackColor = true;
			this.YesButton.Click += new System.EventHandler(this.YesButtonClick);
			// 
			// richTextBox1
			// 
			this.richTextBox1.AccessibleDescription = null;
			this.richTextBox1.AccessibleName = null;
			this.richTextBox1.Anchor = ((System.Windows.Forms.AnchorStyles)(resources.GetObject("richTextBox1.Anchor")));
			this.richTextBox1.AutoSize = ((bool)(resources.GetObject("richTextBox1.AutoSize")));
			this.richTextBox1.BackColor = System.Drawing.Color.White;
			this.richTextBox1.BackgroundImage = null;
			this.richTextBox1.BackgroundImageLayout = ((System.Windows.Forms.ImageLayout)(resources.GetObject("richTextBox1.BackgroundImageLayout")));
			this.richTextBox1.BulletIndent = ((int)(resources.GetObject("richTextBox1.BulletIndent")));
			this.richTextBox1.Dock = ((System.Windows.Forms.DockStyle)(resources.GetObject("richTextBox1.Dock")));
			this.richTextBox1.Font = null;
			this.richTextBox1.ImeMode = ((System.Windows.Forms.ImeMode)(resources.GetObject("richTextBox1.ImeMode")));
			this.richTextBox1.Location = ((System.Drawing.Point)(resources.GetObject("richTextBox1.Location")));
			this.richTextBox1.MaxLength = ((int)(resources.GetObject("richTextBox1.MaxLength")));
			this.richTextBox1.Multiline = ((bool)(resources.GetObject("richTextBox1.Multiline")));
			this.richTextBox1.Name = "richTextBox1";
			this.richTextBox1.ReadOnly = true;
			this.richTextBox1.RightMargin = ((int)(resources.GetObject("richTextBox1.RightMargin")));
			this.richTextBox1.RightToLeft = ((System.Windows.Forms.RightToLeft)(resources.GetObject("richTextBox1.RightToLeft")));
			this.richTextBox1.ScrollBars = ((System.Windows.Forms.RichTextBoxScrollBars)(resources.GetObject("richTextBox1.ScrollBars")));
			this.richTextBox1.Size = ((System.Drawing.Size)(resources.GetObject("richTextBox1.Size")));
			this.richTextBox1.TabIndex = ((int)(resources.GetObject("richTextBox1.TabIndex")));
			this.richTextBox1.Text = resources.GetString("richTextBox1.Text");
			this.richTextBox1.WordWrap = ((bool)(resources.GetObject("richTextBox1.WordWrap")));
			this.richTextBox1.ZoomFactor = ((float)(resources.GetObject("richTextBox1.ZoomFactor")));
			// 
			// label3
			// 
			this.label3.AccessibleDescription = null;
			this.label3.AccessibleName = null;
			this.label3.Anchor = ((System.Windows.Forms.AnchorStyles)(resources.GetObject("label3.Anchor")));
			this.label3.AutoSize = ((bool)(resources.GetObject("label3.AutoSize")));
			this.label3.BackgroundImageLayout = ((System.Windows.Forms.ImageLayout)(resources.GetObject("label3.BackgroundImageLayout")));
			this.label3.Dock = ((System.Windows.Forms.DockStyle)(resources.GetObject("label3.Dock")));
			this.label3.Font = ((System.Drawing.Font)(resources.GetObject("label3.Font")));
			this.label3.ForeColor = System.Drawing.Color.Chocolate;
			this.label3.ImageAlign = ((System.Drawing.ContentAlignment)(resources.GetObject("label3.ImageAlign")));
			this.label3.ImageIndex = ((int)(resources.GetObject("label3.ImageIndex")));
			this.label3.ImageKey = resources.GetString("label3.ImageKey");
			this.label3.ImeMode = ((System.Windows.Forms.ImeMode)(resources.GetObject("label3.ImeMode")));
			this.label3.Location = ((System.Drawing.Point)(resources.GetObject("label3.Location")));
			this.label3.Name = "label3";
			this.label3.RightToLeft = ((System.Windows.Forms.RightToLeft)(resources.GetObject("label3.RightToLeft")));
			this.label3.Size = ((System.Drawing.Size)(resources.GetObject("label3.Size")));
			this.label3.TabIndex = ((int)(resources.GetObject("label3.TabIndex")));
			this.label3.Text = resources.GetString("label3.Text");
			this.label3.TextAlign = ((System.Drawing.ContentAlignment)(resources.GetObject("label3.TextAlign")));
			// 
			// label1
			// 
			this.label1.AccessibleDescription = null;
			this.label1.AccessibleName = null;
			this.label1.Anchor = ((System.Windows.Forms.AnchorStyles)(resources.GetObject("label1.Anchor")));
			this.label1.AutoSize = ((bool)(resources.GetObject("label1.AutoSize")));
			this.label1.BackgroundImageLayout = ((System.Windows.Forms.ImageLayout)(resources.GetObject("label1.BackgroundImageLayout")));
			this.label1.Dock = ((System.Windows.Forms.DockStyle)(resources.GetObject("label1.Dock")));
			this.label1.Font = ((System.Drawing.Font)(resources.GetObject("label1.Font")));
			this.label1.ForeColor = System.Drawing.Color.SteelBlue;
			this.label1.ImageAlign = ((System.Drawing.ContentAlignment)(resources.GetObject("label1.ImageAlign")));
			this.label1.ImageIndex = ((int)(resources.GetObject("label1.ImageIndex")));
			this.label1.ImageKey = resources.GetString("label1.ImageKey");
			this.label1.ImeMode = ((System.Windows.Forms.ImeMode)(resources.GetObject("label1.ImeMode")));
			this.label1.Location = ((System.Drawing.Point)(resources.GetObject("label1.Location")));
			this.label1.Name = "label1";
			this.label1.RightToLeft = ((System.Windows.Forms.RightToLeft)(resources.GetObject("label1.RightToLeft")));
			this.label1.Size = ((System.Drawing.Size)(resources.GetObject("label1.Size")));
			this.label1.TabIndex = ((int)(resources.GetObject("label1.TabIndex")));
			this.label1.Text = resources.GetString("label1.Text");
			this.label1.TextAlign = ((System.Drawing.ContentAlignment)(resources.GetObject("label1.TextAlign")));
			// 
			// label2
			// 
			this.label2.AccessibleDescription = null;
			this.label2.AccessibleName = null;
			this.label2.Anchor = ((System.Windows.Forms.AnchorStyles)(resources.GetObject("label2.Anchor")));
			this.label2.AutoSize = ((bool)(resources.GetObject("label2.AutoSize")));
			this.label2.BackgroundImageLayout = ((System.Windows.Forms.ImageLayout)(resources.GetObject("label2.BackgroundImageLayout")));
			this.label2.Dock = ((System.Windows.Forms.DockStyle)(resources.GetObject("label2.Dock")));
			this.label2.Font = ((System.Drawing.Font)(resources.GetObject("label2.Font")));
			this.label2.ImageAlign = ((System.Drawing.ContentAlignment)(resources.GetObject("label2.ImageAlign")));
			this.label2.ImageIndex = ((int)(resources.GetObject("label2.ImageIndex")));
			this.label2.ImageKey = resources.GetString("label2.ImageKey");
			this.label2.ImeMode = ((System.Windows.Forms.ImeMode)(resources.GetObject("label2.ImeMode")));
			this.label2.Location = ((System.Drawing.Point)(resources.GetObject("label2.Location")));
			this.label2.Name = "label2";
			this.label2.RightToLeft = ((System.Windows.Forms.RightToLeft)(resources.GetObject("label2.RightToLeft")));
			this.label2.Size = ((System.Drawing.Size)(resources.GetObject("label2.Size")));
			this.label2.TabIndex = ((int)(resources.GetObject("label2.TabIndex")));
			this.label2.Text = resources.GetString("label2.Text");
			this.label2.TextAlign = ((System.Drawing.ContentAlignment)(resources.GetObject("label2.TextAlign")));
			// 
			// label5
			// 
			this.label5.AccessibleDescription = null;
			this.label5.AccessibleName = null;
			this.label5.Anchor = ((System.Windows.Forms.AnchorStyles)(resources.GetObject("label5.Anchor")));
			this.label5.AutoSize = ((bool)(resources.GetObject("label5.AutoSize")));
			this.label5.BackgroundImageLayout = ((System.Windows.Forms.ImageLayout)(resources.GetObject("label5.BackgroundImageLayout")));
			this.label5.Dock = ((System.Windows.Forms.DockStyle)(resources.GetObject("label5.Dock")));
			this.label5.Font = ((System.Drawing.Font)(resources.GetObject("label5.Font")));
			this.label5.ImageAlign = ((System.Drawing.ContentAlignment)(resources.GetObject("label5.ImageAlign")));
			this.label5.ImageIndex = ((int)(resources.GetObject("label5.ImageIndex")));
			this.label5.ImageKey = resources.GetString("label5.ImageKey");
			this.label5.ImeMode = ((System.Windows.Forms.ImeMode)(resources.GetObject("label5.ImeMode")));
			this.label5.Location = ((System.Drawing.Point)(resources.GetObject("label5.Location")));
			this.label5.Name = "label5";
			this.label5.RightToLeft = ((System.Windows.Forms.RightToLeft)(resources.GetObject("label5.RightToLeft")));
			this.label5.Size = ((System.Drawing.Size)(resources.GetObject("label5.Size")));
			this.label5.TabIndex = ((int)(resources.GetObject("label5.TabIndex")));
			this.label5.Text = resources.GetString("label5.Text");
			this.label5.TextAlign = ((System.Drawing.ContentAlignment)(resources.GetObject("label5.TextAlign")));
			// 
			// label6
			// 
			this.label6.AccessibleDescription = null;
			this.label6.AccessibleName = null;
			this.label6.Anchor = ((System.Windows.Forms.AnchorStyles)(resources.GetObject("label6.Anchor")));
			this.label6.AutoSize = ((bool)(resources.GetObject("label6.AutoSize")));
			this.label6.BackgroundImageLayout = ((System.Windows.Forms.ImageLayout)(resources.GetObject("label6.BackgroundImageLayout")));
			this.label6.Dock = ((System.Windows.Forms.DockStyle)(resources.GetObject("label6.Dock")));
			this.label6.Font = ((System.Drawing.Font)(resources.GetObject("label6.Font")));
			this.label6.ImageAlign = ((System.Drawing.ContentAlignment)(resources.GetObject("label6.ImageAlign")));
			this.label6.ImageIndex = ((int)(resources.GetObject("label6.ImageIndex")));
			this.label6.ImageKey = resources.GetString("label6.ImageKey");
			this.label6.ImeMode = ((System.Windows.Forms.ImeMode)(resources.GetObject("label6.ImeMode")));
			this.label6.Location = ((System.Drawing.Point)(resources.GetObject("label6.Location")));
			this.label6.Name = "label6";
			this.label6.RightToLeft = ((System.Windows.Forms.RightToLeft)(resources.GetObject("label6.RightToLeft")));
			this.label6.Size = ((System.Drawing.Size)(resources.GetObject("label6.Size")));
			this.label6.TabIndex = ((int)(resources.GetObject("label6.TabIndex")));
			this.label6.Text = resources.GetString("label6.Text");
			this.label6.TextAlign = ((System.Drawing.ContentAlignment)(resources.GetObject("label6.TextAlign")));
			// 
			// label7
			// 
			this.label7.AccessibleDescription = null;
			this.label7.AccessibleName = null;
			this.label7.Anchor = ((System.Windows.Forms.AnchorStyles)(resources.GetObject("label7.Anchor")));
			this.label7.AutoSize = ((bool)(resources.GetObject("label7.AutoSize")));
			this.label7.BackgroundImageLayout = ((System.Windows.Forms.ImageLayout)(resources.GetObject("label7.BackgroundImageLayout")));
			this.label7.Dock = ((System.Windows.Forms.DockStyle)(resources.GetObject("label7.Dock")));
			this.label7.Font = ((System.Drawing.Font)(resources.GetObject("label7.Font")));
			this.label7.ImageAlign = ((System.Drawing.ContentAlignment)(resources.GetObject("label7.ImageAlign")));
			this.label7.ImageIndex = ((int)(resources.GetObject("label7.ImageIndex")));
			this.label7.ImageKey = resources.GetString("label7.ImageKey");
			this.label7.ImeMode = ((System.Windows.Forms.ImeMode)(resources.GetObject("label7.ImeMode")));
			this.label7.Location = ((System.Drawing.Point)(resources.GetObject("label7.Location")));
			this.label7.Name = "label7";
			this.label7.RightToLeft = ((System.Windows.Forms.RightToLeft)(resources.GetObject("label7.RightToLeft")));
			this.label7.Size = ((System.Drawing.Size)(resources.GetObject("label7.Size")));
			this.label7.TabIndex = ((int)(resources.GetObject("label7.TabIndex")));
			this.label7.Text = resources.GetString("label7.Text");
			this.label7.TextAlign = ((System.Drawing.ContentAlignment)(resources.GetObject("label7.TextAlign")));
			// 
			// label8
			// 
			this.label8.AccessibleDescription = null;
			this.label8.AccessibleName = null;
			this.label8.Anchor = ((System.Windows.Forms.AnchorStyles)(resources.GetObject("label8.Anchor")));
			this.label8.AutoSize = ((bool)(resources.GetObject("label8.AutoSize")));
			this.label8.BackgroundImageLayout = ((System.Windows.Forms.ImageLayout)(resources.GetObject("label8.BackgroundImageLayout")));
			this.label8.Dock = ((System.Windows.Forms.DockStyle)(resources.GetObject("label8.Dock")));
			this.label8.Font = ((System.Drawing.Font)(resources.GetObject("label8.Font")));
			this.label8.ImageAlign = ((System.Drawing.ContentAlignment)(resources.GetObject("label8.ImageAlign")));
			this.label8.ImageIndex = ((int)(resources.GetObject("label8.ImageIndex")));
			this.label8.ImageKey = resources.GetString("label8.ImageKey");
			this.label8.ImeMode = ((System.Windows.Forms.ImeMode)(resources.GetObject("label8.ImeMode")));
			this.label8.Location = ((System.Drawing.Point)(resources.GetObject("label8.Location")));
			this.label8.Name = "label8";
			this.label8.RightToLeft = ((System.Windows.Forms.RightToLeft)(resources.GetObject("label8.RightToLeft")));
			this.label8.Size = ((System.Drawing.Size)(resources.GetObject("label8.Size")));
			this.label8.TabIndex = ((int)(resources.GetObject("label8.TabIndex")));
			this.label8.Text = resources.GetString("label8.Text");
			this.label8.TextAlign = ((System.Drawing.ContentAlignment)(resources.GetObject("label8.TextAlign")));
			// 
			// label9
			// 
			this.label9.AccessibleDescription = null;
			this.label9.AccessibleName = null;
			this.label9.Anchor = ((System.Windows.Forms.AnchorStyles)(resources.GetObject("label9.Anchor")));
			this.label9.AutoSize = ((bool)(resources.GetObject("label9.AutoSize")));
			this.label9.BackgroundImageLayout = ((System.Windows.Forms.ImageLayout)(resources.GetObject("label9.BackgroundImageLayout")));
			this.label9.Dock = ((System.Windows.Forms.DockStyle)(resources.GetObject("label9.Dock")));
			this.label9.Font = ((System.Drawing.Font)(resources.GetObject("label9.Font")));
			this.label9.ImageAlign = ((System.Drawing.ContentAlignment)(resources.GetObject("label9.ImageAlign")));
			this.label9.ImageIndex = ((int)(resources.GetObject("label9.ImageIndex")));
			this.label9.ImageKey = resources.GetString("label9.ImageKey");
			this.label9.ImeMode = ((System.Windows.Forms.ImeMode)(resources.GetObject("label9.ImeMode")));
			this.label9.Location = ((System.Drawing.Point)(resources.GetObject("label9.Location")));
			this.label9.Name = "label9";
			this.label9.RightToLeft = ((System.Windows.Forms.RightToLeft)(resources.GetObject("label9.RightToLeft")));
			this.label9.Size = ((System.Drawing.Size)(resources.GetObject("label9.Size")));
			this.label9.TabIndex = ((int)(resources.GetObject("label9.TabIndex")));
			this.label9.Text = resources.GetString("label9.Text");
			this.label9.TextAlign = ((System.Drawing.ContentAlignment)(resources.GetObject("label9.TextAlign")));
			// 
			// label10
			// 
			this.label10.AccessibleDescription = null;
			this.label10.AccessibleName = null;
			this.label10.Anchor = ((System.Windows.Forms.AnchorStyles)(resources.GetObject("label10.Anchor")));
			this.label10.AutoSize = ((bool)(resources.GetObject("label10.AutoSize")));
			this.label10.BackgroundImageLayout = ((System.Windows.Forms.ImageLayout)(resources.GetObject("label10.BackgroundImageLayout")));
			this.label10.Dock = ((System.Windows.Forms.DockStyle)(resources.GetObject("label10.Dock")));
			this.label10.Font = ((System.Drawing.Font)(resources.GetObject("label10.Font")));
			this.label10.ImageAlign = ((System.Drawing.ContentAlignment)(resources.GetObject("label10.ImageAlign")));
			this.label10.ImageIndex = ((int)(resources.GetObject("label10.ImageIndex")));
			this.label10.ImageKey = resources.GetString("label10.ImageKey");
			this.label10.ImeMode = ((System.Windows.Forms.ImeMode)(resources.GetObject("label10.ImeMode")));
			this.label10.Location = ((System.Drawing.Point)(resources.GetObject("label10.Location")));
			this.label10.Name = "label10";
			this.label10.RightToLeft = ((System.Windows.Forms.RightToLeft)(resources.GetObject("label10.RightToLeft")));
			this.label10.Size = ((System.Drawing.Size)(resources.GetObject("label10.Size")));
			this.label10.TabIndex = ((int)(resources.GetObject("label10.TabIndex")));
			this.label10.Text = resources.GetString("label10.Text");
			this.label10.TextAlign = ((System.Drawing.ContentAlignment)(resources.GetObject("label10.TextAlign")));
			// 
			// b0
			// 
			this.b0.AccessibleDescription = null;
			this.b0.AccessibleName = null;
			this.b0.Anchor = ((System.Windows.Forms.AnchorStyles)(resources.GetObject("b0.Anchor")));
			this.b0.AutoSize = ((bool)(resources.GetObject("b0.AutoSize")));
			this.b0.AutoSizeMode = ((System.Windows.Forms.AutoSizeMode)(resources.GetObject("b0.AutoSizeMode")));
			this.b0.BackgroundImage = null;
			this.b0.BackgroundImageLayout = ((System.Windows.Forms.ImageLayout)(resources.GetObject("b0.BackgroundImageLayout")));
			this.b0.Dock = ((System.Windows.Forms.DockStyle)(resources.GetObject("b0.Dock")));
			this.b0.FlatStyle = ((System.Windows.Forms.FlatStyle)(resources.GetObject("b0.FlatStyle")));
			this.b0.Font = null;
			this.b0.ImageAlign = ((System.Drawing.ContentAlignment)(resources.GetObject("b0.ImageAlign")));
			this.b0.ImageIndex = ((int)(resources.GetObject("b0.ImageIndex")));
			this.b0.ImageKey = resources.GetString("b0.ImageKey");
			this.b0.ImeMode = ((System.Windows.Forms.ImeMode)(resources.GetObject("b0.ImeMode")));
			this.b0.Location = ((System.Drawing.Point)(resources.GetObject("b0.Location")));
			this.b0.Name = "b0";
			this.b0.RightToLeft = ((System.Windows.Forms.RightToLeft)(resources.GetObject("b0.RightToLeft")));
			this.b0.Size = ((System.Drawing.Size)(resources.GetObject("b0.Size")));
			this.b0.TabIndex = ((int)(resources.GetObject("b0.TabIndex")));
			this.b0.TextAlign = ((System.Drawing.ContentAlignment)(resources.GetObject("b0.TextAlign")));
			this.b0.TextImageRelation = ((System.Windows.Forms.TextImageRelation)(resources.GetObject("b0.TextImageRelation")));
			this.b0.UseVisualStyleBackColor = true;
			this.b0.Click += new System.EventHandler(this.ColorButtonClick);
			// 
			// b1
			// 
			this.b1.AccessibleDescription = null;
			this.b1.AccessibleName = null;
			this.b1.Anchor = ((System.Windows.Forms.AnchorStyles)(resources.GetObject("b1.Anchor")));
			this.b1.AutoSize = ((bool)(resources.GetObject("b1.AutoSize")));
			this.b1.AutoSizeMode = ((System.Windows.Forms.AutoSizeMode)(resources.GetObject("b1.AutoSizeMode")));
			this.b1.BackgroundImage = null;
			this.b1.BackgroundImageLayout = ((System.Windows.Forms.ImageLayout)(resources.GetObject("b1.BackgroundImageLayout")));
			this.b1.Dock = ((System.Windows.Forms.DockStyle)(resources.GetObject("b1.Dock")));
			this.b1.FlatStyle = ((System.Windows.Forms.FlatStyle)(resources.GetObject("b1.FlatStyle")));
			this.b1.Font = null;
			this.b1.ImageAlign = ((System.Drawing.ContentAlignment)(resources.GetObject("b1.ImageAlign")));
			this.b1.ImageIndex = ((int)(resources.GetObject("b1.ImageIndex")));
			this.b1.ImageKey = resources.GetString("b1.ImageKey");
			this.b1.ImeMode = ((System.Windows.Forms.ImeMode)(resources.GetObject("b1.ImeMode")));
			this.b1.Location = ((System.Drawing.Point)(resources.GetObject("b1.Location")));
			this.b1.Name = "b1";
			this.b1.RightToLeft = ((System.Windows.Forms.RightToLeft)(resources.GetObject("b1.RightToLeft")));
			this.b1.Size = ((System.Drawing.Size)(resources.GetObject("b1.Size")));
			this.b1.TabIndex = ((int)(resources.GetObject("b1.TabIndex")));
			this.b1.TextAlign = ((System.Drawing.ContentAlignment)(resources.GetObject("b1.TextAlign")));
			this.b1.TextImageRelation = ((System.Windows.Forms.TextImageRelation)(resources.GetObject("b1.TextImageRelation")));
			this.b1.UseVisualStyleBackColor = true;
			this.b1.Click += new System.EventHandler(this.ColorButtonClick);
			// 
			// b2
			// 
			this.b2.AccessibleDescription = null;
			this.b2.AccessibleName = null;
			this.b2.Anchor = ((System.Windows.Forms.AnchorStyles)(resources.GetObject("b2.Anchor")));
			this.b2.AutoSize = ((bool)(resources.GetObject("b2.AutoSize")));
			this.b2.AutoSizeMode = ((System.Windows.Forms.AutoSizeMode)(resources.GetObject("b2.AutoSizeMode")));
			this.b2.BackgroundImage = null;
			this.b2.BackgroundImageLayout = ((System.Windows.Forms.ImageLayout)(resources.GetObject("b2.BackgroundImageLayout")));
			this.b2.Dock = ((System.Windows.Forms.DockStyle)(resources.GetObject("b2.Dock")));
			this.b2.FlatStyle = ((System.Windows.Forms.FlatStyle)(resources.GetObject("b2.FlatStyle")));
			this.b2.Font = null;
			this.b2.ImageAlign = ((System.Drawing.ContentAlignment)(resources.GetObject("b2.ImageAlign")));
			this.b2.ImageIndex = ((int)(resources.GetObject("b2.ImageIndex")));
			this.b2.ImageKey = resources.GetString("b2.ImageKey");
			this.b2.ImeMode = ((System.Windows.Forms.ImeMode)(resources.GetObject("b2.ImeMode")));
			this.b2.Location = ((System.Drawing.Point)(resources.GetObject("b2.Location")));
			this.b2.Name = "b2";
			this.b2.RightToLeft = ((System.Windows.Forms.RightToLeft)(resources.GetObject("b2.RightToLeft")));
			this.b2.Size = ((System.Drawing.Size)(resources.GetObject("b2.Size")));
			this.b2.TabIndex = ((int)(resources.GetObject("b2.TabIndex")));
			this.b2.TextAlign = ((System.Drawing.ContentAlignment)(resources.GetObject("b2.TextAlign")));
			this.b2.TextImageRelation = ((System.Windows.Forms.TextImageRelation)(resources.GetObject("b2.TextImageRelation")));
			this.b2.UseVisualStyleBackColor = true;
			this.b2.Click += new System.EventHandler(this.ColorButtonClick);
			// 
			// b3
			// 
			this.b3.AccessibleDescription = null;
			this.b3.AccessibleName = null;
			this.b3.Anchor = ((System.Windows.Forms.AnchorStyles)(resources.GetObject("b3.Anchor")));
			this.b3.AutoSize = ((bool)(resources.GetObject("b3.AutoSize")));
			this.b3.AutoSizeMode = ((System.Windows.Forms.AutoSizeMode)(resources.GetObject("b3.AutoSizeMode")));
			this.b3.BackgroundImage = null;
			this.b3.BackgroundImageLayout = ((System.Windows.Forms.ImageLayout)(resources.GetObject("b3.BackgroundImageLayout")));
			this.b3.Dock = ((System.Windows.Forms.DockStyle)(resources.GetObject("b3.Dock")));
			this.b3.FlatStyle = ((System.Windows.Forms.FlatStyle)(resources.GetObject("b3.FlatStyle")));
			this.b3.Font = null;
			this.b3.ImageAlign = ((System.Drawing.ContentAlignment)(resources.GetObject("b3.ImageAlign")));
			this.b3.ImageIndex = ((int)(resources.GetObject("b3.ImageIndex")));
			this.b3.ImageKey = resources.GetString("b3.ImageKey");
			this.b3.ImeMode = ((System.Windows.Forms.ImeMode)(resources.GetObject("b3.ImeMode")));
			this.b3.Location = ((System.Drawing.Point)(resources.GetObject("b3.Location")));
			this.b3.Name = "b3";
			this.b3.RightToLeft = ((System.Windows.Forms.RightToLeft)(resources.GetObject("b3.RightToLeft")));
			this.b3.Size = ((System.Drawing.Size)(resources.GetObject("b3.Size")));
			this.b3.TabIndex = ((int)(resources.GetObject("b3.TabIndex")));
			this.b3.TextAlign = ((System.Drawing.ContentAlignment)(resources.GetObject("b3.TextAlign")));
			this.b3.TextImageRelation = ((System.Windows.Forms.TextImageRelation)(resources.GetObject("b3.TextImageRelation")));
			this.b3.UseVisualStyleBackColor = true;
			this.b3.Click += new System.EventHandler(this.ColorButtonClick);
			// 
			// b4
			// 
			this.b4.AccessibleDescription = null;
			this.b4.AccessibleName = null;
			this.b4.Anchor = ((System.Windows.Forms.AnchorStyles)(resources.GetObject("b4.Anchor")));
			this.b4.AutoSize = ((bool)(resources.GetObject("b4.AutoSize")));
			this.b4.AutoSizeMode = ((System.Windows.Forms.AutoSizeMode)(resources.GetObject("b4.AutoSizeMode")));
			this.b4.BackgroundImage = null;
			this.b4.BackgroundImageLayout = ((System.Windows.Forms.ImageLayout)(resources.GetObject("b4.BackgroundImageLayout")));
			this.b4.Dock = ((System.Windows.Forms.DockStyle)(resources.GetObject("b4.Dock")));
			this.b4.FlatStyle = ((System.Windows.Forms.FlatStyle)(resources.GetObject("b4.FlatStyle")));
			this.b4.Font = null;
			this.b4.ImageAlign = ((System.Drawing.ContentAlignment)(resources.GetObject("b4.ImageAlign")));
			this.b4.ImageIndex = ((int)(resources.GetObject("b4.ImageIndex")));
			this.b4.ImageKey = resources.GetString("b4.ImageKey");
			this.b4.ImeMode = ((System.Windows.Forms.ImeMode)(resources.GetObject("b4.ImeMode")));
			this.b4.Location = ((System.Drawing.Point)(resources.GetObject("b4.Location")));
			this.b4.Name = "b4";
			this.b4.RightToLeft = ((System.Windows.Forms.RightToLeft)(resources.GetObject("b4.RightToLeft")));
			this.b4.Size = ((System.Drawing.Size)(resources.GetObject("b4.Size")));
			this.b4.TabIndex = ((int)(resources.GetObject("b4.TabIndex")));
			this.b4.TextAlign = ((System.Drawing.ContentAlignment)(resources.GetObject("b4.TextAlign")));
			this.b4.TextImageRelation = ((System.Windows.Forms.TextImageRelation)(resources.GetObject("b4.TextImageRelation")));
			this.b4.UseVisualStyleBackColor = true;
			this.b4.Click += new System.EventHandler(this.ColorButtonClick);
			// 
			// b5
			// 
			this.b5.AccessibleDescription = null;
			this.b5.AccessibleName = null;
			this.b5.Anchor = ((System.Windows.Forms.AnchorStyles)(resources.GetObject("b5.Anchor")));
			this.b5.AutoSize = ((bool)(resources.GetObject("b5.AutoSize")));
			this.b5.AutoSizeMode = ((System.Windows.Forms.AutoSizeMode)(resources.GetObject("b5.AutoSizeMode")));
			this.b5.BackgroundImage = null;
			this.b5.BackgroundImageLayout = ((System.Windows.Forms.ImageLayout)(resources.GetObject("b5.BackgroundImageLayout")));
			this.b5.Dock = ((System.Windows.Forms.DockStyle)(resources.GetObject("b5.Dock")));
			this.b5.FlatStyle = ((System.Windows.Forms.FlatStyle)(resources.GetObject("b5.FlatStyle")));
			this.b5.Font = null;
			this.b5.ImageAlign = ((System.Drawing.ContentAlignment)(resources.GetObject("b5.ImageAlign")));
			this.b5.ImageIndex = ((int)(resources.GetObject("b5.ImageIndex")));
			this.b5.ImageKey = resources.GetString("b5.ImageKey");
			this.b5.ImeMode = ((System.Windows.Forms.ImeMode)(resources.GetObject("b5.ImeMode")));
			this.b5.Location = ((System.Drawing.Point)(resources.GetObject("b5.Location")));
			this.b5.Name = "b5";
			this.b5.RightToLeft = ((System.Windows.Forms.RightToLeft)(resources.GetObject("b5.RightToLeft")));
			this.b5.Size = ((System.Drawing.Size)(resources.GetObject("b5.Size")));
			this.b5.TabIndex = ((int)(resources.GetObject("b5.TabIndex")));
			this.b5.TextAlign = ((System.Drawing.ContentAlignment)(resources.GetObject("b5.TextAlign")));
			this.b5.TextImageRelation = ((System.Windows.Forms.TextImageRelation)(resources.GetObject("b5.TextImageRelation")));
			this.b5.UseVisualStyleBackColor = true;
			this.b5.Click += new System.EventHandler(this.ColorButtonClick);
			// 
			// b6
			// 
			this.b6.AccessibleDescription = null;
			this.b6.AccessibleName = null;
			this.b6.Anchor = ((System.Windows.Forms.AnchorStyles)(resources.GetObject("b6.Anchor")));
			this.b6.AutoSize = ((bool)(resources.GetObject("b6.AutoSize")));
			this.b6.AutoSizeMode = ((System.Windows.Forms.AutoSizeMode)(resources.GetObject("b6.AutoSizeMode")));
			this.b6.BackgroundImage = null;
			this.b6.BackgroundImageLayout = ((System.Windows.Forms.ImageLayout)(resources.GetObject("b6.BackgroundImageLayout")));
			this.b6.Dock = ((System.Windows.Forms.DockStyle)(resources.GetObject("b6.Dock")));
			this.b6.FlatStyle = ((System.Windows.Forms.FlatStyle)(resources.GetObject("b6.FlatStyle")));
			this.b6.Font = null;
			this.b6.ImageAlign = ((System.Drawing.ContentAlignment)(resources.GetObject("b6.ImageAlign")));
			this.b6.ImageIndex = ((int)(resources.GetObject("b6.ImageIndex")));
			this.b6.ImageKey = resources.GetString("b6.ImageKey");
			this.b6.ImeMode = ((System.Windows.Forms.ImeMode)(resources.GetObject("b6.ImeMode")));
			this.b6.Location = ((System.Drawing.Point)(resources.GetObject("b6.Location")));
			this.b6.Name = "b6";
			this.b6.RightToLeft = ((System.Windows.Forms.RightToLeft)(resources.GetObject("b6.RightToLeft")));
			this.b6.Size = ((System.Drawing.Size)(resources.GetObject("b6.Size")));
			this.b6.TabIndex = ((int)(resources.GetObject("b6.TabIndex")));
			this.b6.TextAlign = ((System.Drawing.ContentAlignment)(resources.GetObject("b6.TextAlign")));
			this.b6.TextImageRelation = ((System.Windows.Forms.TextImageRelation)(resources.GetObject("b6.TextImageRelation")));
			this.b6.UseVisualStyleBackColor = true;
			this.b6.Click += new System.EventHandler(this.ColorButtonClick);
			// 
			// label4
			// 
			this.label4.AccessibleDescription = null;
			this.label4.AccessibleName = null;
			this.label4.Anchor = ((System.Windows.Forms.AnchorStyles)(resources.GetObject("label4.Anchor")));
			this.label4.AutoSize = ((bool)(resources.GetObject("label4.AutoSize")));
			this.label4.BackgroundImageLayout = ((System.Windows.Forms.ImageLayout)(resources.GetObject("label4.BackgroundImageLayout")));
			this.label4.Dock = ((System.Windows.Forms.DockStyle)(resources.GetObject("label4.Dock")));
			this.label4.Font = ((System.Drawing.Font)(resources.GetObject("label4.Font")));
			this.label4.ImageAlign = ((System.Drawing.ContentAlignment)(resources.GetObject("label4.ImageAlign")));
			this.label4.ImageIndex = ((int)(resources.GetObject("label4.ImageIndex")));
			this.label4.ImageKey = resources.GetString("label4.ImageKey");
			this.label4.ImeMode = ((System.Windows.Forms.ImeMode)(resources.GetObject("label4.ImeMode")));
			this.label4.Location = ((System.Drawing.Point)(resources.GetObject("label4.Location")));
			this.label4.Name = "label4";
			this.label4.RightToLeft = ((System.Windows.Forms.RightToLeft)(resources.GetObject("label4.RightToLeft")));
			this.label4.Size = ((System.Drawing.Size)(resources.GetObject("label4.Size")));
			this.label4.TabIndex = ((int)(resources.GetObject("label4.TabIndex")));
			this.label4.Text = resources.GetString("label4.Text");
			this.label4.TextAlign = ((System.Drawing.ContentAlignment)(resources.GetObject("label4.TextAlign")));
			// 
			// b7
			// 
			this.b7.AccessibleDescription = null;
			this.b7.AccessibleName = null;
			this.b7.Anchor = ((System.Windows.Forms.AnchorStyles)(resources.GetObject("b7.Anchor")));
			this.b7.AutoSize = ((bool)(resources.GetObject("b7.AutoSize")));
			this.b7.AutoSizeMode = ((System.Windows.Forms.AutoSizeMode)(resources.GetObject("b7.AutoSizeMode")));
			this.b7.BackgroundImage = null;
			this.b7.BackgroundImageLayout = ((System.Windows.Forms.ImageLayout)(resources.GetObject("b7.BackgroundImageLayout")));
			this.b7.Dock = ((System.Windows.Forms.DockStyle)(resources.GetObject("b7.Dock")));
			this.b7.FlatStyle = ((System.Windows.Forms.FlatStyle)(resources.GetObject("b7.FlatStyle")));
			this.b7.Font = null;
			this.b7.ImageAlign = ((System.Drawing.ContentAlignment)(resources.GetObject("b7.ImageAlign")));
			this.b7.ImageIndex = ((int)(resources.GetObject("b7.ImageIndex")));
			this.b7.ImageKey = resources.GetString("b7.ImageKey");
			this.b7.ImeMode = ((System.Windows.Forms.ImeMode)(resources.GetObject("b7.ImeMode")));
			this.b7.Location = ((System.Drawing.Point)(resources.GetObject("b7.Location")));
			this.b7.Name = "b7";
			this.b7.RightToLeft = ((System.Windows.Forms.RightToLeft)(resources.GetObject("b7.RightToLeft")));
			this.b7.Size = ((System.Drawing.Size)(resources.GetObject("b7.Size")));
			this.b7.TabIndex = ((int)(resources.GetObject("b7.TabIndex")));
			this.b7.TextAlign = ((System.Drawing.ContentAlignment)(resources.GetObject("b7.TextAlign")));
			this.b7.TextImageRelation = ((System.Windows.Forms.TextImageRelation)(resources.GetObject("b7.TextImageRelation")));
			this.b7.UseVisualStyleBackColor = true;
			this.b7.Click += new System.EventHandler(this.ColorButtonClick);
			// 
			// b8
			// 
			this.b8.AccessibleDescription = null;
			this.b8.AccessibleName = null;
			this.b8.Anchor = ((System.Windows.Forms.AnchorStyles)(resources.GetObject("b8.Anchor")));
			this.b8.AutoSize = ((bool)(resources.GetObject("b8.AutoSize")));
			this.b8.AutoSizeMode = ((System.Windows.Forms.AutoSizeMode)(resources.GetObject("b8.AutoSizeMode")));
			this.b8.BackgroundImage = null;
			this.b8.BackgroundImageLayout = ((System.Windows.Forms.ImageLayout)(resources.GetObject("b8.BackgroundImageLayout")));
			this.b8.Dock = ((System.Windows.Forms.DockStyle)(resources.GetObject("b8.Dock")));
			this.b8.FlatStyle = ((System.Windows.Forms.FlatStyle)(resources.GetObject("b8.FlatStyle")));
			this.b8.Font = null;
			this.b8.ImageAlign = ((System.Drawing.ContentAlignment)(resources.GetObject("b8.ImageAlign")));
			this.b8.ImageIndex = ((int)(resources.GetObject("b8.ImageIndex")));
			this.b8.ImageKey = resources.GetString("b8.ImageKey");
			this.b8.ImeMode = ((System.Windows.Forms.ImeMode)(resources.GetObject("b8.ImeMode")));
			this.b8.Location = ((System.Drawing.Point)(resources.GetObject("b8.Location")));
			this.b8.Name = "b8";
			this.b8.RightToLeft = ((System.Windows.Forms.RightToLeft)(resources.GetObject("b8.RightToLeft")));
			this.b8.Size = ((System.Drawing.Size)(resources.GetObject("b8.Size")));
			this.b8.TabIndex = ((int)(resources.GetObject("b8.TabIndex")));
			this.b8.TextAlign = ((System.Drawing.ContentAlignment)(resources.GetObject("b8.TextAlign")));
			this.b8.TextImageRelation = ((System.Windows.Forms.TextImageRelation)(resources.GetObject("b8.TextImageRelation")));
			this.b8.UseVisualStyleBackColor = true;
			this.b8.Click += new System.EventHandler(this.ColorButtonClick);
			// 
			// label11
			// 
			this.label11.AccessibleDescription = null;
			this.label11.AccessibleName = null;
			this.label11.Anchor = ((System.Windows.Forms.AnchorStyles)(resources.GetObject("label11.Anchor")));
			this.label11.AutoSize = ((bool)(resources.GetObject("label11.AutoSize")));
			this.label11.BackgroundImageLayout = ((System.Windows.Forms.ImageLayout)(resources.GetObject("label11.BackgroundImageLayout")));
			this.label11.Dock = ((System.Windows.Forms.DockStyle)(resources.GetObject("label11.Dock")));
			this.label11.Font = ((System.Drawing.Font)(resources.GetObject("label11.Font")));
			this.label11.ImageAlign = ((System.Drawing.ContentAlignment)(resources.GetObject("label11.ImageAlign")));
			this.label11.ImageIndex = ((int)(resources.GetObject("label11.ImageIndex")));
			this.label11.ImageKey = resources.GetString("label11.ImageKey");
			this.label11.ImeMode = ((System.Windows.Forms.ImeMode)(resources.GetObject("label11.ImeMode")));
			this.label11.Location = ((System.Drawing.Point)(resources.GetObject("label11.Location")));
			this.label11.Name = "label11";
			this.label11.RightToLeft = ((System.Windows.Forms.RightToLeft)(resources.GetObject("label11.RightToLeft")));
			this.label11.Size = ((System.Drawing.Size)(resources.GetObject("label11.Size")));
			this.label11.TabIndex = ((int)(resources.GetObject("label11.TabIndex")));
			this.label11.Text = resources.GetString("label11.Text");
			this.label11.TextAlign = ((System.Drawing.ContentAlignment)(resources.GetObject("label11.TextAlign")));
			// 
			// b9
			// 
			this.b9.AccessibleDescription = null;
			this.b9.AccessibleName = null;
			this.b9.Anchor = ((System.Windows.Forms.AnchorStyles)(resources.GetObject("b9.Anchor")));
			this.b9.AutoSize = ((bool)(resources.GetObject("b9.AutoSize")));
			this.b9.AutoSizeMode = ((System.Windows.Forms.AutoSizeMode)(resources.GetObject("b9.AutoSizeMode")));
			this.b9.BackgroundImage = null;
			this.b9.BackgroundImageLayout = ((System.Windows.Forms.ImageLayout)(resources.GetObject("b9.BackgroundImageLayout")));
			this.b9.Dock = ((System.Windows.Forms.DockStyle)(resources.GetObject("b9.Dock")));
			this.b9.FlatStyle = ((System.Windows.Forms.FlatStyle)(resources.GetObject("b9.FlatStyle")));
			this.b9.Font = null;
			this.b9.ImageAlign = ((System.Drawing.ContentAlignment)(resources.GetObject("b9.ImageAlign")));
			this.b9.ImageIndex = ((int)(resources.GetObject("b9.ImageIndex")));
			this.b9.ImageKey = resources.GetString("b9.ImageKey");
			this.b9.ImeMode = ((System.Windows.Forms.ImeMode)(resources.GetObject("b9.ImeMode")));
			this.b9.Location = ((System.Drawing.Point)(resources.GetObject("b9.Location")));
			this.b9.Name = "b9";
			this.b9.RightToLeft = ((System.Windows.Forms.RightToLeft)(resources.GetObject("b9.RightToLeft")));
			this.b9.Size = ((System.Drawing.Size)(resources.GetObject("b9.Size")));
			this.b9.TabIndex = ((int)(resources.GetObject("b9.TabIndex")));
			this.b9.TextAlign = ((System.Drawing.ContentAlignment)(resources.GetObject("b9.TextAlign")));
			this.b9.TextImageRelation = ((System.Windows.Forms.TextImageRelation)(resources.GetObject("b9.TextImageRelation")));
			this.b9.UseVisualStyleBackColor = true;
			this.b9.Click += new System.EventHandler(this.ColorButtonClick);
			// 
			// label12
			// 
			this.label12.AccessibleDescription = null;
			this.label12.AccessibleName = null;
			this.label12.Anchor = ((System.Windows.Forms.AnchorStyles)(resources.GetObject("label12.Anchor")));
			this.label12.AutoSize = ((bool)(resources.GetObject("label12.AutoSize")));
			this.label12.BackgroundImageLayout = ((System.Windows.Forms.ImageLayout)(resources.GetObject("label12.BackgroundImageLayout")));
			this.label12.Dock = ((System.Windows.Forms.DockStyle)(resources.GetObject("label12.Dock")));
			this.label12.Font = ((System.Drawing.Font)(resources.GetObject("label12.Font")));
			this.label12.ImageAlign = ((System.Drawing.ContentAlignment)(resources.GetObject("label12.ImageAlign")));
			this.label12.ImageIndex = ((int)(resources.GetObject("label12.ImageIndex")));
			this.label12.ImageKey = resources.GetString("label12.ImageKey");
			this.label12.ImeMode = ((System.Windows.Forms.ImeMode)(resources.GetObject("label12.ImeMode")));
			this.label12.Location = ((System.Drawing.Point)(resources.GetObject("label12.Location")));
			this.label12.Name = "label12";
			this.label12.RightToLeft = ((System.Windows.Forms.RightToLeft)(resources.GetObject("label12.RightToLeft")));
			this.label12.Size = ((System.Drawing.Size)(resources.GetObject("label12.Size")));
			this.label12.TabIndex = ((int)(resources.GetObject("label12.TabIndex")));
			this.label12.Text = resources.GetString("label12.Text");
			this.label12.TextAlign = ((System.Drawing.ContentAlignment)(resources.GetObject("label12.TextAlign")));
			// 
			// label13
			// 
			this.label13.AccessibleDescription = null;
			this.label13.AccessibleName = null;
			this.label13.Anchor = ((System.Windows.Forms.AnchorStyles)(resources.GetObject("label13.Anchor")));
			this.label13.AutoSize = ((bool)(resources.GetObject("label13.AutoSize")));
			this.label13.BackgroundImageLayout = ((System.Windows.Forms.ImageLayout)(resources.GetObject("label13.BackgroundImageLayout")));
			this.label13.Dock = ((System.Windows.Forms.DockStyle)(resources.GetObject("label13.Dock")));
			this.label13.Font = ((System.Drawing.Font)(resources.GetObject("label13.Font")));
			this.label13.ImageAlign = ((System.Drawing.ContentAlignment)(resources.GetObject("label13.ImageAlign")));
			this.label13.ImageIndex = ((int)(resources.GetObject("label13.ImageIndex")));
			this.label13.ImageKey = resources.GetString("label13.ImageKey");
			this.label13.ImeMode = ((System.Windows.Forms.ImeMode)(resources.GetObject("label13.ImeMode")));
			this.label13.Location = ((System.Drawing.Point)(resources.GetObject("label13.Location")));
			this.label13.Name = "label13";
			this.label13.RightToLeft = ((System.Windows.Forms.RightToLeft)(resources.GetObject("label13.RightToLeft")));
			this.label13.Size = ((System.Drawing.Size)(resources.GetObject("label13.Size")));
			this.label13.TabIndex = ((int)(resources.GetObject("label13.TabIndex")));
			this.label13.Text = resources.GetString("label13.Text");
			this.label13.TextAlign = ((System.Drawing.ContentAlignment)(resources.GetObject("label13.TextAlign")));
			// 
			// b10
			// 
			this.b10.AccessibleDescription = null;
			this.b10.AccessibleName = null;
			this.b10.Anchor = ((System.Windows.Forms.AnchorStyles)(resources.GetObject("b10.Anchor")));
			this.b10.AutoSize = ((bool)(resources.GetObject("b10.AutoSize")));
			this.b10.AutoSizeMode = ((System.Windows.Forms.AutoSizeMode)(resources.GetObject("b10.AutoSizeMode")));
			this.b10.BackgroundImage = null;
			this.b10.BackgroundImageLayout = ((System.Windows.Forms.ImageLayout)(resources.GetObject("b10.BackgroundImageLayout")));
			this.b10.Dock = ((System.Windows.Forms.DockStyle)(resources.GetObject("b10.Dock")));
			this.b10.FlatStyle = ((System.Windows.Forms.FlatStyle)(resources.GetObject("b10.FlatStyle")));
			this.b10.Font = null;
			this.b10.ImageAlign = ((System.Drawing.ContentAlignment)(resources.GetObject("b10.ImageAlign")));
			this.b10.ImageIndex = ((int)(resources.GetObject("b10.ImageIndex")));
			this.b10.ImageKey = resources.GetString("b10.ImageKey");
			this.b10.ImeMode = ((System.Windows.Forms.ImeMode)(resources.GetObject("b10.ImeMode")));
			this.b10.Location = ((System.Drawing.Point)(resources.GetObject("b10.Location")));
			this.b10.Name = "b10";
			this.b10.RightToLeft = ((System.Windows.Forms.RightToLeft)(resources.GetObject("b10.RightToLeft")));
			this.b10.Size = ((System.Drawing.Size)(resources.GetObject("b10.Size")));
			this.b10.TabIndex = ((int)(resources.GetObject("b10.TabIndex")));
			this.b10.TextAlign = ((System.Drawing.ContentAlignment)(resources.GetObject("b10.TextAlign")));
			this.b10.TextImageRelation = ((System.Windows.Forms.TextImageRelation)(resources.GetObject("b10.TextImageRelation")));
			this.b10.UseVisualStyleBackColor = true;
			this.b10.Click += new System.EventHandler(this.ColorButtonClick);
			// 
			// b11
			// 
			this.b11.AccessibleDescription = null;
			this.b11.AccessibleName = null;
			this.b11.Anchor = ((System.Windows.Forms.AnchorStyles)(resources.GetObject("b11.Anchor")));
			this.b11.AutoSize = ((bool)(resources.GetObject("b11.AutoSize")));
			this.b11.AutoSizeMode = ((System.Windows.Forms.AutoSizeMode)(resources.GetObject("b11.AutoSizeMode")));
			this.b11.BackgroundImage = null;
			this.b11.BackgroundImageLayout = ((System.Windows.Forms.ImageLayout)(resources.GetObject("b11.BackgroundImageLayout")));
			this.b11.Dock = ((System.Windows.Forms.DockStyle)(resources.GetObject("b11.Dock")));
			this.b11.FlatStyle = ((System.Windows.Forms.FlatStyle)(resources.GetObject("b11.FlatStyle")));
			this.b11.Font = null;
			this.b11.ImageAlign = ((System.Drawing.ContentAlignment)(resources.GetObject("b11.ImageAlign")));
			this.b11.ImageIndex = ((int)(resources.GetObject("b11.ImageIndex")));
			this.b11.ImageKey = resources.GetString("b11.ImageKey");
			this.b11.ImeMode = ((System.Windows.Forms.ImeMode)(resources.GetObject("b11.ImeMode")));
			this.b11.Location = ((System.Drawing.Point)(resources.GetObject("b11.Location")));
			this.b11.Name = "b11";
			this.b11.RightToLeft = ((System.Windows.Forms.RightToLeft)(resources.GetObject("b11.RightToLeft")));
			this.b11.Size = ((System.Drawing.Size)(resources.GetObject("b11.Size")));
			this.b11.TabIndex = ((int)(resources.GetObject("b11.TabIndex")));
			this.b11.TextAlign = ((System.Drawing.ContentAlignment)(resources.GetObject("b11.TextAlign")));
			this.b11.TextImageRelation = ((System.Windows.Forms.TextImageRelation)(resources.GetObject("b11.TextImageRelation")));
			this.b11.UseVisualStyleBackColor = true;
			this.b11.Click += new System.EventHandler(this.ColorButtonClick);
			// 
			// label14
			// 
			this.label14.AccessibleDescription = null;
			this.label14.AccessibleName = null;
			this.label14.Anchor = ((System.Windows.Forms.AnchorStyles)(resources.GetObject("label14.Anchor")));
			this.label14.AutoSize = ((bool)(resources.GetObject("label14.AutoSize")));
			this.label14.BackgroundImageLayout = ((System.Windows.Forms.ImageLayout)(resources.GetObject("label14.BackgroundImageLayout")));
			this.label14.Dock = ((System.Windows.Forms.DockStyle)(resources.GetObject("label14.Dock")));
			this.label14.Font = ((System.Drawing.Font)(resources.GetObject("label14.Font")));
			this.label14.ImageAlign = ((System.Drawing.ContentAlignment)(resources.GetObject("label14.ImageAlign")));
			this.label14.ImageIndex = ((int)(resources.GetObject("label14.ImageIndex")));
			this.label14.ImageKey = resources.GetString("label14.ImageKey");
			this.label14.ImeMode = ((System.Windows.Forms.ImeMode)(resources.GetObject("label14.ImeMode")));
			this.label14.Location = ((System.Drawing.Point)(resources.GetObject("label14.Location")));
			this.label14.Name = "label14";
			this.label14.RightToLeft = ((System.Windows.Forms.RightToLeft)(resources.GetObject("label14.RightToLeft")));
			this.label14.Size = ((System.Drawing.Size)(resources.GetObject("label14.Size")));
			this.label14.TabIndex = ((int)(resources.GetObject("label14.TabIndex")));
			this.label14.Text = resources.GetString("label14.Text");
			this.label14.TextAlign = ((System.Drawing.ContentAlignment)(resources.GetObject("label14.TextAlign")));
			// 
			// b12
			// 
			this.b12.AccessibleDescription = null;
			this.b12.AccessibleName = null;
			this.b12.Anchor = ((System.Windows.Forms.AnchorStyles)(resources.GetObject("b12.Anchor")));
			this.b12.AutoSize = ((bool)(resources.GetObject("b12.AutoSize")));
			this.b12.AutoSizeMode = ((System.Windows.Forms.AutoSizeMode)(resources.GetObject("b12.AutoSizeMode")));
			this.b12.BackgroundImage = null;
			this.b12.BackgroundImageLayout = ((System.Windows.Forms.ImageLayout)(resources.GetObject("b12.BackgroundImageLayout")));
			this.b12.Dock = ((System.Windows.Forms.DockStyle)(resources.GetObject("b12.Dock")));
			this.b12.FlatStyle = ((System.Windows.Forms.FlatStyle)(resources.GetObject("b12.FlatStyle")));
			this.b12.Font = null;
			this.b12.ImageAlign = ((System.Drawing.ContentAlignment)(resources.GetObject("b12.ImageAlign")));
			this.b12.ImageIndex = ((int)(resources.GetObject("b12.ImageIndex")));
			this.b12.ImageKey = resources.GetString("b12.ImageKey");
			this.b12.ImeMode = ((System.Windows.Forms.ImeMode)(resources.GetObject("b12.ImeMode")));
			this.b12.Location = ((System.Drawing.Point)(resources.GetObject("b12.Location")));
			this.b12.Name = "b12";
			this.b12.RightToLeft = ((System.Windows.Forms.RightToLeft)(resources.GetObject("b12.RightToLeft")));
			this.b12.Size = ((System.Drawing.Size)(resources.GetObject("b12.Size")));
			this.b12.TabIndex = ((int)(resources.GetObject("b12.TabIndex")));
			this.b12.TextAlign = ((System.Drawing.ContentAlignment)(resources.GetObject("b12.TextAlign")));
			this.b12.TextImageRelation = ((System.Windows.Forms.TextImageRelation)(resources.GetObject("b12.TextImageRelation")));
			this.b12.UseVisualStyleBackColor = true;
			this.b12.Click += new System.EventHandler(this.ColorButtonClick);
			// 
			// label15
			// 
			this.label15.AccessibleDescription = null;
			this.label15.AccessibleName = null;
			this.label15.Anchor = ((System.Windows.Forms.AnchorStyles)(resources.GetObject("label15.Anchor")));
			this.label15.AutoSize = ((bool)(resources.GetObject("label15.AutoSize")));
			this.label15.BackgroundImageLayout = ((System.Windows.Forms.ImageLayout)(resources.GetObject("label15.BackgroundImageLayout")));
			this.label15.Dock = ((System.Windows.Forms.DockStyle)(resources.GetObject("label15.Dock")));
			this.label15.Font = ((System.Drawing.Font)(resources.GetObject("label15.Font")));
			this.label15.ImageAlign = ((System.Drawing.ContentAlignment)(resources.GetObject("label15.ImageAlign")));
			this.label15.ImageIndex = ((int)(resources.GetObject("label15.ImageIndex")));
			this.label15.ImageKey = resources.GetString("label15.ImageKey");
			this.label15.ImeMode = ((System.Windows.Forms.ImeMode)(resources.GetObject("label15.ImeMode")));
			this.label15.Location = ((System.Drawing.Point)(resources.GetObject("label15.Location")));
			this.label15.Name = "label15";
			this.label15.RightToLeft = ((System.Windows.Forms.RightToLeft)(resources.GetObject("label15.RightToLeft")));
			this.label15.Size = ((System.Drawing.Size)(resources.GetObject("label15.Size")));
			this.label15.TabIndex = ((int)(resources.GetObject("label15.TabIndex")));
			this.label15.Text = resources.GetString("label15.Text");
			this.label15.TextAlign = ((System.Drawing.ContentAlignment)(resources.GetObject("label15.TextAlign")));
			// 
			// label16
			// 
			this.label16.AccessibleDescription = null;
			this.label16.AccessibleName = null;
			this.label16.Anchor = ((System.Windows.Forms.AnchorStyles)(resources.GetObject("label16.Anchor")));
			this.label16.AutoSize = ((bool)(resources.GetObject("label16.AutoSize")));
			this.label16.BackgroundImageLayout = ((System.Windows.Forms.ImageLayout)(resources.GetObject("label16.BackgroundImageLayout")));
			this.label16.Dock = ((System.Windows.Forms.DockStyle)(resources.GetObject("label16.Dock")));
			this.label16.Font = ((System.Drawing.Font)(resources.GetObject("label16.Font")));
			this.label16.ImageAlign = ((System.Drawing.ContentAlignment)(resources.GetObject("label16.ImageAlign")));
			this.label16.ImageIndex = ((int)(resources.GetObject("label16.ImageIndex")));
			this.label16.ImageKey = resources.GetString("label16.ImageKey");
			this.label16.ImeMode = ((System.Windows.Forms.ImeMode)(resources.GetObject("label16.ImeMode")));
			this.label16.Location = ((System.Drawing.Point)(resources.GetObject("label16.Location")));
			this.label16.Name = "label16";
			this.label16.RightToLeft = ((System.Windows.Forms.RightToLeft)(resources.GetObject("label16.RightToLeft")));
			this.label16.Size = ((System.Drawing.Size)(resources.GetObject("label16.Size")));
			this.label16.TabIndex = ((int)(resources.GetObject("label16.TabIndex")));
			this.label16.Text = resources.GetString("label16.Text");
			this.label16.TextAlign = ((System.Drawing.ContentAlignment)(resources.GetObject("label16.TextAlign")));
			// 
			// b13
			// 
			this.b13.AccessibleDescription = null;
			this.b13.AccessibleName = null;
			this.b13.Anchor = ((System.Windows.Forms.AnchorStyles)(resources.GetObject("b13.Anchor")));
			this.b13.AutoSize = ((bool)(resources.GetObject("b13.AutoSize")));
			this.b13.AutoSizeMode = ((System.Windows.Forms.AutoSizeMode)(resources.GetObject("b13.AutoSizeMode")));
			this.b13.BackgroundImage = null;
			this.b13.BackgroundImageLayout = ((System.Windows.Forms.ImageLayout)(resources.GetObject("b13.BackgroundImageLayout")));
			this.b13.Dock = ((System.Windows.Forms.DockStyle)(resources.GetObject("b13.Dock")));
			this.b13.FlatStyle = ((System.Windows.Forms.FlatStyle)(resources.GetObject("b13.FlatStyle")));
			this.b13.Font = null;
			this.b13.ImageAlign = ((System.Drawing.ContentAlignment)(resources.GetObject("b13.ImageAlign")));
			this.b13.ImageIndex = ((int)(resources.GetObject("b13.ImageIndex")));
			this.b13.ImageKey = resources.GetString("b13.ImageKey");
			this.b13.ImeMode = ((System.Windows.Forms.ImeMode)(resources.GetObject("b13.ImeMode")));
			this.b13.Location = ((System.Drawing.Point)(resources.GetObject("b13.Location")));
			this.b13.Name = "b13";
			this.b13.RightToLeft = ((System.Windows.Forms.RightToLeft)(resources.GetObject("b13.RightToLeft")));
			this.b13.Size = ((System.Drawing.Size)(resources.GetObject("b13.Size")));
			this.b13.TabIndex = ((int)(resources.GetObject("b13.TabIndex")));
			this.b13.TextAlign = ((System.Drawing.ContentAlignment)(resources.GetObject("b13.TextAlign")));
			this.b13.TextImageRelation = ((System.Windows.Forms.TextImageRelation)(resources.GetObject("b13.TextImageRelation")));
			this.b13.UseVisualStyleBackColor = true;
			this.b13.Click += new System.EventHandler(this.ColorButtonClick);
			// 
			// label17
			// 
			this.label17.AccessibleDescription = null;
			this.label17.AccessibleName = null;
			this.label17.Anchor = ((System.Windows.Forms.AnchorStyles)(resources.GetObject("label17.Anchor")));
			this.label17.AutoSize = ((bool)(resources.GetObject("label17.AutoSize")));
			this.label17.BackgroundImageLayout = ((System.Windows.Forms.ImageLayout)(resources.GetObject("label17.BackgroundImageLayout")));
			this.label17.Dock = ((System.Windows.Forms.DockStyle)(resources.GetObject("label17.Dock")));
			this.label17.Font = ((System.Drawing.Font)(resources.GetObject("label17.Font")));
			this.label17.ImageAlign = ((System.Drawing.ContentAlignment)(resources.GetObject("label17.ImageAlign")));
			this.label17.ImageIndex = ((int)(resources.GetObject("label17.ImageIndex")));
			this.label17.ImageKey = resources.GetString("label17.ImageKey");
			this.label17.ImeMode = ((System.Windows.Forms.ImeMode)(resources.GetObject("label17.ImeMode")));
			this.label17.Location = ((System.Drawing.Point)(resources.GetObject("label17.Location")));
			this.label17.Name = "label17";
			this.label17.RightToLeft = ((System.Windows.Forms.RightToLeft)(resources.GetObject("label17.RightToLeft")));
			this.label17.Size = ((System.Drawing.Size)(resources.GetObject("label17.Size")));
			this.label17.TabIndex = ((int)(resources.GetObject("label17.TabIndex")));
			this.label17.Text = resources.GetString("label17.Text");
			this.label17.TextAlign = ((System.Drawing.ContentAlignment)(resources.GetObject("label17.TextAlign")));
			// 
			// b14
			// 
			this.b14.AccessibleDescription = null;
			this.b14.AccessibleName = null;
			this.b14.Anchor = ((System.Windows.Forms.AnchorStyles)(resources.GetObject("b14.Anchor")));
			this.b14.AutoSize = ((bool)(resources.GetObject("b14.AutoSize")));
			this.b14.AutoSizeMode = ((System.Windows.Forms.AutoSizeMode)(resources.GetObject("b14.AutoSizeMode")));
			this.b14.BackgroundImage = null;
			this.b14.BackgroundImageLayout = ((System.Windows.Forms.ImageLayout)(resources.GetObject("b14.BackgroundImageLayout")));
			this.b14.Dock = ((System.Windows.Forms.DockStyle)(resources.GetObject("b14.Dock")));
			this.b14.FlatStyle = ((System.Windows.Forms.FlatStyle)(resources.GetObject("b14.FlatStyle")));
			this.b14.Font = null;
			this.b14.ImageAlign = ((System.Drawing.ContentAlignment)(resources.GetObject("b14.ImageAlign")));
			this.b14.ImageIndex = ((int)(resources.GetObject("b14.ImageIndex")));
			this.b14.ImageKey = resources.GetString("b14.ImageKey");
			this.b14.ImeMode = ((System.Windows.Forms.ImeMode)(resources.GetObject("b14.ImeMode")));
			this.b14.Location = ((System.Drawing.Point)(resources.GetObject("b14.Location")));
			this.b14.Name = "b14";
			this.b14.RightToLeft = ((System.Windows.Forms.RightToLeft)(resources.GetObject("b14.RightToLeft")));
			this.b14.Size = ((System.Drawing.Size)(resources.GetObject("b14.Size")));
			this.b14.TabIndex = ((int)(resources.GetObject("b14.TabIndex")));
			this.b14.TextAlign = ((System.Drawing.ContentAlignment)(resources.GetObject("b14.TextAlign")));
			this.b14.TextImageRelation = ((System.Windows.Forms.TextImageRelation)(resources.GetObject("b14.TextImageRelation")));
			this.b14.UseVisualStyleBackColor = true;
			this.b14.Click += new System.EventHandler(this.ColorButtonClick);
			// 
			// label18
			// 
			this.label18.AccessibleDescription = null;
			this.label18.AccessibleName = null;
			this.label18.Anchor = ((System.Windows.Forms.AnchorStyles)(resources.GetObject("label18.Anchor")));
			this.label18.AutoSize = ((bool)(resources.GetObject("label18.AutoSize")));
			this.label18.BackgroundImageLayout = ((System.Windows.Forms.ImageLayout)(resources.GetObject("label18.BackgroundImageLayout")));
			this.label18.Dock = ((System.Windows.Forms.DockStyle)(resources.GetObject("label18.Dock")));
			this.label18.Font = ((System.Drawing.Font)(resources.GetObject("label18.Font")));
			this.label18.ImageAlign = ((System.Drawing.ContentAlignment)(resources.GetObject("label18.ImageAlign")));
			this.label18.ImageIndex = ((int)(resources.GetObject("label18.ImageIndex")));
			this.label18.ImageKey = resources.GetString("label18.ImageKey");
			this.label18.ImeMode = ((System.Windows.Forms.ImeMode)(resources.GetObject("label18.ImeMode")));
			this.label18.Location = ((System.Drawing.Point)(resources.GetObject("label18.Location")));
			this.label18.Name = "label18";
			this.label18.RightToLeft = ((System.Windows.Forms.RightToLeft)(resources.GetObject("label18.RightToLeft")));
			this.label18.Size = ((System.Drawing.Size)(resources.GetObject("label18.Size")));
			this.label18.TabIndex = ((int)(resources.GetObject("label18.TabIndex")));
			this.label18.Text = resources.GetString("label18.Text");
			this.label18.TextAlign = ((System.Drawing.ContentAlignment)(resources.GetObject("label18.TextAlign")));
			// 
			// b15
			// 
			this.b15.AccessibleDescription = null;
			this.b15.AccessibleName = null;
			this.b15.Anchor = ((System.Windows.Forms.AnchorStyles)(resources.GetObject("b15.Anchor")));
			this.b15.AutoSize = ((bool)(resources.GetObject("b15.AutoSize")));
			this.b15.AutoSizeMode = ((System.Windows.Forms.AutoSizeMode)(resources.GetObject("b15.AutoSizeMode")));
			this.b15.BackgroundImage = null;
			this.b15.BackgroundImageLayout = ((System.Windows.Forms.ImageLayout)(resources.GetObject("b15.BackgroundImageLayout")));
			this.b15.Dock = ((System.Windows.Forms.DockStyle)(resources.GetObject("b15.Dock")));
			this.b15.FlatStyle = ((System.Windows.Forms.FlatStyle)(resources.GetObject("b15.FlatStyle")));
			this.b15.Font = null;
			this.b15.ImageAlign = ((System.Drawing.ContentAlignment)(resources.GetObject("b15.ImageAlign")));
			this.b15.ImageIndex = ((int)(resources.GetObject("b15.ImageIndex")));
			this.b15.ImageKey = resources.GetString("b15.ImageKey");
			this.b15.ImeMode = ((System.Windows.Forms.ImeMode)(resources.GetObject("b15.ImeMode")));
			this.b15.Location = ((System.Drawing.Point)(resources.GetObject("b15.Location")));
			this.b15.Name = "b15";
			this.b15.RightToLeft = ((System.Windows.Forms.RightToLeft)(resources.GetObject("b15.RightToLeft")));
			this.b15.Size = ((System.Drawing.Size)(resources.GetObject("b15.Size")));
			this.b15.TabIndex = ((int)(resources.GetObject("b15.TabIndex")));
			this.b15.TextAlign = ((System.Drawing.ContentAlignment)(resources.GetObject("b15.TextAlign")));
			this.b15.TextImageRelation = ((System.Windows.Forms.TextImageRelation)(resources.GetObject("b15.TextImageRelation")));
			this.b15.UseVisualStyleBackColor = true;
			this.b15.Click += new System.EventHandler(this.ColorButtonClick);
			// 
			// SetEditorForm
			// 
			this.AccessibleDescription = null;
			this.AccessibleName = null;
			this.AutoScaleDimensions = ((System.Drawing.SizeF)(resources.GetObject("$this.AutoScaleDimensions")));
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.AutoScroll = ((bool)(resources.GetObject("$this.AutoScroll")));
			this.AutoSize = ((bool)(resources.GetObject("$this.AutoSize")));
			this.AutoSizeMode = ((System.Windows.Forms.AutoSizeMode)(resources.GetObject("$this.AutoSizeMode")));
			this.BackColor = System.Drawing.SystemColors.Control;
			this.BackgroundImage = null;
			this.BackgroundImageLayout = ((System.Windows.Forms.ImageLayout)(resources.GetObject("$this.BackgroundImageLayout")));
			this.ClientSize = ((System.Drawing.Size)(resources.GetObject("$this.ClientSize")));
			this.Controls.Add(this.label18);
			this.Controls.Add(this.b15);
			this.Controls.Add(this.label17);
			this.Controls.Add(this.b14);
			this.Controls.Add(this.label16);
			this.Controls.Add(this.b13);
			this.Controls.Add(this.label15);
			this.Controls.Add(this.b12);
			this.Controls.Add(this.label14);
			this.Controls.Add(this.b11);
			this.Controls.Add(this.b10);
			this.Controls.Add(this.label13);
			this.Controls.Add(this.label12);
			this.Controls.Add(this.b9);
			this.Controls.Add(this.b8);
			this.Controls.Add(this.label11);
			this.Controls.Add(this.b7);
			this.Controls.Add(this.label4);
			this.Controls.Add(this.b6);
			this.Controls.Add(this.b5);
			this.Controls.Add(this.b4);
			this.Controls.Add(this.b3);
			this.Controls.Add(this.b2);
			this.Controls.Add(this.b1);
			this.Controls.Add(this.b0);
			this.Controls.Add(this.label10);
			this.Controls.Add(this.label9);
			this.Controls.Add(this.label8);
			this.Controls.Add(this.label7);
			this.Controls.Add(this.label6);
			this.Controls.Add(this.label5);
			this.Controls.Add(this.label2);
			this.Controls.Add(this.label3);
			this.Controls.Add(this.label1);
			this.Controls.Add(this.richTextBox1);
			this.Controls.Add(this.YesButton);
			this.Controls.Add(this.button1);
			this.Font = null;
			this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
			this.Icon = null;
			this.ImeMode = ((System.Windows.Forms.ImeMode)(resources.GetObject("$this.ImeMode")));
			this.Location = ((System.Drawing.Point)(resources.GetObject("$this.Location")));
			this.MaximumSize = ((System.Drawing.Size)(resources.GetObject("$this.MaximumSize")));
			this.Name = "SetEditorForm";
			this.RightToLeft = ((System.Windows.Forms.RightToLeft)(resources.GetObject("$this.RightToLeft")));
			this.RightToLeftLayout = ((bool)(resources.GetObject("$this.RightToLeftLayout")));
			this.StartPosition = ((System.Windows.Forms.FormStartPosition)(resources.GetObject("$this.StartPosition")));
			this.Text = resources.GetString("$this.Text");
			this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.SetEditorFormFormClosing);
			this.ResumeLayout(false);
		}
		private System.Windows.Forms.Button b15;
		private System.Windows.Forms.Label label18;
		private System.Windows.Forms.Button b14;
		private System.Windows.Forms.Button b13;
		private System.Windows.Forms.Label label17;
		private System.Windows.Forms.Label label16;
		private System.Windows.Forms.Label label15;
		private System.Windows.Forms.Button b12;
		private System.Windows.Forms.Button b11;
		private System.Windows.Forms.Button b10;
		private System.Windows.Forms.Label label14;
		private System.Windows.Forms.Label label13;
		private System.Windows.Forms.Label label12;
		private System.Windows.Forms.Button b9;
		private System.Windows.Forms.Button b8;
		private System.Windows.Forms.Label label11;
		private System.Windows.Forms.Button b7;
		private System.Windows.Forms.Label label4;
		private System.Windows.Forms.Button b6;
		private System.Windows.Forms.Button b5;
		private System.Windows.Forms.Button b4;
		private System.Windows.Forms.Button b3;
		private System.Windows.Forms.Button b2;
		private System.Windows.Forms.Button b1;
		private System.Windows.Forms.Button b0;
		private System.Windows.Forms.Label label10;
		private System.Windows.Forms.Label label9;
		private System.Windows.Forms.Label label8;
		private System.Windows.Forms.Label label7;
		private System.Windows.Forms.Label label6;
		private System.Windows.Forms.Label label5;
		private System.Windows.Forms.Label label3;
		private System.Windows.Forms.Label label2;
		private System.Windows.Forms.Label label1;
		private System.Windows.Forms.RichTextBox richTextBox1;
		private System.Windows.Forms.Button YesButton;
		private System.Windows.Forms.Button button1;
	}
}
