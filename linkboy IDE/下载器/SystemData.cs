﻿
//系统参数
namespace n_ISPSystemData
{
	using System;
	using System.Drawing;
	using System.Collections;
	using System.ComponentModel;
	using System.Windows.Forms;
	using System.Data;
	using System.Runtime.Serialization;
	using System.Runtime.Serialization.Formatters.Binary;
	using System.IO;
	using n_OS;
	
	public static class SystemData
	{
		//初始化
		public static void Init()
		{
			isChanged = false;
		}
		
		//加载对象, 从:  系统数据.ser
		public static void Load()
		{
			stream = File.Open( OS.SystemRoot + "Resource" + OS.PATH_S + "下载器" + OS.PATH_S + "配置数据.ser", FileMode.Open );
			bin = new BinaryFormatter();
			
			ChangeLoad = (bool)bin.Deserialize( stream );
			CheckID = (bool)bin.Deserialize( stream );
			EraseFlash = (bool)bin.Deserialize( stream );
			CheckEmpty = (bool)bin.Deserialize( stream );
			WriteFlash = (bool)bin.Deserialize( stream );
			CheckFlash = (bool)bin.Deserialize( stream );
			
			stream.Close();
		}
		
		//串行化对象,保存到:  系统数据.ser
		public static void Save()
		{
			stream = File.Open( OS.SystemRoot + "Resource" + OS.PATH_S + "下载器" + OS.PATH_S + "配置数据.ser", FileMode.Open );
			bin = new BinaryFormatter();
			bin.Serialize( stream, ChangeLoad );
			bin.Serialize( stream, CheckID );
			bin.Serialize( stream, EraseFlash );
			bin.Serialize( stream, CheckEmpty );
			bin.Serialize( stream, WriteFlash );
			bin.Serialize( stream, CheckFlash );
			
			stream.Close();
		}
		
		// 输出文件流对象
		static Stream stream;
		
		// 串行化对象
		static BinaryFormatter bin;
		
		//是否改变
		public static bool isChanged;
		
		//程序文本字体
		public static bool ChangeLoad;
		public static bool CheckID;
		public static bool EraseFlash;
		public static bool CheckEmpty;
		public static bool WriteFlash;
		public static bool CheckFlash;
	}
}
	
