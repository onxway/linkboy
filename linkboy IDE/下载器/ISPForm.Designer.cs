﻿namespace n_USBASP
{
    partial class ISPForm
    {
        /// <summary>
        /// 必需的设计器变量。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// 清理所有正在使用的资源。
        /// </summary>
        /// <param name="disposing">如果应释放托管资源，为 true；否则为 false。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows 窗体设计器生成的代码

        /// <summary>
        /// 设计器支持所需的方法 - 不要
        /// 使用代码编辑器修改此方法的内容。
        /// </summary>
        private void InitializeComponent()
        {
        	this.components = new System.ComponentModel.Container();
        	System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ISPForm));
        	this.ReadFlashButton = new System.Windows.Forms.Button();
        	this.EraseButton = new System.Windows.Forms.Button();
        	this.WriteFlashButton = new System.Windows.Forms.Button();
        	this.CodeText = new System.Windows.Forms.RichTextBox();
        	this.progressBar1 = new System.Windows.Forms.ProgressBar();
        	this.LineLabel = new System.Windows.Forms.Label();
        	this.label11 = new System.Windows.Forms.Label();
        	this.panel1 = new System.Windows.Forms.Panel();
        	this.tabControl1 = new System.Windows.Forms.TabControl();
        	this.tabPage1 = new System.Windows.Forms.TabPage();
        	this.芯片选择comboBox = new System.Windows.Forms.ComboBox();
        	this.groupBox5 = new System.Windows.Forms.GroupBox();
        	this.label15 = new System.Windows.Forms.Label();
        	this.label13 = new System.Windows.Forms.Label();
        	this.label14 = new System.Windows.Forms.Label();
        	this.label12 = new System.Windows.Forms.Label();
        	this.DownLoadButton = new System.Windows.Forms.Button();
        	this.groupBox3 = new System.Windows.Forms.GroupBox();
        	this.CheckFlashButton = new System.Windows.Forms.Button();
        	this.CheckEmptyButton = new System.Windows.Forms.Button();
        	this.groupBox2 = new System.Windows.Forms.GroupBox();
        	this.DataChangeBox = new System.Windows.Forms.CheckBox();
        	this.CheckFlashBox = new System.Windows.Forms.CheckBox();
        	this.CheckIDBox = new System.Windows.Forms.CheckBox();
        	this.WriteFlashBox = new System.Windows.Forms.CheckBox();
        	this.EraseBox = new System.Windows.Forms.CheckBox();
        	this.CheckEmptyBox = new System.Windows.Forms.CheckBox();
        	this.groupBox1 = new System.Windows.Forms.GroupBox();
        	this.OpenFileButton = new System.Windows.Forms.Button();
        	this.LaterFileButton = new System.Windows.Forms.Button();
        	this.ReloadFileButton = new System.Windows.Forms.Button();
        	this.tabPage3 = new System.Windows.Forms.TabPage();
        	this.label10 = new System.Windows.Forms.Label();
        	this.tabPage2 = new System.Windows.Forms.TabPage();
        	this.textBoxHigh = new System.Windows.Forms.TextBox();
        	this.textBoxLow = new System.Windows.Forms.TextBox();
        	this.label26 = new System.Windows.Forms.Label();
        	this.label27 = new System.Windows.Forms.Label();
        	this.button写入熔丝 = new System.Windows.Forms.Button();
        	this.groupBox6 = new System.Windows.Forms.GroupBox();
        	this.label25 = new System.Windows.Forms.Label();
        	this.label24 = new System.Windows.Forms.Label();
        	this.button1 = new System.Windows.Forms.Button();
        	this.label23 = new System.Windows.Forms.Label();
        	this.label22 = new System.Windows.Forms.Label();
        	this.label21 = new System.Windows.Forms.Label();
        	this.label20 = new System.Windows.Forms.Label();
        	this.label19 = new System.Windows.Forms.Label();
        	this.label18 = new System.Windows.Forms.Label();
        	this.label17 = new System.Windows.Forms.Label();
        	this.label16 = new System.Windows.Forms.Label();
        	this.label9 = new System.Windows.Forms.Label();
        	this.label8 = new System.Windows.Forms.Label();
        	this.label7 = new System.Windows.Forms.Label();
        	this.label6 = new System.Windows.Forms.Label();
        	this.label5 = new System.Windows.Forms.Label();
        	this.label4 = new System.Windows.Forms.Label();
        	this.label3 = new System.Windows.Forms.Label();
        	this.label2 = new System.Windows.Forms.Label();
        	this.label1 = new System.Windows.Forms.Label();
        	this.timer1 = new System.Windows.Forms.Timer(this.components);
        	this.timer2 = new System.Windows.Forms.Timer(this.components);
        	this.panel1.SuspendLayout();
        	this.tabControl1.SuspendLayout();
        	this.tabPage1.SuspendLayout();
        	this.groupBox5.SuspendLayout();
        	this.groupBox3.SuspendLayout();
        	this.groupBox2.SuspendLayout();
        	this.groupBox1.SuspendLayout();
        	this.tabPage3.SuspendLayout();
        	this.tabPage2.SuspendLayout();
        	this.groupBox6.SuspendLayout();
        	this.SuspendLayout();
        	// 
        	// ReadFlashButton
        	// 
        	this.ReadFlashButton.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(192)))), ((int)(((byte)(255)))));
        	this.ReadFlashButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
        	this.ReadFlashButton.ForeColor = System.Drawing.Color.Blue;
        	this.ReadFlashButton.Location = new System.Drawing.Point(10, 62);
        	this.ReadFlashButton.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
        	this.ReadFlashButton.Name = "ReadFlashButton";
        	this.ReadFlashButton.Size = new System.Drawing.Size(75, 20);
        	this.ReadFlashButton.TabIndex = 11;
        	this.ReadFlashButton.Text = "读出Flash";
        	this.ReadFlashButton.UseVisualStyleBackColor = false;
        	this.ReadFlashButton.Click += new System.EventHandler(this.ReadFlashButton_Click);
        	// 
        	// EraseButton
        	// 
        	this.EraseButton.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(192)))), ((int)(((byte)(255)))));
        	this.EraseButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
        	this.EraseButton.ForeColor = System.Drawing.Color.Blue;
        	this.EraseButton.Location = new System.Drawing.Point(10, 109);
        	this.EraseButton.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
        	this.EraseButton.Name = "EraseButton";
        	this.EraseButton.Size = new System.Drawing.Size(75, 20);
        	this.EraseButton.TabIndex = 12;
        	this.EraseButton.Text = "擦除Flash";
        	this.EraseButton.UseVisualStyleBackColor = false;
        	this.EraseButton.Click += new System.EventHandler(this.EraseButton_Click);
        	// 
        	// WriteFlashButton
        	// 
        	this.WriteFlashButton.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(192)))), ((int)(((byte)(255)))));
        	this.WriteFlashButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
        	this.WriteFlashButton.ForeColor = System.Drawing.Color.Blue;
        	this.WriteFlashButton.Location = new System.Drawing.Point(10, 86);
        	this.WriteFlashButton.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
        	this.WriteFlashButton.Name = "WriteFlashButton";
        	this.WriteFlashButton.Size = new System.Drawing.Size(75, 20);
        	this.WriteFlashButton.TabIndex = 14;
        	this.WriteFlashButton.Text = "写入Flash";
        	this.WriteFlashButton.UseVisualStyleBackColor = false;
        	this.WriteFlashButton.Click += new System.EventHandler(this.WriteFlashButton_Click);
        	// 
        	// CodeText
        	// 
        	this.CodeText.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
        	        	        	| System.Windows.Forms.AnchorStyles.Left) 
        	        	        	| System.Windows.Forms.AnchorStyles.Right)));
        	this.CodeText.BackColor = System.Drawing.Color.White;
        	this.CodeText.Font = new System.Drawing.Font("Courier New", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
        	this.CodeText.Location = new System.Drawing.Point(47, 18);
        	this.CodeText.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
        	this.CodeText.Name = "CodeText";
        	this.CodeText.ReadOnly = true;
        	this.CodeText.Size = new System.Drawing.Size(446, 198);
        	this.CodeText.TabIndex = 15;
        	this.CodeText.Text = "";
        	// 
        	// progressBar1
        	// 
        	this.progressBar1.ForeColor = System.Drawing.Color.Blue;
        	this.progressBar1.Location = new System.Drawing.Point(38, 72);
        	this.progressBar1.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
        	this.progressBar1.Name = "progressBar1";
        	this.progressBar1.Size = new System.Drawing.Size(424, 12);
        	this.progressBar1.Style = System.Windows.Forms.ProgressBarStyle.Continuous;
        	this.progressBar1.TabIndex = 17;
        	// 
        	// LineLabel
        	// 
        	this.LineLabel.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
        	        	        	| System.Windows.Forms.AnchorStyles.Left)));
        	this.LineLabel.BackColor = System.Drawing.Color.Transparent;
        	this.LineLabel.Font = new System.Drawing.Font("Courier New", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
        	this.LineLabel.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
        	this.LineLabel.Location = new System.Drawing.Point(3, 20);
        	this.LineLabel.Name = "LineLabel";
        	this.LineLabel.Size = new System.Drawing.Size(45, 192);
        	this.LineLabel.TabIndex = 18;
        	// 
        	// label11
        	// 
        	this.label11.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
        	        	        	| System.Windows.Forms.AnchorStyles.Right)));
        	this.label11.BackColor = System.Drawing.Color.Transparent;
        	this.label11.Font = new System.Drawing.Font("Courier New", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
        	this.label11.ForeColor = System.Drawing.Color.DarkSlateGray;
        	this.label11.Location = new System.Drawing.Point(47, 0);
        	this.label11.Name = "label11";
        	this.label11.Size = new System.Drawing.Size(448, 18);
        	this.label11.TabIndex = 19;
        	this.label11.Text = " 00 01 02 03 04 05 06 07 08 09 0A 0B 0C 0D 0E 0F    0123456789ABCDEF";
        	this.label11.TextAlign = System.Drawing.ContentAlignment.BottomLeft;
        	// 
        	// panel1
        	// 
        	this.panel1.BackColor = System.Drawing.Color.WhiteSmoke;
        	this.panel1.Controls.Add(this.CodeText);
        	this.panel1.Controls.Add(this.LineLabel);
        	this.panel1.Controls.Add(this.label11);
        	this.panel1.Dock = System.Windows.Forms.DockStyle.Fill;
        	this.panel1.Location = new System.Drawing.Point(3, 21);
        	this.panel1.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
        	this.panel1.Name = "panel1";
        	this.panel1.Size = new System.Drawing.Size(495, 219);
        	this.panel1.TabIndex = 20;
        	// 
        	// tabControl1
        	// 
        	this.tabControl1.Controls.Add(this.tabPage1);
        	this.tabControl1.Controls.Add(this.tabPage3);
        	this.tabControl1.Controls.Add(this.tabPage2);
        	this.tabControl1.Dock = System.Windows.Forms.DockStyle.Fill;
        	this.tabControl1.Font = new System.Drawing.Font("宋体", 10.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
        	this.tabControl1.Location = new System.Drawing.Point(0, 0);
        	this.tabControl1.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
        	this.tabControl1.Name = "tabControl1";
        	this.tabControl1.SelectedIndex = 0;
        	this.tabControl1.Size = new System.Drawing.Size(592, 331);
        	this.tabControl1.TabIndex = 21;
        	// 
        	// tabPage1
        	// 
        	this.tabPage1.BackColor = System.Drawing.Color.WhiteSmoke;
        	this.tabPage1.Controls.Add(this.芯片选择comboBox);
        	this.tabPage1.Controls.Add(this.groupBox5);
        	this.tabPage1.Controls.Add(this.DownLoadButton);
        	this.tabPage1.Controls.Add(this.groupBox3);
        	this.tabPage1.Controls.Add(this.groupBox2);
        	this.tabPage1.Controls.Add(this.groupBox1);
        	this.tabPage1.ForeColor = System.Drawing.Color.DarkSlateGray;
        	this.tabPage1.Location = new System.Drawing.Point(4, 24);
        	this.tabPage1.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
        	this.tabPage1.Name = "tabPage1";
        	this.tabPage1.Padding = new System.Windows.Forms.Padding(3, 2, 3, 2);
        	this.tabPage1.Size = new System.Drawing.Size(584, 303);
        	this.tabPage1.TabIndex = 0;
        	this.tabPage1.Text = "芯片编程";
        	// 
        	// 芯片选择comboBox
        	// 
        	this.芯片选择comboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
        	this.芯片选择comboBox.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
        	this.芯片选择comboBox.Font = new System.Drawing.Font("Courier New", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
        	this.芯片选择comboBox.ForeColor = System.Drawing.Color.Black;
        	this.芯片选择comboBox.FormattingEnabled = true;
        	this.芯片选择comboBox.Items.AddRange(new object[] {
        	        	        	"AT89S51",
        	        	        	"AT89S52",
        	        	        	"MEGA8",
        	        	        	"MEGA16",
        	        	        	"MEGA32",
        	        	        	"MEGA64",
        	        	        	"MEGA48",
        	        	        	"MEGA328",
        	        	        	"MEGA644"});
        	this.芯片选择comboBox.Location = new System.Drawing.Point(359, 56);
        	this.芯片选择comboBox.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
        	this.芯片选择comboBox.Name = "芯片选择comboBox";
        	this.芯片选择comboBox.Size = new System.Drawing.Size(119, 26);
        	this.芯片选择comboBox.TabIndex = 0;
        	this.芯片选择comboBox.SelectedIndexChanged += new System.EventHandler(this.芯片选择comboBoxSelectedIndexChanged);
        	// 
        	// groupBox5
        	// 
        	this.groupBox5.Controls.Add(this.progressBar1);
        	this.groupBox5.Controls.Add(this.label15);
        	this.groupBox5.Controls.Add(this.label13);
        	this.groupBox5.Controls.Add(this.label14);
        	this.groupBox5.Controls.Add(this.label12);
        	this.groupBox5.ForeColor = System.Drawing.Color.DarkGoldenrod;
        	this.groupBox5.Location = new System.Drawing.Point(7, 146);
        	this.groupBox5.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
        	this.groupBox5.Name = "groupBox5";
        	this.groupBox5.Padding = new System.Windows.Forms.Padding(3, 2, 3, 2);
        	this.groupBox5.Size = new System.Drawing.Size(471, 89);
        	this.groupBox5.TabIndex = 31;
        	this.groupBox5.TabStop = false;
        	this.groupBox5.Text = "信息";
        	// 
        	// label15
        	// 
        	this.label15.Image = ((System.Drawing.Image)(resources.GetObject("label15.Image")));
        	this.label15.Location = new System.Drawing.Point(7, 19);
        	this.label15.Name = "label15";
        	this.label15.Size = new System.Drawing.Size(27, 26);
        	this.label15.TabIndex = 30;
        	// 
        	// label13
        	// 
        	this.label13.BackColor = System.Drawing.Color.Transparent;
        	this.label13.Font = new System.Drawing.Font("宋体", 10.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
        	this.label13.ForeColor = System.Drawing.Color.Olive;
        	this.label13.Location = new System.Drawing.Point(38, 15);
        	this.label13.Name = "label13";
        	this.label13.Size = new System.Drawing.Size(424, 24);
        	this.label13.TabIndex = 27;
        	// 
        	// label14
        	// 
        	this.label14.BackColor = System.Drawing.Color.Transparent;
        	this.label14.Font = new System.Drawing.Font("宋体", 10.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
        	this.label14.ForeColor = System.Drawing.Color.OrangeRed;
        	this.label14.Location = new System.Drawing.Point(38, 39);
        	this.label14.Name = "label14";
        	this.label14.Size = new System.Drawing.Size(424, 24);
        	this.label14.TabIndex = 28;
        	this.label14.Text = "等待命令...";
        	this.label14.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
        	// 
        	// label12
        	// 
        	this.label12.ForeColor = System.Drawing.Color.Olive;
        	this.label12.Location = new System.Drawing.Point(5, 72);
        	this.label12.Name = "label12";
        	this.label12.Size = new System.Drawing.Size(41, 12);
        	this.label12.TabIndex = 18;
        	this.label12.Text = "进度:";
        	this.label12.TextAlign = System.Drawing.ContentAlignment.BottomLeft;
        	// 
        	// DownLoadButton
        	// 
        	this.DownLoadButton.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
        	this.DownLoadButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
        	this.DownLoadButton.Font = new System.Drawing.Font("宋体", 15F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
        	this.DownLoadButton.ForeColor = System.Drawing.Color.Green;
        	this.DownLoadButton.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
        	this.DownLoadButton.Location = new System.Drawing.Point(359, 13);
        	this.DownLoadButton.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
        	this.DownLoadButton.Name = "DownLoadButton";
        	this.DownLoadButton.Size = new System.Drawing.Size(118, 38);
        	this.DownLoadButton.TabIndex = 18;
        	this.DownLoadButton.Text = "自动下载";
        	this.DownLoadButton.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
        	this.DownLoadButton.UseVisualStyleBackColor = false;
        	this.DownLoadButton.Click += new System.EventHandler(this.DownLoadButtonClick);
        	this.DownLoadButton.Paint += new System.Windows.Forms.PaintEventHandler(this.DownLoadButtonPaint);
        	// 
        	// groupBox3
        	// 
        	this.groupBox3.Controls.Add(this.EraseButton);
        	this.groupBox3.Controls.Add(this.WriteFlashButton);
        	this.groupBox3.Controls.Add(this.ReadFlashButton);
        	this.groupBox3.Controls.Add(this.CheckFlashButton);
        	this.groupBox3.Controls.Add(this.CheckEmptyButton);
        	this.groupBox3.ForeColor = System.Drawing.Color.DodgerBlue;
        	this.groupBox3.Location = new System.Drawing.Point(129, 7);
        	this.groupBox3.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
        	this.groupBox3.Name = "groupBox3";
        	this.groupBox3.Padding = new System.Windows.Forms.Padding(3, 2, 3, 2);
        	this.groupBox3.Size = new System.Drawing.Size(103, 134);
        	this.groupBox3.TabIndex = 26;
        	this.groupBox3.TabStop = false;
        	this.groupBox3.Text = "编程操作";
        	// 
        	// CheckFlashButton
        	// 
        	this.CheckFlashButton.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(192)))), ((int)(((byte)(255)))));
        	this.CheckFlashButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
        	this.CheckFlashButton.ForeColor = System.Drawing.Color.Blue;
        	this.CheckFlashButton.Location = new System.Drawing.Point(10, 39);
        	this.CheckFlashButton.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
        	this.CheckFlashButton.Name = "CheckFlashButton";
        	this.CheckFlashButton.Size = new System.Drawing.Size(75, 20);
        	this.CheckFlashButton.TabIndex = 17;
        	this.CheckFlashButton.Text = "校验Flash";
        	this.CheckFlashButton.UseVisualStyleBackColor = false;
        	this.CheckFlashButton.Click += new System.EventHandler(this.CheckFlashButtonClick);
        	// 
        	// CheckEmptyButton
        	// 
        	this.CheckEmptyButton.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(192)))), ((int)(((byte)(255)))));
        	this.CheckEmptyButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
        	this.CheckEmptyButton.ForeColor = System.Drawing.Color.Blue;
        	this.CheckEmptyButton.Location = new System.Drawing.Point(10, 16);
        	this.CheckEmptyButton.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
        	this.CheckEmptyButton.Name = "CheckEmptyButton";
        	this.CheckEmptyButton.Size = new System.Drawing.Size(75, 20);
        	this.CheckEmptyButton.TabIndex = 16;
        	this.CheckEmptyButton.Text = "检查空片";
        	this.CheckEmptyButton.UseVisualStyleBackColor = false;
        	this.CheckEmptyButton.Click += new System.EventHandler(this.CheckEmptyButtonClick);
        	// 
        	// groupBox2
        	// 
        	this.groupBox2.Controls.Add(this.DataChangeBox);
        	this.groupBox2.Controls.Add(this.CheckFlashBox);
        	this.groupBox2.Controls.Add(this.CheckIDBox);
        	this.groupBox2.Controls.Add(this.WriteFlashBox);
        	this.groupBox2.Controls.Add(this.EraseBox);
        	this.groupBox2.Controls.Add(this.CheckEmptyBox);
        	this.groupBox2.ForeColor = System.Drawing.Color.Green;
        	this.groupBox2.Location = new System.Drawing.Point(238, 7);
        	this.groupBox2.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
        	this.groupBox2.Name = "groupBox2";
        	this.groupBox2.Padding = new System.Windows.Forms.Padding(3, 2, 3, 2);
        	this.groupBox2.Size = new System.Drawing.Size(116, 134);
        	this.groupBox2.TabIndex = 25;
        	this.groupBox2.TabStop = false;
        	this.groupBox2.Text = "自动下载选项";
        	// 
        	// DataChangeBox
        	// 
        	this.DataChangeBox.ForeColor = System.Drawing.Color.Black;
        	this.DataChangeBox.Location = new System.Drawing.Point(14, 13);
        	this.DataChangeBox.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
        	this.DataChangeBox.Name = "DataChangeBox";
        	this.DataChangeBox.Size = new System.Drawing.Size(96, 19);
        	this.DataChangeBox.TabIndex = 27;
        	this.DataChangeBox.Text = "文件改变重载";
        	this.DataChangeBox.UseVisualStyleBackColor = true;
        	this.DataChangeBox.CheckedChanged += new System.EventHandler(this.ConfigChanged);
        	this.DataChangeBox.Paint += new System.Windows.Forms.PaintEventHandler(this.CheckBoxPaint);
        	// 
        	// CheckFlashBox
        	// 
        	this.CheckFlashBox.BackColor = System.Drawing.Color.Transparent;
        	this.CheckFlashBox.ForeColor = System.Drawing.Color.Black;
        	this.CheckFlashBox.Location = new System.Drawing.Point(14, 109);
        	this.CheckFlashBox.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
        	this.CheckFlashBox.Name = "CheckFlashBox";
        	this.CheckFlashBox.Size = new System.Drawing.Size(96, 19);
        	this.CheckFlashBox.TabIndex = 7;
        	this.CheckFlashBox.Text = "校验Flash";
        	this.CheckFlashBox.UseVisualStyleBackColor = false;
        	this.CheckFlashBox.CheckedChanged += new System.EventHandler(this.ConfigChanged);
        	this.CheckFlashBox.Paint += new System.Windows.Forms.PaintEventHandler(this.CheckBoxPaint);
        	// 
        	// CheckIDBox
        	// 
        	this.CheckIDBox.ForeColor = System.Drawing.Color.Black;
        	this.CheckIDBox.Location = new System.Drawing.Point(14, 32);
        	this.CheckIDBox.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
        	this.CheckIDBox.Name = "CheckIDBox";
        	this.CheckIDBox.Size = new System.Drawing.Size(96, 19);
        	this.CheckIDBox.TabIndex = 0;
        	this.CheckIDBox.Text = "比较芯片ID";
        	this.CheckIDBox.UseVisualStyleBackColor = true;
        	this.CheckIDBox.CheckedChanged += new System.EventHandler(this.ConfigChanged);
        	this.CheckIDBox.Paint += new System.Windows.Forms.PaintEventHandler(this.CheckBoxPaint);
        	// 
        	// WriteFlashBox
        	// 
        	this.WriteFlashBox.ForeColor = System.Drawing.Color.Black;
        	this.WriteFlashBox.Location = new System.Drawing.Point(14, 90);
        	this.WriteFlashBox.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
        	this.WriteFlashBox.Name = "WriteFlashBox";
        	this.WriteFlashBox.Size = new System.Drawing.Size(96, 19);
        	this.WriteFlashBox.TabIndex = 2;
        	this.WriteFlashBox.Text = "写入Flash";
        	this.WriteFlashBox.UseVisualStyleBackColor = true;
        	this.WriteFlashBox.CheckedChanged += new System.EventHandler(this.ConfigChanged);
        	this.WriteFlashBox.Paint += new System.Windows.Forms.PaintEventHandler(this.CheckBoxPaint);
        	// 
        	// EraseBox
        	// 
        	this.EraseBox.ForeColor = System.Drawing.Color.Black;
        	this.EraseBox.Location = new System.Drawing.Point(14, 51);
        	this.EraseBox.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
        	this.EraseBox.Name = "EraseBox";
        	this.EraseBox.Size = new System.Drawing.Size(96, 19);
        	this.EraseBox.TabIndex = 1;
        	this.EraseBox.Text = "擦除Flash";
        	this.EraseBox.UseVisualStyleBackColor = true;
        	this.EraseBox.CheckedChanged += new System.EventHandler(this.ConfigChanged);
        	this.EraseBox.Paint += new System.Windows.Forms.PaintEventHandler(this.CheckBoxPaint);
        	// 
        	// CheckEmptyBox
        	// 
        	this.CheckEmptyBox.ForeColor = System.Drawing.Color.Black;
        	this.CheckEmptyBox.Location = new System.Drawing.Point(14, 70);
        	this.CheckEmptyBox.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
        	this.CheckEmptyBox.Name = "CheckEmptyBox";
        	this.CheckEmptyBox.Size = new System.Drawing.Size(96, 19);
        	this.CheckEmptyBox.TabIndex = 3;
        	this.CheckEmptyBox.Text = "检查空片";
        	this.CheckEmptyBox.UseVisualStyleBackColor = true;
        	this.CheckEmptyBox.CheckedChanged += new System.EventHandler(this.ConfigChanged);
        	this.CheckEmptyBox.Paint += new System.Windows.Forms.PaintEventHandler(this.CheckBoxPaint);
        	// 
        	// groupBox1
        	// 
        	this.groupBox1.Controls.Add(this.OpenFileButton);
        	this.groupBox1.Controls.Add(this.LaterFileButton);
        	this.groupBox1.Controls.Add(this.ReloadFileButton);
        	this.groupBox1.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
        	this.groupBox1.Location = new System.Drawing.Point(7, 7);
        	this.groupBox1.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
        	this.groupBox1.Name = "groupBox1";
        	this.groupBox1.Padding = new System.Windows.Forms.Padding(3, 2, 3, 2);
        	this.groupBox1.Size = new System.Drawing.Size(117, 134);
        	this.groupBox1.TabIndex = 24;
        	this.groupBox1.TabStop = false;
        	this.groupBox1.Text = "载入文件";
        	// 
        	// OpenFileButton
        	// 
        	this.OpenFileButton.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(224)))), ((int)(((byte)(192)))));
        	this.OpenFileButton.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
        	this.OpenFileButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
        	this.OpenFileButton.Font = new System.Drawing.Font("宋体", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
        	this.OpenFileButton.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
        	this.OpenFileButton.Location = new System.Drawing.Point(7, 18);
        	this.OpenFileButton.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
        	this.OpenFileButton.Name = "OpenFileButton";
        	this.OpenFileButton.Size = new System.Drawing.Size(89, 26);
        	this.OpenFileButton.TabIndex = 29;
        	this.OpenFileButton.Text = "打开文件";
        	this.OpenFileButton.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
        	this.OpenFileButton.UseVisualStyleBackColor = false;
        	this.OpenFileButton.Click += new System.EventHandler(this.OpenFileButton_Click);
        	// 
        	// LaterFileButton
        	// 
        	this.LaterFileButton.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(224)))), ((int)(((byte)(192)))));
        	this.LaterFileButton.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
        	this.LaterFileButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
        	this.LaterFileButton.Font = new System.Drawing.Font("宋体", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
        	this.LaterFileButton.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
        	this.LaterFileButton.Location = new System.Drawing.Point(7, 51);
        	this.LaterFileButton.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
        	this.LaterFileButton.Name = "LaterFileButton";
        	this.LaterFileButton.Size = new System.Drawing.Size(89, 26);
        	this.LaterFileButton.TabIndex = 28;
        	this.LaterFileButton.Text = "近期文件";
        	this.LaterFileButton.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
        	this.LaterFileButton.UseVisualStyleBackColor = false;
        	this.LaterFileButton.Click += new System.EventHandler(this.Button3Click);
        	this.LaterFileButton.Paint += new System.Windows.Forms.PaintEventHandler(this.LaterFileButtonPaint);
        	// 
        	// ReloadFileButton
        	// 
        	this.ReloadFileButton.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(224)))), ((int)(((byte)(192)))));
        	this.ReloadFileButton.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
        	this.ReloadFileButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
        	this.ReloadFileButton.Font = new System.Drawing.Font("宋体", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
        	this.ReloadFileButton.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
        	this.ReloadFileButton.Location = new System.Drawing.Point(7, 83);
        	this.ReloadFileButton.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
        	this.ReloadFileButton.Name = "ReloadFileButton";
        	this.ReloadFileButton.Size = new System.Drawing.Size(89, 26);
        	this.ReloadFileButton.TabIndex = 27;
        	this.ReloadFileButton.Text = "重载文件";
        	this.ReloadFileButton.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
        	this.ReloadFileButton.UseVisualStyleBackColor = false;
        	this.ReloadFileButton.Click += new System.EventHandler(this.Button4Click);
        	this.ReloadFileButton.Paint += new System.Windows.Forms.PaintEventHandler(this.ReloadFileButtonPaint);
        	// 
        	// tabPage3
        	// 
        	this.tabPage3.Controls.Add(this.panel1);
        	this.tabPage3.Controls.Add(this.label10);
        	this.tabPage3.Location = new System.Drawing.Point(4, 24);
        	this.tabPage3.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
        	this.tabPage3.Name = "tabPage3";
        	this.tabPage3.Padding = new System.Windows.Forms.Padding(3, 2, 3, 2);
        	this.tabPage3.Size = new System.Drawing.Size(499, 237);
        	this.tabPage3.TabIndex = 2;
        	this.tabPage3.Text = "代码编辑";
        	this.tabPage3.UseVisualStyleBackColor = true;
        	// 
        	// label10
        	// 
        	this.label10.BackColor = System.Drawing.Color.WhiteSmoke;
        	this.label10.Dock = System.Windows.Forms.DockStyle.Top;
        	this.label10.Font = new System.Drawing.Font("宋体", 10.5F);
        	this.label10.ForeColor = System.Drawing.Color.OrangeRed;
        	this.label10.Location = new System.Drawing.Point(3, 2);
        	this.label10.Name = "label10";
        	this.label10.Size = new System.Drawing.Size(495, 18);
        	this.label10.TabIndex = 21;
        	this.label10.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
        	// 
        	// tabPage2
        	// 
        	this.tabPage2.BackColor = System.Drawing.Color.WhiteSmoke;
        	this.tabPage2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
        	this.tabPage2.Controls.Add(this.textBoxHigh);
        	this.tabPage2.Controls.Add(this.textBoxLow);
        	this.tabPage2.Controls.Add(this.label26);
        	this.tabPage2.Controls.Add(this.label27);
        	this.tabPage2.Controls.Add(this.button写入熔丝);
        	this.tabPage2.Controls.Add(this.groupBox6);
        	this.tabPage2.Location = new System.Drawing.Point(4, 24);
        	this.tabPage2.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
        	this.tabPage2.Name = "tabPage2";
        	this.tabPage2.Padding = new System.Windows.Forms.Padding(3, 2, 3, 2);
        	this.tabPage2.Size = new System.Drawing.Size(499, 237);
        	this.tabPage2.TabIndex = 3;
        	this.tabPage2.Text = "熔丝设置";
        	// 
        	// textBoxHigh
        	// 
        	this.textBoxHigh.Location = new System.Drawing.Point(363, 86);
        	this.textBoxHigh.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
        	this.textBoxHigh.Name = "textBoxHigh";
        	this.textBoxHigh.Size = new System.Drawing.Size(31, 23);
        	this.textBoxHigh.TabIndex = 25;
        	this.textBoxHigh.Text = "C7";
        	this.textBoxHigh.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
        	// 
        	// textBoxLow
        	// 
        	this.textBoxLow.Location = new System.Drawing.Point(363, 52);
        	this.textBoxLow.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
        	this.textBoxLow.Name = "textBoxLow";
        	this.textBoxLow.Size = new System.Drawing.Size(31, 23);
        	this.textBoxLow.TabIndex = 24;
        	this.textBoxLow.Text = "FF";
        	this.textBoxLow.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
        	// 
        	// label26
        	// 
        	this.label26.Font = new System.Drawing.Font("宋体", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
        	this.label26.Location = new System.Drawing.Point(274, 89);
        	this.label26.Name = "label26";
        	this.label26.Size = new System.Drawing.Size(84, 18);
        	this.label26.TabIndex = 23;
        	this.label26.Text = "熔丝高位:";
        	// 
        	// label27
        	// 
        	this.label27.Font = new System.Drawing.Font("宋体", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
        	this.label27.Location = new System.Drawing.Point(274, 55);
        	this.label27.Name = "label27";
        	this.label27.Size = new System.Drawing.Size(84, 18);
        	this.label27.TabIndex = 22;
        	this.label27.Text = "熔丝低位:";
        	// 
        	// button写入熔丝
        	// 
        	this.button写入熔丝.Location = new System.Drawing.Point(274, 23);
        	this.button写入熔丝.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
        	this.button写入熔丝.Name = "button写入熔丝";
        	this.button写入熔丝.Size = new System.Drawing.Size(64, 18);
        	this.button写入熔丝.TabIndex = 3;
        	this.button写入熔丝.Text = "写入熔丝";
        	this.button写入熔丝.UseVisualStyleBackColor = true;
        	this.button写入熔丝.Click += new System.EventHandler(this.Button写入熔丝Click);
        	// 
        	// groupBox6
        	// 
        	this.groupBox6.Controls.Add(this.label25);
        	this.groupBox6.Controls.Add(this.label24);
        	this.groupBox6.Controls.Add(this.button1);
        	this.groupBox6.Controls.Add(this.label23);
        	this.groupBox6.Controls.Add(this.label22);
        	this.groupBox6.Controls.Add(this.label21);
        	this.groupBox6.Controls.Add(this.label20);
        	this.groupBox6.Controls.Add(this.label19);
        	this.groupBox6.Controls.Add(this.label18);
        	this.groupBox6.Controls.Add(this.label17);
        	this.groupBox6.Controls.Add(this.label16);
        	this.groupBox6.Controls.Add(this.label9);
        	this.groupBox6.Controls.Add(this.label8);
        	this.groupBox6.Controls.Add(this.label7);
        	this.groupBox6.Controls.Add(this.label6);
        	this.groupBox6.Controls.Add(this.label5);
        	this.groupBox6.Controls.Add(this.label4);
        	this.groupBox6.Controls.Add(this.label3);
        	this.groupBox6.Controls.Add(this.label2);
        	this.groupBox6.Controls.Add(this.label1);
        	this.groupBox6.Location = new System.Drawing.Point(7, 5);
        	this.groupBox6.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
        	this.groupBox6.Name = "groupBox6";
        	this.groupBox6.Padding = new System.Windows.Forms.Padding(3, 2, 3, 2);
        	this.groupBox6.Size = new System.Drawing.Size(183, 221);
        	this.groupBox6.TabIndex = 1;
        	this.groupBox6.TabStop = false;
        	this.groupBox6.Text = "目标板芯片配置";
        	// 
        	// label25
        	// 
        	this.label25.Image = ((System.Drawing.Image)(resources.GetObject("label25.Image")));
        	this.label25.Location = new System.Drawing.Point(137, 13);
        	this.label25.Name = "label25";
        	this.label25.Size = new System.Drawing.Size(34, 32);
        	this.label25.TabIndex = 2;
        	// 
        	// label24
        	// 
        	this.label24.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(64)))), ((int)(((byte)(0)))));
        	this.label24.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
        	this.label24.Font = new System.Drawing.Font("Courier New", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
        	this.label24.ForeColor = System.Drawing.Color.Lime;
        	this.label24.Location = new System.Drawing.Point(94, 194);
        	this.label24.Name = "label24";
        	this.label24.Size = new System.Drawing.Size(81, 19);
        	this.label24.TabIndex = 21;
        	this.label24.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
        	// 
        	// button1
        	// 
        	this.button1.Location = new System.Drawing.Point(7, 19);
        	this.button1.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
        	this.button1.Name = "button1";
        	this.button1.Size = new System.Drawing.Size(102, 18);
        	this.button1.TabIndex = 0;
        	this.button1.Text = "读出芯片配置";
        	this.button1.UseVisualStyleBackColor = true;
        	this.button1.Click += new System.EventHandler(this.Button1Click);
        	// 
        	// label23
        	// 
        	this.label23.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(64)))), ((int)(((byte)(0)))));
        	this.label23.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
        	this.label23.Font = new System.Drawing.Font("Courier New", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
        	this.label23.ForeColor = System.Drawing.Color.Lime;
        	this.label23.Location = new System.Drawing.Point(94, 176);
        	this.label23.Name = "label23";
        	this.label23.Size = new System.Drawing.Size(81, 19);
        	this.label23.TabIndex = 20;
        	this.label23.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
        	// 
        	// label22
        	// 
        	this.label22.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(64)))), ((int)(((byte)(0)))));
        	this.label22.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
        	this.label22.Font = new System.Drawing.Font("Courier New", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
        	this.label22.ForeColor = System.Drawing.Color.Lime;
        	this.label22.Location = new System.Drawing.Point(94, 158);
        	this.label22.Name = "label22";
        	this.label22.Size = new System.Drawing.Size(81, 19);
        	this.label22.TabIndex = 19;
        	this.label22.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
        	// 
        	// label21
        	// 
        	this.label21.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(64)))), ((int)(((byte)(0)))));
        	this.label21.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
        	this.label21.Font = new System.Drawing.Font("Courier New", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
        	this.label21.ForeColor = System.Drawing.Color.Lime;
        	this.label21.Location = new System.Drawing.Point(94, 139);
        	this.label21.Name = "label21";
        	this.label21.Size = new System.Drawing.Size(81, 19);
        	this.label21.TabIndex = 18;
        	this.label21.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
        	// 
        	// label20
        	// 
        	this.label20.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(64)))), ((int)(((byte)(0)))));
        	this.label20.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
        	this.label20.Font = new System.Drawing.Font("Courier New", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
        	this.label20.ForeColor = System.Drawing.Color.Lime;
        	this.label20.Location = new System.Drawing.Point(94, 121);
        	this.label20.Name = "label20";
        	this.label20.Size = new System.Drawing.Size(81, 19);
        	this.label20.TabIndex = 17;
        	this.label20.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
        	// 
        	// label19
        	// 
        	this.label19.Font = new System.Drawing.Font("宋体", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
        	this.label19.Location = new System.Drawing.Point(5, 196);
        	this.label19.Name = "label19";
        	this.label19.Size = new System.Drawing.Size(84, 18);
        	this.label19.TabIndex = 16;
        	this.label19.Text = "时钟校正4:";
        	// 
        	// label18
        	// 
        	this.label18.Font = new System.Drawing.Font("宋体", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
        	this.label18.Location = new System.Drawing.Point(5, 159);
        	this.label18.Name = "label18";
        	this.label18.Size = new System.Drawing.Size(84, 18);
        	this.label18.TabIndex = 15;
        	this.label18.Text = "时钟校正2:";
        	// 
        	// label17
        	// 
        	this.label17.Font = new System.Drawing.Font("宋体", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
        	this.label17.Location = new System.Drawing.Point(5, 178);
        	this.label17.Name = "label17";
        	this.label17.Size = new System.Drawing.Size(84, 18);
        	this.label17.TabIndex = 14;
        	this.label17.Text = "时钟校正3:";
        	// 
        	// label16
        	// 
        	this.label16.Font = new System.Drawing.Font("宋体", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
        	this.label16.Location = new System.Drawing.Point(5, 122);
        	this.label16.Name = "label16";
        	this.label16.Size = new System.Drawing.Size(84, 18);
        	this.label16.TabIndex = 13;
        	this.label16.Text = "熔丝扩展位:";
        	// 
        	// label9
        	// 
        	this.label9.Font = new System.Drawing.Font("宋体", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
        	this.label9.Location = new System.Drawing.Point(5, 104);
        	this.label9.Name = "label9";
        	this.label9.Size = new System.Drawing.Size(84, 18);
        	this.label9.TabIndex = 12;
        	this.label9.Text = "熔丝高位:";
        	// 
        	// label8
        	// 
        	this.label8.Font = new System.Drawing.Font("宋体", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
        	this.label8.Location = new System.Drawing.Point(5, 141);
        	this.label8.Name = "label8";
        	this.label8.Size = new System.Drawing.Size(84, 18);
        	this.label8.TabIndex = 11;
        	this.label8.Text = "时钟校正1:";
        	// 
        	// label7
        	// 
        	this.label7.Font = new System.Drawing.Font("宋体", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
        	this.label7.Location = new System.Drawing.Point(5, 86);
        	this.label7.Name = "label7";
        	this.label7.Size = new System.Drawing.Size(84, 18);
        	this.label7.TabIndex = 10;
        	this.label7.Text = "熔丝低位:";
        	// 
        	// label6
        	// 
        	this.label6.Font = new System.Drawing.Font("宋体", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
        	this.label6.Location = new System.Drawing.Point(5, 67);
        	this.label6.Name = "label6";
        	this.label6.Size = new System.Drawing.Size(84, 18);
        	this.label6.TabIndex = 9;
        	this.label6.Text = "加密位:";
        	// 
        	// label5
        	// 
        	this.label5.Font = new System.Drawing.Font("宋体", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
        	this.label5.Location = new System.Drawing.Point(5, 49);
        	this.label5.Name = "label5";
        	this.label5.Size = new System.Drawing.Size(84, 18);
        	this.label5.TabIndex = 2;
        	this.label5.Text = "芯片ID:";
        	// 
        	// label4
        	// 
        	this.label4.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(64)))), ((int)(((byte)(0)))));
        	this.label4.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
        	this.label4.Font = new System.Drawing.Font("Courier New", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
        	this.label4.ForeColor = System.Drawing.Color.Lime;
        	this.label4.Location = new System.Drawing.Point(94, 102);
        	this.label4.Name = "label4";
        	this.label4.Size = new System.Drawing.Size(81, 19);
        	this.label4.TabIndex = 8;
        	this.label4.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
        	// 
        	// label3
        	// 
        	this.label3.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(64)))), ((int)(((byte)(0)))));
        	this.label3.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
        	this.label3.Font = new System.Drawing.Font("Courier New", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
        	this.label3.ForeColor = System.Drawing.Color.Lime;
        	this.label3.Location = new System.Drawing.Point(94, 84);
        	this.label3.Name = "label3";
        	this.label3.Size = new System.Drawing.Size(81, 19);
        	this.label3.TabIndex = 6;
        	this.label3.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
        	// 
        	// label2
        	// 
        	this.label2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(64)))), ((int)(((byte)(0)))));
        	this.label2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
        	this.label2.Font = new System.Drawing.Font("Courier New", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
        	this.label2.ForeColor = System.Drawing.Color.Lime;
        	this.label2.Location = new System.Drawing.Point(94, 66);
        	this.label2.Name = "label2";
        	this.label2.Size = new System.Drawing.Size(81, 19);
        	this.label2.TabIndex = 4;
        	this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
        	// 
        	// label1
        	// 
        	this.label1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(64)))), ((int)(((byte)(0)))));
        	this.label1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
        	this.label1.Font = new System.Drawing.Font("Courier New", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
        	this.label1.ForeColor = System.Drawing.Color.Lime;
        	this.label1.Location = new System.Drawing.Point(94, 47);
        	this.label1.Name = "label1";
        	this.label1.Size = new System.Drawing.Size(81, 19);
        	this.label1.TabIndex = 2;
        	this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
        	// 
        	// timer1
        	// 
        	this.timer1.Tick += new System.EventHandler(this.Timer1Tick);
        	// 
        	// timer2
        	// 
        	this.timer2.Tick += new System.EventHandler(this.Timer2Tick);
        	// 
        	// ISPForm
        	// 
        	this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 15F);
        	this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
        	this.BackColor = System.Drawing.Color.LightGray;
        	this.ClientSize = new System.Drawing.Size(592, 331);
        	this.Controls.Add(this.tabControl1);
        	this.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
        	this.ForeColor = System.Drawing.Color.MidnightBlue;
        	this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
        	this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
        	this.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
        	this.Name = "ISPForm";
        	this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
        	this.Text = "ISP下载器";
        	this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.Form1FormClosing);
        	this.panel1.ResumeLayout(false);
        	this.tabControl1.ResumeLayout(false);
        	this.tabPage1.ResumeLayout(false);
        	this.groupBox5.ResumeLayout(false);
        	this.groupBox3.ResumeLayout(false);
        	this.groupBox2.ResumeLayout(false);
        	this.groupBox1.ResumeLayout(false);
        	this.tabPage3.ResumeLayout(false);
        	this.tabPage2.ResumeLayout(false);
        	this.tabPage2.PerformLayout();
        	this.groupBox6.ResumeLayout(false);
        	this.ResumeLayout(false);
        }
        private System.Windows.Forms.TextBox textBoxLow;
        private System.Windows.Forms.TextBox textBoxHigh;
        private System.Windows.Forms.Label label27;
        private System.Windows.Forms.Label label26;
        private System.Windows.Forms.Button button写入熔丝;
        private System.Windows.Forms.ComboBox 芯片选择comboBox;
        public System.Windows.Forms.Timer timer2;
        private System.Windows.Forms.Timer timer1;
        private System.Windows.Forms.Label label25;
        private System.Windows.Forms.Button ReloadFileButton;
        private System.Windows.Forms.Button LaterFileButton;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.Label label23;
        private System.Windows.Forms.Label label24;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.GroupBox groupBox6;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.CheckBox CheckEmptyBox;
        private System.Windows.Forms.CheckBox WriteFlashBox;
        private System.Windows.Forms.CheckBox EraseBox;
        private System.Windows.Forms.CheckBox CheckIDBox;
        private System.Windows.Forms.CheckBox CheckFlashBox;
        private System.Windows.Forms.CheckBox DataChangeBox;
        private System.Windows.Forms.GroupBox groupBox5;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Button CheckFlashButton;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Button ReadFlashButton;
        private System.Windows.Forms.Button EraseButton;
        private System.Windows.Forms.Button WriteFlashButton;
        private System.Windows.Forms.Button OpenFileButton;
        private System.Windows.Forms.Button DownLoadButton;
        private System.Windows.Forms.Button CheckEmptyButton;
        public System.Windows.Forms.RichTextBox CodeText;
        private System.Windows.Forms.Label LineLabel;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.TabPage tabPage3;
        private System.Windows.Forms.TabPage tabPage2;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.ProgressBar progressBar1;

        #endregion

    }
}

