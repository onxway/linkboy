﻿
namespace n_USBASP_LaterFileForm
{
using System;
using System.IO;
using System.Net;
using System.Text;
using System.Windows.Forms;
using System.Drawing;
using n_CodeBox;
using n_USBASP;
using T;
using n_OS;

/// <summary>
/// Description of WorkForm.
/// </summary>
public partial class LaterHexFileForm : Form
{
	Color HoverColor;
	Color LeaveColor;
	Font HoverFont;
	Font LeaveFont;
	bool isLoading;
	
	//链接标签列表
	public LinkLabel[] links;
	
	//主窗口
	public LaterHexFileForm()
	{
		//
		// The InitializeComponent() call is required for Windows Forms designer support.
		//
		InitializeComponent();
		this.MinimumSize = this.Size;
		this.MaximumSize = this.Size;
		HoverColor = Color.FromArgb(255, 128, 64);
		LeaveColor = Color.FromArgb(0, 128, 255);
		HoverFont = new Font( "宋体", 10, FontStyle.Bold );
		LeaveFont = new Font( "宋体", 11, FontStyle.Regular );
		//初始化
		//
		// TODO: Add constructor code after the InitializeComponent() call.
		//
	}
	
	//设置文件列表
	public void Run()
	{
		string s = VIO.OpenTextFileGB2312( OS.SystemRoot + "Resource" + OS.PATH_S + "下载器" + OS.PATH_S + "近期文件路径.lst" );
		if( s.StartsWith( "<end>" ) ) {
			MessageBox.Show( "没有近期文件" );
			return;
		}
		s = s.Remove( s.IndexOf( "\n<end>" ) );
		string[] Lines = s.Split( '\n' );
		
		//移除上一次的链接控件
		if( links != null ) {
			for( int i = 0; i < links.Length; ++i ) {
				links[ i ].Dispose();
			}
		}
		links = new LinkLabel[ Lines.Length ];
		for( int i = 0; i < Lines.Length; ++i ) {
			
			//获取文件路径和文件名
			string Path = Lines[ i ];
			string FileName = Path.Remove( 0, Path.LastIndexOf( OS.PATH_S ) + 1 );
			
			//处理链接标签项
			links[ i ] = new LinkLabel();
			links[ i ].AutoSize = true;
			links[ i ].LinkBehavior = LinkBehavior.HoverUnderline;
			links[ i ].TextAlign = ContentAlignment.MiddleLeft;
			links[ i ].Location = new Point( 5, i * 19 + 35 );
			links[ i ].Text = ( ( i + 1 ) + "" ).ToString().PadRight( 3, ' ' ) +
				FileName + " (" + Path + ")";
			links[ i ].Name = Path;
			if( !File.Exists( Lines[ i ] ) ) {
				links[ i ].Enabled = false;
			}
			links[ i ].Font = LeaveFont;
			links[ i ].BackColor = Color.Transparent;
			links[ i ].LinkColor = LeaveColor;
			links[ i ].ActiveLinkColor = Color.LimeGreen;
			links[ i ].BorderStyle = BorderStyle.None;
			links[ i ].TextAlign = ContentAlignment.MiddleLeft;
			links[ i ].LinkClicked += new LinkLabelLinkClickedEventHandler( LinksClick );
			links[ i ].MouseEnter += new EventHandler( LinkLabelMouseEnter );
			links[ i ].MouseLeave += new EventHandler( LinkLabelMouseLeave );
			this.Controls.Add( links[ i ] );
		}
		//显示界面
		this.Opacity = 0;
		this.Visible = true;
		isLoading = true;
		this.timer1.Enabled = true;
	}
	
	//获取最后的文件, 没有返回null
	public string GetLatestFile()
	{
		string text = VIO.OpenTextFileGB2312( OS.SystemRoot + "Resource" + OS.PATH_S + "下载器" + OS.PATH_S + "近期文件路径.lst" );
		if( text.StartsWith( "<end>\n" ) ) {
			return null;
		}
		return text.Split( '\n' )[ 0 ];
	}
	
	//添加一个文件
	public void AddFile( string CurrentFile )
	{
		string LaterFilePath = OS.SystemRoot + "Resource" + OS.PATH_S + "下载器" + OS.PATH_S + "近期文件路径.lst";
		string text = VIO.OpenTextFileGB2312( LaterFilePath );
		string[] Lines = text.Split( '\n' );
		text = CurrentFile + "\n";
		for( int i = 0; i < Lines.Length; ++i ) {
			if( i == 20 ) {
				break;
			}
			if( Lines[ i ] != CurrentFile && File.Exists( Lines[ i ] ) ) {
				text += Lines[ i ] + "\n";
			}
		}
		text += "<end>";
		VIO.SaveTextFileGB2312( LaterFilePath, text );
	}
	
	//文件链接事件
	void LinksClick(object sender, LinkLabelLinkClickedEventArgs e)
	{
		this.Visible = false;
		string CurrentFile = ( (LinkLabel)sender ).Name;
		string text = VIO.OpenTextFileGB2312( CurrentFile );
		OutputBox.ShowMessage( "已载入文件: " + CurrentFile );
		T.ISPBox.SetLockMessage( CurrentFile );
		T.ISPBox.LastWriteTime = File.GetLastWriteTime( CurrentFile );
		AddFile( CurrentFile );
		CodeBox.SetCodeText( text );
	}
	
	//鼠标进入链接
	void LinkLabelMouseEnter(object sender, EventArgs e)
	{
		( (LinkLabel)sender ).LinkColor = HoverColor;
		//( (LinkLabel)sender ).BackColor = Color.FromArgb(168, 227, 168);
		//( (LinkLabel)sender ).BorderStyle = BorderStyle.FixedSingle;
		( (LinkLabel)sender ).Font = HoverFont;
	}
	
	//鼠标离开链接
	void LinkLabelMouseLeave(object sender, EventArgs e)
	{
		( (LinkLabel)sender ).LinkColor = LeaveColor;
		//( (LinkLabel)sender ).BackColor = Color.Transparent;
		//( (LinkLabel)sender ).BorderStyle = BorderStyle.None;
		( (LinkLabel)sender ).Font = LeaveFont;
	}
	
	//定时
	void Timer1Tick(object sender, EventArgs e)
	{
		if( isLoading ) {
			this.Opacity += 0.15f;
			if( this.Opacity >= 0.9f ) {
				this.timer1.Enabled = false;
			}
		}
		else {
			this.Opacity -= 0.15f;
			if( this.Opacity <= 0.1f ) {
				this.timer1.Enabled = false;
				this.Visible = false;
			}
		}
	}
	
	//清空近期文件
	void Button2Click(object sender, EventArgs e)
	{
		VIO.SaveTextFileGB2312( OS.SystemRoot + "Resource" + OS.PATH_S + "下载器" + OS.PATH_S + "近期文件路径.lst", "<end>\n" );
		isLoading = false;
		this.timer1.Enabled = true;
	}
	
	void LaterFileFormFormClosing(object sender, FormClosingEventArgs e)
	{
		isLoading = false;
		this.timer1.Enabled = true;
		e.Cancel = true;
	}
}
}
