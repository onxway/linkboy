﻿
namespace n_LaterFilePanel
{
using System;
using System.IO;
using System.Net;
using System.Text;
using System.Windows.Forms;
using System.Drawing;
using System.Diagnostics;
using n_LaterFilesManager;
using n_FileType;
using T;
using n_OS;

/// <summary>
/// Description of WorkForm.
/// </summary>
public class LaterFilePanel : Panel
{
	Color HoverColor;
	Color LeaveColor;
	Color PathColor;
	Font HoverFont;
	Font LeaveFont;
	Font PathFont;
	
	//链接标签列表
	Label[] links;
	Label[] FileLabel;
	Label[] HeadList;
	
	Image MouseOnImage;
	Image NormalImage;
	
	const int HeightOffset = 30;
	const int StartY = 60;
	
	//主窗口
	public LaterFilePanel()
	{
		MouseOnImage = Image.FromFile( OS.SystemRoot +
		                               "Resource" + OS.PATH_S + "LaterFilePanel" + OS.PATH_S + "MouseOnImage.ico" );
		NormalImage = Image.FromFile( OS.SystemRoot +
		                               "Resource" + OS.PATH_S + "LaterFilePanel" + OS.PATH_S + "NImage.ico" );
		
		BackColor = Color.WhiteSmoke;
		BorderStyle = BorderStyle.None;
		Dock = DockStyle.Fill;
		
		LeaveColor = Color.Gray;
		HoverColor = Color.Black;
		
		PathColor = Color.LightGray;
		
		HoverFont = new Font( "微软雅黑", 11, FontStyle.Bold );
		LeaveFont = new Font( "微软雅黑", 11, FontStyle.Regular );
		PathFont = new Font( "微软雅黑", 10, FontStyle.Regular );
		
		links = new Label[ LaterFilesManager.MaxNumber ];
		FileLabel = new Label[ LaterFilesManager.MaxNumber ];
		HeadList = new Label[ LaterFilesManager.MaxNumber ];
		for( int i = 0; i < LaterFilesManager.MaxNumber; ++i ) {
			
			//处理链接标签项
			links[ i ] = new Label();
			links[ i ].AutoSize = true;
			links[ i ].TextAlign = ContentAlignment.MiddleLeft;
			links[ i ].Location = new Point( 60, StartY + i * HeightOffset + 9 );
			links[ i ].Enabled = true;
			links[ i ].Font = LeaveFont;
			links[ i ].ForeColor = LeaveColor;
			links[ i ].BackColor = Color.Transparent;
			links[ i ].BorderStyle = BorderStyle.None;
			links[ i ].Click += new EventHandler( LinksClick );
			links[ i ].MouseEnter += new EventHandler( LinkLabelMouseEnter );
			links[ i ].MouseLeave += new EventHandler( LinkLabelMouseLeave );
			
			//处理文件名路径标签
			FileLabel[ i ] = new Label();
			FileLabel[ i ].Location = new Point( 280, StartY + i * HeightOffset + 9 );
			FileLabel[ i ].Visible = false;
			FileLabel[ i ].AutoSize = true;
			FileLabel[ i ].TextAlign = ContentAlignment.MiddleLeft;
			FileLabel[ i ].Font = PathFont;
			FileLabel[ i ].BackColor = Color.Transparent;
			FileLabel[ i ].ForeColor = PathColor;
			FileLabel[ i ].BorderStyle = BorderStyle.None;
			FileLabel[ i ].BackgroundImageLayout = ImageLayout.None;
			
			//处理头部信息框
			HeadList[ i ] = new Label();
			HeadList[ i ].Location = new Point( 10, StartY + i * HeightOffset + 6 );
			HeadList[ i ].Size = new Size( 25, 25 );
			HeadList[ i ].TextAlign = ContentAlignment.MiddleLeft;
			HeadList[ i ].Font = LeaveFont;
			HeadList[ i ].BackgroundImage = NormalImage;
			HeadList[ i ].BackgroundImageLayout = ImageLayout.Stretch;
			HeadList[ i ].BackColor = Color.Transparent;
			HeadList[ i ].ForeColor = Color.Red;
			HeadList[ i ].BorderStyle = BorderStyle.None;
			
			Controls.Add( links[ i ] );
			Controls.Add( FileLabel[ i ] );
			Controls.Add( HeadList[ i ] );
		}
		
		LaterFilesManager.FileListChanged += Refresh;
		
		UserRefresh();
	}
	
	//设置文件列表
	void UserRefresh()
	{
		string[] LaterFiles = LaterFilesManager.GetFileList();
		int LaterFileLength = 0;
		if( LaterFiles != null ) {
			LaterFileLength = LaterFiles.Length;
		}
		for( int i = 0; i < LaterFilesManager.MaxNumber; ++i ) {
			
			if( i >= LaterFileLength ) {
				links[ i ].Visible = false;
				FileLabel[ i ].Visible = false;
				HeadList[ i ].Visible = false;
				continue;
			}
			//格式化显示字符
			//string FileNumber = ( i + 1 ).ToString().PadRight( 3, ' ' );
			string FileName = LaterFiles[ i ].Remove( 0, LaterFiles[ i ].LastIndexOf( OS.PATH_S ) + 1 );
			
			//处理链接标签项
			FileLabel[ i ].Text = "  " + LaterFiles[ i ];
			FileLabel[ i ].Visible = false;
			links[ i ].Text = FileName;
			links[ i ].Name = i.ToString();
			links[ i ].Visible = true;
			if( File.Exists( LaterFiles[ i ] ) ) {
				links[ i ].Enabled = true;
			}
			else {
				links[ i ].Enabled = false;
			}
			HeadList[ i ].Visible = true;
		}
	}
	
	//文件链接事件
	void LinksClick(object sender, EventArgs e)
	{
		int Index = int.Parse( ( (Label)sender ).Name );
		string LinkFileName = FileLabel[ Index ].Text.TrimStart( ' ' );;
		
		//添加到近期文档列表
		LaterFilesManager.AddFile( LinkFileName );
		UserRefresh();
		
		T.OpenFileAsType( LinkFileName );
	}
	
	//鼠标进入链接
	void LinkLabelMouseEnter(object sender, EventArgs e)
	{
		int Index = int.Parse( ( (Label)sender ).Name );
		Label ll = (Label)sender;
		ll.ForeColor = HoverColor;
		ll.Font = HoverFont;
		FileLabel[ Index ].Location = new Point( ll.Location.X + ll.Width + 5, FileLabel[ Index ].Location.Y );
		FileLabel[ Index ].Visible = true;
		HeadList[ Index ].BackgroundImage = MouseOnImage;
	}
	
	//鼠标离开链接
	void LinkLabelMouseLeave(object sender, EventArgs e)
	{
		int Index = int.Parse( ( (Label)sender ).Name );
		Label ll = (Label)sender;
		ll.ForeColor = LeaveColor;
		ll.Font = LeaveFont;
		FileLabel[ Index ].Visible = false;
		HeadList[ Index ].BackgroundImage = NormalImage;
	}
	
	//暂时不开放此功能
	void ClearFileList(object sender, EventArgs e)
	{
		if( MessageBox.Show( "您确定要清空文件列表吗?", "清空确认", MessageBoxButtons.OKCancel ) == DialogResult.OK ) {
			
			string LaterFilePath = OS.SystemRoot + "Resource" + OS.PATH_S + "LaterFilePanel" + OS.PATH_S + "近期文件路径.lst";
			VIO.SaveTextFileGB2312( LaterFilePath, "<end>" );
			
			UserRefresh();
		}
	}
}

}



