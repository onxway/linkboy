﻿/*
 * 由 SharpDevelop 创建。
 * 用户: bbb_2
 * 日期: 2009-12-1
 * 时间: 11:57
 * 
 * 如果你想要更改该模板，那么请使用“工具 | 选项 | 编辑 | 编辑标准标题”。
 */
namespace n_WorkForm
{
	partial class WorkForm
	{
		/// <summary>
		/// Designer variable used to keep track of non-visual components.
		/// </summary>
		private System.ComponentModel.IContainer components = null;
		
		/// <summary>
		/// Disposes resources used by the form.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing) {
				if (components != null) {
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}
		
		/// <summary>
		/// This method is required for Windows Forms designer support.
		/// Do not change the method contents inside the source code editor. The Forms designer might
		/// not be able to load this method if it was changed manually.
		/// </summary>
		private void InitializeComponent()
		{
			this.components = new System.ComponentModel.Container();
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(WorkForm));
			this.panel1 = new System.Windows.Forms.Panel();
			this.panelExtendMesBox = new System.Windows.Forms.Panel();
			this.buttonExtendReplace9 = new System.Windows.Forms.Label();
			this.buttonExtendReplace8 = new System.Windows.Forms.Label();
			this.buttonExtendReplace7 = new System.Windows.Forms.Label();
			this.buttonExtendReplace6 = new System.Windows.Forms.Label();
			this.buttonExtendReplace5 = new System.Windows.Forms.Label();
			this.buttonExtendReplace4 = new System.Windows.Forms.Label();
			this.buttonExtendReplace3 = new System.Windows.Forms.Label();
			this.buttonExtendButton = new System.Windows.Forms.Label();
			this.buttonExtendReplace2 = new System.Windows.Forms.Label();
			this.richTextBoxExtend = new System.Windows.Forms.RichTextBox();
			this.labelExtendLabel = new System.Windows.Forms.Label();
			this.buttonExtendReplace1 = new System.Windows.Forms.Label();
			this.label打开文件 = new System.Windows.Forms.Label();
			this.splitContainerMain = new System.Windows.Forms.SplitContainer();
			this.treeView = new System.Windows.Forms.TreeView();
			this.splitContainer1 = new System.Windows.Forms.SplitContainer();
			this.TexttabControl = new System.Windows.Forms.TabControl();
			this.contextMenuStrip1 = new System.Windows.Forms.ContextMenuStrip(this.components);
			this.关闭当前文档ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
			this.tabControl1 = new System.Windows.Forms.TabControl();
			this.panel2 = new System.Windows.Forms.Panel();
			this.comboBoxChip = new System.Windows.Forms.ComboBox();
			this.label1 = new System.Windows.Forms.Label();
			this.button关于 = new System.Windows.Forms.Button();
			this.button下载 = new System.Windows.Forms.Button();
			this.button编译 = new System.Windows.Forms.Button();
			this.button打开 = new System.Windows.Forms.Button();
			this.button保存 = new System.Windows.Forms.Button();
			this.button新建 = new System.Windows.Forms.Button();
			this.labelButtonMes = new System.Windows.Forms.Label();
			this.panelExtendMesBox.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.splitContainerMain)).BeginInit();
			this.splitContainerMain.Panel1.SuspendLayout();
			this.splitContainerMain.Panel2.SuspendLayout();
			this.splitContainerMain.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).BeginInit();
			this.splitContainer1.Panel1.SuspendLayout();
			this.splitContainer1.Panel2.SuspendLayout();
			this.splitContainer1.SuspendLayout();
			this.contextMenuStrip1.SuspendLayout();
			this.panel2.SuspendLayout();
			this.SuspendLayout();
			// 
			// panel1
			// 
			this.panel1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
			this.panel1.Location = new System.Drawing.Point(579, 139);
			this.panel1.Name = "panel1";
			this.panel1.Size = new System.Drawing.Size(334, 226);
			this.panel1.TabIndex = 12;
			// 
			// panelExtendMesBox
			// 
			this.panelExtendMesBox.BackColor = System.Drawing.Color.Gainsboro;
			this.panelExtendMesBox.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
			this.panelExtendMesBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
			this.panelExtendMesBox.Controls.Add(this.buttonExtendReplace9);
			this.panelExtendMesBox.Controls.Add(this.buttonExtendReplace8);
			this.panelExtendMesBox.Controls.Add(this.buttonExtendReplace7);
			this.panelExtendMesBox.Controls.Add(this.buttonExtendReplace6);
			this.panelExtendMesBox.Controls.Add(this.buttonExtendReplace5);
			this.panelExtendMesBox.Controls.Add(this.buttonExtendReplace4);
			this.panelExtendMesBox.Controls.Add(this.buttonExtendReplace3);
			this.panelExtendMesBox.Controls.Add(this.buttonExtendButton);
			this.panelExtendMesBox.Controls.Add(this.buttonExtendReplace2);
			this.panelExtendMesBox.Controls.Add(this.richTextBoxExtend);
			this.panelExtendMesBox.Controls.Add(this.labelExtendLabel);
			this.panelExtendMesBox.Controls.Add(this.buttonExtendReplace1);
			this.panelExtendMesBox.Controls.Add(this.label打开文件);
			this.panelExtendMesBox.Location = new System.Drawing.Point(578, 382);
			this.panelExtendMesBox.Name = "panelExtendMesBox";
			this.panelExtendMesBox.Size = new System.Drawing.Size(436, 264);
			this.panelExtendMesBox.TabIndex = 6;
			// 
			// buttonExtendReplace9
			// 
			this.buttonExtendReplace9.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(0)))));
			this.buttonExtendReplace9.Font = new System.Drawing.Font("微软雅黑", 10.5F);
			this.buttonExtendReplace9.ForeColor = System.Drawing.Color.White;
			this.buttonExtendReplace9.ImageAlign = System.Drawing.ContentAlignment.TopCenter;
			this.buttonExtendReplace9.Location = new System.Drawing.Point(314, 237);
			this.buttonExtendReplace9.Name = "buttonExtendReplace9";
			this.buttonExtendReplace9.Size = new System.Drawing.Size(117, 20);
			this.buttonExtendReplace9.TabIndex = 13;
			this.buttonExtendReplace9.Tag = "";
			this.buttonExtendReplace9.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
			this.buttonExtendReplace9.Click += new System.EventHandler(this.ButtonExtendReplaceClick);
			// 
			// buttonExtendReplace8
			// 
			this.buttonExtendReplace8.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(0)))));
			this.buttonExtendReplace8.Font = new System.Drawing.Font("微软雅黑", 10.5F);
			this.buttonExtendReplace8.ForeColor = System.Drawing.Color.White;
			this.buttonExtendReplace8.ImageAlign = System.Drawing.ContentAlignment.TopCenter;
			this.buttonExtendReplace8.Location = new System.Drawing.Point(314, 215);
			this.buttonExtendReplace8.Name = "buttonExtendReplace8";
			this.buttonExtendReplace8.Size = new System.Drawing.Size(117, 20);
			this.buttonExtendReplace8.TabIndex = 12;
			this.buttonExtendReplace8.Tag = "";
			this.buttonExtendReplace8.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
			this.buttonExtendReplace8.Click += new System.EventHandler(this.ButtonExtendReplaceClick);
			// 
			// buttonExtendReplace7
			// 
			this.buttonExtendReplace7.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(0)))));
			this.buttonExtendReplace7.Font = new System.Drawing.Font("微软雅黑", 10.5F);
			this.buttonExtendReplace7.ForeColor = System.Drawing.Color.White;
			this.buttonExtendReplace7.ImageAlign = System.Drawing.ContentAlignment.TopCenter;
			this.buttonExtendReplace7.Location = new System.Drawing.Point(314, 193);
			this.buttonExtendReplace7.Name = "buttonExtendReplace7";
			this.buttonExtendReplace7.Size = new System.Drawing.Size(117, 20);
			this.buttonExtendReplace7.TabIndex = 11;
			this.buttonExtendReplace7.Tag = "";
			this.buttonExtendReplace7.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
			this.buttonExtendReplace7.Click += new System.EventHandler(this.ButtonExtendReplaceClick);
			// 
			// buttonExtendReplace6
			// 
			this.buttonExtendReplace6.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(0)))));
			this.buttonExtendReplace6.Font = new System.Drawing.Font("微软雅黑", 10.5F);
			this.buttonExtendReplace6.ForeColor = System.Drawing.Color.White;
			this.buttonExtendReplace6.ImageAlign = System.Drawing.ContentAlignment.TopCenter;
			this.buttonExtendReplace6.Location = new System.Drawing.Point(314, 171);
			this.buttonExtendReplace6.Name = "buttonExtendReplace6";
			this.buttonExtendReplace6.Size = new System.Drawing.Size(117, 20);
			this.buttonExtendReplace6.TabIndex = 10;
			this.buttonExtendReplace6.Tag = "";
			this.buttonExtendReplace6.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
			this.buttonExtendReplace6.Click += new System.EventHandler(this.ButtonExtendReplaceClick);
			// 
			// buttonExtendReplace5
			// 
			this.buttonExtendReplace5.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(0)))));
			this.buttonExtendReplace5.Font = new System.Drawing.Font("微软雅黑", 10.5F);
			this.buttonExtendReplace5.ForeColor = System.Drawing.Color.White;
			this.buttonExtendReplace5.ImageAlign = System.Drawing.ContentAlignment.TopCenter;
			this.buttonExtendReplace5.Location = new System.Drawing.Point(314, 149);
			this.buttonExtendReplace5.Name = "buttonExtendReplace5";
			this.buttonExtendReplace5.Size = new System.Drawing.Size(117, 20);
			this.buttonExtendReplace5.TabIndex = 9;
			this.buttonExtendReplace5.Tag = "";
			this.buttonExtendReplace5.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
			this.buttonExtendReplace5.Click += new System.EventHandler(this.ButtonExtendReplaceClick);
			// 
			// buttonExtendReplace4
			// 
			this.buttonExtendReplace4.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(0)))));
			this.buttonExtendReplace4.Font = new System.Drawing.Font("微软雅黑", 10.5F);
			this.buttonExtendReplace4.ForeColor = System.Drawing.Color.White;
			this.buttonExtendReplace4.ImageAlign = System.Drawing.ContentAlignment.TopCenter;
			this.buttonExtendReplace4.Location = new System.Drawing.Point(314, 127);
			this.buttonExtendReplace4.Name = "buttonExtendReplace4";
			this.buttonExtendReplace4.Size = new System.Drawing.Size(117, 20);
			this.buttonExtendReplace4.TabIndex = 8;
			this.buttonExtendReplace4.Tag = "";
			this.buttonExtendReplace4.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
			this.buttonExtendReplace4.Click += new System.EventHandler(this.ButtonExtendReplaceClick);
			// 
			// buttonExtendReplace3
			// 
			this.buttonExtendReplace3.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(0)))));
			this.buttonExtendReplace3.Font = new System.Drawing.Font("微软雅黑", 10.5F);
			this.buttonExtendReplace3.ForeColor = System.Drawing.Color.White;
			this.buttonExtendReplace3.ImageAlign = System.Drawing.ContentAlignment.TopCenter;
			this.buttonExtendReplace3.Location = new System.Drawing.Point(314, 105);
			this.buttonExtendReplace3.Name = "buttonExtendReplace3";
			this.buttonExtendReplace3.Size = new System.Drawing.Size(117, 20);
			this.buttonExtendReplace3.TabIndex = 7;
			this.buttonExtendReplace3.Tag = "";
			this.buttonExtendReplace3.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
			this.buttonExtendReplace3.Click += new System.EventHandler(this.ButtonExtendReplaceClick);
			// 
			// buttonExtendButton
			// 
			this.buttonExtendButton.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(128)))), ((int)(((byte)(255)))));
			this.buttonExtendButton.Font = new System.Drawing.Font("微软雅黑", 12F);
			this.buttonExtendButton.ForeColor = System.Drawing.Color.White;
			this.buttonExtendButton.ImageAlign = System.Drawing.ContentAlignment.TopCenter;
			this.buttonExtendButton.Location = new System.Drawing.Point(254, 229);
			this.buttonExtendButton.Name = "buttonExtendButton";
			this.buttonExtendButton.Size = new System.Drawing.Size(53, 28);
			this.buttonExtendButton.TabIndex = 5;
			this.buttonExtendButton.Tag = "";
			this.buttonExtendButton.Text = "信息";
			this.buttonExtendButton.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
			this.buttonExtendButton.Click += new System.EventHandler(this.ButtonExtentClick);
			// 
			// buttonExtendReplace2
			// 
			this.buttonExtendReplace2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(0)))));
			this.buttonExtendReplace2.Font = new System.Drawing.Font("微软雅黑", 10.5F);
			this.buttonExtendReplace2.ForeColor = System.Drawing.Color.White;
			this.buttonExtendReplace2.ImageAlign = System.Drawing.ContentAlignment.TopCenter;
			this.buttonExtendReplace2.Location = new System.Drawing.Point(314, 83);
			this.buttonExtendReplace2.Name = "buttonExtendReplace2";
			this.buttonExtendReplace2.Size = new System.Drawing.Size(117, 20);
			this.buttonExtendReplace2.TabIndex = 4;
			this.buttonExtendReplace2.Tag = "";
			this.buttonExtendReplace2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
			this.buttonExtendReplace2.Click += new System.EventHandler(this.ButtonExtendReplaceClick);
			// 
			// richTextBoxExtend
			// 
			this.richTextBoxExtend.BackColor = System.Drawing.Color.Gainsboro;
			this.richTextBoxExtend.BorderStyle = System.Windows.Forms.BorderStyle.None;
			this.richTextBoxExtend.Font = new System.Drawing.Font("微软雅黑", 10.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
			this.richTextBoxExtend.ForeColor = System.Drawing.Color.Black;
			this.richTextBoxExtend.Location = new System.Drawing.Point(3, 31);
			this.richTextBoxExtend.Name = "richTextBoxExtend";
			this.richTextBoxExtend.ReadOnly = true;
			this.richTextBoxExtend.Size = new System.Drawing.Size(304, 195);
			this.richTextBoxExtend.TabIndex = 2;
			this.richTextBoxExtend.Text = "";
			// 
			// labelExtendLabel
			// 
			this.labelExtendLabel.BackColor = System.Drawing.Color.Transparent;
			this.labelExtendLabel.Font = new System.Drawing.Font("微软雅黑", 10.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
			this.labelExtendLabel.ForeColor = System.Drawing.Color.Blue;
			this.labelExtendLabel.Location = new System.Drawing.Point(3, 5);
			this.labelExtendLabel.Name = "labelExtendLabel";
			this.labelExtendLabel.Size = new System.Drawing.Size(428, 23);
			this.labelExtendLabel.TabIndex = 0;
			this.labelExtendLabel.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// buttonExtendReplace1
			// 
			this.buttonExtendReplace1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(0)))));
			this.buttonExtendReplace1.Font = new System.Drawing.Font("微软雅黑", 10.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
			this.buttonExtendReplace1.ForeColor = System.Drawing.Color.White;
			this.buttonExtendReplace1.ImageAlign = System.Drawing.ContentAlignment.TopCenter;
			this.buttonExtendReplace1.Location = new System.Drawing.Point(314, 61);
			this.buttonExtendReplace1.Name = "buttonExtendReplace1";
			this.buttonExtendReplace1.Size = new System.Drawing.Size(117, 20);
			this.buttonExtendReplace1.TabIndex = 3;
			this.buttonExtendReplace1.Tag = "";
			this.buttonExtendReplace1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
			this.buttonExtendReplace1.Click += new System.EventHandler(this.ButtonExtendReplaceClick);
			// 
			// label打开文件
			// 
			this.label打开文件.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(0)))));
			this.label打开文件.Font = new System.Drawing.Font("微软雅黑", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
			this.label打开文件.ForeColor = System.Drawing.Color.White;
			this.label打开文件.ImageAlign = System.Drawing.ContentAlignment.TopCenter;
			this.label打开文件.Location = new System.Drawing.Point(314, 31);
			this.label打开文件.Name = "label打开文件";
			this.label打开文件.Size = new System.Drawing.Size(117, 28);
			this.label打开文件.TabIndex = 6;
			this.label打开文件.Tag = "";
			this.label打开文件.Text = "打开文件";
			this.label打开文件.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
			this.label打开文件.Click += new System.EventHandler(this.Label打开文件Click);
			// 
			// splitContainerMain
			// 
			this.splitContainerMain.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(238)))), ((int)(((byte)(240)))), ((int)(((byte)(242)))));
			this.splitContainerMain.FixedPanel = System.Windows.Forms.FixedPanel.Panel1;
			this.splitContainerMain.Location = new System.Drawing.Point(12, 55);
			this.splitContainerMain.Name = "splitContainerMain";
			// 
			// splitContainerMain.Panel1
			// 
			this.splitContainerMain.Panel1.BackColor = System.Drawing.Color.BlanchedAlmond;
			this.splitContainerMain.Panel1.Controls.Add(this.treeView);
			// 
			// splitContainerMain.Panel2
			// 
			this.splitContainerMain.Panel2.Controls.Add(this.splitContainer1);
			this.splitContainerMain.Size = new System.Drawing.Size(560, 450);
			this.splitContainerMain.SplitterDistance = 205;
			this.splitContainerMain.SplitterWidth = 6;
			this.splitContainerMain.TabIndex = 15;
			// 
			// treeView
			// 
			this.treeView.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
			this.treeView.Dock = System.Windows.Forms.DockStyle.Fill;
			this.treeView.Font = new System.Drawing.Font("微软雅黑", 10.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
			this.treeView.Location = new System.Drawing.Point(0, 0);
			this.treeView.Name = "treeView";
			this.treeView.Size = new System.Drawing.Size(205, 450);
			this.treeView.TabIndex = 0;
			this.treeView.DoubleClick += new System.EventHandler(this.TreeViewDoubleClick);
			// 
			// splitContainer1
			// 
			this.splitContainer1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(238)))), ((int)(((byte)(240)))), ((int)(((byte)(242)))));
			this.splitContainer1.Dock = System.Windows.Forms.DockStyle.Fill;
			this.splitContainer1.Location = new System.Drawing.Point(0, 0);
			this.splitContainer1.Name = "splitContainer1";
			this.splitContainer1.Orientation = System.Windows.Forms.Orientation.Horizontal;
			// 
			// splitContainer1.Panel1
			// 
			this.splitContainer1.Panel1.BackColor = System.Drawing.Color.WhiteSmoke;
			this.splitContainer1.Panel1.Controls.Add(this.TexttabControl);
			// 
			// splitContainer1.Panel2
			// 
			this.splitContainer1.Panel2.BackColor = System.Drawing.Color.WhiteSmoke;
			this.splitContainer1.Panel2.Controls.Add(this.tabControl1);
			this.splitContainer1.Size = new System.Drawing.Size(349, 450);
			this.splitContainer1.SplitterDistance = 346;
			this.splitContainer1.SplitterWidth = 6;
			this.splitContainer1.TabIndex = 0;
			// 
			// TexttabControl
			// 
			this.TexttabControl.ContextMenuStrip = this.contextMenuStrip1;
			this.TexttabControl.Dock = System.Windows.Forms.DockStyle.Fill;
			this.TexttabControl.Font = new System.Drawing.Font("微软雅黑", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
			this.TexttabControl.Location = new System.Drawing.Point(0, 0);
			this.TexttabControl.Name = "TexttabControl";
			this.TexttabControl.SelectedIndex = 0;
			this.TexttabControl.Size = new System.Drawing.Size(349, 346);
			this.TexttabControl.TabIndex = 0;
			this.TexttabControl.Click += new System.EventHandler(this.TexttabControlClick);
			// 
			// contextMenuStrip1
			// 
			this.contextMenuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
									this.关闭当前文档ToolStripMenuItem});
			this.contextMenuStrip1.Name = "contextMenuStrip1";
			this.contextMenuStrip1.Size = new System.Drawing.Size(149, 26);
			// 
			// 关闭当前文档ToolStripMenuItem
			// 
			this.关闭当前文档ToolStripMenuItem.Name = "关闭当前文档ToolStripMenuItem";
			this.关闭当前文档ToolStripMenuItem.Size = new System.Drawing.Size(148, 22);
			this.关闭当前文档ToolStripMenuItem.Text = "关闭当前文档";
			this.关闭当前文档ToolStripMenuItem.Click += new System.EventHandler(this.关闭当前文档ToolStripMenuItemClick);
			// 
			// tabControl1
			// 
			this.tabControl1.Dock = System.Windows.Forms.DockStyle.Fill;
			this.tabControl1.Font = new System.Drawing.Font("微软雅黑", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
			this.tabControl1.Location = new System.Drawing.Point(0, 0);
			this.tabControl1.Name = "tabControl1";
			this.tabControl1.SelectedIndex = 0;
			this.tabControl1.Size = new System.Drawing.Size(349, 98);
			this.tabControl1.TabIndex = 0;
			this.tabControl1.Click += new System.EventHandler(this.TabControl1Click);
			// 
			// panel2
			// 
			this.panel2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(238)))), ((int)(((byte)(240)))), ((int)(((byte)(242)))));
			this.panel2.Controls.Add(this.comboBoxChip);
			this.panel2.Controls.Add(this.label1);
			this.panel2.Controls.Add(this.button关于);
			this.panel2.Controls.Add(this.button下载);
			this.panel2.Controls.Add(this.button编译);
			this.panel2.Controls.Add(this.button打开);
			this.panel2.Controls.Add(this.button保存);
			this.panel2.Controls.Add(this.button新建);
			this.panel2.Controls.Add(this.labelButtonMes);
			this.panel2.Dock = System.Windows.Forms.DockStyle.Top;
			this.panel2.Location = new System.Drawing.Point(0, 0);
			this.panel2.Name = "panel2";
			this.panel2.Size = new System.Drawing.Size(1032, 49);
			this.panel2.TabIndex = 16;
			// 
			// comboBoxChip
			// 
			this.comboBoxChip.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
			this.comboBoxChip.Font = new System.Drawing.Font("微软雅黑", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
			this.comboBoxChip.FormattingEnabled = true;
			this.comboBoxChip.Items.AddRange(new object[] {
									"Arduino Uno",
									"Arduino Nano",
									"Arduino 644",
									"Arduino 2560",
									"MCS51",
									"VM"});
			this.comboBoxChip.Location = new System.Drawing.Point(535, 12);
			this.comboBoxChip.Name = "comboBoxChip";
			this.comboBoxChip.Size = new System.Drawing.Size(177, 29);
			this.comboBoxChip.TabIndex = 17;
			// 
			// label1
			// 
			this.label1.Font = new System.Drawing.Font("微软雅黑", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
			this.label1.Location = new System.Drawing.Point(416, 12);
			this.label1.Name = "label1";
			this.label1.Size = new System.Drawing.Size(134, 29);
			this.label1.TabIndex = 18;
			this.label1.Text = "目标主板型号：";
			this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// button关于
			// 
			this.button关于.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("button关于.BackgroundImage")));
			this.button关于.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
			this.button关于.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
			this.button关于.Font = new System.Drawing.Font("微软雅黑", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
			this.button关于.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(238)))), ((int)(((byte)(240)))), ((int)(((byte)(242)))));
			this.button关于.Location = new System.Drawing.Point(323, 4);
			this.button关于.Name = "button关于";
			this.button关于.Size = new System.Drawing.Size(60, 38);
			this.button关于.TabIndex = 16;
			this.button关于.UseVisualStyleBackColor = true;
			this.button关于.Click += new System.EventHandler(this.ButtonMesClick);
			this.button关于.MouseDown += new System.Windows.Forms.MouseEventHandler(this.Button关于MouseDown);
			this.button关于.MouseEnter += new System.EventHandler(this.TextButtonMouseEnter);
			this.button关于.MouseLeave += new System.EventHandler(this.TextButtonMouseLeave);
			// 
			// button下载
			// 
			this.button下载.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("button下载.BackgroundImage")));
			this.button下载.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
			this.button下载.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
			this.button下载.Font = new System.Drawing.Font("微软雅黑", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
			this.button下载.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(238)))), ((int)(((byte)(240)))), ((int)(((byte)(242)))));
			this.button下载.Location = new System.Drawing.Point(259, 4);
			this.button下载.Name = "button下载";
			this.button下载.Size = new System.Drawing.Size(60, 38);
			this.button下载.TabIndex = 15;
			this.button下载.UseVisualStyleBackColor = true;
			this.button下载.Click += new System.EventHandler(this.ButtonDownClick);
			this.button下载.MouseEnter += new System.EventHandler(this.TextButtonMouseEnter);
			this.button下载.MouseLeave += new System.EventHandler(this.TextButtonMouseLeave);
			// 
			// button编译
			// 
			this.button编译.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("button编译.BackgroundImage")));
			this.button编译.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
			this.button编译.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
			this.button编译.Font = new System.Drawing.Font("微软雅黑", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
			this.button编译.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(238)))), ((int)(((byte)(240)))), ((int)(((byte)(242)))));
			this.button编译.Location = new System.Drawing.Point(195, 4);
			this.button编译.Name = "button编译";
			this.button编译.Size = new System.Drawing.Size(60, 38);
			this.button编译.TabIndex = 13;
			this.button编译.UseVisualStyleBackColor = true;
			this.button编译.Click += new System.EventHandler(this.ButtonCompileClick);
			this.button编译.MouseEnter += new System.EventHandler(this.TextButtonMouseEnter);
			this.button编译.MouseLeave += new System.EventHandler(this.TextButtonMouseLeave);
			// 
			// button打开
			// 
			this.button打开.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("button打开.BackgroundImage")));
			this.button打开.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
			this.button打开.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
			this.button打开.Font = new System.Drawing.Font("微软雅黑", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
			this.button打开.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(238)))), ((int)(((byte)(240)))), ((int)(((byte)(242)))));
			this.button打开.Location = new System.Drawing.Point(131, 4);
			this.button打开.Name = "button打开";
			this.button打开.Size = new System.Drawing.Size(60, 38);
			this.button打开.TabIndex = 12;
			this.button打开.UseVisualStyleBackColor = true;
			this.button打开.Click += new System.EventHandler(this.ButtonOpenClick);
			this.button打开.MouseEnter += new System.EventHandler(this.TextButtonMouseEnter);
			this.button打开.MouseLeave += new System.EventHandler(this.TextButtonMouseLeave);
			// 
			// button保存
			// 
			this.button保存.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("button保存.BackgroundImage")));
			this.button保存.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
			this.button保存.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
			this.button保存.Font = new System.Drawing.Font("微软雅黑", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
			this.button保存.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(238)))), ((int)(((byte)(240)))), ((int)(((byte)(242)))));
			this.button保存.Location = new System.Drawing.Point(67, 4);
			this.button保存.Name = "button保存";
			this.button保存.Size = new System.Drawing.Size(60, 38);
			this.button保存.TabIndex = 11;
			this.button保存.UseVisualStyleBackColor = true;
			this.button保存.Click += new System.EventHandler(this.ButtonSaveClick);
			this.button保存.MouseEnter += new System.EventHandler(this.TextButtonMouseEnter);
			this.button保存.MouseLeave += new System.EventHandler(this.TextButtonMouseLeave);
			// 
			// button新建
			// 
			this.button新建.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("button新建.BackgroundImage")));
			this.button新建.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
			this.button新建.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
			this.button新建.Font = new System.Drawing.Font("微软雅黑", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
			this.button新建.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(238)))), ((int)(((byte)(240)))), ((int)(((byte)(242)))));
			this.button新建.Location = new System.Drawing.Point(3, 4);
			this.button新建.Name = "button新建";
			this.button新建.Size = new System.Drawing.Size(60, 38);
			this.button新建.TabIndex = 10;
			this.button新建.UseVisualStyleBackColor = true;
			this.button新建.Click += new System.EventHandler(this.ButtonNewClick);
			this.button新建.MouseEnter += new System.EventHandler(this.TextButtonMouseEnter);
			this.button新建.MouseLeave += new System.EventHandler(this.TextButtonMouseLeave);
			// 
			// labelButtonMes
			// 
			this.labelButtonMes.Font = new System.Drawing.Font("微软雅黑", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
			this.labelButtonMes.ForeColor = System.Drawing.Color.DimGray;
			this.labelButtonMes.Location = new System.Drawing.Point(395, 9);
			this.labelButtonMes.Name = "labelButtonMes";
			this.labelButtonMes.RightToLeft = System.Windows.Forms.RightToLeft.No;
			this.labelButtonMes.Size = new System.Drawing.Size(246, 32);
			this.labelButtonMes.TabIndex = 9;
			this.labelButtonMes.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// WorkForm
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.BackColor = System.Drawing.Color.WhiteSmoke;
			this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
			this.ClientSize = new System.Drawing.Size(1032, 717);
			this.Controls.Add(this.panelExtendMesBox);
			this.Controls.Add(this.panel1);
			this.Controls.Add(this.splitContainerMain);
			this.Controls.Add(this.panel2);
			this.DoubleBuffered = true;
			this.Name = "WorkForm";
			this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
			this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.Form_Closing);
			this.Load += new System.EventHandler(this.Form_Load);
			this.panelExtendMesBox.ResumeLayout(false);
			this.splitContainerMain.Panel1.ResumeLayout(false);
			this.splitContainerMain.Panel2.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.splitContainerMain)).EndInit();
			this.splitContainerMain.ResumeLayout(false);
			this.splitContainer1.Panel1.ResumeLayout(false);
			this.splitContainer1.Panel2.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).EndInit();
			this.splitContainer1.ResumeLayout(false);
			this.contextMenuStrip1.ResumeLayout(false);
			this.panel2.ResumeLayout(false);
			this.ResumeLayout(false);
		}
		private System.Windows.Forms.Label label1;
		private System.Windows.Forms.ComboBox comboBoxChip;
		private System.Windows.Forms.Button button关于;
		private System.Windows.Forms.Button button编译;
		private System.Windows.Forms.Button button下载;
		private System.Windows.Forms.Button button新建;
		private System.Windows.Forms.Button button保存;
		private System.Windows.Forms.Button button打开;
		private System.Windows.Forms.TreeView treeView;
		private System.Windows.Forms.ToolStripMenuItem 关闭当前文档ToolStripMenuItem;
		private System.Windows.Forms.ContextMenuStrip contextMenuStrip1;
		private System.Windows.Forms.TabControl TexttabControl;
		private System.Windows.Forms.Label labelButtonMes;
		private System.Windows.Forms.TabControl tabControl1;
		public System.Windows.Forms.SplitContainer splitContainer1;
		private System.Windows.Forms.Panel panel2;
		public System.Windows.Forms.Label buttonExtendReplace9;
		public System.Windows.Forms.Label buttonExtendReplace8;
		public System.Windows.Forms.Label buttonExtendReplace7;
		public System.Windows.Forms.Label buttonExtendReplace5;
		public System.Windows.Forms.SplitContainer splitContainerMain;
		public System.Windows.Forms.Label buttonExtendReplace1;
		public System.Windows.Forms.Label buttonExtendReplace2;
		public System.Windows.Forms.Label label打开文件;
		public System.Windows.Forms.Label buttonExtendButton;
		private System.Windows.Forms.Panel panel1;
		public System.Windows.Forms.RichTextBox richTextBoxExtend;
		public System.Windows.Forms.Label labelExtendLabel;
		private System.Windows.Forms.Panel panelExtendMesBox;
		public System.Windows.Forms.Label buttonExtendReplace3;
		public System.Windows.Forms.Label buttonExtendReplace4;
		public System.Windows.Forms.Label buttonExtendReplace6;
		//private System.Windows.Forms.TabControl tabControl1;
	}
}
