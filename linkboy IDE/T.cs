﻿
using System;
using System.Diagnostics;
using System.Windows.Forms;

using n_CompileMessageForm;
using n_ControlCenter;
using n_ctext;
using n_Drawer;
using n_ErrorListBox;
using n_ET;
using n_FileType;
using n_FindForm;
using n_FlashForm;
using n_MessageForm;
using n_SetEditorForm;
using n_TToolForm;
using n_UnitTreeView;
using n_USBASP;
using n_VisualForm;
using n_WorkForm;
using n_MesForm;

namespace T
{
public static class T
{
	static CTextBox vvctext;
	public static CTextBox ctext {
		get { return vvctext; }
		set {
			vvctext = value;
			G.FindTextBox = value;
		}
	}
	
	public static WorkForm m;
	
	//错误信息界面
	//public static MessageForm ErrorBox;
	//调试器界面
	//public static DebugForm DebugBox;
	//虚拟机工作界面
	//public static VMForm VMBox;
	
	//文本编辑器设计界面
	public static SetEditorForm SetTextBox;
	
	//工具箱
	public static ToolForm ToolBox;
	//ISP下载器界面
	public static ISPForm ISPBox;
	//辅助界面
	public static VisualForm VisualBox;
	
	//信息界面
	public static MesForm MesBox;
	
	[STAThread]
	public static void Main(string[] args)
	{
		Application.EnableVisualStyles();
		Application.SetCompatibleTextRenderingDefault(false);
		
		try {
			//初始化
			n_OS.OS.isGForm = false;
			
			string DefaultFilePath = A.TInit( args );
			
			Init( DefaultFilePath );
			
			//运行工作台
			Application.Run( m );
			
			//+++++++++++++++++++++++++++++++++++++++++++++
			//关闭系统
			A.Close();
			if( T.ISPBox != null ) {
				T.ISPBox.Close();
			}
			//if( VMForm.t != null && VMForm.t.IsAlive ) {
			//	VMForm.t.Abort();
			//}
			
			//显示异常信息
		} catch(Exception e) { MessageBox.Show( e.ToString() ); }
	}
	
	//初始化
	public static void Init( string DefaultFilePath )
	{
		G_Init();
		
		ControlCenter.D_StartCompile = Start;
		ControlCenter.D_ShowError = ShowError;
		ControlCenter.D_HiddError = HiddError;
		
		//错误输出框初始化
		//ErrorBox = new MessageForm();
		
		//控件初始化
		//ControlInit();
		
		//初始化主窗体
		m = new WorkForm( DefaultFilePath );
		
		//StartBox.Visible = false;
	}
	
	//图形界面公共初始化
	public static void G_Init()
	{
		//初始化文本绘图控件
		Drawer.Init();
		
		//必须先进行辅助界面初始化
		VisualBox = new VisualForm();
	}
	
	//控件初始化
	public static void ControlInit()
	{
		//创建元件列表
		UnitTreeView.Init();
		
		//ISP下载界面
		ISPBox = new ISPForm( false );
		//设置文本编辑器界面
		SetTextBox = new SetEditorForm();
		//工具箱
		ToolBox = new ToolForm();
	}
	
	//根据文件类型打开文件
	public static void OpenFileAsType( string FileName )
	{
		if( FileName.EndsWith( "." + FileType.EXEFile ) ||
		   FileName.EndsWith( "." + FileType.EXEFile.ToLower() ) ) {
			ProcessStartInfo Info = new ProcessStartInfo();
			Info.FileName = FileName;
			Process.Start(Info);
		}
		else if( FileName.EndsWith( "." + FileType.HEXFile ) ||
		        FileName.EndsWith( "." + FileType.HEXFile.ToLower() ) ) {
			T.ISPBox.LoadHexFile( FileName );
			T.ISPBox.Run();
		}
		else {
			A.AddNewFile( FileName );
		}
	}
	
	//=====================================================================
	
	//开始编译处理
	static void Start()
	{
		n_OutputBox.OutputBox.Clear();
		ErrorListBox.Clear();
		if( true ) {
			ShowMes();
		}
	}
	
	//显示错误信息
	static void ShowError( bool defaultL )
	{
		ShowMes();
		if( defaultL ) {
			ErrorListBox.ShowError( ET.Show().Split( '\n' ) );
		}
		else {
			ErrorListBox.ShowError( n_PyET.ET.Show().Split( '\n' ) );
		}
	}
	
	//隐藏错误信息
	static void HiddError()
	{
		HideMes();
	}
	
	//弹出信息栏
	public static void ShowMes()
	{
		if( m != null ) {
			m.splitContainer1.SplitterDistance = m.splitContainer1.Height - 230;
		}
	}
	
	//隐藏信息栏
	public static void HideMes()
	{
		if( m != null ) {
			m.splitContainer1.SplitterDistance = m.splitContainer1.Height;
		}
	}
}
}






