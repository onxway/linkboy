﻿
//分页文件类
//发现的BUG:Tab的标签尺寸是默认的,改了字体也不行;

namespace n_FileTabPage
{
using System;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.IO;
using System.Windows.Forms;

using n_CCode;
using n_CharType;
using n_Config;
using n_ControlCenter;
using n_ctext;
using n_FunctionList;
using n_MainSystemData;
using n_OS;
using n_StructList;
using n_UnitList;
using n_VarList;
using n_VarType;
using n_VdataList;
using T;

//分页控件
public class FileTabPage : TabPage
{
	Graphics g;
	Pen p1;
	Pen p2;
	
	Panel TextPanel;
	
	public CCode ccode;
	
	public CTextBox ctext;
	
	RichTextBox r;
	
	string FileName;
	TabControl tab;
	ListBox ShortList;
	
	//行号显示
	Label LineLabel;
	Rectangle rec;
	int firstLine;
	int currentLine;
	int endLine;
	Font LineNumberFont;
	
	//竖条显示
	Timer timer;
	bool isShowLine;
	bool LineIsExist; //当前是否存在竖线
	
	//构造函数
	public FileTabPage( string FilePath, TabControl Tab, string SystemName )
	{
		this.tab = Tab;
		int Index = FilePath.LastIndexOf( OS.PATH_S );
		FileName = FilePath.Remove( 0, Index + 1 );
		this.BackColor = Color.White;
		this.BorderStyle = BorderStyle.None;
		this.Text = FileName;
		Tab.Controls.Add( this );
		Tab.SelectTab( this );
		Tab.Refresh();
		
		if( FilePath.EndsWith( ".rtf" ) ) {
			r = new RichTextBox();
			r.Visible = true;
			r.BorderStyle = BorderStyle.None;
			r.Dock = DockStyle.Fill;
			this.Controls.Add( r );
			r.LoadFile( FilePath );
			
			//r.ReadOnly = true; //允许修改
			
			this.Text = FileName.Remove( FileName.Length - 4 );
			
			ccode = new CCode( FilePath );
			ctext = new CTextBox("" );
			T.ctext = ctext;
			return;
		}
		
		//添加快捷选择框
		ShortList = new ListBox();
		ShortList.Visible = false;
		ShortList.Width = 250;
		ShortList.Height = 300;
		ShortList.Font = SystemData.SourceTextFont;
		ShortList.BackColor = Color.White;
		ShortList.ScrollAlwaysVisible = true;
		ShortList.KeyDown += new KeyEventHandler( List_KeyDown );
		ShortList.LostFocus += new EventHandler( List_LostFocus );
		ShortList.GotFocus += new EventHandler( List_GotFocus );
		this.Controls.Add( ShortList );
		
		//添加文本编辑面板和图形编辑面板
		TextPanel = new Panel();
		TextPanel.Dock = DockStyle.Fill;
		TextPanel.Visible = false;
		this.Controls.Add( TextPanel );
		
		//添加编译源码类
		ccode = new CCode( FilePath );
		ccode.isSystemFile = false;
		ccode.isNew = false;
		ccode.Saved += new EventHandler( Text_Saved );
		
		//添加文本框
		ctext = new CTextBox( ccode.GetCode() );
		ctext.ContextMenuStrip = T.VisualBox.contextM;
		
		if( FilePath.EndsWith( ".py" ) ) {
			ctext.DrawPythonCode = true;
		}
		
		SetCurrent();
		
		TextPanel.Controls.Add( ctext );
		if( SystemName != null ) {
			ctext.isSystemFile = true;
		}
		else {
			ctext.isSystemFile = false;
		}
		ctext.isNew = false;
		ctext.Dock = DockStyle.Fill;
		ctext.SelectionStart = 0;
		ctext.SelectionLength = 0;
		ctext.Focus();
		
		//读取行号字体
		LineNumberFont = new Font( "微软雅黑", ctext.Font.Size );
		
		//文本框事件
		ctext.TextChanged += new EventHandler( Text_Changed );
		ctext.MouseDown += new MouseEventHandler( Text_MouseDown );
		ctext.KeyPress += new KeyPressEventHandler( Text_Press );
		ctext.KeyDown += new KeyEventHandler( Text_KeyDown );
		ctext.VScroll += new EventHandler( Text_VScroll );
		ctext.MouseDown += new MouseEventHandler( TextMouseDown );
		
		//添加左边距
		LineLabel = new Label();
		LineLabel.Visible = SystemData.isShowLeftBox;
		LineLabel.Dock = DockStyle.Left;
		LineLabel.Width = 35;
		LineLabel.BackColor = Color.WhiteSmoke;
		LineLabel.BorderStyle = BorderStyle.None;
		LineLabel.Paint += new PaintEventHandler( Label_Paint );
		TextPanel.Controls.Add( LineLabel );
		
		LineLabel.Width = (int)this.CreateGraphics().MeasureString( "0000", LineNumberFont ).Width + 5;
		rec = new Rectangle( 0, 0, LineLabel.Width, ctext.Font.Height );
		
		StartDrawLineNumber();
		
		//竖条
		timer = new Timer();
		timer.Enabled = false;
		timer.Interval = 1;
		timer.Tick += new EventHandler( Timer_Tick );
		isShowLine = false;
		LineIsExist = false;
		
		g = ctext.CreateGraphics();
		p1 = new Pen( Color.Blue, 1 );
		p1.DashStyle = DashStyle.Dash;
		p2 = new Pen( Color.Red, 1 );
		p2.DashStyle = DashStyle.Dash;
		
		//进行文本着色
		Graphics gg = this.CreateGraphics();
		Point Start = new Point( this.Width / 4, this.Height / 3 );
		Point End = new Point( this.Width * 2 / 3, this.Height / 3 );
		gg.DrawString( "正在渲染文字，请您稍等...", new Font( "宋体", 20, FontStyle.Bold ),
		              new SolidBrush( Color.Black ), Start.X + 30, Start.Y - 30 );
		
		ctext.DrawAllArea();
		TextPanel.Visible = true;
		StartDrawLineNumber();
	}
	
	//构造函数(用于创建空文档)
	public FileTabPage( TabControl Tab )
	{
		this.tab = Tab;
		this.BackColor = SystemData.TextBackColor;
		//this.BackColor = Color.Black;
		this.BorderStyle = BorderStyle.None;
		this.Text = "近期文件";
		Tab.Controls.Add( this );
		Tab.SelectTab( this );
		Tab.Refresh();
	}
	
	//判断是否可以关闭
	public bool TryClose()
	{
		if( ctext != null && ctext.isChanged ) {
			DialogResult dlgResult = MessageBox.Show(
				"文件已经被修改,需要保存吗?", "关闭前文件保存",
				MessageBoxButtons.YesNoCancel, MessageBoxIcon.Question );
			if( dlgResult == DialogResult.Yes ) {
				ccode.SetCode( ctext.Text );
				ccode.Save();
			}
			if( dlgResult == DialogResult.No ) {
				//不保存文件直接退出
			}
			if( dlgResult == DialogResult.Cancel ) {
				return false;
			}
		}
		ctext = null;
		this.Dispose();
		return true;
	}
	
	//设置本页面为当前页面
	public void SetCurrent()
	{
		G.ccode = ccode;
		T.ctext = ctext;
		if( ctext != null ) {
			ctext.Focus();
		}
	}
	
	//竖直滚动条事件
	void Text_VScroll( object sender, EventArgs e )
	{
		StartDrawLineNumber();
		if( isShowLine ) {
			DrawLine( true );
			timer.Enabled = true;
		}
	}
	
	//启动重绘
	void StartDrawLineNumber()
	{
		Point pos = new Point(0, 0);
		int firstIndex = ctext.GetCharIndexFromPosition( pos );
		firstLine = ctext.GetLineFromCharIndex( firstIndex );
		pos.X = ctext.ClientRectangle.Width;
		pos.Y = ctext.ClientRectangle.Height;
		int endIndex = ctext.GetCharIndexFromPosition( pos );
		endLine = ctext.GetLineFromCharIndex( endIndex ) + 1;
		currentLine = ctext.GetLineFromCharIndex( ctext.SelectionStart );
		LineLabel.Invalidate();
	}
	
	//行号重绘事件
	void Label_Paint( object sender, PaintEventArgs e )
	{
		if( !SystemData.isShowLeftBox || !SystemData.isShowLineNumber ) {
			return;
		}
		for( int i = firstLine; i <= endLine; ++i ) {
			rec.Y = ctext.GetPositionFromCharIndex( ctext.GetFirstCharIndexFromLine( i ) ).Y;
			
			if( i == currentLine && SystemData.isShowCurrentIcon ) {
				LinearGradientBrush b = new LinearGradientBrush(
					rec, LineLabel.BackColor, Color.FromArgb(164, 168, 164), 90 );
				b.SetBlendTriangularShape( 0.5f );
				e.Graphics.FillRectangle( b, rec );
			}
			e.Graphics.DrawString( ( i + 1 ).ToString().PadLeft( 4, ' ' ),
			                      LineNumberFont, Brushes.SlateGray, rec );
		}
	}
	
	//添加列表
	void AddList()
	{
		if( ControlCenter.isBusy() ) {
			return;
		}
		string UnitName = "";
		for( int i = ctext.SelectionStart - 1; i >= 0; --i ) {
			char c = ctext.Text[ i ];
			if( !CharType.isLetterOrNumber( c ) && c != '#' ) {
					break;
				}
				UnitName = c + UnitName;
			}
			if( UnitName == ""  ) {
				return;
			}
			string FullUnitName = (UnitName == VarType.Root)? VarType.Root: VarType.Root + "." + UnitName;
			
			int UnitIndex = UnitList.GetIndex( FullUnitName);
			if( UnitIndex == -1 ) {
				//return;
			}
			
			string NameSet = null;
			NameSet += FunctionList.GetFunctionNames( FullUnitName );
			NameSet += VarList.GetVarNames( FullUnitName );
			NameSet += StructList.GetStructNames( FullUnitName );
			NameSet += VdataList.GetVdataNames( FullUnitName );
			NameSet += UnitList.GetUnitNames( FullUnitName );
			if( NameSet == null || NameSet == "" ) {
				return;
			}
		string[] Set = NameSet.TrimEnd( ';' ).Split( ';' );
		ShortList.Items.Clear();
		for( int j = 0; j < Set.Length; ++j ) {
			ShortList.Items.Add( Set[ j ] );
		}
		//ShortList.Height = 20 * Set.Length;
		ShortList.Visible = true;
	}
	
	//列表失去焦点
	void List_LostFocus( object sender, EventArgs e )
	{
		ShortList.Visible = false;
	}
	
	//列表获得焦点
	void List_GotFocus( object sender, EventArgs e )
	{
	}
	
	//快捷框压键事件
	void List_KeyDown( object sender, KeyEventArgs e )
	{
		if( e.KeyCode == Keys.Enter && ShortList.Visible ) {
			e.Handled = true;
			ShortList.Visible = false;
			
			string text = ShortList.SelectedItem.ToString().Split( '\t' )[ 0 ].Trim( ' ' );
			ctext.SelectedText = text;
			return;
		}
	}
	
	//文本压键事件
	void Text_KeyDown( object sender, KeyEventArgs e )
	{
		if( ControlCenter.isBusy() ) {
			return;
		}
		//判断是否按下上下键,切换焦点
		if( ( e.KeyCode == Keys.Up || e.KeyCode == Keys.Down ) && ShortList.Visible ) {
			e.Handled = true;
			ShortList.SelectedIndex = 0;
			ShortList.Focus();
		}
	}
	
	//文本按键事件
	void Text_Press( object sender, KeyPressEventArgs e )
	{
//		if( A.WorkBox.扩展信息窗口.Visible ) {
//			A.WorkBox.扩展信息窗口.Visible = false;
//		}
//		if( CThread.runType != CThread.RunType.Wait ) {
//			return;
//		}
		Point p = ctext.GetPositionFromCharIndex( ctext.SelectionStart );
		int x = p.X + 20;
		int y = p.Y + 20;
//		if( x > A.ctext.Width - ShortList.Width ) {
//			x = A.ctext.Width - ShortList.Width;
//		}
//		if( y > A.ctext.Height - ShortList.Height ) {
//			y = A.ctext.Height - ShortList.Height;
//		}
		ShortList.Location = new Point( x, y );
		
		if( e.KeyChar == '.' ) {
			//添加列表
			AddList();
			return;
		}
		if( CharType.isLetterOrNumber( e.KeyChar ) ) {
			return;
		}
		ShortList.Visible = false;
	}
	
	//文本改变事件
	void Text_Changed( object sender, EventArgs e )
	{
		if( ctext.isChanged ) {
			this.Text = FileName + "*";
		}
		StartDrawLineNumber();
	}
	
	//文本保存事件
	void Text_Saved( object sender, EventArgs e )
	{
		this.Text = FileName;
		ctext.isChanged = false;
	}
	
	//文本点击事件
	void Text_MouseDown( object sender, MouseEventArgs e )
	{
		ShortList.Visible = false;
		StartDrawLineNumber();
		
		bool isAtLine = true;
		//判断鼠标是否点击到语法线上
		if( ctext.SelectionStart < ctext.Text.Length && ctext.Text[ ctext.SelectionStart ] == '\t' ) {
			for( int i = ctext.GetFirstCharIndexOfCurrentLine(); i < ctext.SelectionStart; ++i ) {
				if( ctext.Text[ i ] != '\t' ) {
					isAtLine = false;
				}
			}
		}
		else {
			isAtLine = false;
		}
		if( isAtLine ) {
			isShowLine = true;
			DrawLine( true );
		}
		else {
			isShowLine = false;
			if( LineIsExist ) {
				ctext.Refresh();
				LineIsExist = false;
			}
		}
	}
	
	//文本选择事件
	void Text_Select( object sender, EventArgs e )
	{
		if( tab.SelectedTab != this ) {
			tab.SelectTab( this );
		}
	}
	
	//鼠标按下事件,处理智能感知功能
	void TextMouseDown( object sender, MouseEventArgs e )
	{
		try{
		
		T.m.扩展信息窗口.Visible = false;
		T.m.模块信息按键.Visible = false;
		
		//int MouseIndex = ctext.GetCharIndexFromPosition( e.Location );
		int MouseIndex = ctext.SelectionStart;
		
		//先判断是否为文件包含
		int LineNumber = ctext.GetLineFromCharIndex( MouseIndex );
		string Line = ctext.Lines[ LineNumber ].Replace( '\t', ' ').Trim( ' ' );
		
		if( Line.StartsWith( "#include " ) ) {
			string FileOffsetPath = Line.Remove( 0, Config.LOAD.Length + 1 ).Trim( ' ' );
			string FileFullPath = null;
			
			//判断是否为包含当前目录
			if( FileOffsetPath.StartsWith( "\"" ) && FileOffsetPath.EndsWith( "\"" ) ) {
				FileOffsetPath = FileOffsetPath.Remove( 0, 1 );
				FileOffsetPath = FileOffsetPath.Remove( FileOffsetPath.Length - 1 );
				FileFullPath =  G.ccode.FilePath + FileOffsetPath;
			}
			//判断是否为包含库目录
			else if( FileOffsetPath.StartsWith( "<" ) && FileOffsetPath.EndsWith( ">" ) ) {
				FileOffsetPath = FileOffsetPath.Remove( 0, 1 );
				FileOffsetPath = FileOffsetPath.Remove( FileOffsetPath.Length - 1 );
				FileFullPath =  OS.ModuleLibPath + FileOffsetPath;
			}
			//都不是, 报错
			else {
				//G.FlashBox.Run( "[include]指令格式错误: " + Line + ", 包含当前目录文件是 #include \"filename\", 包含库文件是  #include <filename>" );
				return;
			}
			AddExtendMes( 0, 0, FileFullPath, null, new Point( e.Location.X, e.Location.Y ) );
			return;
		}
		//判断是否在组件或函数名上
		string CName = null;
		int CStart = 0;
		//向后搜索当前字符串
		for( int i = MouseIndex; i < ctext.Text.Length; ++i ) {
			char c = ctext.Text[ i ];
			if( CharType.isLetterOrNumber( c ) ) {
				CName += c;
				continue;
			}
			break;
		}
		int PreIndex = 0;
		//向前搜索当前字符串
		for( int i = MouseIndex - 1; i >= 0; --i ) {
			char c = ctext.Text[ i ];
			if( CharType.isLetterOrNumber( c ) ) {
				CName = c + CName;
				continue;
			}
			PreIndex = i;
			CStart = i + 1;
			break;
		}
		//向前搜索上一个字符串
		string PreName = null;
		int PreStart = 0;
		for( int i = PreIndex - 1; i >= 0; --i ) {
			char c = ctext.Text[ i ];
			if( CharType.isLetterOrNumber( c ) ) {
				PreName = c + PreName;
				continue;
			}
			PreStart = i + 1;
			break;
		}
		AutoDetect( PreName, CName, new Point( e.Location.X, e.Location.Y ) );
		
		}catch{}
	}
	
	//智能感知处理
	void AutoDetect( string PreName, string CName, Point p )
	{
		if( CName == null ) {
			return;
		}
		//判断是否为组件
		if( PreName == null ) {
			int Index = UnitList.GetIndex( "#." + CName );
			if( Index != -1 ) {
				AddExtendMes( 1, Index, CName, null, p );
				return;
			}
		}
		else {
			int Index = FunctionList.GetIndex( "#." + PreName + "." + CName );
			if( Index != -1 ) {
				AddExtendMes( 2, Index, PreName, CName, p );
				return;
			}
			Index = VarList.GetStaticIndex( "#." + PreName + "." + CName );
			if( Index != -1 ) {
				AddExtendMes( 3, Index, PreName, CName, p );
				return;
			}
			Index = StructList.GetIndex( "#." + PreName + "." + CName );
			if( Index != -1 ) {
				AddExtendMes( 4, Index, PreName, CName, p );
				return;
			}
			Index = UnitList.GetIndex( "#." + CName );
			if( Index != -1 ) {
				AddExtendMes( 1, Index, CName, null, p );
				return;
			}
		}
	}
	
	//添加扩展信息, ListType为列表类型: 0-路径, 1-组件, 2-函数, 3-变量, 4-结构体类型
	void AddExtendMes( int ListType, int Index, string ModuleName, string MemberName, Point p )
	{
		T.m.模块信息按键.Visible = true;
		
		T.m.label打开文件.Visible = false;
		T.m.buttonExtendReplace1.Visible = false;
		T.m.buttonExtendReplace2.Visible = false;
		T.m.buttonExtendReplace3.Visible = false;
		T.m.buttonExtendReplace4.Visible = false;
		T.m.buttonExtendReplace5.Visible = false;
		T.m.buttonExtendReplace6.Visible = false;
		T.m.buttonExtendReplace7.Visible = false;
		T.m.buttonExtendReplace8.Visible = false;
		T.m.buttonExtendReplace9.Visible = false;
		
		T.m.labelExtendLabel.Text = ModuleName + " " + MemberName;
		T.m.richTextBoxExtend.Text = "";
		
		//设置信息
		if( ListType == 1 ) {
			int WordIndex = UnitList.Get( Index ).WordIndex;
			T.m.buttonExtendButton.Name = WordIndex.ToString();
//			if( MM != null ) {
//				string ExtS = MM.UserName + ":\n" + MM.ModuleMes + "\n--------------------\n";
//				for( int i = 0; i < MM.MemberMesList.Length; ++i ) {
//					if( MM.MemberMesList[ i ].UserName.StartsWith( "OS_" ) ) {
//						continue;
//					}
//					ExtS += MM.MemberMesList[ i ].UserName + ":\n" + MM.MemberMesList[ i ].Mes + "\n";
//				}
//				T.m.richTextBoxExtend.Text = ExtS;
//			}
//			else {
				T.m.richTextBoxExtend.Text = "Sorry, The description of this Module is missing.";
//			}
		}
		else if( ListType == 2 ) {
			int WordIndex = FunctionList.Get( Index ).WordIndex;
			T.m.buttonExtendButton.Name = WordIndex.ToString();
//			if( MeM != null ) {
//				T.m.richTextBoxExtend.Text = MeM.Mes;
//			}
//			else {
				T.m.richTextBoxExtend.Text = "Sorry, The description of this function is missing.";
//			}
		}
		else if( ListType == 3 ) {
			int WordIndex = VarList.Get( Index ).WordIndex;
			T.m.buttonExtendButton.Name = WordIndex.ToString();
//			if( MeM != null ) {
//				T.m.richTextBoxExtend.Text = MeM.Mes;
//			}
//			else {
				T.m.richTextBoxExtend.Text = "Sorry, I can't find the description of this function";
//			}
		}
		else if( ListType == 4 ) {
			int WordIndex = StructList.Get( Index ).WordIndex;
			T.m.buttonExtendButton.Name = WordIndex.ToString();
//			if( MeM != null ) {
//				T.m.richTextBoxExtend.Text = MeM.Mes;
//			}
//			else {
				T.m.richTextBoxExtend.Text = "Sorry, I can't find the description of this struct type";
//			}
		}
		else if( ListType == 0 ) {
			T.m.模块信息按键.Visible = false;
			
			//设置文件路径到 Name 属性中
			T.m.buttonExtendReplace1.Name = ModuleName;
			T.m.buttonExtendReplace2.Name = ModuleName;
			T.m.buttonExtendReplace3.Name = ModuleName;
			T.m.buttonExtendReplace4.Name = ModuleName;
			T.m.buttonExtendReplace5.Name = ModuleName;
			T.m.buttonExtendReplace6.Name = ModuleName;
			T.m.buttonExtendReplace7.Name = ModuleName;
			T.m.buttonExtendReplace8.Name = ModuleName;
			T.m.buttonExtendReplace9.Name = ModuleName;
			T.m.label打开文件.Name = ModuleName;
			
			if( ModuleName.IndexOf( "$chip$" ) != -1 ) {
				T.m.buttonExtendReplace1.Visible = true;
				T.m.buttonExtendReplace1.Text = "MEGA328";
				T.m.buttonExtendReplace2.Visible = true;
				T.m.buttonExtendReplace2.Text = "MEGA644";
				T.m.buttonExtendReplace3.Visible = true;
				T.m.buttonExtendReplace3.Text = "MEGA2560";
				T.m.buttonExtendReplace4.Visible = true;
				T.m.buttonExtendReplace4.Text = "MEGA8";
				T.m.buttonExtendReplace5.Visible = true;
				T.m.buttonExtendReplace5.Text = "MEGA32";
				T.m.buttonExtendReplace6.Visible = true;
				T.m.buttonExtendReplace6.Text = "MEGA64";
				T.m.buttonExtendReplace7.Visible = true;
				T.m.buttonExtendReplace7.Text = "MEGA128";
				T.m.buttonExtendReplace8.Visible = true;
				T.m.buttonExtendReplace8.Text = "MCS_STC89C51";
				T.m.buttonExtendReplace9.Visible = true;
				T.m.buttonExtendReplace9.Text = "VM";
			}
			else if( ModuleName.IndexOf( "$language$" ) != -1 ) {
				T.m.buttonExtendReplace1.Visible = true;
				T.m.buttonExtendReplace1.Text = "c";
				T.m.buttonExtendReplace2.Visible = true;
				T.m.buttonExtendReplace2.Text = "chinese";
			}
			else if( ModuleName.IndexOf( "$run$" ) != -1 ) {
				T.m.buttonExtendReplace1.Visible = true;
				T.m.buttonExtendReplace1.Text = "run";
				T.m.buttonExtendReplace2.Visible = true;
				T.m.buttonExtendReplace2.Text = "sim";
			}
			else {
				T.m.label打开文件.Visible = true;
			}
			T.m.richTextBoxExtend.Text = "文件路径:\n" + ModuleName;
		}
		else {
			//===不可能到这里
		}
		T.m.扩展信息窗口.Location = new Point( p.X + 60 + T.m.splitContainerMain.SplitterDistance, p.Y + 90 );
		T.m.扩展信息窗口.Visible = true;
	}
	
	//画竖线
	void DrawLine( bool isAllDraw )
	{
		LineIsExist = true;
		
		Point pos = new Point(0, 0);
		int firstIndex = ctext.GetCharIndexFromPosition(pos);
		int firstLine = ctext.GetLineFromCharIndex(firstIndex);
		pos.X = ctext.ClientRectangle.Width;
		pos.Y = ctext.ClientRectangle.Height;
		int endIndex = ctext.GetCharIndexFromPosition(pos);
		int endLine = ctext.GetLineFromCharIndex(endIndex);
		
		for( int line = firstLine; line <= endLine; ++line ) {
			if( !isAllDraw && line - 4 >= firstLine && line + 5 <= endLine ) {
				continue;
			}
			int index = ctext.GetFirstCharIndexFromLine( line );
			int LineLength = index + ctext.Lines[ line ].Length;
			int FontHeight = ctext.Font.Height;
			for( int i = index; i < LineLength; ++i ) {
				if( ctext.Text[ i ] != '\t' ) {
					break;
				}
				if( i == index ) {
					continue;
				}
				Point ph = ctext.GetPositionFromCharIndex( i );
				Point pl = new Point( ph.X, ph.Y + FontHeight + 1 );
				if( ( i - index ) % 2 == 0 ) {
					g.DrawLine( p1, ph, pl );
				}
				else {
					g.DrawLine( p2, ph, pl );
				}
			}
		}
	}
	
	//到时间
	void Timer_Tick( object sender, EventArgs e )
	{
		timer.Enabled = false;
		DrawLine( false );
	}
}
}





