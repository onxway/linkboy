﻿
//分页文件类
//发现的BUG:Tab的标签尺寸是默认的,改了字体也不行;

namespace n_myTabControl
{
using System;
using System.Drawing;
using System.Windows.Forms;

using T;
using n_FileTabPage;
using n_OS;

//分页列表控件
public class myTabControl : TabControl
{
	Icon CloseIconN;
	Icon CloseIconOn;
	Icon NewTabIcon;
	
	//其他标签字体颜色
	Font FontDefault;
	SolidBrush bruFontOther;
	
	//当前标签字体颜色
	Font FontCurrent;
	SolidBrush bruFontCurrent;
	
	//标签文字格式
	StringFormat sf;
	StringFormat sf1;
	
	//上次选中的Tab
	public TabPage LastSelectedTab;
	
	Brush AddBrush;
	Brush myBrush;
	Brush labelBrush;
	Pen myPen;
	Pen SidePen;
	
	//构造函数
	public myTabControl()
	{
		SetStyle(ControlStyles.UserPaint,true);
		//DrawMode = TabDrawMode.OwnerDrawFixed;
		SelectedIndex = 0;
		TabIndex = 0;
		FontDefault = new Font( "微软雅黑", 11, FontStyle.Regular );
		FontCurrent = new Font( "微软雅黑", 11, FontStyle.Bold );
		this.BackColor = Color.Black;
		
		sf = new StringFormat();
		sf.Alignment = StringAlignment.Center;
		sf.LineAlignment = StringAlignment.Center;
		sf1 = new StringFormat();
		sf1.Alignment = StringAlignment.Near;
		sf1.LineAlignment = StringAlignment.Center;
		
		this.MouseDown += new MouseEventHandler( myMouseDown );
		this.MouseMove += new MouseEventHandler( myMouseMove );
		this.MouseLeave += new EventHandler( myMouseLeave );
		
		LastSelectedTab = null;
		
		labelBrush = new SolidBrush( Color.FromArgb( 23, 161, 165 ) );
		
		//加载图标
		CloseIconN = new Icon( OS.SystemRoot + "Resource" + OS.PATH_S + "CloseIcon.ico" );
		CloseIconOn = new Icon( OS.SystemRoot + "Resource" + OS.PATH_S + "CloseIconOn.ico" );
		NewTabIcon = new Icon( OS.SystemRoot + "Resource" + OS.PATH_S + "NewTabIcon.ico" );
		
		bruFontOther = new SolidBrush( Color.Black );// 其他标签字体颜色
		bruFontCurrent = new SolidBrush( Color.Black );// 当前标签字体颜色
		
		AddBrush = Brushes.White;
		myBrush = Brushes.White;
		myPen = new Pen( Color.White, 1 );
		
		SidePen = new Pen( Color.Blue, 1 );
	}
	
	//鼠标移出事件
	void myMouseLeave(object sender, EventArgs e)
	{
		this.Invalidate();
	}
	
	//鼠标移动事件
	void myMouseMove(object sender, MouseEventArgs e)
	{
		/*
		bool tMouseInCloseIcon = MouseInCloseIcon;
		MouseInCloseIcon = false;
		if( e.Y < this.GetTabRect( 0 ).Bottom ) {
			
			Point pt = new Point(e.X, e.Y);
			Rectangle recTab = new Rectangle();
			for (int i = 0; i < this.TabCount; ++i ) {
				if( this.SelectedIndex != i ) {
					continue;
				}
				recTab = this.GetTabRect( i );
				if( recTab.Contains( pt ) ) {
					
					MouseIndex = i;
					
					//如果在空文档上,返回
					if( ( (FileTabPage)this.SelectedTab ).ctext == null ) {
						//???
					}
					if( pt.X > recTab.X + recTab.Width - recTab.Height ) {
						//在关闭按钮区
						MouseInCloseIcon = true;
					}
				}
			}
		}
		if( MouseInCloseIcon && !tMouseInCloseIcon ||
			!MouseInCloseIcon && tMouseInCloseIcon ) {
			Rectangle r = this.GetTabRect( MouseIndex );
			this.Invalidate( r );
		}
		*/
	}
	
	//单击分页文本
	void myMouseDown(object sender, MouseEventArgs e)
	{
		/*
		if( ( e.Y < this.GetTabRect( 0 ).Bottom ) && ( e.Button == MouseButtons.Left ) ) {
			Point pt = new Point(e.X, e.Y);
			Rectangle recTab = new Rectangle();
			for (int i = 0; i < this.TabCount; ++i ) {
				recTab = this.GetTabRect( i );
				if( recTab.Contains( pt ) ) {
					
					FileTabPage FT = (FileTabPage)this.SelectedTab;
					FT.SetCurrent();
					
					//如果单击空文档,返回
					if( FT.ctext == null ) {
						LastSelectedTab = this.SelectedTab;
						
						return;
					}
					if( pt.X > recTab.X + recTab.Width - recTab.Height && LastSelectedTab == this.TabPages[ i ] ) {
						CloseFile();
					}
					else {
						LastSelectedTab = this.SelectedTab;
					}
				}
			}
		}
		*/
	}
	
	/*
	
	//重载绘图事件
	protected override void OnPaint(PaintEventArgs e)
	{
		base.OnPaint(e);
		ReDrawItem( e.Graphics );
	}
	
	//重绘事件
	void ReDrawItem( Graphics g )
	{   
		//获取TabControl主控件的工作区域
		//Rectangle rec = this.ClientRectangle;
		
		int XStart = 0;
		int XEnd = this.Width - 1;
		int YStart = 22;
		int YEnd = this.Height - 1;
		int RWidth = XEnd - XStart;
		int RHeight = YEnd - YStart;
		
		//绘制主控件的背景
		g.FillRectangle( myBrush, 0, 0, this.Width, this.Height );
		
		//绘制标题栏背景
		//g.DrawImage( ToolImage, 0, 0, this.Width, YStart );
		g.FillRectangle( labelBrush, 0, 0, this.Width, YStart );
		
		//绘制大边框
		//g.DrawRectangle( SidePen, XStart, YStart, RWidth, RHeight );
		
		//绘制标签样式
		for(int i = 0; i < this.TabPages.Count; ++i ) {
			
			//获取标签头的工作区域
			Rectangle recChild = this.GetTabRect( i );
			
			Icon CloseIcon = null;
			SolidBrush TextBrush;
			Font f;
			
			if( this.SelectedIndex == i ) {
				TextBrush = bruFontCurrent;
				f = FontCurrent;
				
				CloseIcon = CloseIconN;
			}
			else {
				TextBrush = bruFontOther;
				f = FontDefault;
			}
			if( MouseIndex == i && MouseInCloseIcon ) {
				CloseIcon = CloseIconOn;
			}
			//绘制标签外框
			if( this.SelectedIndex == i ) {
				g.FillRectangle( myBrush, recChild );
				//g.DrawRectangle( SidePen, recChild );
				//g.DrawLine( myPen,
			    //      recChild.X, recChild.Y + recChild.Height,
			    //      recChild.X + recChild.Width, recChild.Y + recChild.Height );
			}
			//绘制新建标签外框
			if( this.SelectedIndex == i && i == this.TabPages.Count - 1 ) {
				Rectangle recChildAdd = new Rectangle(
					recChild.X + 4, recChild.Y + 3, recChild.Width - 6, recChild.Height );
				g.FillRectangle( AddBrush, recChildAdd );
			}
			if( i != this.TabPages.Count - 1 ) {
				
				//绘制关闭按钮和标签头的文字
				if( CloseIcon != null ) {
					
					//Rectangle recChild1 = new Rectangle( recChild.X, recChild.Y, recChild.Width - 18, recChild.Height );
					g.DrawString( this.TabPages[i].Text, f, TextBrush, recChild, sf1 );
					g.DrawIcon( CloseIcon, recChild.X + recChild.Width - 18, recChild.Y + 2 );
				}
				else {
					g.DrawString( this.TabPages[i].Text, f, TextBrush, recChild, sf );
				}
			}
		}
		//获取标签头的工作区域
		Rectangle recChildClose = this.GetTabRect( this.TabPages.Count - 1 );
		
		//绘制新建按钮
		g.DrawIcon( NewTabIcon, recChildClose.X + 1 + recChildClose.Width / 2 - 8, recChildClose.Y + 3 );
	}
	
	*/
	
	//关闭当前文件事件
	public void CloseFile()
	{
		if( T.ctext == null ) {
			A.CodeErrBox.Run( "无法关闭文件, 现在没有打开的文件" );
			return;
		}
		FileTabPage FT = (FileTabPage)this.SelectedTab;
		int Index = this.SelectedIndex;
		if( !FT.TryClose() ) {
			return;
		}
		if( FT == null ) {
			MessageBox.Show( "ERROR" );
		}
		else {
			if( Index == this.TabCount - 1 ) {
				Index -= 1;
				if( Index < 0 ) {
					Index = 0;
				}
			}
			this.SelectedIndex = Index;
			LastSelectedTab = this.SelectedTab;
			((FileTabPage)this.SelectedTab).SetCurrent();
		}
	}
}
}



