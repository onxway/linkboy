﻿
//链表类
using System;

namespace n_TextLinker
{
public class Linker
{
	//构造函数,建立有一个元素的链表
	public Linker()
	{
		CurrentNote = new Note( 0, 0, null, null );
	}
	
	//在当前节点处追加节点
	public void Add( int SelectionStart, int Index, string OldString, string NewString )
	{
		Note NewNote = new Note( SelectionStart, Index, OldString, NewString );
		NewNote.LastNote = CurrentNote;
		CurrentNote.NextNote = NewNote;
		CurrentNote = NewNote;
	}
	
	//把当前节点向首部移动
	public bool MovetoHead()
	{
		if( CurrentNote.LastNote == null ) 
		{
			return false;
		}
		CurrentNote = CurrentNote.LastNote;
		return true;
	}
	
	//把当前节点向尾部移动
	public bool MovetoEnd()
	{
		if( CurrentNote.NextNote == null ) 
		{
			return false;
		}
		CurrentNote = CurrentNote.NextNote;
		return true;
	}
	
	//读当前节点
	public Note GetCurrentNote()
	{
		return CurrentNote;
	}
	
	private Note CurrentNote;	//当前节点
}
//**********************************************
//链表节点类
public class Note
{
	//构造函数,建立链表节点
	public Note( int Selectionstart, int Index, string OldString, string NewString )
	{
		//SelectionStart = Selectionstart;
		Newstring = NewString;
		Oldstring = OldString;
		index = Index;
		LastNote = null;
		NextNote = null;
	}
	//但愿去掉这个后不再出现撤销不稳定的情况...
	//public int SelectionStart; //选中的起点 在 undo 和 redo 中用到两次
	public int index;	//字符串起点索引
	public string Newstring;	//新字符串
	public string Oldstring;	//旧字符串
	public Note LastNote;	//上一个
	public Note NextNote;	//下一个
}
}

