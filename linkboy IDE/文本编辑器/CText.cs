﻿
namespace n_ctext
{
using System;
using System.Drawing;
using System.Runtime.InteropServices;
using System.Windows.Forms;

using n_CharType;
using n_ControlCenter;
using n_Drawer;
using n_MainSystemData;
using HWND = System.IntPtr;
using n_LaterFilesManager;

//高级文本类
public class CTextBox : RichTextBox
{
	public bool EnableColor;
	
	//构造函数
	public CTextBox( string StartCode )
	{
		this.Text = StartCode;
		Recorder = new EditRecorder( this );
		
		this.Multiline = true;
		this.ScrollBars = RichTextBoxScrollBars.Both;
		this.WordWrap = false;
		this.AcceptsTab = true;
		this.DetectUrls = false;
		this.Visible = true;
		//this.Font = SystemData.SourceTextFont;
		this.BackColor = SystemData.TextBackColor;
		this.ForeColor = Color.FromArgb( 0, 0, 0 );//文本默认颜色是黑色
		this.BorderStyle = BorderStyle.None;
		this.Dock = DockStyle.Fill;
		
		DrawPythonCode = false;
		EnableColor = true;
		
		this.SelectAll();
		this.SelectionFont = this.Font;
		
		//变量初始化
		isChanged = false;
		isAllLine = true;
		isUndoOrRedo = false;
		FirstClick = FirstClickNumber;
		FocusFirstClick = true;
		NotRefresh = false;
		isNew = true;
		LastText = this.Text;
		isSystemFile = false;
		LastSelectionStart = 0;
		BackColorExist = false;
		
		//添加事件
		this.TextChanged += new EventHandler( Text_Changed );
		this.KeyDown += new KeyEventHandler( Text_KeyDown );
		this.MouseDown += new MouseEventHandler( Text_MouseDown );
		this.Mydoubleclick += new MydoubleclickEventHandler( Text_DoubleClick );
		this.GotFocus += new EventHandler( Text_GotFocus );
		this.KeyPress += new KeyPressEventHandler( Text_KeyPress );
		this.MyMouseMove += new MyMouseMoveEventHandler( Text_MouseMove );
		
		//SetLineSpace( 20 );
	}
	
	//选项--注释
	public void TurnNoteAndCode()
	{
		if( this.Text == "" ) {
			return;
		}
		this.NotRefresh = true;
		SendMessage( this.Handle, WM_SETREDRAW, 0, IntPtr.Zero );
		int StartLine,EndLine;
		int Length = this.SelectionLength;
		AreaOfSelectedLine( out StartLine, out EndLine );
		bool toCode = this.Lines[ StartLine ].StartsWith( "//" );
		
		for( int CurrentLine = StartLine; CurrentLine <= EndLine; ++CurrentLine ) {
			this.SelectionStart = this.GetFirstCharIndexFromLine( CurrentLine );
			if( toCode ) {
				if( this.Lines[ CurrentLine ].StartsWith( "//" ) ) {
					this.SelectionLength = 2;
					this.SelectedText = "";
				}
			}
			else {
				char[] split = { ' ', '\t' };
				this.SelectionLength = 0;
				if( this.Lines[ CurrentLine ].Trim( split ).Length != 0 ) {
					this.SelectedText = "//";
				}
			}
			
		}
		SendMessage( this.Handle, WM_SETREDRAW, 1, IntPtr.Zero );
		this.Refresh();
		this.SelectionLength = 0;
		this.NotRefresh = false;
	}
	
	//选项--对齐
	public void FormatCode()
	{
		if( this.Text == "" ) {
			return;
		}
		this.NotRefresh = true;
		SendMessage( this.Handle, WM_SETREDRAW, 0, IntPtr.Zero );
		int Length = this.SelectionLength;
		int StartLine,EndLine;
		this.AreaOfSelectedLine( out StartLine, out EndLine );
		char[] Space = { ' ', '\t' };
		int StLine = 0;
		int Shift = 0;
		for( int i = StartLine - 1; i >= 0; -- i ) {
			string Select = this.Lines[ i ].Trim( Space );
			if( Select.EndsWith( "{" ) || Select.StartsWith( "{" ) ) {
				StLine = i + 1;
				while( this.Lines[ i ][ Shift ] == '\t' ) {
					++Shift;
				}
				++Shift;
				break;
			}
		}
		while( StLine <= EndLine ) {
			string Select = this.Lines[ StLine ].Trim( Space );
			if( ( Select.EndsWith( "}" ) || Select.StartsWith( "}" ) ) && Select.IndexOf( "{" ) == -1 ) {
				--Shift;
			}
			if( Shift < 0 ) {
				Shift = 0;
			}
			string SpaceSet = new String( '\t', Shift );
			SelectLine( StLine );
			if( this.SelectedText != SpaceSet + Select ) {
				this.SelectedText = SpaceSet + Select;
			}
			++StLine;
			if( Select.EndsWith( "{" ) || Select.StartsWith( "{" ) ) {
				++Shift;
			}
		}
		SendMessage( this.Handle, WM_SETREDRAW, 1, IntPtr.Zero );
		this.Refresh();
		this.SelectionLength = 0;
		this.NotRefresh = false;
	}
	
	//撤销操作
	public void Undone()
	{
		isUndoOrRedo = true;
		Recorder.Undo();
	}
	
	//重复操作
	public void Redone()
	{
		isUndoOrRedo = true;
		Recorder.Redo();
	}
	
	//选项--复制
	public void SCopy()
	{
		if( this.SelectionLength == 0 ) {
			A.CodeErrBox.Run( "在复制操作之前请先选中一段文字" );
			return;
		}
		this.Copy();
	}
	
	//选项--剪切
	public void SCut()
	{
		if( this.isSystemFile && !SystemData.isSuperUser ) {
			return;
		}
		if( this.SelectionLength == 0 ) {
			A.CodeErrBox.Run( "在剪切操作之前请先选中一段文字" );
			return;
		}
		this.Cut();
	}
	
	//选项--粘贴
	public void SPaste()
	{
		if( this.isSystemFile && !SystemData.isSuperUser ) {
			return;
		}
		IDataObject data= Clipboard.GetDataObject();
        if(data.GetDataPresent( typeof( string ) ) ) {
			string s = (string)data.GetData( typeof( string ) );
			this.SelectedText = s;
        }
	}
	
	//选项--全选
	public void SSelectAll()
	{
		this.SelectionStart = 0;
		this.SelectionLength = this.Text.Length;
	}
	
	//自定义事件列表:
	
	//鼠标双击事件委托
	delegate void MydoubleclickEventHandler( object sender, MouseEventArgs e );
	//鼠标移动事件委托
	delegate void MyMouseMoveEventHandler( object sender, MouseEventArgs e );
	//鼠标双击事件
	event MydoubleclickEventHandler Mydoubleclick;
	//鼠标移动事件
	event MyMouseMoveEventHandler MyMouseMove;
	
	//拦截系统消息--鼠标双击, 鼠标移动, 滚轮移动
	protected override void WndProc( ref Message m )
	{
		//移动鼠标时发生，同WM_MOUSEFIRST
		//const int WM_MOUSEMOVE = 0x200;
		//双击鼠标左键
		const int WM_LBUTTONDBLCLK = 0x203;
		//转动滚轮
		const int WM_MOUSEWHEEL = 0x020a;
		//行滚动
		const int EM_LINESCROLL = 0x00b6;
		
		//滚轮移动
		if ( m.Msg == WM_MOUSEWHEEL ) {
			
			//不同系统上结果不一样, 保险起见
			if( ( (long)m.WParam ) > 0 && (long)m.WParam < 0x7FFFFFFF ) {
				SendMessage( this.Handle, EM_LINESCROLL, 0, (IntPtr)( -4 ) );
			}
			else {
				SendMessage( this.Handle, EM_LINESCROLL, 0, (IntPtr)( 4 ) );
			}
			return;
		}
		//鼠标左键双击事件
		if ( m.Msg == WM_LBUTTONDBLCLK ) {
			MouseEventArgs e = new MouseEventArgs( MouseButtons.Left, 2, MousePosition.X, MousePosition.Y, 0 );
			if( Mydoubleclick != null ) {
				Mydoubleclick( this, e );
			}
			return;
		}
		//鼠标移动事件
		/*
		if ( m.Msg == WM_MOUSEMOVE ) {
			Point p = new Point( 0, 0 );
			MouseButtons button = MouseButtons & MouseButtons.Left;
			MouseEventArgs e = new MouseEventArgs( button, 1,
			    MousePosition.X - this.PointToScreen( p ).X,
				MousePosition.Y - this.PointToScreen( p ).Y,
				0 );
			if( MyMouseMove != null ) {
				MyMouseMove( this, e );
			}
			return;
		}
		*/
		base.WndProc(ref m);
	}
	
	//鼠标移动事件, 鼠标移动时调用坐标类会干扰输入法
	void Text_MouseMove( object sender, MouseEventArgs e )
	{
		if( isNew ) {
			return;
		}
		//是否选中操作
		if( e.Button != MouseButtons.Left || this.Text.Length == 0 ) {
			return;
		}
		if( FirstClick != 0 ) {
			--FirstClick;
			return;
		}
		if( FocusFirstClick ) {
			FocusFirstClick = false;
			return;
		}
		int CurrentLength = 0;
		int CurrentStart = 0;
		//整行文本选择
		if( isAllLine ) {
			if( AllLineTick > 0 ) {
				AllLineTick -= 1;
				return;
			}
			int CIndex = this.GetCharIndexFromPosition( e.Location );
			int CLine = this.GetLineFromCharIndex( CIndex );
			int CFirstIndex = this.GetFirstCharIndexFromLine( CLine );
			if( CFirstIndex >= StartIndex ) {
				CurrentLength = CFirstIndex - StartIndex + this.Lines[ CLine ].Length;
				CurrentStart = StartIndex;
			}
			else {
				int Line = this.GetLineFromCharIndex( StartIndex );
				CurrentLength = StartIndex - CFirstIndex + this.Lines[ Line ].Length;
				CurrentStart = CFirstIndex;
			}
			//去除行起始的制表符
			while( CurrentLength != 0 && this.Text[ CurrentStart ] == '\t' ) {
				++CurrentStart;
				--CurrentLength;
			}
		}
		//单个字符选择
		else {
			int UCIndex = this.GetCharIndexFromPosition( e.Location );
			if( UCIndex >= FIndex ) {
				CurrentStart = FIndex;
				CurrentLength = UCIndex - FIndex;
			}
			else {
				CurrentStart = UCIndex;
				CurrentLength = FIndex - UCIndex;
			}
		}
		if( this.SelectionLength == CurrentLength ) {
			return;
		}
		SendMessage( this.Handle, WM_SETREDRAW, 0, IntPtr.Zero );
		this.SelectionStart = CurrentStart;
		this.SelectionLength = CurrentLength;
		SendMessage( this.Handle, WM_SETREDRAW, 1, IntPtr.Zero );
		this.Refresh();
	}
	
	//鼠标按键事件
	void Text_MouseDown( object sender, MouseEventArgs e )
	{
		T.T.HideMes();
		
		if( BackColorExist ) {
			this.SelectLine( BackColorLine );
			this.SelectionBackColor = SystemData.TextBackColor;
			this.SelectionLength = 0;
			this.SelectionStart += this.Lines[ BackColorLine ].Length;
			BackColorExist = false;
		}
		
		//用于记录撤消操作,不重要
		LastSelectionStart = this.SelectionStart;
		
		if( e.Button != MouseButtons.Left || this.Text.Length == 0 ) {
			return;
		}
		FIndex = this.GetCharIndexFromPosition( e.Location );
		int Line = this.GetLineFromCharIndex( FIndex );
		StartIndex = this.GetFirstCharIndexFromLine( Line );
		//判断光标是否在行尾
		if( this.Text[ FIndex ] != '\n' ) {
			isAllLine = false;
		}
		else {
			isAllLine = true;
			AllLineTick = 5;
		}
		FirstClick = FirstClickNumber;
		
		//选中最后一个时光标变大, 效果不好
		if( SelectionStart == TextLength && SelectionStart > 0 ) {
			SelectionStart = TextLength - 1;
		}
	}
	
	//程序文本鼠标双击事件
	void Text_DoubleClick( object sender, EventArgs e )
	{
		if( this.Text.Length == 0 ) {
			return;
		}
		int start = this.GetFirstCharIndexOfCurrentLine();
		SendMessage( this.Handle, WM_SETREDRAW, 0, IntPtr.Zero );
		if( this.SelectionStart == this.Text.Length ) {
			--this.SelectionStart;
		}
		if( CharType.isLetterOrNumber( this.Text[ this.SelectionStart ] ) ||
		   ( this.SelectionStart > 0 &&
		    CharType.isLetterOrNumber( this.Text[ this.SelectionStart - 1 ] ) ) ) {
			while( this.SelectionStart > 0 ) {
				--this.SelectionStart;
				if( !CharType.isLetterOrNumber( this.Text[ this.SelectionStart ] ) ) {
					++this.SelectionStart;
					break;
				}
			}
			int i = this.SelectionStart + 1;
			while( i < this.Text.Length && CharType.isLetterOrNumber( this.Text[ i ] ) ) {
				++i;
			}
			this.SelectionLength = i - this.SelectionStart;
		}
		SendMessage( this.Handle, WM_SETREDRAW, 1, IntPtr.Zero );
		this.Refresh();
		FirstClick = FirstClickNumber;
	}
	
	//程序文本改变事件
	public void Text_Changed( object sender, EventArgs e )
	{
		if( ControlCenter.isBusy() ) {
			return;
		}
		if( isNew ) {
			return;
		}
		/*
		if( Init ) {
			Font = Drawer.FE;
			SelectionFont = Drawer.FE;
		
			SelectAll();
		
			SelectionFont = Drawer.FE;
			Font = Drawer.FE;
		
			SelectionStart = 0;
			SelectionLength = 0;
		}
		*/
		
		int FirstIndex, EndIndex;
		string NewText = this.Text;
		string OldText = LastText;
		FindChangedText( out FirstIndex, out EndIndex, ref OldText, ref NewText );
		if( this.Text != LastText ) {
			
			isChanged = true;
			SetAreaColor( FirstIndex, EndIndex );
		}
		if( !isUndoOrRedo && this.Text != LastText ) {
			Recorder.AddOper( LastSelectionStart, FirstIndex, OldText, NewText );
		}
		isUndoOrRedo = false;
		LastText = this.Text;
		LastSelectionStart = this.SelectionStart;
	}
	
	//文本按键事件
	void Text_KeyPress( object sender, KeyPressEventArgs e )
	{
		if( ControlCenter.isBusy() ) {
			return;
		}
		if( ControlCenter.isBusy() ) {
			return;
		}
		if( this.isSystemFile && !SystemData.isSuperUser ) {
			return;
		}
		if( e != null &&
		   ( e.KeyChar == '+' || e.KeyChar == '-' || e.KeyChar == '*' || e.KeyChar == '/' || e.KeyChar == '=' ) ) {
			AddNote( e.KeyChar );
		}
		if( this.SelectionLength != 0 ) {
			int Length = this.SelectionLength;
			SendMessage( this.Handle, WM_SETREDRAW, 0, IntPtr.Zero );
			this.SelectionLength = Length;
			this.SelectedText = "";
			SendMessage( this.Handle, WM_SETREDRAW, 1, IntPtr.Zero );
			this.Refresh();
		}
	}
	
	//文本按键事件
	void Text_KeyDown( object sender, KeyEventArgs e )
	{
		if( ControlCenter.isBusy() ) {
			return;
		}
		if( this.isSystemFile && !SystemData.isSuperUser && e.KeyCode == Keys.Return ) {
			e.Handled = true;
			return;
		}
		//回车键
		if( e.KeyCode == Keys.Return ) {
			SwitchReturnButton();
			e.Handled = true;
			return;
		}
		//Home键
		if( e.KeyCode == Keys.Home ) {
			GotoHome();
			e.Handled = true;
		}
		//End键
		if( e.KeyCode == Keys.End ) {
			GotoEnd();
			e.Handled = true;
		}
		if( e.KeyCode == Keys.Up ||
		    e.KeyCode == Keys.Down ||
		    e.KeyCode == Keys.Left ||
		    e.KeyCode == Keys.Right ||
		    e.KeyCode == Keys.Home ||
		    e.KeyCode == Keys.End ) {
			return;
		}
		if( e.KeyCode == Keys.Back ||
		    e.KeyCode == Keys.Delete ) {
			if( this.SelectionLength != 0 ) {
				Text_KeyPress( null, null );
				e.Handled = true;
			}
			return;
		}
		e.Handled = true;
	}
	
	//文本获得焦点事件
	void Text_GotFocus( object sender, EventArgs e )
	{
		FocusFirstClick = true;
	}
	
	//自动添加注释
	void AddNote( char c )
	{
		int CurrentIndex = this.SelectionStart;//当前的序号
		if( CurrentIndex >= 2 && this.Text[ CurrentIndex - 1 ] == '/' && this.Text[ CurrentIndex - 2 ] == '/' ) {
			if( c == '-' ) {
				this.SelectedText = "".PadLeft( 50, c );
			}
			else {
				this.SelectedText = "".PadLeft( 80, c );
			}
		}
	}
	
	//光标转到当前行的起点
	void GotoHome()
	{
		int Line = this.GetLineFromCharIndex( this.SelectionStart );
		int Index = this.GetFirstCharIndexFromLine( Line );
		for( int i = Index; i < Index + this.Lines[ Line ].Length; ++i ) {
			if( this.Text[ i ] != ' ' && this.Text[ i ] != '\t' ) {
				this.SelectionStart = i;
				break;
			}
		}
	}
	
	//光标转到当前行的终点
	void GotoEnd()
	{
		int Line = this.GetLineFromCharIndex( this.SelectionStart );
		int Index = this.GetFirstCharIndexFromLine( Line );
		for( int i = Index + this.Lines[ Line ].Length - 1; i >= Index; --i ) {
			if( this.Text[ i ] != ' ' && this.Text[ i ] != '\t' ) {
				this.SelectionStart = i + 1;
				break;
			}
		}
	}
	
	//设置一个区域的文本颜色
	void SetAreaColor( int StartIndex, int EndIndex )
	{
		if( !EnableColor ) {
			SelectAll();
			SelectionFont = Drawer.FE;
			//SelectionFont = Drawer.FZ;
			SelectionLength = 0;
			return;
		}
		if( this.Text == "" ) {
			return;
		}
		int CurrentIndex = this.SelectionStart;//当前的序号
		while( StartIndex > 0 ) {
			--StartIndex;
			char c = this.Text[ StartIndex ];
			if( c == '\n' ) {
				++StartIndex;
				break;
			}
		}
		int Length = this.Text.Length;
		while( EndIndex < Length - 1 ) {
			char c = this.Text[ EndIndex ];
			
			//注意,有可能输入注释导致当前行全部着色,所以不能判断空格和制表符
//			if( c == ' ' || c == '\n' || c == '\t' ) {
			
			if( c == '\n' ) {
				--EndIndex;
				break;
			}
			++EndIndex;
		}
		SendMessage( this.Handle, WM_SETREDRAW, 0, IntPtr.Zero );
		
		if( DrawPythonCode ) {
			Drawer.PyDrawArea( this, StartIndex, EndIndex );
		}
		else {
			Drawer.DrawArea( this, StartIndex, EndIndex );
		}
		
		this.SelectionStart = CurrentIndex;
		if( !this.NotRefresh ) {
			SendMessage( this.Handle, WM_SETREDRAW, 1, IntPtr.Zero );
			this.Refresh();
		}
	}
	
	//文本颜色渲染区域
	public void DrawAllArea()
	{
		//return;
		if( !EnableColor ) {
			SelectAll();
			SelectionFont = Drawer.FE;
			//SelectionFont = Drawer.FZ;
			SelectionLength = 0;
			return;
		}
		int i = this.Text.IndexOf( "//[配置信息开始]," );
		int mi = this.Text.IndexOf( "//[工作台]," );
		if( i == -1 || mi == -1 ) {
			i = this.Text.Length - 1;
		}
		else {
			i -= 1;
		}
		if( DrawPythonCode ) {
			Drawer.PyDrawArea( this, 0, i );
		}
		else {
			Drawer.DrawArea( this, 0, i );
		}
	}
	
	//设置回车等效字符串
	void SwitchReturnButton()
	{
		//跳过空白文本
		if( this.Text.Length == 0 ) {
			this.Text = "\n";
			++this.SelectionStart;
			return;
		}
		int CurrentIndex = this.SelectionStart;//当前的序号
		int CurrentLine = this.GetLineFromCharIndex( CurrentIndex );//当前的行号
		int StartIndex = this.GetFirstCharIndexFromLine( CurrentLine );
		string s = this.Lines[ CurrentLine ];
		int n;
		
		char hec = '\t';
		if( s.Length > 0 && s[0] == ' ' ) {
			hec = ' ';
		}
		
		for( n = 0; n < s.Length; ++n ) {
			if( s[ n ] != hec ) {
				break;
			}
		}
		if( s.TrimEnd( " \t".ToCharArray() ).EndsWith( "{" ) && CurrentIndex >= StartIndex + s.Length ) {
			++n;
			string Pad = "\n" + new string( hec, n );
			if( CurrentLine + 1 == this.Lines.Length ) {
				Pad += "\n" + new string( hec, n - 1 ) + "}";
			}
			if( CurrentLine + 1 < this.Lines.Length ) {
				s = this.Lines[ CurrentLine + 1 ];
				if( !s.StartsWith( new string( hec, n ) ) && !s.StartsWith( new string( hec, n - 1 ) + "}" ) ) {
					Pad += "\n" + new string( hec, n - 1 ) + "}";
				}
			}
			int Start = this.SelectionStart;
			this.SelectedText = Pad;
			this.SelectionStart = Start + n + 1;
		}
		else {
			this.SelectedText = "\n" + new string( hec, n );
		}
	}
	
	//搜索改变部分的字符串
	void FindChangedText( out int StartIndex, out int EndIndex, ref string LastText, ref string CurrentText )
	{
		if( LastText == CurrentText ) {
			StartIndex = this.SelectionStart;
			EndIndex = this.SelectionStart;
			return;
		}
		//搜索起点
		int Start;
		for( Start = 0; Start < LastText.Length && Start < CurrentText.Length ; ++Start ) {
			if( LastText[ Start ] != CurrentText[ Start ] ) {
				break;
			}
		}
		//搜索终点
		int LastEnd;
		int CurrentEnd;
		for( CurrentEnd = CurrentText.Length - 1, LastEnd = LastText.Length - 1;
		     CurrentEnd > Start && LastEnd > Start;
		     --CurrentEnd, --LastEnd ) {
			if( CurrentText[ CurrentEnd ] != LastText[ LastEnd ] ) {
				break;
			}
		}
		//提取改变的字符串
		LastText = LastText.Substring( Start, LastEnd - Start + 1 );
		CurrentText = CurrentText.Substring( Start, CurrentEnd - Start + 1 );
		StartIndex = Start;
		EndIndex = CurrentEnd;
	}
	
	//计算选中行的起点行号和终点行号
	void AreaOfSelectedLine( out int StartLine, out int EndLine )
	{
		StartLine = this.GetLineFromCharIndex( this.SelectionStart );
		EndLine = this.GetLineFromCharIndex( this.SelectionStart + this.SelectionLength );
	}
	
	//选中某一行
	void SelectLine( int Line )
	{
		int i = this.GetFirstCharIndexFromLine( Line );
		this.SelectionStart = i;
		this.SelectionLength = this.Lines[ Line ].Length;
	}
	
	//选中某一行背景
	public void SelectBackLine( int line )
	{
		this.SelectLine( BackColorLine );
		this.SelectionBackColor = SystemData.TextBackColor;
		this.SelectionLength = 0;
		this.SelectionStart = this.Text.Length;
		this.SelectLine( line );
		this.SelectionBackColor = Color.Orange;
		this.SelectionLength = 0;
		this.SelectionStart += this.Lines[ line ].Length;
		this.BackColorExist = true;
		BackColorLine = line;
	}
	
	//用DLL函数
	[DllImport("user32")]
	static extern int SendMessage(HWND hwnd, int wMsg, int wParam, IntPtr lParam);
	const int WM_SETREDRAW = 0x000B;
	
	
	[DllImport("user32", CharSet = CharSet.Auto)]
    static extern IntPtr SendMessage1(HandleRef hWnd, int msg, int wParam, ref PARAFORMAT2 lParam);
	
    public bool DrawPythonCode;
	
	//设置行间距
	public const int WM_USER = 0x0400;
	public const int EM_GETPARAFORMAT = WM_USER + 61;
	public const int EM_SETPARAFORMAT = WM_USER + 71;
	public const long MAX_TAB_STOPS = 32;
	public const uint PFM_LINESPACING = 0x00000100;
	[StructLayout(LayoutKind.Sequential)]
	private struct PARAFORMAT2
	{
		public int cbSize;
		public uint dwMask;
		public short wNumbering;
		public short wReserved;
		public int dxStartIndent;
		public int dxRightIndent;
		public int dxOffset;
		public short wAlignment;
		public short cTabCount;
		[MarshalAs(UnmanagedType.ByValArray, SizeConst = 32)]
		public int[] rgxTabs;
		public int dySpaceBefore;
		public int dySpaceAfter;
		public int dyLineSpacing;
		public short sStyle;
		public byte bLineSpacingRule;
		public byte bOutlineLevel;
		public short wShadingWeight;
		public short wShadingStyle;
		public short wNumberingStart;
		public short wNumberingStyle;
		public short wNumberingTab;
		public short wBorderSpace;
		public short wBorderWidth;
		public short wBorders;
	}
	
	public void SetLineSpace(int dyLineSpacing)
	{
		PARAFORMAT2 fmt = new PARAFORMAT2();
		fmt.cbSize = Marshal.SizeOf(fmt);
		fmt.bLineSpacingRule = 4;// bLineSpacingRule;
		fmt.dyLineSpacing = dyLineSpacing;
		fmt.dwMask = PFM_LINESPACING;
		try
		{
			SendMessage1(new HandleRef(this, this.Handle), EM_SETPARAFORMAT, 0, ref fmt);
		}
		catch
		{
			
		}
	}
	
	
	EditRecorder Recorder;	//键盘事件记录器
	
	public string LastText;		//上一次的文本
	public bool isChanged;	//是否修改
	bool isUndoOrRedo;		//是否在撤销和重做中
	int StartIndex;			//选中的行起点
	int FIndex;				//选中的点
	bool isAllLine;			//选中整行
	int AllLineTick;		//整行选中
	const int FirstClickNumber = 5;
	int FirstClick;			//第一次点击
	bool FocusFirstClick;	//第一次点击
	public bool isNew;		//新建文件
	public bool NotRefresh;	//禁止刷新
	public bool isSystemFile;  //是否系统文件
	int LastSelectionStart; //用于记录撤销操作的起点,不重要
	bool BackColorExist;   //是否有背景颜色显示,不重要
	int BackColorLine;    //背景颜色的所在行,不重要
}
}

