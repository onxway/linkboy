﻿
namespace n_Drawer
{
using System;
using System.Drawing;
using System.Windows.Forms;

using n_Accidence;
using n_MainSystemData;
using n_WordList;
using n_WordNode;

using n_ctext;

//文本着色类
public static class Drawer
{
	public static Font FE;
	public static Font FE_B;
	public static Font FE_B1;
	
	//初始化
	public static void Init()
	{
		ColorValueList = new Color[ 7 ];
		RefreshColor();
		
		try {
			FE = new Font( "Courier New", 12 );
			FE = new Font( "Consolas", 12 );
		}
		catch {
			
		}
		try {
			FE_B = new Font( "Courier New", 12, FontStyle.Bold );
			FE_B = new Font( "Consolas", 12, FontStyle.Bold );
		}
		catch {
			
		}
		try {
			FE_B1 = new Font( "Courier New", 13, FontStyle.Bold );
			FE_B1 = new Font( "Consolas", 13, FontStyle.Bold );
		}
		catch {
			
		}
	}
	
	//区域着色
	public static void DrawArea( CTextBox ctext, int StartIndex, int EndIndex )
	{
		int Number = EndIndex - StartIndex + 1;
		if( Number == 0 ) {
			return;
		}
		
		ctext.Select( StartIndex, Number );
		ctext.SelectionColor = SystemData.NoteColor;
		ctext.SelectionFont = FE;
		
		//解析对应的文本
		Accidence.CutSource( ctext.Text.Substring( StartIndex, Number ) );
		WordNode[] Word = WordList.GetList();
		int Length = WordList.GetLength();
		
		//逐个词进行着色
		for( int index = 0; index < Length; ++index ) {
			WordNode wnode = Word[ index ];
			int Colunn = wnode.Column;
			int Line = wnode.Line;
			int WordIndex = wnode.Index;
			int size = wnode.WordLength;
			int TypeIndex = wnode.TypeIndex;
			
			ctext.SelectionStart = StartIndex + WordIndex;
			ctext.SelectionLength = size;
			
			//根据词的属性进行着色
			switch( TypeIndex ) {
				case WordNode.OPER:		ctext.SelectionColor = SystemData.OperColor; break;
				case WordNode.KEYWORD:	ctext.SelectionColor = ColorValueList[ wnode.ColorIndex ]; break;
				case WordNode.IDENT:	ctext.SelectionColor = SystemData.IdentColor; break;
				case WordNode.NUMBER:	ctext.SelectionColor = SystemData.NumberColor; break;
				case WordNode.SPLIT:	ctext.SelectionColor = SystemData.SplitColor; break;
				case WordNode.CHAR:		ctext.SelectionColor = SystemData.CharColor; break;
				case WordNode.STRING:	ctext.SelectionColor = SystemData.StringColor; break;
				case WordNode.FRONT:	ctext.SelectionColor = SystemData.SysInsColor; break;
				case WordNode.SYS:		ctext.SelectionColor = SystemData.SysInsColor; break;
				case WordNode.END:		break;
				default:				MessageBox.Show( "未知的着色项: " + TypeIndex ); break;
			}
		}
		ctext.SelectionLength = 0;
		
		/*
		for( int i = StartIndex; i <= EndIndex; ++i ) {
			char c = ctext.Text[i];
			if( c > 128 ) {
				ctext.Select( i, 1 );
				ctext.SelectionFont = FZ;
			}
		}
		ctext.SelectionLength = 0;
		*/
	}
	
	//python代码区域着色
	public static void PyDrawArea( CTextBox ctext, int StartIndex, int EndIndex )
	{
		int Number = EndIndex - StartIndex + 1;
		if( Number == 0 ) {
			return;
		}
		ctext.Select( StartIndex, Number );
		ctext.SelectionColor = SystemData.NoteColor;
		ctext.SelectionFont = FE;
		
		//解析对应的文本
		n_PyAccidence.Accidence.DrawCutSource( ctext.Text.Substring( StartIndex, Number ) );
		n_PyWordNode.WordNode[] Word = n_PyWordList.WordList.GetList();
		int Length = n_PyWordList.WordList.GetLength();
		
		//逐个词进行着色
		for( int index = 0; index < Length; ++index ) {
			n_PyWordNode.WordNode wnode = Word[ index ];
			int Colunn = wnode.Column;
			int Line = wnode.Line;
			int WordIndex = wnode.Index;
			int size = wnode.WordLength;
			int TypeIndex = wnode.TypeIndex;
			
			ctext.SelectionStart = StartIndex + WordIndex;
			ctext.SelectionLength = size;
			
			//根据词的属性进行着色
			switch( TypeIndex ) {
				case n_PyWordNode.WordNode.OPER:		ctext.SelectionColor = SystemData.OperColor; break;
				case n_PyWordNode.WordNode.KEYWORD:	ctext.SelectionColor = ColorValueList[ wnode.ColorIndex ]; break;
				case n_PyWordNode.WordNode.IDENT:	ctext.SelectionColor = SystemData.IdentColor; break;
				case n_PyWordNode.WordNode.NUMBER:	ctext.SelectionColor = SystemData.NumberColor; break;
				case n_PyWordNode.WordNode.SPLIT:	ctext.SelectionColor = SystemData.SplitColor; break;
				case n_PyWordNode.WordNode.CHAR:		ctext.SelectionColor = SystemData.CharColor; break;
				case n_PyWordNode.WordNode.STRING:	ctext.SelectionColor = SystemData.StringColor; break;
				case n_PyWordNode.WordNode.FRONT:	ctext.SelectionColor = SystemData.SysInsColor; break;
				case n_PyWordNode.WordNode.SYS:		ctext.SelectionColor = SystemData.SysInsColor; break;
				case n_PyWordNode.WordNode.END:		break;
				default:				MessageBox.Show( "未知的着色项: " + TypeIndex ); break;
			}
		}
		ctext.SelectionLength = 0;
		
		/*
		for( int i = StartIndex; i <= EndIndex; ++i ) {
			char c = ctext.Text[i];
			if( c > 128 ) {
				ctext.Select( i, 1 );
				ctext.SelectionFont = FZ;
			}
		}
		ctext.SelectionLength = 0;
		*/
	}
	
	//刷新着色颜色
	public static void RefreshColor()
	{
		ColorValueList[ 0 ] = SystemData.MemberValueColor;
		ColorValueList[ 1 ] = SystemData.FunctionTypeColor;
		ColorValueList[ 2 ] = SystemData.InsetConstColor;
		ColorValueList[ 3 ] = SystemData.MemberTypeColor;
		ColorValueList[ 4 ] = SystemData.VarTypeColor;
		ColorValueList[ 5 ] = SystemData.FlowColor;
		ColorValueList[ 6 ] = SystemData.OtherColor;
	}
	
	static Color[] ColorValueList;	//颜色列表
}
}


