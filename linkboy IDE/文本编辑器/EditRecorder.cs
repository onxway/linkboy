﻿
//键盘事件记录器

using System;
using System.Windows.Forms;
using n_TextLinker;
using n_ctext;

//键盘事件记录类
public class EditRecorder
{
	//构造函数
	public EditRecorder( CTextBox Ctext )
	{
		ctext = Ctext;
		EditLinker = new Linker();
	}
	
	//添加字符串操作项
	public void AddOper( int SelectionStart, int Index, string OldString, string NewString )
	{
		EditLinker.Add( SelectionStart, Index, OldString, NewString );
	}
	
	//撤销操作
	public void Undo()
	{
		//读操作
		Note CurrentNote = EditLinker.GetCurrentNote();
		if( !EditLinker.MovetoHead() ) {
			A.CodeErrBox.Run( "已经无法再撤销啦!" );
			return;
		}
		ctext.SelectionStart = CurrentNote.index;
		ctext.SelectionLength = CurrentNote.Newstring.Length;
		ctext.SelectedText = CurrentNote.Oldstring;
		//ctext.SelectionStart = CurrentNote.SelectionStart;
		ctext.Refresh();
	}
	
	//重复操作
	public void Redo()
	{
		if( !EditLinker.MovetoEnd() ) {
			A.CodeErrBox.Run( "已经无法再重做啦!" );
			return;
		}
		//读操作
		Note CurrentNote = EditLinker.GetCurrentNote();
		ctext.SelectionStart = CurrentNote.index;
		ctext.SelectionLength = CurrentNote.Oldstring.Length;
		ctext.SelectedText = CurrentNote.Newstring;
		//ctext.SelectionStart = CurrentNote.SelectionStart + CurrentNote.Newstring.Length;
		ctext.Refresh();
	}
	
	private CTextBox ctext;	//程序文本
	private Linker EditLinker;	//链表
}


