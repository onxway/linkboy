﻿
namespace n_MessageForm
{
using System;
using System.Windows.Forms;
using n_ErrorListBox;
using n_ET;
using n_MainSystemData;
using n_OutputBox;
using n_OS;

/// <summary>
/// Description of WorkForm.
/// </summary>
public partial class MessageForm : Form
{
	//主窗口
	public MessageForm()
	{
		InitializeComponent();
		
		this.tabControl2.Controls.Add( OutputBox.Init( this.tabControl2 ) );
		this.tabControl2.Controls.Add( ErrorListBox.Init( this.tabControl2 ) );
	}
	
	//运行显示
	public void Run()
	{
		ErrorListBox.Clear();
		if( ET.isErrors() ) {
			//ErrorListBox.ShowError( ET.Show().Split( '\n' ) );
		}
		this.Visible = true;
	}
	
	//窗体关闭事件
	void MesFormFormClosing(object sender, FormClosingEventArgs e)
	{
		this.Visible = false;
		e.Cancel = true;
	}
	
	//窗体移动事件
	void MesFormMove(object sender, EventArgs e)
	{
		SystemData.FormLocationList[ SystemData.ErrorBoxIndex ] = this.Location;
		SystemData.isChanged = true;
	}
	
	//窗体加载事件
	void MesFormLoad(object sender, EventArgs e)
	{
		this.Size = SystemData.FormSizeList[ SystemData.ErrorBoxIndex ];
		this.Location = SystemData.FormLocationList[ SystemData.ErrorBoxIndex ];
	}
	
	//窗体尺寸改变事件
	void MesFormResize(object sender, EventArgs e)
	{
		SystemData.FormSizeList[ SystemData.ErrorBoxIndex ] = this.Size;
		SystemData.isChanged = true;
	}
}
}



