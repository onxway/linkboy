﻿/*
 * 由 SharpDevelop 创建。
 * 用户: bbb_2
 * 日期: 2009-11-17
 * 时间: 11:15
 * 
 * 如果你想要更改该模板，那么请使用“工具 | 选项 | 编辑 | 编辑标准标题”。
 */
namespace n_MessageForm
{
	partial class MessageForm
	{
		/// <summary>
		/// Designer variable used to keep track of non-visual components.
		/// </summary>
		private System.ComponentModel.IContainer components = null;
		
		/// <summary>
		/// Disposes resources used by the form.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing) {
				if (components != null) {
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}
		
		/// <summary>
		/// This method is required for Windows Forms designer support.
		/// Do not change the method contents inside the source code editor. The Forms designer might
		/// not be able to load this method if it was changed manually.
		/// </summary>
		private void InitializeComponent()
		{
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MessageForm));
			this.tabControl2 = new System.Windows.Forms.TabControl();
			this.SuspendLayout();
			// 
			// tabControl2
			// 
			this.tabControl2.AccessibleDescription = null;
			this.tabControl2.AccessibleName = null;
			this.tabControl2.Alignment = ((System.Windows.Forms.TabAlignment)(resources.GetObject("tabControl2.Alignment")));
			this.tabControl2.Anchor = ((System.Windows.Forms.AnchorStyles)(resources.GetObject("tabControl2.Anchor")));
			this.tabControl2.Appearance = ((System.Windows.Forms.TabAppearance)(resources.GetObject("tabControl2.Appearance")));
			this.tabControl2.BackgroundImage = null;
			this.tabControl2.BackgroundImageLayout = ((System.Windows.Forms.ImageLayout)(resources.GetObject("tabControl2.BackgroundImageLayout")));
			this.tabControl2.Dock = ((System.Windows.Forms.DockStyle)(resources.GetObject("tabControl2.Dock")));
			this.tabControl2.Font = ((System.Drawing.Font)(resources.GetObject("tabControl2.Font")));
			this.tabControl2.ImeMode = ((System.Windows.Forms.ImeMode)(resources.GetObject("tabControl2.ImeMode")));
			this.tabControl2.Location = ((System.Drawing.Point)(resources.GetObject("tabControl2.Location")));
			this.tabControl2.Multiline = true;
			this.tabControl2.Name = "tabControl2";
			this.tabControl2.RightToLeft = ((System.Windows.Forms.RightToLeft)(resources.GetObject("tabControl2.RightToLeft")));
			this.tabControl2.RightToLeftLayout = ((bool)(resources.GetObject("tabControl2.RightToLeftLayout")));
			this.tabControl2.SelectedIndex = 0;
			this.tabControl2.ShowToolTips = ((bool)(resources.GetObject("tabControl2.ShowToolTips")));
			this.tabControl2.Size = ((System.Drawing.Size)(resources.GetObject("tabControl2.Size")));
			this.tabControl2.TabIndex = ((int)(resources.GetObject("tabControl2.TabIndex")));
			// 
			// MessageForm
			// 
			this.AccessibleDescription = null;
			this.AccessibleName = null;
			this.AutoScaleDimensions = ((System.Drawing.SizeF)(resources.GetObject("$this.AutoScaleDimensions")));
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.AutoScroll = ((bool)(resources.GetObject("$this.AutoScroll")));
			this.AutoSize = ((bool)(resources.GetObject("$this.AutoSize")));
			this.AutoSizeMode = ((System.Windows.Forms.AutoSizeMode)(resources.GetObject("$this.AutoSizeMode")));
			this.BackColor = System.Drawing.Color.White;
			this.BackgroundImage = null;
			this.BackgroundImageLayout = ((System.Windows.Forms.ImageLayout)(resources.GetObject("$this.BackgroundImageLayout")));
			this.ClientSize = ((System.Drawing.Size)(resources.GetObject("$this.ClientSize")));
			this.Controls.Add(this.tabControl2);
			this.Font = null;
			this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.SizableToolWindow;
			this.Icon = null;
			this.ImeMode = ((System.Windows.Forms.ImeMode)(resources.GetObject("$this.ImeMode")));
			this.Location = ((System.Drawing.Point)(resources.GetObject("$this.Location")));
			this.MaximumSize = ((System.Drawing.Size)(resources.GetObject("$this.MaximumSize")));
			this.Name = "MessageForm";
			this.RightToLeft = ((System.Windows.Forms.RightToLeft)(resources.GetObject("$this.RightToLeft")));
			this.RightToLeftLayout = ((bool)(resources.GetObject("$this.RightToLeftLayout")));
			this.ShowInTaskbar = false;
			this.StartPosition = ((System.Windows.Forms.FormStartPosition)(resources.GetObject("$this.StartPosition")));
			this.Text = resources.GetString("$this.Text");
			this.TopMost = true;
			this.Resize += new System.EventHandler(this.MesFormResize);
			this.Move += new System.EventHandler(this.MesFormMove);
			this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.MesFormFormClosing);
			this.Load += new System.EventHandler(this.MesFormLoad);
			this.ResumeLayout(false);
		}
		private System.Windows.Forms.TabControl tabControl2;
	}
}
