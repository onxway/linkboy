﻿
//显示信息类
namespace n_OutputBox
{
using System;
using System.Drawing;
using System.Runtime.InteropServices;
using System.Windows.Forms;
using n_MainSystemData;
using n_Compiler;

using HWND = System.IntPtr;

public static class OutputBox
{
	//编译器软件启动初始化
	public static TabPage Init( TabControl Tab )
	{
		tab = Tab;
		OText = new RichTextBox();
		OText.Dock = DockStyle.Fill;
		OText.Visible = true;
		OText.ReadOnly = true;
		OText.BackColor = Color.White;
		OText.Font = new Font( "微软雅黑", 9, FontStyle.Regular );
		
		OText.BorderStyle = BorderStyle.None;
		
		Compiler.CompilingStep += AddMessage;
		Compiler.CheckStep += AddCheckMessage;
		
		panel = new TabPage();
		panel.Text = "输出信息";
		panel.Controls.Add( OText );
		panel.BorderStyle = BorderStyle.None;
		return panel;
	}
	
	//清除显示信息
	public static void Clear()
	{
		OText.Text = "";
		//tab.SelectTab( panel );
		//tab.Refresh();
	}
	
	//设置显示信息
	public static void AddMessage( int Step )
	{
		string Message = Compiler.GetDescribe( Step );
		//SendMessage( OText.Handle, WM_SETREDRAW, 0, IntPtr.Zero );
		
		if( Message != "" ) {
			if( OText.Text == "" ) {
				OText.Text += Message;
			}
			else {
				OText.Text += "\n" + Message;
			}
		}
		
		
		
		//OText.SelectAll();
		//OText.SelectionFont = OText.Font;
		//OText.SelectionStart = OText.Text.Length;
		//OText.SelectionLength = 0;
		//SendMessage( OText.Handle, WM_SETREDRAW, 1, IntPtr.Zero );
		OText.Refresh();
	}
	
	//设置显示信息
	public static void AddCheckMessage( int Step )
	{
		string Message = Compiler.GetCheckDescribe( Step );
		OText.Text += Message + "\n";
		OText.Refresh();
	}
	
	static TabPage panel;			//窗体
	public static RichTextBox OText;		//输出框
	static TabControl tab;
	
	//用DLL函数控制文本的刷新
	[DllImport("user32")]
	static extern int SendMessage(HWND hwnd, int wMsg, int wParam, IntPtr lParam);
	const int WM_SETREDRAW = 0x0B;
}
}
