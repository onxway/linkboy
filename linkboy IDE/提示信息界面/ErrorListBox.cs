﻿
//命名空间
namespace n_ErrorListBox
{
using System;
using System.Drawing;
using System.Windows.Forms;

using T;

//错误列表类
public static class ErrorListBox
{
	//static Bitmap ErrIcon;
	
	//编译器软件启动初始化
	public static TabPage Init( TabControl Tab )
	{
		tab = Tab;
		ErrorList = new ListBox();
		ErrorList.DrawMode = DrawMode.OwnerDrawFixed;
		ErrorList.DrawItem += new DrawItemEventHandler( ErrorListDrawItem );
		ErrorList.Dock = DockStyle.Fill;
		ErrorList.Visible = true;
		ErrorList.Font = new Font( "微软雅黑", 11, FontStyle.Regular );
		ErrorList.SelectionMode = SelectionMode.One;
		ErrorList.BorderStyle = BorderStyle.None;
		
		ErrorList.SelectedIndexChanged += new EventHandler( ErrorClick );
		ErrorList.MouseEnter += new EventHandler( UserMouseEnter );
		
		//ErrIcon = new Bitmap( n_OS.OS.SystemRoot + @"Resource\gui\GCode\Err.png" );
		
		panel = new TabPage();
		panel.Text = "错误列表";
		panel.Controls.Add( ErrorList );
		panel.BackColor = ErrorList.BackColor;
		panel.BorderStyle = BorderStyle.None;
		return panel;
	}
	
	//清除错误信息
	public static void Clear()
	{
		ErrorList.Items.Clear();
	}
	
	//显示错误信息
	public static void ShowError( string[] Items )
	{
		ErrorList.Items.AddRange( Items );
		tab.SelectTab( panel );
		ErrorList.Focus();
	}
	
	//错误点击事件
	static void ErrorClick( object sender, EventArgs e )
	{
		if( ErrorList.SelectedItem == null ) {
			return;
		}
		string Get = ErrorList.SelectedItem.ToString();
		Get = Get.Remove( Get.IndexOf( "," ) );
		string[] s = Get.Split( ' ' );
		int FileIndex = int.Parse( s[ 1 ] );
		
		//忽略文件
		//string FilePath = UseFileList.GetPath( FileIndex );
		//A.AddNewFile( FilePath );
		
		try {
		if( s[ 0 ] == "词" ) {
			int Line = int.Parse( s[ 2 ] ) - 1;
			
			//Line -= n_GUIcoder.GUIcoder.CodeSysLine;
			
			int StartIndex = int.Parse( s[ 3 ] );
			int Length = int.Parse( s[ 4 ] );
			T.ctext.SelectionStart = T.ctext.GetFirstCharIndexFromLine( Line ) + StartIndex;
			T.ctext.SelectionLength = Length;
		}
		if( s[ 0 ] == "行" ) {
			int Line = int.Parse( s[ 2 ] ) - 1;
			
			//Line -= n_GUIcoder.GUIcoder.CodeSysLine;
			
			T.ctext.SelectionStart = T.ctext.GetFirstCharIndexFromLine( Line );
			T.ctext.SelectionLength = T.ctext.Lines[ Line ].Length;
		}
		}catch(Exception ee) {
			//A.CodeErrBox.Run( "目标文件已关闭或者错误点定位失败:\n" + ee );
		}
		T.ctext.Focus();
		//T.ErrorBox.Visible = false;
	}
	
	//错误列表重绘
	static void ErrorListDrawItem(object sender, DrawItemEventArgs e)
		{
			if( ErrorList.Items.Count == 0 ) {
				return;
			}
			e.DrawBackground();
			ErrorList.ItemHeight = 20;
			
//			LinearGradientBrush myBrush = null;	
//			//为每个项设置背景颜色
//			if( e.Index % 2 == 0 ) {
//				myBrush = new LinearGradientBrush(
//					e.Bounds, Color.FromArgb(223, 221, 255), SystemData.MessageBackColor, 0.0 );
//			}
//			else {
//				myBrush = new LinearGradientBrush(
//					e.Bounds, Color.FromArgb(219, 244, 220), SystemData.MessageBackColor, 0.0 );
//			}
			SolidBrush myBrush = new SolidBrush( Color.White );
			
			e.Graphics.FillRectangle( myBrush, e.Bounds );
			myBrush.Dispose();
			
			
			e.Graphics.DrawString( "✖", e.Font, Brushes.OrangeRed,
		                      e.Bounds.Location.X,
		                      e.Bounds.Location.Y + 2 );
			
			float Offset = e.Graphics.MeasureString( "✖ ", ErrorList.Font ).Width;
			
			//获取要显示的信息
			string Errors = ErrorList.Items[e.Index].ToString();
			int Index = Errors.IndexOf( ',' );
			if( Index != -1 ) {
				Errors = Errors.Remove( 0, Index + 1 );
			}
			else {
				Errors = "<-1> " + Errors;
			}
			
			e.Graphics.DrawString( Errors, e.Font, Brushes.SlateGray,
		                      e.Bounds.Location.X + Offset,
		                      e.Bounds.Location.Y + 2 );
			
			e.DrawFocusRectangle();
		}
	
	static void UserMouseEnter( object sender, EventArgs e )
	{
		ErrorList.Focus();
		ErrorList.Select();
	}
	
	static TabPage panel;		//窗体
	static ListBox ErrorList;		//错误列表框
	static TabControl tab;
}
}
