﻿/*
 * 由SharpDevelop创建。
 * 用户： cap_gpu
 * 日期: 2017/4/9
 * 时间: 8:03
 * 
 * 要改变这种模板请点击 工具|选项|代码编写|编辑标准头文件
 */
namespace n_PyLangForm
{
	partial class PyLangForm
	{
		/// <summary>
		/// Designer variable used to keep track of non-visual components.
		/// </summary>
		private System.ComponentModel.IContainer components = null;
		
		/// <summary>
		/// Disposes resources used by the form.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing) {
				if (components != null) {
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}
		
		/// <summary>
		/// This method is required for Windows Forms designer support.
		/// Do not change the method contents inside the source code editor. The Forms designer might
		/// not be able to load this method if it was changed manually.
		/// </summary>
		private void InitializeComponent()
		{
			System.Windows.Forms.TreeNode treeNode1 = new System.Windows.Forms.TreeNode("1 python3 基础语法");
			this.treeView1 = new System.Windows.Forms.TreeView();
			this.splitContainer1 = new System.Windows.Forms.SplitContainer();
			this.panel1 = new System.Windows.Forms.Panel();
			this.linkLabel1 = new System.Windows.Forms.LinkLabel();
			this.linkLabel14 = new System.Windows.Forms.LinkLabel();
			this.linkLabel2 = new System.Windows.Forms.LinkLabel();
			this.linkLabel15 = new System.Windows.Forms.LinkLabel();
			this.linkLabel4 = new System.Windows.Forms.LinkLabel();
			this.linkLabel13 = new System.Windows.Forms.LinkLabel();
			this.linkLabel3 = new System.Windows.Forms.LinkLabel();
			this.linkLabel9 = new System.Windows.Forms.LinkLabel();
			this.linkLabel8 = new System.Windows.Forms.LinkLabel();
			this.linkLabel10 = new System.Windows.Forms.LinkLabel();
			this.linkLabel7 = new System.Windows.Forms.LinkLabel();
			this.linkLabel11 = new System.Windows.Forms.LinkLabel();
			this.linkLabel6 = new System.Windows.Forms.LinkLabel();
			this.linkLabel12 = new System.Windows.Forms.LinkLabel();
			this.linkLabel5 = new System.Windows.Forms.LinkLabel();
			this.richTextBox1 = new System.Windows.Forms.RichTextBox();
			this.label1 = new System.Windows.Forms.Label();
			//((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).BeginInit();
			this.splitContainer1.Panel1.SuspendLayout();
			this.splitContainer1.Panel2.SuspendLayout();
			this.splitContainer1.SuspendLayout();
			this.panel1.SuspendLayout();
			this.SuspendLayout();
			// 
			// treeView1
			// 
			this.treeView1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(237)))), ((int)(((byte)(225)))));
			this.treeView1.Dock = System.Windows.Forms.DockStyle.Fill;
			this.treeView1.Font = new System.Drawing.Font("微软雅黑", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
			this.treeView1.Location = new System.Drawing.Point(0, 0);
			this.treeView1.Name = "treeView1";
			treeNode1.Name = "basic";
			treeNode1.Text = "1 python3 基础语法";
			this.treeView1.Nodes.AddRange(new System.Windows.Forms.TreeNode[] {
									treeNode1});
			this.treeView1.Size = new System.Drawing.Size(233, 638);
			this.treeView1.TabIndex = 0;
			this.treeView1.AfterSelect += new System.Windows.Forms.TreeViewEventHandler(this.TreeView1AfterSelect);
			// 
			// splitContainer1
			// 
			this.splitContainer1.BackColor = System.Drawing.Color.DarkSeaGreen;
			this.splitContainer1.Dock = System.Windows.Forms.DockStyle.Fill;
			this.splitContainer1.Location = new System.Drawing.Point(0, 86);
			this.splitContainer1.Name = "splitContainer1";
			// 
			// splitContainer1.Panel1
			// 
			this.splitContainer1.Panel1.Controls.Add(this.treeView1);
			// 
			// splitContainer1.Panel2
			// 
			this.splitContainer1.Panel2.BackColor = System.Drawing.Color.WhiteSmoke;
			this.splitContainer1.Panel2.Controls.Add(this.panel1);
			this.splitContainer1.Panel2.Controls.Add(this.richTextBox1);
			this.splitContainer1.Size = new System.Drawing.Size(1009, 638);
			this.splitContainer1.SplitterDistance = 233;
			this.splitContainer1.SplitterWidth = 10;
			this.splitContainer1.TabIndex = 1;
			// 
			// panel1
			// 
			this.panel1.Controls.Add(this.linkLabel1);
			this.panel1.Controls.Add(this.linkLabel14);
			this.panel1.Controls.Add(this.linkLabel2);
			this.panel1.Controls.Add(this.linkLabel15);
			this.panel1.Controls.Add(this.linkLabel4);
			this.panel1.Controls.Add(this.linkLabel13);
			this.panel1.Controls.Add(this.linkLabel3);
			this.panel1.Controls.Add(this.linkLabel9);
			this.panel1.Controls.Add(this.linkLabel8);
			this.panel1.Controls.Add(this.linkLabel10);
			this.panel1.Controls.Add(this.linkLabel7);
			this.panel1.Controls.Add(this.linkLabel11);
			this.panel1.Controls.Add(this.linkLabel6);
			this.panel1.Controls.Add(this.linkLabel12);
			this.panel1.Controls.Add(this.linkLabel5);
			this.panel1.Location = new System.Drawing.Point(3, 3);
			this.panel1.Name = "panel1";
			this.panel1.Size = new System.Drawing.Size(709, 441);
			this.panel1.TabIndex = 16;
			// 
			// linkLabel1
			// 
			this.linkLabel1.ActiveLinkColor = System.Drawing.Color.DarkGoldenrod;
			this.linkLabel1.Font = new System.Drawing.Font("微软雅黑", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
			this.linkLabel1.LinkColor = System.Drawing.Color.Olive;
			this.linkLabel1.Location = new System.Drawing.Point(11, 12);
			this.linkLabel1.Name = "linkLabel1";
			this.linkLabel1.Size = new System.Drawing.Size(709, 23);
			this.linkLabel1.TabIndex = 1;
			this.linkLabel1.TabStop = true;
			this.linkLabel1.Text = "基础语法：https://www.runoob.com/python3/python3-basic-syntax.html";
			this.linkLabel1.VisitedLinkColor = System.Drawing.Color.Olive;
			this.linkLabel1.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.LinkLabelLinkClicked);
			// 
			// linkLabel14
			// 
			this.linkLabel14.ActiveLinkColor = System.Drawing.Color.DarkGoldenrod;
			this.linkLabel14.Font = new System.Drawing.Font("微软雅黑", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
			this.linkLabel14.LinkColor = System.Drawing.Color.Olive;
			this.linkLabel14.Location = new System.Drawing.Point(11, 390);
			this.linkLabel14.Name = "linkLabel14";
			this.linkLabel14.Size = new System.Drawing.Size(709, 23);
			this.linkLabel14.TabIndex = 15;
			this.linkLabel14.TabStop = true;
			this.linkLabel14.Text = "面向对象：https://www.runoob.com/python3/python3-class.html";
			this.linkLabel14.VisitedLinkColor = System.Drawing.Color.Olive;
			this.linkLabel14.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.LinkLabelLinkClicked);
			// 
			// linkLabel2
			// 
			this.linkLabel2.ActiveLinkColor = System.Drawing.Color.DarkGoldenrod;
			this.linkLabel2.Font = new System.Drawing.Font("微软雅黑", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
			this.linkLabel2.LinkColor = System.Drawing.Color.Olive;
			this.linkLabel2.Location = new System.Drawing.Point(11, 39);
			this.linkLabel2.Name = "linkLabel2";
			this.linkLabel2.Size = new System.Drawing.Size(709, 23);
			this.linkLabel2.TabIndex = 2;
			this.linkLabel2.TabStop = true;
			this.linkLabel2.Text = "基本数据类型：https://www.runoob.com/python3/python3-data-type.html";
			this.linkLabel2.VisitedLinkColor = System.Drawing.Color.Olive;
			this.linkLabel2.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.LinkLabelLinkClicked);
			// 
			// linkLabel15
			// 
			this.linkLabel15.ActiveLinkColor = System.Drawing.Color.DarkGoldenrod;
			this.linkLabel15.Font = new System.Drawing.Font("微软雅黑", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
			this.linkLabel15.LinkColor = System.Drawing.Color.Olive;
			this.linkLabel15.Location = new System.Drawing.Point(11, 363);
			this.linkLabel15.Name = "linkLabel15";
			this.linkLabel15.Size = new System.Drawing.Size(709, 23);
			this.linkLabel15.TabIndex = 14;
			this.linkLabel15.TabStop = true;
			this.linkLabel15.Text = "函数：https://www.runoob.com/python3/python3-function.html";
			this.linkLabel15.VisitedLinkColor = System.Drawing.Color.Olive;
			this.linkLabel15.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.LinkLabelLinkClicked);
			// 
			// linkLabel4
			// 
			this.linkLabel4.ActiveLinkColor = System.Drawing.Color.DarkGoldenrod;
			this.linkLabel4.Font = new System.Drawing.Font("微软雅黑", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
			this.linkLabel4.LinkColor = System.Drawing.Color.Olive;
			this.linkLabel4.Location = new System.Drawing.Point(11, 66);
			this.linkLabel4.Name = "linkLabel4";
			this.linkLabel4.Size = new System.Drawing.Size(709, 23);
			this.linkLabel4.TabIndex = 3;
			this.linkLabel4.TabStop = true;
			this.linkLabel4.Text = "注释：https://www.runoob.com/python3/python3-comment.html";
			this.linkLabel4.VisitedLinkColor = System.Drawing.Color.Olive;
			this.linkLabel4.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.LinkLabelLinkClicked);
			// 
			// linkLabel13
			// 
			this.linkLabel13.ActiveLinkColor = System.Drawing.Color.DarkGoldenrod;
			this.linkLabel13.Font = new System.Drawing.Font("微软雅黑", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
			this.linkLabel13.LinkColor = System.Drawing.Color.Olive;
			this.linkLabel13.Location = new System.Drawing.Point(11, 336);
			this.linkLabel13.Name = "linkLabel13";
			this.linkLabel13.Size = new System.Drawing.Size(709, 23);
			this.linkLabel13.TabIndex = 13;
			this.linkLabel13.TabStop = true;
			this.linkLabel13.Text = "迭代器与生成器：https://www.runoob.com/python3/python3-iterator-generator.html";
			this.linkLabel13.VisitedLinkColor = System.Drawing.Color.Olive;
			this.linkLabel13.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.LinkLabelLinkClicked);
			// 
			// linkLabel3
			// 
			this.linkLabel3.ActiveLinkColor = System.Drawing.Color.DarkGoldenrod;
			this.linkLabel3.Font = new System.Drawing.Font("微软雅黑", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
			this.linkLabel3.LinkColor = System.Drawing.Color.Olive;
			this.linkLabel3.Location = new System.Drawing.Point(11, 93);
			this.linkLabel3.Name = "linkLabel3";
			this.linkLabel3.Size = new System.Drawing.Size(709, 23);
			this.linkLabel3.TabIndex = 4;
			this.linkLabel3.TabStop = true;
			this.linkLabel3.Text = "运算符：https://www.runoob.com/python3/python3-basic-operators.html";
			this.linkLabel3.VisitedLinkColor = System.Drawing.Color.Olive;
			this.linkLabel3.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.LinkLabelLinkClicked);
			// 
			// linkLabel9
			// 
			this.linkLabel9.ActiveLinkColor = System.Drawing.Color.DarkGoldenrod;
			this.linkLabel9.Font = new System.Drawing.Font("微软雅黑", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
			this.linkLabel9.LinkColor = System.Drawing.Color.Olive;
			this.linkLabel9.Location = new System.Drawing.Point(11, 309);
			this.linkLabel9.Name = "linkLabel9";
			this.linkLabel9.Size = new System.Drawing.Size(709, 23);
			this.linkLabel9.TabIndex = 12;
			this.linkLabel9.TabStop = true;
			this.linkLabel9.Text = "循环语句：https://www.runoob.com/python3/python3-loop.html";
			this.linkLabel9.VisitedLinkColor = System.Drawing.Color.Olive;
			this.linkLabel9.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.LinkLabelLinkClicked);
			// 
			// linkLabel8
			// 
			this.linkLabel8.ActiveLinkColor = System.Drawing.Color.DarkGoldenrod;
			this.linkLabel8.Font = new System.Drawing.Font("微软雅黑", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
			this.linkLabel8.LinkColor = System.Drawing.Color.Olive;
			this.linkLabel8.Location = new System.Drawing.Point(11, 120);
			this.linkLabel8.Name = "linkLabel8";
			this.linkLabel8.Size = new System.Drawing.Size(709, 23);
			this.linkLabel8.TabIndex = 5;
			this.linkLabel8.TabStop = true;
			this.linkLabel8.Text = "数字：https://www.runoob.com/python3/python3-number.html";
			this.linkLabel8.VisitedLinkColor = System.Drawing.Color.Olive;
			this.linkLabel8.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.LinkLabelLinkClicked);
			// 
			// linkLabel10
			// 
			this.linkLabel10.ActiveLinkColor = System.Drawing.Color.DarkGoldenrod;
			this.linkLabel10.Font = new System.Drawing.Font("微软雅黑", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
			this.linkLabel10.LinkColor = System.Drawing.Color.Olive;
			this.linkLabel10.Location = new System.Drawing.Point(11, 282);
			this.linkLabel10.Name = "linkLabel10";
			this.linkLabel10.Size = new System.Drawing.Size(709, 23);
			this.linkLabel10.TabIndex = 11;
			this.linkLabel10.TabStop = true;
			this.linkLabel10.Text = "条件控制：https://www.runoob.com/python3/python3-conditional-statements.html";
			this.linkLabel10.VisitedLinkColor = System.Drawing.Color.Olive;
			this.linkLabel10.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.LinkLabelLinkClicked);
			// 
			// linkLabel7
			// 
			this.linkLabel7.ActiveLinkColor = System.Drawing.Color.DarkGoldenrod;
			this.linkLabel7.Font = new System.Drawing.Font("微软雅黑", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
			this.linkLabel7.LinkColor = System.Drawing.Color.Olive;
			this.linkLabel7.Location = new System.Drawing.Point(11, 147);
			this.linkLabel7.Name = "linkLabel7";
			this.linkLabel7.Size = new System.Drawing.Size(709, 23);
			this.linkLabel7.TabIndex = 6;
			this.linkLabel7.TabStop = true;
			this.linkLabel7.Text = "字符串：https://www.runoob.com/python3/python3-string.html";
			this.linkLabel7.VisitedLinkColor = System.Drawing.Color.Olive;
			this.linkLabel7.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.LinkLabelLinkClicked);
			// 
			// linkLabel11
			// 
			this.linkLabel11.ActiveLinkColor = System.Drawing.Color.DarkGoldenrod;
			this.linkLabel11.Font = new System.Drawing.Font("微软雅黑", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
			this.linkLabel11.LinkColor = System.Drawing.Color.Olive;
			this.linkLabel11.Location = new System.Drawing.Point(11, 255);
			this.linkLabel11.Name = "linkLabel11";
			this.linkLabel11.Size = new System.Drawing.Size(709, 23);
			this.linkLabel11.TabIndex = 10;
			this.linkLabel11.TabStop = true;
			this.linkLabel11.Text = "集合：https://www.runoob.com/python3/python3-set.html";
			this.linkLabel11.VisitedLinkColor = System.Drawing.Color.Olive;
			this.linkLabel11.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.LinkLabelLinkClicked);
			// 
			// linkLabel6
			// 
			this.linkLabel6.ActiveLinkColor = System.Drawing.Color.DarkGoldenrod;
			this.linkLabel6.Font = new System.Drawing.Font("微软雅黑", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
			this.linkLabel6.LinkColor = System.Drawing.Color.Olive;
			this.linkLabel6.Location = new System.Drawing.Point(11, 174);
			this.linkLabel6.Name = "linkLabel6";
			this.linkLabel6.Size = new System.Drawing.Size(709, 23);
			this.linkLabel6.TabIndex = 7;
			this.linkLabel6.TabStop = true;
			this.linkLabel6.Text = "列表：https://www.runoob.com/python3/python3-list.html";
			this.linkLabel6.VisitedLinkColor = System.Drawing.Color.Olive;
			this.linkLabel6.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.LinkLabelLinkClicked);
			// 
			// linkLabel12
			// 
			this.linkLabel12.ActiveLinkColor = System.Drawing.Color.DarkGoldenrod;
			this.linkLabel12.Font = new System.Drawing.Font("微软雅黑", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
			this.linkLabel12.LinkColor = System.Drawing.Color.Olive;
			this.linkLabel12.Location = new System.Drawing.Point(11, 228);
			this.linkLabel12.Name = "linkLabel12";
			this.linkLabel12.Size = new System.Drawing.Size(709, 23);
			this.linkLabel12.TabIndex = 9;
			this.linkLabel12.TabStop = true;
			this.linkLabel12.Text = "字典：https://www.runoob.com/python3/python3-dictionary.html";
			this.linkLabel12.VisitedLinkColor = System.Drawing.Color.Olive;
			this.linkLabel12.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.LinkLabelLinkClicked);
			// 
			// linkLabel5
			// 
			this.linkLabel5.ActiveLinkColor = System.Drawing.Color.DarkGoldenrod;
			this.linkLabel5.Font = new System.Drawing.Font("微软雅黑", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
			this.linkLabel5.LinkColor = System.Drawing.Color.Olive;
			this.linkLabel5.Location = new System.Drawing.Point(11, 201);
			this.linkLabel5.Name = "linkLabel5";
			this.linkLabel5.Size = new System.Drawing.Size(709, 23);
			this.linkLabel5.TabIndex = 8;
			this.linkLabel5.TabStop = true;
			this.linkLabel5.Text = "元组：https://www.runoob.com/python3/python3-tuple.html";
			this.linkLabel5.VisitedLinkColor = System.Drawing.Color.Olive;
			this.linkLabel5.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.LinkLabelLinkClicked);
			// 
			// richTextBox1
			// 
			this.richTextBox1.BackColor = System.Drawing.Color.WhiteSmoke;
			this.richTextBox1.BorderStyle = System.Windows.Forms.BorderStyle.None;
			this.richTextBox1.Location = new System.Drawing.Point(3, 3);
			this.richTextBox1.Name = "richTextBox1";
			this.richTextBox1.ReadOnly = true;
			this.richTextBox1.Size = new System.Drawing.Size(22, 23);
			this.richTextBox1.TabIndex = 0;
			this.richTextBox1.Text = "";
			this.richTextBox1.Visible = false;
			// 
			// label1
			// 
			this.label1.BackColor = System.Drawing.Color.DarkSeaGreen;
			this.label1.Dock = System.Windows.Forms.DockStyle.Top;
			this.label1.Font = new System.Drawing.Font("微软雅黑", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
			this.label1.ForeColor = System.Drawing.Color.White;
			this.label1.Location = new System.Drawing.Point(0, 0);
			this.label1.Name = "label1";
			this.label1.Size = new System.Drawing.Size(1009, 86);
			this.label1.TabIndex = 2;
			this.label1.Text = "目前linkboy内置的python为测试版，部分高级python功能暂不支持。linkboy每月定时更新，根据开发规划，8月份的版本更新会增加：\r\n1：字符串对" +
			"象内置函数完善 2：列表内置功能完善 4：实现global关键字 5：实现yield关键字";
			this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// PyLangForm
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.BackColor = System.Drawing.Color.WhiteSmoke;
			this.ClientSize = new System.Drawing.Size(1009, 724);
			this.Controls.Add(this.splitContainer1);
			this.Controls.Add(this.label1);
			this.MaximizeBox = false;
			this.MinimizeBox = false;
			this.Name = "PyLangForm";
			this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
			this.Text = "python语法宝典";
			this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.PyLangFormClosing);
			this.splitContainer1.Panel1.ResumeLayout(false);
			this.splitContainer1.Panel2.ResumeLayout(false);
			//((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).EndInit();
			this.splitContainer1.ResumeLayout(false);
			this.panel1.ResumeLayout(false);
			this.ResumeLayout(false);
		}
		private System.Windows.Forms.Panel panel1;
		private System.Windows.Forms.LinkLabel linkLabel1;
		private System.Windows.Forms.LinkLabel linkLabel2;
		private System.Windows.Forms.LinkLabel linkLabel4;
		private System.Windows.Forms.LinkLabel linkLabel3;
		private System.Windows.Forms.LinkLabel linkLabel8;
		private System.Windows.Forms.LinkLabel linkLabel7;
		private System.Windows.Forms.LinkLabel linkLabel6;
		private System.Windows.Forms.LinkLabel linkLabel5;
		private System.Windows.Forms.LinkLabel linkLabel12;
		private System.Windows.Forms.LinkLabel linkLabel11;
		private System.Windows.Forms.LinkLabel linkLabel10;
		private System.Windows.Forms.LinkLabel linkLabel9;
		private System.Windows.Forms.LinkLabel linkLabel13;
		private System.Windows.Forms.LinkLabel linkLabel15;
		private System.Windows.Forms.LinkLabel linkLabel14;
		private System.Windows.Forms.RichTextBox richTextBox1;
		private System.Windows.Forms.Label label1;
		private System.Windows.Forms.SplitContainer splitContainer1;
		private System.Windows.Forms.TreeView treeView1;
	}
}
