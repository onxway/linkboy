﻿
using System;
using System.Drawing;
using System.Windows.Forms;
using System.IO;

namespace n_PyLangForm
{
/// <summary>
/// Description of LocateForm.
/// </summary>
public partial class PyLangForm : Form
{
	TreeNodeCollection cnode;
	
	public PyLangForm()
	{
		InitializeComponent();
		
		cnode = treeView1.Nodes;
		A_FindFile( n_OS.OS.SystemRoot + "Resource\\pyword\\" );
		
		richTextBox1.Dock = DockStyle.Fill;
	}
	
	//运行
	public void Run()
	{
		richTextBox1.Visible = false;
		panel1.Visible = true;
		this.Visible = true;
	}
	
	//遍历指定的文件夹中的全部文件
	void A_FindFile( string sSourcePath )
	{
		//在指定目录及子目录下查找文件,在list中列出子目录及文件
		DirectoryInfo Dir = new DirectoryInfo( sSourcePath );
		DirectoryInfo[] DirSub = Dir.GetDirectories();
		
		//遍历当前文件夹中的所有文件
		foreach( FileInfo f in Dir.GetFiles("*.*", SearchOption.TopDirectoryOnly) ) {
			
			string FilePath = Dir + f.ToString();
			
			string text = Path.GetFileName( FilePath );
			text = text.Remove( text.LastIndexOf( "." ) );
			TreeNode tn = cnode.Add( text );
			tn.Name = FilePath;
			//tn.ForeColor = Color.Gray;
		}
		//遍历所有的子文件夹
		foreach( DirectoryInfo d in DirSub ) {
			
			if( d.ToString().EndsWith( "_files" ) ) {
				continue;
			}
			
			TreeNodeCollection last = cnode;
			TreeNode tn = cnode.Add( d.ToString() );
			tn.Name = "";
			cnode = tn.Nodes;
			
			A_FindFile( Dir + d.ToString() + @"\" );
			
			cnode = last;
		}
	}
	
	//=================================================================================================
	
	//窗体关闭
	void PyLangFormClosing(object sender, FormClosingEventArgs e)
	{
		this.Visible = false;
		e.Cancel = true;
	}
	
	void TreeView1AfterSelect(object sender, TreeViewEventArgs e)
	{
		string filename = e.Node.Name;
		if( filename == "" ) {
			return;
		}
		if( filename == "basic" ) {
			richTextBox1.Visible = false;
			panel1.Visible = true;
			return;
		}
		panel1.Visible = false;
		richTextBox1.Visible = true;
		richTextBox1.LoadFile( filename );
		richTextBox1.SelectionLength = 0;
		richTextBox1.SelectionStart = 0;
	}
	
	void LinkLabelLinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
	{
		string s = ((LinkLabel)sender).Text;
		s = s.Remove( 0, s.IndexOf( "：" ) + 1 );
		
		System.Diagnostics.Process.Start(s);
	}
}
}

