﻿
using System;
using System.Windows.Forms;
using n_ClearChipForm;
using n_CMusicForm;
using n_CopyRightForm;
using n_CWaveForm;
using n_ErrorForm;
using n_GToolForm;
using n_LocateForm;
using n_MQTTForm;
using n_NetToolForm;
using n_PushForm;
using n_PyLangForm;
using n_DocForm;
using n_UARTForm;
using n_CommVUIForm;
using v_GForm;

namespace U
{
public static class U
{
	public static GForm m;
	
	
	//说明信息界面
	public static CopyRightForm CopyRightBox;
	
	//架构图
	public static n_CMapForm.CMapForm CMapBox;
	
	//工具箱
	public static ToolForm ToolBox;
	
	//系统设置
	public static DocForm DocBox;
	
	
	//串口助手
	public static UARTForm UARTBox;
	
	//串口绘图器
	public static CWaveForm CWaveBox;
	
	//串口迷你乐队
	public static CMusicForm CMusicBox;
	
	//鼠标透传器
	public static CommVUIForm CommVUIBox;
	
	//画板
	public static n_ImageEditForm.Form1 ImageEditBox;
	
	//地理方位计算器
	public static LocateForm LocateBox;
	
	//电阻计算器
	public static n_RegCalForm.Form1 RegCalBox;
	
	//网络调试工具
	public static NetToolForm NetToolBox;
	
	//MQTT调试工具
	public static MQTTForm MQTTBox;
	
	//生活小常识
	public static n_NousForm.NousForm NousBox;
	
	//python语言语法框
	public static PyLangForm PyLangBox;
	
	//软件终止运行错误提示框
	public static ErrorForm ErrorBox;
	
	//--------------------------------
	//以下未验证初始化
	
	//清除芯片
	public static ClearChipForm CPForm;
	
	//--------------------------------
	[System.Runtime.InteropServices.DllImport("user32.dll")]
    private static extern bool SetProcessDPIAware();
    
	[STAThread]
	static void Main(string[] Args)
	{
		//if( System.DateTime.Now.Year >= 2022 ) {
			//MessageBox.Show( "当前版本已无效" );
			//return;
		//}
		
		if (Environment.OSVersion.Version.Major >= 6) {
			//MessageBox.Show( "123" );
			SetProcessDPIAware();
		}
		
		/** 
		 * 当前用户是管理员的时候，直接启动应用程序
		 * 如果不是管理员，则使用启动对象启动程序，以确保使用管理员身份运行
		 */
		//获得当前登录的Windows用户标示
		System.Security.Principal.WindowsIdentity identity = System.Security.Principal.WindowsIdentity.GetCurrent();
		//创建Windows用户主题
		Application.EnableVisualStyles();
		
		System.Security.Principal.WindowsPrincipal principal = new System.Security.Principal.WindowsPrincipal(identity);
		//判断当前登录用户是否为管理员
		if (principal.IsInRole(System.Security.Principal.WindowsBuiltInRole.Administrator)) {
			//如果是管理员，则直接运行
			
			Application.EnableVisualStyles();
			
			try {
				//初始化
				n_OS.OS.isGForm = true;
				
				string dfile = A.Init( Args );
				
				if( dfile == "ERROR" ) {
					return;
				}
				
				PushForm.d_ShowVersion = ShowVersion;
				PushForm.d_ShowCMap = ShowCMap;
				
				//T.T.Init( null );
				
				m = new GForm( dfile );
				
				if( G.AutoRunMode ) {
					m.GFormLoad( null, null );
					G.SimBox.Run();
					Application.Run( G.SimBox );
				}
				else {
					
					A.StartBox.Wait();
					
					//运行工作台
					Application.Run( m );
				}
				A.Close();
			}
			catch( Exception e ) {
				//MessageBox.Show( e.ToString() );
				
				ErrorBox = new ErrorForm( e.ToString() );
				Application.Run( ErrorBox );
			}
		}
		else {
			
			//创建启动对象
			System.Diagnostics.ProcessStartInfo startInfo = new System.Diagnostics.ProcessStartInfo();
			//设置运行文件
			startInfo.FileName = System.Windows.Forms.Application.ExecutablePath;
			//设置启动参数
			startInfo.Arguments = String.Join(" ", Args);
			//设置启动动作,确保以管理员身份运行
			startInfo.Verb = "runas";
			//如果不是管理员，则启动UAC
			System.Diagnostics.Process.Start(startInfo);
			//退出
			System.Windows.Forms.Application.Exit();
		}
	}
	
	//显示版本信息
	static void ShowVersion()
	{
		/*
		if( U.VersionBox == null ) {
			U.VersionBox = new VersionForm();
		}
		U.VersionBox.Run();
		*/
		
		if( U.CopyRightBox == null ) {
			U.CopyRightBox = new n_CopyRightForm.CopyRightForm();
		}
		U.CopyRightBox.Run();
	}
	
	//显示架构图
	static void ShowCMap()
	{
		if( U.CMapBox == null ) {
			U.CMapBox = new n_CMapForm.CMapForm();
		}
		U.CMapBox.Run();
	}
}
}




