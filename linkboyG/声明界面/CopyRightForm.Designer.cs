﻿/*
 * 由 SharpDevelop 创建。
 * 用户: bbb_2
 * 日期: 2009-11-17
 * 时间: 11:15
 * 
 * 如果你想要更改该模板，那么请使用“工具 | 选项 | 编辑 | 编辑标准标题”。
 */
namespace n_CopyRightForm
{
	partial class CopyRightForm
	{
		/// <summary>
		/// Designer variable used to keep track of non-visual components.
		/// </summary>
		private System.ComponentModel.IContainer components = null;
		
		/// <summary>
		/// Disposes resources used by the form.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing) {
				if (components != null) {
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}
		
		/// <summary>
		/// This method is required for Windows Forms designer support.
		/// Do not change the method contents inside the source code editor. The Forms designer might
		/// not be able to load this method if it was changed manually.
		/// </summary>
		private void InitializeComponent()
		{
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(CopyRightForm));
			this.tabControl10 = new System.Windows.Forms.TabControl();
			this.tabPage1 = new System.Windows.Forms.TabPage();
			this.richTextBox09 = new System.Windows.Forms.RichTextBox();
			this.tabPage3 = new System.Windows.Forms.TabPage();
			this.richTextBox10 = new System.Windows.Forms.RichTextBox();
			this.tabPage5 = new System.Windows.Forms.TabPage();
			this.richTextBox11 = new System.Windows.Forms.RichTextBox();
			this.tabPage7 = new System.Windows.Forms.TabPage();
			this.richTextBox12 = new System.Windows.Forms.RichTextBox();
			this.tabPage9 = new System.Windows.Forms.TabPage();
			this.richTextBox13 = new System.Windows.Forms.RichTextBox();
			this.tabPage10 = new System.Windows.Forms.TabPage();
			this.richTextBox14 = new System.Windows.Forms.RichTextBox();
			this.tabPage11 = new System.Windows.Forms.TabPage();
			this.richTextBox15 = new System.Windows.Forms.RichTextBox();
			this.tabPage12 = new System.Windows.Forms.TabPage();
			this.richTextBox16 = new System.Windows.Forms.RichTextBox();
			this.tabPage13 = new System.Windows.Forms.TabPage();
			this.richTextBox17 = new System.Windows.Forms.RichTextBox();
			this.tabPage14 = new System.Windows.Forms.TabPage();
			this.richTextBox18 = new System.Windows.Forms.RichTextBox();
			this.tabPage15 = new System.Windows.Forms.TabPage();
			this.richTextBox19 = new System.Windows.Forms.RichTextBox();
			this.tabPage16 = new System.Windows.Forms.TabPage();
			this.richTextBox20 = new System.Windows.Forms.RichTextBox();
			this.tabPage2 = new System.Windows.Forms.TabPage();
			this.richTextBox21 = new System.Windows.Forms.RichTextBox();
			this.tabPage4 = new System.Windows.Forms.TabPage();
			this.richTextBox22 = new System.Windows.Forms.RichTextBox();
			this.tabPage6 = new System.Windows.Forms.TabPage();
			this.richTextBox23 = new System.Windows.Forms.RichTextBox();
			this.panel1 = new System.Windows.Forms.Panel();
			this.label1 = new System.Windows.Forms.Label();
			this.buttonVOS = new System.Windows.Forms.Button();
			this.checkBox1 = new System.Windows.Forms.CheckBox();
			this.tabControl10.SuspendLayout();
			this.tabPage1.SuspendLayout();
			this.tabPage3.SuspendLayout();
			this.tabPage5.SuspendLayout();
			this.tabPage7.SuspendLayout();
			this.tabPage9.SuspendLayout();
			this.tabPage10.SuspendLayout();
			this.tabPage11.SuspendLayout();
			this.tabPage12.SuspendLayout();
			this.tabPage13.SuspendLayout();
			this.tabPage14.SuspendLayout();
			this.tabPage15.SuspendLayout();
			this.tabPage16.SuspendLayout();
			this.tabPage2.SuspendLayout();
			this.tabPage4.SuspendLayout();
			this.tabPage6.SuspendLayout();
			this.panel1.SuspendLayout();
			this.SuspendLayout();
			// 
			// tabControl10
			// 
			this.tabControl10.Controls.Add(this.tabPage1);
			this.tabControl10.Controls.Add(this.tabPage3);
			this.tabControl10.Controls.Add(this.tabPage5);
			this.tabControl10.Controls.Add(this.tabPage7);
			this.tabControl10.Controls.Add(this.tabPage9);
			this.tabControl10.Controls.Add(this.tabPage10);
			this.tabControl10.Controls.Add(this.tabPage11);
			this.tabControl10.Controls.Add(this.tabPage12);
			this.tabControl10.Controls.Add(this.tabPage13);
			this.tabControl10.Controls.Add(this.tabPage14);
			this.tabControl10.Controls.Add(this.tabPage15);
			this.tabControl10.Controls.Add(this.tabPage16);
			this.tabControl10.Controls.Add(this.tabPage2);
			this.tabControl10.Controls.Add(this.tabPage4);
			this.tabControl10.Controls.Add(this.tabPage6);
			resources.ApplyResources(this.tabControl10, "tabControl10");
			this.tabControl10.Name = "tabControl10";
			this.tabControl10.SelectedIndex = 0;
			// 
			// tabPage1
			// 
			this.tabPage1.Controls.Add(this.richTextBox09);
			this.tabPage1.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
			resources.ApplyResources(this.tabPage1, "tabPage1");
			this.tabPage1.Name = "tabPage1";
			this.tabPage1.UseVisualStyleBackColor = true;
			// 
			// richTextBox09
			// 
			this.richTextBox09.BorderStyle = System.Windows.Forms.BorderStyle.None;
			resources.ApplyResources(this.richTextBox09, "richTextBox09");
			this.richTextBox09.Name = "richTextBox09";
			// 
			// tabPage3
			// 
			this.tabPage3.Controls.Add(this.richTextBox10);
			resources.ApplyResources(this.tabPage3, "tabPage3");
			this.tabPage3.Name = "tabPage3";
			this.tabPage3.UseVisualStyleBackColor = true;
			// 
			// richTextBox10
			// 
			this.richTextBox10.BorderStyle = System.Windows.Forms.BorderStyle.None;
			resources.ApplyResources(this.richTextBox10, "richTextBox10");
			this.richTextBox10.Name = "richTextBox10";
			// 
			// tabPage5
			// 
			this.tabPage5.Controls.Add(this.richTextBox11);
			resources.ApplyResources(this.tabPage5, "tabPage5");
			this.tabPage5.Name = "tabPage5";
			this.tabPage5.UseVisualStyleBackColor = true;
			// 
			// richTextBox11
			// 
			this.richTextBox11.BorderStyle = System.Windows.Forms.BorderStyle.None;
			resources.ApplyResources(this.richTextBox11, "richTextBox11");
			this.richTextBox11.Name = "richTextBox11";
			// 
			// tabPage7
			// 
			this.tabPage7.Controls.Add(this.richTextBox12);
			resources.ApplyResources(this.tabPage7, "tabPage7");
			this.tabPage7.Name = "tabPage7";
			this.tabPage7.UseVisualStyleBackColor = true;
			// 
			// richTextBox12
			// 
			this.richTextBox12.BorderStyle = System.Windows.Forms.BorderStyle.None;
			resources.ApplyResources(this.richTextBox12, "richTextBox12");
			this.richTextBox12.Name = "richTextBox12";
			// 
			// tabPage9
			// 
			this.tabPage9.Controls.Add(this.richTextBox13);
			resources.ApplyResources(this.tabPage9, "tabPage9");
			this.tabPage9.Name = "tabPage9";
			this.tabPage9.UseVisualStyleBackColor = true;
			// 
			// richTextBox13
			// 
			this.richTextBox13.BorderStyle = System.Windows.Forms.BorderStyle.None;
			resources.ApplyResources(this.richTextBox13, "richTextBox13");
			this.richTextBox13.Name = "richTextBox13";
			// 
			// tabPage10
			// 
			this.tabPage10.Controls.Add(this.richTextBox14);
			resources.ApplyResources(this.tabPage10, "tabPage10");
			this.tabPage10.Name = "tabPage10";
			this.tabPage10.UseVisualStyleBackColor = true;
			// 
			// richTextBox14
			// 
			this.richTextBox14.BorderStyle = System.Windows.Forms.BorderStyle.None;
			resources.ApplyResources(this.richTextBox14, "richTextBox14");
			this.richTextBox14.Name = "richTextBox14";
			// 
			// tabPage11
			// 
			this.tabPage11.Controls.Add(this.richTextBox15);
			resources.ApplyResources(this.tabPage11, "tabPage11");
			this.tabPage11.Name = "tabPage11";
			this.tabPage11.UseVisualStyleBackColor = true;
			// 
			// richTextBox15
			// 
			this.richTextBox15.BorderStyle = System.Windows.Forms.BorderStyle.None;
			resources.ApplyResources(this.richTextBox15, "richTextBox15");
			this.richTextBox15.Name = "richTextBox15";
			// 
			// tabPage12
			// 
			this.tabPage12.Controls.Add(this.richTextBox16);
			resources.ApplyResources(this.tabPage12, "tabPage12");
			this.tabPage12.Name = "tabPage12";
			this.tabPage12.UseVisualStyleBackColor = true;
			// 
			// richTextBox16
			// 
			this.richTextBox16.BorderStyle = System.Windows.Forms.BorderStyle.None;
			resources.ApplyResources(this.richTextBox16, "richTextBox16");
			this.richTextBox16.Name = "richTextBox16";
			// 
			// tabPage13
			// 
			this.tabPage13.Controls.Add(this.richTextBox17);
			resources.ApplyResources(this.tabPage13, "tabPage13");
			this.tabPage13.Name = "tabPage13";
			this.tabPage13.UseVisualStyleBackColor = true;
			// 
			// richTextBox17
			// 
			this.richTextBox17.BorderStyle = System.Windows.Forms.BorderStyle.None;
			resources.ApplyResources(this.richTextBox17, "richTextBox17");
			this.richTextBox17.Name = "richTextBox17";
			// 
			// tabPage14
			// 
			this.tabPage14.Controls.Add(this.richTextBox18);
			resources.ApplyResources(this.tabPage14, "tabPage14");
			this.tabPage14.Name = "tabPage14";
			this.tabPage14.UseVisualStyleBackColor = true;
			// 
			// richTextBox18
			// 
			this.richTextBox18.BorderStyle = System.Windows.Forms.BorderStyle.None;
			resources.ApplyResources(this.richTextBox18, "richTextBox18");
			this.richTextBox18.Name = "richTextBox18";
			// 
			// tabPage15
			// 
			this.tabPage15.Controls.Add(this.richTextBox19);
			resources.ApplyResources(this.tabPage15, "tabPage15");
			this.tabPage15.Name = "tabPage15";
			this.tabPage15.UseVisualStyleBackColor = true;
			// 
			// richTextBox19
			// 
			this.richTextBox19.BorderStyle = System.Windows.Forms.BorderStyle.None;
			resources.ApplyResources(this.richTextBox19, "richTextBox19");
			this.richTextBox19.Name = "richTextBox19";
			// 
			// tabPage16
			// 
			this.tabPage16.Controls.Add(this.richTextBox20);
			resources.ApplyResources(this.tabPage16, "tabPage16");
			this.tabPage16.Name = "tabPage16";
			this.tabPage16.UseVisualStyleBackColor = true;
			// 
			// richTextBox20
			// 
			this.richTextBox20.BorderStyle = System.Windows.Forms.BorderStyle.None;
			resources.ApplyResources(this.richTextBox20, "richTextBox20");
			this.richTextBox20.Name = "richTextBox20";
			// 
			// tabPage2
			// 
			this.tabPage2.Controls.Add(this.richTextBox21);
			resources.ApplyResources(this.tabPage2, "tabPage2");
			this.tabPage2.Name = "tabPage2";
			this.tabPage2.UseVisualStyleBackColor = true;
			// 
			// richTextBox21
			// 
			this.richTextBox21.BorderStyle = System.Windows.Forms.BorderStyle.None;
			resources.ApplyResources(this.richTextBox21, "richTextBox21");
			this.richTextBox21.Name = "richTextBox21";
			// 
			// tabPage4
			// 
			this.tabPage4.Controls.Add(this.richTextBox22);
			resources.ApplyResources(this.tabPage4, "tabPage4");
			this.tabPage4.Name = "tabPage4";
			this.tabPage4.UseVisualStyleBackColor = true;
			// 
			// richTextBox22
			// 
			this.richTextBox22.BorderStyle = System.Windows.Forms.BorderStyle.None;
			resources.ApplyResources(this.richTextBox22, "richTextBox22");
			this.richTextBox22.Name = "richTextBox22";
			// 
			// tabPage6
			// 
			this.tabPage6.Controls.Add(this.richTextBox23);
			resources.ApplyResources(this.tabPage6, "tabPage6");
			this.tabPage6.Name = "tabPage6";
			this.tabPage6.UseVisualStyleBackColor = true;
			// 
			// richTextBox23
			// 
			this.richTextBox23.BorderStyle = System.Windows.Forms.BorderStyle.None;
			resources.ApplyResources(this.richTextBox23, "richTextBox23");
			this.richTextBox23.Name = "richTextBox23";
			// 
			// panel1
			// 
			this.panel1.BackColor = System.Drawing.Color.WhiteSmoke;
			this.panel1.Controls.Add(this.label1);
			this.panel1.Controls.Add(this.buttonVOS);
			this.panel1.Controls.Add(this.checkBox1);
			resources.ApplyResources(this.panel1, "panel1");
			this.panel1.Name = "panel1";
			// 
			// label1
			// 
			resources.ApplyResources(this.label1, "label1");
			this.label1.ForeColor = System.Drawing.Color.DodgerBlue;
			this.label1.Name = "label1";
			// 
			// buttonVOS
			// 
			this.buttonVOS.BackColor = System.Drawing.Color.Gainsboro;
			resources.ApplyResources(this.buttonVOS, "buttonVOS");
			this.buttonVOS.Name = "buttonVOS";
			this.buttonVOS.UseVisualStyleBackColor = false;
			this.buttonVOS.Click += new System.EventHandler(this.ButtonVOSClick);
			// 
			// checkBox1
			// 
			this.checkBox1.BackColor = System.Drawing.Color.Transparent;
			resources.ApplyResources(this.checkBox1, "checkBox1");
			this.checkBox1.ForeColor = System.Drawing.Color.SlateGray;
			this.checkBox1.Name = "checkBox1";
			this.checkBox1.UseVisualStyleBackColor = false;
			this.checkBox1.CheckedChanged += new System.EventHandler(this.CheckBox1CheckedChanged);
			// 
			// CopyRightForm
			// 
			resources.ApplyResources(this, "$this");
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.BackColor = System.Drawing.Color.White;
			this.Controls.Add(this.tabControl10);
			this.Controls.Add(this.panel1);
			this.Name = "CopyRightForm";
			this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.CopyRightFormFormClosing);
			this.tabControl10.ResumeLayout(false);
			this.tabPage1.ResumeLayout(false);
			this.tabPage3.ResumeLayout(false);
			this.tabPage5.ResumeLayout(false);
			this.tabPage7.ResumeLayout(false);
			this.tabPage9.ResumeLayout(false);
			this.tabPage10.ResumeLayout(false);
			this.tabPage11.ResumeLayout(false);
			this.tabPage12.ResumeLayout(false);
			this.tabPage13.ResumeLayout(false);
			this.tabPage14.ResumeLayout(false);
			this.tabPage15.ResumeLayout(false);
			this.tabPage16.ResumeLayout(false);
			this.tabPage2.ResumeLayout(false);
			this.tabPage4.ResumeLayout(false);
			this.tabPage6.ResumeLayout(false);
			this.panel1.ResumeLayout(false);
			this.ResumeLayout(false);
		}
		private System.Windows.Forms.RichTextBox richTextBox23;
		private System.Windows.Forms.TabPage tabPage6;
		private System.Windows.Forms.RichTextBox richTextBox22;
		private System.Windows.Forms.TabPage tabPage4;
		private System.Windows.Forms.Label label1;
		private System.Windows.Forms.Button buttonVOS;
		private System.Windows.Forms.CheckBox checkBox1;
		private System.Windows.Forms.RichTextBox richTextBox21;
		private System.Windows.Forms.TabPage tabPage2;
		private System.Windows.Forms.RichTextBox richTextBox20;
		private System.Windows.Forms.TabPage tabPage16;
		private System.Windows.Forms.RichTextBox richTextBox19;
		private System.Windows.Forms.TabPage tabPage15;
		private System.Windows.Forms.RichTextBox richTextBox18;
		private System.Windows.Forms.TabPage tabPage14;
		private System.Windows.Forms.RichTextBox richTextBox17;
		private System.Windows.Forms.RichTextBox richTextBox16;
		private System.Windows.Forms.RichTextBox richTextBox15;
		private System.Windows.Forms.RichTextBox richTextBox14;
		private System.Windows.Forms.RichTextBox richTextBox13;
		private System.Windows.Forms.RichTextBox richTextBox12;
		private System.Windows.Forms.RichTextBox richTextBox11;
		private System.Windows.Forms.RichTextBox richTextBox10;
		private System.Windows.Forms.RichTextBox richTextBox09;
		private System.Windows.Forms.TabPage tabPage13;
		private System.Windows.Forms.TabPage tabPage12;
		private System.Windows.Forms.TabPage tabPage11;
		private System.Windows.Forms.TabPage tabPage10;
		private System.Windows.Forms.TabPage tabPage9;
		private System.Windows.Forms.TabPage tabPage7;
		private System.Windows.Forms.TabPage tabPage5;
		private System.Windows.Forms.TabPage tabPage3;
		private System.Windows.Forms.Panel panel1;
		private System.Windows.Forms.TabPage tabPage1;
		private System.Windows.Forms.TabControl tabControl10;
	}
}
