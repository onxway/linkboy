﻿
namespace n_CopyRightForm
{
using System;
using System.IO;
using System.Net;
using System.Text;
using System.Windows.Forms;
using System.Drawing;
using n_OS;

/// <summary>
/// Description of WorkForm.
/// </summary>
public partial class CopyRightForm : Form
{
	//主窗口
	public CopyRightForm()
	{
		//
		// The InitializeComponent() call is required for Windows Forms designer support.
		//
		InitializeComponent();
		
		//初始化
		this.richTextBox09.Text = VIO.OpenTextFileGB2312( OS.SystemRoot + "Resource" + OS.PATH_S + "version" + OS.PATH_S + "2009年.txt" );
		this.richTextBox10.Text = VIO.OpenTextFileGB2312( OS.SystemRoot + "Resource" + OS.PATH_S + "version" + OS.PATH_S + "2010年.txt" );
		this.richTextBox11.Text = VIO.OpenTextFileGB2312( OS.SystemRoot + "Resource" + OS.PATH_S + "version" + OS.PATH_S + "2011年.txt" );
		this.richTextBox12.Text = VIO.OpenTextFileGB2312( OS.SystemRoot + "Resource" + OS.PATH_S + "version" + OS.PATH_S + "2012年.txt" );
		this.richTextBox13.Text = VIO.OpenTextFileGB2312( OS.SystemRoot + "Resource" + OS.PATH_S + "version" + OS.PATH_S + "2013年.txt" );
		this.richTextBox14.Text = VIO.OpenTextFileGB2312( OS.SystemRoot + "Resource" + OS.PATH_S + "version" + OS.PATH_S + "2014年.txt" );
		this.richTextBox15.Text = VIO.OpenTextFileGB2312( OS.SystemRoot + "Resource" + OS.PATH_S + "version" + OS.PATH_S + "2015年.txt" );
		this.richTextBox16.Text = VIO.OpenTextFileGB2312( OS.SystemRoot + "Resource" + OS.PATH_S + "version" + OS.PATH_S + "2016年.txt" );
		this.richTextBox17.Text = VIO.OpenTextFileGB2312( OS.SystemRoot + "Resource" + OS.PATH_S + "version" + OS.PATH_S + "2017年.txt" );
		this.richTextBox18.Text = VIO.OpenTextFileGB2312( OS.SystemRoot + "Resource" + OS.PATH_S + "version" + OS.PATH_S + "2018年.txt" );
		this.richTextBox19.Text = VIO.OpenTextFileGB2312( OS.SystemRoot + "Resource" + OS.PATH_S + "version" + OS.PATH_S + "2019年.txt" );
		this.richTextBox20.Text = VIO.OpenTextFileGB2312( OS.SystemRoot + "Resource" + OS.PATH_S + "version" + OS.PATH_S + "2020年.txt" );
		this.richTextBox21.Text = VIO.OpenTextFileGB2312( OS.SystemRoot + "Resource" + OS.PATH_S + "version" + OS.PATH_S + "2021年.txt" );
		this.richTextBox22.Text = VIO.OpenTextFileGB2312( OS.SystemRoot + "Resource" + OS.PATH_S + "version" + OS.PATH_S + "2022年.txt" );
		this.richTextBox23.Text = VIO.OpenTextFileGB2312( OS.SystemRoot + "Resource" + OS.PATH_S + "version" + OS.PATH_S + "2023年.txt" );
		
		//新增年份时要修改这里
		this.tabControl10.SelectedTab =this.tabPage6; 
		this.richTextBox23.SelectionStart = this.richTextBox21.TextLength - 1;
		this.richTextBox23.ScrollToCaret();
	}
	
	public void Run()
	{
		this.Visible = true;
	}
	
	void CopyRightFormFormClosing(object sender, FormClosingEventArgs e)
	{
		this.Visible = false;
		e.Cancel = true;
	}
	
	void CheckBox1CheckedChanged(object sender, EventArgs e)
	{
		n_MainSystemData.SystemData.ShowNewVersion = !this.checkBox1.Checked;
		n_MainSystemData.SystemData.isChanged = true;
	}
	
	void ButtonVOSClick(object sender, EventArgs e)
	{
		if( n_wlibCom.wlibCom.VBox == null ) {
			n_wlibCom.wlibCom.VBox = new n_VosVersForm.VosVersForm();
		}
		n_wlibCom.wlibCom.VBox.Run( 2 );
	}
}
}



