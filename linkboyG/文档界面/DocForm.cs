﻿
namespace n_DocForm
{
using System;
using System.Windows.Forms;
using n_Compiler;
using n_ET;
using n_MainSystemData;
using n_ParseNet;
using n_Config;
using n_TargetFile;
using n_AVRdude;
using System.IO;
using System.Drawing;

/// <summary>
/// Description of WorkForm.
/// </summary>
public partial class DocForm : Form
{
	//主窗口
	public DocForm()
	{
		//
		// The InitializeComponent() call is required for Windows Forms designer support.
		//
		InitializeComponent();
	}
	
	//显示
	public void Run( string fname )
	{
		//n_wlibCom.wlibCom.SetFont( richTextBox1 );
		
		richTextBox1.BackColor = Color.White;
		richTextBox1.LoadFile( fname );
		
		richTextBox1.Focus();
		this.Visible = true;
	}
	
	void DocFormFormClosing(object sender, FormClosingEventArgs e)
	{
		this.Visible = false;
		e.Cancel = true;
	}
}
}




