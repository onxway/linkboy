﻿
namespace n_ClearChipForm
{
using System;
using System.Drawing;
using System.Windows.Forms;

using c_FormMover;
using n_ISP;
using n_OS;

public partial class ClearChipForm : Form
{
	FormMover fm;
	
	Timer ISPTimer;
	public bool USBDeviceIsExist;
	
	public bool OpenNewFile;
	
	//主窗口
	public ClearChipForm()
	{
		InitializeComponent();
		Rectangle E= Screen.PrimaryScreen.Bounds;
		
		//位置是屏幕左下角
		Point p0 = new Point(
			Screen.PrimaryScreen.WorkingArea.Width- this.Width,
			Screen.PrimaryScreen.WorkingArea.Height - this.Height );
		
		//位置是屏幕中间
		Point p1 = new Point(
			( Screen.PrimaryScreen.WorkingArea.Width- this.Width ) / 2,
			( Screen.PrimaryScreen.WorkingArea.Height - this.Height ) / 2 );
		
		this.Location = p1;
		this.Visible = false;
		
		fm = new FormMover( this );
		
		ISPTimer = new Timer();
		ISPTimer.Interval = 1000;
		ISPTimer.Tick += new EventHandler( ISP_scan );
		ISPTimer.Enabled = true;
		
		USBDeviceIsExist = false;
		OpenNewFile = true;
	}
	
	//扫描
	void ISP_scan( object sender, EventArgs e )
	{
		bool exit = USB.DeviceIsExist();
		if( exit && !USBDeviceIsExist ) {
			
			if( OpenNewFile ) {
				OpenNewFile = false;
				
				//this.Visible = true;
				//this.buttonClear.Focus();
				//this.buttonClear.Select();
			}
		}
		USBDeviceIsExist = exit;
	}
	
	//清空芯片程序
	void ClearChip()
	{
		string FileName = OS.SystemRoot + "Resource" + OS.PATH_S + "clear.hex";
		string text = VIO.OpenTextFileGB2312( FileName );
		ISP.DownLoad( text );
		if( G.CGPanel != null ) {
			G.CGPanel.EndDownLoad();
		}
	}
	
	//点击确定按钮
	void ButtonClearClick(object sender, EventArgs e)
	{
		ClearChip();
		this.Visible = false;
	}
	
	//点击取消按钮
	void ButtonCancelClick(object sender, EventArgs e)
	{
		this.Visible = false;
	}
	
	void CheckBox1CheckedChanged(object sender, EventArgs e)
	{
		if( this.checkBox1.Checked ) {
			ISPTimer.Enabled = false;
			//this.Visible = false;
		}
		else {
			ISPTimer.Enabled = true;
		}
	}
}
}


