﻿
namespace n_GFilePanel
{
using System;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Windows.Forms;

using n_CCode;
using n_ControlCenter;
using n_GUIset;
using n_ImagePanel;

//分页控件
public class GFilePanel : Panel
{
	public CCode ccode;
	public ImagePanel GPanel;
	
	static bool hasShow = false;
	
	//构造函数
	public GFilePanel( string vFilePath, int W, int H )
	{
		this.BackColor = GUIset.myBackColor;
		this.BorderStyle = BorderStyle.None;
		this.Dock = DockStyle.Fill;
		this.Width = W;
		this.Height = H;
		
		//添加图形编程界面
		//GPanel = new ImagePanel( this.Width * 3 / 2, this.Height * 3 / 2 );
		//GPanel = new ImagePanel( this.Width, this.Height, RefreshTimer );
		GPanel = new ImagePanel( this.Width, this.Height );
		//GPanel = new ImagePanel( Screen.PrimaryScreen.WorkingArea.Width, Screen.PrimaryScreen.WorkingArea.Height );
		
		GPanel.GHead.DeleAuto = ControlCenter.Auto;
		GPanel.GHead.DeleDebug = ControlCenter.Debug;
		GPanel.GHead.DeleSimulate = ControlCenter.Simulate;
		this.SizeChanged += GPanel.GHead.SizeChanged;
		
		GPanel.Location = new Point( 0, 0 );
		GPanel.Visible = false;
		this.Controls.Add( GPanel );
		GPanel.Width = W;
		GPanel.Height = H;
		
		n_HardModule.Port.C_Rol = 50;
		GPanel.EventNumber = n_ImagePanel.ImagePanel.DefaultEventNumber;
		
		//添加文本框
		ccode = new CCode( vFilePath );
		G.ccode = ccode;
		
		GPanel.LoadModule( ccode.GUItext, true );
		GPanel.HistoryList_Record();
		
		ccode.isSystemFile = false;
		
		ccode.isNew = false;
		
		//文本框事件
		ccode.Saved += new EventHandler( Text_Saved );
		
		GPanel.Visible = true;
		GPanel.AutoSetTableLanguage();
		GPanel.Focus();
		
		//ChangeDetectTimer.Enabled = true;
		
		//可以触发下载检测提示
		if( U.U.CPForm != null ) {
			U.U.CPForm.USBDeviceIsExist = false;
			U.U.CPForm.OpenNewFile = true;
		}
		GPanel.HaseShow = true;
	}
	
	//判断是否可以关闭
	public bool TryClose()
	{
		//这里是防止鼠标没有点击界面导致未检测文本是否改变
		if( !ccode.isChanged && ccode.GUItext != n_GUIcoder.GUIcoder.VarToText( GPanel ) ) {
			ccode.isChanged = true;
		}
		
		if( !GPanel.isNoModule && ccode.isChanged ) {
			DialogResult dlgResult = MessageBox.Show(
				"文件已经被修改, 需要保存吗?", "关闭前文件保存",
				MessageBoxButtons.YesNoCancel, MessageBoxIcon.Question );
			if( dlgResult == DialogResult.Yes ) {
				if( !ccode.Save() ) {
					return false;
				}
			}
			if( dlgResult == DialogResult.Cancel ) {
				return false;
			}
			if( dlgResult == DialogResult.No ) {
				
				//不保存文件直接退出
				
				n_Backup.Backup.SaveText();
			}
		}
		ccode = null;
		GPanel.Close();
		return true;
	}
	
	//文本保存事件
	void Text_Saved( object sender, EventArgs e )
	{
		//ccode.UserText = G.CodeViewBox.GetUserText();
		ccode.UserText = U.U.m.GetUserText();
		
		ccode.GUItext = GPanel.VarToText();
		ccode.SetCode( ccode.UserText, ccode.GUItext );
		
		if( !hasShow ) {
			hasShow = true;
			//n_Debug.Warning.WarningMessage = "提示: 如需查看此文档的历史版本, 可到这个文件夹找找:\n" + n_Backup.Backup.BackupPath;
		}
	}
}
}





