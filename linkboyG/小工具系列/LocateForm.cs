﻿/*
 * 由SharpDevelop创建。
 * 用户： cap_gpu
 * 日期: 2017/4/9
 * 时间: 8:03
 * 
 * 要改变这种模板请点击 工具|选项|代码编写|编辑标准头文件
 */
using System;
using System.Drawing;
using System.Windows.Forms;

namespace n_LocateForm
{
/// <summary>
/// Description of LocateForm.
/// </summary>
public partial class LocateForm : Form
{
	public LocateForm()
	{
		//
		// The InitializeComponent() call is required for Windows Forms designer support.
		//
		InitializeComponent();
		
		//
		// TODO: Add constructor code after the InitializeComponent() call.
		//
	}
	
	//运行
	public void Run()
	{
		this.Visible = true;
	}
	
	void LocateFormFormClosing(object sender, FormClosingEventArgs e)
	{
		this.Visible = false;
		e.Cancel = true;
	}
	
	void ButtonJISuanClick(object sender, EventArgs e)
	{
		labelResult.Text = "";
		try {
			
			int AE = int.Parse( textBoxAE.Text );
			int AW = int.Parse( textBoxAW.Text );
			int BE = int.Parse( textBoxBE.Text );
			int BW = int.Parse( textBoxBW.Text );
			
			int CE = BE - AE;
			int CW = BW - AW;
			
			//int X = 6371004 * CE * Math.PI / 180 / 1000000;
			//int Y = 6371004 * CW * Math.PI / 180 / 1000000;
			
			//int X = CE * 0.11119499456094666666666666666667;
			//int Y =  CW * 0.11119499456094666666666666666667;
			
			int X = CE * 556 / 5000;
			int Y =  CW * 556 / 5000;
			int R = (int)Math.Sqrt( X*X + Y*Y );
			
			labelResult.Text += "AB两地东西距离:" + X + "米\n" + "AB两地南北距离:" + Y + "米\n";
			labelResult.Text += "AB两地直线距离:" + R + "米\n";
			
			/*
			double AE = double.Parse( textBoxAE.Text );
			double AW = double.Parse( textBoxAW.Text );
			double BE = double.Parse( textBoxBE.Text );
			double BW = double.Parse( textBoxBW.Text );
			AE /= 1000000;
			AW /= 1000000;
			BE /= 1000000;
			BW /= 1000000;
			
			double HAE = AE * Math.PI / 180;
			double HAW = AW * Math.PI / 180;
			double HBE = BE * Math.PI / 180;
			double HBW = BW * Math.PI / 180;
			
			int X = (int)(6371004 * (HBE - HAE));
			int Y = (int)(6371004 * (HBW - HAW));
			int R = (int)Math.Sqrt( X*X + Y*Y );
			
			labelResult.Text += "A地东经度数:" + AE + "\n" + "A地北纬度数:" + AW + "\n" + "B地东经度数:" + BE + "\n" + "B地北纬度数:" + BW + "\n";
			labelResult.Text += "A地东经弧度:" + HAE + "\n" + "A地北纬弧度:" + HAW + "\n" + "B地东经弧度:" + HBE + "\n" + "B地北纬弧度:" + HBW + "\n";
			labelResult.Text += "AB两地东西距离:" + X + "米\n" + "AB两地南北距离:" + Y + "米\n";
			labelResult.Text += "AB两地直线距离:" + R + "米\n";
			*/
		}
		catch {
			MessageBox.Show( "计算异常，请检查输入参数是否合法" );
		}
	}
}
}

