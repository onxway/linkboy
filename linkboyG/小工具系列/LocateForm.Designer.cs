﻿/*
 * 由SharpDevelop创建。
 * 用户： cap_gpu
 * 日期: 2017/4/9
 * 时间: 8:03
 * 
 * 要改变这种模板请点击 工具|选项|代码编写|编辑标准头文件
 */
namespace n_LocateForm
{
	partial class LocateForm
	{
		/// <summary>
		/// Designer variable used to keep track of non-visual components.
		/// </summary>
		private System.ComponentModel.IContainer components = null;
		
		/// <summary>
		/// Disposes resources used by the form.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing) {
				if (components != null) {
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}
		
		/// <summary>
		/// This method is required for Windows Forms designer support.
		/// Do not change the method contents inside the source code editor. The Forms designer might
		/// not be able to load this method if it was changed manually.
		/// </summary>
		private void InitializeComponent()
		{
			this.label1 = new System.Windows.Forms.Label();
			this.label2 = new System.Windows.Forms.Label();
			this.textBoxAE = new System.Windows.Forms.TextBox();
			this.textBoxAW = new System.Windows.Forms.TextBox();
			this.textBoxBW = new System.Windows.Forms.TextBox();
			this.textBoxBE = new System.Windows.Forms.TextBox();
			this.label3 = new System.Windows.Forms.Label();
			this.label4 = new System.Windows.Forms.Label();
			this.buttonJISuan = new System.Windows.Forms.Button();
			this.labelResult = new System.Windows.Forms.Label();
			this.SuspendLayout();
			// 
			// label1
			// 
			this.label1.Font = new System.Drawing.Font("微软雅黑", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
			this.label1.Location = new System.Drawing.Point(12, 9);
			this.label1.Name = "label1";
			this.label1.Size = new System.Drawing.Size(94, 26);
			this.label1.TabIndex = 0;
			this.label1.Text = "A地经度：";
			// 
			// label2
			// 
			this.label2.Font = new System.Drawing.Font("微软雅黑", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
			this.label2.Location = new System.Drawing.Point(12, 38);
			this.label2.Name = "label2";
			this.label2.Size = new System.Drawing.Size(94, 26);
			this.label2.TabIndex = 1;
			this.label2.Text = "A地纬度：";
			// 
			// textBoxAE
			// 
			this.textBoxAE.Font = new System.Drawing.Font("微软雅黑", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
			this.textBoxAE.Location = new System.Drawing.Point(97, 6);
			this.textBoxAE.Name = "textBoxAE";
			this.textBoxAE.Size = new System.Drawing.Size(120, 29);
			this.textBoxAE.TabIndex = 2;
			this.textBoxAE.Text = "116183412";
			this.textBoxAE.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
			// 
			// textBoxAW
			// 
			this.textBoxAW.Font = new System.Drawing.Font("微软雅黑", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
			this.textBoxAW.Location = new System.Drawing.Point(97, 35);
			this.textBoxAW.Name = "textBoxAW";
			this.textBoxAW.Size = new System.Drawing.Size(120, 29);
			this.textBoxAW.TabIndex = 3;
			this.textBoxAW.Text = "39505136";
			this.textBoxAW.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
			// 
			// textBoxBW
			// 
			this.textBoxBW.Font = new System.Drawing.Font("微软雅黑", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
			this.textBoxBW.Location = new System.Drawing.Point(328, 35);
			this.textBoxBW.Name = "textBoxBW";
			this.textBoxBW.Size = new System.Drawing.Size(120, 29);
			this.textBoxBW.TabIndex = 7;
			this.textBoxBW.Text = "39505136";
			this.textBoxBW.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
			// 
			// textBoxBE
			// 
			this.textBoxBE.Font = new System.Drawing.Font("微软雅黑", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
			this.textBoxBE.Location = new System.Drawing.Point(328, 6);
			this.textBoxBE.Name = "textBoxBE";
			this.textBoxBE.Size = new System.Drawing.Size(120, 29);
			this.textBoxBE.TabIndex = 6;
			this.textBoxBE.Text = "116183412";
			this.textBoxBE.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
			// 
			// label3
			// 
			this.label3.Font = new System.Drawing.Font("微软雅黑", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
			this.label3.Location = new System.Drawing.Point(243, 38);
			this.label3.Name = "label3";
			this.label3.Size = new System.Drawing.Size(94, 26);
			this.label3.TabIndex = 5;
			this.label3.Text = "B地纬度：";
			// 
			// label4
			// 
			this.label4.Font = new System.Drawing.Font("微软雅黑", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
			this.label4.Location = new System.Drawing.Point(243, 9);
			this.label4.Name = "label4";
			this.label4.Size = new System.Drawing.Size(94, 26);
			this.label4.TabIndex = 4;
			this.label4.Text = "B地经度：";
			// 
			// buttonJISuan
			// 
			this.buttonJISuan.BackColor = System.Drawing.Color.LightGray;
			this.buttonJISuan.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
			this.buttonJISuan.Font = new System.Drawing.Font("微软雅黑", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
			this.buttonJISuan.ForeColor = System.Drawing.Color.Black;
			this.buttonJISuan.Location = new System.Drawing.Point(483, 6);
			this.buttonJISuan.Name = "buttonJISuan";
			this.buttonJISuan.Size = new System.Drawing.Size(99, 58);
			this.buttonJISuan.TabIndex = 8;
			this.buttonJISuan.Text = "计算方位";
			this.buttonJISuan.UseVisualStyleBackColor = false;
			this.buttonJISuan.Click += new System.EventHandler(this.ButtonJISuanClick);
			// 
			// labelResult
			// 
			this.labelResult.BackColor = System.Drawing.Color.White;
			this.labelResult.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
			this.labelResult.Font = new System.Drawing.Font("微软雅黑", 10.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
			this.labelResult.Location = new System.Drawing.Point(12, 86);
			this.labelResult.Name = "labelResult";
			this.labelResult.Size = new System.Drawing.Size(570, 195);
			this.labelResult.TabIndex = 13;
			// 
			// LocateForm
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.BackColor = System.Drawing.Color.WhiteSmoke;
			this.ClientSize = new System.Drawing.Size(594, 291);
			this.Controls.Add(this.labelResult);
			this.Controls.Add(this.buttonJISuan);
			this.Controls.Add(this.textBoxBW);
			this.Controls.Add(this.textBoxBE);
			this.Controls.Add(this.label3);
			this.Controls.Add(this.label4);
			this.Controls.Add(this.textBoxAW);
			this.Controls.Add(this.textBoxAE);
			this.Controls.Add(this.label2);
			this.Controls.Add(this.label1);
			this.Name = "LocateForm";
			this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
			this.Text = "地理方位计算器";
			this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.LocateFormFormClosing);
			this.ResumeLayout(false);
			this.PerformLayout();
		}
		private System.Windows.Forms.Label labelResult;
		private System.Windows.Forms.Button buttonJISuan;
		private System.Windows.Forms.Label label4;
		private System.Windows.Forms.Label label3;
		private System.Windows.Forms.TextBox textBoxBE;
		private System.Windows.Forms.TextBox textBoxBW;
		private System.Windows.Forms.TextBox textBoxAW;
		private System.Windows.Forms.TextBox textBoxAE;
		private System.Windows.Forms.Label label2;
		private System.Windows.Forms.Label label1;
	}
}
