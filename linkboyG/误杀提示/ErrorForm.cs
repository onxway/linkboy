﻿
namespace n_ErrorForm
{
using System;
using System.Drawing;
using System.Windows.Forms;

using c_FormMover;
using n_ISP;
using n_OS;

public partial class ErrorForm : Form
{
	string Mes;
	
	//主窗口
	public ErrorForm( string mes )
	{
		InitializeComponent();
		
		Mes = mes;
	}
	
	void Button1Click(object sender, EventArgs e)
	{
		//MessageBox.Show( Mes );
		System.Windows.Forms.Clipboard.SetText( Mes );
	}
}
}

