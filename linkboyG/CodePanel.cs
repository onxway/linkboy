﻿
//分页文件类
//发现的BUG:Tab的标签尺寸是默认的,改了字体也不行;

namespace n_CodePanel
{
using System;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.IO;
using System.Windows.Forms;

using n_CCode;
using n_CharType;
using n_Config;
using n_ControlCenter;
using n_ctext;
using n_FunctionList;
using n_MainSystemData;
using n_OS;
using n_StructList;
using n_UnitList;
using n_VarList;
using n_VarType;
using n_VdataList;
using T;

//分页控件
public class CodePanel : Panel
{
	Graphics g;
	Pen p1;
	Pen p2;
	
	public CTextBox ctext;
	
	ListBox ShortList;
	
	//行号显示
	public Label LineLabel;
	Rectangle rec;
	int firstLine;
	int currentLine;
	int endLine;
	Font LineNumberFont;
	
	StringFormat format;
	
	public int SimcurrentLine;
	
	public bool IgnoreChanged;
	
	Image Point;
	
	//构造函数
	public CodePanel()
	{
		this.BackColor = Color.White;
		this.BorderStyle = BorderStyle.None;
		Dock = DockStyle.Fill;
		
		//添加快捷选择框
		ShortList = new ListBox();
		ShortList.Visible = false;
		ShortList.Width = 250;
		ShortList.Height = 300;
		ShortList.Font = SystemData.SourceTextFont;
		ShortList.BackColor = Color.White;
		ShortList.ScrollAlwaysVisible = true;
		ShortList.KeyDown += new KeyEventHandler( List_KeyDown );
		ShortList.LostFocus += new EventHandler( List_LostFocus );
		ShortList.GotFocus += new EventHandler( List_GotFocus );
		this.Controls.Add( ShortList );
		
		//添加文本框
		ctext = new CTextBox( "" );
		ctext.ContextMenuStrip = T.VisualBox.contextM;
		Controls.Add( ctext );
		ctext.isSystemFile = false;
		ctext.isNew = false;
		ctext.Dock = DockStyle.Fill;
		ctext.SelectionStart = 0;
		ctext.SelectionLength = 0;
		
		
		//这里临时屏蔽用户输入
		//ctext.Focus();
		//ctext.ReadOnly = true;
		
		
		
		T.ctext = ctext;
		
		format = new StringFormat();
		format.Alignment = StringAlignment.Center;
		format.LineAlignment = StringAlignment.Center;
		
		Point = new Bitmap( n_OS.OS.SystemRoot + @"Resource\gui\GCode\pt.png" );
		
		//读取行号字体
		LineNumberFont = n_Drawer.Drawer.FE;
		
		//文本框事件
		ctext.TextChanged += new EventHandler( Text_Changed );
		ctext.MouseDown += new MouseEventHandler( Text_MouseDown );
		ctext.KeyPress += new KeyPressEventHandler( Text_Press );
		ctext.KeyDown += new KeyEventHandler( Text_KeyDown );
		ctext.VScroll += new EventHandler( Text_VScroll );
		ctext.MouseDown += new MouseEventHandler( TextMouseDown );
		
		//添加左边距
		LineLabel = new Label();
		LineLabel.Visible = SystemData.isShowLeftBox;
		LineLabel.Dock = DockStyle.Left;
		LineLabel.Width = 35;
		LineLabel.BackColor = Color.White;
		LineLabel.BorderStyle = BorderStyle.None;
		LineLabel.Paint += new PaintEventHandler( Label_Paint );
		Controls.Add( LineLabel );
		
		LineLabel.Width = (int)this.CreateGraphics().MeasureString( "0000", LineNumberFont ).Width + 5;
		rec = new Rectangle( 0, 0, LineLabel.Width - 1, n_Drawer.Drawer.FE.Height );
		
		StartDrawLineNumber();
		
		g = ctext.CreateGraphics();
		p1 = new Pen( Color.Blue, 1 );
		p1.DashStyle = DashStyle.Dash;
		p2 = new Pen( Color.Red, 1 );
		p2.DashStyle = DashStyle.Dash;
		
		IgnoreChanged = false;
		
		//ctext.DrawAllArea();
		
		StartDrawLineNumber();
	}
	
	//竖直滚动条事件
	void Text_VScroll( object sender, EventArgs e )
	{
		StartDrawLineNumber();
	}
	
	//启动重绘
	void StartDrawLineNumber()
	{
		Point pos = new Point(0, 0);
		int firstIndex = ctext.GetCharIndexFromPosition( pos );
		firstLine = ctext.GetLineFromCharIndex( firstIndex );
		pos.X = ctext.ClientRectangle.Width;
		pos.Y = ctext.ClientRectangle.Height;
		int endIndex = ctext.GetCharIndexFromPosition( pos );
		endLine = ctext.GetLineFromCharIndex( endIndex );
		currentLine = ctext.GetLineFromCharIndex( ctext.SelectionStart );
		LineLabel.Invalidate();
	}
	
	//行号重绘事件
	void Label_Paint( object sender, PaintEventArgs e )
	{
		if( !SystemData.isShowLeftBox || !SystemData.isShowLineNumber ) {
			return;
		}
		
		int stY = ctext.GetPositionFromCharIndex( ctext.GetFirstCharIndexFromLine( firstLine ) ).Y;
		int edY = ctext.GetPositionFromCharIndex( ctext.GetFirstCharIndexFromLine( endLine ) ).Y;
		
		for( int i = firstLine; i <= endLine; ++i ) {
			
			int y1 = ctext.GetPositionFromCharIndex( ctext.GetFirstCharIndexFromLine( i ) ).Y;
			int y2 = ctext.GetPositionFromCharIndex( ctext.GetFirstCharIndexFromLine( i + 1 ) ).Y;
			
			rec.Y = y1;
			rec.Height = y2 - y1;
			
			if( i == currentLine ) {
				e.Graphics.DrawString( ( i + 1 ).ToString(), LineNumberFont, Brushes.Black, rec, format );
			}
			else {
				e.Graphics.DrawString( ( i + 1 ).ToString(), LineNumberFont, Brushes.LightSlateGray, rec, format );
			}
			if( G.SimulateMode && i == SimcurrentLine ) {
				//e.Graphics.FillRectangle( Brushes.Orange, rec );
				//e.Graphics.DrawRectangle( Pens.Red, rec );
				e.Graphics.DrawImage( Point, rec.X + (rec.Width - Point.Width), rec.Y + (rec.Height-Point.Height)/2 );
			}
		}
	}
	
	//添加列表
	void AddList()
	{
		if( ControlCenter.isBusy() ) {
			return;
		}
		string UnitName = "";
		for( int i = ctext.SelectionStart - 1; i >= 0; --i ) {
			char c = ctext.Text[ i ];
			if( !CharType.isLetterOrNumber( c ) && c != '#' ) {
					break;
				}
				UnitName = c + UnitName;
			}
			if( UnitName == ""  ) {
				return;
			}
			string FullUnitName = (UnitName == VarType.Root)? VarType.Root: VarType.Root + "." + UnitName;
			
			int UnitIndex = UnitList.GetIndex( FullUnitName);
			if( UnitIndex == -1 ) {
				//return;
			}
			
			string NameSet = null;
			NameSet += FunctionList.GetFunctionNames( FullUnitName );
			NameSet += VarList.GetVarNames( FullUnitName );
			NameSet += StructList.GetStructNames( FullUnitName );
			NameSet += VdataList.GetVdataNames( FullUnitName );
			NameSet += UnitList.GetUnitNames( FullUnitName );
			if( NameSet == null || NameSet == "" ) {
				return;
			}
		string[] Set = NameSet.TrimEnd( ';' ).Split( ';' );
		ShortList.Items.Clear();
		for( int j = 0; j < Set.Length; ++j ) {
			ShortList.Items.Add( Set[ j ] );
		}
		//ShortList.Height = 20 * Set.Length;
		ShortList.Visible = true;
	}
	
	//列表失去焦点
	void List_LostFocus( object sender, EventArgs e )
	{
		ShortList.Visible = false;
	}
	
	//列表获得焦点
	void List_GotFocus( object sender, EventArgs e )
	{
	}
	
	//快捷框压键事件
	void List_KeyDown( object sender, KeyEventArgs e )
	{
		if( e.KeyCode == Keys.Enter && ShortList.Visible ) {
			e.Handled = true;
			ShortList.Visible = false;
			
			string text = ShortList.SelectedItem.ToString().Split( '\t' )[ 0 ].Trim( ' ' );
			ctext.SelectedText = text;
			return;
		}
	}
	
	//文本压键事件
	void Text_KeyDown( object sender, KeyEventArgs e )
	{
		if( ControlCenter.isBusy() ) {
			return;
		}
		//判断是否按下上下键,切换焦点
		if( ( e.KeyCode == Keys.Up || e.KeyCode == Keys.Down ) && ShortList.Visible ) {
			e.Handled = true;
			ShortList.SelectedIndex = 0;
			ShortList.Focus();
		}
	}
	
	//文本按键事件
	void Text_Press( object sender, KeyPressEventArgs e )
	{
//		if( A.WorkBox.扩展信息窗口.Visible ) {
//			A.WorkBox.扩展信息窗口.Visible = false;
//		}
//		if( CThread.runType != CThread.RunType.Wait ) {
//			return;
//		}
		Point p = ctext.GetPositionFromCharIndex( ctext.SelectionStart );
		int x = p.X + 20;
		int y = p.Y + 20;
//		if( x > A.ctext.Width - ShortList.Width ) {
//			x = A.ctext.Width - ShortList.Width;
//		}
//		if( y > A.ctext.Height - ShortList.Height ) {
//			y = A.ctext.Height - ShortList.Height;
//		}
		ShortList.Location = new Point( x, y );
		
		if( e.KeyChar == '.' ) {
			
			try {
			//添加列表
			AddList();
			} catch {
				
			}
			return;
		}
		if( CharType.isLetterOrNumber( e.KeyChar ) ) {
			return;
		}
		ShortList.Visible = false;
	}
	
	//文本改变事件
	void Text_Changed( object sender, EventArgs e )
	{
		StartDrawLineNumber();
		if( !IgnoreChanged && G.ccode != null ) {
			G.ccode.isChanged = true;
			G.ccode.UserText = U.U.m.GetUserText();
		}
	}
	
	//文本保存事件
	void Text_Saved( object sender, EventArgs e )
	{
		
	}
	
	//文本点击事件
	void Text_MouseDown( object sender, MouseEventArgs e )
	{
		ShortList.Visible = false;
		StartDrawLineNumber();
		
		U.U.m.HideMes();
	}
	
	//鼠标按下事件,处理智能感知功能
	void TextMouseDown( object sender, MouseEventArgs e )
	{
		if( n_GUIcoder.GUIcoder.EventFuncCode != null ) {
			ctext.SelectedText = n_GUIcoder.GUIcoder.EventFuncCode;
			n_GUIcoder.GUIcoder.EventFuncCode = null;
		}
	}
}
}





