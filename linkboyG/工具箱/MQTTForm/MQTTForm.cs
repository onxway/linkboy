﻿/*
 * 由SharpDevelop创建。
 * 用户： cap_gpu
 * 日期: 2017/4/9
 * 时间: 8:03
 * 
 * 要改变这种模板请点击 工具|选项|代码编写|编辑标准头文件
 */
using System;
using System.Drawing;
using System.Windows.Forms;
using System.Net.Sockets; 
using System.Net; 
using System.IO;
using System.Threading;
using n_MQTT;

namespace n_MQTTForm
{
/// <summary>
/// Description of LocateForm.
/// </summary>
public partial class MQTTForm : Form
{
	int Tick;
    
	public MQTTForm()
	{
		//
		// The InitializeComponent() call is required for Windows Forms designer support.
		//
		InitializeComponent();
	}
	
	//运行
	public void Run()
	{
		this.Visible = true;
	}
	
	void LocateFormFormClosing(object sender, FormClosingEventArgs e)
	{
		this.Visible = false;
		e.Cancel = true;
	}
	
	void ButtonJISuanClick(object sender, EventArgs e)
	{
		MQTT.Init();
		MQTT.MessageReceived = MesReceived;
		timer1.Enabled = true;
	}
	
	void MesReceived( string mes )
	{
		richTextBox1.Text += "<" + mes + ">";
	}
	
	void Timer1Tick(object sender, EventArgs e)
	{
		Tick++;
		labelTest.Text = Tick.ToString();
	}
	
	void ButtonLogClick(object sender, EventArgs e)
	{
		//string log = "{\"M\":\"checkin\",\"ID\":\"3575\",\"K\":\"82f44c8d8\"}\r\n";
		//ESP8266.SentString( log );
	}
	
	void CheckBoxAutoSendCheckedChanged(object sender, EventArgs e)
	{
		if( checkBoxAutoSend.Checked ) {
			SendTick = 49;
			timerAutoLogin.Enabled = true;
		}
		else {
			timerAutoLogin.Enabled = false;
		}
	}
	
	int SendTick;
	void TimerAutoLoginTick(object sender, EventArgs e)
	{
		SendTick++;
		if( SendTick >= 50 ) {
			SendTick = 0;
			
			Tick += 10000000;
			
			ButtonLogClick( null, null );
		}
	}
	
	void ButtonCloseClick(object sender, EventArgs e)
	{
		MQTT.Close();
	}
	
	void ButtonClearClick(object sender, EventArgs e)
	{
		richTextBox1.Text = "";
	}
	
	void ButtonSendClick(object sender, EventArgs e)
	{
		MQTT.Send( textBoxTopic.Text, textBoxMes.Text );
	}
}
}

