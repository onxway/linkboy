﻿/*
 * 由SharpDevelop创建。
 * 用户： cap_gpu
 * 日期: 2017/4/9
 * 时间: 8:03
 * 
 * 要改变这种模板请点击 工具|选项|代码编写|编辑标准头文件
 */
namespace n_MQTTForm
{
	partial class MQTTForm
	{
		/// <summary>
		/// Designer variable used to keep track of non-visual components.
		/// </summary>
		private System.ComponentModel.IContainer components = null;
		
		/// <summary>
		/// Disposes resources used by the form.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing) {
				if (components != null) {
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}
		
		/// <summary>
		/// This method is required for Windows Forms designer support.
		/// Do not change the method contents inside the source code editor. The Forms designer might
		/// not be able to load this method if it was changed manually.
		/// </summary>
		private void InitializeComponent()
		{
			this.components = new System.ComponentModel.Container();
			this.label1 = new System.Windows.Forms.Label();
			this.textBoxWeb = new System.Windows.Forms.TextBox();
			this.textBoxPort = new System.Windows.Forms.TextBox();
			this.label4 = new System.Windows.Forms.Label();
			this.buttonJISuan = new System.Windows.Forms.Button();
			this.timer1 = new System.Windows.Forms.Timer(this.components);
			this.richTextBox1 = new System.Windows.Forms.RichTextBox();
			this.buttonLog = new System.Windows.Forms.Button();
			this.labelTest = new System.Windows.Forms.Label();
			this.checkBoxAutoSend = new System.Windows.Forms.CheckBox();
			this.timerAutoLogin = new System.Windows.Forms.Timer(this.components);
			this.buttonClose = new System.Windows.Forms.Button();
			this.panel1 = new System.Windows.Forms.Panel();
			this.textBoxMes = new System.Windows.Forms.TextBox();
			this.textBoxTopic = new System.Windows.Forms.TextBox();
			this.label2 = new System.Windows.Forms.Label();
			this.label3 = new System.Windows.Forms.Label();
			this.buttonSend = new System.Windows.Forms.Button();
			this.buttonClear = new System.Windows.Forms.Button();
			this.panel1.SuspendLayout();
			this.SuspendLayout();
			// 
			// label1
			// 
			this.label1.Font = new System.Drawing.Font("微软雅黑", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
			this.label1.Location = new System.Drawing.Point(11, 11);
			this.label1.Name = "label1";
			this.label1.Size = new System.Drawing.Size(94, 26);
			this.label1.TabIndex = 0;
			this.label1.Text = "服务器";
			// 
			// textBoxWeb
			// 
			this.textBoxWeb.BackColor = System.Drawing.Color.DarkSeaGreen;
			this.textBoxWeb.Font = new System.Drawing.Font("微软雅黑", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
			this.textBoxWeb.Location = new System.Drawing.Point(74, 8);
			this.textBoxWeb.Name = "textBoxWeb";
			this.textBoxWeb.Size = new System.Drawing.Size(149, 29);
			this.textBoxWeb.TabIndex = 2;
			this.textBoxWeb.Text = "39.96.23.157";
			this.textBoxWeb.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
			// 
			// textBoxPort
			// 
			this.textBoxPort.BackColor = System.Drawing.Color.DarkSeaGreen;
			this.textBoxPort.Font = new System.Drawing.Font("微软雅黑", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
			this.textBoxPort.Location = new System.Drawing.Point(295, 8);
			this.textBoxPort.Name = "textBoxPort";
			this.textBoxPort.Size = new System.Drawing.Size(82, 29);
			this.textBoxPort.TabIndex = 6;
			this.textBoxPort.Text = "1883";
			this.textBoxPort.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
			// 
			// label4
			// 
			this.label4.Font = new System.Drawing.Font("微软雅黑", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
			this.label4.Location = new System.Drawing.Point(250, 11);
			this.label4.Name = "label4";
			this.label4.Size = new System.Drawing.Size(94, 26);
			this.label4.TabIndex = 4;
			this.label4.Text = "端口";
			// 
			// buttonJISuan
			// 
			this.buttonJISuan.BackColor = System.Drawing.Color.DarkGoldenrod;
			this.buttonJISuan.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
			this.buttonJISuan.Font = new System.Drawing.Font("微软雅黑", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
			this.buttonJISuan.ForeColor = System.Drawing.Color.WhiteSmoke;
			this.buttonJISuan.Location = new System.Drawing.Point(11, 57);
			this.buttonJISuan.Name = "buttonJISuan";
			this.buttonJISuan.Size = new System.Drawing.Size(183, 41);
			this.buttonJISuan.TabIndex = 8;
			this.buttonJISuan.Text = "连接";
			this.buttonJISuan.UseVisualStyleBackColor = false;
			this.buttonJISuan.Click += new System.EventHandler(this.ButtonJISuanClick);
			// 
			// timer1
			// 
			this.timer1.Enabled = true;
			this.timer1.Tick += new System.EventHandler(this.Timer1Tick);
			// 
			// richTextBox1
			// 
			this.richTextBox1.BackColor = System.Drawing.Color.Gainsboro;
			this.richTextBox1.BorderStyle = System.Windows.Forms.BorderStyle.None;
			this.richTextBox1.Dock = System.Windows.Forms.DockStyle.Fill;
			this.richTextBox1.Location = new System.Drawing.Point(0, 203);
			this.richTextBox1.Name = "richTextBox1";
			this.richTextBox1.Size = new System.Drawing.Size(659, 294);
			this.richTextBox1.TabIndex = 9;
			this.richTextBox1.Text = "";
			// 
			// buttonLog
			// 
			this.buttonLog.BackColor = System.Drawing.Color.DarkGoldenrod;
			this.buttonLog.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
			this.buttonLog.Font = new System.Drawing.Font("微软雅黑", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
			this.buttonLog.ForeColor = System.Drawing.Color.WhiteSmoke;
			this.buttonLog.Location = new System.Drawing.Point(488, 57);
			this.buttonLog.Name = "buttonLog";
			this.buttonLog.Size = new System.Drawing.Size(138, 41);
			this.buttonLog.TabIndex = 10;
			this.buttonLog.Text = "设备登陆";
			this.buttonLog.UseVisualStyleBackColor = false;
			this.buttonLog.Click += new System.EventHandler(this.ButtonLogClick);
			// 
			// labelTest
			// 
			this.labelTest.BackColor = System.Drawing.Color.LightGray;
			this.labelTest.Font = new System.Drawing.Font("微软雅黑", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
			this.labelTest.Location = new System.Drawing.Point(488, 8);
			this.labelTest.Name = "labelTest";
			this.labelTest.Size = new System.Drawing.Size(138, 26);
			this.labelTest.TabIndex = 11;
			// 
			// checkBoxAutoSend
			// 
			this.checkBoxAutoSend.Font = new System.Drawing.Font("微软雅黑", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
			this.checkBoxAutoSend.Location = new System.Drawing.Point(488, 104);
			this.checkBoxAutoSend.Name = "checkBoxAutoSend";
			this.checkBoxAutoSend.Size = new System.Drawing.Size(163, 24);
			this.checkBoxAutoSend.TabIndex = 12;
			this.checkBoxAutoSend.Text = "每隔50秒自动登陆";
			this.checkBoxAutoSend.UseVisualStyleBackColor = true;
			this.checkBoxAutoSend.CheckedChanged += new System.EventHandler(this.CheckBoxAutoSendCheckedChanged);
			// 
			// timerAutoLogin
			// 
			this.timerAutoLogin.Interval = 1000;
			this.timerAutoLogin.Tick += new System.EventHandler(this.TimerAutoLoginTick);
			// 
			// buttonClose
			// 
			this.buttonClose.BackColor = System.Drawing.Color.DarkGoldenrod;
			this.buttonClose.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
			this.buttonClose.Font = new System.Drawing.Font("微软雅黑", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
			this.buttonClose.ForeColor = System.Drawing.Color.WhiteSmoke;
			this.buttonClose.Location = new System.Drawing.Point(206, 57);
			this.buttonClose.Name = "buttonClose";
			this.buttonClose.Size = new System.Drawing.Size(183, 41);
			this.buttonClose.TabIndex = 13;
			this.buttonClose.Text = "断开";
			this.buttonClose.UseVisualStyleBackColor = false;
			this.buttonClose.Click += new System.EventHandler(this.ButtonCloseClick);
			// 
			// panel1
			// 
			this.panel1.Controls.Add(this.textBoxMes);
			this.panel1.Controls.Add(this.textBoxTopic);
			this.panel1.Controls.Add(this.label2);
			this.panel1.Controls.Add(this.label3);
			this.panel1.Controls.Add(this.buttonSend);
			this.panel1.Controls.Add(this.buttonClear);
			this.panel1.Controls.Add(this.textBoxPort);
			this.panel1.Controls.Add(this.textBoxWeb);
			this.panel1.Controls.Add(this.label1);
			this.panel1.Controls.Add(this.buttonClose);
			this.panel1.Controls.Add(this.checkBoxAutoSend);
			this.panel1.Controls.Add(this.label4);
			this.panel1.Controls.Add(this.labelTest);
			this.panel1.Controls.Add(this.buttonLog);
			this.panel1.Controls.Add(this.buttonJISuan);
			this.panel1.Dock = System.Windows.Forms.DockStyle.Top;
			this.panel1.Location = new System.Drawing.Point(0, 0);
			this.panel1.Name = "panel1";
			this.panel1.Size = new System.Drawing.Size(659, 203);
			this.panel1.TabIndex = 14;
			// 
			// textBoxMes
			// 
			this.textBoxMes.BackColor = System.Drawing.Color.DarkSeaGreen;
			this.textBoxMes.Font = new System.Drawing.Font("微软雅黑", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
			this.textBoxMes.Location = new System.Drawing.Point(395, 136);
			this.textBoxMes.Name = "textBoxMes";
			this.textBoxMes.Size = new System.Drawing.Size(231, 29);
			this.textBoxMes.TabIndex = 19;
			this.textBoxMes.Text = "hello";
			this.textBoxMes.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
			// 
			// textBoxTopic
			// 
			this.textBoxTopic.BackColor = System.Drawing.Color.DarkSeaGreen;
			this.textBoxTopic.Font = new System.Drawing.Font("微软雅黑", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
			this.textBoxTopic.Location = new System.Drawing.Point(206, 136);
			this.textBoxTopic.Name = "textBoxTopic";
			this.textBoxTopic.Size = new System.Drawing.Size(121, 29);
			this.textBoxTopic.TabIndex = 17;
			this.textBoxTopic.Text = "linkboy";
			this.textBoxTopic.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
			// 
			// label2
			// 
			this.label2.Font = new System.Drawing.Font("微软雅黑", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
			this.label2.Location = new System.Drawing.Point(160, 139);
			this.label2.Name = "label2";
			this.label2.Size = new System.Drawing.Size(111, 26);
			this.label2.TabIndex = 16;
			this.label2.Text = "主题";
			// 
			// label3
			// 
			this.label3.Font = new System.Drawing.Font("微软雅黑", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
			this.label3.Location = new System.Drawing.Point(350, 139);
			this.label3.Name = "label3";
			this.label3.Size = new System.Drawing.Size(111, 26);
			this.label3.TabIndex = 18;
			this.label3.Text = "消息";
			// 
			// buttonSend
			// 
			this.buttonSend.BackColor = System.Drawing.Color.DarkGoldenrod;
			this.buttonSend.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
			this.buttonSend.Font = new System.Drawing.Font("微软雅黑", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
			this.buttonSend.ForeColor = System.Drawing.Color.WhiteSmoke;
			this.buttonSend.Location = new System.Drawing.Point(12, 129);
			this.buttonSend.Name = "buttonSend";
			this.buttonSend.Size = new System.Drawing.Size(138, 41);
			this.buttonSend.TabIndex = 15;
			this.buttonSend.Text = "发送消息";
			this.buttonSend.UseVisualStyleBackColor = false;
			this.buttonSend.Click += new System.EventHandler(this.ButtonSendClick);
			// 
			// buttonClear
			// 
			this.buttonClear.BackColor = System.Drawing.Color.DarkGoldenrod;
			this.buttonClear.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
			this.buttonClear.Font = new System.Drawing.Font("微软雅黑", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
			this.buttonClear.ForeColor = System.Drawing.Color.WhiteSmoke;
			this.buttonClear.Location = new System.Drawing.Point(395, 57);
			this.buttonClear.Name = "buttonClear";
			this.buttonClear.Size = new System.Drawing.Size(61, 41);
			this.buttonClear.TabIndex = 14;
			this.buttonClear.Text = "清空";
			this.buttonClear.UseVisualStyleBackColor = false;
			this.buttonClear.Click += new System.EventHandler(this.ButtonClearClick);
			// 
			// MQTTForm
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.BackColor = System.Drawing.Color.WhiteSmoke;
			this.ClientSize = new System.Drawing.Size(659, 497);
			this.Controls.Add(this.richTextBox1);
			this.Controls.Add(this.panel1);
			this.Name = "MQTTForm";
			this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
			this.Text = "MQTT通信测试";
			this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.LocateFormFormClosing);
			this.panel1.ResumeLayout(false);
			this.panel1.PerformLayout();
			this.ResumeLayout(false);
		}
		private System.Windows.Forms.Button buttonSend;
		private System.Windows.Forms.Label label3;
		private System.Windows.Forms.Label label2;
		private System.Windows.Forms.TextBox textBoxTopic;
		private System.Windows.Forms.TextBox textBoxMes;
		private System.Windows.Forms.Button buttonClear;
		private System.Windows.Forms.Panel panel1;
		private System.Windows.Forms.Button buttonClose;
		private System.Windows.Forms.Timer timerAutoLogin;
		private System.Windows.Forms.CheckBox checkBoxAutoSend;
		private System.Windows.Forms.Label labelTest;
		private System.Windows.Forms.Button buttonLog;
		private System.Windows.Forms.RichTextBox richTextBox1;
		private System.Windows.Forms.Timer timer1;
		private System.Windows.Forms.Button buttonJISuan;
		private System.Windows.Forms.Label label4;
		private System.Windows.Forms.TextBox textBoxPort;
		private System.Windows.Forms.TextBox textBoxWeb;
		private System.Windows.Forms.Label label1;
	}
}
