﻿
namespace n_GToolForm
{
using System;
using System.Windows.Forms;
using n_Compiler;
using n_ET;
using n_MainSystemData;
using n_ParseNet;
using n_Config;
using n_TargetFile;
using n_AVRdude;
using System.Diagnostics;
using System.Drawing;

/// <summary>
/// Description of WorkForm.
/// </summary>
public partial class ToolForm : Form
{
	bool ignore = false;
	
	//主窗口
	public ToolForm()
	{
		//
		// The InitializeComponent() call is required for Windows Forms designer support.
		//
		InitializeComponent();
		this.MinimumSize = this.Size;
		this.MaximumSize = this.Size;
		//初始化
		//
		// TODO: Add constructor code after the InitializeComponent() call.
		//
	}
	
	//显示
	public void Run()
	{
		ignore = true;
		comboBoxDPI.Text = SystemData.DPI_Val.ToString();
		ignore = false;
		
		this.Show();
		this.Activate();
	}
	
	void SetIDEFormFormClosing(object sender, FormClosingEventArgs e)
	{
		this.Visible = false;
		e.Cancel = true;
	}
	
	void Auto()
	{
		if( checkBox1.Checked ) {
			this.Visible = false;
		}
	}
	
	void Button音乐编辑器Click(object sender, EventArgs e)
	{
		if( G.MusicBox == null ) {
			G.MusicBox = new n_MusicForm.MusicForm( null, false );
		}
		G.MusicBox.Run( null );
		
		Auto();
	}
	
	void Buttonarduino串口下载器Click(object sender, EventArgs e)
	{
		AVRdude.myAVRdudeSetForm.Visible = true;
		
		Auto();
	}
	
	void Button串口助手Click(object sender, EventArgs e)
	{
		if( U.U.UARTBox == null ) {
			 U.U.UARTBox = new n_UARTForm.UARTForm();
			 U.U.UARTBox.SingleMode = false;
		}
		U.U.UARTBox.Run();
		
		Auto();
	}
	
	void Button图片编辑器Click(object sender, EventArgs e)
	{
		if( U.U.ImageEditBox == null ) {
			U.U.ImageEditBox = new n_ImageEditForm.Form1( null );
			U.U.ImageEditBox.SingleMode = false;
		}
		U.U.ImageEditBox.Run( null );
		
		Auto();
	}
	
	void ButtonLocateClick(object sender, EventArgs e)
	{
		if( U.U.LocateBox == null ) {
			U.U.LocateBox = new n_LocateForm.LocateForm();
			//U.U.LocateBox.SingleMode = false;
		}
		U.U.LocateBox.Run();
		
		Auto();
	}
	
	void ButtonCWaveClick(object sender, EventArgs e)
	{
		if( U.U.CWaveBox == null ) {
			U.U.CWaveBox = new n_CWaveForm.CWaveForm();
			U.U.CWaveBox.SingleMode = false;
		}
		U.U.CWaveBox.Run();
		
		Auto();
	}
	
	void ButtonCMusicClick(object sender, EventArgs e)
	{
		if( U.U.CMusicBox == null ) {
			U.U.CMusicBox = new n_CMusicForm.CMusicForm();
			U.U.CMusicBox.SingleMode = false;
		}
		U.U.CMusicBox.Run();
		
		Auto();
	}
	
	void ButtonRegCalClick(object sender, EventArgs e)
	{
		if( U.U.RegCalBox == null ) {
			U.U.RegCalBox = new n_RegCalForm.Form1();
			U.U.RegCalBox.SingleMode = false;
		}
		U.U.RegCalBox.Run();
		
		Auto();
	}
	
	void ButtonSysSetClick(object sender, EventArgs e)
	{
		//if( U.U.SysBox == null ) {
		//	U.U.SysBox = new n_SysForm.SysForm();
		//}
		//U.U.SysBox.Run();
		
		A.SetIDEBox.Run();
		
		Auto();
	}
	
	void ButtonNetToolClick(object sender, EventArgs e)
	{
		if( U.U.NetToolBox == null ) {
			U.U.NetToolBox = new n_NetToolForm.NetToolForm();
		}
		U.U.NetToolBox.Run();
		
		Auto();
	}
	
	void ButtonMQTTClick(object sender, EventArgs e)
	{
		if( U.U.MQTTBox == null ) {
			U.U.MQTTBox = new n_MQTTForm.MQTTForm();
		}
		U.U.MQTTBox.Run();
		
		Auto();
	}
	
	void ButtonCompMesMouseDown(object sender, MouseEventArgs e)
	{
		if( e.Button == MouseButtons.Right ) {
			A.ValueTestBox.Run();
		}
	}
	
	void ButtonMesClick(object sender, EventArgs e)
	{
		if( U.U.NousBox == null ) {
			U.U.NousBox = new n_NousForm.NousForm();
		}
		U.U.NousBox.Run();
		
		Auto();
	}
	
	void ButtonBackupClick(object sender, EventArgs e)
	{
		MessageBox.Show( "本文件夹为用户的备份历史文件夹, 当遇到一些异常情况或者特殊情况, 可以到这个文件夹找到历史版本的程序文件. 目前的备份规则是每次刚打开一个文件, 就会复制到这里备份一下, 后续可能会增加每隔指定的时间(比如10分钟)自动备份." );
		
		//string fileToSelect = n_OS.OS.SystemRoot + "备份历史" + n_OS.OS.PATH_S + "说明.txt";
		//string args = string.Format("/Select, {0}", fileToSelect);
		//System.Diagnostics.Process.Start( new System.Diagnostics.ProcessStartInfo("Explorer.exe", args ) );
		
		string fileToSelect = n_OS.OS.SystemRoot + "备份历史" + n_OS.OS.PATH_S;
		string args = string.Format("/Select, {0}", fileToSelect);
		//System.Diagnostics.Process.Start( new System.Diagnostics.ProcessStartInfo( "Explorer.exe" ) );
		System.Diagnostics.Process.Start( fileToSelect );
	}
	
	void ButtonVOSClick(object sender, EventArgs e)
	{
		Process Proc = new Process();
		Proc.StartInfo.WindowStyle = ProcessWindowStyle.Normal;
		//Proc.StartInfo.UseShellExecute = false;
		//Proc.StartInfo.CreateNoWindow = true;
		//Proc.StartInfo.RedirectStandardOutput = true;
		
		Proc.StartInfo.WorkingDirectory = n_OS.OS.SystemRoot;
		Proc.StartInfo.FileName = "vos-config.exe";
		
		try {
			Proc.Start();
		}
		catch {
			
		}
		Auto();
	}
	
	void Button_meyeClick(object sender, EventArgs e)
	{
		Process Proc = new Process();
		Proc.StartInfo.WindowStyle = ProcessWindowStyle.Normal;
		//Proc.StartInfo.UseShellExecute = false;
		//Proc.StartInfo.CreateNoWindow = true;
		//Proc.StartInfo.RedirectStandardOutput = true;
		
		Proc.StartInfo.WorkingDirectory = n_OS.OS.SystemRoot;
		Proc.StartInfo.FileName = @"m-eye\OpenCamera.exe";
		
		try {
			Proc.Start();
		}
		catch {
			
		}
		Auto();
	}
	
	void ComboBoxDPITextChanged(object sender, EventArgs e)
	{
		if( ignore ) {
			return;
		}
		try {
			int val = int.Parse( comboBoxDPI.Text );
			SystemData.DPI_Val = val;
			SystemData.isChanged = true;
		}
		catch {
			MessageBox.Show( "数据格式不合法：" + comboBoxDPI.Text + ", 应为 1 - 100" );
			comboBoxDPI.Text = "10";
		}
	}
}
}




