﻿
using System;
using System.Drawing;
using System.Windows.Forms;
using System.Net.Sockets; 
using System.Net;
using System.IO;
using System.Threading;
using n_ESP8266;

namespace n_NetToolForm
{
/// <summary>
/// Description of LocateForm.
/// </summary>
public partial class NetToolForm : Form
{
	//连接时间 (100ms)
	int Tick;
	
	//自动登录次数
	int AutoLogTime;
	
	bool Connect;
    
	public NetToolForm()
	{
		//
		// The InitializeComponent() call is required for Windows Forms designer support.
		//
		InitializeComponent();
		
		comboBox.SelectedIndex = 0;
		Connect = false;
	}
	
	//运行
	public void Run()
	{
		this.Visible = true;
	}
	
	//发送字符串带有回车
	void SendStringLn( string mes )
	{
		if( !Connect ) {
			MessageBox.Show( "请先点击按钮连接到服务器!" );
			return;
		}
		
		if( checkBoxNewLine.Checked ) {
			ESP8266.SentString( mes + "\r\n" );
		}
		else {
			ESP8266.SentString( mes );
		}
	}
	
	void RefreshText( RichTextBox rr )
	{
		int sei = rr.SelectionStart;
		rr.SelectAll();
		rr.SelectionColor = Color.Black;
		string mes = rr.Text;
		bool chs = false;
		for( int i = 0; i < mes.Length; ++i ) {
			char c = mes[i];
			if( (int)c <= 127 ) {
				if( !chs ) {
					continue;
				}
				if( (c < 'A' || c > 'Z') && (c < '0' || c > '9') ) {
					chs = false;
					continue;
				}
			}
			else {
				chs = true;
			}
			rr.SelectionStart = i;
			rr.SelectionLength = 1;
			rr.SelectionColor = Color.Red;
		}
		rr.SelectionLength = 0;
		rr.SelectionStart = sei;
	}
	
	//提取命令文本
	string GetCmd( string cmd )
	{
		return cmd.Remove( 0, cmd.IndexOf( ":" ) + 1 );
	}
	
	//====================================================
	
	void LocateFormFormClosing(object sender, FormClosingEventArgs e)
	{
		this.Visible = false;
		e.Cancel = true;
	}
	
	void ButtonConnectClick(object sender, EventArgs e)
	{
		if( !Connect ) {
			ESP8266.OpenSim();
			ESP8266.CreatTCP( textBoxWeb.Text, int.Parse( textBoxPort.Text ) );
			timer1.Enabled = true;
			
			AutoLogTime = 0;
			Tick = 0;
			Connect = true;
			buttonConnect.Text = "断开";
			buttonConnect.BackColor = Color.LightSteelBlue;
		}
		else {
			ESP8266.CloseSim();
			ESP8266.CloseTCP();
			
			timer1.Enabled = false;
			Tick = 0;
			labelTest.Text = "";
			Connect = false;
			buttonConnect.Text = "连接到服务器";
			buttonConnect.BackColor = Color.CornflowerBlue;
		}
	}
	
	void Timer1Tick(object sender, EventArgs e)
	{
		Tick++;
		labelTest.Text = "已连接: " + (Tick / 10).ToString() + "秒  心跳次数: " + AutoLogTime + "次(" + SendTick + ")";
		
		richTextBox1.Text = ESP8266.Result;
	}
	
	void ButtonLogClick(object sender, EventArgs e)
	{
		SendStringLn( richTextBoxLogin.Text );
	}
	
	void CheckBoxAutoSendCheckedChanged(object sender, EventArgs e)
	{
		if( checkBoxAutoSend.Checked ) {
			SendTick = 49;
			timerAutoLogin.Enabled = true;
		}
		else {
			timerAutoLogin.Enabled = false;
		}
	}
	
	int SendTick;
	void TimerAutoLoginTick(object sender, EventArgs e)
	{
		SendTick++;
		if( SendTick >= 50 ) {
			SendTick = 0;
			
			AutoLogTime++;
			
			ButtonLogClick( null, null );
		}
	}
	
	void ButtonClearClick(object sender, EventArgs e)
	{
		ESP8266.Result = "";
		richTextBox1.Text = "";
	}
	
	void ComboBoxSelectedIndexChanged(object sender, EventArgs e)
	{
		if( comboBox.SelectedIndex == 1 ) {
			textBoxWeb.Text = "bemfa.com";
			textBoxPort.Text = "8344";
			richTextBoxLogin.Text = "cmd=0&msg=keep";
			checkBoxNewLine.Checked = true;
			
			panelBF.Visible = true;
			panelBK.Visible = false;
		}
		else if( comboBox.SelectedIndex == 2 ) {
			textBoxWeb.Text = "www.bigiot.net";
			textBoxPort.Text = "8181";
			richTextBoxLogin.Text = "{\"M\":\"checkin\",\"ID\":\"设备ID\",\"K\":\"设备APIKEY\"}";
			checkBoxNewLine.Checked = true;
			
			panelBF.Visible = false;
			panelBK.Visible = true;
		}
		else if( comboBox.SelectedIndex == 3 ) {
			textBoxWeb.Text = "www.test.net";
			textBoxPort.Text = "8181";
			richTextBoxLogin.Text = "{\"M\":\"login\",\"ID\":\"3\",\"KEY\":\"DWZempp4\"}";
			checkBoxNewLine.Checked = false;
			
			panelBF.Visible = false;
			panelBK.Visible = false;
		}
		else {
			textBoxWeb.Text = "";
			textBoxPort.Text = "";
			richTextBoxLogin.Text = "";
			checkBoxNewLine.Checked = true;
			
			panelBF.Visible = false;
			panelBK.Visible = false;
		}
		richTextBoxCMD.Text = "";
	}
	
	
	void ComboBoxCmdBKSelectedIndexChanged(object sender, EventArgs e)
	{
		string cmd = GetCmd( comboBoxCmdBK.Text );
		comboBoxCmdBK.Text = cmd;
		richTextBoxCMD.Text = cmd;
	}
	
	void ComboBoxCmdBFSelectedIndexChanged(object sender, EventArgs e)
	{
		string cmd = GetCmd( comboBoxCmdBF.Text );
		comboBoxCmdBF.Text = cmd;
		richTextBoxCMD.Text = cmd;
	}
	
	
	void ButtonCmdClick(object sender, EventArgs e)
	{
		SendStringLn( richTextBoxCMD.Text );
		if( buttonCmd.BackColor == Color.Peru ) {
			buttonCmd.BackColor = Color.SandyBrown;
		}
		else {
			buttonCmd.BackColor = Color.Peru;
		}
	}
	
	void RichTextBoxLoginTextChanged(object sender, EventArgs e)
	{
		RichTextBox rr = (RichTextBox)sender;
		RefreshText( rr );
	}
	
	void RichTextBoxCMDTextChanged(object sender, EventArgs e)
	{
		RichTextBox rr = (RichTextBox)sender;
		RefreshText( rr );
	}
	
	//==========================================================================
	//巴法云专用
	
	void ButtonTopicClick(object sender, EventArgs e)
	{
		if( textBoxKey.Text == "" ) {
			MessageBox.Show( "请输入私钥" );
			return;
		}
		string cmd = GetCmd( comboBoxCmdBF.Items[0].ToString() );
		cmd = cmd.Replace( "私钥", textBoxKey.Text ).Replace( "主题名称", textBoxBF_Topic.Text );
		richTextBoxCMD.Text = cmd;
		
		ButtonCmdClick( null, null );
	}
	
	void ButtonTMessageClick(object sender, EventArgs e)
	{
		if( textBoxKey.Text == "" ) {
			MessageBox.Show( "请输入私钥" );
			return;
		}
		string cmd = GetCmd( comboBoxCmdBF.Items[1].ToString() );
		cmd = cmd.Replace( "私钥", textBoxKey.Text ).Replace( "主题名称", textBoxBF_Topic.Text ).Replace( "消息内容", textBoxBF_TMessage.Text );
		richTextBoxCMD.Text = cmd;
		
		ButtonCmdClick( null, null );
	}
	
	//==========================================================================
	//贝壳物联专用
	
	void ButtonBK_StatusClick(object sender, EventArgs e)
	{
		string cmd = GetCmd( comboBoxCmdBK.Items[4].ToString() );
		richTextBoxCMD.Text = cmd;
		
		ButtonCmdClick( null, null );
	}
	
	void ButtonBK_TimeClick(object sender, EventArgs e)
	{
		string cmd = GetCmd( comboBoxCmdBK.Items[5].ToString() );
		richTextBoxCMD.Text = cmd;
		
		ButtonCmdClick( null, null );
	}
	
	void ButtonBK_UploadClick(object sender, EventArgs e)
	{
		string cmd = GetCmd( comboBoxCmdBK.Items[1].ToString() );
		cmd = cmd.Replace( "本机设备ID", textBoxBK_ID.Text ).Replace( "数据接口1", textBoxBK_DataC.Text ).Replace( "数值1", textBoxBK_DataV.Text );
		richTextBoxCMD.Text = cmd;
		
		ButtonCmdClick( null, null );
	}
	
	void ButtonBK_LoginClick(object sender, EventArgs e)
	{
		string cmd = GetCmd( comboBoxCmdBK.Items[0].ToString() );
		cmd = cmd.Replace( "本机设备ID", textBoxBK_ID.Text ).Replace( "设备APIKEY", textBoxBK_APIKEY.Text );
		richTextBoxLogin.Text = cmd;
		
		ButtonLogClick( null, null );
	}
	
	void ButtonBK_TargetClick(object sender, EventArgs e)
	{
		string cmd = GetCmd( comboBoxCmdBK.Items[3].ToString() );
		cmd = cmd.Replace( "目标设备ID", textBoxBK_TargetID.Text ).Replace( "信息内容", textBoxBK_TMeeage.Text ).Replace( "可选签名标识", "linkboy" );
		richTextBoxCMD.Text = cmd;
		
		ButtonCmdClick( null, null );
	}
}
}



