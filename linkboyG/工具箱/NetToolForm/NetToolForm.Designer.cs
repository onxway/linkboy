﻿/*
 * 由SharpDevelop创建。
 * 用户： cap_gpu
 * 日期: 2017/4/9
 * 时间: 8:03
 * 
 * 要改变这种模板请点击 工具|选项|代码编写|编辑标准头文件
 */
namespace n_NetToolForm
{
	partial class NetToolForm
	{
		/// <summary>
		/// Designer variable used to keep track of non-visual components.
		/// </summary>
		private System.ComponentModel.IContainer components = null;
		
		/// <summary>
		/// Disposes resources used by the form.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing) {
				if (components != null) {
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}
		
		/// <summary>
		/// This method is required for Windows Forms designer support.
		/// Do not change the method contents inside the source code editor. The Forms designer might
		/// not be able to load this method if it was changed manually.
		/// </summary>
		private void InitializeComponent()
		{
			this.components = new System.ComponentModel.Container();
			this.label1 = new System.Windows.Forms.Label();
			this.textBoxWeb = new System.Windows.Forms.TextBox();
			this.textBoxPort = new System.Windows.Forms.TextBox();
			this.label4 = new System.Windows.Forms.Label();
			this.buttonConnect = new System.Windows.Forms.Button();
			this.timer1 = new System.Windows.Forms.Timer(this.components);
			this.richTextBox1 = new System.Windows.Forms.RichTextBox();
			this.buttonLog = new System.Windows.Forms.Button();
			this.checkBoxAutoSend = new System.Windows.Forms.CheckBox();
			this.timerAutoLogin = new System.Windows.Forms.Timer(this.components);
			this.panel1 = new System.Windows.Forms.Panel();
			this.labelTest = new System.Windows.Forms.Label();
			this.richTextBoxCMD = new System.Windows.Forms.RichTextBox();
			this.buttonCmd = new System.Windows.Forms.Button();
			this.richTextBoxLogin = new System.Windows.Forms.RichTextBox();
			this.comboBox = new System.Windows.Forms.ComboBox();
			this.checkBoxNewLine = new System.Windows.Forms.CheckBox();
			this.label2 = new System.Windows.Forms.Label();
			this.label8 = new System.Windows.Forms.Label();
			this.comboBoxCmdBF = new System.Windows.Forms.ComboBox();
			this.comboBoxCmdBK = new System.Windows.Forms.ComboBox();
			this.buttonClear = new System.Windows.Forms.Button();
			this.panel2 = new System.Windows.Forms.Panel();
			this.panelBK = new System.Windows.Forms.Panel();
			this.buttonBK_Time = new System.Windows.Forms.Button();
			this.buttonBK_Status = new System.Windows.Forms.Button();
			this.textBoxBK_TMeeage = new System.Windows.Forms.TextBox();
			this.label13 = new System.Windows.Forms.Label();
			this.textBoxBK_TargetID = new System.Windows.Forms.TextBox();
			this.label14 = new System.Windows.Forms.Label();
			this.buttonBK_Target = new System.Windows.Forms.Button();
			this.textBox1 = new System.Windows.Forms.TextBox();
			this.buttonBK_Login = new System.Windows.Forms.Button();
			this.textBoxBK_DataV = new System.Windows.Forms.TextBox();
			this.label12 = new System.Windows.Forms.Label();
			this.textBoxBK_DataC = new System.Windows.Forms.TextBox();
			this.label11 = new System.Windows.Forms.Label();
			this.buttonBK_Upload = new System.Windows.Forms.Button();
			this.textBoxBK_APIKEY = new System.Windows.Forms.TextBox();
			this.label9 = new System.Windows.Forms.Label();
			this.textBoxBK_ID = new System.Windows.Forms.TextBox();
			this.label10 = new System.Windows.Forms.Label();
			this.panelBF = new System.Windows.Forms.Panel();
			this.textBox2 = new System.Windows.Forms.TextBox();
			this.textBoxKey = new System.Windows.Forms.TextBox();
			this.label7 = new System.Windows.Forms.Label();
			this.textBoxBF_TMessage = new System.Windows.Forms.TextBox();
			this.buttonBF_TMessage = new System.Windows.Forms.Button();
			this.label6 = new System.Windows.Forms.Label();
			this.textBoxBF_Topic = new System.Windows.Forms.TextBox();
			this.buttonBF_Topic = new System.Windows.Forms.Button();
			this.label3 = new System.Windows.Forms.Label();
			this.label5 = new System.Windows.Forms.Label();
			this.panel1.SuspendLayout();
			this.panel2.SuspendLayout();
			this.panelBK.SuspendLayout();
			this.panelBF.SuspendLayout();
			this.SuspendLayout();
			// 
			// label1
			// 
			this.label1.Font = new System.Drawing.Font("微软雅黑", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
			this.label1.Location = new System.Drawing.Point(163, 79);
			this.label1.Name = "label1";
			this.label1.Size = new System.Drawing.Size(94, 26);
			this.label1.TabIndex = 0;
			this.label1.Text = "网站：";
			// 
			// textBoxWeb
			// 
			this.textBoxWeb.BackColor = System.Drawing.Color.White;
			this.textBoxWeb.Font = new System.Drawing.Font("微软雅黑", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
			this.textBoxWeb.Location = new System.Drawing.Point(216, 76);
			this.textBoxWeb.Name = "textBoxWeb";
			this.textBoxWeb.Size = new System.Drawing.Size(237, 29);
			this.textBoxWeb.TabIndex = 2;
			this.textBoxWeb.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
			// 
			// textBoxPort
			// 
			this.textBoxPort.BackColor = System.Drawing.Color.White;
			this.textBoxPort.Font = new System.Drawing.Font("微软雅黑", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
			this.textBoxPort.Location = new System.Drawing.Point(512, 76);
			this.textBoxPort.Name = "textBoxPort";
			this.textBoxPort.Size = new System.Drawing.Size(84, 29);
			this.textBoxPort.TabIndex = 6;
			this.textBoxPort.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
			// 
			// label4
			// 
			this.label4.Font = new System.Drawing.Font("微软雅黑", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
			this.label4.Location = new System.Drawing.Point(459, 79);
			this.label4.Name = "label4";
			this.label4.Size = new System.Drawing.Size(94, 26);
			this.label4.TabIndex = 4;
			this.label4.Text = "端口：";
			// 
			// buttonConnect
			// 
			this.buttonConnect.BackColor = System.Drawing.Color.CornflowerBlue;
			this.buttonConnect.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
			this.buttonConnect.Font = new System.Drawing.Font("微软雅黑", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
			this.buttonConnect.ForeColor = System.Drawing.Color.WhiteSmoke;
			this.buttonConnect.Location = new System.Drawing.Point(12, 69);
			this.buttonConnect.Name = "buttonConnect";
			this.buttonConnect.Size = new System.Drawing.Size(145, 41);
			this.buttonConnect.TabIndex = 8;
			this.buttonConnect.Text = "连接到服务器";
			this.buttonConnect.UseVisualStyleBackColor = false;
			this.buttonConnect.Click += new System.EventHandler(this.ButtonConnectClick);
			// 
			// timer1
			// 
			this.timer1.Tick += new System.EventHandler(this.Timer1Tick);
			// 
			// richTextBox1
			// 
			this.richTextBox1.BackColor = System.Drawing.Color.WhiteSmoke;
			this.richTextBox1.BorderStyle = System.Windows.Forms.BorderStyle.None;
			this.richTextBox1.Dock = System.Windows.Forms.DockStyle.Fill;
			this.richTextBox1.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
			this.richTextBox1.Location = new System.Drawing.Point(54, 0);
			this.richTextBox1.Name = "richTextBox1";
			this.richTextBox1.Size = new System.Drawing.Size(871, 185);
			this.richTextBox1.TabIndex = 9;
			this.richTextBox1.Text = "";
			// 
			// buttonLog
			// 
			this.buttonLog.BackColor = System.Drawing.Color.Peru;
			this.buttonLog.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
			this.buttonLog.Font = new System.Drawing.Font("微软雅黑", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
			this.buttonLog.ForeColor = System.Drawing.Color.WhiteSmoke;
			this.buttonLog.Location = new System.Drawing.Point(12, 116);
			this.buttonLog.Name = "buttonLog";
			this.buttonLog.Size = new System.Drawing.Size(145, 41);
			this.buttonLog.TabIndex = 10;
			this.buttonLog.Text = "登陆/心跳命令";
			this.buttonLog.UseVisualStyleBackColor = false;
			this.buttonLog.Click += new System.EventHandler(this.ButtonLogClick);
			// 
			// checkBoxAutoSend
			// 
			this.checkBoxAutoSend.Font = new System.Drawing.Font("微软雅黑", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
			this.checkBoxAutoSend.Location = new System.Drawing.Point(691, 111);
			this.checkBoxAutoSend.Name = "checkBoxAutoSend";
			this.checkBoxAutoSend.Size = new System.Drawing.Size(171, 52);
			this.checkBoxAutoSend.TabIndex = 12;
			this.checkBoxAutoSend.Text = "每隔50秒自动发送心跳命令";
			this.checkBoxAutoSend.UseVisualStyleBackColor = true;
			this.checkBoxAutoSend.CheckedChanged += new System.EventHandler(this.CheckBoxAutoSendCheckedChanged);
			// 
			// timerAutoLogin
			// 
			this.timerAutoLogin.Interval = 1000;
			this.timerAutoLogin.Tick += new System.EventHandler(this.TimerAutoLoginTick);
			// 
			// panel1
			// 
			this.panel1.Controls.Add(this.labelTest);
			this.panel1.Controls.Add(this.richTextBoxCMD);
			this.panel1.Controls.Add(this.buttonConnect);
			this.panel1.Controls.Add(this.buttonCmd);
			this.panel1.Controls.Add(this.richTextBoxLogin);
			this.panel1.Controls.Add(this.textBoxPort);
			this.panel1.Controls.Add(this.buttonLog);
			this.panel1.Controls.Add(this.comboBox);
			this.panel1.Controls.Add(this.textBoxWeb);
			this.panel1.Controls.Add(this.checkBoxNewLine);
			this.panel1.Controls.Add(this.checkBoxAutoSend);
			this.panel1.Controls.Add(this.label2);
			this.panel1.Controls.Add(this.label1);
			this.panel1.Controls.Add(this.label4);
			this.panel1.Dock = System.Windows.Forms.DockStyle.Top;
			this.panel1.Location = new System.Drawing.Point(0, 0);
			this.panel1.Name = "panel1";
			this.panel1.Size = new System.Drawing.Size(925, 218);
			this.panel1.TabIndex = 14;
			// 
			// labelTest
			// 
			this.labelTest.BackColor = System.Drawing.Color.Transparent;
			this.labelTest.Font = new System.Drawing.Font("微软雅黑", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
			this.labelTest.ForeColor = System.Drawing.Color.Gray;
			this.labelTest.Location = new System.Drawing.Point(474, 18);
			this.labelTest.Name = "labelTest";
			this.labelTest.Size = new System.Drawing.Size(388, 29);
			this.labelTest.TabIndex = 11;
			this.labelTest.Text = "连接时间: 300秒 心跳次数: 40次";
			this.labelTest.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
			// 
			// richTextBoxCMD
			// 
			this.richTextBoxCMD.Font = new System.Drawing.Font("微软雅黑", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
			this.richTextBoxCMD.Location = new System.Drawing.Point(163, 163);
			this.richTextBoxCMD.Multiline = false;
			this.richTextBoxCMD.Name = "richTextBoxCMD";
			this.richTextBoxCMD.Size = new System.Drawing.Size(699, 41);
			this.richTextBoxCMD.TabIndex = 25;
			this.richTextBoxCMD.Text = "";
			this.richTextBoxCMD.TextChanged += new System.EventHandler(this.RichTextBoxCMDTextChanged);
			// 
			// buttonCmd
			// 
			this.buttonCmd.BackColor = System.Drawing.Color.Peru;
			this.buttonCmd.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
			this.buttonCmd.Font = new System.Drawing.Font("微软雅黑", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
			this.buttonCmd.ForeColor = System.Drawing.Color.WhiteSmoke;
			this.buttonCmd.Location = new System.Drawing.Point(12, 163);
			this.buttonCmd.Name = "buttonCmd";
			this.buttonCmd.Size = new System.Drawing.Size(145, 41);
			this.buttonCmd.TabIndex = 19;
			this.buttonCmd.Text = "发送自定义命令";
			this.buttonCmd.UseVisualStyleBackColor = false;
			this.buttonCmd.Click += new System.EventHandler(this.ButtonCmdClick);
			// 
			// richTextBoxLogin
			// 
			this.richTextBoxLogin.Font = new System.Drawing.Font("微软雅黑", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
			this.richTextBoxLogin.Location = new System.Drawing.Point(163, 116);
			this.richTextBoxLogin.Multiline = false;
			this.richTextBoxLogin.Name = "richTextBoxLogin";
			this.richTextBoxLogin.Size = new System.Drawing.Size(522, 41);
			this.richTextBoxLogin.TabIndex = 24;
			this.richTextBoxLogin.Text = "";
			this.richTextBoxLogin.TextChanged += new System.EventHandler(this.RichTextBoxLoginTextChanged);
			// 
			// comboBox
			// 
			this.comboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
			this.comboBox.Font = new System.Drawing.Font("微软雅黑", 12F);
			this.comboBox.FormattingEnabled = true;
			this.comboBox.Items.AddRange(new object[] {
									"默认",
									"巴法云",
									"贝壳物联",
									"济南市中区物联"});
			this.comboBox.Location = new System.Drawing.Point(73, 18);
			this.comboBox.Name = "comboBox";
			this.comboBox.Size = new System.Drawing.Size(204, 29);
			this.comboBox.TabIndex = 16;
			this.comboBox.SelectedIndexChanged += new System.EventHandler(this.ComboBoxSelectedIndexChanged);
			// 
			// checkBoxNewLine
			// 
			this.checkBoxNewLine.Font = new System.Drawing.Font("微软雅黑", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
			this.checkBoxNewLine.Location = new System.Drawing.Point(290, 20);
			this.checkBoxNewLine.Name = "checkBoxNewLine";
			this.checkBoxNewLine.Size = new System.Drawing.Size(185, 24);
			this.checkBoxNewLine.TabIndex = 22;
			this.checkBoxNewLine.Text = "命令后带有回车\\r\\n";
			this.checkBoxNewLine.UseVisualStyleBackColor = true;
			// 
			// label2
			// 
			this.label2.Font = new System.Drawing.Font("微软雅黑", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
			this.label2.Location = new System.Drawing.Point(12, 21);
			this.label2.Name = "label2";
			this.label2.Size = new System.Drawing.Size(94, 26);
			this.label2.TabIndex = 17;
			this.label2.Text = "模板：";
			// 
			// label8
			// 
			this.label8.Font = new System.Drawing.Font("微软雅黑", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
			this.label8.Location = new System.Drawing.Point(768, 59);
			this.label8.Name = "label8";
			this.label8.Size = new System.Drawing.Size(145, 26);
			this.label8.TabIndex = 25;
			this.label8.Text = "快速命令输入：";
			this.label8.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
			// 
			// comboBoxCmdBF
			// 
			this.comboBoxCmdBF.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
			this.comboBoxCmdBF.Font = new System.Drawing.Font("微软雅黑", 12F);
			this.comboBoxCmdBF.FormattingEnabled = true;
			this.comboBoxCmdBF.Items.AddRange(new object[] {
									"订阅主题:cmd=1&uid=私钥&topic=主题名称",
									"发布信息:cmd=2&uid=私钥&topic=主题名称&msg=消息内容"});
			this.comboBoxCmdBF.Location = new System.Drawing.Point(775, 108);
			this.comboBoxCmdBF.Name = "comboBoxCmdBF";
			this.comboBoxCmdBF.Size = new System.Drawing.Size(139, 29);
			this.comboBoxCmdBF.TabIndex = 23;
			this.comboBoxCmdBF.SelectedIndexChanged += new System.EventHandler(this.ComboBoxCmdBFSelectedIndexChanged);
			// 
			// comboBoxCmdBK
			// 
			this.comboBoxCmdBK.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
			this.comboBoxCmdBK.Font = new System.Drawing.Font("微软雅黑", 12F);
			this.comboBoxCmdBK.FormattingEnabled = true;
			this.comboBoxCmdBK.Items.AddRange(new object[] {
									"设备登陆:{\"M\":\"checkin\",\"ID\":\"本机设备ID\",\"K\":\"设备APIKEY\"}",
									"上传数据:{\"M\":\"update\",\"ID\":\"本机设备ID\",\"V\":{\"数据接口1\":\"数值1\"}}",
									"上传多个数据:{\"M\":\"update\",\"ID\":\"本机设备ID\",\"V\":{\"数据接口1\":\"数值1\",\"数据接口2\":\"数值2\"}}",
									"设备通信:{\"M\":\"say\",\"ID\":\"目标设备ID\",\"C\":\"信息内容\",\"SIGN\":\"可选签名标识\"}",
									"查询状态:{\"M\":\"status\"}",
									"查询服务器时间:{\"M\":\"time\",\"F\":\"Y-m-d\"}"});
			this.comboBoxCmdBK.Location = new System.Drawing.Point(774, 90);
			this.comboBoxCmdBK.Name = "comboBoxCmdBK";
			this.comboBoxCmdBK.Size = new System.Drawing.Size(139, 29);
			this.comboBoxCmdBK.TabIndex = 21;
			this.comboBoxCmdBK.SelectedIndexChanged += new System.EventHandler(this.ComboBoxCmdBKSelectedIndexChanged);
			// 
			// buttonClear
			// 
			this.buttonClear.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(230)))), ((int)(((byte)(230)))), ((int)(((byte)(230)))));
			this.buttonClear.Dock = System.Windows.Forms.DockStyle.Left;
			this.buttonClear.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
			this.buttonClear.Font = new System.Drawing.Font("微软雅黑", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
			this.buttonClear.ForeColor = System.Drawing.Color.Black;
			this.buttonClear.Location = new System.Drawing.Point(0, 0);
			this.buttonClear.Name = "buttonClear";
			this.buttonClear.Size = new System.Drawing.Size(54, 185);
			this.buttonClear.TabIndex = 14;
			this.buttonClear.Text = "清空";
			this.buttonClear.UseVisualStyleBackColor = false;
			this.buttonClear.Click += new System.EventHandler(this.ButtonClearClick);
			// 
			// panel2
			// 
			this.panel2.Controls.Add(this.richTextBox1);
			this.panel2.Controls.Add(this.buttonClear);
			this.panel2.Dock = System.Windows.Forms.DockStyle.Fill;
			this.panel2.Location = new System.Drawing.Point(0, 532);
			this.panel2.Name = "panel2";
			this.panel2.Size = new System.Drawing.Size(925, 185);
			this.panel2.TabIndex = 15;
			// 
			// panelBK
			// 
			this.panelBK.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
			this.panelBK.Controls.Add(this.buttonBK_Time);
			this.panelBK.Controls.Add(this.buttonBK_Status);
			this.panelBK.Controls.Add(this.textBoxBK_TMeeage);
			this.panelBK.Controls.Add(this.label13);
			this.panelBK.Controls.Add(this.textBoxBK_TargetID);
			this.panelBK.Controls.Add(this.label14);
			this.panelBK.Controls.Add(this.buttonBK_Target);
			this.panelBK.Controls.Add(this.textBox1);
			this.panelBK.Controls.Add(this.buttonBK_Login);
			this.panelBK.Controls.Add(this.textBoxBK_DataV);
			this.panelBK.Controls.Add(this.label12);
			this.panelBK.Controls.Add(this.textBoxBK_DataC);
			this.panelBK.Controls.Add(this.label11);
			this.panelBK.Controls.Add(this.buttonBK_Upload);
			this.panelBK.Controls.Add(this.textBoxBK_APIKEY);
			this.panelBK.Controls.Add(this.label9);
			this.panelBK.Controls.Add(this.textBoxBK_ID);
			this.panelBK.Controls.Add(this.label10);
			this.panelBK.Controls.Add(this.comboBoxCmdBK);
			this.panelBK.Controls.Add(this.label8);
			this.panelBK.Dock = System.Windows.Forms.DockStyle.Top;
			this.panelBK.Location = new System.Drawing.Point(0, 218);
			this.panelBK.Name = "panelBK";
			this.panelBK.Size = new System.Drawing.Size(925, 150);
			this.panelBK.TabIndex = 26;
			// 
			// buttonBK_Time
			// 
			this.buttonBK_Time.BackColor = System.Drawing.Color.Silver;
			this.buttonBK_Time.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
			this.buttonBK_Time.Font = new System.Drawing.Font("微软雅黑", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
			this.buttonBK_Time.ForeColor = System.Drawing.Color.Black;
			this.buttonBK_Time.Location = new System.Drawing.Point(573, 98);
			this.buttonBK_Time.Name = "buttonBK_Time";
			this.buttonBK_Time.Size = new System.Drawing.Size(132, 41);
			this.buttonBK_Time.TabIndex = 34;
			this.buttonBK_Time.Text = "服务器时间";
			this.buttonBK_Time.UseVisualStyleBackColor = false;
			this.buttonBK_Time.Click += new System.EventHandler(this.ButtonBK_TimeClick);
			// 
			// buttonBK_Status
			// 
			this.buttonBK_Status.BackColor = System.Drawing.Color.Silver;
			this.buttonBK_Status.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
			this.buttonBK_Status.Font = new System.Drawing.Font("微软雅黑", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
			this.buttonBK_Status.ForeColor = System.Drawing.Color.Black;
			this.buttonBK_Status.Location = new System.Drawing.Point(573, 50);
			this.buttonBK_Status.Name = "buttonBK_Status";
			this.buttonBK_Status.Size = new System.Drawing.Size(132, 41);
			this.buttonBK_Status.TabIndex = 33;
			this.buttonBK_Status.Text = "查询设备状态";
			this.buttonBK_Status.UseVisualStyleBackColor = false;
			this.buttonBK_Status.Click += new System.EventHandler(this.ButtonBK_StatusClick);
			// 
			// textBoxBK_TMeeage
			// 
			this.textBoxBK_TMeeage.BackColor = System.Drawing.Color.White;
			this.textBoxBK_TMeeage.Font = new System.Drawing.Font("微软雅黑", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
			this.textBoxBK_TMeeage.Location = new System.Drawing.Point(474, 57);
			this.textBoxBK_TMeeage.Name = "textBoxBK_TMeeage";
			this.textBoxBK_TMeeage.Size = new System.Drawing.Size(75, 29);
			this.textBoxBK_TMeeage.TabIndex = 48;
			this.textBoxBK_TMeeage.Text = "Hello!";
			this.textBoxBK_TMeeage.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
			// 
			// label13
			// 
			this.label13.Font = new System.Drawing.Font("微软雅黑", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
			this.label13.Location = new System.Drawing.Point(383, 60);
			this.label13.Name = "label13";
			this.label13.Size = new System.Drawing.Size(112, 26);
			this.label13.TabIndex = 47;
			this.label13.Text = "发送信息：";
			// 
			// textBoxBK_TargetID
			// 
			this.textBoxBK_TargetID.BackColor = System.Drawing.Color.White;
			this.textBoxBK_TargetID.Font = new System.Drawing.Font("微软雅黑", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
			this.textBoxBK_TargetID.Location = new System.Drawing.Point(474, 14);
			this.textBoxBK_TargetID.Name = "textBoxBK_TargetID";
			this.textBoxBK_TargetID.Size = new System.Drawing.Size(75, 29);
			this.textBoxBK_TargetID.TabIndex = 46;
			this.textBoxBK_TargetID.Text = "D1234";
			this.textBoxBK_TargetID.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
			// 
			// label14
			// 
			this.label14.Font = new System.Drawing.Font("微软雅黑", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
			this.label14.Location = new System.Drawing.Point(383, 17);
			this.label14.Name = "label14";
			this.label14.Size = new System.Drawing.Size(112, 26);
			this.label14.TabIndex = 45;
			this.label14.Text = "目标设备：";
			// 
			// buttonBK_Target
			// 
			this.buttonBK_Target.BackColor = System.Drawing.Color.Silver;
			this.buttonBK_Target.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
			this.buttonBK_Target.Font = new System.Drawing.Font("微软雅黑", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
			this.buttonBK_Target.ForeColor = System.Drawing.Color.Black;
			this.buttonBK_Target.Location = new System.Drawing.Point(383, 98);
			this.buttonBK_Target.Name = "buttonBK_Target";
			this.buttonBK_Target.Size = new System.Drawing.Size(166, 41);
			this.buttonBK_Target.TabIndex = 44;
			this.buttonBK_Target.Text = "目标设备通信";
			this.buttonBK_Target.UseVisualStyleBackColor = false;
			this.buttonBK_Target.Click += new System.EventHandler(this.ButtonBK_TargetClick);
			// 
			// textBox1
			// 
			this.textBox1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
			this.textBox1.BorderStyle = System.Windows.Forms.BorderStyle.None;
			this.textBox1.Font = new System.Drawing.Font("微软雅黑", 12F);
			this.textBox1.Location = new System.Drawing.Point(698, 17);
			this.textBox1.Name = "textBox1";
			this.textBox1.Size = new System.Drawing.Size(215, 22);
			this.textBox1.TabIndex = 43;
			this.textBox1.Text = "https://www.bigiot.net/";
			this.textBox1.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
			// 
			// buttonBK_Login
			// 
			this.buttonBK_Login.BackColor = System.Drawing.Color.Silver;
			this.buttonBK_Login.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
			this.buttonBK_Login.Font = new System.Drawing.Font("微软雅黑", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
			this.buttonBK_Login.ForeColor = System.Drawing.Color.Black;
			this.buttonBK_Login.Location = new System.Drawing.Point(18, 98);
			this.buttonBK_Login.Name = "buttonBK_Login";
			this.buttonBK_Login.Size = new System.Drawing.Size(187, 41);
			this.buttonBK_Login.TabIndex = 42;
			this.buttonBK_Login.Text = "登陆命令";
			this.buttonBK_Login.UseVisualStyleBackColor = false;
			this.buttonBK_Login.Click += new System.EventHandler(this.ButtonBK_LoginClick);
			// 
			// textBoxBK_DataV
			// 
			this.textBoxBK_DataV.BackColor = System.Drawing.Color.White;
			this.textBoxBK_DataV.Font = new System.Drawing.Font("微软雅黑", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
			this.textBoxBK_DataV.Location = new System.Drawing.Point(305, 57);
			this.textBoxBK_DataV.Name = "textBoxBK_DataV";
			this.textBoxBK_DataV.Size = new System.Drawing.Size(72, 29);
			this.textBoxBK_DataV.TabIndex = 41;
			this.textBoxBK_DataV.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
			// 
			// label12
			// 
			this.label12.Font = new System.Drawing.Font("微软雅黑", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
			this.label12.Location = new System.Drawing.Point(211, 60);
			this.label12.Name = "label12";
			this.label12.Size = new System.Drawing.Size(112, 26);
			this.label12.TabIndex = 40;
			this.label12.Text = "数据内容：";
			// 
			// textBoxBK_DataC
			// 
			this.textBoxBK_DataC.BackColor = System.Drawing.Color.White;
			this.textBoxBK_DataC.Font = new System.Drawing.Font("微软雅黑", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
			this.textBoxBK_DataC.Location = new System.Drawing.Point(305, 14);
			this.textBoxBK_DataC.Name = "textBoxBK_DataC";
			this.textBoxBK_DataC.Size = new System.Drawing.Size(72, 29);
			this.textBoxBK_DataC.TabIndex = 39;
			this.textBoxBK_DataC.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
			// 
			// label11
			// 
			this.label11.Font = new System.Drawing.Font("微软雅黑", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
			this.label11.Location = new System.Drawing.Point(211, 17);
			this.label11.Name = "label11";
			this.label11.Size = new System.Drawing.Size(112, 26);
			this.label11.TabIndex = 38;
			this.label11.Text = "数据接口：";
			// 
			// buttonBK_Upload
			// 
			this.buttonBK_Upload.BackColor = System.Drawing.Color.Silver;
			this.buttonBK_Upload.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
			this.buttonBK_Upload.Font = new System.Drawing.Font("微软雅黑", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
			this.buttonBK_Upload.ForeColor = System.Drawing.Color.Black;
			this.buttonBK_Upload.Location = new System.Drawing.Point(211, 98);
			this.buttonBK_Upload.Name = "buttonBK_Upload";
			this.buttonBK_Upload.Size = new System.Drawing.Size(166, 41);
			this.buttonBK_Upload.TabIndex = 37;
			this.buttonBK_Upload.Text = "上传数据";
			this.buttonBK_Upload.UseVisualStyleBackColor = false;
			this.buttonBK_Upload.Click += new System.EventHandler(this.ButtonBK_UploadClick);
			// 
			// textBoxBK_APIKEY
			// 
			this.textBoxBK_APIKEY.BackColor = System.Drawing.Color.White;
			this.textBoxBK_APIKEY.Font = new System.Drawing.Font("微软雅黑", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
			this.textBoxBK_APIKEY.Location = new System.Drawing.Point(89, 57);
			this.textBoxBK_APIKEY.Name = "textBoxBK_APIKEY";
			this.textBoxBK_APIKEY.Size = new System.Drawing.Size(116, 29);
			this.textBoxBK_APIKEY.TabIndex = 36;
			this.textBoxBK_APIKEY.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
			// 
			// label9
			// 
			this.label9.Font = new System.Drawing.Font("微软雅黑", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
			this.label9.Location = new System.Drawing.Point(12, 60);
			this.label9.Name = "label9";
			this.label9.Size = new System.Drawing.Size(112, 26);
			this.label9.TabIndex = 35;
			this.label9.Text = "APIKEY：";
			// 
			// textBoxBK_ID
			// 
			this.textBoxBK_ID.BackColor = System.Drawing.Color.White;
			this.textBoxBK_ID.Font = new System.Drawing.Font("微软雅黑", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
			this.textBoxBK_ID.Location = new System.Drawing.Point(89, 14);
			this.textBoxBK_ID.Name = "textBoxBK_ID";
			this.textBoxBK_ID.Size = new System.Drawing.Size(116, 29);
			this.textBoxBK_ID.TabIndex = 34;
			this.textBoxBK_ID.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
			// 
			// label10
			// 
			this.label10.Font = new System.Drawing.Font("微软雅黑", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
			this.label10.Location = new System.Drawing.Point(12, 17);
			this.label10.Name = "label10";
			this.label10.Size = new System.Drawing.Size(112, 26);
			this.label10.TabIndex = 33;
			this.label10.Text = "设备ID：";
			// 
			// panelBF
			// 
			this.panelBF.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
			this.panelBF.Controls.Add(this.textBox2);
			this.panelBF.Controls.Add(this.textBoxKey);
			this.panelBF.Controls.Add(this.label7);
			this.panelBF.Controls.Add(this.textBoxBF_TMessage);
			this.panelBF.Controls.Add(this.buttonBF_TMessage);
			this.panelBF.Controls.Add(this.label6);
			this.panelBF.Controls.Add(this.textBoxBF_Topic);
			this.panelBF.Controls.Add(this.buttonBF_Topic);
			this.panelBF.Controls.Add(this.label3);
			this.panelBF.Controls.Add(this.comboBoxCmdBF);
			this.panelBF.Controls.Add(this.label5);
			this.panelBF.Dock = System.Windows.Forms.DockStyle.Top;
			this.panelBF.Location = new System.Drawing.Point(0, 368);
			this.panelBF.Name = "panelBF";
			this.panelBF.Size = new System.Drawing.Size(925, 164);
			this.panelBF.TabIndex = 27;
			// 
			// textBox2
			// 
			this.textBox2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
			this.textBox2.BorderStyle = System.Windows.Forms.BorderStyle.None;
			this.textBox2.Font = new System.Drawing.Font("微软雅黑", 12F);
			this.textBox2.Location = new System.Drawing.Point(698, 13);
			this.textBox2.Name = "textBox2";
			this.textBox2.Size = new System.Drawing.Size(216, 22);
			this.textBox2.TabIndex = 44;
			this.textBox2.Text = "https://cloud.bemfa.com/";
			this.textBox2.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
			// 
			// textBoxKey
			// 
			this.textBoxKey.BackColor = System.Drawing.Color.White;
			this.textBoxKey.Font = new System.Drawing.Font("微软雅黑", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
			this.textBoxKey.Location = new System.Drawing.Point(73, 10);
			this.textBoxKey.Name = "textBoxKey";
			this.textBoxKey.Size = new System.Drawing.Size(472, 29);
			this.textBoxKey.TabIndex = 32;
			this.textBoxKey.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
			// 
			// label7
			// 
			this.label7.Font = new System.Drawing.Font("微软雅黑", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
			this.label7.Location = new System.Drawing.Point(18, 13);
			this.label7.Name = "label7";
			this.label7.Size = new System.Drawing.Size(113, 26);
			this.label7.TabIndex = 31;
			this.label7.Text = "私钥：";
			// 
			// textBoxBF_TMessage
			// 
			this.textBoxBF_TMessage.BackColor = System.Drawing.Color.White;
			this.textBoxBF_TMessage.Font = new System.Drawing.Font("微软雅黑", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
			this.textBoxBF_TMessage.Location = new System.Drawing.Point(73, 115);
			this.textBoxBF_TMessage.Name = "textBoxBF_TMessage";
			this.textBoxBF_TMessage.Size = new System.Drawing.Size(144, 29);
			this.textBoxBF_TMessage.TabIndex = 30;
			this.textBoxBF_TMessage.Text = "Hello!";
			this.textBoxBF_TMessage.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
			// 
			// buttonBF_TMessage
			// 
			this.buttonBF_TMessage.BackColor = System.Drawing.Color.Silver;
			this.buttonBF_TMessage.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
			this.buttonBF_TMessage.Font = new System.Drawing.Font("微软雅黑", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
			this.buttonBF_TMessage.ForeColor = System.Drawing.Color.Black;
			this.buttonBF_TMessage.Location = new System.Drawing.Point(239, 108);
			this.buttonBF_TMessage.Name = "buttonBF_TMessage";
			this.buttonBF_TMessage.Size = new System.Drawing.Size(132, 41);
			this.buttonBF_TMessage.TabIndex = 29;
			this.buttonBF_TMessage.Text = "向主题发消息";
			this.buttonBF_TMessage.UseVisualStyleBackColor = false;
			this.buttonBF_TMessage.Click += new System.EventHandler(this.ButtonTMessageClick);
			// 
			// label6
			// 
			this.label6.Font = new System.Drawing.Font("微软雅黑", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
			this.label6.Location = new System.Drawing.Point(12, 118);
			this.label6.Name = "label6";
			this.label6.Size = new System.Drawing.Size(94, 26);
			this.label6.TabIndex = 28;
			this.label6.Text = "消息：";
			// 
			// textBoxBF_Topic
			// 
			this.textBoxBF_Topic.BackColor = System.Drawing.Color.White;
			this.textBoxBF_Topic.Font = new System.Drawing.Font("微软雅黑", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
			this.textBoxBF_Topic.Location = new System.Drawing.Point(73, 68);
			this.textBoxBF_Topic.Name = "textBoxBF_Topic";
			this.textBoxBF_Topic.Size = new System.Drawing.Size(144, 29);
			this.textBoxBF_Topic.TabIndex = 27;
			this.textBoxBF_Topic.Text = "topic";
			this.textBoxBF_Topic.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
			// 
			// buttonBF_Topic
			// 
			this.buttonBF_Topic.BackColor = System.Drawing.Color.Silver;
			this.buttonBF_Topic.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
			this.buttonBF_Topic.Font = new System.Drawing.Font("微软雅黑", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
			this.buttonBF_Topic.ForeColor = System.Drawing.Color.Black;
			this.buttonBF_Topic.Location = new System.Drawing.Point(239, 61);
			this.buttonBF_Topic.Name = "buttonBF_Topic";
			this.buttonBF_Topic.Size = new System.Drawing.Size(132, 41);
			this.buttonBF_Topic.TabIndex = 26;
			this.buttonBF_Topic.Text = "订阅主题";
			this.buttonBF_Topic.UseVisualStyleBackColor = false;
			this.buttonBF_Topic.Click += new System.EventHandler(this.ButtonTopicClick);
			// 
			// label3
			// 
			this.label3.Font = new System.Drawing.Font("微软雅黑", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
			this.label3.Location = new System.Drawing.Point(775, 77);
			this.label3.Name = "label3";
			this.label3.Size = new System.Drawing.Size(145, 26);
			this.label3.TabIndex = 26;
			this.label3.Text = "快速命令输入：";
			this.label3.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// label5
			// 
			this.label5.Font = new System.Drawing.Font("微软雅黑", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
			this.label5.Location = new System.Drawing.Point(12, 71);
			this.label5.Name = "label5";
			this.label5.Size = new System.Drawing.Size(94, 26);
			this.label5.TabIndex = 26;
			this.label5.Text = "主题：";
			// 
			// NetToolForm
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.BackColor = System.Drawing.Color.WhiteSmoke;
			this.ClientSize = new System.Drawing.Size(925, 717);
			this.Controls.Add(this.panel2);
			this.Controls.Add(this.panelBF);
			this.Controls.Add(this.panelBK);
			this.Controls.Add(this.panel1);
			this.Name = "NetToolForm";
			this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
			this.Text = "网络通信测试";
			this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.LocateFormFormClosing);
			this.panel1.ResumeLayout(false);
			this.panel1.PerformLayout();
			this.panel2.ResumeLayout(false);
			this.panelBK.ResumeLayout(false);
			this.panelBK.PerformLayout();
			this.panelBF.ResumeLayout(false);
			this.panelBF.PerformLayout();
			this.ResumeLayout(false);
		}
		private System.Windows.Forms.Button buttonBK_Target;
		private System.Windows.Forms.Label label14;
		private System.Windows.Forms.TextBox textBoxBK_TargetID;
		private System.Windows.Forms.Label label13;
		private System.Windows.Forms.TextBox textBoxBK_TMeeage;
		private System.Windows.Forms.TextBox textBox2;
		private System.Windows.Forms.TextBox textBox1;
		private System.Windows.Forms.Button buttonBK_Login;
		private System.Windows.Forms.Label label12;
		private System.Windows.Forms.TextBox textBoxBK_DataV;
		private System.Windows.Forms.Button buttonBK_Upload;
		private System.Windows.Forms.Label label11;
		private System.Windows.Forms.TextBox textBoxBK_DataC;
		private System.Windows.Forms.Label label10;
		private System.Windows.Forms.TextBox textBoxBK_ID;
		private System.Windows.Forms.Label label9;
		private System.Windows.Forms.TextBox textBoxBK_APIKEY;
		private System.Windows.Forms.Button buttonBK_Status;
		private System.Windows.Forms.Button buttonBK_Time;
		private System.Windows.Forms.Label label7;
		private System.Windows.Forms.TextBox textBoxKey;
		private System.Windows.Forms.Label label5;
		private System.Windows.Forms.Label label6;
		private System.Windows.Forms.Button buttonBF_TMessage;
		private System.Windows.Forms.TextBox textBoxBF_TMessage;
		private System.Windows.Forms.Button buttonBF_Topic;
		private System.Windows.Forms.TextBox textBoxBF_Topic;
		private System.Windows.Forms.Label label3;
		private System.Windows.Forms.Panel panelBF;
		private System.Windows.Forms.Panel panelBK;
		private System.Windows.Forms.Label label8;
		private System.Windows.Forms.Panel panel2;
		private System.Windows.Forms.RichTextBox richTextBoxCMD;
		private System.Windows.Forms.RichTextBox richTextBoxLogin;
		private System.Windows.Forms.ComboBox comboBoxCmdBF;
		private System.Windows.Forms.CheckBox checkBoxNewLine;
		private System.Windows.Forms.Button buttonCmd;
		private System.Windows.Forms.ComboBox comboBoxCmdBK;
		private System.Windows.Forms.Label label2;
		private System.Windows.Forms.ComboBox comboBox;
		private System.Windows.Forms.Button buttonClear;
		private System.Windows.Forms.Panel panel1;
		private System.Windows.Forms.Timer timerAutoLogin;
		private System.Windows.Forms.CheckBox checkBoxAutoSend;
		private System.Windows.Forms.Label labelTest;
		private System.Windows.Forms.Button buttonLog;
		private System.Windows.Forms.RichTextBox richTextBox1;
		private System.Windows.Forms.Timer timer1;
		private System.Windows.Forms.Button buttonConnect;
		private System.Windows.Forms.Label label4;
		private System.Windows.Forms.TextBox textBoxPort;
		private System.Windows.Forms.TextBox textBoxWeb;
		private System.Windows.Forms.Label label1;
	}
}
