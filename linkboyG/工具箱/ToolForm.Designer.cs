﻿/*
 * 由 SharpDevelop 创建。
 * 用户: bbb_2
 * 日期: 2009-11-17
 * 时间: 11:15
 * 
 * 如果你想要更改该模板，那么请使用“工具 | 选项 | 编辑 | 编辑标准标题”。
 */
namespace n_GToolForm
{
	partial class ToolForm
	{
		/// <summary>
		/// Designer variable used to keep track of non-visual components.
		/// </summary>
		private System.ComponentModel.IContainer components = null;
		
		/// <summary>
		/// Disposes resources used by the form.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing) {
				if (components != null) {
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}
		
		/// <summary>
		/// This method is required for Windows Forms designer support.
		/// Do not change the method contents inside the source code editor. The Forms designer might
		/// not be able to load this method if it was changed manually.
		/// </summary>
		private void InitializeComponent()
		{
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ToolForm));
			this.button串口助手 = new System.Windows.Forms.Button();
			this.buttonLocate = new System.Windows.Forms.Button();
			this.checkBox1 = new System.Windows.Forms.CheckBox();
			this.buttonCWave = new System.Windows.Forms.Button();
			this.buttonCMusic = new System.Windows.Forms.Button();
			this.groupBox1 = new System.Windows.Forms.GroupBox();
			this.buttonSysSet = new System.Windows.Forms.Button();
			this.buttonNetTool = new System.Windows.Forms.Button();
			this.buttonMQTT = new System.Windows.Forms.Button();
			this.buttonMes = new System.Windows.Forms.Button();
			this.buttonBackup = new System.Windows.Forms.Button();
			this.buttonVOS = new System.Windows.Forms.Button();
			this.button_meye = new System.Windows.Forms.Button();
			this.groupBox2 = new System.Windows.Forms.GroupBox();
			this.groupBox3 = new System.Windows.Forms.GroupBox();
			this.label2 = new System.Windows.Forms.Label();
			this.comboBoxDPI = new System.Windows.Forms.ComboBox();
			this.groupBox1.SuspendLayout();
			this.groupBox2.SuspendLayout();
			this.groupBox3.SuspendLayout();
			this.SuspendLayout();
			// 
			// button串口助手
			// 
			this.button串口助手.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(241)))), ((int)(((byte)(241)))), ((int)(((byte)(241)))));
			resources.ApplyResources(this.button串口助手, "button串口助手");
			this.button串口助手.ForeColor = System.Drawing.Color.Black;
			this.button串口助手.Name = "button串口助手";
			this.button串口助手.UseVisualStyleBackColor = false;
			this.button串口助手.Click += new System.EventHandler(this.Button串口助手Click);
			// 
			// buttonLocate
			// 
			this.buttonLocate.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(241)))), ((int)(((byte)(241)))), ((int)(((byte)(241)))));
			resources.ApplyResources(this.buttonLocate, "buttonLocate");
			this.buttonLocate.ForeColor = System.Drawing.Color.Black;
			this.buttonLocate.Name = "buttonLocate";
			this.buttonLocate.UseVisualStyleBackColor = false;
			this.buttonLocate.Click += new System.EventHandler(this.ButtonLocateClick);
			// 
			// checkBox1
			// 
			this.checkBox1.Checked = true;
			this.checkBox1.CheckState = System.Windows.Forms.CheckState.Checked;
			resources.ApplyResources(this.checkBox1, "checkBox1");
			this.checkBox1.ForeColor = System.Drawing.Color.DarkGray;
			this.checkBox1.Name = "checkBox1";
			this.checkBox1.UseVisualStyleBackColor = true;
			// 
			// buttonCWave
			// 
			this.buttonCWave.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(241)))), ((int)(((byte)(241)))), ((int)(((byte)(241)))));
			resources.ApplyResources(this.buttonCWave, "buttonCWave");
			this.buttonCWave.ForeColor = System.Drawing.Color.Black;
			this.buttonCWave.Name = "buttonCWave";
			this.buttonCWave.UseVisualStyleBackColor = false;
			this.buttonCWave.Click += new System.EventHandler(this.ButtonCWaveClick);
			// 
			// buttonCMusic
			// 
			this.buttonCMusic.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(241)))), ((int)(((byte)(241)))), ((int)(((byte)(241)))));
			resources.ApplyResources(this.buttonCMusic, "buttonCMusic");
			this.buttonCMusic.ForeColor = System.Drawing.Color.Black;
			this.buttonCMusic.Name = "buttonCMusic";
			this.buttonCMusic.UseVisualStyleBackColor = false;
			this.buttonCMusic.Click += new System.EventHandler(this.ButtonCMusicClick);
			// 
			// groupBox1
			// 
			this.groupBox1.Controls.Add(this.buttonCWave);
			this.groupBox1.Controls.Add(this.buttonCMusic);
			this.groupBox1.Controls.Add(this.button串口助手);
			resources.ApplyResources(this.groupBox1, "groupBox1");
			this.groupBox1.ForeColor = System.Drawing.Color.Gray;
			this.groupBox1.Name = "groupBox1";
			this.groupBox1.TabStop = false;
			// 
			// buttonSysSet
			// 
			this.buttonSysSet.BackColor = System.Drawing.Color.White;
			resources.ApplyResources(this.buttonSysSet, "buttonSysSet");
			this.buttonSysSet.ForeColor = System.Drawing.Color.Gainsboro;
			this.buttonSysSet.Name = "buttonSysSet";
			this.buttonSysSet.UseVisualStyleBackColor = false;
			this.buttonSysSet.Click += new System.EventHandler(this.ButtonSysSetClick);
			// 
			// buttonNetTool
			// 
			this.buttonNetTool.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(241)))), ((int)(((byte)(241)))), ((int)(((byte)(241)))));
			resources.ApplyResources(this.buttonNetTool, "buttonNetTool");
			this.buttonNetTool.ForeColor = System.Drawing.Color.Black;
			this.buttonNetTool.Name = "buttonNetTool";
			this.buttonNetTool.UseVisualStyleBackColor = false;
			this.buttonNetTool.Click += new System.EventHandler(this.ButtonNetToolClick);
			// 
			// buttonMQTT
			// 
			this.buttonMQTT.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(241)))), ((int)(((byte)(241)))), ((int)(((byte)(241)))));
			resources.ApplyResources(this.buttonMQTT, "buttonMQTT");
			this.buttonMQTT.ForeColor = System.Drawing.Color.Black;
			this.buttonMQTT.Name = "buttonMQTT";
			this.buttonMQTT.UseVisualStyleBackColor = false;
			this.buttonMQTT.Click += new System.EventHandler(this.ButtonMQTTClick);
			// 
			// buttonMes
			// 
			this.buttonMes.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(241)))), ((int)(((byte)(241)))), ((int)(((byte)(241)))));
			resources.ApplyResources(this.buttonMes, "buttonMes");
			this.buttonMes.ForeColor = System.Drawing.Color.Black;
			this.buttonMes.Name = "buttonMes";
			this.buttonMes.UseVisualStyleBackColor = false;
			this.buttonMes.Click += new System.EventHandler(this.ButtonMesClick);
			// 
			// buttonBackup
			// 
			this.buttonBackup.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(241)))), ((int)(((byte)(241)))), ((int)(((byte)(241)))));
			resources.ApplyResources(this.buttonBackup, "buttonBackup");
			this.buttonBackup.ForeColor = System.Drawing.Color.Black;
			this.buttonBackup.Name = "buttonBackup";
			this.buttonBackup.UseVisualStyleBackColor = false;
			this.buttonBackup.Click += new System.EventHandler(this.ButtonBackupClick);
			// 
			// buttonVOS
			// 
			this.buttonVOS.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(241)))), ((int)(((byte)(241)))), ((int)(((byte)(241)))));
			resources.ApplyResources(this.buttonVOS, "buttonVOS");
			this.buttonVOS.ForeColor = System.Drawing.Color.Black;
			this.buttonVOS.Name = "buttonVOS";
			this.buttonVOS.UseVisualStyleBackColor = false;
			this.buttonVOS.Click += new System.EventHandler(this.ButtonVOSClick);
			// 
			// button_meye
			// 
			this.button_meye.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(241)))), ((int)(((byte)(241)))), ((int)(((byte)(241)))));
			resources.ApplyResources(this.button_meye, "button_meye");
			this.button_meye.ForeColor = System.Drawing.Color.Black;
			this.button_meye.Name = "button_meye";
			this.button_meye.UseVisualStyleBackColor = false;
			this.button_meye.Click += new System.EventHandler(this.Button_meyeClick);
			// 
			// groupBox2
			// 
			this.groupBox2.Controls.Add(this.buttonNetTool);
			this.groupBox2.Controls.Add(this.buttonLocate);
			this.groupBox2.Controls.Add(this.buttonBackup);
			this.groupBox2.Controls.Add(this.buttonMQTT);
			this.groupBox2.Controls.Add(this.buttonMes);
			resources.ApplyResources(this.groupBox2, "groupBox2");
			this.groupBox2.ForeColor = System.Drawing.Color.Gray;
			this.groupBox2.Name = "groupBox2";
			this.groupBox2.TabStop = false;
			// 
			// groupBox3
			// 
			this.groupBox3.Controls.Add(this.label2);
			this.groupBox3.Controls.Add(this.comboBoxDPI);
			this.groupBox3.Controls.Add(this.buttonVOS);
			this.groupBox3.Controls.Add(this.button_meye);
			resources.ApplyResources(this.groupBox3, "groupBox3");
			this.groupBox3.ForeColor = System.Drawing.Color.Gray;
			this.groupBox3.Name = "groupBox3";
			this.groupBox3.TabStop = false;
			// 
			// label2
			// 
			this.label2.ForeColor = System.Drawing.Color.Tomato;
			resources.ApplyResources(this.label2, "label2");
			this.label2.Name = "label2";
			// 
			// comboBoxDPI
			// 
			this.comboBoxDPI.FormattingEnabled = true;
			this.comboBoxDPI.Items.AddRange(new object[] {
									resources.GetString("comboBoxDPI.Items"),
									resources.GetString("comboBoxDPI.Items1"),
									resources.GetString("comboBoxDPI.Items2"),
									resources.GetString("comboBoxDPI.Items3"),
									resources.GetString("comboBoxDPI.Items4"),
									resources.GetString("comboBoxDPI.Items5"),
									resources.GetString("comboBoxDPI.Items6"),
									resources.GetString("comboBoxDPI.Items7"),
									resources.GetString("comboBoxDPI.Items8"),
									resources.GetString("comboBoxDPI.Items9"),
									resources.GetString("comboBoxDPI.Items10"),
									resources.GetString("comboBoxDPI.Items11"),
									resources.GetString("comboBoxDPI.Items12"),
									resources.GetString("comboBoxDPI.Items13"),
									resources.GetString("comboBoxDPI.Items14")});
			resources.ApplyResources(this.comboBoxDPI, "comboBoxDPI");
			this.comboBoxDPI.Name = "comboBoxDPI";
			this.comboBoxDPI.TextChanged += new System.EventHandler(this.ComboBoxDPITextChanged);
			// 
			// ToolForm
			// 
			resources.ApplyResources(this, "$this");
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.BackColor = System.Drawing.Color.White;
			this.Controls.Add(this.buttonSysSet);
			this.Controls.Add(this.groupBox3);
			this.Controls.Add(this.groupBox2);
			this.Controls.Add(this.groupBox1);
			this.Controls.Add(this.checkBox1);
			this.ForeColor = System.Drawing.Color.Black;
			this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
			this.MaximizeBox = false;
			this.MinimizeBox = false;
			this.Name = "ToolForm";
			this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.SetIDEFormFormClosing);
			this.groupBox1.ResumeLayout(false);
			this.groupBox2.ResumeLayout(false);
			this.groupBox3.ResumeLayout(false);
			this.ResumeLayout(false);
		}
		private System.Windows.Forms.ComboBox comboBoxDPI;
		private System.Windows.Forms.Label label2;
		private System.Windows.Forms.GroupBox groupBox3;
		private System.Windows.Forms.GroupBox groupBox2;
		private System.Windows.Forms.Button button_meye;
		private System.Windows.Forms.Button buttonVOS;
		private System.Windows.Forms.Button buttonBackup;
		private System.Windows.Forms.Button buttonMes;
		private System.Windows.Forms.Button buttonMQTT;
		private System.Windows.Forms.Button buttonNetTool;
		private System.Windows.Forms.Button buttonSysSet;
		private System.Windows.Forms.GroupBox groupBox1;
		private System.Windows.Forms.Button buttonCMusic;
		private System.Windows.Forms.Button buttonCWave;
		private System.Windows.Forms.CheckBox checkBox1;
		private System.Windows.Forms.Button buttonLocate;
		private System.Windows.Forms.Button button串口助手;
	}
}
