﻿
namespace n_ModuleConfigForm
{
using System;
using System.IO;
using System.Net;
using System.Text;
using System.Windows.Forms;
using System.Drawing;
using n_GUIcoder;
//using n_MainSystemData;
//using n_FileType;
using System.Data;
//using n_Include;
//using n_Config;
using n_OS;

/// <summary>
/// Description of WorkForm.
/// </summary>
public partial class ModuleConfigForm : Form
{
	string CurrentFile;
	string Folder;
	
	bool vvisIgnore;
	public bool isIgnoreTextChanged {
		get { return vvisIgnore; }
		set {
			vvisIgnore = value;
			if( vvisIgnore ) {
				//MessageBox.Show( "忽略修改" );
			}
		}
	}
	bool vvisChanged;
	public bool isChanged {
		get { return vvisChanged; }
		set {
			vvisChanged = value;
			if( vvisChanged ) {
				this.labelChanged.Text = "已修改";
			}
			else {
				this.labelChanged.Text = "";
			}
		}
	}
	
	int LanguageIndex;
	int MemberIndex;
	
	DataTable LanguageTable;
	ModuleMessage[] ModuleMessageList;
	
	string CopyString;
	
	//主窗口
	public ModuleConfigForm( string vStartFile )
	{
		InitializeComponent();
		
		LanguageIndex = 0;
		MemberIndex = 0;
		isIgnoreTextChanged = false;
		isChanged = false;
		CopyString = "";
		
		CurrentFile = vStartFile;
	}
	
	//加载文件
	public void LoadFile( string FilePath )
	{
		CurrentFile = FilePath;
		
		Folder = FilePath.Remove( FilePath.LastIndexOf( OS.PATH_S ) + 1 );
		string FunctionName = FilePath.Remove( 0, FilePath.LastIndexOf( OS.PATH_S ) + 1 );
		FunctionName = FunctionName.Remove( FunctionName.IndexOf( "." ) );
		
		this.textBox功能名称.Text = FunctionName;
		this.textBox内核文件.Text = "driver" + OS.PATH_S + FunctionName + ".txt";
		this.label驱动文件路径.Text = Folder + this.textBox内核文件.Text;
		this.comboBoxSimulateType.SelectedIndex = 0;
		
		this.FilePathlabel.Text = FilePath;
		
		this.CodeBox.Text = VIO.OpenTextFileGB2312( FilePath );
		
		int ElementIndex = 0;
		string[][] ElementList = null;
		InitTable();
		
		string[][] ConfigList = GUIcoder.GetConfigFromText( this.CodeBox.Text );
		for( int i = 0; i < ConfigList.Length; ++i ) {
			switch( ConfigList[ i ][ 0 ] ) {
				case "[组件名称]":
					ConfigList[ i ][ 0 ] = "module";
					ConfigList[ i ][ 1 ] = FunctionName;
					ElementList[ ElementIndex ] = ConfigList[ i ];
					++ElementIndex;
					break;
				case "[组件文件]":
					this.textBox组件文件.Text = ConfigList[ i ][ 1 ];
					break;
				case "[模块型号]":
					this.textBoxChipType.Text = ConfigList[ i ][ 1 ];
					break;
				case "[资源占用]":
					ConfigList[ i ][ 0 ] = "";
					this.textBoxResource.Text = string.Join( ",", ConfigList[ i ] ).Trim( ',' );
					break;
				case "[py参数]":
					this.textBoxPy.Text = ConfigList[ i ][ 1 ];
					break;
				case "[控件属性]":
					SetUIModuleEXCheck( ConfigList[ i ][ 1 ] );
					break;
				case "[公共引用]":
					ConfigList[ i ][ 0 ] = "";
					this.textBoxCommonRef.Text = string.Join( " ", ConfigList[ i ] ).Trim( ' ' );
					break;
				case "[实物图片]":
					ConfigList[ i ][ 0 ] = "";
					this.textBoxImage.Text = string.Join( " ", ConfigList[ i ] ).Trim( ' ' );
					break;
				case "[仿真类型]":
					this.comboBoxSimulateType.SelectedIndex = int.Parse( ConfigList[ i ][ 1 ] );
					break;
				case "[语言列表]":
					ModuleMessageList = GUIcoder.LoadExtendFile( Folder + FunctionName, ConfigList[ i ] );
					SetExtendFile( ModuleMessageList );
					SetLanguageList( ConfigList[ i ] );
					ElementList = new string[ ConfigList.Length - 1 ][];
					break;
				case "[元素子项]":
					ConfigList[ i ][ 0 ] = "";
					string[] TempConfig = string.Join( "`", ConfigList[ i ] ).TrimStart( '`' ).Replace( '+', ' ' ).Split( '`' );
					ElementList[ ElementIndex ] = TempConfig;
					++ElementIndex;
					break;
				default:
					MessageBox.Show( "<ModuleConfig> 未知的配置项: " + ConfigList[ i ][ 0 ] );
					break;
			}
		}
		try {
			this.richTextBox驱动程序.Text = VIO.OpenTextFileGB2312( Folder + this.textBox内核文件.Text );
		}
		catch {
			MessageBox.Show( "未找到驱动程序文件:" + Folder + this.textBox内核文件.Text );
		}
		string[] Interface = GetInterfaceDesc( this.richTextBox驱动程序.Text );
		FillTable( Interface, ElementList );
		
		isChanged = false;
	}
	
	//设置控件属性是否有效
	void SetUIModuleEXCheck( string ex )
	{
		this.checkBox文字.Checked = false;
		this.checkBox字体.Checked = false;
		this.checkBox文字颜色.Checked = false;
		this.checkBox边框颜色.Checked = false;
		this.checkBox背景颜色.Checked = false;
		
		string[] List = ex.Split( ',' );
		for( int i = 0; i < List.Length; ++i ) {
			
			switch( List[ i ] ) {
				case "文字": this.checkBox文字.Checked = true; break;
				case "字体": this.checkBox字体.Checked = true; break;
				case "文字颜色": this.checkBox文字颜色.Checked = true; break;
				case "边框颜色": this.checkBox边框颜色.Checked = true; break;
				case "背景颜色": this.checkBox背景颜色.Checked = true; break;
				default: this.textBoxExtendButton.Text += List[i].Replace( '+', ' ' ) + "\n"; break;
			}
		}
	}
	
	//设置说明文件
	public void SetExtendFile( ModuleMessage[] Mes )
	{
		this.richTextBoxNote.Text = "";
		for( int i = 0; i < Mes.Length; ++i ) {
			if( Mes[ i ] == null ) {
				continue;
			}
			this.richTextBoxNote.Text += "\n//------------------------------------\n";
			this.richTextBoxNote.Text += Mes[ i ].ModuleMes + "\n";
			for( int j = 0; j < Mes[ i ].MemberMesList.Length; ++j ) {
				this.richTextBoxNote.Text += Mes[ i ].MemberMesList[ j ].UserName + ":\n";
				this.richTextBoxNote.Text += Mes[ i ].MemberMesList[ j ].Mes + "\n";
			}
		}
	}
	
	//从一段程序文本中提取接口说明,每个接口包括类型和名称
	string[] GetInterfaceDesc( string Text )
	{
		string Result = null;
		string[] TextLines = Text.Split( '\n' );
		for( int Line = 0; Line < TextLines.Length; ++Line ) {
			string CurentLine = TextLines[ Line ].Replace( '\t', ' ' );
			CurentLine = CurentLine.Trim( ' ' );
			if( CurentLine.EndsWith( "," ) || CurentLine.EndsWith( ";" ) ) {
				CurentLine = CurentLine.Remove( CurentLine.Length - 1 );
			}
			string[] Cut = CurentLine.Split( ' ' );
			if( Cut[ 0 ] != "//[i]" ) {
				continue;
			}
			if( Cut.Length < 3 ) {
				MessageBox.Show( "格式不对: " + CurentLine );
				continue;
			}
			//解析属性设置
			if( Cut[ 1 ] == InterfaceType.Unit ||
			    Cut[ 1 ] == InterfaceType.Vtype ||
			    Cut[ 1 ] == InterfaceType.Event ||
			    Cut[ 1 ] == InterfaceType.Struct ||
			    Cut[ 1 ] == InterfaceType.LinkUnit ) {
				Result += Cut[ 1 ] + " " + Cut[ 2 ] + "\n";
			}
			else if( Cut[ 1 ].StartsWith( InterfaceType.Function_ ) ) {
				for( int i = 3; i < Cut.Length; ++i ) {
					Cut[ 1 ] += "_" + Cut[ i ];
				}
				Result += Cut[ 1 ] + " " + Cut[ 2 ] + "\n";
			}
			else if( Cut[ 1 ].StartsWith( InterfaceType.Var_ ) ||
			         Cut[ 1 ].StartsWith( InterfaceType.Const_ ) ||
					 Cut[ 1 ].StartsWith( InterfaceType.Interface_ ) ||
			         Cut[ 1 ].StartsWith( InterfaceType.LinkInterface_ ) ||
			         Cut[ 1 ].StartsWith( InterfaceType.LinkVar_ ) ||
			         Cut[ 1 ].StartsWith( InterfaceType.LinkConst_ ) ||
			         Cut[ 1 ].StartsWith( InterfaceType.LinkSysConst_ ) ||
			         Cut[ 1 ].StartsWith( InterfaceType.LinkSysVar_ ) ) {
				Result += Cut[ 1 ] + " " + Cut[ 2 ] + "\n";
			}
			else if( Cut[ 1 ] == InterfaceType.Split ) {
				Result += Cut[ 1 ] + " " + Cut[ 2 ] + "\n";
				continue;
			}
			else {
				MessageBox.Show( "未知的类型: " + Cut[ 1 ] +  " (" + CurentLine + ")" );
			}
		}
		if( Result == null ) {
			return null;
		}
		return Result.TrimEnd( '\n' ).Split( '\n' );
	}
	
	//填充成员列表
	void FillTable( string[] DriverMes, string[][] ElementList )
	{
		//遍历添加表格
		if( ElementList != null ) {
			LanguageTable.Rows.Add( ElementList[ 0 ] );
		}
		else {
			LanguageTable.Rows.Add( new string[] { "module", "default" } );
		}
		if( DriverMes == null ) {
			return;
		}
		for( int i = 0; i < DriverMes.Length; ++i ) {
			string[] Cut = DriverMes[ i ].Split( ' ' );
			string[] AddRow = Cut;
			if( ElementList != null ) {
				for( int j = 0; j < ElementList.Length; ++j ) {
					if( ElementList[ j ] == null ) {
						continue;
					}
					if( Cut[ 1 ] == ElementList[ j ][ 1 ] ) {
						AddRow = ElementList[ j ];
						AddRow[ 0 ] = Cut[ 0 ];
						break;
					}
				}
			}
			LanguageTable.Rows.Add( AddRow );
		}
		LanguageTable.RowChanged += new DataRowChangeEventHandler( anyTextChanged );
	}
	
	//初始化表格,单独提取出来主要是为了从一个新文本文件保存为B文件
	void InitTable()
	{
		LanguageTable = new DataTable();
		LanguageTable.Clear();
		LanguageTable.Columns.Add( "type" );
		LanguageTable.Columns.Add( "inner" );
		
		this.dataGridView1.DataSource = LanguageTable;
		
		this.dataGridView1.Columns[ 0 ].Width = 250;
		this.dataGridView1.Columns[ 0 ].ReadOnly = true;
		this.dataGridView1.Columns[ 0 ].SortMode = DataGridViewColumnSortMode.NotSortable;
		DataGridViewCellStyle ds0 = new DataGridViewCellStyle();
		ds0.BackColor = Color.SlateGray;
		ds0.ForeColor = Color.White;
		this.dataGridView1.Columns[ 0 ].DefaultCellStyle = ds0;
		
		this.dataGridView1.Columns[ 1 ].Width = 150;
		this.dataGridView1.Columns[ 1 ].ReadOnly = true;
		this.dataGridView1.Columns[ 1 ].SortMode = DataGridViewColumnSortMode.NotSortable;
		DataGridViewCellStyle ds1 = new DataGridViewCellStyle();
		ds1.BackColor = Color.LightGray;
		this.dataGridView1.Columns[ 1 ].DefaultCellStyle = ds1;
	}
	
	//设置语言列表
	void SetLanguageList( string[] LangList )
	{
		for( int i = 1; i < LangList.Length; ++i ) {
			if( LangList[ i ] == "" ) {
				continue;
			}
			LanguageTable.Columns.Add( LangList[ i ] );
			this.dataGridView1.Columns[ i + 1 ].SortMode = DataGridViewColumnSortMode.NotSortable;
			this.dataGridView1.Columns[ i + 1 ].Width = 250;
		}
	}
	
	//获取语言列表
	string GetLanguageString()
	{
		string Result = null;
		for( int i = 2; i < LanguageTable.Columns.Count; ++i ) {
			Result += " " + LanguageTable.Columns[ i ].ColumnName;
		}
		if( Result != null ) {
			return Result + ",";
		}
		return null;
	}
	
	//获取组件名称
	string GetModuleName()
	{
		string Result = "";
		for( int j = 1; j < LanguageTable.Rows[ 0 ].ItemArray.Length; ++j ) {
			Result += " " + LanguageTable.Rows[ 0 ].ItemArray[ j ];
		}
		return Result + ",";
	}
	
	//获取元素子项
	string GetLanguageElement()
	{
		string Result = null;
		for( int i = 1; i < LanguageTable.Rows.Count; ++i ) {
			Result += "//[元素子项]";
			for( int j = 0; j < LanguageTable.Rows[ i ].ItemArray.Length; ++j ) {
				string Mem = LanguageTable.Rows[ i ].ItemArray[ j ].ToString();
				Mem = Mem.Replace( ' ', '+' );
				Result += " " + Mem;
			}
			Result += ",\n";
		}
		return Result;
	}
	
	//保存语言文件组
	void SaveLanguageFiles()
	{
		Directory.Delete( Folder + this.textBox功能名称.Text, true );
		//if( !Directory.Exists( Folder + this.textBox功能名称.Text ) ) {
			Directory.CreateDirectory( Folder + this.textBox功能名称.Text );
		//}
		
		//--------------------------------------------------------
		//生成并保存外层组件封装代码
		for( int i = 2; i < LanguageTable.Columns.Count; ++i ) {
			string LanguageName = LanguageTable.Columns[ i ].ColumnName;
			string LanguageText = "";
			
			for( int j = 1; j < LanguageTable.Rows.Count; ++j ) {
				string Type = LanguageTable.Rows[ j ].ItemArray[ 0 ].ToString();
				string BaseName = LanguageTable.Rows[ j ].ItemArray[ 1 ].ToString();
				string NewName = LanguageTable.Rows[ j ].ItemArray[ i ].ToString();
				
				if( Type == InterfaceType.Unit ) {
					LanguageText += "public link unit " + NewName + " {} = driver." + BaseName + ";\n";
				}
				else if( Type.StartsWith( InterfaceType.Function_ ) ) {
					string[] NameList = NewName.TrimEnd( ' ' ).Split( ' ' );
					string FuncName = "";
					string VarList = "(";
					for( int ni = 0; ni < NameList.Length; ++ni ) {
						if( NameList[ ni ] == "" ) {
							continue;
						}
						if( NameList[ ni ] == "#" ) {
							VarList += "bit n" + ni + ",";
							if( LanguageName != n_Language.Language.Mod_c ) {
								FuncName += "_";
							}
						}
						else {
							FuncName += NameList[ ni ];
						}
					}
					VarList = VarList.TrimEnd( ',' );
					LanguageText += "public link void " + FuncName + VarList + "){} = driver." + BaseName + ";\n";
				}
				else if( Type == InterfaceType.Event ) {
					//LanguageText += "LINK T OS_" + BaseName + " = driver." + BaseName + ";\n";
				}
				else if( Type.StartsWith( InterfaceType.Const_ ) ) {
					LanguageText += "public link bit " + NewName + " = " + "driver." + BaseName + ";\n";
				}
				else if( Type.StartsWith( InterfaceType.Var_ ) ) {
					LanguageText += "public link bit " + NewName + " = " + "driver." + BaseName + ";\n";
				}
				else if( Type == InterfaceType.Struct ) {
					LanguageText += "public link struct " + NewName + " {} = " + "driver." + BaseName + ";\n";
				}
				else if( Type == InterfaceType.Vtype ) {
					LanguageText += "public link memory " + NewName + " {} = " + "driver." + BaseName + ";\n";
				}
				else if( Type == InterfaceType.LinkUnit ) {
					string[] TypeMes = Type.Split( '_' );
					LanguageText += "public link unit " + NewName + " {}\n";
					LanguageText += "driver." + BaseName + " = " + NewName + ";\n";
				}
				else if( Type.StartsWith( InterfaceType.LinkConst_ ) ) {
					string[] TypeMes = Type.Split( '_' );
					LanguageText += "public const " + TypeMes[ 1 ] + " " + NewName + " = 0;\n";
					LanguageText += "driver." + BaseName + " = " + NewName + ";\n";
				}
				else if( Type.StartsWith( InterfaceType.LinkVar_ ) ) {
					string[] TypeMes = Type.Split( '_' );
					if( TypeMes[ 1 ] == GType.g_color ) {
						TypeMes[ 1 ] = GType.g_int32;
					}
					LanguageText += "public link " + TypeMes[ 1 ] + " " + NewName + " = " + "driver." + BaseName + ";\n";
				}
				else if( Type.StartsWith( InterfaceType.LinkSysVar_ ) ) {
					string[] TypeMes = Type.Split( '_' );
					LanguageText += "public link " + TypeMes[ 1 ] + " " + NewName + " = " + "driver." + BaseName + ";\n";
				}
				else if( Type.StartsWith( InterfaceType.LinkSysConst_ ) ) {
					string[] TypeMes = Type.Split( '_' );
					LanguageText += "public const " + TypeMes[ 1 ] + " " + NewName + " = 0;\n";
					LanguageText += "driver." + BaseName + " = " + NewName + ";\n";
				}
				else if( Type.StartsWith( InterfaceType.Interface_ ) ) {
					LanguageText += "public link unit " + NewName + " {} = driver." + BaseName + ";\n";
				}
				else if( Type.StartsWith( InterfaceType.LinkInterface_ ) ) {
					LanguageText += "public link unit " + NewName + " {}\n";
					LanguageText += "driver." + BaseName + " = " + NewName + ";\n";
				}
				else if( Type.StartsWith( InterfaceType.Split ) ) {
					//LanguageText += "public link unit " + NewName + " {}\n";
					//LanguageText += "driver." + BaseName + " = " + NewName + ";\n";
				}
				else {
					MessageBox.Show( "<SaveLanguageFiles> 暂不支持转换此类型: " + Type );
				}
			}
			//保存语言文件
			string LanguageFile = Folder + this.textBox功能名称.Text + OS.PATH_S + LanguageName + ".txt";
			
			if( LanguageText == null) {
				MessageBox.Show( "语言名称不能为空!" );
			}
			
			VIO.SaveTextFileGB2312( LanguageFile, LanguageText );
		}
		//--------------------------------------------------------
		//保存组件信息描述文件
		for( int i = 2; i < LanguageTable.Columns.Count; ++i ) {
			string LanguageName = LanguageTable.Columns[ i ].ColumnName;
			
			//保存组件扩展信息
			string ExtendText = "<module>\n";
			if( ModuleMessageList[ i - 2 ] != null ) {
				ExtendText += ModuleMessageList[ i - 2 ].ModuleMes + "\n";
			}
			ExtendText += "</module>\n";
			string mBaseName = LanguageTable.Rows[ 0 ].ItemArray[ 1 ].ToString();
			string mNewName = LanguageTable.Rows[ 0 ].ItemArray[ i ].ToString();
			ExtendText += "<name> " + mBaseName + " " + mNewName + ",\n";
			ExtendText += "</name>\n";
			for( int j = 1; j < LanguageTable.Rows.Count; ++j ) {
				string Type = LanguageTable.Rows[ j ].ItemArray[ 0 ].ToString();
				string BaseName = LanguageTable.Rows[ j ].ItemArray[ 1 ].ToString();
				string NewName = LanguageTable.Rows[ j ].ItemArray[ i ].ToString();
				
				//保存成员扩展信息
				ExtendText += "<member> " + Type + " " + BaseName + " " + NewName + ",\n";
				if( ModuleMessageList[ i - 2 ] != null ) {
					
					MemberMes m = ModuleMessageList[ i - 2 ].GetMemberMessageFromName( BaseName );
					if( m != null ) {
						ExtendText += m.Mes + "\n";
					}
				}
				ExtendText += "</member>" + ",\n";
			}
			if( !Directory.Exists( Folder + this.textBox功能名称.Text ) ) {
				Directory.CreateDirectory( Folder + this.textBox功能名称.Text );
			}
			//保存描述文件
			string ExtendFile = Folder + this.textBox功能名称.Text + OS.PATH_S + "extend_" + LanguageName + ".txt";
			
			VIO.SaveTextFileGB2312( ExtendFile, ExtendText );
		}
	}
	
	//获取占用的资源列表
	string GetUsedResource()
	{
		string Res = this.textBoxResource.Text.Trim( ',' );
		if( Res == "" ) {
			return Res + ",";
		}
		ResourceList myResourceList = new ResourceList();
		
		string[] ResList = Res.Split( ',' );
		Res = "";
		for( int i = 0; i < ResList.Length; ++i ) {
			if( myResourceList.GetIndex( ResList[ i ] ) == -1 ) {
				MessageBox.Show( "未知的资源类型: " + ResList[ i ] );
			}
			Res += ResList[ i ] + ",";
		}
		return Res;
	}
	
	//获取图形控件扩展项列表
	string GetEXList()
	{
		string r = null;
		if( this.checkBox文字.Checked ) {
			r += this.checkBox文字.Text + ",";
		}
		if( this.checkBox字体.Checked ) {
			r += this.checkBox字体.Text + ",";
		}
		if( this.checkBox文字颜色.Checked ) {
			r += this.checkBox文字颜色.Text + ",";
		}
		if( this.checkBox背景颜色.Checked ) {
			r += this.checkBox背景颜色.Text + ",";
		}
		if( this.checkBox边框颜色.Checked ) {
			r += this.checkBox边框颜色.Text + ",";
		}
		if( this.textBoxExtendButton.Text != "" ) {
			string[] cut = this.textBoxExtendButton.Text.TrimEnd( '\n' ).Split( '\n' );
			for( int i = 0; i < cut.Length; ++i ) {
				if( cut[i] == "" ) {
					continue;
				}
				r += cut[i].Replace( ' ', '+' ) + ",";
			}
		}
		return r;
	}
	
	//设置信息框但不触发文本改变事件
	void SetModuleMessage( string text )
	{
		if( this.richTextBox描述信息.Text != text ) {
			isIgnoreTextChanged = true;
			this.richTextBox描述信息.Text = text;
			
			n_wlibCom.wlibCom.SetFont( this.richTextBox描述信息 );
		}
	}
	
	//添加一个操作信息
	void SetOperMessage( string mes )
	{
		this.labelOperMessage.BackColor = Color.LightPink;
		this.labelOperMessage.Text = mes;
	}
	
	//清除操作信息
	void ClearOperMessage()
	{
		this.labelOperMessage.BackColor = Color.LightGray;
		this.labelOperMessage.Text = "";
	}
	
	void anyTextChanged( object sender, EventArgs e )
	{
		isChanged = true;
	}
	
	//窗体加载
	void ModuleConfigFormLoad(object sender, EventArgs e)
	{
		if( CurrentFile != null ) {
			LoadFile( CurrentFile );
		}
	}
	
	//窗体关闭事件
	void ModuleConfigFormClosing(object sender, FormClosingEventArgs e)
	{
		if( isChanged ) {
			DialogResult d = MessageBox.Show( "文件已经被修改过, 需要保存吗?", "保存提示", MessageBoxButtons.YesNoCancel );
			if( d == DialogResult.Cancel ) {
				e.Cancel = true;
				return;
			}
			if( d == DialogResult.Yes ) {
				Button保存Click( null, null );
			}
		}
	}
	
	void Button打开Click(object sender, EventArgs e)
	{
		//打开文件对话框
		OpenFileDialog OpenFileDlg = new OpenFileDialog();
		OpenFileDlg.Filter = "驱动文件 *.B | *.B";
		OpenFileDlg.Title = "请选择一个驱动文件";
		DialogResult dlgResult = OpenFileDlg.ShowDialog();
		if(dlgResult == DialogResult.OK) {
			CurrentFile = OpenFileDlg.FileName;
			LoadFile( CurrentFile );
		}
		OpenFileDlg.Dispose();
	}
	
	void Button保存Click(object sender, EventArgs e)
	{
		if( CurrentFile == null ) {
			MessageBox.Show( "当前没有打开某个配置文件,无法保存" );
			return;
		}
		string Result = null;
		string ModuleFile = this.textBox组件文件.Text;
		
		Result += "//[配置信息开始],\n";
		
		if( ModuleFile != "" ) {
			Result += "//[组件文件] " + ModuleFile + ",\n";
		}
		
		Result += "//[语言列表]" + GetLanguageString() + "\n";
		Result += "//[组件名称]" + GetModuleName() + "\n";
		Result += "//[模块型号] " + this.textBoxChipType.Text + ",\n";
		Result += "//[资源占用] " + GetUsedResource() + "\n";
		
		if( textBoxPy.Text != "" ) {
			Result += "//[py参数] " + textBoxPy.Text + ",\n";
		}
		
		if( this.textBoxImage.Text != "" ) {
			Result += "//[实物图片] " + this.textBoxImage.Text + ",\n";
		}
		
		Result += "//[仿真类型] " + comboBoxSimulateType.SelectedIndex + ",\n";
		string ex = GetEXList();
		if( ex != null ) {
			Result += "//[控件属性] " + ex + "\n";
		}
		if( this.textBoxCommonRef.Text != "" ) {
			Result += "//[公共引用] " + this.textBoxCommonRef.Text + ",\n";
		}
		Result += GetLanguageElement();
		Result += "//[配置信息结束],\n";
		Result += "\n";
		Result += "unit " + this.textBox功能名称.Text + "\n";
		Result += "{\n";
		Result += "\t#include \"" + this.textBox功能名称.Text + OS.PATH_S + GUIcoder.GetSysDefine( "language" ) + ".txt\"\n";
		if( File.Exists( Folder + "driver" + OS.PATH_S + "io.txt" ) ) {
			Result += "\t#include \"driver" + OS.PATH_S + "io.txt\"\n";
			Result += "\tdriver.io = io;\n";
		}
		Result += "\tdriver =\n";
		Result += "\t#include \"driver" + OS.PATH_S + this.textBox功能名称.Text + ".txt\"\n";
		Result += "}\n";
		
		this.CodeBox.Text = Result;
		
		VIO.SaveTextFileGB2312( CurrentFile, Result );
		
		//保存语言子项的文件组
		SaveLanguageFiles();
		
		isChanged = false;
		
		SetOperMessage( "已经保存" );
	}
	
	void Button保存驱动文件Click(object sender, EventArgs e)
	{
		string FilePath = Folder + this.textBox内核文件.Text;
		VIO.SaveTextFileGB2312( FilePath, this.richTextBox驱动程序.Text );
	}
	
	void Button添加语言Click(object sender, EventArgs e)
	{
		string Name = this.textBox语言名称.Text;
		if( Name == "" ) {
			MessageBox.Show( "请先输入语言名称" );
			return;
		}
		foreach( DataColumn d in LanguageTable.Columns ) {
			if( d.ColumnName == Name ) {
				MessageBox.Show( "语言添加失败, 已经有同名的语言: " + Name );
				return;
			}
		}
		LanguageTable.Columns.Add( Name );
		
		//更新组件信息列表
		int mLength = 0;
		if( ModuleMessageList != null ) {
			mLength = ModuleMessageList.Length;
		}
		ModuleMessage[] ModuleMessageList1 = new ModuleMessage[ mLength + 1 ];
		for( int i = 0; ModuleMessageList != null && i < ModuleMessageList.Length; ++i) {
			ModuleMessageList1[ i ] = ModuleMessageList[ i ];
		}
		ModuleMessage newM = new ModuleMessage();
		newM.Language = Name;
		newM.ModuleMes = "";
		MemberMes[] MemberMesList = new MemberMes[ LanguageTable.Rows.Count - 1 ];
		newM.MemberMesList = MemberMesList;
		for( int j = 1; j < LanguageTable.Rows.Count; ++j ) {
			string BaseName = LanguageTable.Rows[ j ].ItemArray[ 1 ].ToString();
			MemberMesList[ j - 1 ] = new MemberMes();
			MemberMesList[ j - 1 ].Type = "null";
			MemberMesList[ j - 1 ].InnerName = BaseName;
			MemberMesList[ j - 1 ].UserName = BaseName;
			MemberMesList[ j - 1 ].Mes = "";
		}
		ModuleMessageList1[ mLength ] = newM;
		
		ModuleMessageList = ModuleMessageList1;
		
		isChanged = true;
	}
	
	void Button删除语言Click(object sender, EventArgs e)
	{
		string Name = this.textBox语言名称.Text;
		if( Name == "" ) {
			MessageBox.Show( "请先输入语言名称" );
			return;
		}
		DataColumn deleteD = null;
		int SIndex = 0;
		for( int i = 0; i < LanguageTable.Columns.Count; ++i ) {
			if( LanguageTable.Columns[i].ColumnName == Name ) {
				deleteD = LanguageTable.Columns[i];
				SIndex = i - 2;
				break;
			}
		}
		if( deleteD != null ) {
			if( MessageBox.Show( "您确实要删除这个语言吗?\n" + Name, "删除确认", MessageBoxButtons.YesNo ) ==
			    DialogResult.Yes ) {
				LanguageTable.Columns.Remove( deleteD );
				
				for( int i = SIndex; i < ModuleMessageList.Length - 1; ++i ) {
					ModuleMessageList[i] = ModuleMessageList[i + 1];
				}
				isChanged = true;
			}
		}
		else {
			MessageBox.Show( "删除失败, 没有此语言: " + Name );
		}
	}
	
	int CurrentCulumnIndex;
	void Button修改语言Click(object sender, EventArgs e)
	{
		LanguageTable.Columns[ CurrentCulumnIndex ].ColumnName = this.textBox语言名称.Text;
		isChanged = true;
	}
	
	//单元格点击事件
	void DataGridView1CellClick(object sender, DataGridViewCellEventArgs e)
	{
		ClearOperMessage();
		
		//更新语言名称
		CurrentCulumnIndex = e.ColumnIndex;
		if( e.ColumnIndex != -1 ) {
			this.textBox语言名称.Text = LanguageTable.Columns[ e.ColumnIndex ].ColumnName;
		}
		
		//判断点击区域是否为成员单元格
		if( e.ColumnIndex == -1 || e.RowIndex == -1 ) {
			//SetModuleMessage( null );
			//SetOperMessage( "无效的点击!" );
			return;
		}
		LanguageIndex = e.ColumnIndex - 2;
		MemberIndex = e.RowIndex;
		
		if( LanguageIndex < 0 ) {
			SetModuleMessage( "无信息显示, 未选中成员" );
			return;
		}
		if( MemberIndex == 0 ) {
			string ModuleMes = ModuleMessageList[ LanguageIndex ].ModuleMes;
			SetModuleMessage( ModuleMes );
		}
		else {
			string BaseName = LanguageTable.Rows[ MemberIndex ].ItemArray[ 1 ].ToString();
			MemberMes m = ModuleMessageList[ LanguageIndex ].GetMemberMessageFromName( BaseName );
			if( m != null ) {
				SetModuleMessage( m.Mes );
			}
			else {
				SetModuleMessage( "" );
				SetOperMessage( "发现空的成员描述!" );
			}
		}
	}
	
	void RichTextBox描述信息TextChanged(object sender, EventArgs e)
	{
		//如果忽略修改, 直接返回
		if( isIgnoreTextChanged ) {
			isIgnoreTextChanged = false;
			return;
		}
		//如果点击无效的单元格, 直接返回
		if( LanguageIndex < 0 || MemberIndex < 0 ) {
			SetOperMessage( "描述信息修改无效!" );
			return;
		}
		//获取描述文字
		string Des = this.richTextBox描述信息.Text;
		
		//判断是否修改组件描述
		if( MemberIndex == 0 ) {
			isChanged = true;
			ModuleMessageList[ LanguageIndex ].ModuleMes = Des;
			return;
		}
		string Type = LanguageTable.Rows[ MemberIndex ].ItemArray[ 0 ].ToString();
		string BaseName = LanguageTable.Rows[ MemberIndex ].ItemArray[ 1 ].ToString();
		string NewName = LanguageTable.Rows[ MemberIndex ].ItemArray[ LanguageIndex + 2 ].ToString();
		
		//判断是否修改组件成员描述
		MemberMes m = ModuleMessageList[ LanguageIndex ].GetMemberMessageFromName( BaseName );
		if( m != null ) {
			isChanged = true;
			m.Mes = Des;
			return;
		}
		//新建一个组件成员
		SetOperMessage( "新建成员描述" );
		
		isChanged = true;
		int MListLength = ModuleMessageList[ LanguageIndex ].MemberMesList.Length;
		MemberMes[] MemberMesList = new MemberMes[ MListLength + 1 ];
		for( int i = 0; i < MListLength; ++i ) {
			MemberMesList[ i ] = ModuleMessageList[ LanguageIndex ].MemberMesList[ i ];
		}
		MemberMesList[ MListLength ] = new MemberMes();
		MemberMesList[ MListLength ].InnerName = BaseName;
		MemberMesList[ MListLength ].UserName = NewName;
		MemberMesList[ MListLength ].Type = Type;
		MemberMesList[ MListLength ].Mes = Des;
		ModuleMessageList[ LanguageIndex ].MemberMesList = MemberMesList;
	}
	
	void Button复制Click(object sender, EventArgs e)
	{
		CopyString = this.richTextBox描述信息.Text;
	}
	
	void Button粘贴Click(object sender, EventArgs e)
	{
		this.richTextBox描述信息.Text = CopyString;
		isChanged = true;
	}
	
	void ButtonSaveBaseClick(object sender, EventArgs e)
	{
		if( CurrentFile == null ) {
			MessageBox.Show( "当前没有打开某个配置文件,无法保存" );
			return;
		}
		string Result = null;
		string ModuleFile = this.textBox组件文件.Text;
		
		Result += "//[配置信息开始],\n";
		
		if( ModuleFile != "" ) {
			Result += "//[组件文件] " + ModuleFile + ",\n";
		}
		
		Result += "//[语言列表]" + GetLanguageString() + "\n";
		Result += "//[组件名称]" + GetModuleName() + "\n";
		Result += "//[模块型号] " + this.textBoxChipType.Text + ",\n";
		Result += "//[资源占用] " + GetUsedResource() + "\n";
		
		if( this.textBoxImage.Text != "" ) {
			Result += "//[实物图片] " + this.textBoxImage.Text + ",\n";
		}
		if( textBoxPy.Text != "" ) {
			Result += "//[py参数] " + textBoxPy.Text + ",\n";
		}
		Result += "//[仿真类型] " + comboBoxSimulateType.SelectedIndex + ",\n";
		string ex = GetEXList();
		if( ex != null ) {
			Result += "//[控件属性] " + ex + "\n";
		}
		if( this.textBoxCommonRef.Text != "" ) {
			Result += "//[公共引用] " + this.textBoxCommonRef.Text + ",\n";
		}
		Result += GetLanguageElement();
		Result += "//[配置信息结束],\n";
		Result += "\n";
		Result += "unit " + this.textBox功能名称.Text + "\n";
		Result += "{\n";
		Result += "\t#include \"" + this.textBox功能名称.Text + OS.PATH_S + GUIcoder.GetSysDefine( "language" ) + ".txt\"\n";
		if( File.Exists( Folder + "driver" + OS.PATH_S + "io.txt" ) ) {
			Result += "\t#include \"driver" + OS.PATH_S + "io.txt\"\n";
			Result += "\tdriver.io = io;\n";
		}
		Result += "\tdriver =\n";
		Result += "\t#include \"driver" + OS.PATH_S + this.textBox功能名称.Text + ".txt\"\n";
		Result += "}\n";
		
		this.CodeBox.Text = Result;
		
		VIO.SaveTextFileGB2312( CurrentFile, Result );
		
		//保存语言子项的文件组
		//SaveLanguageFiles();
		
		isChanged = false;
		
		SetOperMessage( "已经保存" );
	}
}
}



