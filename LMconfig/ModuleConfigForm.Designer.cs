﻿/*
 * 由 SharpDevelop 创建。
 * 用户: bbb_2
 * 日期: 2009-11-17
 * 时间: 11:15
 * 
 * 如果你想要更改该模板，那么请使用“工具 | 选项 | 编辑 | 编辑标准标题”。
 */
namespace n_ModuleConfigForm
{
	partial class ModuleConfigForm
	{
		/// <summary>
		/// Designer variable used to keep track of non-visual components.
		/// </summary>
		private System.ComponentModel.IContainer components = null;
		
		/// <summary>
		/// Disposes resources used by the form.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing) {
				if (components != null) {
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}
		
		/// <summary>
		/// This method is required for Windows Forms designer support.
		/// Do not change the method contents inside the source code editor. The Forms designer might
		/// not be able to load this method if it was changed manually.
		/// </summary>
		private void InitializeComponent()
		{
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ModuleConfigForm));
			this.tabControl1 = new System.Windows.Forms.TabControl();
			this.tabPage1 = new System.Windows.Forms.TabPage();
			this.dataGridView1 = new System.Windows.Forms.DataGridView();
			this.panel2 = new System.Windows.Forms.Panel();
			this.textBoxPy = new System.Windows.Forms.TextBox();
			this.label9 = new System.Windows.Forms.Label();
			this.textBoxChipType = new System.Windows.Forms.TextBox();
			this.label8 = new System.Windows.Forms.Label();
			this.textBoxImage = new System.Windows.Forms.TextBox();
			this.label7 = new System.Windows.Forms.Label();
			this.comboBoxSimulateType = new System.Windows.Forms.ComboBox();
			this.label6 = new System.Windows.Forms.Label();
			this.textBoxCommonRef = new System.Windows.Forms.TextBox();
			this.button修改语言 = new System.Windows.Forms.Button();
			this.panel5 = new System.Windows.Forms.Panel();
			this.textBoxExtendButton = new System.Windows.Forms.RichTextBox();
			this.checkBox文字 = new System.Windows.Forms.CheckBox();
			this.checkBox文字颜色 = new System.Windows.Forms.CheckBox();
			this.checkBox边框颜色 = new System.Windows.Forms.CheckBox();
			this.checkBox背景颜色 = new System.Windows.Forms.CheckBox();
			this.checkBox字体 = new System.Windows.Forms.CheckBox();
			this.textBoxResource = new System.Windows.Forms.TextBox();
			this.label1 = new System.Windows.Forms.Label();
			this.button删除语言 = new System.Windows.Forms.Button();
			this.textBox语言名称 = new System.Windows.Forms.TextBox();
			this.button添加语言 = new System.Windows.Forms.Button();
			this.textBox内核文件 = new System.Windows.Forms.TextBox();
			this.textBox组件文件 = new System.Windows.Forms.TextBox();
			this.textBox功能名称 = new System.Windows.Forms.TextBox();
			this.label5 = new System.Windows.Forms.Label();
			this.label2 = new System.Windows.Forms.Label();
			this.label3 = new System.Windows.Forms.Label();
			this.label4 = new System.Windows.Forms.Label();
			this.panel4 = new System.Windows.Forms.Panel();
			this.button粘贴 = new System.Windows.Forms.Button();
			this.button复制 = new System.Windows.Forms.Button();
			this.richTextBox描述信息 = new System.Windows.Forms.RichTextBox();
			this.labelOperMessage = new System.Windows.Forms.Label();
			this.tabPage3 = new System.Windows.Forms.TabPage();
			this.richTextBox驱动程序 = new System.Windows.Forms.RichTextBox();
			this.panel3 = new System.Windows.Forms.Panel();
			this.label驱动文件路径 = new System.Windows.Forms.Label();
			this.button保存驱动文件 = new System.Windows.Forms.Button();
			this.tabPage2 = new System.Windows.Forms.TabPage();
			this.CodeBox = new System.Windows.Forms.RichTextBox();
			this.tabPage4 = new System.Windows.Forms.TabPage();
			this.richTextBoxNote = new System.Windows.Forms.RichTextBox();
			this.panel1 = new System.Windows.Forms.Panel();
			this.buttonSaveBase = new System.Windows.Forms.Button();
			this.labelChanged = new System.Windows.Forms.Label();
			this.FilePathlabel = new System.Windows.Forms.Label();
			this.button打开 = new System.Windows.Forms.Button();
			this.button保存 = new System.Windows.Forms.Button();
			this.tabControl1.SuspendLayout();
			this.tabPage1.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
			this.panel2.SuspendLayout();
			this.panel5.SuspendLayout();
			this.panel4.SuspendLayout();
			this.tabPage3.SuspendLayout();
			this.panel3.SuspendLayout();
			this.tabPage2.SuspendLayout();
			this.tabPage4.SuspendLayout();
			this.panel1.SuspendLayout();
			this.SuspendLayout();
			// 
			// tabControl1
			// 
			this.tabControl1.Controls.Add(this.tabPage1);
			this.tabControl1.Controls.Add(this.tabPage3);
			this.tabControl1.Controls.Add(this.tabPage2);
			this.tabControl1.Controls.Add(this.tabPage4);
			resources.ApplyResources(this.tabControl1, "tabControl1");
			this.tabControl1.Name = "tabControl1";
			this.tabControl1.SelectedIndex = 0;
			// 
			// tabPage1
			// 
			this.tabPage1.Controls.Add(this.dataGridView1);
			this.tabPage1.Controls.Add(this.panel2);
			this.tabPage1.Controls.Add(this.panel4);
			resources.ApplyResources(this.tabPage1, "tabPage1");
			this.tabPage1.Name = "tabPage1";
			this.tabPage1.UseVisualStyleBackColor = true;
			// 
			// dataGridView1
			// 
			this.dataGridView1.AllowUserToAddRows = false;
			this.dataGridView1.AllowUserToResizeRows = false;
			this.dataGridView1.BackgroundColor = System.Drawing.Color.Silver;
			this.dataGridView1.BorderStyle = System.Windows.Forms.BorderStyle.None;
			this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
			resources.ApplyResources(this.dataGridView1, "dataGridView1");
			this.dataGridView1.GridColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
			this.dataGridView1.MultiSelect = false;
			this.dataGridView1.Name = "dataGridView1";
			this.dataGridView1.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
			this.dataGridView1.RowTemplate.Height = 23;
			this.dataGridView1.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.CellSelect;
			this.dataGridView1.ShowEditingIcon = false;
			this.dataGridView1.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.DataGridView1CellClick);
			// 
			// panel2
			// 
			this.panel2.BackColor = System.Drawing.Color.LightGray;
			this.panel2.Controls.Add(this.textBoxPy);
			this.panel2.Controls.Add(this.label9);
			this.panel2.Controls.Add(this.textBoxChipType);
			this.panel2.Controls.Add(this.label8);
			this.panel2.Controls.Add(this.textBoxImage);
			this.panel2.Controls.Add(this.label7);
			this.panel2.Controls.Add(this.comboBoxSimulateType);
			this.panel2.Controls.Add(this.label6);
			this.panel2.Controls.Add(this.textBoxCommonRef);
			this.panel2.Controls.Add(this.button修改语言);
			this.panel2.Controls.Add(this.panel5);
			this.panel2.Controls.Add(this.textBoxResource);
			this.panel2.Controls.Add(this.label1);
			this.panel2.Controls.Add(this.button删除语言);
			this.panel2.Controls.Add(this.textBox语言名称);
			this.panel2.Controls.Add(this.button添加语言);
			this.panel2.Controls.Add(this.textBox内核文件);
			this.panel2.Controls.Add(this.textBox组件文件);
			this.panel2.Controls.Add(this.textBox功能名称);
			this.panel2.Controls.Add(this.label5);
			this.panel2.Controls.Add(this.label2);
			this.panel2.Controls.Add(this.label3);
			this.panel2.Controls.Add(this.label4);
			resources.ApplyResources(this.panel2, "panel2");
			this.panel2.Name = "panel2";
			// 
			// textBoxPy
			// 
			resources.ApplyResources(this.textBoxPy, "textBoxPy");
			this.textBoxPy.Name = "textBoxPy";
			// 
			// label9
			// 
			resources.ApplyResources(this.label9, "label9");
			this.label9.Name = "label9";
			// 
			// textBoxChipType
			// 
			resources.ApplyResources(this.textBoxChipType, "textBoxChipType");
			this.textBoxChipType.Name = "textBoxChipType";
			// 
			// label8
			// 
			resources.ApplyResources(this.label8, "label8");
			this.label8.Name = "label8";
			// 
			// textBoxImage
			// 
			resources.ApplyResources(this.textBoxImage, "textBoxImage");
			this.textBoxImage.Name = "textBoxImage";
			// 
			// label7
			// 
			resources.ApplyResources(this.label7, "label7");
			this.label7.Name = "label7";
			// 
			// comboBoxSimulateType
			// 
			this.comboBoxSimulateType.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
			this.comboBoxSimulateType.FormattingEnabled = true;
			this.comboBoxSimulateType.Items.AddRange(new object[] {
									resources.GetString("comboBoxSimulateType.Items"),
									resources.GetString("comboBoxSimulateType.Items1"),
									resources.GetString("comboBoxSimulateType.Items2")});
			resources.ApplyResources(this.comboBoxSimulateType, "comboBoxSimulateType");
			this.comboBoxSimulateType.Name = "comboBoxSimulateType";
			// 
			// label6
			// 
			resources.ApplyResources(this.label6, "label6");
			this.label6.Name = "label6";
			// 
			// textBoxCommonRef
			// 
			resources.ApplyResources(this.textBoxCommonRef, "textBoxCommonRef");
			this.textBoxCommonRef.Name = "textBoxCommonRef";
			// 
			// button修改语言
			// 
			resources.ApplyResources(this.button修改语言, "button修改语言");
			this.button修改语言.Name = "button修改语言";
			this.button修改语言.UseVisualStyleBackColor = true;
			this.button修改语言.Click += new System.EventHandler(this.Button修改语言Click);
			// 
			// panel5
			// 
			this.panel5.BackColor = System.Drawing.Color.DarkGray;
			this.panel5.Controls.Add(this.textBoxExtendButton);
			this.panel5.Controls.Add(this.checkBox文字颜色);
			this.panel5.Controls.Add(this.checkBox边框颜色);
			this.panel5.Controls.Add(this.checkBox背景颜色);
			this.panel5.Controls.Add(this.checkBox文字);
			this.panel5.Controls.Add(this.checkBox字体);
			resources.ApplyResources(this.panel5, "panel5");
			this.panel5.Name = "panel5";
			// 
			// textBoxExtendButton
			// 
			this.textBoxExtendButton.BorderStyle = System.Windows.Forms.BorderStyle.None;
			resources.ApplyResources(this.textBoxExtendButton, "textBoxExtendButton");
			this.textBoxExtendButton.Name = "textBoxExtendButton";
			// 
			// checkBox文字
			// 
			resources.ApplyResources(this.checkBox文字, "checkBox文字");
			this.checkBox文字.Name = "checkBox文字";
			this.checkBox文字.UseVisualStyleBackColor = true;
			this.checkBox文字.CheckedChanged += new System.EventHandler(this.anyTextChanged);
			// 
			// checkBox文字颜色
			// 
			resources.ApplyResources(this.checkBox文字颜色, "checkBox文字颜色");
			this.checkBox文字颜色.Name = "checkBox文字颜色";
			this.checkBox文字颜色.UseVisualStyleBackColor = true;
			this.checkBox文字颜色.CheckedChanged += new System.EventHandler(this.anyTextChanged);
			// 
			// checkBox边框颜色
			// 
			resources.ApplyResources(this.checkBox边框颜色, "checkBox边框颜色");
			this.checkBox边框颜色.Name = "checkBox边框颜色";
			this.checkBox边框颜色.UseVisualStyleBackColor = true;
			this.checkBox边框颜色.CheckedChanged += new System.EventHandler(this.anyTextChanged);
			// 
			// checkBox背景颜色
			// 
			resources.ApplyResources(this.checkBox背景颜色, "checkBox背景颜色");
			this.checkBox背景颜色.Name = "checkBox背景颜色";
			this.checkBox背景颜色.UseVisualStyleBackColor = true;
			this.checkBox背景颜色.CheckedChanged += new System.EventHandler(this.anyTextChanged);
			// 
			// checkBox字体
			// 
			resources.ApplyResources(this.checkBox字体, "checkBox字体");
			this.checkBox字体.Name = "checkBox字体";
			this.checkBox字体.UseVisualStyleBackColor = true;
			this.checkBox字体.CheckedChanged += new System.EventHandler(this.anyTextChanged);
			// 
			// textBoxResource
			// 
			resources.ApplyResources(this.textBoxResource, "textBoxResource");
			this.textBoxResource.Name = "textBoxResource";
			this.textBoxResource.TextChanged += new System.EventHandler(this.anyTextChanged);
			// 
			// label1
			// 
			resources.ApplyResources(this.label1, "label1");
			this.label1.Name = "label1";
			// 
			// button删除语言
			// 
			resources.ApplyResources(this.button删除语言, "button删除语言");
			this.button删除语言.Name = "button删除语言";
			this.button删除语言.UseVisualStyleBackColor = true;
			this.button删除语言.Click += new System.EventHandler(this.Button删除语言Click);
			// 
			// textBox语言名称
			// 
			resources.ApplyResources(this.textBox语言名称, "textBox语言名称");
			this.textBox语言名称.Name = "textBox语言名称";
			// 
			// button添加语言
			// 
			resources.ApplyResources(this.button添加语言, "button添加语言");
			this.button添加语言.Name = "button添加语言";
			this.button添加语言.UseVisualStyleBackColor = true;
			this.button添加语言.Click += new System.EventHandler(this.Button添加语言Click);
			// 
			// textBox内核文件
			// 
			this.textBox内核文件.BackColor = System.Drawing.Color.Silver;
			this.textBox内核文件.ForeColor = System.Drawing.Color.Navy;
			resources.ApplyResources(this.textBox内核文件, "textBox内核文件");
			this.textBox内核文件.Name = "textBox内核文件";
			this.textBox内核文件.ReadOnly = true;
			// 
			// textBox组件文件
			// 
			resources.ApplyResources(this.textBox组件文件, "textBox组件文件");
			this.textBox组件文件.Name = "textBox组件文件";
			this.textBox组件文件.TextChanged += new System.EventHandler(this.anyTextChanged);
			// 
			// textBox功能名称
			// 
			this.textBox功能名称.BackColor = System.Drawing.Color.Silver;
			this.textBox功能名称.ForeColor = System.Drawing.Color.Navy;
			resources.ApplyResources(this.textBox功能名称, "textBox功能名称");
			this.textBox功能名称.Name = "textBox功能名称";
			this.textBox功能名称.ReadOnly = true;
			// 
			// label5
			// 
			resources.ApplyResources(this.label5, "label5");
			this.label5.Name = "label5";
			// 
			// label2
			// 
			resources.ApplyResources(this.label2, "label2");
			this.label2.Name = "label2";
			// 
			// label3
			// 
			resources.ApplyResources(this.label3, "label3");
			this.label3.Name = "label3";
			// 
			// label4
			// 
			resources.ApplyResources(this.label4, "label4");
			this.label4.Name = "label4";
			// 
			// panel4
			// 
			this.panel4.Controls.Add(this.button粘贴);
			this.panel4.Controls.Add(this.button复制);
			this.panel4.Controls.Add(this.richTextBox描述信息);
			this.panel4.Controls.Add(this.labelOperMessage);
			resources.ApplyResources(this.panel4, "panel4");
			this.panel4.Name = "panel4";
			// 
			// button粘贴
			// 
			this.button粘贴.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
			this.button粘贴.ForeColor = System.Drawing.Color.Green;
			resources.ApplyResources(this.button粘贴, "button粘贴");
			this.button粘贴.Name = "button粘贴";
			this.button粘贴.UseVisualStyleBackColor = false;
			this.button粘贴.Click += new System.EventHandler(this.Button粘贴Click);
			// 
			// button复制
			// 
			this.button复制.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
			this.button复制.ForeColor = System.Drawing.Color.Green;
			resources.ApplyResources(this.button复制, "button复制");
			this.button复制.Name = "button复制";
			this.button复制.UseVisualStyleBackColor = false;
			this.button复制.Click += new System.EventHandler(this.Button复制Click);
			// 
			// richTextBox描述信息
			// 
			this.richTextBox描述信息.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
			this.richTextBox描述信息.BorderStyle = System.Windows.Forms.BorderStyle.None;
			resources.ApplyResources(this.richTextBox描述信息, "richTextBox描述信息");
			this.richTextBox描述信息.ForeColor = System.Drawing.Color.Black;
			this.richTextBox描述信息.Name = "richTextBox描述信息";
			this.richTextBox描述信息.TextChanged += new System.EventHandler(this.RichTextBox描述信息TextChanged);
			// 
			// labelOperMessage
			// 
			this.labelOperMessage.BackColor = System.Drawing.Color.LightGray;
			resources.ApplyResources(this.labelOperMessage, "labelOperMessage");
			this.labelOperMessage.ForeColor = System.Drawing.Color.Red;
			this.labelOperMessage.Name = "labelOperMessage";
			// 
			// tabPage3
			// 
			this.tabPage3.Controls.Add(this.richTextBox驱动程序);
			this.tabPage3.Controls.Add(this.panel3);
			resources.ApplyResources(this.tabPage3, "tabPage3");
			this.tabPage3.Name = "tabPage3";
			this.tabPage3.UseVisualStyleBackColor = true;
			// 
			// richTextBox驱动程序
			// 
			this.richTextBox驱动程序.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
			resources.ApplyResources(this.richTextBox驱动程序, "richTextBox驱动程序");
			this.richTextBox驱动程序.Name = "richTextBox驱动程序";
			// 
			// panel3
			// 
			this.panel3.Controls.Add(this.label驱动文件路径);
			this.panel3.Controls.Add(this.button保存驱动文件);
			resources.ApplyResources(this.panel3, "panel3");
			this.panel3.Name = "panel3";
			// 
			// label驱动文件路径
			// 
			resources.ApplyResources(this.label驱动文件路径, "label驱动文件路径");
			this.label驱动文件路径.Name = "label驱动文件路径";
			// 
			// button保存驱动文件
			// 
			resources.ApplyResources(this.button保存驱动文件, "button保存驱动文件");
			this.button保存驱动文件.Name = "button保存驱动文件";
			this.button保存驱动文件.UseVisualStyleBackColor = true;
			this.button保存驱动文件.Click += new System.EventHandler(this.Button保存驱动文件Click);
			// 
			// tabPage2
			// 
			this.tabPage2.Controls.Add(this.CodeBox);
			resources.ApplyResources(this.tabPage2, "tabPage2");
			this.tabPage2.Name = "tabPage2";
			this.tabPage2.UseVisualStyleBackColor = true;
			// 
			// CodeBox
			// 
			this.CodeBox.BorderStyle = System.Windows.Forms.BorderStyle.None;
			resources.ApplyResources(this.CodeBox, "CodeBox");
			this.CodeBox.Name = "CodeBox";
			this.CodeBox.ReadOnly = true;
			// 
			// tabPage4
			// 
			this.tabPage4.Controls.Add(this.richTextBoxNote);
			resources.ApplyResources(this.tabPage4, "tabPage4");
			this.tabPage4.Name = "tabPage4";
			this.tabPage4.UseVisualStyleBackColor = true;
			// 
			// richTextBoxNote
			// 
			this.richTextBoxNote.BorderStyle = System.Windows.Forms.BorderStyle.None;
			resources.ApplyResources(this.richTextBoxNote, "richTextBoxNote");
			this.richTextBoxNote.Name = "richTextBoxNote";
			this.richTextBoxNote.ReadOnly = true;
			// 
			// panel1
			// 
			this.panel1.BackColor = System.Drawing.Color.Gray;
			this.panel1.Controls.Add(this.buttonSaveBase);
			this.panel1.Controls.Add(this.labelChanged);
			this.panel1.Controls.Add(this.FilePathlabel);
			this.panel1.Controls.Add(this.button打开);
			this.panel1.Controls.Add(this.button保存);
			resources.ApplyResources(this.panel1, "panel1");
			this.panel1.Name = "panel1";
			// 
			// buttonSaveBase
			// 
			this.buttonSaveBase.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
			resources.ApplyResources(this.buttonSaveBase, "buttonSaveBase");
			this.buttonSaveBase.ForeColor = System.Drawing.Color.DimGray;
			this.buttonSaveBase.Name = "buttonSaveBase";
			this.buttonSaveBase.UseVisualStyleBackColor = false;
			this.buttonSaveBase.Click += new System.EventHandler(this.ButtonSaveBaseClick);
			// 
			// labelChanged
			// 
			this.labelChanged.BackColor = System.Drawing.Color.WhiteSmoke;
			resources.ApplyResources(this.labelChanged, "labelChanged");
			this.labelChanged.ForeColor = System.Drawing.Color.Blue;
			this.labelChanged.Name = "labelChanged";
			// 
			// FilePathlabel
			// 
			resources.ApplyResources(this.FilePathlabel, "FilePathlabel");
			this.FilePathlabel.ForeColor = System.Drawing.Color.White;
			this.FilePathlabel.Name = "FilePathlabel";
			// 
			// button打开
			// 
			this.button打开.BackColor = System.Drawing.Color.LightSalmon;
			resources.ApplyResources(this.button打开, "button打开");
			this.button打开.ForeColor = System.Drawing.Color.White;
			this.button打开.Name = "button打开";
			this.button打开.UseVisualStyleBackColor = false;
			this.button打开.Click += new System.EventHandler(this.Button打开Click);
			// 
			// button保存
			// 
			this.button保存.BackColor = System.Drawing.Color.CornflowerBlue;
			resources.ApplyResources(this.button保存, "button保存");
			this.button保存.ForeColor = System.Drawing.Color.White;
			this.button保存.Name = "button保存";
			this.button保存.UseVisualStyleBackColor = false;
			this.button保存.Click += new System.EventHandler(this.Button保存Click);
			// 
			// ModuleConfigForm
			// 
			resources.ApplyResources(this, "$this");
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.BackColor = System.Drawing.Color.White;
			this.Controls.Add(this.tabControl1);
			this.Controls.Add(this.panel1);
			this.MaximizeBox = false;
			this.MinimizeBox = false;
			this.Name = "ModuleConfigForm";
			this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.ModuleConfigFormClosing);
			this.Load += new System.EventHandler(this.ModuleConfigFormLoad);
			this.tabControl1.ResumeLayout(false);
			this.tabPage1.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
			this.panel2.ResumeLayout(false);
			this.panel2.PerformLayout();
			this.panel5.ResumeLayout(false);
			this.panel4.ResumeLayout(false);
			this.tabPage3.ResumeLayout(false);
			this.panel3.ResumeLayout(false);
			this.tabPage2.ResumeLayout(false);
			this.tabPage4.ResumeLayout(false);
			this.panel1.ResumeLayout(false);
			this.ResumeLayout(false);
		}
		private System.Windows.Forms.Label label9;
		private System.Windows.Forms.TextBox textBoxPy;
		private System.Windows.Forms.Button buttonSaveBase;
		private System.Windows.Forms.Label label8;
		private System.Windows.Forms.TextBox textBoxChipType;
		private System.Windows.Forms.Label label7;
		private System.Windows.Forms.TextBox textBoxImage;
		private System.Windows.Forms.Label label6;
		private System.Windows.Forms.ComboBox comboBoxSimulateType;
		private System.Windows.Forms.TextBox textBoxCommonRef;
		private System.Windows.Forms.Label label4;
		private System.Windows.Forms.Button button修改语言;
		private System.Windows.Forms.Button button粘贴;
		private System.Windows.Forms.Button button复制;
		private System.Windows.Forms.CheckBox checkBox文字;
		private System.Windows.Forms.CheckBox checkBox背景颜色;
		private System.Windows.Forms.CheckBox checkBox边框颜色;
		private System.Windows.Forms.CheckBox checkBox文字颜色;
		private System.Windows.Forms.CheckBox checkBox字体;
		private System.Windows.Forms.Panel panel5;
		private System.Windows.Forms.Label labelOperMessage;
		private System.Windows.Forms.Panel panel4;
		private System.Windows.Forms.Label labelChanged;
		private System.Windows.Forms.RichTextBox richTextBox描述信息;
		private System.Windows.Forms.TextBox textBoxResource;
		private System.Windows.Forms.Label label1;
		private System.Windows.Forms.TextBox textBox功能名称;
		private System.Windows.Forms.RichTextBox richTextBoxNote;
		private System.Windows.Forms.TabPage tabPage4;
		private System.Windows.Forms.Label label驱动文件路径;
		private System.Windows.Forms.TextBox textBox语言名称;
		private System.Windows.Forms.Button button删除语言;
		private System.Windows.Forms.Button button添加语言;
		private System.Windows.Forms.Button button保存驱动文件;
		private System.Windows.Forms.Panel panel3;
		private System.Windows.Forms.DataGridView dataGridView1;
		private System.Windows.Forms.TabPage tabPage2;
		private System.Windows.Forms.Panel panel2;
		private System.Windows.Forms.RichTextBox richTextBox驱动程序;
		private System.Windows.Forms.TabPage tabPage3;
		private System.Windows.Forms.Label label5;
		private System.Windows.Forms.TextBox textBox内核文件;
		private System.Windows.Forms.Label label3;
		private System.Windows.Forms.RichTextBox CodeBox;
		private System.Windows.Forms.Button button保存;
		private System.Windows.Forms.Button button打开;
		private System.Windows.Forms.TextBox textBox组件文件;
		private System.Windows.Forms.Label label2;
		private System.Windows.Forms.Label FilePathlabel;
		private System.Windows.Forms.Panel panel1;
		private System.Windows.Forms.TabPage tabPage1;
		private System.Windows.Forms.TabControl tabControl1;
		private System.Windows.Forms.RichTextBox textBoxExtendButton;
	}
}
