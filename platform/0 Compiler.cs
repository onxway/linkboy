﻿
//系统参数
namespace i_Compiler
{
using System;
using n_OS;

public static class Compiler
{
	//打开系统文件
	public static string OpenCompileFile( string FileName )
	{
		return VIO.OpenTextFileUTF8( OS.CompileRoot + FileName );
	}
	
	//打开程序文件, 路径为绝对路径
	public static string OpenIncludeFile( string Path )
	{
		return VIO.OpenTextFileGB2312( Path );
	}
	
	//打开程序文件
	public static string OpenProgFile( string Path )
	{
		return VIO.OpenTextFileGB2312( Path );
	}
	
	//判断文件是否存在
	public static bool FileExists( string Path )
	{
		return VIO.FileExists( Path );
	}
	
	//输出一个信息
	public static void Show( string m )
	{
		VIO.Show( m );
	}
}
}





