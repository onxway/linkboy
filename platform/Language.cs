﻿


namespace n_Language
{
using System;

public static class Language
{
	public const string Mod_c = "c";
	public const string Mod_Chinese = "chinese";
	
	
	public static string UILanguage;
	public static bool isChinese;
	public static bool isEnglish;
	
	public static string NotSave;
	public static string DownLoad;
	public static string SwitchPanel;
	public static string DebugMode;
	public static string Export;
	public static string SimMode;
	public static string WorldMode;
	public static string Save;
	public static string Open;
	public static string New;
	public static string SaveAs;
	public static string VosPad;
	public static string RemoPad;
	public static string Python;
	public static string SysCode;
	public static string Tool;
	public static string CurrentVersion1;
	public static string Service;
	
	
	public static string RedoTip1;
	public static string UndoTip1;
	
	public static string RedoTip2;
	public static string UndoTip2;
	
	
	public static string UserValueSet;
	public static string UserValueSetTip;
	
	public static string Copy;
	public static string CopyTip;
	
	public static string CopyClip;
	public static string CopyClipTip;
	
	public static string Paste;
	public static string PasteTip;
	
	public static string Prt;
	public static string PrtTip;
	
	public static string L_Ins;
	public static string L_Element;
	public static string L_Module;
	
	//指令系列
	public static string Void;
	public static string Bool;
	public static string Int32;
	public static string Fix;
	public static string Forever;
	public static string UserDefine;
	public static string FlashTimes;
	public static string Setting;
	public static string Loop1;
	public static string Loop2;
	public static string If;
	public static string IfMes;
	public static string Else;
	public static string AddElse;
	public static string DelElse;
	public static string End;
	public static string ForeverMes;
	public static string I_While1;
	public static string I_While2;
	public static string I_WhileMes;
	public static string I_SHunXu;
	public static string I_FanXu;
	public static string I_List;
	public static string I_IterMes;
	public static string I_IterSetMes1;
	public static string I_IterSetMes2;
	public static string I_Wait;
	
	public static string I_Return;
	public static string I_Break;
	public static string I_Continue;
	
	public static string NewIns;
	public static string BaseType;
	public static string BoolType;
	public static string IntType;
	public static string FixType;
	public static string FloatType;
	public static string RoleType;
	
	public static string ModuleFunc;
	public static string ModuleFuncMes;
	public static string GFixMes;
	public static string MusicVarMes;
	
	public static string EXP_System;
	public static string EXP_SystemMes;
	public static string EXP_Number;
	public static string EXP_NumberMes;
	public static string EXP_Var;
	public static string EXP_VarMes;
	public static string EXP_Oper;
	public static string EXP_OperMes;
	public static string EXP_User;
	public static string EXP_UserMes;
	
	public static string SYS_VersionMes1;
	public static string SYS_VersionMes2;
	public static string SYS_VersionMes3;
	public static string SYS_VersionMesA1;
	public static string SYS_VersionMesA2;
	public static string SYS_VersionMesB1;
	public static string SYS_VersionMesB2;
	
	public static string SIM_True;
	public static string SIM_False;
	
	//仿真快捷键提示
	public static string SimShortcutKey;
	
	//问题级别
	public static string MesLevel1;
	public static string MesLevel2;
	
	//CCode
	public static string CC_SaveFile;
	public static string CC_ProFile;
	public static string CC_OtherFile;
	public static string CC_SaveAsHead;
	public static string CC_SaveAs;
	
	//MyFileObjectPanel
	public static string MFP_Delete;
	public static string MFP_Left;
	public static string MFP_Right;
	public static string MFP_Swap;
	public static string MFP_Mes;
	public static string MFP_Example;
	
	//Head
	public static string Head_DeleteIns;
	
	public static string Module_Copy;
	public static string Module_CopyButton;
	public static string RunMode_Step;
	public static string RunMode_Full;
	
	//初始化
	public static void SetLanguage( string l )
	{
		UILanguage = l;
		
		if( UILanguage == "Chinese" ) {
			
			isChinese = true;
			
			NotSave = "您的设计图还没有存放到电脑硬盘上";
			DownLoad = "编译下载程序到控制板中 (使用自研编译器系统)";
			SwitchPanel = "在图形配置界面和程序编辑界面切换";
			DebugMode = "开启调试模式 (可显示变量的数值变化情况和传感器的实时数据波形)";
			Export = "导出图形界面对应的C/C++程序, 可在 arduino / keil-C51 / MDK / Eclipse 等平台编译下载 (使用第三方编译器)";
			SimMode = "仿真:通过电脑模拟运行程序(按下左Shift键查看快捷键)";
			WorldMode = "在电脑虚拟环境中让您的作品动起来";
			Save = "保存当前的实验台文件";
			Open = "打开现有的实验台文件";
			New = "新建一个实验台文件";
			SaveAs = "当前文件另存为...";
			VosPad = "通过外挂神器可以对第三方开发板进行图形化编程, 并支持调用本地的C/C++程序";
			RemoPad = "通过遥控器可以对各个硬件模块进行独立控制";
			Python = "编辑和查看Python代码";
			SysCode = "切换到crux程序代码编辑页面";
			Tool = "各种常用的小工具软件";
			CurrentVersion1 = "点击查看在线消息盒, 软件版本为";
			
			Service = "遇到问题可联系人工客服";
			
			RedoTip1 = "重做(还可重做";
			UndoTip1 = "撤销(还可撤销";
			RedoTip2 = "步)";
			UndoTip2 = "步)";
			
			UserValueSet = "高级参数设置";
			UserValueSetTip = "设置本程序中的高级参数";
			
			Copy = "复制双份指令\n\n注意先用鼠标\n右键框选指令";
			CopyTip = "点击后会复制出同样的指令 (先用鼠标右键选中需要复制的指令组)";
			
			CopyClip = "复制";
			CopyClipTip = "右键框选内容复制到剪贴板";
			
			Paste = "粘贴";
			PasteTip = "从剪贴板粘贴到当前界面";
			
			Prt = "屏幕截图\n快捷键: F";
			PrtTip = "截取屏幕指定区域的图片 (F";
			
			L_Ins = "指令";
			L_Element = "元素";
			L_Module = "模块";
			
			Void = "无值";
			Bool = "条件量";
			Int32 = "数值量";
			Fix = "小数量";
			Forever = "反复执行";
			UserDefine = "自定义指令";
			FlashTimes = "?void 闪烁 ?int32 _a 次";
			Setting = "设置";
			
			Loop1 = "反复执行";
			Loop2 = "次";
			
			If = "如果";
			Else = "否则";
			AddElse = "加否则";
			DelElse = "删否则";
			IfMes = "<如果>判断语句默认不带有<否则>分支, 如需要<否则>分支, 请把鼠标放置在<如果>语句的结束块, 会出现<加否则>, 点击即可.";
			
			End = "结束";
			ForeverMes = "<反复执行语句>是高手专用, 小白谨慎使用. " +
									  "因为它会阻塞当前事件永不退出, 导致当前事件不再响应下一次的触发(除非使用退出循环指令). " +
								      "强烈建议修改您的程序, 去掉这个语句. 可以用 <控制器.反复执行> 代替反复执行语句, 或者用定时器定时执行";
			
			I_While1 = "当";
			I_While2 = "时反复执行";
			I_WhileMes = "<条件循环>语句, 当条件成立时, 反复循环, 条件不成立则退出循环.";
			
			I_Wait = "等待 直到";
			
			NewIns = "新指令";
			BaseType = "基本量";
			BoolType = "条件量";
			IntType = "整数值";
			FixType = "小数量";
			FloatType = "浮点数";
			RoleType = "角色";
			
			I_SHunXu = "顺序遍历";
			I_FanXu = "反序遍历";
			I_List = "列表";
			I_IterMes = "<遍历>语句默认是正序遍历, 按照角色的创建顺序依次遍历, 如需要反序遍历, 请鼠标点击本语句即可设置";
			I_IterSetMes1 = "是否将遍历模式设置为顺序遍历? 点击是设置为顺序遍历, 点击否设置为反序遍历.";
			I_IterSetMes2 = "遍历模式设置";
			
			I_Return = "返回";
			I_Break = "退出循环";
			I_Continue = "继续循环";
			
			ModuleFunc = "?void @MFUNC 模块类 功能指令";
			ModuleFuncMes = "由于<模块功能指令>很常用, 所以专门增加了一个快捷操作, 通过鼠标双击空白区域也可以添加一个此指令. ";
			GFixMes = "小数量(fix)类型的变量, 目前版本为精确到小数点后3位, 可表示的数据范围比普通整数类型小了1000倍, 此类型一般不常用, 因为大部分模块指令的参数都是整数类型";
			MusicVarMes = "音乐变量如果存放双声道的音乐, 用普通的扬声器播放时只能播放第一声道, 推荐用<双声道扬声器>播放, 音效更好. 缺点是音量稍小, 可以用纸杯或纸盒挖洞固定扬声器, 利用共振增大声音";
			
			SimShortcutKey = "仿真状态下:\n";
			SimShortcutKey += "   1. 按住 左Ctrl 键为自锁模式, 鼠标点击可独立触发多个传感器 (第一次点击表示按下, 第二次点击表示松开...)\n";
			SimShortcutKey += "   2. 按住 左Shift键 + 字符键(字母/数字/标点)的同时, 鼠标点击传感器, 系统会绑定对应字符按键和传感器, 可用按键代替鼠标触发传感器\n";
			SimShortcutKey += "   3. 按住左上角 ESC 键的同时, 鼠标点击传感器, 可解除传感器按键绑定,解除后则只能通过鼠标点击触发";
			
			MesLevel1 = "问题级别:";
			MesLevel2 = " (描述级-1 提示级-2 报警级-3)\n";
			
			
			EXP_System = "[系统]";
			EXP_Number = "[数字]";
			EXP_Var = "[变量]";
			EXP_Oper = "[运算]";
			EXP_User = "[全局自定义]";
			EXP_SystemMes = "系统指令类, 包括启用、禁用和结束指定的事件。在启用指定事件的状态下，如果对应事件的条件成立，那么系统就会执行此事件里的所有用户指令序列；在禁用指定事件的状态下，那么系统将不再检测此事件是否成立，也不执行事件里的指令序列；结束指定事件是指立刻让指定事件停止运行， 例如事件处于延时、等待、或者反复执行的时候，可以用这个指令强制结束事件。";
			EXP_NumberMes = "编辑各种数值型常量和字符，字符串等。数值和字符是完全通用的，每个字符的数值大小等于其对应的ascii码。注意：颜色和字符一样，本质上也是数值";
			EXP_VarMes = "用户的所有自定义变量";
			EXP_OperMes = "各种数学运算";
			EXP_UserMes = "用户的所有自定义变量, 指令.";
			
			SYS_VersionMes1 = "版本校验提示";
			SYS_VersionMes2 = "当前软件版本号:";
			SYS_VersionMes3 = " - 打开文件版本号:";
			SYS_VersionMesA1 = "此文件为新版linkboy创建(";
			SYS_VersionMesA2 = "). 如打开出错请尝试到官网下载最新版linkboy";
			SYS_VersionMesB1 = "此文件为旧版linkboy创建(";
			SYS_VersionMesB2 = "). 将按新格式保存文件";
			
			SIM_True = "是(成立)";
			SIM_False = "否(不成立)";
			
			CC_SaveFile = "保存文件";
			CC_ProFile = "项目文件(*.";
			CC_OtherFile = "|其他扩展名项目文件|*.*";
			CC_SaveAs = "您正在编辑系统内置示例, 建议先点击<另存为>按钮保存到本地后再进行编辑";
			CC_SaveAsHead = "另存为提示";
			
			MFP_Delete = "删除";
			MFP_Left = "左旋";
			MFP_Right = "右旋";
			MFP_Swap = "翻转";
			MFP_Mes = "说明";
			MFP_Example = "示例";
			
			Head_DeleteIns = "拖到这里可删除";
			
			Module_Copy = "本页面的模块信息已复制到剪贴板中";
			Module_CopyButton = "复制本页信息";
			RunMode_Step = "单步执行 (空格键)";
			RunMode_Full = "全速执行 (回车键)";
		}
		if( UILanguage == "English" ) {
			
			isEnglish = true;
			
			NotSave = "Your design has not been stored on the computer hard disk yet.";
			DownLoad = "Download the program to the control board";
			SwitchPanel = "Switch panel at graphics and code";
			DebugMode = "DebugModen: open Debug Mode (Displays numerical variations of variables and real-time data waveforms of sensors)";
			Export = "Export the code for Artduino ande other platform";
			SimMode = "Emulation: Running the program by computer simulation (press the left Shift key to view the shortcut key)";
			WorldMode = "Make Your Works Active in Computer Virtual Environment";
			Save = "Save the current lab file";
			Open = "Open the current lab file";
			New = "Create a new lab file";
			SaveAs = "The current file is saved as...";
			VosPad = "Vos Mode";
			RemoPad = "Control Pad";
			Python = "Editing and viewing Python code";
			SysCode = "viewing UI code";
			Tool = "A variety of commonly used gadget software";
			CurrentVersion1 = "Click to view the online message box, the software version is ";
			Service = "Contact manual customer service if you encounter problems";
			
			RedoTip1 = "Redo(Surplus";
			UndoTip1 = "Undo(Surplus";
			RedoTip2 = "Step)";
			UndoTip2 = "Step)";
			
			UserValueSet = "UserValueSet";
			UserValueSetTip = "High-level parameters in this program can be set to achieve fine-tuning of the program.";
			
			Copy = "Copy\nTo Current";
			CopyTip = "The same instructions will be copied when clicked (first select the group of instructions that need to be copied with the right mouse button)";
			
			CopyClip = "Copy";
			CopyClipTip = "Click Copy to Clipboard, then you can open a new program and paste it in";
			
			Paste = "Paste";
			PasteTip = "The instructions copied to the clipboard before extraction can be pasted into the current program interface.";
			
			Prt = "ScreenCapture\nShortcutKey:F";
			PrtTip = "Shortcut keys can be used to capture pictures of specific areas on the screen";
			
			L_Ins = "Program";
			L_Element = "Element";
			L_Module = "Module";
			
			Void = "void";
			Bool = "bool";
			Int32 = "int32";
			Fix = "float";
			Forever = "loop";
			UserDefine = "CustomDirectives";
			FlashTimes = "?void Flash ?int32 _a Times";
			Setting = "Set";
			
			Loop1 = "loop";
			Loop2 = "times";
			
			If = "if";
			Else = "else";
			AddElse = "AddElse";
			DelElse = "DelElse";
			IfMes = "If the judgement statement does not have < else > branch by default, if you need < else > branch, please place the mouse at the end of < if > sentence and < AddElse > will appear. Just click.";
			
			End = "end";
			ForeverMes = "<Repeated Execution Statement> is specially designed for beginners to use cautiously." +
									  "Because it blocks the current event from never exiting, the current event will no longer respond to the next trigger (unless an exit loop instruction is used)." +
								      "It is strongly recommended that you modify your program to remove this statement. You can use < Controller. Repeated execution > instead of repeated execution statement, or use a timer to execute it regularly.";
			
			I_While1 = "while";
			I_While2 = "";
			I_WhileMes = "The sentence of <conditional cycle> repeats when the condition is established, and withdraws when the condition is not established.";
			I_IterSetMes1 = "Is the traversal mode set to sequential traversal? Click YES is set to sequential traversal, click NO is set to reverse traversal.";
			I_IterSetMes2 = "Traversal mode settings";
			
			I_Wait = "wait until";
			
			NewIns = "new";
			BaseType = "base";
			BoolType = "bool";
			IntType = "int32";
			FixType = "fix";
			FloatType = "float";
			RoleType = "role";
			
			I_SHunXu = "iter-up";
			I_FanXu = "iter-do";
			I_List = "list";
			I_IterMes = "The < iteration > statement is traversed in positive order by default, according to the order of role creation. If you need to traverse in reverse order, please click this statement to set it.";
			
			I_Return = "return";
			I_Break = "break";
			I_Continue = "continue";
			
			ModuleFunc = "?void @MFUNC Module Func";
			ModuleFuncMes = "Because <Module Function Directive> is very common, a shortcut operation is specially added, which can also be added by double-clicking the blank area with the mouse.";
			GFixMes = "The current version of small quantity (fix) type variable is accurate to 3 decimal places. The data range that can be represented is 1000 times smaller than that of ordinary integer type. This type is not commonly used, because most of the parameters of module instructions are integer type";
			MusicVarMes = "If music variables are stored in two-channel music, they can only play the first channel when playing with ordinary loudspeakers. It is recommended to play with <two-channel loudspeakers> for better sound effect. The disadvantage is that the volume is slightly smaller, so the loudspeaker can be fixed by digging holes in paper cups or cartons, and the sound can be increased by resonance.";
			
			
			SimShortcutKey = "In the state of simulation:\n";
			SimShortcutKey += "1. Hold the left Ctrl key for self-locking mode, mouse click can trigger multiple sensors independently\n";
			SimShortcutKey += "2. When the left Shift key + letter key is pressed, the mouse clicks on the sensor, the system will bind the corresponding letter keys and sensors, and the key can be used instead of the mouse trigger sensor\n";
			SimShortcutKey += "3. While holding ESC key in the upper left corner, the mouse clicks on the sensor, which can release the binding of the sensor key, then can only be triggered by the mouse click after the release";
			
			MesLevel1 = "Level:";
			MesLevel2 = " (Description-1 Prompt-2 Alarm-3)\n";
			
			EXP_System = "[system]";
			EXP_Number = "[number]";
			EXP_Var = "[var]";
			EXP_Oper = "[operate]";
			EXP_User = "[custom]";
			
			EXP_SystemMes = "System instruction classes, including enabling, disabling, and terminating specified events. When the specified event is enabled, if the condition of the corresponding event is valid, then the system will execute all the user instruction sequences in the event; if the specified event is disabled, the system will no longer detect whether the event is valid or not, nor execute the instruction sequence in the event; ending the specified event means that the specified event is stopped immediately, such as the event being delayed, etc. This command can be used to force an end to an event when it is to be executed or repeated.";
			EXP_NumberMes = "Edit numeric constants and characters, strings, etc. Numbers and characters are completely universal. The numerical size of each character is equal to its corresponding ASCII code.";
			EXP_VarMes = "All user-defined variables";
			EXP_OperMes = "Various Mathematical Operations";
			EXP_UserMes = "All user-defined instructions and all events. Using events as instructions is equivalent to triggering an event manually.";
			
			SYS_VersionMes1 = "Version Check Tips";
			SYS_VersionMes2 = "Current software version number:";
			SYS_VersionMes3 = " - Open the file version number:";
			SYS_VersionMesA1 = "This file is created for the new version of linkboy (";
			SYS_VersionMesA2 = "If you open it incorrectly, please try downloading the latest version of linkboy on the official website.";
			SYS_VersionMesB1 = "This file is created for the old version of linkboy (";
			SYS_VersionMesB2 = "). The new format will be saved automatically later.";
			
			SIM_True = "YES(true)";
			SIM_False = "NO(false)";
			
			CC_SaveFile = "Save File";
			CC_ProFile = "Project File(*.";
			CC_OtherFile = "|Other File|*.*";
			CC_SaveAs = "You are editing the built-in example of the system. It is recommended that you click the < Save as > button to save it locally before editing it.";
			CC_SaveAsHead = "Save As";
			
			MFP_Delete = "Del";
			MFP_Left = "Left";
			MFP_Right = "Right";
			MFP_Swap = "Swap";
			MFP_Mes = "info";
			MFP_Example = "Exam";
			
			Head_DeleteIns = "Drag here to delete";
			
			Module_Copy = "The module information for this page has been copied to the clipboard";
			Module_CopyButton = "Copy info";
			RunMode_Step = "Step execution (Spacebar)";
			RunMode_Full = "Full speed execution (Enter)";
		}
	}
}
}

