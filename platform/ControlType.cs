﻿
//命名空间
namespace c_ControlType
{
using System;


//图形控件类型
public static class ControlType
{
	public const int N_SYS = 0;
	
	public const string ControlPad = "ControlPad";
	public const int N_ControlPad = 1;
	
	public const string Button = "Button";
	public const int N_Button = 2;
	
	public const string Panel = "Panel";
	public const int N_Panel = 3;
	
	public const string NumberBox = "NumberBox";
	public const int N_NumberBox = 4;
	
	public const string Label = "Label";
	public const int N_Label = 5;
	
	public const string TrackBar = "TrackBar";
	public const int N_TrackBar = 6;
	
	public const string CheckBox = "CheckBox";
	public const int N_CheckBox = 7;
	
	public const string ImagePanel = "ImagePanel";
	public const int N_ImagePanel = 8;
	
	public const string MachineArm = "MachineArm";
	public const int N_MachineArm = 15;
	
	public const string WaveBox = "WaveBox";
	public const int N_WaveBox = 16;
	
	public const string BitmapBox = "BitmapBox";
	public const int N_BitmapBox = 17;
	
	public const string MIDI = "MIDI";
	public const int N_MIDI = 20;
	
	public const string KeyBoard = "KeyBoard";
	public const int N_KeyBoard = 21;
	
	public const string Sound = "Sound";
	public const int N_Sound = 22;
	
	public const string SerialPort = "SerialPort";
	public const int N_SerialPort = 23;
	
	public const string Clock = "Clock";
	public const int N_Clock = 24;
	
	public const string KeyTrig = "KeyTrig";
	public const int N_KeyTrig = 25;
	
	public const string Sprite = "Sprite";
	public const int N_Sprite = 40;
	
	public const string Ble20 = "Ble20";
	public const int N_Ble20 = 70;
	
	public const string Telephone = "Telephone";
	public const int N_Telephone = 71;
	
	public const string Ble40 = "Ble40";
	public const int N_Ble40 = 72;
	
	public const string VHTest = "VHTest";
	public const int N_VHTest = 73;
	
	public const string OpenCV = "OpenCV";
	public const int N_OpenCV = 74;
	
	public const string Camera = "Camera";
	public const int N_Camera = 75;
}
}


