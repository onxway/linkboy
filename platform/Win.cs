﻿
using System;
using System.Windows.Forms;
//using n_OS;

namespace p_Win
{
//命令行工具类
public static class CommandLine
{
	//获取启动文件名
	public static string GetStartPath( string[] args )
	{
		if( ( args != null ) && ( args.Length > 0 ) ) {
			string StartFileName = null;
			string filePath = "";
			int si = 0;
			for( int i = si; i < args.Length; ++i ) {
				filePath += " " + args[ i ];
				
				//MessageBox.Show( args[ i ] );
			}
			StartFileName = filePath.Trim( ' ' );
			
			//MessageBox.Show( "<" + StartFileName + ">" );
			//MessageBox.Show( ":" + Environment.CommandLine + ":" );
			
			
			//这种方法，双击文件的命令行，会在exe文件名加上""，而在cmd里运行，择不含""，兼容性差
			
			/*
			//另一种获取命令行的方法
			string cmd = Environment.CommandLine.Remove( 0, 1 );
			cmd = cmd.Remove( 0, cmd.IndexOf( '"' ) + 2 );
			
			//有的系统上命令行的第二个文件名也会带有双引号, 需要去除
			if( cmd.StartsWith( "\"" ) || cmd.EndsWith( "\"" ) ) {
				cmd = cmd.Trim( "\"".ToCharArray() );
			}
			if( StartFileName != cmd ) {
				StartFileName = cmd;
			}
			MessageBox.Show( "<" + StartFileName + ">" );
			*/
			
			return StartFileName;
		}
		return null;
	}
}
}



