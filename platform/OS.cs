﻿
//#define OS_ANDROID
//#define OS_LINUX
#define OS_WIN


namespace n_OS
{
using System;

#if OS_WIN
	using System.IO;
#endif

#if OS_ANDROID
	using test20140822;
	using Android.Widget;
	using Android.App;
#endif

public static class PFunc
{
	public static class PinYin
	{
		public static string GetPinyin( string name )
		{
			#if OS_WIN
				return NPinyin.Pinyin.GetPinyin( name );
			#endif
			
			#if OS_LINUX
				return name;
			#endif
			
			#if OS_ANDROID
				return name;
			#endif
		}
	}
}
public static class OS
{
	//系统文件所在根目录的父目录 - 暂时无用
	public static string SystemPrePath_temp;
	
	//系统文件所在根目录
	public static string SystemRoot;
	
	//组件库根目录
	public static string ModuleLibPath;
	
	//用户组件库根目录
	public static string UserLibName;
	
	//编译器系统文件所在根目录
	public static string CompileRoot;
	
	//文件路径的分隔符
	public static string PATH_S;
	
	//文件路径的分隔符
	public static char PATH_SC;
	
	//用户配置符号
	public static string USER;
	
	//目前的用户
	public const string USER_linkboy = "linkboy";
	
	//发行版扩展信息, 如截止期限等
	public static string ExtMessage;
	
	//扩展名
	public static string LinkboyFile;
	public const string CruxFile = "cx";
	
	//判断软件是否为图形界面
	public static bool isGForm;
	
	//初始化
	public static void Init()
	{
		//-------------------------------------
		#if OS_ANDROID
			PATH_S = @"/";
			PATH_SC = '/';
			SystemRoot = "";
		#elif OS_WIN
			PATH_S = @"\";
			PATH_SC = '\\';
			SystemRoot = AppDomain.CurrentDomain.BaseDirectory;
		#elif OS_LINUX
			PATH_S = "/";
			PATH_SC = '/';
			SystemRoot = AppDomain.CurrentDomain.BaseDirectory;
		#else
			#error "操作系统未定义"
		#endif
		
		int i = SystemRoot.Remove( SystemRoot.Length - 1 ).LastIndexOf( PATH_SC );
		SystemPrePath_temp = SystemRoot.Remove( i + 1 );
		
		//-------------------------------------
		//输入输出初始化
		VIO.Init();
		
		//-------------------------------------
		//路径无关项
		ModuleLibPath = SystemRoot + "Lib" + OS.PATH_S;
		CompileRoot = SystemRoot;
		
		ExtMessage = null;
		string extpath = SystemRoot + "RV" + PATH_S + "config.txt";
		if( File.Exists( extpath ) ) {
			string[] temp = VIO.OpenTextFileGB2312( extpath ).Split( '\n' )[0].Split( ' ' );
			
			UserLibName = "第三方模块库";
			USER = temp[0];
			LinkboyFile = temp[1];
			
			if( temp.Length > 2 ) {
				ExtMessage = temp[2];
			}
		}
	}
}
public static class VIO
{
	/*
	#if OS_WIN
		static System.Windows.Forms.RichTextBox r;
	#elif OS_ANDROID
		public static Activity ac;
	#endif
	*/
	
	//初始化
	public static void Init()
	{
		/*
		#if OS_WIN
			r = new System.Windows.Forms.RichTextBox();
		#endif
		*/
	}
	/*
	//打开RTF文件并提取文本
	public static string OpenRtfFile( string FileName )
	{
		r.LoadFile( FileName );
		return r.Text;
	}
	*/
	
	//打开文本文件
	public static string OpenTextFileGB2312( string FileName )
	{
		#if OS_LINUX
		System.Text.Encoding.RegisterProvider (System.Text.CodePagesEncodingProvider.Instance);
		#endif
		
		string s = File.ReadAllText( FileName, System.Text.Encoding.GetEncoding("gb2312") );
		return s.Replace( "\r\n", "\n" );
		
		//#elif OS_ANDROID
		//	return A_IO.Open( FileName );
		//#endif
	}
	
	//保存文本文件
	public static void SaveTextFileGB2312( string FileName, string Text )
	{
		Text = Text.Replace( "\r\n", "\n" );
		Text = Text.Replace( "\n", "\r\n" );
		
		#if OS_LINUX
		System.Text.Encoding.RegisterProvider (System.Text.CodePagesEncodingProvider.Instance);
		#endif
		
		System.IO.File.WriteAllText( FileName, Text, System.Text.Encoding.GetEncoding( "gb2312" ) );
	}
	
	//打开文本文件
	public static string OpenTextFileUTF8( string FileName )
	{
		//#if OS_WIN
			
			string s = File.ReadAllText( FileName, System.Text.Encoding.GetEncoding("utf-8") );
			return s.Replace( "\r\n", "\n" );
			
		//#elif OS_ANDROID
		//	return A_IO.Open( FileName );
		//#endif
	}
	
	//保存文本文件
	public static void SaveTextFileUTF8( string FileName, string Text )
	{
		Text = Text.Replace( "\r\n", "\n" );
		Text = Text.Replace( "\n", "\r\n" );
		
		//保存时带有 BOM 标记
		//System.IO.File.WriteAllText( FileName, Text, System.Text.Encoding.GetEncoding( "utf-8" ) );
		
		//这个也是无BOM格式 - 未测试
		//System.Text.UTF8Encoding utf8 = new System.Text.UTF8Encoding(false);
		//File.WriteAllText(FilePath, strContent, utf8);
		
		//以UTF-8不带BOM格式重新写入文件
		System.Text.Encoding end = new System.Text.UTF8Encoding(false);
		using( StreamWriter sw = new StreamWriter( FileName, false, end) )
		{
			sw.Write( Text );
		}
	}
	
	//判断文件是否存在
	public static bool FileExists( string Path )
	{
		
		return File.Exists( Path );
		
		//#elif OS_ANDROID
		//	return true;
		//#else
		//	#error "操作系统未定义"
		//#endif
	}
	
	//输出一个信息
	public static void Show( string m )
	{
		#if OS_WIN
			System.Windows.Forms.MessageBox.Show( "<" + m + ">" );
		#elif OS_LINUX
			//System.Windows.Forms.MessageBox.Show( "<" + m + ">" );
		#elif OS_ANDROID
			Toast.MakeText( ac, m, Android.Widget.ToastLength.Long ).Show();
		#endif
		
	}
}
}





