﻿
using System;
using n_UARTForm;
using System.Windows.Forms;


namespace CommTool
{
	class Program
	{
		public static void Main(string[] args)
		{
			Application.EnableVisualStyles();
			Application.SetCompatibleTextRenderingDefault(false);
			
			UARTForm f = new UARTForm();
			f.SingleMode = true;
			Application.Run( f );
		}
	}
}


