﻿
namespace n_BigiotForm
{
using System;
using System.Windows.Forms;

/// <summary>
/// Description of WorkForm.
/// </summary>
public partial class BigiotForm : Form
{
	public delegate void D_TextSend( string t );
	public D_TextSend d_TextSend;
	
	public delegate void D_SysSend();
	public D_SysSend d_SysSend;
	
	int TimeTick;
	
	//主窗口
	public BigiotForm()
	{
		//
		// The InitializeComponent() call is required for Windows Forms designer support.
		//
		InitializeComponent();
		this.MinimumSize = this.Size;
		this.MaximumSize = this.Size;
		//初始化
		//
		// TODO: Add constructor code after the InitializeComponent() call.
		//
	}
	
	//窗体运行
	public void Run()
	{
		this.Visible = true;
	}
	
	//窗体关闭事件
	void FindFormFormClosing(object sender, FormClosingEventArgs e)
	{
		this.Visible = false;
		e.Cancel = true;
	}
	
	//发送按钮按下时
	void ButtonClick(object sender, EventArgs e)
	{
		if( d_TextSend != null ) {
			string s = ((Button)sender).Text;
			s = s.Split( '\n' )[1];
			d_TextSend( s );
		}
	}
	
	void CheckBox1CheckedChanged(object sender, EventArgs e)
	{
		if( checkBox1.Checked ) {
			TimeTick = 49;
			timer1.Enabled = true;
		}
		else {
			timer1.Enabled = false;
		}
	}
	
	void Timer1Tick(object sender, EventArgs e)
	{
		TimeTick++;
		
		if( TimeTick >= 50 ) {
			TimeTick = 0;
			
			ButtonClick( button2, null );
			if( d_SysSend != null ) {
				d_SysSend();
			}
		}
		
		labelTick.Text = (50 - TimeTick) + "秒";
	}
}
}


