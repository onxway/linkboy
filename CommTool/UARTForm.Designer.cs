﻿/*
 * 由 SharpDevelop 创建。
 * 用户: bbb_2
 * 日期: 2009-11-17
 * 时间: 11:15
 * 
 * 如果你想要更改该模板，那么请使用“工具 | 选项 | 编辑 | 编辑标准标题”。
 */
namespace n_UARTForm
{
	partial class UARTForm
	{
		/// <summary>
		/// Designer variable used to keep track of non-visual components.
		/// </summary>
		private System.ComponentModel.IContainer components = null;
		
		/// <summary>
		/// Disposes resources used by the form.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing) {
				if (components != null) {
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}
		
		/// <summary>
		/// This method is required for Windows Forms designer support.
		/// Do not change the method contents inside the source code editor. The Forms designer might
		/// not be able to load this method if it was changed manually.
		/// </summary>
		private void InitializeComponent()
		{
			this.components = new System.ComponentModel.Container();
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(UARTForm));
			this.panel1 = new System.Windows.Forms.Panel();
			this.panel6 = new System.Windows.Forms.Panel();
			this.richTextBox接收 = new System.Windows.Forms.RichTextBox();
			this.richTextBox1 = new System.Windows.Forms.RichTextBox();
			this.richTextBox2 = new System.Windows.Forms.RichTextBox();
			this.panel4 = new System.Windows.Forms.Panel();
			this.comboBox波特率 = new System.Windows.Forms.ComboBox();
			this.checkBoxAutoClear = new System.Windows.Forms.CheckBox();
			this.button清空 = new System.Windows.Forms.Button();
			this.button打开串口 = new System.Windows.Forms.Button();
			this.checkBoxDTR_RTS = new System.Windows.Forms.CheckBox();
			this.radioButton显示_字符形式 = new System.Windows.Forms.RadioButton();
			this.radioButton显示_十进制数字形式 = new System.Windows.Forms.RadioButton();
			this.radioButton显示_十六进制数字形式 = new System.Windows.Forms.RadioButton();
			this.comboBox串口号 = new System.Windows.Forms.ComboBox();
			this.panel3 = new System.Windows.Forms.Panel();
			this.labelShowHex = new System.Windows.Forms.Label();
			this.richTextBox发送 = new System.Windows.Forms.RichTextBox();
			this.button发送 = new System.Windows.Forms.Button();
			this.panel5 = new System.Windows.Forms.Panel();
			this.label4 = new System.Windows.Forms.Label();
			this.label2 = new System.Windows.Forms.Label();
			this.label1 = new System.Windows.Forms.Label();
			this.radioButton十六进制数字 = new System.Windows.Forms.RadioButton();
			this.checkBox自动发送 = new System.Windows.Forms.CheckBox();
			this.textBoxSendT = new System.Windows.Forms.TextBox();
			this.radioButton十进制数字 = new System.Windows.Forms.RadioButton();
			this.label3 = new System.Windows.Forms.Label();
			this.radioButton字符形式 = new System.Windows.Forms.RadioButton();
			this.timerAutoSend = new System.Windows.Forms.Timer(this.components);
			this.timer1 = new System.Windows.Forms.Timer(this.components);
			this.button1 = new System.Windows.Forms.Button();
			this.label5 = new System.Windows.Forms.Label();
			this.button2 = new System.Windows.Forms.Button();
			this.button3 = new System.Windows.Forms.Button();
			this.button9 = new System.Windows.Forms.Button();
			this.label6 = new System.Windows.Forms.Label();
			this.buttonApp = new System.Windows.Forms.Button();
			this.buttonBigiot = new System.Windows.Forms.Button();
			this.labelMes = new System.Windows.Forms.Label();
			this.panel8266 = new System.Windows.Forms.Panel();
			this.panelModbus = new System.Windows.Forms.Panel();
			this.textBoxFuncCode10 = new System.Windows.Forms.TextBox();
			this.label15 = new System.Windows.Forms.Label();
			this.textBoxNumber10 = new System.Windows.Forms.TextBox();
			this.textBoxStartAddr10 = new System.Windows.Forms.TextBox();
			this.textBoxSlaAddr10 = new System.Windows.Forms.TextBox();
			this.textBoxFuncCode = new System.Windows.Forms.TextBox();
			this.label14 = new System.Windows.Forms.Label();
			this.labelModbusR = new System.Windows.Forms.Label();
			this.label13 = new System.Windows.Forms.Label();
			this.buttonPack = new System.Windows.Forms.Button();
			this.textBoxNumber = new System.Windows.Forms.TextBox();
			this.textBoxStartAddr = new System.Windows.Forms.TextBox();
			this.textBoxSlaAddr = new System.Windows.Forms.TextBox();
			this.label9 = new System.Windows.Forms.Label();
			this.label10 = new System.Windows.Forms.Label();
			this.label11 = new System.Windows.Forms.Label();
			this.label7 = new System.Windows.Forms.Label();
			this.panel8 = new System.Windows.Forms.Panel();
			this.button8266 = new System.Windows.Forms.Button();
			this.label8 = new System.Windows.Forms.Label();
			this.buttonModbus = new System.Windows.Forms.Button();
			this.label12 = new System.Windows.Forms.Label();
			this.timerModbus = new System.Windows.Forms.Timer(this.components);
			this.panel1.SuspendLayout();
			this.panel6.SuspendLayout();
			this.panel4.SuspendLayout();
			this.panel3.SuspendLayout();
			this.panel5.SuspendLayout();
			this.panel8266.SuspendLayout();
			this.panelModbus.SuspendLayout();
			this.panel8.SuspendLayout();
			this.SuspendLayout();
			// 
			// panel1
			// 
			this.panel1.Controls.Add(this.panel6);
			this.panel1.Controls.Add(this.panel4);
			this.panel1.Controls.Add(this.panel3);
			this.panel1.Controls.Add(this.panel5);
			resources.ApplyResources(this.panel1, "panel1");
			this.panel1.Name = "panel1";
			// 
			// panel6
			// 
			this.panel6.Controls.Add(this.richTextBox接收);
			this.panel6.Controls.Add(this.richTextBox1);
			this.panel6.Controls.Add(this.richTextBox2);
			resources.ApplyResources(this.panel6, "panel6");
			this.panel6.Name = "panel6";
			// 
			// richTextBox接收
			// 
			this.richTextBox接收.BorderStyle = System.Windows.Forms.BorderStyle.None;
			resources.ApplyResources(this.richTextBox接收, "richTextBox接收");
			this.richTextBox接收.Name = "richTextBox接收";
			// 
			// richTextBox1
			// 
			this.richTextBox1.BackColor = System.Drawing.Color.WhiteSmoke;
			this.richTextBox1.BorderStyle = System.Windows.Forms.BorderStyle.None;
			resources.ApplyResources(this.richTextBox1, "richTextBox1");
			this.richTextBox1.ForeColor = System.Drawing.Color.DarkGray;
			this.richTextBox1.Name = "richTextBox1";
			this.richTextBox1.ReadOnly = true;
			// 
			// richTextBox2
			// 
			this.richTextBox2.BackColor = System.Drawing.Color.WhiteSmoke;
			this.richTextBox2.BorderStyle = System.Windows.Forms.BorderStyle.None;
			resources.ApplyResources(this.richTextBox2, "richTextBox2");
			this.richTextBox2.ForeColor = System.Drawing.Color.DarkGray;
			this.richTextBox2.Name = "richTextBox2";
			this.richTextBox2.ReadOnly = true;
			// 
			// panel4
			// 
			this.panel4.BackColor = System.Drawing.Color.WhiteSmoke;
			this.panel4.Controls.Add(this.comboBox波特率);
			this.panel4.Controls.Add(this.checkBoxAutoClear);
			this.panel4.Controls.Add(this.button清空);
			this.panel4.Controls.Add(this.button打开串口);
			this.panel4.Controls.Add(this.checkBoxDTR_RTS);
			this.panel4.Controls.Add(this.radioButton显示_字符形式);
			this.panel4.Controls.Add(this.radioButton显示_十进制数字形式);
			this.panel4.Controls.Add(this.radioButton显示_十六进制数字形式);
			this.panel4.Controls.Add(this.comboBox串口号);
			resources.ApplyResources(this.panel4, "panel4");
			this.panel4.Name = "panel4";
			// 
			// comboBox波特率
			// 
			resources.ApplyResources(this.comboBox波特率, "comboBox波特率");
			this.comboBox波特率.FormattingEnabled = true;
			this.comboBox波特率.Items.AddRange(new object[] {
									resources.GetString("comboBox波特率.Items"),
									resources.GetString("comboBox波特率.Items1"),
									resources.GetString("comboBox波特率.Items2"),
									resources.GetString("comboBox波特率.Items3"),
									resources.GetString("comboBox波特率.Items4"),
									resources.GetString("comboBox波特率.Items5"),
									resources.GetString("comboBox波特率.Items6"),
									resources.GetString("comboBox波特率.Items7"),
									resources.GetString("comboBox波特率.Items8")});
			this.comboBox波特率.Name = "comboBox波特率";
			// 
			// checkBoxAutoClear
			// 
			resources.ApplyResources(this.checkBoxAutoClear, "checkBoxAutoClear");
			this.checkBoxAutoClear.ForeColor = System.Drawing.Color.CadetBlue;
			this.checkBoxAutoClear.Name = "checkBoxAutoClear";
			this.checkBoxAutoClear.UseVisualStyleBackColor = true;
			// 
			// button清空
			// 
			this.button清空.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(189)))), ((int)(((byte)(219)))), ((int)(((byte)(236)))));
			resources.ApplyResources(this.button清空, "button清空");
			this.button清空.ForeColor = System.Drawing.Color.Black;
			this.button清空.Name = "button清空";
			this.button清空.UseVisualStyleBackColor = false;
			this.button清空.Click += new System.EventHandler(this.Button清空Click);
			// 
			// button打开串口
			// 
			this.button打开串口.BackColor = System.Drawing.Color.LightGray;
			resources.ApplyResources(this.button打开串口, "button打开串口");
			this.button打开串口.ForeColor = System.Drawing.Color.Black;
			this.button打开串口.Name = "button打开串口";
			this.button打开串口.UseVisualStyleBackColor = false;
			this.button打开串口.Click += new System.EventHandler(this.Button打开串口Click);
			// 
			// checkBoxDTR_RTS
			// 
			resources.ApplyResources(this.checkBoxDTR_RTS, "checkBoxDTR_RTS");
			this.checkBoxDTR_RTS.ForeColor = System.Drawing.Color.Black;
			this.checkBoxDTR_RTS.Name = "checkBoxDTR_RTS";
			this.checkBoxDTR_RTS.UseVisualStyleBackColor = true;
			// 
			// radioButton显示_字符形式
			// 
			this.radioButton显示_字符形式.Checked = true;
			resources.ApplyResources(this.radioButton显示_字符形式, "radioButton显示_字符形式");
			this.radioButton显示_字符形式.ForeColor = System.Drawing.Color.Black;
			this.radioButton显示_字符形式.Name = "radioButton显示_字符形式";
			this.radioButton显示_字符形式.TabStop = true;
			this.radioButton显示_字符形式.UseVisualStyleBackColor = true;
			// 
			// radioButton显示_十进制数字形式
			// 
			resources.ApplyResources(this.radioButton显示_十进制数字形式, "radioButton显示_十进制数字形式");
			this.radioButton显示_十进制数字形式.ForeColor = System.Drawing.Color.Black;
			this.radioButton显示_十进制数字形式.Name = "radioButton显示_十进制数字形式";
			this.radioButton显示_十进制数字形式.UseVisualStyleBackColor = true;
			// 
			// radioButton显示_十六进制数字形式
			// 
			resources.ApplyResources(this.radioButton显示_十六进制数字形式, "radioButton显示_十六进制数字形式");
			this.radioButton显示_十六进制数字形式.ForeColor = System.Drawing.Color.Black;
			this.radioButton显示_十六进制数字形式.Name = "radioButton显示_十六进制数字形式";
			this.radioButton显示_十六进制数字形式.UseVisualStyleBackColor = true;
			// 
			// comboBox串口号
			// 
			this.comboBox串口号.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
			resources.ApplyResources(this.comboBox串口号, "comboBox串口号");
			this.comboBox串口号.FormattingEnabled = true;
			this.comboBox串口号.Name = "comboBox串口号";
			this.comboBox串口号.Click += new System.EventHandler(this.ComboBox串口号Click);
			// 
			// panel3
			// 
			this.panel3.BackColor = System.Drawing.Color.WhiteSmoke;
			this.panel3.Controls.Add(this.labelShowHex);
			this.panel3.Controls.Add(this.richTextBox发送);
			this.panel3.Controls.Add(this.button发送);
			resources.ApplyResources(this.panel3, "panel3");
			this.panel3.Name = "panel3";
			// 
			// labelShowHex
			// 
			resources.ApplyResources(this.labelShowHex, "labelShowHex");
			this.labelShowHex.BackColor = System.Drawing.Color.WhiteSmoke;
			this.labelShowHex.ForeColor = System.Drawing.Color.White;
			this.labelShowHex.Name = "labelShowHex";
			// 
			// richTextBox发送
			// 
			resources.ApplyResources(this.richTextBox发送, "richTextBox发送");
			this.richTextBox发送.BackColor = System.Drawing.Color.White;
			this.richTextBox发送.BorderStyle = System.Windows.Forms.BorderStyle.None;
			this.richTextBox发送.Name = "richTextBox发送";
			this.richTextBox发送.TextChanged += new System.EventHandler(this.RichTextBox发送TextChanged);
			// 
			// button发送
			// 
			this.button发送.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(224)))), ((int)(((byte)(192)))));
			resources.ApplyResources(this.button发送, "button发送");
			this.button发送.ForeColor = System.Drawing.Color.Black;
			this.button发送.Name = "button发送";
			this.button发送.UseVisualStyleBackColor = false;
			this.button发送.Click += new System.EventHandler(this.Button发送Click);
			// 
			// panel5
			// 
			this.panel5.BackColor = System.Drawing.Color.WhiteSmoke;
			this.panel5.Controls.Add(this.label4);
			this.panel5.Controls.Add(this.label2);
			this.panel5.Controls.Add(this.label1);
			this.panel5.Controls.Add(this.radioButton十六进制数字);
			this.panel5.Controls.Add(this.checkBox自动发送);
			this.panel5.Controls.Add(this.textBoxSendT);
			this.panel5.Controls.Add(this.radioButton十进制数字);
			this.panel5.Controls.Add(this.label3);
			this.panel5.Controls.Add(this.radioButton字符形式);
			resources.ApplyResources(this.panel5, "panel5");
			this.panel5.Name = "panel5";
			// 
			// label4
			// 
			resources.ApplyResources(this.label4, "label4");
			this.label4.ForeColor = System.Drawing.Color.Chocolate;
			this.label4.Name = "label4";
			// 
			// label2
			// 
			resources.ApplyResources(this.label2, "label2");
			this.label2.ForeColor = System.Drawing.Color.Chocolate;
			this.label2.Name = "label2";
			// 
			// label1
			// 
			resources.ApplyResources(this.label1, "label1");
			this.label1.ForeColor = System.Drawing.Color.Chocolate;
			this.label1.Name = "label1";
			// 
			// radioButton十六进制数字
			// 
			resources.ApplyResources(this.radioButton十六进制数字, "radioButton十六进制数字");
			this.radioButton十六进制数字.ForeColor = System.Drawing.Color.Black;
			this.radioButton十六进制数字.Name = "radioButton十六进制数字";
			this.radioButton十六进制数字.UseVisualStyleBackColor = true;
			// 
			// checkBox自动发送
			// 
			resources.ApplyResources(this.checkBox自动发送, "checkBox自动发送");
			this.checkBox自动发送.ForeColor = System.Drawing.Color.Black;
			this.checkBox自动发送.Name = "checkBox自动发送";
			this.checkBox自动发送.UseVisualStyleBackColor = true;
			this.checkBox自动发送.CheckedChanged += new System.EventHandler(this.CheckBox自动发送CheckedChanged);
			// 
			// textBoxSendT
			// 
			this.textBoxSendT.BorderStyle = System.Windows.Forms.BorderStyle.None;
			resources.ApplyResources(this.textBoxSendT, "textBoxSendT");
			this.textBoxSendT.Name = "textBoxSendT";
			this.textBoxSendT.TextChanged += new System.EventHandler(this.TextBoxSendTTextChanged);
			// 
			// radioButton十进制数字
			// 
			resources.ApplyResources(this.radioButton十进制数字, "radioButton十进制数字");
			this.radioButton十进制数字.ForeColor = System.Drawing.Color.Black;
			this.radioButton十进制数字.Name = "radioButton十进制数字";
			this.radioButton十进制数字.UseVisualStyleBackColor = true;
			// 
			// label3
			// 
			resources.ApplyResources(this.label3, "label3");
			this.label3.ForeColor = System.Drawing.Color.Black;
			this.label3.Name = "label3";
			// 
			// radioButton字符形式
			// 
			this.radioButton字符形式.Checked = true;
			resources.ApplyResources(this.radioButton字符形式, "radioButton字符形式");
			this.radioButton字符形式.ForeColor = System.Drawing.Color.Black;
			this.radioButton字符形式.Name = "radioButton字符形式";
			this.radioButton字符形式.TabStop = true;
			this.radioButton字符形式.UseVisualStyleBackColor = true;
			// 
			// timerAutoSend
			// 
			this.timerAutoSend.Interval = 500;
			this.timerAutoSend.Tick += new System.EventHandler(this.TimerAutoSendTick);
			// 
			// timer1
			// 
			this.timer1.Interval = 50;
			this.timer1.Tick += new System.EventHandler(this.Timer1Tick);
			// 
			// button1
			// 
			this.button1.BackColor = System.Drawing.Color.DarkKhaki;
			resources.ApplyResources(this.button1, "button1");
			this.button1.ForeColor = System.Drawing.Color.Black;
			this.button1.Name = "button1";
			this.button1.UseVisualStyleBackColor = false;
			this.button1.Click += new System.EventHandler(this.ButtonATClick);
			// 
			// label5
			// 
			this.label5.BackColor = System.Drawing.Color.WhiteSmoke;
			resources.ApplyResources(this.label5, "label5");
			this.label5.ForeColor = System.Drawing.Color.SlateGray;
			this.label5.Name = "label5";
			// 
			// button2
			// 
			this.button2.BackColor = System.Drawing.Color.LightSteelBlue;
			resources.ApplyResources(this.button2, "button2");
			this.button2.ForeColor = System.Drawing.Color.Black;
			this.button2.Name = "button2";
			this.button2.UseVisualStyleBackColor = false;
			this.button2.Click += new System.EventHandler(this.ButtonATClick);
			// 
			// button3
			// 
			this.button3.BackColor = System.Drawing.Color.LightSteelBlue;
			resources.ApplyResources(this.button3, "button3");
			this.button3.ForeColor = System.Drawing.Color.Black;
			this.button3.Name = "button3";
			this.button3.UseVisualStyleBackColor = false;
			this.button3.Click += new System.EventHandler(this.ButtonATClick);
			// 
			// button9
			// 
			this.button9.BackColor = System.Drawing.Color.LightSteelBlue;
			resources.ApplyResources(this.button9, "button9");
			this.button9.ForeColor = System.Drawing.Color.Black;
			this.button9.Name = "button9";
			this.button9.UseVisualStyleBackColor = false;
			this.button9.Click += new System.EventHandler(this.ButtonATClick);
			// 
			// label6
			// 
			resources.ApplyResources(this.label6, "label6");
			this.label6.ForeColor = System.Drawing.Color.DimGray;
			this.label6.Name = "label6";
			// 
			// buttonApp
			// 
			this.buttonApp.BackColor = System.Drawing.Color.DarkSeaGreen;
			resources.ApplyResources(this.buttonApp, "buttonApp");
			this.buttonApp.ForeColor = System.Drawing.Color.Black;
			this.buttonApp.Name = "buttonApp";
			this.buttonApp.UseVisualStyleBackColor = false;
			this.buttonApp.Click += new System.EventHandler(this.ButtonAppClick);
			// 
			// buttonBigiot
			// 
			this.buttonBigiot.BackColor = System.Drawing.Color.DarkSeaGreen;
			resources.ApplyResources(this.buttonBigiot, "buttonBigiot");
			this.buttonBigiot.ForeColor = System.Drawing.Color.Black;
			this.buttonBigiot.Name = "buttonBigiot";
			this.buttonBigiot.UseVisualStyleBackColor = false;
			this.buttonBigiot.Click += new System.EventHandler(this.ButtonBigiotClick);
			// 
			// labelMes
			// 
			resources.ApplyResources(this.labelMes, "labelMes");
			this.labelMes.ForeColor = System.Drawing.Color.Red;
			this.labelMes.Name = "labelMes";
			this.labelMes.Click += new System.EventHandler(this.LabelMesClick);
			// 
			// panel8266
			// 
			this.panel8266.BackColor = System.Drawing.Color.WhiteSmoke;
			this.panel8266.Controls.Add(this.buttonBigiot);
			this.panel8266.Controls.Add(this.buttonApp);
			this.panel8266.Controls.Add(this.label6);
			this.panel8266.Controls.Add(this.button9);
			this.panel8266.Controls.Add(this.button3);
			this.panel8266.Controls.Add(this.button2);
			this.panel8266.Controls.Add(this.label5);
			this.panel8266.Controls.Add(this.button1);
			resources.ApplyResources(this.panel8266, "panel8266");
			this.panel8266.Name = "panel8266";
			// 
			// panelModbus
			// 
			this.panelModbus.BackColor = System.Drawing.Color.WhiteSmoke;
			this.panelModbus.Controls.Add(this.textBoxFuncCode10);
			this.panelModbus.Controls.Add(this.label15);
			this.panelModbus.Controls.Add(this.textBoxNumber10);
			this.panelModbus.Controls.Add(this.textBoxStartAddr10);
			this.panelModbus.Controls.Add(this.textBoxSlaAddr10);
			this.panelModbus.Controls.Add(this.textBoxFuncCode);
			this.panelModbus.Controls.Add(this.label14);
			this.panelModbus.Controls.Add(this.labelModbusR);
			this.panelModbus.Controls.Add(this.label13);
			this.panelModbus.Controls.Add(this.buttonPack);
			this.panelModbus.Controls.Add(this.textBoxNumber);
			this.panelModbus.Controls.Add(this.textBoxStartAddr);
			this.panelModbus.Controls.Add(this.textBoxSlaAddr);
			this.panelModbus.Controls.Add(this.label9);
			this.panelModbus.Controls.Add(this.label10);
			this.panelModbus.Controls.Add(this.label11);
			this.panelModbus.Controls.Add(this.label7);
			resources.ApplyResources(this.panelModbus, "panelModbus");
			this.panelModbus.Name = "panelModbus";
			// 
			// textBoxFuncCode10
			// 
			resources.ApplyResources(this.textBoxFuncCode10, "textBoxFuncCode10");
			this.textBoxFuncCode10.Name = "textBoxFuncCode10";
			this.textBoxFuncCode10.ReadOnly = true;
			// 
			// label15
			// 
			this.label15.BackColor = System.Drawing.Color.Transparent;
			resources.ApplyResources(this.label15, "label15");
			this.label15.ForeColor = System.Drawing.Color.SlateGray;
			this.label15.Name = "label15";
			// 
			// textBoxNumber10
			// 
			resources.ApplyResources(this.textBoxNumber10, "textBoxNumber10");
			this.textBoxNumber10.Name = "textBoxNumber10";
			this.textBoxNumber10.ReadOnly = true;
			// 
			// textBoxStartAddr10
			// 
			resources.ApplyResources(this.textBoxStartAddr10, "textBoxStartAddr10");
			this.textBoxStartAddr10.Name = "textBoxStartAddr10";
			this.textBoxStartAddr10.ReadOnly = true;
			// 
			// textBoxSlaAddr10
			// 
			resources.ApplyResources(this.textBoxSlaAddr10, "textBoxSlaAddr10");
			this.textBoxSlaAddr10.Name = "textBoxSlaAddr10";
			this.textBoxSlaAddr10.ReadOnly = true;
			// 
			// textBoxFuncCode
			// 
			resources.ApplyResources(this.textBoxFuncCode, "textBoxFuncCode");
			this.textBoxFuncCode.Name = "textBoxFuncCode";
			this.textBoxFuncCode.TextChanged += new System.EventHandler(this.TextBoxTextChanged);
			// 
			// label14
			// 
			resources.ApplyResources(this.label14, "label14");
			this.label14.Name = "label14";
			// 
			// labelModbusR
			// 
			this.labelModbusR.BackColor = System.Drawing.Color.Gainsboro;
			resources.ApplyResources(this.labelModbusR, "labelModbusR");
			this.labelModbusR.ForeColor = System.Drawing.Color.Black;
			this.labelModbusR.Name = "labelModbusR";
			this.labelModbusR.Click += new System.EventHandler(this.LabelModbusRClick);
			// 
			// label13
			// 
			this.label13.BackColor = System.Drawing.Color.Transparent;
			resources.ApplyResources(this.label13, "label13");
			this.label13.ForeColor = System.Drawing.Color.SlateGray;
			this.label13.Name = "label13";
			// 
			// buttonPack
			// 
			this.buttonPack.BackColor = System.Drawing.Color.DarkSeaGreen;
			resources.ApplyResources(this.buttonPack, "buttonPack");
			this.buttonPack.ForeColor = System.Drawing.Color.White;
			this.buttonPack.Name = "buttonPack";
			this.buttonPack.UseVisualStyleBackColor = false;
			this.buttonPack.Click += new System.EventHandler(this.ButtonPackClick);
			// 
			// textBoxNumber
			// 
			resources.ApplyResources(this.textBoxNumber, "textBoxNumber");
			this.textBoxNumber.Name = "textBoxNumber";
			this.textBoxNumber.TextChanged += new System.EventHandler(this.TextBoxTextChanged);
			// 
			// textBoxStartAddr
			// 
			resources.ApplyResources(this.textBoxStartAddr, "textBoxStartAddr");
			this.textBoxStartAddr.Name = "textBoxStartAddr";
			this.textBoxStartAddr.TextChanged += new System.EventHandler(this.TextBoxTextChanged);
			// 
			// textBoxSlaAddr
			// 
			resources.ApplyResources(this.textBoxSlaAddr, "textBoxSlaAddr");
			this.textBoxSlaAddr.Name = "textBoxSlaAddr";
			this.textBoxSlaAddr.TextChanged += new System.EventHandler(this.TextBoxTextChanged);
			// 
			// label9
			// 
			resources.ApplyResources(this.label9, "label9");
			this.label9.Name = "label9";
			// 
			// label10
			// 
			resources.ApplyResources(this.label10, "label10");
			this.label10.Name = "label10";
			// 
			// label11
			// 
			resources.ApplyResources(this.label11, "label11");
			this.label11.Name = "label11";
			// 
			// label7
			// 
			this.label7.BackColor = System.Drawing.Color.WhiteSmoke;
			resources.ApplyResources(this.label7, "label7");
			this.label7.ForeColor = System.Drawing.Color.SlateGray;
			this.label7.Name = "label7";
			// 
			// panel8
			// 
			this.panel8.BackColor = System.Drawing.Color.WhiteSmoke;
			this.panel8.Controls.Add(this.labelMes);
			this.panel8.Controls.Add(this.button8266);
			this.panel8.Controls.Add(this.label8);
			this.panel8.Controls.Add(this.buttonModbus);
			this.panel8.Controls.Add(this.label12);
			resources.ApplyResources(this.panel8, "panel8");
			this.panel8.Name = "panel8";
			// 
			// button8266
			// 
			this.button8266.BackColor = System.Drawing.Color.CornflowerBlue;
			resources.ApplyResources(this.button8266, "button8266");
			this.button8266.ForeColor = System.Drawing.Color.White;
			this.button8266.Name = "button8266";
			this.button8266.UseVisualStyleBackColor = false;
			this.button8266.Click += new System.EventHandler(this.Button8266Click);
			// 
			// label8
			// 
			this.label8.BackColor = System.Drawing.Color.Transparent;
			resources.ApplyResources(this.label8, "label8");
			this.label8.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(48)))), ((int)(((byte)(142)))), ((int)(((byte)(220)))));
			this.label8.Name = "label8";
			// 
			// buttonModbus
			// 
			this.buttonModbus.BackColor = System.Drawing.Color.CornflowerBlue;
			resources.ApplyResources(this.buttonModbus, "buttonModbus");
			this.buttonModbus.ForeColor = System.Drawing.Color.White;
			this.buttonModbus.Name = "buttonModbus";
			this.buttonModbus.UseVisualStyleBackColor = false;
			this.buttonModbus.Click += new System.EventHandler(this.ButtonModbusClick);
			// 
			// label12
			// 
			this.label12.BackColor = System.Drawing.Color.Gainsboro;
			resources.ApplyResources(this.label12, "label12");
			this.label12.Name = "label12";
			// 
			// timerModbus
			// 
			this.timerModbus.Interval = 10;
			this.timerModbus.Tick += new System.EventHandler(this.TimerModbusTick);
			// 
			// UARTForm
			// 
			resources.ApplyResources(this, "$this");
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.BackColor = System.Drawing.Color.WhiteSmoke;
			this.Controls.Add(this.panel1);
			this.Controls.Add(this.panel8);
			this.Controls.Add(this.panelModbus);
			this.Controls.Add(this.panel8266);
			this.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
			this.Name = "UARTForm";
			this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.UARTFormFormClosing);
			this.panel1.ResumeLayout(false);
			this.panel6.ResumeLayout(false);
			this.panel4.ResumeLayout(false);
			this.panel3.ResumeLayout(false);
			this.panel5.ResumeLayout(false);
			this.panel5.PerformLayout();
			this.panel8266.ResumeLayout(false);
			this.panelModbus.ResumeLayout(false);
			this.panelModbus.PerformLayout();
			this.panel8.ResumeLayout(false);
			this.ResumeLayout(false);
		}
		private System.Windows.Forms.TextBox textBoxSlaAddr10;
		private System.Windows.Forms.TextBox textBoxStartAddr10;
		private System.Windows.Forms.TextBox textBoxNumber10;
		private System.Windows.Forms.Label label15;
		private System.Windows.Forms.TextBox textBoxFuncCode10;
		private System.Windows.Forms.Label label14;
		private System.Windows.Forms.TextBox textBoxFuncCode;
		private System.Windows.Forms.Timer timerModbus;
		private System.Windows.Forms.Label labelModbusR;
		private System.Windows.Forms.Label label13;
		private System.Windows.Forms.Label label12;
		private System.Windows.Forms.Button buttonPack;
		private System.Windows.Forms.Label label11;
		private System.Windows.Forms.Label label10;
		private System.Windows.Forms.Label label9;
		private System.Windows.Forms.TextBox textBoxSlaAddr;
		private System.Windows.Forms.TextBox textBoxStartAddr;
		private System.Windows.Forms.TextBox textBoxNumber;
		private System.Windows.Forms.Button button8266;
		private System.Windows.Forms.Button buttonModbus;
		private System.Windows.Forms.Label label8;
		private System.Windows.Forms.Label label7;
		private System.Windows.Forms.Panel panel8;
		private System.Windows.Forms.Panel panelModbus;
		private System.Windows.Forms.Timer timer1;
		private System.Windows.Forms.Label labelMes;
		private System.Windows.Forms.Button buttonBigiot;
		private System.Windows.Forms.Button buttonApp;
		private System.Windows.Forms.Button button9;
		private System.Windows.Forms.Label label6;
		private System.Windows.Forms.CheckBox checkBoxAutoClear;
		private System.Windows.Forms.Button button3;
		private System.Windows.Forms.Button button1;
		private System.Windows.Forms.Label label5;
		private System.Windows.Forms.Button button2;
		private System.Windows.Forms.Label label4;
		private System.Windows.Forms.Panel panel6;
		private System.Windows.Forms.Panel panel5;
		private System.Windows.Forms.Label labelShowHex;
		private System.Windows.Forms.Panel panel4;
		private System.Windows.Forms.Panel panel3;
		private System.Windows.Forms.Panel panel8266;
		private System.Windows.Forms.RichTextBox richTextBox2;
		private System.Windows.Forms.RichTextBox richTextBox1;
		private System.Windows.Forms.CheckBox checkBoxDTR_RTS;
		private System.Windows.Forms.RadioButton radioButton显示_字符形式;
		private System.Windows.Forms.RadioButton radioButton显示_十进制数字形式;
		private System.Windows.Forms.RadioButton radioButton显示_十六进制数字形式;
		private System.Windows.Forms.TextBox textBoxSendT;
		private System.Windows.Forms.Label label3;
		private System.Windows.Forms.CheckBox checkBox自动发送;
		private System.Windows.Forms.Timer timerAutoSend;
		private System.Windows.Forms.RadioButton radioButton字符形式;
		private System.Windows.Forms.RadioButton radioButton十进制数字;
		private System.Windows.Forms.RadioButton radioButton十六进制数字;
		private System.Windows.Forms.Label label1;
		private System.Windows.Forms.Label label2;
		private System.Windows.Forms.Panel panel1;
		private System.Windows.Forms.Button button清空;
		private System.Windows.Forms.Button button发送;
		private System.Windows.Forms.RichTextBox richTextBox发送;
		private System.Windows.Forms.RichTextBox richTextBox接收;
		private System.Windows.Forms.ComboBox comboBox波特率;
		private System.Windows.Forms.ComboBox comboBox串口号;
		private System.Windows.Forms.Button button打开串口;
	}
}
