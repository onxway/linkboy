


//**********************************************
//定义内部字节地址存储器

public unit sys
{
	public memory base
	{
		target = iointerface;
		type = [ uint32 34, 127 ] uint8;
	}
	public memory code
	{
		target = iointerface;
		type = [ uint32 100, 0xffff ] uint8;  //[0x0080, 0x1fff],
	}
	unit iointerface
	{
		//uint8 写入和读出
		public uint8 get_uint8( uint32 addr )
		{
		
			#asm "mov a,&addr+2"
			#asm "jnz SYS_get_uint8_code"
			#asm "mov r0,&addr"
			#asm "mov a,@r0"
			#asm "ret"
			
			#asm "SYS_get_uint8_code:"
			#asm "mov dpl,&addr"
			#asm "mov dph,&addr+1"
			#asm "clr a"
			#asm "movc a,@a+dptr"
		}
		public void set_uint8( uint32 addr, uint8 data )
		{
			#asm "mov r0,&addr"
			#asm "mov a,&data"
			#asm "mov @r0,a"
		}
		//uint16 写入和读出
		public uint16 get_uint16( uint32 addr )
		{
			#asm "mov a,&addr+2"
			#asm "jnz SYS_get_uint16_code"
			
			#asm "mov r0,&addr"
			#asm "mov a,@r0"
			#asm "mov b,a"
			#asm "inc r0"
			#asm "mov a,@r0"
			#asm "xch a,b"
			#asm "ret"
			
			#asm "SYS_get_uint16_code:"
			#asm "mov dpl,&addr"
			#asm "mov dph,&addr+1"
			#asm "clr a"
			#asm "movc a,@a+dptr"
			#asm "mov b,a"
			#asm "mov a,#1"
			#asm "movc a,@a+dptr"
			#asm "xch a,b"
		}
		public void set_uint16( uint32 addr, uint16 data )
		{
			#asm "mov r0,&addr"
			#asm "mov a,&data"
			#asm "mov @r0,a"
			#asm "inc r0"
			#asm "mov a,&data+1"
			#asm "mov @r0,a"
		}
		//uint32 写入和读出
		public uint32 get_uint32( uint32 addr )
		{
			
		}
		public void set_uint32( uint32 addr, uint32 data )
		{
			
		}
	}
}













