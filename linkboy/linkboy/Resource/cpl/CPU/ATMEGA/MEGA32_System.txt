
//**********************************************
//定义内部字节地址存储器

public unit sys
{
	public memory base
	{
		target = iointerface;
		type = [ uint32 0x0060, 0x085f ] uint8;
	}
	public memory code
	{
		target = iointerface;
		type = [ uint32 0x0080, 0xffff ] uint8;  //[0x0080, 0x1fff],
	}
	unit iointerface
	{
		//uint8 写入和读出
		public uint8 get_uint8( uint32 addr )
		{
			#asm "ldd r30,y+0"
			#asm "ldd r31,y+1"
			#asm "ldd r26,y+2"
			
			#asm "cpi r26,0"
			#asm "brne @code"
			#asm "ld r20,z"
			#asm "jmp @end"
			#asm "@code:"
			#asm "lpm r20,z"
			#asm "@end:"
		}
		public void set_uint8( uint32 addr, uint8 data )
		{
			#asm "ldd r30,y+0"
			#asm "ldd r31,y+1"
			#asm "ldd r26,y+2"
			#asm "cpi r26,0"
			#asm "breq @base"
			#asm "ret"
			#asm "@base:"
			#asm "ldd r20,y+4"
			#asm "st z,r20"
		}
		//uint16 写入和读出
		public uint16 get_uint16( uint32 addr )
		{
			#asm "ldd r30,y+0"
			#asm "ldd r31,y+1"
			#asm "ldd r26,y+2"
			
			#asm "cpi r26,0"
			#asm "brne @code"
			#asm "ld r20,z+"
			#asm "ld r21,z"
			#asm "jmp @end"
			#asm "@code:"
			#asm "lpm r20,z+"
			#asm "lpm r21,z"
			#asm "@end:"
		}
		public void set_uint16( uint32 addr, uint16 data )
		{
			#asm "ldd r30,y+0"
			#asm "ldd r31,y+1"
			#asm "ldd r26,y+2"
			#asm "cpi r26,0"
			#asm "breq @base"
			#asm "ret"
			#asm "@base:"
			#asm "ldd r20,y+4"
			#asm "ldd r21,y+5"
			#asm "st z+,r20"
			#asm "st z,r21"
		}
		//uint32 写入和读出
		public uint32 get_uint32( uint32 addr )
		{
			#asm "ldd r30,y+0"
			#asm "ldd r31,y+1"
			
			#asm "ldd r26,y+2"
			
			#asm "cpi r26,0"
			#asm "brne @code"
			#asm "ld r20,z+"
			#asm "ld r21,z+"
			#asm "ld r22,z+"
			#asm "ld r23,z"
			#asm "jmp @end"
			#asm "@code:"
			#asm "lpm r20,z+"
			#asm "lpm r21,z+"
			#asm "lpm r22,z+"
			#asm "lpm r23,z"
			#asm "@end:"
		}
		public void set_uint32( uint32 addr, uint32 data )
		{
			#asm "ldd r30,y+0"
			#asm "ldd r31,y+1"
			#asm "ldd r26,y+2"
			#asm "cpi r26,0"
			#asm "breq @base"
			#asm "ret"
			#asm "@base:"
			#asm "ldd r20,y+4"
			#asm "ldd r21,y+5"
			#asm "ldd r22,y+6"
			#asm "ldd r23,y+7"
			#asm "st z+,r20"
			#asm "st z+,r21"
			#asm "st z+,r22"
			#asm "st z,r23"
		}
	}
}






