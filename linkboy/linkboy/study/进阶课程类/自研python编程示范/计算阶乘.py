
#阶乘计算函数
def JC( n ):
	if n == 1:
		return n
	else:
		return n * JC( n - 1 )

#主循环读取数据计算阶乘后显示到屏幕上
while True:
	d = input()
	r = JC( d )
	print( r )

