#对于一个非零整数a，除以另一个整数b，如果余数是0，那么就可以说b是a的因数
def is_factor( b, a ):
	if a % b == 0:
		return True
	else:
		return False

#主循环读取数据并显示这个数据的所有因数到屏幕上
#例如输入36，屏幕会依次显示1，2，3，4，6，9，12，18
while True:
	d = input()
	for i in range( 1, d ):
		if is_factor( i, d ):
			print( i )
			sleep( 1000 )

