
#通过矩阵键盘输入一个数字, 程序会输出此数字范围内的所有质数
while( True ):
	num = input()
	for i in range(2, num + 1 ):
		if isPrime( i ):
			print( i )
			sleep( 1000 )
	
#判断输入的数据是否为质数,是的话返回True,不是返回False
def isPrime( n ):
	b = True
	for ii in range(2, n):
		if(n % ii) == 0:
			b = False
			break
	return b
	