
unit core1
{
	//[i] set BOOT_MODE unit ?
	public link unit BOOT_MODE {}
	
	//[i] setu BOOT1 unit ?
	public link unit BOOT1 {}
	
	//[i] setu BOOT0 unit ?
	public link unit BOOT0 {}
	
	//[i] setu RAM unit ?
	public link unit RAM {}
	
	//[i] setu ROM unit ?
	public link unit ROM {}
	
	//[i] setu SysTick unit ?
	public link unit SysTick {}
	
	//[i] setu DMA unit ?
	public link unit DMA {}
	
	//[i] setu GPIO unit ?
	public link unit GPIO {}
	
	#include "code.h"
	
	#define NOP() #.nop()
	//uint32 NOP;
	
	public link void delay_us( uint32 t ) {} = #.delay_us;
	public link void delay_clk( uint32 t ) {} = #.delay_clk;
	
	
	uint32 VSP;
	uint32 DP;
	uint32 TDP;
	uint32 TTDP;
	uint32 A;
	uint32 B;
	
	uint32 PC_tmp;
	uint32 PC_tmp1;
	uint32 inscode;
	uint8 Code;
	uint32 BValue;
	
	uint32 U1;
	uint32 UFF;
	uint32 UFFFF;
	uint32 BB;
	uint32 NBB;
	uint32 CA;
	uint32 IOval;
	
	//ROM IP
	link uint32 romDO = ROM.romDO;
	link uint32 romDI = ROM.romDI;
	link bit romWE = ROM.romWE;
	link bit romCE = ROM.romCE;
	link uint32 vos_PC = ROM.vos_PC;
	
	//RAM IP
	link uint8 ramDO = RAM.ramDO;
	link uint8 ramDI = RAM.ramDI;
	link bit ramWE = RAM.ramWE;
	link bit ramCE = RAM.ramCE;
	link uint32 ramADDR = RAM.ramADDR;
	
	uint16 DMA_Start;
	uint16 DMA_Len;
	uint16 DMA_CAddr;
	
	void main(void)
	{
		//#.BitPU.WE = 0;
		
		ramWE = 0;
		romWE = 0;
		ramCE = 1;
		romCE = 1;
		BOOT_MODE.DIR = 0;
		
		//等待其他IP初始化完成
		delay_us( 10000 );
		
		if( BOOT_MODE.IN != 0 ) {
			//正常装载程序
			BOOT1.Load();
		}
		else {
			//串口循环读取
			BOOT0.Load();
		}
		
		//loop {
		
		para {
			VSP = 3000; // 2047 //4095 这里应设置为 RAM 的上端
			vos_PC = 0;
			DP = 0;
			TDP = 0;
			TTDP = 0;
			
			U1 = 1;
			UFF = 0xFF;
			UFFFF = 0xFFFF;
		}
		//等待第一条指令读取出来
		NOP();
		
		loop {
		//while( #.UART_RX.rx_ok == 0 ) {
			
			//2022.10.24 临时测试... 应该可以去掉吧
			//NOP();
			//NOP();
			
			if(  DMA.IRQ == 1 ) {
				
				uint8 id = (DMA.IDout & 0b1100_0000) + 0b0100_0000;
				
				para { ramADDR = DMA.DMA_CAddr; ramDI = (uint8)(uint16)DMA.IDout; ramWE = 1; }
				para { ramWE = 0; DMA.DMA_CAddr += 1; }
				
				do {
					para { ramADDR += 1; ramDI = (uint8)(uint16)DMA.Dataout; ramWE = 1; }
					para { ramWE = 0; DMA.Dataout >>= 8; id -= 0b0100_0000; DMA.DMA_CAddr += 1; }
				}
				while( id != 0 );
				
				DMA.IRQ = 0;
				if( DMA.DMA_CAddr >= DMA.DMA_Start + DMA.DMA_Len ) {
					DMA.DMA_CAddr = DMA.DMA_Start;
				}
			}
			
			para {
				inscode = romDO;
				vos_PC += 1;
			}
			para {
				Code = (uint8)inscode;
				BValue = inscode >> 8;
			}
			
			if( Code == N_Save_0_local_bool || Code == N_Save_0_local_bit || Code == N_Save_0_local_uint8 || Code == N_Save_0_local_uint16 ||
			    Code == N_Save_0_local_uint32 || Code == N_Save_0_local_int8 || Code == N_Save_0_local_int16 || Code ==  N_Save_0_local_int32 ) {
				BValue += DP;
			}
			if( Code == N_Load_0_local_bool || Code == N_Load_0_local_bit || Code == N_Load_0_local_uint8 || Code == N_Load_0_local_uint16 ||
				Code == N_Load_0_local_uint32 || Code == N_Load_0_local_int8 || Code == N_Load_0_local_int16 || Code == N_Load_0_local_int32 ) {
				BValue += DP;
			}
			if( Code == N_Load_1_local_bool || Code == N_Load_1_local_bit || Code == N_Load_1_local_uint8 || Code == N_Load_1_local_uint16 ||
				Code == N_Load_1_local_uint32 || Code == N_Load_1_local_int8 || Code == N_Load_1_local_int16 || Code == N_Load_1_local_int32 ) {
				BValue += DP;
			}
			//指令散转
			switch( Code ) {
			
			//加这两句导致异常
			//case N_Nop:
			//case N_Nop0:
			//	break;
			
			case N_CLI: break;
			case N_SEI: break;
			
			case N_Goto:
				vos_PC = BValue >> 2;
				//取指令缓冲
				NOP();
				break;
			
			//函数调用
			case N_Call:
				para {
					A = vos_PC;
					VSP -= 4;
				}
				//写入数据
				para { ramADDR = VSP + 1; ramDI = (uint8)(uint16)A; ramWE = 1; }
				para { ramWE = 0; A >>= 8; }
				para { ramADDR += 1; ramDI = (uint8)(uint16)A; ramWE = 1; }
				para { ramWE = 0; A >>= 8; }
				para { ramADDR += 1; ramDI = (uint8)(uint16)A; ramWE = 1; }
				para { ramWE = 0; A >>= 8; }
				para { ramADDR += 1; ramDI = (uint8)(uint16)A; ramWE = 1; }
				para {
					ramWE = 0;
					DP = TTDP;
					vos_PC = BValue >> 2;
				}
				//取指令缓冲
				NOP();
				break;
			
			//中断返回
			case N_IRet:
			
			//返回指令
			case N_Ret:
				//读一个操作数
				ramADDR = VSP + 4;
				NOP();
				para { vos_PC = ramDO; ramADDR -= 1; }
				vos_PC <<= 8;
				para { vos_PC |= ramDO; ramADDR -= 1; }
				vos_PC <<= 8;
				para { vos_PC |= ramDO; ramADDR -= 1; }
				vos_PC <<= 8;
				para { vos_PC |= ramDO;	VSP += 4; }
				
				//取指令缓冲
				NOP();
				break;
			
			//设置堆栈
			case N_SetStack: DP = BValue; break;
			
			//堆栈递增
			case N_StackInc:
				para {
					TTDP = DP + BValue;
					TDP = DP + BValue;
				}
				break;
			
			//堆栈递减
			case N_StackDec: DP -= BValue; break;
			
			//传递参数 (实参 -> 形参)
			case N_TransVar_bit: case N_TransVar_bool: case N_TransVar_uint8: case N_TransVar_int8:
				para { ramADDR = TDP; ramDI = (uint8)(uint16)A; ramWE = 1; }
				para { ramWE = 0; TDP += 1; }
				break;
			
			case N_TransVar_uint16: case N_TransVar_int16:
				//写入数据
				para { ramADDR = TDP; ramDI = (uint8)(uint16)A; ramWE = 1; }
				para { ramWE = 0; TDP += 1; A >>= 8; }
				para { ramADDR += 1; ramDI = (uint8)(uint16)A; ramWE = 1; }
				para { ramWE = 0; TDP += 1; }
				break;
			
			case N_TransVar_uint32: case N_TransVar_int32:
				//写入数据
				para { ramADDR = TDP; ramDI = (uint8)(uint16)A; ramWE = 1; }
				para { ramWE = 0; TDP += 1; A >>= 8; }
				para { ramADDR += 1; ramDI = (uint8)(uint16)A; ramWE = 1; }
				para { ramWE = 0; TDP += 1; A >>= 8; }
				para { ramADDR += 1; ramDI = (uint8)(uint16)A; ramWE = 1; }
				para { ramWE = 0; TDP += 1; A >>= 8; }
				para { ramADDR += 1; ramDI = (uint8)(uint16)A; ramWE = 1; }
				para { ramWE = 0; TDP += 1; }
				break;
			
			//接口调用
			case N_Interface:
				para {
					DP = TTDP;
					inscode = romDO;
					vos_PC += 1;
				}
				//取指令缓冲
				NOP();
				
				if( inscode == 4 ) {
					
					//读一个操作数
					ramADDR = DP + 3;
					NOP();
					para { A = ramDO; ramADDR -= 1; }
					A <<= 8;
					para { A |= ramDO; ramADDR -= 1; }
					A <<= 8;
					para { A |= ramDO; ramADDR -= 1; }
					A <<= 8;
					A |= ramDO;
					
					//读一个操作数
					ramADDR = DP + 7;
					NOP();
					para { B = ramDO; ramADDR -= 1; }
					B <<= 8;
					para { B |= ramDO; ramADDR -= 1; }
					B <<= 8;
					para { B |= ramDO; ramADDR -= 1; }
					B <<= 8;
					B |= ramDO;
					
					//方向模式设置
					if( (A & 0x300) == 0x000 ) {
						IOval = U1 << (A&0xFF);
						if( (A&0xE0) == 0 ) {
							if( B == 0 ) { GPIO.PORTA_DIR &= ~IOval; } else { GPIO.PORTA_DIR |= IOval; }
						}
						else {
							if( B == 0 ) { GPIO.PORTB_DIR &= ~IOval; } else { GPIO.PORTB_DIR |= IOval; }
						}
					}
					//输出状态设置
					else if( (A & 0x300) == 0x100 ) {
						IOval = U1 << (A&0xFF);
						if( (A&0xE0) == 0 ) {
							if( B == 0 ) { GPIO.PORTA &= ~IOval; } else { GPIO.PORTA |= IOval; }
						}
						else {
							if( B == 0 ) { GPIO.PORTB &= ~IOval; } else { GPIO.PORTB |= IOval; }
						}
					}
					else {
						//...
					}
				}
				else if( inscode == 5 ) {
					
					//读一个操作数
					ramADDR = DP + 3;
					NOP();
					para { A = ramDO; ramADDR -= 1; }
					A <<= 8;
					para { A |= ramDO; ramADDR -= 1; }
					A <<= 8;
					para { A |= ramDO; ramADDR -= 1; }
					A <<= 8;
					A |= ramDO;
					
					//读取输入端口
					if( (A & 0x300) == 0x200 ) {
						IOval = U1 << (A&0xFF);
						if( (A&0xE0) == 0 ) {
							if( (GPIO.PORTA_IN & IOval) != 0 ) { A = 1; } else { A = 0; }
						}
						else {
							if( (GPIO.PORTB_IN & IOval) != 0 ) { A = 1; } else { A = 0; }
						}
					}
					else {
						//...
					}
				}
				//读取系统 Tick
				else if( inscode == 22 ) {
					//读一个操作数
					ramADDR = DP;
					NOP();
					A = ramDO;
					
					//读一个操作数
					ramADDR = DP + 4;
					NOP();
					para { B = ramDO; ramADDR -= 1; }
					B <<= 8;
					para { B |= ramDO; ramADDR -= 1; }
					B <<= 8;
					para { B |= ramDO; ramADDR -= 1; }
					B <<= 8;
					B |= ramDO;
					
					if( A == 1 ) {
						A = SysTick.Value;
					}
					else if( A == 2 ) {
						//临时代码
						while( #.BitPU.in_en != #.BitPU.in_deal ) {}
						#.BitPU.Data = B;
						#.BitPU.in_en = ~#.BitPU.in_deal;
						#.BitPU.WE = 1;
						delay_clk(1);
						#.BitPU.WE = 0;
					}
					else if( A == 3 ) {
						DMA.DMA_Start = (uint16)(B >> 16);
						DMA.DMA_Len = (uint16)B;
						DMA.DMA_CAddr = DMA_Start;
					}
					else if( A == 4 ) {
						A = DMA.DMA_CAddr;
					}
					else {
						//...
					}
				}
				else {
					//...
				}
				break;
			
			case N_MoveAtoB: B = A; break;
			
			case N_SETA_uint8: case N_SETA_uint16: case N_SETA_uint32:
			case N_SETA_sint8: case N_SETA_sint16: case N_SETA_sint32:
				para {
					A = romDO;
					vos_PC += 1;
				}
				//取指令缓冲
				NOP();
				break;
			
			case N_SETB_uint8: case N_SETB_uint16: case N_SETB_uint32:
			case N_SETB_sint8: case N_SETB_sint16: case N_SETB_sint32:
				para {
					B = romDO;
					vos_PC += 1;
				}
				//取指令缓冲
				NOP();
				break;
			
			case N_Load_PC_local_uint:
				ramADDR = BValue + 3;
				NOP();
				para { vos_PC = ramDO; ramADDR -= 1; }
				vos_PC <<= 8;
				para { vos_PC |= ramDO; ramADDR -= 1; }
				vos_PC <<= 8;
				para { vos_PC |= ramDO; ramADDR -= 1; }
				vos_PC <<= 8;
				vos_PC |= ramDO;
				vos_PC >>= 2;
				
				//取指令缓冲
				//NOP();
				break;
			
			//内存指针访问
			case N_LoadBASE_uint8:
				if( (B & 0x00FF0000) == 0 ) {
					ramADDR = B;
					NOP();
					A = ramDO;
				}
				else {
					para {
						PC_tmp = vos_PC;
						vos_PC = (B & 0xFFFF) >> 2;
						PC_tmp1 = (B & 0b11) << 3;
					}
					NOP();
					A = romDO;
					para {
						vos_PC = PC_tmp;
						A = (A >> PC_tmp1) & 0xFF;
					}
					//取指令缓冲
					//NOP();
				}
				break;
			
			case N_LoadBASE_uint16:
				if( (B & 0x00FF0000) == 0 ) {
					ramADDR = B + 1;
					NOP();
					para { A = ramDO; ramADDR -= 1; }
					A <<= 8;
					A |= ramDO;
				}
				else {
					A = 0; //Flash_ReadByte(B & 0xFFFF);
				}
				break;
			
			case N_LoadBASE_uint32:
				if( (B & 0x00FF0000) == 0 ) {
					ramADDR = B + 3;
					NOP();
					para { A = ramDO; ramADDR -= 1; }
					A <<= 8;
					para { A |= ramDO; ramADDR -= 1; }
					A <<= 8;
					para { A |= ramDO; ramADDR -= 1; }
					A <<= 8;
					A |= ramDO;
				}
				else {
					A = 0; //Flash_ReadByte(B & 0xFFFF);
				}
				break;
			
			case N_SaveBASE_uint8:
				para { ramADDR = B; ramDI = (uint8)(uint16)A; ramWE = 1; }
				ramWE = 0;
				break;
			
			case N_SaveBASE_uint16:
				para { ramADDR = B; ramDI = (uint8)(uint16)A; ramWE = 1; }
				para { ramWE = 0; A >>= 8; }
				para { ramADDR += 1; ramDI = (uint8)(uint16)A; ramWE = 1; }
				ramWE = 0;
				break;
			
			case N_SaveBASE_uint32:
				para { ramADDR = B; ramDI = (uint8)(uint16)A; ramWE = 1; }
				para { ramWE = 0; A >>= 8; }
				para { ramADDR += 1; ramDI = (uint8)(uint16)A; ramWE = 1; }
				para { ramWE = 0; A >>= 8; }
				para { ramADDR += 1; ramDI = (uint8)(uint16)A; ramWE = 1; }
				para { ramWE = 0; A >>= 8; }
				para { ramADDR += 1; ramDI = (uint8)(uint16)A; ramWE = 1; }
				ramWE = 0;
				break;
			
			case N_Save_0_static_bool: case N_Save_0_static_bit: case N_Save_0_static_uint8: case N_Save_0_static_int8:
			case N_Save_0_local_bool: case N_Save_0_local_bit: case N_Save_0_local_uint8: case N_Save_0_local_int8:
				para { ramADDR = BValue; ramDI = (uint8)(uint16)A; ramWE = 1; }
				ramWE = 0;
				break;
			
			case N_Save_0_static_uint16: case N_Save_0_static_int16: case N_Save_0_local_uint16: case N_Save_0_local_int16:
				//写入数据
				para { ramADDR = BValue; ramDI = (uint8)(uint16)A; ramWE = 1; }
				para { ramWE = 0; A >>= 8; }
				para { ramADDR += 1; ramDI = (uint8)(uint16)A; ramWE = 1; }
				ramWE = 0;
				break;
			
			case N_Save_0_static_uint32:case N_Save_0_static_int32: case N_Save_0_local_uint32: case N_Save_0_local_int32:
				//写入数据
				para { ramADDR = BValue; ramDI = (uint8)(uint16)A; ramWE = 1; }
				para { ramWE = 0; A >>= 8; }
				para { ramADDR += 1; ramDI = (uint8)(uint16)A; ramWE = 1; }
				para { ramWE = 0; A >>= 8; }
				para { ramADDR += 1; ramDI = (uint8)(uint16)A; ramWE = 1; }
				para { ramWE = 0; A >>= 8; }
				para { ramADDR += 1; ramDI = (uint8)(uint16)A; ramWE = 1; }
				ramWE = 0;
				break;
			
			case N_Load_0_static_bool: case N_Load_0_static_bit: case N_Load_0_static_uint8: case N_Load_0_static_int8:
			case N_Load_0_local_bool:case N_Load_0_local_bit: case N_Load_0_local_uint8: case N_Load_0_local_int8:
				//读一个操作数
				ramADDR = BValue;
				NOP();
				A = ramDO;
				
				//int8符号扩展
				if( ( Code == N_Load_0_static_int8 || Code == N_Load_0_local_int8 ) && ( A & 0x80 ) != 0 ) {
					A |= 0xFFFFFF00;
				}
				break;
			
			case N_Load_0_static_uint16: case N_Load_0_static_int16: case N_Load_0_local_uint16: case N_Load_0_local_int16:
				//读一个操作数
				ramADDR = BValue + 1;
				NOP();
				para { A = ramDO; ramADDR -= 1; }
				A <<= 8;
				A |= ramDO;
				
				//int16符号扩展
				if( ( Code == N_Load_0_static_int16 || Code == N_Load_0_local_int16 ) && ( A & 0x8000 ) != 0 ) {
					A |= 0xFFFF0000;
				}
				break;
			
			case N_Load_0_static_uint32: case N_Load_0_static_int32: case N_Load_0_local_uint32: case N_Load_0_local_int32:
				//读一个操作数
				ramADDR = BValue + 3;
				NOP();
				para { A = ramDO; ramADDR -= 1; }
				A <<= 8;
				para { A |= ramDO; ramADDR -= 1; }
				A <<= 8;
				para { A |= ramDO; ramADDR -= 1; }
				A <<= 8;
				A |= ramDO;
				break;
			
			case N_Load_1_static_bool: case N_Load_1_static_bit: case N_Load_1_static_uint8: case N_Load_1_static_int8:
			case N_Load_1_local_bool: case N_Load_1_local_bit: case N_Load_1_local_uint8: case N_Load_1_local_int8:
				//读一个操作数
				ramADDR = BValue;
				NOP();
				B = ramDO;
				
				//int8符号扩展
				if( ( Code == N_Load_1_static_int8 || Code == N_Load_1_local_int8 ) && ( B & 0x80 ) != 0 ) {
					B |= 0xFFFFFF00;
				}
				break;
			
			case N_Load_1_static_uint16: case N_Load_1_static_int16: case N_Load_1_local_uint16: case N_Load_1_local_int16:
				//读一个操作数
				ramADDR = BValue + 1;
				NOP();
				para { B = ramDO; ramADDR -= 1; }
				B <<= 8;
				B |= ramDO;
				
				//int16符号扩展
				if( ( Code == N_Load_1_static_int16 || Code == N_Load_1_local_int16 ) && ( B & 0x8000 ) != 0 ) {
					B |= 0xFFFF0000;
				}
				break;
			
			case N_Load_1_static_uint32: case N_Load_1_static_int32: case N_Load_1_local_uint32: case N_Load_1_local_int32:
				//读一个操作数
				ramADDR = BValue + 3;
				NOP();
				para { B = ramDO; ramADDR -= 1; }
				B <<= 8;
				para { B |= ramDO; ramADDR -= 1; }
				B <<= 8;
				para { B |= ramDO; ramADDR -= 1; }
				B <<= 8;
				B |= ramDO;
				break;
			
			case N_TrueGoto_bool:
				if( A != 0 ) {
					vos_PC = BValue >> 2;
					//取指令缓冲
					NOP();
				}
				break;
			
			case N_FalseGoto_bool:
				if( A == 0 ) {
					vos_PC = BValue >> 2;
					//取指令缓冲
					NOP();
				}
				break;
			
			case N_DecGoto_x:
				
				if( A == 0 ) {
					vos_PC = BValue >> 2;
					//取指令缓冲
					NOP();
				}
				else {
					A -= 1;
				}
				break;
			
			case N_InsAddr: A = BValue; break;
			
			//可能是一个没用的指令??
			//else if( Code == N_ReadAddr_local_uint32 ) {			A = DP + Flash_ReadUint32( vos_PC + 4 ); PC_Step = 8; }
			
			case N_AddrOffset_uintx_uintx_uintx:
				
				if( BValue == 1 ) {
					A += B;
				}
				else if( BValue == 2 ) {
					A += B << 1;
				}
				else if( BValue == 4 ) {
					A += B << 2;
				}
				else {
					//这个数值小的话, 可能有问题? 参考数码管显示数字3异常
					//loop( 20 ) {
					//	NOP();
					//}
					A += B * BValue;
				}
				break;
			
			case N_StructOffset_uintx_uintx: A += BValue; break;
			
			case N_ReadSys_sint8: break;
			case N_ReadSys_sint16: break;
			case N_ReadSys_sint32: break;
			
			case N_AutoSwitch_int16_int8: break;
			case N_AutoSwitch_int32_int8: break;
			case N_AutoSwitch_int32_int16: break;
			case N_AutoSwitch_fix_int8: A <<= 10; break;
			case N_AutoSwitch_fix_int16: A <<= 10; break;
			case N_AutoSwitch_fix_int32: A <<= 10; break;
			//else if( Code == N_AutoSwitch_int32_fix ) { A >>>= 10; }
			//----------------------------------------------------------
			case N_UserSwitch_sint8_uint8: break;
			case N_UserSwitch_sint16_uint16: break;
			case N_UserSwitch_sint32_uint32: break;
			case N_UserSwitch_uint8_sint8: break;
			case N_UserSwitch_uint16_sint16: break;
			case N_UserSwitch_uint32_sint32: break;
			
			case N_UserSwitch_uint8_uint16: A &= 0xFF; break;
			case N_UserSwitch_uint8_uint32: A &= 0xFF; break;
			case N_UserSwitch_uint16_uint32: A &= 0xFFFF; break;
			
			case N_UserSwitch_sint8_sint16: break;
			case N_UserSwitch_sint8_sint32: break;
			case N_UserSwitch_sint16_sint32: break;
			//----------------------------------------------------------
			case N_Not_bool_bool:			if( A == 0 ) A = 1; else A = 0; break;
			case N_And_bool_bool_bool:		if( A != 0 && B != 0 ) A = 1; else A = 0; break;
			case N_Or_bool_bool_bool:		if( A != 0 || B != 0 ) A = 1; else A = 0; break;
			//----------------------------------------------------------
			case N_Neg_int8_int8: case N_Neg_int16_int16: case N_Neg_int32_int32: A = (uint)-(int)A; break;
			case N_Abs_x_x: if( (int)A < 0 )  A = (uint)-(int)A; break;
			case N_Inc_x_x: A += 1; break;
			case N_Dec_x_x: A -= 1; break;
			//----------------------------------------------------------
			case N_Add_uint8_uint8_uint8: case N_Add_uint16_uint16_uint16: case N_Add_uint32_uint32_uint32:
			case N_Add_int8_int8_int8: case N_Add_int16_int16_int16: case N_Add_int32_int32_int32:
				A += B;
				
				if( Code == N_Add_uint8_uint8_uint8 ) {
					A &= 0xFF;
				}
				if( Code == N_Add_uint16_uint16_uint16 ) {
					A &= 0xFFFF;
				}
				break;
			
			//----------------------------------------------------------
			case N_Sub_uint8_uint8_uint8: case N_Sub_uint16_uint16_uint16: case N_Sub_uint32_uint32_uint32:
			case N_Sub_int8_int8_int8: case N_Sub_int16_int16_int16: case N_Sub_int32_int32_int32:
				A -= B;
				
				if( Code == N_Sub_uint8_uint8_uint8 ) {
					A &= 0xFF;
				}
				if( Code == N_Sub_uint16_uint16_uint16 ) {
					A &= 0xFFFF;
				}
				break;
			
			//----------------------------------------------------------
			case N_Mult_uint8_uint8_uint8: case N_Mult_uint16_uint16_uint16: case N_Mult_uint32_uint32_uint32:
			case N_Mult_int8_int8_int8: case N_Mult_int16_int16_int16: case N_Mult_int32_int32_int32:
				
				//这个数值小的话, 导致数码管显示3变成了8
				//loop( 20 ) {
				//	NOP();
				//}
				A *= B;
				break;
			
			//else if( Code == N_Mult_fix_fix_fix ) {	l_a = A; l_b = vos_B; l_a *= l_b; l_a >>= 10; A = (int32)l_a; }
			
			case N_Div_uint8_uint8_uint8: case N_Div_uint16_uint16_uint16: case N_Div_uint32_uint32_uint32:
			case N_Div_int8_int8_int8: case N_Div_int16_int16_int16: case N_Div_int32_int32_int32:
				
				A = A / B;
				break;
			
			//else if( Code == N_Div_fix_fix_fix ) { l_a = A; l_b = vos_B; l_a <<= 10; l_a /= l_b; A = (int32)l_a; }

			//----------------------------------------------------------
			case N_Mod_uint8_uint8_uint8: case N_Mod_uint16_uint16_uint16: case N_Mod_uint32_uint32_uint32:
			case N_Mod_int8_int8_int8: case N_Mod_int16_int16_int16: case N_Mod_int32_int32_int32:
				
				// A = A % B;
				break;
			
			case N_Large_bool_uint_uint:			if ( A > B ) A = 1; else A = 0; break;
			case N_Large_bool_sint_sint:			if ( (int)A > (int)B ) A = 1; else A = 0; break;
			case N_LargeEqual_bool_uint_uint:		if ( A >= B ) A = 1; else A = 0; break;
			case N_LargeEqual_bool_sint_sint:		if ( (int)A >= (int)B ) A = 1; else A = 0; break;
			case N_Small_bool_uint_uint:			if ( A < B ) A = 1; else A = 0; break;
			case N_Small_bool_sint_sint:			if ( (int)A < (int)B ) A = 1; else A = 0; break;
			case N_SmallEqual_bool_uint_uint:		if ( A <= B ) A = 1; else A = 0; break;
			case N_SmallEqual_bool_sint_sint:		if ( (int)A <= (int)B ) A = 1; else A = 0; break;
			case N_Equal_bool_uint_uint: case N_Equal_bool_sint_sint:		 if ( A == B ) A = 1; else A = 0; break;
			case N_NotEqual_bool_uint_uint: case N_NotEqual_bool_sint_sint:	 if ( A != B ) A = 1; else A = 0; break;
			
			case N_Com_bit_bit: A ^= 0x01; break;
			case N_Com_uint8_uint8: A ^= 0xFF; break;
			case N_Com_uint16_uint16: A ^= 0xFFFF; break;
			case N_Com_uint32_uint32: A ^= 0xFFFFFFFF; break;
			case N_And_x_x_x: A &= B; break;
			case N_Or_x_x_x: A |= B; break;
			case N_Xor_x_x_x: A ^= B; break;
			case N_Right_x_x_x: A >>= B; break;
			case N_Left_uint8_uint8_uint8: A <<= B; A &= 0xFF; break;
			case N_Left_uint16_uint16_uint8: A <<= B; A &= 0xFFFF; break;
			case N_Left_uint32_uint32_uint8: A <<= B; break;
			
			case N_ReadSub_bit_uint8: case N_ReadSub_bit_uint16: case N_ReadSub_bit_uint32: A >>= BValue; A &= 0x01; break;
			case N_ReadSub_uint8_uint32: case N_ReadSub_uint8_uint16: A >>= BValue; A &= 0xFF; break;
			case N_ReadSub_uint16_uint32: A >>= BValue; A &= 0xFFFF; break;
			
			case N_WriteSub_uint8_uint8_bit: case N_WriteSub_uint16_uint16_bit: case N_WriteSub_uint32_uint32_bit:
				para { BB = U1 << BValue; NBB = ~(U1 << BValue); }
				if( B == 0 ) A &= NBB; else A |= BB;
				break;
			
			case N_WriteSub_uint16_uint16_uint8: case N_WriteSub_uint32_uint32_uint8:
				para { BB = (B << BValue); CA = ~(UFF << BValue); }
				A &= CA;
				A |= BB;
				break;
			
			case N_WriteSub_uint32_uint32_uint16:
				para { BB = (B << BValue); CA = ~(UFFFF << BValue); }
				A &= CA;
				A |= BB;
				break;
			
			default:
				//loop {
					//灯灭
					//#.M_PORTA = 0b1110;
					//delay_us( 500000 );
					//灯亮
					//#.M_PORTA = 0b0000;
					//delay_us( 500000 );
				//}
				break;
			}
		}
		//bootloader();
		//SPI_Save();
		//}
	}
}



