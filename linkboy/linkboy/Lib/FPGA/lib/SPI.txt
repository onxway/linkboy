
//link unit CS {} = #.GPIO.D3;
//link unit SI {} = #.GPIO.B3;
//link unit SO {} = #.GPIO.F4;
//link unit WP {} = #.GPIO.F5;
//link unit HOLD {} = #.GPIO.B1;
//link unit CLK {} = #.GPIO.H4;

unit core_SPI
{
	//[i] set CS unit ?
	link unit CS {}
	//[i] set SI unit ?
	link unit SI {}
	//[i] set SO unit ?
	link unit SO {}
	//[i] set WP unit ?
	link unit WP {}
	//[i] set HOLD unit ?
	link unit HOLD {}
	//[i] set CLK unit ?
	link unit CLK {}
	
	public uint8 cmd_type;
	public uint8 cmd_data;
	public bit cmd_start _OD_INTER;
	public uint8 r_data;
	
	link void delay() {} = #.delay_clk;
	
	#define DELAY() //delay( 5 )
	
	void main(void)
	{
		//para {
			cmd_start = 0;
			CS.DIR = 1;
			SI.DIR = 1;
			SO.DIR = 0;
			WP.DIR = 1;
			HOLD.DIR = 1;
			CLK.DIR = 1;
	
			HOLD.OUT = 1;
			WP.OUT = 1;
			CS.OUT = 1;
			CLK.OUT = 1;
		//}
		loop {
			if( cmd_start == 1 ) {
				
				switch( cmd_type ) {
					case 1: CS.OUT = 0; break;
					case 2: CS.OUT = 1; break;
					case 3:
						//页写入时这里必须要额外一个时钟 (说明文档里: 锁定最后一个字节)
						CLK.OUT = 0;
		
						//由于时钟个数需要为精确的8的倍数, 因此这里需要先让CS拉高!!!!
						CS.OUT = 1;
						
						CLK.OUT = 1;
						break;
					case 4: WriteByte( cmd_data ); break;
					case 5: ReadByte(); break;
					default: break;
				}
				cmd_start = 0;
			}
		}
	}
	void WriteByte( uint8 data )
	{
		DELAY();
		
		loop( 8 ) {
			CLK.OUT = 0;
			DELAY();
			if( (data & 0x80) == 0 ) {
				SI.OUT = 0;
			}
			else {
				SI.OUT = 1;
			}
			DELAY();
			CLK.OUT = 1;
			DELAY();
			data <<= 1;
		}
	}
	void ReadByte(void)
	{
		DELAY();
		
		r_data = 0;
		loop( 8 ) {
			r_data <<= 1;
			CLK.OUT = 0;
			DELAY();
			if( SO.IN != 0 ) {
				r_data |= 1;
			}
			DELAY();
			CLK.OUT = 1;
			DELAY();
		}
	}
}


