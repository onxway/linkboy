
unit GPIO
{
//每个unit针脚结尾的注释用于图形界面提取针脚对应的位号
unit A3 { link bit OUT = #.PORTA.0; link bit DIR = #.PORTA_DIR.0; link bit IN = #.PORTA_IN.0; }		//[g] 0
unit R3 { link bit OUT = #.PORTA.1; link bit DIR = #.PORTA_DIR.1; link bit IN = #.PORTA_IN.1; }		//[g] 1
unit P13 { link bit OUT = #.PORTA.2; link bit DIR = #.PORTA_DIR.2; link bit IN = #.PORTA_IN.2; }		//[g] 2
unit J14 { link bit OUT = #.PORTA.3; link bit DIR = #.PORTA_DIR.3; link bit IN = #.PORTA_IN.3; }		//[g] 3
unit B10 { link bit OUT = #.PORTA.4; link bit DIR = #.PORTA_DIR.4; link bit IN = #.PORTA_IN.4; }		//[g] 4
unit B14 { link bit OUT = #.PORTA.5; link bit DIR = #.PORTA_DIR.5; link bit IN = #.PORTA_IN.5; }		//[g] 5
unit A14 { link bit OUT = #.PORTA.6; link bit DIR = #.PORTA_DIR.6; link bit IN = #.PORTA_IN.6; }		//[g] 6
unit B15 { link bit OUT = #.PORTA.7; link bit DIR = #.PORTA_DIR.7; link bit IN = #.PORTA_IN.7; }		//[g] 7
unit B16 { link bit OUT = #.PORTA.8; link bit DIR = #.PORTA_DIR.8; link bit IN = #.PORTA_IN.8; }		//[g] 8
unit C16 { link bit OUT = #.PORTA.9; link bit DIR = #.PORTA_DIR.9; link bit IN = #.PORTA_IN.9; }		//[g] 9
unit C15 { link bit OUT = #.PORTA.10; link bit DIR = #.PORTA_DIR.10; link bit IN = #.PORTA_IN.10; }	//[g] 10
unit E16 { link bit OUT = #.PORTA.11; link bit DIR = #.PORTA_DIR.11; link bit IN = #.PORTA_IN.11; }	//[g] 11
unit P16 { link bit OUT = #.PORTA.12; link bit DIR = #.PORTA_DIR.12; link bit IN = #.PORTA_IN.12; }	//[g] 12
unit J11 { link bit OUT = #.PORTA.13; link bit DIR = #.PORTA_DIR.13; link bit IN = #.PORTA_IN.13; }	//[g] 13
unit L12 { link bit OUT = #.PORTA.14; link bit DIR = #.PORTA_DIR.14; link bit IN = #.PORTA_IN.14; }	//[g] 14
unit M12 { link bit OUT = #.PORTA.15; link bit DIR = #.PORTA_DIR.15; link bit IN = #.PORTA_IN.15; }	//[g] 15
unit H13 { link bit OUT = #.PORTA.16; link bit DIR = #.PORTA_DIR.16; link bit IN = #.PORTA_IN.16; }	//[g] 16
unit J13 { link bit OUT = #.PORTA.17; link bit DIR = #.PORTA_DIR.17; link bit IN = #.PORTA_IN.17; }	//[g] 17
unit R16 { link bit OUT = #.PORTA.18; link bit DIR = #.PORTA_DIR.18; link bit IN = #.PORTA_IN.18; }	//[g] 18
unit N12 { link bit OUT = #.PORTA.19; link bit DIR = #.PORTA_DIR.19; link bit IN = #.PORTA_IN.19; }	//[g] 19
unit P12 { link bit OUT = #.PORTA.20; link bit DIR = #.PORTA_DIR.20; link bit IN = #.PORTA_IN.20; }	//[g] 20
unit N11 { link bit OUT = #.PORTA.21; link bit DIR = #.PORTA_DIR.21; link bit IN = #.PORTA_IN.21; }	//[g] 21
unit L10 { link bit OUT = #.PORTA.22; link bit DIR = #.PORTA_DIR.22; link bit IN = #.PORTA_IN.22; }	//[g] 22
unit P11 { link bit OUT = #.PORTA.23; link bit DIR = #.PORTA_DIR.23; link bit IN = #.PORTA_IN.23; }	//[g] 23
unit M10 { link bit OUT = #.PORTA.24; link bit DIR = #.PORTA_DIR.24; link bit IN = #.PORTA_IN.24; }	//[g] 24
unit R7 { link bit OUT = #.PORTA.25; link bit DIR = #.PORTA_DIR.25; link bit IN = #.PORTA_IN.25; }		//[g] 25
unit T7 { link bit OUT = #.PORTA.26; link bit DIR = #.PORTA_DIR.26; link bit IN = #.PORTA_IN.26; }		//[g] 26
unit P5 { link bit OUT = #.PORTA.27; link bit DIR = #.PORTA_DIR.27; link bit IN = #.PORTA_IN.27; }		//[g] 27
unit N5 { link bit OUT = #.PORTA.28; link bit DIR = #.PORTA_DIR.28; link bit IN = #.PORTA_IN.28; }		//[g] 28
unit R2 { link bit OUT = #.PORTA.29; link bit DIR = #.PORTA_DIR.29; link bit IN = #.PORTA_IN.29; }		//[g] 29
unit P2 { link bit OUT = #.PORTA.30; link bit DIR = #.PORTA_DIR.30; link bit IN = #.PORTA_IN.30; }		//[g] 30
unit N4 { link bit OUT = #.PORTA.31; link bit DIR = #.PORTA_DIR.31; link bit IN = #.PORTA_IN.31; }		//[g] 31

unit D3 { link bit OUT = #.PORTB.0; link bit DIR = #.PORTB_DIR.0; link bit IN = #.PORTB_IN.0; }		//[g] 32
unit B3 { link bit OUT = #.PORTB.1; link bit DIR = #.PORTB_DIR.1; link bit IN = #.PORTB_IN.1; }		//[g] 33
unit F4 { link bit OUT = #.PORTB.2; link bit DIR = #.PORTB_DIR.2; link bit IN = #.PORTB_IN.2; }		//[g] 34
unit F5 { link bit OUT = #.PORTB.3; link bit DIR = #.PORTB_DIR.3; link bit IN = #.PORTB_IN.3; }		//[g] 35
unit B1 { link bit OUT = #.PORTB.4; link bit DIR = #.PORTB_DIR.4; link bit IN = #.PORTB_IN.4; }		//[g] 36
unit H4 { link bit OUT = #.PORTB.5; link bit DIR = #.PORTB_DIR.5; link bit IN = #.PORTB_IN.5; }		//[g] 37

	link unit R{} = R3;
	link unit G{} = P13;
	link unit B{} = J14;
	
	link uint32 PORTA_IN = #.PORTA_IN;
	link uint32 PORTA = #.PORTA;
	link uint32 PORTA_DIR = #.PORTA_DIR;

	link uint32 PORTB_IN = #.PORTB_IN;
	link uint32 PORTB = #.PORTB;
	link uint32 PORTB_DIR = #.PORTB_DIR;
}

uint32 PORTA_IN = #addr 1234567893;
uint32 PORTA = #addr 1234567892;
uint32 PORTA_DIR;

uint32 PORTB_IN = #addr 1234567893;
uint32 PORTB = #addr 1234567892;
uint32 PORTB_DIR;


