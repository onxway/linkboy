<module>
"按钮"组件上边有一个微型开关, 当用户按下时就会触发“按键按下事件”，松开的时候，触发“按键松开事件”。
</module>
<name> Pack 按钮,
</name>
<member> var_uint8 OS_EventFlag OS_EventFlag,

</member>,
<member> event key0_press_event 按下时,
当按钮刚刚被按下的时候会自动触发此事件.
</member>,
<member> event key0_up_event 松开时,
当按钮松开的时候会触发此事件
</member>,
<member> function_void OS_init OS_init,

</member>,
