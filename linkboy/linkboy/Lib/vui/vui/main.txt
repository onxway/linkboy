
//鼠标系统
int16 zui_MX;
int16 zui_MY;

int16 zui_MX_s;
int16 zui_MY_s;
int16 zui_MX_e;
int16 zui_MY_e;
bool NeedRefreshMouse;
bool NeedRefreshObj;

const int16 zui_LEN = 15;
int16 zui_Number;

const uint8 zui_NONE = 0;
const uint8 zui_PANEL = 1;
const uint8 zui_BUTTON = 2;
const uint8 zui_LABEL = 3;
const uint8 zui_TRACEBAR = 4;

//仅用于仿真
int16 v_MouseX;
int16 v_MouseY;
int8 v_MouseButton;

//控件系统
struct zui_obj
{
	uint8 Type;
	bool MouseDown;
	int16 SX;
	int16 SY;
	int16 Width;
	int16 Height;
	bool NeedRefresh;
	
	int32 BackColor;
	int32 BackColorTmp;
	int32 EageColor;
	int32 TextColor;
	->[]int8 Text;
	
	int32 Value;
	uint8 Event;
}
[zui_LEN]struct zui_obj zui_OList;


int32 tmp_p;
->[]#.code int8 tmp_pp = #addrv tmp_p;

//引用模块
link unit screen {} = #.屏幕;
int32 WScale;
int32 HScale;

void zui_Init(void)
{
	v_MouseX = 0;
	v_MouseX = 0;
	v_MouseButton = 0;
	
	zui_MX = 120;
	zui_MY = 120;
	zui_Number = 0;
	int32 n = #.vui_ObjList[0];
	int16 st = 1;
	for( zui_Number = 0; zui_Number < n; zui_Number += 1 ) {
		zui_OList[zui_Number].Type = (uint8)(uint16)(uint)#.vui_ObjList[st];
		zui_OList[zui_Number].SX = (int16)#.vui_ObjList[st+1];
		zui_OList[zui_Number].SY = (int16)#.vui_ObjList[st+2];
		int32 x = #.vui_ObjList[st+1];
		int32 y = #.vui_ObjList[st+2];
		int32 w = #.vui_ObjList[st+3];
		int32 h = #.vui_ObjList[st+4];
		if( zui_Number == 0 ) {
			WScale = screen.宽度 * 100 / w;
			HScale = screen.高度 * 100 / h;
			w = screen.宽度;
			h = screen.高度;
		}
		else {
			x = x * WScale / 100;
			y = y * HScale / 100;
			w = w * WScale / 100;
			h = h * HScale / 100;
		}
		zui_OList[zui_Number].SX = (int16)x;
		zui_OList[zui_Number].SY = (int16)y;
		zui_OList[zui_Number].Width = (int16)w;
		zui_OList[zui_Number].Height = (int16)h;
		zui_OList[zui_Number].Event = 0;
		zui_OList[zui_Number].BackColor = #.vui_ObjList[st+5];
		zui_OList[zui_Number].EageColor = #.vui_ObjList[st+6];
		zui_OList[zui_Number].TextColor = #.vui_ObjList[st+7];
		
		//这里暂不支持直接设置引用, 需要通过共用变量中转一下 2023.6.1
		//zui_OList[zui_Number].Text -> #.vui_ObjList[st+8];
		tmp_p = #.vui_ObjList[st+8];
		zui_OList[zui_Number].Text -> tmp_pp;
		
		zui_OList[zui_Number].MouseDown = false;
		zui_OList[zui_Number].NeedRefresh = true;
		st += 9;
	}
	
	NeedRefreshMouse = true;
	NeedRefreshObj = false;
}
//设置文本
void vui_SetText( int16 id, []int8 text )
{
	//tmp_p = #.vui_ObjList[st+8];
	//zui_OList[zui_Number].Text -> tmp_pp;
	zui_OList[id].Text -> text;
	zui_OList[id].NeedRefresh = true;
	
	NeedRefreshObj = true;
}
//获取值
int32 vui_GetValue( int16 id )
{
	return zui_OList[id].Value;
}
void vui_MouseSet__( int32 x, int32 y )
{
	zui_MX = (int16)x;
	zui_MY = (int16)y;
	vui_MouseMove();
}
void vui_MouseAdd__( int32 x, int32 y )
{
	zui_MX += (int16)x;
	zui_MY += (int16)y;
	
	//相对位移模式下, 进行边界限定
	if( zui_MX < 0 ) zui_MX = 0;
	if( zui_MX > (int16)screen.宽度 - 1 ) zui_MX = (int16)screen.宽度 - 1;
	if( zui_MY < 0 ) zui_MY = 0;
	if( zui_MY > (int16)screen.高度 - 1 ) zui_MY = (int16)screen.高度 - 1;
	
	vui_MouseMove();
}
void vui_MouseMove(void)
{
	for( int16 i = 0; i < zui_Number; i += 1 ) {
		
		if( zui_OList[i].Type == zui_PANEL ) {
			continue;
		}
		int16 x = zui_OList[i].SX;
		int16 y = zui_OList[i].SY;
		int16 w = zui_OList[i].Width;
		int16 h = zui_OList[i].Height;
		
		if( zui_OList[i].Type == zui_TRACEBAR ) {
			if( zui_OList[i].MouseDown ) {
				int32 v = zui_MX - x;
				if( v >= w - 2 ) {
					v = w - 2;
				}
				if( v < 0 ) {
					v = 0;
				}
				zui_OList[i].Value = v;
				zui_OList[i].Event |= 0x01;
				zui_OList[i].NeedRefresh = true;
			}
		}
	}
	NeedRefreshMouse = true;
}
void vui_MouseDown(void)
{
	for( int16 i = 0; i < zui_Number; i += 1 ) {
		
		int16 x = zui_OList[i].SX;
		int16 y = zui_OList[i].SY;
		int16 w = zui_OList[i].Width;
		int16 h = zui_OList[i].Height;
		bool mouse_on = zui_MX >= x && zui_MX < x+w && zui_MY >= y && zui_MY < y+h;
		if( zui_OList[i].Type == zui_BUTTON ) {
			if( mouse_on ) {
				zui_OList[i].MouseDown = true;
				zui_OList[i].Event |= 0x01;
				zui_OList[i].NeedRefresh = true;
				zui_OList[i].BackColorTmp = zui_OList[i].BackColor;
				uint32 cc = (uint)zui_OList[i].BackColor;
				uint8 r = cc.16(uint8);
				uint8 g = cc.8(uint8);
				uint8 b = cc.0(uint8);
				uint16 cv = r;
				cv += g;
				cv += b;
				if( cv > 200 ) { //128*3
					if( r >= 40 ) r -= 40; else r = 0;
					if( g >= 40 ) g -= 40; else g = 0;
					if( b >= 40 ) b -= 40; else b = 0;
				}
				else {
					if( r <= 215 ) r += 40; else r = 255;
					if( g <= 215 ) g += 40; else g = 255;
					if( b <= 215 ) b += 40; else b = 255;
				}
				cc.16(uint8) = r;
				cc.8(uint8) = g;
				cc.0(uint8) = b;
				zui_OList[i].BackColor = (int)cc;
			}
		}
		if( zui_OList[i].Type == zui_TRACEBAR ) {
			if( mouse_on ) {
				zui_OList[i].MouseDown = true;
				zui_OList[i].Event |= 0x01;
				zui_OList[i].NeedRefresh = true;
				zui_OList[i].Value = zui_MX - x;
			}
		}
	}
	NeedRefreshMouse = true;
}
void vui_MouseUp(void)
{
	for( int16 i = 0; i < zui_Number; i += 1 ) {
		if( zui_OList[i].Type == zui_BUTTON ) {
			if( zui_OList[i].MouseDown ) {
				zui_OList[i].MouseDown = false;
				zui_OList[i].Event |= 0x02;
				zui_OList[i].NeedRefresh = true;
				zui_OList[i].BackColor = zui_OList[i].BackColorTmp;
			}
		}
		if( zui_OList[i].Type == zui_TRACEBAR ) {
			if( zui_OList[i].MouseDown ) {
				zui_OList[i].MouseDown = false;
				zui_OList[i].NeedRefresh = true;
			}
		}
		
	}
	NeedRefreshMouse = true;
}
void zui_Loop(void)
{
	VMouseEvent();
	
	if( !NeedRefreshMouse && !NeedRefreshObj ) {
		return;
	}
	NeedRefreshObj = false;
	bool notdrawback = true;
	
	for( int16 i = 0; i < zui_Number; i += 1 ) {
		
		int16 x = zui_OList[i].SX;
		int16 y = zui_OList[i].SY;
		int16 w = zui_OList[i].Width;
		int16 h = zui_OList[i].Height;
		
		bool local_refresh = false;
		if( NeedRefreshMouse && zui_OList[i].Type != zui_PANEL ) {
			int16 x1 = x;
			int16 y1 = y;
			int16 x2 = x+w;
			int16 y2 = y+h;
			if( x1 < zui_MX_s ) {
				x1 = zui_MX_s;
			}
			if( y1 < zui_MY_s ) {
				y1 = zui_MY_s;
			}
			if( x2 > zui_MX_s + zui_MX_e ) {
				x2 = zui_MX_s + zui_MX_e;
			}
			if( y2 > zui_MY_s + zui_MY_e ) {
				y2 = zui_MY_s + zui_MY_e;
			}
			if( x1 < x2 && y1 < y2 ) {
				
				if( notdrawback ) {
					notdrawback = false;
					if( zui_MX_s < x || zui_MY_s < y || zui_MX_s+zui_MX_e >= x+w || zui_MY_s+zui_MY_e >= y+h ) {
						screen.颜色 = zui_OList[0].BackColor;
						形状显示器.实心矩形x_y_宽_高_( zui_MX_s, zui_MY_s, zui_MX_e, zui_MY_e );
					}
				}
				screen.颜色 = zui_OList[i].BackColor;
				形状显示器.实心矩形x_y_宽_高_( x1, y1, x2-x1, y2-y1 );
				screen.颜色 = zui_OList[i].EageColor;
				if( x == x1 ) {
					形状显示器.垂直线x_y_长度_( x1, y1, y2-y1 );
				}
				if( x + w == x2 ) {
					形状显示器.垂直线x_y_长度_( x2 - 1, y1, y2-y1 );
				}
				if( y == y1 ) {
					形状显示器.水平线x_y_长度_( x1, y1, x2-x1 );
				}
				if( y + h == y2 ) {
					形状显示器.水平线x_y_长度_( x1, y2 - 1, x2-x1 );
				}
				local_refresh = true;
			}
		}
		if( !local_refresh && !zui_OList[i].NeedRefresh ) {
			continue;
		}
		if( zui_OList[i].NeedRefresh ) {
			screen.颜色 = zui_OList[i].BackColor;
			形状显示器.实心矩形x_y_宽_高_( x+1, y+1, w-2, h-2 );
			screen.颜色 = zui_OList[i].EageColor;
			if( zui_OList[i].Type == zui_PANEL ) { //临时忽略边框颜色
				screen.颜色 = zui_OList[i].BackColor;
			}
			形状显示器.空心矩形x_y_宽_高_( x, y, w, h );
			zui_OList[i].NeedRefresh = false;
		}
		if( zui_OList[i].Type == zui_BUTTON || zui_OList[i].Type == zui_LABEL ) {
			信息显示器.设置水平对齐为_( 1 );
			信息显示器.设置垂直对齐为_( 1 );
			screen.颜色 = zui_OList[i].TextColor;
			信息显示器.在x_y_处显示信息_( x + w/2, y + h/2, zui_OList[i].Text );
		}
		if( zui_OList[i].Type == zui_TRACEBAR ) {
			screen.颜色 = zui_OList[i].TextColor;
			形状显示器.实心矩形x_y_宽_高_( x+1, y+3, zui_OList[i].Value, h-6 );
		}
	}
	if( NeedRefreshMouse ) {
		if( notdrawback ) {
			screen.颜色 = zui_OList[0].BackColor;
			形状显示器.实心矩形x_y_宽_高_( zui_MX_s, zui_MY_s, zui_MX_e, zui_MY_e );
		}
		screen.颜色 = 0x00000000;
		形状显示器.空心圆x_y_半径_( zui_MX, zui_MY, 4 );
		NeedRefreshMouse = false;
	}
	zui_MX_s = zui_MX - 5;
	zui_MY_s = zui_MY - 5;
	zui_MX_e = 10;
	zui_MY_e = 10;
}

void VMouseEvent()
{
	int16 mx = (int16)#.OS.VUI_Interface( 0, 0 );
	int16 my = (int16)#.OS.VUI_Interface( 1, 0 );
	int8 md = (int8)(int16)#.OS.VUI_Interface( 2, 0 );
	
	if( v_MouseX != mx || v_MouseY != my ) {
		v_MouseX = mx;
		v_MouseY = my;
		vui_MouseSet__( v_MouseX, v_MouseY );
	}
	if( v_MouseButton != md ) {
		v_MouseButton = md;
		if( v_MouseButton != 0 ) {
			vui_MouseDown();
		}
		else {
			vui_MouseUp();
		}
	}	
}


