

unit Pack
{
	public const uint16 ID = 0;

	//[i] var_uint8 OS_EventFlag;
	uint8 OS_EventFlag;
	//[i] event StartEvent;
	//[i] event IdleEvent;

	//[i] function_void OS_init;
	public void OS_init()
	{
		OS_EventFlag = 0;
		OS_EventFlag.0(bit) = 1;

		LED.D0_DIR = 1;
		close_led();
	}
	//[i] function_void OS_thread;
	public void OS_thread()
	{
		OS_EventFlag.1(bit) = 1;
	}
	
	//---------------------------------------------------
	//[i] function_void open_led;
	public void open_led()
	{
		LED.D0_OUT = 0;
	}
	//---------------------------------------------------
	//[i] function_void close_led;
	public void close_led()
	{
		LED.D0_OUT = 1;
	}
	//---------------------------------------------------
	//[i] function_void swap_led;
	public void swap_led()
	{
		LED.D0_OUT = ~LED.D0_OUT;
	}
	
	public link unit LED {}
}

#.COM_IO = #.this;
#.USER0 = #.this;

const int8 UART_ID = 0;
driver.LED = D2;

#include <vos_mcu\common\vdata.txt>
#include <ESP8266\common\ESP8266\mio_run.txt>







