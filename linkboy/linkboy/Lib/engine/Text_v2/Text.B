//[配置信息开始],
//[组件文件] Text.M,
//[语言列表] c chinese,
//[组件名称] Text Text 信息显示器,
//[模块型号] 数码管/1602,
//[资源占用] ,
//[仿真类型] 1,
//[控件属性] 文字,字体,文字颜色,背景颜色,
//[元素子项] linkinterface_char char char char,
//[元素子项] function_void OS_init OS_init OS_init,
//[元素子项] function_void clear Clear 清空,
//[元素子项] function_void_old_int32 clear_line ClearLine+# 清空第+#+行,
//[元素子项] function_void_int32_int32_Cstring show_text ShowText+#+#+# 在第+#+行第+#+列显示信息+#,
//[元素子项] function_void_int32_int32_Cstring show_text_f ShowTextF+#+#+# 在第+#+行第+#+列向前显示信息+#,
//[元素子项] function_void_int32_int32_int32 show_number_ff ShowNumberF+#+#+# 在第+#+行第+#+列向前显示数字+#,
//[元素子项] function_void_old_int32_int32_int32 show_number_f ShowNumberF1+#+#+# 在第+#+行第+#+列显示数字+#,
//[元素子项] function_void_int32_int32_int32 show_number ShowNumber+#+#+# 在第+#+行第+#+列向后显示数字+#,
//[元素子项] function_void_int32_int32 set_format PadLeftByChar+#+# 用字符+#+把数字补齐到+#+位,
//[元素子项] function_void clear_format ClearPadleft 取消数字位数补齐,
//[配置信息结束],

unit Text
{
	#include "Text\$language$.txt"
	driver =
	#include "driver\Text.txt"
}
