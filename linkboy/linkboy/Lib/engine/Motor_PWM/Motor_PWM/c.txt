public link unit motor {}
driver.motor = motor;
public link unit analog {} = driver.analog;
public link void SetValue(bit n1){} = driver.set_value;
public link bit Speed = driver.speed;
public link void OS_init(){} = driver.OS_init;
public link void OS_run100us(){} = driver.OS_run100us;
