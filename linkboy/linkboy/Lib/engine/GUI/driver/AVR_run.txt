
	public void DrawIconBase( uint8 dtype, struct icon icon, int32 XX, int32 YY )
	{
		uint16 YBlockNumber = icon.area / 8;
		
		//行列遍历输出图标
		uint16 BufferIndex = 0;
		int16 X = (int16)XX;
		for( uint8 i = 0; i < icon.ColumnL; i += 1 ) {
			BufferIndex = i;
			int16 Y = (int16)YY;
			for( uint16 YN = 0; YN < YBlockNumber; YN += 1 ) {
				uint8 data = icon.buffer[BufferIndex];
				
				if( dtype == 0 ) {
					loop( 8 ) {
						if( data.0(bit) == 1 ) {
							map.pixel( X, Y );
						}
						//else {
						//	map.clear_pixel( X, Y );
						//}
						data >>= 1;
						Y += 1;
					}
				}
				//这个指令实际上已经过时
				if( dtype == 1 ) {
					loop( 8 ) {
						//if( data.0(bit) == 1 ) {
						//	map.clear_pixel( X, Y );
						//}
						data >>= 1;
						Y += 1;
					}
				}
				if( dtype == 2 ) {
					loop( 8 ) {
						if( data.0(bit) == 1 ) {
							if( map.get_pixel( X, Y ) != 0 ) {
								isHit = true;
								return;
							}
						}
						data >>= 1;
						Y += 1;
					}
				}
				BufferIndex += icon.ColumnL;
			}
			X += 1;
		}
	}


