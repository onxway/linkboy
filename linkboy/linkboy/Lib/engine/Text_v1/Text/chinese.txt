public link unit char {}
driver.char = char;
public link void OS_init(){} = driver.OS_init;
public link void 用字符_把数字补齐到_位(bit n1,bit n3){} = driver.set_format;
public link void 取消数字位数补齐(){} = driver.clear_format;
public link void 清空(){} = driver.clear;
public link void 清空第_行(bit n1){} = driver.clear_line;
public link void 在第_行第_列显示信息_(bit n1,bit n3,bit n5){} = driver.show_text;
public link void 在第_行第_列向前显示信息_(bit n1,bit n3,bit n5){} = driver.show_text_f;
public link void 在第_行第_列显示数字_(bit n1,bit n3,bit n5){} = driver.show_number;
public link void 在第_行第_列向前显示数字_(bit n1,bit n3,bit n5){} = driver.show_number_f;
