<module>
此组件播放一段音乐, 音乐数据用乐谱变量表示, 用户可以直接使用乐谱编辑器编辑乐谱.
</module>
<name> Pack 音乐播放器,
</name>
<member> var_uint8 OS_time OS_time,

</member>,
<member> var_uint8 OS_EventFlag OS_EventFlag,

</member>,
<member> event trig1_event 通道1音符变化时,

</member>,
<member> event trig2_event 通道2音符变化时,

</member>,
<member> var_int32 CurrentTone1 通道1当前音符,

</member>,
<member> var_int32 CurrentTone2 通道2当前音符,

</member>,
<member> linkinterface_tone tone tone,

</member>,
<member> var_bool isPause 播放暂停中,
如果当前暂停中，则此变量为“真”
</member>,
<member> var_bool isStop 播放已结束,
如果乐曲播放结束，则此变量设置为“真”
</member>,
<member> function_void OS_init OS_init,

</member>,
<member> function_void_music play0 播放 #,
播放指定的音乐. 注意如果当前正在播放中, 则此指令无效, 需要先使用停止播放指令! 解码器可以播放任意通道数目的音乐, 不过如果底层的音频模块支持的通道数过少的时候, 则会只播放前若干个通道. 例如播放一个带有两通道的音乐, 如果底层的播放模块为外接的小喇叭, 那么实际播放的只是第一个通道的音乐. 如果外接的是电脑声卡模块或者双通道和弦模块, 则可以同时播放这两个通道.
</member>,
<member> function_void stop 停止,
停止播放音乐
</member>,
<member> function_void Pause 暂停,
暂停播放音乐
</member>,
<member> function_void Continue 继续,
继续播放当前音乐
</member>,
<member> function_void OS_run OS_run,

</member>,
