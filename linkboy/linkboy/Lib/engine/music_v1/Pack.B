//[配置信息开始],
//[组件文件] Module.M,
//[语言列表] c chinese,
//[组件名称] Pack Player 音乐播放器,
//[模块型号] ,
//[资源占用] ,
//[仿真类型] 1,
//[控件属性] 文字,字体,文字颜色,背景颜色,
//[元素子项] var_uint8 OS_time OS_time OS_time,
//[元素子项] var_uint8 OS_EventFlag OS_EventFlag OS_EventFlag,
//[元素子项] event trig1_event Chanel1ChangeEvent 通道1音符变化时,
//[元素子项] event trig2_event Chanel2ChangeEvent 通道2音符变化时,
//[元素子项] var_int32 CurrentTone1 Chanel1Tone 通道1当前音符,
//[元素子项] var_int32 CurrentTone2 Chanel2Tone 通道2当前音符,
//[元素子项] linkinterface_tone tone tone tone,
//[元素子项] var_bool isPause isPause 播放暂停中,
//[元素子项] var_bool isStop isStop 播放已结束,
//[元素子项] function_void OS_init OS_init OS_init,
//[元素子项] function_void_music play0 Play+# 播放+#,
//[元素子项] function_void stop Stop 停止,
//[元素子项] function_void Pause Pause 暂停,
//[元素子项] function_void Continue Continue 继续,
//[元素子项] function_void OS_run OS_run OS_run,
//[配置信息结束],

unit Pack
{
	#include "Pack\$language$.txt"
	driver =
	#include "driver\Pack.txt"
}
