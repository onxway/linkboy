//[配置信息开始],
//[组件文件] module.M,
//[语言列表] c chinese,
//[组件名称] Clock AlarmClock 日期时间比较器,
//[资源占用] ,
//[控件属性] 文字,字体,文字颜色,背景颜色,
//[元素子项] linkinterface_clock clock0 clock0 clock0,
//[元素子项] linkinterface_clock clock1 clock1 clock1,
//[元素子项] var_uint8 OS_EventFlag OS_EventFlag OS_EventFlag,
//[元素子项] event time_event TimeReadyEvent 时间到时,
//[元素子项] var_uint8 OS_time OS_time OS_time,
//[元素子项] function_void OS_init OS_init OS_init,
//[元素子项] function_void OS_run OS_run OS_run,
//[元素子项] function_void start Start 运行,
//[元素子项] function_void stop Stop 停止,
//[配置信息结束],

unit Clock
{
	#include "Clock\$language$.txt"
	driver =
	#include "driver\Clock.txt"
}
