//[配置信息开始],
//[组件文件] Module.M,
//[语言列表] c chinese,
//[组件名称] Pack Modbus Modbus,
//[模块型号] RTU通信模式,
//[资源占用] USART,
//[仿真类型] 1,
//[元素子项] linkinterface_reader reader0 reader0 reader0,
//[元素子项] linkconst_int32_50 send_len SendLength 发送缓冲区长度,
//[元素子项] var_uint8 OS_time OS_time OS_time,
//[元素子项] var_uint8 OS_EventFlag OS_EventFlag OS_EventFlag,
//[元素子项] event receive_data_event ReceiveDataEvent 接收到一帧数据时,
//[元素子项] event receive_error_event ReceiveErrorEvent 数据接收出错时,
//[元素子项] function_void OS_init OS_init OS_init,
//[元素子项] function_void OS_run OS_run OS_run,
//[元素子项] function_void OS_thread OS_thread OS_thread,
//[元素子项] function_void_int32_int32 SetData SetData+#+# 设置发送区第+#+个字节为+#,
//[元素子项] function_void_int32 Send Send+# 发送+#+个数据,
//[元素子项] function_int32_int32 GetData GetData+# 读取接收区第+#+个字节,
//[配置信息结束],

unit Pack
{
	#include "Pack\$language$.txt"
	driver =
	#include "driver\Pack.txt"
}
