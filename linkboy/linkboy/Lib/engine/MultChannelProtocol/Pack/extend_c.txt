<module>

</module>
<name> Pack Protocol,
</name>
<member> linkinterface_reader reader0 reader0,

</member>,
<member> var_uint8 OS_EventFlag OS_EventFlag,

</member>,
<member> event trig0_event Trig0Event,

</member>,
<member> event trig1_event Trig1Event,

</member>,
<member> event trig2_event Trig2Event,

</member>,
<member> event trig3_event Trig3Event,

</member>,
<member> event trig4_event Trig4Event,

</member>,
<member> event trig5_event Trig5Event,

</member>,
<member> event trig6_event Trig6Event,

</member>,
<member> event trig7_event Trig7Event,

</member>,
<member> event error_event ErrorEvent,

</member>,
<member> linkconst_int32_4 Number Number,

</member>,
<member> function_void OS_init OS_init,

</member>,
<member> function_void OS_thread OS_thread,

</member>,
<member> function_int32_int32 get_data GetData #,

</member>,
<member> function_void_int32_int32 set_data SetData # #,

</member>,
<member> function_void_int32 set_channel SetChannel #,

</member>,
<member> function_void send Send,

</member>,
