
//2015.5.27
//开始设计串口协议解析器

unit Protocol
{
	public const uint16 ID = 0;

	//[i] linkinterface_reader reader0;
	link unit reader0 {}
	
	//[i] var_uint8 OS_EventFlag;
	public uint16 OS_EventFlag;
	//[i] event trig_event;
	//[i] event error_event;
	
	int16 index;
	
	bool ProtocolPause;
	uint8 Length;
	
	//[i] linkconst_int32_4 Number;
	public const int32 Number = 0;
	
	[int32*Number] buffer;
	
	//---------------------------------------------------
	//[i] function_void OS_init;
	public void OS_init()
	{
		OS_EventFlag = 0;
		
		index = 0;
		Length = 0;
		
		ProtocolPause = false;
	}
	//---------------------------------------------------
	//[i] function_void OS_thread;
	public void OS_thread()
	{
		if( ProtocolPause && OS_EventFlag.0(bit) == 0 ) {
			index = 0;
			ProtocolPause = false;
		}
		if( ProtocolPause ) {
			return;
		}
		//判断数据是否就绪
		if( !reader0.IsReady() ){
			return;
		}
		if( index == 0 ) {
			reader0.SetStart();
		}
		uint8 data = (uint8)(uint16)(uint)reader0.GetData();
		index += 1;
		
		if( index == 1 && data != 0xAA ) {
			OS_EventFlag.1(bit) = 1;
			index = 0;
			return;
		}
		if( index == 2 ) {
			Length = data;
			return;
		}
		if( (uint)index < 2 + Length ) {
			return;
		}
		ProtocolPause = true;
		
		OS_EventFlag.0(bit) = 1;
	}
	//---------------------------------------------------
	//[i] function_int32 get_data int32;
	int32 get_data( int32 i )
	{
		i -= 1;
		i *= 4;
		uint8 data0 = (uint8)(uint16)(uint)reader0.ReadBuffer( i + 2 );
		uint8 data1 = (uint8)(uint16)(uint)reader0.ReadBuffer( i + 3 );
		uint8 data2 = (uint8)(uint16)(uint)reader0.ReadBuffer( i + 4 );
		uint8 data3 = (uint8)(uint16)(uint)reader0.ReadBuffer( i + 5 );
		uint32 d;
		d.0(uint8) = data0;
		d.8(uint8) = data1;
		d.16(uint8) = data2;
		d.24(uint8) = data3;
		return (int)d;
	}
	//---------------------------------------------------
	//[i] function_void set_data int32 int32;
	void set_data( int32 channel, int32 data )
	{
		if( channel < 1 || channel > Number ) return; 
		channel -= 1;
		buffer[channel] = data;
	}
	//---------------------------------------------------
	//[i] function_void send;
	void send()
	{
		reader0.SetData( 0xAA );
		reader0.SetData( Number * 4 );
		for( int8 i = 0; i < Number; i += 1 ) {
			uint32 d = (uint)buffer[i];
			reader0.SetData( (int)( d.0(uint8) ) );
			reader0.SetData( (int)( d.8(uint8) ) );
			reader0.SetData( (int)( d.16(uint8) ) );
			reader0.SetData( (int)( d.24(uint8) ) );
		}
	}
}



