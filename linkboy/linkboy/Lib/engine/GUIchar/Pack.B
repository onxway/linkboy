//[配置信息开始],
//[组件文件] Module.M,
//[语言列表] c chinese,
//[组件名称] Pack Screen 点阵字符显示器,
//[资源占用] ,
//[控件属性] 文字,字体,文字颜色,背景颜色,
//[元素子项] linkinterface_map map map map,
//[元素子项] interface_char char char char,
//[元素子项] function_void OS_init OS_init OS_init,
//[元素子项] function_void clear Clear 清空,
//[元素子项] function_void_int32 clear_line ClearLine+# 清空第+#+行,
//[元素子项] function_void_int32_int32_Achar print_line PrintChar+#+#+# 第+#+行第+#+列显示字符+#,
//[元素子项] function_void_bitmap SetNumberFont SetNumberFont+# 设置数字字体为+#,
//[配置信息结束],

unit Pack
{
	#include "Pack\$language$.txt"
	driver =
	#include "driver\Pack.txt"
}
