//[配置信息开始],
//[组件文件] GModule.M,
//[语言列表] c chinese,
//[组件名称] GLED LED 绿灯,
//[资源占用] ,
//[控件属性] 文字,字体,文字颜色,背景颜色,
//[元素子项] interface_led led0 led0 led0,
//[元素子项] interface_digital digital0 digital0 digital0,
//[元素子项] function_void OS_init OS_init OS_init,
//[元素子项] function_void open Open 点亮,
//[元素子项] function_void close Close 熄灭,
//[元素子项] function_void swap swap 反转,
//[配置信息结束],

unit GLED
{
	#include "GLED\$language$.txt"
	driver =
	#include "driver\GLED.txt"
}
