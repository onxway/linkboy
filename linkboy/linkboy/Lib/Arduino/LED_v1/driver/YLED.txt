
unit LED
{
	public link unit DN {}
	public link unit DP {}
	
	bit DN_D0_OUT;
	bit DP_D0_OUT;
	
	//[i] interface_led led0;
	//[i] interface_digital digital0;
	
	unit led0
	{
		public link void Open() {}
		public link void Close() {}
	}
	led0.Open = open;
	led0.Close = close;

	unit digital0
	{
		public link void open() {}
		public link void close() {}
	}
	digital0.open = open;
	digital0.close = close;

	//[i] function_void OS_init;
	public void OS_init()
	{
		if( DN.TYPE >= 10 ) DN.D0_DIR = 1;
		if( DP.TYPE >= 10 ) DP.D0_DIR = 1;
		
		close();
	}
	
	//[i] function_void open;
	public void open()
	{
		if( DN.TYPE >= 10 ) { DN.D0_OUT = 0; DN_D0_OUT = 0; }
		if( DP.TYPE >= 10 ) { DP.D0_OUT = 1; DP_D0_OUT = 1; }
	}
	
	//[i] function_void close;
	public void close()
	{
		if( DN.TYPE >= 10 ) { DN.D0_OUT = 1; DN_D0_OUT = 1; return; }
		if( DP.TYPE >= 10 ) { DP.D0_OUT = 0; DP_D0_OUT = 0; return; }
	}
	//[i] function_void swap;
	public void swap()
	{
		if( DN.TYPE >= 10 ) {
			if( DP.TYPE >= 10 ) { DP.D0_OUT = 1; DP_D0_OUT = 1; }
			DN_D0_OUT = ~DN_D0_OUT;
			DN.D0_OUT = DN_D0_OUT;
			return;
		}
		if( DP.TYPE >= 10 ) {
			DP_D0_OUT = ~DP_D0_OUT;
			DP.D0_OUT = DP_D0_OUT;
			return;
		}
	}
}


