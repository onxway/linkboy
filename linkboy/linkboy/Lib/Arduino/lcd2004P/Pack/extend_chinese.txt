<module>
LCD2004液晶屏, 可以显示4行字符, 每行最多显示20个字符. 注：本模块独立使用仅能显示单个字符，如果连接到“文字显示引擎”上，将会有更加高级的功能，比如直接显示数值、一串英文信息等。
</module>
<name> Pack 屏幕2004,
</name>
<member> interface_char char char,

</member>,
<member> function_void OS_init OS_init,

</member>,
<member> function_void OS_thread OS_thread,

</member>,
<member> function_void clear 清空,
这个指令清空屏幕的第一行和第二行, 即全部显示空格(无字符)
</member>,
<member> function_void_int32 clear_line 清空第 # 行,
清空屏幕上指定的行, 行数范围是 1 2 3 或者 4
</member>,
<member> function_void_int32_int32_int32 print_line 第 # 行第 # 列显示字符 #,
在指定的行列上显示一个字符, 行列均以左上角为起点, 从1开始. 行数范围是 1 2 3 或者 4, 列数范围是 1到20
</member>,
