
//液晶屏元件,处理器无关代码

unit lcd1602
{
	public link unit RS {}
	public link unit RW {}
	public link unit E {}
	public link unit D_0 {}
	public link unit D_1 {}
	public link unit D_2 {}
	public link unit D_3 {}
	public link unit D_4 {}
	public link unit D_5 {}
	public link unit D_6 {}
	public link unit D_7 {}
	
	link bit RS_OUT = RS.D0_OUT;
	link bit RW_OUT = RW.D0_OUT;
	link bit E_OUT = E.D0_OUT;
	link bit D_0_OUT = D_0.D0_OUT;
	link bit D_1_OUT = D_1.D0_OUT;
	link bit D_2_OUT = D_2.D0_OUT;
	link bit D_3_OUT = D_3.D0_OUT;
	link bit D_4_OUT = D_4.D0_OUT;
	link bit D_5_OUT = D_5.D0_OUT;
	link bit D_6_OUT = D_6.D0_OUT;
	link bit D_7_OUT = D_7.D0_OUT;
	
	//---------------------------------------------------
	public void port_init()
	{
		RS.D0_DIR = 1;
		RW.D0_DIR = 1;
		E.D0_DIR = 1;
		D_0.D0_DIR = 1;
		D_1.D0_DIR = 1;
		D_2.D0_DIR = 1;
		D_3.D0_DIR = 1;
		D_4.D0_DIR = 1;
		D_5.D0_DIR = 1;
		D_6.D0_DIR = 1;
		D_7.D0_DIR = 1;
		
		RS_OUT = 0;
		RW_OUT = 0;
		E_OUT = 0;
	}
	void send_data( uint8 data )
	{
		RS_OUT = 1;
		RW_OUT = 0;
		send_byte( data );
	}
	void send_command( uint8 com )
	{
		RS_OUT = 0;
		RW_OUT = 0;
		send_byte( com );
	}
	//---------------------------------------------------
	void send_byte( uint8 b )
	{
		D_0_OUT = b.0(bit);
		D_1_OUT = b.1(bit);
		D_2_OUT = b.2(bit);
		D_3_OUT = b.3(bit);
		D_4_OUT = b.4(bit);
		D_5_OUT = b.5(bit);
		D_6_OUT = b.6(bit);
		D_7_OUT = b.7(bit);
		E_OUT = 1;
		E_OUT = 0;
	}
}






