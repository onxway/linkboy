public link bit OS_time = driver.OS_time;
public link bit OS_EventFlag = driver.OS_EventFlag;
public link void OS_init(){} = driver.OS_init;
public link void OS_thread(){} = driver.OS_thread;
public link void OS_run(){} = driver.OS_run;
public link void OS_ClearWatchDog(){} = driver.OS_ClearWatchDog;
