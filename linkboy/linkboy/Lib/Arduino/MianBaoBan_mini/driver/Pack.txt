
unit Pack
{
	public const uint16 ID = 0;
	
	public link unit PW0 {}
	public link unit PW1 {}
	public link unit PW2 {}
	public link unit PW3 {}

	public link unit P01 {}
	public link unit P02 {}
	public link unit P03 {}
	public link unit P04 {}
	public link unit P05 {}
	public link unit P06 {}
	public link unit P07 {}
	public link unit P08 {}
	public link unit P09 {}

	public link unit P10 {}
	public link unit P11 {}
	public link unit P12 {}
	public link unit P13 {}
	public link unit P14 {}
	public link unit P15 {}
	public link unit P16 {}
	public link unit P17 {}
	public link unit P18 {}
	public link unit P19 {}
	
	public link unit P20 {}
	public link unit P21 {}
	public link unit P22 {}
	public link unit P23 {}

	public link unit P50 {}
	public link unit P51 {}
	public link unit P52 {}
	public link unit P53 {}
	public link unit P54 {}
	public link unit P55 {}
	public link unit P56 {}
	public link unit P57 {}
	public link unit P58 {}
	public link unit P59 {}

	public link unit P60 {}
	public link unit P61 {}
	public link unit P62 {}
	public link unit P63 {}
	public link unit P64 {}
	public link unit P65 {}
	public link unit P66 {}
	public link unit P67 {}
	public link unit P68 {}
	public link unit P69 {}

	public link unit P70 {}
	public link unit P71 {}
	public link unit P72 {}
}

public link unit PW0 {} = driver.PW0;
public link unit PW1 {} = driver.PW1;
public link unit PW2 {} = driver.PW2;
public link unit PW3 {} = driver.PW3;

public link unit P01 {} = driver.P01;
public link unit P02 {} = driver.P02;
public link unit P03 {} = driver.P03;
public link unit P04 {} = driver.P04;
public link unit P05 {} = driver.P05;
public link unit P06 {} = driver.P06;
public link unit P07 {} = driver.P07;
public link unit P08 {} = driver.P08;
public link unit P09 {} = driver.P09;

public link unit P10 {} = driver.P10;
public link unit P11 {} = driver.P11;
public link unit P12 {} = driver.P12;
public link unit P13 {} = driver.P13;
public link unit P14 {} = driver.P14;
public link unit P15 {} = driver.P15;
public link unit P16 {} = driver.P16;
public link unit P17 {} = driver.P17;
public link unit P18 {} = driver.P18;
public link unit P19 {} = driver.P19;

public link unit P20 {} = driver.P20;
public link unit P21 {} = driver.P21;
public link unit P22 {} = driver.P22;
public link unit P23 {} = driver.P23;

public link unit P50 {} = driver.P50;
public link unit P51 {} = driver.P51;
public link unit P52 {} = driver.P52;
public link unit P53 {} = driver.P53;
public link unit P54 {} = driver.P54;
public link unit P55 {} = driver.P55;
public link unit P56 {} = driver.P56;
public link unit P57 {} = driver.P57;
public link unit P58 {} = driver.P58;
public link unit P59 {} = driver.P59;

public link unit P60 {} = driver.P60;
public link unit P61 {} = driver.P61;
public link unit P62 {} = driver.P62;
public link unit P63 {} = driver.P63;
public link unit P64 {} = driver.P64;
public link unit P65 {} = driver.P65;
public link unit P66 {} = driver.P66;
public link unit P67 {} = driver.P67;
public link unit P68 {} = driver.P68;
public link unit P69 {} = driver.P69;

public link unit P70 {} = driver.P70;
public link unit P71 {} = driver.P71;
public link unit P72 {} = driver.P72;

