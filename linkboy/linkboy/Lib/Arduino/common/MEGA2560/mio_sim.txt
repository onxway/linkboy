
#include <system\arduino\MEGA2560-io.txt>

#include <Arduino\common\mybit.txt>

PORT.UART_ID = UART_ID;

PORT.D2_DIR = D0.D0_DIR;
PORT.D2_IN = D0.D0_IN;
PORT.D2_OUT = D0.D0_OUT;
PORT.D2_PUL = D0.D0_PUL;

PORT.D3_DIR = D1.D0_DIR;
PORT.D3_IN = D1.D0_IN;
PORT.D3_OUT = D1.D0_OUT;
PORT.D3_PUL = D1.D0_PUL;

unit PORT
{
	public const int8 UART_ID = 0;
	public link bit D2_DIR; public link bit D2_IN; public link bit D2_OUT; public link bit D2_PUL;
	public link bit D3_DIR; public link bit D3_IN; public link bit D3_OUT; public link bit D3_PUL;
}

D0.my_bit = my_bit;
public unit D0
{
	public const uint8 INDEX = 0;
	public link memory my_bit {}
	public my_bit bit D0_DIR = #addr 0x0000 + INDEX;
	public my_bit bit D0_OUT = #addr 0x0100 + INDEX;
	public my_bit bit D0_IN = #addr 0x0200 + INDEX;
	public my_bit bit D0_PUL = #addr 0x0300 + INDEX;
}
D1.my_bit = my_bit;
public unit D1
{
	public const uint8 INDEX = 1;
	public link memory my_bit {}
	public my_bit bit D0_DIR = #addr 0x0000 + INDEX;
	public my_bit bit D0_OUT = #addr 0x0100 + INDEX;
	public my_bit bit D0_IN = #addr 0x0200 + INDEX;
	public my_bit bit D0_PUL = #addr 0x0300 + INDEX;
}
D2.my_bit = my_bit;
public unit D2
{
	public const uint8 INDEX = 2;
	public link memory my_bit {}
	public my_bit bit D0_DIR = #addr 0x0000 + INDEX;
	public my_bit bit D0_OUT = #addr 0x0100 + INDEX;
	public my_bit bit D0_IN = #addr 0x0200 + INDEX;
	public my_bit bit D0_PUL = #addr 0x0300 + INDEX;
}
D3.my_bit = my_bit;
public unit D3
{
	public const uint8 INDEX = 3;
	public link memory my_bit {}
	public my_bit bit D0_DIR = #addr 0x0000 + INDEX;
	public my_bit bit D0_OUT = #addr 0x0100 + INDEX;
	public my_bit bit D0_IN = #addr 0x0200 + INDEX;
	public my_bit bit D0_PUL = #addr 0x0300 + INDEX;
}
D4.my_bit = my_bit;
public unit D4
{
	public const uint8 INDEX = 4;
	public link memory my_bit {}
	public my_bit bit D0_DIR = #addr 0x0000 + INDEX;
	public my_bit bit D0_OUT = #addr 0x0100 + INDEX;
	public my_bit bit D0_IN = #addr 0x0200 + INDEX;
	public my_bit bit D0_PUL = #addr 0x0300 + INDEX;
}
D5.my_bit = my_bit;
public unit D5
{
	public const uint8 INDEX = 5;
	public link memory my_bit {}
	public my_bit bit D0_DIR = #addr 0x0000 + INDEX;
	public my_bit bit D0_OUT = #addr 0x0100 + INDEX;
	public my_bit bit D0_IN = #addr 0x0200 + INDEX;
	public my_bit bit D0_PUL = #addr 0x0300 + INDEX;
}
D6.my_bit = my_bit;
public unit D6
{
	public const uint8 INDEX = 6;
	public link memory my_bit {}
	public my_bit bit D0_DIR = #addr 0x0000 + INDEX;
	public my_bit bit D0_OUT = #addr 0x0100 + INDEX;
	public my_bit bit D0_IN = #addr 0x0200 + INDEX;
	public my_bit bit D0_PUL = #addr 0x0300 + INDEX;
}
D7.my_bit = my_bit;
public unit D7
{
	public const uint8 INDEX = 7;
	public link memory my_bit {}
	public my_bit bit D0_DIR = #addr 0x0000 + INDEX;
	public my_bit bit D0_OUT = #addr 0x0100 + INDEX;
	public my_bit bit D0_IN = #addr 0x0200 + INDEX;
	public my_bit bit D0_PUL = #addr 0x0300 + INDEX;
}
D8.my_bit = my_bit;
public unit D8
{
	public const uint8 INDEX = 8;
	public link memory my_bit {}
	public my_bit bit D0_DIR = #addr 0x0000 + INDEX;
	public my_bit bit D0_OUT = #addr 0x0100 + INDEX;
	public my_bit bit D0_IN = #addr 0x0200 + INDEX;
	public my_bit bit D0_PUL = #addr 0x0300 + INDEX;
}
D9.my_bit = my_bit;
public unit D9
{
	public const uint8 INDEX = 9;
	public link memory my_bit {}
	public my_bit bit D0_DIR = #addr 0x0000 + INDEX;
	public my_bit bit D0_OUT = #addr 0x0100 + INDEX;
	public my_bit bit D0_IN = #addr 0x0200 + INDEX;
	public my_bit bit D0_PUL = #addr 0x0300 + INDEX;
}
D10.my_bit = my_bit;
public unit D10
{
	public const uint8 INDEX = 10;
	public link memory my_bit {}
	public my_bit bit D0_DIR = #addr 0x0000 + INDEX;
	public my_bit bit D0_OUT = #addr 0x0100 + INDEX;
	public my_bit bit D0_IN = #addr 0x0200 + INDEX;
	public my_bit bit D0_PUL = #addr 0x0300 + INDEX;
}
D11.my_bit = my_bit;
public unit D11
{
	public const uint8 INDEX = 11;
	public link memory my_bit {}
	public my_bit bit D0_DIR = #addr 0x0000 + INDEX;
	public my_bit bit D0_OUT = #addr 0x0100 + INDEX;
	public my_bit bit D0_IN = #addr 0x0200 + INDEX;
	public my_bit bit D0_PUL = #addr 0x0300 + INDEX;
}
D12.my_bit = my_bit;
public unit D12
{
	public const uint8 INDEX = 12;
	public link memory my_bit {}
	public my_bit bit D0_DIR = #addr 0x0000 + INDEX;
	public my_bit bit D0_OUT = #addr 0x0100 + INDEX;
	public my_bit bit D0_IN = #addr 0x0200 + INDEX;
	public my_bit bit D0_PUL = #addr 0x0300 + INDEX;
}
D13.my_bit = my_bit;
public unit D13
{
	public const uint8 INDEX = 13;
	public link memory my_bit {}
	public my_bit bit D0_DIR = #addr 0x0000 + INDEX;
	public my_bit bit D0_OUT = #addr 0x0100 + INDEX;
	public my_bit bit D0_IN = #addr 0x0200 + INDEX;
	public my_bit bit D0_PUL = #addr 0x0300 + INDEX;
}
D14.my_bit = my_bit;
public unit D14
{
	public const uint8 INDEX = 14;
	public link memory my_bit {}
	public my_bit bit D0_DIR = #addr 0x0000 + INDEX;
	public my_bit bit D0_OUT = #addr 0x0100 + INDEX;
	public my_bit bit D0_IN = #addr 0x0200 + INDEX;
	public my_bit bit D0_PUL = #addr 0x0300 + INDEX;
}
D15.my_bit = my_bit;
public unit D15
{
	public const uint8 INDEX = 15;
	public link memory my_bit {}
	public my_bit bit D0_DIR = #addr 0x0000 + INDEX;
	public my_bit bit D0_OUT = #addr 0x0100 + INDEX;
	public my_bit bit D0_IN = #addr 0x0200 + INDEX;
	public my_bit bit D0_PUL = #addr 0x0300 + INDEX;
}
D16.my_bit = my_bit;
public unit D16
{
	public const uint8 INDEX = 16;
	public link memory my_bit {}
	public my_bit bit D0_DIR = #addr 0x0000 + INDEX;
	public my_bit bit D0_OUT = #addr 0x0100 + INDEX;
	public my_bit bit D0_IN = #addr 0x0200 + INDEX;
	public my_bit bit D0_PUL = #addr 0x0300 + INDEX;
}
D17.my_bit = my_bit;
public unit D17
{
	public const uint8 INDEX = 17;
	public link memory my_bit {}
	public my_bit bit D0_DIR = #addr 0x0000 + INDEX;
	public my_bit bit D0_OUT = #addr 0x0100 + INDEX;
	public my_bit bit D0_IN = #addr 0x0200 + INDEX;
	public my_bit bit D0_PUL = #addr 0x0300 + INDEX;
}
D18.my_bit = my_bit;
public unit D18
{
	public const uint8 INDEX = 18;
	public link memory my_bit {}
	public my_bit bit D0_DIR = #addr 0x0000 + INDEX;
	public my_bit bit D0_OUT = #addr 0x0100 + INDEX;
	public my_bit bit D0_IN = #addr 0x0200 + INDEX;
	public my_bit bit D0_PUL = #addr 0x0300 + INDEX;
}
D19.my_bit = my_bit;
public unit D19
{
	public const uint8 INDEX = 19;
	public link memory my_bit {}
	public my_bit bit D0_DIR = #addr 0x0000 + INDEX;
	public my_bit bit D0_OUT = #addr 0x0100 + INDEX;
	public my_bit bit D0_IN = #addr 0x0200 + INDEX;
	public my_bit bit D0_PUL = #addr 0x0300 + INDEX;
}
D20.my_bit = my_bit;
public unit D20
{
	public const uint8 INDEX = 20;
	public link memory my_bit {}
	public my_bit bit D0_DIR = #addr 0x0000 + INDEX;
	public my_bit bit D0_OUT = #addr 0x0100 + INDEX;
	public my_bit bit D0_IN = #addr 0x0200 + INDEX;
	public my_bit bit D0_PUL = #addr 0x0300 + INDEX;
}
D21.my_bit = my_bit;
public unit D21
{
	public const uint8 INDEX = 21;
	public link memory my_bit {}
	public my_bit bit D0_DIR = #addr 0x0000 + INDEX;
	public my_bit bit D0_OUT = #addr 0x0100 + INDEX;
	public my_bit bit D0_IN = #addr 0x0200 + INDEX;
	public my_bit bit D0_PUL = #addr 0x0300 + INDEX;
}
D22.my_bit = my_bit;
public unit D22
{
	public const uint8 INDEX = 22;
	public link memory my_bit {}
	public my_bit bit D0_DIR = #addr 0x0000 + INDEX;
	public my_bit bit D0_OUT = #addr 0x0100 + INDEX;
	public my_bit bit D0_IN = #addr 0x0200 + INDEX;
	public my_bit bit D0_PUL = #addr 0x0300 + INDEX;
}
D23.my_bit = my_bit;
public unit D23
{
	public const uint8 INDEX = 23;
	public link memory my_bit {}
	public my_bit bit D0_DIR = #addr 0x0000 + INDEX;
	public my_bit bit D0_OUT = #addr 0x0100 + INDEX;
	public my_bit bit D0_IN = #addr 0x0200 + INDEX;
	public my_bit bit D0_PUL = #addr 0x0300 + INDEX;
}
D24.my_bit = my_bit;
public unit D24
{
	public const uint8 INDEX = 24;
	public link memory my_bit {}
	public my_bit bit D0_DIR = #addr 0x0000 + INDEX;
	public my_bit bit D0_OUT = #addr 0x0100 + INDEX;
	public my_bit bit D0_IN = #addr 0x0200 + INDEX;
	public my_bit bit D0_PUL = #addr 0x0300 + INDEX;
}
D25.my_bit = my_bit;
public unit D25
{
	public const uint8 INDEX = 25;
	public link memory my_bit {}
	public my_bit bit D0_DIR = #addr 0x0000 + INDEX;
	public my_bit bit D0_OUT = #addr 0x0100 + INDEX;
	public my_bit bit D0_IN = #addr 0x0200 + INDEX;
	public my_bit bit D0_PUL = #addr 0x0300 + INDEX;
}
D26.my_bit = my_bit;
public unit D26
{
	public const uint8 INDEX = 26;
	public link memory my_bit {}
	public my_bit bit D0_DIR = #addr 0x0000 + INDEX;
	public my_bit bit D0_OUT = #addr 0x0100 + INDEX;
	public my_bit bit D0_IN = #addr 0x0200 + INDEX;
	public my_bit bit D0_PUL = #addr 0x0300 + INDEX;
}
D27.my_bit = my_bit;
public unit D27
{
	public const uint8 INDEX = 27;
	public link memory my_bit {}
	public my_bit bit D0_DIR = #addr 0x0000 + INDEX;
	public my_bit bit D0_OUT = #addr 0x0100 + INDEX;
	public my_bit bit D0_IN = #addr 0x0200 + INDEX;
	public my_bit bit D0_PUL = #addr 0x0300 + INDEX;
}
D28.my_bit = my_bit;
public unit D28
{
	public const uint8 INDEX = 28;
	public link memory my_bit {}
	public my_bit bit D0_DIR = #addr 0x0000 + INDEX;
	public my_bit bit D0_OUT = #addr 0x0100 + INDEX;
	public my_bit bit D0_IN = #addr 0x0200 + INDEX;
	public my_bit bit D0_PUL = #addr 0x0300 + INDEX;
}
D29.my_bit = my_bit;
public unit D29
{
	public const uint8 INDEX = 29;
	public link memory my_bit {}
	public my_bit bit D0_DIR = #addr 0x0000 + INDEX;
	public my_bit bit D0_OUT = #addr 0x0100 + INDEX;
	public my_bit bit D0_IN = #addr 0x0200 + INDEX;
	public my_bit bit D0_PUL = #addr 0x0300 + INDEX;
}
D30.my_bit = my_bit;
public unit D30
{
	public const uint8 INDEX = 30;
	public link memory my_bit {}
	public my_bit bit D0_DIR = #addr 0x0000 + INDEX;
	public my_bit bit D0_OUT = #addr 0x0100 + INDEX;
	public my_bit bit D0_IN = #addr 0x0200 + INDEX;
	public my_bit bit D0_PUL = #addr 0x0300 + INDEX;
}
D31.my_bit = my_bit;
public unit D31
{
	public const uint8 INDEX = 31;
	public link memory my_bit {}
	public my_bit bit D0_DIR = #addr 0x0000 + INDEX;
	public my_bit bit D0_OUT = #addr 0x0100 + INDEX;
	public my_bit bit D0_IN = #addr 0x0200 + INDEX;
	public my_bit bit D0_PUL = #addr 0x0300 + INDEX;
}
D32.my_bit = my_bit;
public unit D32
{
	public const uint8 INDEX = 32;
	public link memory my_bit {}
	public my_bit bit D0_DIR = #addr 0x0000 + INDEX;
	public my_bit bit D0_OUT = #addr 0x0100 + INDEX;
	public my_bit bit D0_IN = #addr 0x0200 + INDEX;
	public my_bit bit D0_PUL = #addr 0x0300 + INDEX;
}
D33.my_bit = my_bit;
public unit D33
{
	public const uint8 INDEX = 33;
	public link memory my_bit {}
	public my_bit bit D0_DIR = #addr 0x0000 + INDEX;
	public my_bit bit D0_OUT = #addr 0x0100 + INDEX;
	public my_bit bit D0_IN = #addr 0x0200 + INDEX;
	public my_bit bit D0_PUL = #addr 0x0300 + INDEX;
}
D34.my_bit = my_bit;
public unit D34
{
	public const uint8 INDEX = 34;
	public link memory my_bit {}
	public my_bit bit D0_DIR = #addr 0x0000 + INDEX;
	public my_bit bit D0_OUT = #addr 0x0100 + INDEX;
	public my_bit bit D0_IN = #addr 0x0200 + INDEX;
	public my_bit bit D0_PUL = #addr 0x0300 + INDEX;
}
D35.my_bit = my_bit;
public unit D35
{
	public const uint8 INDEX = 35;
	public link memory my_bit {}
	public my_bit bit D0_DIR = #addr 0x0000 + INDEX;
	public my_bit bit D0_OUT = #addr 0x0100 + INDEX;
	public my_bit bit D0_IN = #addr 0x0200 + INDEX;
	public my_bit bit D0_PUL = #addr 0x0300 + INDEX;
}
D36.my_bit = my_bit;
public unit D36
{
	public const uint8 INDEX = 36;
	public link memory my_bit {}
	public my_bit bit D0_DIR = #addr 0x0000 + INDEX;
	public my_bit bit D0_OUT = #addr 0x0100 + INDEX;
	public my_bit bit D0_IN = #addr 0x0200 + INDEX;
	public my_bit bit D0_PUL = #addr 0x0300 + INDEX;
}
D37.my_bit = my_bit;
public unit D37
{
	public const uint8 INDEX = 37;
	public link memory my_bit {}
	public my_bit bit D0_DIR = #addr 0x0000 + INDEX;
	public my_bit bit D0_OUT = #addr 0x0100 + INDEX;
	public my_bit bit D0_IN = #addr 0x0200 + INDEX;
	public my_bit bit D0_PUL = #addr 0x0300 + INDEX;
}
D38.my_bit = my_bit;
public unit D38
{
	public const uint8 INDEX = 38;
	public link memory my_bit {}
	public my_bit bit D0_DIR = #addr 0x0000 + INDEX;
	public my_bit bit D0_OUT = #addr 0x0100 + INDEX;
	public my_bit bit D0_IN = #addr 0x0200 + INDEX;
	public my_bit bit D0_PUL = #addr 0x0300 + INDEX;
}
D39.my_bit = my_bit;
public unit D39
{
	public const uint8 INDEX = 39;
	public link memory my_bit {}
	public my_bit bit D0_DIR = #addr 0x0000 + INDEX;
	public my_bit bit D0_OUT = #addr 0x0100 + INDEX;
	public my_bit bit D0_IN = #addr 0x0200 + INDEX;
	public my_bit bit D0_PUL = #addr 0x0300 + INDEX;
}
D40.my_bit = my_bit;
public unit D40
{
	public const uint8 INDEX = 40;
	public link memory my_bit {}
	public my_bit bit D0_DIR = #addr 0x0000 + INDEX;
	public my_bit bit D0_OUT = #addr 0x0100 + INDEX;
	public my_bit bit D0_IN = #addr 0x0200 + INDEX;
	public my_bit bit D0_PUL = #addr 0x0300 + INDEX;
}
D41.my_bit = my_bit;
public unit D41
{
	public const uint8 INDEX = 41;
	public link memory my_bit {}
	public my_bit bit D0_DIR = #addr 0x0000 + INDEX;
	public my_bit bit D0_OUT = #addr 0x0100 + INDEX;
	public my_bit bit D0_IN = #addr 0x0200 + INDEX;
	public my_bit bit D0_PUL = #addr 0x0300 + INDEX;
}
D42.my_bit = my_bit;
public unit D42
{
	public const uint8 INDEX = 42;
	public link memory my_bit {}
	public my_bit bit D0_DIR = #addr 0x0000 + INDEX;
	public my_bit bit D0_OUT = #addr 0x0100 + INDEX;
	public my_bit bit D0_IN = #addr 0x0200 + INDEX;
	public my_bit bit D0_PUL = #addr 0x0300 + INDEX;
}
D43.my_bit = my_bit;
public unit D43
{
	public const uint8 INDEX = 43;
	public link memory my_bit {}
	public my_bit bit D0_DIR = #addr 0x0000 + INDEX;
	public my_bit bit D0_OUT = #addr 0x0100 + INDEX;
	public my_bit bit D0_IN = #addr 0x0200 + INDEX;
	public my_bit bit D0_PUL = #addr 0x0300 + INDEX;
}
D44.my_bit = my_bit;
public unit D44
{
	public const uint8 INDEX = 44;
	public link memory my_bit {}
	public my_bit bit D0_DIR = #addr 0x0000 + INDEX;
	public my_bit bit D0_OUT = #addr 0x0100 + INDEX;
	public my_bit bit D0_IN = #addr 0x0200 + INDEX;
	public my_bit bit D0_PUL = #addr 0x0300 + INDEX;
}
D45.my_bit = my_bit;
public unit D45
{
	public const uint8 INDEX = 45;
	public link memory my_bit {}
	public my_bit bit D0_DIR = #addr 0x0000 + INDEX;
	public my_bit bit D0_OUT = #addr 0x0100 + INDEX;
	public my_bit bit D0_IN = #addr 0x0200 + INDEX;
	public my_bit bit D0_PUL = #addr 0x0300 + INDEX;
}
D46.my_bit = my_bit;
public unit D46
{
	public const uint8 INDEX = 46;
	public link memory my_bit {}
	public my_bit bit D0_DIR = #addr 0x0000 + INDEX;
	public my_bit bit D0_OUT = #addr 0x0100 + INDEX;
	public my_bit bit D0_IN = #addr 0x0200 + INDEX;
	public my_bit bit D0_PUL = #addr 0x0300 + INDEX;
}
D47.my_bit = my_bit;
public unit D47
{
	public const uint8 INDEX = 47;
	public link memory my_bit {}
	public my_bit bit D0_DIR = #addr 0x0000 + INDEX;
	public my_bit bit D0_OUT = #addr 0x0100 + INDEX;
	public my_bit bit D0_IN = #addr 0x0200 + INDEX;
	public my_bit bit D0_PUL = #addr 0x0300 + INDEX;
}
D48.my_bit = my_bit;
public unit D48
{
	public const uint8 INDEX = 48;
	public link memory my_bit {}
	public my_bit bit D0_DIR = #addr 0x0000 + INDEX;
	public my_bit bit D0_OUT = #addr 0x0100 + INDEX;
	public my_bit bit D0_IN = #addr 0x0200 + INDEX;
	public my_bit bit D0_PUL = #addr 0x0300 + INDEX;
}
D49.my_bit = my_bit;
public unit D49
{
	public const uint8 INDEX = 49;
	public link memory my_bit {}
	public my_bit bit D0_DIR = #addr 0x0000 + INDEX;
	public my_bit bit D0_OUT = #addr 0x0100 + INDEX;
	public my_bit bit D0_IN = #addr 0x0200 + INDEX;
	public my_bit bit D0_PUL = #addr 0x0300 + INDEX;
}

D50.my_bit = my_bit;
public unit D50
{
	public const uint8 INDEX = 50;
	public link memory my_bit {}
	public my_bit bit D0_DIR = #addr 0x0000 + INDEX;
	public my_bit bit D0_OUT = #addr 0x0100 + INDEX;
	public my_bit bit D0_IN = #addr 0x0200 + INDEX;
	public my_bit bit D0_PUL = #addr 0x0300 + INDEX;
}
D51.my_bit = my_bit;
public unit D51
{
	public const uint8 INDEX = 51;
	public link memory my_bit {}
	public my_bit bit D0_DIR = #addr 0x0000 + INDEX;
	public my_bit bit D0_OUT = #addr 0x0100 + INDEX;
	public my_bit bit D0_IN = #addr 0x0200 + INDEX;
	public my_bit bit D0_PUL = #addr 0x0300 + INDEX;
}
D52.my_bit = my_bit;
public unit D52
{
	public const uint8 INDEX = 52;
	public link memory my_bit {}
	public my_bit bit D0_DIR = #addr 0x0000 + INDEX;
	public my_bit bit D0_OUT = #addr 0x0100 + INDEX;
	public my_bit bit D0_IN = #addr 0x0200 + INDEX;
	public my_bit bit D0_PUL = #addr 0x0300 + INDEX;
}
D53.my_bit = my_bit;
public unit D53
{
	public const uint8 INDEX = 53;
	public link memory my_bit {}
	public my_bit bit D0_DIR = #addr 0x0000 + INDEX;
	public my_bit bit D0_OUT = #addr 0x0100 + INDEX;
	public my_bit bit D0_IN = #addr 0x0200 + INDEX;
	public my_bit bit D0_PUL = #addr 0x0300 + INDEX;
}
A0.my_bit = my_bit;
public unit A0
{
	public const uint8 INDEX = 54;
	public link memory my_bit {}
	public my_bit bit D0_DIR = #addr 0x0000 + INDEX;
	public my_bit bit D0_OUT = #addr 0x0100 + INDEX;
	public my_bit bit D0_IN = #addr 0x0200 + INDEX;
	public my_bit bit D0_PUL = #addr 0x0300 + INDEX;
}
A1.my_bit = my_bit;
public unit A1
{
	public const uint8 INDEX = 55;
	public link memory my_bit {}
	public my_bit bit D0_DIR = #addr 0x0000 + INDEX;
	public my_bit bit D0_OUT = #addr 0x0100 + INDEX;
	public my_bit bit D0_IN = #addr 0x0200 + INDEX;
	public my_bit bit D0_PUL = #addr 0x0300 + INDEX;
}
A2.my_bit = my_bit;
public unit A2
{
	public const uint8 INDEX = 56;
	public link memory my_bit {}
	public my_bit bit D0_DIR = #addr 0x0000 + INDEX;
	public my_bit bit D0_OUT = #addr 0x0100 + INDEX;
	public my_bit bit D0_IN = #addr 0x0200 + INDEX;
	public my_bit bit D0_PUL = #addr 0x0300 + INDEX;
}
A3.my_bit = my_bit;
public unit A3
{
	public const uint8 INDEX = 57;
	public link memory my_bit {}
	public my_bit bit D0_DIR = #addr 0x0000 + INDEX;
	public my_bit bit D0_OUT = #addr 0x0100 + INDEX;
	public my_bit bit D0_IN = #addr 0x0200 + INDEX;
	public my_bit bit D0_PUL = #addr 0x0300 + INDEX;
}
A4.my_bit = my_bit;
public unit A4
{
	public const uint8 INDEX = 58;
	public link memory my_bit {}
	public my_bit bit D0_DIR = #addr 0x0000 + INDEX;
	public my_bit bit D0_OUT = #addr 0x0100 + INDEX;
	public my_bit bit D0_IN = #addr 0x0200 + INDEX;
	public my_bit bit D0_PUL = #addr 0x0300 + INDEX;
}
A5.my_bit = my_bit;
public unit A5
{
	public const uint8 INDEX = 59;
	public link memory my_bit {}
	public my_bit bit D0_DIR = #addr 0x0000 + INDEX;
	public my_bit bit D0_OUT = #addr 0x0100 + INDEX;
	public my_bit bit D0_IN = #addr 0x0200 + INDEX;
	public my_bit bit D0_PUL = #addr 0x0300 + INDEX;
}
A6.my_bit = my_bit;
public unit A6
{
	public const uint8 INDEX = 60;
	public link memory my_bit {}
	public my_bit bit D0_DIR = #addr 0x0000 + INDEX;
	public my_bit bit D0_OUT = #addr 0x0100 + INDEX;
	public my_bit bit D0_IN = #addr 0x0200 + INDEX;
	public my_bit bit D0_PUL = #addr 0x0300 + INDEX;
}
A7.my_bit = my_bit;
public unit A7
{
	public const uint8 INDEX = 61;
	public link memory my_bit {}
	public my_bit bit D0_DIR = #addr 0x0000 + INDEX;
	public my_bit bit D0_OUT = #addr 0x0100 + INDEX;
	public my_bit bit D0_IN = #addr 0x0200 + INDEX;
	public my_bit bit D0_PUL = #addr 0x0300 + INDEX;
}
A8.my_bit = my_bit;
public unit A8
{
	public const uint8 INDEX = 62;
	public link memory my_bit {}
	public my_bit bit D0_DIR = #addr 0x0000 + INDEX;
	public my_bit bit D0_OUT = #addr 0x0100 + INDEX;
	public my_bit bit D0_IN = #addr 0x0200 + INDEX;
	public my_bit bit D0_PUL = #addr 0x0300 + INDEX;
}
A9.my_bit = my_bit;
public unit A9
{
	public const uint8 INDEX = 63;
	public link memory my_bit {}
	public my_bit bit D0_DIR = #addr 0x0000 + INDEX;
	public my_bit bit D0_OUT = #addr 0x0100 + INDEX;
	public my_bit bit D0_IN = #addr 0x0200 + INDEX;
	public my_bit bit D0_PUL = #addr 0x0300 + INDEX;
}
A10.my_bit = my_bit;
public unit A10
{
	public const uint8 INDEX = 64;
	public link memory my_bit {}
	public my_bit bit D0_DIR = #addr 0x0000 + INDEX;
	public my_bit bit D0_OUT = #addr 0x0100 + INDEX;
	public my_bit bit D0_IN = #addr 0x0200 + INDEX;
	public my_bit bit D0_PUL = #addr 0x0300 + INDEX;
}
A11.my_bit = my_bit;
public unit A11
{
	public const uint8 INDEX = 65;
	public link memory my_bit {}
	public my_bit bit D0_DIR = #addr 0x0000 + INDEX;
	public my_bit bit D0_OUT = #addr 0x0100 + INDEX;
	public my_bit bit D0_IN = #addr 0x0200 + INDEX;
	public my_bit bit D0_PUL = #addr 0x0300 + INDEX;
}
A12.my_bit = my_bit;
public unit A12
{
	public const uint8 INDEX = 66;
	public link memory my_bit {}
	public my_bit bit D0_DIR = #addr 0x0000 + INDEX;
	public my_bit bit D0_OUT = #addr 0x0100 + INDEX;
	public my_bit bit D0_IN = #addr 0x0200 + INDEX;
	public my_bit bit D0_PUL = #addr 0x0300 + INDEX;
}
A13.my_bit = my_bit;
public unit A13
{
	public const uint8 INDEX = 67;
	public link memory my_bit {}
	public my_bit bit D0_DIR = #addr 0x0000 + INDEX;
	public my_bit bit D0_OUT = #addr 0x0100 + INDEX;
	public my_bit bit D0_IN = #addr 0x0200 + INDEX;
	public my_bit bit D0_PUL = #addr 0x0300 + INDEX;
}
A14.my_bit = my_bit;
public unit A14
{
	public const uint8 INDEX = 68;
	public link memory my_bit {}
	public my_bit bit D0_DIR = #addr 0x0000 + INDEX;
	public my_bit bit D0_OUT = #addr 0x0100 + INDEX;
	public my_bit bit D0_IN = #addr 0x0200 + INDEX;
	public my_bit bit D0_PUL = #addr 0x0300 + INDEX;
}
A15.my_bit = my_bit;
public unit A15
{
	public const uint8 INDEX = 69;
	public link memory my_bit {}
	public my_bit bit D0_DIR = #addr 0x0000 + INDEX;
	public my_bit bit D0_OUT = #addr 0x0100 + INDEX;
	public my_bit bit D0_IN = #addr 0x0200 + INDEX;
	public my_bit bit D0_PUL = #addr 0x0300 + INDEX;
}
