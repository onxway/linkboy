
//------------------------------
//图形化连接端口

#include <system\arduino\MEGA2560-io.txt>

unit PORT
{
	public const int8 UART_ID = 0;
	public link bit D2_DIR = #.COM_MCU.DDRE.0; public link bit D2_IN = #.COM_MCU.PINE.0; public link bit D2_OUT = #.COM_MCU.PORTE.0; public link bit D2_PUL = #.COM_MCU.PORTE.0; 
	public link bit D3_DIR = #.COM_MCU.DDRE.1; public link bit D3_IN = #.COM_MCU.PINE.1; public link bit D3_OUT = #.COM_MCU.PORTE.1; public link bit D3_PUL = #.COM_MCU.PORTE.1;
}
unit GND
{
	public const uint8 TYPE = 0;
}
unit GND1
{
	public const uint8 TYPE = 0;
}
unit GND2
{
	public const uint8 TYPE = 0;
}
unit VCC1
{
	public const uint8 TYPE = 2;
}

unit D0
{
	public const uint8 TYPE = 10;
	public link bit D0_DIR = #.COM_MCU.DDRE.0; public link bit D0_IN = #.COM_MCU.PINE.0; public link bit D0_OUT = #.COM_MCU.PORTE.0; public link bit D0_PUL = #.COM_MCU.PORTE.0;
}
unit D1
{
	public const uint8 TYPE = 10;
	public link bit D0_DIR = #.COM_MCU.DDRE.1; public link bit D0_IN = #.COM_MCU.PINE.1; public link bit D0_OUT = #.COM_MCU.PORTE.1; public link bit D0_PUL = #.COM_MCU.PORTE.1;
}
unit D2
{
	public const uint8 TYPE = 10;
	public link bit D0_DIR = #.COM_MCU.DDRE.4; public link bit D0_IN = #.COM_MCU.PINE.4; public link bit D0_OUT = #.COM_MCU.PORTE.4; public link bit D0_PUL = #.COM_MCU.PORTE.4;
}
unit D3
{
	public const uint8 TYPE = 10;
	public link bit D0_DIR = #.COM_MCU.DDRE.5; public link bit D0_IN = #.COM_MCU.PINE.5; public link bit D0_OUT = #.COM_MCU.PORTE.5; public link bit D0_PUL = #.COM_MCU.PORTE.5;
}
unit D4
{
	public const uint8 TYPE = 10;
	public link bit D0_DIR = #.COM_MCU.DDRG.5; public link bit D0_IN = #.COM_MCU.PING.5; public link bit D0_OUT = #.COM_MCU.PORTG.5; public link bit D0_PUL = #.COM_MCU.PORTG.5;
}
unit D5
{
	public const uint8 TYPE = 10;
	public link bit D0_DIR = #.COM_MCU.DDRE.3; public link bit D0_IN = #.COM_MCU.PINE.3; public link bit D0_OUT = #.COM_MCU.PORTE.3; public link bit D0_PUL = #.COM_MCU.PORTE.3;
}
unit D6
{
	public const uint8 TYPE = 10;
	public link bit D0_DIR = #.COM_MCU.DDRH.3; public link bit D0_IN = #.COM_MCU.PINH.3; public link bit D0_OUT = #.COM_MCU.PORTH.3; public link bit D0_PUL = #.COM_MCU.PORTH.3;
}
unit D7
{
	public const uint8 TYPE = 10;
	public link bit D0_DIR = #.COM_MCU.DDRH.4; public link bit D0_IN = #.COM_MCU.PINH.4; public link bit D0_OUT = #.COM_MCU.PORTH.4; public link bit D0_PUL = #.COM_MCU.PORTH.4;
}
unit D8
{
	public const uint8 TYPE = 10;
	public link bit D0_DIR = #.COM_MCU.DDRH.5; public link bit D0_IN = #.COM_MCU.PINH.5; public link bit D0_OUT = #.COM_MCU.PORTH.5; public link bit D0_PUL = #.COM_MCU.PORTH.5;
}
unit D9
{
	public const uint8 TYPE = 10;
	public link bit D0_DIR = #.COM_MCU.DDRH.6; public link bit D0_IN = #.COM_MCU.PINH.6; public link bit D0_OUT = #.COM_MCU.PORTH.6; public link bit D0_PUL = #.COM_MCU.PORTH.6;
}
unit D10
{
	public const uint8 TYPE = 10;
	public link bit D0_DIR = #.COM_MCU.DDRB.4; public link bit D0_IN = #.COM_MCU.PINB.4; public link bit D0_OUT = #.COM_MCU.PORTB.4; public link bit D0_PUL = #.COM_MCU.PORTB.4;
}
unit D11
{
	public const uint8 TYPE = 10;
	public link bit D0_DIR = #.COM_MCU.DDRB.5; public link bit D0_IN = #.COM_MCU.PINB.5; public link bit D0_OUT = #.COM_MCU.PORTB.5; public link bit D0_PUL = #.COM_MCU.PORTB.5;
}
unit D12
{
	public const uint8 TYPE = 10;
	public link bit D0_DIR = #.COM_MCU.DDRB.6; public link bit D0_IN = #.COM_MCU.PINB.6; public link bit D0_OUT = #.COM_MCU.PORTB.6; public link bit D0_PUL = #.COM_MCU.PORTB.6;
}
unit D13
{
	public const uint8 TYPE = 10;
	public link bit D0_DIR = #.COM_MCU.DDRB.7; public link bit D0_IN = #.COM_MCU.PINB.7; public link bit D0_OUT = #.COM_MCU.PORTB.7; public link bit D0_PUL = #.COM_MCU.PORTB.7;
}

unit D14
{
	public const uint8 TYPE = 10;
	public link bit D0_DIR = #.COM_MCU.DDRJ.1; public link bit D0_IN = #.COM_MCU.PINJ.1; public link bit D0_OUT = #.COM_MCU.PORTJ.1; public link bit D0_PUL = #.COM_MCU.PORTJ.1;
}
unit D15
{
	public const uint8 TYPE = 10;
	public link bit D0_DIR = #.COM_MCU.DDRJ.0; public link bit D0_IN = #.COM_MCU.PINJ.0; public link bit D0_OUT = #.COM_MCU.PORTJ.0; public link bit D0_PUL = #.COM_MCU.PORTJ.0;
}
unit D16
{
	public const uint8 TYPE = 10;
	public link bit D0_DIR = #.COM_MCU.DDRH.1; public link bit D0_IN = #.COM_MCU.PINH.1; public link bit D0_OUT = #.COM_MCU.PORTH.1; public link bit D0_PUL = #.COM_MCU.PORTH.1;
}
unit D17
{
	public const uint8 TYPE = 10;
	public link bit D0_DIR = #.COM_MCU.DDRH.0; public link bit D0_IN = #.COM_MCU.PINH.0; public link bit D0_OUT = #.COM_MCU.PORTH.0; public link bit D0_PUL = #.COM_MCU.PORTH.0;
}
unit D18
{
	public const uint8 TYPE = 10;
	public link bit D0_DIR = #.COM_MCU.DDRD.3; public link bit D0_IN = #.COM_MCU.PIND.3; public link bit D0_OUT = #.COM_MCU.PORTD.3; public link bit D0_PUL = #.COM_MCU.PORTD.3;
}
unit D19
{
	public const uint8 TYPE = 10;
	public const int8 INT_INDEX = 2;
	public link bit D0_DIR = #.COM_MCU.DDRD.2; public link bit D0_IN = #.COM_MCU.PIND.2; public link bit D0_OUT = #.COM_MCU.PORTD.2; public link bit D0_PUL = #.COM_MCU.PORTD.2;
}
unit D20
{
	public const uint8 TYPE = 10;
	public const int8 INT_INDEX = 1;
	public link bit D0_DIR = #.COM_MCU.DDRD.1; public link bit D0_IN = #.COM_MCU.PIND.1; public link bit D0_OUT = #.COM_MCU.PORTD.1; public link bit D0_PUL = #.COM_MCU.PORTD.1;
}
unit D21
{
	public const uint8 TYPE = 10;
	public const int8 INT_INDEX = 0;
	public link bit D0_DIR = #.COM_MCU.DDRD.0; public link bit D0_IN = #.COM_MCU.PIND.0; public link bit D0_OUT = #.COM_MCU.PORTD.0; public link bit D0_PUL = #.COM_MCU.PORTD.0;
}


unit D22
{
	public const uint8 TYPE = 10;
	public link bit D0_DIR = #.COM_MCU.DDRA.0; public link bit D0_IN = #.COM_MCU.PINA.0; public link bit D0_OUT = #.COM_MCU.PORTA.0; public link bit D0_PUL = #.COM_MCU.PORTA.0;
}
unit D23
{
	public const uint8 TYPE = 10;
	public link bit D0_DIR = #.COM_MCU.DDRA.1; public link bit D0_IN = #.COM_MCU.PINA.1; public link bit D0_OUT = #.COM_MCU.PORTA.1; public link bit D0_PUL = #.COM_MCU.PORTA.1;
}
unit D24
{
	public const uint8 TYPE = 10;
	public link bit D0_DIR = #.COM_MCU.DDRA.2; public link bit D0_IN = #.COM_MCU.PINA.2; public link bit D0_OUT = #.COM_MCU.PORTA.2; public link bit D0_PUL = #.COM_MCU.PORTA.2;
}
unit D25
{
	public const uint8 TYPE = 10;
	public link bit D0_DIR = #.COM_MCU.DDRA.3; public link bit D0_IN = #.COM_MCU.PINA.3; public link bit D0_OUT = #.COM_MCU.PORTA.3; public link bit D0_PUL = #.COM_MCU.PORTA.3;
}
unit D26
{
	public const uint8 TYPE = 10;
	public link bit D0_DIR = #.COM_MCU.DDRA.4; public link bit D0_IN = #.COM_MCU.PINA.4; public link bit D0_OUT = #.COM_MCU.PORTA.4; public link bit D0_PUL = #.COM_MCU.PORTA.4;
}
unit D27
{
	public const uint8 TYPE = 10;
	public link bit D0_DIR = #.COM_MCU.DDRA.5; public link bit D0_IN = #.COM_MCU.PINA.5; public link bit D0_OUT = #.COM_MCU.PORTA.5; public link bit D0_PUL = #.COM_MCU.PORTA.5;
}
unit D28
{
	public const uint8 TYPE = 10;
	public link bit D0_DIR = #.COM_MCU.DDRA.6; public link bit D0_IN = #.COM_MCU.PINA.6; public link bit D0_OUT = #.COM_MCU.PORTA.6; public link bit D0_PUL = #.COM_MCU.PORTA.6;
}
unit D29
{
	public const uint8 TYPE = 10;
	public link bit D0_DIR = #.COM_MCU.DDRA.7; public link bit D0_IN = #.COM_MCU.PINA.7; public link bit D0_OUT = #.COM_MCU.PORTA.7; public link bit D0_PUL = #.COM_MCU.PORTA.7;
}


unit D30
{
	public const uint8 TYPE = 10;
	public link bit D0_DIR = #.COM_MCU.DDRC.7; public link bit D0_IN = #.COM_MCU.PINC.7; public link bit D0_OUT = #.COM_MCU.PORTC.7; public link bit D0_PUL = #.COM_MCU.PORTC.7;
}
unit D31
{
	public const uint8 TYPE = 10;
	public link bit D0_DIR = #.COM_MCU.DDRC.6; public link bit D0_IN = #.COM_MCU.PINC.6; public link bit D0_OUT = #.COM_MCU.PORTC.6; public link bit D0_PUL = #.COM_MCU.PORTC.6;
}
unit D32
{
	public const uint8 TYPE = 10;
	public link bit D0_DIR = #.COM_MCU.DDRC.5; public link bit D0_IN = #.COM_MCU.PINC.5; public link bit D0_OUT = #.COM_MCU.PORTC.5; public link bit D0_PUL = #.COM_MCU.PORTC.5;
}
unit D33
{
	public const uint8 TYPE = 10;
	public link bit D0_DIR = #.COM_MCU.DDRC.4; public link bit D0_IN = #.COM_MCU.PINC.4; public link bit D0_OUT = #.COM_MCU.PORTC.4; public link bit D0_PUL = #.COM_MCU.PORTC.4;
}
unit D34
{
	public const uint8 TYPE = 10;
	public link bit D0_DIR = #.COM_MCU.DDRC.3; public link bit D0_IN = #.COM_MCU.PINC.3; public link bit D0_OUT = #.COM_MCU.PORTC.3; public link bit D0_PUL = #.COM_MCU.PORTC.3;
}
unit D35
{
	public const uint8 TYPE = 10;
	public link bit D0_DIR = #.COM_MCU.DDRC.2; public link bit D0_IN = #.COM_MCU.PINC.2; public link bit D0_OUT = #.COM_MCU.PORTC.2; public link bit D0_PUL = #.COM_MCU.PORTC.2;
}
unit D36
{
	public const uint8 TYPE = 10;
	public link bit D0_DIR = #.COM_MCU.DDRC.1; public link bit D0_IN = #.COM_MCU.PINC.1; public link bit D0_OUT = #.COM_MCU.PORTC.1; public link bit D0_PUL = #.COM_MCU.PORTC.1;
}
unit D37
{
	public const uint8 TYPE = 10;
	public link bit D0_DIR = #.COM_MCU.DDRC.0; public link bit D0_IN = #.COM_MCU.PINC.0; public link bit D0_OUT = #.COM_MCU.PORTC.0; public link bit D0_PUL = #.COM_MCU.PORTC.0;
}


unit D38
{
	public const uint8 TYPE = 10;
	public link bit D0_DIR = #.COM_MCU.DDRD.7; public link bit D0_IN = #.COM_MCU.PIND.7; public link bit D0_OUT = #.COM_MCU.PORTD.7; public link bit D0_PUL = #.COM_MCU.PORTD.7;
}
unit D39
{
	public const uint8 TYPE = 10;
	public link bit D0_DIR = #.COM_MCU.DDRG.2; public link bit D0_IN = #.COM_MCU.PING.2; public link bit D0_OUT = #.COM_MCU.PORTG.2; public link bit D0_PUL = #.COM_MCU.PORTG.2;
}
unit D40
{
	public const uint8 TYPE = 10;
	public link bit D0_DIR = #.COM_MCU.DDRG.1; public link bit D0_IN = #.COM_MCU.PING.1; public link bit D0_OUT = #.COM_MCU.PORTG.1; public link bit D0_PUL = #.COM_MCU.PORTG.1;
}
unit D41
{
	public const uint8 TYPE = 10;
	public link bit D0_DIR = #.COM_MCU.DDRG.0; public link bit D0_IN = #.COM_MCU.PING.0; public link bit D0_OUT = #.COM_MCU.PORTG.0; public link bit D0_PUL = #.COM_MCU.PORTG.0;
}


unit D42
{
	public const uint8 TYPE = 10;
	public link bit D0_DIR = #.COM_MCU.DDRL.7; public link bit D0_IN = #.COM_MCU.PINL.7; public link bit D0_OUT = #.COM_MCU.PORTL.7; public link bit D0_PUL = #.COM_MCU.PORTL.7;
}
unit D43
{
	public const uint8 TYPE = 10;
	public link bit D0_DIR = #.COM_MCU.DDRL.6; public link bit D0_IN = #.COM_MCU.PINL.6; public link bit D0_OUT = #.COM_MCU.PORTL.6; public link bit D0_PUL = #.COM_MCU.PORTL.6;
}
unit D44
{
	public const uint8 TYPE = 10;
	public link bit D0_DIR = #.COM_MCU.DDRL.5; public link bit D0_IN = #.COM_MCU.PINL.5; public link bit D0_OUT = #.COM_MCU.PORTL.5; public link bit D0_PUL = #.COM_MCU.PORTL.5;
}
unit D45
{
	public const uint8 TYPE = 10;
	public link bit D0_DIR = #.COM_MCU.DDRL.4; public link bit D0_IN = #.COM_MCU.PINL.4; public link bit D0_OUT = #.COM_MCU.PORTL.4; public link bit D0_PUL = #.COM_MCU.PORTL.4;
}
unit D46
{
	public const uint8 TYPE = 10;
	public link bit D0_DIR = #.COM_MCU.DDRL.3; public link bit D0_IN = #.COM_MCU.PINL.3; public link bit D0_OUT = #.COM_MCU.PORTL.3; public link bit D0_PUL = #.COM_MCU.PORTL.3;
}
unit D47
{
	public const uint8 TYPE = 10;
	public link bit D0_DIR = #.COM_MCU.DDRL.2; public link bit D0_IN = #.COM_MCU.PINL.2; public link bit D0_OUT = #.COM_MCU.PORTL.2; public link bit D0_PUL = #.COM_MCU.PORTL.2;
}
unit D48
{
	public const uint8 TYPE = 10;
	public link bit D0_DIR = #.COM_MCU.DDRL.1; public link bit D0_IN = #.COM_MCU.PINL.1; public link bit D0_OUT = #.COM_MCU.PORTL.1; public link bit D0_PUL = #.COM_MCU.PORTL.1;
}
unit D49
{
	public const uint8 TYPE = 10;
	public link bit D0_DIR = #.COM_MCU.DDRL.0; public link bit D0_IN = #.COM_MCU.PINL.0; public link bit D0_OUT = #.COM_MCU.PORTL.0; public link bit D0_PUL = #.COM_MCU.PORTL.0;
}


unit D50
{
	public const uint8 TYPE = 10;
	public link bit D0_DIR = #.COM_MCU.DDRB.3; public link bit D0_IN = #.COM_MCU.PINB.3; public link bit D0_OUT = #.COM_MCU.PORTB.3; public link bit D0_PUL = #.COM_MCU.PORTB.3;
}
unit D51
{
	public const uint8 TYPE = 10;
	public link bit D0_DIR = #.COM_MCU.DDRB.2; public link bit D0_IN = #.COM_MCU.PINB.2; public link bit D0_OUT = #.COM_MCU.PORTB.2; public link bit D0_PUL = #.COM_MCU.PORTB.2;
}
unit D52
{
	public const uint8 TYPE = 10;
	public link bit D0_DIR = #.COM_MCU.DDRB.1; public link bit D0_IN = #.COM_MCU.PINB.1; public link bit D0_OUT = #.COM_MCU.PORTB.1; public link bit D0_PUL = #.COM_MCU.PORTB.1;
}
unit D53
{
	public const uint8 TYPE = 10;
	public link bit D0_DIR = #.COM_MCU.DDRB.0; public link bit D0_IN = #.COM_MCU.PINB.0; public link bit D0_OUT = #.COM_MCU.PORTB.0; public link bit D0_PUL = #.COM_MCU.PORTB.0;
}



//AD口
unit A0
{
	public const uint8 TYPE = 10;
	public const int8 AD_INDEX = 0;
	public link bit D0_DIR = #.COM_MCU.DDRF.0; public link bit D0_IN = #.COM_MCU.PINF.0; public link bit D0_OUT = #.COM_MCU.PORTF.0; public link bit D0_PUL = #.COM_MCU.PORTF.0;
}
unit A1
{
	public const uint8 TYPE = 10;
	public const int8 AD_INDEX = 1;
	public link bit D0_DIR = #.COM_MCU.DDRF.1; public link bit D0_IN = #.COM_MCU.PINF.1; public link bit D0_OUT = #.COM_MCU.PORTF.1; public link bit D0_PUL = #.COM_MCU.PORTF.1;
}
unit A2
{
	public const uint8 TYPE = 10;
	public const int8 AD_INDEX = 2;
	public link bit D0_DIR = #.COM_MCU.DDRF.2; public link bit D0_IN = #.COM_MCU.PINF.2; public link bit D0_OUT = #.COM_MCU.PORTF.2; public link bit D0_PUL = #.COM_MCU.PORTF.2;
}
unit A3
{
	public const uint8 TYPE = 10;
	public const int8 AD_INDEX = 3;
	public link bit D0_DIR = #.COM_MCU.DDRF.3; public link bit D0_IN = #.COM_MCU.PINF.3; public link bit D0_OUT = #.COM_MCU.PORTF.3; public link bit D0_PUL = #.COM_MCU.PORTF.3;
}
unit A4
{
	public const uint8 TYPE = 10;
	public const int8 AD_INDEX = 4;
	public link bit D0_DIR = #.COM_MCU.DDRF.4; public link bit D0_IN = #.COM_MCU.PINF.4; public link bit D0_OUT = #.COM_MCU.PORTF.4; public link bit D0_PUL = #.COM_MCU.PORTF.4;
}
unit A5
{
	public const uint8 TYPE = 10;
	public const int8 AD_INDEX = 5;
	public link bit D0_DIR = #.COM_MCU.DDRF.5; public link bit D0_IN = #.COM_MCU.PINF.5; public link bit D0_OUT = #.COM_MCU.PORTF.5; public link bit D0_PUL = #.COM_MCU.PORTF.5;
}
unit A6
{
	public const uint8 TYPE = 10;
	public const int8 AD_INDEX = 6;
	public link bit D0_DIR = #.COM_MCU.DDRF.6; public link bit D0_IN = #.COM_MCU.PINF.6; public link bit D0_OUT = #.COM_MCU.PORTF.6; public link bit D0_PUL = #.COM_MCU.PORTF.6;
}
unit A7
{
	public const uint8 TYPE = 10;
	public const int8 AD_INDEX = 7;
	public link bit D0_DIR = #.COM_MCU.DDRF.7; public link bit D0_IN = #.COM_MCU.PINF.7; public link bit D0_OUT = #.COM_MCU.PORTF.7; public link bit D0_PUL = #.COM_MCU.PORTF.7;
}
//AD口
unit A8
{
	public const uint8 TYPE = 10;
	public const int8 AD_INDEX = 8;
	public link bit D0_DIR = #.COM_MCU.DDRK.0; public link bit D0_IN = #.COM_MCU.PINK.0; public link bit D0_OUT = #.COM_MCU.PORTK.0; public link bit D0_PUL = #.COM_MCU.PORTK.0;
}
unit A9
{
	public const uint8 TYPE = 10;
	public const int8 AD_INDEX = 9;
	public link bit D0_DIR = #.COM_MCU.DDRK.1; public link bit D0_IN = #.COM_MCU.PINK.1; public link bit D0_OUT = #.COM_MCU.PORTK.1; public link bit D0_PUL = #.COM_MCU.PORTK.1;
}
unit A10
{
	public const uint8 TYPE = 10;
	public const int8 AD_INDEX = 10;
	public link bit D0_DIR = #.COM_MCU.DDRK.2; public link bit D0_IN = #.COM_MCU.PINK.2; public link bit D0_OUT = #.COM_MCU.PORTK.2; public link bit D0_PUL = #.COM_MCU.PORTK.2;
}
unit A11
{
	public const uint8 TYPE = 10;
	public const int8 AD_INDEX = 11;
	public link bit D0_DIR = #.COM_MCU.DDRK.3; public link bit D0_IN = #.COM_MCU.PINK.3; public link bit D0_OUT = #.COM_MCU.PORTK.3; public link bit D0_PUL = #.COM_MCU.PORTK.3;
}
unit A12
{
	public const uint8 TYPE = 10;
	public const int8 AD_INDEX = 12;
	public link bit D0_DIR = #.COM_MCU.DDRK.4; public link bit D0_IN = #.COM_MCU.PINK.4; public link bit D0_OUT = #.COM_MCU.PORTK.4; public link bit D0_PUL = #.COM_MCU.PORTK.4;
}
unit A13
{
	public const uint8 TYPE = 10;
	public const int8 AD_INDEX = 13;
	public link bit D0_DIR = #.COM_MCU.DDRK.5; public link bit D0_IN = #.COM_MCU.PINK.5; public link bit D0_OUT = #.COM_MCU.PORTK.5; public link bit D0_PUL = #.COM_MCU.PORTK.5;
}
unit A14
{
	public const uint8 TYPE = 10;
	public const int8 AD_INDEX = 14;
	public link bit D0_DIR = #.COM_MCU.DDRK.6; public link bit D0_IN = #.COM_MCU.PINK.6; public link bit D0_OUT = #.COM_MCU.PORTK.6; public link bit D0_PUL = #.COM_MCU.PORTK.6;
}
unit A15
{
	public const uint8 TYPE = 10;
	public const int8 AD_INDEX = 15;
	public link bit D0_DIR = #.COM_MCU.DDRK.7; public link bit D0_IN = #.COM_MCU.PINK.7; public link bit D0_OUT = #.COM_MCU.PORTK.7; public link bit D0_PUL = #.COM_MCU.PORTK.7;
}






