<module>

</module>
<name> Pack SerialPort,
</name>
<member> function_void OS_init OS_init,

</member>,
<member> function_void OS_thread OS_thread,

</member>,
<member> interface_reader reader0 reader0,

</member>,
<member> interface_writer writer0 writer0,

</member>,
<member> linkconst_int16_32 max_length MaxLength,

</member>,
<member> linkconst_int32_9600 baud Baud,

</member>,
<member> var_uint8 OS_EventFlag OS_EventFlag,

</member>,
<member> event receive_event ReceiveEvent,

</member>,
<member> function_void_int32 set_baud SetBaud #,

</member>,
<member> function_int32 get_data GetData,

</member>,
<member> function_bool is_ready isReady,

</member>,
<member> function_void print_return PrintReturn,

</member>,
<member> function_void_int32 print_char PrintChar #,

</member>,
<member> function_void_Cstring print_string PrintString #,

</member>,
<member> function_void_int32 print_number PrintNumber #,

</member>,
<member> function_void_Cstring print_hex_list PrintHexList #,

</member>,
