
unit SEG7X4A
{
	public const uint16 ID = 0;

	public link unit A {}
	public link unit B {}
	public link unit C {}
	public link unit D {}
	public link unit E {}
	public link unit F {}
	public link unit G {}
	public link unit H {}
	
	public link unit S1 {}
	public link unit S2 {}
	public link unit S3 {}
	public link unit S4 {}
	
	//---------------------------------------------------
	//提供的接口
	//[i] interface_char char;
	unit char
	{
		link void clear() {}
		link void clear_line(int32 l) {}
		link void write_char(int32 l,int32 c,[#.code int8*?] string) {}
		public const int8 PerWidth = 1;
	}
	char.clear = clear;
	char.clear_line = clear_line;
	char.write_char = write_char;
	
	//显示常量类型
	const uint8 NO_DATA = 0x00;
	
	//---------------------------------------------------
	//[i] var_uint8 OS_time;
	public uint8 OS_time;
	
	//四个显示单元和小数点位置
	[uint8*4] data;
	[uint8*4] data_show;
	bool is_changed;
	
	uint8 point;
	
	uint8 current_index;
	uint8 current_select;
	
	#include "$run$.txt"

	//==============================================
	//[i] function_void OS_init;
	public void OS_init()
	{
		Run_Init();
		
		is_changed = false;
		
		clear();
		clear_point();
		OS_time = 5;
		reset();
	}
	//==============================================
	//[i] function_void OS_run;
	
	//函数重定向...

	//==============================================
	//[i] function_void OS_thread;
	public void OS_thread()
	{
		if( is_changed ) {
			is_changed = false;
			data_show[0] = data[0];
			data_show[1] = data[1];
			data_show[2] = data[2];
			data_show[3] = data[3];
			DataChanged();
		}
	}
	//==============================================
	//[i] function_void clear;
	public void clear()
	{
		clear_line( 1 );
	}
	//==============================================
	//[i] function_void clear_line int32;
	public void clear_line(int32 l)
	{
		if( l != 1 ) return;
		data[0] = NO_SEG;
		data[1] = NO_SEG;
		data[2] = NO_SEG;
		data[3] = NO_SEG;
		
		is_changed = true;
	}
	//==============================================
	//[i] function_void write_char int32 int32 int32;
	public void write_char( int32 l, int32 c, int32 ch )
	{
		if( l != 1 ) return;
		int16 cc = (int16)c;
		if( cc < 1 || cc > 4 ) return;
		if( ch < 0x20 ) {
			ch + 0x10;
		}
		else {
			ch - 0x20;
		}
		data[cc - 1] = seg_table[(int16)ch];
		is_changed = true;
	}
	//==============================================
	//[i] function_void show_point int32;
	public void show_point( int32 column )
	{
		if( column < 1 || column > 4 ) return;
		int8 c = (int8)(int16)column;
		switch( c ) {
			== 1: point | 0B0000_0001; break;
			== 2: point | 0B0000_0010; break;
			== 3: point | 0B0000_0100; break;
			== 4: point | 0B0000_1000; break;
			default: break;
		}
		is_changed = true;
	}
	//==============================================
	//[i] function_void hide_point int32;
	public void hide_point( int32 column )
	{
		if( column < 1 || column > 4 ) return;
		int8 c = (int8)(int16)column;
		switch( c ) {
			== 1: point & ~0B0000_0001; break;
			== 2: point & ~0B0000_0010; break;
			== 3: point & ~0B0000_0100; break;
			== 4: point & ~0B0000_1000; break;
			default: break;
		}
		is_changed = true;
	}
	//==============================================
	//[i] function_void clear_point;
	public void clear_point()
	{
		point = 0B0000_0000;
		is_changed = true;
	}
	//==============================================
	void reset()
	{
		current_index = 0;
		current_select = SELECT_DATA0;
	}
	//==============================================
	const uint8 SELECT_DATA0 = 0B0000_1000;
	const uint8 SELECT_DATA1 = 0B0000_0100;
	const uint8 SELECT_DATA2 = 0B0000_0010;
	const uint8 SELECT_DATA3 = 0B0000_0001;
	const uint8 NO_SELECT = 0X00;
	const uint8 NO_SEG = 0X00;
	
	
	//字形表
	[#.code uint8*?] seg_table =
	{
		//SP     !     "     #
		0x00, 0x00, 0x00, 0x00,
		// $     &     %     '
		0x00, 0x00, 0x00, 0x00,
		// (     )     *     +
		0x00, 0x00, 0x00, 0x00,
		// ,            -     .     /
		0x00, 0b0100_0000, 0x00, 0x00,
		
		0b0011_1111, 0b0000_0110, 0b0101_1011, 0b0100_1111,	//00-- 0 1 2 3
		0b0110_0110, 0b0110_1101, 0b0111_1101, 0b0000_0111,	//04-- 4 5 6 7
		0b0111_1111, 0b0110_1111, 					//08-- 8 9
		
		0b0111_0111, 0b0111_1100, //A B
		0b0011_1001, 0b0101_1110, 0b0111_1001, 0b0111_0001, //0C-- C D E F
		0b0000_0000, 0b0000_0001, 0b0100_0000, 0b0000_1000, //10-- N ^ - _
		0b0000_0001, 0b0000_0010, 0b0000_0100, 0b0000_1000, //14--
		0b0001_0000, 0b0010_0000, //CIRCLE_0 1 2 3 4 5
		0b0000_1001, 0b0001_0010, 0b0010_0100, //1A-- DCIRCLE_0 1 2
		0b0000_0011, 0b0000_0110, 0b0000_1100, 0b0001_1000, 0b0011_0000, 0b0010_0001, //1D-- BCIRCLE_0 1 2 3 4 5
		0b0000_0111, 0b0000_1110, 0b0001_1100, 0b0011_1000, 0b0011_0001, 0b0010_0011  //23-- ECIRCLE_0 1 2 3 4 5
	};
}


