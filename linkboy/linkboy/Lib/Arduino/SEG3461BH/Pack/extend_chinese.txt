<module>
注意, 此数码管为4位共阳极数码管, 对应型号为3461BH或者兼容型号, 共阳极. 4位数码管可以在指定位置上显示字符。本模块比较适合显示数字，如果显示英文字符，则不容易分辨，仅能显示有限的字符，如A，E，D等，M，W等字符显示不出来。本模块可以和“文字显示器”结合起来，可以提供更高级的功能，比如直接显示十进制数值。
</module>
<name> Pack 共阳极4位数码管,
</name>
<member> interface_char char char,

</member>,
<member> var_uint8 OS_time OS_time,
"空字符"是一个内置常量, 把这个常量赋值给哪个显示位, 哪个显示位就不显示字符.
</member>,
<member> function_void OS_init OS_init,

</member>,
<member> function_void OS_run OS_run,

</member>,
<member> function_void OS_thread OS_thread,

</member>,
<member> function_void clear 清空,
"清屏"指令把数码管屏幕全部清空, 不显示任何数字.
</member>,
<member> function_void_int32 clear_line 清空第 # 行,
把数码管屏幕指定的行清空, 不显示任何数字.
</member>,
<member> function_void_int32_int32_int32 write_char 第 # 行第 # 列显示字符 #,
在指定的行列处显示一个字符, 行列数都是从 1 开始
</member>,
<member> function_void_int32 show_point 显示第 # 个小数点,
显示数码管的某个位的小数点,位数从左端开始为1,往右依次递增.
</member>,
<member> function_void_int32 hide_point 隐藏第 # 个小数点,
隐藏数码管的某个位的小数点,位数从左端开始为1,往右依次递增.
</member>,
<member> function_void clear_point 隐藏全部小数点,
隐藏数码管的所有位的小数点
</member>,
