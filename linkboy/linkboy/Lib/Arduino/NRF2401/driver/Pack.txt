
//无线通信模块nrf2401
//芯片:	处理器无关代码

//版本:	在版本v0中固定发送4个字节数据,这个版本改成了6个数据;
//可以设置超时时间

//2013年3月12日
//把程序改成事件驱动方式, 去掉了超时时间,模式转换等;

//2015年4月23日
//加上了带有返回值的发送函数, 应用中可以判断是否发送成功, 并进行相应的处理

//2015年5月21日
//数据包数目改为了32个


//2015.11.13
// L: 20  RF_CH: 0  地址也改了

unit NRF2401
{
	public const uint16 ID = 0;
	
	public link unit CE {}
	public link unit IRQ {}
	public link unit CSN {}
	public link unit CLK {}
	public link unit MISO {}
	public link unit MOSI {}
	
	//[i] var_uint8 OS_time,
	uint8 OS_time;
	
	//[i] var_uint8 OS_EventFlag;
	uint8 OS_EventFlag;
	//[i] event receive_event;
	
	[uint8*LENGTH] Rbuffer;
	[uint8*LENGTH] Tbuffer;
	
	const uint8 LENGTH = 32;
	//const uint8 LENGTH = 20;
	
	[uint8*5] local_address;
	[uint8*5] target_address;
	
	bool isSend;
	
	//---------------------------------------------------
	//[i] function_void OS_init;
	public void OS_init()
	{
		OS_time = 1;
		OS_EventFlag = 0;
		
		isSend = false;
		
		local_address[0] = 0;//0x34;
		local_address[1] = 0x43;
		local_address[2] = 0x10;
		local_address[3] = 0x10;
		local_address[4] = 0x01;
		
		target_address[0] = 0;//0x34;
		target_address[1] = 0x43;
		target_address[2] = 0x10;
		target_address[3] = 0x10;
		target_address[4] = 0x01;
		
		target_init();
	}
	//---------------------------------------------------
	//[i] function_void set_local_address int32;
	//public void set_local_address( int32 addr )
	
	//[i] function_void set_target_address int32
	//public void set_target_address( int32 addr )
	
	//---------------------------------------------------
	//读取接收数据
	//[i] function_int32 GetData int32;
	public int32 GetData( int32 i )
	{
		int16 ii = ((int16)i - 1) * 4;
		uint32 d = Rbuffer[ii];
		d.8(uint8) = Rbuffer[ii + 1];
		d.16(uint8) = Rbuffer[ii + 2];
		d.24(uint8) = Rbuffer[ii + 3];
		return (int)d;
	}
	//---------------------------------------------------
	//设置发送数据
	//[i] function_void SetData int32 int32;
	public void SetData( int32 i, int32 d )
	{
		uint32 dd = (uint)d;
		int16 ii = ((int16)i - 1) * 4;
		Tbuffer[ii] = dd.0(uint8);
		Tbuffer[ii + 1] = dd.8(uint8);
		Tbuffer[ii + 2] = dd.16(uint8);
		Tbuffer[ii + 3] = dd.24(uint8);
	}
	
	
	
	//---------------------------------------------------
	//发送成功发返回 true, 失败返回 false
	//[i] function_void send_buffer;
	
	//---------------------------------------------------
	//[i] function_void OS_thread;
	
	
	#include "$run$.txt"
}








