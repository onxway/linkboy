
//无线通信模块NRF2401的SPI接口驱动程序
//芯片:	AVR芯片


	public void port_init()
	{
		CE.D0_DIR = 1;
		CE.D0_OUT = 0;
		
		CSN.D0_DIR = 1;
		CSN.D0_OUT = 1;
		
		IRQ.D0_DIR = 0;
		IRQ.D0_OUT = 1;
	}
	
	public unit spi
	{
		public link bit SCK_DIR;	//时钟线
		public link bit SCK_IN;		//时钟线
		public link bit SCK_OUT;	//时钟线
		
		public link bit MISO_DIR;	//读数据端口
		public link bit MISO_IN;	//读数据端口
		public link bit MISO_OUT;	//读数据端口
		
		public link bit MOSI_DIR;	//写数据端口
		public link bit MOSI_IN;	//写数据端口
		public link bit MOSI_OUT;	//写数据端口
		
		//=============================================
		//初始化
		public void init()
		{
			SCK_DIR = 1;
			SCK_OUT = 0;
			
			MOSI_DIR = 1;
			MOSI_OUT = 0;
			
			MISO_DIR = 0;
			MOSI_OUT = 1;
		}
		//=============================================
		//写一个字节
		public void write_byte( uint8 data )
		{
			loop( 8 ) {
				MOSI_OUT = data.7(bit);
				#asm "nop"
				SCK_OUT = 1;
				#asm "nop"
				SCK_OUT = 0;
				data <<= 1;
			}
		}
		//=============================================
		//读一个字节
		public uint8 read_byte()
		{
			MOSI_OUT = 0;
			uint8 data;
			loop( 8 ) {
				data <<= 1;
				SCK_OUT = 1;
				#asm "nop"
				data.0(bit) = MISO_IN;
				#asm "nop"
				SCK_OUT = 0;
			}
			return data;
		}
	}















