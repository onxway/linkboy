
//无线通信模块nrf2401
//芯片:	处理器无关代码

//版本:	在版本v0中固定发送4个字节数据,这个版本改成了6个数据;
//可以设置超时时间

//2013年3月12日 把程序改成事件驱动方式, 去掉了超时时间,模式转换等;

unit NRF2401
{
	public link unit io {}
	
	//[i] var_uint8 OS_time,
	uint8 OS_time;
	
	//[i] var_uint8 OS_EventFlag;
	uint8 OS_EventFlag;
	//[i] event receive_event;
	
	//[i] var_array buffer,
	public [uint8*LENGTH] buffer;
	
	const uint8 LENGTH = 6;
	
	[uint8*5] local_address;
	[uint8*5] target_address;
	
	bool isSend;
	
	#include "AVR_11MHZ.txt"
	
	//---------------------------------------------------
	//[i] function_void OS_init;
	public void OS_init()
	{
		port_init();
		spi.init();
		set( CONFIG, 0b0000_1111 );		//CRC使能，16位CRC校验，上电,接收模式
		set( EN_AA, 0x01 );			//使能接收通道0自动应答
		set( EN_RXADDR, 0x01 );			//使能接收通道0
		set( SETUP_AW, 0x03 );			//设置地址宽度 5个字节
		set( SETUP_RETR, 0xff );		//自动重发延时等待4000us+86us，自动重发15次
		set( RF_CH, 40 );				//选择射频通道40
		set( RF_SETUP, 0x07 );			//数据传输率1Mbps，发射功率0dBm，低噪声放大器增益
		uint8 status = get( STATUS );		//返回状态寄存器
		write_command( FLUSH_TX);
		write_command( FLUSH_RX);
		set( STATUS, status|0b0111_0000 );	//清除TX_DS或MAX_RT中断标志
		
		//补充的
		//接收通道0选择和发送通道相同有效数据宽度
		set( RX_PW_P0, LENGTH );
		
		local_address[0] = 0x00;
		local_address[1] = 0x43;
		local_address[2] = 0x10;
		local_address[3] = 0x10;
		local_address[4] = 0x01;
		
		target_address[0] = 0x00;
		target_address[1] = 0x43;
		target_address[2] = 0x10;
		target_address[3] = 0x10;
		target_address[4] = 0x01;
		
		OS_set_receive_mode();
		
		OS_time = 1;
		OS_EventFlag = 0;
		
		isSend = false;
	}
	//---------------------------------------------------
	//[i] function_void OS_run;
	public void OS_run()
	{
		if( !isSend && io.IRQ_IN == 0 ) {
			
			//从无线模块中读取数据
			read_buffer( RD_RX_PLOAD, buffer, LENGTH );
			uint8 sta = get( STATUS );
			set( STATUS, sta );
			write_command( FLUSH_RX );
			
			OS_EventFlag.0(bit) = 1;
		}
	}
	//---------------------------------------------------
	//[i] function_void send_buffer;
	public void send_buffer()
	{
		isSend = true;
		
		OS_set_send_mode();
		
		//接收通道0选择和发送通道相同有效数据宽度
		set( RX_PW_P0, LENGTH );
		
		io.CE_OUT = 0;
		//写数据包到TX FIFO
		write_buffer( WR_TX_PLOAD, buffer, LENGTH );
		//写入发送地址
		//write_buffer( WRITE_REG+TX_ADDR, target_address, 5 );
		//为了应答接收设备，接收通道0地址和发送地址相同
		//write_buffer( WRITE_REG+RX_ADDR_P0, target_address, 5 );
		io.CE_OUT = 1;
		
		//等待应答
		repeat until io.IRQ_IN == 0; {}
		uint8 status = get( STATUS );	//返回状态寄存器
		set( STATUS, status );  	//清除TX_DS或MAX_RT中断标志
		write_command( FLUSH_TX );
		
		OS_set_receive_mode();
		
		isSend = false;
	}
	//---------------------------------------------------
	//[i] function_bool OS_isOK;
	public bool OS_isOK()
	{
		set( RF_CH, 0x03 );
		if( get( RF_CH ) != 0x03 ) return false;
		set( RF_CH, 0x40 );
		if( get( RF_CH ) != 0x40 ) return false;
		return true;
	}
	//---------------------------------------------------
	//[i] function_void set_local_address uint8;
	public void set_local_address( uint8 addr )
	{
		local_address[0] = addr;
	}
	//---------------------------------------------------
	//[i] function_void set_target_address uint8
	public void set_target_address( uint8 addr )
	{
		target_address[0] = addr;
	}
	//=================================================================================
	void OS_to_normal_mode()
	{
		uint8 c = get( CONFIG );
		c | 0b0000_0010;
		set( CONFIG, c );
	}
	void OS_to_power_down_mode()
	{
		uint8 c = get( CONFIG );
		c & 0b1111_1101;
		set( CONFIG, c );
	}
	void OS_set_receive_mode()
	{
		io.CE_OUT = 0;
		
		//写入接收通道地址
		write_buffer( WRITE_REG+RX_ADDR_P0, local_address, 5 );
		uint8 c = get( CONFIG );
		c | 0b0000_0001;
		set( CONFIG, c );
		io.CE_OUT = 1;
	}
	void OS_set_send_mode()
	{
		io.CE_OUT = 0;
		uint8 c = get( CONFIG );
		c & 0b1111_1110;
		set( CONFIG, c );
		io.CE_OUT = 1;
	}
	//--------------------------------
	void write_command( uint8 cmd )
	{
		io.CSN_OUT = 0;
		spi.write_byte( cmd );
		io.CSN_OUT = 1;
	}
	//把p_buf缓存中的数据写入到nRF24L01，通常用来写入发射通道数据或接收/发送地址
	void write_buffer( uint8 reg, [uint8*?] p_buf, uint8 length )
	{
		io.CSN_OUT = 0;
		spi.write_byte( reg );
		repeat first uint8 i = 0; each i + 1; until i == length; {
			spi.write_byte( p_buf[i] );
		}
		io.CSN_OUT = 1;
	}
	//读取nRF2401某个缓冲区内容
	void read_buffer( uint8 reg, [uint8*?]p_buf, uint8 length)
	{
		io.CSN_OUT = 0;
		spi.write_byte( reg );
		repeat first uint8 i = 0; each i + 1; until i == length; {
			p_buf[i] = spi.read_byte();
		}
		io.CSN_OUT = 1;
	}
	//---------------------------------------------------
	//写寄存器
	void set( uint8 addr, uint8 data )
	{
		io.CSN_OUT = 0;
		spi.write_byte( WRITE_REG + addr );
		spi.write_byte( data );
		io.CSN_OUT = 1;
	}
	//读寄存器
	uint8 get( uint8 addr )
	{
		io.CSN_OUT = 0;
		spi.write_byte( READ_REG + addr );
		uint8 data = spi.read_byte();
		io.CSN_OUT = 1;
		return data;
	}
	//=================================================================================
	const uint8 CONFIG = 		0x00;  //'Config'
	const uint8 EN_AA = 		0x01;  //'Enable Auto Acknowledgment'
	const uint8 EN_RXADDR = 	0x02;  //'Enabled RX addresses'
	const uint8 SETUP_AW = 		0x03;  //'Setup address width'
	const uint8 SETUP_RETR = 	0x04;  //'Setup Auto. Retrans'
	const uint8 RF_CH = 		0x05;  //'RF channel'
	const uint8 RF_SETUP = 		0x06;  //'RF setup'
	const uint8 STATUS = 		0x07;  //'Status'
	const uint8 OBSERVE_TX = 	0x08;  //'Observe TX'
	const uint8 CD = 			0x09;  //'Carrier Detect'
	const uint8 RX_ADDR_P0 = 	0x0A;  //频道0接收数据地址
	const uint8 RX_ADDR_P1 = 	0x0B;  //'RX address pipe1'
	const uint8 RX_ADDR_P2 = 	0x0C;  //'RX address pipe2'
	const uint8 RX_ADDR_P3 = 	0x0D;  //'RX address pipe3'
	const uint8 RX_ADDR_P4 = 	0x0E;  //'RX address pipe4'
	const uint8 RX_ADDR_P5 = 	0x0F;  //'RX address pipe5'
	const uint8 TX_ADDR = 		0x10;  //'TX address'
	const uint8 RX_PW_P0 = 		0x11;  //接收频道0接收数据长度
	const uint8 RX_PW_P1 = 		0x12;  //'RX payload width; pipe1'
	const uint8 RX_PW_P2 = 		0x13;  //'RX payload width; pipe2'
	const uint8 RX_PW_P3 = 		0x14;  //'RX payload width; pipe3'
	const uint8 RX_PW_P4 = 		0x15;  //'RX payload width; pipe4'
	const uint8 RX_PW_P5 = 		0x16;  //'RX payload width; pipe5'
	const uint8 FIFO_STATUS = 	0x17;  //'FIFO Status Register'
	//SPI nRF24L01 commands
	const uint8 READ_REG =		0x00;  //Define read command to register
	const uint8 WRITE_REG =		0x20;  //Define write command to register
	const uint8 RD_RX_PLOAD =	0x61;  //Define RX payload register address
	const uint8 WR_TX_PLOAD =	0xA0;  //Define TX payload register address
	const uint8 FLUSH_TX =		0xE1;  //Define flush TX register command
	const uint8 FLUSH_RX =		0xE2;  //Define flush RX register command
	const uint8 REUSE_TX_PL =	0xE3;  //Define reuse TX payload register command
	const uint8 NOP =			0xFF;  //Define No Operation; might be used to read status register
	
	//SPI串行接口元件;高位在前;低位在后
	spi.SCK_DIR = io.SCK_DIR; spi.SCK_IN = io.SCK_IN; spi.SCK_OUT = io.SCK_OUT;
	spi.MISO_DIR = io.MISO_DIR; spi.MISO_IN = io.MISO_IN; spi.MISO_OUT = io.MISO_OUT;
	spi.MOSI_DIR = io.MOSI_DIR; spi.MOSI_IN = io.MOSI_IN; spi.MOSI_OUT = io.MOSI_OUT;
}








