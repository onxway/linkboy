
unit IR
{
	public const uint16 ID = 0;
	
	//[i] linkinterface_IRreceive receive;
	public link unit receive {}
	
	//[i] var_uint16 OS_EventFlag;
	public uint16 OS_EventFlag;
	//[i] event receive_event;
	//[i] event nodata_event;
	//[i] event POWER_event;
	//[i] event MENU_event;
	//[i] event TEST_event;
	//[i] event RETURN_event;
	//[i] event VOLINC_event;
	//[i] event VOLDEC_event;
	//[i] event PRE_event;
	//[i] event NEXT_event;
	//[i] event PLAY_event;
	//[i] event C_event;
	//[i] event NUMBER_event;
	
	//[i] var_int32 data;
	public int32 data;
	
	bool is_press; //按下标志,表示当前是否有键按下
	
	//------------------------------------------------------------
	//[i] var_uint16 OS_time;
	public uint8 OS_time;
	
	#include <system\common\IR\$run$.txt>
	
	//---------------------------------------------------
	//[i] function_void OS_init;
	void OS_init()
	{
		OS_EventFlag = 0;
		OS_time = 100;
		is_press = false;
		data = NONE;
		RunInit();
	}
	
	//---------------------------------------------------
	//[i] function_void OS_run;
	
	//...
	
	void SetFlag( int8 temp )
	{
		OS_EventFlag.0(bit) = 1;
		
		if( temp == POWER ) {
			OS_EventFlag.2(bit) = 1;
		}
		else if( temp == MENU ) {
			OS_EventFlag.3(bit) = 1;
		}
		else if( temp == TEST ) {
			OS_EventFlag.4(bit) = 1;
		}
		else if( temp == RETURN ) {
			OS_EventFlag.5(bit) = 1;
		}
		else if( temp == VOLINC ) {
			OS_EventFlag.6(bit) = 1;
		}
		else if( temp == VOLDEC ) {
			OS_EventFlag.7(bit) = 1;
		}
		else if( temp == PRE ) {
			OS_EventFlag.8(bit) = 1;
		}
		else if( temp == NEXT ) {
			OS_EventFlag.9(bit) = 1;
		}
		else if( temp == PLAY ) {
			OS_EventFlag.10(bit) = 1;
		}
		else if( temp == C ) {
			OS_EventFlag.11(bit) = 1;
		}
		else if( temp <= 9 ) {
			OS_EventFlag.12(bit) = 1;
		}
		else {
			//...
		}
		is_press = true;
		data = temp;
	}
	
	//[i] split split;
	
	//下列常量表示遥控器上各个按键码
	//[i] const_int32 NONE;
	public const int8 NONE = -1;
	//[i] const_int32 NUM0;
	public const int8 NUM0 = 0;//0x16
	//[i] const_int32 NUM1;
	public const int8 NUM1 = 1;//0x0C
	//[i] const_int32 NUM2;
	public const int8 NUM2 = 2;//0x18
	//[i] const_int32 NUM3;
	public const int8 NUM3 = 3;//0x5E
	//[i] const_int32 NUM4;
	public const int8 NUM4 = 4;//0x08
	//[i] const_int32 NUM5;
	public const int8 NUM5 = 5;//0x1C
	//[i] const_int32 NUM6;
	public const int8 NUM6 = 6;//0x5A
	//[i] const_int32 NUM7;
	public const int8 NUM7 = 7;//0x42
	//[i] const_int32 NUM8;
	public const int8 NUM8 = 8;//0x52
	//[i] const_int32 NUM9;
	public const int8 NUM9 = 9;//0x4A
	//[i] const_int32 POWER;
	public const int8 POWER = 10;//0x45
	///[i] const_int32 CH;
	public const int8 CH = 11;//0x46
	//[i] const_int32 MENU;
	public const int8 MENU = 12;//0x47
	//[i] const_int32 TEST;
	public const int8 TEST = 13;//0x44
	//[i] const_int32 VOLINC;
	public const int8 VOLINC = 14;//0x40
	//[i] const_int32 RETURN;
	public const int8 RETURN = 15;//0x43
	//[i] const_int32 PRE;
	public const int8 PRE = 16;//0x07
	//[i] const_int32 PLAY;
	public const int8 PLAY = 17;//0x15
	//[i] const_int32 NEXT;
	public const int8 NEXT = 18;//0x09
	//[i] const_int32 VOLDEC;
	public const int8 VOLDEC = 19;//0x19
	//[i] const_int32 C;
	public const int8 C = 20;//0x0D

	//按键码转换表,通过转换表把原始按键码转换为规范的码值,例如数字键的码值即为数字本身
	const int8 KEY_NUMBER = 21;
	[KEY_NUMBER]#.code uint8 KEY_CODE =
	{
	//数字键 0 - 9
	// 0    1    2    3    4    5    6    7    8    9
	0x16,0x0C,0x18,0x5E,0x08,0x1C,0x5A,0x42,0x52,0x4A,
	
	//其他功能键
	//10   11   12   13   14   15   16   17   18   19   20
	0x45,0x46,0x47,0x44,0x40,0x43,0x07,0x15,0x09,0x19,0x0D,
	};
}




















