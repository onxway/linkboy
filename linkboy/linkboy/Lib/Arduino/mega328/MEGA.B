//[配置信息开始],
//[组件文件] module.M,
//[语言列表] c chinese,
//[组件名称] MEGA Controller 控制器,
//[模块型号] MEGA328,
//[资源占用] TIMER0,
//[实物图片] 0.png,
//[仿真类型] 1,
//[控件属性] 文字,字体,文字颜色,背景颜色,
//[元素子项] var_uint8 OS_time OS_time OS_time,
//[元素子项] var_uint8 OS_EventFlag OS_EventFlag OS_EventFlag,
//[元素子项] event StartEvent StartEvent 初始化,
//[元素子项] event IdleEvent IdleEvent 反复执行,
//[元素子项] function_void OS_init OS_init OS_init,
//[元素子项] function_void OS_thread OS_thread OS_thread,
//[元素子项] function_void OS_run OS_run OS_run,
//[元素子项] function_void OS_ClearWatchDog OS_ClearWatchDog OS_ClearWatchDog,
//[配置信息结束],

unit MEGA
{
	#include "MEGA\$language$.txt"
	driver =
	#include "driver\MEGA.txt"
}
