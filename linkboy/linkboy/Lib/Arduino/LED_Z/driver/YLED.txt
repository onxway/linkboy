
unit LED
{
	public link unit DN {}
	
	//[i] interface_led led0;
	//[i] interface_digital digital0;
	
	unit led0
	{
		public link void Open() {}
		public link void Close() {}
	}
	led0.Open = open;
	led0.Close = close;

	unit digital0
	{
		public link void open() {}
		public link void close() {}
	}
	digital0.open = open;
	digital0.close = close;

	//[i] function_void OS_init;
	public void OS_init()
	{
		DN.D0_DIR = 1;
		close();
	}
	
	//[i] function_void open;
	public void open()
	{
		DN.D0_OUT = 0;
	}
	
	//[i] function_void close;
	public void close()
	{
		DN.D0_OUT = 1;
	}
	//[i] function_void swap;
	public void swap()
	{
		DN.D0_OUT = ~DN.D0_OUT;
	}
}


