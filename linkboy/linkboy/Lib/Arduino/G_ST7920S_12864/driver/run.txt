
	//---------------------------------------------------
	//[i] function_void OS_init;
	public void OS_init()
	{
		command.OS_init();
		Clear();
	}
	//---------------------------------------------------
	//清空
	//[i] function_void Clear;
	public void Clear()
	{
		uint8 addr = 0x80;
		loop( 32 ) {
			command.write_command( addr );
			command.write_command( 0x80 );
			loop( 32 ) {
				command.write_data( 0x00 );
			}
			addr += 1;
		}
	}
	
	//---------------------------------------------------
	//[i] function_void set_uint8 int16 int16 uint8;
	void set_uint8( int16 line, int16 area, uint8 d )
	{
		if( area < 0 || area >= AreaNumber || line < 0 || line >= ColumnNumber ) {
			return;
		}
		buffer[area*ColumnNumber + line] = d;
		
		//设置数据到液晶屏硬件
		int16 area0;
		if( area % 2 != 0 ) {
			area0 = area - 1;
		}
		else {
			area0 = area;
		}
		if( line >= 32 ) {
			command.write_command( 0x80 + (uint8)(uint)line - 32 );
			command.write_command( 0x8F - (uint8)(uint)area / 2 );
		}
		else {
			command.write_command( 0x80 + (uint8)(uint)line );
			command.write_command( 0x87 - (uint8)(uint)area / 2 );
		}
		//顺序调一下
		command.write_data( buffer[(area0+1)*ColumnNumber + line] ); //再发送低字节
		command.write_data( buffer[area0*ColumnNumber + line] ); //先发送高字节
	}
	
	//---------------------------------------------------
	//[i] function_uint8 get_uint8 int16 int16;
	uint8 get_uint8( int16 line, int16 area )
	{
		if( area < 0 ) return 0;
		if( area < 0 || area >= AreaNumber || line < 0 || line >= ColumnNumber ) return BackData;
		return buffer[area*ColumnNumber + line];
	}













