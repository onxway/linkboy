
//外部中断类
unit inter
{
	public const uint8 OS_channel_INT = 0;
	link void INT_event() {}
	
	//---------------------------------------------------
	public void init()
	{
		if( OS_channel_INT == 0 ) init_D2();
		if( OS_channel_INT == 1 ) init_D3();
		if( OS_channel_INT == 2 ) init_B2();
	}
	//---------------------------------------------------
	void init_D2()
	{
		//中断0下降沿触发中断
		#.MEGA2560.EICRA.0(bit) = 0;
		#.MEGA2560.EICRA.1(bit) = 1;
		#.MEGA2560.EIFR.0(bit) = 1; //清中断0标志
		#.MEGA2560.EIMSK.0(bit) = 1; //打开中断0允许
	}
	//---------------------------------------------------
	void init_D3()
	{
		//中断1下降沿触发中断
		#.MEGA2560.EICRA.2(bit) = 0;
		#.MEGA2560.EICRA.3(bit) = 1;
		#.MEGA2560.EIFR.1(bit) = 1; //清中断1标志
		#.MEGA2560.EIMSK.1(bit) = 1; //打开中断1允许
	}
	//---------------------------------------------------
	void init_B2()
	{
		//中断2下降沿触发中断
		#.MEGA2560.EICRA.4(bit) = 0;
		#.MEGA2560.EICRA.5(bit) = 1;
		#.MEGA2560.EIFR.2(bit) = 1; //清中断2标志
		#.MEGA2560.EIMSK.2(bit) = 1; //打开中断2允许
	}
	//---------------------------------------------------
	//中断服务函数
	interrupt[ {0x0002, 0x0004, 0x0006}[OS_channel_INT] ]
	void Interrupt()
	{
		INT_event();
	}
}









