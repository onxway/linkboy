//[配置信息开始],
//[组件文件] Module.M,
//[语言列表] c chinese,
//[组件名称] Pack Ultrasonic 超声波测距器,
//[模块型号] 不精确(纯软件测量),
//[资源占用] ,
//[实物图片] 1.png,2.png,
//[仿真类型] 1,
//[元素子项] function_void OS_init OS_init OS_init,
//[元素子项] function_int32_debug get_value GetValue 障碍物距离,
//[元素子项] function_int32_old get_good_value GetGoodValue 精确障碍物距离,
//[配置信息结束],

unit Pack
{
	#include "Pack\$language$.txt"
	driver =
	#include "driver\Pack.txt"
}
