
unit ST7565
{
	public const uint16 ID = 0;
	
	public link unit RS {}
	public link unit RW {}
	public link unit E {}
	
	link bit CS_IN = RS.D0_IN;
	link bit CS_OUT = RS.D0_OUT;
	link bit CS_DIR = RS.D0_DIR;
	
	link bit SID_IN = RW.D0_IN;
	link bit SID_OUT = RW.D0_OUT;
	link bit SID_DIR = RW.D0_DIR;
	
	link bit SCK_IN = E.D0_IN;
	link bit SCK_OUT = E.D0_OUT;
	link bit SCK_DIR = E.D0_DIR;
	
	//---------------------------------------------------
	//[i] function OS_init;
	public void OS_init()
	{
		//端口初始化
		PORT_init();
		
		write_command( 0x30 );
		write_command( 0x30 );
		write_command( 0x0c );
		write_command( 0x01 );
		write_command( 0x06 );
		
		//转换到图形模式
		//write_command( 0x34 );
		//write_command( 0x36 );
	}
	//---------------------------------------------------
	void PORT_init()
	{
		CS_DIR = 1;
		CS_OUT = 1;
		
		SID_DIR = 1;
		SID_OUT = 1;
		
		SCK_DIR = 1;
		SCK_OUT = 0;
		
		delay_1us( 200 );
	}
	
	public void refresh( [uint8*64] buffer )
	{
		write_command( 0x80 );
		uint8 i = 0;
		loop( 64 ) {
			write_data( buffer[i] );
			i += 1;
		}
	}
	
	//---------------------------------------------------
	public void write_command( uint8 C )
	{
		//---------------------------------------------------
		SID_OUT = 1;
		delay();
		loop( 5 ) {
			SCK_OUT = 1;
			delay();
			SCK_OUT = 0;
			delay();
		}
		//---------------------------------------------------
		SID_OUT = 0;
		delay();
		SCK_OUT = 1;
		delay();
		SCK_OUT = 0;
		delay();
		//---------------------------------------------------
		SID_OUT = 0;
		delay();
		SCK_OUT = 1;
		delay();
		SCK_OUT = 0;
		delay();
		//---------------------------------------------------
		SendByte( C );
	}
	//---------------------------------------------------
	public void write_data( uint8 D )
	{
		SID_OUT = 1;
		loop( 5 ) {
			SCK_OUT = 1;
			delay();
			SCK_OUT = 0;
			delay();
		}
		
		SID_OUT = 0;
		delay();
		SCK_OUT = 1;
		delay();
		SCK_OUT = 0;
		delay();
		
		SID_OUT = 1;
		delay();
		SCK_OUT = 1;
		delay();
		SCK_OUT = 0;
		delay();
		
		SendByte( D );
	}
	
	//---------------------------------------------------
	void SendByte( uint8 b )
	{
		SID_OUT = 0;
		delay();
		SCK_OUT = 1;
		delay();
		SCK_OUT = 0;
		delay();
		
		loop( 4 ) {
			SID_OUT = b.7(bit);
			b <<= 1;
			SCK_OUT = 1;
			delay();
			SCK_OUT = 0;
			delay();
		}
		
		SID_OUT = 0;
		delay();
		loop( 4 ) {
			SCK_OUT = 1;
			delay();
			SCK_OUT = 0;
			delay();
		}
		
		loop( 4 ) {
			SID_OUT = b.7(bit);
			b <<= 1;
			SCK_OUT = 1;
			delay();
			SCK_OUT = 0;
			delay();
		}
		
		SID_OUT = 0;
		delay();
		loop( 4 ) {
			SCK_OUT = 1;
			delay();
			SCK_OUT = 0;
			delay();
		}
		
		SID_OUT = 1;
	}
	
	void delay()
	{
		delay_1us( 10 );
	}
	
	//延时1uS函数
	link void delay_1us( uint8 tt ) {} = #.sys_delayer.delay_n_1us;
}











