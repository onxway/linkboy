
	//*******************************************************************
	//温度检测芯片DS18B20, ATMEGA8单片机, 11M外部晶振
	
	//2014.1.20 
	//注意这里的脉冲时间是根据编译器优化前的情况, 优化后的情况尚未经过稳定性检验
	
	//---------------------------------------------------
	//端口初始化
	void port_init()
	{
		DS_DIR = 1;
		DS_OUT = 1;
	}
	//---------------------------------------------------
	//启动温度转换
	void start_sd()
	{
		DS_OUT = 0;
		loop( 8 ) {
			loop( 204 ) {}//loop( 140 ) {}
		}
		DS_OUT = 1;
		loop( 8 ) {
			loop( 138 ) {}//loop( 95 ) {}
		}
	}
	//---------------------------------------------------
	//写一个字节
	void write_byte( uint8 data )
	{
		#asm "cli"
		loop( 8 ) {
			DS_OUT = 0;
			loop( 22 ) {}//loop( 15 ) {}
			DS_OUT = data.0(bit);
			data >> 1;
			loop( 52 ) {}//loop( 36 ) {}
			DS_OUT = 1;
		}
		#asm "sei"
	}
	//---------------------------------------------------
	//读一个字节
	uint8 read_byte()
	{
		uint8 data;
		#asm "cli"
		loop( 8 ) {
			DS_OUT = 0;
			loop( 12 ) {}//loop( 8 ) {}
			DS_OUT = 1;
			DS_DIR = 0;
			data >> 1;
			data.7(bit) = DS_IN;
			DS_DIR = 1;
			loop( 39 ) {}//loop( 27 ) {}
		}
		#asm "sei"
		return data;
	}












