

	//=================================================================================
	
	void RunInit()
	{
		S1.D0_DIR = 1;
		S2.D0_DIR = 1;
		S3.D0_DIR = 1;
		S4.D0_DIR = 1;
		S5.D0_DIR = 1;
		S6.D0_DIR = 1;
		S7.D0_DIR = 1;
		S8.D0_DIR = 1;
		
		S1.D0_OUT = 0;
		S2.D0_OUT = 0;
		S3.D0_OUT = 0;
		S4.D0_OUT = 0;
		S5.D0_OUT = 0;
		S6.D0_OUT = 0;
		S7.D0_OUT = 0;
		S8.D0_OUT = 0;
		
		A.D0_DIR = 1;
		B.D0_DIR = 1;
		C.D0_DIR = 1;
		D.D0_DIR = 1;
		E.D0_DIR = 1;
		F.D0_DIR = 1;
		G.D0_DIR = 1;
		H.D0_DIR = 1;
		
		A.D0_OUT = 1;
		B.D0_OUT = 1;
		C.D0_OUT = 1;
		D.D0_OUT = 1;
		E.D0_OUT = 1;
		F.D0_OUT = 1;
		G.D0_OUT = 1;
		H.D0_OUT = 1;
	}
	public void OS_run()
	{
		uint8 seg_data = data_show[current_index];
		
		//显示当前位的数据
		send_2_byte( seg_data, current_select );
		current_select >> 1;
		current_index + 1;
		if( current_index == 8 ) reset();
	}
	void DataChanged()
	{
		data_show[0] = data[0];
		data_show[1] = data[1];
		data_show[2] = data[2];
		data_show[3] = data[3];
		data_show[4] = data[4];
		data_show[5] = data[5];
		data_show[6] = data[6];
		data_show[7] = data[7];
	}
	void reset()
	{
		current_index = 0;
		current_select = SELECT_DATA0;
	}
	void send_2_byte( uint8 d, uint8 select )
	{
		select = ~select;
		
		A.D0_OUT = 1;
		B.D0_OUT = 1;
		C.D0_OUT = 1;
		D.D0_OUT = 1;
		E.D0_OUT = 1;
		F.D0_OUT = 1;
		G.D0_OUT = 1;
		H.D0_OUT = 1;
		
		S1.D0_OUT = d.0(bit);
		S2.D0_OUT = d.1(bit);
		S3.D0_OUT = d.2(bit);
		S4.D0_OUT = d.3(bit);
		S5.D0_OUT = d.4(bit);
		S6.D0_OUT = d.5(bit);
		S7.D0_OUT = d.6(bit);
		S8.D0_OUT = d.7(bit);
		
		A.D0_OUT = select.0(bit);
		B.D0_OUT = select.1(bit);
		C.D0_OUT = select.2(bit);
		D.D0_OUT = select.3(bit);
		E.D0_OUT = select.4(bit);
		F.D0_OUT = select.5(bit);
		G.D0_OUT = select.6(bit);
		H.D0_OUT = select.7(bit);
	}
	
	
	
	
	
	
