
unit Port
{
	public const uint16 ID = 0;
	
	public link unit PORT {}
	
	public link bit OUT_DIR = PORT.D0_DIR;
	public link bit OUT_IN = PORT.D0_IN;
	public link bit OUT_OUT = PORT.D0_OUT;

	//[i] linkconst_bit_1 IStatus;
	public const bit IStatus = 1;

	//---------------------------------------------------
	//[i] function_void OS_init;
	public void OS_init()
	{
		#include "$run$.txt"

		OUT_DIR = 1;
		OUT_OUT = IStatus;
	}
	//---------------------------------------------------
	//[i] function_void SetStatus int32;
	public void SetStatus( int32 s )
	{
		if( s == 0 ) {
			OUT_OUT = 0;
		}
		else {
			OUT_OUT = 1;
		}
	}
	//---------------------------------------------------
	//[i] function_int32 GetStatus;
	public int32 GetStatus()
	{
		if( OUT_OUT == 0 ) {
			return 0;
		}
		else {
			return 1;
		}
		//语法合规
		return 0;
	}
}




