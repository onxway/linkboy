//[配置信息开始],
//[组件文件] Module.M,
//[语言列表] c chinese,
//[组件名称] Pack PWM PWM输出器,
//[模块型号] 不占资源,
//[资源占用] ,
//[仿真类型] 1,
//[元素子项] var_int32_w Value Value 当前值,
//[元素子项] var_uint8 OS_time OS_time OS_time,
//[元素子项] function_void OS_init OS_init OS_init,
//[元素子项] function_void OS_thread OS_thread OS_thread,
//[元素子项] function_void OS_run OS_run OS_run,
//[元素子项] function_void_int32 set_value SetValue+# 设置当前值为+#,
//[配置信息结束],

unit Pack
{
	#include "Pack\$language$.txt"
	driver =
	#include "driver\Pack.txt"
}
