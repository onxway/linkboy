<module>
这是一个软件实现的PWM输出器, 分辨率是0-100, 周期是0.05秒. 也就是一秒钟可以输出20个完整的PWM波形. 每个波形里边有100刻度
</module>
<name> Pack PWM输出器,
</name>
<member> var_int32_w Value 当前值,
PWM输出器的当前值, 范围是0-100. 当设置 <当前值> 为0时, 端口输出恒定的低电平; 当设置为100时, 输出恒定的高电平; 其他值则对应不同脉冲宽度的PWM波形.
</member>,
<member> var_uint8 OS_time OS_time,

</member>,
<member> function_void OS_init OS_init,

</member>,
<member> function_void OS_thread OS_thread,

</member>,
<member> function_void OS_run OS_run,

</member>,
<member> function_void_int32 set_value 设置当前值为 #,

</member>,
