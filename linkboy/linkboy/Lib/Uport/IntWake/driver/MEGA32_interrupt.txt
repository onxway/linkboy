
//外部中断类
unit inter
{
	public const uint8 OS_channel_INT = 0;
	link unit MCU {} = #.MCU;
	link void INT_event() {}
	
	//---------------------------------------------------
	public void init()
	{
		if( OS_channel_INT == 0 ) init_D2();
		if( OS_channel_INT == 1 ) init_D3();
		if( OS_channel_INT == 2 ) init_B2();
	}
	//---------------------------------------------------
	void init_D2()
	{
		//MCU.DDRD.2(bit) = 0;
		//MCU.PORTD.2(bit) = 1;
		//MCU.MCUCR.0(bit) = 0; MCU.MCUCR.1(bit) = 1;//MCU.MCUCR = 0b0000_0010; //中断0下降沿触发中断
		MCU.MCUCR.0(bit) = 1; MCU.MCUCR.1(bit) = 1;//MCU.MCUCR = 0b0000_0010; //中断0上升沿触发中断
		MCU.GIFR.6(bit) = 1;//MCU.GIFR = 0b0100_0000; //清中断0标志
		MCU.GICR.6(bit) = 1;//MCU.GICR = 0b0100_0000; //打开中断0允许
	}
	//---------------------------------------------------
	void init_D3()
	{
		//MCU.DDRD.3(bit) = 0;
		//MCU.PORTD.3(bit) = 1;
		//MCU.MCUCR.2(bit) = 0; MCU.MCUCR.3(bit) = 1;//MCU.MCUCR = 0b0000_1000; //中断1下降沿触发中断
		MCU.MCUCR.2(bit) = 1; MCU.MCUCR.3(bit) = 1;//MCU.MCUCR = 0b0000_1000; //中断1上升沿触发中断
		MCU.GIFR.7(bit) = 1;//MCU.GIFR = 0b1000_0000; //清中断1标志
		MCU.GICR.7(bit) = 1;//MCU.GICR = 0b1000_0000; //打开中断1允许
	}
	//---------------------------------------------------
	void init_B2()
	{
		//MCU.DDRB.3(bit) = 0;
		//MCU.PORTB.3(bit) = 1;
		//MCU.MCUCSR.6(bit) = 0;//MCU.MCUCSR = 0b0000_0000; //中断2下降沿触发中断
		MCU.MCUCSR.6(bit) = 1;//MCU.MCUCSR = 0b0000_0000; //中断2上升沿触发中断
		MCU.GIFR.5(bit) = 1;//MCU.GIFR = 0b0010_0000; //清中断2标志
		MCU.GICR.5(bit) = 1;//MCU.GICR = 0b0010_0000; //打开中断2允许
	}
	//---------------------------------------------------
	//中断服务函数
	interrupt[ {0x0002, 0x0004, 0x0006}[OS_channel_INT] ]
	void Interrupt()
	{
		INT_event();
	}
}









