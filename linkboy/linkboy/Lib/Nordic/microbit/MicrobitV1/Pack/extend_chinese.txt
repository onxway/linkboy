<module>
BBC出品的开发板，芯片型号为NRF51系列，当主板刚开始通电时触发 "初始化" 事件, 之后反复触发 "反复执行" 事件.
</module>
<name> Pack 控制器,
</name>
<member> var_uint8 OS_EventFlag OS_EventFlag,

</member>,
<member> event StartEvent 初始化,

</member>,
<member> event IdleEvent 反复执行,

</member>,
<member> function_void OS_init OS_init,

</member>,
<member> function_void OS_thread OS_thread,

</member>,
