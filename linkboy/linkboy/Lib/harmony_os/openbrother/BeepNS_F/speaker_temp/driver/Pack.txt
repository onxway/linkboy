
unit speaker
{
	public const uint16 ID = 0;
	
	#include "$run$.txt"
	
	public link unit IO {} = #.COM_IO.A5;
	//这个方法报错
	//IO = #.COM_IO.A16;
	//这个可以
	//IO = #.控制器.A16;

	public link bit SOUND_DIR = IO.D0_DIR;
	public link bit SOUND_IN = IO.D0_IN;
	public link bit SOUND_OUT = IO.D0_OUT;
	
	//[i] interface_analog analog;
	unit analog
	{
		public link void SetValue( int32 v ) {}
		public link void Stop() {}
	}
	analog.SetValue = play_tone;
	analog.Stop = stop;
	
	//[i] interface_tone tone1;
	public unit tone1
	{
		public link void play( int8 channal, int8 tone, int8 vol ) {}
		public link void stop( int8 channal, int8 s ) {}
		public link uint8 switch_time( uint8 t ) {}
		public link bool isBusy;
		public link bool isNeed;
	}
	tone1.play = _play;
	tone1.stop = _stop;
	tone1.switch_time = _switch_time;
	tone1.isBusy = NullFalse;
	tone1.isNeed = NullFalse;
	bool NullFalse;
	
	void _play( int8 channal, int8 tone, int8 vol )
	{
		if( channal == 1 ) {
			play_tone( tone );
		}
	}
	void _stop( int8 channal, int8 tone )
	{
		if( channal == 1 ) {
			stop();
		}
	}
	uint8 _switch_time( uint8 t )
	{
		return t;
	}
	
	//----------------------------------------------
	//[i] function_void OS_init;
	public void OS_init()
	{
		timer_init();
		SOUND_DIR = 1;
		SOUND_OUT = 0;
		NullFalse = false;
	}
	//----------------------------------------------
	//[i] function_void play_tone int32;
	//public void play_tone( int32 t )
	
	//----------------------------------------------
	//[i] function_void stop;
	//public void stop()
}



