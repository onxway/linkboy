
unit SEG7X4A
{
	public const uint16 ID = 0;
	
	public link unit DIN {}
	public link unit CLK {}
	public link unit CS {}
	
	link bit DATA_IN = DIN.D0_IN;
	link bit DATA_OUT = DIN.D0_OUT;
	link bit DATA_DIR = DIN.D0_DIR;
	
	link bit LOCK_IN = CS.D0_IN;
	link bit LOCK_OUT = CS.D0_OUT;
	link bit LOCK_DIR = CS.D0_DIR;
	
	link bit SHIFT_IN = CLK.D0_IN;
	link bit SHIFT_OUT = CLK.D0_OUT;
	link bit SHIFT_DIR = CLK.D0_DIR;
	
	//[i] interface_map map;
	public unit map
	{
		link int8 AreaNumber;
		link int16 LineNumber;
		link uint8 BackData;
		link uint8 ClearData;
		link void Clear() {}
		link void set_pos( int16 Line, int16 area ) {}
		link void set_uint8( uint8 d ) {}
		link uint16 get_uint8() {}
		public const int8 TYPE = 1;
	}
	map.AreaNumber = AreaNumber;
	map.LineNumber = LineNumber;
	map.BackData = BackData;
	map.ClearData = ClearData;
	map.set_pos = set_pos;
	map.set_uint8 = set_uint8;
	map.get_uint8 = get_uint8;
	map.Clear = clear;
	
	//四个显示单元和小数点位置
	[uint8*8] data;
	bool is_changed;
	
	const uint8 SELECT_DATA0 = 0X80;
	const uint8 NO_SELECT = 0X00;
	const uint8 NO_SEG = 0X00;
	
	//---------------------------------------------------
	//[i] function_void OS_init;
	public void OS_init()
	{
		MAX7219.init();
		
		is_changed = false;
		
		BackData = 0x00;
		ClearData = 0x00;
		
		clear();
	}
	//---------------------------------------------------
	//[i] function_void OS_thread;
	public void OS_thread()
	{
		if( is_changed ) {
			is_changed = false;
			MAX7219.send( data );
		}
	}
	//==============================================
	//[i] function_void set_light int32;
	public void set_light( int32 light )
	{
		MAX7219.set_light( (uint8)(uint16)(uint)light );
	}
	//---------------------------------------------------
	//[i] function_void clear;
	public void clear()
	{
		data[0] = NO_SEG;
		data[1] = NO_SEG;
		data[2] = NO_SEG;
		data[3] = NO_SEG;
		data[4] = NO_SEG;
		data[5] = NO_SEG;
		data[6] = NO_SEG;
		data[7] = NO_SEG;
		
		is_changed = true;
	}
	//---------------------------------------------------
	//[i] function_void open int32 int32;
	public void open( int32 l, int32 c )
	{
		int8 ll = 8 - (int8)(int16)l;
		int8 cc = (int8)(int16)c - 1;
		if( ll < 0 || ll > 7 || cc < 0 || cc > 7 ) return;
		data[cc] = data[cc] | (0x80 >> (uint)ll);
		is_changed = true;
	}
	//---------------------------------------------------
	//[i] function_void close int32 int32;
	public void close( int32 l, int32 c )
	{
		int8 ll = 8 - (int8)(int16)l;
		int8 cc = (int8)(int16)c - 1;
		if( ll < 0 || ll > 7 || cc < 0 || cc > 7 ) return;
		data[cc] = data[cc] & ~(0x80 >> (uint)ll);
		is_changed = true;
	}
	//---------------------------------------------------
	//[i] function_void swap int32 int32;
	public void swap( int32 l, int32 c )
	{
		int8 ll = 8 - (int8)(int16)l;
		int8 cc = (int8)(int16)c - 1;
		if( ll < 0 || ll > 7 || cc < 0 || cc > 7 ) return;
		data[cc] = data[cc] ^ (0x80 >> (uint)ll);
		is_changed = true;
	}
	//---------------------------------------------------
	//[i] function_bool is_open int32 int32;
	public bool is_open( int32 l, int32 c )
	{
		int8 ll = 8 - (int8)(int16)l;
		int8 cc = (int8)(int16)c - 1;
		if( ll < 0 || ll > 7 || cc < 0 || cc > 7 ) return false;
		return data[cc] & (0x80 >> (uint)ll) != 0;
	}
	
	
	
	//[i] var_int8 AreaNumber;
	public const int8 AreaNumber = 1;
	//[i] var_int16 LineNumber;
	public const int16 LineNumber = 8;
	//[i] var_uint8 BackData;
	public uint8 BackData;
	//[i] var_uint8 ClearData;
	public uint8 ClearData;
	
	//---------------------------------------------------
	//[i] function_void set_uint8 uint8;
	void set_uint8( int16 line, int16 area, uint8 d )
	{
		if( area >= 0 && area < AreaNumber && line >= 0 && line < LineNumber ) {
			data[area*LineNumber + line] = d;
			is_changed = true;
		}
		line += 1;
	}
	//---------------------------------------------------
	//[i] function_uint8 get_uint8;
	uint8 get_uint8( int16 line, int16 area )
	{
		if( area < 0 || area >= AreaNumber || line < 0 || line >= LineNumber ) return BackData;
		return data[area*LineNumber + line];
	}
	
	#include "$run$.txt"
}


