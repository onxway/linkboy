<module>
红外遥控器, 当用户按键时就会触发 <按键按下事件>, 用户可以在事件处理代码中读取按键值进行处理, 比如控制小车前进后退等. 当按键松开时就会触发 <按键松开事件>
</module>
<name> IRpad 遥控器,
</name>
<member> linkinterface_IRreceive receive receive,

</member>,
<member> var_uint16 OS_EventFlag OS_EventFlag,

</member>,
<member> event receive_event 任意键按下时,

</member>,
<member> event nodata_event 任意键松开时,

</member>,
<member> event NUMBER_event 数字键按下时,

</member>,
<member> event CH_event 频道键按下时,

</member>,
<member> event CH0_event 频道减按下时,

</member>,
<member> event CH1_event 频道加按下时,

</member>,
<member> event PREV_event 前一个按下时,

</member>,
<member> event NEXT_event 后一个按下时,

</member>,
<member> event PLAY_event 播放键按下时,

</member>,
<member> event EQ_event EQ键按下时,

</member>,
<member> event VOL0_event 音量减按下时,

</member>,
<member> event VOL1_event 音量加按下时,

</member>,
<member> event A100_event A100键按下时,

</member>,
<member> event A200_event A200键按下时,

</member>,
<member> var_int32 data 按键值,
这是一个变量, 保存当前正在按下的按键值, 一般在 <按键按下事件> 中根据这个数值进行相应的操作, 例如按键值等于数字0,小车前进,按键值等于数字1,小车后退,... 下边这些是遥控器上的按键常量, 可以和 <按键值> 进行比较, 哪个键按下, 那么 <按键值> 就等于那个键的常量值. 为了方便使用, 遥控器的数字键0-9 的实际编码也依次是 0 - 9, 即 <数字0>按键常量的数值是0, <按键1>按键常量的数值是1, ... 依次类推.
</member>,
<member> var_uint16 OS_time OS_time,

</member>,
<member> function_void OS_init OS_init,

</member>,
<member> function_void OS_run OS_run,

</member>,
<member> split split split,

</member>,
<member> const_int32 NONE 无按键按下,
:按键值常量, 代表键盘上无按键按下, 数值为-1
</member>,
<member> const_int32 NUM0 数字0,
:按键值常量, 代表键盘上的数字0键, 数值为0
</member>,
<member> const_int32 NUM1 数字1,
:按键值常量, 代表键盘上的数字1键, 数值为1
</member>,
<member> const_int32 NUM2 数字2,
:按键值常量, 代表键盘上的数字2键, 数值为2
</member>,
<member> const_int32 NUM3 数字3,
:按键值常量, 代表键盘上的数字3键, 数值为3
</member>,
<member> const_int32 NUM4 数字4,
:按键值常量, 代表键盘上的数字4键, 数值为4
</member>,
<member> const_int32 NUM5 数字5,
:按键值常量, 代表键盘上的数字5键, 数值为5
</member>,
<member> const_int32 NUM6 数字6,
:按键值常量, 代表键盘上的数字6键, 数值为6
</member>,
<member> const_int32 NUM7 数字7,
:按键值常量, 代表键盘上的数字7键, 数值为7
</member>,
<member> const_int32 NUM8 数字8,
:按键值常量, 代表键盘上的数字8键, 数值为8
</member>,
<member> const_int32 NUM9 数字9,
:按键值常量, 代表键盘上的数字9键, 数值为9
</member>,
<member> const_int32 CH0 频道减,
:按键值常量, 代表键盘上的频道减键, 数值为10
</member>,
<member> const_int32 CH 频道键,
:按键值常量, 代表键盘上的频道键, 数值为11
</member>,
<member> const_int32 CH1 频道加,
:按键值常量, 代表键盘上的频道加键, 数值为12
</member>,
<member> const_int32 PREV 前一个,
:按键值常量, 代表键盘上的上一个键, 数值为13
</member>,
<member> const_int32 NEXT 后一个,
:按键值常量, 代表键盘上的下一个键, 数值为14
</member>,
<member> const_int32 PLAY 播放键,
:按键值常量, 代表键盘上的播放键, 数值为15
</member>,
<member> const_int32 VOL0 音量减,
:按键值常量, 代表键盘上的音量减键, 数值为16
</member>,
<member> const_int32 VOL1 音量加,
:按键值常量, 代表键盘上的音量加键, 数值为17
</member>,
<member> const_int32 EQ EQ,
:按键值常量, 代表键盘上的EQ键, 数值为18
</member>,
<member> const_int32 A100 A100,
:按键值常量, 代表键盘上的A100键, 数值为19
</member>,
<member> const_int32 A200 A200,
:按键值常量, 代表键盘上的A200键, 数值为20
</member>,
