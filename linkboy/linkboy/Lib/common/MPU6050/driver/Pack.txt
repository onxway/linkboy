
unit BH1750FVI
{
	public const uint16 ID = 0;

	public link unit SDA {}
	public link unit SCL {}
	
	
	//[i] var_uint8 OS_time;
	public uint8 OS_time;
	
	//[i] var_int32 aX;
	public int32 aX;
	//[i] var_int32 aY;
	public int32 aY;
	
	//[i] var_int32 accelX;
	public int32 accelX;
	//[i] var_int32 accelY;
	public int32 accelY;
	//[i] var_int32 accelZ;
	public int32 accelZ;
	
	//[i] var_int32 gyroX;
	public int32 gyroX;
	//[i] var_int32 gyroY;
	public int32 gyroY;
	//[i] var_int32 gyroZ;
	public int32 gyroZ;
	
	Coor =
	#include <software\Coor\driver\Pack.txt>
	
	#include "$run$.txt"
	
	//--------------------------------------------
	//[i] function_void OS_init;
	
	//[i] function_void OS_run;
}







