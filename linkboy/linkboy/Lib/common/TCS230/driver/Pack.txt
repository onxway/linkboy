
unit Pack
{
	public const uint16 ID = 0;

	public link unit OUT {}
	public link unit OE {}
	public link unit S0 {}
	public link unit S1 {}
	public link unit S2 {}
	public link unit S3 {}
	
	//[i] var_uint8 OS_time;
	uint8 OS_time;
	
	//输出原始值
	
	//[i] var_int32 Rvalue;
	int32 Rvalue;
	
	//[i] var_int32 Gvalue;
	int32 Gvalue;
	
	//[i] var_int32 Bvalue;
	int32 Bvalue;
	
	//颜色分量
	
	//[i] var_int32 RCvalue;
	int32 RCvalue;
	
	//[i] var_int32 GCvalue;
	int32 GCvalue;
	
	//[i] var_int32 BCvalue;
	int32 BCvalue;
	
	//白平衡校正参数
	int16 R_balanc;
	int16 G_balanc;
	int16 B_balanc;
	
	uint8 type;
	
	#include "$run$.txt"
	
	//---------------------------------------------------
	//初始化
	//[i] function_void OS_init;
	public void OS_init()
	{
		OUT.D0_DIR = 0;
		OUT.D0_OUT = 1;
		
		OE.D0_DIR = 1;
		OE.D0_OUT = 0;
		
		S0.D0_DIR = 1;
		S0.D0_OUT = 1;
		
		S1.D0_DIR = 1;
		S1.D0_OUT = 1;
		
		S2.D0_DIR = 1;
		S2.D0_OUT = 0;
		
		S3.D0_DIR = 1;
		S3.D0_OUT = 0;
		
		OS_time = 10;
		type = 0;
		
		R_balanc = 500;
		G_balanc = 400;
		B_balanc = 500;
		
		TimerInit();
	}
	
	//---------------------------------------------------
	//[i] function_void OS_run;
	//public void OS_run()
	
	//---------------------------------------------------
	//[i] function_void set_white0;
	public void set_white0()
	{
		R_balanc = (int16)Rvalue;
		G_balanc = (int16)Gvalue;
		B_balanc = (int16)Bvalue;
	}
	
	//---------------------------------------------------
	//[i] function_void set_white int32 int32 int32;
	public void set_white( int32 r, int32 g, int32 b )
	{
		R_balanc = (int16)r;
		G_balanc = (int16)g;
		B_balanc = (int16)b;
	}
	
	//根据各个颜色输出值计算出各个颜色分量(0-255)
	void cal_color()
	{
		RCvalue = Rvalue * 255 / R_balanc;
		if( RCvalue > 255 ) {
			RCvalue = 255;
		}
		GCvalue = Gvalue * 255 / G_balanc;
		if( GCvalue > 255 ) {
			GCvalue = 255;
		}
		BCvalue = Bvalue * 255 / B_balanc;
		if( BCvalue > 255 ) {
			BCvalue = 255;
		}
	}
}









