//[配置信息开始],
//[组件文件] Module.M,
//[语言列表] c chinese,
//[组件名称] Pack ColorSensor 颜色传感器,
//[模块型号] TCS230,
//[资源占用] TIMER1,
//[实物图片] 1.png,2.png,
//[仿真类型] 1,
//[元素子项] var_uint8 OS_time OS_time OS_time,
//[元素子项] var_int32 Rvalue RedValue 红光亮度,
//[元素子项] var_int32 Gvalue GreenValue 绿光亮度,
//[元素子项] var_int32 Bvalue BlueValue 蓝光亮度,
//[元素子项] var_int32 RCvalue R 红色分量,
//[元素子项] var_int32 GCvalue G 绿色分量,
//[元素子项] var_int32 BCvalue B 蓝色分量,
//[元素子项] function_void OS_init OS_init OS_init,
//[元素子项] function_void OS_run OS_run OS_run,
//[元素子项] function_void set_white0 SetWhiteByCurrent 设置当前光为白光,
//[元素子项] function_void_int32_int32_int32 set_white SetWhiteByRGB+#+#+# 设置红+#+，绿+#+，蓝+#+为白光,
//[配置信息结束],

unit Pack
{
	#include "Pack\$language$.txt"
	driver =
	#include "driver\Pack.txt"
}
