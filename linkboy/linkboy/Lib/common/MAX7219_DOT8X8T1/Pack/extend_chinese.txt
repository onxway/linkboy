<module>
模块的驱动芯片型号为MAX7219, 可以通过指令设置指定行列处的小灯是否点亮, 行列数从1开始, 有效范围是1 - 8.
</module>
<name> Pack 屏幕,
</name>
<member> interface_map map map,

</member>,
<member> function_void OS_init OS_init,

</member>,
<member> function_void OS_thread OS_thread,

</member>,
<member> function_void_int32 set_light 设置屏幕亮度为 #,
注意屏幕亮度的有效范围是0-15, 0表示很暗, 15表示最亮, 其他值分别表示对应的亮度. 初始化默认是15(最亮).
</member>,
<member> function_void clear 清空,
"清屏"指令把屏幕全部清空, 不显示任何图案.
</member>,
<member> function_void_int32_int32 open 点亮第 # 行第 # 列,
点亮点阵的指定行列的LED, 行列数从1开始, 有效范围是1 - 8.
</member>,
<member> function_void_int32_int32 close 熄灭第 # 行第 # 列,
熄灭点阵的指定行列的LED, 行列数从1开始, 有效范围是1 - 8.
</member>,
<member> function_void_int32_int32 swap 反转第 # 行第 # 列,
反转点阵的指定行列的LED, 行列数从1开始, 有效范围是1 - 8.
</member>,
<member> function_bool_int32_int32 is_open 第 # 行第 # 列为点亮状态,
判断指定行列的LED是否为点亮状态, 行列数从1开始, 有效范围是1 - 8.
</member>,
<member> var_int8 AreaNumber AreaNumber,

</member>,
<member> var_int16 LineNumber LineNumber,

</member>,
<member> var_uint8 BackData 背景数据,

</member>,
<member> var_uint8 ClearData 清屏数据,

</member>,
<member> function_void_int16_int16 set_pos 设置当前列为 # 区域为 #,

</member>,
<member> function_void_uint8 set_uint8 添加数据 #,

</member>,
<member> function_uint8 get_uint8 get_uint8,

</member>,
