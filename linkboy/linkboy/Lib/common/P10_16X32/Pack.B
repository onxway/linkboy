//[配置信息开始],
//[组件文件] Module.M,
//[语言列表] c chinese,
//[组件名称] Pack Screen 点阵LED,
//[模块型号] P10(1R)-V706,
//[资源占用] ,
//[实物图片] 1.png,
//[仿真类型] 1,
//[控件属性] 文字,字体,文字颜色,背景颜色,
//[元素子项] interface_map map map map,
//[元素子项] var_uint8 OS_time OS_time OS_time,
//[元素子项] function_void OS_init OS_init OS_init,
//[元素子项] function_void OS_run OS_run OS_run,
//[元素子项] function_void OS_thread OS_thread OS_thread,
//[元素子项] function_void clear Clear 清空,
//[元素子项] function_void_int32_int32 open Open+#+# 点亮第+#+行第+#+列,
//[元素子项] function_void_int32_int32 close Close+#+# 熄灭第+#+行第+#+列,
//[元素子项] function_void_int32_int32 swap Swap+#+# 反转第+#+行第+#+列,
//[元素子项] function_bool_int32_int32 is_open IsOpen+#+# 第+#+行第+#+列为点亮状态,
//[元素子项] var_int8 AreaNumber AreaNumber AreaNumber,
//[元素子项] var_int16 LineNumber LineNumber LineNumber,
//[元素子项] var_uint8 BackData BackData 背景数据,
//[元素子项] var_uint8 ClearData ClearData 清屏数据,
//[元素子项] function_void_int16_int16 set_pos SetPos+#+# 设置当前列为+#+区域为+#,
//[元素子项] function_void_uint8 set_uint8 set_uint8+# 添加数据+#,
//[元素子项] function_uint8 get_uint8 get_uint8 get_uint8,
//[配置信息结束],

unit Pack
{
	#include "Pack\$language$.txt"
	driver =
	#include "driver\Pack.txt"
}
