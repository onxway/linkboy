<module>
"电子表"组件会自动计时, 含有 年, 月, 日, 星期, 时, 分, 秒 等属性, 用户读取这些属性显示到屏幕上就可以构成一个实用的电子表. 也可以对这些属性进行写入(即设置时间).
</module>
<name> Pack 时钟,
</name>
<member> interface_clock clock0 clock0,

</member>,
<member> var_int32_w year 年,
 "年"属性表示电子表当前的年数, 这个属性会自动在内部每年加 1, 年份的范围是 2000年 - 2099年
</member>,
<member> var_int32_w month 月,
 "月"属性表示电子表当前的月数, 这个属性会自动在内部每个月加 1, 超过 12 后从 1 开始继续累加.
</member>,
<member> var_int32_w day 日,
 "日"属性表示电子表当前的天数, 这个属性会自动在内部每天加 1, 超过 28/29/30/31 天后从 1 开始继续累加.
</member>,
<member> var_int32_w hour 时,
 "时"属性表示电子表当前的小时数, 这个属性会自动在内部每小时加 1, 超过 23 后从 0 开始继续累加.
</member>,
<member> var_int32_w minute 分,
 "分"属性表示电子表当前的分, 这个属性会自动在内部每分钟加 1, 超过 59 后从 0 开始继续累加.
</member>,
<member> var_int32_w second 秒,
 "秒"属性表示电子表当前的秒数, 这个属性会自动在内部每秒钟加 1, 超过 59 后从 0 开始继续累加.
</member>,
<member> var_int32_w week 星期,
 "星期"属性表示电子表当前的星期数, 这个属性会自动在内部每星期加 1, 超过 7 则从 1 开始继续累加. 注意这个属性的取值范围是 1,2,3,4,5,6,7. 不包括0
</member>,
<member> var_uint8 OS_EventFlag OS_EventFlag,

</member>,
<member> event time_changed 时间改变时,
当时间改变时会触发此事件, 因为时间是每秒钟改变一次,所以这个事件是每秒钟触发一次
</member>,
<member> var_uint8 OS_time OS_time,

</member>,
<member> function_void OS_init OS_init,

</member>,
<member> function_void OS_run OS_run,

</member>,
<member> function_void_time set_time 设置时间为 #,

</member>,
<member> function_bool_Time time_equal 当前时间等于 #,

</member>,
