//[配置信息开始],
//[组件文件] Module.M,
//[语言列表] c chinese,
//[组件名称] Pack DoubleKey 循迹模块,
//[模块型号] ,
//[资源占用] ,
//[仿真类型] 1,
//[控件属性] 文字,字体,文字颜色,背景颜色,
//[元素子项] var_uint8 OS_EventFlag OS_EventFlag OS_EventFlag,
//[元素子项] event key0_press_event BlueKeyPressEvent 右边传感器有反光时,
//[元素子项] event key0_up_event BlueKeyUpEvent 右边传感器无反光时,
//[元素子项] event key1_press_event GreenKeyPressEvent 中间传感器有反光时,
//[元素子项] event key1_up_event GreenKeyUpEvent 中间传感器无反光时,
//[元素子项] event key2_press_event YellowKeyPressEvent 左边传感器有反光时,
//[元素子项] event key2_up_event YellowKeyUpEvent 左边传感器无反光时,
//[元素子项] var_bool key0_press BlueKeyPress 右边传感器有反光,
//[元素子项] var_bool key1_press GreenKeyPress 中间传感器有反光,
//[元素子项] var_bool key2_press YellowKeyPress 左边传感器有反光,
//[元素子项] var_bool key0_notpress key0_notpress 右边传感器无反光,
//[元素子项] var_bool key1_notpress key1_notpress 中间传感器无反光,
//[元素子项] var_bool key2_notpress key2_notpress 左边传感器无反光,
//[元素子项] var_bool OS_time OS_time OS_time,
//[元素子项] function_void OS_init OS_init OS_init,
//[元素子项] function_void OS_run OS_run OS_run,
//[配置信息结束],

unit Pack
{
	#include "Pack\$language$.txt"
	driver =
	#include "driver\Pack.txt"
}
