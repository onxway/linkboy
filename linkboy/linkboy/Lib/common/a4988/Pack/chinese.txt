public link bit OS_time = driver.OS_time;
public link bit 当前位置 = driver.angle;
public const int32 转速 = 0;
driver.speed = 转速;
public link void OS_init(){} = driver.OS_init;
public link void OS_thread(){} = driver.OS_thread;
public link void OS_run100us(){} = driver.OS_run100us;
public link void 设置转速为_(bit n1){} = driver.set_speed;
public link void 正转(){} = driver.run_right;
public link void 反转(){} = driver.run_left;
public link void 正转_步(bit n1){} = driver.run_right_t;
public link void 反转_步(bit n1){} = driver.run_left_t;
public link void 转到目标位置_(bit n1){} = driver.run_to;
public link void 停止(){} = driver.stop;
public link void 刹停(){} = driver.short_stop;
public link void 处于旋转状态(){} = driver.Running;
public link void 处于静止状态(){} = driver.Stoped;
public link void 设为锁定模式(){} = driver.power_on;
public link void 设为空闲模式(){} = driver.power_off;
public link void 开启加减速(){} = driver.accel_on;
public link void 关闭加减速(){} = driver.accel_off;
