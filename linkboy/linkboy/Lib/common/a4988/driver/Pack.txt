
unit motor
{
	public const uint16 ID = 0;
	
	public link unit STEP {}
	public link unit DIR {}
	public link unit ENABLE {}
	
	//[i] var_uint8 OS_time;
	uint8 OS_time;
	
	//步进电机当前角度
	//[i] var_int32_w angle;
	int32 angle;
	
	//[i] linkconst_int32_200_步/每秒 speed;
	public const int32 speed = 0;
	int16 myspeed;
	int16 realspeed;

	int16 offset;
	
	//方向  0:停止  1:正转  2:反转
	uint8 tDIR;
	
	int32 time;
	
	//是否加速启动
	bool upspeed;
	//是否降速停止
	bool downspeed;
	
	bool accel;
	bool runforever;
	
	//----------------------------------------------------
	//[i] function_void OS_init;
	public void OS_init()
	{
		STEP.D0_DIR = 1;
		STEP.D0_OUT = 0;
		DIR.D0_DIR = 1;
		DIR.D0_OUT = 0;
		ENABLE.D0_DIR = 1;
		ENABLE.D0_OUT = 0;
		
		tDIR = 0;
		offset = 0;
		realspeed = 0;
		time = 0;
		runforever = false;
		upspeed = false;
		downspeed = false;
		myspeed = (int16)speed;
		
		accel = true;
		
		DeviceInit();
	}
	//----------------------------------------------------
	//[i] function_void OS_thread;
	public void OS_thread()
	{
		
	}
	
	//----------------------------------------------------
	//[i] function_void OS_run100us;
	#include "$run$.txt"
	
	//----------------------------------------------------
	//[i] function_void set_speed int32;
	public void set_speed( int32 s )
	{
		#asm "cli"
		myspeed = (int16)s;
		//realspeed = myspeed;
		#asm "sei"
	}
	
	//----------------------------------------------------
	//[i] function_void run_right;
	public void run_right()
	{
		#asm "cli"
		if( realspeed != 0 && tDIR == 2 ) {
			realspeed = 0;
		}
		tDIR = 1;
		time = 0;
		//DIR.D0_OUT = 0;
		runforever = true;
		upspeed = true;
		downspeed = false;
		#asm "sei"
	}
	//[i] function_void run_left;
	public void run_left()
	{
		#asm "cli"
		if( realspeed != 0 && tDIR == 1 ) {
			realspeed = 0;
		}
		tDIR = 2;
		time = 0;
		//DIR.D0_OUT = 1;
		runforever = true;
		upspeed = true;
		downspeed = false;
		#asm "sei"
	}
	//[i] function_void run_right_t int32;
	public void run_right_t( int32 t )
	{
		#asm "cli"
		if( realspeed != 0 && tDIR == 2 ) {
			realspeed = 0;
		}
		time = t;
		tDIR = 1;
		//DIR.D0_OUT = 0;
		runforever = false;
		upspeed = true;
		downspeed = false;
		#asm "sei"
	}
	//[i] function_void run_left_t int32;
	public void run_left_t( int32 t )
	{
		#asm "cli"
		if( realspeed != 0 && tDIR == 1 ) {
			realspeed = 0;
		}
		time = t;
		tDIR = 2;
		//DIR.D0_OUT = 1;
		runforever = false;
		upspeed = true;
		downspeed = false;
		#asm "sei"
	}
	//[i] function_void run_to int32;
	public void run_to( int32 t )
	{
		#asm "cli"
		int32 t_angle = angle;
		#asm "sei"
		
		if( t == t_angle ) {
			//#asm "sei" //BEA方案不可用这个形式
			return;
		}
		if( t > t_angle ) {
			run_right_t( t - angle );
		}
		else {
			run_left_t( angle - t );
		}
	}
	//[i] function_void stop;
	public void stop()
	{
		#asm "cli"
		if( !accel ) {
			tDIR = 0;
			runforever = false;
			downspeed = false;
			upspeed = false;
			time = 0;
			realspeed = 0;
		}
		else {
			runforever = false;
			downspeed = true;
			upspeed = false;
			time = realspeed;
			time = time * realspeed / 20000;
		}
		#asm "sei"
	}
	//[i] function_void short_stop;
	public void short_stop()
	{
		#asm "cli"
		tDIR = 0;
		runforever = false;
		downspeed = false;
		upspeed = false;
		time = 0;
		realspeed = 0;
		#asm "sei"
	}
	
	//[i] function_bool Running;
	public bool Running()
	{
		return tDIR != 0;
	}
	//[i] function_bool Stoped;
	public bool Stoped()
	{
		return tDIR == 0;
	}
	
	//=================================================================================
	//[i] function_void power_on;
	public void power_on()
	{
		ENABLE.D0_OUT = 0;
	}
	//[i] function_void power_off;
	public void power_off()
	{
		ENABLE.D0_OUT = 1;
	}
	
	//[i] function_void accel_on;
	public void accel_on()
	{
		accel = true;
	}
	//[i] function_void accel_off;
	public void accel_off()
	{
		accel = false;
	}
}







