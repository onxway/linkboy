
unit IR
{
	public const uint16 ID = 0;
	
	public link unit CLK {}
	public link unit DATA {}
	public link unit CMD {}
	public link unit ATT {}
	
	receive1.CLK = CLK;
	receive1.DATA = DATA;
	receive1.CMD = CMD;
	receive1.ATT = ATT;
	
	//[i] interface_IRreceive receive1;
	public unit receive1
	{
		public link unit CLK {}
		public link unit DATA {}
		public link unit CMD {}
		public link unit ATT {}
	}
}



