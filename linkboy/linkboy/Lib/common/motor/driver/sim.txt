
void init()
{
	#.OS.REMO_ModuleWrite( (int)(ID * 0x00010000 + 0x0001), 100 );
}
	
	//[i] function_void set_power int32;
	public void set_power( int32 p )
	{
		if( p < 0 ) {
			p = 0;
		}
		if( p > 100 ) {
			p = 100;
		}
		#.OS.REMO_ModuleWrite( (int)(ID * 0x00010000 + 0x0001), p );
	}
	//[i] function_void run_right;
	public void run_right()
	{
		#.OS.REMO_ModuleWrite( (int)(ID * 0x00010000 + 0x0000), 1 );
	}
	//[i] function_void run_left;
	public void run_left()
	{
		#.OS.REMO_ModuleWrite( (int)(ID * 0x00010000 + 0x0000), 2 );
	}
	//[i] function_void stop;
	public void stop()
	{
		#.OS.REMO_ModuleWrite( (int)(ID * 0x00010000 + 0x0000), 0 );
	}
	//[i] function_void short_stop;
	public void short_stop()
	{
		#.OS.REMO_ModuleWrite( (int)(ID * 0x00010000 + 0x0000), 0 );
	}


