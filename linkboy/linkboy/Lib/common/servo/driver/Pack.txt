

unit Pack
{
	public const uint8 ID = 0;
	public const uint8 SERVO_ID = 0;
	
	link unit D {}
	
	//[i] var_uint8 OS_time;
	uint8 OS_time;
	
	uint8 tick;
	uint8 ti;
	
	//---------------------------------------------------
	//初始化
	//[i] function_void OS_init;
	public void OS_init()
	{
		dev_init();
		D.D0_DIR = 1;
		D.D0_OUT = 0;
		
		OS_time = 1;
		
		tick = 0;
		ti = 0;
		
		SetValue( 0 );
	}
	//---------------------------------------------------
	//[i] function_void OS_run;
	public void OS_run()
	{
		if( tick == 0 ) {
			if( C_Angle != Angle ) {
				C_Angle = Angle;
				SetValue( C_Angle );
			}
		}
		else {
			ti += 1;
			if( ti == tick ) {
				ti = 0;
				if( C_Angle > Angle ) {
					C_Angle -= 1;
					SetValue( C_Angle );
					return;
				}
				if( C_Angle < Angle ) {
					C_Angle += 1;
					SetValue( C_Angle );
					return;
				}
			}
		}
	}
	//---------------------------------------------------
	
	//[i] var_int32_w Angle;
	public int32 Angle;
	
	public int32 C_Angle;
	
	//设置步进时间
	//[i] function_void SetTime int32;
	public void SetTime( int32 t )
	{
		if( t < 0 ) t = 0;
		if( t > 255 ) t = 255;
		tick = (uint8)(uint16)(uint)t;
	}
	
	
	//设置角度
	//[i] function_void_old SetValue int32;
	
	#include "$run$.txt"
}


