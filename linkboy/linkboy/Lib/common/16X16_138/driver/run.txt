

	//=================================================================================
	
	uint8 select;
	
	void RunInit()
	{
		A_DIR = 1;
		A_OUT = 0;
		B_DIR = 1;
		B_OUT = 0;
		C_DIR = 1;
		C_OUT = 0;
		D_DIR = 1;
		D_OUT = 0;
		
		G_DIR = 1;
		G_OUT = 1;
		H595.init();
	}
	public void OS_run()
	{
		select += 1;
		select %= 16;
		
		H595.send_byte( ~data_show[select] );
		H595.send_byte( ~data_show[16+select] );
		
		G_OUT = 1;
		
		H595.lock();
		A_OUT = select.0(bit);
		B_OUT = select.1(bit);
		C_OUT = select.2(bit);
		D_OUT = select.3(bit);
		
		G_OUT = 0;
	}
	void DataChanged()
	{
		for( uint8 i = 0; i < 32; i + 1 ) {
			data_show[i] = data[i];
		}
	}
	
	
	//==============================================
	H595.DATA_IN = DATA_IN; H595.DATA_OUT = DATA_OUT; H595.DATA_DIR = DATA_DIR;
	H595.LOCK_IN = LOCK_IN; H595.LOCK_OUT = LOCK_OUT; H595.LOCK_DIR = LOCK_DIR;
	H595.SHIFT_IN = SHIFT_IN; H595.SHIFT_OUT = SHIFT_OUT; H595.SHIFT_DIR = SHIFT_DIR;
	
	unit H595
	{
		public link bit DATA_IN; public link bit DATA_OUT; public link bit DATA_DIR;
		public link bit LOCK_IN; public link bit LOCK_OUT; public link bit LOCK_DIR;
		public link bit SHIFT_IN; public link bit SHIFT_OUT; public link bit SHIFT_DIR;
		
		//==============================================
		public void init()
		{
			DATA_DIR = 1;
			DATA_OUT = 0;
			SHIFT_DIR = 1;
			SHIFT_OUT = 0;
			LOCK_DIR = 1;
			LOCK_OUT = 0;
		}
		//==============================================
		//发送一组数据--包含片选和对应的字形代码
		public void send_byte( uint8 data )
		{
			loop( 8 ) {
				DATA_OUT = data.0(bit);
				data >> 1;
				SHIFT_OUT = 1;
				SHIFT_OUT = 0;
			}
			
		}
		//==============================================
		void lock()
		{
			LOCK_OUT = 1;
			LOCK_OUT = 0;
		}
	}
	
	
	
	
	
	
