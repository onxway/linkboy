
<fold>动画游戏实验室,Software Module Series,230-235-240;3

	<item>背景类,Animation,
		地图 flash\Map\Pack.B,
		背景图片 flash\BackImage\Pack.B,
		背景颜色 flash\BackColor\Pack.B,
		<nextcolumn>
		摄像机 flash\View\View.B,
	</item>

	<item>动画类,Animation,
		精灵 flash\Sprite\Sprite.B,
		<nextcolumn>
		键盘 flash\Keyboard\Pack.B,
	</item>
</fold>

<fold>——————,——————,120-175-180;2
</fold>

<fold>蜗牛(vono)操作系统,gui-tool,230-235-240;1

	<item>键鼠透传助手,mtool,
		工具 vui\Tool\Pack.B,
	</item>

	<item>内核与组件,kernel,
		内核 vui\OS内核.lab,
	</item>

	<item>GUI布局和控件类,obj,
		窗体 vui\ControlPad\ControlPad.B,
		按钮 vui\Button\Button.B,
		滑动条 vui\TraceBar\Pack.B,
		标签 vui\Label\Label.B,
		
		//标签 zui\obj\label\Pack.B,
	</item>

	<item>应用(App)商店,AppStore,
		_ vui\AppStore\秒表.lab,
		_ vui\AppStore\计算器.lab,
	</item>

</fold>

//<fold>框架系列,framework,230-235-240;1

	//<item>物联网(ESP8266从机),iot,
	//	通信 framework\WIFI数据收发器.lab,
	//	<nextcolumn>
	//	贝壳物联 framework\贝壳物联.lab,
	//	济南市中区物联网 framework\济南市中区物联网.lab,
	//</item>

	//<item>物联网(ESP32),iot,
	//	通信 framework\WIFI数据收发器(ESP32).lab,
	//	<nextcolumn>
	//	巴法云 framework\巴法云.lab,
	//</item>

	//<item>游戏,iot,
	//	俄罗斯方块 framework\俄罗斯方块.lab,
	//</item>

//</fold>

<fold>软件模块,Software Module,230-235-240;13

	<item>程序控制,System,
		虚拟控制器 software\SYS\Pack.B,SS:N,
		系统设置 software\SYS_CMD\Pack.B,SS:N,
	</item>

	<item>数据处理,Data Processing,
		随机数发生器 software\random\random.B,
		不重复随机数发生器 software\random_nr\Pack.B,
		//动态字符串 software\string_d\Pack.B,
		字符串 software\string_buffer_v1\Pack.B,
		字符串列表 software\string_list\Pack.B,
		日期 software\Date\Pack.B,
		数据变换器 software\map\Pack.B,
		滤波器 software\Filter\Pack.B,
		滤波器 software\MFilter\Pack.B,
		模拟量触发器 software\SchmittTriger\Pack.B,
		数组 software\Arrary\Pack.B,
		数据列表 software\DataBuffer\Pack.B,
		队列 software\Queue\Pack.B,
	</item>

	<item>数据转换,Data Conversion,
		坐标变换器 software\Coor_fix\Pack.B,
		数学运算库 software\Math\Pack.B,
		颜色变换器 software\Color\Pack.B,
		经纬度变换器 software\JingWeiDu\Pack.B,
		莫尔斯编码器 software\MorseCoding\Pack.B,
	</item>

	<item>定时延时,Time and Delay,
		延时器 software\delayer\delayer.B,
		高精度延时器 software\soft_delayer\delayer.B,
		定时器 software\timer_v1\timer.B,
		计时器 software\count_up\count_up.B,
		倒计时器 software\count_down\count_down.B,
	</item>

</fold>

<fold>功能扩展模块,Function Module,230-235-240;1

	<item>字符屏显示功能扩展,Image and Text,
		文字显示器 engine\Text_v2\Text.B(10'0),
	</item>

	<item>点阵屏显示功能扩展,Image and Text,
		文字显示器 engine\GText\Text.B(10'0),
		字符点阵 engine\GUIchar1\Pack.B(120'30),
		图形显示器 engine\GUI\icon.B(230'30),
		形状显示器 engine\shape\Pack.B(340'30),
		路径绘图器 engine\path\Pack.B(450'30),
		图片显示器 engine\c_bmp\Pack.B,
	</item>

	<item>声音/灯光功能扩展,Sound and Light,
		音乐解码器 engine\music_v1\Pack.B(10'60),
		三角波发生器 engine\wave\TrangleWave\TriangleWave.B(141'0),
		亮度调节器 engine\LED_PWM\LED_PWM.B(221'74),
		闪烁控制器 engine\Flash\Flash.B(330'70),
	</item>

	<item>串口协议功能扩展,Serial Protocal,
		协议解析器 engine\Protocol_v1\Protocol_v1.B(10'40),
		Modbus engine\Modbus\Pack.B(185'40),
		//数据收发器 engine\SingleChannelProtocol\Pack.B(105'0),
		//多通道数据收发器 engine\MultChannelProtocol\Pack.B(230'0),
	</item>

	<item>上位机串口通信,Serial Communication,
		电脑串口 Arduino\UART-PC\Pack.B(115'150),
		波形显示器 engine\CWaveBox\Pack.B(10'0),
		迷你乐队 engine\CMusicBox\Pack.B(120'0),
		//远程接口 engine\remo_api\Pack.B(230'0),
		//机器视觉 engine\CMeyeBox\Pack.B(230'0),
	</item>

	<item>图像识别算子,CV,
		//颜色分析 kendryte\ColorOper\Pack.B,
		机器视觉 software\EI\Basic\Pack.B,
		机器视觉 software\EI\MChar\Pack.B,
		机器视觉 software\EI\MLine\Pack.B,
	</item>

	<item>C语言扩展包(导出模式),c extend,
		arduino-base ndk\arduino-base.cx,
		键盘 ndk\Keyboard.cx,
		wifi ndk\ESP32C3-WIFI.cx,
	</item>
	
</fold>

<fold>物联网IoT,IoT,230-235-240;1

	<item>蓝牙BLE,BLE,
		蓝牙客户端 web\BLE_Client\Pack.B,
	</item>

	<item>WIFI,WIFI,
		wifiAP web\WIFI\Pack.B,SS:N,
		wifiSTA web\WIFI_STA\Pack.B,SS:N,
	</item>

	<item>TCP / IP,TCP / IP,
		TCP_Server web\TCP_Server\Pack.B,SS:N,
		TCP_Client web\TCP_Client\Pack.B,SS:N,
	</item>

	<item>HTTP客户端,HTTP client,
		HTTP_Server web\HTTP_Client\Pack.B,SS:N,
	</item>

	<item>arduino基础件,arduino base,
		basic ndk\arduino.cx,
	</item>

	//<item>网络通信基础件,net,
	//	WIFI ndk\WIFI.cx,
	//	WIFIClient ndk\WIFIClient.cx,
	//	WIFIServer ndk\WIFIServer.cx,
	//	HTTPClient ndk\HTTPClient.cx,
	//</item>
	
</fold>

<fold>——————,——————,120-175-180;1
</fold>

//<fold>自研指令集微处理器,FPGA CPU,230-235-240;1

	//<item>安路科技,ARM32 Master Control Board,
	//	基本控制板 FPGA\ANLOGIC\Pack.B,
	//</item>

	//<item>IO协处理器编译工具,ARM32 Master Control Board,
	//	IDE circuit\0IDE\ASM\Module.M,
	//</item>

	//<item>IO实时协处理器,I/O CPU,
	//	BitPU FPGA\BitPU\Pack.B,
	//</item>
	
	//<item>安路科技 Anlogic,Anlogic,
	//	基本控制板 FPGA\TANG\Pack.B,
	//</item>

//</fold>

<fold>上位机,windows,230-235-240;1

	<item>基础组件,window,
		//基本控制板 vos_pc\pc\Pack.B,SS:N, 过时了
		帧图 kendryte\frame\Pack.B,SS:N,
	</item>

</fold>

//<fold>通用MCU,Main Control Board Series,230-235-240;1
	
	//<item>51内核 通用,ARM32 Master Control Board,
	//	基本控制板 vos_mcu\C51\Pack.B,SS:N,
	//</item>

	//<item>通用芯片,ARM32 Master Control Board,
	//	基本控制板 vos_mcu\mcu32\Pack.B,SS:N,
	//</item>

	//<item>ARM32位芯片系列,ARM32 Master Control Board,
	//	基本控制板 STM32\arm32ABCD\Pack.B,SS:N,
	//	基本控制板 STM32\arm32EFGH\Pack.B,SS:N,
	//</item>

//</fold>

<fold>OpenHarmony,OpenHarmony,230-235-240;1

	<item>操作系统内核功能,kernel,
		内核 harmony_os\kernel\Pack.B,SS:N,
	</item>

	<item>开源大师兄,openbrother,
		基本控制板 harmony_os\openbrother\main_ui\Pack.B,
		扩展板 harmony_os\openbrother\ComExt\Pack.B,
		扩展板 harmony_os\openbrother\PCA9685\Module.M,
		//    屏幕 harmony_os\openbrother\SH1106_12864_i2c\Module.M,
		//    按键 harmony_os\openbrother\Key_F\Module.M,
		//    buton5 harmony_os\openbrother\button5\Pack.B,
		蜂鸣器 harmony_os\openbrother\BeepNS_F\Module.M,
		//声音传感器 ESP32\zkb\SoundTrig\Module.M,
		//扩展板 ESP32\zkb\ComExt\Pack.B,
	</item>

	<item>小熊派(BearPi),BearPi,
		基本控制板 harmony_os\bear_pi\Pack.B,
	</item>

	<item>润和软件(HiHope),HiHope,
		基本控制板 harmony_os\hihope\Pack.B,
		基本控制板 winnermicro\w800-rh\Pack.B,
	</item>

	<item>东方创科,DFCK,
		基本控制板 harmony_os\DFKC\main\Pack.B,SS:N,
		//基本控制板 harmony_os\DFKC\screen\Module.M,SS:N,
	</item>

	<item>小凌派,XLP,
		基本控制板 harmony_os\xlp\Pack.B,
	</item>
</fold>

<fold>物联网 ESP32/8266,ESP32/8266,230-235-240;1

	<item>掌控板,ZK-ESP32,
		基本控制板 ESP32\zkb\main_ui\Pack.B,SS:N,
		基本控制板 ESP32\zkb\main\Pack.B,SS:O,
		//    屏幕 ESP32\zkb\SH1106_12864_i2c\Module.M,
		//    按键 ESP32\zkb\Key_F\Module.M,
		//    buton5 ESP32\zkb\button5\Pack.B,
		//    rgb3 ESP32\zkb\rgb3\Pack.B,
		扩展板 ESP32\zkb\ComExt\Pack.B,
		蜂鸣器 ESP32\zkb\BeepNS_F\Module.M,
		//声音传感器 ESP32\zkb\SoundTrig\Module.M,
	</item>

	<item>ESP32模组,ESP32,
		基本控制板 ESP32\ESP32A\Pack.B,
		基本控制板 ESP32\ESP32WEMOS\Pack.B,
		基本控制板 ESP32\ESP32cam\Pack.B,
	</item>
	
	<item>ESP32C3模组,ESP32C3,
		基本控制板 ESP32\ESP-C3-12F\Pack.B,	
		基本控制板 ESP32\goouuu-ESP32C3\Pack.B,
	</item>
	
	<item>TTGO / LILYGO,TTGO / LILYGO,
		基本控制板 ESP32\TTGO\VM-Display\Pack.B,
		屏幕 ESP32\ext-mod\screen\Pack.B,
		基本控制板 ESP32\TTGO\T-OI-plus\Pack.B,
	</item>

	<item>ESP8266模组,ESP8266,
		基本控制板 ESP8266\NodeMCU\Pack.B,
		基本控制板 ESP8266\NodeMCU1\Pack.B,
		基本控制板 ESP8266\D1\Pack.B,
	</item>

</fold>

<fold>兆易创新 GD32,GD32,230-235-240;1

	<item>GD32智控板,GD32 ZKB,
		基本控制板 GD32\340BOARD_MOTOR\Pack.B,
		基本控制板 GD32\340BOARD\Pack.B,
		控制板 temp\ext\智控板12864\Pack.B,
		控制板 temp\ext\智控板IPS240X240\Pack.B,
	</item>

	<item>GD32(RISC-V),GD32(RISC-V),
		基本控制板 GD32\longan_nano\Pack.B,
		基本控制板 GD32\RISC-V-T1\Pack.B,
	</item>

	<item>GD32(ARM),GD32(ARM),
		基本控制板 GD32\E231START\Pack.B,
		基本控制板 GD32\F303START\Pack.B,
		//基本控制板 GD32\MJZN\Pack.B,
	</item>

	<item>GD32W 物联网,GD32W,
		基本控制板 GD32\W515T1\Pack.B,
		基本控制板 GD32\W515T2\Pack.B,
	</item>

	<item>小熊派(BearPi),BearPi,
		基本控制板 GD32\BearPi\Pack.B,
	</item>

</fold>

<fold>意法半导体 STM32,STM32,230-235-240;1
	
	<item>STM32F1XX主控板,STM32F1XX,
		基本控制板 STM32\stm32f103c8t6_b\Pack.B,
		基本控制板 STM32\stm32f103c8t6_blue\Pack.B,
		基本控制板 STM32\stm32f103c8t6_T1\Pack.B,
		基本控制板 STM32\stm32f103c8t6_USB_blue\Pack.B,
		基本控制板 STM32\stm32f103rct6_T1\Pack.B,
	</item>

	<item>STM32F4XX主控板,STM32F4XX,
		基本控制板 STM32\stm32f405rgt6_T1\Pack.B,
		基本控制板 STM32\nucleo411re\Pack.B,
	</item>

	//<item> 中移物联OneNet,OneNet,
	//	基本控制板 STM32\stm32f103ret6_kylin\Pack.B,
	//</item>

	//<item> 虚谷号,VBoard,
	//	基本控制板 dfl\vvboard\MEGA.B,SS:N,
	//</item>

</fold>

<fold>Arduino 主控板,Arduino,230-235-240;1

	<item>Arduino (通用),Arduino Std,
		基本控制板 vos_mcu\exmcu\Pack.B,SS:N,
	</item>
	
	<item>Arduino nano/UNO,Arduino nano/UNO,
		基本控制板 Arduino\nanox\MEGA.B,
		基本控制板 Arduino\uno\MEGA.B,
	</item>

	<item>Arduino mini,Arduino mini,
		//基本控制板 temp\m128t1\Pack.B,
		//<nextcolumn>
		基本控制板 Arduino\mini_8m\MEGA.B,SS:N,
		基本控制板 Arduino\mini\MEGA.B,
	</item>
	
	<item>Arduino pro micro,Arduino pro micro,
		基本控制板 Arduino8\micro\Pack.B,
	</item>

	<item>Arduino leonardo,Arduino leonardo,
		基本控制板 Arduino8\leonardo\Pack.B,
	</item>
	
	<item>Arduino 644/2560,Arduino 644/2560,
		基本控制板 cboard\b_m644pa\MEGA.B,
		基本控制板 Arduino\2560\Pack.B,
		基本控制板 Arduino\2560B\Pack.B,
		//<nextcolumn>
		//自定义控制板 user\new\Nano.B,SS:N,
		//自定义控制板 user\new\Uno.B,SS:N,
		//基本控制板 Arduino\VHgen\Pack.B===========,
		//<nextcolumn>
		//连接器 Arduino\client_channel\uart.B===========,
	</item>

	<item>Arduino 盾板,Arduino Shield,
		扩展板 Arduino\uno-shield\MEGA.B,
		扩展板 dfl\uno\MEGA.B,
		扩展板 Arduino\nano-uno\MEGA.B,
		扩展板 Arduino\nano-unored\MEGA.B,
		扩展板 Arduino\shieldl293d\MEGA.B,
		扩展板 Arduino\uno-PS2\MEGA.B,
	</item>

	//<item>Arduino 输出组合,Output Combination,
	//	点阵 temp\ext\8X8\Pack.B,
	//	数码管 temp\ext\seg4\Pack.B,
	//	液晶屏 temp\ext\1602\Pack.B,
	//	<nextcolumn>
	//	2路马达 temp\ext\motor\Pack.B,
	//	4路马达 temp\ext\motor4\Pack.B,
	//	<nextcolumn>
	//	扩展板 temp\ext\644board\Pack.B,
	//</item>

	//<item>Arduino 传感组合,Sensor Combination,
	//	LD3320 temp\ext\LD3320\Pack.B,
	//	<nextcolumn>
	//	ESP8266 temp\ext\ESP8266\Pack.B,
	//	nrf2401 temp\ext\nrf2401\Pack.B,
	//	<nextcolumn>
	//	rc522 temp\ext\rc522\Pack.B,
	//</item>

	//<item>Arduino 输入输出组合,Sensor Combination,
	//	wifiMotor temp\ext\wifiMotor\Pack.B,
	//	<nextcolumn>
	//	学习板 temp\ext\study\Pack.B,
	//</item>

	<item>Arduino功能扩展,Arduino lib,
		存储器 software\eeprom\eeprom.B,SS:N,
		<nextcolumn>
		休眠 software\sleep-avr\Pack.B,SS:N,
		休眠唤醒端口 Uport\IntWake\Pack.B,SS:N,
	</item>

</fold>

<fold>其他主板,other,230-235-240;1

	<item>沁恒 WCH,STM32 Master Control Board,
		基本控制板 WCH\UNO\Pack.B,
	</item>
	
	<item>中科蓝讯 AB32,AB32 Master Control Board,
		基本控制板 AB32\UNO\Pack.B,
	</item>

	<item>国民技术 Nation,N32 Master Control Board,
		基本控制板 Nation\N32WB031STB\Pack.B,
	</item>

	<item>Maix-Bit主控(K210),Maix-Bit(K210),
		基本控制板 kendryte\maix-bit\Pack.B,
	</item>

//	<item>扩展模块,GD32 Master Control Board,
//		摄像头 kendryte\camera\Pack.B,
//		帧图 kendryte\frame\Pack.B,SS:N,
//		屏幕 kendryte\screen\Pack.B,
//	</item>

	<item>micro:bit V1,micro:bit V1,
		Mbit Nordic\microbit\MicrobitV1-ui\Pack.B,SS:N,
		Mbit Nordic\microbit\MicrobitV1\Pack.B,SS:O,
		//    屏幕 Nordic\microbit\display\Module.M,
		//    按键 Nordic\microbit\Key_F\Module.M,
		扩展板 Nordic\microbit\ComExt\Pack.B,
	</item>
	
	<item>树莓派Pico-RP2040,Pico-RP2040,
		控制板 pico\pico\Pack.B,
	</item>

	<item>Seeed Studio,Seeed Studio,
		控制板 seeed\XIAO-SAMD\Pack.B,
	</item>
	
	<item>联盛德微电子,winnermicro,
		基本控制板 winnermicro\w801\Pack.B,
		基本控制板 winnermicro\w806\Pack.B,
		//<nextcolumn>
		//基本控制板 winnermicro\w800-rh\Pack.B,
		基本控制板 winnermicro\w800-t1\Pack.B,
	</item>

	<item>TG7100C 系列,TG7100C,
		基本控制板 t-head\TG7100C_T1\Pack.B,
		基本控制板 t-head\TG7100C_T2\Pack.B,
	</item>

	<item>合宙 LuatOS,TG7100C,
		基本控制板 luatos\AIR001\Pack.B,
	</item>

	<item>STC 宏晶,STC,
		基本控制板 vos_mcu\C51\Pack.B,SS:N,
		基本控制板 STC\STC12C5A60S2\Pack.B,
		基本控制板 STC\STC15W4K\Pack.B,
	</item>

	<item>全志(AllWinner),Master Control Board,
		基本控制板 allwinner\D1\Pack.B,
	</item>

	//板子不完善
	//<item>华镇电子-语音识别,Master Control Board,
	//	基本控制板 VB590\T1\Pack.B,
	//</item>

</fold>

<fold>——————,——————,120-175-180;1
</fold>

<fold>传感输入模块,Input Module,230-235-240;1

	<item>按键输入,Key Input,
		<nextcolumn>
		按钮 input\Key_F\Module.M,
		<nextcolumn>
		红按键 input\button\single_key_red.M,
		蓝按键 input\button\single_key_blue.M,
		<nextcolumn>
		白按键 input\button1\single_key_white.M,
		红按键 input\button1\single_key_red.M,
		黄按键 input\button1\single_key_yellow.M,
		绿按键 input\button1\single_key_green.M,
		蓝按键 input\button1\single_key_blue.M,
		<nextcolumn>
		1位电容按键 input\Touch1_T1\Module.M,
		1位电容按键 input\Touch1\Module.M,
		4位电容按键 input\Touch4\Module.M,
		8位电容按键 input\Touch8\Module.M,
		按钮 UModel\input\Key_F\White1.M,SS:N,
		按钮 UModel\input\Key_Z\White1.M,SS:N,
	</item>

	<item>机械感应检测,Machine Sensor,
		微动开关 Arduino\MKey_F\Key.B,
		微动开关 input\Key_F_T1\Module.M,
		碰触开关 group1\KeyTrig\Pack.B,
		磁簧管 Arduino\CiHuangGuan_F\Pack.B,
		门磁开关 common\MenCi\Pack.B,
		倾斜传感器 Arduino\QingXie\Pack.B,
		倾斜传感器 input\QingXie\Module.M,
		水位浮球开关 common\Water\Pack.B,
		震动传感器 input\ZhenDong\Module.M,
		//震动传感器 input\ZhenDong1\Module.M,
		震动传感器 input\ZhenDong2\Module.M,
		震动传感器 group1\hit\Pack.B,
	</item>

	<item>触发传感器,Trigger Sensor,
		红外避障传感器 Arduino\IRDetect\Pack.B,
		红外探测器 group1\IRdetect\Pack.B,
		旋转编码器 group1\Encoder\Pack.B,
		旋转编码器 group1\CrossRol\Pack.B,
		光电对管 input\DuiGuanTrigB\Module.M,
		光电对管 input\DuiGuanTrig\Module.M,
		人体热释电传感器 Arduino\IRTrig\Pack.B,
		光照强度计 group1\light\Pack.B,
		温度计 group1\Temperature\Pack.B,
		光电避障 group1\RedLightTrig\Pack.B,
		霍尔磁场传感器 group1\Huor\Pack.B,
		火焰报警器 input\Fire\Module.M,
		循迹模块 input\XunJi1V1\Module.M,
		<nextcolumn>
		声音传感器 UModel\input\SoundTrig\Module.M,SS:N,
		声控模块 group1\SoundTrig\Pack.B,
		声控模块 group1\SoundTrig1\Pack.B,
		压电模块 input\YaDian\Module.M,
		<nextcolumn>
		循迹模块 input\XunJiV1\Module.M,
		3路循迹模块 common\XunJi3V1\Pack.B,
		//<nextcolumn>
		//高精度数字光照传感器 Arduino\TSL2561\pack.B===========,
	</item>

	<item>数值传感器,Numerical Sensor,
		滑变电阻 UModel\input\ADR\Module.M,SS:N,
		滑动变阻器 Electronic\input\ADR\Module.M,
		滑变电阻 input\ADR\Module.M,
		滑变电阻 input\Slide\Module.M,
		摇杆 input\AD_XYK\Module.M,
		<nextcolumn>
		脉搏传感器 group1\MaiboTrig\Pack.B,
		称重传感器 common\hx711\Pack.B,
		<nextcolumn>
		//声音传感器 UModel\input\Sound\Module.M,SS:N,
		//声音传感器 input\SoundAD\Module.M,
		声音传感器 input\Sound\Module.M,
		<nextcolumn>
		光照传感器 UModel\input\LightR_Z\Module.M,SS:N,
		光照传感器 UModel\input\LightR_F\Module.M,SS:N,
		//电阻 Arduino\RegisterPULL\Register.B,
		光敏变阻器 Electronic\input\LightR\Module.M,
		//光敏电阻 input\LightR\Module.M,
		//光敏电阻 input\LightR1\Module.M,
		//光敏电阻 input\LightRZ\Module.M,
		//光敏电阻 input\LightR1Z\Module.M,
		光敏电阻 input\LightR2\Module.M,
		<nextcolumn>
		高精度数字光照传感器 Arduino\GY30_BH1750FVI\pack.B,
		数字光照传感器 Arduino\TSL2561\pack.B,
		颜色传感器 input\TCS34725_P\Module.M,
		颜色传感器 common\TCS230\pack.B,
	</item>

	<item>温湿度传感器,Temp / Humidity,
		温敏电阻 Arduino\ADtemprature\Pack.B,
		<nextcolumn>
		温度传感器 UModel\input\18B20\Module.M,SS:N,
		温度计 Electronic\input\ds18b20\Module.M,
		温度计 input\ds18b20_L\Module.M,
		温度计 input\ds18b20\Module.M,
		<nextcolumn>
		DHT11温湿度计 input\dht11_T0\Module.M,
		温湿度计 input\dht11\Module.M,
		温湿度计 input\dht11_T1\Module.M,
		<nextcolumn>
		DHT22温湿度计 input\dht22_T0\Module.M,
		DHT22温湿度计 input\dht22_T2\Module.M,
		DHT21温湿度计 input\dht22_T1\Module.M,
		<nextcolumn>
		热电偶max6675 input\max6675\Module.M,
		<nextcolumn>
		//土壤湿度传感器 common\TuRang\Pack.B,
		土壤湿度传感器 input\TuRang\Module.M,
		雨滴传感器 common\YuDi\Pack.B,
	</item>

	<item>探测传感器,Detection Sensor,
		飞行时间测距 input\VL53L0X\Module.M,
		夏普红外测距 group1\SharpIR\Pack.B,
		超声波测距模块 input\ultrasonic_u\Module.M,SS:N,
		超声波测距模块 input\ultrasonic\Module.M,
		<nextcolumn>
		北斗定位器 input\BeiDou1\Module.M,
		北斗定位器 input\BeiDou\Module.M,
		USB-HID input\CH9350\Module.M,
		语音识别模块 input\SU-03T_p\Module.M,
		语音识别模块 group1\LD3320A\Pack.B,
		射频卡模块 input\RC522\Module.M,
		<nextcolumn>
		加速度传感器 common\GY61\Pack.B,
		加速度传感器 common\ADXL345\Pack.B,
		MPU6050 common\MPU6050\Pack.B,
		地磁场传感器 input\HMC5883L\Module.M,
	</item>

	<item>环境与气象传感器,Environment Sensor,
		烟雾传感器 input\MQ2_AD\Module.M,
		紫外线传感器 input\ML8511\Module.M,
		雷电传感器 input\AS3935\Module.M,
		<nextcolumn>
		风速传感器 input\WindSpeed\Module.M,
		风向传感器 input\WindDir\Module.M,
		<nextcolumn>
		雨滴模块 group1\Rain\Pack.B,
		雨量计 input\RainCount\Module.M,
		<nextcolumn>
		PH传感器 common\PH\Pack.B,
		PM2.5模块 group1\PM2_5\Pack.B,
	</item>
</fold>

<fold>键盘和遥控,Keyboard/Remote,230-235-240;1

	<item>AD键盘,AD Keyboard,
		键盘 input\AD_Button\Red2\Module.M,
		//键盘 input\AD_Button\Black2\Module.M,
		<nextcolumn>
		键盘 input\AD_Button\Red\Module.M,SS:N,
		//键盘 input\AD_Button\Black\Module.M,SS:N,
	</item>

	<item>矩阵键盘,Matrix Keyboard,
		4*3矩阵键盘 input\Key4X3C\Module.M,
		4*4矩阵键盘 input\Key4X4C\Module.M,
		<nextcolumn>
		4*3矩阵键盘 input\Key4X3\Module.M,
		4*4矩阵键盘 input\Key4X4\Module.M,
		<nextcolumn>
		4键盘 input\button4\Module.M,
		4*4矩阵键盘 group1\Key4_4\Pack.B,
	</item>

	<item>红外遥控,Infrared Remote,
		//红外接收 UModel\input\IRpad_linker\Module.M,SS:N,
		红外遥控接收头 Electronic\input\IRpad_linker\Module.M,
		//红外遥控接收器 input\IRpad_linker\Module.M,
		红外遥控接收器 input\IRpad_linker1\Module.M,
		<nextcolumn>
		红外遥控器 common\IRpad\IRpad.B,
		<nextcolumn>
		红外遥控器 Arduino\IRpad\IRpad1.B,
		<nextcolumn>
		红外遥控器 common\IRpad_B\IRpad.B,
		<nextcolumn>
		红外遥控器 common\IRpad_WB\IRpad.B,
	</item>

	<item>无线遥控,Wireless Remote,
		无线遥控器 common\ps2\Pack.B,
		遥控接收板 common\ps2_linker\Pack.B,
		遥控接收板 common\ps2_linker1\Pack.B,
		<nextcolumn>
		无线遥控ppm input\ppm-pad\Module.M,
		<nextcolumn>
		无线遥控接收器 input\RF_Pad_Black\RFpad_linker\Module.M,
		无线遥控器4 input\RF_Pad_Black\RFpad4\Module.M,
		无线遥控器2 input\RF_Pad_Black\RFpad2\Module.M,
		无线遥控器1 input\RF_Pad_Black\RFpad1\Module.M,
		<nextcolumn>
		无线遥控器 group1\RFpad\Pack.B,
		无线遥控接收器 group1\RFpad_linker\Pack.B,
	</item>
</fold>

<fold>LED灯和数码点阵,LED and NixieTube,230-235-240;1

	<item>LED灯,Lighting,
		LED灯 UModel\output\LED_A1\Red1.M,SS:N,
		LED灯 UModel\output\LED_A0\Red1.M,SS:N,
		<nextcolumn>
		电阻 Arduino\RegisterLINE\Register.B,
		指示灯 output\LED_F\WModule.M,
		指示灯 output\LED_F\RModule.M,
		指示灯 output\LED_F\YModule.M,
		指示灯 output\LED_F\GModule.M,
		指示灯 output\LED_F\BModule.M,
		//指示灯 output\LED_F\PModule.M,
		<nextcolumn>
		红色指示灯 output\LED\RLED\Module.M,
		黄色指示灯 output\LED\YLED\Module.M,
		绿色指示灯 output\LED\GLED\Module.M,
		蓝色指示灯 output\LED\BLED\Module.M,
		红绿 UModel\output\RG_LED1\Module.M,SS:N,
		红绿 UModel\output\RG_LED0\Module.M,SS:N,
	</item>

	<item>RGB彩灯,RGB Lighting,
		彩灯 output\RGB_lightT1\Module.M,
		彩灯 output\RGB_LED4\Module.M,
		彩灯 output\RGB_LED2\Module.M,
		//彩灯 output\RGB_LED3\Module.M,
		<nextcolumn>
		七彩灯 output\LED\RGB_LED\Module.M,
		//三色灯 output\LED\RGB_LED_Black\Module.M,
		<nextcolumn>
		七彩灯 output\LED\RGB_LED1\Module.M,
		七彩灯 output\LED\VRBG_LED_WHITE\Module.M,
		//七彩灯 output\LED\VRBG_LED_RED\Module.M,
		<nextcolumn>
		多色灯 output\RGB_LED_T1\Module.M,
	</item>

	<item>灯带与灯环,Lamp Bands and Rings,
		24路灯环 output\RGB_Circle24\Module.M,
		<nextcolumn>
		16路灯环 output\RGB_Circle16\Module.M,
		<nextcolumn>
		12路灯环 output\RGB_Circle12\Module.M,
		<nextcolumn>
		8路灯环 output\RGB_Circle8\Module.M,
		<nextcolumn>
		灯带 output\RGB_lightArray\Module.M,
	</item>

	<item>LED数码点阵元件,Lattice and DigitalTube,
		8*8点阵 output\SEG788BS\Module.M,
		8*8点阵 output\SEG7088AH\Module.M,SS:N,
		<nextcolumn>
		共阴3位数码管 Arduino\SEG3361AH_v1\Pack.B,SS:N,
		共阳3位数码管 Arduino\SEG3361BH_v1\Pack.B,SS:N,
		共阴4位数码管 Arduino\SEG3461AH_v1\Pack.B,SS:N,
		共阳4位数码管 Arduino\SEG3461BH_v1\Pack.B,SS:N,
		<nextcolumn>
		共阴1位数码管 Arduino\SEG7X1ALG5611AH\SEG7X1ALG5611AH.B,SS:N,
		共阳1位数码管 Arduino\SEG7X1BLG5611BH\SEG7X1BLG5611BH.B,SS:N,
		共阴4位数码管 Arduino\SEG7X4A\SEG7X4A.B,
		共阳4位数码管 Arduino\SEG7X4B\SEG7X4B.B,SS:N,
		<nextcolumn>
		共阴4位数码管 Arduino\SEG8041AH\Pack.B,SS:N,
		共阳4位数码管 Arduino\SEG8041BH\Pack.B,SS:N,
		<nextcolumn>
		共阴1位数码管 Arduino\SEG7X1A\SEG7X1A.B,SS:N,
		共阳1位数码管 Arduino\SEG7X1B\SEG7X1B.B,SS:N,
	</item>

	<item>数码管,DigitalTube,
		4位数码管 output\TM1650\Module.M,
		4位数码管 output\TM1637_4\Module.M,
		4位数码管 common\SEG8_4_T1\Pack.B,
		4位数码管 output\SEG8_4_T2\Module.M,
		<nextcolumn>
		8位数码管 group1\SEG8_8_T1\Pack.B,
		8位数码管 group1\MAX7219_SEG8_8\Pack.B,
	</item>

	<item>LED点阵,LED Matrix,
		8X8点阵 output\MAX7219_Y1X1\Module.M,
		8X8点阵 output\MAX7219_1X1\Module.M,
		XY点阵 output\MAX7219_XY\Module.M,SS:N,
		<nextcolumn>
		16X8点阵 output\MAX7219_2X1\Module.M,
		16X16点阵 output\MAX7219_2X2\Module.M,
		<nextcolumn>
		32X8点阵 output\MAX7219_4X1\Module.M,
		32X16点阵 output\MAX7219_4X2\Module.M,
		<nextcolumn>
		LED点阵 output\P10_16X32\Module.M,
		16X16点阵 output\16X16_138\Module.M,
	</item>

</fold>

<fold>液晶显示屏,LCD display,230-235-240;1

	<item>字符液晶屏,Character LCD Screen,
		1602液晶屏 Arduino\lcd1602PCF8574\Pack.B,
		1602液晶屏 Arduino\lcd1602\Pack.B,SS:N,
		1602液晶屏 Arduino\lcd1602P\Pack.B,
		<nextcolumn>
		2004液晶屏 Arduino\lcd2004PCF8574\Pack.B,
		2004液晶屏 Arduino\lcd2004\Pack.B,SS:N,
		2004液晶屏 Arduino\lcd2004P\Pack.B,
		<nextcolumn>
		12864液晶屏 Arduino\ST7920S_12864\ram.B,
		12864液晶屏 output\ST7920G\Module.M,SS:N,
	</item>

	<item>单色点阵液晶屏,Lattice LCD Screens,
		12864液晶屏 output\SH1106_12864_i2c\Module.M,
		//12864液晶屏 output\SH1106_12864_i2ct1\Module.M,
		12864液晶屏 output\SH1106_12864_i2c0.96\Module.M,
		<nextcolumn>
		12864液晶屏 output\SH1106_12864t1\Module.M,
		12864液晶屏 output\SH1106_12864t2\Module.M,
		//12864液晶屏 output\SH1106_12864\Module.M,
		<nextcolumn>
		12864液晶屏 output\SH1106_12864_font\Module.M,
		<nextcolumn>
		12864液晶屏 output\ST7565_12864_font\Module.M,
	</item>

	<item>彩色液晶屏,Color LCD screen,
		彩屏 output\ST7789_240X240T1\Module.M,
		彩屏 output\ILI9341\Module.M,
	</item>

</fold>

<fold>驱动输出,Driver and Output,230-235-240;1

	<item>声音输出,Voice Output,
		蜂鸣器 UModel\output\Beep\Beep_F\Module.M,SS:N,
		蜂鸣器 UModel\output\Beep\Beep_Z\Module.M,SS:N,
		<nextcolumn>
		蜂鸣器 output\Beep_F\Module.M,
		蜂鸣器 output\BeepT1_F\Module.M,
		蜂鸣器 output\BeepM1\Module.M,
		//蜂鸣器 output\BeepM1F\Module.M,
		//蜂鸣器 output\BeepM1FT2\Module.M, 主要是默认电平高低问题需要处理一下
		//<nextcolumn>
		//蜂鸣器 output\BeepM2\Module.M,
		<nextcolumn>
		无源蜂鸣器 output\BeepNS_F\Module.M,
		扬声器 Electronic\output\speaker\Module.M,
		扬声器 Electronic\output\speakerT2\Module.M,SS:N,
		<nextcolumn>
		//和弦播放器 output\WaveTable\Module.M,
		//扬声器 common\melody2\tone.B,SS:N,
		<nextcolumn>
		语音录放模块 output\ISD1820\Module.M,
		JQ8900S output\JQ8900S\Module.M,
		DFPlayer output\DFPlayer\Module.M,
		<nextcolumn>
		语音朗读模块 output\UART_Sound\Module.M,
		语音朗读模块 output\SYN6288\Module.M,
	</item>

	<item>驱动和控制,Motor and Drive,
		马达驱动器 output\L298motor_linker\Module.M,
		马达驱动器 output\6612motor_linker_v1\Module.M,
		马达驱动器 output\NY9M135Amotor_linker\Module.M,
		马达驱动器 output\DRV8833BLK\Module.M,
		马达驱动器 output\9110motor_linker\Module.M,
		<nextcolumn>
		步进马达驱动器 output\a4988\Module.M,
		步进马达驱动器 output\StepMotorDriver\Module.M,
		<nextcolumn>
		步进马达 common\stepmotor_v1\Pack.B,SS:N,
		步进马达驱动器 group1\stepmotor_linker_v1\Pack.B,
		<nextcolumn>
		振动马达 group1\zmotor\Pack.B,
		单路继电器 output\relay1\Module.M,
		单路继电器 output\relay1a\Module.M,
		双路继电器 group1\relay2\Pack.B,
	</item>

	<item>马达和舵机,Motor and Servo,
		马达 common\motor\Pack.B,SS:N,
		<nextcolumn>
		180度舵机 output\servo\Module.M,SS:N,
		<nextcolumn>
		270度舵机 output\servo270\Module.M,SS:N,
		<nextcolumn>
		360度舵机 output\servo360\Module.M,
	</item>

	<item>信号输出,Signal output,
		PWM输出器 output\PCA9685\Module.M,
		频率合成器 group1\AD9850\Pack.B,
	</item>

</fold>

<fold>时间/存储/通信/扩展,Time/Store/Comm/Ext,230-235-240;1

	<item>辅助元件,Auxiliary Element,
		电源正极 Arduino\VCC_v1\VCC.B,SS:N,
		电源正极 Arduino\VCC_3.3V\VCC.B,SS:N,
		电源负极 Arduino\GND_v1\GND.B,SS:N,
		悬空引脚 Uport\NCPort\Pack.B,SS:N,
		//悬空引脚 Uport\NPort\Pack.B,SS:N, 
		电阻 Arduino\RegisterPULL\Register.B,
		电阻 Arduino\RegisterLINE\Register.B,
		电容 Electronic\assist\Capacity\Module.M,SS:N,
		<nextcolumn>
		面包板 Arduino\MianBaoBan_mini\Pack.B,SS:N,
		面包板 Arduino\MianBaoBan\Pack.B,SS:N,
		<nextcolumn>
		电池盒 common\power\Pack.B,SS:N,
		电池盒 common\power9V\Pack.B,SS:N,
		扬声器 Electronic\assist\Speaker\Module.M,SS:N,
	</item>

	<item>时钟计时,Clock timing,
		时钟模块 group1\ds1302\Pack.B,
		<nextcolumn>
		时钟芯片 UModel\input\DS1307\Module.M,SS:N,
		时钟模块 input\DS1307\Module.M,
		时钟模块 input\DS3231\Module.M,
		时钟模块 input\DS3231_T1\Module.M,
		<nextcolumn>
		时钟芯片 UModel\input\PCF8563T\Module.M,SS:N,
		时钟模块 input\PCF8563T\Module.M,
	</item>

	<item>数据存储,Data storage,
		存储器 extmodule\24C-IIC\24c02\Module.M,
		存储器 extmodule\24C-IIC\24c256\Module.M,
	</item>

	<item>有线和无线通信,Wired and Wireless,
		ESP8266 temp\8266pad\Pack.B,SS:O,
		ESP8266 iot_v1\ESP8266C\Module.M,
		ESP8266 iot_v1\ESP8266T\Module.M,
		ESP8266 iot_v1\ESP8266B\Module.M,
		<nextcolumn>
		蓝牙HC06 iot_v1\HC06\Module.M,
		NRF2401 Arduino\NRF2401\Pack.B,
		<nextcolumn>
		I2C Arduino\PCF8574\Pack.B,
		RS232 iot_v1\RS232\Module.M,
	</item>

	<item>标准通信和协议,Standard communication,
		I2C Uport\I2C\Pack.B,SS:N,
		<nextcolumn>
		硬件串口 iot_v1\UART\UART.M,SS:N,
		软件串口 software\SOFT_UART\UART.B,SS:N,
	</item>

	<item>扩展功能,Extend,
		Arduino Arduino\Arduino-base\Pack.B,
		<nextcolumn>
		电平检测端口 Uport\Input\Input.B,SS:N,
		电平输出端口 Uport\Output\Output.B,SS:N,
		AD转换端口 Uport\ADInput\ADInput.B,SS:N,
		脉冲计数端口 Uport\IntCounter\Pack.B,SS:N,
		<nextcolumn>
		PWM端口 Uport\PWM\Pack.B,SS:N,
		PWM端口 Uport\Soft_PWM\Pack.B,SS:N,
		PWM端口 Uport\Soft_PWM_fast\Pack.B,SS:N,
		<nextcolumn>
		频率输出端口 Uport\FREQ\Pack.B,SS:N,
	</item>

</fold>

<fold>工程师系列,Engineer Series,230-235-240;1

	<item>接口,Interface Class,
		PAD chip\Pad\Pack.B,
		<nextcolumn>
		D2 chip\IPort\D2\Pack.B,
		D4 chip\IPort\D4\Pack.B,
		D5 chip\IPort\D5\Pack.B,
		D6 chip\IPort\D6\Pack.B,
		D7 chip\IPort\D7\Pack.B,
		D8 chip\IPort\D8\Pack.B,
		D9 chip\IPort\D9\Pack.B,
		D10 chip\IPort\D10\Pack.B,
		D13 chip\IPort\D13\Pack.B,
		<nextcolumn>
		PGD6 chip\IPort\PGD6\Pack.B,
		PGD8 chip\IPort\PGD8\Pack.B,
		PGD10 chip\IPort\PGD10\Pack.B,
		PGD16 chip\IPort\PGD16\Pack.B,
	</item>

	<item>AVR芯片,AVR Chip Class,
		基本控制板 Arduino\mega328\QFP.B,
		基本控制板 Arduino\mega328\MEGA.B,
	</item>

	<item>其他元件,Other Components,
		按键 chip\cust\key_f.M,
		TB6612FNG chip\TB6612FNG\Pack.B,
		彩屏 chip\IPS130\Module.M,
		<nextcolumn>
		H74595 Arduino\H74595\H74595.B,
		H74595 Arduino\H74595X2\H74595X2.B,
		ULN2803 Arduino\ULN2803\ULN2803.B,
		<nextcolumn>
		时钟模块 common\ds1302\Pack.B,
	</item>

</fold>

//<fold>—第三方厂商系列—,——————,120-175-180;1
//</fold>

//<fold><添加更多模块...>,<add new>,150-175-180;1
//</fold>

//表示文件结束
</fold>





