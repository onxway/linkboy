
unit motor
{
	public const uint16 ID = 0;
	
	public link unit INA1 {}
	public link unit INA2 {}
	public link unit INB1 {}
	public link unit INB2 {}
	
	public link unit PWMA {}
	public link unit PWMB {}
	public link unit STBY {}
	
	int8 valueA;
	int8 valueB;
	int8 tick;
	
	//---------------------------------------------------
	//[i] function_void set_valueA int32;
	void set_valueA( int32 v )
	{
		if( v < 0 ) {
			valueA = 0;
		}
		else if( v > 100 ) {
			valueA = 100;
		}
		else {
			valueA = (int8)(int16)v;
		}
	}
	//---------------------------------------------------
	//[i] function_void set_valueB int32;
	void set_valueB( int32 v )
	{
		if( v < 0 ) {
			valueB = 0;
		}
		else if( v > 100 ) {
			valueB = 100;
		}
		else {
			valueB = (int8)(int16)v;
		}
	}
	//---------------------------------------------------
	//[i] function_void OS_init;
	public void OS_init()
	{
		INA1.D0_DIR = 1;
		INA1.D0_OUT = 0;
		INA2.D0_DIR = 1;
		INA2.D0_OUT = 0;
		
		INB1.D0_DIR = 1;
		INB1.D0_OUT = 0;
		INB2.D0_DIR = 1;
		INB2.D0_OUT = 0;
		
		STBY.D0_DIR = 1;
		STBY.D0_OUT = 1;
		
		PWMA.D0_DIR = 1;
		PWMA.D0_OUT = 1;
		PWMB.D0_DIR = 1;
		PWMB.D0_OUT = 1;
		
		valueA = 100;
		valueB = 100;
		tick = 0;
	}
	//---------------------------------------------------
	//[i] function_void OS_run100us;
	public void OS_run100us()
	{
		tick % 100;
		bool ResetA = tick == 0;
		bool ResetB = ResetA;
		
		if( tick == valueA ) {
			PWMA.D0_OUT = 0;
			ResetA = false;
		}
		if( tick == valueB ) {
			PWMB.D0_OUT = 0;
			ResetB = false;
		}
		tick + 1;
		
		if( ResetA ) {
			PWMA.D0_OUT = 1;
		}
		if( ResetB ) {
			PWMB.D0_OUT = 1;
		}
	}
	//---------------------------------------------------
	//[i] function_void open;
	public void open()
	{
		STBY.D0_OUT = 1;
	}
	//---------------------------------------------------
	//[i] function_void close;
	public void close()
	{
		STBY.D0_OUT = 0;
	}
}
link unit OUTA1 {} = driver.INA1;
link unit OUTA2 {} = driver.INA2;
link unit OUTB1 {} = driver.INB1;
link unit OUTB2 {} = driver.INB2;





