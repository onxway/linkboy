//[配置信息开始],
//[组件文件] button_red.M,
//[语言列表] c chinese,
//[组件名称] single_key_red BlueKey 红按钮,
//[模块型号] ,
//[资源占用] ,
//[实物图片] R1.png,R2.png,R3.png,
//[仿真类型] 1,
//[控件属性] 文字,字体,文字颜色,背景颜色,
//[元素子项] var_uint8 OS_EventFlag OS_EventFlag OS_EventFlag,
//[元素子项] event key0_press_event KeyPressEvent 按钮按下时,
//[元素子项] event key0_up_event KeyUpEvent 按钮松开时,
//[元素子项] var_bool key0_press KeyPress 按钮按下,
//[元素子项] var_uint8 OS_time OS_time OS_time,
//[元素子项] function_void OS_init OS_init OS_init,
//[元素子项] function_void OS_run OS_run OS_run,
//[配置信息结束],

unit single_key_red
{
	#include "single_key_red\$language$.txt"
	driver =
	#include "driver\single_key_red.txt"
}
