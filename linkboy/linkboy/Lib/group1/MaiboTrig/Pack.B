//[配置信息开始],
//[组件文件] Module.M,
//[语言列表] c chinese,
//[组件名称] Pack PulseSensor 脉搏传感器,
//[模块型号] ,
//[资源占用] ,
//[实物图片] 1.png,2.png,
//[仿真类型] 1,
//[元素子项] function_void OS_init OS_init OS_init,
//[元素子项] function_int32_debug get_data Value 数值,
//[元素子项] linkconst_int32_0 MinValue MinValue 最小值,
//[元素子项] linkconst_int32_1000 MaxValue MaxValue 最大值,
//[配置信息结束],

unit Pack
{
	#include "Pack\$language$.txt"
	driver =
	#include "driver\Pack.txt"
}
