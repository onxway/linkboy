<module>
通过滑动组件上边的旋钮可以调节数值，输出的数值范围可设置，当滑到最左端时，数值为最小值；滑到最右端时数值为最大值.
</module>
<name> Pack 旋钮电阻器,
</name>
<member> function_void OS_init OS_init,

</member>,
<member> function_int32_debug get_data 数值,
调用这个指令返回当前的滑动值，输出数值范围可设置，当滑到最左端时，数值为最小值；滑到最右端时数值为最大值；
</member>,
<member> linkconst_int32_0 MinValue 最小值,
设置直线滑动电阻器的最小值, 当从最左端滑到最右端时, 读取的数值将会从最小值变化到最大值
</member>,
<member> linkconst_int32_1000 MaxValue 最大值,
设置直线滑动电阻器的最大值, 当从最左端滑到最右端时, 读取的数值将会从最小值变化到最大值
</member>,
