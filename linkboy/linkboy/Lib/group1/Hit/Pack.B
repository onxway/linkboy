//[配置信息开始],
//[组件文件] Module.M,
//[语言列表] c chinese,
//[组件名称] Pack VibrationSensor 震动检测器,
//[模块型号] ,
//[资源占用] ,
//[实物图片] 1.png,2.png,
//[仿真类型] 1,
//[控件属性] 文字,字体,文字颜色,背景颜色,
//[元素子项] var_uint8 OS_EventFlag OS_EventFlag OS_EventFlag,
//[元素子项] event key0_press_event VibrationEvent 发生震动时,
//[元素子项] var_uint8 OS_time OS_time OS_time,
//[元素子项] function_void OS_init OS_init OS_init,
//[元素子项] function_void OS_run OS_run OS_run,
//[配置信息结束],

unit Pack
{
	#include "Pack\$language$.txt"
	driver =
	#include "driver\Pack.txt"
}
