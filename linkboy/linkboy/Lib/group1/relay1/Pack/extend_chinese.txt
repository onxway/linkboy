<module>
继电器模块，可通过指令控制继电器动作，例如执行“接通”指令，那么继电器就会打开吸合，“断开”指令会让继电器断开。
</module>
<name> Pack 继电器,
</name>
<member> interface_digital digital0 digital0,

</member>,
<member> function_void OS_init OS_init,

</member>,
<member> function_void open 接通,
让继电器接通
</member>,
<member> function_void close 断开,
让继电器断开
</member>,
<member> function_void swap 翻转,
让继电器翻转，当之前接通时，则断开；当之前断开时，则接通。
</member>,
