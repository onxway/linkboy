public link unit digital0 {} = driver.digital0;
public link void OS_init(){} = driver.OS_init;
public link void open(){} = driver.open;
public link void close(){} = driver.close;
public link void swap(){} = driver.swap;
