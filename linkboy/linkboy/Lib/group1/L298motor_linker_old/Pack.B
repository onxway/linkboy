//[配置信息开始],
//[组件文件] Module.M,
//[语言列表] c chinese,
//[组件名称] Pack MotorDriver 电机驱动板,
//[模块型号] L298,
//[资源占用] ,
//[实物图片] 1.png,2.png,
//[仿真类型] 1,
//[控件属性] 文字,字体,文字颜色,背景颜色,
//[元素子项] var_bool OS_time OS_time OS_time,
//[元素子项] function_void OS_init OS_init OS_init,
//[配置信息结束],

unit Pack
{
	#include "Pack\$language$.txt"
	driver =
	#include "driver\Pack.txt"
}
