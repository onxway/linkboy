
//AVR系列单片机的按键驱动程序

unit key
{
	public const uint16 ID = 0;

	public link unit D {}
	
	#include "$run$.txt"
	
	//[i] var_uint8 OS_EventFlag;
	uint8 OS_EventFlag;
	
	//[i] event key0_press_event;
	//[i] event key0_up_event;
	
	//---------------------
	//[i] var_bool key0_press;
	public bool key0_press;

	//---------------------
	//[i] var_bool key0_not_press;
	public bool key0_not_press;

	//---------------------
	//[i] var_uint8 OS_time;
	public uint8 OS_time;
	
	bit last_key0_press;
	
	//---------------------------------------------------
	//[i] function_void OS_init;
	public void OS_init()
	{
		KEY0_DIR = 0;
		KEY0_OUT = 1;
		
		OS_time = 1;
		OS_EventFlag = 0;
		last_key0_press = 0;

		key0_press = false;
		key0_not_press = true;
	}
	//---------------------------------------------------
	//[i] function_void OS_run;
	public void OS_run()
	{
		//一定要先读取到一个临时变量中!!!!
		//2015.9.14 22:41
		bit KEY0 = KEY0_IN;
		
		bit key0_down = KEY0 & ~last_key0_press;
		bit key0_up = ~KEY0 & last_key0_press;
		if( key0_down == 1 ) OS_EventFlag.0(bit) = 1;
		if( key0_up == 1 ) OS_EventFlag.1(bit) = 1;
		last_key0_press = KEY0;
		
		if( KEY0 == 1 ) {
			key0_press = true;
			key0_not_press = false;
		}
		else {
			key0_press = false;
			key0_not_press = true;
		}
	}
}




