<module>

</module>
<name> Pack WIFI热点,
</name>
<member> var_uint8 OS_EventFlag OS_EventFlag,

</member>,
<member> event join_event 客户端连接时,
创建热点后, 如果有其他的客户端连接到了这个热点上, 则触发一次此事件.
</member>,
<member> event leave_event 客户端断开时,
创建热点后, 如果有其他的客户端从这个热点上断开连接, 则触发一次此事件.
</member>,
<member> var_bool isConnect 客户端已连接,
创建热点后, 如果有其他的客户端连接到了这个热点上, 则此属性为真
</member>,
<member> function_void OS_init OS_init,

</member>,
<member> function_void OS_thread OS_thread,

</member>,
<member> function_void_Cstring_Cstring ESP32_wifi_softap 创建AP热点 # 密码为 #,
创建一个热点(无线路由器), 其他手机等设备可以搜索wifi并连接到此热点. 密码位数至少要8位字符. 密码为空的话表示开放式热点, 不需要输入密码即可连接.
</member>,
