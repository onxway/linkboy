	
	//----------------------------------------------
	//[i] function_void play_tone int32;
	public void play_tone( int32 t )
	{
		int16 n = (int16)t;
		uint16 data = tone_table[n];
		#.MEGA32.OCR1AH = data.8(uint8);
		#.MEGA32.OCR1AL = data.0(uint8);
		#.MEGA32.TIMSK | 0b0001_0000;
	}
	//----------------------------------------------
	//[i] function_void stop;
	public void stop()
	{
		SOUND_OUT = 0;
		#.MEGA32.TIMSK & 0b1110_1111;
	}
	//----------------------------------------------
	//定时器初始化
	//晶振 6MHz, 分频比为1, 计数脉冲频率为 6000000Hz
	//计数值为 23 * 256 + 112 = 6000
	//中断频率为 6000000Hz / 6000 = 1000Hz
	void timer_init()
	{
		//工作模式设置
		#.MEGA32.TCCR1A = 0;
		#.MEGA32.TCCR1B = 0b0000_1001;
		//定时间隔设置
		#.MEGA32.OCR1AH = 23;
		#.MEGA32.OCR1AL = 112;
		//使能中断
		//MCU TIMSK | 0b0001_0000;
		//清空中断
		#.MEGA32.TIFR | 0b0001_0000;
	}
	//----------------------------------------------
	//中断服务函数
	interrupt [#.MEGA32.WATCH.timer1_compA]
	void timer1_compA()
	{
		SOUND_OUT = ~SOUND_OUT;
	}
	