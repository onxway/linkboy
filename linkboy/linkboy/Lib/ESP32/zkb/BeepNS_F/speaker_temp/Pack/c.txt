public link unit analog {} = driver.analog;
public link unit tone1 {} = driver.tone1;
public link void OS_init(){} = driver.OS_init;
public link void PlayTone(bit n1){} = driver.play_tone;
public link void Stop(){} = driver.stop;
