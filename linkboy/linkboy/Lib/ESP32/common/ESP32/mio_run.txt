
#include <system\arduino\ESP32-io.txt>

PORT.UART_ID = UART_ID;

PORT.D2_DIR = A10.D0_DIR;
PORT.D2_IN = A10.D0_IN;
PORT.D2_OUT = A10.D0_OUT;
PORT.D2_PUL = A10.D0_PUL;

PORT.D3_DIR = A9.D0_DIR;
PORT.D3_IN = A9.D0_IN;
PORT.D3_OUT = A9.D0_OUT;
PORT.D3_PUL = A9.D0_PUL;

unit PORT
{
	public const int8 UART_ID = 0;
	public link bit D2_DIR; public link bit D2_IN; public link bit D2_OUT; public link bit D2_PUL;
	public link bit D3_DIR; public link bit D3_IN; public link bit D3_OUT; public link bit D3_PUL;
}

public unit A0
{
	//public const int8 AD_INDEX = 0;
	public const uint8 INDEX = 0;
	public #.pre.my_DIR bit D0_DIR = #addr INDEX;
	public #.pre.my_OUT bit D0_OUT = #addr INDEX;
	public #.pre.my_IN bit D0_IN = #addr INDEX;
	public #.pre.my_PUL bit D0_PUL = #addr INDEX;
}
public unit A1
{
	//public const int8 AD_INDEX = 1;
	public const uint8 INDEX = 1;
	public #.pre.my_DIR bit D0_DIR = #addr INDEX;
	public #.pre.my_OUT bit D0_OUT = #addr INDEX;
	public #.pre.my_IN bit D0_IN = #addr INDEX;
	public #.pre.my_PUL bit D0_PUL = #addr INDEX;
}
public unit A2
{
	//public const int8 AD_INDEX = 2;
	public const uint8 INDEX = 2;
	public #.pre.my_DIR bit D0_DIR = #addr INDEX;
	public #.pre.my_OUT bit D0_OUT = #addr INDEX;
	public #.pre.my_IN bit D0_IN = #addr INDEX;
	public #.pre.my_PUL bit D0_PUL = #addr INDEX;
}
public unit A3
{
	//public const int8 AD_INDEX = 3;
	public const uint8 INDEX = 3;
	public #.pre.my_DIR bit D0_DIR = #addr INDEX;
	public #.pre.my_OUT bit D0_OUT = #addr INDEX;
	public #.pre.my_IN bit D0_IN = #addr INDEX;
	public #.pre.my_PUL bit D0_PUL = #addr INDEX;
}
public unit A4
{
	//public const int8 AD_INDEX = 4;
	public const uint8 INDEX = 4;
	public #.pre.my_DIR bit D0_DIR = #addr INDEX;
	public #.pre.my_OUT bit D0_OUT = #addr INDEX;
	public #.pre.my_IN bit D0_IN = #addr INDEX;
	public #.pre.my_PUL bit D0_PUL = #addr INDEX;
}
public unit A5
{
	//public const int8 AD_INDEX = 5;
	public const uint8 INDEX = 5;
	public #.pre.my_DIR bit D0_DIR = #addr INDEX;
	public #.pre.my_OUT bit D0_OUT = #addr INDEX;
	public #.pre.my_IN bit D0_IN = #addr INDEX;
	public #.pre.my_PUL bit D0_PUL = #addr INDEX;
}
public unit A6
{
	//public const int8 AD_INDEX = 6;
	public const uint8 INDEX = 6;
	public #.pre.my_DIR bit D0_DIR = #addr INDEX;
	public #.pre.my_OUT bit D0_OUT = #addr INDEX;
	public #.pre.my_IN bit D0_IN = #addr INDEX;
	public #.pre.my_PUL bit D0_PUL = #addr INDEX;
}
public unit A7
{
	//public const int8 AD_INDEX = 7;
	public const uint8 INDEX = 7;
	public #.pre.my_DIR bit D0_DIR = #addr INDEX;
	public #.pre.my_OUT bit D0_OUT = #addr INDEX;
	public #.pre.my_IN bit D0_IN = #addr INDEX;
	public #.pre.my_PUL bit D0_PUL = #addr INDEX;
}
public unit A8
{
	public const uint8 INDEX = 8;
	public #.pre.my_DIR bit D0_DIR = #addr INDEX;
	public #.pre.my_OUT bit D0_OUT = #addr INDEX;
	public #.pre.my_IN bit D0_IN = #addr INDEX;
	public #.pre.my_PUL bit D0_PUL = #addr INDEX;
}
public unit A9
{
	public const uint8 INDEX = 9;
	public #.pre.my_DIR bit D0_DIR = #addr INDEX;
	public #.pre.my_OUT bit D0_OUT = #addr INDEX;
	public #.pre.my_IN bit D0_IN = #addr INDEX;
	public #.pre.my_PUL bit D0_PUL = #addr INDEX;
}
public unit A10
{
	public const uint8 INDEX = 10;
	public #.pre.my_DIR bit D0_DIR = #addr INDEX;
	public #.pre.my_OUT bit D0_OUT = #addr INDEX;
	public #.pre.my_IN bit D0_IN = #addr INDEX;
	public #.pre.my_PUL bit D0_PUL = #addr INDEX;
}
public unit A11
{
	public const uint8 INDEX = 11;
	public #.pre.my_DIR bit D0_DIR = #addr INDEX;
	public #.pre.my_OUT bit D0_OUT = #addr INDEX;
	public #.pre.my_IN bit D0_IN = #addr INDEX;
	public #.pre.my_PUL bit D0_PUL = #addr INDEX;
}
public unit A12
{
	public const uint8 INDEX = 12;
	public #.pre.my_DIR bit D0_DIR = #addr INDEX;
	public #.pre.my_OUT bit D0_OUT = #addr INDEX;
	public #.pre.my_IN bit D0_IN = #addr INDEX;
	public #.pre.my_PUL bit D0_PUL = #addr INDEX;
}
public unit A13
{
	public const uint8 INDEX = 13;
	public #.pre.my_DIR bit D0_DIR = #addr INDEX;
	public #.pre.my_OUT bit D0_OUT = #addr INDEX;
	public #.pre.my_IN bit D0_IN = #addr INDEX;
	public #.pre.my_PUL bit D0_PUL = #addr INDEX;
}
public unit A14
{
	public const uint8 INDEX = 14;
	public #.pre.my_DIR bit D0_DIR = #addr INDEX;
	public #.pre.my_OUT bit D0_OUT = #addr INDEX;
	public #.pre.my_IN bit D0_IN = #addr INDEX;
	public #.pre.my_PUL bit D0_PUL = #addr INDEX;
}
public unit A15
{
	public const uint8 INDEX = 15;
	public #.pre.my_DIR bit D0_DIR = #addr INDEX;
	public #.pre.my_OUT bit D0_OUT = #addr INDEX;
	public #.pre.my_IN bit D0_IN = #addr INDEX;
	public #.pre.my_PUL bit D0_PUL = #addr INDEX;
}
public unit A16
{
	public const uint8 INDEX = 16;
	public #.pre.my_DIR bit D0_DIR = #addr INDEX;
	public #.pre.my_OUT bit D0_OUT = #addr INDEX;
	public #.pre.my_IN bit D0_IN = #addr INDEX;
	public #.pre.my_PUL bit D0_PUL = #addr INDEX;
}
public unit A17
{
	public const uint8 INDEX = 17;
	public #.pre.my_DIR bit D0_DIR = #addr INDEX;
	public #.pre.my_OUT bit D0_OUT = #addr INDEX;
	public #.pre.my_IN bit D0_IN = #addr INDEX;
	public #.pre.my_PUL bit D0_PUL = #addr INDEX;
}
public unit A18
{
	public const uint8 INDEX = 18;
	public #.pre.my_DIR bit D0_DIR = #addr INDEX;
	public #.pre.my_OUT bit D0_OUT = #addr INDEX;
	public #.pre.my_IN bit D0_IN = #addr INDEX;
	public #.pre.my_PUL bit D0_PUL = #addr INDEX;
}
public unit A19
{
	public const uint8 INDEX = 19;
	public #.pre.my_DIR bit D0_DIR = #addr INDEX;
	public #.pre.my_OUT bit D0_OUT = #addr INDEX;
	public #.pre.my_IN bit D0_IN = #addr INDEX;
	public #.pre.my_PUL bit D0_PUL = #addr INDEX;
}
public unit A20
{
	public const uint8 INDEX = 20;
	public #.pre.my_DIR bit D0_DIR = #addr INDEX;
	public #.pre.my_OUT bit D0_OUT = #addr INDEX;
	public #.pre.my_IN bit D0_IN = #addr INDEX;
	public #.pre.my_PUL bit D0_PUL = #addr INDEX;
}
public unit A21
{
	public const uint8 INDEX = 21;
	public #.pre.my_DIR bit D0_DIR = #addr INDEX;
	public #.pre.my_OUT bit D0_OUT = #addr INDEX;
	public #.pre.my_IN bit D0_IN = #addr INDEX;
	public #.pre.my_PUL bit D0_PUL = #addr INDEX;
}
public unit A22
{
	public const uint8 INDEX = 22;
	public #.pre.my_DIR bit D0_DIR = #addr INDEX;
	public #.pre.my_OUT bit D0_OUT = #addr INDEX;
	public #.pre.my_IN bit D0_IN = #addr INDEX;
	public #.pre.my_PUL bit D0_PUL = #addr INDEX;
}
public unit A23
{
	public const uint8 INDEX = 23;
	public #.pre.my_DIR bit D0_DIR = #addr INDEX;
	public #.pre.my_OUT bit D0_OUT = #addr INDEX;
	public #.pre.my_IN bit D0_IN = #addr INDEX;
	public #.pre.my_PUL bit D0_PUL = #addr INDEX;
}
public unit A24
{
	public const uint8 INDEX = 24;
	public #.pre.my_DIR bit D0_DIR = #addr INDEX;
	public #.pre.my_OUT bit D0_OUT = #addr INDEX;
	public #.pre.my_IN bit D0_IN = #addr INDEX;
	public #.pre.my_PUL bit D0_PUL = #addr INDEX;
}
public unit A25
{
	public const uint8 INDEX = 25;
	public #.pre.my_DIR bit D0_DIR = #addr INDEX;
	public #.pre.my_OUT bit D0_OUT = #addr INDEX;
	public #.pre.my_IN bit D0_IN = #addr INDEX;
	public #.pre.my_PUL bit D0_PUL = #addr INDEX;
}
public unit A26
{
	public const uint8 INDEX = 26;
	public #.pre.my_DIR bit D0_DIR = #addr INDEX;
	public #.pre.my_OUT bit D0_OUT = #addr INDEX;
	public #.pre.my_IN bit D0_IN = #addr INDEX;
	public #.pre.my_PUL bit D0_PUL = #addr INDEX;
}
public unit A27
{
	public const uint8 INDEX = 27;
	public #.pre.my_DIR bit D0_DIR = #addr INDEX;
	public #.pre.my_OUT bit D0_OUT = #addr INDEX;
	public #.pre.my_IN bit D0_IN = #addr INDEX;
	public #.pre.my_PUL bit D0_PUL = #addr INDEX;
}
public unit A28
{
	public const uint8 INDEX = 28;
	public #.pre.my_DIR bit D0_DIR = #addr INDEX;
	public #.pre.my_OUT bit D0_OUT = #addr INDEX;
	public #.pre.my_IN bit D0_IN = #addr INDEX;
	public #.pre.my_PUL bit D0_PUL = #addr INDEX;
}
public unit A29
{
	public const uint8 INDEX = 29;
	public #.pre.my_DIR bit D0_DIR = #addr INDEX;
	public #.pre.my_OUT bit D0_OUT = #addr INDEX;
	public #.pre.my_IN bit D0_IN = #addr INDEX;
	public #.pre.my_PUL bit D0_PUL = #addr INDEX;
}
public unit A30
{
	public const uint8 INDEX = 30;
	public #.pre.my_DIR bit D0_DIR = #addr INDEX;
	public #.pre.my_OUT bit D0_OUT = #addr INDEX;
	public #.pre.my_IN bit D0_IN = #addr INDEX;
	public #.pre.my_PUL bit D0_PUL = #addr INDEX;
}
public unit A31
{
	public const uint8 INDEX = 31;
	public #.pre.my_DIR bit D0_DIR = #addr INDEX;
	public #.pre.my_OUT bit D0_OUT = #addr INDEX;
	public #.pre.my_IN bit D0_IN = #addr INDEX;
	public #.pre.my_PUL bit D0_PUL = #addr INDEX;
}
public unit A32
{
	public const uint8 INDEX = 32;
	public #.pre.my_DIR bit D0_DIR = #addr INDEX;
	public #.pre.my_OUT bit D0_OUT = #addr INDEX;
	public #.pre.my_IN bit D0_IN = #addr INDEX;
	public #.pre.my_PUL bit D0_PUL = #addr INDEX;
}
public unit A33
{
	public const uint8 INDEX = 33;
	public #.pre.my_DIR bit D0_DIR = #addr INDEX;
	public #.pre.my_OUT bit D0_OUT = #addr INDEX;
	public #.pre.my_IN bit D0_IN = #addr INDEX;
	public #.pre.my_PUL bit D0_PUL = #addr INDEX;
}
public unit A34
{
	public const uint8 INDEX = 34;
	public #.pre.my_DIR bit D0_DIR = #addr INDEX;
	public #.pre.my_OUT bit D0_OUT = #addr INDEX;
	public #.pre.my_IN bit D0_IN = #addr INDEX;
	public #.pre.my_PUL bit D0_PUL = #addr INDEX;
}
public unit A35
{
	public const uint8 INDEX = 35;
	public #.pre.my_DIR bit D0_DIR = #addr INDEX;
	public #.pre.my_OUT bit D0_OUT = #addr INDEX;
	public #.pre.my_IN bit D0_IN = #addr INDEX;
	public #.pre.my_PUL bit D0_PUL = #addr INDEX;
}
public unit A36
{
	public const uint8 INDEX = 36;
	public #.pre.my_DIR bit D0_DIR = #addr INDEX;
	public #.pre.my_OUT bit D0_OUT = #addr INDEX;
	public #.pre.my_IN bit D0_IN = #addr INDEX;
	public #.pre.my_PUL bit D0_PUL = #addr INDEX;
}
public unit A37
{
	public const uint8 INDEX = 37;
	public #.pre.my_DIR bit D0_DIR = #addr INDEX;
	public #.pre.my_OUT bit D0_OUT = #addr INDEX;
	public #.pre.my_IN bit D0_IN = #addr INDEX;
	public #.pre.my_PUL bit D0_PUL = #addr INDEX;
}
public unit A38
{
	public const uint8 INDEX = 38;
	public #.pre.my_DIR bit D0_DIR = #addr INDEX;
	public #.pre.my_OUT bit D0_OUT = #addr INDEX;
	public #.pre.my_IN bit D0_IN = #addr INDEX;
	public #.pre.my_PUL bit D0_PUL = #addr INDEX;
}
public unit A39
{
	public const uint8 INDEX = 39;
	public #.pre.my_DIR bit D0_DIR = #addr INDEX;
	public #.pre.my_OUT bit D0_OUT = #addr INDEX;
	public #.pre.my_IN bit D0_IN = #addr INDEX;
	public #.pre.my_PUL bit D0_PUL = #addr INDEX;
}
public unit A40
{
	public const uint8 INDEX = 40;
	public #.pre.my_DIR bit D0_DIR = #addr INDEX;
	public #.pre.my_OUT bit D0_OUT = #addr INDEX;
	public #.pre.my_IN bit D0_IN = #addr INDEX;
	public #.pre.my_PUL bit D0_PUL = #addr INDEX;
}
public unit A41
{
	public const uint8 INDEX = 41;
	public #.pre.my_DIR bit D0_DIR = #addr INDEX;
	public #.pre.my_OUT bit D0_OUT = #addr INDEX;
	public #.pre.my_IN bit D0_IN = #addr INDEX;
	public #.pre.my_PUL bit D0_PUL = #addr INDEX;
}
public unit A42
{
	public const uint8 INDEX = 42;
	public #.pre.my_DIR bit D0_DIR = #addr INDEX;
	public #.pre.my_OUT bit D0_OUT = #addr INDEX;
	public #.pre.my_IN bit D0_IN = #addr INDEX;
	public #.pre.my_PUL bit D0_PUL = #addr INDEX;
}
public unit A43
{
	public const uint8 INDEX = 43;
	public #.pre.my_DIR bit D0_DIR = #addr INDEX;
	public #.pre.my_OUT bit D0_OUT = #addr INDEX;
	public #.pre.my_IN bit D0_IN = #addr INDEX;
	public #.pre.my_PUL bit D0_PUL = #addr INDEX;
}
public unit A44
{
	public const uint8 INDEX = 44;
	public #.pre.my_DIR bit D0_DIR = #addr INDEX;
	public #.pre.my_OUT bit D0_OUT = #addr INDEX;
	public #.pre.my_IN bit D0_IN = #addr INDEX;
	public #.pre.my_PUL bit D0_PUL = #addr INDEX;
}
public unit A45
{
	public const uint8 INDEX = 45;
	public #.pre.my_DIR bit D0_DIR = #addr INDEX;
	public #.pre.my_OUT bit D0_OUT = #addr INDEX;
	public #.pre.my_IN bit D0_IN = #addr INDEX;
	public #.pre.my_PUL bit D0_PUL = #addr INDEX;
}
public unit A46
{
	public const uint8 INDEX = 46;
	public #.pre.my_DIR bit D0_DIR = #addr INDEX;
	public #.pre.my_OUT bit D0_OUT = #addr INDEX;
	public #.pre.my_IN bit D0_IN = #addr INDEX;
	public #.pre.my_PUL bit D0_PUL = #addr INDEX;
}
public unit A47
{
	public const uint8 INDEX = 47;
	public #.pre.my_DIR bit D0_DIR = #addr INDEX;
	public #.pre.my_OUT bit D0_OUT = #addr INDEX;
	public #.pre.my_IN bit D0_IN = #addr INDEX;
	public #.pre.my_PUL bit D0_PUL = #addr INDEX;
}



















