
unit Pack
{
	const int32 DEVICE = 303;
	interface[0x0100 * DEVICE + 0x00] void VEX_ESP32_TCP_Init( []uint8 buf, int32 n ) {}
	interface[0x0100 * DEVICE + 0x01] void VEX_ESP32_TCP_Client( []uint8 IP, int32 port ) {}
	interface[0x0100 * DEVICE + 0x02] bool VEX_ESP32_TCP_isConnected() {}
	interface[0x0100 * DEVICE + 0x03] void VEX_ESP32_TCP_SendString( []uint8 text ) {}
	interface[0x0100 * DEVICE + 0x04] int32 VEX_ESP32_TCP_GetReceiveLength() {}

	public const uint16 ID = 0;
	
	//[i] linkconst_int32_500 MaxLength;
	public const int32 MaxLength = 500;
	
	//[i] var_Cstring buffer;
	[MaxLength+1]uint8 buffer;
	
	//[i] var_int32 Length;
	public int32 Length;
	
	
	//[i] var_uint8 OS_EventFlag;
	public uint8 OS_EventFlag;
	
	//[i] var_bool isConnected;
	public bool isConnected;
	
	//[i] event TCP_connected_event;
	//[i] event TCP_not_connected_event;
	//[i] event TCP_receive_event;
	
	//---------------------------------------------------
	//[i] function_void OS_init;
	public void OS_init()
	{
		OS_EventFlag = 0;
		isConnected = false;
		VEX_ESP32_TCP_Init( buffer, MaxLength );
	}
	//[i] function_void OS_thread;
	public void OS_thread()
	{
		//连接状态处理
		bool c = VEX_ESP32_TCP_isConnected();
		if( c && !isConnected ) {
			OS_EventFlag.0(bit) = 1;
		}
		if( !c && isConnected ) {
			OS_EventFlag.1(bit) = 1;
		}
		isConnected = c;
		
		//数据接收处理
		int32 len = VEX_ESP32_TCP_GetReceiveLength();
		if( len > 0 ) {
			Length = len;
			//buffer[0] += '0';
			//buffer[1] += '0';
			OS_EventFlag.2(bit) = 1;
		}
	}
	//---------------------------------------------------
	//[i] function_void ESP32_TCP_Client Cstring int32;
	public void ESP32_TCP_Client( []uint8 IP, int32 port )
	{
		VEX_ESP32_TCP_Client( IP, port );
	}
	//---------------------------------------------------
	//[i] function_void ESP32_TCP_SendString Cstring;
	public void ESP32_TCP_SendString( []uint8 text )
	{
		VEX_ESP32_TCP_SendString( text );
	}
	//---------------------------------------------------
	//[i] function_int32 ESP32_TCP_GetData int32;
	public int32 ESP32_TCP_GetData( int32 i )
	{
		return (int)buffer[i - 1];
	}
}













