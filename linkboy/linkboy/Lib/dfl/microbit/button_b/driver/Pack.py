
$last = False
$按键按下 = False
$按键松开 = True

$OS_EventFlag = 0

def $OS_init():
	pass

def $OS_run():
	pass

def $OS_thread():
	global $OS_EventFlag, $last, $按键按下, $按键松开
	t = button_b.is_pressed()
	if t and (not $last):
		$OS_EventFlag |= 0b00000001
	if (not t) and $last:
		$OS_EventFlag |= 0b00000010
	$last = t
	$按键按下 = t
	$按键松开 = not t

