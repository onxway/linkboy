
//MEGA单片机组件
unit MEGA
{
	public const uint16 ID = 0;
	
	#include "mybit_$run$.txt"
	
	
	//---------------------------------------------------
	//[i] var_uint8 OS_time;
	public uint8 OS_time;
	
	//[i] var_uint8 OS_EventFlag;
	uint8 OS_EventFlag;
	//[i] event StartEvent;
	//[i] event IdleEvent;
	
	//---------------------------------------------------
	//[i] function_void OS_init;
	public void OS_init()
	{
		FlashOpen = false;
		FlashForever = false;
		FlashNumber = 0;
		swap = 0;
		
		OS_time = 50;
		OS_EventFlag = 0;
		OS_EventFlag.0(bit) = 1;
		
		//开机用于等待外设准备好(带有阻容复位的外设)
		loop( 10 ) {
			delay();
		}
		LED_DDR = 1;
		LED_PORT = 0;
	}
	//---------------------------------------------------
	//[i] function_void OS_thread;
	public void OS_thread()
	{
		OS_EventFlag.1(bit) = 1;
	}
	//---------------------------------------------------
	//[i] function_void OS_run;
	public void OS_run()
	{
		//闪烁进程
		if( !FlashOpen ) return;
		swap %= 10;
		bit OUT;
		if( swap < 1 ) {
			OUT = 1;
		}
		else {
			OUT = 0;
		}
		swap += 1;
		
		if( FlashForever ) {
			LED_PORT = OUT;
			return;
		}
		if( FlashNumber == 0 ) {
			FlashOpen = false;
			return;
		}
		if( LED_PORT == 1 && OUT == 0 ) {
			FlashNumber - 1;
		}
		LED_PORT = OUT;
	}
	
	//---------------------------------------------------
	//[i] function_void LightOpen;
	public void LightOpen()
	{
		FlashOpen = false;
		LED_PORT = 1;
	}
	//---------------------------------------------------
	//[i] function_void LightClose;
	public void LightClose()
	{
		FlashOpen = false;
		LED_PORT = 0;
	}
	//---------------------------------------------------
	//[i] function_void LightSwap;
	public void LightSwap()
	{
		FlashOpen = false;
		LED_PORT = ~LED_PORT;
	}
	//---------------------------------------------------
	//[i] function_void LightFlashTimes int32;
	public void LightFlashTimes( int32 n )
	{
		#asm "cli"
		FlashNumber = n;
		#asm "sei"
		swap = 0;
		FlashForever = false;
		FlashOpen = true;
	}
	//---------------------------------------------------
	//[i] function_void LightFlash;
	public void LightFlash()
	{
		#asm "cli"
		FlashNumber = 0;
		#asm "sei"
		FlashForever = true;
		FlashOpen = true;
	}
	//---------------------------------------------------
	void delay()
	{
		loop( 250 ) loop( 250 ) {}
	}
	
	uint8 swap;
	int32 FlashNumber;
	bool FlashOpen;
	bool FlashForever;
}

#include <Arduino\common\MEGA328\mio_$run$.txt>

//导出IO接口
#.COM_IO = #.this;







