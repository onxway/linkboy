

unit Pack
{
	public const uint16 ID = 0;

	//[i] var_uint8 OS_EventFlag;
	uint8 OS_EventFlag;
	//[i] event StartEvent;
	//[i] event IdleEvent;

	//[i] function_void OS_init;
	public void OS_init()
	{
		OS_EventFlag = 0;
		OS_EventFlag.0(bit) = 1;
		LED.D0_DIR = 1;
		close_led();
	}
	//[i] function_void OS_thread;
	public void OS_thread()
	{
		OS_EventFlag.1(bit) = 1;
	}
	//[i] function_void OS_ClearWatchDog;
	public void OS_ClearWatchDog()
	{
		
	}
	//---------------------------------------------------
	//[i] function_void open_led;
	public void open_led()
	{
		LED.D0_OUT = 0;
	}
	//---------------------------------------------------
	//[i] function_void close_led;
	public void close_led()
	{
		LED.D0_OUT = 1;
	}
	
	
	//=================================================================================
	
	memory my_bit { type = [ uint16 0, 65535 ] uint8; }
	
	void set_uint8( uint16 addr, uint8 b )
	{
		#.OS.REMO_DataChannelWrite( (int)addr, (int)b );
	}
	//---------------------------------------------------
	uint8 get_uint8( uint16 addr )
	{
		int32 d = #.OS.REMO_DataChannelRead( (int)addr );
		return (uint8)(uint16)(uint)d;
	}
	
	public memory VHdata { type = [ uint16 0, 65535 ] uint32; }
	void set_uint32( uint16 addr, uint32 b )
	{
		#.OS.REMO_DataChannelWrite( (int)addr, (int)b );
	}
	//---------------------------------------------------
	uint32 get_uint32( uint16 addr )
	{
		int32 d = #.OS.REMO_DataChannelRead( (int)addr );
		return (uint)d;
	}
	
	public link unit LED {}
}

#.USER0 = driver;

#include "mio_$run$.txt"







