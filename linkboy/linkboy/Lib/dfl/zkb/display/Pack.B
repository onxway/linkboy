//[配置信息开始],
//[组件文件] Module.M,
//[语言列表] c chinese,
//[组件名称] Pack Screen 屏幕,
//[模块型号] ,
//[资源占用] ,
//[py参数] ;py;,
//[仿真类型] 1,
//[元素子项] function_void OS_init OS_init OS_init,
//[元素子项] function_void show show 显示生效,
//[元素子项] function_void Clear Clear 显示清空,
//[元素子项] function_void Set Set 显示全亮,
//[元素子项] function_void_int32_int32_int32 pixel Pixel+#+#+# 设置OLED坐标x+#+y+#+显示像素+#,
//[元素子项] function_void_int32_int32_int32_int32 hline hline+#+#+#+# 水平线x+#+y+#+长度+#+像素+#,
//[元素子项] function_void_int32_int32_int32_int32 vline vline+#+#+#+# 垂直线x+#+y+#+长度+#+像素+#,
//[元素子项] function_void_int32_int32_int32_int32_int32 line line+#+#+#+#+# 线段x1+#+y1+#+到x2+#+y2+#+像素+#,
//[元素子项] function_void_int32_int32_int32_int32_int32 rect rect+#+#+#+#+# 空心矩形x+#+y+#+宽+#+高+#+像素+#,
//[元素子项] function_void_int32_int32_int32_int32_int32 fill_rect fill_rect+#+#+#+#+# 实心矩形x+#+y+#+宽+#+高+#+像素+#,
//[元素子项] function_void_int32_int32_int32_int32_int32_int32_int32 triangle triangle+#+#+#+#+#+#+# 空心三角x+#+y+#+x2+#+y2+#+x3+#+y3+#+像素+#,
//[元素子项] function_void_int32_int32_int32_int32 circle circle+#+#+#+# 空心圆x+#+y+#+半径+#+像素+#,
//[元素子项] function_void_int32_int32_int32_int32_bitmap_int32 bitmap bitmap+#+#+#+#+#+# 图像x+#+y+#+宽+#+高+#+点阵+#+像素+#,
//[配置信息结束],

unit Pack
{
	#include "Pack\$language$.txt"
	driver =
	#include "driver\Pack.txt"
}
