//[配置信息开始],
//[组件文件] module.M,
//[语言列表] c chinese,
//[组件名称] MEGA Controller 控制器,
//[模块型号] 型号-虚谷号,
//[资源占用] TIMER0,
//[实物图片] a1.jpg,
//[仿真类型] 2,
//[控件属性] 文字,字体,文字颜色,背景颜色,
//[元素子项] var_uint8 OS_time OS_time OS_time,
//[元素子项] var_uint8 OS_EventFlag OS_EventFlag OS_EventFlag,
//[元素子项] event StartEvent StartEvent 初始化,
//[元素子项] event IdleEvent IdleEvent 反复执行,
//[元素子项] function_void OS_init OS_init OS_init,
//[元素子项] function_void OS_thread OS_thread OS_thread,
//[元素子项] function_void OS_run OS_run OS_run,
//[元素子项] function_void OS_ClearWatchDog OS_ClearWatchDog OS_ClearWatchDog,
//[元素子项] function_void LightOpen LightOpen 指示灯点亮,
//[元素子项] function_void LightClose LightClose 指示灯熄灭,
//[元素子项] function_void LightSwap LightSwap 指示灯反转,
//[元素子项] function_void_int32 LightFlashTimes LightFlashTimes+# 指示灯闪烁+#+次,
//[元素子项] function_void LightFlash LightFlash 指示灯闪烁,
//[配置信息结束],

unit MEGA
{
	#include "MEGA\$language$.txt"
	driver =
	#include "driver\MEGA.txt"
}
