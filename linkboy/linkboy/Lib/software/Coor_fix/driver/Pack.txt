
unit Pack
{
	public const uint16 ID = 0;
	
	//[i] linkinterface_math math0;
	link unit math0 {}

	#include <system\common\math\bitwise_verification.txt>
	
	fix Angle;
	fix R;
	
	fix X;
	fix Y;
	
	//---------------------------------------------------
	//[i] function_void OS_init;
	public void OS_init()
	{
		
	}
	//---------------------------------------------------
	//[i] function_void SetCoor fix fix;
	public void SetCoor( fix x, fix y )
	{
		X = x;
		Y = y;
		fix r = X * X + Y * Y;
		R = math0.Sqrt( r );
		
		fix AX = +X;
		fix AY = +Y;
		
		bool swch = false;
		if( AY > AX ) {
			swch = true;
			AY = AX;
		}
		fix a = AY / R;
		
		Angle = math0.Arcsin( a );
		
		if( swch ) {
			Angle = math0.h_Pi - Angle;
		}
		if( X < 0s ) {
			Angle = math0.Pi - Angle;
		}
		if( Y < 0s ) {
			Angle = math0.D_Pi - Angle;
		}
	}
	///[i] function_fix getX;
	public fix getX()
	{
		return X;
	}
	///[i] function_fix getY;
	public fix getY()
	{
		return Y;
	}
	//---------------------------------------------------
	///[i] function_void SetAngleR fix fix;
	public void SetAngleR( fix a, fix r )
	{
		Angle = a;
		R = r;
		
		//这里需要计算更新坐标
	}
	//[i] function_fix getA;
	public fix getA()
	{
		return Angle;
	}
	//[i] function_fix getR;
	public fix getR()
	{
		return R;
	}
}













