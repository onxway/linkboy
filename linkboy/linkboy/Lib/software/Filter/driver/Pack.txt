
unit Filter
{
	public const uint16 ID = 0;
	
	//[i] linkconst_int32_3 Number;
	public const int32 Number = 0;
	
	[int32*Number] Buffer;
	int8 CurrentIndex;
	
	//---------------------------------------------------
	//[i] function_void OS_init;
	public void OS_init()
	{
		CurrentIndex = 0;
	}
	//---------------------------------------------------
	//[i] function_void add_data int32;
	public void add_data( int32 d )
	{
		Buffer[CurrentIndex] = d;
		CurrentIndex += 1;
		CurrentIndex %= (int8)(int16)Number;
	}
	//---------------------------------------------------
	//[i] function_int32 get_data;
	public int32 get_data()
	{
		int32 t = 0;
		int8 i = 0;
		loop( Number ) {
			t += Buffer[i];
			i += 1;
		}
		return t/Number;
	}
}






