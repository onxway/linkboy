<module>
定时器组件，当定时器计时到指定的时间间隔时会触发一个定时器时间到事件，然后继续从0开始计时直到下一次触发事件，如此反复。系统上电后，定时器默认是启动状态。
</module>
<name> timer 定时器,
</name>
<member> var_uint8 OS_EventFlag OS_EventFlag,

</member>,
<member> event TimeEvent 时间到时,
时间到事件，当定时器计时到指定的时间间隔时，触发此事件，然后从零开始继续计时。直到下一次再次触发此事件，如此反复。
</member>,
<member> var_uint8 OS_time OS_time,

</member>,
<member> linkconst_int32_1.0_秒 Time 定时时间,
设置定时器的时间间隔(单位:秒)
</member>,
<member> var_bool isRun 正在运行,
通过这个属性判断定时器是否正在计时工作中. 调用 "停止" 指令会是这个条件不成立; 调用 "开始" 指令会试这个条件成立.
</member>,
<member> function_void OS_init OS_init,

</member>,
<member> function_void OS_run OS_run,

</member>,
<member> function_void Stop 停止,
 "停止"指令让定时器停止工作, 内部计时点保持在当前的数值不变. 由于不再计时, 所以不会触发时间到事件, 直到调用"开始"指令之后才会继续从当前内部计时点继续计时.
</member>,
<member> function_void Start 开始,
 "开始"指令让定时器继续工作, 从内部计时点开始继续计时, 直到等于定时器的定时时间后触发"时间到事件", 并从零开始继续下一次计时.
</member>,
<member> function_void Clear 清零,
把定时器的内部计时点清零, 这样会把定时器的"时间到事件"往后推迟一小段时间. 因为把内部计时点清零, 导致定时器从零开始, 因此执行这个指令之后, 定时器需要再次计时一个完整的定时时间才会再次触发"时间到事件".
</member>,
<member> function_void Trig 触发时间到事件,
 执行"触发时间到事件"指令后会立刻触发一次"时间到事件", 但是不影响定时器的内部运行, 定时器从内部计时点继续计时. 例如一个间隔为10分钟的定时器, 假如计时到第7分钟时候, 调用"手动触发事件"指令, 那么系统会立刻执行一次这个定时器的"时间到事件", 而定时器则是从7分钟继续计时, 因此3分钟之后会再次触发一次"时间到事件", 再过10分钟还会触发一次, ... 如此反复.
</member>,
