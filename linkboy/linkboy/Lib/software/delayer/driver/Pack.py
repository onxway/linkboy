
$time = 0

def $OS_init():
	pass

def $OS_run():
	pass

def $OS_thread():
	pass

def $延时毫秒( t ):
	global $time
	$time = t // 10

def $延时秒( t ):
	$延时毫秒( t * 1000 )

def $延时分( t ):
	$延时秒( t * 60 )

def $延时小时( t ):
	$延时分( t * 60 )

def $延时天( t ):
	$延时小时( t * 24 )


