
	const uint16 MAX_LENGTH = 2048;
	
	link uint8 EECR = #.MEGA2560.EECR;
	
	public void set_uint8( uint16 addr, uint8 data)
	{
		while( #.MEGA2560.EECR.1(bit) == 1 ) {}
		#.MEGA2560.EEARH = addr.8(uint8);
		#.MEGA2560.EEARL = addr.0(uint8);
		#.MEGA2560.EEDR = data;
		#asm "cli"
		#asm "sbi &EECR-0x20,2"
		#asm "sbi &EECR-0x20,1"
		#asm "sei"
	}
	public uint8 get_uint8( uint16 addr)
	{
		while( #.MEGA2560.EECR.1(bit) == 1 ) {}
		#.MEGA2560.EEARH = addr.8(uint8);
		#.MEGA2560.EEARL = addr.0(uint8);
		#asm "sbi &EECR-0x20,0"
		return #.MEGA2560.EEDR;
	}




