public link bit Time = driver.count_time;
public link bit OS_time = driver.OS_time;
public link void OS_init(){} = driver.OS_init;
public link void Clear(){} = driver.clear;
public link void Begin(){} = driver.begin;
public link void Pause(){} = driver.pause;
public link void OS_run(){} = driver.OS_run;
