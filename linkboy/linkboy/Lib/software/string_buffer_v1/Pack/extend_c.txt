<module>

</module>
<name> Pack StringBuffer,
</name>
<member> linkconst_int32_50 MaxLength MaxLength,

</member>,
<member> var_Cstring buffer Value,

</member>,
<member> var_int32 Length Length,

</member>,
<member> function_void OS_init OS_init,

</member>,
<member> function_void Clear Clear,

</member>,
<member> function_int32_int32 get_char GetChar #,

</member>,
<member> function_bool_Cstring equal Equal #,

</member>,
<member> function_void_Cstring SetString SetString #,

</member>,
<member> function_void_Cstring AddString AddString #,

</member>,
<member> function_void_int32 AddCChar AddChar #,

</member>,
<member> split split split,

</member>,
<member> function_void_int32 AddNumber AddNumber #,

</member>,
<member> function_void_int32 AddNumberHex AddHexNumber #,

</member>,
<member> function_void_int32_int32 set_format PadLeftByChar # #,

</member>,
<member> function_void clear_format ClearPadLeft,

</member>,
<member> split split split,

</member>,
<member> function_int32 get_value GetValue,

</member>,
<member> function_int32 get_value_hex GetHexValue,

</member>,
