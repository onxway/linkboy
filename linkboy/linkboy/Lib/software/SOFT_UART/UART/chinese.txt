public const int32 波特率 = 0;
driver.baud = 波特率;
public link void OS_init(){} = driver.OS_init;
public link void 设置波特率为_(bit n1){} = driver.set_baud;
public link void 发送字符串_(bit n1){} = driver.print_string;
public link void 以字符串形式发送数字_(bit n1){} = driver.print_number;
public link void 发送字符_(bit n1){} = driver.print_char;
public link void 等待并接收一个数据(){} = driver.get_byte;
