<module>

</module>
<name> UART SerialPort,
</name>
<member> linkconst_int32_9600 baud Baudrate,

</member>,
<member> function_void OS_init OS_init,

</member>,
<member> function_void_int32 set_baud SetBaudrate #,

</member>,
<member> function_void_Cstring print_string PrintString #,

</member>,
<member> function_void_int32 print_number PrintNumber #,

</member>,
<member> function_void_int32 print_char PrintChar #,

</member>,
<member> function_int32 get_byte GetByte,

</member>,
