//[配置信息开始],
//[组件文件] count_down.M,
//[语言列表] c chinese,
//[组件名称] count_down CountDownTimer 倒计时器,
//[模块型号] ,
//[资源占用] ,
//[仿真类型] 1,
//[控件属性] 文字,字体,文字颜色,背景颜色,
//[元素子项] var_uint8 OS_EventFlag OS_EventFlag OS_EventFlag,
//[元素子项] event time_event TimeEvent 时间到时,
//[元素子项] var_uint8 OS_time OS_time OS_time,
//[元素子项] var_bool time_ready isReady 时间到,
//[元素子项] function_void OS_init OS_init OS_init,
//[元素子项] function_void_float set_time SetTime+# 经过+#+秒后触发,
//[元素子项] function_void_int32 set_mstime SetTimeMS+# 经过+#+毫秒后触发,
//[元素子项] function_void stop Stop 停止倒计时,
//[元素子项] function_void OS_run OS_run OS_run,
//[配置信息结束],

unit count_down
{
	#include "count_down\$language$.txt"
	driver =
	#include "driver\count_down.txt"
}
