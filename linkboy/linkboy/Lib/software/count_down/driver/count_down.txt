
unit count_down
{
	public const uint16 ID = 0;
	
	public link unit io {}
	
	int32 count_time;
	
	//[i] var_uint8 OS_EventFlag;
	public uint8 OS_EventFlag;
	//[i] event time_event;
	
	//[i] var_uint8 OS_time;
	public uint8 OS_time;
	
	//[i] var_bool time_ready;
	public bool time_ready;
	
	bool start_count;
	int32 tick;
	
	//------------------------------------------
	//[i] function_void OS_init;
	public void OS_init()
	{
		OS_time = 1;
		OS_EventFlag = 0;
		tick = 0;
		start_count = false;
	}
	//------------------------------------------
	//[i] function_void set_time float;
	public void set_time( fix t )
	{
		uint32 d = (uint)t;
		d = d * 1000 / 1024;
		
		count_time = (int)d;
		tick = 0;
		start_count = true;
		time_ready = false;
	}
	//------------------------------------------
	//[i] function_void set_mstime int32;
	public void set_mstime( int32 t )
	{
		count_time = t;
		tick = 0;
		start_count = true;
		time_ready = false;
	}
	//------------------------------------------
	//[i] function_void stop;
	public void stop()
	{
		start_count = false;
	}
	//------------------------------------------
	//[i] function_void OS_run;
	public void OS_run()
	{
		if( !start_count ) return;
		tick += #.SYS_Tick;
		if( tick >= count_time ) {
			OS_EventFlag.0(bit) = 1;
			start_count = false;
			time_ready = true;
		}
		#include "$run$.txt"
	}
}

