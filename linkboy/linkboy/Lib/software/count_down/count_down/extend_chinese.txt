<module>
倒计时器组件, 用户可以设置一个延时时间, 并触发倒计时器工作, 当时间到达后会触发 "时间到事件". 注意 "倒计时器组件" 是单次触发的, 每次倒计时为零并执行 "时间到事件" 之后, 倒计时器自动停止, 如果需要启动下一次倒计时, 需要再次调用触发指令.
</module>
<name> count_down 倒计时器,
</name>
<member> var_uint8 OS_EventFlag OS_EventFlag,

</member>,
<member> event time_event 时间到时,
时间到事件, 当定时器计时到指定的时间时, 触发此事件. 注意此事件每次只会触发一次, 之后倒计时器就会处于休眠状态, 除非用户再次调用设置延时时间指令让倒计时器开始一个新的倒计时过程.
</member>,
<member> var_uint8 OS_time OS_time,

</member>,
<member> var_bool time_ready 时间到,
如果设置的延时时间达到之后，此标志位成立
</member>,
<member> function_void OS_init OS_init,

</member>,
<member> function_void_float set_time 经过 # 秒后触发,
设置倒计时器的倒计时时间，并立即开始倒计时, 时间单位是秒. 如果正在倒计时, 则此指令会更新倒计时的时间, 从最后一次设置的时间开始倒计时.
</member>,
<member> function_void_int32 set_mstime 经过 # 毫秒后触发,
设置倒计时器的倒计时时间，并立即开始倒计时, 时间单位是毫秒. 如果正在倒计时, 则此指令会更新倒计时的时间, 从最后一次设置的时间开始倒计时.
</member>,
<member> function_void stop 停止倒计时,
本指令可以停止倒计时的过程, 停止后将不再倒计时, 也不会触发"时间到"事件
</member>,
<member> function_void OS_run OS_run,

</member>,
