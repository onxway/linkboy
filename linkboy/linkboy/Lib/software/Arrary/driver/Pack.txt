
unit Filter
{
	public const uint16 ID = 0;
	
	link memory Mem {} = #.base;
	
	//[i] linkconst_int32_30 Number;
	public const int32 Number = 0;
	
	[Mem int32*Number] Buffer;
	
	//---------------------------------------------------
	//[i] function_void OS_init;
	public void OS_init()
	{
		//CurrentIndex = 0;
	}
	//---------------------------------------------------
	//[i] function_void set_data int32 int32;
	public void set_data( int32 i, int32 d )
	{
		i -= 1;
		if( i < 0 || i >= Number ) {
			return;
		}
		Buffer[(uint16)(uint)i] = d;
	}
	//---------------------------------------------------
	//[i] function_int32 get_data int32;
	public int32 get_data( int32 i )
	{
		i -= 1;
		if( i < 0 || i >= Number ) {
			return 0;
		}
		return Buffer[(uint16)(uint)i];
	}
}






