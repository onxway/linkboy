//[配置信息开始],
//[组件文件] Module.M,
//[语言列表] c chinese,
//[组件名称] Pack Map 数据映射器,
//[模块型号] ,
//[资源占用] ,
//[实物图片] a1.jpg,
//[仿真类型] 1,
//[控件属性] 文字,字体,文字颜色,背景颜色,
//[元素子项] linkconst_int32_0 I_Min InputMin 输入最小值,
//[元素子项] linkconst_int32_1000 I_Max InputMax 输入最大值,
//[元素子项] linkconst_int32_0 O_Min OutputMin 输出最小值,
//[元素子项] linkconst_int32_100 O_Max OutputMax 输出最大值,
//[元素子项] function_void OS_init OS_init OS_init,
//[元素子项] function_int32_int32 get_data GetLimitedData+# 计算+#+的有限制映射值,
//[元素子项] function_int32_int32 get_data1 GetUnlimitedData+# 计算+#+的无限制映射值,
//[配置信息结束],

unit Pack
{
	#include "Pack\$language$.txt"
	driver =
	#include "driver\Pack.txt"
}
