
unit random
{
	public const uint16 ID = 0;
	
	//[i] linkconst_int32_0 MinValue;
	public const int32 MinValue = 0;
	//[i] linkconst_int32_100 MaxValue;
	public const int32 MaxValue = 0;
	
	int32 v_MinValue;
	int32 v_MaxValue;
	
	int32 cidx;
	uint32 num;
	
	uint32 seed;
	int32 tick;
	
	[]#.code uint16 plist = {
		10007,11059,12323,13697,14767,15773,16903,17389,18427,19319,
		20107,21773,22751,23417,24799,25541,26723,27733,28759,29851,
		30197,31469,32789,33857,34913,35729,36653,37379,38237,39901,
		40583,41539,42017,43517,44887,45523,46691,47057,48247,49537,
	};
	
	const uint8 MAX_TIME = 8;
	
	[MAX_TIME]uint16 pidx;
	
	//---------------------------------------------------
	//[i] function_void OS_init;
	public void OS_init()
	{
		SetArea( MinValue, MaxValue );
		
		num = (uint)( MaxValue - MinValue + 1 );
		set_seed( 0 );
	}
	//---------------------------------------------------
	//[i] function_void OS_thread;
	public void OS_thread()
	{
		tick += 1;
	}
	//---------------------------------------------------
	//[i] function_void set_seed int32;
	public void set_seed( int32 sd )
	{
		sd += (int)#.OS0.tick;
		sd += tick;
		
		seed += 45677 * ~(uint)sd + 123456789;
		seed ^= 0xAAAAAAAA;
		
		uint16 i = (uint16)seed & 0x1F;
		uint32 j = seed >> 5;
		
		for( uint8 n = 0; n < MAX_TIME; n += 1 ) {
			pidx[n] = (i + (uint16)(j&0x07)) % 40; i += 1; j >>= 3;
		}
	}
	//---------------------------------------------------
	//[i] function_int32 data;
	public int32 data()
	{
		cidx += 1;
		if( cidx > v_MaxValue ) {
			cidx = v_MinValue;
		}
		uint32 data = (uint)(cidx - v_MinValue);
		
		for( uint8 i = 0; i < MAX_TIME; i += 2 ) {
			
			uint32 p1 = plist[pidx[i]];
			data = data_once( data, p1, 4 );
			
			p1 = plist[pidx[i+1]];
			data = data_once( data, p1, 5 );
		}
		
		cdata = v_MinValue + (int)data;
		
		return cdata;
	}
	
	//[i] var_int32 cdata;
	int32 cdata;
	
	//---------------------------------------------------
	//[i] function_void SetArea int32 int32;
	public void SetArea( int32 min, int32 max )
	{
		v_MinValue = min;
		v_MaxValue = max;
		num = (uint)( v_MaxValue - v_MinValue + 1 );
		cidx = v_MinValue;
	}
	
	//---------------------------------------------------
	uint32 data_once( uint32 data, uint32 p, uint32 block )
	{
		data = (p * data) % num;
		
		//return data;
		
		uint32 n = num - num % block;
		
		if( data < n / block ) {
			return data + n * 2 / block;
		}
		else if( data < n * 2 / block ) {
			return data;
		}
		else if( data < n * 3 / block ) {
			return data - n * 2 / block;
		}
		else {
			return data;
		}
		//语法合规
		return 0;
	}
}






















