
操作说明:
linkboy软件自带了一些第三方模块库, 进入 "隐藏的第三方模块库" 将需要的模块库文件夹
剪切到本目录下并重新打开linkboy即可看到.
如果没有需要的模块库, 请联系第三方厂商索取.


----------------------------------------------------------------

第三方厂商注意:
不要修改第三方厂商硬件库文件夹名字. 修改之后, 打开某些程序文件时可能提示找不到指定器件;
一旦定好库文件夹名字, 也不要在以后升级时轻易修改名字!
因用户用到你公司硬件, 程序文件中将含有库文件夹名字标记, 改名会导致找不到相关器件

强烈建议厂家自己的硬件库文件夹用 "公司名+产品名" 命名, 防止和其他厂家冲突,
不允许用类似: lib, arduino, model, mydevice 等通用名字命名厂家自己的硬件库文件夹!

如果有第三方厂商添加模块到linkboy中遇到问题或者需要请我们帮忙添加，
可以到官网联系我们或者邮箱 910360201@qq.com




