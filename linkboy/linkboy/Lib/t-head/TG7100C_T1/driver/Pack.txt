

unit Pack
{
	public const uint16 ID = 0;

	//[i] var_uint8 OS_EventFlag;
	uint8 OS_EventFlag;
	//[i] event StartEvent;
	//[i] event IdleEvent;

	//[i] function_void OS_init;
	public void OS_init()
	{
		OS_EventFlag = 0;
		OS_EventFlag.0(bit) = 1;
		LED1.D0_DIR = 1;
		LED2.D0_DIR = 1;
		close_led1();
		close_led2();
	}
	//[i] function_void OS_thread;
	public void OS_thread()
	{
		OS_EventFlag.1(bit) = 1;
	}
	//[i] function_void OS_ClearWatchDog;
	public void OS_ClearWatchDog()
	{
		
	}
	//---------------------------------------------------
	//[i] function_void open_led1;
	public void open_led1()
	{
		LED1.D0_OUT = 1;
	}
	//---------------------------------------------------
	//[i] function_void close_led1;
	public void close_led1()
	{
		LED1.D0_OUT = 0;
	}
	//---------------------------------------------------
	//[i] function_void swap_led1;
	public void swap_led1()
	{
		LED1.D0_OUT = ~LED1.D0_OUT;
	}
	//---------------------------------------------------
	//[i] function_void open_led2;
	public void open_led2()
	{
		LED2.D0_OUT = 1;
	}
	//---------------------------------------------------
	//[i] function_void close_led2;
	public void close_led2()
	{
		LED2.D0_OUT = 0;
	}
	//---------------------------------------------------
	//[i] function_void swap_led2;
	public void swap_led2()
	{
		LED2.D0_OUT = ~LED2.D0_OUT;
	}
	
	//=================================================================================
	
	memory my_bit { type = [ uint16 0, 65535 ] uint8; }
	
	void set_uint8( uint16 addr, uint8 b )
	{
		#.OS.REMO_DataChannelWrite( (int)addr, (int)b );
	}
	//---------------------------------------------------
	uint8 get_uint8( uint16 addr )
	{
		int32 d = #.OS.REMO_DataChannelRead( (int)addr );
		return (uint8)(uint16)(uint)d;
	}
	
	public memory VHdata { type = [ uint16 0, 65535 ] uint32; }
	void set_uint32( uint16 addr, uint32 b )
	{
		#.OS.REMO_DataChannelWrite( (int)addr, (int)b );
	}
	//---------------------------------------------------
	uint32 get_uint32( uint16 addr )
	{
		int32 d = #.OS.REMO_DataChannelRead( (int)addr );
		return (uint)d;
	}
	
	public link unit LED1 {}
	public link unit LED2 {}
}

#.USER0 = driver;

driver.LED1 = D0;
driver.LED2 = D1;

#.COM_IO = #.this;
#include <vos_mcu\common\pin\mio_run.txt>







