<module>
GD32ARM内核开发板，芯片型号为GD32F303CGT6，此组件是基本组件, 主板上有一个红色的指示灯(连接到B15针脚), 用户可以通过指令控制小灯点亮, 熄灭, 或者闪烁. 另外还有一个事件触发源: 系统启动事件, 当板子开始通电后就会自动触发此事件, 用户可以在这个事件处理代码中做一些初始化工作. 
</module>
<name> Pack 控制器,
</name>
<member> var_uint8 OS_EventFlag OS_EventFlag,

</member>,
<member> event StartEvent 初始化,
初始化事件, 当板子开始通电后就会自动触发此事件, 用户可以在这个事件处理代码中做一些初始化工作. 无论任何时候, 系统都会保证这个事件是先于其他任何事件执行, 并且只会执行一次(每次系统上电). 另外在这个事件中可以调用延时, 这时会切换到其他事件, 直到时间到达才会回来. 因此如果在这个事件中带有一些初始化代码, 尽量在调用延时指令之前把所有的初始化都做好.
</member>,
<member> event IdleEvent 反复执行,
系统空闲事件(反复执行)，这个事件会反复的执行，一遍又一遍。可用于屏幕显示参数，反复刷新等。
</member>,
<member> function_void OS_init OS_init,

</member>,
<member> function_void OS_thread OS_thread,

</member>,
<member> function_void OS_ClearWatchDog OS_ClearWatchDog,

</member>,
<member> function_void open_led 指示灯点亮,
此指令让板子上的指示灯点亮
</member>,
<member> function_void close_led 指示灯熄灭,
此指令让板子上的指示灯熄灭
</member>,
<member> function_void swap_led 指示灯反转,
此指令让板子上的指示灯反转, 当之前指示灯点亮时, 那么此指令让指示灯熄灭; 当之前指示灯熄灭时, 此指令让指示灯点亮.
</member>,
