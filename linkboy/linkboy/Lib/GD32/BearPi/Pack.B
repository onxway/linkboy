//[配置信息开始],
//[组件文件] Module.M,
//[语言列表] c chinese,
//[组件名称] Pack MainBoard 控制器,
//[模块型号] GD32F303RGT6,
//[资源占用] ,
//[实物图片] a1.jpg,a2.jpg,
//[仿真类型] 1,
//[元素子项] var_uint8 OS_EventFlag OS_EventFlag OS_EventFlag,
//[元素子项] event StartEvent StartEvent 初始化,
//[元素子项] event IdleEvent IdleEvent 反复执行,
//[元素子项] function_void OS_init OS_init OS_init,
//[元素子项] function_void OS_thread OS_thread OS_thread,
//[元素子项] function_void OS_ClearWatchDog OS_ClearWatchDog OS_ClearWatchDog,
//[元素子项] function_void open_led OpenLed 指示灯点亮,
//[元素子项] function_void close_led CloseLed 指示灯熄灭,
//[元素子项] function_void swap_led ToggleLed 指示灯反转,
//[配置信息结束],

unit Pack
{
	#include "Pack\$language$.txt"
	driver =
	#include "driver\Pack.txt"
}
