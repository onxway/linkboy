public link bit OS_EventFlag = driver.OS_EventFlag;
public link void OS_init(){} = driver.OS_init;
public link void OS_thread(){} = driver.OS_thread;
public link void OS_ClearWatchDog(){} = driver.OS_ClearWatchDog;
public link void OpenLed(){} = driver.open_led;
public link void CloseLed(){} = driver.close_led;
public link void ToggleLed(){} = driver.swap_led;
