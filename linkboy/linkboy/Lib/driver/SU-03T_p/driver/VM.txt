
const int32 DEVICE = 25;

interface[0x0100 * DEVICE + 0x00] void vos_SetPin( uint8 pin ) {}
interface[0x0100 * DEVICE + 0x01] uint16 vos_GetValue() {}

void device_init()
{
	vos_SetPin( DATA.INDEX );
}
void OS_thread()
{
	uint16 val = vos_GetValue();
	
	//#.N = (int)val;
	
	value = (int)( (val + 5) / 10 ) - 1;
	
	if( value < 0 ) {
		return;
	}
	if( value == 0 ) {
		OS_EventFlag.0(bit) = 1;
	}
	else {
		OS_EventFlag.1(bit) = 1;
	}
}


