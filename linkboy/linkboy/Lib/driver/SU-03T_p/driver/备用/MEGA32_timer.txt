
void TimerInit()
{
	//工作模式设置 普通模式 晶振:11.0592MHz
	//计数器累加到0xFFFF后从零开始继续累加
	//分频比为64
	#.MEGA32.TCCR1A = 0;
	#.MEGA32.TCCR1B = 0b0000_0011;
	//关闭中断
	#.MEGA32.TIMSK & 0B1110_1111;
	//清空中断
	#.MEGA32.TIFR = 0b0001_0000;
}

#define MACRO_CLEAR_TIMER() #.MEGA32.TCNT1H = 0;#.MEGA32.TCNT1L = 0
#define MACRO_GET_TIMER() v.0(uint8) = #.MEGA32.TCNT1L; v.8(uint8) = #.MEGA32.TCNT1H


