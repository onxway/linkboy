//[配置信息开始],
//[语言列表] c chinese,
//[组件名称] Pack TrackingSensor 循迹传感器,
//[模块型号] ,
//[资源占用] ,
//[仿真类型] 1,
//[控件属性] 文字,字体,文字颜色,背景颜色,
//[元素子项] var_uint8 OS_EventFlag OS_EventFlag OS_EventFlag,
//[元素子项] event key0_press_event BlackTurnsWhiteEvent 黑色变为白色时,
//[元素子项] event key0_up_event WhiteTurnsBlackEvent 白色变为黑色时,
//[元素子项] var_bool key0_press WhiteDetected 检测到白色,
//[元素子项] var_bool key0_notpress BlackDetected 检测到黑色,
//[元素子项] var_uint8 OS_time OS_time OS_time,
//[元素子项] function_void OS_init OS_init OS_init,
//[元素子项] function_void OS_run OS_run OS_run,
//[配置信息结束],

unit Pack
{
	#include "Pack\$language$.txt"
	driver =
	#include "driver\Pack.txt"
}
