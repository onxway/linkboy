
unit ADbasic
{
	public const uint16 ID = 0;

	public link unit SW {}
	
	public link unit PX {}
	//const uint8 channelX = PX.INDEX;
	public link unit PY {}
	//const uint8 channelY = PY.INDEX;
	
	public const int32 MinValue = 0;
	public const int32 MaxValue = 1000;
	
	public void OS_init()
	{
		#.OS.REMO_ModuleWrite( (int)(ID * 0x00010000 + 0x0000), 500 );
		#.OS.REMO_ModuleWrite( (int)(ID * 0x00010000 + 0x0001), 500 );
	}
	//---------------------------------------------------
	//[i] function_int32 get_dataX;
	public int32 get_dataX()
	{
		return MinValue + #.OS.REMO_ModuleRead( (int)(ID * 0x00010000 + 0x0000) ) * (MaxValue - MinValue)/1024;
	}
	//---------------------------------------------------
	//[i] function_int32 get_dataY;
	public int32 get_dataY()
	{
		return MinValue + #.OS.REMO_ModuleRead( (int)(ID * 0x00010000 + 0x0001) ) * (MaxValue - MinValue)/1024;
	}
}





