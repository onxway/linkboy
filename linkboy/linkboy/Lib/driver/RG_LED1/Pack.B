//[配置信息开始],
//[语言列表] english_Pascal chinese,
//[组件名称] Pack LED 双色灯,
//[模块型号] 共阴极(高电平亮),
//[资源占用] ,
//[仿真类型] 1,
//[元素子项] interface_led ledR ledR ledR,
//[元素子项] interface_led ledG ledG ledG,
//[元素子项] function_void OS_init OS_init OS_init,
//[元素子项] function_void open_red OpenRed 点亮红灯,
//[元素子项] function_void close_red CloseRed 熄灭红灯,
//[元素子项] function_void open_green OpenGreen 点亮绿灯,
//[元素子项] function_void close_green CloseGreen 熄灭绿灯,
//[配置信息结束],

unit Pack
{
	#include "Pack\$language$.txt"
	driver =
	#include "driver\Pack.txt"
}
