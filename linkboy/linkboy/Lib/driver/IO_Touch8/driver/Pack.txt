
//AVR系列单片机的按键驱动程序

unit button
{
	public const uint16 ID = 0;

	public link unit OUT1 {}
	public link unit OUT2 {}
	public link unit OUT3 {}
	public link unit OUT4 {}
	public link unit OUT5 {}
	public link unit OUT6 {}
	public link unit OUT7 {}
	public link unit OUT8 {}

	public link bit KEY0_DIR = OUT1.D0_DIR;
	public link bit KEY0_IN = OUT1.D0_IN;
	public link bit KEY0_OUT = OUT1.D0_OUT;
	public link bit KEY1_DIR = OUT2.D0_DIR;
	public link bit KEY1_IN = OUT2.D0_IN;
	public link bit KEY1_OUT = OUT2.D0_OUT;
	public link bit KEY2_DIR = OUT3.D0_DIR;
	public link bit KEY2_IN = OUT3.D0_IN;
	public link bit KEY2_OUT = OUT3.D0_OUT;
	public link bit KEY3_DIR = OUT4.D0_DIR;
	public link bit KEY3_IN = OUT4.D0_IN;
	public link bit KEY3_OUT = OUT4.D0_OUT;
	public link bit KEY4_DIR = OUT5.D0_DIR;
	public link bit KEY4_IN = OUT5.D0_IN;
	public link bit KEY4_OUT = OUT5.D0_OUT;
	public link bit KEY5_DIR = OUT6.D0_DIR;
	public link bit KEY5_IN = OUT6.D0_IN;
	public link bit KEY5_OUT = OUT6.D0_OUT;
	public link bit KEY6_DIR = OUT7.D0_DIR;
	public link bit KEY6_IN = OUT7.D0_IN;
	public link bit KEY6_OUT = OUT7.D0_OUT;
	public link bit KEY7_DIR = OUT8.D0_DIR;
	public link bit KEY7_IN = OUT8.D0_IN;
	public link bit KEY7_OUT = OUT8.D0_OUT;

	//------------------------------------------------------------
	//[i] var_uint8 OS_EventFlag;
	public uint32 OS_EventFlag;
	
	//[i] event key0_press_event;
	//[i] event key0_up_event;
	
	//[i] event key1_press_event;
	//[i] event key1_up_event;
	
	//[i] event key2_press_event;
	//[i] event key2_up_event;
	
	//[i] event key3_press_event;
	//[i] event key3_up_event;

	//[i] event key4_press_event;
	//[i] event key4_up_event;
	
	//[i] event key5_press_event;
	//[i] event key5_up_event;
	
	//[i] event key6_press_event;
	//[i] event key6_up_event;
	
	//[i] event key7_press_event;
	//[i] event key7_up_event;
	
	//[i] event key_press_event;
	//[i] event key_up_event;

	//---------------------
	//[i] var_bool OS_time;
	public uint8 OS_time;
	
	//---------------------
	//[i] var_int32 Value;
	public int32 Value;

	uint8 last_key_press;
	
	//------------------------------------------------------------
	//[i] function_void OS_init;
	public void OS_init()
	{
		#include "$run$.txt"

		OS_time = 50;
		OS_EventFlag = 0;
		Value = 0;
		KEY0_DIR = 0;
		KEY0_OUT = 1;
		KEY1_DIR = 0;
		KEY1_OUT = 1;
		KEY2_DIR = 0;
		KEY2_OUT = 1;
		KEY3_DIR = 0;
		KEY3_OUT = 1;
		KEY4_DIR = 0;
		KEY4_OUT = 1;
		KEY5_DIR = 0;
		KEY5_OUT = 1;
		KEY6_DIR = 0;
		KEY6_OUT = 1;
		KEY7_DIR = 0;
		KEY7_OUT = 1;
		
		last_key_press = 0xFF;
	}
	//------------------------------------------------------------
	//[i] function_void OS_run;
	public void OS_run()
	{
		//按键0处理
		//一定要先读取到一个临时变量中!!!!
		//2015.9.14 22:41
		uint8 KeyData;
		
		KeyData.0(bit) = KEY0_IN;
		KeyData.1(bit) = KEY1_IN;
		KeyData.2(bit) = KEY2_IN;
		KeyData.3(bit) = KEY3_IN;
		KeyData.4(bit) = KEY4_IN;
		KeyData.5(bit) = KEY5_IN;
		KeyData.6(bit) = KEY6_IN;
		KeyData.7(bit) = KEY7_IN;
		KeyData = ~KeyData;
		uint8 tKeyValue = KeyData;
		uint16 Event = 0x01;
		int8 i = 0;
		loop( 8 ) {
			bit key_down = ~tKeyValue.0(bit) & last_key_press.0(bit);
			bit key_up = tKeyValue.0(bit) & ~last_key_press.0(bit);
			if( key_down == 1 ) {
				OS_EventFlag | Event;
				OS_EventFlag.16(bit) = 1;
				Value = i + 1;
			}
			Event <<= 1;
			if( key_up == 1 ) {
				OS_EventFlag | Event;
				OS_EventFlag.17(bit) = 1;
			}
			Event <<= 1;
			
			tKeyValue >>= 1;
			last_key_press >>= 1;
			i += 1;
		}
		last_key_press = KeyData;
	}
}





