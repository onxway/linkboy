//[配置信息开始],
//[语言列表] c chinese,
//[组件名称] Pack Key 八位电容按键,
//[模块型号] ,
//[资源占用] ,
//[仿真类型] 1,
//[控件属性] 文字,字体,文字颜色,背景颜色,
//[元素子项] var_uint8 OS_EventFlag OS_EventFlag OS_EventFlag,
//[元素子项] event key0_press_event Key1DownEvent 触点1按下时,
//[元素子项] event key0_up_event Key1UpEvent 触点1松开时,
//[元素子项] event key1_press_event Key2DownEvent 触点2按下时,
//[元素子项] event key1_up_event Key2UpEvent 触点2松开时,
//[元素子项] event key2_press_event Key3DownEvent 触点3按下时,
//[元素子项] event key2_up_event Key3UpEvent 触点3松开时,
//[元素子项] event key3_press_event Key4DownEvent 触点4按下时,
//[元素子项] event key3_up_event Key4UpEvent 触点4松开时,
//[元素子项] event key4_press_event Key5DownEvent 触点5按下时,
//[元素子项] event key4_up_event Key5UpEvent 触点5松开时,
//[元素子项] event key5_press_event Key6DownEvent 触点6按下时,
//[元素子项] event key5_up_event Key6UpEvent 触点6松开时,
//[元素子项] event key6_press_event Key7DownEvent 触点7按下时,
//[元素子项] event key6_up_event Key7UpEvent 触点7松开时,
//[元素子项] event key7_press_event Key8DownEvent 触点8按下时,
//[元素子项] event key7_up_event Key8UpEvent 触点8松开时,
//[元素子项] event key_press_event AnyKeyDownEvent 任意触点按下时,
//[元素子项] event key_up_event AnyKeyUpEvent 任意触点松开时,
//[元素子项] var_bool OS_time OS_time OS_time,
//[元素子项] var_int32 Value Value 按键值,
//[元素子项] function_void OS_init OS_init OS_init,
//[元素子项] function_void OS_run OS_run OS_run,
//[配置信息结束],

unit Pack
{
	#include "Pack\$language$.txt"
	driver =
	#include "driver\Pack.txt"
}
