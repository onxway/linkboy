//[配置信息开始],
//[语言列表] c chinese,
//[组件名称] Pack PiezoelectricSensor 压电传感器,
//[模块型号] 输出0/1数字量,
//[资源占用] ,
//[仿真类型] 1,
//[元素子项] var_uint8 OS_EventFlag OS_EventFlag OS_EventFlag,
//[元素子项] event key0_press_event knockTrigEvent 敲击震动触发时,
//[元素子项] var_bool key0_press knockTrig 有敲击震动,
//[元素子项] var_uint8 OS_time OS_time OS_time,
//[元素子项] function_void OS_init OS_init OS_init,
//[元素子项] function_void OS_run OS_run OS_run,
//[配置信息结束],

unit Pack
{
	#include "Pack\$language$.txt"
	driver =
	#include "driver\Pack.txt"
}
