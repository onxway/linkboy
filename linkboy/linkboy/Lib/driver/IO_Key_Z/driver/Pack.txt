
//AVR系列单片机的按键驱动程序

unit key
{
	public const uint16 ID = 0;
	
	public link unit OUT {}
	
	//[i] var_uint8 OS_EventFlag;
	uint8 OS_EventFlag;
	
	//[i] event key0_press_event;
	//[i] event key0_up_event;
	
	//---------------------
	//[i] var_bool key0_press;
	public bool key0_press;
	//---------------------
	//[i] var_bool key0_notpress;
	public bool key0_notpress;
	//---------------------
	//[i] var_uint8 OS_time;
	public uint8 OS_time;
	
	bit last_key0_press;
	
	
	
	//过时的
	//[i] var_bool_old key0_press_old;
	//[i] var_bool_old key0_notpress_old;
	public link bool key0_press_old = key0_press;
	public link bool key0_notpress_old = key0_notpress;
	
	
	
	//---------------------------------------------------
	//[i] function_void OS_init;
	public void OS_init()
	{
		#include "$run$.txt"

		OUT.D0_DIR = 0;
		OUT.D0_OUT = 1;
		
		OS_time = 50;
		OS_EventFlag = 0;
		last_key0_press = 0;
		
		key0_press = false;
		key0_notpress = true;
	}
	//---------------------------------------------------
	//[i] function_void OS_run;
	public void OS_run()
	{
		bit KEY0_IN = OUT.D0_IN;
		
		bit key0_down = KEY0_IN & ~last_key0_press;
		bit key0_up = ~KEY0_IN & last_key0_press;
		if( key0_down == 1 ) OS_EventFlag.0(bit) = 1;
		if( key0_up == 1 ) OS_EventFlag.1(bit) = 1;
		last_key0_press = KEY0_IN;
		
		if( KEY0_IN != 0 ) {
			key0_press = true;
			key0_notpress = false;
		}
		else {
			key0_press = false;
			key0_notpress = true;
		}
	}
}




