

const int32 DEVICE = 11;

interface[0x0100 * DEVICE + 0x00] void VEX_start_sd( uint8 idx ){}
interface[0x0100 * DEVICE + 0x01] void VEX_write_byte( uint8 idx, uint8 b ){}
interface[0x0100 * DEVICE + 0x02] uint8 VEX_read_byte( uint8 idx ){}
	
	
	//---------------------------------------------------
	//端口初始化
	void port_init()
	{
		DS_DIR = 1;
		DS_OUT = 1;
	}
	//---------------------------------------------------
	//启动温度转换
	void start_sd()
	{
		VEX_start_sd( DS.INDEX );
	}
	//---------------------------------------------------
	//写一个字节
	void write_byte( uint8 data )
	{
		VEX_write_byte( DS.INDEX, data );
	}
	//---------------------------------------------------
	//读一个字节
	uint8 read_byte()
	{
		return VEX_read_byte( DS.INDEX );
	}










