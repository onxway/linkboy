<module>
土壤湿度传感器的输出数值会随着周围土壤环境湿度的变化而变化, 当土壤水分升高时, 湿度会变大; 水分下降时, 湿度会变小. 数据的输出范围可设置, 当分分含量从很干燥变化到很潮湿时, <数值>量将会从最小值变化到最大值.
</module>
<name> Pack 土壤湿度传感器,
</name>
<member> function_void OS_init OS_init,

</member>,
<member> function_int32_debug get_data 数值,
调用这个指令返回当前的土壤湿度数值
</member>,
<member> linkconst_int32_0 MinValue 最小值,
设置输出数据的最小值
</member>,
<member> linkconst_int32_1023 MaxValue 最大值,
设置输出数据的最大值
</member>,
