//[配置信息开始],
//[语言列表] c chinese,
//[组件名称] Pack StepMotorDriver 步进电机驱动器,
//[模块型号] A4988,
//[资源占用] ,
//[实物图片] 1.png,2.png,3.png,
//[仿真类型] 1,
//[元素子项] var_uint8 OS_time OS_time OS_time,
//[元素子项] var_int32_w angle Angle 当前位置,
//[元素子项] linkconst_int32_200_步/每秒 speed Speed 转速,
//[元素子项] function_void OS_init OS_init OS_init,
//[元素子项] function_void OS_thread OS_thread OS_thread,
//[元素子项] function_void OS_run100us OS_run100us OS_run100us,
//[元素子项] function_void_int32 set_speed SetSpeed+# 设置转速为+#,
//[元素子项] function_void run_right RunRight 正转,
//[元素子项] function_void run_left RunLeft 反转,
//[元素子项] function_void_int32 run_right_t RunRightStep+# 正转+#+步,
//[元素子项] function_void_int32 run_left_t RunLeftStep+# 反转+#+步,
//[元素子项] function_void_int32 run_to RunTo+# 转到目标位置+#,
//[元素子项] function_void stop Stop 停止,
//[元素子项] function_void short_stop ShortStop 刹停,
//[元素子项] function_bool Running isRunning 处于旋转状态,
//[元素子项] function_bool Stoped isStoped 处于静止状态,
//[元素子项] function_void power_on PowerOn 设为锁定模式,
//[元素子项] function_void power_off PowerOff 设为空闲模式,
//[元素子项] function_void accel_on AccelOn 开启加减速,
//[元素子项] function_void accel_off AccelOff 关闭加减速,
//[配置信息结束],

unit Pack
{
	#include "Pack\$language$.txt"
	driver =
	#include "driver\Pack.txt"
}
