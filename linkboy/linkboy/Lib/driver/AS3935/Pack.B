//[配置信息开始],
//[语言列表] c chinese,
//[组件名称] Pack LightningSensor 雷电传感器,
//[模块型号] AS3935,
//[资源占用] ,
//[仿真类型] 1,
//[元素子项] var_uint8 OS_EventFlag OS_EventFlag OS_EventFlag,
//[元素子项] var_uint8 OS_time OS_time OS_time,
//[元素子项] event noise_event NoiseEvent 检测到噪音时,
//[元素子项] event dist_event DistEvent 检测到干扰时,
//[元素子项] event lighting_event LightingEvent 检测到雷电时,
//[元素子项] function_void OS_init OS_init OS_init,
//[元素子项] function_void OS_run OS_run OS_run,
//[元素子项] function_int32 get_distance_km DistanceKm 雷电距离,
//[配置信息结束],

unit Pack
{
	#include "Pack\$language$.txt"
	driver =
	#include "driver\Pack.txt"
}
