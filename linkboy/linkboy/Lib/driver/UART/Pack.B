//[配置信息开始],
//[语言列表] c chinese,
//[组件名称] Pack SerialPort 串口,
//[模块型号] ,
//[资源占用] USART,
//[仿真类型] 1,
//[元素子项] function_void OS_init OS_init OS_init,
//[元素子项] function_void OS_thread OS_thread OS_thread,
//[元素子项] interface_reader reader0 reader0 reader0,
//[元素子项] interface_writer writer0 writer0 writer0,
//[元素子项] linkconst_int16_50 max_length MaxLength 最大长度,
//[元素子项] linkconst_int32_9600 baud Baudrate 波特率,
//[元素子项] var_uint8 OS_EventFlag OS_EventFlag OS_EventFlag,
//[元素子项] event receive_event ReceiveEvent 接收缓冲区有数据时,
//[元素子项] function_void_int32 set_baud SetBaudrate+# 设置波特率为+#,
//[元素子项] function_int32 get_data GetData 获取一个数据,
//[元素子项] function_bool is_ready isReady 存在有效数据,
//[元素子项] function_void print_return PrintReturn 发送换行符,
//[元素子项] function_void_int32 print_char PrintChar+# 发送字符+#,
//[元素子项] function_void_Cstring print_string PrintString+# 发送字符串+#,
//[元素子项] function_void_int32 print_number PrintNumber+# 以字符串形式发送数字+#,
//[元素子项] function_void_Cstring print_hex_list PrintHexList+# 发送十六进制序列+#,
//[配置信息结束],

unit Pack
{
	#include "Pack\$language$.txt"
	driver =
	#include "driver\Pack.txt"
}
