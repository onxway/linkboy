<module>
VL53L0X飞行时间测距传感器是新一代激光测距模块，VL53LOX是完全集成的传感器，配有嵌入式红外、人眼安全激光，先进的滤波器和超高速光子探测阵列，测量距离更长，速度和精度更高。
  VL53L0X的感测能力可以支持各种功能，包括各种创新用户界面的手势感测或接近检测，扫地机器人、服务性机器人的障碍物探测与防撞系统，家电感应面版、笔记本电脑的用户存在检测或电源开关监控器，以及无人机和物联网(IoT)产品等。
产品参数
1.工作电压：3～5V
2.通信方式：IIC
3.测量绝对距离：2m
4.Xshutdown（复位）/GPIO（中断）
5.尺寸25MM*10.7MM
</module>
<name> Pack 测距传感器,
</name>
<member> var_uint8 OS_time OS_time,

</member>,
<member> var_bool ok 距离有效,

</member>,
<member> var_int32 Distance 距离,
测量障碍物到探测模块的距离, 单位是毫米.
</member>,
<member> var_int32 Acnt A参数,

</member>,
<member> var_int32 Scnt S参数,

</member>,
<member> function_void OS_init OS_init,

</member>,
<member> function_void OS_run OS_run,

</member>,
