//[配置信息开始],
//[语言列表] english_UL english_Pascal chinese,
//[组件名称] Pack G GamePad 三色灯,
//[模块型号] 共阳极,
//[资源占用] ,
//[仿真类型] 1,
//[元素子项] interface_led ledR ledR ledR ledR,
//[元素子项] interface_led ledG ledG ledG ledG,
//[元素子项] interface_led ledB ledB ledB ledB,
//[元素子项] function_void OS_init OS_init OS_init OS_init,
//[元素子项] function_void open_red open_red OpenRed 点亮红灯,
//[元素子项] function_void close_red close_red CloseRed 熄灭红灯,
//[元素子项] function_void open_green open_green OpenGreen 点亮绿灯,
//[元素子项] function_void close_green close_green CloseGreen 熄灭绿灯,
//[元素子项] function_void open_blue open_blue OpenBlue 点亮蓝灯,
//[元素子项] function_void close_blue close_blue CloseBlue 熄灭蓝灯,
//[配置信息结束],

unit Pack
{
	#include "Pack\$language$.txt"
	driver =
	#include "driver\Pack.txt"
}
