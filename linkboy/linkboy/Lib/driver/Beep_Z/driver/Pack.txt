
unit LED
{
	public const uint16 ID = 0;
	
	public link unit D {}
	
	#include "$run$.txt"
	
	//[i] var_bool open_status;
	bool open_status;
	//[i] var_bool close_status;
	bool close_status;

	//[i] function_void OS_init;
	public void OS_init()
	{
		D.D0_DIR = 1;
		open_status = false;
		close_status = true;
		close();
	}
	
	//[i] function_void open;
	public void open()
	{
		D.D0_OUT = 0;
		open_status = false;
		close_status = true;
	}
	
	//[i] function_void close;
	public void close()
	{
		D.D0_OUT = 1;
		open_status = false;
		close_status = true;
	}
	//[i] function_void swap;
	public void swap()
	{
		D.D0_OUT = ~D.D0_OUT;
		if( D.D0_OUT == 0 ) {
			open_status = true;
			close_status = false;
		}
		else {
			open_status = false;
			close_status = true;
		}
	}
}


