<module>
"小按钮"组件上边有一个微型开关, 当用户按下时就会触发“按钮按下事件”，松开的时候，触发“按钮松开事件”。
</module>
<name> Pack 电容按键,
</name>
<member> var_uint8 OS_EventFlag OS_EventFlag,

</member>,
<member> event key0_press_event 按钮按下时,
当按钮刚刚被按下的时候会自动触发此事件.
</member>,
<member> event key0_up_event 按钮松开时,
当按钮松开的时候会触发此事件
</member>,
<member> var_bool key0_press 按钮按下,
 "按键按下"属性是一个只读属性, 当按钮按下的时候, 这个值被自动设置为 YES, 当按钮松开的时候, 这个值被设置为 NO. 因此用户可以通过这个属性知道按钮是否被按下.
</member>,
<member> var_uint8 OS_time OS_time,

</member>,
<member> function_void OS_init OS_init,

</member>,
<member> function_void OS_run OS_run,

</member>,
