
//串口元件

unit uart
{
	public const uint16 ID = 0;

	//public link unit TXD {}
	public link unit RXD {}
	
	//[i] linkconst_int32_9600 baud;
	public const int32 baud = 9600;
	
	//---------------------
	//[i] var_uint8 OS_time;
	public uint8 OS_time;
	
	uint8 tick;
	
	//---------------------
	//[i] var_bool ok;
	public bool ok;
	//---------------------
	//[i] var_bool notok;
	public bool notok;
	
	//[i] var_uint8 OS_EventFlag;
	public uint8 OS_EventFlag;

	//[i] event receive_ne_event;
	//[i] event receive_time_event;
	
	//[i] var_Cstring Ebuffer;
	[int8*12] Ebuffer;
	//[i] var_Cstring Nbuffer;
	[int8*12] Nbuffer;
	
	
	//[i] var_int32 E;
	int32 E;
	//[i] var_int32 N;
	int32 N;
	
	//[i] var_int32 Year;
	int32 Year;
	//[i] var_int32 Mouth;
	int32 Mouth;
	//[i] var_int32 Day;
	int32 Day;
	//[i] var_int32 Hour;
	int32 Hour;
	//[i] var_int32 Minute;
	int32 Minute;
	//[i] var_int32 Second;
	int32 Second;
	
	bool GNGGA;
	bool GNZDA;
	
	
	uart.PORT.D2_DIR = RXD.D0_DIR;
	uart.PORT.D2_OUT = RXD.D0_OUT;
	uart.PORT.D2_PUL = RXD.D0_PUL;
	
	bit temp_bit;
	uart.PORT.D3_DIR = temp_bit;
	uart.PORT.D3_OUT = temp_bit;
	uart.PORT.D3_PUL = temp_bit;
	const int16 max_length = 500;
	uart.max_length = max_length;
	uart.baud = baud;
	unit uart
	{
		public const int32 baud = 9600;
		unit PORT
		{
			public link bit D2_DIR;
			public link bit D2_OUT;
			public link bit D2_PUL;

			public link bit D3_DIR;
			public link bit D3_OUT;
			public link bit D3_PUL;
		}
		#include <system\common\uart\uart_v1.txt>
	}
	#include "$run$.txt"
	
	//----------------------------------------------------
	//[i] function_void OS_init;
	public void OS_init()
	{
		OS_EventFlag = 0;
		
		uart.OS_init();

		//设置默认波特率
		uart.set_baud( baud );
		
		OS_time = 100;
		tick = 0;

		ok = false;
		notok = true;
	}
	//---------------------------------------------------
	//[i] function_void OS_run;
	//public void OS_run()

	//---------------------------------------------------
	//[i] function_void OS_thread;
	public void OS_thread()
	{
		loop {
			//判断数据是否就绪
			if( !uart.reader0.IsReady() ){
				break;
			}
			int8 data = (int8)(int16)uart.reader0.GetData();
			buffer[length] = data;
			length += 1;
			RXC();
		}
	}
	
	[100]int8 buffer;
	int16 length;

	void RXC()
	{
		if( length == 1 ) {
			if( buffer[0] != '$' ) {
				length = 0;
			}
			return;
		}
		//判断协议头是否合法
		if( length == 6 ) {
			GNGGA = buffer[1] == 'G' && buffer[2] == 'N' && buffer[3] == 'G' && buffer[4] == 'G' && buffer[5] == 'A';
			GNZDA = buffer[1] == 'G' && buffer[2] == 'N' && buffer[3] == 'Z' && buffer[4] == 'D' && buffer[5] == 'A';
			if( !GNGGA && !GNZDA ) {
				length = 0;
			}
			return;
		}
		//判断是否协议结束
		if( buffer[length-1] != '*' ) {
			return;
		}
		//判断当前是否为时间信息
		if( GNZDA ) {
			TIME();
		}
		//判断当前是否为经纬度信息
		if( GNGGA ) {
			NE();
		}
	}
	//---------------------------------------------------
	void TIME()
	{
		if( buffer[13] != '.' ) {
			return;
		}
		int32 temp;
		
		Year = 0;
		temp = (buffer[24] - '0'); temp *= 1000; Year += temp;
		temp = (buffer[25] - '0'); temp *= 100; Year += temp;
		temp = (buffer[26] - '0'); temp *= 10; Year += temp;
		temp = (buffer[27] - '0'); Year += temp;
		
		Mouth = 0;
		temp = (buffer[21] - '0'); temp *= 10; Mouth += temp;
		temp = (buffer[22] - '0'); Mouth += temp;
		
		Day = 0;
		temp = (buffer[18] - '0'); temp *= 10; Day += temp;
		temp = (buffer[19] - '0'); Day += temp;
		
		Hour = 0;
		temp = (buffer[7] - '0'); temp *= 10; Hour += temp;
		temp = (buffer[8] - '0'); Hour += temp;
		
		Minute = 0;
		temp = (buffer[9] - '0'); temp *= 10; Minute += temp;
		temp = (buffer[10] - '0'); Minute += temp;
		
		Second = 0;
		temp = (buffer[11] - '0'); temp *= 10; Second += temp;
		temp = (buffer[12] - '0'); Second += temp;
		
		Hour += 8;
		if( Hour >= 24 ) {
			Hour -= 24;
			Day += 1;
			if( Day > get_day( (int8)(int16)Mouth ) ) {
				Day = 1;
				Mouth += 1;
				if( Mouth > 12 ) {
					Mouth = 1;
					Year += 1;
				}
			}
		}
		
		//不要忘了清除长度! 
		length = 0;
		
		OS_EventFlag.1(bit) = 1;
	}
	//---------------------------------------------------
	void NE()
	{
		if( buffer[29] != 'N' || buffer[43] != 'E' ) {
			return;
		}
		tick = 0;
		ok = true;
		notok = false;
		
		for( int8 i = 0; i < 10; i += 1 ) {
			Nbuffer[i] = buffer[i + 18];
		}
		for( int8 i1 = 0; i1 < 11; i1 += 1 ) {
			Ebuffer[i1] = buffer[i1 + 31];
		}
		Nbuffer[9] = 0;
		Ebuffer[10] = 0;
		
		int32 temp;
		int32 temp2;
		
		N = 0;
		temp = (buffer[18] - '0'); temp *= 10000000; N += temp;
		temp = (buffer[19] - '0'); temp *= 1000000; N += temp;
		
		temp2 = 0;
		temp = (buffer[20] - '0'); temp *= 100000; temp2 += temp;
		temp = (buffer[21] - '0'); temp *= 10000; temp2 += temp;
		
		temp = (buffer[23] - '0'); temp *= 1000; temp2 += temp;
		temp = (buffer[24] - '0'); temp *= 100; temp2 += temp;
		temp = (buffer[25] - '0'); temp *= 10; temp2 += temp;
		temp = (buffer[26] - '0'); temp2 += temp;
		temp2 *= 5;
		temp2 /= 3;
		N += temp2;
		
		E = 0;
		temp = (buffer[31] - '0'); temp *= 100000000; E += temp;
		temp = (buffer[32] - '0'); temp *= 10000000; E += temp;
		temp = (buffer[33] - '0'); temp *= 1000000; E += temp;
		
		temp2 = 0;
		temp = (buffer[34] - '0'); temp *= 100000; temp2 += temp;
		temp = (buffer[35] - '0'); temp *= 10000; temp2 += temp;
		
		temp = (buffer[37] - '0'); temp *= 1000; temp2 += temp;
		temp = (buffer[38] - '0'); temp *= 100; temp2 += temp;
		temp = (buffer[39] - '0'); temp *= 10; temp2 += temp;
		temp = (buffer[40] - '0'); temp2 += temp;
		temp2 *= 5;
		temp2 /= 3;
		E += temp2;
		
		//不要忘了清除长度! 
		length = 0;
		
		OS_EventFlag.0(bit) = 1;
	}
	
	//计算指定月份的天数
	int8 get_day( int8 m )
	{
		switch( (uint)m ) {
			case 1:	return 31;
			case 2://2月闰年为29天
				if( Year%4==0 ) {
            			return 29;
       			}
				else {
            			return 28;
				}
			case 3:	return 31;
			case 4:	return 30;
			case 5:	return 31;
			case 6:	return 30;
			case 7:	return 31;
			case 8:	return 31;
			case 9:	return 30;
			case 10:return 31;
			case 11:return 30;
			case 12:return 31;
			default: return 0;
		}
		//语法合规
		return 0;
	}
}













