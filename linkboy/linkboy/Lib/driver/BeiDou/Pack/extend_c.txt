<module>

</module>
<name> Pack BeidouLocator,
</name>
<member> linkconst_int32_9600 baud Baud,

</member>,
<member> var_uint8 OS_time OS_time,

</member>,
<member> var_bool ok Ok,

</member>,
<member> var_bool notok NotOk,

</member>,
<member> var_uint8 OS_EventFlag OS_EventFlag,

</member>,
<member> event receive_ne_event ReceivePositionEvent,

</member>,
<member> event receive_time_event ReceiveTimeEvent,

</member>,
<member> var_Cstring Ebuffer EastLongitudeString,

</member>,
<member> var_Cstring Nbuffer NorthLatitudeString,

</member>,
<member> var_int32 E EastLongitude,

</member>,
<member> var_int32 N NorthLatitude,

</member>,
<member> var_int32 Year Year,

</member>,
<member> var_int32 Mouth Mouth,

</member>,
<member> var_int32 Day Day,

</member>,
<member> var_int32 Hour Hour,

</member>,
<member> var_int32 Minute Minute,

</member>,
<member> var_int32 Second Second,

</member>,
<member> function_void OS_init OS_init,

</member>,
<member> function_void OS_run OS_run,

</member>,
<member> function_void OS_thread OS_thread,

</member>,
