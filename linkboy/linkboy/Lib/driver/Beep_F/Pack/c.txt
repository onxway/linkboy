public link bit isOpen = driver.open_status;
public link bit isClose = driver.close_status;
public link void OS_init(){} = driver.OS_init;
public link void Open(){} = driver.open;
public link void Close(){} = driver.close;
public link void Toggle(){} = driver.swap;
