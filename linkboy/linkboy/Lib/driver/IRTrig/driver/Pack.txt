
//AVR系列单片机的按键驱动程序

unit key
{
	public const uint16 ID = 0;
	
	public link unit D {}
	
	#include "$run$.txt"
	
	public link bit KEY0_DIR = D.D0_DIR;
	public link bit KEY0_IN = D.D0_IN;
	public link bit KEY0_OUT = D.D0_OUT;
	
	//[i] var_uint8 OS_EventFlag;
	uint8 OS_EventFlag;
	
	//[i] event key0_press_event;
	
	//---------------------
	//[i] var_bool key0_press;
	public bool key0_press;
	//---------------------
	//[i] var_uint8 OS_time;
	public uint8 OS_time;
	
	bit last_key0_press;
	
	//---------------------------------------------------
	//[i] function_void OS_init;
	public void OS_init()
	{
		KEY0_DIR = 0;
		KEY0_OUT = 1;
		
		OS_time = 50;
		OS_EventFlag = 0;
		last_key0_press = 1;
	}
	//---------------------------------------------------
	//[i] function_void OS_run;
	public void OS_run()
	{
		bit KEY0 = ~KEY0_IN;
		
		bit key0_down = ~KEY0 & last_key0_press;
		if( key0_down == 1 ) OS_EventFlag.0(bit) = 1;
		last_key0_press = KEY0;
		
		if( KEY0 == 0 ) {
			key0_press = true;
		}
		else {
			key0_press = false;
		}
	}
}




