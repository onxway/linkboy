//[配置信息开始],
//[语言列表] c chinese,
//[组件名称] Pack Key 按键,
//[模块型号] 按下时为低电平,
//[资源占用] ,
//[仿真类型] 1,
//[控件属性] 文字,字体,文字颜色,背景颜色,
//[元素子项] var_uint8 OS_EventFlag OS_EventFlag OS_EventFlag,
//[元素子项] event key0_press_event DownEvent 按下时,
//[元素子项] event key0_up_event UpEvent 松开时,
//[元素子项] var_bool key0_press isDown 按下,
//[元素子项] var_bool key0_notpress isUp 松开,
//[元素子项] var_uint8 OS_time OS_time OS_time,
//[元素子项] var_bool_old key0_press_old key0_press_old 按键按下,
//[元素子项] var_bool_old key0_notpress_old key0_notpress_old 按键松开,
//[元素子项] function_void OS_init OS_init OS_init,
//[元素子项] function_void OS_run OS_run OS_run,
//[配置信息结束],

unit Pack
{
	#include "Pack\$language$.txt"
	driver =
	#include "driver\Pack.txt"
}
