
//时钟芯片底层驱动类
//修改日期: 2015.4.12
//加上了一个 isSafe 变量, 防止事件轮询进程读取时间时被 OS_run 读取秒标志中断导致时序丢失

unit var
{
	public const uint16 ID = 0;
	
	//[i] interface_clock clock0;
	public unit clock0
	{
		public const bool CanChange = true;
		public link bool isSafe;
		
		public link int32 second;
		public link int32 minute;
		public link int32 hour;
		public link int32 day;
		public link int32 month;
		public link int32 year;
	}
	clock0.isSafe = isSafe;
	clock0.second = second;
	clock0.minute = minute;
	clock0.hour = hour;
	clock0.day = day;
	clock0.month = month;
	clock0.year = year;
	
	public link unit SCL {}
	public link unit SDA {}
	
	//[i] var_uint8 OS_EventFlag;
	public uint8 OS_EventFlag;
	//[i] event time_changed;
	
	//---------------------
	//[i] var_uint8 OS_time;
	public uint8 OS_time;
	
	uint8 last_time;
	
	public bool isSafe;
	
	//[i] var_int32_w year;
	//[i] var_int32_w month;
	//[i] var_int32_w day;
	//[i] var_int32_w hour;
	//[i] var_int32_w minute;
	//[i] var_int32_w second;
	//[i] var_int32_w week;
	
	//限位表
	[#.code uint16*?] Area1 =
	{
		0, 0, 0, 1, 1, 1, 2000
	};
	[#.code uint16*?] Area2 =
	{
		59, 59, 23, 31, 7, 12, 2099
	};
	
	#include "$run$.txt"
	
	//---------------------------------------------------
	//初始化
	//[i] function_void OS_init;
	
	//---------------------------------------------------
	//[i] function_void OS_run;
	
	//---------------------------------------------------
	//[i] function_void set_time time;
	void set_time( [#.code int8*?] time )
	{
		int32 D1000 = 1000;
		int32 D100 = 100;
		int32 D10 = 10;
		
		int32 t_year = (time[0]-'0')*D1000 + (time[1]-'0')*D100 + (time[2]-'0')*D10 + (time[3]-'0');
		int32 t_month = (time[5]-'0')*D10 + (time[6]-'0');
		int32 t_day = (time[8]-'0')*D10 + (time[9]-'0');
		
		if( t_year != 0 ) year = t_year;
		if( t_month != 0 ) month = t_month;
		if( t_day != 0 ) day = t_day;
		
		hour = (time[11]-'0')*D10 + (time[12]-'0');
		minute = (time[14]-'0')*D10 + (time[15]-'0');
		second = (time[17]-'0')*D10 + (time[18]-'0');
	}
	//---------------------------------------------------
	//[i] function_bool time_equal Time;
	public bool time_equal( [int8*?] time )
	{
		if( time[0] != 0 && time[0] != year ) return false;
		if( time[1] != 0 && time[1] != month ) return false;
		if( time[2] != 0 && time[2] != day ) return false;
		if( time[3] != hour ) return false;
		if( time[4] != minute ) return false;
		if( time[5] != second ) return false;
		return true;
	}
}









