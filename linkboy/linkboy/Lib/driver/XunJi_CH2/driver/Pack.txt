
//AVR系列单片机的按键驱动程序

unit button
{
	public const uint16 ID = 0;
	
	public link unit K1 {}
	public link unit K2 {}
	
	#include "$run$.txt"
	
	public link bit KEY1_DIR = K1.D0_DIR;
	public link bit KEY1_IN = K1.D0_IN;
	public link bit KEY1_OUT = K1.D0_OUT;
	public link bit KEY2_DIR = K2.D0_DIR;
	public link bit KEY2_IN = K2.D0_IN;
	public link bit KEY2_OUT = K2.D0_OUT;
	
	//------------------------------------------------------------
	//[i] var_uint8 OS_EventFlag;
	public uint16 OS_EventFlag;
	
	//[i] event key1_press_event;
	//[i] event key1_up_event;
	
	//[i] event key2_press_event;
	//[i] event key2_up_event;

	//---------------------
	//[i] var_bool key1_press;
	public bool key1_press;
	//---------------------
	//[i] var_bool key2_press;
	public bool key2_press;

	//---------------------
	//[i] var_bool key1_notpress;
	public bool key1_notpress;
	//---------------------
	//[i] var_bool key2_notpress;
	public bool key2_notpress;

	//---------------------
	//[i] var_bool OS_time;
	public uint8 OS_time;
	
	bit last_key1_press;
	
	bit last_key2_press;
	
	//------------------------------------------------------------
	//[i] function_void OS_init;
	public void OS_init()
	{
		OS_time = 1;
		OS_EventFlag = 0;
		
		KEY1_DIR = 0;
		KEY1_OUT = 1;
		KEY2_DIR = 0;
		KEY2_OUT = 1;
		
		last_key1_press = 1;
		last_key2_press = 1;
		
		key1_press = false;
		key2_press = false;

		key1_notpress = true;
		key2_notpress = true;
	}
	//------------------------------------------------------------
	//[i] function_void OS_run;
	public void OS_run()
	{
		//按键1处理
		//一定要先读取到一个临时变量中!!!!
		//2015.9.14 22:41
		bit KEY1 = ~KEY1_IN;
		
		bit key1_down = ~KEY1 & last_key1_press;
		bit key1_up = KEY1 & ~last_key1_press;
		if( key1_down == 1 ) OS_EventFlag.0(bit) = 1;
		if( key1_up == 1 ) OS_EventFlag.1(bit) = 1;
		last_key1_press = KEY1;
		
		if( KEY1 == 0 ) {
			key1_press = true;
			key1_notpress = false;
		} else {
			key1_press = false;
			key1_notpress = true;
		}
		//按键2处理
		//一定要先读取到一个临时变量中!!!!
		//2015.9.14 22:41
		bit KEY2 = ~KEY2_IN;
		
		bit key2_down = ~KEY2 & last_key2_press;
		bit key2_up = KEY2 & ~last_key2_press;
		if( key2_down == 1 ) OS_EventFlag.2(bit) = 1;
		if( key2_up == 1 ) OS_EventFlag.3(bit) = 1;
		last_key2_press = KEY2;
		
		if( KEY2 == 0 ) {
			key2_press = true;
			key2_notpress = false;
		} else {
			key2_press = false;
			key2_notpress = true;
		}
	}
}





















