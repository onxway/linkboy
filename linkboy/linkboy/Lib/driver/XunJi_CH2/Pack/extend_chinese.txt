<module>
循迹模块上边有2路光电传感器，每个对应的传感器都可以检测下方是否有白色反光，并触发对应的事件，2个传感器独立检测，互不干扰。这2个独立传感器分别标注为A和B
</module>
<name> Pack 循迹模块,
</name>
<member> var_uint8 OS_EventFlag OS_EventFlag,

</member>,
<member> event key1_press_event A路传感器黑色变为白色时,

</member>,
<member> event key1_up_event A路传感器白色变为黑色时,

</member>,
<member> event key2_press_event B路传感器黑色变为白色时,

</member>,
<member> event key2_up_event B路传感器白色变为黑色时,

</member>,
<member> var_bool key1_press A路传感器检测到白色,

</member>,
<member> var_bool key2_press B路传感器检测到白色,

</member>,
<member> var_bool key1_notpress A路传感器检测到黑色,

</member>,
<member> var_bool key2_notpress B路传感器检测到黑色,

</member>,
<member> var_bool OS_time OS_time,

</member>,
<member> function_void OS_init OS_init,

</member>,
<member> function_void OS_run OS_run,

</member>,
