//[配置信息开始],
//[语言列表] c chinese,
//[组件名称] Pack Thermocouple 温度传感器,
//[模块型号] max6675,
//[资源占用] ,
//[仿真类型] 1,
//[元素子项] var_bool sta_ok sta_ok 已连接,
//[元素子项] function_void OS_init OS_init OS_init,
//[元素子项] function_int32_debug get_integer Integer 整数部分,
//[元素子项] function_int32 get_float Float 小数部分,
//[元素子项] function_int32 get_temperature Value 原始值,
//[配置信息结束],

unit Pack
{
	#include "Pack\$language$.txt"
	driver =
	#include "driver\Pack.txt"
}
