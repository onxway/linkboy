
//=================================================================================
	TM1637.DATA_IN = DATA_IN; TM1637.DATA_OUT = DATA_OUT; TM1637.DATA_DIR = DATA_DIR;
	TM1637.SHIFT_IN = SHIFT_IN; TM1637.SHIFT_OUT = SHIFT_OUT; TM1637.SHIFT_DIR = SHIFT_DIR;
	
	unit TM1637
	{
		TM1637.SDA_IN = DATA_IN; TM1637.SDA_OUT = DATA_OUT; TM1637.SDA_DIR = DATA_DIR;
		TM1637.SCL_OUT = SHIFT_OUT; TM1637.SCL_DIR = SHIFT_DIR;
		TM1637 =
		#include <system\common\TM1637.txt>
		
		public link bit DATA_IN; public link bit DATA_OUT; public link bit DATA_DIR;
		public link bit SHIFT_IN; public link bit SHIFT_OUT; public link bit SHIFT_DIR;
		
		//---------------------------------------------------
		//初始化
		public void init()
		{
			TM1637.OS_init();
			
			//设置亮度
			TM1637.set_command( 0B1000_1111 );
			
			//数据命令设置 : 普通模式, 设置固定地址, 写数据到内部寄存器
			TM1637.set_command( 0B0100_0100 );
		}
		//---------------------------------------------------
		//设置亮度
		public void set_light( uint8 d )
		{
			if( d == 0 ) {
				TM1637.set_command( 0B1000_0000 );
			}
			else {
				d -= 1;
				d &= 0b00000111;
				TM1637.set_command( 0B1000_1000 + d );
			}
		}
		//---------------------------------------------------
		//发送一组数据, 1-点亮  0-熄灭
		// bit0: G
		// bit1: F
		// bit2: E
		// bit3: D
		// bit4: C
		// bit5: B
		// bit6: A
		// bit7: H
		public void send( [uint8*?] data, uint8 point )
		{
			for( uint8 i = 0; i < 8; i += 1 ) {
				uint8 d = data[i];
				d.7(bit) = point.0(bit);
				point >>= 1;
				
				TM1637.set_data( 0B1100_0000 + i, d );
			}
		}
	}
