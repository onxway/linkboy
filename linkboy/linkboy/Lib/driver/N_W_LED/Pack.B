//[配置信息开始],
//[语言列表] english_Pascal chinese,
//[组件名称] Pack Pack 白暖灯,
//[模块型号] 共阴极(高电平亮),
//[资源占用] ,
//[仿真类型] 1,
//[元素子项] interface_led ledN ledN ledN,
//[元素子项] interface_led ledW ledW ledW,
//[元素子项] function_void OS_init OS_init OS_init,
//[元素子项] function_void open_n OpenN 打开暖灯,
//[元素子项] function_void close_n CloseN 关闭暖灯,
//[元素子项] function_void open_w OpenW 打开白灯,
//[元素子项] function_void close_w CloseW 关闭白灯,
//[配置信息结束],

unit Pack
{
	#include "Pack\$language$.txt"
	driver =
	#include "driver\Pack.txt"
}
