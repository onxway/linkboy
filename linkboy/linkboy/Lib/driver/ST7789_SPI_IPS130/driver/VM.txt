
const int32 DEVICE = 4;

interface[0x0100 * DEVICE + 0x00] void VEX_SetPin( uint8 din, uint8 clk, uint8 res, uint8 dc ){}
interface[0x0100 * DEVICE + 0x01] void VEX_Init( int32 w, int32 h ) {}
interface[0x0100 * DEVICE + 0x02] void VEX_draw_pixel( int32 x, int32 y ) {}
interface[0x0100 * DEVICE + 0x03] void VEX_clear_pixel( int32 x, int32 y ) {}
interface[0x0100 * DEVICE + 0x04] void LCD_Clear(uint16 Color) {}

interface[0x0100 * DEVICE + 0x05] void set_fore(uint16 Color, bool t) {}
interface[0x0100 * DEVICE + 0x06] void set_back(uint16 Color, bool t) {}

interface[0x0100 * DEVICE + 0x07] void LCD_Address_Set( uint16 x1, uint16 y1, uint16 x2, uint16 y2 ) {}
interface[0x0100 * DEVICE + 0x08] void add_color(void) {}


//=============================================

public void Lcd_Init()
{
	VEX_SetPin( DIN.INDEX, CLK.INDEX, RES.INDEX, DC.INDEX );
	VEX_Init( LCD_W, LCD_H );
}
void draw_pixel( int32 x, int32 y )
{
	VEX_draw_pixel( x, y );
}
void clear_pixel( int32 x, int32 y )
{
	VEX_clear_pixel( x, y );
}
public void set_refresh()
{
}



