<module>

</module>
<name> Pack7219 屏幕,
</name>
<member> function_void_int32 set_light 设置屏幕亮度为 #,
注意屏幕亮度的有效范围是0-15, 0表示很暗, 15表示最亮, 其他值分别表示对应的亮度. 初始化默认是15(最亮).
</member>,
<member> interface_map map map,

</member>,
<member> var_int32 Width 宽度,

</member>,
<member> var_int32 Height 高度,

</member>,
<member> var_int32_w PenWidth 画笔宽度,

</member>,
<member> var_int32_w ForColor 前景颜色,

</member>,
<member> var_int32_w BackColor 背景颜色,

</member>,
<member> function_void OS_init OS_init,

</member>,
<member> function_void OS_thread OS_thread,

</member>,
<member> function_void Clear 清屏,

</member>,
<member> function_void_int32_int32 pixel 在x # y # 处画点,

</member>,
<member> function_void_int32_int32 clear_pixel 在x # y # 处清除点,

</member>,
<member> function_int32_int32_int32 get_pixel 获取x # y # 处颜色,

</member>,
<member> function_void_int32_int32 Move 卷轴 # 步到Y坐标 # 为止,

</member>,
<member> function_void Refresh 刷新屏幕,

</member>,
