//[配置信息开始],
//[语言列表] c chinese,
//[组件名称] Pack7219XY Screen 屏幕,
//[模块型号] SH1106-2线I2C接口,
//[资源占用] ,
//[仿真类型] 1,
//[元素子项] linkconst_int32_4 H_NUMBER H_NUMBER 行级联数目,
//[元素子项] linkconst_int32_4 L_NUMBER L_NUMBER 列级联数目,
//[元素子项] function_void_int32 set_light SetLight+# 设置屏幕亮度为+#,
//[元素子项] interface_map map map map,
//[元素子项] var_int32 Width Width 宽度,
//[元素子项] var_int32 Height Height 高度,
//[元素子项] var_int32_w PenWidth PenWidth 画笔宽度,
//[元素子项] var_int32_w ForColor ForeColor 前景颜色,
//[元素子项] var_int32_w BackColor BackColor 背景颜色,
//[元素子项] function_void OS_init OS_init OS_init,
//[元素子项] function_void OS_thread OS_thread OS_thread,
//[元素子项] function_void Clear Clear 清屏,
//[元素子项] function_void_int32_int32 pixel DrawPixel+#+# 在x+#+y+#+处画点,
//[元素子项] function_void_int32_int32 clear_pixel ClearPixel+#+# 在x+#+y+#+处清除点,
//[元素子项] function_int32_int32_int32 get_pixel GetPixel+#+# 获取x+#+y+#+处颜色,
//[元素子项] function_void_int32_int32 Move Move+#+# 卷轴+#+步到Y坐标+#+为止,
//[元素子项] function_void Refresh Refresh 刷新屏幕,
//[配置信息结束],

unit Pack7219XY
{
	#include "Pack7219XY\$language$.txt"
	driver =
	#include "driver\Pack7219XY.txt"
}
