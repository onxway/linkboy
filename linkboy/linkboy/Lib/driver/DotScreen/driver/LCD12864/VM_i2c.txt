
public link unit SCL {}
public link unit SDA {}

const int32 DEVICE = 2;

interface[0x0100 * DEVICE + 0x00] void VEX_SetPin( uint8 sck, uint8 sda ){}
interface[0x0100 * DEVICE + 0x01] void VEX_Init( int8 offset ){}
interface[0x0100 * DEVICE + 0x02] void VEX_Refresh( [uint8*?] buf ){}

public void Config_init()
{
	VEX_SetPin( SCL.INDEX, SDA.INDEX );
}




