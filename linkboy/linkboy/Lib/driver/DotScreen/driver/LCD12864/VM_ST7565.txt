
public link unit SCL {}
public link unit SI {}
public link unit RES {}
public link unit DC {}

public link unit ICSI {}
public link unit ICCS {}
public link unit ICSCL {}
public link unit ICSO {}

const int32 DEVICE = 1;

interface[0x0100 * DEVICE + 0x00] void VEX_SetPin( int8 type, uint8 sck, uint8 si, uint8 cs, uint8 dc ){}
interface[0x0100 * DEVICE + 0x01] void VEX_Init( int8 offset ){}
interface[0x0100 * DEVICE + 0x02] void VEX_Refresh( [uint8*?] buf ){}
interface[0x0100 * DEVICE + 0x03] void VEX_SetUint8( int16 ar, int16 li, uint8 d ){}


public void Config_init()
{
	VEX_SetPin( 1, SCL.INDEX, SI.INDEX, RES.INDEX, DC.INDEX );

	RES.D0_DIR = 1;
	RES.D0_OUT = 0;
	//loop( 100 ) loop( 100 ) {}
	delay_ms( 1 );
	RES.D0_OUT = 1;
	//loop( 100 ) loop( 100 ) {}
	delay_ms( 1 );
}

//延时函数
//link void delay_us( uint8 t ) {} = #.sys_delayer.delay_n_1us;
//link void delay_10us( uint8 t ) {} = #.sys_delayer.delay_n_10us;
link void delay_ms( int32 t ) {} = #.sys_delayer.delay_ms;

