
public link unit SCL {}
public link unit SI {}
public link unit CS {}
public link unit RES {}
public link unit DC {}

public link unit FSO {}
public link unit CS2 {}

command.SCL = SCL;
command.SI = SI;
command.DC = DC;
command.RES = RES;

unit command
{
	public link unit SCL {}
	public link unit SI {}
	public link unit RES {}
	public link unit DC {}
	
	link bit DC_IN = DC.D0_IN;
	link bit DC_OUT = DC.D0_OUT;
	link bit DC_DIR = DC.D0_DIR;

	link bit RES_IN = RES.D0_IN;
	link bit RES_OUT = RES.D0_OUT;
	link bit RES_DIR = RES.D0_DIR;
	
	link bit SI_IN = SI.D0_IN;
	link bit SI_OUT = SI.D0_OUT;
	link bit SI_DIR = SI.D0_DIR;
	
	link bit SCL_IN = SCL.D0_IN;
	link bit SCL_OUT = SCL.D0_OUT;
	link bit SCL_DIR = SCL.D0_DIR;
	
	//---------------------------------------------------
	//[i] function OS_init;
	public void OS_init()
	{
		//端口初始化
		PORT_init();
		
		//CS_OUT = 0;
		
		//软件初始化
		write_command( 0xae );//--turn off oled panel
		write_command( 0x00 );//---set low column address
		write_command( 0x10 );//---set high column address
		write_command( 0x40 );//--set start line address  Set Mapping RAM Display Start Line (0x00~0x3F)
		write_command( 0x81 );//--set contrast control register
		write_command( 0xcf );// Set SEG Output Current Brightness
		write_command( 0xa1 );//--Set SEG/Column Mapping     0xa0左右反置 0xa1正常
		write_command( 0xc8 );//Set COM/Row Scan Direction   0xc0上下反置 0xc8正常
		write_command( 0xa6 );//--set normal display
		write_command( 0xa8 );//--set multiplex ratio(1 to 64)
		write_command( 0x3f );//--1/64 duty
		write_command( 0xd3 );//-set display offset	Shift Mapping RAM Counter (0x00~0x3F)
		write_command( 0x00 );//-not offset
		write_command( 0xd5 );//--set display clock divide ratio/oscillator frequency
		write_command( 0x80 );//--set divide ratio, Set Clock as 100 Frames/Sec
		write_command( 0xd9 );//--set pre-charge period
		write_command( 0xf1 );//Set Pre-Charge as 15 Clocks & Discharge as 1 Clock
		write_command( 0xda );//--set com pins hardware configuration
		write_command( 0x12 );
		write_command( 0xdb );//--set vcomh
		write_command( 0x40 );//Set VCOM Deselect Level
		write_command( 0x20 );//-Set Page Addressing Mode (0x00/0x01/0x02)
		write_command( 0x02 );//
		write_command( 0x8d );//--set Charge Pump enable/disable
		write_command( 0x14 );//--set(0x10) disable
		write_command( 0xa4 );// Disable Entire Display On (0xa4/0xa5)
		write_command( 0xa6 );// Disable Inverse Display On (0xa6/a7)
		write_command( 0xaf );//--turn on oled panel
		
		
		//CS_OUT = 1;
	}

	//---------------------------------------------------
	void PORT_init()
	{
		DC_DIR = 1;
		DC_OUT = 0;
		
		SI_DIR = 1;
		SI_OUT = 0;
		
		SCL_DIR = 1;
		SCL_OUT = 1;
		
		RES.D0_OUT = 0;
		loop( 20 ) loop( 10 ) {}
		RES.D0_OUT = 1;
		loop( 20 ) loop( 10 ) {}
	}
	//---------------------------------------------------
	//[i] function write_command,
	public void write_command( uint8 C )
	{
		DC_OUT = 0;
		loop( 8 ) {
			SI_OUT = C.7(bit);
			C <<= 1;
			SCL_OUT = 0;
			SCL_OUT = 1;
		}
	}
	//---------------------------------------------------
	//[i] function write_data,
	public void write_data( uint8 D )
	{
		DC_OUT = 1;
		loop( 8 ) {
			SI_OUT = D.7(bit);
			D <<= 1;
			SCL_OUT = 0;
			SCL_OUT = 1;
		}
	}
}


#include "type1.txt"








