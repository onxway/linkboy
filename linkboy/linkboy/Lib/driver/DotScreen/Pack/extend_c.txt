<module>

</module>
<name> Pack Screen,
</name>
<member> interface_map map map,

</member>,
<member> var_int32 Width Width,

</member>,
<member> var_int32 Height Height,

</member>,
<member> var_int32_w PenWidth PenWidth,

</member>,
<member> var_int32_w ForColor ForeColor,

</member>,
<member> var_int32_w BackColor BackColor,

</member>,
<member> function_void OS_init OS_init,

</member>,
<member> function_void OS_thread OS_thread,

</member>,
<member> function_void Clear Clear,

</member>,
<member> function_void_int32_int32 pixel DrawPixel # #,

</member>,
<member> function_void_int32_int32 clear_pixel ClearPixel # #,

</member>,
<member> function_int32_int32_int32 get_pixel GetPixel # #,

</member>,
<member> function_void_int32_int32 Move Move # #,

</member>,
<member> function_void Refresh Refresh,

</member>,
