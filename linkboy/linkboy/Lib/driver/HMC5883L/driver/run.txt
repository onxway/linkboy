
	I2C.SDA = SDA;
	I2C.SCL = SCL;
	I2C =
	#include <system\common\I2C\driver.txt>
	
	uint8 DevAddr;
	uint8 ValueAddr;
	
	//--------------------------------------------
	//[i] function_void OS_init;
	public void OS_init()
	{
		I2C.Init();
		
		DevAddr = 0x3C; //ԭװHMC5883
		ValueAddr = 0x03;
		I2C.A1_set_uint8( DevAddr, 0x02, 0x00 );
		
		//DevAddr = 0x1A; //����QMC5883
		//ValueAddr = 0x00;
		//I2C.A1_set_uint8( DevAddr, 0x09, 0x0D );
		//I2C.A1_set_uint8( DevAddr, 0x0B, 0x01 );
		//I2C.A1_set_uint8( DevAddr, 0x20, 0x40 );
		//I2C.A1_set_uint8( DevAddr, 0x21, 0x01 );
	}
	//---------------------------------------------------
	//[i] function_int32 get_channel0;
	public int32 get_channel0()
	{
		I2C.A1_get_byte6( DevAddr, ValueAddr );
		uint16 d1;
		uint16 d2;
		uint16 d3;
		
		d1.0(uint8) = I2C.byte1;
		d1.8(uint8) = I2C.byte0;
		
		return (int)d1;
	}
	//---------------------------------------------------
	//[i] function_int32 get_channel1;
	public int32 get_channel1()
	{
		I2C.A1_get_byte6( DevAddr, ValueAddr );
		uint16 d1;
		uint16 d2;
		uint16 d3;
		
		d1.0(uint8) = I2C.byte3;
		d1.8(uint8) = I2C.byte2;
		
		return (int)d1;
	}
	//---------------------------------------------------
	//[i] function_int32 get_channel2;
	public int32 get_channel2()
	{
		I2C.A1_get_byte6( DevAddr, ValueAddr );
		uint16 d1;
		uint16 d2;
		uint16 d3;
		
		d1.0(uint8) = I2C.byte5;
		d1.8(uint8) = I2C.byte4;
		
		return (int)d1;
	}
	
	
	