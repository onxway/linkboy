//[配置信息开始],
//[语言列表] c chinese,
//[组件名称] Pack DigitalTube 四位数码管,
//[模块型号] TM1650,
//[资源占用] ,
//[仿真类型] 1,
//[控件属性] 文字,字体,文字颜色,背景颜色,
//[元素子项] interface_char char char char,
//[元素子项] function_void OS_init OS_init OS_init,
//[元素子项] function_void OS_thread OS_thread OS_thread,
//[元素子项] function_void clear Clear 清空,
//[元素子项] function_void_int32_int32 print_char ShowChar+#+# 在第+#+列显示字符+#,
//[元素子项] function_void_int32 show_point ShowPoint+# 显示第+#+个小数点,
//[元素子项] function_void_int32 hide_point HidePoint+# 隐藏第+#+个小数点,
//[元素子项] function_void clear_point ClearPoint 隐藏全部小数点,
//[元素子项] function_void_int32 set_light SetBrightness+# 设置屏幕亮度为+#,
//[配置信息结束],

unit Pack
{
	#include "Pack\$language$.txt"
	driver =
	#include "driver\Pack.txt"
}
