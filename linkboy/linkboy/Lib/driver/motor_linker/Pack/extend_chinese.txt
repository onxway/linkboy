<module>
可以驱动两个直流马达. PWM周期默认为1不需要改动，如果用户需要马达能在很低的转速下转动，则需要适当增加PWM周期数值，例如设置为100，则1秒钟才输出一个完整PWM信号，可驱动马达在极低转速下转动。
</module>
<name> Pack 电机驱动板,
</name>
<member> linkconst_int32_1 TT 周期,
马达驱动器的PWM调速周期，有效范围是1-200，默认为1，单位是0.1ms，也就是一秒钟会产生100个完整的PWM信号（每个PWM信号包含100个脉冲分辨率）。
</member>,
<member> function_void OS_init OS_init,

</member>,
<member> function_void_old_int32 set_t1 设置PWM周期为 #,
范围是1-200，数值越大，PWM周期越大，马达越抖动，但是马达的最低启动功率也会降低，例如：默认PWM周期设置为1时，马达功率需要超过30左右才会转动；如果设置PWM周期为100之后，则功率超过10马达即可转动。需要用户根据实际情况权衡设置。
</member>,
<member> function_void_int32 set_t 设置周期为 #,
范围是1-200，数值越大，PWM周期越大，马达越抖动，但是马达的最低启动功率也会降低，例如：默认PWM周期设置为1时，马达功率需要超过30左右才会转动；如果设置PWM周期为100之后，则功率超过10马达即可转动。需要用户根据实际情况权衡设置。
</member>,
<member> function_void OS_run100us OS_run100us,

</member>,
