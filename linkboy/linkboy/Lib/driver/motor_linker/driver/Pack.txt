
unit motor
{
	public const uint16 ID = 0;
	
	public link unit INA1 {}
	public link unit INA2 {}
	public link unit INB1 {}
	public link unit INB2 {}
	
	int8 valueA;
	int8 valueB;
	bit DTA1;
	bit DTA2;
	bit DTB1;
	bit DTB2;
	
	int8 tick;
	
	//[i] linkconst_int32_1 TT;
	public const int32 TT = 1;
	
	int8 T;
	int8 tt;
	
	//[i] function_void OS_init;
	public void OS_init()
	{
		INA1.D0_DIR = 1;
		INA1.D0_OUT = 0;
		INA2.D0_DIR = 1;
		INA2.D0_OUT = 0;
		
		INB1.D0_DIR = 1;
		INB1.D0_OUT = 0;
		INB2.D0_DIR = 1;
		INB2.D0_OUT = 0;
		
		valueA = 100;
		valueB = 100;
		tick = 0;
		
		DTA1 = 0;
		DTA2 = 0;
		DTB1 = 0;
		DTB2 = 0;
		
		T = (int8)(int16)TT;
		tt = 0;

		vex_init();
	}
	
	//---------------------------------------------------
	//[i] function_void_old set_t1 int32;

	public link void set_t1( int32 t ) {} = set_t;

	//[i] function_void set_t int32;
	public void set_t( int32 t )
	{
		T = (int8)(int16)t;
		tt = 0;
	}
	
	//---------------------------------------------------
	//[i] function_void OS_run100us;

	#include "$run$.txt"
}
//---------------------------------------------------
OUTA1.D = driver.INA1;
OUTA1.power = driver.valueA;
OUTA1.DT = driver.DTA1;
unit OUTA1
{
	link unit D {}
	link int8 power;
	link int8 DT;
}
OUTA2.D = driver.INA2;
OUTA2.power = driver.valueA;
OUTA2.DT = driver.DTA2;
unit OUTA2
{
	link unit D {}
	link int8 power;
	link int8 DT;
}
//---------------------------------------------------
OUTB1.D = driver.INB1;
OUTB1.power = driver.valueB;
OUTB1.DT = driver.DTB1;
unit OUTB1
{
	link unit D {}
	link int8 power;
	link int8 DT;
}
OUTB2.D = driver.INB2;
OUTB2.power = driver.valueB;
OUTB2.DT = driver.DTB2;
unit OUTB2
{
	link unit D {}
	link int8 power;
	link int8 DT;
}
