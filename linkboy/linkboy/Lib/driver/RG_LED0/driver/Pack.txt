
unit Pack
{
	public const uint16 ID = 0;
	
	#include "$run$.txt"
	
	public link unit R {}
	public link unit G {}
	public link unit B {}
	
	link bit GREEN_DIR = G.D0_DIR;
	link bit GREEN_OUT = G.D0_OUT;
	
	link bit RED_DIR = R.D0_DIR;
	link bit RED_OUT = R.D0_OUT;
	
	//link bit BLUE_DIR = B.D0_DIR;
	//link bit BLUE_OUT = B.D0_OUT;

	bit BLUE_DIR;
	bit BLUE_OUT;
	
	//[i] interface_led ledR;
	unit ledR
	{
		public link void Open() {}
		public link void Close() {}
	}
	ledR.Open = open_red;
	ledR.Close = close_red;
	
	//[i] interface_led ledG;
	unit ledG
	{
		public link void Open() {}
		public link void Close() {}
	}
	ledG.Open = open_green;
	ledG.Close = close_green;
	
	//---------------------------------------------------
	//��ʼ��
	//[i] function_void OS_init;
	//��ʼ��
	public void OS_init()
	{
		RED_DIR = 1;
		RED_OUT = 1;
		GREEN_DIR = 1;
		GREEN_OUT = 1;
		//BLUE_DIR = 1;
		//BLUE_OUT = 1;
		Init();
	}
	//---------------------------------------------------
	//�������
	//[i] function_void open_red;
	
	//---------------------------------------------------
	//Ϩ����
	//[i] function_void close_red;
	
	//---------------------------------------------------
	//�����̵�
	//[i] function_void open_green;
	
	//---------------------------------------------------
	//Ϩ���̵�
	//[i] function_void close_green;
	
	//---------------------------------------------------
	//��������
	///[i] function_void open_blue;
	
	//---------------------------------------------------
	//Ϩ������
	///[i] function_void close_blue;
}























