
unit LED
{
	public const uint16 ID = 0;
	
	public link unit DP {}
	
	//[i] interface_led led0;
	//[i] interface_digital digital0;
	
	//[i] var_bool open_status;
	bool open_status;
	//[i] var_bool close_status;
	bool close_status;
	
	unit led0
	{
		public link void Open() {}
		public link void Close() {}
	}
	led0.Open = open;
	led0.Close = close;

	unit digital0
	{
		public link void open() {}
		public link void close() {}
	}
	digital0.open = open;
	digital0.close = close;

	//[i] function_void OS_init;
	public void OS_init()
	{
		DP.D0_DIR = 1;
		close();
	}
	
	//[i] function_void open;
	public void open()
	{
		DP.D0_OUT = 0;
		open_status = true;
		close_status = false;
	}
	
	//[i] function_void close;
	public void close()
	{
		DP.D0_OUT = 1;
		open_status = false;
		close_status = true;
	}
	//[i] function_void swap;
	public void swap()
	{
		DP.D0_OUT = ~DP.D0_OUT;
		if( DP.D0_OUT == 0 ) {
			open_status = true;
			close_status = false;
		}
		else {
			open_status = false;
			close_status = true;
		}
	}
	//[i] function_void set_status int32;
	public void set_status( int32 t )
	{
		if( t == 0 ) {
			close();
		}
		else {
			open();
		}
	}
}




