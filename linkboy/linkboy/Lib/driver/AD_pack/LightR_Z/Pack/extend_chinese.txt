<module>
光敏电阻模块, 可以感应环境光线亮度. 当环境光线从特别黑暗变化到非常亮的时候, <光线强度> 就会从 <最小值> 变化到 <最大值>.
</module>
<name> Pack 光敏电阻,
</name>
<member> function_void OS_init OS_init,

</member>,
<member> function_int32_debug get_data 光线强度,
调用这个指令返回当前的光线强度，当环境光线为完全黑暗时，数值为最小值；特别亮的时候数值为最大值；
</member>,
<member> linkconst_int32_0 MinValue 最小值,
设置光线感应器的最小值
</member>,
<member> linkconst_int32_1023 MaxValue 最大值,
设置光线感应器的最大值
</member>,
