<module>

</module>
<name> Pack Clock,
</name>
<member> interface_clock clock0 clock0,

</member>,
<member> var_uint8 OS_EventFlag OS_EventFlag,

</member>,
<member> event time_changed TimeChanged,

</member>,
<member> var_uint8 OS_time OS_time,

</member>,
<member> var_int32_w year Year,

</member>,
<member> var_int32_w month Month,

</member>,
<member> var_int32_w day Day,

</member>,
<member> var_int32_w hour Hour,

</member>,
<member> var_int32_w minute Minute,

</member>,
<member> var_int32_w second Second,

</member>,
<member> var_int32_w week Week,

</member>,
<member> function_void OS_init OS_init,

</member>,
<member> function_void OS_run OS_run,

</member>,
<member> function_void_time set_time SetTime #,

</member>,
<member> function_bool_Time time_equal TimeEqual #,

</member>,
