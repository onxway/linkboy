
	#include "command_$config$.txt"
	
	[uint8*BUFFER_NUMBER] buffer;
	
	int8 Offset;
	
	//---------------------------------------------------
	//[i] function_void OS_init;
	public void OS_init()
	{
		#include "$SHSSD$.txt"
		
		command.OS_init();
		NeedRefresh = false;
		BackData = 0xFF;
		ClearData = 0x00;
		Clear();
	}
	//---------------------------------------------------
	//[i] function_void OS_thread;
	void OS_thread()
	{
		if( NeedRefresh ) {
			uint8 line = 0;
			uint16 index = 0;
			loop( 8 ) {
				command.write_command( 0xB0 + line );
				command.write_command( 0x00 + (uint)Offset );
				command.write_command( 0x10 );
				line += 1;
				loop( 128 ) {
					command.write_data( buffer[index] );
					index += 1;
				}
			}
			NeedRefresh = false;
		}
	}
	//---------------------------------------------------
	//[i] function_void Clear;
	void Clear()
	{
		for( int16 i = 0; i < BUFFER_NUMBER; i + 1 ) {
			buffer[i] = ClearData;
		}
		NeedRefresh = true;
	}
	//---------------------------------------------------
	//[i] function_bool isOpen int32 int32;
	bool isOpen( int32 x, int32 y )
	{
		int8 m = (int8)(int16)y % 8;
		uint16 yy = (uint16)(uint)y;
		uint16 index = yy / 8;
		index = index * 128 + (uint16)(uint)x;
		uint8 data = buffer[index];
		data >>= (uint)m;
		return data.0(bit) == 1;
	}
	//---------------------------------------------------
	//[i] function_void Move int32 int32;
	void Move( int32 v, int32 vy )
	{
		uint8 yy = (uint8)(uint16)(uint)vy;
		uint8 off = yy % 8;
		yy /= 8;
		
		uint8 sft = (uint8)(uint16)(uint)v;
		for( uint8 x = 4; x < 92; x += 1 ) {
			uint8 lastData = 0;
			uint8 y;
			for( y = 0; y <= yy; y += 1 ) {
				uint16 index = y;
				index = index * 128 + x;
				uint16 data = buffer[index];
				
				if( y == yy ) {
					if( off == 0 ) {
						break;
					}
					uint8 b = 0b1111_1111 << off;
					lastData |= data.0(uint8) << sft;
					lastData &= ~b;
					
					data &= b;
				}
				else {
					data <<= sft;
				}
				
				data.0(uint8) = data.0(uint8) | lastData;
				buffer[index] = data.0(uint8);
				lastData = data.8(uint8);
			}
		}
		NeedRefresh = true;
	}
	//---------------------------------------------------
	//[i] function_void set_uint8 uint8;
	void set_uint8( int16 line, int16 area, uint8 d )
	{
		if( area < 0 || area >= AreaNumber || line < 0 || line >= ColumnNumber ) return;
		buffer[area*ColumnNumber + line] = d;
		NeedRefresh = true;
		
		//int16 ll = line + Offset;
		//command.write_command( 0xB0 + (uint8)(uint)area );
		//command.write_command( 0x10 | ((uint8)(uint)ll >> 4) );
		//command.write_command( (uint8)(uint)ll & 0x0F );
		//command.write_data( d );
	}
	
	//---------------------------------------------------
	//[i] function_uint8 get_uint8;
	uint8 get_uint8( int16 line, int16 area )
	{
		if( area < 0 ) return 0;
		if( area < 0 || area >= AreaNumber || line < 0 || line >= ColumnNumber ) return BackData;
		return buffer[area*ColumnNumber + line];
	}
	
	