<module>
电脑键盘可以模拟触发按键按下和按键松开. 我们最常用的是 <按一下 ? 键> 这个指令, 因为它包括了按下和松开的整个过程.
</module>
<name> Pack 电脑键盘,
</name>
<member> linksysconst_uint8 ID ID,

</member>,
<member> function_void OS_init OS_init,

</member>,
<member> function_void_int32 KeyPress 按一下 # 键,
执行这个指令相当于用户按一下电脑键盘上的指定按键, 因为这个指令包括按下和松开两个过程, 一般情况下我们都用这个指令, 而不用 <按住 ? 键> 和 <松开 ? 键>.
</member>,
<member> function_void_int32 KeyDown 按住 # 键,
执行这个指令相当于用户按住电脑键盘上的指定按键, 注意是一直按下, 直到执行了 <松开 ? 键> 指令才会松开按键.
</member>,
<member> function_void_int32 KeyUp 松开 # 键,
执行这个指令相当于用户松开电脑键盘上的指定按键
</member>,
<member> const_int32 VK_CAPITAL 大小写锁定键,

</member>,
<member> const_int32 VK_LSHIFT 左边SHIFT键,

</member>,
<member> const_int32 VK_RETURN 回车键,

</member>,
<member> const_int32 VK_BACK 后退键,

</member>,
<member> const_int32 VK_LWIN WIN徽标键,

</member>,
<member> const_int32 VK_ESCAPE ESC退出键,

</member>,
<member> const_int32 VK_CONTROL CONTROL控制键,

</member>,
<member> const_int32 VK_MENU 菜单键,

</member>,
<member> const_int32 VK_HOME HOME键,

</member>,
<member> const_int32 VK_END END键,

</member>,
<member> const_int32 VK_INSERT 插入键,

</member>,
<member> const_int32 VK_DELETE 删除键,

</member>,
<member> const_int32 VK_PRIOR PRIOR键,

</member>,
<member> const_int32 VK_NEXT 下一个键,

</member>,
<member> const_int32 VK_UP 上箭头,

</member>,
<member> const_int32 VK_DOWN 下箭头,

</member>,
<member> const_int32 VK_LEFT 左箭头,

</member>,
<member> const_int32 VK_RIGHT 右箭头,

</member>,
