//[配置信息开始],
//[组件文件] SerialPort.M,
//[语言列表] c chinese,
//[组件名称] SerialPort SerialPort 串口,
//[资源占用] ,
//[控件属性] 字体,文字颜色,背景颜色,
//[元素子项] linksysconst_uint8 ID ID ID,
//[元素子项] interface_reader reader0 reader0 reader0,
//[元素子项] var_uint8 OS_EventFlag OS_EventFlag OS_EventFlag,
//[元素子项] event ReceiveEvent ReceiveEvent 存在有效数据时,
//[元素子项] linkconst_int32_9600 baud baud 波特率,
//[元素子项] var_int32 X X X,
//[元素子项] var_int32 Y Y Y,
//[元素子项] var_int32 Width Width 宽度,
//[元素子项] var_int32 Height Height 高度,
//[元素子项] var_int32 BackColor BackColor 背景颜色,
//[元素子项] var_int32 ForeColor ForeColor 文字颜色,
//[元素子项] function_void OS_init OS_init OS_init,
//[元素子项] function_void OS_thread OS_thread OS_thread,
//[元素子项] function_void_Astring print_string print_string+# 发送字符串+#,
//[元素子项] function_void_int32 print_number print_number+# 以字符串形式发送数字+#,
//[元素子项] function_void_int32 print_char print_char+# 发送字符+#,
//[元素子项] function_int32 get_data get_data 获取一个数据,
//[元素子项] function_bool is_ready is_ready 存在有效数据,
//[元素子项] function_void_int32 SetBaud SetBaud+# 设置波特率+#,
//[元素子项] function_void_Cstring SetValue SetValue+# 设置扩展参数+#,
//[元素子项] function_void_Cstring SetFont SetFont+# 设置字体+#,
//[配置信息结束],

unit SerialPort
{
	#include "SerialPort\$language$.txt"
	driver =
	#include "driver\SerialPort.txt"
}
