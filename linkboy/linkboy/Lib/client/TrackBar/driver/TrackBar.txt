
unit TrackBar
{
	link memory linka {} = #.linker.linka;
	
	//[i] linksysconst_uint8 ID;
	public const uint8 ID = 0;
	
	//[i] var_int32 X;
	public linka int32 X = #addr ID * 0x0100 + 0x01;
	//[i] var_int32 Y;
	public linka int32 Y = #addr ID * 0x0100 + 0x02;
	//[i] var_int32 Width;
	public linka int32 Width = #addr ID * 0x0100 + 0x03;
	//[i] var_int32 Height;
	public linka int32 Height = #addr ID * 0x0100 + 0x04;
	//[i] var_int32 BackColor;
	public linka int32 BackColor = #addr ID * 0x0100 + 0x05;
	//[i] var_int32 ForeColor;
	public linka int32 ForeColor = #addr ID * 0x0100 + 0x06;
	
	//[i] var_int32 Value;
	public linka int32 Value = #addr ID * 0x0100 + 0x08;
	//[i] linkvar_int32_0 MinValue;
	public linka int32 MinValue = #addr ID * 0x0100 + 0x21;
	//[i] linkvar_int32_100 MaxValue;
	public linka int32 MaxValue = #addr ID * 0x0100 + 0x22;
	
	//[i] var_uint8 OS_EventFlag;
	public uint8 OS_EventFlag;
	//[i] event ValueChanged;
	
	//---------------------------------------------------
	//[i] function_void OS_init;
	public void OS_init()
	{
		OS_EventFlag = 0;
	}
}


