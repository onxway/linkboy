//[配置信息开始],
//[组件文件] ControlPad.M,
//[语言列表] c chinese,
//[组件名称] ControlPad Form 窗体,
//[资源占用] ,
//[控件属性] 背景颜色,button+设置扩展属性,
//[元素子项] linksysconst_uint8 ID ID ID,
//[元素子项] var_int32 Width Width 宽度,
//[元素子项] var_int32 Height Height 高度,
//[元素子项] var_int32 BackColor BackColor 背景颜色,
//[元素子项] var_int32 MouseX MouseX 鼠标X坐标,
//[元素子项] var_int32 MouseY MouseY 鼠标Y坐标,
//[元素子项] var_uint8 OS_EventFlag OS_EventFlag OS_EventFlag,
//[元素子项] event press_event PressEvent 鼠标按下时,
//[元素子项] event up_event UpEvent 鼠标松开时,
//[元素子项] event move_event MoveEvent 鼠标移动时,
//[元素子项] function_void OS_init OS_init OS_init,
//[元素子项] function_bool MousePress MousePress 鼠标按下,
//[元素子项] function_void_Cstring SetImage SetImage+# 设置背景图片+#,
//[配置信息结束],

unit ControlPad
{
	#include "ControlPad\$language$.txt"
	driver =
	#include "driver\ControlPad.txt"
}
