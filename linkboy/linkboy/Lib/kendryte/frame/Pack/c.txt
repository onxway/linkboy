public link bit Width = driver.Width;
public link bit Height = driver.Height;
public link unit map {} = driver.map;
public link bit Color = driver.Color;
public link void OS_init(){} = driver.OS_init;
public link void Clear(){} = driver.clear;
public link void DrawPixel(bit n1,bit n2){} = driver.draw_pixel;
