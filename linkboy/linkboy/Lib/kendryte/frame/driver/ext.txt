
	//函数说明：LCD清屏函数
	//入口数据：无
	//返回值：  无
	void LCD_Clear(uint16 color)
	{
		uint16 w = (uint16)(uint)LCD_W;
		uint16 h = (uint16)(uint)LCD_H;
		LCD_Address_Set( 0, 0, w-1, h-1 );

		loop( w ) {
			loop( h ) {
				LCD_WR_DATA(color);
			}
		}
	}
	//==============================================
	//[i] function_void draw_pixel int32 int32;
	public void draw_pixel( int32 x, int32 y )
	{
		if( x < 0 || x >= LCD_W ) return;
		if( y < 0 || y >= LCD_H ) return;
		
		LCD_DrawPoint( (uint16)(uint)x, (uint16)(uint)y, s_ForColor );
	}
	//=================================================================================
	//函数说明：LCD显示点
	//入口数据：x,y   起始坐标
	//返回值：  无
	void LCD_DrawPoint( uint16 x, uint16 y, uint16 color )
	{
		LCD_Address_Set(x,y,x,y);//设置光标位置
		LCD_WR_DATA(color);
	}
	void set_refresh()
	{
	}
	void set_fore( uint16 c )
	{
	}



