
#include <system\arduino\K210-io.txt>

G0.my_bit = driver.my_bit;
public unit G0
{
	public const uint8 INDEX = 0;
	public link memory my_bit {}
	public my_bit bit D0_DIR = #addr 0x0000 + INDEX;
	public my_bit bit D0_OUT = #addr 0x0100 + INDEX;
	public my_bit bit D0_IN = #addr 0x0200 + INDEX;
	public my_bit bit D0_PUL = #addr 0x0300 + INDEX;
}
G1.my_bit = driver.my_bit;
public unit G1
{
	public const uint8 INDEX = 1;
	public link memory my_bit {}
	public my_bit bit D0_DIR = #addr 0x0000 + INDEX;
	public my_bit bit D0_OUT = #addr 0x0100 + INDEX;
	public my_bit bit D0_IN = #addr 0x0200 + INDEX;
	public my_bit bit D0_PUL = #addr 0x0300 + INDEX;
}
G2.my_bit = driver.my_bit;
public unit G2
{
	public const uint8 INDEX = 2;
	public link memory my_bit {}
	public my_bit bit D0_DIR = #addr 0x0000 + INDEX;
	public my_bit bit D0_OUT = #addr 0x0100 + INDEX;
	public my_bit bit D0_IN = #addr 0x0200 + INDEX;
	public my_bit bit D0_PUL = #addr 0x0300 + INDEX;
}
G3.my_bit = driver.my_bit;
public unit G3
{
	public const uint8 INDEX = 3;
	public link memory my_bit {}
	public my_bit bit D0_DIR = #addr 0x0000 + INDEX;
	public my_bit bit D0_OUT = #addr 0x0100 + INDEX;
	public my_bit bit D0_IN = #addr 0x0200 + INDEX;
	public my_bit bit D0_PUL = #addr 0x0300 + INDEX;
}
G4.my_bit = driver.my_bit;
public unit G4
{
	public const uint8 INDEX = 4;
	public link memory my_bit {}
	public my_bit bit D0_DIR = #addr 0x0000 + INDEX;
	public my_bit bit D0_OUT = #addr 0x0100 + INDEX;
	public my_bit bit D0_IN = #addr 0x0200 + INDEX;
	public my_bit bit D0_PUL = #addr 0x0300 + INDEX;
}
G5.my_bit = driver.my_bit;
public unit G5
{
	public const uint8 INDEX = 5;
	public link memory my_bit {}
	public my_bit bit D0_DIR = #addr 0x0000 + INDEX;
	public my_bit bit D0_OUT = #addr 0x0100 + INDEX;
	public my_bit bit D0_IN = #addr 0x0200 + INDEX;
	public my_bit bit D0_PUL = #addr 0x0300 + INDEX;
}
G6.my_bit = driver.my_bit;
public unit G6
{
	public const uint8 INDEX = 6;
	public link memory my_bit {}
	public my_bit bit D0_DIR = #addr 0x0000 + INDEX;
	public my_bit bit D0_OUT = #addr 0x0100 + INDEX;
	public my_bit bit D0_IN = #addr 0x0200 + INDEX;
	public my_bit bit D0_PUL = #addr 0x0300 + INDEX;
}
G7.my_bit = driver.my_bit;
public unit G7
{
	public const uint8 INDEX = 7;
	public link memory my_bit {}
	public my_bit bit D0_DIR = #addr 0x0000 + INDEX;
	public my_bit bit D0_OUT = #addr 0x0100 + INDEX;
	public my_bit bit D0_IN = #addr 0x0200 + INDEX;
	public my_bit bit D0_PUL = #addr 0x0300 + INDEX;
}
G8.my_bit = driver.my_bit;
public unit G8
{
	public const uint8 INDEX = 8;
	public link memory my_bit {}
	public my_bit bit D0_DIR = #addr 0x0000 + INDEX;
	public my_bit bit D0_OUT = #addr 0x0100 + INDEX;
	public my_bit bit D0_IN = #addr 0x0200 + INDEX;
	public my_bit bit D0_PUL = #addr 0x0300 + INDEX;
}
G9.my_bit = driver.my_bit;
public unit G9
{
	public const uint8 INDEX = 9;
	public link memory my_bit {}
	public my_bit bit D0_DIR = #addr 0x0000 + INDEX;
	public my_bit bit D0_OUT = #addr 0x0100 + INDEX;
	public my_bit bit D0_IN = #addr 0x0200 + INDEX;
	public my_bit bit D0_PUL = #addr 0x0300 + INDEX;
}
G10.my_bit = driver.my_bit;
public unit G10
{
	public const uint8 INDEX = 10;
	public link memory my_bit {}
	public my_bit bit D0_DIR = #addr 0x0000 + INDEX;
	public my_bit bit D0_OUT = #addr 0x0100 + INDEX;
	public my_bit bit D0_IN = #addr 0x0200 + INDEX;
	public my_bit bit D0_PUL = #addr 0x0300 + INDEX;
}
G11.my_bit = driver.my_bit;
public unit G11
{
	public const uint8 INDEX = 11;
	public link memory my_bit {}
	public my_bit bit D0_DIR = #addr 0x0000 + INDEX;
	public my_bit bit D0_OUT = #addr 0x0100 + INDEX;
	public my_bit bit D0_IN = #addr 0x0200 + INDEX;
	public my_bit bit D0_PUL = #addr 0x0300 + INDEX;
}
G12.my_bit = driver.my_bit;
public unit G12
{
	public const uint8 INDEX = 12;
	public link memory my_bit {}
	public my_bit bit D0_DIR = #addr 0x0000 + INDEX;
	public my_bit bit D0_OUT = #addr 0x0100 + INDEX;
	public my_bit bit D0_IN = #addr 0x0200 + INDEX;
	public my_bit bit D0_PUL = #addr 0x0300 + INDEX;
}
G13.my_bit = driver.my_bit;
public unit G13
{
	public const uint8 INDEX = 13;
	public link memory my_bit {}
	public my_bit bit D0_DIR = #addr 0x0000 + INDEX;
	public my_bit bit D0_OUT = #addr 0x0100 + INDEX;
	public my_bit bit D0_IN = #addr 0x0200 + INDEX;
	public my_bit bit D0_PUL = #addr 0x0300 + INDEX;
}
G14.my_bit = driver.my_bit;
public unit G14
{
	public const uint8 INDEX = 14;
	public link memory my_bit {}
	public my_bit bit D0_DIR = #addr 0x0000 + INDEX;
	public my_bit bit D0_OUT = #addr 0x0100 + INDEX;
	public my_bit bit D0_IN = #addr 0x0200 + INDEX;
	public my_bit bit D0_PUL = #addr 0x0300 + INDEX;
}
G15.my_bit = driver.my_bit;
public unit G15
{
	public const uint8 INDEX = 15;
	public link memory my_bit {}
	public my_bit bit D0_DIR = #addr 0x0000 + INDEX;
	public my_bit bit D0_OUT = #addr 0x0100 + INDEX;
	public my_bit bit D0_IN = #addr 0x0200 + INDEX;
	public my_bit bit D0_PUL = #addr 0x0300 + INDEX;
}
G16.my_bit = driver.my_bit;
public unit G16
{
	public const uint8 INDEX = 16;
	public link memory my_bit {}
	public my_bit bit D0_DIR = #addr 0x0000 + INDEX;
	public my_bit bit D0_OUT = #addr 0x0100 + INDEX;
	public my_bit bit D0_IN = #addr 0x0200 + INDEX;
	public my_bit bit D0_PUL = #addr 0x0300 + INDEX;
}
G17.my_bit = driver.my_bit;
public unit G17
{
	public const uint8 INDEX = 17;
	public link memory my_bit {}
	public my_bit bit D0_DIR = #addr 0x0000 + INDEX;
	public my_bit bit D0_OUT = #addr 0x0100 + INDEX;
	public my_bit bit D0_IN = #addr 0x0200 + INDEX;
	public my_bit bit D0_PUL = #addr 0x0300 + INDEX;
}
G18.my_bit = driver.my_bit;
public unit G18
{
	public const uint8 INDEX = 18;
	public link memory my_bit {}
	public my_bit bit D0_DIR = #addr 0x0000 + INDEX;
	public my_bit bit D0_OUT = #addr 0x0100 + INDEX;
	public my_bit bit D0_IN = #addr 0x0200 + INDEX;
	public my_bit bit D0_PUL = #addr 0x0300 + INDEX;
}
G19.my_bit = driver.my_bit;
public unit G19
{
	public const uint8 INDEX = 19;
	public link memory my_bit {}
	public my_bit bit D0_DIR = #addr 0x0000 + INDEX;
	public my_bit bit D0_OUT = #addr 0x0100 + INDEX;
	public my_bit bit D0_IN = #addr 0x0200 + INDEX;
	public my_bit bit D0_PUL = #addr 0x0300 + INDEX;
}
G20.my_bit = driver.my_bit;
public unit G20
{
	public const uint8 INDEX = 20;
	public link memory my_bit {}
	public my_bit bit D0_DIR = #addr 0x0000 + INDEX;
	public my_bit bit D0_OUT = #addr 0x0100 + INDEX;
	public my_bit bit D0_IN = #addr 0x0200 + INDEX;
	public my_bit bit D0_PUL = #addr 0x0300 + INDEX;
}
G21.my_bit = driver.my_bit;
public unit G21
{
	public const uint8 INDEX = 21;
	public link memory my_bit {}
	public my_bit bit D0_DIR = #addr 0x0000 + INDEX;
	public my_bit bit D0_OUT = #addr 0x0100 + INDEX;
	public my_bit bit D0_IN = #addr 0x0200 + INDEX;
	public my_bit bit D0_PUL = #addr 0x0300 + INDEX;
}
G22.my_bit = driver.my_bit;
public unit G22
{
	public const uint8 INDEX = 22;
	public link memory my_bit {}
	public my_bit bit D0_DIR = #addr 0x0000 + INDEX;
	public my_bit bit D0_OUT = #addr 0x0100 + INDEX;
	public my_bit bit D0_IN = #addr 0x0200 + INDEX;
	public my_bit bit D0_PUL = #addr 0x0300 + INDEX;
}
G23.my_bit = driver.my_bit;
public unit G23
{
	public const uint8 INDEX = 23;
	public link memory my_bit {}
	public my_bit bit D0_DIR = #addr 0x0000 + INDEX;
	public my_bit bit D0_OUT = #addr 0x0100 + INDEX;
	public my_bit bit D0_IN = #addr 0x0200 + INDEX;
	public my_bit bit D0_PUL = #addr 0x0300 + INDEX;
}
G24.my_bit = driver.my_bit;
public unit G24
{
	public const uint8 INDEX = 24;
	public link memory my_bit {}
	public my_bit bit D0_DIR = #addr 0x0000 + INDEX;
	public my_bit bit D0_OUT = #addr 0x0100 + INDEX;
	public my_bit bit D0_IN = #addr 0x0200 + INDEX;
	public my_bit bit D0_PUL = #addr 0x0300 + INDEX;
}
G25.my_bit = driver.my_bit;
public unit G25
{
	public const uint8 INDEX = 25;
	public link memory my_bit {}
	public my_bit bit D0_DIR = #addr 0x0000 + INDEX;
	public my_bit bit D0_OUT = #addr 0x0100 + INDEX;
	public my_bit bit D0_IN = #addr 0x0200 + INDEX;
	public my_bit bit D0_PUL = #addr 0x0300 + INDEX;
}
G26.my_bit = driver.my_bit;
public unit G26
{
	public const uint8 INDEX = 26;
	public link memory my_bit {}
	public my_bit bit D0_DIR = #addr 0x0000 + INDEX;
	public my_bit bit D0_OUT = #addr 0x0100 + INDEX;
	public my_bit bit D0_IN = #addr 0x0200 + INDEX;
	public my_bit bit D0_PUL = #addr 0x0300 + INDEX;
}
G27.my_bit = driver.my_bit;
public unit G27
{
	public const uint8 INDEX = 27;
	public link memory my_bit {}
	public my_bit bit D0_DIR = #addr 0x0000 + INDEX;
	public my_bit bit D0_OUT = #addr 0x0100 + INDEX;
	public my_bit bit D0_IN = #addr 0x0200 + INDEX;
	public my_bit bit D0_PUL = #addr 0x0300 + INDEX;
}
G28.my_bit = driver.my_bit;
public unit G28
{
	public const uint8 INDEX = 28;
	public link memory my_bit {}
	public my_bit bit D0_DIR = #addr 0x0000 + INDEX;
	public my_bit bit D0_OUT = #addr 0x0100 + INDEX;
	public my_bit bit D0_IN = #addr 0x0200 + INDEX;
	public my_bit bit D0_PUL = #addr 0x0300 + INDEX;
}
G29.my_bit = driver.my_bit;
public unit G29
{
	public const uint8 INDEX = 29;
	public link memory my_bit {}
	public my_bit bit D0_DIR = #addr 0x0000 + INDEX;
	public my_bit bit D0_OUT = #addr 0x0100 + INDEX;
	public my_bit bit D0_IN = #addr 0x0200 + INDEX;
	public my_bit bit D0_PUL = #addr 0x0300 + INDEX;
}
G30.my_bit = driver.my_bit;
public unit G30
{
	public const uint8 INDEX = 30;
	public link memory my_bit {}
	public my_bit bit D0_DIR = #addr 0x0000 + INDEX;
	public my_bit bit D0_OUT = #addr 0x0100 + INDEX;
	public my_bit bit D0_IN = #addr 0x0200 + INDEX;
	public my_bit bit D0_PUL = #addr 0x0300 + INDEX;
}
G31.my_bit = driver.my_bit;
public unit G31
{
	public const uint8 INDEX = 31;
	public link memory my_bit {}
	public my_bit bit D0_DIR = #addr 0x0000 + INDEX;
	public my_bit bit D0_OUT = #addr 0x0100 + INDEX;
	public my_bit bit D0_IN = #addr 0x0200 + INDEX;
	public my_bit bit D0_PUL = #addr 0x0300 + INDEX;
}
G32.my_bit = driver.my_bit;
public unit G32
{
	public const uint8 INDEX = 32;
	public link memory my_bit {}
	public my_bit bit D0_DIR = #addr 0x0000 + INDEX;
	public my_bit bit D0_OUT = #addr 0x0100 + INDEX;
	public my_bit bit D0_IN = #addr 0x0200 + INDEX;
	public my_bit bit D0_PUL = #addr 0x0300 + INDEX;
}
G33.my_bit = driver.my_bit;
public unit G33
{
	public const uint8 INDEX = 33;
	public link memory my_bit {}
	public my_bit bit D0_DIR = #addr 0x0000 + INDEX;
	public my_bit bit D0_OUT = #addr 0x0100 + INDEX;
	public my_bit bit D0_IN = #addr 0x0200 + INDEX;
	public my_bit bit D0_PUL = #addr 0x0300 + INDEX;
}
G34.my_bit = driver.my_bit;
public unit G34
{
	public const uint8 INDEX = 34;
	public link memory my_bit {}
	public my_bit bit D0_DIR = #addr 0x0000 + INDEX;
	public my_bit bit D0_OUT = #addr 0x0100 + INDEX;
	public my_bit bit D0_IN = #addr 0x0200 + INDEX;
	public my_bit bit D0_PUL = #addr 0x0300 + INDEX;
}
G35.my_bit = driver.my_bit;
public unit G35
{
	public const uint8 INDEX = 35;
	public link memory my_bit {}
	public my_bit bit D0_DIR = #addr 0x0000 + INDEX;
	public my_bit bit D0_OUT = #addr 0x0100 + INDEX;
	public my_bit bit D0_IN = #addr 0x0200 + INDEX;
	public my_bit bit D0_PUL = #addr 0x0300 + INDEX;
}
G36.my_bit = driver.my_bit;
public unit G36
{
	public const uint8 INDEX = 36;
	public link memory my_bit {}
	public my_bit bit D0_DIR = #addr 0x0000 + INDEX;
	public my_bit bit D0_OUT = #addr 0x0100 + INDEX;
	public my_bit bit D0_IN = #addr 0x0200 + INDEX;
	public my_bit bit D0_PUL = #addr 0x0300 + INDEX;
}
G37.my_bit = driver.my_bit;
public unit G37
{
	public const uint8 INDEX = 37;
	public link memory my_bit {}
	public my_bit bit D0_DIR = #addr 0x0000 + INDEX;
	public my_bit bit D0_OUT = #addr 0x0100 + INDEX;
	public my_bit bit D0_IN = #addr 0x0200 + INDEX;
	public my_bit bit D0_PUL = #addr 0x0300 + INDEX;
}
G38.my_bit = driver.my_bit;
public unit G38
{
	public const uint8 INDEX = 38;
	public link memory my_bit {}
	public my_bit bit D0_DIR = #addr 0x0000 + INDEX;
	public my_bit bit D0_OUT = #addr 0x0100 + INDEX;
	public my_bit bit D0_IN = #addr 0x0200 + INDEX;
	public my_bit bit D0_PUL = #addr 0x0300 + INDEX;
}
G39.my_bit = driver.my_bit;
public unit G39
{
	public const uint8 INDEX = 39;
	public link memory my_bit {}
	public my_bit bit D0_DIR = #addr 0x0000 + INDEX;
	public my_bit bit D0_OUT = #addr 0x0100 + INDEX;
	public my_bit bit D0_IN = #addr 0x0200 + INDEX;
	public my_bit bit D0_PUL = #addr 0x0300 + INDEX;
}
















