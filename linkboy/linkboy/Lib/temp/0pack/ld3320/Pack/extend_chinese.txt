<module>
本套装包含一个语音识别主控板, 可通过编程设置识别词, 实现识别到指定单词后编程控制LED灯亮灭, 舵机控制开关门等, 本模块支持中英文单词识别, 可快速构建人工智能语音控制作品
</module>
<name> Pack 人工智能(语音识别版)套装,
</name>
<member> function_void base 基础类,
主控板，数据线，杜邦线，四节电池盒
</member>,
<member> function_void input 输入类,
按键，光敏电阻传感器，震动传感器，红外避障传感器，火焰传感器，红外遥控器，红外接收器，超声波测距模块，温度传感器，温湿度传感器
</member>,
<member> function_void output 输出类,
LED灯，RGB彩灯，蜂鸣器，扬声器，4位数码管，点阵屏幕，LCD1602屏幕，马达模块，舵机，步进电机和驱动器，灯环
</member>,
