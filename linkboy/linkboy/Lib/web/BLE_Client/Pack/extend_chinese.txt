<module>

</module>
<name> Pack BLE客户端,
</name>
<member> function_void OS_init OS_init,

</member>,
<member> function_void_Cstring BLE_begin 开始运行 #,
以指定的名字开启BLE客户端
</member>,
<member> function_void_int32 SendData 发送数据 #,
发送的数据应在0-255之间
</member>,
<member> function_bool Available 存在数据,
如果蓝牙服务端发来的数据未被读取完, 属性为真
</member>,
<member> function_int32 GetData 读取数据,
从蓝牙服务端发来的数据中读取一个数据, 使用本指令之前应判断<存在数据>为真
</member>,
