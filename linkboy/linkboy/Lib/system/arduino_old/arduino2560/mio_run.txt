

unit D0
{
	public link bit D0_DIR = #.COM_MCU.DDRD.0; public link bit D0_IN = #.COM_MCU.PIND.0; public link bit D0_OUT = #.COM_MCU.PORTD.0; public link bit D0_PUL = #.COM_MCU.PORTD.0;
}
unit D1
{
	public link bit D0_DIR = #.COM_MCU.DDRD.1; public link bit D0_IN = #.COM_MCU.PIND.1; public link bit D0_OUT = #.COM_MCU.PORTD.1; public link bit D0_PUL = #.COM_MCU.PORTD.1;
}
unit D2
{
	public link bit D0_DIR = #.COM_MCU.DDRD.2; public link bit D0_IN = #.COM_MCU.PIND.2; public link bit D0_OUT = #.COM_MCU.PORTD.2; public link bit D0_PUL = #.COM_MCU.PORTD.2;
}
unit D3
{
	public link bit D0_DIR = #.COM_MCU.DDRD.3; public link bit D0_IN = #.COM_MCU.PIND.3; public link bit D0_OUT = #.COM_MCU.PORTD.3; public link bit D0_PUL = #.COM_MCU.PORTD.3;
}
unit D4
{
	public link bit D0_DIR = #.COM_MCU.DDRD.4; public link bit D0_IN = #.COM_MCU.PIND.4; public link bit D0_OUT = #.COM_MCU.PORTD.4; public link bit D0_PUL = #.COM_MCU.PORTD.4;
}
unit D5
{
	public link bit D0_DIR = #.COM_MCU.DDRD.5; public link bit D0_IN = #.COM_MCU.PIND.5; public link bit D0_OUT = #.COM_MCU.PORTD.5; public link bit D0_PUL = #.COM_MCU.PORTD.5;
}
unit D6
{
	public link bit D0_DIR = #.COM_MCU.DDRD.6; public link bit D0_IN = #.COM_MCU.PIND.6; public link bit D0_OUT = #.COM_MCU.PORTD.6; public link bit D0_PUL = #.COM_MCU.PORTD.6;
}
unit D7
{
	public link bit D0_DIR = #.COM_MCU.DDRD.7; public link bit D0_IN = #.COM_MCU.PIND.7; public link bit D0_OUT = #.COM_MCU.PORTD.7; public link bit D0_PUL = #.COM_MCU.PORTD.7;
}
unit D8
{
	public link bit D0_DIR = #.COM_MCU.DDRB.0; public link bit D0_IN = #.COM_MCU.PINB.0; public link bit D0_OUT = #.COM_MCU.PORTB.0; public link bit D0_PUL = #.COM_MCU.PORTB.0;
}
unit D9
{
	public link bit D0_DIR = #.COM_MCU.DDRB.1; public link bit D0_IN = #.COM_MCU.PINB.1; public link bit D0_OUT = #.COM_MCU.PORTB.1; public link bit D0_PUL = #.COM_MCU.PORTB.1;
}
unit D10
{
	public link bit D0_DIR = #.COM_MCU.DDRB.2; public link bit D0_IN = #.COM_MCU.PINB.2; public link bit D0_OUT = #.COM_MCU.PORTB.2; public link bit D0_PUL = #.COM_MCU.PORTB.2;
}
unit D11
{
	public link bit D0_DIR = #.COM_MCU.DDRB.3; public link bit D0_IN = #.COM_MCU.PINB.3; public link bit D0_OUT = #.COM_MCU.PORTB.3; public link bit D0_PUL = #.COM_MCU.PORTB.3;
}
unit D12
{
	public link bit D0_DIR = #.COM_MCU.DDRB.4; public link bit D0_IN = #.COM_MCU.PINB.4; public link bit D0_OUT = #.COM_MCU.PORTB.4; public link bit D0_PUL = #.COM_MCU.PORTB.4;
}
unit D13
{
	public link bit D0_DIR = #.COM_MCU.DDRB.5; public link bit D0_IN = #.COM_MCU.PINB.5; public link bit D0_OUT = #.COM_MCU.PORTB.5; public link bit D0_PUL = #.COM_MCU.PORTB.5;
}
//AD��
unit A0
{
	public link bit D0_DIR = #.COM_MCU.DDRC.0; public link bit D0_IN = #.COM_MCU.PINC.0; public link bit D0_OUT = #.COM_MCU.PORTC.0; public link bit D0_PUL = #.COM_MCU.PORTC.0;
}
unit A1
{
	public link bit D0_DIR = #.COM_MCU.DDRC.1; public link bit D0_IN = #.COM_MCU.PINC.1; public link bit D0_OUT = #.COM_MCU.PORTC.1; public link bit D0_PUL = #.COM_MCU.PORTC.1;
}
unit A2
{
	public link bit D0_DIR = #.COM_MCU.DDRC.2; public link bit D0_IN = #.COM_MCU.PINC.2; public link bit D0_OUT = #.COM_MCU.PORTC.2; public link bit D0_PUL = #.COM_MCU.PORTC.2;
}
unit A3
{
	public link bit D0_DIR = #.COM_MCU.DDRC.3; public link bit D0_IN = #.COM_MCU.PINC.3; public link bit D0_OUT = #.COM_MCU.PORTC.3; public link bit D0_PUL = #.COM_MCU.PORTC.3;
}
unit A4
{
	public link bit D0_DIR = #.COM_MCU.DDRC.4; public link bit D0_IN = #.COM_MCU.PINC.4; public link bit D0_OUT = #.COM_MCU.PORTC.4; public link bit D0_PUL = #.COM_MCU.PORTC.4;
}
unit A5
{
	public link bit D0_DIR = #.COM_MCU.DDRC.5; public link bit D0_IN = #.COM_MCU.PINC.5; public link bit D0_OUT = #.COM_MCU.PORTC.5; public link bit D0_PUL = #.COM_MCU.PORTC.5;
}
unit A6
{
	public link bit D0_DIR = #.COM_MCU.DDRC.6; public link bit D0_IN = #.COM_MCU.PINC.6; public link bit D0_OUT = #.COM_MCU.PORTC.6; public link bit D0_PUL = #.COM_MCU.PORTC.6;
}
unit A7
{
	public link bit D0_DIR = #.COM_MCU.DDRC.7; public link bit D0_IN = #.COM_MCU.PINC.7; public link bit D0_OUT = #.COM_MCU.PORTC.7; public link bit D0_PUL = #.COM_MCU.PORTC.7;
}



