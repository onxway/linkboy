
//������չ�洢��
unit xdata [ uint16 * 8 ][ 0, 65535 ]
{
	interface void set( uint16 addr, uint8 data )
	{
		#asm "mov dpl,&addr"
		#asm "mov dph,&addr+1"
		#asm "mov a,&data"
		#asm "movx @dptr,a"
	}
	interface uint8 get( uint16 addr )
	{
		#asm "mov dpl,&addr"
		#asm "mov dph,&addr+1"
		#asm "movx a,@dptr"
	}
}
