
unit idata_pack
{
	public vdata user
	{
		unitlink = idata;
		addrtype = uint8;
		datatype = uint8;
		addrsize = [128,255];
	}
	unit idata
	{
		//uint8
		public void set_uint8( uint8 addr, uint8 data )
		{
			#asm "mov r0,&addr"
			#asm "mov a,&data"
			#asm "mov @r0,a"
		}
		public uint8 get_uint8( uint8 addr )
		{
			#asm "mov r0,&addr"
			#asm "mov a,@r0"
		}
		//uint16
		public void set_uint16( uint8 addr, uint16 data )
		{
			#asm "mov r0,&addr"
			#asm "mov a,&data"
			#asm "mov @r0,a"
			#asm "inc r0"
			#asm "mov a,&data+1"
			#asm "mov @r0,a"
		}
		public uint16 get_uint16( uint8 addr )
		{
			#asm "mov r0,&addr"
			#asm "mov a,@r0"
			#asm "mov b,a"
			#asm "inc r0"
			#asm "mov a,@r0"
			#asm "xch a,b"
		}
	}
}


