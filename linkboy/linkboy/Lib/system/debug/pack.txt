
unit Pack
{
	public const uint16 ID = 0;

	const int16 max_length = 7;
	[int8*max_length] buffer;
	int16 length;
	int16 start;
	int16 cindex;
	
	public const int32 XTAL = $freq$;
	
	uint32 UD32;
	
	//加载芯片底层代码
	#include "$chip$_UART0.txt"
	
	
	//----------------------------------------------------
	//串口初始化,波特率:9600,8个数据位,1个停止位,没有奇偶校验
	public void OS_init()
	{
		Debug_Init();
		
		//设置默认波特率
		set_baud( 115200 );
		
		length = 0;
		start = 0;
		cindex = 0;
	}
	//---------------------------------------------------
	//扫描数据
	public void OS_thread()
	{
		//...
	}
	
	//发送一个数据协议包
	public void send_data( uint8 i, int32 d )
	{
		send_byte( 0xAA );
		send_byte( i );
		send_int32( d );
		
		uint8 sum = i + UD32.0(uint8) + UD32.8(uint8) + UD32.16(uint8) + UD32.24(uint8);
		send_byte( sum );
	}
	
	
	//=================================================================================
	
	
	//---------------------------------------------------
	//发送一个字节
	public void send_byte( uint8 data )
	{
		WAIT_SEND_OK();
		UDR = data;
	}
	//---------------------------------------------------
	//发送一个字符
	public void send_int32( int32 data )
	{
		UD32 = (uint)data;
		send_byte( UD32.0(uint8) );
		send_byte( UD32.8(uint8) );
		send_byte( UD32.16(uint8) );
		send_byte( UD32.24(uint8) );
	}
	
	
	
	//数据接收中断
	INTERRUPT_UART
	void RXC()
	{
		//必须在中断程序中读取 UDR,否则在退出后会重新进入中断
		buffer[length] = (int)UDR;
		length += 1;
		length %= max_length;
	}
}











