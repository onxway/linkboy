
###########################################################
#空类型
class NoneType():
	pass
###########################################################
#Int类型
class Int():
	<sys> innercall
	def __neg__(self):
		py.PYVM.Lib_Int_neg()
	<sys> innercall
	def __add__(self, v):
		py.PYVM.Lib_Int_Add()
	<sys> innercall
	def __sub__(self, v):
		py.PYVM.Lib_Int_Sub()
	<sys> innercall
	def __power__(self, v):
		py.PYVM.Lib_Int_Power()
	<sys> innercall
	def __mul__(self, v):
		py.PYVM.Lib_Int_Mul()
	#<sys> innercall
	#def __div__(self, v):
	#	py.PYVM.Lib_Int_Div()
	<sys> innercall
	def __d_div__(self, v):
		py.PYVM.Lib_Int_D_Div()
	<sys> innercall
	def __mod__(self, v):
		py.PYVM.Lib_Int_Mod()

	<sys> innercall
	def __inp_add__(self, v):
		py.PYVM.Lib_Int_InpAdd()
	<sys> innercall
	def __inp_sub__(self, v):
		py.PYVM.Lib_Int_InpSub()
	<sys> innercall
	def __inp_power__(self, v):
		py.PYVM.Lib_Int_InpPower()
	<sys> innercall
	def __inp_mul__(self, v):
		py.PYVM.Lib_Int_InpMul()
	<sys> innercall
	def __inp_d_div__(self, v):
		py.PYVM.Lib_Int_InpD_Div()
	<sys> innercall
	def __inp_mod__(self, v):
		py.PYVM.Lib_Int_InpMod()

	<sys> innercall
	def __cmp_le__(self, v):
		py.PYVM.Lib_Int_CmpLE()
	<sys> innercall
	def __cmp_se__(self, v):
		py.PYVM.Lib_Int_CmpSE()
	<sys> innercall
	def __cmp_l__(self, v):
		py.PYVM.Lib_Int_CmpL()
	<sys> innercall
	def __cmp_s__(self, v):
		py.PYVM.Lib_Int_CmpS()
	<sys> innercall
	def __cmp_e__(self, v):
		py.PYVM.Lib_Int_CmpE()
	<sys> innercall
	def __cmp_ne__(self, v):
		py.PYVM.Lib_Int_CmpNE()
###########################################################
#Bool类型
class Bool():
	pass
###########################################################
#String类型
class String():
	<sys> innercall
	def __add__(self, v):
		py.PYVM.Lib_String_Add()
	<sys> innercall
	def __inp_add__(self, v):
		py.PYVM.Lib_String_InpAdd()
###########################################################
#List类型
class List():
	<sys> innercall
	def __inp_add__(self, v):
		py.PYVM.Lib_List_Append()
	<sys> innercall
	def append(self, v):
		py.PYVM.Lib_List_Append()

	<sys> innercall
	def __iter__(self):
		py.PYVM.Lib_List_Iter()
		return self
	<sys> innercall
	def __next__(self):
		py.PYVM.Lib_List_Next()
	<sys> innercall
	def __getitem__(self, v):
		py.PYVM.Lib_List_GetItem()
	<sys> innercall
	def __setitem__(self, i, v):
		py.PYVM.Lib_List_SetItem()
###########################################################
#range迭代器
class range():
	<sys> innercall
	def __init__(self,v_start,v_end,v_step):
		py.PYVM.Lib_Range_Init()
		return self
	<sys> innercall
	def __iter__(self):
		py.PYVM.Lib_Range_Iter()
		return self
	<sys> innercall
	def __next__(self):
		py.PYVM.Lib_Range_Next()
###########################################################
#系统内置函数

<sys> innercall
def itype( o ):
	py.PYVM.Lib_itype()

###########################################################
<sys> addline

