

	//[i] function_void pinMode int32 int32;
	void pinMode( int32 p, int32 m )
	{
		if( p > 31 || p < 0 ) {
			return;
		}
		bit b;
		if( m != 0 ) {
			b = 1;
		}
		else {
			b = 0;
		}
		switch( (uint8)(uint16)(uint)p ) {
			case 0: A0.D0_DIR = b; break;
			case 1: A1.D0_DIR = b; break;
			case 2: A2.D0_DIR = b; break;
			case 3: A3.D0_DIR = b; break;
			case 4: A4.D0_DIR = b; break;
			case 5: A5.D0_DIR = b; break;
			case 6: A6.D0_DIR = b; break;
			case 7: A7.D0_DIR = b; break;
			case 8: A8.D0_DIR = b; break;
			case 9: A9.D0_DIR = b; break;

			case 10: A10.D0_DIR = b; break;
			case 11: A11.D0_DIR = b; break;
			case 12: A12.D0_DIR = b; break;
			case 13: A13.D0_DIR = b; break;
			case 14: A14.D0_DIR = b; break;
			case 15: A15.D0_DIR = b; break;
			case 16: A16.D0_DIR = b; break;
			case 17: A17.D0_DIR = b; break;
			case 18: A18.D0_DIR = b; break;
			case 19: A19.D0_DIR = b; break;

			case 20: A20.D0_DIR = b; break;
			case 21: A21.D0_DIR = b; break;
			case 22: A22.D0_DIR = b; break;
			case 23: A23.D0_DIR = b; break;
			case 24: A24.D0_DIR = b; break;
			case 25: A25.D0_DIR = b; break;
			case 26: A26.D0_DIR = b; break;
			case 27: A27.D0_DIR = b; break;
			case 28: A28.D0_DIR = b; break;
			case 29: A29.D0_DIR = b; break;

			case 30: A30.D0_DIR = b; break;
			case 31: A31.D0_DIR = b; break;

			default: break;
		}
	}
	//[i] function_void digitalWrite int32 int32;
	void digitalWrite( int32 p, int32 d )
	{
		if( p > 31 || p < 0 ) {
			return;
		}
		bit b;
		if( d != 0 ) {
			b = 1;
		}
		else {
			b = 0;
		}
		switch( (uint8)(uint16)(uint)p ) {
			case 0: A0.D0_OUT = b; break;
			case 1: A1.D0_OUT = b; break;
			case 2: A2.D0_OUT = b; break;
			case 3: A3.D0_OUT = b; break;
			case 4: A4.D0_OUT = b; break;
			case 5: A5.D0_OUT = b; break;
			case 6: A6.D0_OUT = b; break;
			case 7: A7.D0_OUT = b; break;
			case 8: A8.D0_OUT = b; break;
			case 9: A9.D0_OUT = b; break;

			case 10: A10.D0_OUT = b; break;
			case 11: A11.D0_OUT = b; break;
			case 12: A12.D0_OUT = b; break;
			case 13: A13.D0_OUT = b; break;
			case 14: A14.D0_OUT = b; break;
			case 15: A15.D0_OUT = b; break;
			case 16: A16.D0_OUT = b; break;
			case 17: A17.D0_OUT = b; break;
			case 18: A18.D0_OUT = b; break;
			case 19: A19.D0_OUT = b; break;

			case 20: A20.D0_OUT = b; break;
			case 21: A21.D0_OUT = b; break;
			case 22: A22.D0_OUT = b; break;
			case 23: A23.D0_OUT = b; break;
			case 24: A24.D0_OUT = b; break;
			case 25: A25.D0_OUT = b; break;
			case 26: A26.D0_OUT = b; break;
			case 27: A27.D0_OUT = b; break;
			case 28: A28.D0_OUT = b; break;
			case 29: A29.D0_OUT = b; break;

			case 30: A30.D0_OUT = b; break;
			case 31: A31.D0_OUT = b; break;

			default: break;
		}
	}
	//[i] function_int32 digitalRead int32;
	int32 digitalRead( int32 p )
	{
		if( p > 31 || p < 0 ) {
			return 0;
		}
		bit b;
		switch( (uint8)(uint16)(uint)p ) {
			case 0: b = A0.D0_IN; break;
			case 1: b = A1.D0_IN; break;
			case 2: b = A2.D0_IN; break;
			case 3: b = A3.D0_IN; break;
			case 4: b = A4.D0_IN; break;
			case 5: b = A5.D0_IN; break;
			case 6: b = A6.D0_IN; break;
			case 7: b = A7.D0_IN; break;
			case 8: b = A8.D0_IN; break;
			case 9: b = A9.D0_IN; break;

			case 10: b = A10.D0_IN; break;
			case 11: b = A11.D0_IN; break;
			case 12: b = A12.D0_IN; break;
			case 13: b = A13.D0_IN; break;
			case 14: b = A14.D0_IN; break;
			case 15: b = A15.D0_IN; break;
			case 16: b = A16.D0_IN; break;
			case 17: b = A17.D0_IN; break;
			case 18: b = A18.D0_IN; break;
			case 19: b = A19.D0_IN; break;

			case 20: b = A20.D0_IN; break;
			case 21: b = A21.D0_IN; break;
			case 22: b = A22.D0_IN; break;
			case 23: b = A23.D0_IN; break;
			case 24: b = A24.D0_IN; break;
			case 25: b = A25.D0_IN; break;
			case 26: b = A26.D0_IN; break;
			case 27: b = A27.D0_IN; break;
			case 28: b = A28.D0_IN; break;
			case 29: b = A29.D0_IN; break;

			case 30: b = A30.D0_IN; break;
			case 31: b = A31.D0_IN; break;

			default: break;
		}
		if( b == 0 ) {
			return 0;
		}
		else {
			return 1;
		}
		return 0; //这里仅为语法合规
	}







