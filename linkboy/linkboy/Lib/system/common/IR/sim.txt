	
	int8 LastData;
	
	void RunInit()
	{
		LastData = 0;
	}
	
	void OS_run()
	{
		int8 KeyList = (int8)(int16)#.OS.REMO_ModuleRead( (int)(ID * 0x00010000 + 0x0000) );
		if( LastData != KeyList ) {
			LastData = KeyList;
			if( KeyList == -1 ) {
				OS_EventFlag.1(bit) = 1;
				is_press = false;
				data = NONE;
			}
			else {
				SetFlag( KeyList );
			}
		}
	}
	
	