
//等待数据发送成功
#define WAIT_SEND_OK(); while( #.MEGA328.UCSR0A.5(bit) == 0 ) {} #.MEGA328.UCSR0A.5(bit) = 1;

//等待接收到数据
#define WAIT_RECEIVE(); while( #.MEGA328.UCSR0A.7(bit) == 0 ) {}

//定义串口数据寄存器
#define UDR #.MEGA328.UDR0

//定义串口中断源
#define INTERRUPT_UART interrupt [#.MEGA328.WATCH.USART_RX]

//设置波特率
void set_baud( int32 t_baud )
{
	//7 = 1: 清空发送标志
	//5 = 1: 发送就绪
	#.MEGA328.UCSR0A = 0b0110_0010;
	//7 = 1: 使能发送中断
	//4 = 1: 允许接收
	//3 = 1: 允许发送
	#.MEGA328.UCSR0B = 0b1001_1000;
	#.MEGA328.UCSR0C = 0b0000_0110;
	
	//Baud = (Fosc/16) / (UBRR + 1)
	//UBRR = (Fosc/16) / Boud - 1
	
	//uint32 X = 11059200;
	//uint32 X = 12000000;

	uint16 UBRR = (uint16)(uint)(XTAL / 800) / (uint16)((uint)t_baud / 100) - 1;
	#.MEGA328.UBRR0H = UBRR.8(uint8);
	#.MEGA328.UBRR0L = UBRR.0(uint8);
}




