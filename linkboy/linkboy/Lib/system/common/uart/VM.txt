
	const int32 DEVICE = 19;
	interface[0x0100 * DEVICE + 0x00] void VEX_init( int8 id, []uint8 v_buffer, int16 v_max_length, int16 v_length* ){}
	interface[0x0100 * DEVICE + 0x01] void VEX_set_baud( int8 id, int32 b ){}
	interface[0x0100 * DEVICE + 0x02] void VEX_print_char( int8 id, uint8 data ){}
	
	int8 UID;
	
	//IO初始化
	void IO_init()
	{
		if( UART_ID != -99 ) {
			UID = UART_ID;
		}
		else {
			UID = UART_NUMBER;
		}
		VEX_init( UID, buffer, max_length, length );
	}
	//设置波特率
	void set_baud( int32 t_baud )
	{
		VEX_set_baud( UID, t_baud );
	}
	//发送一个字节
	public void print_byte( int8 data )
	{
		VEX_print_char( UID, (uint)data );
	}
	//数据接收中断
	void DataTest()
	{
		
	}

	