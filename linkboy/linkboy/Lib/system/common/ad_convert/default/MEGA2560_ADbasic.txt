
//2016.4.4
//发现了一个BUG, 小于4分频, 会导致AD转换出现问题

//2016.4.9
//发现加上DIDR0寄存器设置定后干扰了按键端口, 不知道为何, 于是去掉了.

unit ADbasic
{
	public const uint16 ID = 0;
	
	public link unit PORT {}
	const uint8 channel = PORT.AD_INDEX;
	
	public const int32 MinValue = 0;
	public const int32 MaxValue = 1024;
	
	//---------------------------------------------------
	//[i] function_void OS_init;
	public void OS_init()
	{
		PORT.D0_DIR = 0;
		PORT.D0_OUT = 0;
		#.MEGA2560.ADCSRA = 0b1000_0011;  //8分频
		#.MEGA2560.ADMUX = 0b0100_0000;
		
		//uint8 BIT = 0x01;
		//#.MEGA2560.DIDR0 = #.MEGA2560.DIDR0 | (BIT << 1);
	}
	//---------------------------------------------------
	//[i] function_int32 get_data;
	public int32 get_data()
	{
		uint8 nc = channel & 0x07;
		if( channel > 7 ) {
			#.MEGA2560.ADCSRB.3(bit) = 1;
		}
		else {
			#.MEGA2560.ADCSRB.3(bit) = 0;
		}
		#.MEGA2560.ADMUX = 0b0100_0000 + nc;
		#.MEGA2560.ADCSRA.6(bit) = 1;
		while( #.MEGA2560.ADCSRA.6(bit) == 1 ) {}
		
		uint16 data;
		data.0(uint8) = #.MEGA2560.ADCL;
		data.8(uint8) = #.MEGA2560.ADCH;
		
		//丢弃此结果
		//uint32 d = data;
		//return MinValue + (int)d * (MaxValue - MinValue) / 1024;
		
		#.MEGA2560.ADMUX = 0b0100_0000 + nc;
		#.MEGA2560.ADCSRA.6(bit) = 1;
		while( #.MEGA2560.ADCSRA.6(bit) == 1 ) {}
		
		data.0(uint8) = #.MEGA2560.ADCL;
		data.8(uint8) = #.MEGA2560.ADCH;
		
		uint32 d = data;
		return MinValue + (int)d * (MaxValue - MinValue + 1) / 1024;
	}
}





