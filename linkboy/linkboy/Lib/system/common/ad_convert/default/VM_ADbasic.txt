
unit ADbasic
{
	public const uint16 ID = 0;
	
	public link unit PORT {}
	
	public const int32 MinValue = 0;
	public const int32 MaxValue = 1000;
	
	#include "$run$.txt"
	
	//---------------------------------------------------
	//[i] function_void OS_init;
	public void OS_init()
	{
		PORT.D0_DIR = 0;

		//通过写入数据, 触发初始化
		ADvalue = 0;
	}
	//---------------------------------------------------
	//[i] function_int32 get_data;
	public int32 get_data()
	{
		return MinValue + ADvalue * (MaxValue - MinValue + 1) / 1024;
	}
}





