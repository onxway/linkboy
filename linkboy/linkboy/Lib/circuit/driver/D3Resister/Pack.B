//[配置信息开始],
//[语言列表] c chinese,
//[组件名称] Pack Resister 滑动电阻,
//[模块型号] ,
//[资源占用] ,
//[仿真类型] 1,
//[元素子项] var_fix U1 U1 U1,
//[元素子项] var_fix I1 I1 I1,
//[元素子项] var_fix U2 U2 U2,
//[元素子项] var_fix I2 I2 I2,
//[元素子项] var_fix U3 U3 U3,
//[元素子项] var_fix I3 I3 I3,
//[元素子项] linkconst_fix_10.0_KΩ MaxR MaxR 总电阻,
//[元素子项] linkconst_fix_5.0_KΩ InitR InitR 初始电阻,
//[元素子项] function_void OS_init OS_init OS_init,
//[元素子项] function_void OS_thread OS_thread OS_thread,
//[配置信息结束],

unit Pack
{
	#include "Pack\$language$.txt"
	driver =
	#include "driver\Pack.txt"
}
