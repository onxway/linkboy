
unit Pack
{
	public const uint16 ID = 0;
	
	public const int16 CC_ID = 0;
	link unit S_C {} = #.SYS_CTCOM;

	//[i] var_fix U1;
	public ->fix U1;
	//[i] var_fix I1;
	public ->fix I1;
	
	//[i] var_fix U2;
	public ->fix U2;
	//[i] var_fix I2;
	public ->fix I2;
	
	//[i] var_fix U3;
	public ->fix U3;
	//[i] var_fix I3;
	public ->fix I3;
	
	//[i] var_fix U4;
	public ->fix U4;
	//[i] var_fix I4;
	public ->fix I4;

	//[i] var_fix U5;
	public ->fix U5;
	//[i] var_fix I5;
	public ->fix I5;
	
	//[i] var_fix U6;
	public ->fix U6;
	//[i] var_fix I6;
	public ->fix I6;
	
	//[i] var_fix U7;
	public ->fix U7;
	//[i] var_fix I7;
	public ->fix I7;

	//[i] var_fix U8;
	public ->fix U8;
	//[i] var_fix I8;
	public ->fix I8;
	
	//[i] var_fix U9;
	public ->fix U9;
	//[i] var_fix I9;
	public ->fix I9;

	//---------------------------------------------------
	//[i] function_void OS_init;
	public void OS_init()
	{
		U1 -> S_C.ElmList[CC_ID][S_C.EV1]; I1 -> S_C.ElmList[CC_ID][S_C.EI1];
		U2 -> S_C.ElmList[CC_ID][S_C.EV2]; I2 -> S_C.ElmList[CC_ID][S_C.EI2];
		U3 -> S_C.ElmList[CC_ID][S_C.EV3]; I3 -> S_C.ElmList[CC_ID][S_C.EI3];
		U4 -> S_C.ElmList[CC_ID][S_C.EV4]; I4 -> S_C.ElmList[CC_ID][S_C.EI4];
		U5 -> S_C.ElmList[CC_ID][S_C.EV5]; I5 -> S_C.ElmList[CC_ID][S_C.EI5];
		U6 -> S_C.ElmList[CC_ID][S_C.EV6]; I6 -> S_C.ElmList[CC_ID][S_C.EI6];
		U7 -> S_C.ElmList[CC_ID][S_C.EV7]; I7 -> S_C.ElmList[CC_ID][S_C.EI7];
		U8 -> S_C.ElmList[CC_ID][S_C.EV8]; I8 -> S_C.ElmList[CC_ID][S_C.EI8];

		U9 -> S_C.ElmList[CC_ID][S_C.EV9]; I9 -> S_C.ElmList[CC_ID][S_C.EI9];
	}
}


