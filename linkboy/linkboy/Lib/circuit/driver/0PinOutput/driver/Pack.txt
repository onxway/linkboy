
unit Input
{
	public const uint16 ID = 0;
	
	public const int16 CC_ID = 0;
	link unit S_C {} = #.SYS_CTCOM;

	//[i] var_fix U1;
	public ->fix U1;

	//[i] var_fix I1;
	public ->fix I1;

	public ->fix key;

	#include "$run$.txt"
	
	//---------------------------------------------------
	//[i] function_void OS_init;
	public void OS_init()
	{
		D0_DIR = 1;
		D0_OUT = 0;
		
		U1 -> S_C.ElmList[CC_ID][S_C.EV1]; I1 -> S_C.ElmList[CC_ID][S_C.EI1];
		key -> S_C.ElmList[CC_ID][S_C.X1];
	}
	//---------------------------------------------------
	//[i] function_void OS_thread;
	public void OS_thread()
	{
		if( U1 > 500.0 ) {
			D0_OUT = 1;
		}
		else {
			D0_OUT = 0;
		}
	}
}


