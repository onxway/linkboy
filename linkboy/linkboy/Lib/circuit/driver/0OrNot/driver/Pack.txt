
unit Pack
{
	public const uint16 ID = 0;
	
	public const int16 CC_ID = 0;
	link unit S_C {} = #.SYS_CTCOM;

	//[i] var_fix U1;
	public ->fix U1;
	//[i] var_fix I1;
	public ->fix I1;
	
	//[i] var_fix U2;
	public ->fix U2;
	//[i] var_fix I2;
	public ->fix I2;
	
	//[i] var_fix U3;
	public ->fix U3;
	//[i] var_fix I3;
	public ->fix I3;

	//---------------------------------------------------
	//[i] function_void OS_init;
	public void OS_init()
	{
		U1 -> S_C.ElmList[CC_ID][S_C.EV1];
		I1 -> S_C.ElmList[CC_ID][S_C.EI1];
		U2 -> S_C.ElmList[CC_ID][S_C.EV2];
		I2 -> S_C.ElmList[CC_ID][S_C.EI2];
		U3 -> S_C.ElmList[CC_ID][S_C.EV3];
		I3 -> S_C.ElmList[CC_ID][S_C.EI3];
	}
}


