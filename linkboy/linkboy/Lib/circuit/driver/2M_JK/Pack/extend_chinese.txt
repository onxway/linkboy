<module>
在脉冲输入端CP的上升沿时刻才会根据输入端信号更新输出端,具体规则:当输入端J和K均为0时,输出端保持不变;当J=1,K=0时,输出端将更新为1;当J=0,K=1时,输出端将更新为0;当J和K均为1时,输出端将会翻转一次(0变为1,1变为0)
</module>
<name> Pack JK触发器,
</name>
<member> var_fix U1 U1,

</member>,
<member> var_fix I1 I1,

</member>,
<member> var_fix U2 U2,

</member>,
<member> var_fix I2 I2,

</member>,
<member> var_fix U3 U3,

</member>,
<member> var_fix I3 I3,

</member>,
<member> var_fix U4 U4,

</member>,
<member> var_fix I4 I4,

</member>,
<member> var_fix U5 U5,

</member>,
<member> var_fix I5 I5,

</member>,
<member> function_void OS_init OS_init,

</member>,
