

#include "sysctl.h"
#include <uart.h>
#include "w25qxx.h"
#include <gpiohs.h>
#include <fpioa.h>


//定义用户ROM区起始地址
#define Flash_USER_ADDR 0

#include "user/config_c.h"

//==========================================

//这里添加用户的相关外设初始化, Tick系统等
void vos_User_Init(void)
{
	//plic_init(); //中断控制初始化

	IO_Init();
	vos_Flash_Init();
}
//用户启动, 初始化下载串口等
void vos_User_Start(void) {}
//开启全局中断
void vos_SYS_InterruptEnable(void)
{
	sysctl_enable_irq(); //使能中断
}
//关闭全局中断
void vos_SYS_InterruptDisable(void)
{
	sysctl_disable_irq(); //禁用中断
}

#define UART_NUM UART_DEVICE_3
uint8 send_buffer[10];

//打开指定ID的串口并设置波特率
void vos_USART_Open( int8 ID, uint32 b )
{
	uart_init(UART_NUM);
	uart_configure(UART_NUM, 115200, 8, UART_STOP_1, UART_PARITY_NONE);
}
//向指定ID的串口发送字节
void vos_USART_WriteByte( int8 ID, uint8 tempc )
{
	send_buffer[0] = tempc;
	uart_send_data(UART_NUM, send_buffer, 1);
}

uint32 vos_Timer_Tick0(void)
{
	return read_cycle();
}
uint32 vos_Timer_Tick0MS(void)
{
	return sysctl_clock_get_freq(SYSCTL_CLOCK_CPU) / 1000UL;
}

#ifdef VOS_US_Tick
uint32 vos_Timer_Tick1(void)
{
	return read_cycle();
}
uint32 vos_Timer_Tick1US(void)
{
	return sysctl_clock_get_freq(SYSCTL_CLOCK_CPU) / 1000000UL;
}
#endif

#ifndef VOS_US_Tick
void SoftDelay_1us( int32 t ) {}
#endif


#include <timer.h>
int timer_callback(void* ctx);

int ctx = 0;

//开启定时器
void vos_DrvTimer_Start(void)
{
	timer_init(0); //初始化定时器0
	timer_set_interval(0, 0, 10000); //设置定时器时间间隔 10微秒
	timer_irq_register(0, 0, 0, 1, timer_callback, &ctx); //不是单次中断
	timer_set_enable(0, 0, 1); //使能
}
//关闭定时器
void vos_DrvTimer_Stop(void)
{
	//...
}
//100KHz定时器调用
int timer_callback(void* ctx)
{
	//触发频率为 100KHz
	remo_tick_run();
	return 0;
}

uint8 IO_Data[48];

void IO_Init(void)
{
	fpioa_set_function(0, FUNC_GPIOHS0);
	fpioa_set_function(1, FUNC_GPIOHS1);
	fpioa_set_function(2, FUNC_GPIOHS16);	//DC
	fpioa_set_function(3, FUNC_GPIOHS18);	//RST
	//fpioa_set_function(4, FUNC_GPIOHS4);	//ISP
	//fpioa_set_function(5, FUNC_GPIOHS5);	//ISP
	fpioa_set_function(6, FUNC_GPIOHS6);
	fpioa_set_function(7, FUNC_GPIOHS7);
	fpioa_set_function(8, FUNC_GPIOHS19);	//CAM
	fpioa_set_function(9, FUNC_GPIOHS9);

	fpioa_set_function(10, FUNC_GPIOHS10);
	fpioa_set_function(11, FUNC_GPIOHS11);
	fpioa_set_function(12, FUNC_GPIOHS12);
	fpioa_set_function(13, FUNC_GPIOHS13);
	fpioa_set_function(14, FUNC_GPIOHS14);
	fpioa_set_function(15, FUNC_GPIOHS15);
	//fpioa_set_function(16, FUNC_GPIOHS16);	//BOOT
	fpioa_set_function(17, FUNC_GPIOHS17);
	//fpioa_set_function(18, FUNC_GPIOHS18);	//MIC
	//fpioa_set_function(19, FUNC_GPIOHS19);	//MIC

	//fpioa_set_function(20, FUNC_GPIOHS20);	//MIC
	fpioa_set_function(21, FUNC_GPIOHS21);
	fpioa_set_function(22, FUNC_GPIOHS22);
	fpioa_set_function(23, FUNC_GPIOHS23);
	fpioa_set_function(24, FUNC_GPIOHS24);
	fpioa_set_function(25, FUNC_GPIOHS25);
	//fpioa_set_function(26, FUNC_GPIOHS26);	//SD
	//fpioa_set_function(27, FUNC_GPIOHS27);	//SD
	//fpioa_set_function(28, FUNC_GPIOHS28);	//SD
	//fpioa_set_function(29, FUNC_GPIOHS29);	//SD

	fpioa_set_function(30, FUNC_GPIOHS30);
	fpioa_set_function(31, FUNC_GPIOHS31);

	fpioa_set_function(32, FUNC_GPIOHS20);	//MIC
	fpioa_set_function(33, FUNC_GPIOHS26);	//SD
	fpioa_set_function(34, FUNC_GPIOHS27);	//SD
	fpioa_set_function(35, FUNC_GPIOHS28);	//SD

	//未用 可以分配给MIC
	//FUNC_GPIO29
}
//GPIO输出模式函数 d==0:上拉输入模式 d!=0:输出模式
void IO_DirWrite( uint8 i, uint8 d )
{
	if (d == 0) {
		gpiohs_set_drive_mode(i, GPIO_DM_INPUT_PULL_UP);
	}
	else {
		gpiohs_set_drive_mode(i, GPIO_DM_OUTPUT);
	}
}
//GPIO输出电平函数 d==0:低电平, d!=0: 高电平
void IO_OutWrite( uint8 i, uint8 d )
{
	if (d == 0) {
		gpiohs_set_pin( i, 0 );
	}
	else {
		gpiohs_set_pin( i, 1 );
	}
	IO_Data[i] = d;
}
//读取GPIO输出电平函数
uint8 IO_OutRead( uint8 i )
{
	return IO_Data[i];
}
//读取GPIO输入电平函数
uint8 IO_InRead( uint8 i )
{
	return gpiohs_get_pin( i );
}

void vos_IO_AnalogOpen( uint8 i, uint8 d ) {}
int32 vos_IO_AnalogRead( uint8 i ) { return 0; }
void vos_IO_PWM_SetPin( uint8 clk ) {}
void vos_IO_PWM_SetData( uint8 clk, int32 data32, int32 max ) {}


#define DATA_ADDRESS 0xB00000
#define Flash_PageSize 1024
#define Flash_PageNumber 128  //用到128K

void vos_Flash_Init(void)
{
	uint32 spi_index = 3;
	uint8_t manuf_id, device_id;

	w25qxx_init(spi_index, 0, 60000000);

	w25qxx_read_id(&manuf_id, &device_id);

	if((manuf_id != 0xEF && manuf_id != 0xC8) || (device_id != 0x17 && device_id != 0x16)) {
		//初始化出现问题
		return;
	}
}
void vos_Flash_Load(void)
{
	w25qxx_read_data(DATA_ADDRESS, vos_CODE_RAM, Flash_PageNumber * Flash_PageSize);
}
void vos_Flash_Save(void)
{
	w25qxx_write_data(DATA_ADDRESS, vos_CODE_RAM, Flash_PageNumber * Flash_PageSize);
}
void vos_Flash_Clear(void) {}
//写一个uint32到指定Flash地址
void vos_Flash_WriteUint32( uint32 addr, uint32 b ) {}

//短延时, 后续应该尽量换成 nop 指令
volatile int32 Timer_i;
void remo_WS2812_Delay_0_4us(void)
{
	Timer_i += 1;
	Timer_i += 1;

	//Timer_i += 1;
	//Timer_i += 1;
	//Timer_i += 1;

	// Timer_i += 1;
	// Timer_i += 1;
}
void vos_WS2812_WriteData( uint8 id, uint32 d )
{
	// GD32V-108MHz
	// F411RE-100MHz
	for (int8 i = 0; i < 24; ++i) {
		gpiohs_set_pin( id, 1 );
		if ((d & 0x800000) == 0) {
			remo_WS2812_Delay_0_4us();
			gpiohs_set_pin( id, 0 );
			remo_WS2812_Delay_0_4us();
			remo_WS2812_Delay_0_4us();
			remo_WS2812_Delay_0_4us();
			remo_WS2812_Delay_0_4us();
			remo_WS2812_Delay_0_4us();
		}
		else {
			remo_WS2812_Delay_0_4us();
			remo_WS2812_Delay_0_4us();
			remo_WS2812_Delay_0_4us();
			remo_WS2812_Delay_0_4us();
			remo_WS2812_Delay_0_4us();
			gpiohs_set_pin( id, 0 );
			remo_WS2812_Delay_0_4us();
			remo_WS2812_Delay_0_4us();
		}
		d <<= 1;
	}
}


void vos_UserExtDriver( uint32 v24, uint32 v8 )
{
	if( v24 == 600 ) {
		switch( v8 ) {
			case 0:
				A = EI_CrObj_VOS_GetNum();
				break;
			case 1:
				i0 = Mem_Get_int32( DP + 0 );
				i1 = Mem_Get_int32( DP + 4 );
				A = EI_CrObj_VOS_GetResult( i0, i1 );
				break;
			default:
				Error( E_VM_NativeError, v8 );
				vos_Running = false;
				break;
		}
	}
}

