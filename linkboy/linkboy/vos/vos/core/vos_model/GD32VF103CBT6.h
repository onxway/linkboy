

//-------------------------------------
//28 * 1024
//#define C_remo_SYS_BASE_LENGTH 14000    //28672  因为电路仿真引入了RAM, 这里减小一下

//-------------------------------------
#define Flash_PageSize 1024
#define Flash_StartPage 50	//128KB 共计128  50K SYS
#define Flash_PageNumber 78 //给用户分配78K

//定义用户ROM区起始地址
#define Flash_USER_ADDR (0x08000000 + Flash_PageSize * Flash_StartPage)


#include "user/config_c.h"

//----------------------------------------

#include "gd32vf103_usart.h"
#include "gd32vf103_gpio.h"
#include "gd32vf103.h"
#include "core/remo_Device\GD32VF103x_c.h"



