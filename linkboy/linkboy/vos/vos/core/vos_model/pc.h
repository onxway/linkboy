
//定义用户ROM区起始地址
#define Flash_USER_ADDR 0

#include "user/config_c.h"

//==========================================

//这里添加用户的相关外设初始化, Tick系统等
void vos_User_Init(void) {}
//用户启动, 初始化下载串口等
void vos_User_Start(void) {}
//开启全局中断
void vos_SYS_InterruptEnable(void) {}
//关闭全局中断
void vos_SYS_InterruptDisable(void) {}

//打开指定ID的串口并设置波特率
void vos_USART_Open( int8 ID, uint32 b ) {}
//向指定ID的串口发送字节
void vos_USART_WriteByte( int8 ID, uint8 tempc ) {}
//串口中断
void USARTn_IRQHandler(void)
{
	uint8 Data = 0x55;
	//UART_List_Receive( <n>, Data ); //存储到对应的缓冲区
	//Deal( Data );	//如果是协议串口还需要执行此函数
}

uint32 vos_Timer_Tick0(void) { return 0; }
uint32 vos_Timer_Tick0MS(void) { return 1; }

#ifdef VOS_US_Tick
uint32 vos_Timer_Tick1(void) { return 0; }
uint32 vos_Timer_Tick1US(void) {return 1; }
#endif

#ifndef VOS_US_Tick
void SoftDelay_1us( int32 t ) {}
void Timer_StartUS() {}
uint32 Timer_GetUS()
{
	return 0;
}
#endif

//开启定时器
void vos_DrvTimer_Start(void) {}
//关闭定时器
void vos_DrvTimer_Stop(void) {}
//100KHz定时器调用: remo_tick_run();

//GPIO输出模式函数 d==0:上拉输入模式 d!=0:输出模式
void IO_DirWrite( uint8 i, uint8 d ) {}
//GPIO输出电平函数 d==0:低电平, d!=0: 高电平
void IO_OutWrite( uint8 i, uint8 d )
{
	//t_Color cc = EI_BLACK;
	//if( d != 0 ) {
	//	cc = EI_RED;
	//}
	//DrawCircle( cc, 200, 200, 30 );
}

//读取GPIO输出电平函数
uint8 IO_OutRead( uint8 i ) { return 0; }
//读取GPIO输入电平函数
uint8 IO_InRead( uint8 i ) { return 0; }

void vos_IO_AnalogOpen( uint8 i, uint8 d ) {}
int32 vos_IO_AnalogRead( uint8 i ) { return 0; }
void vos_IO_PWM_SetPin( uint8 clk ) {}
void vos_IO_PWM_SetData( uint8 clk, int32 data32, int32 max ) {}

void vos_Flash_Load(void) {}
void vos_Flash_Save(void) {}
void vos_Flash_Clear(void) {}
//写一个uint32到指定Flash地址
void vos_Flash_WriteUint32( uint32 addr, uint32 b ) {}

void vos_WS2812_WriteData( uint8 id, uint32 d ) {}

void K210_IMG_Deal( uint32 v );
void vos_UserExtDriver( uint32 v24, uint32 v8 )
{
	switch( v24 ) {
		case 202:	K210_IMG_Deal( v8 ); break;
		default:	break;
	}
}

