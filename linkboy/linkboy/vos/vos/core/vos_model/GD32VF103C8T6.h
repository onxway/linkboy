
//GD32VF103C8T6 108MHz 64KB 20KB

//-------------------------------------
//15 * 1024
//#define C_remo_SYS_BASE_LENGTH 15360

//-------------------------------------
#define Flash_PageSize 1024
#define Flash_StartPage 38	//64KB 共计64  38K USER   26K APP
#define Flash_PageNumber 26 //给用户分配26K

//定义用户ROM区起始地址
#define Flash_USER_ADDR (0x08000000 + Flash_PageSize * Flash_StartPage)


#include "user/config_c.h"

//----------------------------------------

#include "gd32vf103_usart.h"
#include "gd32vf103_gpio.h"
#include "gd32vf103.h"
#include "core/remo_Device\GD32VF103x_c.h"


