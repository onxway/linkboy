
//-------------------------------------

#define Flash_PageSize 1024
#define Flash_StartPage 34
#define Flash_PageNumber 30 //用到30K

//本语句仅适用于从最低位的规范区间计算
#define Flash_USER_ADDR (0x08000000 + Flash_PageSize * Flash_StartPage)

#include "user/config_c.h"

//==========================================

#include "gd32f3x0_usart.h"
#include "gd32f3x0_gpio.h"
#include "gd32f3x0.h"


#define remo_CLOCK_M 84

#include "core/remo_Device/GD32F3x0_c.h"

//===========================================

//短延时, 后续应该尽量换成 nop 指令
volatile int32 Timer_i;
void vos_WS2812_Delay_0_4us(void)
{
	/*
	Timer_i += 1;
	Timer_i += 1;
	Timer_i += 1;
	Timer_i += 1;
	Timer_i += 1;
	*/
	
	//以下针对 E103CBT6 120MHz
	Timer_i += 1;
	Timer_i += 1;
	//Timer_i += 1;
}

void IO_OutWrite_0( uint8 i )
{
	GPIO_BC(GPION_LIST[i]) = GPIO_LIST[i];
}
void IO_OutWrite_1( uint8 i )
{
	GPIO_BOP(GPION_LIST[i]) = GPIO_LIST[i];
}

void vos_WS2812_WriteData( uint8 id, uint32 d )
{
	//GD32V-108MHz
	//F411RE-100MHz
	int8 i;
	
	for( i = 0; i < 24; ++i ) {
		
		IO_OutWrite_1( id );
		if( (d & 0x800000) == 0 ) {
			Timer_i += 1;
			//Timer_i += 1;
			//Timer_i += 1;
			//Timer_i += 1;
			IO_OutWrite_0( id );
			Timer_i += 1;
			Timer_i += 1;
			Timer_i += 1;
			Timer_i += 1;
			Timer_i += 1;
			Timer_i += 1;
			Timer_i += 1;
			Timer_i += 1;
			Timer_i += 1;
			Timer_i += 1;
			Timer_i += 1;
			Timer_i += 1;
			Timer_i += 1;
			Timer_i += 1;
		}
		else {
			Timer_i += 1;
			Timer_i += 1;
			Timer_i += 1;
			Timer_i += 1;
			Timer_i += 1;
			Timer_i += 1;
			Timer_i += 1;
			Timer_i += 1;
			Timer_i += 1;
			Timer_i += 1;
			Timer_i += 1;
			Timer_i += 1;
			Timer_i += 1;
			Timer_i += 1;
			Timer_i += 1;
			Timer_i += 1;
			IO_OutWrite_0( id );
			Timer_i += 1;
			Timer_i += 1;
			Timer_i += 1;
			Timer_i += 1;
			//Timer_i += 1;
			//Timer_i += 1;
		}
		d <<= 1;
	}
}

void vos_UserExtDriver( uint32 v24, uint32 v8 )
{
	
}

