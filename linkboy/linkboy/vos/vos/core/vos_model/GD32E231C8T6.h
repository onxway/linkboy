
//5 * 1024
//#define VOS_SYS_BASE_LENGTH 5120

#define VOS_Flash_PageSize 1024
//#define VOS_Flash_StartPage 28	//2020.3.5 已用: 23K, 预留28K
#define VOS_Flash_StartPage 41	//2021.10.17 已用: 39K 奇怪 用了这么多...
#define VOS_Flash_PageNumber 23 //用户23K

//本语句仅适用于从最低位的规范区间计算
#define Flash_USER_ADDR (0x08000000 + VOS_Flash_PageSize * VOS_Flash_StartPage)

#include "user/config_c.h"

//------------------------------------

#include "gd32e23x_usart.h"
#include "gd32e23x_gpio.h"
#include "gd32e23x_timer.h"
#include "gd32e23x_adc.h"
#include "gd32e23x_dma.h"
#include "gd32e23x.h"
#include "core/remo_Device/gd32e231x_c.h"




