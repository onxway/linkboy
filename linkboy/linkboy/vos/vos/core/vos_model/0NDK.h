
//已用ID: 0x010026

//-------------------

//通过串口打印一段信息
@0x01000E void Serial_print( string mes );
//通过串口打印一段信息, 结尾带有回车符
@0x01000F void Serial_println( string mes );

//创建指定名字和密码的热点, 密码建议不要小于8位数
@0x010000 void WiFi_softAP( string ssid, string pass );
//关闭当前AP，若wifioff为true则还将还原网络设置
@0x010010 void WiFi_softAPdisconnect();
//返回连接到AP的客户端数量
@0x010011 int32 WiFi_softAPgetStationNum();

//连接到指定的路由器(热点), 参数分别为路由器名和密码
@0x010007 void WiFi_begin( string ssid, string pass );
//断开连接
@0x010012 void WiFi_disconnect();
//重新连接
@0x010013 void WiFi_reconnect();
//如果已连接到指定的路由器, 返回真
@0x01000D bool t_WiFi_status_equal_WL_CONNECTED();

//WIFI服务器开始工作
@0x010001 void WiFiServer_begin();

//获取连接到当前服务器的客户端
@0x010002 void t_WiFiClient_E_WiFiServer_available();
//如果客户端存在(非空), 返回真
@0x010003 bool t_WiFiClient_not_null();

//客户端连接到指定的网站和端口
@0x010008 void WiFiClient_connect( string web, int32 port );
//客户端停止连接
@0x010014 void WiFiClient_stop();
//如果客户端已连接到网站, 返回真
@0x010004 bool WiFiClient_connected();
//通过客户端发送字符串
@0x010009 void WiFiClient_print( string mes );
//如果客户端具有可读的数据, 返回真
@0x010005 bool WiFiClient_available();
//从客户端读取一个数据
@0x010006 int32 WiFiClient_read();

//用指定的url启动一个HTTP连接
@0x01000A void HTTPClient_begin( string url );
//如果HTTP获取数据正常, 返回真
@0x01000B bool t_HTTPClient_GET_equal_HTTP_CODE_OK();
//读取网页返回的字符串
@0x01000C void HTTPClient_getString();
//是否存在可读字符
@0x010015 bool t_HTTPClient_StringNotNull(void);
//读取一个字符
@0x010016 int32 t_HTTPClient_StringGetChar(void);
//结束
@0x010017 void HTTPClient_end(void);

//初始化蓝牙
@0x010018 void BluetoothSerial_begin( string name );
//如果蓝牙客户端具有可读的数据, 返回真
@0x010019 bool BluetoothSerial_available();
//从蓝牙客户端读取一个数据
@0x01001A int32 BluetoothSerial_read();
//向蓝牙客户端写一个数据
@0x01001B void BluetoothSerial_write( int32 d );

//-----------------------------
//彩屏类
@0x01001C void Screen_Init();
@0x01001D void Screen_draw_pixel( int32 x, int32 y );
@0x01001E void Screen_clear_pixel( int32 x, int32 y );
@0x01001F void Screen_clear(uint16 Color);
@0x010020 void Screen_set_fore(uint16 Color);
//@0x010021 void Screen_set_back(uint16 Color, bool t); //过时
@0x010022 void Screen_address_set( uint16 x1, uint16 y1, uint16 x2, uint16 y2 );
@0x010023 void Screen_add_color(void);
@0x010024 void Screen_add_color_n( int32 n );
@0x010025 void Screen_set_color( uint32 c );
@0x010026 void Screen_push_colors( string addr, int32 n );
