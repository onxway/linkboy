
//配置RAM尺寸
//const int32 remo_SYS_BASE_LENGTH = 7408; //17 * 1024  //2020.8.28 增加了电路仿真17408 -> 7408
//#define remo_SYS_BASE_LENGTH 17408 //17 * 1024  //2021.5.25 移除了电路仿真 

//配置ROM尺寸 一共64K
#define PageSize 1024
#define StartPage 22
#define PageNumber 42  //13为C6T6  45为C8T6

//定义用户ROM区起始地址
#define Flash_USER_ADDR (0x08000000 + PageSize * StartPage)

#include "user/config_c.h"

//----------------------------------------

//2020.7.13 遇到一部分芯片不运行，最后发现初始化不能超过48个引脚，可能是国产芯片
#define NUMBER 48//112

#include "stm32f10x.h"
#include "core/remo_Device/STM32F10X_c.h"




