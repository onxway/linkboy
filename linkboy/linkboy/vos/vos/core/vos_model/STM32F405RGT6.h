
//大于50K可能有问题, 不知道为什么 (VEX内核是2字节地址)
//const int32 remo_SYS_BASE_LENGTH = 46000; //60000; //125 * 1024 需要减去14000CC引擎


#include "stm32f4xx_flash.h"

//F4XX:
//16K 16K 16K 16K 64K 128K 128K 128K 128K 128K ...

#define ADDR_FLASH_SECTOR_0     ((uint32_t)0x08000000) /* Base @ of Sector 0, 16 Kbytes */
#define ADDR_FLASH_SECTOR_1     ((uint32_t)0x08004000) /* Base @ of Sector 1, 16 Kbytes */
#define ADDR_FLASH_SECTOR_2     ((uint32_t)0x08008000) /* Base @ of Sector 2, 16 Kbytes */
#define ADDR_FLASH_SECTOR_3     ((uint32_t)0x0800C000) /* Base @ of Sector 3, 16 Kbytes */
#define ADDR_FLASH_SECTOR_4     ((uint32_t)0x08010000) /* Base @ of Sector 4, 64 Kbytes */
#define ADDR_FLASH_SECTOR_5     ((uint32_t)0x08020000) /* Base @ of Sector 5, 128 Kbytes */
#define ADDR_FLASH_SECTOR_6     ((uint32_t)0x08040000) /* Base @ of Sector 6, 128 Kbytes */
#define ADDR_FLASH_SECTOR_7     ((uint32_t)0x08060000) /* Base @ of Sector 7, 128 Kbytes */
#define ADDR_FLASH_SECTOR_8     ((uint32_t)0x08080000) /* Base @ of Sector 8, 128 Kbytes */
#define ADDR_FLASH_SECTOR_9     ((uint32_t)0x080A0000) /* Base @ of Sector 9, 128 Kbytes */
#define ADDR_FLASH_SECTOR_10    ((uint32_t)0x080C0000) /* Base @ of Sector 10, 128 Kbytes */
#define ADDR_FLASH_SECTOR_11    ((uint32_t)0x080E0000) /* Base @ of Sector 11, 128 Kbytes */

#define Flash_USER_ADDR ADDR_FLASH_SECTOR_6

#define USER_SECTOR FLASH_Sector_6

//=========================================

#include "user/config_c.h"

//=========================================

#include "stm32f4xx.h"
#include "stm32f4xx_conf.h"


void IO_Init(void);
void vos_DrvTimer_Init(void);

//系统初始化
void vos_User_Init()
{
	IO_Init();
	vos_DrvTimer_Init();
	SysTick_Config(0xFFFFFF);//从 0xFFFFFF 倒计时
}
void vos_User_Start()
{
	vos_USART_Open( 1, 115200 );
}

#include "core/remo_device/STM32F4xx_SYS_c.h"

#include "core/remo_device/STM32F4xx_USART1_c.h"

//每微秒包含的时钟数目
const int32 vos_Tick_pus = 168;

#include "core/remo_device/STM32F4xx_Timer_c.h"

#include "core/remo_device/STM32F4xx_IO_c.h"

#include "core/remo_device/STM32F4xx_Flash_c.h"

#include "core/remo_device/STM32F4xx_DrvTimer_c.h"


//短延时, 后续应该尽量换成 nop 指令
static volatile int32 remo_WS2812_i;
void vos_WS2812_Delay_0_4us(void)
{
	//100MHz
	remo_WS2812_i += 1;
	remo_WS2812_i += 1;
	remo_WS2812_i += 1;
	remo_WS2812_i += 1;
	//Timer_i += 1;
	//Timer_i += 1;
	
	/*
	__nop();
	__nop();
	__nop();
	*/
}

void vos_WS2812_WriteData( uint8 id, uint32 d )
{
	int8 i;
	
	//GD32V-108MHz
	//F411RE-100MHz
	for( i = 0; i < 24; ++i ) {
		
		IO_OutWrite( id, 1 );
		if( (d & 0x800000) == 0 ) {
			vos_WS2812_Delay_0_4us();
			IO_OutWrite( id, 0 );
			vos_WS2812_Delay_0_4us();
			vos_WS2812_Delay_0_4us();
			vos_WS2812_Delay_0_4us();
			//Timer_Delay_0_4us();
		}
		else {
			vos_WS2812_Delay_0_4us();
			vos_WS2812_Delay_0_4us();
			vos_WS2812_Delay_0_4us();
			IO_OutWrite( id, 0 );
			vos_WS2812_Delay_0_4us();
		}
		d <<= 1;
	}
}

void vos_UserExtDriver( uint32 v24, uint32 v8 )
{
	
}







