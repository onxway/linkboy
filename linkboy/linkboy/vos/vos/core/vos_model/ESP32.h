
#include "esp_attr.h"

//定义用户ROM区起始地址
#define Flash_USER_ADDR 0

#include "user/config_c.h"

void main_uart_set_baudrate( uint32 b );
void main_send_byte( uint8 data );

void IO_Init(void);
void vos_Flash_Init(void);
void vos_DrvTimer_Init(void);

//-------------------------------------
//16 * 1024
//#define C_remo_SYS_BASE_LENGTH 16384


//系统初始化
void vos_User_Init()
{
	IO_Init();
	vos_Flash_Init();
	vos_DrvTimer_Init();
}
void vos_User_Start()
{
	vos_USART_Open( 0, 115200 );
}
//开启全局中断
void vos_SYS_InterruptEnable()
{
	//■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■
}
//关闭全局中断
void vos_SYS_InterruptDisable()
{
	//■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■
}

//串口波特率
void vos_USART_Open( int8 ID, uint32 b )
{
	if( ID == 0 ) {
		main_uart_set_baudrate( b );
	}
}
//发送字节
void vos_USART_WriteByte( int8 ID, uint8 tempc )
{
	if( ID == 0 ) {
		main_send_byte( tempc );
	}
}

#include "esp_timer.h"

uint32 vos_Timer_Tick0()
{
	return esp_timer_get_time();
}
uint32 vos_Timer_Tick0MS()
{
	return 1000;
}
uint32 vos_Timer_Tick1()
{
	return esp_timer_get_time();
}
uint32 vos_Timer_Tick1US()
{
	return 1;
}

#include "driver/gpio.h"
#include "driver/adc.h"

uint8 ADList[] = {4, 5, 6, 7, 0, 0xFF, 0xFF, 3 };
#define AD_START 32

//端口输出缓冲
uint8 OutList[39];

void IO_Init()
{
	gpio_pad_select_gpio( 2 );
	gpio_pad_select_gpio( 4 );
	gpio_pad_select_gpio( 5 );
	gpio_pad_select_gpio( 12 );
	gpio_pad_select_gpio( 13 );
	gpio_pad_select_gpio( 14 );
	gpio_pad_select_gpio( 15 );
	gpio_pad_select_gpio( 16 );
	gpio_pad_select_gpio( 17 );
	gpio_pad_select_gpio( 18 );
	gpio_pad_select_gpio( 19 );
	gpio_pad_select_gpio( 21 );
	gpio_pad_select_gpio( 22 );
	gpio_pad_select_gpio( 23 );
	gpio_pad_select_gpio( 25 );
	gpio_pad_select_gpio( 26 );
	gpio_pad_select_gpio( 27 );
	gpio_pad_select_gpio( 32 );
	gpio_pad_select_gpio( 33 );
	
	//只能输入
	gpio_pad_select_gpio( 34 );
	gpio_pad_select_gpio( 35 );
	gpio_pad_select_gpio( 36 );
	gpio_pad_select_gpio( 39 );
	
	//gpio_set_direction( i, GPIO_MODE_INPUT );
	//gpio_set_pull_mode( i, GPIO_PULLUP_ONLY );
}
void IRAM_ATTR IO_DirWrite( uint8 i, uint8 d )
{
	if( d == 0 ) {
		gpio_set_direction( i, GPIO_MODE_INPUT );
		gpio_pullup_en( i );
		gpio_pulldown_dis( i );
	}
	else {
		gpio_set_direction( i, GPIO_MODE_OUTPUT );
		gpio_pullup_dis( i );
		gpio_pulldown_dis( i );
	}
}
void IRAM_ATTR IO_OutWrite( uint8 i, uint8 d )
{
	if( d == 0 ) {
		gpio_set_level(i, 0);
	}
	else {
		gpio_set_level(i, 1);
	}
	OutList[i] = d;
}
uint8 IRAM_ATTR IO_OutRead( uint8 i )
{
	//return gpio_get_level( i );
	return OutList[i];
}
uint8 IRAM_ATTR IO_InRead( uint8 i )
{
	return gpio_get_level( i );
}
void IRAM_ATTR vos_IO_AnalogOpen( uint8 i, uint8 d )
{
	//i += 0; //这里应该加上模拟量引脚序列的偏移量
	adc1_config_width( ADC_WIDTH_10Bit ); //ADC_WIDTH_12Bit
	adc1_config_channel_atten( ADList[i - AD_START], ADC_ATTEN_11db );
}
int32 IRAM_ATTR vos_IO_AnalogRead( uint8 i )
{
	return adc1_get_raw( ADList[i - AD_START] ); //adc1_get_voltage 应该用这个函数 但是头文件未定义, 找不到
	return 0;
}
void vos_IO_PWM_SetPin( uint8 clk ) {}
void vos_IO_PWM_SetData( uint8 clk, int32 data32, int32 max ) {}



#include "esp_partition.h"

const esp_partition_t *partition;

void vos_Flash_Init()
{
	partition = esp_partition_find_first(ESP_PARTITION_TYPE_DATA, ESP_PARTITION_SUBTYPE_ANY, "storage1");
}
void vos_Flash_Load()
{
	esp_partition_read(partition, 0, vos_CODE_RAM, VOS_SYS_ROM_LENGTH);
}
void vos_Flash_Save()
{
	esp_partition_write(partition, 0, vos_CODE_RAM, VOS_SYS_ROM_LENGTH);
}
void vos_Flash_Clear()
{
	//全部擦除
	esp_partition_erase_range(partition, 0, partition->size);
}
void Flash_WriteUint32( uint32 addr, uint32 b ) {}

//========================================================================================

/* 这里是软件定时器, 关闭系统调度后 就无法再触发定时器事件
#include "esp_log.h"
void test_timer_periodic_cb(void *arg);

//定义定时器句柄
esp_timer_handle_t test_p_handle = 0;

//定义一个周期重复运行的定时器结构体
esp_timer_create_args_t test_periodic_arg = { .callback =
		&test_timer_periodic_cb, //设置回调函数
		.arg = NULL, //不携带参数
		.name = "TestPeriodicTimer" //定时器名字
		};

//初始化定时器
void remo_DrvTimer_Init()
{
	//开始创建一个重复周期的定时器并且执行
	esp_err_t err = esp_timer_create( &test_periodic_arg, &test_p_handle );
	
	//重复触发定时器, 单位是微秒
	err = esp_timer_start_periodic( test_p_handle, 10 );
}
void test_timer_periodic_cb(void *arg) {

	//int64_t tick = esp_timer_get_time(); //距离定时器开启时间间隔
	//esp_err_t err = esp_timer_stop(test_p_handle); //停止定时器
	
	//触发频率为 100KHz
	remo_tick_run();
}
*/

#include "driver/timer.h"
#define TIMER_DIVIDER         16  //  Hardware timer clock divider
#define TIMER_SCALE           (TIMER_BASE_CLK / TIMER_DIVIDER)  // convert counter value to seconds

void IRAM_ATTR timer_group0_isr(void *para);
void vos_DrvTimer_Init()
{
	timer_config_t config;
	config.divider = TIMER_DIVIDER;
	config.counter_dir = TIMER_COUNT_UP;
	config.counter_en = TIMER_PAUSE;
	config.alarm_en = TIMER_ALARM_EN;
	config.intr_type = TIMER_INTR_LEVEL;
	config.auto_reload = true;
    timer_init(TIMER_GROUP_0, TIMER_0, &config);

    /* Timer's counter will initially start from value below.
       Also, if auto_reload is set, this value will be automatically reload on alarm */
    timer_set_counter_value(TIMER_GROUP_0, TIMER_0, 0x00000000ULL);
    
    /* Configure the alarm value and the interrupt on alarm. */
    timer_set_alarm_value(TIMER_GROUP_0, TIMER_0, 0.00001 * TIMER_SCALE);
    timer_enable_intr(TIMER_GROUP_0, TIMER_0);
    timer_isr_register(TIMER_GROUP_0, TIMER_0, timer_group0_isr,
                       (void *) TIMER_0, ESP_INTR_FLAG_IRAM, NULL);

}
void vos_DrvTimer_Start(void)
{
	timer_start(TIMER_GROUP_0, TIMER_0);
}
void vos_DrvTimer_Stop(void)
{
	timer_pause(TIMER_GROUP_0, TIMER_0);
}
void IRAM_ATTR timer_group0_isr(void *para)
{
    //timer_spinlock_take();
    
    //触发频率为 100KHz
    remo_tick_run();
    
    //int timer_idx = (int) para;
    
    timer_group_intr_clr_in_isr(TIMER_GROUP_0, TIMER_0);
    /* After the alarm has been triggered
      we need enable it again, so it is triggered the next time */
    timer_group_enable_alarm_in_isr(TIMER_GROUP_0, TIMER_0);
    
    //timer_spinlock_give(TIMER_GROUP_0);
}

//短延时, 后续应该尽量换成 nop 指令
volatile int32 Timer_i;
void vos_WS2812_Delay_0_4us(void)
{
	Timer_i += 1;
	Timer_i += 1;


	Timer_i += 1;
	Timer_i += 1;
	Timer_i += 1;

	//Timer_i += 1;
	//Timer_i += 1;
}
void IRAM_ATTR IO_OutWrite_0( uint8 i )
{
	gpio_set_level(i, 0);
	OutList[i] = 0;
}
void IRAM_ATTR IO_OutWrite_1( uint8 i )
{
	gpio_set_level(i, 1);
	OutList[i] = 1;
}
void vos_WS2812_WriteData( uint8 id, uint32 d )
{
	//GD32V-108MHz
	//F411RE-100MHz
	for( int8 i = 0; i < 24; ++i ) {

		IO_OutWrite_1( id );
		if( (d & 0x800000) == 0 ) {
			vos_WS2812_Delay_0_4us();
			IO_OutWrite_0( id );
			vos_WS2812_Delay_0_4us();
			vos_WS2812_Delay_0_4us();
			vos_WS2812_Delay_0_4us();
		}
		else {
			vos_WS2812_Delay_0_4us();
			vos_WS2812_Delay_0_4us();
			vos_WS2812_Delay_0_4us();
			IO_OutWrite_0( id );
			vos_WS2812_Delay_0_4us();
		}
		d <<= 1;
	}
}

//=================================================

void ESP32_wifiAP_Deal( uint32 v );
void ESP32_wifiSTA_Deal( uint32 v );
void ESP32_TCP_Server_Deal( uint32 v );
void ESP32_TCP_Client_Deal( uint32 v );


//处理接口请求 (主中转)
void vos_UserExtDriver( uint32 v24, uint32 v8 )
{
	switch( v24 ) {
		
		//K210专用 200 -299
		
		//ESP32专用 300 - 399
		case 300:	ESP32_wifiAP_Deal( v8 ); break;
		case 301:	ESP32_wifiSTA_Deal( v8 ); break;
		case 302:	ESP32_TCP_Server_Deal( v8 ); break;
		case 303:	ESP32_TCP_Client_Deal( v8 ); break;
		
		default:	Error( E_VM_InterfaceError, (uint8)v24 );
					vos_Running = false;
					break;
	}
}


//=====================================================================

//ESP32 wifi AP接口
void wifi_init_softap(uint8 *p_SSID, uint8 *p_PASS);
void ESP32_wifiAP_softap( uint32 ssid, uint32 pass );

extern bool wifiAP_isConnected;

void ESP32_wifiAP_Deal( uint32 v )
{
	switch( v ) {
		case 0:
			u0 = Mem_Get_uint32( DP + 0 );
			u1 = Mem_Get_uint32( DP + 4 );
			ESP32_wifiAP_softap( u0, u1 );
			break;
		case 1:
			A = wifiAP_isConnected;
			break;
		//其他
		default:
			Error( E_VM_InterfaceError, (uint8)v );
			break;
	}
}
void ESP32_wifiAP_softap( uint32 ssid, uint32 pass )
{
	uint8 *p_SSID = Mem_GetAddr( ssid );
	uint8 *p_PASS = Mem_GetAddr( pass );;
	
	/*
	//得到SSID
	bool isCode = (ssid & 0xFF0000) != 0;
	ssid &= 0xFFFF;
	if( isCode ) {
		p_SSID = (uint8*)Flash_GetAddr( ssid );
	}
	else {
		p_SSID = &BASE[ssid];
	}
	//得到PASS
	isCode = (pass & 0xFF0000) != 0;
	pass &= 0xFFFF;
	if( isCode ) {
		p_PASS = (uint8*)Flash_GetAddr( pass );
	}
	else {
		p_PASS = &BASE[ssid];
	}
	*/
	//初始化wifi
	wifi_init_softap( p_SSID, p_PASS );
}

//=====================================================================

//ESP32 wifi接口
void ESP32_wifiSTA_softap( uint32 ssid, uint32 pass );
void wifi_init_sta(uint8 *p_SSID, uint8 *p_PASS);
void wifiSTA_VOS_Reconnect(void);
void wifiSTA_VOS_Disconnect(void);

extern bool wifiSTA_isConnected;

void ESP32_wifiSTA_Deal( uint32 v )
{
	switch( v ) {
		case 0:
			
			break;
		case 1:
			u0 = Mem_Get_uint32( DP + 0 );
			u1 = Mem_Get_uint32( DP + 4 );
			ESP32_wifiSTA_softap( u0, u1 );
			break;
		case 2:
			A = wifiSTA_isConnected;
			break;
		//其他
		default:
			Error( E_VM_InterfaceError, (uint8)v );
			break;
	}
}
void ESP32_wifiSTA_softap( uint32 ssid, uint32 pass )
{
	uint8 *p_SSID = Mem_GetAddr( ssid );
	uint8 *p_PASS = Mem_GetAddr( pass );;
	
	//初始化wifi
	wifi_init_sta( p_SSID, p_PASS );
}
//=====================================================================
//ESP32 TCP_Server接口

void Main_TCP_Init( uint32 buf, int32 n );
void Main_StartTCP( int32 port );
void ESP32_TCP_Server_SendString( uint32 text );
void TCP_Server_SendString(uint8 *p );

extern bool TCP_isConnected;
extern int32 TCP_RLength;

void ESP32_TCP_Server_Deal( uint32 v )
{
	switch( v ) {
		case 0:
			u0 = Mem_Get_uint32( DP + 0 );
			i0 = Mem_Get_int32( DP + 4 );
			Main_TCP_Init( u0, i0 );
			break;
		case 1:
			i0 = Mem_Get_int32( DP + 0 );
			Main_StartTCP( i0 );
			break;
		case 2:
			A = TCP_isConnected;
			break;
		case 3:
			u0 = Mem_Get_uint32( DP + 0 );
			ESP32_TCP_Server_SendString( u0 );
			break;
		case 4:
			A = TCP_RLength;
			TCP_RLength = 0;
			break;
		//其他
		default:
			Error( E_VM_InterfaceError, (uint8)v );
			break;
	}
}
void ESP32_TCP_Server_SendString( uint32 addr )
{
	uint8 *p = Mem_GetAddr( addr );
	/*
	//得到text
	bool isCode = (addr & 0xFF0000) != 0;
	addr &= 0xFFFF;
	if( isCode ) {
		p = (uint8*)Flash_GetAddr( addr );
	}
	else {
		p = &BASE[addr];
	}
	*/
	//发送字符串
	TCP_Server_SendString( p );
}

//=====================================================================
//ESP32 TCP_Client接口

void Main_TCP_Client_Init( uint32 buf, int32 n );
void Main_TCP_Client_Connect( uint32 IP, int32 port );
void ESP32_TCP_Client_SendString( uint32 text );
void TCP_Client_SendString(uint8 *p );

extern bool TCP_Client_isConnected;
extern int32 TCP_Client_RLength;

void ESP32_TCP_Client_Deal( uint32 v )
{
	switch( v ) {
		case 0:
			u0 = Mem_Get_uint32( DP + 0 );
			i0 = Mem_Get_int32( DP + 4 );
			Main_TCP_Client_Init( u0, i0 );
			break;
		case 1:
			u0 = Mem_Get_uint32( DP + 0 );
			i0 = Mem_Get_int32( DP + 4 );
			Main_TCP_Client_Connect( u0, i0 );
			break;
		case 2:
			A = TCP_Client_isConnected;
			break;
		case 3:
			u0 = Mem_Get_uint32( DP + 0 );
			ESP32_TCP_Client_SendString( u0 );
			break;
		case 4:
			A = TCP_Client_RLength;
			TCP_Client_RLength = 0;
			break;
		//其他
		default:
			Error( E_VM_InterfaceError, (uint8)v );
			break;
	}
}
void ESP32_TCP_Client_SendString( uint32 addr )
{
	uint8 *p = Mem_GetAddr( addr );
	
	//发送字符串
	TCP_Client_SendString( p );
}





