
//定义用户ROM区起始地址
#define Flash_USER_ADDR 0

#include "user/config_c.h"

//==========================================

TIM_HandleTypeDef htim0;
uint32 vos_Tick_Value;
bool vos_DrvOpen;

//用户初始化
void vos_User_Init()
{
	//从ADC中看到的初始化
	__HAL_RCC_ADC_CLK_ENABLE();
	__HAL_RCC_GPIO_CLK_ENABLE();
	
	vos_Tick_Value = 0;
	vos_DrvOpen = false;
	
	__HAL_RCC_TIM_CLK_ENABLE();
	HAL_NVIC_SetPriority(TIM_IRQn, 0);
	HAL_NVIC_EnableIRQ(TIM_IRQn);
	
	htim0.Instance = TIM0;
	htim0.Init.Unit = TIM_UNIT_US;
	htim0.Init.Period = 10; //单位是us
	htim0.Init.AutoReload = TIM_AUTORELOAD_PRELOAD_ENABLE;
	if (HAL_TIM_Base_Init(&htim0) != HAL_OK) {
		Error_Handler();
	}
	
	//这里开启会导致异常. 又好了...
	HAL_TIM_Base_Start_IT(&htim0);
}
//用户启动
void vos_User_Start()
{
	vos_USART_Open( 0, 115200 );
	vos_USART_Open( 1, 115200 );
}
//开启全局中断
void vos_SYS_InterruptEnable()
{
	__enable_excp_irq();
	vos_DrvOpen = true;
}
//关闭全局中断
void vos_SYS_InterruptDisable()
{
	__disable_excp_irq();
	vos_DrvOpen = false;
}

// 大于等于0，0：接收不定长数据即可触发中断回调；大于0：接收N个长度数据才触发中断回调
#define IT_LEN 0
static uint8_t vos_uart_buf0[32] = {0}; // 必须大于等于32字节
static uint8_t vos_uart_buf1[32] = {0}; // 必须大于等于32字节
static uint8_t vos_uart_buf2[32] = {0}; // 必须大于等于32字节
static uint8_t vos_uart_buf3[32] = {0}; // 必须大于等于32字节
static uint8_t vos_uart_buf4[32] = {0}; // 必须大于等于32字节
static uint8_t vos_uart_buf5[32] = {0}; // 必须大于等于32字节
UART_HandleTypeDef huart0;
UART_HandleTypeDef huart1;
UART_HandleTypeDef huart2;
UART_HandleTypeDef huart3;
UART_HandleTypeDef huart4;
UART_HandleTypeDef huart5;

uint8_t tx_buf[10] = {0};

void vos_Error_Handler(void)
{
	while (1) {
	}
}

//串口打开
void vos_USART_Open( int8 ID, uint32 baud )
{
	UART_HandleTypeDef *p_uart;
	switch( ID ) {
		case 0:	p_uart = &huart0; break;
		case 1:	p_uart = &huart1; break;
		case 2:	p_uart = &huart2; break;
		case 3:	p_uart = &huart3; break;
		case 4:	p_uart = &huart4; break;
		case 5:	p_uart = &huart5; break;
		default: return;
	}
	p_uart->Init.BaudRate = baud;
	p_uart->Init.WordLength = UART_WORDLENGTH_8B;
	p_uart->Init.StopBits = UART_STOPBITS_1;
	p_uart->Init.Parity = UART_PARITY_NONE;
	p_uart->Init.Mode = UART_MODE_TX | UART_MODE_RX;
	p_uart->Init.HwFlowCtl = UART_HWCONTROL_NONE;
	
	//只需调用一次，接收够设定的长度，进入中断回调，用户需要在中断回调中取走数据，
	//此处设置了0个字节，即不定长
	//HAL_UART_Receive_IT(&huart0, vos_uart_buf0, IT_LEN);
	
	if( ID == 0 ) {
		__HAL_RCC_UART0_CLK_ENABLE();
		__HAL_AFIO_REMAP_UART0_TX(GPIOB, GPIO_PIN_19); //	PB19: UART0_TX
		__HAL_AFIO_REMAP_UART0_RX(GPIOB, GPIO_PIN_20); //	PB20: UART0_RX
		HAL_NVIC_SetPriority(UART0_IRQn, 0);
		HAL_NVIC_EnableIRQ(UART0_IRQn);
		
		huart0.Instance = UART0;
		if (HAL_UART_Init(&huart0) != HAL_OK) {
			vos_Error_Handler();
		}
		HAL_UART_Receive_IT(&huart0, vos_uart_buf0, IT_LEN);
	}
	else if( ID == 1 ) {
		__HAL_RCC_UART1_CLK_ENABLE();
		__HAL_AFIO_REMAP_UART1_TX(GPIOB, GPIO_PIN_6); //	PB6: UART1_TX
		__HAL_AFIO_REMAP_UART1_RX(GPIOB, GPIO_PIN_7); //	PB7: UART1_RX
		HAL_NVIC_SetPriority(UART1_IRQn, 0);
		HAL_NVIC_EnableIRQ(UART1_IRQn);
		
		huart1.Instance = UART1;
		if (HAL_UART_Init(&huart1) != HAL_OK) {
			vos_Error_Handler();
		}
		HAL_UART_Receive_IT(&huart1, vos_uart_buf1, IT_LEN);
	}
	else if( ID == 2 ) {
		__HAL_RCC_UART2_CLK_ENABLE();
		__HAL_AFIO_REMAP_UART2_TX(GPIOB, GPIO_PIN_2); //	PB2: UART2_TX
		__HAL_AFIO_REMAP_UART2_RX(GPIOB, GPIO_PIN_3); //	PB3: UART2_RX
		HAL_NVIC_SetPriority(UART2_5_IRQn, 0);
		HAL_NVIC_EnableIRQ(UART2_5_IRQn);
		
		huart2.Instance = UART2;
		if (HAL_UART_Init(&huart2) != HAL_OK) {
			vos_Error_Handler();
		}
		HAL_UART_Receive_IT(&huart2, vos_uart_buf2, IT_LEN);
	}
	else if( ID == 3 ) {
		__HAL_RCC_UART3_CLK_ENABLE();
		__HAL_AFIO_REMAP_UART3_TX(GPIOB, GPIO_PIN_0); //	PB0: UART3_TX
		__HAL_AFIO_REMAP_UART3_RX(GPIOB, GPIO_PIN_1); //	PB1: UART3_RX
		HAL_NVIC_SetPriority(UART2_5_IRQn, 0);
		HAL_NVIC_EnableIRQ(UART2_5_IRQn);
		
		huart3.Instance = UART3;
		if (HAL_UART_Init(&huart3) != HAL_OK) {
			vos_Error_Handler();
		}
		HAL_UART_Receive_IT(&huart3, vos_uart_buf3, IT_LEN);
	}
	else if( ID == 4 ) {
		__HAL_RCC_UART4_CLK_ENABLE();
		
		__HAL_AFIO_REMAP_UART4_TX(GPIOB, GPIO_PIN_4); //	PB4: UART4_TX
		__HAL_AFIO_REMAP_UART4_RX(GPIOB, GPIO_PIN_5); //	PB5: UART4_RX
		//__HAL_AFIO_REMAP_UART4_TX(GPIOA, GPIO_PIN_8); //	PA8: UART4_TX
		//__HAL_AFIO_REMAP_UART4_RX(GPIOA, GPIO_PIN_9); //	PA9: UART4_RX
		
		HAL_NVIC_SetPriority(UART2_5_IRQn, 0);
		HAL_NVIC_EnableIRQ(UART2_5_IRQn);
		
		huart4.Instance = UART4;
		if (HAL_UART_Init(&huart4) != HAL_OK) {
			vos_Error_Handler();
		}
		HAL_UART_Receive_IT(&huart4, vos_uart_buf4, IT_LEN);
	}
	else if( ID == 5 ) {
		__HAL_RCC_UART5_CLK_ENABLE();
		__HAL_AFIO_REMAP_UART5_TX(GPIOA, GPIO_PIN_12); //	PA12: UART5_TX
		__HAL_AFIO_REMAP_UART5_RX(GPIOA, GPIO_PIN_13); //	PA13: UART5_RX
		HAL_NVIC_SetPriority(UART2_5_IRQn, 0);
		HAL_NVIC_EnableIRQ(UART2_5_IRQn);
		
		huart5.Instance = UART5;
		if (HAL_UART_Init(&huart5) != HAL_OK) {
			vos_Error_Handler();
		}
		HAL_UART_Receive_IT(&huart5, vos_uart_buf5, IT_LEN);
	}
	else {
		//...
	}
}
//发送字节
void vos_USART_WriteByte( int8 ID, uint8 tempc )
{
	tx_buf[0] = tempc;
	
	if( ID == 0 ) {
		HAL_UART_Transmit(&huart0, tx_buf, 1, 1000);
	}
	else if( ID == 1 ) {
		HAL_UART_Transmit(&huart1, tx_buf, 1, 1000 );
	}
	else if( ID == 2 ) {
		HAL_UART_Transmit(&huart2, tx_buf, 1, 1000);
	}
	else if( ID == 3 ) {
		HAL_UART_Transmit(&huart3, tx_buf, 1, 1000);
	}
	else if( ID == 4 ) {
		HAL_UART_Transmit(&huart4, tx_buf, 1, 1000);
	}
	else if( ID == 5 ) {
		HAL_UART_Transmit(&huart5, tx_buf, 1, 1000);
	}
	else {
		//...
	}
}

uint32 vos_Timer_Tick0()
{
	return vos_Tick_Value;
}
uint32 vos_Timer_Tick0MS()
{
	return 100;
}
void HAL_TIM_Callback(TIM_HandleTypeDef *htim)
{
	vos_Tick_Value++;
	
	if( vos_DrvOpen ) {
		remo_tick_run();
	}
}
uint32 vos_Timer_Tick1()
{
	return 0;
}
uint32 vos_Timer_Tick1US()
{
	return 1;
}
#ifndef VOS_US_Tick
volatile uint32 SoftDelay_Temp;
void SoftDelay_1us( int32 nCount )
{
	for(; nCount > 0; nCount--) {
		for( uint8 i = 0; i < 21; ++i ) {
			SoftDelay_Temp++;
		}
	}
}
#endif

//开启定时器
void vos_DrvTimer_Start()
{
	vos_DrvOpen = true;
}
//关闭定时器
void vos_DrvTimer_Stop()
{
	vos_DrvOpen = false;
}

GPIO_InitTypeDef vos_GPIO_InitStruct = {0};
uint8 vos_IO_OutBit[48];

void IO_DirWrite( uint8 i, uint8 d )
{
	GPIO_TypeDef *GPIOx = GPIOA;
	
	if( i >= 16 ) {
		i -= 16;
		GPIOx = GPIOB;
	}
	uint32 x1 = 0x01;
	uint32 pin = x1 << i;
	
	if( d == 0 ) {
		vos_GPIO_InitStruct.Mode = GPIO_MODE_INPUT;
		vos_GPIO_InitStruct.Pull = GPIO_PULLUP;
	}
	else {
		vos_GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT;
		vos_GPIO_InitStruct.Pull = GPIO_NOPULL;
	}
	vos_GPIO_InitStruct.Pin = pin;
	HAL_GPIO_Init( GPIOx, &vos_GPIO_InitStruct );
}
void IO_OutWrite( uint8 i, uint8 d )
{
	GPIO_TypeDef *GPIOx = GPIOA;
	vos_IO_OutBit[i] = d;
	if( i >= 16 ) {
		i -= 16;
		GPIOx = GPIOB;
	}
	uint32 x1 = 0x01;
	uint32 pin = x1 << i;
	
	//原版 驱动彩灯不行
	//HAL_GPIO_WritePin( GPIOx, pin, d );
	
	
	if( d == 0 ) {
		CLEAR_BIT(GPIOx->DATA, pin);
	}
	else {
		SET_BIT(GPIOx->DATA, pin);
	}
}
uint8 IO_OutRead( uint8 i )
{
	return vos_IO_OutBit[i];
}
uint8 IO_InRead( uint8 i )
{
	GPIO_TypeDef *GPIOx = GPIOA;
	
	if( i >= 16 ) {
		i -= 16;
		GPIOx = GPIOB;
	}
	uint32 x1 = 0x01;
	uint32 pin = x1 << i;
	
	return HAL_GPIO_ReadPin( GPIOx, pin );
}

ADC_HandleTypeDef hadc0;
ADC_HandleTypeDef hadc1;
ADC_HandleTypeDef hadc2;
ADC_HandleTypeDef hadc3;

void vos_IO_AnalogOpen( uint8 i, uint8 d )
{
	if ( i == 1 ) {
		hadc0.Instance = ADC;
		hadc0.Init.channel = ADC_CHANNEL_0;
		hadc0.Init.freq = 1000;
		if (HAL_ADC_Init(&hadc0) != HAL_OK) {}
		__HAL_AFIO_REMAP_ADC(GPIOA, GPIO_PIN_1);
	}
	else if ( i == 4 ) {
		hadc1.Instance = ADC;
		hadc1.Init.channel = ADC_CHANNEL_0;
		hadc1.Init.freq = 1000;
		if (HAL_ADC_Init(&hadc1) != HAL_OK) {}
		__HAL_AFIO_REMAP_ADC(GPIOA, GPIO_PIN_4);
	}
	else if ( i == 3 ) {
		hadc2.Instance = ADC;
		hadc2.Init.channel = ADC_CHANNEL_0;
		hadc2.Init.freq = 1000;
		if (HAL_ADC_Init(&hadc2) != HAL_OK) {}
		__HAL_AFIO_REMAP_ADC(GPIOA, GPIO_PIN_3);
	}
	else if ( i == 2 ) {
		hadc3.Instance = ADC;
		hadc3.Init.channel = ADC_CHANNEL_0;
		hadc3.Init.freq = 1000;
		if (HAL_ADC_Init(&hadc3) != HAL_OK) {}
		__HAL_AFIO_REMAP_ADC(GPIOA, GPIO_PIN_2);
	}
	else {
		//...
	}
}
int32 vos_IO_AnalogRead( uint8 i )
{
	int32 value = 0;
	if( i == 1 ) {
		HAL_ADC_Start(&hadc0);
		HAL_ADC_PollForConversion(&hadc0);
		HAL_ADC_Stop(&hadc0);
		value = HAL_ADC_GetValue(&hadc0);
	}
	if( i == 4 ) {
		HAL_ADC_Start(&hadc1);
		HAL_ADC_PollForConversion(&hadc1);
		HAL_ADC_Stop(&hadc1);
		value = HAL_ADC_GetValue(&hadc1);
	}
	if( i == 3 ) {
		HAL_ADC_Start(&hadc2);
		HAL_ADC_PollForConversion(&hadc2);
		HAL_ADC_Stop(&hadc2);
		value = HAL_ADC_GetValue(&hadc2);
	}
	if( i == 2 ) {
		HAL_ADC_Start(&hadc3);
		HAL_ADC_PollForConversion(&hadc3);
		HAL_ADC_Stop(&hadc3);
		value = HAL_ADC_GetValue(&hadc3);
	}
	else {
		//...
	}
	//这一段是测试出来的.....
	value = value / 64 + 512;
	if( value < 0 ) value = 0;
	if( value > 1023 ) value = 1023;
	return value;
}
void vos_IO_PWM_SetPin( uint8 clk )
{
	
}
void vos_IO_PWM_SetData( uint8 clk, int32 data32, int32 max )
{
	
}

/******************************0x80FFFFF
 *            USER PARAM
 * ****************************0x80XXXXX
 *            RUN IMAGE
 * ****************************0x8010400
 *            RUN IMAGE HEADER
 * ****************************0x8010000
 *            SECBOOT IMAGE
 * ****************************0x8002400
 *            SECBOOT IMAGE HEAER
 * ****************************0x8002000
 *            CHIP DATA
 * ****************************0x8000000*/

#define VOS_TEST_ADDR 0x80000

void vos_Flash_Load()
{
	HAL_FLASH_Read( VOS_TEST_ADDR, vos_CODE_RAM, VOS_SYS_ROM_LENGTH );
}

void vos_Flash_Save()
{
	HAL_FLASH_Write( VOS_TEST_ADDR, vos_CODE_RAM, VOS_SYS_ROM_LENGTH );
}
void vos_Flash_Clear()
{
	
}
void vos_Flash_WriteUint32( uint32 addr, uint32 b )
{
	
}

volatile int32 Timer_i;

void vos_WS2812_Delay_0_4us()
{
	Timer_i += 1;
	//Timer_i += 1;
	//Timer_i += 1;
}
void vos_WS2812_WriteData( uint8 id, uint32 d )
{
	for( int8 i = 0; i < 24; ++i ) {
		
		if( (d & 0x800000) == 0 ) {
			IO_OutWrite( id, 1 );
			Timer_i += 1;
			IO_OutWrite( id, 0 );
			vos_WS2812_Delay_0_4us();
			vos_WS2812_Delay_0_4us();
			vos_WS2812_Delay_0_4us();
			vos_WS2812_Delay_0_4us();
			vos_WS2812_Delay_0_4us();
			vos_WS2812_Delay_0_4us();
			vos_WS2812_Delay_0_4us();
		}
		else {
			IO_OutWrite( id, 1 );
			vos_WS2812_Delay_0_4us();
			vos_WS2812_Delay_0_4us();
			vos_WS2812_Delay_0_4us();
			vos_WS2812_Delay_0_4us();
			vos_WS2812_Delay_0_4us();
			vos_WS2812_Delay_0_4us();
			vos_WS2812_Delay_0_4us();
			IO_OutWrite( id, 0 );
			//Timer_i += 1;
		}
		d <<= 1;
	}
}
void vos_UserExtDriver( uint32 v24, uint32 v8 )
{
	
}

