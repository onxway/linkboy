/*
 * Copyright (c) 2021 linkboy
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include <stdio.h>

#include <unistd.h>

#include "ohos_init.h"
#include "cmsis_os2.h"

#include "iot_gpio.h"

#include "hi_io.h"
#include "hi_adc.h"
#include "hi_gpio.h"
#include "hi_pwm.h"
#include "hi_uart.h"
#include "hi_time.h"
#include "hi_flash.h"
#include "hi_hrtimer.h"
#include "hi_partition_table.h"

//========================================================================================

//定义用户ROM区起始地址
#define Flash_USER_ADDR 0

#include "user/config_c.h"

//#define C_remo_SYS_BASE_LENGTH 4096 //30K

void IO_Init(void);
void Flash_Init(void);
void remo_DrvTimer_Init(void);

//系统初始化
void vos_User_Init()
{
	IO_Init();
	Flash_Init();
	remo_DrvTimer_Init();
}
void vos_User_Start()
{
	vos_USART_Open( 0, 115200 );
	//这里应该打开串口标志!
}
//开启全局中断
void vos_SYS_InterruptEnable()
{
	// __enable_irq();
}
//关闭全局中断
void vos_SYS_InterruptDisable()
{
	//__disable_irq();
}

uint8_t uart_send_buff[10];

//初始化串口0
void remo_USART0_Init(uint32 BaudRate)
{
    hi_uart_attribute uart_attr = {
        .baud_rate = BaudRate,
        //data_bits: 8bits
        .data_bits = 8,
        .stop_bits = 1,
        .parity = 0,
    };
	IoTGpioInit(HI_IO_NAME_GPIO_3);
    IoTGpioSetDir(HI_IO_NAME_GPIO_3, IOT_GPIO_DIR_OUT);
    IoTGpioInit(HI_IO_NAME_GPIO_4);
    IoTGpioSetDir(HI_IO_NAME_GPIO_4, IOT_GPIO_DIR_IN);
    
    hi_io_set_func(HI_IO_NAME_GPIO_3, HI_IO_FUNC_GPIO_3_UART0_TXD); /* uart0 tx */ 
    hi_io_set_func(HI_IO_NAME_GPIO_4, HI_IO_FUNC_GPIO_4_UART0_RXD); /* uart0 rx */ 

	uint32_t ret = hi_uart_init( HI_UART_IDX_0, &uart_attr, NULL );
    if ( ret != HI_ERR_SUCCESS ) {
        printf("Failed to init uart! Err code = %d\n", ret);
        return;
    }
}
//初始化串口1
void remo_USART1_Init(uint32 BaudRate)
{
    hi_uart_attribute uart_attr = {
        .baud_rate = BaudRate,
        //data_bits: 8bits
        .data_bits = 8,
        .stop_bits = 1,
        .parity = 0,
    };
	IoTGpioInit(HI_IO_NAME_GPIO_6);
    IoTGpioSetDir(HI_IO_NAME_GPIO_6, IOT_GPIO_DIR_OUT);
    IoTGpioInit(HI_IO_NAME_GPIO_5);
    IoTGpioSetDir(HI_IO_NAME_GPIO_5, IOT_GPIO_DIR_IN);
    
    hi_io_set_func(HI_IO_NAME_GPIO_6, HI_IO_FUNC_GPIO_6_UART1_TXD); /* uart0 tx */ 
    hi_io_set_func(HI_IO_NAME_GPIO_5, HI_IO_FUNC_GPIO_5_UART1_RXD); /* uart0 rx */ 

	uint32_t ret = hi_uart_init( HI_UART_IDX_1, &uart_attr, NULL );
    if ( ret != HI_ERR_SUCCESS ) {
        printf("Failed to init uart! Err code = %d\n", ret);
        return;
    }
}
void remo_USART2_Init(uint32 BaudRate)
{
    hi_uart_attribute uart_attr = {
        .baud_rate = BaudRate,
        //data_bits: 8bits
        .data_bits = 8,
        .stop_bits = 1,
        .parity = 0,
    };
	IoTGpioInit(HI_IO_NAME_GPIO_11);
    IoTGpioSetDir(HI_IO_NAME_GPIO_11, IOT_GPIO_DIR_OUT);
    IoTGpioInit(HI_IO_NAME_GPIO_12);
    IoTGpioSetDir(HI_IO_NAME_GPIO_12, IOT_GPIO_DIR_IN);

    //Initialize uart driver
    hi_io_set_func(HI_IO_NAME_GPIO_11, HI_IO_FUNC_GPIO_11_UART2_TXD); /* uart2 tx */ 
    hi_io_set_func(HI_IO_NAME_GPIO_12, HI_IO_FUNC_GPIO_12_UART2_RXD); /* uart2 rx */ 

	uint32_t ret = hi_uart_init( HI_UART_IDX_2, &uart_attr, NULL );
    if ( ret != HI_ERR_SUCCESS ) {
        printf("Failed to init uart! Err code = %d\n", ret);
        return;
    }
}
//串口波特率
void vos_USART_Open( int8 ID, uint32 b )
{
	if( ID == 0 ) {
		remo_USART0_Init( b );
	}
	if( ID == 1 ) {
		remo_USART1_Init( b );
	}
	if( ID == 2 ) {
		remo_USART2_Init( b );
	}
}
//发送字节
void vos_USART_WriteByte( int8 ID, uint8 tempc )
{
	if( ID == 0 ) {
		uart_send_buff[0] = tempc;
		hi_uart_write( HI_UART_IDX_0, uart_send_buff, 1 );
	}
	if( ID == 1 ) {
		uart_send_buff[0] = tempc;
		hi_uart_write( HI_UART_IDX_1, uart_send_buff, 1 );
	}
	if( ID == 2 ) {
		uart_send_buff[0] = tempc;
		hi_uart_write( HI_UART_IDX_2, uart_send_buff, 1 );
	}
}

//====================================================================================

uint32 vos_Timer_Tick0()
{
	return (uint32)hi_get_us();
}
uint32 vos_Timer_Tick0MS()
{
	return 1000;
}
uint32 vos_Timer_Tick1()
{
	return (uint32)hi_get_us();
}
uint32 vos_Timer_Tick1US()
{
	return 1;
}

//========================================================================================

#define NUMBER HI_GPIO_IDX_MAX

uint32 GPIO_MODE_List[NUMBER] = {
	HI_IO_FUNC_GPIO_0_GPIO,
	HI_IO_FUNC_GPIO_1_GPIO,
	HI_IO_FUNC_GPIO_2_GPIO,
	HI_IO_FUNC_GPIO_3_GPIO,
	HI_IO_FUNC_GPIO_4_GPIO,
	HI_IO_FUNC_GPIO_5_GPIO,
	HI_IO_FUNC_GPIO_6_GPIO,
	HI_IO_FUNC_GPIO_7_GPIO,
	HI_IO_FUNC_GPIO_8_GPIO,
	HI_IO_FUNC_GPIO_9_GPIO,
	HI_IO_FUNC_GPIO_10_GPIO,
	HI_IO_FUNC_GPIO_11_GPIO,
	HI_IO_FUNC_GPIO_12_GPIO,
	HI_IO_FUNC_GPIO_13_GPIO,
	HI_IO_FUNC_GPIO_14_GPIO,
};

void IO_Init()
{
	hi_gpio_init();

	for( uint8 i = 0; i < NUMBER; ++i ) {
		hi_gpio_set_dir( i, IOT_GPIO_DIR_IN );
		hi_io_set_pull( i, HI_IO_PULL_UP );
		hi_io_set_func( i, GPIO_MODE_List[i] );
	}

	//初始化PWM
	if (hi_pwm_set_clock(PWM_CLK_160M) != HI_ERR_SUCCESS) {
		return; // (unsigned int)HAL_WIFI_IOT_FAILURE;
    }
}

void IO_DirWrite( uint8 i, uint8 d )
{
	hi_io_set_func( i, GPIO_MODE_List[i] );

	if( d == 0 ) {
		hi_gpio_set_dir( i, IOT_GPIO_DIR_IN );
		hi_io_set_pull( i, HI_IO_PULL_UP );
	}
	else {
		hi_gpio_set_dir( i, IOT_GPIO_DIR_OUT );
	}
}
void IO_OutWrite( uint8 i, uint8 d )
{
	//A3 A4需要加上这两行指令, 研究下为什么
	//hi_io_set_func( i, GPIO_MODE_List[i] );

	if( d == 0 ) {
		hi_gpio_set_ouput_val( i, 0 );
	}
	else {
		hi_gpio_set_ouput_val( i, 1 );
	}
}
uint8 IO_OutRead( uint8 i )
{
	hi_gpio_value hv;
	hi_gpio_get_output_val( i, &hv );
	return hv;
}
uint8 IO_InRead( uint8 i )
{
	hi_gpio_value hv;
	hi_gpio_get_input_val( i, &hv );
	return hv;
}
//-------------------------------------------------
int8 IO_AnalogGetID( uint8 i )
{
	/*
	IO4  -> ADC1
	IO5  -> ADC2
	IO7  -> ADC3
	IO9  -> ADC4
	IO11 -> ADC5
	IO12 -> ADC0
	IO13 -> ADC6
	*/
	switch( i ) {
		case 4: return 1;
		case 5: return 2;
		case 7: return 3;
		case 9: return 4;
		case 11: return 5;
		case 12: return 0;
		case 13: return 6;
		default: return -1;
	}
}
void vos_IO_AnalogOpen( uint8 i, uint8 d )
{
	int8 j = IO_AnalogGetID( i );
	if( j == -1 ) {
		return;
	}
	hi_gpio_set_dir( (uint8)j, IOT_GPIO_DIR_IN );
	hi_io_set_pull( (uint8)j, HI_IO_PULL_NONE );
	hi_io_set_func( (uint8)j, GPIO_MODE_List[i] );
}
int32 vos_IO_AnalogRead( uint8 i )
{
	int8 j = IO_AnalogGetID( i );
	if( j == -1 ) {
		return;
	}
	uint16 data = 0;
	uint32 ret = hi_adc_read( (hi_adc_channel_index)j, &data, HI_ADC_EQU_MODEL_1, HI_ADC_CUR_BAIS_3P3V, 0 ); //HI_ADC_CUR_BAIS_DEFAULT
	if( ret != HI_ERR_SUCCESS ) {
		return 0;
	}
	uint32 ddd = data;
	return (int)ddd / 2;
}
//-------------------------------------------------

const uint8 PWM_List[] = {3,4,2,5,1,2,3,0,1, 0,1,2,3,4,5};

void vos_IO_PWM_SetPin( uint8 clk )
{
	//设置GPIO_2引脚复用功能为PWM
    hi_io_set_func( clk, 5 ); //HI_IO_FUNC_GPIO_2_PWM2_OUT 都是常量5
    hi_pwm_init( PWM_List[clk] );
}

void vos_IO_PWM_SetData( uint8 clk, int32 data, int32 max )
{
	hi_pwm_start( PWM_List[clk], data, max );
	//hi_pwm_stop( 2 );
}

//========================================================================================

//参考示例里边设定为常量 #define TEST_FLASH_OFFSET 0x001FF000

//读取到的地址是 0x001F0000, SIZE是 0x00005000, 但增加了0x1800, 可能是占用另一个分区,
//一共是26624 (0x6800)
#define VOS_TEMP_FlashArea 0x1800

hi_u32 usr_partion_addr;
hi_u32 usr_partion_size;

void Flash_Init()
{
	hi_flash_init();

	hi_flash_partition_init();

	//获取用户分区
    hi_get_usr_partition_table( &usr_partion_addr, &usr_partion_size );

	//usr_partion_size += 0x2000; //可能是不行???
	usr_partion_size += VOS_TEMP_FlashArea;

	//获取文件系统分区
	//hi_get_fs_partition_table( &usr_partion_addr, &usr_partion_size );
}
void vos_Flash_Load()
{
	hi_flash_read( usr_partion_addr, usr_partion_size, vos_CODE_RAM );
}
void vos_Flash_Save()
{
	hi_flash_write( usr_partion_addr, usr_partion_size, vos_CODE_RAM, true );
}
void vos_Flash_Clear()
{
	
}
void vos_Flash_WriteUint32( uint32 addr, uint32 b )
{
	
}
//========================================================================================

hi_u32 drv_timer;
bool remo_DrvTimer_open;

uint32 remo_DrvTimer_temp;
uint8 remo_DrvTimer_tick;

bool remo_DrvTimer_running;

//初始化定时器
void remo_DrvTimer_Init()
{
	// create timer handle
    hi_u32 ret = hi_hrtimer_create( &drv_timer );
	if( ret != HI_ERR_SUCCESS ) {
		printf("=====ERROR===== hrtimer handle create ret is: %d !!\r\n", ret);
		return;
	}
	remo_DrvTimer_open = false;
	remo_DrvTimer_running = false;

	remo_DrvTimer_tick = 0;
}
static hi_void hrtimer_callback( hi_u32 data )
{
	if( remo_DrvTimer_running ) {
		return;
	}
	//必须要放到最前边, 才能保证时间精度
	if( remo_DrvTimer_open ) {
		int ret = hi_hrtimer_start( drv_timer, 100, hrtimer_callback, 0 );
	}
	remo_DrvTimer_running = true;

	//100KHz 部分模块可能有误差
	remo_tick_run10us();

	remo_tick_run100us();

	remo_DrvTimer_tick++;
	if( remo_DrvTimer_tick >= 10 ) {
		remo_DrvTimer_tick = 0;
		remo_tick_run1000us();
	}
	remo_DrvTimer_running = false;


	remo_DrvTimer_temp++;
	if( remo_DrvTimer_temp == 10000 ) {
		remo_DrvTimer_temp = 0;
		//remo_USART_write( 'A' );
	}

}
//开启定时器
void vos_DrvTimer_Start()
{
	remo_DrvTimer_open = true;
	int ret = hi_hrtimer_start( drv_timer, 100, hrtimer_callback, 0 );
}
//停止定时器
void vos_DrvTimer_Stop()
{
	remo_DrvTimer_open = false;
	hi_hrtimer_stop( drv_timer );
}

//========================================================================================

//短延时, 后续应该尽量换成 nop 指令
volatile int32 Timer_i;
void vos_WS2812_Delay_0_4us(void)
{
	//以下针对 E103CBT6 120MHz
	Timer_i += 1;
	Timer_i += 1;
	//Timer_i += 1;
}
void vos_WS2812_WriteData( uint8 id, uint32 d )
{
	//GD32V-108MHz
	//F411RE-100MHz
	
	for( int8 i = 0; i < 24; ++i ) {
	
		hi_gpio_set_ouput_val( id, 1 );
		if( (d & 0x800000) == 0 ) {
			Timer_i += 1;
			//Timer_i += 1;
			//Timer_i += 1;
			//Timer_i += 1;
			hi_gpio_set_ouput_val( id, 0 );
			Timer_i += 1;
			Timer_i += 1;
			Timer_i += 1;
			Timer_i += 1;
			Timer_i += 1;
			Timer_i += 1;
			Timer_i += 1;
			Timer_i += 1;
			Timer_i += 1;
			Timer_i += 1;
			Timer_i += 1;
			Timer_i += 1;
			Timer_i += 1;
			Timer_i += 1;
		}
		else {
			Timer_i += 1;
			Timer_i += 1;
			Timer_i += 1;
			Timer_i += 1;
			Timer_i += 1;
			Timer_i += 1;
			Timer_i += 1;
			Timer_i += 1;
			Timer_i += 1;
			Timer_i += 1;
			Timer_i += 1;
			Timer_i += 1;
			Timer_i += 1;
			Timer_i += 1;
			Timer_i += 1;
			Timer_i += 1;
			hi_gpio_set_ouput_val( id, 0 );
			Timer_i += 1;
			Timer_i += 1;
			Timer_i += 1;
			Timer_i += 1;
			//Timer_i += 1;
			//Timer_i += 1;
		}
		d <<= 1;
	}
}
//===========================================
void ESP32_wifiAP_Deal( uint32 v );
void ESP32_wifiSTA_Deal( uint32 v );
void ESP32_TCP_Server_Deal( uint32 v );
void ESP32_TCP_Client_Deal( uint32 v );

void vos_UserExtDriver( uint32 v24, uint32 v8 )
{
	switch( v24 ) {
		
		//K210专用 200 -299
		
		//ESP32专用 300 - 399
		case 300:	ESP32_wifiAP_Deal( v8 ); break;
		case 301:	ESP32_wifiSTA_Deal( v8 ); break;
		case 302:	ESP32_TCP_Server_Deal( v8 ); break;
		case 303:	ESP32_TCP_Client_Deal( v8 ); break;
		
		default:	Error( E_VM_InterfaceError, (uint8)v24 );
					vos_Running = false;
					break;
	}
}

//ESP32 wifi AP接口
void wifi_init_softap(uint8 *p_SSID, uint8 *p_PASS);
void ESP32_wifiAP_softap( uint32 ssid, uint32 pass );

extern bool wifiAP_isConnected;

void ESP32_wifiAP_Deal( uint32 v )
{
	switch( v ) {
		case 0:
			u0 = Mem_Get_uint32( DP + 0 );
			u1 = Mem_Get_uint32( DP + 4 );
			ESP32_wifiAP_softap( u0, u1 );
			break;
		case 1:
			A = wifiAP_isConnected;
			break;
		//其他
		default:
			Error( E_VM_InterfaceError, (uint8)v );
			break;
	}
}
void ESP32_wifiAP_softap( uint32 ssid, uint32 pass )
{
	uint8 *p_SSID = Mem_GetAddr( ssid );
	uint8 *p_PASS = Mem_GetAddr( pass );
	
	/*
	//得到SSID
	bool isCode = (ssid & 0xFF0000) != 0;
	ssid &= 0xFFFF;
	if( isCode ) {
		p_SSID = (uint8*)Flash_GetAddr( ssid );
	}
	else {
		p_SSID = &BASE[ssid];
	}
	//得到PASS
	isCode = (pass & 0xFF0000) != 0;
	pass &= 0xFFFF;
	if( isCode ) {
		p_PASS = (uint8*)Flash_GetAddr( pass );
	}
	else {
		p_PASS = &BASE[ssid];
	}
	*/
	//初始化wifi
	wifi_init_softap( p_SSID, p_PASS );
}

//=====================================================================

//ESP32 wifi接口
void ESP32_wifiSTA_softap( uint32 ssid, uint32 pass );
void wifi_init_sta(uint8 *p_SSID, uint8 *p_PASS);
void wifiSTA_VOS_Reconnect(void);
void wifiSTA_VOS_Disconnect(void);

extern bool wifiSTA_isConnected;

void ESP32_wifiSTA_Deal( uint32 v )
{
	switch( v ) {
		case 0:
			
			break;
		case 1:
			u0 = Mem_Get_uint32( DP + 0 );
			u1 = Mem_Get_uint32( DP + 4 );
			ESP32_wifiSTA_softap( u0, u1 );
			break;
		case 2:
			A = wifiSTA_isConnected;
			break;
		//其他
		default:
			Error( E_VM_InterfaceError, (uint8)v );
			break;
	}
}
void ESP32_wifiSTA_softap( uint32 ssid, uint32 pass )
{
	uint8 *p_SSID = Mem_GetAddr( ssid );
	uint8 *p_PASS = Mem_GetAddr( pass );;
	
	//初始化wifi
	wifi_init_sta( p_SSID, p_PASS );
}
//=====================================================================
//ESP32 TCP_Server接口

void Main_TCP_Init( uint32 buf, int32 n );
void Main_StartTCP( int32 port );
void ESP32_TCP_Server_SendString( uint32 text );
void TCP_Server_SendString(uint8 *p );

extern bool TCP_isConnected;
extern int32 TCP_RLength;

void ESP32_TCP_Server_Deal( uint32 v )
{
	switch( v ) {
		case 0:
			u0 = Mem_Get_uint32( DP + 0 );
			i0 = Mem_Get_int32( DP + 4 );
			Main_TCP_Init( u0, i0 );
			break;
		case 1:
			i0 = Mem_Get_int32( DP + 0 );
			Main_StartTCP( i0 );
			break;
		case 2:
			A = TCP_isConnected;
			break;
		case 3:
			u0 = Mem_Get_uint32( DP + 0 );
			ESP32_TCP_Server_SendString( u0 );
			break;
		case 4:
			A = TCP_RLength;
			TCP_RLength = 0;
			break;
		//其他
		default:
			Error( E_VM_InterfaceError, (uint8)v );
			break;
	}
}
void ESP32_TCP_Server_SendString( uint32 addr )
{
	uint8 *p = Mem_GetAddr( addr );
	/*
	//得到text
	bool isCode = (addr & 0xFF0000) != 0;
	addr &= 0xFFFF;
	if( isCode ) {
		p = (uint8*)Flash_GetAddr( addr );
	}
	else {
		p = &BASE[addr];
	}
	*/
	//发送字符串
	TCP_Server_SendString( p );
}

//=====================================================================
//ESP32 TCP_Client接口

void Main_TCP_Client_Init( uint32 buf, int32 n );
void Main_TCP_Client_Connect( uint32 IP, int32 port );
void ESP32_TCP_Client_SendString( uint32 text );
void TCP_Client_SendString(uint8 *p );

extern bool TCP_Client_isConnected;
extern int32 TCP_Client_RLength;

void ESP32_TCP_Client_Deal( uint32 v )
{
	switch( v ) {
		case 0:
			u0 = Mem_Get_uint32( DP + 0 );
			i0 = Mem_Get_int32( DP + 4 );
			Main_TCP_Client_Init( u0, i0 );
			break;
		case 1:
			u0 = Mem_Get_uint32( DP + 0 );
			i0 = Mem_Get_int32( DP + 4 );
			Main_TCP_Client_Connect( u0, i0 );
			break;
		case 2:
			A = TCP_Client_isConnected;
			break;
		case 3:
			u0 = Mem_Get_uint32( DP + 0 );
			ESP32_TCP_Client_SendString( u0 );
			break;
		case 4:
			A = TCP_Client_RLength;
			TCP_Client_RLength = 0;
			break;
		//其他
		default:
			Error( E_VM_InterfaceError, (uint8)v );
			break;
	}
}
void ESP32_TCP_Client_SendString( uint32 addr )
{
	uint8 *p = Mem_GetAddr( addr );
	
	//发送字符串
	TCP_Client_SendString( p );
}



