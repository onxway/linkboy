
/*

ARM® Cortex®-M4 32-bit MCU, 
Max Speed (MHz): 120; 
Memory (Bytes) Flash: 1024K; 
Memory (Bytes) SRAM: 96K; 
I/O: up to 37; 
Timer GPTM (16bit): 10; 
Timer Advanced TM (16bit): 1; 
Timer Basic TM (16bit): 2; 
Timer SysTick (24bit): 1; 
Timer WDG: 2; 
Timer RTC: 1; 
Connectivity I2C: 2; 
Connectivity SPI: 3; 
Connectivity USB 2 FS: 1; 
Connectivity I2S: 2; 
Analog Interface 12bit ADC Units (CHs): 3(10); 
Analog Interface 12bit DAC Units: 2; 
Package: LQFP48

*/

//-------------------------------------
//64 * 1024
//#define C_remo_SYS_BASE_LENGTH 0x10000

//-------------------------------------

#define Flash_PageSize 2048
#define Flash_StartPage 24	//2021.6.7 已用: 45K, 预留48K
#define Flash_PageNumber 64 //用到128K

//本语句仅适用于从最低位的规范区间计算
#define Flash_USER_ADDR (0x08000000 + Flash_PageSize * Flash_StartPage)

#include "user/config_c.h"

//==========================================

#include "gd32f30x_usart.h"
#include "gd32f30x_gpio.h"
#include "gd32f30x.h"


#define remo_CLOCK_M 120

#include "core/remo_Device/GD32F303x_c.h"

//===========================================

//短延时, 后续应该尽量换成 nop 指令
volatile int32 Timer_i;
void vos_WS2812_Delay_0_4us(void)
{
	/*
	Timer_i += 1;
	Timer_i += 1;
	Timer_i += 1;
	Timer_i += 1;
	Timer_i += 1;
	*/
	
	//以下针对 E103CBT6 120MHz
	Timer_i += 1;
	Timer_i += 1;
	//Timer_i += 1;
}

void IO_OutWrite_0( uint8 i )
{
	GPIO_BC(GPION_LIST[i]) = GPIO_LIST[i];
}
void IO_OutWrite_1( uint8 i )
{
	GPIO_BOP(GPION_LIST[i]) = GPIO_LIST[i];
}

void vos_WS2812_WriteData( uint8 id, uint32 d )
{
	//GD32V-108MHz
	//F411RE-100MHz
	
	for( int8 i = 0; i < 24; ++i ) {
		
		IO_OutWrite_1( id );
		if( (d & 0x800000) == 0 ) {
			Timer_i += 1;
			//Timer_i += 1;
			//Timer_i += 1;
			//Timer_i += 1;
			IO_OutWrite_0( id );
			Timer_i += 1;
			Timer_i += 1;
			Timer_i += 1;
			Timer_i += 1;
			Timer_i += 1;
			Timer_i += 1;
			Timer_i += 1;
			Timer_i += 1;
			Timer_i += 1;
			Timer_i += 1;
			Timer_i += 1;
			Timer_i += 1;
			Timer_i += 1;
			Timer_i += 1;
		}
		else {
			Timer_i += 1;
			Timer_i += 1;
			Timer_i += 1;
			Timer_i += 1;
			Timer_i += 1;
			Timer_i += 1;
			Timer_i += 1;
			Timer_i += 1;
			Timer_i += 1;
			Timer_i += 1;
			Timer_i += 1;
			Timer_i += 1;
			Timer_i += 1;
			Timer_i += 1;
			Timer_i += 1;
			Timer_i += 1;
			IO_OutWrite_0( id );
			Timer_i += 1;
			Timer_i += 1;
			Timer_i += 1;
			Timer_i += 1;
			//Timer_i += 1;
			//Timer_i += 1;
		}
		d <<= 1;
	}
}

void vos_UserExtDriver( uint32 v24, uint32 v8 )
{
	
}

