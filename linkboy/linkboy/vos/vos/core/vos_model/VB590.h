
//定义用户ROM区起始地址, 源码和固件模式设置为0即可 未用
#define Flash_USER_ADDR 0

#include "user/config_c.h"


static csi_gpio_t mygpio;

//系统初始化
void vos_User_Init()
{
	csi_gpio_init(& mygpio, 0 );
}
//系统初始化
void vos_User_Start()
{
	
}
//开启全局中断
void vos_SYS_InterruptEnable()
{
	//【注意】: 这里添加开启全局中断程序
}
//关闭全局中断
void vos_SYS_InterruptDisable()
{
	//【注意】: 这里添加关闭全局中断程序
}

//串口波特率
void vos_USART_Open( int8 ID, uint32 b )
{
	
}
//发送字节
void vos_USART_WriteByte( int8 ID, uint8 tempc )
{
	
}
//Deal( data );	
//UART_List_Receive( 0, data );


#include <drv/tick.h>

uint32 vos_Timer_Tick0()
{
	return csi_tick_get_us();
}
uint32 vos_Timer_Tick0MS()
{
	return 1000;
}
uint32 vos_Timer_Tick1()
{
	return csi_tick_get_us();
}
uint32 vos_Timer_Tick1US()
{
	return 1;
}


#include <drv/gpio.h>

void IO_DirWrite( uint8 i, uint8 d )
{
	uint32 pin = 0x01;
	uint32 mask = pin << i;
	if( d == 0 ) {
		csi_gpio_dir( &mygpio, mask,  GPIO_DIRECTION_INPUT );
		csi_gpio_mode( &mygpio, mask,  GPIO_MODE_PULLUP );
	}
	else {
		csi_gpio_dir( &mygpio, mask,  GPIO_DIRECTION_OUTPUT );
	}
}
void IO_OutWrite( uint8 i, uint8 d )
{
	uint32 pin = 0x01;
	uint32 mask = pin << i;
	if( d == 0 ) {
		csi_gpio_write( &mygpio, mask,  GPIO_PIN_LOW );
	}
	else {
		csi_gpio_write( &mygpio, mask,  GPIO_PIN_HIGH );
	}
}
uint8 IO_OutRead( uint8 i )
{
	return 0; //【注意】: 这里添加读取GPIO输出电平函数
}
uint8 IO_InRead( uint8 i )
{
	return 0; //【注意】: 这里添加读取GPIO输入电平函数
}
void vos_IO_AnalogOpen( uint8 i, uint8 d )
{
	
}
int32 vos_IO_AnalogRead( uint8 i )
{
	return 0; //【注意】: 这里添加读取GPIO-ADC输入值函数
}
void vos_IO_PWM_SetPin( uint8 clk )
{
	
}
void vos_IO_PWM_SetData( uint8 clk, int32 data, int32 max )
{
	
}

void vos_Flash_Load()
{	
}
void vosFlash_Save()
{
}
void vosFlash_Clear()
{
}
void vosFlash_WriteUint32( uint32 addr, uint32 b )
{
}

//开启定时器
void vos_DrvTimer_Start()
{
	//【注意】: 这里添加开启定时器函数
}
//关闭定时器
void vos_DrvTimer_Stop()
{
	//【注意】: 这里添加关闭定时器函数
}

//【注意】: 这里修改为实际的定时器中断回调函数
void TIMER_X_IRQHandler()
{
	//触发频率为 100KHz
	remo_tick_run();
}

void vos_WS2812_Delay_0_4us(void)
{
}

void vos_WS2812_WriteData( uint8 id, uint32 d )
{
}

void vos_UserExtDriver( uint32 v24, uint32 v8 )
{
}



