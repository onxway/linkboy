
//定义用户ROM区起始地址
#define Flash_USER_ADDR 0

#include "user/config_c.h"

//==========================================

//系统初始化
void vos_User_Init()
{
	
}
//用户启动, 初始化下载串口等
void vos_User_Start()
{
	
}
//开启全局中断
void vos_SYS_InterruptEnable()
{
	//【注意】: 这里添加开启全局中断程序
}
//关闭全局中断
void vos_SYS_InterruptDisable()
{
	//【注意】: 这里添加关闭全局中断程序
}
//串口波特率
void vos_USART_Open( int8 ID, uint32 b )
{

}
//发送字节
void vos_USART_WriteByte( int8 ID, uint8 tempc )
{

}
uint32 vos_Timer_Tick0()
{
	return micros();
}
uint32 vos_Timer_Tick0MS()
{
	return 1000;
}
uint32 vos_Timer_Tick1()
{
	return micros();
}
uint32 vos_Timer_Tick1US()
{
	return 1;
}
//开启定时器
void vos_DrvTimer_Start()
{
	
}
//关闭定时器
void vos_DrvTimer_Stop()
{
	
}
void IO_DirWrite( uint8 i, uint8 d )
{
	if( d == 0 ) {
		pinMode( i, INPUT_PULLUP );
	}
	else {
		pinMode( i, OUTPUT );
	}
}
void IO_OutWrite( uint8 i, uint8 d )
{
	if( d == 0 ) {
		digitalWrite( i, LOW );
	}
	else {
		digitalWrite( i, HIGH );
	}
}
uint8 IO_OutRead( uint8 i )
{
	return 0; //【注意】: 这里添加读取GPIO输出电平函数
}
uint8 IO_InRead( uint8 i )
{
	return digitalRead( i );
}
void vos_IO_AnalogOpen( uint8 i, uint8 d )
{
	
}
int32 vos_IO_AnalogRead( uint8 i )
{
	return 0; //【注意】: 这里添加读取GPIO-ADC输入值函数
}
void vos_IO_PWM_SetPin( uint8 clk )
{
	
}
void vos_IO_PWM_SetData( uint8 clk, int32 data32, int32 max )
{
	
}
void vos_Flash_Load()
{
}
void vos_Flash_Save()
{
}
void vos_Flash_Clear()
{
	//【注意】: 这里添加清空用户区域Flash函数
}
void vos_Flash_WriteUint32( uint32 addr, uint32 b )
{
	//【注意】: 这里添加写一个uint32到指定Flash地址函数
}
void vos_WS2812_Delay_0_4us(void)
{
	
}
void vos_WS2812_WriteData( uint8 id, uint32 d )
{
	
}
void vos_UserExtDriver( uint32 v24, uint32 v8 )
{
	
}



