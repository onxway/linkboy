
//-------------------------------------
//注意 32K 但是超过15可能就不行了 不知道为什么 (编译失败) 2020.8.16 可能是GD32E系列包文件设置错误
//30 * 1024
//2020.7.29 增加了CC后需要进一步减小
//#define C_remo_SYS_BASE_LENGTH 18000 //30K

//-------------------------------------

#define Flash_PageSize 1024
#define Flash_StartPage 28	//2020.3.5 已用: 28K, 预留28K
#define Flash_PageNumber 100 //用到100K

//本语句仅适用于从最低位的规范区间计算
#define Flash_USER_ADDR (0x08000000 + Flash_PageSize * Flash_StartPage)

#include "user/config_c.h"

//========================================================================================

#include "gd32e10x_usart.h"
#include "gd32e10x_gpio.h"
#include "gd32e10x.h"

#define remo_CLOCK_M 120

#include "core/remo_Device/GD32F303x_c.h"

//短延时, 后续应该尽量换成 nop 指令
volatile int32 Timer_i;
void vos_WS2812_Delay_0_4us(void)
{
	/*
	Timer_i += 1;
	Timer_i += 1;
	Timer_i += 1;
	Timer_i += 1;
	Timer_i += 1;
	*/
	
	//以下针对 E103CBT6 120MHz
	Timer_i += 1;
	Timer_i += 1;
	//Timer_i += 1;
}
void IO_OutWrite_0( uint8 i )
{
	GPIO_BC(GPION_LIST[i]) = GPIO_LIST[i];
}
void IO_OutWrite_1( uint8 i )
{
	GPIO_BOP(GPION_LIST[i]) = GPIO_LIST[i];
}
void vos_WS2812_WriteData( uint8 id, uint32 d )
{
	//GD32V-108MHz
	//F411RE-100MHz
	
	for( int8 i = 0; i < 24; ++i ) {
		
		IO_OutWrite_1( id );
		if( (d & 0x800000) == 0 ) {
			Timer_i += 1;
			//Timer_i += 1;
			//Timer_i += 1;
			//Timer_i += 1;
			IO_OutWrite_0( id );
			Timer_i += 1;
			Timer_i += 1;
			Timer_i += 1;
			Timer_i += 1;
			Timer_i += 1;
			Timer_i += 1;
			Timer_i += 1;
			Timer_i += 1;
			Timer_i += 1;
			Timer_i += 1;
			Timer_i += 1;
			Timer_i += 1;
			Timer_i += 1;
			Timer_i += 1;
		}
		else {
			Timer_i += 1;
			Timer_i += 1;
			Timer_i += 1;
			Timer_i += 1;
			Timer_i += 1;
			Timer_i += 1;
			Timer_i += 1;
			Timer_i += 1;
			Timer_i += 1;
			Timer_i += 1;
			Timer_i += 1;
			Timer_i += 1;
			Timer_i += 1;
			Timer_i += 1;
			Timer_i += 1;
			Timer_i += 1;
			IO_OutWrite_0( id );
			Timer_i += 1;
			Timer_i += 1;
			Timer_i += 1;
			Timer_i += 1;
			//Timer_i += 1;
			//Timer_i += 1;
		}
		d <<= 1;
	}
}

void vos_UserExtDriver( uint32 v24, uint32 v8 )
{
	
}
