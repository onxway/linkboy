
#include "ch32v10x.h"


//#define PAGE_WRITE_START_ADDR  ((uint32_t)0x0800F000) /* Start from 60K */
#define PAGE_WRITE_START_ADDR  ((uint32_t)0x08006000) /* Start from 24K */ //这里应该根据编译的hex尺寸, 进行调节修改
#define PAGE_WRITE_END_ADDR    ((uint32_t)0x08010000) /* End at 63K */
#define FLASH_PAGE_SIZE                   1024
#define FLASH_PAGES_TO_BE_PROTECTED FLASH_WRProt_AllPages
//定义用户ROM区起始地址
#define Flash_USER_ADDR PAGE_WRITE_START_ADDR


#include "user/config_c.h"

//==========================================
void IO_Init(void);

//这里添加用户的相关外设初始化, Tick系统等
void vos_User_Init()
{
	IO_Init();
}

//用户启动, 初始化下载串口等
void vos_User_Start()
{
	vos_USART_Open( 1, 115200 );
}
//开启全局中断
void vos_SYS_InterruptEnable() {}
//关闭全局中断
void vos_SYS_InterruptDisable() {}

//打开指定ID的串口并设置波特率
void vos_USART_Open( int8 ID, uint32 b )
{
	GPIO_InitTypeDef  GPIO_InitStructure;
	USART_InitTypeDef USART_InitStructure;
	NVIC_InitTypeDef  NVIC_InitStructure;
	
	RCC_APB2PeriphClockCmd(RCC_APB2Periph_USART1, ENABLE);
	
	/* USART1 TX-->A.9   RX-->A.10 */
	GPIO_InitStructure.GPIO_Pin = GPIO_Pin_9;
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_AF_PP;
	GPIO_Init(GPIOA, &GPIO_InitStructure);
	GPIO_InitStructure.GPIO_Pin = GPIO_Pin_10;
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_IN_FLOATING;
	GPIO_Init(GPIOA, &GPIO_InitStructure);
	
	USART_InitStructure.USART_BaudRate = b;
	USART_InitStructure.USART_WordLength = USART_WordLength_8b;
	USART_InitStructure.USART_StopBits = USART_StopBits_1;
	USART_InitStructure.USART_Parity = USART_Parity_No;
	USART_InitStructure.USART_HardwareFlowControl = USART_HardwareFlowControl_None;
	USART_InitStructure.USART_Mode = USART_Mode_Tx | USART_Mode_Rx;
	
	USART_Init(USART1, &USART_InitStructure);
	USART_ITConfig(USART1, USART_IT_RXNE, ENABLE);
	
	NVIC_InitStructure.NVIC_IRQChannel = USART1_IRQn;
	NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority=1;
	NVIC_InitStructure.NVIC_IRQChannelSubPriority = 1;
	NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE;
	NVIC_Init(&NVIC_InitStructure);
	
	USART_Cmd(USART1, ENABLE);
}
//向指定ID的串口发送字节
void vos_USART_WriteByte( int8 ID, uint8 tempc )
{
	if( ID == 1 ) {
		USART_SendData( USART1, tempc );
		while(USART_GetFlagStatus(USART1, USART_FLAG_TXE) == RESET) {} /* waiting for sending finish */
	}
}

void USART1_IRQHandler(void) __attribute__((interrupt("WCH-Interrupt-fast")));
void USART1_IRQHandler(void)
{
	if(USART_GetITStatus(USART1, USART_IT_RXNE) != RESET) {
		uint8 Data = USART_ReceiveData(USART1);
		UART_List_Receive( 1, Data ); //存储到对应的缓冲区
		Deal( Data );	//如果是协议串口还需要执行此函数
	}
}

uint32 vos_Timer_Tick0() { return 0; }
uint32 vos_Timer_Tick0MS() { return 1; }

uint32 vos_Timer_Tick1() { return 0; }
uint32 vos_Timer_Tick1US() { return 1; }

void SoftDelay_1us( int32 t ) {}

//开启定时器
void vos_DrvTimer_Start() {}
//关闭定时器
void vos_DrvTimer_Stop() {}
//100KHz定时器调用: remo_tick_run();

GPIO_TypeDef* VOS_GPIOX[4];
uint16_t VOS_GPIOn[16];

void IO_Init()
{
	VOS_GPIOX[0] = GPIOA;
	VOS_GPIOX[1] = GPIOB;
	VOS_GPIOX[2] = GPIOC;
	VOS_GPIOX[3] = GPIOD;
	
	VOS_GPIOn[0] = GPIO_Pin_0;
	VOS_GPIOn[1] = GPIO_Pin_1;
	VOS_GPIOn[2] = GPIO_Pin_2;
	VOS_GPIOn[3] = GPIO_Pin_3;
	VOS_GPIOn[4] = GPIO_Pin_4;
	VOS_GPIOn[5] = GPIO_Pin_5;
	VOS_GPIOn[6] = GPIO_Pin_6;
	VOS_GPIOn[7] = GPIO_Pin_7;
	VOS_GPIOn[8] = GPIO_Pin_8;
	VOS_GPIOn[9] = GPIO_Pin_9;
	VOS_GPIOn[10] = GPIO_Pin_10;
	VOS_GPIOn[11] = GPIO_Pin_11;
	VOS_GPIOn[12] = GPIO_Pin_12;
	VOS_GPIOn[13] = GPIO_Pin_13;
	VOS_GPIOn[14] = GPIO_Pin_14;
	VOS_GPIOn[15] = GPIO_Pin_15;
	
	RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOA,ENABLE);
	RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOB,ENABLE);
	RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOC,ENABLE);
	RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOD,ENABLE);
}

//GPIO输出模式函数 d==0:上拉输入模式 d!=0:输出模式
void IO_DirWrite( uint8 i, uint8 d )
{
	GPIO_InitTypeDef GPIO_InitStructure;
	GPIO_InitStructure.GPIO_Pin = VOS_GPIOn[i%16];
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
	
	if( d == 0 ) {
		GPIO_InitStructure.GPIO_Mode = GPIO_Mode_IPU;
	}
	else {
		GPIO_InitStructure.GPIO_Mode = GPIO_Mode_Out_PP;
	}
	
	GPIO_Init(VOS_GPIOX[i/16], &GPIO_InitStructure);
}

//GPIO输出电平函数 d==0:低电平, d!=0: 高电平
void IO_OutWrite( uint8 i, uint8 d )
{
	GPIO_WriteBit( VOS_GPIOX[i/16], VOS_GPIOn[i%16], ((d==0) ? Bit_RESET:Bit_SET) );
}


//读取GPIO输出电平函数
uint8 IO_OutRead( uint8 i )
{
	return GPIO_ReadOutputDataBit( VOS_GPIOX[i/16], VOS_GPIOn[i%16] );
}
//读取GPIO输入电平函数
uint8 IO_InRead( uint8 i )
{
	return GPIO_ReadInputDataBit( VOS_GPIOX[i/16], VOS_GPIOn[i%16] );
}

void vos_IO_AnalogOpen( uint8 i, uint8 d ) {}
int32 vos_IO_AnalogRead( uint8 i ) { return 0; }
void vos_IO_PWM_SetPin( uint8 clk ) {}
void vos_IO_PWM_SetData( uint8 clk, int32 data32, int32 max ) {}

//========================================
typedef enum {FAILED = 0, PASSED = !FAILED} TestStatus;

volatile FLASH_Status FLASHStatus = FLASH_COMPLETE;
uint32_t WRPR_Value = 0xFFFFFFFF, ProtectedPages = 0x0;
uint32 NbrOfPage;

volatile TestStatus MemoryProgramStatus = PASSED;
volatile TestStatus MemoryEraseStatus = PASSED;

uint32_t EraseCounter = 0x0, Address = 0x0;

void vos_Flash_Load() {}
void vos_Flash_Save() {}
void vos_Flash_Clear()
{
	FLASH_Unlock();
	WRPR_Value = FLASH_GetWriteProtectionOptionByte();

	NbrOfPage = (PAGE_WRITE_END_ADDR - PAGE_WRITE_START_ADDR) / FLASH_PAGE_SIZE;
	
	//如果被保护, 需要先解锁
	if( (WRPR_Value & FLASH_PAGES_TO_BE_PROTECTED) != 0x00) {
		FLASH_ClearFlag(FLASH_FLAG_BSY | FLASH_FLAG_EOP|FLASH_FLAG_PGERR |FLASH_FLAG_WRPRTERR);
	}
	//遍历擦除数据
	for(EraseCounter = 0; (EraseCounter < NbrOfPage) && (FLASHStatus == FLASH_COMPLETE); EraseCounter++) {
		FLASHStatus = FLASH_ErasePage(PAGE_WRITE_START_ADDR + (FLASH_PAGE_SIZE * EraseCounter));
		if(FLASHStatus != FLASH_COMPLETE) {
			//...ERROR
		}
	}
	//数据校验
    Address = PAGE_WRITE_START_ADDR;
    while((Address < PAGE_WRITE_END_ADDR) && (MemoryEraseStatus != FAILED)) {
		if((*(__IO uint16_t*) Address) != 0xFFFF) {
			//...ERROR
		}
		Address += 2;
	}
	FLASH_Lock();
}
//写一个uint32到指定Flash地址
void vos_Flash_WriteUint32( uint32 addr, uint32 b )
{
	FLASH_Unlock();
	FLASH_ProgramWord( PAGE_WRITE_START_ADDR + addr, b );
	FLASH_Lock();
}
void vos_WS2812_Delay_0_4us(void) {}
void vos_WS2812_WriteData( uint8 id, uint32 d ) {}

void vos_UserExtDriver( uint32 v24, uint32 v8 ) {}

