
void vos_tick_core( void );

bool vos_Running;
bool vos_step_ok;

//App启动以来经过的时间, 此时间累加到大约2000之后停止累加;
uint32 StartTimeMS;

#define VOS_SYSRAM_TICK 0
#define VOS_SYSRAM_LOOP 4

#define VOS_TICK_VALUE 10

//==========================================================================
//公共函数
uint8 String_length( uint8* p )
{
	uint8 len = 0;
	while( *p != 0) {
		p++;
		len++;
	}
	return len;
}
uint8 StringConst_length( const uint8* p )
{
	uint8 len = 0;
	while( *p != 0) {
		p++;
		len++;
	}
	return len;
}
int32 String_Copy( uint8 *ps, uint8 *pd )
{
	int n = 0;
	while( *ps != 0 ) {
		*pd = *ps;
		ps++;
		pd++;
		n++;
	}
	*pd = 0;
	return n;
}
//==========================================================================

//加载并执行目标文件程序
void LoadAndRun(void)
{
	uint32 Head;
	
	//IO初始化
	//IO_Init();
	
	//公共定时器进程驱动器初始化
	remo_tick_init();
	
	//串口通信需要放到IO初始化之后 (不需要了)
	UART_List_Init();
	
	//装载Flash
	vos_Flash_Load();
	
	//用户启动
	vos_User_Start();
	
	VM_Reset();
	
	/*
	//ULength = 0;
	vos_Running = true;
	
	//这里需要好好研究一下, 为何每次都需要手工启动定时器
	Timer_SetTimeMs( 10 );
	
	//开启高速驱动定时器 (ESP32防止读写flash cache冲突, 放到最后)
	remo_DrvTimer_Start();
	*/
	vos_step_ok = false;
	vos_Running = false;
	Head = Flash_ReadUint32(0);
	Head &= 0x000000FF;
	if( Head == 0x00000003 ) { //0x00008003
	    vos_Running = true;
		StartTimeMS = 0;
		
		//这里需要好好研究一下, 为何每次都需要手工启动定时器
		Timer_Reset();
		Timer_SetTimeMS( VOS_TICK_VALUE );
	
		//开启高速驱动定时器 (ESP32防止读写flash cache冲突, 放到最后)
		vos_DrvTimer_Start();
	}
}
//系统启动时执行一次 - 如果是K210等需要反复读取缓冲区的, 调用此函数, 而不是 setup
//系统启动时执行一次 - 本函数适用于带有串口中断进行Deal处理的芯片
void vos_setup(void)
{
	//对于STM32系列好像默认就是开启的
	vos_SYS_InterruptEnable();
	
	//协议解析器初始化 (必须要在Flash初始化之前)
	Proto_Init();
	
	//用户初始化
	vos_User_Init();
	
	//软件延时器初始化
	SoftDelay_Init();
	
	//程序空间初始化
	vos_PSpace_Init();
	
	//定时器初始化
	Timer_Init();
	
	//加载并执行目标文件程序
	LoadAndRun();
	
	//这里设置是否禁用上电后自动执行用户代码
	//vos_Running = false;
}

#ifdef VOS_Tick

//================================================

bool isStartTimerOut(void)
{
	if( StartTimeMS >= 2000 ) {
		return true;
	}
	if( Timer_isTimeOut() ) {
		Timer_SetTimeMS( VOS_TICK_VALUE );
		StartTimeMS += VOS_TICK_VALUE;
	}
	return false;
}

//均匀执行
void vos_step( uint32 n )
{
	int32 t;
	uint32 i = 0;
	if( !vos_Running ) {
		return;
	}
	if( !isStartTimerOut() ) {
		return;
	}
	if( vos_step_ok ) {
		if( !Timer_isTimeOut() ) {
			return;
		}
		//读取VEX_Tick
		t = Mem_Get_int32( VOS_SYSRAM_TICK );
		if( t == 0 ) {
			t = 100;
		}
		Timer_SetTimeMS( (uint16)t );
		vos_step_ok = false;
		
		if( myTimerEnable && vos_EA ) {
			
			//触发中断
			VM_ClearSysValue();
			VM_AddInterruptEvent( 0x11 );
			
			VM_Interrupt();
		}
	}
	for( i = 0; i < n; i++ ) {
		VM_Run();
		if( Mem_Get_uint8( VOS_SYSRAM_LOOP ) == 1 ) {
			Mem_Set_uint8( VOS_SYSRAM_LOOP, 0 );
			vos_step_ok = true;
			break;
		}
	}
}

//系统反复调用此函数
void vos_loop(void)
{
	int32 t;
	if( !vos_Running ) {
		return;
	}
	if( !isStartTimerOut() ) {
		return;
	}
	if( myTimerEnable ) {
		if( !Timer_isTimeOut() ) {
			return;
		}
		//读取VEX_Tick
		t = Mem_Get_int32( VOS_SYSRAM_TICK );
		if( t == 0 ) {
			t = 100;
		}
		Timer_SetTimeMS( (uint16)t );
	}
	vos_tick_core();
}
#endif

//系统定时调用此函数
void vos_tick(void)
{
	if( !vos_Running ) {
		return;
	}
	if( StartTimeMS < 2000 ) {
		StartTimeMS += VOS_TICK_VALUE;
		return;
	}
	vos_tick_core();
}
void vos_tick_core()
{
	//判断定时器是否时间到
	if( myTimerEnable && vos_EA ) {
		
		//触发中断
		VM_ClearSysValue();
		VM_AddInterruptEvent( 0x11 );
		
		VM_Interrupt();
	}
	//执行一次主流程
	do {
		VM_Run();
	}
	while( Mem_Get_uint8( VOS_SYSRAM_LOOP ) == 0 );
	Mem_Set_uint8( VOS_SYSRAM_LOOP, 0 );
}



