
#ifndef _remo_SoftDelay_h_
#define _remo_SoftDelay_h_

#include "remo_typedef.h"

void Timer_Init(void);
void Timer_Reset(void);
void Timer_SetTimeMS( uint16 t );
bool Timer_isTimeOut(void);
void Timer_StartUS(void);
uint32 Timer_GetUS(void);

void SoftDelay_Init(void);
void SoftDelay_1us( int32 nCount );
void SoftDelay_10us(int32 nCount);
void SoftDelay_ms(int32 nCount);
void SoftDelay_s(int32 nCount);

#endif

