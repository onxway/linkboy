

bool Done;

#define VOS_UARTLEN 10

static uint16 UV_buffer_addr[VOS_UARTLEN];
static int16 UV_max_length[VOS_UARTLEN];
static uint16 UV_length_addr[VOS_UARTLEN];
static bool UV_isOpen[VOS_UARTLEN];


void UART_List_Init()
{
	uint8 i = 0;
	for( i = 0; i < VOS_UARTLEN; ++i ) {
		UV_max_length[i] = 0;
		UV_length_addr[i] = 0;
		UV_isOpen[i] = false;
	}
}
bool remo_USART_isOpen( int8 ID )
{
	ID += 1;
	return UV_isOpen[ID];
}
void UART_List_Receive( int8 ID, uint8 data )
{
	ID += 1;
	if( UV_max_length[ID] > 0 ) {
		int16 cl = Mem_Get_int16( UV_length_addr[ID] );
		BASE[UV_buffer_addr[ID] + cl] = data;
		cl++;
		if( cl >= UV_max_length[ID] ) {
			cl = 0;
		}
		Mem_Set_int16( UV_length_addr[ID], cl );
	}
}
void UART_List_SetAddr( int8 ID, uint32 addr, int16 max, uint32 len_addr )
{
	ID += 1;
	UV_buffer_addr[ID] = addr;
	UV_max_length[ID] = max;
	UV_length_addr[ID] = len_addr;
	UV_isOpen[ID] = true;
}
//----------------------------------------------------
//proto接口函数
void vos_USART_WriteByte( int8 ID, uint8 tempc );

//发送字节
void remo_USART_write( uint8 tempc )
{
	vos_USART_WriteByte( UART_DEFAULT_ID, tempc );
}
//发送字符串
void remo_USART_SendString(uint8 *tempp)
{
	while( *tempp != 0 )
	{
		remo_USART_write( *tempp++ );
	}
}
//发送常量字符串
void remo_USART_SendStringConst(const uint8 *tempp)
{
	while( *tempp != 0 )
	{
		remo_USART_write( *tempp++ );
	}
}



