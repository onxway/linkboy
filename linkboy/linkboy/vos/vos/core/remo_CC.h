
#ifndef _remo_CC_h_
#define _remo_CC_h_

#include "remo_typedef.h"

void remo_CC_Init( void );
void remo_CC_Reset( uint32 addr1, uint32 addr2, uint16 n, uint32 addr3 );
void remo_CC_Deal( void );


#endif
