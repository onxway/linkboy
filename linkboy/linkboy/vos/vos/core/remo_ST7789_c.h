
//#define delay(); SoftDelay_10us( 1 );

//---------------------------------------------------------
void remo_ST7789_LCD_Writ_Bus(uint8 dat);
void remo_ST7789_PORT_init( void );
void remo_ST7789_SetPin( uint8 vDIN, uint8 vCLK, uint8 vRES, uint8 vDC );
void remo_ST7789_Init( int32 x, int32 y, uint8 dt );
void remo_ST7789_LCD_WR_DATA8(uint8 dat);
void remo_ST7789_LCD_WR_DATA(uint16 dat);
void remo_ST7789_LCD_WR_REG(uint8 dat);
void remo_ST7789_LCD_Address_Set(uint16 x1,uint16 y1,uint16 x2,uint16 y2);
void remo_ST7789_LCD_draw_pixel( int32 x, int32 y );
//---------------------------------------------------------

int32 remo_ST7789_LCD_W;
int32 remo_ST7789_LCD_H;
 
uint16 s_ForColor;

uint8 remo_ST7789_DIN;
uint8 remo_ST7789_CLK;
uint8 remo_ST7789_RES;
uint8 remo_ST7789_DC;

void remo_ST7789_SetPin( uint8 vDIN, uint8 vCLK, uint8 vRES, uint8 vDC )
{
	remo_ST7789_DIN = vDIN;
	remo_ST7789_CLK = vCLK;
	remo_ST7789_RES = vRES;
	remo_ST7789_DC = vDC;
}

void ST7789_Init(void)
{
	//************* Start Initial Sequence **********
	remo_ST7789_LCD_WR_REG(0x36);
	remo_ST7789_LCD_WR_DATA8(0x00);

	remo_ST7789_LCD_WR_REG(0x3A);
	remo_ST7789_LCD_WR_DATA8(0x05);

	remo_ST7789_LCD_WR_REG(0xB2);
	remo_ST7789_LCD_WR_DATA8(0x0C);
	remo_ST7789_LCD_WR_DATA8(0x0C);
	remo_ST7789_LCD_WR_DATA8(0x00);
	remo_ST7789_LCD_WR_DATA8(0x33);
	remo_ST7789_LCD_WR_DATA8(0x33);

	remo_ST7789_LCD_WR_REG(0xB7);
	remo_ST7789_LCD_WR_DATA8(0x35);

	remo_ST7789_LCD_WR_REG(0xBB);
	remo_ST7789_LCD_WR_DATA8(0x19);

	remo_ST7789_LCD_WR_REG(0xC0);
	remo_ST7789_LCD_WR_DATA8(0x2C);

	remo_ST7789_LCD_WR_REG(0xC2);
	remo_ST7789_LCD_WR_DATA8(0x01);

	remo_ST7789_LCD_WR_REG(0xC3);
	remo_ST7789_LCD_WR_DATA8(0x12);

	remo_ST7789_LCD_WR_REG(0xC4);
	remo_ST7789_LCD_WR_DATA8(0x20);

	remo_ST7789_LCD_WR_REG(0xC6);
	remo_ST7789_LCD_WR_DATA8(0x0F);

	remo_ST7789_LCD_WR_REG(0xD0);
	remo_ST7789_LCD_WR_DATA8(0xA4);
	remo_ST7789_LCD_WR_DATA8(0xA1);

	remo_ST7789_LCD_WR_REG(0xE0);
	remo_ST7789_LCD_WR_DATA8(0xD0);
	remo_ST7789_LCD_WR_DATA8(0x04);
	remo_ST7789_LCD_WR_DATA8(0x0D);
	remo_ST7789_LCD_WR_DATA8(0x11);
	remo_ST7789_LCD_WR_DATA8(0x13);
	remo_ST7789_LCD_WR_DATA8(0x2B);
	remo_ST7789_LCD_WR_DATA8(0x3F);
	remo_ST7789_LCD_WR_DATA8(0x54);
	remo_ST7789_LCD_WR_DATA8(0x4C);
	remo_ST7789_LCD_WR_DATA8(0x18);
	remo_ST7789_LCD_WR_DATA8(0x0D);
	remo_ST7789_LCD_WR_DATA8(0x0B);
	remo_ST7789_LCD_WR_DATA8(0x1F);
	remo_ST7789_LCD_WR_DATA8(0x23);

	remo_ST7789_LCD_WR_REG(0xE1);
	remo_ST7789_LCD_WR_DATA8(0xD0);
	remo_ST7789_LCD_WR_DATA8(0x04);
	remo_ST7789_LCD_WR_DATA8(0x0C);
	remo_ST7789_LCD_WR_DATA8(0x11);
	remo_ST7789_LCD_WR_DATA8(0x13);
	remo_ST7789_LCD_WR_DATA8(0x2C);
	remo_ST7789_LCD_WR_DATA8(0x3F);
	remo_ST7789_LCD_WR_DATA8(0x44);
	remo_ST7789_LCD_WR_DATA8(0x51);
	remo_ST7789_LCD_WR_DATA8(0x2F);
	remo_ST7789_LCD_WR_DATA8(0x1F);
	remo_ST7789_LCD_WR_DATA8(0x1F);
	remo_ST7789_LCD_WR_DATA8(0x20);
	remo_ST7789_LCD_WR_DATA8(0x23);

	remo_ST7789_LCD_WR_REG(0x21);
	
	remo_ST7789_LCD_WR_REG(0x11);
	//Delay (120);

	SoftDelay_ms( 10 );

	remo_ST7789_LCD_WR_REG(0x29);
}
void ILI9341_Init(void)
{
	//2.2inch TM2.2-G2.2 Init 20171020 
	remo_ST7789_LCD_WR_REG(0x11);  
	remo_ST7789_LCD_WR_DATA8(0x00); 

	remo_ST7789_LCD_WR_REG(0xCF);  
	remo_ST7789_LCD_WR_DATA8(0X00); 
	remo_ST7789_LCD_WR_DATA8(0XC1); 
	remo_ST7789_LCD_WR_DATA8(0X30);

	remo_ST7789_LCD_WR_REG(0xED);  
	remo_ST7789_LCD_WR_DATA8(0X64); 
	remo_ST7789_LCD_WR_DATA8(0X03); 
	remo_ST7789_LCD_WR_DATA8(0X12);
	remo_ST7789_LCD_WR_DATA8(0X81);

	remo_ST7789_LCD_WR_REG(0xE8);  
	remo_ST7789_LCD_WR_DATA8(0X85); 
	remo_ST7789_LCD_WR_DATA8(0X11); 
	remo_ST7789_LCD_WR_DATA8(0X78);

	remo_ST7789_LCD_WR_REG(0xF6);  
	remo_ST7789_LCD_WR_DATA8(0X01); 
	remo_ST7789_LCD_WR_DATA8(0X30); 
	remo_ST7789_LCD_WR_DATA8(0X00);

	remo_ST7789_LCD_WR_REG(0xCB);  
	remo_ST7789_LCD_WR_DATA8(0X39); 
	remo_ST7789_LCD_WR_DATA8(0X2C); 
	remo_ST7789_LCD_WR_DATA8(0X00);
	remo_ST7789_LCD_WR_DATA8(0X34);
	remo_ST7789_LCD_WR_DATA8(0X05);

	remo_ST7789_LCD_WR_REG(0xF7);  
	remo_ST7789_LCD_WR_DATA8(0X20); 

	remo_ST7789_LCD_WR_REG(0xEA);  
	remo_ST7789_LCD_WR_DATA8(0X00); 
	remo_ST7789_LCD_WR_DATA8(0X00); 

	remo_ST7789_LCD_WR_REG(0xC0);  
	remo_ST7789_LCD_WR_DATA8(0X20); 

	remo_ST7789_LCD_WR_REG(0xC1);  
	remo_ST7789_LCD_WR_DATA8(0X11); 

	remo_ST7789_LCD_WR_REG(0xC5);  
	remo_ST7789_LCD_WR_DATA8(0X31); 
	remo_ST7789_LCD_WR_DATA8(0X3C); 

	remo_ST7789_LCD_WR_REG(0xC7);  
	remo_ST7789_LCD_WR_DATA8(0XA9); 

	remo_ST7789_LCD_WR_REG(0x3A);  
	remo_ST7789_LCD_WR_DATA8(0X55); 
	
	remo_ST7789_LCD_WR_REG(0x36);  
	//#if USE_HORIZONTAL
		 remo_ST7789_LCD_WR_DATA8(0xE8);//oив?ив2?и║y
	//#else
	//	 remo_ST7789_LCD_WR_DATA8(0x48);//и║и▓?ив2?и║y 
	//#endif

	remo_ST7789_LCD_WR_REG(0xB1);  
	remo_ST7789_LCD_WR_DATA8(0X00); 
	remo_ST7789_LCD_WR_DATA8(0X18); 

	remo_ST7789_LCD_WR_REG(0xB4);  
	remo_ST7789_LCD_WR_DATA8(0X00); 
	remo_ST7789_LCD_WR_DATA8(0X00); 

	remo_ST7789_LCD_WR_REG(0xF2);  
	remo_ST7789_LCD_WR_DATA8(0X00); 

	remo_ST7789_LCD_WR_REG(0x26);  
	remo_ST7789_LCD_WR_DATA8(0X01); 

	remo_ST7789_LCD_WR_REG(0xE0);  
	remo_ST7789_LCD_WR_DATA8(0X0F); 
	remo_ST7789_LCD_WR_DATA8(0X17); 
	remo_ST7789_LCD_WR_DATA8(0X14); 
	remo_ST7789_LCD_WR_DATA8(0X09); 
	remo_ST7789_LCD_WR_DATA8(0X0C); 
	remo_ST7789_LCD_WR_DATA8(0X06); 
	remo_ST7789_LCD_WR_DATA8(0X43); 
	remo_ST7789_LCD_WR_DATA8(0X75); 
	remo_ST7789_LCD_WR_DATA8(0X36); 
	remo_ST7789_LCD_WR_DATA8(0X08); 
	remo_ST7789_LCD_WR_DATA8(0X13); 
	remo_ST7789_LCD_WR_DATA8(0X05); 
	remo_ST7789_LCD_WR_DATA8(0X10); 
	remo_ST7789_LCD_WR_DATA8(0X0B); 
	remo_ST7789_LCD_WR_DATA8(0X08); 

	remo_ST7789_LCD_WR_REG(0xE1);  
	remo_ST7789_LCD_WR_DATA8(0X00); 
	remo_ST7789_LCD_WR_DATA8(0X1F); 
	remo_ST7789_LCD_WR_DATA8(0X23); 
	remo_ST7789_LCD_WR_DATA8(0X03); 
	remo_ST7789_LCD_WR_DATA8(0X0E); 
	remo_ST7789_LCD_WR_DATA8(0X04); 
	remo_ST7789_LCD_WR_DATA8(0X39); 
	remo_ST7789_LCD_WR_DATA8(0X25); 
	remo_ST7789_LCD_WR_DATA8(0X4D); 
	remo_ST7789_LCD_WR_DATA8(0X06); 
	remo_ST7789_LCD_WR_DATA8(0X0D); 
	remo_ST7789_LCD_WR_DATA8(0X0B); 
	remo_ST7789_LCD_WR_DATA8(0X33); 
	remo_ST7789_LCD_WR_DATA8(0X37); 
	remo_ST7789_LCD_WR_DATA8(0X0F); 

	remo_ST7789_LCD_WR_REG(0x29);  
}

void remo_ST7789_Init( int32 w, int32 h, uint8 dt )
{
	D_pixel = remo_ST7789_LCD_draw_pixel;
	
	remo_ST7789_LCD_W = w;
	remo_ST7789_LCD_H = h;
	
	//端口初始化
	remo_ST7789_PORT_init();
	
	if( dt == 0 ) {
		ST7789_Init();
	}
	else {
		ILI9341_Init();
	}
}
void remo_ST7789_PORT_init( void )
{
	/*
	//端口初始化
	IO_DirWrite( remo_ST7789_DIN, 1 );
	IO_DirWrite( remo_ST7789_CLK, 1 );
	IO_DirWrite( remo_ST7789_DC, 1 );
	IO_DirWrite( remo_ST7789_RES, 1 );
	
	IO_OutWrite( remo_ST7789_CLK, 1 );
	IO_OutWrite( remo_ST7789_DC, 0 );
	IO_OutWrite( remo_ST7789_RES, 1 );
	*/
}

//函数说明：LCD清屏函数
//入口数据：无
//返回值：  无
void remo_ST7789_LCD_Clear( uint16 Color )
{
	uint16 i;
	uint16 j;
	
	remo_ST7789_LCD_Address_Set( 0, 0, remo_ST7789_LCD_W-1, remo_ST7789_LCD_H-1 );

	for( i=0; i<remo_ST7789_LCD_W; i+=1 ) {
		for ( j=0; j<remo_ST7789_LCD_H; j+=1 ) {
			remo_ST7789_LCD_WR_DATA(Color);
		}
	}
}
void remo_ST7789_LCD_set_fore(uint16 Color)
{
	s_ForColor = Color;
}
void remo_ST7789_LCD_draw_pixel( int32 x, int32 y )
{
	if( x < 0 || x >= remo_ST7789_LCD_W ) return;
	if( y < 0 || y >= remo_ST7789_LCD_H ) return;
	
	remo_ST7789_LCD_Address_Set(x,y,x,y);//设置光标位置
	remo_ST7789_LCD_WR_DATA( s_ForColor );
}
//=================================================================================
//函数说明：LCD串行数据写入函数
//入口数据：dat  要写入的串行数据
//返回值：  无
void remo_ST7789_LCD_Writ_Bus(uint8 dat)
{
	uint8 i;
	for( i = 0; i < 8; ++i ) {
		IO_OutWrite( remo_ST7789_CLK, 0 );
		IO_OutWrite( remo_ST7789_DIN, dat & 0x80 );
		IO_OutWrite( remo_ST7789_CLK, 1 );
		dat <<= 1;
	}
}
//函数说明：LCD写入数据
//入口数据：dat 写入的数据
//返回值：  无
void remo_ST7789_LCD_WR_DATA8(uint8 dat)
{
	IO_OutWrite( remo_ST7789_DC, 1 );
	remo_ST7789_LCD_Writ_Bus(dat);
}
//函数说明：LCD写入数据
//入口数据：dat 写入的数据
//返回值：  无
void remo_ST7789_LCD_WR_DATA(uint16 dat)
{
	IO_OutWrite( remo_ST7789_DC, 1 );
	remo_ST7789_LCD_Writ_Bus( (dat >> 8) & 0xFF );
	remo_ST7789_LCD_Writ_Bus( dat & 0xFF );
}
//函数说明：LCD写入命令
//入口数据：dat 写入的命令
//返回值：  无
void remo_ST7789_LCD_WR_REG(uint8 dat)
{
	IO_OutWrite( remo_ST7789_DC, 0 );
	remo_ST7789_LCD_Writ_Bus(dat);
}
//函数说明：设置起始和结束地址
//入口数据：x1,x2 设置列的起始和结束地址
//y1,y2 设置行的起始和结束地址
//返回值：  无
void remo_ST7789_LCD_Address_Set(uint16 x1,uint16 y1,uint16 x2,uint16 y2)
{
	remo_ST7789_LCD_WR_REG(0x2a);//列地址设置
	remo_ST7789_LCD_WR_DATA(x1);
	remo_ST7789_LCD_WR_DATA(x2);
	remo_ST7789_LCD_WR_REG(0x2b);//行地址设置
	remo_ST7789_LCD_WR_DATA(y1);
	remo_ST7789_LCD_WR_DATA(y2);
	remo_ST7789_LCD_WR_REG(0x2c);//储存器写
}
void remo_ST7789_LCD_add_color(void)
{
	remo_ST7789_LCD_WR_DATA( s_ForColor );
}
void remo_ST7789_LCD_add_color_n( int32 n )
{
	for( int32 d = 0; d < n; ++d ) {
		remo_ST7789_LCD_WR_DATA( s_ForColor );
	}
}
void remo_ST7789_LCD_push_colors( uint32 addr, int32 n )
{
    uint16 *p_addr = (uint16*)Mem_GetAddr( addr );
    for( int32 d = 0; d < n; ++d ) {
		remo_ST7789_LCD_WR_DATA( p_addr[d] );
	}
}





