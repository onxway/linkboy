
#ifndef _entry_h_
#define _entry_h_

#include "remo_typedef.h"

extern bool vos_Running;

void vos_setup0( void );
void vos_setup( void );

//均匀执行
void vos_step( uint32 n );

//非均匀执行
void vos_loop( void );

//定时执行
void vos_tick( void );

#endif



