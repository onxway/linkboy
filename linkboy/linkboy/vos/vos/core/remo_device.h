
#ifndef __REMO_DEVICE_H
#define __REMO_DEVICE_H

#include "remo_typedef.h"

//================================================================

//MES
uint8 remo_SYS_DEV_VERSION( void );
const uint8* remo_SYS_DEV_MESSAGE( void );

//SYS
extern uint8 BASE[];

void vos_User_Init( void );
void vos_User_Start( void );
void vos_SYS_InterruptEnable( void );
void vos_SYS_InterruptDisable( void );

//USART
bool remo_USART_isOpen( int8 ID );
void vos_USART_Open( int8 ID, uint32 b );
void vos_USART_WriteByte( int8 ID, uint8 tempc );
void vos_USART_ReceiveByte( void );

void remo_USART_write(uint8 tempc);
void remo_USART_SendString(uint8 *tempp);
void remo_USART_SendStringConst(const uint8 *tempp);

//Tick
uint32 vos_Timer_Tick0(void);
uint32 vos_Timer_Tick0MS(void);

uint32 vos_Timer_Tick1(void);
uint32 vos_Timer_Tick1US(void);

void Timer_Delay_0_4us(void);

//remo_IO
void IO_DirWrite( uint8 i, uint8 d );
//void IO_PullWrite( uint8 i, uint8 d );
void IO_OutWrite( uint8 i, uint8 d );
uint8 IO_OutRead( uint8 i );
uint8 IO_InRead( uint8 i );
void vos_IO_AnalogOpen( uint8 i, uint8 d );
int32 vos_IO_AnalogRead( uint8 i );
void vos_IO_PWM_SetPin( uint8 clk );
void vos_IO_PWM_SetData( uint8 clk, int32 data32, int32 max );

//remo_Flash
int32 Flash_GetSize(void);
void vos_Flash_Clear( void );
void vos_Flash_Save( void );
void vos_Flash_Load( void );
void vos_Flash_WriteUint32( uint32 addr, uint32 b );
uint8 Flash_ReadByte( uint32 addr );
uint32 Flash_ReadUint32( uint32 addr );
uint8* Flash_GetAddr( uint32 addr );

//remo_DrvTimer
void vos_DrvTimer_Start( void );
void vos_DrvTimer_Stop( void );

void vos_WS2812_Delay_0_4us(void);
void vos_WS2812_WriteData( uint8 id, uint32 d );

//================================================================
#endif

