
#ifndef _proto_h_
#define _proto_h_

#include "remo_typedef.h"

void Proto_Init( void );
void Deal( uint8 d );
void Error( uint32 ET, uint32 EC );
void Debug( uint32 data8 );

#endif



