
uint32 c_time1_temp;
uint32 c_time1;

void Timer_Init()
{
	c_time1_temp = 0;
	c_time1 = 0;
}
void Timer_Reset()
{
	c_time1 = vos_Timer_Tick0();
}
void Timer_SetTimeMS( uint16 t )
{
	c_time1_temp = t * vos_Timer_Tick0MS();
	
	//这里应该判断一下, 当差距过大时, 应该做一下处理, 直接设置为当前值
	c_time1 += c_time1_temp; //不带误差
	//c_time1 = vos_Timer_Tick0() + c_time1_temp; //带有微小延迟
}
bool Timer_isTimeOut()
{
	//必须要进行截断! 否则时间会很快触发, STM32系列是24位Tick, 因此最多进行24位截断, 也可以更小
	uint32 c = (c_time1 - vos_Timer_Tick0()) & 0xFFFFFF;
	return (c > c_time1_temp);
}
//========================================================================================

uint32 c_time2;

//初始化
void SoftDelay_Init()
{
	//FREQ = t;
	//SoftDelay_t = FREQ / (7200000 / 7); //  /7基于GD32V测试通过
	//对于STM32F1C8T6来说不能除以7. 再考虑到tick占用, 传进来的t=f/8
	
	c_time2 = 0;
}

//延时10微秒
//volatile int32 i_delay;
//volatile int32 j_delay;

#ifdef VOS_US_Tick

//读取计数值的函数和读取每微秒的计数值函数, 相除即可得经过的微秒数
void Timer_StartUS()
{
	c_time2 = vos_Timer_Tick1();
}
uint32 Timer_GetUS()
{
	//这里也需要截断吧? STM是24位定时器!!! 2022.9.9
	return ((vos_Timer_Tick1() - c_time2) & 0xFFFFFF) / vos_Timer_Tick1US();
}
void SoftDelay_1us( int32 nCount )
{
	uint32 tus = 0;
	Timer_StartUS();
	do {
		tus = Timer_GetUS();
	}
	while( tus < nCount );
}
void SoftDelay_10us( int32 nCount )
{
	uint32 tus = 0;
	for(; nCount > 0; nCount--) {
		Timer_StartUS();
		do {
			tus = Timer_GetUS();
		}
		while( tus < 10 );
	}
}
//延时毫秒
void SoftDelay_ms(int32 nCount)
{
	//注意: 不要用这个方式, 因为会导致时间不准!!!
	//for(; nCount > 0; nCount--) {
	//	SoftDelay_10us( 100 );
	//}
	
	uint32 tus = 0;
	for(; nCount > 0; nCount--) {
		Timer_StartUS();
		do {
			tus = Timer_GetUS();
		}
		while( tus < 1000 );
	}
}

#else

//延时微秒
void SoftDelay_10us(int32 nCount)
{
	for(; nCount > 0; nCount--) {
		SoftDelay_1us( 10 );
	}
}

//延时毫秒
void SoftDelay_ms(int32 nCount)
{
	for(; nCount > 0; nCount--) {
		SoftDelay_10us( 100 );
	}
}

#endif

//延时秒
void SoftDelay_s(int32 nCount)
{
	for(; nCount > 0; nCount--) {
		SoftDelay_ms( 1000 );
	}
}

