
#include "vos_PSpace_c.h"

#ifdef VOS_ExtDriver

#include "remo_I2C_c.h"
#include "remo_SHT1106_DCSPI_c.h"
#include "remo_SHT1106_I2C_c.h"
#include "remo_ST7789_c.h"
#include "remo_ST7920_c.h"

#endif

#include "UART_List_c.h"
#include "remo_CC_c.h"
#include "remo_tick_c.h"

#include "remo_SoftDelay_c.h"
#include "VM_c.h"
#include "entry_c.h"
#include "proto_c.h"
#include "vos_Test_c.h"


