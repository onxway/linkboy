

void usart_baudrate_set( USART_TypeDef* USARTx, uint32 BaudRate );

//初始化串口1
void remo_USART1_Init(uint32 BaudRate)
{
   //GPIO端口设置
    GPIO_InitTypeDef GPIO_InitStructure;
	NVIC_InitTypeDef NVIC_InitStructure;
	
	RCC_AHB1PeriphClockCmd(RCC_AHB1Periph_GPIOA,ENABLE); //使能GPIOA时钟
	RCC_APB2PeriphClockCmd(RCC_APB2Periph_USART1,ENABLE);//使能USART1时钟
    
	//串口1对应引脚复用映射
	GPIO_PinAFConfig(GPIOA,GPIO_PinSource9,GPIO_AF_USART1); //GPIOA9复用为USART1
	GPIO_PinAFConfig(GPIOA,GPIO_PinSource10,GPIO_AF_USART1); //GPIOA10复用为USART1
	
	//USART1端口配置
	GPIO_InitStructure.GPIO_Pin = GPIO_Pin_9 | GPIO_Pin_10; //GPIOA9与GPIOA10
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_AF;//复用功能
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;	//速度50MHz
	GPIO_InitStructure.GPIO_OType = GPIO_OType_PP; //推挽复用输出
	GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_UP; //上拉
	GPIO_Init(GPIOA,&GPIO_InitStructure); //初始化PA9，PA10
	
    //USART1 初始化设置
	usart_baudrate_set( USART1, BaudRate );
	
	USART_Cmd(USART1, ENABLE);  //使能串口1 
	
	USART_ClearFlag(USART1, USART_FLAG_TC);
	
	USART_ITConfig(USART1, USART_IT_RXNE, ENABLE);//开启相关中断
	
	//Usart1 NVIC 配置
    NVIC_InitStructure.NVIC_IRQChannel = USART1_IRQn;//串口1中断通道
	NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority=3;//抢占优先级3
	NVIC_InitStructure.NVIC_IRQChannelSubPriority =3;		//子优先级3
	NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE;			//IRQ通道使能
	NVIC_Init(&NVIC_InitStructure);	//根据指定的参数初始化VIC寄存器、
}
//串口波特率
void usart_baudrate_set( USART_TypeDef* USARTx, uint32 BaudRate )
{
	USART_InitTypeDef USART_InitStructure;
	USART_InitStructure.USART_BaudRate = BaudRate;//波特率设置
	USART_InitStructure.USART_WordLength = USART_WordLength_8b;//字长为8位数据格式
	USART_InitStructure.USART_StopBits = USART_StopBits_1;//一个停止位
	USART_InitStructure.USART_Parity = USART_Parity_No;//无奇偶校验位
	USART_InitStructure.USART_HardwareFlowControl = USART_HardwareFlowControl_None;//无硬件数据流控制
	USART_InitStructure.USART_Mode = USART_Mode_Rx | USART_Mode_Tx;	//收发模式
	USART_Init(USARTx, &USART_InitStructure); //初始化串口1
}
//串口中断
void USART1_IRQHandler(void)
{
	if(USART_GetITStatus(USART1, USART_IT_RXNE)== SET)
	{
        uint8 Data = USART_ReceiveData(USART1);
		USART_ClearFlag(USART1,USART_FLAG_TC);
		
		Deal( Data );
		
		//存储到对应的缓冲区
		UART_List_Receive( 1, Data );
    }
}


//串口波特率
void vos_USART_Open( int8 ID, uint32 b )
{
	if( ID == 1 ) {
		remo_USART1_Init( 115200 );
	}
}
//发送字节
void vos_USART_WriteByte( int8 ID, uint8 tempc )
{
	/*
	USART2->DR = tempc;
	while(USART_GetFlagStatus(USART2, USART_FLAG_TC) == RESET);
	*/
	//while(USART_GetFlagStatus(USART1,USART_FLAG_TC)!=SET);
	
	if( ID == 1 ) {
		//while((USART2->SR&0x0040)==0);
		//while( USART_GetFlagStatus(USART2, USART_FLAG_TXE) == RESET );
		//USART2->DR = tempc;
		
		//USART2->DR = tempc;
		//while(USART_GetFlagStatus(USART2, USART_FLAG_TC) == RESET);
	
		//while(USART_GetFlagStatus(USART1,USART_FLAG_TC)!=SET); 
	
		USART_SendData(USART1, tempc);
		while(USART_GetFlagStatus(USART1, USART_FLAG_TC) == RESET);
	
		//while((USART1->SR&0X40)==0);//循环发送,直到发送完毕   
		//USART1->DR = tempc;
	}
}


