
#include "stm32f4xx_adc.h"

#define NUMBER 144
uint16_t GPIO_LIST[NUMBER];
GPIO_TypeDef *(GPION_LIST[NUMBER]);

GPIO_InitTypeDef GPIO_InitStructure;

void adc_init(void);
void ADC_Reset(ADC_TypeDef* ADCx);

void IO_Init()
{
	uint16 pin;
	uint8 i;
	GPIO_TypeDef *(GPIONAME_LIST[9]);
	
	RCC_AHB1PeriphClockCmd(RCC_AHB1Periph_GPIOA,ENABLE);
	RCC_AHB1PeriphClockCmd(RCC_AHB1Periph_GPIOB,ENABLE);
	RCC_AHB1PeriphClockCmd(RCC_AHB1Periph_GPIOC,ENABLE);
	RCC_AHB1PeriphClockCmd(RCC_AHB1Periph_GPIOD,ENABLE);
	RCC_AHB1PeriphClockCmd(RCC_AHB1Periph_GPIOE,ENABLE);
	RCC_AHB1PeriphClockCmd(RCC_AHB1Periph_GPIOF,ENABLE);
	RCC_AHB1PeriphClockCmd(RCC_AHB1Periph_GPIOG,ENABLE);
	RCC_AHB1PeriphClockCmd(RCC_AHB1Periph_GPIOH,ENABLE);
	RCC_AHB1PeriphClockCmd(RCC_AHB1Periph_GPIOI,ENABLE);
	
	GPIO_InitStructure.GPIO_Mode=GPIO_Mode_IN;
	GPIO_InitStructure.GPIO_Speed=GPIO_Speed_50MHz;
	GPIO_InitStructure.GPIO_OType=GPIO_OType_PP;
	GPIO_InitStructure.GPIO_PuPd=GPIO_PuPd_UP;
	
	GPIONAME_LIST[0] = GPIOA;
	GPIONAME_LIST[1] = GPIOB;
	GPIONAME_LIST[2] = GPIOC;
	GPIONAME_LIST[3] = GPIOD;
	GPIONAME_LIST[4] = GPIOE;
	GPIONAME_LIST[5] = GPIOF;//不可初始化
	GPIONAME_LIST[6] = GPIOG;//不可初始化
	GPIONAME_LIST[7] = GPIOH;
	GPIONAME_LIST[8] = GPIOI;//不可初始化
	
	pin = 1;
	for( i = 0; i < NUMBER; i++ ) {
		
		GPIO_LIST[i] = pin;
		pin <<= 1;
		if( pin == 0 ) {
			pin = 1;
		}
		GPION_LIST[i] = GPIONAME_LIST[i/16];
	}
	
	for( i = 0; i < NUMBER; ++i ) {
		
		//忽略PH0 PH1配置 (时钟引脚)
		if( i == 112 || i == 113 ) {
			continue;
		}
		
		
		if( i >= 5*16 && i < 6*16 ) {
			continue;
		}
		if( i >= 6*16 && i < 7*16 ) {
			continue;
		}
		if( i >= 8*16 && i < 9*16 ) {
			continue;
		}
		
		
		GPIO_InitStructure.GPIO_Pin = GPIO_LIST[i];
		GPIO_Init(GPION_LIST[i],&GPIO_InitStructure);
	}
	adc_init();
}
void adc_init(void)
{
	/*
	ADC_InitTypeDef ADC_InitStructure;
	//结构体_ADC基础-声明
	GPIO_InitTypeDef GPIO_InitStructure;
	//结构体_引脚基础-声明
	RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOA | RCC_APB2Periph_ADC1, ENABLE);
	//时钟开启_GPIOA,ADC1
	
	GPIO_InitStructure.GPIO_Pin = GPIO_Pin_1;
	//结构体_引脚-PA1
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
	//结构体_引脚-引脚频率_50Mhz
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_AIN;
	//结构体_引脚-引脚模式_模拟输入
	GPIO_Init(GPIOA, &GPIO_InitStructure);
	*/
	
	ADC_InitTypeDef ADC_InitStructure;
	ADC_CommonInitTypeDef ADC_CommonInitStructure;
	
	RCC_APB2PeriphClockCmd(RCC_APB2Periph_ADC1, ENABLE);
	
	ADC_CommonInitStructure.ADC_DMAAccessMode = ADC_DMAAccessMode_Disabled;/*DMA失能*/
	ADC_CommonInitStructure.ADC_Mode = ADC_Mode_Independent;/*独立模式*/
	ADC_CommonInitStructure.ADC_Prescaler = ADC_Prescaler_Div4;/*APB2的4分频 即84/4=21M*/
	ADC_CommonInitStructure.ADC_TwoSamplingDelay = ADC_TwoSamplingDelay_5Cycles;/*两个采样阶段的延时5个时钟*/
	ADC_CommonInit(&ADC_CommonInitStructure);/*初始化*/

	/*初始化ADC1*/
	ADC_InitStructure.ADC_Resolution = ADC_Resolution_12b;/*12位模式*/
	ADC_InitStructure.ADC_ScanConvMode = DISABLE;/*非扫描模式*/
	ADC_InitStructure.ADC_ContinuousConvMode = DISABLE;/*关闭连续转换*/
	ADC_InitStructure.ADC_ExternalTrigConvEdge = ADC_ExternalTrigConvEdge_None;/*禁止触发检测 使用软件触发*/
	ADC_InitStructure.ADC_DataAlign = ADC_DataAlign_Right;/*右对齐*/
	ADC_InitStructure.ADC_NbrOfConversion = 1;/*只使用1通道 规则通为1*/
	ADC_Init(ADC1, &ADC_InitStructure);/*初始化*/
	ADC_Cmd(ADC1, ENABLE);/*开启ADC*/
	
 	//ADC_Reset(ADC1);
	//ADC校准函数
}
/*
void ADC_Reset(ADC_TypeDef* ADCx)
{
	ADC_ResetCalibration(ADCx);
	//复位校准寄存器             寄存器置1
	while(ADC_GetResetCalibrationStatus(ADCx));
	//等待校准寄存器复位完成      寄存器置0
	ADC_StartCalibration(ADCx);
	//ADC校准                    寄存器置1
	while(ADC_GetCalibrationStatus(ADCx));
	//等待校准完成                寄存器置0
}
*/
int32 vos_IO_AD_Read(ADC_TypeDef* ADCx, uint8 i)
{
	//ADC_RegularChannelConfig(ADC1, ADC_Channel_1, 1, ADC_SampleTime_1Cycles5);
	ADC_RegularChannelConfig(ADC1, i, 1, ADC_SampleTime_480Cycles);
	
	ADC_SoftwareStartConv(ADCx);
	//开关_ADC软件触发-开关 状态寄存器为0
	while(!ADC_GetFlagStatus(ADCx,ADC_FLAG_EOC));
	//等待转换结束 寄存器置1
	return ADC_GetConversionValue(ADCx);
	//因为读取了数据寄存器，状态寄存器自动清0
}
void IO_DirWrite( uint8 i, uint8 d )
{
	if( d == 0 ) {
		GPIO_InitStructure.GPIO_Mode = GPIO_Mode_IN;
	}
	else {
		GPIO_InitStructure.GPIO_Mode = GPIO_Mode_OUT;
	}
	GPIO_InitStructure.GPIO_Pin = GPIO_LIST[i];
	GPIO_Init( GPION_LIST[i], &GPIO_InitStructure );
}
void IO_OutWrite( uint8 i, uint8 d )
{
	if( d == 0 ) {
		//GPIO_ResetBits(GPION_LIST[i],GPIO_LIST[i]);
		GPION_LIST[i]->BSRRH = GPIO_LIST[i];
	}
	else {
		//GPIO_SetBits(GPION_LIST[i],GPIO_LIST[i]);
		GPION_LIST[i]->BSRRL = GPIO_LIST[i];
	}
}
uint8 IO_OutRead( uint8 i )
{
	return GPIO_ReadOutputDataBit(GPION_LIST[i],GPIO_LIST[i]);
}
uint8 IO_InRead( uint8 i )
{
	return GPIO_ReadInputDataBit(GPION_LIST[i],GPIO_LIST[i]);
}
void vos_IO_AnalogOpen( uint8 i, uint8 d )
{
	GPIO_InitStructure.GPIO_Pin = GPIO_LIST[i];
	//结构体_引脚-PA1
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
	//结构体_引脚-引脚频率_50Mhz
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_AIN;
	//结构体_引脚-引脚模式_模拟输入
	GPIO_Init(GPION_LIST[i], &GPIO_InitStructure);
}
int32 vos_IO_AnalogRead( uint8 i )
{
	int32 d;
	
	//这里为 PB0 - PB1
	if( i >= 16 && i <= 17 ) {
		i -= 8;
	}
	//这里为 PC0 - PC5
	else if( i >= 32 && i <= 37 ) {
		i -= 22;
	}
	//这里为 PA0 - PA7
	else {
		//...
	}
	d = vos_IO_AD_Read(ADC1, i);
	return d / 4;
}
void vos_IO_PWM_SetPin( uint8 clk )
{
	
}
void vos_IO_PWM_SetData( uint8 clk, int32 data, int32 max )
{
	
}







