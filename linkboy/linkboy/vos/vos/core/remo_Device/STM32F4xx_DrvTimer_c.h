
#include "STM32f4xx_tim.h"


void vos_DrvTimer_Init()
{
    TIM_TimeBaseInitTypeDef TIM_TimeBaseInitStruct;
    NVIC_InitTypeDef NVIC_InitStruct;
    
    TIM_TimeBaseInitStruct.TIM_CounterMode = TIM_CounterMode_Down;
	
	//1000ms
    //TIM_TimeBaseInitStruct.TIM_Period = 10000-1;
    //TIM_TimeBaseInitStruct.TIM_Prescaler = 7200-1;
	
	//10us
	TIM_TimeBaseInitStruct.TIM_Period = 1000-1;
	TIM_TimeBaseInitStruct.TIM_Prescaler = 0;
	
	//2020.3.1 对于F4系列 下边这个TIM_CKD_DIV1必须要设置 否则定时器不工作, 不知道为什么
	TIM_TimeBaseInitStruct.TIM_ClockDivision=TIM_CKD_DIV1;
	
	
    RCC_APB1PeriphClockCmd(RCC_APB1Periph_TIM2, ENABLE);
	
    TIM_TimeBaseInit( TIM2, & TIM_TimeBaseInitStruct);
    TIM_ITConfig(TIM2,TIM_IT_Update, ENABLE);
    TIM_ClearITPendingBit( TIM2,TIM_IT_Update);
	
	NVIC_InitStruct.NVIC_IRQChannel = TIM2_IRQn;
    NVIC_InitStruct.NVIC_IRQChannelCmd = ENABLE;
    NVIC_Init( &NVIC_InitStruct);
    TIM_Cmd( TIM2, DISABLE);
}
void vos_DrvTimer_Start()
{
	 TIM_Cmd( TIM2, ENABLE);
}
void vos_DrvTimer_Stop()
{
	 TIM_Cmd( TIM2, DISABLE);
}
void TIM2_IRQHandler(void)
{
	if(RESET != TIM_GetITStatus( TIM2,TIM_IT_Update)) {
		TIM_ClearITPendingBit( TIM2,TIM_IT_Update);
		
		//100KHz
		remo_tick_run();
	}
}













