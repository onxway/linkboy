
//GD32F330 晶振频率: 84MHz

void IO_Init(void);
void remo_DrvTimer_Init(void);

//系统初始化
void vos_User_Init()
{
	SysTick_Config(0xFFFFFF);//从 0xFFFFFF 倒计时
	
	IO_Init();
	remo_DrvTimer_Init();
}
void vos_User_Start()
{
	vos_USART_Open( 0, 115200 );
}
//开启全局中断
void vos_SYS_InterruptEnable()
{
	__enable_irq();
}
//关闭全局中断
void vos_SYS_InterruptDisable()
{
	__disable_irq();
}


//初始化串口0
void remo_USART0_Init(uint32 BaudRate)
{
/* enable COM GPIO clock */
    rcu_periph_clock_enable(RCU_GPIOA);

    /* enable USART clock */
    rcu_periph_clock_enable(RCU_USART0);

    /* connect port to USARTx_Tx */
    gpio_af_set( GPIOA, GPIO_AF_1, GPIO_PIN_9 );

    /* connect port to USARTx_Rx */
    gpio_af_set( GPIOA, GPIO_AF_1, GPIO_PIN_10 );

    /* configure USART Tx as alternate function push-pull */
    gpio_mode_set(GPIOA, GPIO_MODE_AF, GPIO_PUPD_PULLUP, GPIO_PIN_9);
    gpio_output_options_set(GPIOA, GPIO_OTYPE_PP, GPIO_OSPEED_10MHZ, GPIO_PIN_9);

    /* configure USART Rx as alternate function push-pull */
    gpio_mode_set(GPIOA, GPIO_MODE_AF, GPIO_PUPD_PULLUP, GPIO_PIN_10);
    gpio_output_options_set(GPIOA, GPIO_OTYPE_PP, GPIO_OSPEED_10MHZ, GPIO_PIN_10);

    /* USART configure */
    usart_deinit(USART0);
    usart_baudrate_set(USART0, BaudRate );
    usart_receive_config(USART0, USART_RECEIVE_ENABLE);
    usart_transmit_config(USART0, USART_TRANSMIT_ENABLE);

    usart_enable(USART0);
       
	nvic_irq_enable(USART0_IRQn, 3U, 3U);
	usart_interrupt_enable(USART0,USART_INT_RBNE);
}
//串口波特率
void vos_USART_Open( int8 ID, uint32 b )
{
	if( ID == 0 ) {
		remo_USART0_Init( b );
	}
}
//发送字节
void vos_USART_WriteByte( int8 ID, uint8 tempc )
{
	if( ID == 0 ) {
		usart_data_transmit(USART0, (uint8_t) tempc);
		while (usart_flag_get(USART0, USART_FLAG_TBE) == RESET)
		{
			//...
		}
	}
}
//--------------------------------------------------
//串口中断
void USART0_IRQHandler(void)
{
	uint8 Data;
    if(RESET != usart_interrupt_flag_get(USART0, USART_INT_FLAG_RBNE)){
        /* read one byte from the receive data register */
    	Data = (uint8_t)usart_data_receive(USART0);
		
    	Deal( Data );
		
		//存储到对应的缓冲区
		UART_List_Receive( 0, Data );
    }
}

uint32 vos_Timer_Tick0()
{
	return ~SysTick->VAL;
}
uint32 vos_Timer_Tick0MS()
{
	return remo_CLOCK_M * 1000;
}
uint32 vos_Timer_Tick1()
{
	return ~SysTick->VAL;
}
uint32 vos_Timer_Tick1US()
{
	return remo_CLOCK_M;
}

#define NUMBER 64

uint32 GPIO_LIST[NUMBER];
uint32 GPION_LIST[NUMBER];

void rcu_config(void);
void adc_config(void);

void IO_Init()
{
	uint8 i;
	
	GPIO_LIST[0] = GPIO_PIN_0;
	GPIO_LIST[1] = GPIO_PIN_1;
	GPIO_LIST[2] = GPIO_PIN_2;
	GPIO_LIST[3] = GPIO_PIN_3;
	GPIO_LIST[4] = GPIO_PIN_4;
	GPIO_LIST[5] = GPIO_PIN_5;
	GPIO_LIST[6] = GPIO_PIN_6;
	GPIO_LIST[7] = GPIO_PIN_7;
	GPIO_LIST[8] = GPIO_PIN_8;
	GPIO_LIST[9] = GPIO_PIN_9;
	GPIO_LIST[10] = GPIO_PIN_10;
	GPIO_LIST[11] = GPIO_PIN_11;
	GPIO_LIST[12] = GPIO_PIN_12;
	GPIO_LIST[13] = GPIO_PIN_13;
	GPIO_LIST[14] = GPIO_PIN_14;
	GPIO_LIST[15] = GPIO_PIN_15;
	
	GPIO_LIST[16] = GPIO_PIN_0;
	GPIO_LIST[17] = GPIO_PIN_1;
	GPIO_LIST[18] = GPIO_PIN_2;
	GPIO_LIST[19] = GPIO_PIN_3;
	GPIO_LIST[20] = GPIO_PIN_4;
	GPIO_LIST[21] = GPIO_PIN_5;
	GPIO_LIST[22] = GPIO_PIN_6;
	GPIO_LIST[23] = GPIO_PIN_7;
	GPIO_LIST[24] = GPIO_PIN_8;
	GPIO_LIST[25] = GPIO_PIN_9;
	GPIO_LIST[26] = GPIO_PIN_10;
	GPIO_LIST[27] = GPIO_PIN_11;
	GPIO_LIST[28] = GPIO_PIN_12;
	GPIO_LIST[29] = GPIO_PIN_13;
	GPIO_LIST[30] = GPIO_PIN_14;
	GPIO_LIST[31] = GPIO_PIN_15;
	
	GPIO_LIST[32] = GPIO_PIN_0;
	GPIO_LIST[33] = GPIO_PIN_1;
	GPIO_LIST[34] = GPIO_PIN_2;
	GPIO_LIST[35] = GPIO_PIN_3;
	GPIO_LIST[36] = GPIO_PIN_4;
	GPIO_LIST[37] = GPIO_PIN_5;
	GPIO_LIST[38] = GPIO_PIN_6;
	GPIO_LIST[39] = GPIO_PIN_7;
	GPIO_LIST[40] = GPIO_PIN_8;
	GPIO_LIST[41] = GPIO_PIN_9;
	GPIO_LIST[42] = GPIO_PIN_10;
	GPIO_LIST[43] = GPIO_PIN_11;
	GPIO_LIST[44] = GPIO_PIN_12;
	GPIO_LIST[45] = GPIO_PIN_13;
	GPIO_LIST[46] = GPIO_PIN_14;
	GPIO_LIST[47] = GPIO_PIN_15;
	
	GPIO_LIST[48] = GPIO_PIN_0;
	GPIO_LIST[49] = GPIO_PIN_1;
	GPIO_LIST[50] = GPIO_PIN_2;
	GPIO_LIST[51] = GPIO_PIN_3;
	GPIO_LIST[52] = GPIO_PIN_4;
	GPIO_LIST[53] = GPIO_PIN_5;
	GPIO_LIST[54] = GPIO_PIN_6;
	GPIO_LIST[55] = GPIO_PIN_7;
	GPIO_LIST[56] = GPIO_PIN_8;
	GPIO_LIST[57] = GPIO_PIN_9;
	GPIO_LIST[58] = GPIO_PIN_10;
	GPIO_LIST[59] = GPIO_PIN_11;
	GPIO_LIST[60] = GPIO_PIN_12;
	GPIO_LIST[61] = GPIO_PIN_13;
	GPIO_LIST[62] = GPIO_PIN_14;
	GPIO_LIST[63] = GPIO_PIN_15;
	
	for( i = 0; i < NUMBER; ++i ) {
		if( i < 16 ) {
			GPION_LIST[i] = GPIOA;
			rcu_periph_clock_enable(RCU_GPIOA);
		}
		else if( i < 32 ) {
			GPION_LIST[i] = GPIOB;
			rcu_periph_clock_enable(RCU_GPIOB);
		}
		else if( i < 48 ) {
			GPION_LIST[i] = GPIOC;
			rcu_periph_clock_enable(RCU_GPIOC);
		}
		else {
			GPION_LIST[i] = GPIOD;
			rcu_periph_clock_enable(RCU_GPIOD);
		}
	}
	
	rcu_config();
	adc_config();
}
void IO_DirWrite( uint8 i, uint8 d )
{
	if( d == 0 ) {
		gpio_mode_set( GPION_LIST[i], GPIO_MODE_INPUT, GPIO_PUPD_PULLUP, GPIO_LIST[i] );
		gpio_output_options_set( GPION_LIST[i], GPIO_OTYPE_PP, GPIO_OSPEED_50MHZ, GPIO_LIST[i] );
		gpio_bit_reset( GPION_LIST[i], GPIO_LIST[i] );
	}
	else {
		gpio_mode_set( GPION_LIST[i], GPIO_MODE_OUTPUT, GPIO_PUPD_NONE, GPIO_LIST[i] );
		gpio_output_options_set( GPION_LIST[i], GPIO_OTYPE_PP, GPIO_OSPEED_50MHZ, GPIO_LIST[i] );
		gpio_bit_reset( GPION_LIST[i], GPIO_LIST[i] );
	}
}
void IO_OutWrite( uint8 i, uint8 d )
{
	if( d == 0 ) {
		gpio_bit_reset( GPION_LIST[i], GPIO_LIST[i] );
	}
	else {
		gpio_bit_set( GPION_LIST[i], GPIO_LIST[i] );
	}
}
uint8 IO_OutRead( uint8 i )
{
	return gpio_output_bit_get(GPION_LIST[i], GPIO_LIST[i]);
}
uint8 IO_InRead( uint8 i )
{
	return gpio_input_bit_get(GPION_LIST[i], GPIO_LIST[i]);
}


void rcu_config(void)
{
    //rcu_periph_clock_enable(RCU_GPIOA);
    rcu_periph_clock_enable(RCU_ADC);
    rcu_adc_clock_config(RCU_ADCCK_APB2_DIV6);
}
void adc_config(void)
{
    adc_special_function_config(ADC_CONTINUOUS_MODE, ENABLE);
    adc_external_trigger_source_config(ADC_REGULAR_CHANNEL, ADC_EXTTRIG_REGULAR_NONE);
    adc_data_alignment_config(ADC_DATAALIGN_RIGHT);
    adc_channel_length_config(ADC_REGULAR_CHANNEL, 1U);
    //adc_regular_channel_config(0U, ADC_CHANNEL_2, ADC_SAMPLETIME_55POINT5);
    adc_external_trigger_config(ADC_REGULAR_CHANNEL, ENABLE);
    adc_resolution_config(ADC_RESOLUTION_10B);
    adc_enable();
    SoftDelay_ms(5);
    adc_calibration_enable();
	adc_software_trigger_enable(ADC_REGULAR_CHANNEL);
}


void vos_IO_AnalogOpen( uint8 i, uint8 d )
{
	gpio_mode_set( GPION_LIST[i], GPIO_MODE_ANALOG, GPIO_PUPD_NONE, GPIO_LIST[i] );
}
int32 vos_IO_AnalogRead( uint8 i )
{
	adc_regular_channel_config(0U, i, ADC_SAMPLETIME_55POINT5);
	adc_flag_clear(ADC_FLAG_EOC);
    while(SET != adc_flag_get(ADC_FLAG_EOC)) {}
    return ADC_RDATA;
}
void vos_IO_PWM_SetPin( uint8 clk )
{
	
}

void vos_IO_PWM_SetData( uint8 clk, int32 data, int32 max )
{
	
}

void vos_Flash_Load()
{
}
void vos_Flash_Save()
{
}
void vos_Flash_Clear()
{
	uint8 i;
	
	fmc_unlock();
	for( i = 0; i < Flash_PageNumber; i++ ) { 
		fmc_state_enum fmc_state = fmc_page_erase( Flash_USER_ADDR + Flash_PageSize * i );
		
		if( fmc_state != FMC_READY ) {
			remo_USART_write( 0x56 );
		}
	}
	fmc_lock();
}
void vos_Flash_WriteUint32( uint32 addr, uint32 b )
{
	fmc_state_enum fmc_state;
	
	fmc_unlock();
	fmc_state = fmc_word_program( Flash_USER_ADDR + addr, b );
	
	if( fmc_state != FMC_READY ) {
		
		remo_USART_write( 0x34 );
		remo_USART_write( fmc_state );
	}
	fmc_lock();
}

//初始化定时器
void remo_DrvTimer_Init()
{
	timer_parameter_struct timer13_parameter;
	//时钟使能
	rcu_periph_clock_enable(RCU_TIMER13);
	//定时器配置
	timer_deinit(TIMER13);
	timer13_parameter.period = 10 - 1; //配置为10us
	timer13_parameter.prescaler = 84 - 1; //分频因子
	timer13_parameter.clockdivision = TIMER_CKDIV_DIV1;
	timer13_parameter.alignedmode = TIMER_COUNTER_EDGE;
	timer13_parameter.counterdirection = TIMER_COUNTER_UP;
	timer13_parameter.repetitioncounter = 0;
	timer_init(TIMER13,&timer13_parameter);
	//中断配置
	nvic_irq_enable(TIMER13_IRQn,3,3);
	//清除中断标志
	timer_interrupt_flag_clear(TIMER13,TIMER_INT_FLAG_UP);
	//中断使能
	timer_interrupt_enable(TIMER13,TIMER_INT_UP);
	//定时器使能
	timer_disable(TIMER13);
}
//开启定时器
void vos_DrvTimer_Start()
{
	timer_enable( TIMER13 );
}
//关闭定时器
void vos_DrvTimer_Stop()
{
	timer_disable( TIMER13 );
}
void TIMER13_IRQHandler()
{
	timer_interrupt_flag_clear(TIMER13,TIMER_INTF_UPIF);

	//触发频率为 100KHz
	remo_tick_run();
}







