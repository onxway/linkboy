

void usart_baudrate_set(USART_TypeDef* USARTx, uint32 BaudRate);

//初始化串口2
void remo_USART2_Init(uint32 BaudRate)
{
	GPIO_InitTypeDef GPIO_InitStructure;

	//PA2:USART2_TX  ; PA3:USART2_RX
	RCC_AHB1PeriphClockCmd(RCC_AHB1Periph_GPIOA, ENABLE); 
	RCC_APB1PeriphClockCmd(RCC_APB1Periph_USART2, ENABLE);

	GPIO_PinAFConfig(GPIOA, GPIO_PinSource2, GPIO_AF_USART2);  
	GPIO_PinAFConfig(GPIOA, GPIO_PinSource3, GPIO_AF_USART2);

	GPIO_InitStructure.GPIO_Pin = GPIO_Pin_2 | GPIO_Pin_3;
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
	GPIO_InitStructure.GPIO_OType = GPIO_OType_PP;
	GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_UP;
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_AF;
	GPIO_Init(GPIOA, &GPIO_InitStructure);

	usart_baudrate_set( USART2, BaudRate );

	USART_ClearFlag(USART2, USART_FLAG_TC);
	USART_Cmd(USART2, ENABLE);
	
	NVIC_InitTypeDef NVIC_InitTypestucture;
   NVIC_InitTypestucture.NVIC_IRQChannel=USART2_IRQn;
   NVIC_InitTypestucture.NVIC_IRQChannelCmd=ENABLE;
   NVIC_InitTypestucture.NVIC_IRQChannelPreemptionPriority=1;
   NVIC_InitTypestucture.NVIC_IRQChannelSubPriority=1;
   
   NVIC_Init(&NVIC_InitTypestucture);
   
   USART_ITConfig(USART2,USART_IT_RXNE,ENABLE);
}
void usart_baudrate_set(USART_TypeDef* USARTx, uint32 BaudRate)
{
	USART_InitTypeDef USART_InitStructure;
	USART_InitStructure.USART_BaudRate = BaudRate;//波特率设置
	USART_InitStructure.USART_WordLength = USART_WordLength_8b;
	USART_InitStructure.USART_StopBits = USART_StopBits_1;
	USART_InitStructure.USART_Parity = USART_Parity_No;
	USART_InitStructure.USART_HardwareFlowControl = USART_HardwareFlowControl_None;
	USART_InitStructure.USART_Mode = USART_Mode_Rx | USART_Mode_Tx;
	USART_Init(USARTx, &USART_InitStructure);
}

void USART2_IRQHandler(void)
{
	uint8 Data;
	if(USART_GetITStatus(USART2,USART_IT_RXNE))
	{
        Data = USART_ReceiveData(USART2);
		USART_ClearFlag(USART2,USART_FLAG_TC);
		
		Deal( Data );
		
		//存储到对应的缓冲区
		UART_List_Receive( 2, Data );
    }
}

//串口波特率
void vos_USART_Open( int8 ID, uint32 b )
{
	if( ID == 2 ) {
		remo_USART2_Init( b );
	}
}
//发送字节
void vos_USART_WriteByte( int8 ID, uint8 tempc )
{
	/*
	USART2->DR = tempc;
	while(USART_GetFlagStatus(USART2, USART_FLAG_TC) == RESET);
	*/
	//while(USART_GetFlagStatus(USART1,USART_FLAG_TC)!=SET);
	
	if( ID == 2 ) {
		//while((USART2->SR&0x0040)==0);
		while( USART_GetFlagStatus(USART2, USART_FLAG_TXE) == RESET );
		USART2->DR = tempc;
	}
}







