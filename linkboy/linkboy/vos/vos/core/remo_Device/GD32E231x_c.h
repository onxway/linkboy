
//========================================================================================

void IO_Init(void);
void vos_DrvTimer_Init(void);

//系统初始化
void vos_User_Init()
{
	SysTick_Config(0xFFFFFF);//从 0xFFFFFF 倒计时
	
	IO_Init();
	vos_DrvTimer_Init();
}
void vos_User_Start()
{
	vos_USART_Open( 0, 115200 );
}
//开启全局中断
void vos_SYS_InterruptEnable()
{
	__enable_irq();
}
//关闭全局中断
void vos_SYS_InterruptDisable()
{
	__disable_irq();
}


//初始化串口0
void remo_USART0_Init(uint32 BaudRate)
{
	//eclic_irq_enable(USART0_IRQn, 1, 0);
	nvic_irq_enable(USART0_IRQn, 0);
	
	/* enable GPIO clock */
	rcu_periph_clock_enable(RCU_GPIOA);
	/* enable USART clock */
	rcu_periph_clock_enable(RCU_USART0);

	/* connect port to USARTx_Tx */
    gpio_af_set(GPIOA, GPIO_AF_1, GPIO_PIN_9);

    /* connect port to USARTx_Rx */
    gpio_af_set(GPIOA, GPIO_AF_1, GPIO_PIN_10);

    /* configure USART Tx as alternate function push-pull */
    gpio_mode_set(GPIOA, GPIO_MODE_AF, GPIO_PUPD_PULLUP, GPIO_PIN_9);
    gpio_output_options_set(GPIOA, GPIO_OTYPE_PP, GPIO_OSPEED_10MHZ, GPIO_PIN_9);

    /* configure USART Rx as alternate function push-pull */
    gpio_mode_set(GPIOA, GPIO_MODE_AF, GPIO_PUPD_PULLUP, GPIO_PIN_10);
    gpio_output_options_set(GPIOA, GPIO_OTYPE_PP, GPIO_OSPEED_10MHZ, GPIO_PIN_10);
	
	/* USART configure */
	usart_deinit(USART0);
	
	usart_baudrate_set( USART0, BaudRate );
	
	usart_word_length_set(USART0, USART_WL_8BIT);
	usart_stop_bit_set(USART0, USART_STB_1BIT);
	usart_parity_config(USART0, USART_PM_NONE);
	usart_hardware_flow_rts_config(USART0, USART_RTS_DISABLE);
	usart_hardware_flow_cts_config(USART0, USART_CTS_DISABLE);
	usart_receive_config(USART0, USART_RECEIVE_ENABLE);
	usart_transmit_config(USART0, USART_TRANSMIT_ENABLE);
	usart_enable(USART0);
	/* enable USART0 receive interrupt */
	usart_interrupt_enable(USART0, USART_INT_RBNE);
}

void vos_USART_Open( int8 ID, uint32 b )
{
	if( ID == 0 ) {
		//对于E23X系列来说, 好像这个语句无法修改波特率
		//usart_baudrate_set(USART0, b);
		
		remo_USART0_Init( b );
	}
}
//串口中断
void USART0_IRQHandler(void)
{
	uint8 Data;
    if(RESET != usart_interrupt_flag_get(USART0, USART_INT_FLAG_RBNE)){
        /* read one byte from the receive data register */
    	Data = (uint8_t)usart_data_receive(USART0);
		
    	Deal( Data );
		
	//存储到对应的缓冲区
	UART_List_Receive( 0, Data );
    }
}
//发送字节
void vos_USART_WriteByte( int8 ID, uint8 tempc )
{
	/*
	USART2->DR = tempc;
	while(USART_GetFlagStatus(USART2, USART_FLAG_TC) == RESET);
	*/
	//while(USART_GetFlagStatus(USART1,USART_FLAG_TC)!=SET);
	
	if( ID == 0 ) {
		usart_data_transmit(USART0, (uint8_t) tempc);
		while (usart_flag_get(USART0, USART_FLAG_TBE) == RESET)
		{
			//...
		}
	}
}

//====================================================================================

void SysTick_Handler(void)
{
	//ntime--;
}

uint32 vos_Timer_Tick0()
{
	return ~SysTick->VAL;
}
uint32 vos_Timer_Tick0MS()
{
	return 72000;
}
uint32 vos_Timer_Tick1()
{
	return ~SysTick->VAL;
}
uint32 vos_Timer_Tick1US()
{
	return 72;
}

//========================================================================================

void IO_timer_config( void );
void IO_adc_config(void);
void IO_rcu_config(void);

#define NUMBER 64

uint32 GPIO_LIST[NUMBER];
uint32 GPION_LIST[NUMBER];
rcu_periph_enum GPIO_CLK[NUMBER];





uint16_t ad_value[2];
void dma_config(void)
{
	/* enable DMA clock */
    rcu_periph_clock_enable(RCU_DMA);
	
    dma_parameter_struct dma_init_struct;

    /* initialize DMA channel0 */
    dma_deinit(DMA_CH0);
    dma_init_struct.direction    = DMA_PERIPHERAL_TO_MEMORY;
    dma_init_struct.memory_addr  = (uint32_t)ad_value;
    dma_init_struct.memory_inc   = DMA_MEMORY_INCREASE_ENABLE;
    dma_init_struct.memory_width = DMA_MEMORY_WIDTH_16BIT;
    dma_init_struct.number       = 2;
    dma_init_struct.periph_addr  = (uint32_t)&(ADC_RDATA);
    dma_init_struct.periph_inc   = DMA_PERIPH_INCREASE_DISABLE;
    dma_init_struct.periph_width = DMA_PERIPHERAL_WIDTH_16BIT;
    dma_init_struct.priority     = DMA_PRIORITY_ULTRA_HIGH;
    dma_init(DMA_CH0, &dma_init_struct);
    
    /* configure DMA mode */
    dma_circulation_enable(DMA_CH0);
    dma_memory_to_memory_disable(DMA_CH0);
    
    /* enable DMA channel0 */
    dma_channel_enable(DMA_CH0);
}
void timer_config(void)
{
    /* enable TIMER0 clock */
    rcu_periph_clock_enable(RCU_TIMER0);
	
    timer_oc_parameter_struct timer_ocintpara;
    timer_parameter_struct timer_initpara;

    timer_deinit(TIMER0);

    /* TIMER0 configuration */
    timer_initpara.prescaler         = 5;// 72/6 = 12MHz
    timer_initpara.alignedmode       = TIMER_COUNTER_EDGE;
    timer_initpara.counterdirection  = TIMER_COUNTER_UP;
    timer_initpara.period            = 399;// 12MHz/400 = 30KHz
    timer_initpara.clockdivision     = TIMER_CKDIV_DIV1;
    timer_initpara.repetitioncounter = 0;
    timer_init(TIMER0, &timer_initpara);

    /* CH0 configuration in PWM mode1 */
    timer_ocintpara.ocpolarity  = TIMER_OC_POLARITY_LOW;
    timer_ocintpara.outputstate = TIMER_CCX_ENABLE;
    timer_channel_output_config(TIMER0, TIMER_CH_0, &timer_ocintpara);

    timer_channel_output_pulse_value_config(TIMER0, TIMER_CH_0, 100);
    timer_channel_output_mode_config(TIMER0, TIMER_CH_0, TIMER_OC_MODE_PWM1);
    timer_channel_output_shadow_config(TIMER0, TIMER_CH_0, TIMER_OC_SHADOW_DISABLE);

    /* auto-reload preload enable */
    timer_auto_reload_shadow_enable(TIMER0);
    timer_primary_output_config(TIMER0, ENABLE);
}
void adc_config(void)
{
    /* ADCCLK = PCLK2/6 */
    rcu_adc_clock_config(RCU_ADCCK_APB2_DIV6);// ADC时钟为 72/6 = 12MHz

    /* enable ADC clock */
    rcu_periph_clock_enable(RCU_ADC);
	
	dma_config();
	
	timer_config();
    /* ADC channel length config */
    adc_channel_length_config(ADC_REGULAR_CHANNEL, 1);

    /* ADC regular channel config */
    adc_regular_channel_config(0, ADC_CHANNEL_1, ADC_SAMPLETIME_55POINT5);//序号0 通道1，采样周期55.5

    /* ADC external trigger enable */
    adc_external_trigger_config(ADC_REGULAR_CHANNEL, ENABLE);
    /* ADC external trigger source config */
    adc_external_trigger_source_config(ADC_REGULAR_CHANNEL, ADC_EXTTRIG_REGULAR_T0_CH0);//选择TIMER0_CH0为外部触发源
    /* ADC data alignment config */
    adc_data_alignment_config(ADC_DATAALIGN_RIGHT);
    /* enable ADC interface */
    adc_enable();
    /* ADC calibration and reset calibration */
    adc_calibration_enable();
    /* ADC DMA function enable */
    adc_dma_mode_enable();
}




void IO_Init()
{
	uint8 i;
	
	GPIO_LIST[0] = GPIO_PIN_0;
	GPIO_LIST[1] = GPIO_PIN_1;
	GPIO_LIST[2] = GPIO_PIN_2;
	GPIO_LIST[3] = GPIO_PIN_3;
	GPIO_LIST[4] = GPIO_PIN_4;
	GPIO_LIST[5] = GPIO_PIN_5;
	GPIO_LIST[6] = GPIO_PIN_6;
	GPIO_LIST[7] = GPIO_PIN_7;
	GPIO_LIST[8] = GPIO_PIN_8;
	GPIO_LIST[9] = GPIO_PIN_9;
	GPIO_LIST[10] = GPIO_PIN_10;
	GPIO_LIST[11] = GPIO_PIN_11;
	GPIO_LIST[12] = GPIO_PIN_12;
	GPIO_LIST[13] = GPIO_PIN_13;
	GPIO_LIST[14] = GPIO_PIN_14;
	GPIO_LIST[15] = GPIO_PIN_15;
	
	GPIO_LIST[16] = GPIO_PIN_0;
	GPIO_LIST[17] = GPIO_PIN_1;
	GPIO_LIST[18] = GPIO_PIN_2;
	GPIO_LIST[19] = GPIO_PIN_3;
	GPIO_LIST[20] = GPIO_PIN_4;
	GPIO_LIST[21] = GPIO_PIN_5;
	GPIO_LIST[22] = GPIO_PIN_6;
	GPIO_LIST[23] = GPIO_PIN_7;
	GPIO_LIST[24] = GPIO_PIN_8;
	GPIO_LIST[25] = GPIO_PIN_9;
	GPIO_LIST[26] = GPIO_PIN_10;
	GPIO_LIST[27] = GPIO_PIN_11;
	GPIO_LIST[28] = GPIO_PIN_12;
	GPIO_LIST[29] = GPIO_PIN_13;
	GPIO_LIST[30] = GPIO_PIN_14;
	GPIO_LIST[31] = GPIO_PIN_15;
	
	GPIO_LIST[32] = GPIO_PIN_0;
	GPIO_LIST[33] = GPIO_PIN_1;
	GPIO_LIST[34] = GPIO_PIN_2;
	GPIO_LIST[35] = GPIO_PIN_3;
	GPIO_LIST[36] = GPIO_PIN_4;
	GPIO_LIST[37] = GPIO_PIN_5;
	GPIO_LIST[38] = GPIO_PIN_6;
	GPIO_LIST[39] = GPIO_PIN_7;
	GPIO_LIST[40] = GPIO_PIN_8;
	GPIO_LIST[41] = GPIO_PIN_9;
	GPIO_LIST[42] = GPIO_PIN_10;
	GPIO_LIST[43] = GPIO_PIN_11;
	GPIO_LIST[44] = GPIO_PIN_12;
	GPIO_LIST[45] = GPIO_PIN_13;
	GPIO_LIST[46] = GPIO_PIN_14;
	GPIO_LIST[47] = GPIO_PIN_15;
	
	GPIO_LIST[48] = GPIO_PIN_0;
	GPIO_LIST[49] = GPIO_PIN_1;
	GPIO_LIST[50] = GPIO_PIN_2;
	GPIO_LIST[51] = GPIO_PIN_3;
	GPIO_LIST[52] = GPIO_PIN_4;
	GPIO_LIST[53] = GPIO_PIN_5;
	GPIO_LIST[54] = GPIO_PIN_6;
	GPIO_LIST[55] = GPIO_PIN_7;
	GPIO_LIST[56] = GPIO_PIN_8;
	GPIO_LIST[57] = GPIO_PIN_9;
	GPIO_LIST[58] = GPIO_PIN_10;
	GPIO_LIST[59] = GPIO_PIN_11;
	GPIO_LIST[60] = GPIO_PIN_12;
	GPIO_LIST[61] = GPIO_PIN_13;
	GPIO_LIST[62] = GPIO_PIN_14;
	GPIO_LIST[63] = GPIO_PIN_15;
	
	for( i = 0; i < NUMBER; ++i ) {
		if( i < 16 ) {
			GPION_LIST[i] = GPIOA;
			GPIO_CLK[i] = RCU_GPIOA;
		}
		else if( i < 32 ) {
			GPION_LIST[i] = GPIOB;
			GPIO_CLK[i] = RCU_GPIOB;
		}
		else if( i < 48 ) {
			GPION_LIST[i] = GPIOC;
			GPIO_CLK[i] = RCU_GPIOC;
		}
		else {
			///GPION_LIST[i] = GPIOD;
			///GPIO_CLK[i] = RCU_GPIOD;
		}
	}
	
	for( i = 0; i < NUMBER; ++i ) {

		/* enable the led clock */
		rcu_periph_clock_enable(GPIO_CLK[i]);
		/* configure led GPIO port */
		///gpio_init(GPION_LIST[i], GPIO_MODE_IPU, GPIO_OSPEED_50MHZ, GPIO_LIST[i]);
		//gpio_mode_set(GPION_LIST[i], GPIO_MODE_INPUT, GPIO_PUPD_PULLUP, GPIO_LIST[i]);
		GPIO_BOP(GPION_LIST[i]) = GPIO_LIST[i];
	}
	
	adc_config();
}

void IO_DirWrite( uint8 i, uint8 d )
{
	if( d == 0 ) {
		gpio_mode_set(GPION_LIST[i], GPIO_MODE_INPUT, GPIO_PUPD_PULLUP, GPIO_LIST[i]);
		//gpio_mode_set(GPION_LIST[i], GPIO_MODE_INPUT, GPIO_PUPD_NONE, GPIO_LIST[i]);
	}
	else {
		gpio_mode_set(GPION_LIST[i], GPIO_MODE_OUTPUT, GPIO_PUPD_NONE, GPIO_LIST[i]);
		gpio_output_options_set(GPION_LIST[i], GPIO_OTYPE_PP, GPIO_OSPEED_50MHZ, GPIO_LIST[i]);
	}
}
void IO_OutWrite( uint8 i, uint8 d )
{
	if( d == 0 ) {
		GPIO_BC(GPION_LIST[i]) = GPIO_LIST[i];
	}
	else {
		GPIO_BOP(GPION_LIST[i]) = GPIO_LIST[i];
	}
}
uint8 IO_OutRead( uint8 i )
{
	return gpio_output_bit_get( GPION_LIST[i], GPIO_LIST[i] );
	//return GPIO_ReadOutputDataBit(GPION_LIST[i],GPIO_LIST[i]);
	//return 0;
}
uint8 IO_InRead( uint8 i )
{
	return gpio_input_bit_get(GPION_LIST[i],GPIO_LIST[i]);
}
void vos_IO_AnalogOpen( uint8 i, uint8 d )
{
	//i += 0; //这里应该加上模拟量引脚序列的偏移量
	
	gpio_mode_set( GPION_LIST[i], GPIO_MODE_ANALOG, GPIO_PUPD_NONE, GPIO_LIST[i] );
}
int32 vos_IO_AnalogRead( uint8 i )
{
	//这里为 PB0 - PB1
	if( i >= 16 && i <= 17 ) {
		i -= 8;
	}
	//这里为 PC0 - PC5
	else if( i >= 32 && i <= 37 ) {
		i -= 22;
	}
	//这里为 PA0 - PA7
	else {
		//...
	}
	
	/* ADC regular channel config */
    adc_regular_channel_config(0, i, ADC_SAMPLETIME_55POINT5);//序号0 通道1，采样周期55.5
	
	uint16_t recv_ad_val = 0;
	timer_enable(TIMER0);
	/* test on channel0 transfer complete flag */
	while( !dma_flag_get(DMA_CH0, DMA_FLAG_FTF));
	/* clear channel0 transfer complete flag */
	dma_flag_clear(DMA_CH0, DMA_FLAG_FTF);
	
	/* TIMER0 counter disable */
	timer_disable(TIMER0);
	recv_ad_val = (ad_value[0]+ad_value[1])/2;
	
	// 4096 -> 1024
	return recv_ad_val / 4;
}
void vos_IO_PWM_SetPin( uint8 clk )
{
}
void vos_IO_PWM_SetData( uint8 clk, int32 data, int32 max )
{
}

void vos_Flash_Load()
{
}
void vos_Flash_Save()
{
}
void vos_Flash_Clear()
{
	uint8 i;
	fmc_unlock();
	for( i = 0; i < VOS_Flash_PageNumber; i++ ) { 
		fmc_state_enum fmc_state = fmc_page_erase( Flash_USER_ADDR + VOS_Flash_PageSize * i );
		
		if( fmc_state != FMC_READY ) {
			//remo_USART_write( 0x56 );
			Error( E_FlashError, E_FlashError_Erase );
		}
	}
	fmc_lock();
}
void vos_Flash_WriteUint32( uint32 addr, uint32 b )
{
	fmc_unlock();
	fmc_state_enum fmc_state = fmc_word_program( Flash_USER_ADDR + addr, b );
	
	if( fmc_state != FMC_READY ) {
		
		//remo_USART_write( 0x34 );
		//remo_USART_write( fmc_state );
		Error( E_FlashError, E_FlashError_Write );
	}
	fmc_lock();
}

//========================================================================================
uint8 remo_DrvTimer_tick1;
uint8 remo_DrvTimer_tick2;

//初始化定时器
void vos_DrvTimer_Init()
{
	//eclic_irq_enable(TIMER5_IRQn, 1, 0);
	nvic_irq_enable(TIMER5_IRQn, 0);
	
	rcu_periph_clock_enable(RCU_TIMER5);

    //timer_oc_parameter_struct timer_ocintpara;
    timer_parameter_struct timer_initpara;

    timer_deinit(TIMER5);

    /* TIMER5 configuration */
    timer_initpara.prescaler         = 0;
    timer_initpara.alignedmode       = TIMER_COUNTER_EDGE;
    timer_initpara.counterdirection  = TIMER_COUNTER_UP;
    timer_initpara.period            = 720;
    timer_initpara.clockdivision     = TIMER_CKDIV_DIV1;
    timer_initpara.repetitioncounter = 0;
    timer_init(TIMER5,&timer_initpara);

    /* auto-reload preload enable */
    timer_auto_reload_shadow_enable(TIMER5);
    timer_interrupt_enable(TIMER5,TIMER_INT_UP);
    timer_disable( TIMER5 );
	
	remo_DrvTimer_tick1 = 0;
	remo_DrvTimer_tick2 = 0;
}
void vos_DrvTimer_Start()
{
	timer_enable( TIMER5 );
}
void vos_DrvTimer_Stop()
{
	timer_disable( TIMER5 );
}
//uint32 ttt;
void TIMER5_IRQHandler()
{
	timer_interrupt_flag_clear(TIMER5,TIMER_INTF_UPIF);
	
	remo_tick_run();
	
	/*
	//触发频率为 100KHz
	remo_tick_run10us();
	
	remo_DrvTimer_tick1++;
	if( remo_DrvTimer_tick1 == 10 ) {
		remo_DrvTimer_tick1 = 0;
		remo_tick_run100us();
		
		remo_DrvTimer_tick2++;
		if( remo_DrvTimer_tick2 == 10 ) {
			remo_DrvTimer_tick2 = 0;
			remo_tick_run1000us();
		}
	}
	*/
}

//========================================================================================

void IO_OutWrite_0( uint8 i )
{
	GPIO_BC(GPION_LIST[i]) = GPIO_LIST[i];
}
void IO_OutWrite_1( uint8 i )
{
	GPIO_BOP(GPION_LIST[i]) = GPIO_LIST[i];
}
//短延时, 后续应该尽量换成 nop 指令
volatile int32 Timer_i;
void vos_WS2812_Delay_0_4us(void)
{
	Timer_i += 1;
	Timer_i += 1;


	Timer_i += 1;
	Timer_i += 1;
	Timer_i += 1;

	//Timer_i += 1;
	//Timer_i += 1;
}

void vos_WS2812_WriteData( uint8 id, uint32 d )
{
	//GD32V-108MHz
	//F411RE-100MHz
	int8 i;
	for( i = 0; i < 24; ++i ) {

		IO_OutWrite_1( id );
		if( (d & 0x800000) == 0 ) {
			vos_WS2812_Delay_0_4us();
			IO_OutWrite_0( id );
			vos_WS2812_Delay_0_4us();
			vos_WS2812_Delay_0_4us();
			vos_WS2812_Delay_0_4us();
		}
		else {
			vos_WS2812_Delay_0_4us();
			vos_WS2812_Delay_0_4us();
			vos_WS2812_Delay_0_4us();
			IO_OutWrite_0( id );
			vos_WS2812_Delay_0_4us();
		}
		d <<= 1;
	}
}

void vos_UserExtDriver( uint32 v24, uint32 v8 )
{
	
}




