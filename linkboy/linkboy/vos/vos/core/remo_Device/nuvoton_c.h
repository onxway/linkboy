
void IO_Init(void);
void Flash_Init(void);

//系统初始化
void vos_User_Init()
{
	IO_Init();
	Flash_Init();
	SysTick_Config(0xFFFFFF);//从 0xFFFFFF 倒计时
}
//系统初始化
void vos_User_Start()
{
	//...
}
//开启全局中断
void vos_SYS_InterruptEnable()
{
	__enable_irq();
}
//关闭全局中断
void vos_SYS_InterruptDisable()
{
	__disable_irq();
}

void main_add_byte( uint8 data );

uint8 send_buffer[10];

//初始化串口0
void remo_USART0_Init(uint32 BaudRate)
{
	/* Configure UART0 and set UART0 Baudrate */
	UART_Open(UART0, BaudRate);
	
	/* for ePy Lite UART0 port  */
	SYS->GPD_MFPL &= ~(SYS_GPD_MFPL_PD2MFP_Msk | SYS_GPD_MFPL_PD3MFP_Msk);
	SYS->GPD_MFPL |= (SYS_GPD_MFPL_PD2MFP_UART0_RXD | SYS_GPD_MFPL_PD3MFP_UART0_TXD);
	
	/* Enable Interrupt and install the call back function */
	UART_ENABLE_INT(UART0, (UART_INTEN_RDAIEN_Msk | UART_INTEN_THREIEN_Msk | UART_INTEN_RXTOIEN_Msk));
	NVIC_EnableIRQ(UART0_IRQn);
}

//初始化串口1
void remo_USART1_Init(uint32 BaudRate)
{
	/* Configure UART0 and set UART1 Baudrate */
	UART_Open(UART1, BaudRate);
	
	/* for ePy Lite UART1 BLE port  */
	SYS->GPA_MFPH &= ~(SYS_GPA_MFPH_PA9MFP_Msk | SYS_GPA_MFPH_PA8MFP_Msk);
	SYS->GPA_MFPH |= (SYS_GPA_MFPH_PA8MFP_UART1_RXD | SYS_GPA_MFPH_PA9MFP_UART1_TXD);
	
	/* Enable Interrupt and install the call back function */
	UART_ENABLE_INT(UART1, (UART_INTEN_RDAIEN_Msk | UART_INTEN_THREIEN_Msk | UART_INTEN_RXTOIEN_Msk));
	NVIC_EnableIRQ(UART1_IRQn);
}

//初始化串口3
void remo_USART3_Init(uint32 BaudRate)
{
	/* Configure UART0 and set UART1 Baudrate */
	UART_Open(UART3, BaudRate);
	
	SYS->GPD_MFPL &= ~(SYS_GPD_MFPL_PD0MFP_Msk | SYS_GPD_MFPL_PD1MFP_Msk);
	SYS->GPD_MFPL |= (SYS_GPD_MFPL_PD0MFP_UART3_RXD | SYS_GPD_MFPL_PD1MFP_UART3_TXD);
	
	/* Enable Interrupt and install the call back function */
	UART_ENABLE_INT(UART3, (UART_INTEN_RDAIEN_Msk | UART_INTEN_THREIEN_Msk | UART_INTEN_RXTOIEN_Msk));
	NVIC_EnableIRQ(UART3_IRQn);
}
//串口波特率
void vos_USART_Open( int8 ID, uint32 BaudRate )
{
	if( ID == -1 ) {
		//remo_USART_v_Init(BaudRate);
	}
	if( ID == 0 ) {
		remo_USART0_Init(BaudRate);
	}
	if( ID == 1 ) {
		remo_USART1_Init(BaudRate);
	}
	if( ID == 3 ) {
		remo_USART3_Init(BaudRate);
	}
}
//发送字节
void vos_USART_WriteByte( int8 ID, uint8 tempc )
{
	if( ID == -1 ) {
		main_add_byte( tempc );
	}
	if( ID == 0 ) {
		UART_WRITE(UART0, tempc);
	}
	if( ID == 1 ) {
		UART_WRITE(UART1, tempc);
	}
	if( ID == 3 ) {
		UART_WRITE(UART3, tempc);
	}
}

void UART0_IRQHandler(void)
{
	uint32 u32IntStatus = UART0->INTSTS;

	if((u32IntStatus & UART_INTSTS_RDAINT_Msk) || (u32IntStatus & UART_INTSTS_RXTOINT_Msk)) {
		/* Receiver FIFO threshold level is reached or Rx time out */

		/* Get all the input characters */
		while( (!UART_GET_RX_EMPTY(UART0)) ) {
			/* Get the character from UART Buffer */
			uint8 Data = UART_READ(UART0);    /* Rx trigger level is 1 byte*/
			UART_List_Receive( 0, Data );
		}
	}
	if(u32IntStatus & UART_INTSTS_THREINT_Msk) {
		/* No more data, just stop Tx (Stop work) */
		UART0->INTEN &= ~UART_INTEN_THREIEN_Msk;
	}
}
void UART1_IRQHandler(void)
{
	uint32 u32IntStatus = UART1->INTSTS;

	if((u32IntStatus & UART_INTSTS_RDAINT_Msk) || (u32IntStatus & UART_INTSTS_RXTOINT_Msk)) {
		/* Receiver FIFO threshold level is reached or Rx time out */

		/* Get all the input characters */
		while( (!UART_GET_RX_EMPTY(UART1)) ) {
			/* Get the character from UART Buffer */
			uint8 Data = UART_READ(UART1);    /* Rx trigger level is 1 byte*/
			UART_List_Receive( 1, Data );
		}
	}
	if(u32IntStatus & UART_INTSTS_THREINT_Msk) {
		/* No more data, just stop Tx (Stop work) */
		UART1->INTEN &= ~UART_INTEN_THREIEN_Msk;
	}
}
void UART3_IRQHandler(void)
{
	uint32 u32IntStatus = UART3->INTSTS;

	if((u32IntStatus & UART_INTSTS_RDAINT_Msk) || (u32IntStatus & UART_INTSTS_RXTOINT_Msk)) {
		/* Receiver FIFO threshold level is reached or Rx time out */

		/* Get all the input characters */
		while( (!UART_GET_RX_EMPTY(UART3)) ) {
			/* Get the character from UART Buffer */
			uint8 Data = UART_READ(UART3);    /* Rx trigger level is 1 byte*/
			UART_List_Receive( 3, Data );
		}
	}
	if(u32IntStatus & UART_INTSTS_THREINT_Msk) {
		/* No more data, just stop Tx (Stop work) */
		UART3->INTEN &= ~UART_INTEN_THREIEN_Msk;
	}
}
//====================================================================================
__IO uint32 ntime;
__IO uint32 c_time_ms;
__IO uint32 c_time_us;

void SysTick_Handler(void)
{
	//ntime--;
}

uint32 vos_Timer_Tick0()
{
	return ~SysTick->VAL;
}
uint32 vos_Timer_Tick0MS()
{
	return 192000;
}
uint32 vos_Timer_Tick1()
{
	return ~SysTick->VAL;
}
uint32 vos_Timer_Tick1US()
{
	return 192;
}
//====================================================================================

void IO_AnalogInit(void);

const uint32 BIT_LIST[32] = {
	BIT0, BIT1, BIT2, BIT3, BIT4, BIT5, BIT6, BIT7, BIT8, BIT9,
	BIT10,BIT11,BIT12,BIT13,BIT14,BIT15,BIT16,BIT17,BIT18,BIT19,
	BIT20,BIT21,BIT22,BIT23,BIT24,BIT25,BIT26,BIT27,BIT28,BIT29,
	BIT30,BIT31
};

static GPIO_T *IO_port[6];
uint8 vos_IO_OutBit[96];

void IO_Init()
{
	IO_port[0] = PA;
	IO_port[1] = PB;
	IO_port[2] = PC;
	IO_port[3] = PD;
	IO_port[4] = PE;
	IO_port[5] = PF;
	
	IO_AnalogInit();
}

void IO_DirWrite( uint8 i, uint8 d )
{
	if( d == 0 ) {
		GPIO_SetMode( IO_port[i/16], BIT_LIST[i%16], GPIO_MODE_INPUT );
		GPIO_SetPullCtl( IO_port[i/16], BIT_LIST[i%16], GPIO_PUSEL_PULL_UP ); //GPIO_PUSEL_PULL_DOWN
	}
	else {
		GPIO_SetMode( IO_port[i/16], BIT_LIST[i%16], GPIO_MODE_OUTPUT );
	}
}
void IO_OutWrite( uint8 i, uint8 d )
{
	vos_IO_OutBit[i] = d;
	if( d == 0 ) {
		GPIO_PIN_DATA( i/16, i%16 ) = 0;
	}
	else {
		GPIO_PIN_DATA( i/16, i%16 ) = 1;
	}
}
uint8 IO_OutRead( uint8 i )
{
	return vos_IO_OutBit[i];
}
uint8 IO_InRead( uint8 i )
{
	return GPIO_PIN_DATA( i/16, i%16 );
}
//--------------------------------------------------
volatile uint8 g_u8AdcIntFlag;

void IO_AnalogInit()
{
	/* Enable EADC module clock */
    CLK_EnableModuleClock(EADC_MODULE);

    /* EADC clock source is 96MHz, set divider to 255, EADC clock is 96/255 MHz */
    CLK_SetModuleClock(EADC_MODULE, 0, CLK_CLKDIV0_EADC(255));

    /* Set reference voltage to external pin (3.3V) */
    SYS_SetVRef(SYS_VREFCTL_VREF_PIN);
	
	/* Set input mode as single-end and enable the A/D converter */
    EADC_Open(EADC, EADC_CTL_DIFFEN_SINGLE_END);
	
	/* Clear the A/D ADINT0 interrupt flag for safe */
    EADC_CLR_INT_FLAG(EADC, EADC_STATUS2_ADIF0_Msk);
	
	EADC_ConfigSampleModule(EADC, 0, EADC_SOFTWARE_TRIGGER, 0);
	EADC_ConfigSampleModule(EADC, 1, EADC_SOFTWARE_TRIGGER, 1);
	EADC_ConfigSampleModule(EADC, 2, EADC_SOFTWARE_TRIGGER, 2);
	EADC_ConfigSampleModule(EADC, 3, EADC_SOFTWARE_TRIGGER, 3);
	EADC_ConfigSampleModule(EADC, 4, EADC_SOFTWARE_TRIGGER, 4);
	EADC_ConfigSampleModule(EADC, 5, EADC_SOFTWARE_TRIGGER, 5);
	EADC_ConfigSampleModule(EADC, 6, EADC_SOFTWARE_TRIGGER, 6);
	EADC_ConfigSampleModule(EADC, 7, EADC_SOFTWARE_TRIGGER, 7);
	EADC_ConfigSampleModule(EADC, 8, EADC_SOFTWARE_TRIGGER, 8);
	EADC_ConfigSampleModule(EADC, 9, EADC_SOFTWARE_TRIGGER, 9);
	EADC_ConfigSampleModule(EADC, 10, EADC_SOFTWARE_TRIGGER, 10);
	EADC_ConfigSampleModule(EADC, 11, EADC_SOFTWARE_TRIGGER, 11);
	EADC_ConfigSampleModule(EADC, 14, EADC_SOFTWARE_TRIGGER, 14);
	EADC_ConfigSampleModule(EADC, 15, EADC_SOFTWARE_TRIGGER, 15);

	EADC_ConfigSampleModule(EADC, 16, EADC_SOFTWARE_TRIGGER, 16);
	EADC_ConfigSampleModule(EADC, 17, EADC_SOFTWARE_TRIGGER, 17);
	EADC_ConfigSampleModule(EADC, 18, EADC_SOFTWARE_TRIGGER, 18);
  
	
	EADC_ENABLE_INT(EADC, BIT0);//Enable sample module A/D ADINT0 interrupt.
	NVIC_EnableIRQ(EADC00_IRQn);
}
void vos_IO_AnalogOpen( uint8 i, uint8 d )
{
	i -= 16;
	uint32 MY_BIT = 0x01 << i;
	
    /* Set sample module 16 external sampling time to 0xF */
    EADC_SetExtendSampleTime(EADC, i, 0xF);

    /* Enable the sample module 16 interrupt.  */
    EADC_ENABLE_SAMPLE_MODULE_INT(EADC, 0, MY_BIT);//Enable sample module 16 interrupt.
}
int32 vos_IO_AnalogRead( uint8 i )
{
	i -= 16;
	uint32 MY_BIT = 0x01 << i;
	
	g_u8AdcIntFlag = 0;
    EADC_START_CONV(EADC, MY_BIT);
	
    /* Wait EADC conversion done */
    while( g_u8AdcIntFlag == 0 );
	
    /* Get the conversion result of the sample module 16 */
    int32 data = EADC_GET_CONV_DATA(EADC, i);
	return data / 4;
}
void EADC00_IRQHandler(void)
{
    g_u8AdcIntFlag = 1;
    EADC_CLR_INT_FLAG(EADC, EADC_STATUS2_ADIF0_Msk);      /* Clear the A/D ADINT0 interrupt flag */
}

void vos_IO_PWM_SetPin( uint8 clk )
{
	
}

void vos_IO_PWM_SetData( uint8 clk, int32 data, int32 max )
{
	
}

int32 set_data_flash_base(uint32_t u32DFBA);

void Flash_Init()
{
	SYS_UnlockReg();
	
	UART_Open(UART0, 115200);
	
    FMC_Open(); // Enable FMC ISP function
	
    // Enable Data Flash and set base address.
    if (set_data_flash_base(Flash_USER_ADDR) < 0) {
        //printf("Failed to set Data Flash base address!\n"); // error message
		return;
    }
    // Get booting source (APROM/LDROM)
    printf("Boot Mode ............................. ");
    if (FMC_GetBootSource() == 0) {
        //printf("[APROM]\n");           // debug message
	}
    else
    {
        //printf("[LDROM]\n");           // debug message
        //printf("WARNING: The driver sample code must execute in AP mode!\n");
    }

    uint32 u32Data = FMC_ReadCID();           // Get company ID, should be 0xDA.
    //printf("  Company ID ............................ [0x%08x]\n", u32Data);   // information message

    u32Data = FMC_ReadPID();           // Get product ID.
    //printf("  Product ID ............................ [0x%08x]\n", u32Data);   // information message

    for (uint8 i = 0; i < 3; i++)            // Get 96-bits UID.
    {
        u32Data = FMC_ReadUID(i);
        //printf("  Unique ID %d ........................... [0x%08x]\n", i, u32Data);  // information message
    }

    for (uint8 i = 0; i < 4; i++)            // Get 4 words UCID.
    {
        u32Data = FMC_ReadUCID(i);
        //printf("  Unique Customer ID %d .................. [0x%08x]\n", i, u32Data);  // information message
    }

    // Read User Configuration CONFIG0
    //printf("  User Config 0 ......................... [0x%08x]\n", FMC_Read(FMC_CONFIG_BASE));
    // Read User Configuration CONFIG1
    //printf("  User Config 1 ......................... [0x%08x]\n", FMC_Read(FMC_CONFIG_BASE+4));

    // Read Data Flash base address
    u32Data = FMC_ReadDataFlashBaseAddr();
    //printf("  Data Flash Base Address ............... [0x%08x]\n", u32Data);   // information message
	
	//SYS_LockReg();
}

void vos_Flash_Load()
{
}

void vos_Flash_Save()
{
}

//设置地址
int32 set_data_flash_base(uint32_t u32DFBA)
{
    uint32_t   au32Config[2]; // User Configuration

    // Read User Configuration 0 & 1
    if (FMC_ReadConfig(au32Config, 2) < 0) {
        //printf("Read User Config failed!\n");       // Error message
        return -1;                     // failed to read User Configuration
    }

    // Check if Data Flash is enabled and is expected address.
    if ((!(au32Config[0] & 0x1)) && (au32Config[1] == u32DFBA))
        return 0;                      // no need to modify User Configuration

    FMC_ENABLE_CFG_UPDATE();           // Enable User Configuration update.

    au32Config[0] &= ~0x1;             // Clear CONFIG0 bit 0 to enable Data Flash
    au32Config[1] = u32DFBA;           // Give Data Flash base address

    // Update User Configuration settings.
    if (FMC_WriteConfig(au32Config, 2) < 0)
		//printf("Write User Config failed!\n");       // Error message
        return -1;                     // failed to write user configuration

    //printf("Set Data Flash base as 0x%x.\n", Flash_USER_ADDR);  // debug message

    // Perform chip reset to make new User Config take effect.
    SYS->IPRST0 = SYS_IPRST0_CHIPRST_Msk;
	
	FMC_Close();
	
    return 0;                          // success
}

void vos_Flash_Clear()
{
	FMC_Open();
	for ( uint32 u32Addr = Flash_USER_ADDR; u32Addr < DATA_FLASH_TEST_END; u32Addr += FMC_FLASH_PAGE_SIZE) {
        int32 err = FMC_Erase( u32Addr ); // Erase page
		if( err == -1 ) {
			Error( E_FlashError, E_FlashError_Erase );
		}
	}
	FMC_Close();
}
void vos_Flash_WriteUint32( uint32 addr, uint32 b )
{
	FMC_Open();
	FMC_Write( Flash_USER_ADDR + addr, b );
	FMC_Close();
}


//初始化定时器
void remo_DrvTimer_Init()
{
	/*
    // Set timer frequency to 100KHZ
    TIMER_Open( TIMER0, TIMER_PERIODIC_MODE, 1 );

    // Enable timer interrupt
    TIMER_EnableInt(TIMER0);
    NVIC_EnableIRQ(TMR0_IRQn);
	TIMER_Close(TIMER0);
	*/
}
//开启定时器
void vos_DrvTimer_Start()
{
	// Set timer frequency to 100KHZ
    TIMER_Open( TIMER0, TIMER_PERIODIC_MODE, 100000 );

    // Enable timer interrupt
    TIMER_EnableInt(TIMER0);
	
    NVIC_EnableIRQ(TMR0_IRQn);
    TIMER_Start(TIMER0);
}
//停止定时器
void vos_DrvTimer_Stop()
{
	TIMER_Close(TIMER0);
}
void TMR0_IRQHandler(void)
{
	//if(TIMER_GetIntFlag(TIMER0) == 1)
    //{
    /* Clear Timer1 time-out interrupt flag */
    //TIMER_ClearIntFlag(TIMER0);
	
	
    //触发频率为 100KHz
	remo_tick_run();
	
	//remo_USART_write( 'X' );
	
    // clear timer interrupt flag
    TIMER_ClearIntFlag(TIMER0);
	
	
	//}
}
//=================================================================================

//短延时, 后续应该尽量换成 nop 指令
volatile int32 Timer_i;
void vos_WS2812_Delay_0_4us(void)
{
	//GD32V
	Timer_i += 1;
	//Timer_i += 1;
}

void vos_WS2812_WriteData( uint8 id, uint32 d )
{
	//用于STM32F103-72MHz
	for( int8 i = 0; i < 24; ++i ) {
		
		IO_OutWrite( id, 1 );
		if( (d & 0x800000) == 0 ) {
			IO_OutWrite( id, 0 );
			vos_WS2812_Delay_0_4us();
		}
		else {
			vos_WS2812_Delay_0_4us();
			IO_OutWrite( id, 0 );
		}
		d <<= 1;
	}
}
void vos_UserExtDriver( uint32 v24, uint32 v8 )
{
	
}











