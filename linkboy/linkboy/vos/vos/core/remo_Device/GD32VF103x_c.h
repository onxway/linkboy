
//========================================================================================

void IO_Init(void);
void remo_DrvTimer_Init(void);

//系统初始化
void vos_User_Init()
{
	IO_Init();
	remo_DrvTimer_Init();
}
void vos_User_Start()
{
	vos_USART_Open( 0, 115200 );
}
//开启全局中断
void vos_SYS_InterruptEnable()
{
	eclic_global_interrupt_enable();
}
//关闭全局中断
void vos_SYS_InterruptDisable()
{
	eclic_global_interrupt_disable();
}

//初始化串口0
void remo_USART0_Init(uint32 BaudRate)
{
	eclic_irq_enable(USART0_IRQn, 1, 0);
	//enable GPIO clock
	rcu_periph_clock_enable(RCU_GPIOA);
	//enable USART clock
	rcu_periph_clock_enable(RCU_USART0);

	//connect port to USARTx_Tx
	gpio_init(GPIOA, GPIO_MODE_AF_PP, GPIO_OSPEED_50MHZ, GPIO_PIN_9);

	//connect port to USARTx_Rx
	gpio_init(GPIOA, GPIO_MODE_IN_FLOATING, GPIO_OSPEED_50MHZ, GPIO_PIN_10);

	//USART configure
	usart_deinit(USART0);
	usart_baudrate_set(USART0, BaudRate);
	usart_word_length_set(USART0, USART_WL_8BIT);
	usart_stop_bit_set(USART0, USART_STB_1BIT);
	usart_parity_config(USART0, USART_PM_NONE);
	usart_hardware_flow_rts_config(USART0, USART_RTS_DISABLE);
	usart_hardware_flow_cts_config(USART0, USART_CTS_DISABLE);
	usart_receive_config(USART0, USART_RECEIVE_ENABLE);
	usart_transmit_config(USART0, USART_TRANSMIT_ENABLE);
	usart_enable(USART0);
	//enable USART0 receive interrupt
	usart_interrupt_enable(USART0, USART_INT_RBNE);
}

//串口波特率
void vos_USART_Open( int8 ID, uint32 b )
{
	if( ID == 0 ) {
		remo_USART0_Init( b );
	}
}
//发送字节
void vos_USART_WriteByte( int8 ID, uint8 tempc )
{
	/*
	USART2->DR = tempc;
	while(USART_GetFlagStatus(USART2, USART_FLAG_TC) == RESET);
	*/
	//while(USART_GetFlagStatus(USART1,USART_FLAG_TC)!=SET);

	if( ID == 0 ) {
		usart_data_transmit(USART0, (uint8_t) tempc);
		while (usart_flag_get(USART0, USART_FLAG_TBE) == RESET)
		{
			//...
		}
	}
}
//串口中断
void USART0_IRQHandler(void)
{
	uint8 Data;
	
    if(RESET != usart_interrupt_flag_get(USART0, USART_INT_FLAG_RBNE)){
        //read one byte from the receive data register
    	Data = (uint8_t)usart_data_receive(USART0);

    	Deal( Data );

    	//存储到对应的缓冲区
    	UART_List_Receive( 0, Data );
    }
}

/*
//初始化串口4
void remo_USART_Init(uint32 BaudRate)
{
	Serial_OK = false;

	eclic_irq_enable(USART4_IRQn, 1, 0);
	//enable GPIO clock
	rcu_periph_clock_enable(RCU_GPIOC);
	rcu_periph_clock_enable(RCU_GPIOD);
	//enable USART clock
	rcu_periph_clock_enable(RCU_USART4);

	//connect port to USARTx_Tx
	gpio_init(GPIOC, GPIO_MODE_AF_PP, GPIO_OSPEED_50MHZ, GPIO_PIN_12);

	//connect port to USARTx_Rx
	gpio_init(GPIOD, GPIO_MODE_IN_FLOATING, GPIO_OSPEED_50MHZ, GPIO_PIN_2);

	//USART configure
	usart_deinit(USART4);
	usart_baudrate_set(USART4, 115200U);
	usart_word_length_set(USART4, USART_WL_8BIT);
	usart_stop_bit_set(USART4, USART_STB_1BIT);
	usart_parity_config(USART4, USART_PM_NONE);
	usart_hardware_flow_rts_config(USART4, USART_RTS_DISABLE);
	usart_hardware_flow_cts_config(USART4, USART_CTS_DISABLE);
	usart_receive_config(USART4, USART_RECEIVE_ENABLE);
	usart_transmit_config(USART4, USART_TRANSMIT_ENABLE);
	usart_enable(USART4);
	/* enable USART0 receive interrupt
	usart_interrupt_enable(USART4, USART_INT_RBNE);
}

//串口中断
void USART4_IRQHandler(void)
{
    if(RESET != usart_interrupt_flag_get(USART4, USART_INT_FLAG_RBNE)){
        //read one byte from the receive data register
    	Serial_Data = (uint8_t)usart_data_receive(USART4);

    	Deal( Serial_Data );
    }
}
//发送字节
void remo_USART_write(uint8 tempc)
{
	usart_data_transmit(USART4, (uint8_t) tempc);
	while (usart_flag_get(USART4, USART_FLAG_TBE) == RESET)
	{
		//...
	}
}
//发送字符串
void remo_USART_SendString(uint8 *tempp)
{
	while(*tempp!=0)
	{
		remo_USART_write(*tempp++);
	}
}
*/


uint32 vos_Timer_Tick0()
{
	return get_timer_value();
}
uint32 vos_Timer_Tick0MS()
{
	return SystemCoreClock / 4000;
}
uint32 vos_Timer_Tick1()
{
	return get_timer_value();
}
uint32 vos_Timer_Tick1US()
{
	return SystemCoreClock / 4000000;
}

//========================================================================================

void IO_timer_config( void );
void IO_adc_config(void);
void IO_rcu_config(void);

#define NUMBER 64

uint32 GPIO_LIST[NUMBER];
uint32 GPION_LIST[NUMBER];
rcu_periph_enum GPIO_CLK[NUMBER];

void IO_Init()
{
	//https://blog.csdn.net/zoomdy/article/details/101386700
	//AFIO_PCF0 = (AFIO_PCF0 & 0xF8FFFFFF) | 0x04000000;
	//gpio_pin_remap_config(GPIO_SWJ_DISABLE_REMAP, ENABLE);

	//这个函数一定要放到开始!!!!! 2020.8.15
	rcu_periph_clock_enable( RCU_AF );

	//禁用JTAG这样写不行
	//gpio_pin_remap_config(GPIO_SWJ_DISABLE_REMAP, ENABLE);
	//要这么写：
	AFIO_PCF0 = (AFIO_PCF0 & 0xF8FFFFFF) | 0x04000000;

	//禁用 JTAG 和 SWD
	//gpio_pin_remap_config(GPIO_SWJ_DISABLE_REMAP, ENABLE);
	//AFIO_PCF0 = (AFIO_PCF0 & 0xF8FFFFFF) | 0x04000000;
	//gpio_pin_remap_config(GPIO_SWJ_NONJTRST_REMAP, ENABLE);

	GPIO_LIST[0] = GPIO_PIN_0;
	GPIO_LIST[1] = GPIO_PIN_1;
	GPIO_LIST[2] = GPIO_PIN_2;
	GPIO_LIST[3] = GPIO_PIN_3;
	GPIO_LIST[4] = GPIO_PIN_4;
	GPIO_LIST[5] = GPIO_PIN_5;
	GPIO_LIST[6] = GPIO_PIN_6;
	GPIO_LIST[7] = GPIO_PIN_7;
	GPIO_LIST[8] = GPIO_PIN_8;
	GPIO_LIST[9] = GPIO_PIN_9;
	GPIO_LIST[10] = GPIO_PIN_10;
	GPIO_LIST[11] = GPIO_PIN_11;
	GPIO_LIST[12] = GPIO_PIN_12;
	GPIO_LIST[13] = GPIO_PIN_13;
	GPIO_LIST[14] = GPIO_PIN_14;
	GPIO_LIST[15] = GPIO_PIN_15;
	
	GPIO_LIST[16] = GPIO_PIN_0;
	GPIO_LIST[17] = GPIO_PIN_1;
	GPIO_LIST[18] = GPIO_PIN_2;
	GPIO_LIST[19] = GPIO_PIN_3;
	GPIO_LIST[20] = GPIO_PIN_4;
	GPIO_LIST[21] = GPIO_PIN_5;
	GPIO_LIST[22] = GPIO_PIN_6;
	GPIO_LIST[23] = GPIO_PIN_7;
	GPIO_LIST[24] = GPIO_PIN_8;
	GPIO_LIST[25] = GPIO_PIN_9;
	GPIO_LIST[26] = GPIO_PIN_10;
	GPIO_LIST[27] = GPIO_PIN_11;
	GPIO_LIST[28] = GPIO_PIN_12;
	GPIO_LIST[29] = GPIO_PIN_13;
	GPIO_LIST[30] = GPIO_PIN_14;
	GPIO_LIST[31] = GPIO_PIN_15;
	
	GPIO_LIST[32] = GPIO_PIN_0;
	GPIO_LIST[33] = GPIO_PIN_1;
	GPIO_LIST[34] = GPIO_PIN_2;
	GPIO_LIST[35] = GPIO_PIN_3;
	GPIO_LIST[36] = GPIO_PIN_4;
	GPIO_LIST[37] = GPIO_PIN_5;
	GPIO_LIST[38] = GPIO_PIN_6;
	GPIO_LIST[39] = GPIO_PIN_7;
	GPIO_LIST[40] = GPIO_PIN_8;
	GPIO_LIST[41] = GPIO_PIN_9;
	GPIO_LIST[42] = GPIO_PIN_10;
	GPIO_LIST[43] = GPIO_PIN_11;
	GPIO_LIST[44] = GPIO_PIN_12;
	GPIO_LIST[45] = GPIO_PIN_13;
	GPIO_LIST[46] = GPIO_PIN_14;
	GPIO_LIST[47] = GPIO_PIN_15;
	
	GPIO_LIST[48] = GPIO_PIN_0;
	GPIO_LIST[49] = GPIO_PIN_1;
	GPIO_LIST[50] = GPIO_PIN_2;
	GPIO_LIST[51] = GPIO_PIN_3;
	GPIO_LIST[52] = GPIO_PIN_4;
	GPIO_LIST[53] = GPIO_PIN_5;
	GPIO_LIST[54] = GPIO_PIN_6;
	GPIO_LIST[55] = GPIO_PIN_7;
	GPIO_LIST[56] = GPIO_PIN_8;
	GPIO_LIST[57] = GPIO_PIN_9;
	GPIO_LIST[58] = GPIO_PIN_10;
	GPIO_LIST[59] = GPIO_PIN_11;
	GPIO_LIST[60] = GPIO_PIN_12;
	GPIO_LIST[61] = GPIO_PIN_13;
	GPIO_LIST[62] = GPIO_PIN_14;
	GPIO_LIST[63] = GPIO_PIN_15;
	
	for( uint8 i = 0; i < NUMBER; ++i ) {
		if( i < 16 ) {
			GPION_LIST[i] = GPIOA;
			GPIO_CLK[i] = RCU_GPIOA;
		}
		else if( i < 32 ) {
			GPION_LIST[i] = GPIOB;
			GPIO_CLK[i] = RCU_GPIOB;
		}
		else if( i < 48 ) {
			GPION_LIST[i] = GPIOC;
			GPIO_CLK[i] = RCU_GPIOC;
		}
		else {
			GPION_LIST[i] = GPIOD;
			GPIO_CLK[i] = RCU_GPIOD;
		}
	}
	
	for( uint8 i = 0; i < NUMBER; ++i ) {

		/* enable the led clock */
		rcu_periph_clock_enable(GPIO_CLK[i]);
		/* configure led GPIO port */
		gpio_init(GPION_LIST[i], GPIO_MODE_IPU, GPIO_OSPEED_50MHZ, GPIO_LIST[i]);
		GPIO_BOP(GPION_LIST[i]) = GPIO_LIST[i];
	}
	IO_rcu_config();
	IO_adc_config();
	IO_timer_config();
	timer_enable(TIMER1);
}

void IO_rcu_config(void)
{
    /* enable GPIOA clock */
    //rcu_periph_clock_enable(RCU_GPIOA);
    /* enable ADC clock */
    rcu_periph_clock_enable(RCU_ADC0);
    /* enable timer1 clock */
    rcu_periph_clock_enable(RCU_TIMER1);
    /* config ADC clock */
    rcu_adc_clock_config(RCU_CKADC_CKAPB2_DIV8);
}

void IO_adc_config(void)
{
    /* reset ADC */
    adc_deinit(ADC0);
    /* ADC mode config */
    adc_mode_config(ADC_MODE_FREE);
    /* ADC contineous function enable */
    adc_special_function_config(ADC0, ADC_CONTINUOUS_MODE, DISABLE);
    /* ADC scan mode disable */
    adc_special_function_config(ADC0, ADC_SCAN_MODE, DISABLE);
    /* ADC data alignment config */
    adc_data_alignment_config(ADC0, ADC_DATAALIGN_RIGHT);
    /* ADC channel length config */
    adc_channel_length_config(ADC0, ADC_REGULAR_CHANNEL, 1);

    /* ADC trigger config */
    adc_external_trigger_source_config(ADC0, ADC_REGULAR_CHANNEL, ADC0_1_EXTTRIG_REGULAR_T1_CH1);
    adc_external_trigger_config(ADC0, ADC_REGULAR_CHANNEL, ENABLE);

    /* ADC regular channel config */
    //adc_regular_channel_config(ADC0, 0, ADC_CHANNEL_3, ADC_SAMPLETIME_55POINT5);

    /* enable ADC interface */
    adc_enable(ADC0);
    SoftDelay_ms(1);
    /* ADC calibration and reset calibration */
    adc_calibration_enable(ADC0);

    /* ADC DMA function enable */
    //adc_dma_mode_enable(ADC0);

    //gpio_init(GPIOA, GPIO_MODE_AIN, GPIO_OSPEED_50MHZ, GPIO_PIN_3);
}

void IO_timer_config(void)
{
    timer_oc_parameter_struct timer_ocintpara;
    timer_parameter_struct timer_initpara;

    timer_deinit(TIMER1);

    /* TIMER1 configuration */
    timer_initpara.prescaler         = 5;
    timer_initpara.alignedmode       = TIMER_COUNTER_EDGE;
    timer_initpara.counterdirection  = TIMER_COUNTER_UP;
    timer_initpara.period            = 199;
    timer_initpara.clockdivision     = TIMER_CKDIV_DIV1;
    timer_initpara.repetitioncounter = 0;
    timer_init(TIMER1,&timer_initpara);

    /* CH1 configuration in PWM mode1 */
    timer_channel_output_struct_para_init(&timer_ocintpara);
    timer_ocintpara.ocpolarity  = TIMER_OC_POLARITY_LOW;
    timer_ocintpara.outputstate = TIMER_CCX_ENABLE;
    timer_channel_output_config(TIMER1, TIMER_CH_1, &timer_ocintpara);

    timer_channel_output_pulse_value_config(TIMER1, TIMER_CH_1, 100);
    timer_channel_output_mode_config(TIMER1, TIMER_CH_1, TIMER_OC_MODE_PWM1);
    timer_channel_output_shadow_config(TIMER1, TIMER_CH_1, TIMER_OC_SHADOW_DISABLE);

    /* auto-reload preload enable */
    timer_auto_reload_shadow_enable(TIMER1);
}
void IO_DirWrite( uint8 i, uint8 d )
{
	if( d == 0 ) {
		gpio_init(GPION_LIST[i], GPIO_MODE_IPU, GPIO_OSPEED_50MHZ, GPIO_LIST[i]); //GPIO_Mode_IPD
	}
	else {
		gpio_init(GPION_LIST[i], GPIO_MODE_OUT_PP, GPIO_OSPEED_50MHZ, GPIO_LIST[i]);
	}
}
void IO_OutWrite( uint8 i, uint8 d )
{
	if( d == 0 ) {
		GPIO_BC(GPION_LIST[i]) = GPIO_LIST[i];
	}
	else {
		GPIO_BOP(GPION_LIST[i]) = GPIO_LIST[i];
	}
}
uint8 IO_OutRead( uint8 i )
{
	return gpio_output_bit_get( GPION_LIST[i], GPIO_LIST[i] );
	//return GPIO_ReadOutputDataBit(GPION_LIST[i],GPIO_LIST[i]);
	//return 0;
}
uint8 IO_InRead( uint8 i )
{
	return gpio_input_bit_get( GPION_LIST[i], GPIO_LIST[i] );
}
void vos_IO_AnalogOpen( uint8 i, uint8 d )
{
	//i += 0; //这里应该加上模拟量引脚序列的偏移量
	gpio_init(GPION_LIST[i], GPIO_MODE_AIN, GPIO_OSPEED_50MHZ, GPIO_LIST[i]);
}
int32 vos_IO_AnalogRead( uint8 i )
{
	adc_regular_channel_config(ADC0, 0, i, ADC_SAMPLETIME_55POINT5);

	SoftDelay_ms( 1 );

	//等待转换完成
	int32 d = ADC_RDATA(ADC0);
	return d / 4;
}
void vos_IO_PWM_SetPin( uint8 clk )
{
	
}
void vos_IO_PWM_SetData( uint8 clk, int32 data, int32 max )
{
	
}

void vos_Flash_Load()
{
}
void vos_Flash_Save()
{
}
void vos_Flash_Clear()
{
	fmc_unlock();
	for( uint8 i = 0; i < Flash_PageNumber; i++ ) { 
		fmc_page_erase( Flash_USER_ADDR + Flash_PageSize * i );
	}
	fmc_lock();
}
void vos_Flash_WriteUint32( uint32 addr, uint32 b )
{
	fmc_unlock();
	fmc_halfword_program( Flash_USER_ADDR + addr, (uint16)b );
	fmc_halfword_program( Flash_USER_ADDR + addr + 2, (uint16)(b >> 16) );
	fmc_lock();
}


//初始化定时器
void remo_DrvTimer_Init()
{
	eclic_irq_enable(TIMER5_IRQn, 1, 0);

	rcu_periph_clock_enable(RCU_TIMER5);

    //timer_oc_parameter_struct timer_ocintpara;
    timer_parameter_struct timer_initpara;

    timer_deinit(TIMER5);

    /* TIMER5 configuration */
    timer_initpara.prescaler         = 0;
    timer_initpara.alignedmode       = TIMER_COUNTER_EDGE;
    timer_initpara.counterdirection  = TIMER_COUNTER_UP;
    timer_initpara.period            = 1080;
    timer_initpara.clockdivision     = TIMER_CKDIV_DIV1;
    timer_initpara.repetitioncounter = 0;
    timer_init(TIMER5,&timer_initpara);

    /* auto-reload preload enable */
    timer_auto_reload_shadow_enable(TIMER5);
    timer_interrupt_enable(TIMER5,TIMER_INT_UP);

    timer_disable( TIMER5 );
}
void vos_DrvTimer_Start()
{
	timer_enable( TIMER5 );
}
void vos_DrvTimer_Stop()
{
	timer_disable( TIMER5 );
}
void TIMER5_IRQHandler()
{
	timer_interrupt_flag_clear(TIMER5,TIMER_INTF_UPIF);

	//触发频率为 100KHz
	remo_tick_run();
}

//========================================================================================

//短延时, 后续应该尽量换成 nop 指令
volatile int32 Timer_i;
void vos_WS2812_Delay_0_4us(void)
{
	Timer_i += 1;
	Timer_i += 1;


	Timer_i += 1;
	Timer_i += 1;
	Timer_i += 1;

	//Timer_i += 1;
	//Timer_i += 1;
}

void vos_WS2812_WriteData( uint8 id, uint32 d )
{
	//GD32V-108MHz
	//F411RE-100MHz
	for( int8 i = 0; i < 24; ++i ) {

		GPIO_BOP(GPION_LIST[id]) = GPIO_LIST[id];
		if( (d & 0x800000) == 0 ) {
			vos_WS2812_Delay_0_4us();
			GPIO_BC(GPION_LIST[id]) = GPIO_LIST[id];
			vos_WS2812_Delay_0_4us();
			vos_WS2812_Delay_0_4us();
			vos_WS2812_Delay_0_4us();
		}
		else {
			vos_WS2812_Delay_0_4us();
			vos_WS2812_Delay_0_4us();
			vos_WS2812_Delay_0_4us();
			GPIO_BC(GPION_LIST[id]) = GPIO_LIST[id];
			vos_WS2812_Delay_0_4us();
		}
		d <<= 1;
	}
}
void vos_UserExtDriver( uint32 v24, uint32 v8 )
{
}






