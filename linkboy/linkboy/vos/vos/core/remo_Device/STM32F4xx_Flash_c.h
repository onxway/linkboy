
void vos_Flash_Load()
{
}

void vos_Flash_Save()
{
}

void vos_Flash_Clear()
{
	FLASH_Unlock();
	
	/* Clear pending flags (if any) */  
	FLASH_ClearFlag(FLASH_FLAG_EOP | FLASH_FLAG_OPERR | FLASH_FLAG_WRPERR | FLASH_FLAG_PGAERR | FLASH_FLAG_PGPERR|FLASH_FLAG_PGSERR); 
	
    /* Device voltage range supposed to be [2.7V to 3.6V], the operation will be done by word */ 
	if (FLASH_EraseSector(USER_SECTOR, VoltageRange_3) != FLASH_COMPLETE) { 
		//...
	}
	FLASH_Lock();
}

void vos_Flash_WriteUint32( uint32 addr, uint32 b )
{
	FLASH_Status FLASHStatus;
	
	FLASH_Unlock();
	
	//if( FLASH_ProgramHalfWord(Flash_USER_ADDR + addr, b) == FLASH_COMPLETE ) {
	
	FLASHStatus = FLASH_ProgramHalfWord( Flash_USER_ADDR + addr, (uint16)b );
	FLASHStatus = FLASH_ProgramHalfWord( Flash_USER_ADDR + addr + 2, (uint16)(b >> 16) );
	
	FLASH_Lock();
}











