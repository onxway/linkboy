

//========================================================================================
void IO_Init(void);
void vos_DrvTimer_Init(void);

//系统初始化
void vos_User_Init(void)
{
	IO_Init();
	vos_DrvTimer_Init();
	SysTick_Config( 0xFFFFFF );
}
//用户启动
void vos_User_Start(void)
{
	vos_USART_Open( 1, 115200 );
}
//开启全局中断
void vos_SYS_InterruptEnable(void)
{
	__enable_irq();
}
//关闭全局中断
void vos_SYS_InterruptDisable(void)
{
	__disable_irq();
}

#include "stm32f10x_usart.h"

//初始化串口1
void remo_USART1_Init(void)
{
	GPIO_InitTypeDef GPIO_InitStructure;
	NVIC_InitTypeDef NVIC_InitStructure;
	
	RCC_APB2PeriphClockCmd(RCC_APB2Periph_USART1 | RCC_APB2Periph_GPIOA, ENABLE); 
	
	//PA9 TXD1
	GPIO_InitStructure.GPIO_Pin = GPIO_Pin_9;
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
  	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_AF_PP;
   	GPIO_Init(GPIOA, &GPIO_InitStructure);
	
	//PA10 RXD1
	GPIO_InitStructure.GPIO_Pin = GPIO_Pin_10;
  	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_IN_FLOATING;
   	GPIO_Init(GPIOA, &GPIO_InitStructure);
	
	//remo_USART_Baudrate( 1, BaudRate );
	
	USART_ITConfig(USART1, USART_IT_RXNE, ENABLE);
	//USART_ITConfig(USART1, USART_IT_TXE, ENABLE);
	USART_Cmd(USART1, ENABLE);
	
	NVIC_PriorityGroupConfig(NVIC_PriorityGroup_0);
	NVIC_InitStructure.NVIC_IRQChannel = USART1_IRQn;
	NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority = 3;
	NVIC_InitStructure.NVIC_IRQChannelSubPriority = 2;
	NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE;
	NVIC_Init(&NVIC_InitStructure);
}
//初始化串口2
void remo_USART2_Init(void)
{
	GPIO_InitTypeDef GPIO_InitStructure;
	NVIC_InitTypeDef NVIC_InitStructure;
	
	RCC_APB2PeriphClockCmd(RCC_APB2Periph_AFIO, ENABLE);
	RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOA, ENABLE);
	RCC_APB1PeriphClockCmd(RCC_APB1Periph_USART2, ENABLE);
	
	//PA2 TXD2
	GPIO_InitStructure.GPIO_Pin = GPIO_Pin_2;
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
  	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_AF_PP;
   	GPIO_Init(GPIOA, &GPIO_InitStructure);
	
	//PA3 RXD2
	GPIO_InitStructure.GPIO_Pin = GPIO_Pin_3;
  	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_IN_FLOATING;
   	GPIO_Init(GPIOA, &GPIO_InitStructure);
	
	//remo_USART_Baudrate( 2, BaudRate );
	
	USART_ITConfig(USART2, USART_IT_RXNE, ENABLE);
	//USART_ITConfig(USART2, USART_IT_TXE, ENABLE);
	USART_Cmd(USART2, ENABLE);
	
	NVIC_PriorityGroupConfig(NVIC_PriorityGroup_0);
	NVIC_InitStructure.NVIC_IRQChannel = USART2_IRQn;
	NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority = 0;
	NVIC_InitStructure.NVIC_IRQChannelSubPriority = 0;
	NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE;
	NVIC_Init(&NVIC_InitStructure);
}
//初始化串口3
void remo_USART3_Init(void)
{
	GPIO_InitTypeDef GPIO_InitStructure;
	NVIC_InitTypeDef NVIC_InitStructure;
	
	RCC_APB2PeriphClockCmd(RCC_APB2Periph_AFIO, ENABLE);
	RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOB, ENABLE);
	RCC_APB1PeriphClockCmd(RCC_APB1Periph_USART3, ENABLE);
	
	//PB10 TXD3
	GPIO_InitStructure.GPIO_Pin = GPIO_Pin_10;
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
  	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_AF_PP;
   	GPIO_Init(GPIOB, &GPIO_InitStructure);
	
	//PB11 RXD3
	GPIO_InitStructure.GPIO_Pin = GPIO_Pin_11;
  	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_IN_FLOATING;
   	GPIO_Init(GPIOB, &GPIO_InitStructure);
	
	//remo_USART_Baudrate( 3, BaudRate );
	
	USART_ITConfig(USART3, USART_IT_RXNE, ENABLE);
	//USART_ITConfig(USART3, USART_IT_TXE, ENABLE);
	USART_Cmd(USART3, ENABLE);
	
	NVIC_PriorityGroupConfig(NVIC_PriorityGroup_0);
	NVIC_InitStructure.NVIC_IRQChannel = USART3_IRQn;
	NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority = 3;
	NVIC_InitStructure.NVIC_IRQChannelSubPriority = 2;
	NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE;
	NVIC_Init(&NVIC_InitStructure);
}

//串口波特率
void vos_USART_Open( int8 ID, uint32 BaudRate )
{
	USART_InitTypeDef USART_InitStructure;
	
    USART_InitStructure.USART_BaudRate = BaudRate;
	USART_InitStructure.USART_WordLength = USART_WordLength_8b;
	USART_InitStructure.USART_StopBits = USART_StopBits_1;
	USART_InitStructure.USART_Parity = USART_Parity_No;
	USART_InitStructure.USART_HardwareFlowControl = USART_HardwareFlowControl_None;
	USART_InitStructure.USART_Mode = USART_Mode_Rx | USART_Mode_Tx;
	
	if( ID == 1 ) {
		remo_USART1_Init();
		USART_Init(USART1, &USART_InitStructure);
	}
	if( ID == 2 ) {
		remo_USART2_Init();
		USART_Init(USART2, &USART_InitStructure);
	}
	if( ID == 3 ) {
		remo_USART3_Init();
		USART_Init(USART3, &USART_InitStructure);
	}
	
}
//发送字节
void vos_USART_WriteByte( int8 ID, uint8 tempc )
{
	/*
	USART2->DR = tempc;
	while(USART_GetFlagStatus(USART2, USART_FLAG_TC) == RESET);
	*/
	//while(USART_GetFlagStatus(USART1,USART_FLAG_TC)!=SET);
	
	if( ID == 1 ) {
		USART_SendData(USART1, tempc);
		while( USART_GetFlagStatus(USART1, USART_FLAG_TC) == RESET );
	}
	if( ID == 2 ) {
		USART_SendData(USART2, tempc);
		while( USART_GetFlagStatus(USART2, USART_FLAG_TC) == RESET );
	}
	if( ID == 3 ) {
		USART_SendData(USART3, tempc);
		while( USART_GetFlagStatus(USART3, USART_FLAG_TC) == RESET );
	}
}

//--------------------------------------------------
//串口1中断
void USART1_IRQHandler(void)
{
	if(USART_GetITStatus(USART1, USART_IT_RXNE)== SET)
	{
		uint8 Data = USART_ReceiveData(USART1);
		USART_ClearFlag(USART1,USART_FLAG_TC);
		
		Deal( Data );
		
		//存储到对应的缓冲区
		UART_List_Receive( 1, Data );
    }
}
//--------------------------------------------------
//串口2中断
void USART2_IRQHandler(void)
{
	if(USART_GetITStatus(USART2, USART_IT_RXNE)== SET)
	{
        uint8 Data = USART_ReceiveData(USART2);
		USART_ClearFlag(USART2,USART_FLAG_TC);
		
		//Deal( Data );
		
		//存储到对应的缓冲区
		UART_List_Receive( 2, Data );
    }
}
//--------------------------------------------------
//串口3中断
void USART3_IRQHandler(void)
{
	if(USART_GetITStatus(USART3, USART_IT_RXNE)== SET)
	{
        uint8 Data = USART_ReceiveData(USART3);
		USART_ClearFlag(USART3,USART_FLAG_TC);
		
		//Deal( Data );
		
		//存储到对应的缓冲区
		UART_List_Receive( 3, Data );
    }
}

//====================================================================================

//此函数删除后可能导致下载模式无法工作, ...
void SysTick_Handler(void)
{
	//ntime--;
}

uint32 vos_Timer_Tick0()
{
	return ~SysTick->VAL;
}
uint32 vos_Timer_Tick0MS()
{
	return 72000;
}
uint32 vos_Timer_Tick1()
{
	return ~SysTick->VAL;
}
uint32 vos_Timer_Tick1US()
{
	return 72;
}

//====================================================================================
#include "stm32f10x_adc.h"
#include "stm32f10x_tim.h"

//112在有些STM32F103C8T6上有问题
//#define NUMBER 48//112

uint16_t GPIO_LIST[NUMBER];
GPIO_TypeDef *(GPION_LIST[NUMBER]);

GPIO_InitTypeDef GPIO_InitStructure;

void adc_init(void);
void ADC_Reset(ADC_TypeDef* ADCx);

//PWM输出初始化
//arr：自动重装值
//psc：时钟预分频数
void TIM1_PWM_Init( uint16 arr,uint16 psc )
{
	GPIO_InitTypeDef GPIO_InitStructure;
	TIM_TimeBaseInitTypeDef  TIM_TimeBaseStructure;
	TIM_OCInitTypeDef  TIM_OCInitStructure;

	RCC_APB2PeriphClockCmd(RCC_APB2Periph_TIM1, ENABLE);// 使能TIM1时钟
 	RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOA , ENABLE);  //使能GPIO外设时钟使能
	
	//设置该引脚为复用输出功能,输出TIM1 CH1的PWM脉冲波形
	GPIO_InitStructure.GPIO_Pin = GPIO_Pin_8; //TIM_CH1
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_AF_PP;  //复用推挽输出
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
	GPIO_Init(GPIOA, &GPIO_InitStructure);

	TIM_TimeBaseStructure.TIM_Period = arr; //设置在下一个更新事件装入活动的自动重装载寄存器周期的值 80K
	TIM_TimeBaseStructure.TIM_Prescaler =psc; //设置用来作为TIMx时钟频率除数的预分频值  不分频
	TIM_TimeBaseStructure.TIM_ClockDivision = 0; //设置时钟分割:TDTS = Tck_tim
	TIM_TimeBaseStructure.TIM_CounterMode = TIM_CounterMode_Up;  //TIM向上计数模式
	TIM_TimeBaseInit(TIM1, &TIM_TimeBaseStructure); //根据TIM_TimeBaseInitStruct中指定的参数初始化TIMx的时间基数单位

	TIM_OCInitStructure.TIM_OCMode = TIM_OCMode_PWM2; //选择定时器模式:TIM脉冲宽度调制模式2
	TIM_OCInitStructure.TIM_OutputState = TIM_OutputState_Enable; //比较输出使能
	TIM_OCInitStructure.TIM_Pulse = 0; //设置待装入捕获比较寄存器的脉冲值
	TIM_OCInitStructure.TIM_OCPolarity = TIM_OCPolarity_High; //输出极性:TIM输出比较极性高
	TIM_OC1Init(TIM1, &TIM_OCInitStructure);  //根据TIM_OCInitStruct中指定的参数初始化外设TIMx
	TIM_CtrlPWMOutputs(TIM1,ENABLE);	//MOE 主输出使能
	TIM_OC1PreloadConfig(TIM1, TIM_OCPreload_Enable);  //CH1预装载使能	
	TIM_ARRPreloadConfig(TIM1, ENABLE); //使能TIMx在ARR上的预装载寄存器
	TIM_Cmd(TIM1, ENABLE);  //使能TIM1
}

void IO_Init()
{
	uint8 i;
	uint16 pin;
	GPIO_TypeDef *(GPIONAME_LIST[7]);
	
	RCC_APB2PeriphClockCmd( RCC_APB2Periph_GPIOA, ENABLE);
	RCC_APB2PeriphClockCmd( RCC_APB2Periph_GPIOB, ENABLE);
	RCC_APB2PeriphClockCmd( RCC_APB2Periph_GPIOC, ENABLE);
	RCC_APB2PeriphClockCmd( RCC_APB2Periph_GPIOD, ENABLE);
	RCC_APB2PeriphClockCmd( RCC_APB2Periph_GPIOE, ENABLE);
	RCC_APB2PeriphClockCmd( RCC_APB2Periph_GPIOF, ENABLE);
	RCC_APB2PeriphClockCmd( RCC_APB2Periph_GPIOG, ENABLE);
	
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_IPU; //GPIO_Mode_Out_PP;
	
	GPIONAME_LIST[0] = GPIOA;
	GPIONAME_LIST[1] = GPIOB;
	GPIONAME_LIST[2] = GPIOC;
	GPIONAME_LIST[3] = GPIOD;
	GPIONAME_LIST[4] = GPIOE;
	GPIONAME_LIST[5] = GPIOF;
	GPIONAME_LIST[6] = GPIOG;
	
	pin = 1;
	for( i = 0; i < NUMBER; i++ ) {
		
		//设置引脚
		GPIO_LIST[i] = pin;
		pin <<= 1;
		if( pin == 0 ) {
			pin = 1;
		}
		//设置寄存器
		GPION_LIST[i] = GPIONAME_LIST[i/16];
	}
	
	for( i = 0; i < NUMBER; ++i ) {
		GPIO_InitStructure.GPIO_Pin = GPIO_LIST[i];
		GPIO_Init(GPION_LIST[i],&GPIO_InitStructure);
	}
	
	//需要禁用JTAG 否则 A15 B3 B4 不起作用
	RCC_APB2PeriphClockCmd(RCC_APB2Periph_AFIO, ENABLE);
	GPIO_PinRemapConfig(GPIO_Remap_SWJ_JTAGDisable, ENABLE);
	
	//GPIO_PinRemapConfig(GPIO_Remap_SWJ_Disable, ENABLE); //仅用于特殊情况!  完全禁用SWD及JTAG
	
	ADC_DeInit(ADC1);
	adc_init();
	TIM1_PWM_Init( 1000, 0 );
	
	TIM_SetCompare1(TIM1, 200 );
}

void adc_init(void)
{
	/*
	ADC_InitTypeDef ADC_InitStructure;
	//结构体_ADC基础-声明
	GPIO_InitTypeDef GPIO_InitStructure;
	//结构体_引脚基础-声明
	RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOA | RCC_APB2Periph_ADC1, ENABLE);
	//时钟开启_GPIOA,ADC1
	
	GPIO_InitStructure.GPIO_Pin = GPIO_Pin_1;
	//结构体_引脚-PA1
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
	//结构体_引脚-引脚频率_50Mhz
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_AIN;
	//结构体_引脚-引脚模式_模拟输入
	GPIO_Init(GPIOA, &GPIO_InitStructure);
	*/
	
	ADC_InitTypeDef ADC_InitStructure;
	RCC_APB2PeriphClockCmd(RCC_APB2Periph_ADC1, ENABLE);
	
	//结构体_引脚_结束配置
	ADC_InitStructure.ADC_Mode = ADC_Mode_Independent;
	//结构体_ADC-总模式_独立模式
	ADC_InitStructure.ADC_ScanConvMode = DISABLE;
	//结构体_ADC-是否扫描_单通道
	ADC_InitStructure.ADC_ContinuousConvMode = DISABLE;
	//结构体_ADC-是否连续_单次
	ADC_InitStructure.ADC_ExternalTrigConv = ADC_ExternalTrigConv_None;
	//结构体_ADC-触发方式_软件触发
	ADC_InitStructure.ADC_DataAlign = ADC_DataAlign_Right;
	//结构体_ADC-对齐方式_右对齐
	ADC_InitStructure.ADC_NbrOfChannel = 1;
	//结构体_ADC-通道个数_单通道
	ADC_Init(ADC1, &ADC_InitStructure);
	//结构体_ADC_结束配置
	
	//RCC_ADCCLKConfig(RCC_PCLK2_Div2);
	//ADC1-时钟分频配置       PS:原始输入的ADC输入时钟是1/2的系统时钟
	ADC_RegularChannelConfig(ADC1, ADC_Channel_0, 1, ADC_SampleTime_1Cycles5);
	//配置内容和ADC外设的具体对接函数   (ADC端口 ,ADC通道 ,转换序号-第几个转换 ,转换的周期)
	ADC_Cmd(ADC1,ENABLE);
	//开关_ADC-总开关
 	ADC_Reset(ADC1);
	//ADC校准函数
}
void ADC_Reset(ADC_TypeDef* ADCx)
{
	ADC_ResetCalibration(ADCx);
	//复位校准寄存器             寄存器置1
	while(ADC_GetResetCalibrationStatus(ADCx));
	//等待校准寄存器复位完成      寄存器置0
	ADC_StartCalibration(ADCx);
	//ADC校准                    寄存器置1
	while(ADC_GetCalibrationStatus(ADCx));
	//等待校准完成                寄存器置0
}
int32 IO_AD_Read(ADC_TypeDef* ADCx, uint8 i)
{
	//ADC_RegularChannelConfig(ADC1, ADC_Channel_1, 1, ADC_SampleTime_1Cycles5);
	ADC_RegularChannelConfig(ADC1, i, 1, ADC_SampleTime_1Cycles5);
	
	ADC_SoftwareStartConvCmd(ADCx,ENABLE);
	//开关_ADC软件触发-开关 状态寄存器为0
	while(!ADC_GetFlagStatus(ADCx,ADC_FLAG_EOC));
	//等待转换结束 寄存器置1
	return ADC_GetConversionValue(ADCx);
	//因为读取了数据寄存器，状态寄存器自动清0
}

void IO_DirWrite( uint8 i, uint8 d )
{
	if( d == 0 ) {
		GPIO_InitStructure.GPIO_Mode = GPIO_Mode_IPU; //下拉为GPIO_Mode_IPD
	}
	else {
		GPIO_InitStructure.GPIO_Mode = GPIO_Mode_Out_PP;
	}
	GPIO_InitStructure.GPIO_Pin = GPIO_LIST[i];
	GPIO_Init( GPION_LIST[i], &GPIO_InitStructure );
}
void IO_OutWrite( uint8 i, uint8 d )
{
	if( d == 0 ) {
		//GPIO_ResetBits(GPION_LIST[i],GPIO_LIST[i]);
		GPION_LIST[i]->BRR = GPIO_LIST[i];
	}
	else {
		//GPIO_SetBits(GPION_LIST[i],GPIO_LIST[i]);
		GPION_LIST[i]->BSRR = GPIO_LIST[i];
	}
}
uint8 IO_OutRead( uint8 i )
{
	return GPIO_ReadOutputDataBit(GPION_LIST[i], GPIO_LIST[i]);
}
uint8 IO_InRead( uint8 i )
{
	return GPIO_ReadInputDataBit(GPION_LIST[i], GPIO_LIST[i]);
}
void vos_IO_AnalogOpen( uint8 i, uint8 d )
{
	GPIO_InitStructure.GPIO_Pin = GPIO_LIST[i];
	//结构体_引脚-PA1
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
	//结构体_引脚-引脚频率_50Mhz
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_AIN;
	//结构体_引脚-引脚模式_模拟输入
	GPIO_Init(GPION_LIST[i], &GPIO_InitStructure);
}
int32 vos_IO_AnalogRead( uint8 i )
{
	int32 d;
	//这里为 PB0 - PB1
	if( i >= 16 && i <= 17 ) {
		i -= 8;
	}
	//这里为 PC0 - PC5
	else if( i >= 32 && i <= 37 ) {
		i -= 22;
	}
	//这里为 PA0 - PA7
	else {
		//...
	}
	d = IO_AD_Read(ADC1, i);
	return d / 4;
}
void vos_IO_PWM_SetPin( uint8 clk )
{
	
}

void vos_IO_PWM_SetData( uint8 clk, int32 data, int32 max )
{
	
}

//====================================================================================


#include "stm32f10x_flash.h"

/*
#define PageSize 1024
//#define StartPage 50  //12??64  14K
//#define PageNumber 14

#define StartPage 17
#define PageNumber 47
*/

void vos_Flash_Load()
{
}

void vos_Flash_Save()
{
}

//获取Flash尺寸, 单位是KB
int32 Flash_GetSize_old()
{
	return PageNumber * PageSize / 1024;
}
void vos_Flash_Clear()
{
	uint8 i;
	FLASH_Status FLASHStatus;
	
	FLASH_Unlock();
	FLASHStatus = FLASH_COMPLETE;
	for( i = 0; i < PageNumber; i++ ) { 
		FLASHStatus = FLASH_ErasePage( Flash_USER_ADDR + PageSize * i );
		if( FLASHStatus != FLASH_COMPLETE ) {
			
		}
	}
	FLASH_Lock();
}
void vos_Flash_WriteUint32( uint32 addr, uint32 b )
{
	FLASH_Status FLASHStatus;
	FLASH_Unlock();
	FLASHStatus = FLASH_ProgramHalfWord( Flash_USER_ADDR + addr, (uint16)b );
	FLASHStatus = FLASH_ProgramHalfWord( Flash_USER_ADDR + addr + 2, (uint16)(b >> 16) );
	if( FLASHStatus ) {
		//...
	}
	FLASH_Lock();
}


//========================================================================================

#include "STM32f10x_tim.h"

//初始化定时器
void vos_DrvTimer_Init()
{
    TIM_TimeBaseInitTypeDef TIM_TimeBaseInitStruct;
    NVIC_InitTypeDef NVIC_InitStruct;
    
    TIM_TimeBaseInitStruct.TIM_CounterMode = TIM_CounterMode_Down;
	
	//1000ms
    //TIM_TimeBaseInitStruct.TIM_Period = 10000-1;
    //TIM_TimeBaseInitStruct.TIM_Prescaler = 7200-1;
	
	//10us
	TIM_TimeBaseInitStruct.TIM_Period = 720-1;
	TIM_TimeBaseInitStruct.TIM_Prescaler = 0;

    RCC_APB1PeriphClockCmd(RCC_APB1Periph_TIM2, ENABLE);

    TIM_TimeBaseInit( TIM2, & TIM_TimeBaseInitStruct);
    TIM_ITConfig(TIM2,TIM_IT_Update, ENABLE);
    TIM_ClearITPendingBit( TIM2,TIM_IT_Update);
	
	 NVIC_InitStruct.NVIC_IRQChannel = TIM2_IRQn;
    NVIC_InitStruct.NVIC_IRQChannelCmd = ENABLE;
    NVIC_Init( &NVIC_InitStruct);
    TIM_Cmd( TIM2, DISABLE);
}
//开启定时器
void vos_DrvTimer_Start()
{
	TIM_Cmd( TIM2, ENABLE);
}
//停止定时器
void vos_DrvTimer_Stop()
{
	TIM_Cmd( TIM2, DISABLE);
}
void TIM2_IRQHandler(void)
{
	if(RESET != TIM_GetITStatus( TIM2,TIM_IT_Update)) {
		TIM_ClearITPendingBit( TIM2,TIM_IT_Update);
		
		//触发频率为 100KHz
		remo_tick_run();
	}
}

//=================================================================================

//短延时, 后续应该尽量换成 nop 指令
volatile int32 Timer_i;
void vos_WS2812_Delay_0_4us(void)
{
	//GD32V
	Timer_i += 1;
	//Timer_i += 1;
	
	/*
	__nop();
	__nop();
	__nop();
	__nop();
	__nop();
	__nop();
	__nop();
	__nop();
	*/
}

void vos_WS2812_WriteData( uint8 id, uint32 d )
{
	int8 i;
	//用于STM32F103-72MHz
	for( i = 0; i < 24; ++i ) {
		//IO_OutWrite_1( id );
		GPION_LIST[id]->BSRR = GPIO_LIST[i];
		if( (d & 0x800000) == 0 ) {
			//IO_OutWrite_0( id );
			GPION_LIST[id]->BRR = GPIO_LIST[i];
			vos_WS2812_Delay_0_4us();
		}
		else {
			vos_WS2812_Delay_0_4us();
			//IO_OutWrite_0( id );
			GPION_LIST[id]->BRR = GPIO_LIST[i];
		}
		d <<= 1;
	}
}

void vos_UserExtDriver( uint32 v24, uint32 v8 )
{
	
}










