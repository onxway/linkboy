
//<<VOS_CONFIG>>

#include "../core/vos.h"
#include "../core/common_c.h"

//注意: 目前版本的vos, 用户NDK接口最多支持201个本地函数封装
//函数序号v的范围: 0 - 200

//<<VOS_NDK_HAED>>

void vos_NDK( uint8 func_index )
{

//<<VOS_NDK_VAR>>

	switch( func_index ) {
		
//<<VOS_NDK>>
		
		default:
			Error( E_VM_NativeError, func_index );
			vos_Running = false;
		break;
	}
}

#define VOS_DEV_VERSION '<<VOS_VER>>'
//CC指电路仿真引擎, 0表示不包含, 其他数值表示包含
const uint8 VOS_DEV_MESSAGE[] = "CPU:<<VOS_CPU>>,FREQ:default,CC:0";

//-------------------------------------

//下载协议串口: -1表示无串口或者虚拟串口, 0 - N 表示实际串口
#define UART_DEFAULT_ID <<VOS_UART_DID>>

#include "../core/main_c.h"

