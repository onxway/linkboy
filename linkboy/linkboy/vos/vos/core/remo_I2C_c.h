
void remo_I2C_delay(void);
void remo_I2C_wait(void);
void remo_I2C_start_iic(void);
void remo_I2C_end_iic(void);
uint8 remo_I2C_read_byte(void);
void remo_I2C_write_byte( uint8 data8 );
void remo_I2C_Ack(void);
void remo_I2C_NoAck(void);

//---------------------------------------------------------

#define remo_I2C_delay() 

//如果是驱动OV摄像头, 需要替换成此函数
void remo_I2C_delay_OV(void)
{
	SoftDelay_1us( 5 );
}

//---------------------------------------------------
//向一个8位地址写入一个字节
void A1_set_uint8( uint8 DeviceAddr, uint8 addr, uint8 data )
{
	remo_I2C_start_iic();
	remo_I2C_write_byte( DeviceAddr );
	remo_I2C_write_byte( addr );
	remo_I2C_write_byte( data );
	remo_I2C_end_iic();
}
//从一个8位地址读取一个字节
uint8 A1_get_uint8( uint8 DeviceAddr, uint8 addr )
{
	uint8 data = 0;
	
	remo_I2C_start_iic();
	remo_I2C_write_byte( DeviceAddr );
	remo_I2C_write_byte( addr );
	
	//remo_I2C_end_iic();//OV7670需要增加这个
	//remo_I2C_delay();
	
	remo_I2C_start_iic();
	remo_I2C_write_byte( DeviceAddr | 0x01 );
	data = remo_I2C_read_byte();
	remo_I2C_NoAck();
	remo_I2C_end_iic();
	return data;
}
//从一个8位地址读取一个字节, 包含完整的结束操作, 用于 OV7670等
uint8 A1_get_uint8_full( uint8 DeviceAddr, uint8 addr )
{
	uint8 data = 0;
	
	remo_I2C_start_iic();
	remo_I2C_write_byte( DeviceAddr );
	remo_I2C_write_byte( addr );
	
	remo_I2C_end_iic(); //OV7670需要增加这个
	remo_I2C_delay();
	
	remo_I2C_start_iic();
	remo_I2C_write_byte( DeviceAddr | 0x01 );
	data = remo_I2C_read_byte();
	remo_I2C_NoAck();
	remo_I2C_end_iic();
	return data;
}


//---------------------------------------------------------

uint8 vos_I2C_SCL;
uint8 vos_I2C_SDA;

void remo_I2C_SetPin( uint8 v_scl, uint8 v_sda )
{
	vos_I2C_SCL = v_scl;
	vos_I2C_SDA = v_sda;
}

void remo_I2C_Init()
{
	IO_DirWrite( vos_I2C_SCL, 1 );
	IO_DirWrite( vos_I2C_SDA, 1 );
	
	IO_OutWrite( vos_I2C_SCL, 1 );
	IO_OutWrite( vos_I2C_SDA, 1 );
	
	remo_I2C_start_iic();
	remo_I2C_end_iic();
	
	SoftDelay_ms( 50 );
}

//开始IIC通信
void remo_I2C_start_iic()
{
	IO_OutWrite( vos_I2C_SDA, 1 );
	IO_OutWrite( vos_I2C_SCL, 1 );
	remo_I2C_delay();
	IO_OutWrite( vos_I2C_SDA, 0 );
	remo_I2C_delay();
	IO_OutWrite( vos_I2C_SCL, 0 );
	remo_I2C_delay();
}
//结束IIC通信
void remo_I2C_end_iic()
{
	IO_OutWrite( vos_I2C_SDA, 0 );
	IO_OutWrite( vos_I2C_SCL, 1 );
	remo_I2C_delay();
	IO_OutWrite( vos_I2C_SDA, 1 );
	remo_I2C_delay();
}
//发送应答
void remo_I2C_Ack()
{
	remo_I2C_delay();
	IO_OutWrite( vos_I2C_SDA, 0 );		//应答
	remo_I2C_delay();
	IO_OutWrite( vos_I2C_SCL, 1 );
	remo_I2C_delay();
	IO_OutWrite( vos_I2C_SCL, 0 );
	remo_I2C_delay();
	IO_OutWrite( vos_I2C_SDA, 0 );
	remo_I2C_delay();
}
//发送非应答
void remo_I2C_NoAck()
{
	remo_I2C_delay();
	IO_OutWrite( vos_I2C_SDA, 1 );		//非应答
	remo_I2C_delay();
	IO_OutWrite( vos_I2C_SCL, 1 );
	remo_I2C_delay();
	IO_OutWrite( vos_I2C_SCL, 0 );
	remo_I2C_delay();
	IO_OutWrite( vos_I2C_SDA, 0 );
	remo_I2C_delay();
}
//等待应答信号
void remo_I2C_wait()
{
	uint32 Wait_tick = 0;
	remo_I2C_delay();
	IO_DirWrite( vos_I2C_SDA, 0 );
	remo_I2C_delay();
	IO_OutWrite( vos_I2C_SCL, 1 );
	SoftDelay_1us( 50 ); //很关键
	
	while( IO_InRead( vos_I2C_SDA ) == 1 && Wait_tick < 1000000 ) { Wait_tick++; }
	IO_OutWrite( vos_I2C_SCL, 0 );
	remo_I2C_delay();
	IO_DirWrite( vos_I2C_SDA, 1 );
	remo_I2C_delay();
}
//写一个字节
void remo_I2C_write_byte( uint8 data8 )
{
	uint8 i;
	for( i = 0; i < 8; ++i ) {
		IO_OutWrite( vos_I2C_SDA, data8 & 0x80 );
		data8 <<= 1;
		remo_I2C_delay();
		IO_OutWrite( vos_I2C_SCL, 1 );
		remo_I2C_delay();
		IO_OutWrite( vos_I2C_SCL, 0 );
		remo_I2C_delay();
	}
	remo_I2C_wait();
}
//读一个字节
uint8 remo_I2C_read_byte(void)
{
	uint8 i;
	uint8 data = 0;
	
	IO_DirWrite( vos_I2C_SDA, 0 );
	remo_I2C_delay();
	for( i = 0; i < 8; ++i ) {
		data <<= 1;
		if( IO_InRead( vos_I2C_SDA ) == 1 ) {
			data |= 1;
		}
		IO_OutWrite( vos_I2C_SCL, 1 );
		remo_I2C_delay();
		IO_OutWrite( vos_I2C_SCL, 0 );
		remo_I2C_delay();
	}
	IO_DirWrite( vos_I2C_SDA, 1 );
	remo_I2C_delay();
	return data;
}









