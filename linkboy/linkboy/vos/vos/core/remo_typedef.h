
#ifndef _remo_typedef_h_
#define _remo_typedef_h_

#define true 1
#define false 0

#define bool unsigned char

#define uint8 unsigned char
#define int8 signed char
#define uint16 unsigned short
#define int16 signed short

//-----------------------------------------
#ifdef VOS_8Bit
	//以下适用于8位处理器
	#define uint32 unsigned long
	#define int32 signed long
#else
	//以下适用于32位处理器
	#define uint32 unsigned int
	#define int32 signed int
#endif
//-----------------------------------------

#define int64 long long
#define fix float

/*
typedef unsigned char uint8;
typedef signed char int8;

typedef unsigned short uint16;
typedef short int16;

typedef unsigned int uint32;
typedef int int32;
*/

#endif


