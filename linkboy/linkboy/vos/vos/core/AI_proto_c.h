
//========================================================
//AI命令信息

#define VM_AI 0xE0	//AI命令

uint8 RBuffer[10];

int32 v_MouseX;
int32 v_MouseY;
uint8 v_MouseButton;

int32 tttt = 0;

void AI_Init()
{
	v_MouseX = 0;
	v_MouseY = 0;
	v_MouseButton = 0;
}
void AI_Cmd( uint32 data )
{
	RBuffer[Pro_Cnum - 1] = data;

	//这里表示刚接收到协议命令类型, 一般直接忽略
	if( Pro_Cnum < Pro_Length + 3 ) {
		return;
	}
	Pro_Cnum = 0;

	uint8 AI_CMD = RBuffer[4];
	uint8 AI_D0 = RBuffer[5];
	uint8 AI_D1 = RBuffer[6];
	uint8 AI_D2 = RBuffer[7];
	uint8 AI_D3 = RBuffer[8];
	uint8 AI_SUM = RBuffer[9];
	
	//串口打开和关闭命令
	if( AI_CMD == 1 ) {
		if( AI_D0 == 0 ) {
			//STOP = false;
		}
		if( AI_D0 == 1 ) {
			//SendGVar();
			//EI_UserSelColor = EI_RED;
		}
		return;
	}
	//校验和
	if( (uint8)(AI_CMD + AI_D0 + AI_D1 + AI_D2 + AI_D3) % 256 != AI_SUM ) {
		return;
	}

	if( AI_CMD == 2 ) {
		uint16 mx = AI_D1; mx <<= 8;
		mx += AI_D0;
		uint16 my = AI_D3; my <<= 8;
		my += AI_D2;
		v_MouseX = (int)mx;
		v_MouseY = (int)my;
		return;
	}
	if( AI_CMD == 3 ) {
		v_MouseButton = AI_D0;
		return;
	}
	if( AI_CMD == 4 ) {
		//鼠标滚轮
		return;
	}
	//设置标志位
	if( AI_CMD >= 0x10 ) {
		//AI_CMD -= 0x10;
		//GVarList[16 + AI_CMD + 0] = AI_D0;
		//GVarList[16 + AI_CMD + 1] = AI_D1;
		//GVarList[16 + AI_CMD + 2] = AI_D2;
		//GVarList[16 + AI_CMD + 3] = AI_D3;
		return;
	}
}



