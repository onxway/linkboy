
#ifdef VOS_ArduinoAVR
const PROGMEM uint32 vos_CodeList[] = {
#else
const uint32 vos_CodeList[] = {
#endif

#ifndef VOS_FLASH
//'l', 'i', 'n', 'k', 'b', 'o', 'y', ':',
0x6B6E696C, 0x3A796F62,
#include "../user/app.h"
#endif

0
};
static uint8* vos_pCode;
//volatile uint32 vos_temp;

//============================================
#if (defined VOS_FLASH) && (!defined VOS_IN_ROM)

uint8 vos_CODE_RAM[VOS_SYS_ROM_LENGTH]  __attribute__((aligned(32)));

void vos_SpaceClear(void)
{
	uint32 i;
	for( i = 0; i < VOS_SYS_ROM_LENGTH; i++ ) {
		vos_CODE_RAM[i] = 0x00;
	}
}
void vos_RAM_WriteUint32( uint32 addr, uint32 b )
{
	vos_CODE_RAM[addr+0] = b & 0xFF; b >>= 8;
	vos_CODE_RAM[addr+1] = b & 0xFF; b >>= 8;
	vos_CODE_RAM[addr+2] = b & 0xFF; b >>= 8;
	vos_CODE_RAM[addr+3] = b & 0xFF;
}
#endif

//获取Flash尺寸, 单位是KB
int32 Flash_GetSize()
{
	return VOS_SYS_ROM_LENGTH;
}

void vos_PSpace_Init(void)
{
	//vos_temp = vos_CodeFlag[vos_temp];
	
	#ifdef VOS_FLASH
		#ifndef VOS_IN_ROM
			vos_SpaceClear();
			vos_pCode = (uint8*)vos_CODE_RAM;
		#else
			vos_pCode = (uint8*)Flash_USER_ADDR;
		#endif
	#else
		vos_pCode = (uint8*)(vos_CodeList + 2);
	#endif
}

//============================================
#ifdef VOS_ArduinoAVR
uint8 Flash_ReadByte( uint32 addr )
{
	return pgm_read_byte(vos_pCode + addr);
}
uint32 Flash_ReadUint32( uint32 addr )
{
	return pgm_read_dword(vos_pCode + addr);
}
#else
uint8 Flash_ReadByte( uint32 addr )
{
	return *(uint8*)(vos_pCode + addr);
}
uint32 Flash_ReadUint32( uint32 addr )
{
	return *(uint32*)(vos_pCode + addr);
}
#endif
//============================================

uint8* Flash_GetAddr( uint32 addr )
{
	return (uint8*)(vos_pCode + addr);
}




