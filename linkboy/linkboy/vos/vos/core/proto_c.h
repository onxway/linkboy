
#ifndef VOS_FLASH

void Deal( uint8 d ) {}
void Proto_Init( void ) {}
void Error( uint32 ET, uint32 EC ) {}
void Debug( uint32 data32 ) {}

#else

void Deal( uint8 d );
void SendData( int32 d );
void Proto_Init( void );

void RunIns( uint8 d );
//void SendData( int32 d );

void SetAddr( uint8 d );
void Read( uint8 d );
void Store( uint8 d );
void Check( uint8 d );
void Version( uint8 d );
void Error( uint32 ET, uint32 EC );
void Debug( uint32 data32 );

//----------------------------------------------------------------------------------
//协议解析器

uint32 Pro_Cnum;
uint8 Pro_VM_status;
uint16 Pro_Length;

bool Pro_downloading;

#define VM_HEAD 0xAA
#define VM_SETADDR 0xF0	//设置指令地址
#define VM_STORE 0xF1	//存储指令
#define VM_READ 0xF2	//读取指令
#define VM_CHECK 0xF3	//读取校验和

#define VM_DEBUG 0xFD	//调试信息
#define VM_ERROR 0xFE	//协议错误
#define VM_VERSION 0xFF	//读取版本信息
	//#define VM_VERSION_STOP 0
	//#define VM_VERSION_CONTINUE 1

//====版本记录====

//A.B: 0.0 创世版 - 2019.5.8
//A.B: 1.0 2020.2.27 完善了大部分的硬件接口相关模块
//A.B: 1.9内测版 完成了点阵屏幕和彩屏的一些加速器

//A.B: 2.0 改名为 vos，增加了基础串口
//A.B: 2.1 增加了电路仿真引擎
//A.B: 2.30 (2021.6.7) 增加了switch语句字节码
//A.B: 2.031 (2021.6.22) 完善了步进驱动方波模式; 调整了版本号机制, 增加为3位数字
//A.B: 2.032 (2021.7.28) 增加了PS2摇杆模拟量，改善了稳定性
//A.B: 2.40 (2021.10.15) 升级为单文件模式, RAM和ROM改为字节单位
//A.B: 2.41 (2021.12.6) 修复了ROM超出64K的程序无法执行的问题 (下载时候uint16地址溢出回卷)
//A.B: 2.42 (2022.5.15) 增加了多路马达驱动器的支持, 可以创建多个6612等模块
//A.B: 2.50 (2022.9.9) 增加了fix<-int8/16, 增加了自增自减指令
//A.B: 2.51 (2022.11.18) 增加了ST77989的区域设置指令
//A.B: 2.60 (2022.12.3) 增加了指令压缩解码功能

const uint8 VM_VERSION_A = 2;
//注意: 以下子版本号一律从10开始! 防止混淆, 如 v3和v13 第一眼看上去 v3 版本更大一些
const uint8 VM_VERSION_B = 60;

#include "AI_proto_c.h"

/*
//已过时
#define ULEN 200
uint8 UBuffer[ULEN];
uint8 ULength;

//获取一个串口数据
int16 Proto_GetData( uint8 id )
{
	int16 d = -1;
	if( ULength > 0 ) {
		remo_SYS_interrupt_disable();
		ULength--;
		d = UBuffer[ULength];
		remo_SYS_interrupt_enable();
	}
	return d;
}
	if( ULength < ULEN ) {
		UBuffer[ULength] = d;
		ULength++;
	}
*/


//处理数据
void Deal( uint8 d )
{
	Pro_Cnum++;
	//判断是否为合法数据头部
	if( Pro_Cnum == 1 ) {
		if( d != VM_HEAD ) {
			//... 报错
			Pro_Cnum = 0;
		}
		return;
	}
	//判断是否为数据长度低位
	if( Pro_Cnum == 2 ) {
		Pro_Length = d;
		return;
	}
	//判断是否为数据长度高位
	if( Pro_Cnum == 3 ) {
		Pro_Length += d * 256;
		return;
	}
	//如果是运行指令状态, 接收一个指令并解码运行
	if( Pro_Cnum == 4 ) {
		Pro_VM_status = d;
	}
	//关闭虚拟机, 防止共同占用数据通道造成异常
	//Running = false;
	
	
	//协议锁机制 - 只有先发送版本命令才会解锁
	if( vos_Running && (Pro_VM_status != VM_VERSION || Pro_Length != 1) && Pro_VM_status != VM_AI ) {
		Pro_Cnum = 0;
		return;
	}
	
	//如果是设置指令地址
	switch( Pro_VM_status ) {
		case VM_SETADDR:	SetAddr( d ); break;
		case VM_STORE:		Store( d ); break;
		case VM_READ:		Read( d ); break;
		case VM_CHECK:		Check( d ); break;
		
		case VM_AI:		AI_Cmd( d ); break;
		
		case VM_VERSION:	Version( d ); break;
		default:			Error( E_ProError, Pro_VM_status ); Pro_Cnum = 0; break;
	}
}
//==================================================================================
//指令接收器

uint8 CheckSum;

void Proto_Init()
{
	Pro_Cnum = 0;
	Pro_VM_status = 0;
	CheckSum = 0;
	
	Pro_downloading = false;
	
	AI_Init();
}
//--------------------------------------------

uint32 InsDisc_CurrentIndex;

void CODE_Start()
{
	InsDisc_CurrentIndex = 0;
	vos_Flash_Clear();
}
void CODE_AddInsPage( uint8 *ins, uint32 Cnum )
{
	uint32 i;
	uint32 d;
	uint32 d1;
	uint32 d2;
	uint32 d3;
	for( i = 0; i < Cnum; i += 4 ) {
		d = *ins;
		++ins;
		d1 = *ins;
		++ins;
		d2 = *ins;
		++ins;
		d3 = *ins;
		++ins;
		d += ((d1 << 8) + (d2 << 16) + (d3 << 24));
		
		#ifdef VOS_IN_ROM
			vos_Flash_WriteUint32( InsDisc_CurrentIndex, d );
		#else
			vos_RAM_WriteUint32( InsDisc_CurrentIndex, d );
		#endif
		
		InsDisc_CurrentIndex += 4;
	}
}
//--------------------------------------------
//接收地址并设置
uint16 SetAddr_Addr;
void SetAddr( uint8 d )
{
	if( Pro_Cnum == 4 ) {
		return;
	}
	if( Pro_Cnum == 5 ) {
		SetAddr_Addr = d;
		return;
	}
	if( Pro_Cnum == 6 ) {
		SetAddr_Addr += d * 256;
		Pro_Cnum = 0;
		
		CheckSum = 0;
		CODE_Start();
		
		remo_USART_write( VM_HEAD );
		remo_USART_write( 1 );
		remo_USART_write( 0 );
		remo_USART_write( VM_SETADDR );
	}
}
//--------------------------------------------
//读取指令
uint16 AddrH;
void Read( uint8 d )
{
	uint32 i;
	uint16 Addr;
	if( Pro_Cnum == 4 ) {
		return;
	}
	if( Pro_Cnum == 5 ) {
		AddrH = d;
		return;
	}
	if( Pro_Cnum == 6 ) {
		Addr = AddrH * 256 + d;
		Pro_Cnum = 0;
		
		remo_USART_write( VM_HEAD );
		remo_USART_write( 1 );
		remo_USART_write( 1 );
		remo_USART_write( VM_READ );
		
		for( i = 0; i < 256; ++i ) {
			remo_USART_write( Flash_ReadByte( Addr + i ) );
		}
	}
}
//--------------------------------------------
//接收一个指令并存储
uint32 Number;

//指令存储缓冲区
uint8 ByteCodeBuffer[256];
uint32 Cnum;

void Store( uint8 d )
{
	if( Pro_Cnum == 4 ) {
		Number = Pro_Length - 1;
		Cnum = 0;
		return;
	}
	CheckSum ^= d;
	ByteCodeBuffer[ Cnum ] = d;
	++Cnum;
	if( Cnum == Number ) {
		Pro_Cnum = 0;
		
		CODE_AddInsPage( ByteCodeBuffer, Cnum );
		
		remo_USART_write( VM_HEAD );
		remo_USART_write( 1 );
		remo_USART_write( 0 );
		remo_USART_write( VM_STORE );
	}
}
//--------------------------------------------
//发回校验和
void Check( uint8 d )
{
	Pro_Cnum = 0;
	
	remo_USART_write( VM_HEAD );
	remo_USART_write( 2 );
	remo_USART_write( 0 );
	remo_USART_write( VM_CHECK );
	remo_USART_write( CheckSum );
	
	vos_Flash_Save();
	
	//由于 LoadAndRun 会初始化串口 需要延时一小段时间等待数据发送完成
	
//#ifdef VOS_Tick
//	SoftDelay_ms(100);
//#endif
	
	//加载并执行目标文件程序
	LoadAndRun();
	
	Pro_downloading = false;
}
//--------------------------------------------
//版本信息协议
void Version( uint8 d )
{
	uint8 slen;
	uint32 ram = VOS_SYS_BASE_LENGTH;
	uint32 rom = Flash_GetSize();
	
	Pro_Cnum = 0;
	slen = StringConst_length( VOS_DEV_MESSAGE );
	
	remo_USART_write( VM_HEAD );
	
	//数据长度 2字节
	remo_USART_write( 14 + slen );
	remo_USART_write( 0 );
	
	remo_USART_write( VM_VERSION );
	remo_USART_write( VM_VERSION_A );
	remo_USART_write( VM_VERSION_B );
	
	//这里是为了和旧版本兼容
	remo_USART_write( ram / 1024 );
	remo_USART_write( rom / 1024 );
	
	remo_USART_write( VOS_DEV_VERSION );
	
	//2020.2.27 增加了RAM和ROM信息
	remo_USART_write( (uint8)(ram % 256) ); ram /= 256;
	remo_USART_write( (uint8)(ram % 256) ); ram /= 256;
	remo_USART_write( (uint8)(ram % 256) ); ram /= 256;
	remo_USART_write( (uint8)(ram % 256) );
	
	remo_USART_write( (uint8)(rom % 256) ); rom /= 256;
	remo_USART_write( (uint8)(rom % 256) ); rom /= 256;
	remo_USART_write( (uint8)(rom % 256) ); rom /= 256;
	remo_USART_write( (uint8)(rom % 256) );
	
	remo_USART_SendStringConst( VOS_DEV_MESSAGE );
	
	//这里放置需要停止的所有外设代码
	vos_DrvTimer_Stop();
	vos_Running = false;
	Pro_downloading = true;
}
//--------------------------------------------
//协议出错处理程序
void Error( uint32 ET, uint32 EC )
{
	remo_USART_write( VM_HEAD );
	remo_USART_write( 3 );
	remo_USART_write( 0 );
	remo_USART_write( VM_ERROR );
	remo_USART_write( ET );
	remo_USART_write( EC );
}
//========================================================
//调试信息
void Debug( uint32 data32 )
{
	remo_USART_write( VM_HEAD );
	remo_USART_write( 5 );
	remo_USART_write( 0 );
	remo_USART_write( VM_DEBUG );
	remo_USART_write( data32 & 0xFF );
	remo_USART_write( data32 >> 8 & 0xFF );
	remo_USART_write( data32 >> 16 & 0xFF );
	remo_USART_write( data32 >> 24 & 0xFF );
}


#endif



