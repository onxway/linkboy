﻿/*
 * 由 SharpDevelop 创建。
 * 用户: bbb_2
 * 日期: 2009-11-17
 * 时间: 11:15
 * 
 * 如果你想要更改该模板，那么请使用“工具 | 选项 | 编辑 | 编辑标准标题”。
 */
namespace n_CMusicForm
{
	partial class CMusicForm
	{
		/// <summary>
		/// Designer variable used to keep track of non-visual components.
		/// </summary>
		private System.ComponentModel.IContainer components = null;
		
		/// <summary>
		/// Disposes resources used by the form.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing) {
				if (components != null) {
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}
		
		/// <summary>
		/// This method is required for Windows Forms designer support.
		/// Do not change the method contents inside the source code editor. The Forms designer might
		/// not be able to load this method if it was changed manually.
		/// </summary>
		private void InitializeComponent()
		{
			this.components = new System.ComponentModel.Container();
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(CMusicForm));
			this.button打开串口 = new System.Windows.Forms.Button();
			this.comboBox串口号 = new System.Windows.Forms.ComboBox();
			this.comboBox波特率 = new System.Windows.Forms.ComboBox();
			this.checkBoxDTR_RTS = new System.Windows.Forms.CheckBox();
			this.panel1 = new System.Windows.Forms.Panel();
			this.comboBoxTimber = new System.Windows.Forms.ComboBox();
			this.panel2 = new System.Windows.Forms.Panel();
			this.timer1 = new System.Windows.Forms.Timer(this.components);
			this.panel1.SuspendLayout();
			this.SuspendLayout();
			// 
			// button打开串口
			// 
			this.button打开串口.BackColor = System.Drawing.Color.DarkGray;
			resources.ApplyResources(this.button打开串口, "button打开串口");
			this.button打开串口.ForeColor = System.Drawing.Color.White;
			this.button打开串口.Name = "button打开串口";
			this.button打开串口.UseVisualStyleBackColor = false;
			this.button打开串口.Click += new System.EventHandler(this.Button打开串口Click);
			// 
			// comboBox串口号
			// 
			this.comboBox串口号.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
			resources.ApplyResources(this.comboBox串口号, "comboBox串口号");
			this.comboBox串口号.FormattingEnabled = true;
			this.comboBox串口号.Name = "comboBox串口号";
			this.comboBox串口号.Click += new System.EventHandler(this.ComboBox串口号Click);
			// 
			// comboBox波特率
			// 
			resources.ApplyResources(this.comboBox波特率, "comboBox波特率");
			this.comboBox波特率.FormattingEnabled = true;
			this.comboBox波特率.Items.AddRange(new object[] {
									resources.GetString("comboBox波特率.Items"),
									resources.GetString("comboBox波特率.Items1"),
									resources.GetString("comboBox波特率.Items2"),
									resources.GetString("comboBox波特率.Items3"),
									resources.GetString("comboBox波特率.Items4"),
									resources.GetString("comboBox波特率.Items5"),
									resources.GetString("comboBox波特率.Items6"),
									resources.GetString("comboBox波特率.Items7"),
									resources.GetString("comboBox波特率.Items8")});
			this.comboBox波特率.Name = "comboBox波特率";
			// 
			// checkBoxDTR_RTS
			// 
			resources.ApplyResources(this.checkBoxDTR_RTS, "checkBoxDTR_RTS");
			this.checkBoxDTR_RTS.ForeColor = System.Drawing.Color.White;
			this.checkBoxDTR_RTS.Name = "checkBoxDTR_RTS";
			this.checkBoxDTR_RTS.UseVisualStyleBackColor = true;
			// 
			// panel1
			// 
			this.panel1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(134)))), ((int)(((byte)(171)))), ((int)(((byte)(198)))));
			this.panel1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
			this.panel1.Controls.Add(this.comboBoxTimber);
			this.panel1.Controls.Add(this.button打开串口);
			this.panel1.Controls.Add(this.comboBox波特率);
			this.panel1.Controls.Add(this.checkBoxDTR_RTS);
			this.panel1.Controls.Add(this.comboBox串口号);
			resources.ApplyResources(this.panel1, "panel1");
			this.panel1.Name = "panel1";
			// 
			// comboBoxTimber
			// 
			this.comboBoxTimber.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
			resources.ApplyResources(this.comboBoxTimber, "comboBoxTimber");
			this.comboBoxTimber.FormattingEnabled = true;
			this.comboBoxTimber.Items.AddRange(new object[] {
									resources.GetString("comboBoxTimber.Items"),
									resources.GetString("comboBoxTimber.Items1"),
									resources.GetString("comboBoxTimber.Items2"),
									resources.GetString("comboBoxTimber.Items3"),
									resources.GetString("comboBoxTimber.Items4"),
									resources.GetString("comboBoxTimber.Items5"),
									resources.GetString("comboBoxTimber.Items6"),
									resources.GetString("comboBoxTimber.Items7"),
									resources.GetString("comboBoxTimber.Items8"),
									resources.GetString("comboBoxTimber.Items9"),
									resources.GetString("comboBoxTimber.Items10"),
									resources.GetString("comboBoxTimber.Items11"),
									resources.GetString("comboBoxTimber.Items12"),
									resources.GetString("comboBoxTimber.Items13"),
									resources.GetString("comboBoxTimber.Items14"),
									resources.GetString("comboBoxTimber.Items15"),
									resources.GetString("comboBoxTimber.Items16"),
									resources.GetString("comboBoxTimber.Items17"),
									resources.GetString("comboBoxTimber.Items18"),
									resources.GetString("comboBoxTimber.Items19"),
									resources.GetString("comboBoxTimber.Items20"),
									resources.GetString("comboBoxTimber.Items21"),
									resources.GetString("comboBoxTimber.Items22"),
									resources.GetString("comboBoxTimber.Items23"),
									resources.GetString("comboBoxTimber.Items24"),
									resources.GetString("comboBoxTimber.Items25"),
									resources.GetString("comboBoxTimber.Items26"),
									resources.GetString("comboBoxTimber.Items27"),
									resources.GetString("comboBoxTimber.Items28"),
									resources.GetString("comboBoxTimber.Items29"),
									resources.GetString("comboBoxTimber.Items30"),
									resources.GetString("comboBoxTimber.Items31"),
									resources.GetString("comboBoxTimber.Items32"),
									resources.GetString("comboBoxTimber.Items33"),
									resources.GetString("comboBoxTimber.Items34"),
									resources.GetString("comboBoxTimber.Items35"),
									resources.GetString("comboBoxTimber.Items36"),
									resources.GetString("comboBoxTimber.Items37"),
									resources.GetString("comboBoxTimber.Items38"),
									resources.GetString("comboBoxTimber.Items39"),
									resources.GetString("comboBoxTimber.Items40"),
									resources.GetString("comboBoxTimber.Items41"),
									resources.GetString("comboBoxTimber.Items42"),
									resources.GetString("comboBoxTimber.Items43"),
									resources.GetString("comboBoxTimber.Items44"),
									resources.GetString("comboBoxTimber.Items45"),
									resources.GetString("comboBoxTimber.Items46"),
									resources.GetString("comboBoxTimber.Items47"),
									resources.GetString("comboBoxTimber.Items48"),
									resources.GetString("comboBoxTimber.Items49"),
									resources.GetString("comboBoxTimber.Items50"),
									resources.GetString("comboBoxTimber.Items51"),
									resources.GetString("comboBoxTimber.Items52"),
									resources.GetString("comboBoxTimber.Items53"),
									resources.GetString("comboBoxTimber.Items54"),
									resources.GetString("comboBoxTimber.Items55"),
									resources.GetString("comboBoxTimber.Items56"),
									resources.GetString("comboBoxTimber.Items57"),
									resources.GetString("comboBoxTimber.Items58"),
									resources.GetString("comboBoxTimber.Items59"),
									resources.GetString("comboBoxTimber.Items60"),
									resources.GetString("comboBoxTimber.Items61"),
									resources.GetString("comboBoxTimber.Items62"),
									resources.GetString("comboBoxTimber.Items63"),
									resources.GetString("comboBoxTimber.Items64"),
									resources.GetString("comboBoxTimber.Items65"),
									resources.GetString("comboBoxTimber.Items66"),
									resources.GetString("comboBoxTimber.Items67"),
									resources.GetString("comboBoxTimber.Items68"),
									resources.GetString("comboBoxTimber.Items69"),
									resources.GetString("comboBoxTimber.Items70"),
									resources.GetString("comboBoxTimber.Items71"),
									resources.GetString("comboBoxTimber.Items72"),
									resources.GetString("comboBoxTimber.Items73"),
									resources.GetString("comboBoxTimber.Items74"),
									resources.GetString("comboBoxTimber.Items75"),
									resources.GetString("comboBoxTimber.Items76"),
									resources.GetString("comboBoxTimber.Items77"),
									resources.GetString("comboBoxTimber.Items78"),
									resources.GetString("comboBoxTimber.Items79"),
									resources.GetString("comboBoxTimber.Items80"),
									resources.GetString("comboBoxTimber.Items81"),
									resources.GetString("comboBoxTimber.Items82"),
									resources.GetString("comboBoxTimber.Items83"),
									resources.GetString("comboBoxTimber.Items84"),
									resources.GetString("comboBoxTimber.Items85"),
									resources.GetString("comboBoxTimber.Items86"),
									resources.GetString("comboBoxTimber.Items87"),
									resources.GetString("comboBoxTimber.Items88"),
									resources.GetString("comboBoxTimber.Items89"),
									resources.GetString("comboBoxTimber.Items90"),
									resources.GetString("comboBoxTimber.Items91"),
									resources.GetString("comboBoxTimber.Items92"),
									resources.GetString("comboBoxTimber.Items93"),
									resources.GetString("comboBoxTimber.Items94"),
									resources.GetString("comboBoxTimber.Items95"),
									resources.GetString("comboBoxTimber.Items96"),
									resources.GetString("comboBoxTimber.Items97"),
									resources.GetString("comboBoxTimber.Items98"),
									resources.GetString("comboBoxTimber.Items99"),
									resources.GetString("comboBoxTimber.Items100"),
									resources.GetString("comboBoxTimber.Items101"),
									resources.GetString("comboBoxTimber.Items102"),
									resources.GetString("comboBoxTimber.Items103"),
									resources.GetString("comboBoxTimber.Items104"),
									resources.GetString("comboBoxTimber.Items105"),
									resources.GetString("comboBoxTimber.Items106"),
									resources.GetString("comboBoxTimber.Items107"),
									resources.GetString("comboBoxTimber.Items108"),
									resources.GetString("comboBoxTimber.Items109"),
									resources.GetString("comboBoxTimber.Items110"),
									resources.GetString("comboBoxTimber.Items111"),
									resources.GetString("comboBoxTimber.Items112"),
									resources.GetString("comboBoxTimber.Items113"),
									resources.GetString("comboBoxTimber.Items114"),
									resources.GetString("comboBoxTimber.Items115"),
									resources.GetString("comboBoxTimber.Items116"),
									resources.GetString("comboBoxTimber.Items117"),
									resources.GetString("comboBoxTimber.Items118"),
									resources.GetString("comboBoxTimber.Items119"),
									resources.GetString("comboBoxTimber.Items120"),
									resources.GetString("comboBoxTimber.Items121"),
									resources.GetString("comboBoxTimber.Items122"),
									resources.GetString("comboBoxTimber.Items123"),
									resources.GetString("comboBoxTimber.Items124"),
									resources.GetString("comboBoxTimber.Items125"),
									resources.GetString("comboBoxTimber.Items126"),
									resources.GetString("comboBoxTimber.Items127"),
									resources.GetString("comboBoxTimber.Items128"),
									resources.GetString("comboBoxTimber.Items129"),
									resources.GetString("comboBoxTimber.Items130"),
									resources.GetString("comboBoxTimber.Items131"),
									resources.GetString("comboBoxTimber.Items132"),
									resources.GetString("comboBoxTimber.Items133"),
									resources.GetString("comboBoxTimber.Items134"),
									resources.GetString("comboBoxTimber.Items135"),
									resources.GetString("comboBoxTimber.Items136"),
									resources.GetString("comboBoxTimber.Items137"),
									resources.GetString("comboBoxTimber.Items138"),
									resources.GetString("comboBoxTimber.Items139"),
									resources.GetString("comboBoxTimber.Items140"),
									resources.GetString("comboBoxTimber.Items141"),
									resources.GetString("comboBoxTimber.Items142"),
									resources.GetString("comboBoxTimber.Items143")});
			this.comboBoxTimber.Name = "comboBoxTimber";
			this.comboBoxTimber.SelectedIndexChanged += new System.EventHandler(this.ComboBoxTimberSelectedIndexChanged);
			// 
			// panel2
			// 
			resources.ApplyResources(this.panel2, "panel2");
			this.panel2.Name = "panel2";
			// 
			// timer1
			// 
			this.timer1.Enabled = true;
			this.timer1.Interval = 50;
			this.timer1.Tick += new System.EventHandler(this.Timer1Tick);
			// 
			// CMusicForm
			// 
			resources.ApplyResources(this, "$this");
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.BackColor = System.Drawing.Color.Black;
			this.Controls.Add(this.panel2);
			this.Controls.Add(this.panel1);
			this.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
			this.Name = "CMusicForm";
			this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.UARTFormFormClosing);
			this.panel1.ResumeLayout(false);
			this.ResumeLayout(false);
		}
		private System.Windows.Forms.Timer timer1;
		private System.Windows.Forms.ComboBox comboBoxTimber;
		private System.Windows.Forms.Panel panel2;
		private System.Windows.Forms.Panel panel1;
		private System.Windows.Forms.CheckBox checkBoxDTR_RTS;
		private System.Windows.Forms.ComboBox comboBox波特率;
		private System.Windows.Forms.ComboBox comboBox串口号;
		private System.Windows.Forms.Button button打开串口;
	}
}
