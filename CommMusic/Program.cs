﻿
using System;
using n_CMusicForm;
using System.Windows.Forms;


namespace n_CWave
{
	class Program
	{
		public static void Main(string[] args)
		{
			Application.EnableVisualStyles();
			Application.SetCompatibleTextRenderingDefault(false);
			
			CMusicForm f = new CMusicForm();
			f.SingleMode = true;
			Application.Run( f );
		}
	}
}


