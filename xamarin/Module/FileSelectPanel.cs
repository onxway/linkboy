﻿using System.IO;
using n_SG_MyButton;
using n_SG_MyLabel;
using n_SG_MyPanel;
using n_M;
using n_SG_MyCheckBox;
using n_LocalIO;
#if OS_win
using System.Drawing;
#endif

#if OS_android
using Android.Graphics;
#endif

namespace n_FileSelectPanel
{
	public class FileSelectPanel : MyPanel
{
	public delegate void D_SetComplete( string Result );
	public D_SetComplete SetComplete;
	
	int Number;
	
	MyPanel ListPanel;
	
	//构造函数
	public FileSelectPanel( int x, int y, int w, int h ) : base( x, y, w, h )
	{
		BackColor = Color.WhiteSmoke;
		Visible = false;
		
		int ButtonHeight = M.MaxHeight / 10;
		int Padding = 5;
		int StartY = M.MaxHeight;
		int StartX = Padding;
		
		StartY -= ButtonHeight;
		MyButton bCancel = new MyButton( Color.Orange, "取消", 30, Color.White, StartX, StartY - Padding, M.MaxWidth - Padding*2, ButtonHeight );
		bCancel.TouchClickEvent = CancelButtonTouchClickEvent;
		Add( 1, bCancel );
		
		StartY -= ButtonHeight;
		MyCheckBox cb = new MyCheckBox( Color.Transparent, "启动APP后自动加载选中的文件", 30, Color.OrangeRed, StartX, StartY - Padding, M.MaxWidth - Padding*2, ButtonHeight );
		cb.CheckChanged = AutoCheckChanged;
		//Add( 1, cb );
		
		ListPanel = new MyPanel( 0, 0, Width, Height );
		Add( 0, ListPanel );
	}
	
	//显示面板
	public void Show( string Path )
	{
		ListPanel.Clear( 0 );
		Number = 0;
		
		if( Directory.Exists( Path ) ) {
			DirectoryInfo Dir = new DirectoryInfo( Path );
			foreach( FileInfo s in Dir.GetFiles() ) {
				//Add( Path + "/" + s );
				Add( s.ToString() );
			}
		}
		else {
			MyLabel mes = new MyLabel( Color.WhiteSmoke, "请在手机内存卡(不是SD卡)根目录下新建linkboy文件夹", 30, Color.OrangeRed, 0, 0, M.MaxWidth, 200 );
			MyLabel mes1 = new MyLabel( Color.WhiteSmoke, "然后把vex文件拷贝进去再重新运行remo", 30, Color.OrangeRed, 0, 200, M.MaxWidth, 200 );
			Add( 1, mes );
			Add( 1, mes1 );
		}
		
		Visible = true;
	}
	
	public void Quit()
	{
		Visible = false;
	}
	
	void AutoCheckChanged( object sender )
	{
		MyCheckBox c = (MyCheckBox)sender;
		//AutoMgr.Save( c.Checked.ToString() );
	}
	
	void Add( string File )
	{
		MyButton b = new MyButton( Color.SlateBlue, File, 25, Color.White, 5, 5 + 80*Number, Width - 10, 75 );
		b.TouchClickEvent = BleClick;
		b.Message = File;
		ListPanel.Add( 0, b );
		
		Number++;
	}
	
	//-------------------------------
	
	void BleClick( object sender )
	{
		Visible = false;
		if( SetComplete != null ) {
			SetComplete( ((MyButton)sender).Message );
		}
	}
	
	void CancelButtonTouchClickEvent( object o )
	{
		Quit();
	}
}
public static class MacMgr
{
	//-------------------------------
	
	public static void Save( string Mac )
	{
		LocalIO.Save( "MAC", Mac );
	}
	
	public static string Open()
	{
		string d = LocalIO.Load( "MAC" );
		return d;
	}
}
}


