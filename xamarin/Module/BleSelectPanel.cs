﻿
using System;
using n_SG;
using n_SG_MyButton;
using n_SG_MyLabel;
using n_SG_MyPanel;
using n_SYS;
using n_M;
using n_SG_MyComboBox;
using n_SG_MyCheckBox;
using System.Text;
using n_Ble;
using n_Ble20;
using n_LocalIO;
#if OS_win
using System.Drawing;
#endif

#if OS_android
using Android.Graphics;
using Android.Views;
using Android.Bluetooth;
#endif

namespace n_BleSelectPanel
{
public class BleSelectPanel : MyPanel
{
	public delegate void D_SetComplete( string Result );
	public D_SetComplete SetComplete;
	
	public bool Ble40_not20;
	
	int Number;
	
	MyPanel ListPanel;
	
	//构造函数
	public BleSelectPanel( int x, int y, int w, int h ) : base( x, y, w, h )
	{
		BackColor = Color.WhiteSmoke;
		Visible = false;
		
		Ble40_not20 = false;
		
		int ButtonHeight = M.MaxHeight / 10;
		int Padding = 5;
		int StartY = M.MaxHeight;
		int StartX = Padding;
		
		StartY -= ButtonHeight;
		MyButton bCancel = new MyButton( Color.DimGray, "取消", Height/10, Color.White, StartX, StartY - Padding, M.MaxWidth - Padding*2, ButtonHeight );
		bCancel.TouchClickEvent = CancelButtonTouchClickEvent;
		Add( 1, bCancel );
		
		StartY -= ButtonHeight;
		MyCheckBox cb = new MyCheckBox( Color.Transparent, "启动APP后自动连接历史蓝牙设备", Height/10, Color.OrangeRed, StartX, StartY - Padding, M.MaxWidth - Padding*2, ButtonHeight );
		cb.CheckChanged = AutoCheckChanged;
		//Add( 1, cb );
		
		ListPanel = new MyPanel( 0, 0, Width, Height );
		Add( 0, ListPanel );
		
		Ble.BleScan = BleScan;
		Ble20.DeviceFound = BleScan;
		try {
			Ble.AutoMacAddr = MacMgr.Open();
		}
		catch {
			//...
		}
		try {
			Ble.AutoScan = AutoMgr.Open() == "True";
			cb.Checked = Ble.AutoScan;
		}
		catch {
			//...
		}
	}
	
	//显示面板
	public void Show()
	{
		if( Ble40_not20 ) {
			Ble.RestartBle();
			//注意这两个需要放到重启指令后边
			Ble.AutoConnect = false;
			Ble.AutoScan = true;
		}
		else {
			Ble20.RestartBle();
		}
		
		this.Visible = true;
		
		ListPanel.Clear( 0 );
		Number = 0;
	}
	
	public void Quit()
	{
		if( Ble40_not20 ) {
			Ble.StopScan();
			Ble.AutoConnect = true;
		}
		else {
			Ble20.StopScan();
		}
		
		Visible = false;
	}
	
	void AutoCheckChanged( object sender )
	{
		MyCheckBox c = (MyCheckBox)sender;
		AutoMgr.Save( c.Checked.ToString() );
	}
	
	void BleScan( int i, string Name, string Addr )
	{
		MyButton b = new MyButton( Color.SlateBlue, Name + "(" + Addr + ")", 25, Color.White, 5, 5 + (Height/10+10)*Number, Width - 10, Height/10 );
		b.TouchClickEvent = BleClick;
		if( Ble40_not20 ) {
			b.Message = i.ToString();
		}
		else {
			b.Message = Addr;
		}
		ListPanel.Add( 0, b );
		
		SYS.Refresh();
		Number++;
	}
	
	//-------------------------------
	
	void BleClick( object sender )
	{
		MyButton mb = (MyButton)sender;
		if( Ble40_not20 ) {
			int i = int.Parse( mb.Message );
			string Mac = Ble.Connect( i );
			MacMgr.Save( Mac );
		}
		else {
			Ble20.ConnectDevice( mb.Message );
			MacMgr.Save( mb.Message );
		}
		
		Quit();
	}
	
	void CancelButtonTouchClickEvent( object o )
	{
		Quit();
	}
}
public static class MacMgr
{
	//-------------------------------
	
	public static void Save( string Mac )
	{
		LocalIO.Save( "MAC", Mac );
	}
	
	public static string Open()
	{
		string d = LocalIO.Load( "MAC" );
		return d;
	}
}
public static class AutoMgr
{
	//-------------------------------
	
	public static void Save( string Mac )
	{
		LocalIO.Save( "Auto", Mac );
	}
	
	public static string Open()
	{
		string d = LocalIO.Load( "Auto" );
		return d;
	}
}
}


