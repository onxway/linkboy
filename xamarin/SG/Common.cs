﻿
using System;

namespace n_Common
{
public static class Common
{
	public class Font
	{
		public string Name;
		public float Size;
		
		public bool Bold;
		public bool Italic;
		public bool Underline;
		public bool Strikeout;
		
		//构造函数
		public Font( string n, float s )
		{
			Name = n;
			Size = s;
			
			Bold = false;
			Italic = false;
			Underline = false;
			Strikeout = false;
		}
	}
	
	//初始化
//	public static void Init()
//	{
//	}
	
	//根据文本描述返回字体
	public static Font GetFontFromString( string Font )
	{
		string[] FontList = Font.Split( ',' );
		string FontName = FontList[ 0 ].Trim( ' ' );
		float FontSize = float.Parse( FontList[ 1 ].Trim( ' ' ) );
		if( FontList.Length > 3 ) {
			return null;
		}
		
		#if OS_android
		FontSize = FontSize * 4 / 3;
		#endif
		
		Font f = new Font( FontName, FontSize );
		
		if( FontList.Length == 3 ) {
			string BIUS = FontList[ 2 ].Trim( ' ' );
			if( BIUS.IndexOf( "B" ) != -1 ) {
				f.Bold = true;
			}
			if( BIUS.IndexOf( "I" ) != -1 ) {
				f.Italic = true;
			}
			if( BIUS.IndexOf( "U" ) != -1 ) {
				f.Underline = true;
			}
			if( BIUS.IndexOf( "S" ) != -1 ) {
				f.Strikeout = true;
			}
		}
		return f;
	}
}
}

