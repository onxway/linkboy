﻿
using System;
using n_SYS;
using n_M;
using n_SG_MyPanel;
using n_SG_MyButton;
using n_SG_MyLabel;
using n_SG_MyNumberBox;
using n_SG_MyTrackBar;
using n_SG_MySound;
using n_Common;
using n_IO;

#if OS_win
using n_SG_MyMIDI;
using n_SG_MyKeyBoard;
using n_SG_MySerialPort;
#endif

using n_SG_MyCheckBox;
using n_SG_MySprite;
using n_SG_MyWaveBox;
using n_SG_MyBitmapBox;

#if OS_android
using Android.Graphics;
#endif

#if OS_win
using System.Drawing;
#endif

namespace n_Linker
{
public static class Linker
{
	public delegate void D_StartRun();
	public static D_StartRun StartRun;
	
	public static bool isConnected;
	
	static byte[] Buffer;
	const int MaxLength = 100;
	static int Length;
	
	static bool isStringData;
	
	static byte[] SendBuffer;
	
	//初始化
	public static void Init( object a )
	{
		Common.Init();
		
		IO.Init( a );
		
		isConnected = false;
		Buffer = new byte[ MaxLength ];
		Length = 0;
		isStringData = false;
		
		SendBuffer = new byte[100];
		
		SYS.DealMessage = DealData;
	}
	
	//关闭
	public static void Close()
	{
		IO.Close();
	}
	
	//唤醒
	public static void Resume()
	{
		IO.Resume();
	}
	
	//蓝牙连接建立事件
	static void BleConnected()
	{
		System.Threading.Thread.Sleep( 1100 );
		if( !isConnected ) {
			SendStartCommand();
		}
	}
	
	//处理一个接收字节
	public static void DealData( int b )
	{
		Buffer[ Length ] = (byte)b;
		++Length;
		
		//判断是否为返回确认数据
		if( Length == 1 ) {
			if( Buffer[0] == 0x55 ) {
				isStringData = false;
			}
			else if( Buffer[0] == 0xAA ) {
				isStringData = true;
			}
			else {
				Error( "协议解析错误: " + Buffer[0] + "\n" );
				Length = 0;
			}
			return;
		}
		if( isStringData ) {
			if( Length < 5 || Buffer[ Length - 1 ] != 0 ) {
				return;
			}
		}
		else {
			if( Length < Buffer[1] + 3 ) {
				return;
			}
		}
		Length = 0;
		
		int DataNumber = Buffer[1];
		int ID = Buffer[2];
		int Command = Buffer[3];
		
		//-------------------------------------------------------------
		
		//判断是否为发给系统
		if( ID == 0 ) {
			//判断是否复位软件
			if( Command == 0 ) {
				if( !isConnected ) {
					SendStartCommand();
				}
				isConnected = true;
				Log( "控制板探测数据\n" );
			}
			//判断是否建立控件
			if( Command == 1 ) {
				int cID = Buffer[4];
				int CType = Buffer[5];
				
				if( CType == 1 ) {
					MyPanel myPanel = new MyPanel( 10, 10, 10, 10 );
					myPanel.TouchDownEvent = UserPanelDown;
					myPanel.TouchUpEvent = UserPanelUp;
					myPanel.TouchMoveEvent = UserPanelMove;
					myPanel.BackColor = Color.SlateBlue;
					myPanel.ID = cID;
					myPanel.isControlPad = true;
					M.MyControlList[cID] = myPanel;
					Log( "创建 ControlPad\n" );
				}
				if( CType == 2 ) {
					MyButton myButton = new MyButton( Color.OrangeRed, "" + cID, 30, Color.WhiteSmoke, 10, 10, 10, 10 );
					myButton.TouchDownEvent = UserButtonDown;
					myButton.TouchUpEvent = UserButtonUp;
					myButton.ID = cID;
					M.MyControlList[cID] = myButton;
					Log( "创建 Button\n" );
				}
				if( CType == 3 ) {
					MyPanel myPanel = new MyPanel( 50, 50 + cID * 100, 150, 50 );
					myPanel.TouchDownEvent = UserPanelDown;
					myPanel.TouchUpEvent = UserPanelUp;
					myPanel.ID = cID;
					M.MyControlList[cID] = myPanel;
					Log( "创建 Panel\n" );
				}
				if( CType == 4 ) {
					MyNumberBox myNumberBox = new MyNumberBox( Color.OrangeRed, 0, 30, Color.WhiteSmoke, 10, 10, 10, 10 );
					myNumberBox.ID = cID;
					M.MyControlList[cID] = myNumberBox;
					Log( "创建 NumberBox\n" );
				}
				if( CType == 5 ) {
					MyLabel myLabel = new MyLabel( Color.OrangeRed, "", 30, Color.WhiteSmoke, 10, 10, 10, 10 );
					myLabel.ID = cID;
					M.MyControlList[cID] = myLabel;
					Log( "创建 Label\n" );
				}
				if( CType == 6 ) {
					MyTrackBar myTrackBar = new MyTrackBar( Color.LightBlue, Color.Blue, 10, 10, 10, 10 );
					myTrackBar.ValueChanged = TraceBarChanged;
					myTrackBar.ID = cID;
					M.MyControlList[cID] = myTrackBar;
					Log( "创建 TrackBar\n" );
				}
				if( CType == 7 ) {
					MyCheckBox myCheckBox = new MyCheckBox( Color.LightBlue, "选择框", 20, Color.Blue, 10, 10, 10, 10 );
					myCheckBox.CheckChanged = CheckBoxChanged;
					myCheckBox.ID = cID;
					M.MyControlList[cID] = myCheckBox;
					Log( "创建 CheckBox\n" );
				}
				if( CType == 16 ) {
					MyWaveBox myWaveBox = new MyWaveBox( Color.OrangeRed, 30, Color.WhiteSmoke, 10, 10, 10, 10 );
					myWaveBox.ID = cID;
					M.MyControlList[cID] = myWaveBox;
					Log( "创建 WaveBox\n" );
				}
				if( CType == 17 ) {
					MyBitmapBox myBitmapBox = new MyBitmapBox( Color.OrangeRed, Color.WhiteSmoke, 10, 10, 10, 10 );
					myBitmapBox.ID = cID;
					M.MyControlList[cID] = myBitmapBox;
					Log( "创建 BitmapBox\n" );
				}
				if( CType == 20 ) {
					#if OS_win
					MyMIDI myMidi = new MyMIDI();
					myMidi.ID = cID;
					myMidi.isHidenModule = true;
					M.MyControlList[cID] = myMidi;
					Log( "创建 MyMIDI\n" );
					#endif
					
					#if OS_android
					SYS.AddMessage( "ERROR:" + CType );
					#endif
				}
				if( CType == 21 ) {
					#if OS_win
					MyKeyBoard mykey = new MyKeyBoard();
					mykey.ID = cID;
					mykey.isHidenModule = true;
					M.MyControlList[cID] = mykey;
					Log( "创建 MyKeyBoard\n" );
					#endif
					
					#if OS_android
					SYS.AddMessage( "ERROR:" + CType );
					#endif
				}
				if( CType == 22 ) {
					#if OS_win
					MySound mysound = new MySound();
					mysound.ID = cID;
					mysound.isHidenModule = true;
					M.MyControlList[cID] = mysound;
					Log( "创建 MySound\n" );
					#endif
					
					#if OS_android
					SYS.AddMessage( "ERROR:" + CType );
					#endif
				}
				if( CType == 23 ) {
					#if OS_win
					MySerialPort mys = new MySerialPort();
					mys.ID = cID;
					mys.isHidenModule = true;
					M.MyControlList[cID] = mys;
					Log( "创建 MySerialPort\n" );
					#endif
					
					#if OS_android
					SYS.AddMessage( "ERROR:" + CType );
					#endif
				}
				if( CType == 40 ) {
					MySprite mysp = new MySprite( 10, 10, 10, 10 );
					mysp.ID = cID;
					M.MyControlList[cID] = mysp;
					Log( "创建 MySprite\n" );
				}
			}
			//判断是否开始运行
			if( Command == 2 ) {
				M.StartRunControl();
				if( StartRun != null ) {
					StartRun();
				}
				Log( "控制板进入运行态\n" );
				
				//SYS.SetMessage( "" );
			}
			//判断是否为设置当前文件夹
			if( Command == 3 ) {
				string s = n_SG_MyControl.MyControl.DecodeGBK( Buffer );
				n_CommonData.CommonData.SetBasePath( s );
				Log( "设置当前目录\n" );
				
				//SYS.SetMessage( "" );
			}
		}
		else {
			try {
				M.MyControlList[ ID ].Decode( Command, Buffer );
			}
			catch {
				//n_V.V.VMBox.Text = ID.ToString();
				n_SYS.SYS.AddMessage( "DealData ERROR, ID: " + ID );
			}
			
			Log( "发给控件数据\n" );
		}
		
		SYS.Refresh();
	}
	
	//协议解析log
	static void Log( string mes )
	{
		//SYS.AddMessage( mes );
	}
	
	//协议出错
	static void Error( string mes )
	{
		//SYS.AddMessage( mes );
	}
	
	//==============================================================
	
	//选择框改变事件
	static void CheckBoxChanged( object sender )
	{
		int ID = ((MyCheckBox)sender).ID;
		SendEventFlag( ID, 0x01 );
	}
	
	//滑动条数值变化事件
	static void TraceBarChanged( object sender )
	{
		int ID = ((MyTrackBar)sender).ID;
		SendEventFlag( ID, 0x01 );
	}
	
	//用户控件按钮按下事件
	static void UserButtonDown( object sender )
	{
		int ID = ((MyButton)sender).ID;
		SendEventFlag( ID, 0x01 );
	}
	
	//用户控件按钮松开事件
	static void UserButtonUp( object sender )
	{
		int ID = ((MyButton)sender).ID;
		SendEventFlag( ID, 0x02 );
	}
	
	//用户控件面板按下事件
	static void UserPanelDown( object sender, int x, int y )
	{
		MyPanel mp = (MyPanel)sender;
		mp.SetMouseLocation( x, y );
		int ID = mp.ID;
		SendEventFlag( ID, 0x01 );
	}
	
	//用户控件面板松开事件
	static void UserPanelUp( object sender, int x, int y )
	{
		MyPanel mp = (MyPanel)sender;
		mp.SetMouseLocation( x, y );
		int ID = mp.ID;
		SendEventFlag( ID, 0x02 );
	}
	
	//用户控件面板鼠标移动事件
	static void UserPanelMove( object sender, int x, int y )
	{
		MyPanel mp = (MyPanel)sender;
		mp.SetMouseLocation( x, y );
		int ID = mp.ID;
		
		if( n_IO.IO.ReadData ) return;
		
		SendEventFlag( ID, 0x04 );
	}
	
	//==============================================================
	
	//发送数据
	public static void SendData( int n, byte[] Buffer )
	{
		IO.SendData( n, Buffer );
	}
	
	//发送事件标志
	public static void SendEventFlag( int ID, byte e )
	{
		while( n_IO.IO.ReadData ) {}
		
		SendBuffer[0] = 0x55;
		SendBuffer[1] = 3;
		SendBuffer[2] = 0x01;
		SendBuffer[3] = (byte)ID;
		SendBuffer[4] = e;
		SendBuffer[5] = 0x00;
		
		IO.SendData( 6, SendBuffer );
	}
	
	//发送启动命令
	public static void SendStartCommand()
	{
		SendBuffer[0] = 0x55;
		SendBuffer[1] = 0x00;
		SendBuffer[2] = 0x00;
		IO.SendData( 3, SendBuffer );
	}
}
}

