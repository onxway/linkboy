﻿
using System;
using n_Linker;
using n_SYS;

#if OS_android
using Android.Graphics;
using n_Ble;
#endif

#if OS_win
using System.IO.Ports;
using System.Threading;
#endif

namespace n_IO
{
public static class IO
{
	public delegate void D_SendDataEvent( int n, byte[] Buffer );
	public static D_SendDataEvent SendDataEvent;
	
	public static bool Busy;
	
	//此变量为临时, 请注意查看remo 备忘文件. 2015.11.28
	public static bool ReadData;
	
	#if OS_win
	static SerialPort[] PortList;
	static int PortNumber;
	public static SerialPort port;
	static System.Threading.Thread t;
	static byte[] WriteBuffer;
	static int Length;
	#endif
	
	//初始化
	public static void Init( object a )
	{
		Busy = false;
		ReadData = false;
		
		#if OS_android
		Ble.Init( a );
		Ble.ReceiveData = BleReceiveData;
		Ble.BleConnected = BleConnected;
		#endif
		
		#if OS_win
		PortList = new SerialPort[ 20 ];
		PortNumber = 0;
		WriteBuffer = new byte[50];
		Length = 0;
		t = new Thread( new ThreadStart( ReceiveThread ) );
		t.Priority = ThreadPriority.Normal;
		
		if( n_VMForm.VMForm.FilePath == null ) {
			SearchRightPort();
		}
		
		#endif
	}
	
	//关闭
	public static void Close()
	{
		#if OS_android
		Ble.Close();
		#endif
		
		#if OS_win
		for( int i = 0; i < PortNumber; ++i ) {
			if( PortList[i] != null && PortList[i].IsOpen ) {
				PortList[i].Close();
			}
		}
		if( Linker.isConnected ) {
			t.Abort();
			t.Join();
		}
		
		#endif
	}
	
	//发送数据
	public static void SendData( int n, byte[] Buffer )
	{
		#if OS_android
		//Busy = true;
		Ble.SendData( n, Buffer );
		//Busy = false;
		System.Threading.Thread.Sleep( 20 );
		#endif
		
		#if OS_win
		Busy = true;
		if( port != null ) {
			port.Write( Buffer, 0, n );
		}
		if( SendDataEvent != null ) {
			SendDataEvent( n, Buffer );
		}
		
		Busy = false;
		#endif
	}
	
	//唤醒
	public static void Resume()
	{
		#if OS_android
		Ble.Resume();
		#endif
	}
	
	//==============================================
	
	#if OS_android
	
	//蓝牙连接建立事件
	static void BleConnected()
	{
		System.Threading.Thread.Sleep( 1100 );
		if( !Linker.isConnected ) {
			Linker.SendStartCommand();
		}
	}
	
	//蓝牙接收数据
	static void BleReceiveData( byte[] b )
	{
		for( int i = 0; i < b.Length; i++ ) {
			SYS.SendMessageFromThread( b[i] );
		}
	}
	
	#endif
	
	//==============================================
	
	#if OS_win
	
	//搜索串口
	static void SearchRightPort()
	{
		//获取端口
		Microsoft.VisualBasic.Devices.Computer pc = new Microsoft.VisualBasic.Devices.Computer();
		foreach( string Name in pc.Ports.SerialPortNames ) {
			InitPort( Name );
		}
		
		//哎.... 下边这个等待真吭人!
		//while( !isConnected ) {
		//	System.Windows.Forms.Application.DoEvents();
		//	System.Threading.Thread.Sleep( 100 );
		//}
	}
	
	//建立端口
	static void InitPort( string Name )
	{
		try {
			//初始化硬件串口
			SerialPort p = new System.IO.Ports.SerialPort( Name );
			
			p.BaudRate = 9600;
			//p.BaudRate = 38400;
			//p.BaudRate = 115200;
			
			p.DataBits = 8;
			p.Parity = System.IO.Ports.Parity.None;
			p.StopBits = System.IO.Ports.StopBits.One;
			p.ReadTimeout = 100;
			
			p.Open();
			p.DataReceived += new SerialDataReceivedEventHandler( AckDataReceived );
			
			PortList[PortNumber] = p;
			PortNumber++;
		}
		catch {
			//...
		}
	}
	
	//监听握手数据
	static void AckDataReceived( object sender, EventArgs e)
	{
		SerialPort p = (SerialPort)sender;
		byte b = (byte)p.ReadByte();
		
		WriteBuffer[Length] = b;
		Length++;
		
		if( Length == 1 && b != 0x55 ) {
			Length = 0;
			return;
		}
		if( Length < 5 ) {
			return;
		}
		if( WriteBuffer[1] != 0x02 || WriteBuffer[2] != 0x00 || WriteBuffer[3] != 0x00 || WriteBuffer[4] != 0x00 ) {
			Length = 0;
			return;
		}
		p.DataReceived -= new SerialDataReceivedEventHandler( AckDataReceived );
		port = p;
		Linker.isConnected = true;
		
		port.DiscardInBuffer();
		port.DiscardOutBuffer(); //这两行去掉后有时会无法自动重启连接
		Linker.SendStartCommand();
		
		Length = 0;
		t.Start();
	}
	
	//数据处理进程
	static void ReceiveThread()
	{
		while( true ) {
			
			//发现这个等待不可少
			while( Busy ) {}
			
			try {
			byte b = (byte)port.ReadByte();
			//SYS.SendMessageFromThread( b );
			Linker.DealData( b );
			}
			catch {
				
			}
		}
	}
	
	#endif
}
}

