﻿
using System;
using n_SG;
using n_Common;
using n_SG_MyControl;

#if OS_android
using Android.Graphics;
#endif

#if OS_win
using System.Drawing;
#endif

namespace n_SG_MyButton
{
public class MyButton : MyControl
{
	public delegate void d_TouchDown( object sender );
	public d_TouchDown TouchDownEvent;
	
	public delegate void d_TouchUp( object sender );
	public d_TouchUp TouchUpEvent;
	
	public delegate void d_TouchClick( object sender );
	public d_TouchClick TouchClickEvent;
	
	//是否带有锁定功能
	public bool CanLock;
	public bool Checked;
	
	public bool DrawBack;
	
	public Bitmap BackBitmap;
	
	public string Text;
	public float FontSize;
	
	enum E_ElementType
	{
		OnlyBitmap,
		OnlyString,
	}
	E_ElementType ElementType;
	
	public enum E_BackBitmapLayout
	{
		Center, Stretch
	}
	public E_BackBitmapLayout BackBitmapAlign;
	
	//构造函数 - 文本信息为空, 显示图片
	public MyButton( Bitmap bm, int x, int y, int w, int h ) : base( x, y, w, h )
	{
		BackBitmap = bm;
		//BackBitmap = SG.GetGrayBitmap( BackBitmap1 );
		
		Text = null;
		
		ElementType = E_ElementType.OnlyBitmap;
		BackBitmapAlign = E_BackBitmapLayout.Stretch;
		
		BackColor = Color.Black;
		DrawBack = false;
		EdgeColor = Color.Black;
		
		CanLock = false;
		Checked = false;
	}
	
	//构造函数 - 图片为空, 显示文本信息
	public MyButton( Color bc, string t, float fs, Color fc, int x, int y, int w, int h ) : base( x, y, w, h )
	{
		BackBitmap = null;
		
		BackColor = bc;
		
		Text = t;
		ForeColor = fc;
		FontSize = fs;
		
		ElementType = E_ElementType.OnlyString;
		DrawBack = true;
		
		EdgeColor = Color.Black;
		
		CanLock = false;
		Checked = false;
	}
	
	//=========================================================================
	
	//绘制控件
	public override void Draw()
	{
		int x = GetAbsoluteStaX();
		int y = GetAbsoluteStaY();
		
		switch( ElementType ) {
			case E_ElementType.OnlyBitmap: Draw_OnlyBitmap( x, y ); break;
			case E_ElementType.OnlyString: Draw_OnlyString( x, y ); break;
			default: Draw_Default(); break;
		}
		SG.MRectangle.Draw( EdgeColor, 1, x, y, DL_Width, DL_Height );
	}
	
	//绘制控件
	void Draw_OnlyBitmap( int x, int y )
	{
		if( DrawBack ) {
			SG.MRectangle.Fill( BackColor, x, y, DL_Width, DL_Height );
		}
		if( BackBitmapAlign == E_BackBitmapLayout.Stretch ) {
			SG.MBitmap.Draw( BackBitmap, x, y, DL_Width, DL_Height );
		}
		if( BackBitmapAlign == E_BackBitmapLayout.Center ) {
			SG.MBitmap.Draw( BackBitmap, x + DL_Width/2 - BackBitmap.Width/2, y + DL_Height/2 - BackBitmap.Height/2 );
		}
		if( isTouchDown ) {
			SG.MRectangle.Draw( BackColor, 1, x, y, DL_Width, DL_Height );
			//SG.MBitmap.Draw( BackBitmap, x + Width/2 - BackBitmap.Width/2, y + Height/2 - BackBitmap.Height/2 );
		}
		else {
			//SG.MRectangle.Fill( BackColor, x, y, Width, Height );
		}
	}
	
	//绘制控件
	void Draw_OnlyString( int x, int y )
	{
		if( isTouchDown ) {
			SG.MRectangle.Draw( BackColor, 1, x, y, DL_Width, DL_Height );
		}
		else if( Checked ) {
			//...
		}
		else {
			if( DrawBack ) {
				SG.MRectangle.Fill( BackColor, x, y, DL_Width, DL_Height );
			}
		}
		SG.MString.DrawAtCenter( Text, ForeColor, FontSize, x + DL_Width/2, y + DL_Height/2 );
	}
	
	//绘制控件
	void Draw_Default()
	{
		SG.MRectangle.Fill( Color.Red, V_StaX, V_StaY, DL_Width, DL_Height );	
	}
	
	//=========================================================================
	
	//触控按下事件
	public override bool TouchDown( int x, int y )
	{
		if( Contain( x, y ) ) {
			if( CanLock ) {
				Checked = !Checked;
			}
			if( TouchDownEvent != null ) {
				TouchDownEvent( this );
			}
			isTouchDown = true;
		}
		else {
			isTouchDown = false;
		}
		return isTouchDown;
	}
	
	//触控松开事件
	public override void TouchUp( int x, int y )
	{
		if( isTouchDown ) {
			if( TouchUpEvent != null ) {
				TouchUpEvent( this );
			}
			if( TouchClickEvent != null ) {
				TouchClickEvent( this );
			}
		}
		isTouchDown = false;
	}
	
	//触控移动事件
	public override bool TouchMove( int x, int y )
	{
		return false;
	}
	
	//==================================================================
	
	//命令解码
	public override void Decode( int Command, byte[] Buffer )
	{
		if( Command == SetText || Command == SetFont ) {
			string s = DecodeGBK( Buffer );
			if( Command == SetText ) {
				this.Text = s;
			}
			else if( Command == SetFont ) {
				Common.Font f = Common.GetFontFromString( s );
				this.FontSize = (int)(f.Size);
			}
			else {
				//...
			}
		}
		else {
			this.V_Decode( Command, Buffer );
		}
	}
}
}

