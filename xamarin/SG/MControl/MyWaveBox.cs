﻿
using System;
using n_SG;
using n_Common;
using n_SG_MyControl;

#if OS_android
using Android.Graphics;
#endif

#if OS_win
using System.Drawing;
#endif

namespace n_SG_MyWaveBox
{
public class MyWaveBox : MyControl
{
	const int v_Number = 0x20;
	
	public delegate void D_TouchDownEvent( object sender );
	public D_TouchDownEvent TouchDownEvent;
	
	public const int CMD_AddValue = 0x20;
	public const int CMD_SetChannel = 0x21;
	
	public int FontSize;
	
	int[][] DataBuffer;
	int[] Number;
	const int MaxLength = 1000;
	const int CNumber = 3;
	int UserNumber;
	int CurrentChannel;
	
	Color[] ColorList;
	
	int VisibleNumber;
	int ZeroLineY;
	
	int LastX;
	int LastY;
	
	Bitmap BackBitmap;
	Graphics g;
	bool NeedRefresh;
	
	//构造函数 - 图片为空, 显示文本信息
	public MyWaveBox( Color bc, int fs, Color fc, int x, int y, int w, int h ) : base( x, y, w, h )
	{
		BackColor = bc;
		
		ForeColor = fc;
		FontSize = fs;
		
		DataBuffer = new int[CNumber][];
		for( int i = 0; i < DataBuffer.Length; ++i ) {
			DataBuffer[i] = new int[MaxLength];
		}
		Number = new int[CNumber];
		
		CurrentChannel = 0;
		UserNumber = 3;
		
		ColorList = new Color[CNumber];
		ColorList[0] = Color.OrangeRed;
		ColorList[1] = Color.Blue;
		ColorList[2] = Color.Green;
		
		VisibleNumber = 20;
		ZeroLineY = h/2;
		NeedRefresh = false;
		n_Linker.Linker.StartRun += RefreshBaseData;
		
		RefreshBaseData();
	}
	
	//更新数据
	public void RefreshBaseData()
	{
		ZeroLineY = DL_Height/2;
		VisibleNumber = DL_Width/5;
		
		BackBitmap = new Bitmap( DL_Width, DL_Height );
		g = Graphics.FromImage( BackBitmap );
		g.SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.HighQuality;
		NeedRefresh = true;
	}
	
	//=========================================================================
	
	//绘制控件
	public override void Draw()
	{
		int x = GetAbsoluteStaX();
		int y = GetAbsoluteStaY();
		
		if( NeedRefresh ) {
			NeedRefresh = false;
			DrawData();
		}
		SG.MBitmap.Draw( BackBitmap, x, y );
	}
	
	//绘制当前数据缓冲区到背景图片中
	void DrawData()
	{
		SG.SetObject( g );
		SG.MRectangle.Fill( BackColor, 0, 0, DL_Width, DL_Height );
		//SG.MString.DrawAtLeft( Value.ToString(), ForeColor, FontSize, x + Width/2, y + Height/2 );
		//SG.MLine.Draw( ForeColor, 2, x, y, x + Width, y + Height );
		
		//绘制纵坐标尺
		int t = 1;
		while( true ) {
			int y1 = ZeroLineY - t*10;
			int y2 = ZeroLineY + t*10;
			
			if( t % 10 == 0 ) {
				SG.MLine.Draw( Color.Gray, 1, 0, y1, DL_Width, y1 );
				SG.MLine.Draw( Color.Gray, 1, 0, y2, DL_Width, y2 );
			}
			else {
				SG.MLine.Draw( Color.Silver, 1, 0, y1, DL_Width, y1 );
				SG.MLine.Draw( Color.Silver, 1, 0, y2, DL_Width, y2 );
			}
			t++;
			if( y1 < 0 && y2 > DL_Height ) {
				break;
			}
		}
		SG.MLine.Draw( Color.Black, 1, 0, ZeroLineY, DL_Width, ZeroLineY );
		
		//绘制曲线
		for( int channel = UserNumber - 1; channel >= 0; channel-- ) {
			for( int i = Number[channel] - 1, j = 0; j < VisibleNumber; j++, i-- ) {
				if( i < 0 ) {
					i = MaxLength - 1;
				}
				int n = i - 1;
				if( n < 0 ) {
					n = MaxLength - 1;
				}
				int dc = DataBuffer[channel][i];
				int dn = DataBuffer[channel][n];
				int cx = j * DL_Width / VisibleNumber;
				int nx = (j + 1) * DL_Width / VisibleNumber;
				SG.MLine.Draw( ColorList[channel], 1, cx, ZeroLineY - dc, nx, ZeroLineY - dn );
			}
		}
		SG.SetPreObject();
	}
	
	//=========================================================================
	
	//触控按下事件
	public override bool TouchDown( int x, int y )
	{
		if( Contain( x, y ) ) {
			isTouchDown = true;
			
			LastX = x;
			LastY = y;
			
			if( TouchDownEvent != null ) {
				TouchDownEvent( this );
			}
		}
		else {
			isTouchDown = false;
		}
		
		return isTouchDown;
	}
	
	//触控松开事件
	public override void TouchUp( int x, int y )
	{
		isTouchDown = false;
	}
	
	//触控移动事件
	public override bool TouchMove( int x, int y )
	{
		if( isTouchDown ) {
			ZeroLineY += (y - LastY);
			LastX = x;
			LastY = y;
			NeedRefresh = true;
		}
		return false;
	}
	
	//==================================================================
	
	//命令解码
	public override void Decode( int Command, byte[] Buffer )
	{
		if( Command == SetText || Command == SetFont ) {
			string s = DecodeGBK( Buffer );
			if( Command == SetText ) {
				if( s != "" ) {
					//...
				}
			}
			else if( Command == SetFont ) {
				Common.Font f = Common.GetFontFromString( s );
				this.FontSize = (int)(f.Size);
			}
			else {
				//...
			}
		}
		else if( Command == CMD_SetChannel ) {
			CurrentChannel = SwitchData( Buffer, 4 ) - 1;
			if( CurrentChannel < 0 ) {
				CurrentChannel = 0;
			}
		}
		else if( Command == CMD_AddValue ) {
			int Data = SwitchData( Buffer, 4 );
			DataBuffer[CurrentChannel][ Number[CurrentChannel] ] = Data;
			Number[CurrentChannel]++;
			Number[CurrentChannel] %= MaxLength;
			NeedRefresh = true;
		}
		else {
			this.V_Decode( Command, Buffer );
			ColorList[0] = ForeColor;
		}
	}
}
}

