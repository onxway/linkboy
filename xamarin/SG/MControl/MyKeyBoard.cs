﻿
using System;
using n_SG;
using n_Common;
using n_SG_MyControl;
using n_KeyBoard;
using System.Drawing;

namespace n_SG_MyKeyBoard
{
public class MyKeyBoard : MyControl
{
	const int v_MaxTick = 0x20;
	
	const int CMD_Open = 0x22;
	const int CMD_Close = 0x23;
	
	public int KeyValue;
	public string KeyList;
	public int KIgnore;
	
	//构造函数 - 图片为空, 显示文本信息
	public MyKeyBoard() : base( 0, 0, 0, 0 )
	{
		KeyValue = 0;
		KeyList = "";
		KIgnore = 0;
	}
	
	//绘制控件
	public override void Draw()
	{
		if( isHidenModule ) {
			return;
		}
		
		int x = GetAbsoluteStaX();
		int y = GetAbsoluteStaY();
		//SG.MRectangle.Draw( Color.WhiteSmoke, 1, x, y, Width, Height );
		
		SG.MRectangle.Fill( Color.Green, x, y, DL_Width, DL_Height );
		
		SG.MString.DrawAtCenter( "按键监听", Color.WhiteSmoke, 11, x + DL_Width/2, y + DL_Height/2 );
		
		if( KeyList == "" ) {
			SG.MString.DrawAtLeft( "无监听按键", Color.WhiteSmoke, 11, x + DL_Width + 5, y );
		}
		else {
			SG.MString.DrawAtLeft( "监听按键: " + KeyList, Color.WhiteSmoke, 11, x + DL_Width + 5, y );
		}
	}
	
	//触控按下事件
	public override bool TouchDown( int x, int y )
	{
		return false;
	}
	
	//触控松开事件
	public override void TouchUp( int x, int y )
	{
	}
	
	//触控移动事件
	public override bool TouchMove( int x, int y )
	{
		return false;
	}
	
	//==================================================================
	
	//命令解码
	public override void Decode( int Command, byte[] Buffer )
	{
		if( Command == SetText ) {
			KeyList = DecodeGBK( Buffer );
			if( Command == SetText ) {
				string[] cut = KeyList.Split( ' ' );
				
				SetKeyValue( cut[1] );
				KeyBoard.SetKeyEvent( KeyValue, KeyEvent, int.Parse( cut[0] ) );
			}
			else {
				//...
			}
		}
		else if( Command == CMD_Open ) {
			KeyBoard.Open( KeyValue );
		}
		else if( Command == CMD_Close ) {
			KeyBoard.Close( KeyValue );
		}
		else {
			this.V_Decode( Command, Buffer );
		}
	}
	
	protected override void ExWrite( int Addr, int Data )
	{
		if( Addr == v_MaxTick ) {
			KeyBoard.SetKeyMaxTick( KeyValue, Data );
		}
		else {
			VCodeWriteError( Addr );
		}
	}
	
	void SetKeyValue( string KeyName )
	{
		if( KeyName == "ENTER" ) {
			KeyValue = 13;
		}
		else if( KeyName == "空格" ) {
			KeyValue = 32;
		}
		else if( KeyName == "Ctrl" ) {
			KeyValue = 17;
		}
		else if( KeyName == "Alt" ) {
			KeyValue = 18;
		}
		else if( KeyName == "↑" ) {
			KeyValue = 38;
		}
		else if( KeyName == "↓" ) {
			KeyValue = 40;
		}
		else if( KeyName == "←" ) {
			KeyValue = 37;
		}
		else if( KeyName == "→" ) {
			KeyValue = 39;
		}
		else {
			KeyValue = (int)KeyName[0];
		}
	}
	
	void KeyEvent( int status )
	{
		byte ei = 0;
		if( status == 0 ) {
			ei = 4;
		}
		else if( status == 1 ) {
			ei = 1;
		}
		else if( status == 2 ) {
			ei = 2;
		}
		else {
			return;
		}
		
		int ID = this.ID;
		n_Linker.Linker.SendEventFlag( ID, ei );
	}
}
}

