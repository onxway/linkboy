﻿
using System;
using n_SG;
using n_Common;
using n_SG_MyControl;

#if OS_android
using Android.Graphics;
#endif

#if OS_win
using System.Drawing;
#endif

namespace n_SG_MyLabel
{
public class MyLabel : MyControl
{
	public delegate void D_TouchDownEvent( object sender );
	public D_TouchDownEvent TouchDownEvent;
	
	public string Text;
	public int FontSize;
	
	public Bitmap BackBitmap;
	
	enum E_ElementType
	{
		OnlyBitmap,
		OnlyString,
	}
	E_ElementType ElementType;
	public bool isTemp = false;
	
	//构造函数 - 文本信息为空, 显示图片
	public MyLabel( Bitmap bm, int x, int y, int w, int h ) : base( x, y, w, h )
	{
		BackBitmap = bm;
		
		Text = null;
		
		ElementType = E_ElementType.OnlyBitmap;
		
		BackColor = Color.Black;
		ForeColor = Color.White;
		
		EdgeColor = Color.Black;
	}
	
	//构造函数 - 图片为空, 显示文本信息
	public MyLabel( Color bc, string t, int fs, Color fc, int x, int y, int w, int h ) : base( x, y, w, h )
	{
		BackBitmap = null;
		
		BackColor = bc;
		
		Text = t;
		ForeColor = fc;
		FontSize = fs;
		
		ElementType = E_ElementType.OnlyString;
		
		EdgeColor = Color.Black;
	}
	
	//=========================================================================
	
	//绘制控件
	public override void Draw()
	{
		switch( ElementType ) {
			case E_ElementType.OnlyBitmap: Draw_OnlyBitmap(); break;
			case E_ElementType.OnlyString: Draw_OnlyString(); break;
			default: Draw_Default(); break;
		}
		
		int x = GetAbsoluteStaX();
		int y = GetAbsoluteStaY();
		SG.MRectangle.Draw( EdgeColor, 1, x, y, DL_Width, DL_Height );
	}
	
	//绘制控件
	void Draw_OnlyBitmap()
	{
		if( BackBitmap != null ) {
			int x = GetAbsoluteStaX();
			int y = GetAbsoluteStaY();
			
			//SG.MBitmap.Draw( BackBitmap, x + Width/2 - BackBitmap.Width/2, y + Height/2 - BackBitmap.Height/2 );
			SG.MBitmap.Draw( BackBitmap, x, y, DL_Width, DL_Height );
		}
		
	}
	
	//绘制控件
	void Draw_OnlyString()
	{
		int x = GetAbsoluteStaX();
		int y = GetAbsoluteStaY();
		
		SG.MRectangle.Fill( BackColor, x, y, DL_Width, DL_Height );
		SG.MString.DrawAtCenter( Text, ForeColor, FontSize, x + DL_Width/2, y + DL_Height/2 );
	}
	
	//绘制控件
	void Draw_Default()
	{
		SG.MRectangle.Fill( Color.Red, V_StaX, V_StaY, DL_Width, DL_Height );	
	}
	
	//=========================================================================
	
	//触控按下事件
	public override bool TouchDown( int x, int y )
	{
		if( Contain( x, y ) ) {
			isTouchDown = true;
			if( TouchDownEvent != null ) {
				TouchDownEvent( this );
			}
		}
		else {
			isTouchDown = false;
		}
		
		return isTouchDown;
	}
	
	//触控松开事件
	public override void TouchUp( int x, int y )
	{
		isTouchDown = false;
	}
	
	//触控移动事件
	public override bool TouchMove( int x, int y )
	{
		return false;
	}
	
	//==================================================================
	
	//命令解码
	public override void Decode( int Command, byte[] Buffer )
	{
		if( Command == SetText || Command == SetFont ) {
			string s = DecodeGBK( Buffer );
			if( Command == SetText ) {
				this.Text = s;
			}
			else if( Command == SetFont ) {
				Common.Font f = Common.GetFontFromString( s );
				this.FontSize = (int)(f.Size);
			}
			else {
				//...
			}
		}
		else {
			this.V_Decode( Command, Buffer );
		}
	}
}
}

