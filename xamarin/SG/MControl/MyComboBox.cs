﻿
using System;
using n_SG;
using n_SG_MyControl;

#if OS_android
using Android.Graphics;
#endif

#if OS_win
using System.Drawing;
#endif

namespace n_SG_MyComboBox
{
public class MyComboBox : MyControl
{
	public delegate void d_TouchDown( object sender );
	public d_TouchDown TouchDownEvent;
	
	public delegate void d_TouchUp( object sender );
	public d_TouchUp TouchUpEvent;
	
	public delegate void d_TouchClick( object sender );
	public d_TouchClick TouchClickEvent;
	
	public string Text;
	public float FontSize;
	
	bool isActive;
	int PerHeight;
	
	string[] ItemList;
	int ItemLength;
	
	int v_SelectedIndex;
	public int SelectedIndex {
		set {
			v_SelectedIndex = value;
			if( value != -1 ) {
				Text = ItemList[value];
			}
		}
		get {
			return v_SelectedIndex;
		}
	}
	
	const int ItemStartY = 0;
	
	bool TempSelected;
	
	//构造函数
	public MyComboBox( Color bc, float fs, Color fc, int x, int y, int w, int h ) : base( x, y, w, h )
	{
		BackColor = bc;
		
		Text = "";
		ForeColor = fc;
		FontSize = fs;
		
		isActive = false;
		
		PerHeight = h;
		
		ItemList = new string[50];
		ItemLength = 0;
		
		SelectedIndex = -1;
	}
	
	//清空条目
	public void Clear()
	{
		ItemLength = 0;
	}
	
	//添加条目
	public void Add( string ItemName )
	{
		ItemList[ItemLength] = ItemName;
		ItemLength++;
	}
	
	//添加条目
	public void AddList( string[] ItemNameList )
	{
		for( int i = 0; i < ItemNameList.Length; i++ ) {
			ItemList[ItemLength] = ItemNameList[i];
			ItemLength++;
		}
	}
	
	//=========================================================================
	
	//绘制控件
	public override void Draw()
	{
		int x = GetAbsoluteStaX();
		int y = GetAbsoluteStaY();
		
		SG.MRectangle.Fill( BackColor, x, y, DL_Width, DL_Height );
		if( isTouchDown ) {
			//SG.MRectangle.Draw( ForeColor, 1, x, y, Width, Height );
		}
		SG.MString.DrawAtLeft( Text, ForeColor, FontSize, x + 5, y + PerHeight/2 );
		SG.MLine.Draw( Color.Gray, 2, x + DL_Width - 40 - 5, y + PerHeight/2 - 10, x + DL_Width - 20 - 5, y + PerHeight/2 + 10 );
		SG.MLine.Draw( Color.Gray, 2, x + DL_Width - 5, y + PerHeight/2 - 10, x + DL_Width - 20 - 5, y + PerHeight/2 + 10 );
		
		if( isActive ) {
			for( int i = 0; i < ItemLength; ++i ) {
				SG.MLine.Draw( Color.SlateBlue, 1, x, y + PerHeight + ItemStartY + PerHeight*i, x + DL_Width, y + PerHeight + ItemStartY + PerHeight*i );
				
				if( i == SelectedIndex ) {
					SG.MRectangle.Fill( Color.SlateBlue, x, y + PerHeight + ItemStartY + PerHeight*i, DL_Width, PerHeight );
					SG.MString.DrawAtLeft( ItemList[i], Color.WhiteSmoke, FontSize, x + 5, y + PerHeight + ItemStartY + PerHeight*i + PerHeight/2 );
				}
				else {
					SG.MString.DrawAtLeft( ItemList[i], ForeColor, FontSize, x + 5, y + PerHeight + ItemStartY + PerHeight*i + PerHeight/2 );
				}
			}
			SG.MRectangle.Draw( Color.SlateBlue, 1, x, y + PerHeight + ItemStartY, DL_Width, PerHeight*ItemLength );
		}
	}
	
	//=========================================================================
	
	//触控按下事件
	public override bool TouchDown( int x, int y )
	{
		if( Contain( x, y ) ) {
			if( TouchDownEvent != null ) {
				TouchDownEvent( this );
			}
			isTouchDown = true;
			
			if( !isActive ) {
				isActive = true;
				TempSelected = false;
				DL_Height = PerHeight + ItemStartY + PerHeight * ItemLength;
			}
			else {
				TempSelected = true;
			}
			Refresh( x, y );
		}
		else {
			isTouchDown = false;
			
			if( isActive ) {
				isActive = false;
				DL_Height = PerHeight;
			}
		}
		
		return isTouchDown;
	}
	
	//触控松开事件
	public override void TouchUp( int x, int y )
	{
		if( isTouchDown ) {
			if( TempSelected && SelectedIndex != -1 ) {
				Text = ItemList[SelectedIndex];
				isActive = false;
				DL_Height = PerHeight;
			}
			if( TouchUpEvent != null ) {
				TouchUpEvent( this );
			}
			if( TouchClickEvent != null ) {
				TouchClickEvent( this );
			}
		}
		isTouchDown = false;
	}
	
	//触控移动事件
	public override bool TouchMove( int x, int y )
	{
		if( !Contain( x, y ) ) {
			return false;
		}
		Refresh( x, y );
		return true;
	}
	
	void Refresh( int x, int y )
	{
		for( int i = 0; i < ItemLength; ++i ) {
			int SY = V_StaY + PerHeight + ItemStartY + PerHeight*i;
			if( x >= V_StaX && x < V_StaX + DL_Width && y >= SY && y < SY + PerHeight ) {
				SelectedIndex = i;
				TempSelected = true;
				break;
			}
		}
	}
	
	//==================================================================
	
	//命令解码
	public override void Decode( int Command, byte[] Buffer )
	{
		this.V_Decode( Command, Buffer );
	}
}
}

