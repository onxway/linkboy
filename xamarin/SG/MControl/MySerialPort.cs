﻿
using System;
using n_SG;
using n_Common;
using n_SG_MyControl;
using n_SG_MyComboBox;

#if OS_android
using Android.Graphics;
#endif

#if OS_win
using c_PlaySound;
using System.IO.Ports;
using System.Drawing;
//using System.Windows.Forms;
using Dongzr.MidiLite;
#endif

namespace n_SG_MySerialPort
{
public class MySerialPort : MyControl
{
	public delegate void d_TouchDown( object sender );
	public d_TouchDown TouchDownEvent;
	
	int[] DL;
	
	const int CMD_SetValue = 0x10;
	const int CMD_SendData = 0x11;
	const int CMD_SetBaud = 0x12;
	
	const int CMD_SetStart = 0x13;
	const int CMD_SetN = 0x14;
	const int CMD_SetFont = 0x15;
	
	const int v_Data = 0x20;
	const int v_Number = 0x21;
	const int v_CData = 0x22;
	
	public float FontSize;
	
	MmTimer t;
	
	//构造函数 - 图片为空, 显示文本信息
	public MySerialPort( Color bc, float fs, Color fc, int x, int y, int w, int h ) : base( x, y, w, h )
	{
		SendBuffer = new byte[100];
		Baud = 9600;
		
		RBuffer = new byte[MaxLength];
		Start = 0;
		Length = 0;
		
		BackColor = bc;
		ForeColor = fc;
		FontSize = fs;
		
		DL = new int[ 256 ];
		
		t = new MmTimer();
		t.Interval = 10;
		t.Tick += new EventHandler( TimerTick );
		
		if( SG.Running ) {
			t.Start();
		}
	}
	
	//=========================================================================
	
	//绘制控件
	public override void Draw()
	{
		int x = GetAbsoluteStaX();
		int y = GetAbsoluteStaY();
		SG.MRectangle.Fill( BackColor, x, y,DL_Width, DL_Height );
		SG.MString.DrawAtCenter( "设置串口", ForeColor, FontSize, x + DL_Width/2, y + DL_Height / 2 );
	}
	
	//=========================================================================
	
	//触控按下事件
	public override bool TouchDown( int x, int y )
	{
		if( Contain( x, y ) ) {
			if( TouchDownEvent != null ) {
				TouchDownEvent( this );
			}
			isTouchDown = true;
		}
		else {
			isTouchDown = false;
		}
		
		return isTouchDown;
	}
	
	//触控松开事件
	public override void TouchUp( int x, int y )
	{
	}
	
	//触控移动事件
	public override bool TouchMove( int x, int y )
	{
		return false;
	}
	
	//==================================================================
	
	//命令解码
	public override void Decode( int Command, byte[] Buffer )
	{
		if( Command == CMD_SetValue ) {
			string s = DecodeGBK( Buffer );
			Open( s, false );
		}
		else if( Command == CMD_SendData ) {
			int Data = SwitchData( Buffer, 4 );
			SendBuffer[0] = (byte)Data;
			p.Write( SendBuffer, 0, 1 );
		}
		else if( Command == CMD_SetBaud ) {
			int Data = SwitchData( Buffer, 4 );
			Baud = Data;
			if( p != null ) {
				p.BaudRate = Baud;
			}
		}
		else if( Command == 0x80 + v_Data ) {
			DL[v_Data] = RBuffer[Start];
			Start++;
			Start %= MaxLength;
			this.V_Decode( Command, Buffer );
		}
		else if( Command == 0x80 + v_Number ) {
			DL[v_Number] = Length - Start; 
			if( DL[v_Number] < 0 ) {
				DL[v_Number] += MaxLength;
			}
			this.V_Decode( Command, Buffer );
		}
		else if( Command == CMD_SetStart ) {
			cindex = Start;
		}
		else if( Command == CMD_SetN ) {
			int Data = SwitchData( Buffer, 4 );
			N = Data;
		}
		else if( Command == 0x80 + v_CData ) {
			int i = (cindex + N) % MaxLength;
			DL[v_CData] = RBuffer[i];
			this.V_Decode( Command, Buffer );
		}
		else if( Command == CMD_SetFont ) {
			string s = DecodeGBK( Buffer );
			Common.Font f = Common.GetFontFromString( s );
			this.FontSize = (int)(f.Size);
		}
		else {
			this.V_Decode( Command, Buffer );
		}
	}
	
	protected override int ExRead( int Addr )
	{
		switch( Addr ) {
			case v_Data:	return DL[v_Data];
			case v_Number:	return DL[v_Number];
			case v_CData:	return DL[v_CData];
			default:	VCodeReadError( Addr ); return 0;
		}
	}
	
	//==========================================
	
	SerialPort p;
	byte[] SendBuffer;
	int Baud;
	
	const int MaxLength = 1000;
	byte[] RBuffer;
	int Start;
	int Length;
	
	int cindex;
	int N;
	
	//打开串口
	public void Open( string Name, bool DTR )
	{
		try {
			if( p != null && p.IsOpen ) {
				p.Close();
			}
			
			//初始化硬件串口
			p = new System.IO.Ports.SerialPort( Name );
			p.BaudRate = Baud;
			p.DataBits = 8;
			p.Parity = System.IO.Ports.Parity.None;
			p.StopBits = System.IO.Ports.StopBits.One;
			p.ReadTimeout = 1;
			
			if( DTR ) {
				p.RtsEnable = true;
				p.DtrEnable = true;
			}
			
			p.Open();
		}
		catch {
			n_SYS.SYS.AddMessage( "SerialPort Open Error\n" );
		}
	}
	
	void TimerTick( object sender, EventArgs e )
	{
		try {
			if( p != null ) {
				RBuffer[Length] = (byte)p.ReadByte();
				Length++;
				Length %= MaxLength;
			}
		}
		catch {
			
		}
	}
}
}



