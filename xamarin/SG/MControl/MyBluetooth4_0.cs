﻿
using System;
using n_SG;
using n_Common;
using n_SG_MyControl;
using n_SG_MyComboBox;

#if OS_android
using Android.Graphics;
using n_Ble;
#endif

#if OS_win
using c_PlaySound;
using System.IO.Ports;
using System.Drawing;
//using System.Windows.Forms;
using Dongzr.MidiLite;
#endif

namespace n_SG_MyBle40
{
public class MyBle40 : MyControl
{
	public delegate void d_TouchDown( object sender );
	public d_TouchDown TouchDownEvent;
	
	public int[] DL;
	
	const int CMD_SetValue = 0x10;
	const int CMD_SendData = 0x11;
	
	const int CMD_SetStart = 0x13;
	const int CMD_SetN = 0x14;
	const int CMD_SetFont = 0x15;
	
	const int v_Data = 0x20;
	const int v_Number = 0x21;
	const int v_CData = 0x22;
	
	public float FontSize;
	
	//构造函数 - 图片为空, 显示文本信息
	public MyBle40( Color bc, float fs, Color fc, int x, int y, int w, int h ) : base( x, y, w, h )
	{
		SendBuffer = new byte[100];
		
		RBuffer = new byte[MaxLength];
		Start = 0;
		Length = 0;
		
		DL = new int[ 256 ];
		
		BackColor = bc;
		ForeColor = fc;
		FontSize = fs;
		
		if( SG.Running ) {
			//t.Start();
		}
		#if OS_android
		Ble.ReceiveData += this.DataReceived;
		#endif
	}
	
	//=========================================================================
	
	//绘制控件
	public override void Draw()
	{
		int x = GetAbsoluteStaX();
		int y = GetAbsoluteStaY();
		SG.MRectangle.Fill( BackColor, x, y, DL_Width, DL_Height );
		SG.MString.DrawAtCenter( "选择蓝牙4.0设备", ForeColor, FontSize, x + DL_Width/2, y + DL_Height / 2 );
	}
	
	//=========================================================================
	
	//触控按下事件
	public override bool TouchDown( int x, int y )
	{
		if( Contain( x, y ) ) {
			if( TouchDownEvent != null ) {
				TouchDownEvent( this );
			}
			isTouchDown = true;
		}
		else {
			isTouchDown = false;
		}
		return isTouchDown;
	}
	
	//触控松开事件
	public override void TouchUp( int x, int y )
	{
		
	}
	
	//触控移动事件
	public override bool TouchMove( int x, int y )
	{
		return false;
	}
	
	//==================================================================
	
	//命令解码
	public override void Decode( int Command, byte[] Buffer )
	{
		if( Command == CMD_SetValue ) {
			string s = DecodeGBK( Buffer );
			//Open( s );
		}
		else if( Command == CMD_SendData ) {
			int Data = SwitchData( Buffer, 4 );
			SendBuffer[0] = (byte)Data;
			#if OS_android

			Ble.SendByte( SendBuffer[0] );
			
			/*
			//System.Threading.Thread.Sleep( 500 );
			SendBuffer[0] = 0xAA;
			SendBuffer[1] = 1;
			SendBuffer[2] = 52;
			SendBuffer[3] = 1;
			Ble.SendBuffer( SendBuffer, 4 );
			n_SYS.SYS.AddMessage( "123\n" );
			*/

			#endif
		}
		else if( Command == 0x80 + v_Data ) {
			DL[v_Data] = RBuffer[Start];
			Start++;
			Start %= MaxLength;
			this.V_Decode( Command, Buffer );
		}
		else if( Command == 0x80 + v_Number ) {
			DL[v_Number] = Length - Start;
			if( DL[v_Number] < 0 ) {
				DL[v_Number] += MaxLength;
			}
			this.V_Decode( Command, Buffer );
		}
		else if( Command == CMD_SetStart ) {
			cindex = Start;
		}
		else if( Command == CMD_SetN ) {
			int Data = SwitchData( Buffer, 4 );
			N = Data;
		}
		else if( Command == 0x80 + v_CData ) {
			int i = (cindex + N) % MaxLength;
			DL[v_CData] = RBuffer[i];
			this.V_Decode( Command, Buffer );
		}
		if( Command == CMD_SetFont ) {
			string s = DecodeGBK( Buffer );
			Common.Font f = Common.GetFontFromString( s );
			this.FontSize = (int)(f.Size);
		}
		else {
			this.V_Decode( Command, Buffer );
		}
	}
	
	//==========================================
	
	byte[] SendBuffer;
	
	const int MaxLength = 1000;
	byte[] RBuffer;
	int Start;
	int Length;
	
	int cindex;
	int N;
	
	void DataReceived( byte b )
	{
			RBuffer[Length] = b;
			Length++;
			Length %= MaxLength;
	}
}
}



