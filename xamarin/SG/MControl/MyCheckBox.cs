﻿
using System;
using n_SG;
using n_Common;
using n_SG_MyControl;

#if OS_android
using Android.Graphics;
#endif

#if OS_win
using System.Drawing;
#endif

namespace n_SG_MyCheckBox
{
public class MyCheckBox : MyControl
{
	const int v_Checked = 0x20;
	
	public delegate void d_CheckChanged( object sender );
	public d_CheckChanged CheckChanged;
	
	public string Text;
	
	public float FontSize;
	
	public bool Checked;
	
	//构造函数 - 图片为空, 显示文本信息
	public MyCheckBox( Color bc, string t, float fs, Color fc, int x, int y, int w, int h ) : base( x, y, w, h )
	{
		BackColor = bc;
		
		Text = t;
		ForeColor = fc;
		FontSize = fs;
		
		Checked = false;
	}
	
	//=========================================================================
	
	//绘制控件
	public override void Draw()
	{
		int x = GetAbsoluteStaX();
		int y = GetAbsoluteStaY();
		
		if( isTouchDown ) {
			SG.MRectangle.Draw( ForeColor, 1, x, y, DL_Width, DL_Height );
		}
		else {
			SG.MRectangle.Fill( BackColor, x, y, DL_Width, DL_Height );
		}
		
		int SQW = DL_Height / 2;
		SG.MRectangle.Draw( ForeColor, 2, x, y + DL_Height/2 - SQW/2, SQW, SQW );
		if( Checked ) {
			SG.MRectangle.Fill( ForeColor, x + 2, y + 2 + DL_Height/2 - SQW/2, SQW-4, SQW-4 );
		}
		
		SG.MString.DrawAtLeft( Text, ForeColor, FontSize, x + SQW + 10, y + DL_Height/2 );
	}
	
	//=========================================================================
	
	//触控按下事件
	public override bool TouchDown( int x, int y )
	{
		if( Contain( x, y ) ) {
			isTouchDown = true;
			Checked = !Checked;
			if( CheckChanged != null ) {
				CheckChanged( this );
			}
		}
		else {
			isTouchDown = false;
		}
		
		return isTouchDown;
	}
	
	//触控松开事件
	public override void TouchUp( int x, int y )
	{
		if( isTouchDown ) {
			
		}
		isTouchDown = false;
	}
	
	//触控移动事件
	public override bool TouchMove( int x, int y )
	{
		return false;
	}
	
	//==================================================================
	
	//命令解码
	public override void Decode( int Command, byte[] Buffer )
	{
		if( Command == SetText || Command == SetFont ) {
			string s = DecodeGBK( Buffer );
			if( Command == SetText ) {
				this.Text = s;
			}
			else if( Command == SetFont ) {
				Common.Font f = Common.GetFontFromString( s );
				this.FontSize = (int)(f.Size);
			}
			else {
				//...
			}
		}
		else {
			this.V_Decode( Command, Buffer );
		}
	}
	
	protected override int ExRead( int Addr )
	{
		switch( Addr ) {
			case v_Checked:	return GetInt( this.Checked );
			default:	VCodeReadError( Addr ); return 0;
		}
	}
	
	protected override void ExWrite( int Addr, int Data )
	{
		switch( Addr ) {
				case v_Checked:	this.Checked = GetBool( Data ); break;
			default:	VCodeWriteError( Addr ); break;
		}
	}
}
}

