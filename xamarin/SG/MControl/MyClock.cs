﻿
using System;
using n_SG;
using n_Common;
using n_SG_MyControl;

#if OS_android
using Android.Graphics;
#endif

#if OS_win
using System.Drawing;
#endif

namespace n_SG_MyClock
{
public class MyClock : MyControl
{
	public string Text;
	public int FontSize;
	
	//是否绘制边框
	public bool DrawEdge;
	public Color EdgeColor;
	
	int LastSecond;
	
	const int v_Year = 0x20;
	int DL_Year;
	const int v_Month = 0x21;
	int DL_Month;
	const int v_Day = 0x22;
	int DL_Day;
	const int v_Hour = 0x23;
	int DL_Hour;
	const int v_Minute = 0x24;
	int DL_Minute;
	const int v_Second = 0x25;
	int DL_Second;
	const int v_DayOfWeek = 0x26;
	int DL_DayOfWeek;
	
	//构造函数 - 图片为空, 显示文本信息
	public MyClock( Color bc, int fs, Color fc, int x, int y, int w, int h ) : base( x, y, w, h )
	{
		BackColor = bc;
		
		ForeColor = fc;
		FontSize = fs;
		
		DrawEdge = false;
		EdgeColor = Color.Black;
		
		LastSecond = 0;
		n_M.M.SysTick += this.SysTick;
	}
	
	//=========================================================================
	
	//绘制控件
	public override void Draw()
	{
		int x = GetAbsoluteStaX();
		int y = GetAbsoluteStaY();
		
		if( DrawEdge ) {
			SG.MRectangle.Draw( EdgeColor, 1, x, y, DL_Width, DL_Height );
		}
		SG.MRectangle.Fill( BackColor, x, y, DL_Width, DL_Height );
		SG.MString.DrawAtCenter( System.DateTime.Now.ToString( "F" ), ForeColor, FontSize, x + DL_Width/2, y + DL_Height/2 );
	}
	
	//=========================================================================
	
	//触控按下事件
	public override bool TouchDown( int x, int y )
	{
		if( Contain( x, y ) ) {
			isTouchDown = true;
		}
		else {
			isTouchDown = false;
		}
		
		return isTouchDown;
	}
	
	//触控松开事件
	public override void TouchUp( int x, int y )
	{
		isTouchDown = false;
	}
	
	//触控移动事件
	public override bool TouchMove( int x, int y )
	{
		return false;
	}
	
	//==================================================================
	
	//命令解码
	public override void Decode( int Command, byte[] Buffer )
	{
		if( Command == SetText || Command == SetFont ) {
			string s = DecodeGBK( Buffer );
			if( Command == SetText ) {
				this.Text = s;
			}
			else if( Command == SetFont ) {
				Common.Font f = Common.GetFontFromString( s );
				this.FontSize = (int)(f.Size);
			}
			else {
				//...
			}
		}
		else {
			this.V_Decode( Command, Buffer );
		}
	}
	
	protected override int ExRead( int Addr )
	{
		switch( Addr ) {
			case v_Year:		return DL_Year;
			case v_Month:		return DL_Month;
			case v_Day:			return DL_Day;
			case v_Hour:		return DL_Hour;
			case v_Minute:		return DL_Minute;
			case v_Second:		return DL_Second;
			case v_DayOfWeek:	return DL_DayOfWeek;
			default:	VCodeReadError( Addr ); return 0;
		}
	}
	
	void SysTick()
	{
		if( LastSecond != System.DateTime.Now.Second ) {
			LastSecond = System.DateTime.Now.Second;
			
			//这里更新各个数据
			DL_Year = System.DateTime.Now.Year;
			DL_Month = System.DateTime.Now.Month;
			DL_Day = System.DateTime.Now.Day;
			DL_Hour = System.DateTime.Now.Hour;
			DL_Minute = System.DateTime.Now.Minute;
			DL_Second = System.DateTime.Now.Second;
			DL_DayOfWeek = (int)System.DateTime.Now.DayOfWeek;
			
			n_Linker.Linker.SendEventFlag( this.ID, 0x01 );
			
			n_SYS.SYS.Refresh();
		}
	}
}
}

