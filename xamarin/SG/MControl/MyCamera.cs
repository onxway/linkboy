﻿
using System;
using n_SG;
using n_Common;
using n_SG_MyControl;

#if OS_android
using Android.Graphics;
#endif

#if OS_win
using System.Drawing;
#endif

namespace n_SG_MyCamera
{
public class MyCamera : MyControl
{
	public delegate void D_TouchDownEvent( object sender );
	public D_TouchDownEvent TouchDownEvent;
	
	//摄像机的标准距离(通过摄像机透镜参数可计算出)
	int StandardD;
	
	//摄像机镜头距离场景标准距离的相对偏移量(允许正数和负数)
	const int v_Offset = 0x20;
	int Offset;
	
	//摄像机镜头的角度, 当角度大于零时顺时针旋转(场景则是逆时针旋转);
	const int v_Angle = 0x21;
	int Angle;
	
	//是否绘制边框
	public bool DrawEdge;
	public Color EdgeColor;
	
	public string Text;
	public int FontSize;
	
	//构造函数 - 图片为空, 显示文本信息
	public MyCamera( Color bc, string t, int fs, Color fc, int x, int y, int w, int h ) : base( x, y, w, h )
	{
		BackColor = bc;
		
		Text = t;
		ForeColor = fc;
		FontSize = fs;
		
		EdgeColor = Color.Black;
		
		StandardD = 1000;
	}
	
	//=========================================================================
	
	//绘制控件
	public override void Draw()
	{
		if( EditEnable ) {
			int x = GetAbsoluteStaX();
			int y = GetAbsoluteStaY();
			SG.MRectangle.Draw( EdgeColor, 1, x, y, DL_Width, DL_Height );
		}
	}
	
	//=========================================================================
	
	//触控按下事件
	public override bool TouchDown( int x, int y )
	{
		if( Contain( x, y ) ) {
			isTouchDown = true;
			if( TouchDownEvent != null ) {
				TouchDownEvent( this );
			}
		}
		else {
			isTouchDown = false;
		}
		
		return isTouchDown;
	}
	
	//触控松开事件
	public override void TouchUp( int x, int y )
	{
		isTouchDown = false;
	}
	
	//触控移动事件
	public override bool TouchMove( int x, int y )
	{
		return false;
	}
	
	//==================================================================
	
	//命令解码
	public override void Decode( int Command, byte[] Buffer )
	{
		if( Command == SetText || Command == SetFont ) {
			string s = DecodeGBK( Buffer );
			if( Command == SetText ) {
				this.Text = s;
			}
			else if( Command == SetFont ) {
				Common.Font f = Common.GetFontFromString( s );
				this.FontSize = (int)(f.Size);
			}
			else {
				//...
			}
		}
		else {
			//反复刷新Rol, 其实没有必要
			//DL_RolOffsetX = DL_Width/2;
			//DL_RolOffsetY = DL_Height/2;
			
			this.V_Decode( Command, Buffer );
			if( Command < 128 ) {
				RefreshToSys();
			}
		}
	}
	
	protected override int ExRead( int Addr )
	{
		switch( Addr ) {
			case v_Offset:	return Offset;
			case v_Angle:	return Angle;
			default:	VCodeReadError( Addr ); return 0;
		}
	}
	
	protected override void ExWrite( int Addr, int Data )
	{
		switch( Addr ) {
			case v_Offset:	Offset = Data; break;
			case v_Angle:	Angle = Data; break;
			default:	VCodeWriteError( Addr ); break;
		}
	}
	
	//刷新控件参数到系统中
	public void RefreshToSys()
	{
		if( !EditEnable ) {
			n_M.M.CamMidX = this.V_MidX;
			n_M.M.CamMidY = this.V_MidY;
			n_M.M.CamScale = (float)(StandardD - Offset) / (StandardD);
			n_M.M.CamAngle = (float)Angle;
		}
	}
}
}

