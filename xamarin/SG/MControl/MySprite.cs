﻿
using System;
using n_SG;
using n_Common;
using n_SG_MyControl;
using n_ValueSet;

#if OS_android
using Android.Graphics;
#endif

#if OS_win
using System.Drawing;
using System.Drawing.Imaging;
using System.Drawing.Drawing2D;
#endif

namespace n_SG_MySprite
{
public class MySprite : MyControl
{
	public delegate void d_TouchDown( object sender );
	public d_TouchDown TouchDownEvent;
	
	public delegate void d_TouchUp( object sender );
	public d_TouchUp TouchUpEvent;
	
	public int TargetID;
	
	public string Text;
	public int FontSize;
	
	public Bitmap[] BackBitmapList;
	public int ImageNumber;
	
	string v_ExtendValue;
	public string ExtendValue {
		get {
			return v_ExtendValue;
		}
		set {
			if( v_ExtendValue != value ) {
				v_ExtendValue = value;
				
				ValueSet.SetValue( v_ExtendValue );
				
				//string[] cut = v_ExtendValue.Split( '*' );
				for( int i = 0; i < ValueSet.FileList.Length; ++i ) {
					FileError = false;
					string fp = n_CommonData.CommonData.GetFullPath( ValueSet.FileList[i] );
					if( fp != null ) {
						
						Image ti = Image.FromFile( fp );
						BackBitmapList[ImageNumber] = new Bitmap( ti );
						ti.Dispose();
					}
					else {
						//这里应该显示一个错误的图片标志, 表示图片未找到
						BackBitmapList[ImageNumber] = null;
						FileError = true;
					}
					ImageNumber++;
				}
			}
		}
	}
	
	bool FileError;
	
	const int CMD_AddBitmap = 0x20;
	const int CMD_Run = 0x30;
	const int CMD_Undo = 0x31;
	const int CMD_StopSay = 0x32;
	const int CMD_thread = 0x33;
	public const int CMD_SetID = 0x34;
	
	public int Scale;
	const int v_Scale = 0x40;
	
	public int Angle;
	const int v_Angle = 0x41;
	
	public int XScale;
	const int v_XScale = 0x42;
	
	public int YScale;
	const int v_YScale = 0x43;
	
	public int Opacity;
	const int v_Opacity = 0x44;
	
	public int ImageIndex;
	const int v_ImageIndex = 0x45;
	
	public bool Alive;
	const int v_Alive = 0x46;
	
	//这两个属性已经过时了
	const int v_OS_RolX = 0x47;
	const int v_OS_RolY = 0x48;
	
	public int HitID;
	const int v_HitID = 0x50;
	
	// 0:不碰撞  1:到达屏幕边缘触发碰撞  2:完全离开屏幕后触发碰撞
	public int DL_EageHitType;
	const int v_EageHitType = 0x51;
	
	public bool Hited;
	const int v_Hited = 0x52;
	
	int Bounce;
	const int v_Bounce = 0x53;
	
	public int XSpeed;
	const int v_XSpeed = 0x54;
	public int YSpeed;
	const int v_YSpeed = 0x55;
	
	int FowardAngle;
	const int v_FowardAngle = 0x56;
	
	
	int u_MidX;
	int u_MidY;
	int u_Width;
	int u_Height;
	int u_Scale;
	int u_Angle;
	int u_XScale;
	int u_YScale;
	bool isHited;
	
	public GraphicsPath myGraphicsPath;
	int x0;
	int y0;
	int x1;
	int y1;
	int x2;
	int y2;
	int x3;
	int y3;
	
	//float fX;
	//float fY;
	
	bool RunDown;
	bool RunUp;
	bool RunLeft;
	bool RunRight;
	
	//构造函数 - 文本信息为空, 显示图片
	public MySprite( int x, int y, int w, int h ) : base( x, y, w, h )
	{
		BackBitmapList = new Bitmap[ 50 ];
		ImageNumber = 1;
		
		FontSize = 12;
		Text = null;
		
		BackColor = Color.Black;
		ForeColor = Color.White;
		
		XScale = 100;
		YScale = 100;
		Scale = 100;
		Angle = 0;
		
		FileError = false;
		
		isHited = false;
		
		ImageIndex = 1;
		
		Opacity = 100;
		
		Alive = true;
		
		DL_EageHitType = 0;
		
		Bounce = -1;
		
		XSpeed = 0;
		YSpeed = 0;
		
		FowardAngle = 0;
		
		RunUp = false;
		RunDown = false;
		RunLeft = false;
		RunRight = false;
		
		myGraphicsPath = new GraphicsPath();
		
		n_Linker.Linker.StartRun += RefreshBaseData;
		n_M.M.AfterDraw += DrawMes;
		
		if( SG.Running ) {
			n_M.M.SpriteTimer.Tick += new EventHandler( SysTimerTick );
		}
	}
	
	//更新数据
	void RefreshBaseData()
	{
		RefreshHitPoint();
		RefreshSafeLocation();
	}
	
	//=========================================================================
	
	//绘制控件
	public override void Draw()
	{
		if( !Alive ) {
			return;
		}
		lock( n_SYS.SYS.V.InnerChanging ) {
		int rolx = GetAbsoluteRolX();
		int roly = GetAbsoluteRolY();
		
		if( BackBitmapList[ImageIndex] != null ) {
			//SG.MBitmap.Draw( BackBitmap, x + Width/2 - BackBitmap.Width/2, y + Height/2 - BackBitmap.Height/2 );
			
			int w = DL_Width * Scale / 100 * XScale / 100;
			int h = DL_Height * Scale / 100 * YScale / 100;
			int nsx = rolx - DL_RolOffsetX * Scale / 100 * XScale / 100;
			int nsy = roly - DL_RolOffsetY * Scale / 100 * YScale / 100;
			
			GraphicsState gs = SG.g.Save();
			SG.Rotate( rolx, roly, -(Angle - FowardAngle) );
			
			
			
			//不要再用透明度了, 太慢了!!!!
			//SG.MBitmap.Draw_slow( BackBitmapList[ImageIndex], nsx, nsy, w, h, Opacity );
			SG.g.DrawImage( BackBitmapList[ImageIndex], nsx, nsy, w, h );
			
			
			
			//绘制角色边框
			//SG.MRectangle.Draw( Color.Green, 1, nsx, nsy, w, h );
			
			//SG.Rotate( x + DL_RolOffsetX, y + DL_RolOffsetY, (Angle - FowardAngle) );
			SG.g.Restore( gs );
			
			//测试: 绘制碰撞边界
			if( DebugMode ) {
				SG.MLine.Draw( Color.Red, 1, x0, y0, x1, y1 );
				SG.MLine.Draw( Color.Red, 1, x1, y1, x2, y2 );
				SG.MLine.Draw( Color.Red, 1, x2, y2, x3, y3 );
				SG.MLine.Draw( Color.Red, 1, x3, y3, x0, y0 );
			}
		}
		if( FileError ) {
			SG.MRectangle.Draw( Color.Red, 1, GetAbsoluteStaX(), GetAbsoluteStaY(), DL_Width, DL_Height );
			SG.MLine.Draw( Color.Red, 1, GetAbsoluteStaX(), GetAbsoluteStaY(), GetAbsoluteStaX() + DL_Width, GetAbsoluteStaY() + DL_Height );
			SG.MLine.Draw( Color.Red, 1, GetAbsoluteStaX(), GetAbsoluteStaY(), GetAbsoluteStaX() + DL_Width, GetAbsoluteStaY() - DL_Height );
		}
		}
	}
	
	//绘制外形
	public void DrawEage()
	{
		int x = GetAbsoluteStaX();
		int y = GetAbsoluteStaY();
		if( BackBitmapList[ImageIndex] != null ) {
			//SG.MBitmap.Draw( BackBitmap, x + Width/2 - BackBitmap.Width/2, y + Height/2 - BackBitmap.Height/2 );
			
			int w = DL_Width * Scale / 100 * XScale / 100;
			int h = DL_Height * Scale / 100 * YScale / 100;
			
			int sx = DL_RolOffsetX * Scale / 100 * XScale / 100;
			int sy = DL_RolOffsetY * Scale / 100 * YScale / 100;
			
			//SG.Rotate( x + sx, y + sy, -(Angle - FowardAngle) );
			if( !SG.Running ) {
				//SG.MRectangle.Draw( Color.YellowGreen, 1, x-w/2, y-h/2, w, h );
			}
			//SG.Reset();
		}
		else {
			if( !SG.Running ) {
				SG.MRectangle.Draw( Color.Red, 1, x, y, DL_Width, DL_Height );
			}
		}
	}
	
	//绘制扩展元素
	public void DrawMes()
	{
		//Text = "奇怪了, 圣诞老人怎么还不出来呢, 我都快要急死了!!!!$%^*(&^%##@@ ...";
		
		if( Text == "" || Text == null ) {
			return;
		}
		int x = GetAbsoluteMidX();
		int y = GetAbsoluteMidY();
		
		int ww, hh;
		SG.MString.MeasureSize( Text, FontSize, 200, out ww, out hh );
		
		int tw = ww*1414/1100 + 10;
		int th = hh*1414/800 + 10;
		
		int rw = DL_Width * Scale / 100 * XScale / 100;
		int rh = DL_Height * Scale / 100 * YScale / 100;
		
		if( x < n_M.M.V_CamWidth/2 ) {
			x = x + rw/2;
		}
		else {
			x = x - rw/2 - tw;
		}
		if( y < n_M.M.V_CamHeight / 2 ) {
			y = y + rh/2;
		}
		else {
			y = y - rh/2 - th;
		}
		
		SG.MEllipse.Fill( Color.WhiteSmoke, x, y, tw, th );
		SG.MEllipse.Draw( Color.Gray, 1, x, y, tw, th );
		//SG.MRectangle.Fill( Color.WhiteSmoke, x, y, ww, hh );
		SG.MString.DrawAtRectangle( Text, Color.Black, FontSize, x + (tw-ww)/2, y + (th-hh)/2, ww, hh );
	}
	
	//=========================================================================
	
	//触控按下事件
	public override bool TouchDown( int x, int y )
	{
		if( !Alive ) {
			return false;
		}
		if( Contain( x, y ) ) {
			isTouchDown = true;
			if( TouchDownEvent != null ) {
				TouchDownEvent( this );
			}
		}
		else {
			isTouchDown = false;
		}
		
		return isTouchDown;
	}
	
	//触控松开事件
	public override void TouchUp( int x, int y )
	{
		if( !Alive ) {
			return;
		}
		if( isTouchDown ) {
			if( TouchUpEvent != null ) {
				TouchUpEvent( this );
			}
			isTouchDown = false;
		}
	}
	
	//触控移动事件
	public override bool TouchMove( int x, int y )
	{
		if( !Alive ) {
			return false;
		}
		return false;
	}
	
	//==================================================================
	
	//命令解码
	public override void Decode( int Command, byte[] Buffer )
	{
		if( Command == SetText || Command == SetFont || Command == CMD_AddBitmap ) {
			string s = DecodeGBK( Buffer );
			if( Command == SetText ) {
				this.Text = s;
			}
			else if( Command == SetFont ) {
				Common.Font f = Common.GetFontFromString( s );
				this.FontSize = (int)(f.Size);
			}
			else if( Command == CMD_AddBitmap ) {
				this.ExtendValue = s;
			}
			else {
				//...
			}
		}
		else if( Command == CMD_StopSay ) {
			this.Text = "";
		}
		else if( Command == CMD_thread ) {
			//...
		}
		else if( Command == CMD_Run ) {
			int Data = SwitchData( Buffer, 4 );
			float ox = (float)( Data * Math.Cos( Math.PI * this.Angle/180 ) );
			float oy = -(float)( Data * Math.Sin( Math.PI * this.Angle/180 ) );
			RefreshSafeLocation();
			t_DL_StaX += ox;
			t_DL_StaY += oy;
			Refresh();
		}
		else if( Command == CMD_Undo ) {
			UndoSafeLocation();
		}
		else if( Command == CMD_SetID ) {
			int Data = SwitchData( Buffer, 4 );
			TargetID = Data;
		}
		else {
			//这里应该判断出运动方向, 以便为后续的上下左右方向碰撞事件做标志位
			if( (Command & 0x80) == 0 ) {
				RefreshSafeLocation();
				this.V_Decode( Command, Buffer );
				Refresh();
			}
			else {
				this.V_Decode( Command, Buffer );
			}
		}
	}
	
	protected override int ExRead( int Addr )
	{
		switch( Addr ) {
			case v_Scale:		return Scale;
			case v_Angle:		return Angle;
			case v_XScale:		return XScale;
			case v_YScale:		return YScale;
			case v_Opacity:		return Opacity;
			case v_ImageIndex:	return ImageIndex;
			case v_Alive:		return GetInt( Alive );
			
			//这两个属性已经过时了
			case v_OS_RolX:		return DL_RolOffsetX;
			case v_OS_RolY:		return DL_RolOffsetY;
			
			case v_HitID:		return HitID;
			case v_EageHitType:	return DL_EageHitType;
			case v_Hited:		return GetInt( Hited );
			case v_Bounce:		return Bounce;
			case v_XSpeed:		return XSpeed;
			case v_YSpeed:		return YSpeed;
			case v_FowardAngle:	return FowardAngle;
			default:			VCodeReadError( Addr ); return 0;
		}
	}
	
	protected override void ExWrite( int Addr, int Data )
	{
		switch( Addr ) {
			case v_Scale:		Scale = Data; break;
			case v_Angle:		Angle = Data; break;
			case v_XScale:		XScale = Data; break;
			case v_YScale:		YScale = Data; break;
			case v_Opacity:		Opacity = Data; break;
			case v_ImageIndex:	ImageIndex = Data; break;
			case v_Alive:		Alive = GetBool( Data ); break;
			
			//这两个属性已经过时了
			case v_OS_RolX:		DL_RolOffsetX = Data; break;
			case v_OS_RolY:		DL_RolOffsetY = Data; break;
			
			case v_HitID:		HitID = Data; break;
			case v_EageHitType:	DL_EageHitType = Data; break;
			case v_Hited:		Hited = GetBool( Data ); break;
			case v_Bounce:		Bounce = Data; break;
			case v_XSpeed:		XSpeed = Data; break;
			case v_YSpeed:		YSpeed = Data;  break;
			case v_FowardAngle:	FowardAngle = Data; break;
			default:			VCodeWriteError( Addr ); break;
		}
	}
	
	void SysTimerTick( object sender, EventArgs e )
	{
		if( !this.Alive ) {
			return;
		}
		//t_DL_StaX += XSpeed;
		//t_DL_StaY += YSpeed;
		//Refresh();
	}
	
	//刷新控件参数
	void Refresh()
	{
		//天啊...... 系统没有启动时不能进行碰撞检测!!!! 折腾一晚上, 唉.....
		if( !n_M.M.isStart ) {
			return;
		}
		if( this.V_MidX != u_MidX ||
			this.V_MidY != u_MidY ||
			this.Scale != u_Scale ||
			this.Angle != u_Angle ||
			this.XScale != u_XScale ||
			this.YScale != u_YScale ) {
			RunUp = false;
			if( this.V_MidY < u_MidY ) {
				RunUp = true;
			}
			RunDown = false;
			if( this.V_MidY > u_MidY ) {
				RunDown = true;
			}
			RunLeft = false;
			if( this.V_MidX < u_MidX ) {
				RunLeft = true;
			}
			RunRight = false;
			if( this.V_MidX > u_MidX ) {
				RunRight = true;
			}
			lock( n_SYS.SYS.V.InnerChanging ) {
			RefreshHitPoint();
			HitDetect();
			}
		}
	}
	
	//更新碰撞点
	void RefreshHitPoint()
	{
		double ax = Math.Cos( Math.PI * this.Angle/180 );
		double ay = -Math.Sin( Math.PI * this.Angle/180 );
		
		int rolx = GetAbsoluteRolX();
		int roly = GetAbsoluteRolY();
		int w = DL_Width * Scale / 100 * XScale / 100;
		int h = DL_Height * Scale / 100 * YScale / 100;
		int nsx = rolx - DL_RolOffsetX * Scale / 100 * XScale / 100;
		int nsy = roly - DL_RolOffsetY * Scale / 100 * YScale / 100;
		
		int tx0 = nsx - rolx;
		int ty0 = nsy - roly;
		int tx1 = nsx + w - rolx;
		int ty1 = nsy - roly;
		int tx2 = nsx + w - rolx;
		int ty2 = nsy + h - roly;
		int tx3 = nsx - rolx;
		int ty3 = nsy + h - roly;
		
		x0 = rolx + (int)(tx0 * ax - ty0 * ay);
		y0 = roly + (int)(ty0 * ax + tx0 * ay);
		x1 = rolx + (int)(tx1 * ax - ty1 * ay);
		y1 = roly + (int)(ty1 * ax + tx1 * ay);
		x2 = rolx + (int)(tx2 * ax - ty2 * ay);
		y2 = roly + (int)(ty2 * ax + tx2 * ay);
		x3 = rolx + (int)(tx3 * ax - ty3 * ay);
		y3 = roly + (int)(ty3 * ax + tx3 * ay);
		
		myGraphicsPath.Reset();
		Point[] inputponint = new Point[]{
			new Point( x0, y0 ),
			new Point( x1, y1 ),
			new Point( x2, y2 ),
			new Point( x3, y3 ),
		};
		myGraphicsPath.AddPolygon(inputponint);
	}
	
	//判断是否发生了碰撞
	void HitDetect()
	{
		isHited = false;
		Hited = isHited;
		
		if( !this.Alive ) {
			return;
		}
		MyControl[] mList = n_M.M.MyControlList;
		int Length = n_M.M.ListLength;
		
		HitID = 0;
		
		//查找角色控件并进行碰撞检测
		for( int i = 1; i < Length; ++i ) {
			if( mList[i] == null ) {
				continue;
			}
			if( !(mList[i] is MySprite) || mList[i] == this ) {
				continue;
			}
			MySprite ts = (MySprite)mList[i];
			if( !ts.Alive ) {
				continue;
			}
			//注意一定要进行双向判断, 两个对象任一个有顶点在另一个对象内, 即为碰撞
			if( ts.myGraphicsPath.IsVisible( x0, y0 ) ||
			    ts.myGraphicsPath.IsVisible( x1, y1 ) ||
			    ts.myGraphicsPath.IsVisible( x2, y2 ) ||
			    ts.myGraphicsPath.IsVisible( x3, y3 ) ||
			    this.myGraphicsPath.IsVisible( ts.x0, ts.y0 ) ||
			    this.myGraphicsPath.IsVisible( ts.x1, ts.y1 ) ||
			    this.myGraphicsPath.IsVisible( ts.x2, ts.y2 ) ||
			    this.myGraphicsPath.IsVisible( ts.x3, ts.y3 ) 
			   ) {
				isHited = true;
				HitID = i;
				break;
			}
		}
		isHited |= BackImageHitDetect();
		
		//触发碰撞事件
		if( isHited ) {
			if( RunUp ) {
				n_Linker.Linker.SendEventFlag( ID, 0x08 );
				YSpeed = 0;
			}
			if( RunDown ) {
				n_Linker.Linker.SendEventFlag( ID, 0x10 );
				YSpeed = 0;
			}
			if( RunLeft ) {
				n_Linker.Linker.SendEventFlag( ID, 0x20 );
				XSpeed = 0;
			}
			if( RunRight ) {
				n_Linker.Linker.SendEventFlag( ID, 0x40 );
				XSpeed = 0;
			}
			
			n_Linker.Linker.SendEventFlag( ID, 0x01 );
			
			if( HitID > 0 ) {
				MySprite ts = (MySprite)n_M.M.MyControlList[HitID];
				ts.HitID = ID;
				n_Linker.Linker.SendEventFlag( HitID, 0x01 );
			}
		}
		//bool HitEage = false;
		
		
		//if( EageHitType != 0 ) {
		int x_start = (int)(n_M.M.CamMidX - n_M.M.V_CamWidth/2);
		int x_end = (int)(n_M.M.CamMidX + n_M.M.V_CamWidth/2);
		int y_start = (int)(n_M.M.CamMidY - n_M.M.V_CamHeight/2);
		int y_end = (int)(n_M.M.CamMidY + n_M.M.V_CamHeight/2);
		
		bool outeage0 = x0 < x_start || x0 > x_end || y0 < y_start || y0 > y_end;
		bool outeage1 = x1 < x_start || x1 > x_end || y1 < y_start || y1 > y_end;
		bool outeage2 = x2 < x_start || x2 > x_end || y2 < y_start || y2 > y_end;
		bool outeage3 = x3 < x_start || x3 > x_end || y3 < y_start || y3 > y_end;
		
		if( (outeage0 || outeage1 || outeage2 || outeage3) ) {//EageHitType == 1 && 
			HitID = 0;
			n_Linker.Linker.SendEventFlag( ID, 0x02 );
			isHited = true;
			//HitEage = true;
		}
		if( (outeage0 && outeage1 && outeage2 && outeage3) ) {//EageHitType == 2 && 
			HitID = 0;
			n_Linker.Linker.SendEventFlag( ID, 0x04 );
			isHited = true;
			//HitEage = true;
		}
		//}
		
		
		if( !isHited ) {
			RefreshSafeLocation();
		}
		else if( Bounce < 0 ) {
			RefreshSafeLocation();
			//... 继续运行
		}
		//这里临时禁用了边界阻拦功能
		else if( Bounce == 0 ) {
			UndoSafeLocation();
		}
		//这里应该根据 bounce 的数值, 进行反弹处理
		else {
			//...
		}
		Hited = isHited;
	}
	
	//判断是否和背景图片发生碰撞
	bool BackImageHitDetect()
	{
		if( !this.Alive ) {
			return false;
		}
		if( n_Linker.Linker.tMyImagePanel == null ) {
			return false;
		}
		bool ht = false;
		ht = n_Linker.Linker.tMyImagePanel.isHited( x0, y0 );
		ht |= n_Linker.Linker.tMyImagePanel.isHited( x1, y1 );
		ht |= n_Linker.Linker.tMyImagePanel.isHited( x2, y2 );
		ht |= n_Linker.Linker.tMyImagePanel.isHited( x3, y3 );
		
		ht |= n_Linker.Linker.tMyImagePanel.isHited( (x0 + x1)/2, (y0 + y1)/2 );
		ht |= n_Linker.Linker.tMyImagePanel.isHited( (x1 + x2)/2, (y1 + y2)/2 );
		ht |= n_Linker.Linker.tMyImagePanel.isHited( (x2 + x3)/2, (y2 + y3)/2 );
		ht |= n_Linker.Linker.tMyImagePanel.isHited( (x3 + x0)/2, (y3 + y0)/2 );
		
		return ht;
	}
	
	//更新未碰撞位置
	void RefreshSafeLocation()
	{
		u_MidX = V_MidX;
		u_MidY = V_MidY;
		u_Width = DL_Width;
		u_Height = DL_Height;
		u_Scale = Scale;
		u_Angle = Angle;
		u_XScale = XScale;
		u_YScale = YScale;
	}
	
	//恢复最近的安全位置
	void UndoSafeLocation()
	{
		V_MidX = u_MidX;
		V_MidY = u_MidY;
		
		DL_Width = u_Width;
		DL_Height = u_Height;
		Scale = u_Scale;
		Angle = u_Angle;
		XScale = u_XScale;
		YScale = u_YScale;
		
		RefreshHitPoint();
	}
}
}



