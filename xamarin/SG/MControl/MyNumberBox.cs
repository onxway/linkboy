﻿
using System;
using n_SG;
using n_Common;
using n_SG_MyControl;

#if OS_android
using Android.Graphics;
#endif

#if OS_win
using System.Drawing;
#endif

namespace n_SG_MyNumberBox
{
public class MyNumberBox : MyControl
{
	const int v_Number = 0x20;
	
	public delegate void D_TouchDownEvent( object sender );
	public D_TouchDownEvent TouchDownEvent;
	
	public int FontSize;
	
	//构造函数 - 图片为空, 显示文本信息
	public MyNumberBox( Color bc, int v, int fs, Color fc, int x, int y, int w, int h ) : base( x, y, w, h )
	{
		BackColor = bc;
		
		DL_Value = v;
		ForeColor = fc;
		FontSize = fs;
	}
	
	//=========================================================================
	
	//绘制控件
	public override void Draw()
	{
		int x = GetAbsoluteStaX();
		int y = GetAbsoluteStaY();
		
		SG.MRectangle.Fill( BackColor, x, y, DL_Width, DL_Height );
		SG.MString.DrawAtCenter( DL_Value.ToString(), ForeColor, FontSize, x + DL_Width/2, y + DL_Height/2 );
	}
	
	//=========================================================================
	
	//触控按下事件
	public override bool TouchDown( int x, int y )
	{
		if( Contain( x, y ) ) {
			isTouchDown = true;
			if( TouchDownEvent != null ) {
				TouchDownEvent( this );
			}
		}
		else {
			isTouchDown = false;
		}
		
		return isTouchDown;
	}
	
	//触控松开事件
	public override void TouchUp( int x, int y )
	{
		isTouchDown = false;
	}
	
	//触控移动事件
	public override bool TouchMove( int x, int y )
	{
		return false;
	}
	
	//==================================================================
	
	//命令解码
	public override void Decode( int Command, byte[] Buffer )
	{
		if( Command == SetText || Command == SetFont ) {
			string s = DecodeGBK( Buffer );
			if( Command == SetText ) {
				if( s != "" ) {
					DL_Value = int.Parse( s );
				}
			}
			else if( Command == SetFont ) {
				Common.Font f = Common.GetFontFromString( s );
				this.FontSize = (int)(f.Size);
			}
			else {
				//...
			}
		}
		else {
			this.V_Decode( Command, Buffer );
		}
	}
}
}

