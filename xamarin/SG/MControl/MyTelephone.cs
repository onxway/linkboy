﻿
using System;
using n_SG;
using n_Common;
using n_SG_MyControl;
using n_SG_MyComboBox;

#if OS_android
using Android.Graphics;
using Android.Telephony;
using Android.Content;
using Android.Net;
#endif

#if OS_win
using c_PlaySound;
using System.IO.Ports;
using System.Drawing;
//using System.Windows.Forms;
using Dongzr.MidiLite;
#endif

namespace n_SG_MyTelephone
{
public class MyTelephone : MyControl
{
	public delegate void d_TouchDown( object sender );
	public d_TouchDown TouchDownEvent;
	
	const int CMD_SetValue = 0x10; //未用
	const int CMD_SetTelephone = 0x11;
	const int CMD_SendMessage = 0x12;
	const int CMD_CallPhone = 0x13;
	
	public float FontSize;
	
	string TelephoneNumber;
	
	//构造函数 - 图片为空, 显示文本信息
	public MyTelephone( Color bc, float fs, Color fc, int x, int y, int w, int h ) : base( x, y, w, h )
	{
		BackColor = bc;
		ForeColor = fc;
		FontSize = fs;
		
		TelephoneNumber = null;
		
		if( SG.Running ) {
			//t.Start();
		}
	}
	
	//=========================================================================
	
	//绘制控件
	public override void Draw()
	{
//		int x = GetAbsoluteStaX();
//		int y = GetAbsoluteStaY();
//		SG.MRectangle.Fill( BackColor, x, y, Width, Height );
//		SG.MString.DrawAtCenter( "选择蓝牙设备", ForeColor, FontSize, x + Width/2, y + Height / 2 );
	}
	
	//=========================================================================
	
	//触控按下事件
	public override bool TouchDown( int x, int y )
	{
		if( Contain( x, y ) ) {
			if( TouchDownEvent != null ) {
				TouchDownEvent( this );
			}
			isTouchDown = true;
		}
		else {
			isTouchDown = false;
		}
		return isTouchDown;
	}
	
	//触控松开事件
	public override void TouchUp( int x, int y )
	{
		
	}
	
	//触控移动事件
	public override bool TouchMove( int x, int y )
	{
		return false;
	}
	
	//==================================================================
	
	//命令解码
	public override void Decode( int Command, byte[] Buffer )
	{
		if( Command == CMD_SetTelephone ) {
			TelephoneNumber = DecodeGBK( Buffer );
		}
		else if( Command == CMD_SendMessage ) {
			string s = DecodeGBK( Buffer );
			#if OS_android
			//n_SYS.SYS.AddMessage( "MES - " + TelephoneNumber + ":" + s + "\n" );
			SmsManager smsManager = SmsManager.Default;
			smsManager.SendTextMessage( TelephoneNumber, null, s, null, null );
			#endif
		}
		else if( Command == CMD_CallPhone ) {
			#if OS_android
			Intent intent = new Intent(Intent.ActionCall, Android.Net.Uri.Parse("tel:"+ TelephoneNumber ));
			n_SYS.SYS.A.StartActivity( intent );
			#endif
		}
		else {
			this.V_Decode( Command, Buffer );
		}
	}
}
}



