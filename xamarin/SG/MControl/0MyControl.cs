﻿
using System.Text;
using n_Linker;
using System.Drawing;

//#if OS_android
//using n_Gb2312Encoding;
//#endif

#if OS_win
#endif


namespace n_SG_MyControl
{
public abstract class MyControl
{
	public MyControl owner;
	
	//是否允许编辑
	public static bool EditEnable;
	
	//是否为调试模式(是的话会显示一些边框和信息等)
	public static bool DebugMode;
	
	//Y坐标是否原生坐标系(Y方向向下)
	public static bool Y_Default;
	
	bool isMousePress;
	float LastX;
	float LastY;
	int MouseOnSizePort;
	int MouseSelectSizePort;
	float BSX;
	float BSY;
	float BaseWidth;
	float BaseHeight;
	
	public int ID;
	public string Name;
	
	//仅用来临时开放给Sprite类, 用来累加不失真
	protected float t_DL_StaX;
	protected float t_DL_StaY;
	
	public int V_StaX {
		set {
			t_DL_StaX = value;
		}
		get {
			return (int)t_DL_StaX;
		}
	}
	
	public int V_MidX {
		set {
			t_DL_StaX = value - DL_Width / 2;
		}
		get {
			return (int)t_DL_StaX + DL_Width / 2;
		}
	}
	
	public int V_RolX {
		set {
			t_DL_StaX = value - DL_RolOffsetX;
		}
		get {
			return (int)t_DL_StaX + DL_RolOffsetX;
		}
	}
	
	public int V_StaY {
		set {
			t_DL_StaY = value;
		}
		get {
			return (int)t_DL_StaY;
		}
	}
	
	public int V_MidY {
		set {
			t_DL_StaY = value - DL_Height / 2;
		}
		get {
			return (int)t_DL_StaY + DL_Height / 2;
		}
	}
	
	public int V_RolY {
		set {
			t_DL_StaY = value - DL_RolOffsetY;
		}
		get {
			return (int)t_DL_StaY + DL_RolOffsetY;
		}
	}
	
	public const int c_RolX = 0x01;
	public const int c_RolY = 0x02;
	
	const int c_Width = 0x03;
	public int DL_Width;
	const int c_Height = 0x04;
	public int DL_Height;
	
	public const int c_BackColor = 0x05;
	public Color BackColor;
	public const int c_ForeColor = 0x06;
	public Color ForeColor;
	public const int c_EageColor = 0x07;
	public Color EdgeColor;
	
	const int c_Value = 0x08;
	public int DL_Value;
	
	const int c_RolOffsetX = 0x09;
	public int DL_RolOffsetX;
	const int c_RolOffsetY = 0x0A;
	public int DL_RolOffsetY;
	
	public const int SetText = 0x10;
	public const int SetFont = 0x11;
	
	//其他用户命令从 0x20 依次开始
	//...
	
	public bool isTouchDown;
	public bool Visible;
	public string Message;
	
	public bool isHidenModule;
	public bool isControlPad;
	
	static StringBuilder sb;
	
	const int SizePadding = 9;
	
	//#if OS_android
	//static Gb2312Encoding GB2312;
	//#endif
	
	byte[] SendBuffer;
	
	//静态初始化
	public static void Init()
	{
		Y_Default = true;
		EditEnable = false;
		DebugMode = false;
	}
	
	//构造函数
	protected MyControl( int x, int y, int w, int h )
	{
		owner = null;
		
		Name = "";
		
		isMousePress = false;
		LastX = 0;
		LastY = 0;
		MouseOnSizePort = -1;
		MouseSelectSizePort = -1;
		BSX = 0;
		BSY = 0;
		BaseWidth = 0;
		BaseHeight = 0;
		
		DL_RolOffsetX = 0;
		DL_RolOffsetY = 0;
		
		ID = 0;
		
		V_StaX = x;
		V_StaY = y;
		DL_Width = w;
		DL_Height = h;
		
		isTouchDown = false;
		isControlPad = false;
		
		Visible = true;
		isHidenModule = false;
		
		Message = null;
		
		sb = new StringBuilder( 50 );
		
		SendBuffer = new byte[9];
		
		//#if OS_android
		//GB2312 = new Gb2312Encoding();
		//#endif
	}
	
	//判断是否在对象上
	protected virtual bool Contain( int x, int y )
	{
		if( n_M.M.SelectedObj == this ) {
			if( x >= V_StaX - SizePadding && x < V_StaX + DL_Width + SizePadding && y >= V_StaY - SizePadding && y < V_StaY + DL_Height + SizePadding ) {
				return true;
			}
			return false;
		}
		else {
			if( x >= V_StaX && x < V_StaX + DL_Width && y >= V_StaY && y < V_StaY + DL_Height ) {
				return true;
			}
			return false;
		}
	}
	
	//绘制控件
	public abstract void Draw();
	
	//绘制控件
	public void Draw1()
	{
		if( this == n_M.M.SelectedObj ) {
			
			for( int i = 0; i < 8; ++i ) {
				Rectangle r = GetRectangleOfSizePort( i );
				if( MouseOnSizePort == i ) {
					n_SG.SG.MRectangle.Fill( Color.OrangeRed, r.X, r.Y, r.Width - 1, r.Height - 1 );
					n_SG.SG.MRectangle.Draw( Color.Red, 1, r.X, r.Y, r.Width - 1, r.Height - 1 );
				}
				else {
					n_SG.SG.MRectangle.Fill( Color.LightGreen, r.X, r.Y, r.Width - 1, r.Height - 1 );
					n_SG.SG.MRectangle.Draw( Color.Green, 1, r.X, r.Y, r.Width - 1, r.Height - 1 );
				}
			}
		}
	}
	
	//----------------------------------------------------------
	//运行操控事件
	
	//触控按下事件, 当触控点在控件上时, 返回 true, 否则返回 false
	public abstract bool TouchDown( int x, int y );
	
	//触控松开事件, 当触控点在控件上时, 返回 true, 否则返回 false
	public abstract void TouchUp( int x, int y );
	
	//触控移动事件, 当触控点在控件上时, 返回 true, 否则返回 false
	public abstract bool TouchMove( int x, int y );
	
	//----------------------------------------------------------
	//边界调整事件
	
	//触控按下事件, 当触控点在控件上时, 返回 true, 否则返回 false
	public bool EditTouchDown( int x, int y )
	{
		if( Contain( x, y ) ) {
			isMousePress = true;
			LastX = x;
			LastY = y;
			
			MouseSelectSizePort = MouseOnSizePort;
			BSX = V_StaX;
			BSY = V_StaY;
			BaseWidth = DL_Width;
			BaseHeight = DL_Height;
			
			return true;
		}
		return false;
	}
	
	//触控松开事件, 当触控点在控件上时, 返回 true, 否则返回 false
	public void EditTouchUp( int x, int y )
	{
		isMousePress = false;
	}
	
	//触控移动事件, 当触控点在控件上时, 返回 true, 否则返回 false
	public bool EditTouchMove( int x, int y )
	{
		MouseOnSizePort = GetNumberOfSizePort( x, y );
		
		if( !isMousePress ) {
			return false;
		}
		//如果没有按下尺寸调节方框
		if( MouseSelectSizePort == -1 ) {
			V_StaX += (int)(x - LastX);
			V_StaY += (int)(y - LastY);
			LastX = x;
			LastY = y;
		}
		else {
			//BSX = this.SX;
			//BSY = this.SY;
			float BWidth = this.BaseWidth;
			float BHeight = this.BaseHeight;
			float bsx = BSX;
			float bsy = BSY;
			switch( MouseSelectSizePort ) {
				case 0: bsy += y - LastY; BHeight -= y - LastY; bsx += x - LastX; BWidth -= x - LastX; break;
				case 1: bsy += y - LastY; BHeight -= y - LastY; break;
				case 2: bsy += y - LastY; BHeight -= y - LastY; BWidth += x - LastX; break;
				case 3: BWidth += x - LastX; break;
				
				case 4: BWidth += x - LastX; BHeight += y - LastY; break;
				case 5: BHeight += y - LastY; break;
				case 6: BHeight += y - LastY; bsx += x - LastX; BWidth -= x - LastX; break;
				case 7: bsx += x - LastX; BWidth -= x - LastX; break;
				default: break;
			}
			if( BWidth >= SizePadding + 2 ) {
				//this.SX = BSX;
				this.BaseWidth = BWidth;
				LastX = x;
				BSX = bsx;
			}
			if( BHeight >= SizePadding + 2 ) {
				//this.SY = BSY;
				this.BaseHeight = BHeight;
				LastY = y;
				BSY = bsy;
			}
			//设置尺寸
			DL_Width = (int)BaseWidth;
			DL_Height = (int)BaseHeight;
			
			switch( MouseSelectSizePort ) {
				case 0: V_StaX = (int)(BSX - (DL_Width - this.BaseWidth)); V_StaY = (int)(BSY - (DL_Height - this.BaseHeight)); break;
				case 1: V_StaX = (int)BSX; V_StaY = (int)(BSY - (DL_Height - this.BaseHeight)); break;
				case 2: V_StaX = (int)BSX; V_StaY = (int)(BSY - (DL_Height - this.BaseHeight)); break;
				case 3: V_StaX = (int)BSX; V_StaY = (int)BSY; break;
				
				case 4: V_StaX = (int)BSX; V_StaY = (int)BSY; break;
				case 5: V_StaX = (int)BSX; V_StaY = (int)BSY; break;
				case 6: V_StaX = (int)(BSX - (DL_Width - this.BaseWidth)); V_StaY = (int)BSY; break;
				case 7: V_StaX = (int)(BSX - (DL_Width - this.BaseWidth)); V_StaY = (int)BSY; break;
				default: break;
			}
		}
		return false;
	}
	
	//判断坐标在哪个端口上, 从左上角开始顺时针依次为 0 1 2 3 4 5 6 7, 不在任何端口则返回-1
	int GetNumberOfSizePort( int mX, int mY )
	{
		for( int i = 0; i < 8; ++i ) {
			if( GetRectangleOfSizePort( i ).Contains( mX, mY ) ) {
				return i;
			}
		}
		return -1;
	}
	
	//获取指定索引的尺寸端口
	Rectangle GetRectangleOfSizePort( int Index )
	{
		int sx = GetAbsoluteStaX();
		int sy = GetAbsoluteStaY();
		int mx = GetAbsoluteMidX();
		int my = GetAbsoluteMidY();
		
		switch ( Index ) {
			case 0: return new Rectangle( sx - SizePadding, sy - SizePadding, SizePadding, SizePadding );
			case 1: return new Rectangle( mx - SizePadding / 2, sy - SizePadding, SizePadding, SizePadding );
			case 2: return new Rectangle( sx + DL_Width, sy - SizePadding, SizePadding, SizePadding );
			case 3: return new Rectangle( sx + DL_Width, my - SizePadding / 2, SizePadding, SizePadding );
			case 4: return new Rectangle( sx + DL_Width, sy + DL_Height, SizePadding, SizePadding );
			case 5: return new Rectangle( mx - SizePadding / 2, sy + DL_Height, SizePadding, SizePadding );
			case 6: return new Rectangle( sx - SizePadding, sy + DL_Height, SizePadding, SizePadding );
			case 7: return new Rectangle( sx - SizePadding, my - SizePadding / 2, SizePadding, SizePadding );
			default: return Rectangle.Empty;
		}
	}
	
	//----------------------------------------------------------
	
	//获取全局StaX坐标
	protected int GetAbsoluteStaX()
	{
		return owner == null ? V_StaX: V_StaX + owner.GetAbsoluteStaX();
	}
	
	//获取全局StaY坐标
	protected int GetAbsoluteStaY()
	{
		return owner == null ? V_StaY: V_StaY + owner.GetAbsoluteStaY();
	}
	
	//获取全局MidX坐标
	protected int GetAbsoluteMidX()
	{
		return owner == null ? V_MidX: V_MidX + owner.GetAbsoluteStaX();
	}
	
	//获取全局MidY坐标
	protected int GetAbsoluteMidY()
	{
		return owner == null ? V_MidY: V_MidY + owner.GetAbsoluteStaY();
	}
	
	//获取全局RolX坐标
	protected int GetAbsoluteRolX()
	{
		return owner == null ? V_RolX: V_RolX + owner.GetAbsoluteStaX();
	}
	
	//获取全局RolY坐标
	protected int GetAbsoluteRolY()
	{
		return owner == null ? V_RolY: V_RolY + owner.GetAbsoluteStaY();
	}
	
	//==========================================================
	
	//解码器
	public abstract void Decode( int Command, byte[] Buffer );
	
	//虚拟内存访问指令解码
	protected void V_Decode( int Command, byte[] Buffer )
	{
		if( Command >= 128 ) {
			Command -= 128;
			int Data = VRead( Command );
			byte D0 = 0;
			byte D1 = 0;
			byte D2 = 0;
			byte D3 = 0;
			IntToByte( Data, out D0, out D1, out D2, out D3 );
			SendBuffer[ 0 ] = 0x55;
			SendBuffer[ 1 ] = 6;
			SendBuffer[ 2 ] = 0x00;
			SendBuffer[ 3 ] = (byte)ID;
			SendBuffer[ 4 ] = D0;
			SendBuffer[ 5 ] = D1;
			SendBuffer[ 6 ] = D2;
			SendBuffer[ 7 ] = D3;
			SendBuffer[ 8 ] = 0;
			Linker.SendData( 9, SendBuffer );
		}
		else {
			int Data = SwitchData( Buffer, 4 );
			VWrite( Command, Data );
		}
	}
	
	//虚拟内存读取指令
	int VRead( int addr )
	{
		switch( addr ) {
			case c_RolX:		return V_RolX;
			case c_RolY:		return MyControl.Y_Default? V_RolY: -V_RolY;
			case c_Width:		return DL_Width;
			case c_Height:		return DL_Height;
			case c_Value:		return DL_Value;
			case c_RolOffsetX:	return DL_RolOffsetX;
			case c_RolOffsetY:	return DL_RolOffsetY;
			//case c_BackColor:	return 0;
			//case c_ForeColor:	return 0;
			//case c_FrameColor:return 0;
			default:			return ExRead( addr );
		}
	}
	
	//虚拟内存写入指令
	void VWrite( int addr, int Data )
	{
		switch( addr ) {
			case c_RolX:		V_RolX = Data; break;
			case c_RolY:		V_RolY = MyControl.Y_Default? Data: -Data; break;
			case c_Width:		DL_Width = Data; break;
			case c_Height:		DL_Height = Data; break;
			case c_Value:		DL_Value = Data; break;
			case c_RolOffsetX:	DL_RolOffsetX = Data; break;
			case c_RolOffsetY:	DL_RolOffsetY = Data; break;
			case c_BackColor:
				#if OS_android
				this.BackColor = new Color( Data );
				#endif
				#if OS_win
				this.BackColor = Color.FromArgb( Data );
				#endif
				break;
			case c_ForeColor:
				#if OS_android
				this.ForeColor = new Color( Data );
				#endif
				#if OS_win
				this.ForeColor = Color.FromArgb( Data );
				#endif
				break;
			case c_EageColor:
				#if OS_android
				this.EdgeColor = new Color( Data );
				#endif
				#if OS_win
				this.EdgeColor = Color.FromArgb( Data );
				#endif
				break;
			default:			ExWrite( addr, Data ); break;
		}
	}
	
	//默认处理函数, 待覆盖
	protected virtual int ExRead( int Addr )
	{
		VCodeReadError( Addr );
		return 0;
	}
	
	//默认处理函数, 待覆盖
	protected virtual void ExWrite( int Addr, int Data )
	{
		VCodeWriteError( Addr );
	}
	
	protected void VCodeReadError( int c )
	{
		n_SYS.SYS.AddMessage( "E_vR(ID" + this.ID + "):" + c + " " );
	}
	
	protected void VCodeWriteError( int c )
	{
		n_SYS.SYS.AddMessage( "E_vW(ID" + this.ID + "):" + c + " " );
	}
	
	protected bool GetBool( int d )
	{
		return (d & 0xFF) != 0;
	}
	
	protected int GetInt( bool b )
	{
		return b? 1: 0;
	}
	
	//解码一个GBK字符串
	public static string DecodeGBK( byte[] Buffer )
	{
		int Length = 0;
		for( int i = 4; i < Buffer.Length; i++ ) {
			int ci = Buffer[ i ];
			if( ci == 0 ) {
				break;
			}
			Length++;
		}
		
		//#if OS_android
		//return GB2312.GBKtoUnicode( Buffer, 4, Length );
		//#endif
		
		//#if OS_win
		char[] cc = System.Text.Encoding.GetEncoding(936).GetChars( Buffer, 4, Length );
		sb.Length = 0;
		for( int i = 0 ; i < cc.Length; ++i ) {
			sb.Append( cc[i].ToString() );
		}
		return sb.ToString();
		//#endif
	}
	
	//解码一个unicode字符串
	public static string DecodeUnicode( byte[] Buffer )
	{
		sb.Length = 0;
		for( int i = 4; i < Buffer.Length; i += 2 ) {
			int Low = Buffer[ i ];
			int High = Buffer[ i + 1 ];
			char c = (char)( Low + High * 256 );
			if( c == 0 ) {
				break;
			}
			sb.Append( c );
		}
		return sb.ToString();
	}
	
	//==========================================================
	
	//分解数据
	static void IntToByte( int D, out byte d0, out byte d1, out byte d2, out byte d3 )
	{
		long L = D;
		if( L < 0 ) {
			L += int.MaxValue;
			L += int.MaxValue;
			L += 2;
		}
		d0 = (byte)( L % 256 ); L /= 256;
		d1 = (byte)( L % 256 ); L /= 256;
		d2 = (byte)( L % 256 ); L /= 256;
		d3 = (byte)( L % 256 ); L /= 256;
	}
	
	protected static int SwitchData( byte[] buffer, int StartIndex )
	{
		long D0 = buffer[StartIndex + 0];
		long D1 = buffer[StartIndex + 1];
		long D2 = buffer[StartIndex + 2];
		long D3 = buffer[StartIndex + 3];
		long d = D0 + D1*256 + D2*256*256 + D3*256*256*256;
		long MaxValue = (long)65536 * 65536;
		if( d >= MaxValue / 2 ) {
			d -= MaxValue;
		}
		return (int)d;
	}
}
}


