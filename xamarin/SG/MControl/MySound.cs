﻿
using System;
using n_SG;
using n_Common;
using n_SG_MyControl;

#if OS_android
using Android.Graphics;
#endif

#if OS_win
using c_PlaySound;
#endif

namespace n_SG_MySound
{
public class MySound : MyControl
{
	const int CMD_Play = 0x01;
	const int CMD_Stop = 0x02;
	const int CMD_Pause = 0x03;
	const int CMD_Resume = 0x04;
	const int CMD_AddFile = 0x10;
	
	string[] FileList;
	int Length;
	
	//构造函数 - 图片为空, 显示文本信息
	public MySound() : base( 0, 0, 0, 0 )
	{
		//DataList[v_Value] = 0;
		
		FileList = new string[ 20 ];
		Length = 1;
	}
	
	//=========================================================================
	
	//绘制控件
	public override void Draw()
	{
		if( isHidenModule ) {
			return;
		}
	}
	
	//=========================================================================
	
	//触控按下事件
	public override bool TouchDown( int x, int y )
	{
		return false;
	}
	
	//触控松开事件
	public override void TouchUp( int x, int y )
	{
	}
	
	//触控移动事件
	public override bool TouchMove( int x, int y )
	{
		return false;
	}
	
	//==================================================================
	
	//命令解码
	public override void Decode( int Command, byte[] Buffer )
	{
		//备用命令
		if( Command == 0 ) {
			return;
		}
		if( Command == CMD_Play ) {
			int i = Buffer[4];
			int v = Buffer[5];
			
			string fp = n_CommonData.CommonData.GetFullPath( FileList[i] );
			if( fp != null ) {
				PlaySound.Play( this.ID, fp );
			}
			else {
				//...出错了
			}
			return;
		}
		if( Command == CMD_Stop ) {
			PlaySound.Stop( this.ID );
			return;
		}
		if( Command == CMD_Pause ) {
			PlaySound.pause( this.ID );
			return;
		}
		if( Command == CMD_Resume ) {
			PlaySound.resume( this.ID );
			return;
		}
		if( Command == CMD_AddFile ) {
			string s = DecodeGBK( Buffer );
			FileList[Length] = s;
			Length++;
			return;
		}
	}
}
}

