﻿
using System;
using n_SG;
using n_Common;
using n_SG_MyControl;

#if OS_android
using Android.Graphics;
#endif

#if OS_win
using System.Drawing;
#endif

namespace n_SG_MyImagePanel
{
public class MyImagePanel : MyControl
{
	public delegate void d_TouchDown( object sender );
	public d_TouchDown TouchDownEvent;
	
	public delegate void d_TouchUp( object sender );
	public d_TouchUp TouchUpEvent;
	
	public delegate void d_TouchClick( object sender );
	public d_TouchClick TouchClickEvent;
	
	//是否绘制边框
	public bool DrawEdge;
	public Color EdgeColor;
	
	public Bitmap BackBitmap;
	
	public string Text;
	public float FontSize;
	
	public const int CMD_SetImage = 0x20;
	
	public const int v_SX = 0x21;
	public const int v_SY = 0x22;
	
	string v_ExtendValue;
	public string ExtendValue {
		get {
			return v_ExtendValue;
		}
		set {
			if( v_ExtendValue != value ) {
				v_ExtendValue = value;
				
				#if OS_win
				if( System.IO.File.Exists( v_ExtendValue ) ) {
					Image ti = Image.FromFile( v_ExtendValue );
					BackBitmap = new Bitmap( ti );
					ti.Dispose();
					//g = Graphics.FromImage( BackBitmap );
				}
				else if( System.IO.File.Exists( n_CommonData.CommonData.GetBasePath() + v_ExtendValue ) ) {
					Image ti = Image.FromFile( n_CommonData.CommonData.GetBasePath() + v_ExtendValue );
					BackBitmap = new Bitmap( ti );
					ti.Dispose();
					//g = Graphics.FromImage( BackBitmap );
				}
				else {
					//这里应该显示一个错误的图片标志, 表示图片未找到
					BackBitmap = null;
				}
				#endif
			}
		}
	}
	
	//构造函数 - 文本信息为空, 显示图片
	public MyImagePanel( int x, int y, int w, int h ) : base( x, y, w, h )
	{
		BackBitmap = null;
		
		Text = null;
		
		BackColor = Color.Black;
	}
	
	//判断给定坐标处是否发生碰撞
	public bool isHited( int x, int y )
	{
		int stX = this.GetAbsoluteStaX();
		int stY = this.GetAbsoluteStaY();
		
		if( x < stX || x >= stX + DL_Width ) return false;
		if( y < stY || y >= stY + DL_Height ) return false;
		
		int bx = (x - stX) * BackBitmap.Width / DL_Width;
		int by = (y - stY) * BackBitmap.Height / DL_Height;
		
		if( BackBitmap.GetPixel( bx, by ).A == 0 ) {
			return false;
		}
		return true;
	}
	
	//=========================================================================
	
	//绘制控件
	public override void Draw()
	{
		int x = GetAbsoluteStaX();
		int y = GetAbsoluteStaY();
		
		lock( n_SYS.SYS.V.InnerChanging ) {
		if( BackBitmap != null ) {
			
			System.Drawing.Drawing2D.GraphicsState gs = SG.g.Save();
			SG.g.InterpolationMode = System.Drawing.Drawing2D.InterpolationMode.NearestNeighbor;
			SG.g.PixelOffsetMode = System.Drawing.Drawing2D.PixelOffsetMode.Half;
			
			SG.MBitmap.Draw( BackBitmap, x, y, DL_Width, DL_Height );
			
			SG.g.Restore( gs );
		}
		else {
			SG.MRectangle.Draw( Color.Red, 1, x, y, DL_Width, DL_Height );
		}
		}
		
		if( DrawEdge ) {
			SG.MRectangle.Draw( EdgeColor, 1, x, y, DL_Width, DL_Height );
		}
	}
	
	//=========================================================================
	
	//触控按下事件
	public override bool TouchDown( int x, int y )
	{
		if( Contain( x, y ) ) {
			if( TouchDownEvent != null ) {
				TouchDownEvent( this );
			}
			isTouchDown = true;
		}
		else {
			isTouchDown = false;
		}
		return isTouchDown;
	}
	
	//触控松开事件
	public override void TouchUp( int x, int y )
	{
		if( isTouchDown ) {
			if( TouchUpEvent != null ) {
				TouchUpEvent( this );
			}
			if( TouchClickEvent != null ) {
				TouchClickEvent( this );
			}
		}
		isTouchDown = false;
	}
	
	//触控移动事件
	public override bool TouchMove( int x, int y )
	{
		return false;
	}
	
	//==================================================================
	
	//命令解码
	public override void Decode( int Command, byte[] Buffer )
	{
		if( Command == SetText || Command == SetFont || Command == CMD_SetImage ) {
			string s = DecodeGBK( Buffer );
			if( Command == SetText ) {
				this.Text = s;
			}
			else if( Command == SetFont ) {
				Common.Font f = Common.GetFontFromString( s );
				this.FontSize = (int)(f.Size);
			}
			else if( Command == CMD_SetImage ) {
				this.ExtendValue = s;
			}
			else {
				//...
			}
		}
		else {
			this.V_Decode( Command, Buffer );
		}
	}
}
}

