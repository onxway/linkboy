﻿
using System;
using n_SG;
using n_Common;
using n_SG_MyControl;

#if OS_android
using Android.Graphics;
#endif

#if OS_win
using System.Runtime.InteropServices;
#endif

namespace n_SG_MyKeyTrig
{
public class MyKeyTrig : MyControl
{
	const int CMD_KeyPress = 1;
	const int CMD_KeyDown = 2;
	const int CMD_KeyUp = 3;
	
	#if OS_win
	[DllImport("USER32.DLL")]
	public static extern void keybd_event(byte bVk, byte bScan, int dwFlags, int dwExtraInfo);  //导入模拟键盘的方法
	#endif
	
	//构造函数 - 图片为空, 显示文本信息
	public MyKeyTrig() : base( 0, 0, 0, 0 )
	{
		
	}
	
	//=========================================================================
	
	//绘制控件
	public override void Draw()
	{
		if( isHidenModule ) {
			return;
		}
	}
	
	//=========================================================================
	
	//触控按下事件
	public override bool TouchDown( int x, int y )
	{
		return false;
	}
	
	//触控松开事件
	public override void TouchUp( int x, int y )
	{
	}
	
	//触控移动事件
	public override bool TouchMove( int x, int y )
	{
		return false;
	}
	
	//==================================================================
	
	//命令解码
	public override void Decode( int Command, byte[] Buffer )
	{
		//备用命令
		if( Command == 0 ) {
			return;
		}
		if( Command == CMD_KeyPress ) {
			byte i = Buffer[4];
			//n_SYS.SYS.AddMessage( "PRESS: " + i + "\n" );
			//KeyEvent.MykeyDown( i );
			keybd_event(i, 0, 0, 0);
			System.Threading.Thread.Sleep( 100 );
			//KeyEvent.MykeyUp( i );
			keybd_event(i, 0, 2, 0);
			return;
		}
		if( Command == CMD_KeyDown ) {
			byte i = Buffer[4];
			//n_SYS.SYS.AddMessage( "DOWN: " + i + "\n" );
			//KeyEvent.MykeyDown( i );
			keybd_event(i, 0, 0, 0);
			return;
		}
		if( Command == CMD_KeyUp ) {
			byte i = Buffer[4];
			//n_SYS.SYS.AddMessage( "UP: " + i + "\n" );
			//KeyEvent.MykeyUp( i );
			keybd_event(i, 0, 2, 0);
			return;
		}
	}
}
}

