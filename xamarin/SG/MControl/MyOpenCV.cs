﻿
using System;
using n_SG;
using n_Common;
using n_SG_MyControl;
using n_SG_MyComboBox;

#if OS_android
using Android.Graphics;
#endif

#if OS_win
using c_PlaySound;
using System.IO.Ports;
using System.Drawing;
//using System.Windows.Forms;
#endif

namespace n_SG_MyOpenCV
{
public class MyOpenCV : MyControl
{
	public delegate void d_TouchDown( object sender );
	public d_TouchDown TouchDownEvent;
	
	const int CMD_SetValue = 0x10;
	const int CMD_SendData = 0x11;
	const int CMD_SetBaud = 0x12;
	
	const int CMD_SetStart = 0x13;
	const int CMD_SetN = 0x14;
	const int CMD_SetFont = 0x15;
	
	public const int v_Exist = 0x20;
	public bool DL_Exist;
	public const int v_TX = 0x21;
	public int DL_Tx;
	public const int v_TY = 0x22;
	public int DL_Ty;
	public const int v_TZ = 0x23;
	public int DL_Tz;
	
	public float FontSize;
	
	//构造函数 - 图片为空, 显示文本信息
	public MyOpenCV( Color bc, float fs, Color fc, int x, int y, int w, int h ) : base( x, y, w, h )
	{
		BackColor = bc;
		ForeColor = fc;
		FontSize = fs;
		
		if( SG.Running ) {
			//...
		}
	}
	
	//=========================================================================
	
	//绘制控件
	public override void Draw()
	{
		int x = GetAbsoluteStaX();
		int y = GetAbsoluteStaY();
		SG.MRectangle.Fill( BackColor, x, y, DL_Width, DL_Height );
		SG.MString.DrawAtCenter( "视频识别", ForeColor, FontSize, x + DL_Width/2, y + DL_Height / 2 );
	}
	
	//=========================================================================
	
	//触控按下事件
	public override bool TouchDown( int x, int y )
	{
		if( Contain( x, y ) ) {
			if( TouchDownEvent != null ) {
				TouchDownEvent( this );
			}
			isTouchDown = true;
		}
		else {
			isTouchDown = false;
		}
		
		return isTouchDown;
	}
	
	//触控松开事件
	public override void TouchUp( int x, int y )
	{
	}
	
	//触控移动事件
	public override bool TouchMove( int x, int y )
	{
		return false;
	}
	
	//==================================================================
	
	//命令解码
	public override void Decode( int Command, byte[] Buffer )
	{
		if( Command == CMD_SetValue ) {
			string s = DecodeGBK( Buffer );
			Open( s, false );
		}
		
		else if( Command == 0x80 + v_Exist ) {
			this.V_Decode( Command, Buffer );
		}
		else if( Command == 0x80 + v_TX ) {
			this.V_Decode( Command, Buffer );
		}
		else if( Command == 0x80 + v_TY ) {
			this.V_Decode( Command, Buffer );
		}
		else if( Command == 0x80 + v_TZ ) {
			this.V_Decode( Command, Buffer );
		}
		if( Command == CMD_SetFont ) {
			string s = DecodeGBK( Buffer );
			Common.Font f = Common.GetFontFromString( s );
			this.FontSize = (int)(f.Size);
		}
		else {
			this.V_Decode( Command, Buffer );
		}
	}
	
	protected override int ExRead( int Addr )
	{
		switch( Addr ) {
			case v_Exist:	return DL_Exist? 1: 0;
			case v_TX:		return DL_Tx;
			case v_TY:		return DL_Ty;
			case v_TZ:		return DL_Tz;
			default:		VCodeReadError( Addr ); return 0;
		}
	}
	
	protected override void ExWrite( int Addr, int Data )
	{
		switch( Addr ) {
			case v_Exist:	DL_Exist = Data != 0? true: false; break;
			case v_TX:		DL_Tx = Data; break;
			case v_TY:		DL_Ty = Data; break;
			case v_TZ:		DL_Tz = Data; break;
			default:		VCodeWriteError( Addr ); break;
		}
	}
	
	//==========================================
	
	//打开串口
	public void Open( string Name, bool DTR )
	{
		try {
			//...
		}
		catch {
			n_SYS.SYS.AddMessage( "OpenCV Open Error\n" );
		}
	}
}
}



