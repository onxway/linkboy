﻿
using System;
using n_SG;
using n_Common;
using n_SG_MyControl;

#if OS_android
using Android.Graphics;
#endif

#if OS_win
using System.Drawing;
#endif

namespace n_SG_MyTrackBar
{
public class MyTrackBar : MyControl
{
	const int v_MinValue = 0x21;
	int DL_MaxValue;
	const int v_MaxValue = 0x22;
	int DL_MinValue;
	
	public delegate void d_ValueChanged( object sender );
	public d_ValueChanged ValueChanged;
	
	//构造函数 - 图片为空, 显示文本信息
	public MyTrackBar( Color bc, Color fc, int x, int y, int w, int h ) : base( x, y, w, h )
	{
		DL_MaxValue = 100;
		DL_MinValue = 0;
		DL_Value = 0;
		
		BackColor = bc;
		ForeColor = fc;
	}
	
	//=========================================================================
	
	//绘制控件
	public override void Draw()
	{
		int x = GetAbsoluteStaX();
		int y = GetAbsoluteStaY();
		
		if( isTouchDown ) {
			
		}
		int LineHeight = DL_Height / 3;
		
		int sx = x + DL_Width * (DL_Value - DL_MinValue) / (DL_MaxValue - DL_MinValue);
		SG.MRectangle.Fill( BackColor, x, y + DL_Height/2 - LineHeight/2, DL_Width, LineHeight );
		SG.MCircle.Fill( BackColor, x, y + DL_Height/2, LineHeight/2 );
		SG.MCircle.Fill( BackColor, x + DL_Width, y + DL_Height/2, LineHeight/2 );
		
		SG.MCircle.Fill( ForeColor, 150, sx, y + DL_Height/2, DL_Height / 2 );
		SG.MCircle.Fill( Color.WhiteSmoke, 50, sx, y + DL_Height/2, LineHeight / 2 );
	}
	
	//=========================================================================
	
	//触控按下事件
	public override bool TouchDown( int x, int y )
	{
		if( Contain( x, y ) ) {
			isTouchDown = true;
			SetValue( x );
		}
		else {
			isTouchDown = false;
		}
		
		return isTouchDown;
	}
	
	//触控松开事件
	public override void TouchUp( int x, int y )
	{
		if( isTouchDown ) {
			
		}
		isTouchDown = false;
	}
	
	//触控移动事件
	public override bool TouchMove( int x, int y )
	{
		if( isTouchDown ) {
			if( x < V_StaX ) x = V_StaX;
			if( x >= V_StaX + DL_Width ) x = V_StaX + DL_Width;
			SetValue( x );
			return true;
		}
		
		if( Contain( x, y ) ) {
			return true;
		}
		else {
			return false;
		}
	}
	
	void SetValue( int x )
	{
		x += DL_Width/(DL_MaxValue - DL_MinValue)/2;
		int nv = DL_MinValue + (DL_MaxValue - DL_MinValue) * (x - V_StaX) / DL_Width;
		
		if( DL_Value != nv ) {
			DL_Value = nv;
			if( ValueChanged != null ) {
				ValueChanged( this );
			}
		}
	}
	
	//==================================================================
	
	//命令解码
	public override void Decode( int Command, byte[] Buffer )
	{
		this.V_Decode( Command, Buffer );
	}
	
	protected override int ExRead( int Addr )
	{
		switch( Addr ) {
			case v_MinValue:	return DL_MinValue;
			case v_MaxValue:	return DL_MaxValue;
			default:	VCodeReadError( Addr ); return 0;
		}
	}
	
	protected override void ExWrite( int Addr, int Data )
	{
		switch( Addr ) {
			case v_MinValue:	DL_MinValue = Data; break;
			case v_MaxValue:	DL_MaxValue = Data; break;
			default:	VCodeWriteError( Addr ); break;
		}
	}
}
}

