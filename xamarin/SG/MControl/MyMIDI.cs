﻿
using System;
using n_SG;
using n_Common;
using n_SG_MyControl;

#if OS_android
using Android.Graphics;
#endif

#if OS_win
using c_MIDI;
#endif

namespace n_SG_MyMIDI
{
public class MyMIDI : MyControl
{
	const int CMD_SetTimbre = 0x01;
	const int CMD_PlayTone = 0x02;
	const int CMD_StopTone = 0x03;
	
	int last0;
	int last1;
	
	//构造函数 - 图片为空, 显示文本信息
	public MyMIDI() : base( 0, 0, 0, 0 )
	{
		//DataList[v_Value] = 0;
		
		last0 = -1;
		last1 = -1;
	}
	
	//=========================================================================
	
	//绘制控件
	public override void Draw()
	{
		if( isHidenModule ) {
			return;
		}
	}
	
	//=========================================================================
	
	//触控按下事件
	public override bool TouchDown( int x, int y )
	{
		return false;
	}
	
	//触控松开事件
	public override void TouchUp( int x, int y )
	{
	}
	
	//触控移动事件
	public override bool TouchMove( int x, int y )
	{
		return false;
	}
	
	//==================================================================
	
	//命令解码
	public override void Decode( int Command, byte[] Buffer )
	{
		//备用命令
		if( Command == 0 ) {
			return;
		}
		if( Command == CMD_SetTimbre ) {
			int c = Buffer[4];
			int t = Buffer[5];
			Music.SetTimbre( c, t );
			return;
		}
		if( Command == CMD_PlayTone ) {
			int c = Buffer[4];
			int s = Buffer[5];
			int v = Buffer[6];
			
			if( c == 1 ) {
				if( last0 != -1 ) {
					Music.CloseSound( c, last0 );
				}
				Music.PlaySound( c, s, v );
				last0 = s;
			}
			if( c == 2 ) {
				if( last1 != -1 ) {
					Music.CloseSound( c, last1 );
				}
				Music.PlaySound( c, s, v );
				last1 = s;
			}
			return;
		}
		if( Command == CMD_StopTone ) {
			int c = Buffer[4];
			int s = Buffer[5];
			
			if( c == 0 ) {
				if( last0 != -1 ) {
					Music.CloseSound( c, last0 );
				}
			}
			if( c == 1 ) {
				if( last1 != -1 ) {
					Music.CloseSound( c, last1 );
				}
			}
			return;
		}
	}
}
}

