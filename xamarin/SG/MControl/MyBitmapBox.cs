﻿
using System;
using n_SG;
using n_Common;
using n_SG_MyControl;

#if OS_android
using Android.Graphics;
#endif

#if OS_win
using System.Drawing;
#endif

namespace n_SG_MyBitmapBox
{
public class MyBitmapBox : MyControl
{
	const int v_MapValue = 0x10;
	int DL_MapValue;
	
	const int CMD_Clear = 0x11;
	const int CMD_SetValue = 0x12;
	const int CMD_SetAddr = 0x13;
	
	Brush BackBrush;
	Brush ForeBrush;
	
	Bitmap BackBitmap;
	Graphics g;
	bool NeedRefresh;
	
	int CurrentArea;
	int CurrentLine;
	int[,] ImageBuffer;
	bool[,] ChangeList;
	public int LineNumber;
	public int AreaNumber;
	int PerWidth;
	
	//构造函数 - 图片为空, 显示文本信息
	public MyBitmapBox( Color bc, Color fc, int x, int y, int w, int h ) : base( x, y, w, h )
	{
		BackColor = bc;
		
		ForeColor = fc;
		
		PerWidth = 8;
		
		CurrentArea = 0;
		CurrentLine = 0;
		
		NeedRefresh = false;
		n_Linker.Linker.StartRun += RefreshBaseData;
		
		RefreshBaseData();
	}
	
	//更新数据
	public void RefreshBaseData()
	{
		BackBrush = new SolidBrush( BackColor );
		ForeBrush = new SolidBrush( ForeColor );
		
		BackBitmap = new Bitmap( DL_Width, DL_Height );
		g = Graphics.FromImage( BackBitmap );
		g.SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.HighSpeed;
		g.Clear( Color.Transparent );
		
		LineNumber = (DL_Width - 1) / PerWidth;
		AreaNumber = (DL_Height - 1) / PerWidth / 8;
		ImageBuffer = new int[LineNumber, AreaNumber];
		ChangeList = new bool[LineNumber, AreaNumber];
		
		int sx = (DL_Width - LineNumber*PerWidth)/2;
		int sy = (DL_Height - AreaNumber*8*PerWidth)/2;
		
		//绘制网格线
		for( int line = 0; line < LineNumber+1; ++line ) {
			int x = sx + line*PerWidth;
			g.DrawLine( Pens.Gray, x, sy, x, sy + (AreaNumber*8) * PerWidth );
		}
		for( int area = 0; area < AreaNumber*8+1; ++area ) {
			int y = sy + area*PerWidth;
			g.DrawLine( Pens.Gray, sx, y, sx + LineNumber * PerWidth, y );
		}
		
		ClearImage();
		
		NeedRefresh = true;
	}
	
	void ClearImage()
	{
		for( int line = 0; line < LineNumber; ++line ) {
			for( int area = 0; area < AreaNumber; ++area ) {
				ImageBuffer[line,area] = 0;
				ChangeList[line, area] = true;
			}
		}
		if( !SG.Running && LineNumber >= 8 && AreaNumber >= 1 ) {
			ImageBuffer[ 1, 0 ] = 0x24;
			ImageBuffer[ 2, 0 ] = 0x40;
			ImageBuffer[ 3, 0 ] = 0x80;
			ImageBuffer[ 4, 0 ] = 0x80;
			ImageBuffer[ 5, 0 ] = 0x40;
			ImageBuffer[ 6, 0 ] = 0x24;
		}
		DrawData();
	}
	
	//=========================================================================
	
	//绘制控件
	public override void Draw()
	{
		int x = GetAbsoluteStaX();
		int y = GetAbsoluteStaY();
		
		if( NeedRefresh ) {
			NeedRefresh = false;
			DrawData();
		}
		SG.MBitmap.Draw( BackBitmap, x, y );
	}
	
	//绘制当前数据缓冲区到背景图片中
	void DrawData()
	{
		SG.SetObject( g );
		
		//SG.MRectangle.Fill( BackColor, 0, 0, Width, Height );
		//SG.MString.DrawAtLeft( Value.ToString(), ForeColor, FontSize, x + Width/2, y + Height/2 );
		//SG.MLine.Draw( ForeColor, 2, 0, 0, Width, Height );
		
		int sx = (DL_Width - LineNumber*PerWidth)/2;
		int sy = (DL_Height - AreaNumber*8*PerWidth)/2;
		
		for( int line = 0; line < LineNumber; ++line ) {
			for( int area = 0; area < AreaNumber; ++area ) {
				if( !ChangeList[line, area] ) {
					continue;
				}
				ChangeList[line, area] = false;
				int b = ImageBuffer[line, area];
				for( int n = 0; n < 8; ++n ) {
					
					int x = line;
					int y = area*8+n;
					x = sx + x * PerWidth + 1;
					y = sy + y * PerWidth + 1;
					if( (b & 1) != 0 ) {
						g.FillRectangle( ForeBrush, x, y, PerWidth - 1, PerWidth - 1 );
						//g.DrawRectangle( BackColor, x, y, 7, 7 );
					}
					else {
						g.FillRectangle( BackBrush, x, y, PerWidth - 1, PerWidth - 1 );
						//g.DrawRectangle( BackColor, x, y, 7, 7 );
						
					}
					b >>= 1;
				}
			}
		}
		
		SG.SetPreObject();
	}
	
	//=========================================================================
	
	//触控按下事件
	public override bool TouchDown( int x, int y )
	{
		return false;
	}
	
	//触控松开事件
	public override void TouchUp( int x, int y )
	{
		isTouchDown = false;
	}
	
	//触控移动事件
	public override bool TouchMove( int x, int y )
	{
		return false;
	}
	
	//==================================================================
	
	//命令解码
	public override void Decode( int Command, byte[] Buffer )
	{
		if( Command == CMD_Clear ) {
			ClearImage();
		}
		else if( Command == CMD_SetValue ) {
			int Linel = Buffer[4];
			int Lineh = Buffer[5];
			int area = Buffer[6];
			int data = Buffer[7];
			
			ImageBuffer[Linel+Lineh*256,area] = data;
			ChangeList[Linel+Lineh*256, area] = true;
			NeedRefresh = true;
		}
		else if( Command == CMD_SetAddr ) {
			int Linel = Buffer[4];
			int Lineh = Buffer[5];
			CurrentLine = Linel + 256 * Lineh;
			CurrentArea = Buffer[6];
			DL_MapValue = ImageBuffer[CurrentLine,CurrentArea];
		}
		else {
			this.V_Decode( Command, Buffer );
		}
	}
	
	protected override int ExRead( int Addr )
	{
		switch( Addr ) {
			case v_MapValue:	return DL_MapValue;
			default:	VCodeReadError( Addr ); return 0;
		}
	}
}
}

