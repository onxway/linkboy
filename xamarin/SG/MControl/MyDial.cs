﻿
using System;
using n_SG;
using n_Common;
using n_SG_MyControl;

#if OS_android
using Android.Graphics;
#endif

#if OS_win
using System.Drawing;
#endif

namespace n_SG_MyDial
{
public class MyDial : MyControl
{
	const int v_Number = 0x20;
	
	public delegate void D_TouchDownEvent( object sender );
	public D_TouchDownEvent TouchDownEvent;
	
	public int Value;
	
	public Color BackColor;
	public Color ForeColor;
	
	//构造函数 - 图片为空, 显示文本信息
	public MyDial( Color bc, int v, Color fc, int x, int y, int w ) : base( x, y, w, w )
	{
		BackColor = bc;
		
		Value = v;
		ForeColor = fc;
	}
	
	//=========================================================================
	
	//绘制控件
	public override void Draw()
	{
		int x = GetAbsoluteStaX();
		int y = GetAbsoluteStaY();
		
		SG.MArc.Draw( Color.Red, 2, x + Width/2, y + Height/2, Width/2, 0, 90 );
		SG.MArc.Draw( Color.Blue, 2, x + Width/2, y + Height/2, Width/2, 0, Value );
	}
	
	//=========================================================================
	
	//触控按下事件
	public override bool TouchDown( int x, int y )
	{
		if( Contain( x, y ) ) {
			isTouchDown = true;
			if( TouchDownEvent != null ) {
				TouchDownEvent( this );
			}
		}
		else {
			isTouchDown = false;
		}
		
		return isTouchDown;
	}
	
	//触控松开事件
	public override void TouchUp( int x, int y )
	{
		isTouchDown = false;
	}
	
	//触控移动事件
	public override bool TouchMove( int x, int y )
	{
		return false;
	}
	
	//==================================================================
	
	//刷新控件参数
	protected override void Refresh()
	{
		int X = DataList[ v_X ];
		int Y = DataList[ v_Y ];
		int Width = DataList[ v_Width ];
		int Height = DataList[ v_Height ];
		this.StaX = X;
		this.StaY = Y;
		this.Width = Width;
		this.Height = Height;
		
		#if OS_android
		this.BackColor = new Color( DataList[ v_BackColor ] );
		this.ForeColor = new Color( DataList[ v_ForeColor ] );
		#endif
		
		#if OS_win
		this.BackColor = Color.FromArgb( DataList[ v_BackColor ] );
		this.ForeColor = Color.FromArgb( DataList[ v_ForeColor ] );
		#endif
		
		Value = DataList[ v_Number ];
	}
	
	//命令解码
	public override void Decode( int Command, byte[] Buffer )
	{
		if( Command == SetText || Command == SetFont ) {
			string s = "";
			for( int i = 4; i < Buffer.Length; i += 2 ) {
				int Low = Buffer[ i ];
				int High = Buffer[ i + 1 ];
				char c = (char)( Low + High * 256 );
				if( c == 0 ) {
					break;
				}
				s += c.ToString();
			}
			if( Command == SetText ) {
				if( s != "" ) {
					this.Value = int.Parse( s );
				}
			}
			else {
				//...
			}
		}
		else {
			this.V_Decode( Command, Buffer );
		}
	}
}
}

