﻿
using System;
using n_SG_MyControl;

#if OS_android
using Android.App;
using Android.Content;
using Android.OS;
using Android.Widget;
using Android.Runtime;
using Android.Util;
using Android.Views;
using Android.Graphics;
#endif

#if OS_win
using System.Drawing;
#endif

using n_SG;

namespace n_SG_MyPanel
{
public class MyPanel : MyControl
{
	public delegate void d_TouchDown( object sender, int mx, int my );
	public d_TouchDown TouchDownEvent;
	
	public delegate void d_TouchUp( object sender, int mx, int my );
	public d_TouchUp TouchUpEvent;
	
	public delegate void d_TouchClick( object sender, int mx, int my );
	public d_TouchClick TouchClickEvent;
	
	public delegate void d_TouchMove( object sender, int mx, int my );
	public d_TouchMove TouchMoveEvent;
	
	//预绘图事件
	public delegate void d_PreDrawEvent( MyPanel m );
	public d_PreDrawEvent PreDrawEvent;
	
	const int MaxNumber = 10;
	public MyControl[][] ControlList;
	public int[] ControlListLength;
	
	bool ObjectTouched;
	
	//鼠标按键状态 0: 无按键, 1: 左键按下
	const int v_ButtonIndex = 0x20;
	bool DL_ButtonPress;
	const int v_MouseX = 0x21;
	int DL_MouseX;
	const int v_MouseY = 0x22;
	int DL_MouseY;
	
	const int CMD_SetImage = 0x30;
	
	//public bool CanMove;
	public bool CanMoveX;
	public bool CanMoveY;
	public bool AllMove;
	
	string v_ExtendValue;
	public string ExtendValue {
		get {
			return v_ExtendValue;
		}
		set {
			if( v_ExtendValue != value ) {
				v_ExtendValue = value;
				
				#if OS_win
				if( System.IO.File.Exists( v_ExtendValue ) ) {
					Image ti = Image.FromFile( v_ExtendValue );
					BackBitmap = new Bitmap( ti );
					ti.Dispose();
					g = Graphics.FromImage( BackBitmap );
				}
				else if( System.IO.File.Exists( n_CommonData.CommonData.GetBasePath() + v_ExtendValue ) ) {
					Image ti = Image.FromFile( n_CommonData.CommonData.GetBasePath() + v_ExtendValue );
					BackBitmap = new Bitmap( ti );
					ti.Dispose();
					g = Graphics.FromImage( BackBitmap );
				}
				else {
					//这里应该显示一个错误的图片标志, 表示图片未找到
					BackBitmap = null;
				}
				#endif
			}
		}
	}
	
	Bitmap BackBitmap;
	
	#if OS_win
	public Graphics g;
	#endif
	
	#if OS_android
	public Android.Graphics.Canvas g;
	#endif
	
	public bool isBackGroud;
	
	int LastX;
	int LastY;
	
	int TempLastX;
	int TempLastY;
	public bool TempisMoved;
	
	MyControl tempSelected;
	
	public MyPanel( int x, int y, int w, int h ) : base( x, y, w, h )
	{
		ControlListLength = new int[ MaxNumber ];
		ControlList = new MyControl[ MaxNumber ][];
		for( int i = 0; i < MaxNumber; i++ ) {
			ControlList[i] = new MyControl[ 100 ];
			ControlListLength[i] = 0;
		}
		ObjectTouched = false;
		
		LastX = 0;
		LastY = 0;
		CanMoveX = false;
		CanMoveY = false;
		AllMove = false;
		
		tempSelected = null;
		TempLastX = 0;
		TempLastY = 0;
		TempisMoved = false;
		
		isBackGroud = false;
		
		v_ExtendValue = "";
		
		EdgeColor = Color.White;
		
		/*
		#if OS_win
		BackBitmap = new Bitmap( w, h );
		g = Graphics.FromImage( BackBitmap );
		#endif
		
		#if OS_android
		BackBitmap = Bitmap.CreateBitmap( w, h, Bitmap.Config.Argb8888 );
		#endif
		*/
	}
	
	public int Add( int Level, MyControl c )
	{
		ControlList[Level][ ControlListLength[Level] ] = c;
		ControlListLength[Level]++;
		
		c.owner = this;
		return ControlListLength[Level] - 1;
	}
	
	public void Clear( int Level )
	{
		for( int i = 0; i < ControlListLength[Level]; ++i ) {
			ControlList[Level][ i ] = null;
		}
		ControlListLength[Level] = 0;
	}
	
	//设置鼠标当前坐标
	public void SetMouseLocation( int x, int y )
	{
		DL_MouseX = x;
		DL_MouseY = Y_Default? y: -y;
		
		DL_ButtonPress = isTouchDown;
	}
	
	//----------------------------------------------------
	
	public override bool TouchDown( int x, int y )
	{
		if( !isBackGroud && !Contain( x, y ) ) {
			return false;
		}
		tempSelected = null;
		ObjectTouched = false;
		for( int i = MaxNumber - 1; i >= 0; i-- ) {
			if( TouchDownLevel( i, x - V_StaX, y - V_StaY ) ) {
				break;
			}
		}
		n_M.M.SelectedObj = tempSelected;
		
		//if( !ObjectTouched ) {
			isTouchDown = true;
			if( TouchDownEvent != null ) {
				TouchDownEvent( this, x, y );
			}
		//}
		
		if( AllMove || !ObjectTouched ) {
			LastX = x;
			LastY = y;
			
			TempLastX = x;
			TempLastY = y;
			TempisMoved = false;
		}
		return true;
	}
	
	public override void TouchUp( int x, int y )
	{
		if( isTouchDown ) {
			if( TouchUpEvent != null ) {
				TouchUpEvent( this, x, y );
			}
			if( TouchClickEvent != null ) {
				TouchClickEvent( this, x, y );
			}
		}
		isTouchDown = false;
		for( int i = MaxNumber - 1; i >= 0; i-- ) {
			for( int j = 0; j < ControlListLength[i]; j++ ) {
				if( !ControlList[i][j].Visible ) {
					continue;
				}
				if( MyControl.EditEnable ) {
					ControlList[i][j].EditTouchUp( x - V_StaX, y - V_StaY );
				}
				else {
					ControlList[i][j].TouchUp( x - V_StaX, y - V_StaY );
				}
			}
		}
	}
	
	public override bool TouchMove( int x, int y )
	{
		if( EditEnable ) {
			if( n_M.M.SelectedObj != null && n_M.M.SelectedObj.EditTouchMove( x, y ) ) {
				return true;
			}
		}
		if( isTouchDown && ( AllMove || !ObjectTouched ) ) {
			if( CanMoveX ) {
				V_StaX += x - LastX;
				LastX = x;
			}
			if( CanMoveY ) {
				V_StaY += y - LastY;
				LastY = y;
			}
		}
		
		if( Math.Abs( TempLastX - LastX ) + Math.Abs( TempLastY - LastY ) > 10 ) {
			TempisMoved = true;
		}
		else {
			TempisMoved = false;
		}
		
		if( !isBackGroud && !Contain( x, y ) ) {
			return false;
		}
		for( int i = MaxNumber - 1; i >= 0; i-- ) {
			for( int j = 0; j < ControlListLength[i]; j++ ) {
				if( !ControlList[i][j].Visible ) {
					continue;
				}
				if( MyControl.EditEnable ) {
					
					
					
					if( ControlList[i][j].EditTouchMove( x - V_StaX, y - V_StaY ) ) {
						//return true;
					}
				}
				else {
					if( ControlList[i][j].TouchMove( x - V_StaX, y - V_StaY ) ) {
						//return true;
					}
				}
				
			}
		}
		if( TouchMoveEvent != null ) {
			TouchMoveEvent( this, x, y );
		}
		return true;
	}
	
	public override void Draw()
	{
		int x = GetAbsoluteStaX();
		int y = GetAbsoluteStaY();
		
		if( BackBitmap != null ) {
			if( isBackGroud ) {
				int hh = Y_Default? 0: -DL_Height;
				SG.MBitmap.Draw( BackBitmap, 0, hh, DL_Width, DL_Height );
			}
			else {
				SG.MBitmap.Draw( BackBitmap, x, y, DL_Width, DL_Height );
				SG.MRectangle.Draw( EdgeColor, 1, x, y, DL_Width, DL_Height );
			}
		}
		else {
			//因为之前在M文件中已经清空了背景, 所以这里不再需要清空
			if( !isBackGroud ) {
				SG.MRectangle.Draw( BackColor, 1, x, y, DL_Width, DL_Height );
				
				//这里在编辑界面需要激活, 否则显示不了窗体颜色
				//SG.MRectangle.Fill( BackColor, x, y, V_Width, V_Height );
			}
		}
		if( PreDrawEvent != null ) {
			PreDrawEvent( this );
		}
		for( int i = 0; i < MaxNumber; i++ ) {
			DrawLevel( i );
		}
		if( n_SG_MyControl.MyControl.EditEnable && n_M.M.SelectedObj != null ) {
			n_M.M.SelectedObj.Draw1();
		}
	}
	
	//----------------------------------------------------
	
	protected bool TouchDownLevel( int Level, int x, int y )
	{
		if( EditEnable ) {
			if( n_M.M.SelectedObj != null && n_M.M.SelectedObj.EditTouchDown( x, y ) ) {
				tempSelected = n_M.M.SelectedObj;
				return true;
			}
		}
		for( int i = ControlListLength[Level] - 1; i >= 0; i-- ) {
			if( !ControlList[Level][i].Visible ) {
				continue;
			}
			if( MyControl.EditEnable ) {
				ObjectTouched |= ControlList[Level][i].EditTouchDown( x, y );
				if( ObjectTouched ) {
					tempSelected = ControlList[Level][i];
				}
			}
			else {
				ObjectTouched |= ControlList[Level][i].TouchDown( x, y );
			}
			if( ObjectTouched ) {
				break;
			}
		}
		
		if( tempSelected == null ) {
			if( Contain( x, y ) ) {
				tempSelected = this;
			}
		}
		return ObjectTouched;
	}
	
	protected void DrawLevel( int Level )
	{
		for( int i = 0; i < ControlListLength[Level]; i++ ) {
			if( !ControlList[Level][i].Visible ) {
				continue;
			}
			ControlList[Level][i].Draw();
		}
	}
	
	//==================================================================
	
	//命令解码
	public override void Decode( int Command, byte[] Buffer )
	{
		if( Command == SetText ) {
			string s = DecodeGBK( Buffer );
			#if OS_win
			n_SYS.SYS.SetTitle( s );
			#endif
		}
		else if( Command == CMD_SetImage ) {
			string s = DecodeGBK( Buffer );
			this.ExtendValue = s;
		}
		else {
			this.V_Decode( Command, Buffer );
		}
	}
	
	protected override int ExRead( int Addr )
	{
		switch( Addr ) {
			case v_ButtonIndex:	return GetInt( DL_ButtonPress );
			case v_MouseX:		return DL_MouseX;
			case v_MouseY:		return DL_MouseY;
			default:		VCodeReadError( Addr ); return 0;
		}
	}
}
}

