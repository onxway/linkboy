﻿
//版本记录：
//2015.7.14 图片替换颜色这里太慢了, 最后发现是JNI费时间, 替换成先把所有像素读出来再写回去解决问题;


#if OS_android
using Android.Graphics;
#endif

#if OS_win
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Drawing.Imaging;
#endif

namespace n_SG
{
public static class SG
{
	public static bool Running;
	
	#if OS_android
	static Android.Graphics.Canvas canvas_once;
	static Android.Graphics.Canvas canvas;
	#endif
	
	#if OS_win
	static Graphics g_once;
	public static Graphics g;
	#endif
	
	//初始化
	public static void Init()
	{
		#if OS_android
		canvas_once = null;
		canvas = null;
		#endif
		
		#if OS_win
		g_once = null;
		g =null;
		#endif
		
		Running = true;
		
		MString.Init();
		MLine.Init();
		MRectangle.Init();
		MCircle.Init();
		MEllipse.Init();
		MArc.Init();
		MBitmap.Init();
	}
	
	//设置图形对象
	public static void SetObject( object c )
	{
		#if OS_android
		canvas_once = canvas;
		canvas = (Android.Graphics.Canvas)c;
		#endif
		
		#if OS_win
		g_once = g;
		g = (Graphics)c;
		#endif
	}
	
	//设置图形对象为之前的对象
	public static void SetPreObject()
	{
		#if OS_android
		canvas = canvas_once;
		#endif
		
		#if OS_win
		g = g_once;
		#endif
	}
	
	//平移变换
	public static void Transfer( float x, float y )
	{
		#if OS_win
		g.TranslateTransform( x, y );
		#endif
	}
	
	//旋转指定的角度
	public static void Rotate( float x, float y, float i )
	{
		#if OS_win
		g.TranslateTransform( x, y );
		g.RotateTransform( i );
		g.TranslateTransform( -x, -y );
		#endif
	}
	
	//放缩指定的比例
	public static void Scale( float x, float y, float i )
	{
		#if OS_win
		g.TranslateTransform( x, y );
		g.ScaleTransform( i, i );
		g.TranslateTransform( -x, -y );
		#endif
	}
	
	//翻转
	public static void Swap( float x, float y, float i, float j )
	{
		#if OS_win
		g.TranslateTransform( x, y );
		g.ScaleTransform( i, j );
		g.TranslateTransform( -x, -y );
		#endif
	}
	
	//恢复原状
	public static void Reset()
	{
		#if OS_win
		//不要用这个指令, 否则会复位所有设置, 包括界面放缩参数
		//g.ResetTransform();
		
		
		
		#endif
	}
	
	//=========================================================================
	//绘制字符串类
	public static class MString
	{
		public static bool Bold = false;
		
		#if OS_android
		static Paint paint;
		#endif
		
		#if OS_win
		static SolidBrush BrushFill0;
		public static Font[] font;
		static Font[] font0;
		static Font[] font1;
		static SolidBrush TextBrush;
		public static string FontName = null;
		#endif
		
		//--------------------------------------------------------
		
		//初始化绘制辅助量
		public static void Init()
		{
			#if OS_android
			paint = new Paint() { Color = Color.Black, AntiAlias = true, StrokeWidth = 1, Alpha = 255 };
			paint.SetStyle( Paint.Style.Fill );
			#endif
			
			#if OS_win
			BrushFill0 = new SolidBrush(Color.Black);
			TextBrush = new SolidBrush( Color.Black );
			
			//创建默认字体
			font0 = new Font[1000];
			font0[0] = new Font( "微软雅黑", 0.1f );
			for( int i = 1; i < font0.Length; i++ ) {
				font0[i] = new Font( "微软雅黑", (float)i / 10 );
			}
			font = font0;
			
			#endif
		}
		
		//添加新字体
		public static void AddNewFont( string Name, bool Bold )
		{
			//创建特殊字体
			font1 = new Font[1000];
			font1[0] = new Font( Name, 0.1f );
			for( int i = 1; i < font1.Length; i++ ) {
				if( Bold ) {
					font1[i] = new Font( Name, (float)i / 10, FontStyle.Bold );
				}
				else {
					font1[i] = new Font( Name, (float)i / 10 );
				}
			}
			
		}
		
		//设置字体
		public static void SetDefultFont( bool b )
		{
			if( b ) {
				font = font0;
			}
			else {
				font = font1;
			}
		}
		
		//绘制字符串, 靠左显示
		public static void DrawAtLeft( string mes, Color c, float size, float x, float y )
		{
			#if OS_android
			paint.Color = c;
			paint.TextSize = size;
			paint.TextAlign = Paint.Align.Left;
			
			Paint.FontMetricsInt fontMetrics = paint.GetFontMetricsInt();
			int baseline = y - (fontMetrics.Bottom - fontMetrics.Top) / 2 - fontMetrics.Top;
			
			canvas.DrawText( mes, x, baseline, paint );
			#endif
			
			#if OS_win
			int SizeIndex = (int)(size * 10);
			SizeF s = g.MeasureString( mes, font[SizeIndex] );
			TextBrush.Color = c;
			g.DrawString( mes, font[SizeIndex], TextBrush, x, y - (int)s.Height / 2 );
			#endif
		}
		
		//绘制字符串, 居中显示
		public static void DrawAtCenter( string mes, Color c, float size, float x, float y )
		{
			#if OS_android
			paint.Color = c;
			paint.TextSize = size;
			paint.TextAlign = Paint.Align.Center;
			
			Paint.FontMetricsInt fontMetrics = paint.GetFontMetricsInt();
			int baseline = y - (fontMetrics.Bottom - fontMetrics.Top) / 2 - fontMetrics.Top;
			
			canvas.DrawText( mes, x, baseline, paint );
			#endif
			
			#if OS_win
			int SizeIndex = (int)(size * 10);
			SizeF s = g.MeasureString( mes, font[SizeIndex] );
			TextBrush.Color = c;
			g.DrawString( mes, font[SizeIndex], TextBrush, x - (int)s.Width / 2, y - (int)s.Height / 2 );
			#endif
		}
		
		//绘制字符串, 靠右显示
		public static void DrawAtRight( string mes, Color c, float size, float x, float y )
		{
			#if OS_android
			paint.Color = c;
			paint.TextSize = size;
			paint.TextAlign = Paint.Align.Right;
			
			Paint.FontMetricsInt fontMetrics = paint.GetFontMetricsInt();
			int baseline = y - (fontMetrics.Bottom - fontMetrics.Top) / 2 - fontMetrics.Top;
			
			canvas.DrawText( mes, x, baseline, paint );
			#endif
			
			#if OS_win
			int SizeIndex = (int)(size * 10);
			SizeF s = g.MeasureString( mes, font[SizeIndex] );
			TextBrush.Color = c;
			g.DrawString( mes, font[SizeIndex], TextBrush, x - (int)s.Width, y - (int)s.Height / 2 );
			#endif
		}
		
		//绘制字符串, 靠右显示 012 分别表示指定的参考点靠左/居中/靠右 , 靠上/居中/靠下
		public static void Draw( string mes, Color c, float size, int PX, int PY, float x, float y )
		{
			#if OS_android
			paint.Color = c;
			paint.TextSize = size;
			paint.TextAlign = Paint.Align.Right;
			
			Paint.FontMetricsInt fontMetrics = paint.GetFontMetricsInt();
			int baseline = y - (fontMetrics.Bottom - fontMetrics.Top) / 2 - fontMetrics.Top;
			
			canvas.DrawText( mes, x, baseline, paint );
			#endif
			
			#if OS_win
			int SizeIndex = (int)(size * 10);
			SizeF s = g.MeasureString( mes, font[SizeIndex] );
			TextBrush.Color = c;
			float ox = 0;
			switch( PX ) {
				case 0: ox = 0; break;
				case 1: ox = s.Width/2; break;
				case 2: ox = s.Width; break;
			}
			float oy = 0;
			switch( PY ) {
				case 0: oy = 0; break;
				case 1: oy = s.Height/2; break;
				case 2: oy = s.Height; break;
			}
			g.DrawString( mes, font[SizeIndex], TextBrush, x - ox, y - oy );
			#endif
		}
		
		//绘制字符串, 方框内显示
		public static void DrawAtRectangle( string mes, Color c, float size, float x, float y, float w, float h )
		{
			#if OS_android
			n_SYS.SYS.AddMessage( "不支持绘制文本到矩形\n" );
			#endif
			
			#if OS_win
			int SizeIndex = (int)(size * 10);
			TextBrush.Color = c;
			g.DrawString( mes, font[SizeIndex], TextBrush, new RectangleF( x, y, w + 5, h + 1 ) );
			#endif
		}
		
		//给定长度, 测量字符串的显示长度和宽度
		public static void MeasureSize( string mes, float size, int MaxWidth, out int w, out int h )
		{
			#if OS_win
			int SizeIndex = (int)(size * 10);
			SizeF s = g.MeasureString( mes, font[SizeIndex], MaxWidth );
			w = (int)s.Width;
			h = (int)s.Height;
			#endif
			
			#if OS_android
			w = 0;
			h = 0;
			#endif
		}
	}
	//=========================================================================
	//线条绘制类
	public static class MLine
	{
		#if OS_android
		static Paint PaintDraw0;
		static Paint PaintDrawC2;
		#endif
		
		#if OS_win
		static Pen PenDrawLine0;
		static Pen PenDrawLineDot0;
		#endif
		
		//--------------------------------------------------------
		
		//初始化绘制辅助量
		public static void Init()
		{
			#if OS_android
			PaintDraw0 = new Paint() { Color = Color.Black, AntiAlias = true, StrokeWidth = 1 };
			PaintDraw0.SetStyle( Paint.Style.Stroke );
			PaintDraw0.Alpha = 255;
			
			PaintDrawC2 = new Paint() { Color = Color.Black, AntiAlias = true, StrokeWidth = 1 };
			#endif
			
			#if OS_win
			PenDrawLine0 = new Pen(Color.Black);
			PenDrawLine0.Width = 1;
			PenDrawLineDot0 = new Pen(Color.Black);
			PenDrawLineDot0.DashStyle = DashStyle.Dot;
			PenDrawLineDot0.Width = 1;
			#endif
		}
		
		//绘制直线0
		public static void Draw( Color c, float w, float x1, float y1, float x2, float y2 )
		{
			#if OS_android
			PaintDraw0.Color = c;
			PaintDraw0.StrokeWidth = w;
			canvas.DrawLine( x1, y1, x2, y2, PaintDraw0 );
			#endif
			
			#if OS_win
			PenDrawLine0.Color = c;
			PenDrawLine0.Width = w;
			g.DrawLine( PenDrawLine0, x1, y1, x2, y2 );
			#endif
		}
		
		//绘制虚直线0
		public static void DrawDot( Color c, float w, float x1, float y1, float x2, float y2 )
		{
			#if OS_android
			PaintDraw0.Color = c;
			PaintDraw0.StrokeWidth = w;
			canvas.DrawLine( x1, y1, x2, y2, PaintDraw0 );
			#endif
			
			#if OS_win
			PenDrawLineDot0.Color = c;
			PenDrawLineDot0.Width = w;
			g.DrawLine( PenDrawLineDot0, x1, y1, x2, y2 );
			#endif
		}
		
		//绘制彩色线条1
		public static void Draw( Color c1, Color c2, float w, float x1, float y1, float x2, float y2 )
		{
			#if OS_android
			int[] colors = new int[2];
			float[] positions = new float[2];
			// 第1个点
			colors[0] = c1;
			positions[0] = 0;
			// 第2个点
			colors[1] = c2;
			positions[1] = 1;
			LinearGradient shader = new LinearGradient( x1, y1, x2, y2, colors, positions, Shader.TileMode.Mirror);
			PaintDrawC2.SetShader(shader);
			PaintDrawC2.StrokeWidth = w;
			
			canvas.DrawLine(x1, y1, x2, y2, PaintDrawC2 );
			#endif
		}
	}
	
	//=========================================================================
	//绘制矩形框类
	public static class MRectangle
	{
		#if OS_android
		static Paint PaintDraw0;
		static Paint PaintFill0;
		static Paint PaintFill_alpha;
		
		static Paint PaintDrawRound0;
		static Paint PaintFillRound0;
		static Paint PaintFillRound_alpha;
		
		static RectF rectf;
		#endif
		
		#if OS_win
		static Pen PenDraw0;
		static SolidBrush BrushFill0;
		static SolidBrush BrushFill_alpha;
		static GraphicsPath roundedRect;
		#endif
		
		//--------------------------------------------------------
		
		//初始化绘制辅助量
		public static void Init()
		{
			#if OS_android
			PaintDraw0 = new Paint() { Color = Color.Black, AntiAlias = true, StrokeWidth = 1 };
			PaintDraw0.SetStyle( Paint.Style.Stroke );
			PaintDraw0.Alpha = 255;
			
			PaintFill0 = new Paint() { Color = Color.Black, AntiAlias = true, StrokeWidth = 1 };
			PaintFill0.SetStyle( Paint.Style.Fill );
			PaintFill0.Alpha = 255;
			
			PaintFill_alpha = new Paint() { Color = Color.Black, AntiAlias = true, StrokeWidth = 1 };
			PaintFill_alpha.SetStyle( Paint.Style.Fill );
			
			PaintDrawRound0 = new Paint() { Color = Color.Black, AntiAlias = true, StrokeWidth = 1 };
			PaintDrawRound0.SetStyle( Paint.Style.Stroke );
			PaintDrawRound0.Alpha = 255;
			
			PaintFillRound0 = new Paint() { Color = Color.Black, AntiAlias = true, StrokeWidth = 1 };
			PaintFillRound0.SetStyle( Paint.Style.Fill );
			PaintFillRound0.Alpha = 255;
			
			PaintFillRound_alpha = new Paint() { Color = Color.Black, AntiAlias = true, StrokeWidth = 1 };
			PaintFillRound_alpha.SetStyle( Paint.Style.Fill );
			
			rectf = new RectF( 0, 0, 0, 0 );
			#endif
			
			#if OS_win
			PenDraw0 = new Pen( Color.Black );
			PenDraw0.Width = 1;
			
			BrushFill0 = new SolidBrush( Color.Black );
			BrushFill_alpha = new SolidBrush( Color.Black );
			
			roundedRect = new GraphicsPath();
			#endif
		}
		
		//绘制矩形0
		public static void Draw( Color c, float w, float x, float y, float width, float height )
		{
			#if OS_android
			PaintDraw0.Color = c;
			PaintDraw0.StrokeWidth = w;
			
			canvas.DrawRect( x, y, x + width, y + height, PaintDraw0 );
			#endif
			
			#if OS_win
			PenDraw0.Color = c;
			PenDraw0.Width = w;
			g.DrawRectangle( PenDraw0, x, y, width, height );
			#endif
		}
		
		//填充矩形0
		public static void Fill( Color c, float x, float y, float width, float height )
		{
			#if OS_android
			PaintFill0.Color = c;
			
			canvas.DrawRect( x, y, x + width, y + height, PaintFill0 );
			#endif
			
			#if OS_win
			BrushFill0.Color = c;
			g.FillRectangle( BrushFill0, x, y, width, height );
			#endif
		}
		
		//填充矩形, 带有透明度
		public static void Fill( Color c, int alpha, float x, float y, float width, float height )
		{
			#if OS_android
			PaintFill_alpha.Color = c;
			PaintFill_alpha.Alpha = alpha;
			
			canvas.DrawRect( x, y, x + width, y + height, PaintFill_alpha );
			#endif
			
			#if OS_win
			c = Color.FromArgb( alpha, c.R, c.G, c.B );
			BrushFill_alpha.Color = c;
			g.FillRectangle( BrushFill_alpha, x, y, width, height );
			#endif
		}
		
		//绘制圆角矩形0
		public static void DrawRound( Color c, int w, int rx, int ry, int x, int y, int width, int height )
		{
			#if OS_android
			PaintDrawRound0.Color = c;
			PaintDrawRound0.StrokeWidth = w;
			
			rectf.Left = x;
			rectf.Top = y;
			rectf.Right = x + width;
			rectf.Bottom = y + height;
			
			canvas.DrawRoundRect( rectf, rx, ry, PaintDrawRound0 );
			#endif
			
			#if OS_win
			roundedRect.Reset();
			bool isRound = false;
			if( rx != 0 && ry != 0 ) {
				isRound = true;
			}
			if( isRound ) {
				roundedRect.AddArc( x, y, rx * 2, ry * 2, 180, 90);
			}
			roundedRect.AddLine( x + rx, y, x + width - rx, y );
			if( isRound ) {
				roundedRect.AddArc( x + width - rx * 2, y, rx * 2, ry * 2, 270, 90);
			}
			roundedRect.AddLine( x + width, y + ry, x + width, y + height - ry );
			if( isRound ) {
				roundedRect.AddArc( x + width - rx * 2, y + height - ry * 2, rx * 2, ry * 2, 0, 90);
			}
			roundedRect.AddLine( x + width - rx, y + height, x + rx, y + height );
			if( isRound ) {
				roundedRect.AddArc( x, y + height - ry * 2, rx * 2, ry * 2, 90, 90);
			}
			roundedRect.AddLine( x, y + height - ry, x, y + ry);
			roundedRect.CloseFigure();
			PenDraw0.Color = c;
			PenDraw0.Width = w;
			g.DrawPath( PenDraw0, roundedRect );
			#endif
		}
		
		//填充圆角矩形0
		public static void FillRound( Color c, int rx, int ry, int x, int y, int width, int height )
		{
			#if OS_android
			PaintFillRound0.Color = c;
			
			rectf.Left = x;
			rectf.Top = y;
			rectf.Right = x + width;
			rectf.Bottom = y + height;
			
			canvas.DrawRoundRect( rectf, rx, ry, PaintFillRound0 );
			#endif
			
			#if OS_win
			roundedRect.Reset();
			bool isRound = false;
			if( rx != 0 && ry != 0 ) {
				isRound = true;
			}
			if( isRound ) {
				roundedRect.AddArc( x, y, rx * 2, ry * 2, 180, 90);
			}
			roundedRect.AddLine( x + rx, y, x + width - rx, y );
			if( isRound ) {
				roundedRect.AddArc( x + width - rx * 2, y, rx * 2, ry * 2, 270, 90);
			}
			roundedRect.AddLine( x + width, y + ry, x + width, y + height - ry );
			if( isRound ) {
				roundedRect.AddArc( x + width - rx * 2, y + height - ry * 2, rx * 2, ry * 2, 0, 90);
			}
			roundedRect.AddLine( x + width - rx, y + height, x + rx, y + height );
			if( isRound ) {
				roundedRect.AddArc( x, y + height - ry * 2, rx * 2, ry * 2, 90, 90);
			}
			roundedRect.AddLine( x, y + height - ry, x, y + ry);
			roundedRect.CloseFigure();
			BrushFill0.Color = c;
			g.FillPath( BrushFill0, roundedRect );
			#endif
		}
		
		//填充圆角矩形1, 带有透明度
		public static void FillRound( Color c, int alpha, int rx, int ry, int x, int y, int width, int height )
		{
			#if OS_android
			PaintFillRound_alpha.Color = c;
			PaintFillRound_alpha.Alpha = alpha;
			
			rectf.Left = x;
			rectf.Top = y;
			rectf.Right = x + width;
			rectf.Bottom = y + height;
			
			canvas.DrawRoundRect( rectf, rx, ry, PaintFillRound_alpha );
			#endif
		}
	}
	//=========================================================================
	//绘制圆形类
	public static class MCircle
	{
		#if OS_android
		static Paint PaintDraw0;
		static Paint PaintFill0;
		static Paint PaintFill_alpha;
		#endif
		
		#if OS_win
		static Pen PenDraw0;
		static SolidBrush BrushFill0;
		#endif
		
		//--------------------------------------------------------
		
		//初始化绘制辅助量
		public static void Init()
		{
			#if OS_android
			PaintDraw0 = new Paint() { Color = Color.Black, AntiAlias = true, StrokeWidth = 1 };
			PaintDraw0.SetStyle( Paint.Style.Stroke );
			PaintDraw0.Alpha = 255;
			
			PaintFill0 = new Paint() { Color = Color.Black, AntiAlias = true, StrokeWidth = 1 };
			PaintFill0.SetStyle( Paint.Style.Fill );
			PaintFill0.Alpha = 255;
			
			PaintFill_alpha = new Paint() { Color = Color.Black, AntiAlias = true, StrokeWidth = 1 };
			PaintFill_alpha.SetStyle( Paint.Style.Fill );
			#endif
			
			#if OS_win
			PenDraw0 = new Pen( Color.Black );
			PenDraw0.Width = 1;
			
			BrushFill0 = new SolidBrush( Color.Black );
			#endif
		}
		
		//绘制圆形0
		public static void Draw( Color c, float w, float x, float y, float radius )
		{
			#if OS_android
			PaintDraw0.Color = c;
			PaintDraw0.StrokeWidth = w;
			
			canvas.DrawCircle( x, y, radius, PaintDraw0 );
			#endif
			
			#if OS_win
			PenDraw0.Color = c;
			PenDraw0.Width = w;
			g.DrawEllipse( PenDraw0, x - radius, y - radius, radius * 2, radius * 2 );
			#endif
		}
		
		//填充圆形0
		public static void Fill( Color c, float x, float y, float radius )
		{
			#if OS_android
			PaintFill0.Color = c;
			
			canvas.DrawCircle( x, y, radius, PaintFill0 );
			#endif
			
			#if OS_win
			BrushFill0.Color = c;
			g.FillEllipse( BrushFill0, x - radius, y - radius, radius * 2, radius * 2 );
			#endif
		}
		
		//填充圆形1, 带有透明度
		public static void Fill( Color c, int alpha, float x, float y, float radius )
		{
			#if OS_android
			PaintFill_alpha.Color = c;
			PaintFill_alpha.Alpha = alpha;
			
			canvas.DrawCircle( x, y, radius, PaintFill_alpha );
			#endif
			
			#if OS_win
			Color fc = Color.FromArgb( alpha, c.R, c.G, c.B );
			BrushFill0.Color = fc;
			g.FillEllipse( BrushFill0, x - radius, y - radius, radius * 2, radius * 2 );
			#endif
		}
	}
	//=========================================================================
	//绘制椭圆类
	public static class MEllipse
	{
		#if OS_android
		static Paint PaintDraw0;
		static Paint PaintFill0;
		static Paint PaintFill_alpha;
		static RectF rectf;
		#endif
		
		#if OS_win
		static Pen PenDraw0;
		static SolidBrush BrushFill0;
		#endif
		
		//--------------------------------------------------------
		
		//初始化绘制辅助量
		public static void Init()
		{
			#if OS_android
			
			rectf = new RectF( 0, 0, 0, 0 );
			
			PaintDraw0 = new Paint() { Color = Color.Black, AntiAlias = true, StrokeWidth = 1 };
			PaintDraw0.SetStyle( Paint.Style.Stroke );
			PaintDraw0.Alpha = 255;
			
			PaintFill0 = new Paint() { Color = Color.Black, AntiAlias = true, StrokeWidth = 1 };
			PaintFill0.SetStyle( Paint.Style.Fill );
			PaintFill0.Alpha = 255;
			
			PaintFill_alpha = new Paint() { Color = Color.Black, AntiAlias = true, StrokeWidth = 1 };
			PaintFill_alpha.SetStyle( Paint.Style.Fill );
			#endif
			
			#if OS_win
			PenDraw0 = new Pen( Color.Black );
			PenDraw0.Width = 1;
			
			BrushFill0 = new SolidBrush( Color.Black );
			#endif
		}
		
		//绘制椭圆0
		public static void Draw( Color c, int width, int x, int y, int w, int h )
		{
			#if OS_android
			PaintDraw0.Color = c;
			PaintDraw0.StrokeWidth = w;
			rectf.Left = x;
			rectf.Top = y;
			rectf.Right = x + w;
			rectf.Bottom = y + h;
			canvas.DrawOval( rectf, PaintDraw0 );
			#endif
			
			#if OS_win
			PenDraw0.Color = c;
			PenDraw0.Width = width;
			g.DrawEllipse( PenDraw0, x, y, w, h );
			#endif
		}
		
		//填充椭圆0
		public static void Fill( Color c, int x, int y, int w, int h )
		{
			#if OS_android
			PaintFill0.Color = c;
			rectf.Left = x;
			rectf.Top = y;
			rectf.Right = x + w;
			rectf.Bottom = y + h;
			canvas.DrawOval( rectf, PaintFill0 );
			#endif
			
			#if OS_win
			BrushFill0.Color = c;
			g.FillEllipse( BrushFill0, x, y, w, h );
			#endif
		}
	}
	//=========================================================================
	//绘制弧线类
	public static class MArc
	{
		#if OS_android
		static Paint PaintDraw0;
		static RectF rectf;
		#endif
		
		#if OS_win
		
		#endif
		
		//--------------------------------------------------------
		
		//初始化绘制辅助量
		public static void Init()
		{
			#if OS_android
			PaintDraw0 = new Paint() { Color = Color.Black, AntiAlias = true, StrokeWidth = 1 };
			PaintDraw0.SetStyle( Paint.Style.Stroke );
			PaintDraw0.Alpha = 255;
			rectf = new RectF( 0, 0, 0, 0 );
			#endif
		}
		
		//绘制弧形
		//c:			颜色
		//w:			线条宽度
		//x:			圆弧中心X坐标
		//y:			圆弧中心Y坐标
		//radius:		圆弧半径
		//startAngle:	起始角度
		//sweepAngle:	圆弧本身角度
		public static void Draw( Color c, int w, int x, int y, int radius, int startAngle, int sweepAngle )
		{
			#if OS_android
			PaintDraw0.Color = c;
			PaintDraw0.StrokeWidth = w;
			
			rectf.Left = x - radius;
			rectf.Top = y - radius;
			rectf.Right = x + radius;
			rectf.Bottom = y + radius;
			
			canvas.DrawArc( rectf, startAngle, sweepAngle, false, PaintDraw0 );
			#endif
		}
	}
	//=========================================================================
	//绘制图片类
	public static class MBitmap
	{
		#if OS_android
		static RectF rectf;
		static int[] Pixel;
		#endif
		
		#if OS_win
		static float[][] nArray;
		static ImageAttributes attributes;
		#endif
		
		//--------------------------------------------------------
		
		//初始化绘制辅助量
		public static void Init()
		{
			#if OS_android
			rectf = new RectF( 0, 0, 0, 0 );
			#endif
			
			#if OS_win
			nArray = new float[][] {
				new float[] { 1, 0, 0, 0, 0 },
				new float[] { 0, 1, 0, 0, 0 },
				new float[] { 0, 0, 1, 0, 0 },
				new float[] { 0, 0, 0, 1, 0 },
				new float[] { 0, 0, 0, 0, 1 }
			};
			attributes = new ImageAttributes();
			#endif
		}
		
		//初始化图片替换器
		public static void InitReplace( int w, int h )
		{
			#if OS_android
			Pixel = new int[w * h];
			#endif
		}
		
		//获取灰度图片
		public static Bitmap GetGrayBitmap( Bitmap bitmap, int A, int B )
		{
			#if OS_android
			int width = bitmap.Width;
			int height = bitmap.Height;
			Bitmap br = Bitmap.CreateBitmap( width, height, bitmap.GetConfig() );
			
			Canvas canvas = new Canvas( br );//以base为模板创建canvas对象
			canvas.DrawBitmap(bitmap, new Matrix(),new Paint());
			
			for(int i = 0; i < width; i++)//遍历像素点
			{
				for(int j = 0; j < height; j++)
				{
					int color = bitmap.GetPixel(i, j);
					
					int r = Color.GetRedComponent( color );
					int g = Color.GetGreenComponent( color );
					int b = Color.GetBlueComponent( color );
					int a = Color.GetAlphaComponent( color );
					
					int avg = (r+g+b)/3;//RGB均值
					
					if( avg > 230 ) {
						br.SetPixel(i, j,Color.Argb(a, r, g, b));
					}
					else {
						avg = A + (B-A) * avg / 255;
						br.SetPixel(i, j,Color.Argb(a, avg, avg, avg));
					}
					
					
				}
			}
			return br;
			#endif
			
			#if OS_win
			return null;
			#endif
		}
		
		//获取蓝色图片
		public static Bitmap GetBlueBitmap( Bitmap bitmap )
		{
			#if OS_android
			int width = bitmap.Width;
			int height = bitmap.Height;
			Bitmap br = Bitmap.CreateBitmap( width, height, bitmap.GetConfig() );
			
			Canvas canvas = new Canvas( br );//以base为模板创建canvas对象
			canvas.DrawBitmap(bitmap, new Matrix(),new Paint());
			
			for(int i = 0; i < width; i++)//遍历像素点
			{
				for(int j = 0; j < height; j++)
				{
					int color = bitmap.GetPixel(i, j);
					
					int r = Color.GetRedComponent( color );
					int g = Color.GetGreenComponent( color );
					int b = Color.GetBlueComponent( color );
					int a = Color.GetAlphaComponent( color );
					
					int avg = (r+g+b)/3;//RGB均值
					int red_green = 255 - ( 255 - avg ) / 2;
					if( red_green < 0 ) {
						red_green = 0;
					}
					
					
					if( avg > 230 ) {
						br.SetPixel(i, j,Color.Argb(a, r, g, b));
					}
					else {
						br.SetPixel(i, j,Color.Argb(a, red_green, red_green, 255) );
					}
				}
			}
			return br;
			#endif
			
			#if OS_win
			return null;
			#endif
		}
		
		//颜色替换
		public static Bitmap Replace( Bitmap bitmap, Color Old, Color New )
		{
			#if OS_android
			int width = bitmap.Width;
			int height = bitmap.Height;
			
			Bitmap br = Bitmap.CreateBitmap( width, height, bitmap.GetConfig() );
			Canvas canvas = new Canvas( br );//以base为模板创建canvas对象
			canvas.DrawBitmap(bitmap, new Matrix(),new Paint());
			
			bitmap.GetPixels( Pixel, 0, width, 0, 0, width, height );
			for( int i = 0; i < width; i++ ) {
				for( int j = 0; j < height; j++ ) {
					if( Pixel[i*width+j] == Old.ToArgb() ) {
						Pixel[i*width+j] = New.ToArgb();
					}
				}
			}
			br.SetPixels( Pixel, 0, width, 0, 0, width, height );
			return br;
			
//			for( int i = 0; i < width; i++ ) {
//				for( int j = 0; j < height; j++ ) {
//					int color = bitmap.GetPixel(i, j);
//					if( color == Old.ToArgb() ) {
//						br.SetPixel(i, j, New );
//					}
//				}
//			}
//			return br;
			#endif
			
			#if OS_win
			return null;
			#endif
		}
		
		//颜色替换, 用新颜色替换除了白色和
		public static Bitmap Replace_NeedChangToFast( Bitmap bitmap, Color New )
		{
			#if OS_android
			int width = bitmap.Width;
			int height = bitmap.Height;
			Bitmap br = Bitmap.CreateBitmap( width, height, bitmap.GetConfig() );
			
			Canvas canvas = new Canvas( br );//以base为模板创建canvas对象
			canvas.DrawBitmap(bitmap, new Matrix(),new Paint());
			
			//Bitmap br = bitmap.Copy( bitmap.GetConfig(), true );
			
			for(int i = 0; i < width; i++)//遍历像素点
			{
				for(int j = 0; j < height; j++)
				{
					int color = bitmap.GetPixel(i, j);
					
					int r = Color.GetRedComponent( color );
					int g = Color.GetGreenComponent( color );
					int b = Color.GetBlueComponent( color );
					int a = Color.GetAlphaComponent( color );
					
					int avg = (r+g+b)/3;//RGB均值
					if( a == 0 ) {
						avg = 255;
					}
					r = r + (255 - r) * avg / 255;
					g = g + (255 - g) * avg / 255;
					b = b + (255 - b) * avg / 255;
					br.SetPixel(i, j,Color.Argb(255, r, g, b));
				}
			}
			return br;
			#endif
			
			#if OS_win
			return null;
			#endif
		}
		
		//绘制图片
		public static void Draw( Bitmap bitmap, int x, int y )
		{
			#if OS_android
			//paint.Alpha = 255;
			
			canvas.DrawBitmap( bitmap, x, y, null );
			#endif
			
			#if OS_win
			g.DrawImage( bitmap, x, y );
			#endif
		}
		
		//绘制图片
		public static void Draw( Bitmap bitmap, int x, int y, int Opacity )
		{
			#if OS_android
			//paint.Alpha = 255;
			
			canvas.DrawBitmap( bitmap, x, y, null );
			#endif
			
			#if OS_win
			g.DrawImage( bitmap, x, y );
			#endif
		}
		
		//绘制图片
		public static void Draw( Bitmap bitmap, int x, int y, int w, int h )
		{
			#if OS_android
			//paint.Alpha = 255;
			
			rectf.Left = x;
			rectf.Top = y;
			rectf.Right = x + w;
			rectf.Bottom = y + h;
			
			canvas.DrawBitmap( bitmap, null, rectf, null );
			#endif
			
			#if OS_win
			g.DrawImage( bitmap, x, y, w, h );
			#endif
		}
		
		//绘制图片
		public static void Draw_slow( Bitmap bitmap, int x, int y, int w, int h, int Opacity )
		{
			#if OS_android
			//paint.Alpha = 255;
			
			rectf.Left = x;
			rectf.Top = y;
			rectf.Right = x + w;
			rectf.Bottom = y + h;
			
			canvas.DrawBitmap( bitmap, null, rectf, null );
			#endif
			
			#if OS_win
			nArray[3][3] = (float)Opacity / 100;
			ColorMatrix matrix = new ColorMatrix( nArray );
			attributes.SetColorMatrices( matrix, matrix, ColorMatrixFlag.Default, ColorAdjustType.Bitmap );
			g.DrawImage( bitmap, new Rectangle( x, y, w, h ), 0, 0, bitmap.Width, bitmap.Height, GraphicsUnit.Pixel, attributes );
			#endif
		}
		
		//绘制图片
		public static void Draw_slow( Image bitmap, int x, int y, int w, int h, int Opacity )
		{
			#if OS_android
			//paint.Alpha = 255;
			
			rectf.Left = x;
			rectf.Top = y;
			rectf.Right = x + w;
			rectf.Bottom = y + h;
			
			canvas.DrawBitmap( bitmap, null, rectf, null );
			#endif
			
			#if OS_win
			nArray[3][3] = (float)Opacity / 100;
			ColorMatrix matrix = new ColorMatrix( nArray );
			attributes.SetColorMatrices( matrix, matrix, ColorMatrixFlag.Default, ColorAdjustType.Bitmap );
			g.DrawImage( bitmap, new Rectangle( x, y, w, h ), 0, 0, bitmap.Width, bitmap.Height, GraphicsUnit.Pixel, attributes );
			#endif
		}
	}
}
}


