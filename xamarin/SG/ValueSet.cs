﻿
using System;

namespace n_ValueSet
{
	static class ValueSet
{
	public static string[] FileList;
	static string[][] ValueList;
	
	//设置参数
	public static void SetValue( string s )
	{
		if( s == null ) {
			return;
		}
		string[] cut = s.Split( '*' );
		int i = 0;
		
		ValueList = null;
		FileList = null;
		
		if( cut[0].StartsWith( "E=" ) ) {
			i = 1;
			string[] c = cut[0].Remove( 0, 2 ).Trim( ',' ).Split( ',' );
			ValueList = new string[c.Length][];
			for( int x = 0; x < c.Length; ++x ) {
				if( c[x] == "" ) {
					continue;
				}
				string[] cc = c[x].Split( ':' );
				ValueList[x] = cc;
			}
		}
		string[] tFileList = new string[ cut.Length - i ];
		int n = 0;
		for( ; i < cut.Length; ++i ) {
			if( cut[i] != "" ) {
				tFileList[n] = cut[i];
				++n;
			}
		}
		FileList = new string[n];
		for( int j = 0; j < FileList.Length; ++j ) {
			FileList[j] = tFileList[j];
		}
	}
	
	//获取一个属性值
	public static string GetValue( string Name )
	{
		if( ValueList == null || ValueList.Length == 0 ) {
			return null;
		}
		for( int i = 0; i < ValueList.Length; ++i ) {
			if( ValueList[i][0] == Name ) {
				return ValueList[i][1];
			}
		}
		return null;
	}
}
}

