﻿
using System;

using Android.App;
using Android.Content;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Android.OS;
using System.Net;
using System.IO;
using System.Threading;

using Android.Graphics;
using System.Collections.Generic;
using Android.Util;
using n_Ble_inside;
using Android.Bluetooth;
using n_SYS;
using Java.Util;

namespace n_Ble20
{
	public static class Ble20
	{
		//数据接收委托
		public delegate void D_ReceiveData( byte b );
		public static D_ReceiveData ReceiveData;
		
		//扫描到设备事件
		public delegate void D_DeviceFound( int i, string Name, string Addr );
		public static D_DeviceFound DeviceFound;
		
		static BluetoothAdapter mBtAdapter;
		static BluetoothReceiver bluetoothReceiver;
		
		static BluetoothSocket mBtSocket;
		static Stream ostream;
		static Stream istream;
		
		static Thread t_MyTimer;
		
		//初始化
		public static void Init()
		{
			mBtAdapter = BluetoothAdapter.DefaultAdapter;
			
			//如果蓝牙没有开启, 则强制开启
			if( !mBtAdapter.IsEnabled ) {
				mBtAdapter.Enable();
			}
			//Register for broadcasts when a device is discovered
			IntentFilter discoveryFilter = new IntentFilter(BluetoothDevice.ActionFound);
			//Register for broadcasts when discovery has finished
			//discoveryFilter.AddAction(BluetoothDevice.ActionFound);
			bluetoothReceiver = new BluetoothReceiver();
			SYS.A.RegisterReceiver(bluetoothReceiver, discoveryFilter);
			
			t_MyTimer = new Thread( t_MyTimerThread );
		}
		
		//重新启动蓝牙
		public static void RestartBle()
		{
			mBtAdapter.StartDiscovery();
		}
		
		//停止扫描
		public static void StopScan()
		{
			mBtAdapter.CancelDiscovery();
		}
		
		//关闭
		public static void Close()
		{
			try {
			if( t_MyTimer.IsAlive ) {
				t_MyTimer.Abort();
				t_MyTimer.Join();
			}
			}
			catch {
			//...
			}
			SYS.A.UnregisterReceiver( bluetoothReceiver );
			try {
				mBtSocket.Close();
			}
			catch {
				//...
			}
			mBtAdapter.CancelDiscovery();
		}
		
		//连接到指定设备
		public static void ConnectDevice( string Addr )
		{
			//取消搜索设备的动作，否则接下来的设备连接会失败
			mBtAdapter.CancelDiscovery();
			
			try {
			BluetoothDevice mBtDevice = mBtAdapter.GetRemoteDevice( Addr );
			mBtSocket = mBtDevice.CreateRfcommSocketToServiceRecord( UUID.FromString("00001101-0000-1000-8000-00805F9B34FB") );
			mBtSocket.Connect();
			
			ostream = mBtSocket.OutputStream;
			istream = mBtSocket.InputStream;
			
			t_MyTimer.Start();
			}catch {
				SYS.AddMessage( "蓝牙打开失败!\n" );
			}
		}
		
		//发送数据
		public static void Write( byte[] buffer, int number )
		{
			if( ostream != null ) {
				ostream.Write( buffer, 0, number );
			}
            else {
            	SYS.AddMessage( "蓝牙设备尚未打开!\n" );
            }
		}
		
		
		
    	//====================================================================
		
		//服务监听器
		class BluetoothReceiver : BroadcastReceiver
		{
			public override void OnReceive(Context context, Intent intent)
			{
				string action = intent.Action;
				
				if( action == BluetoothDevice.ActionFound ) {
					
					BluetoothDevice device = (BluetoothDevice)intent.GetParcelableExtra(BluetoothDevice.ExtraDevice);
					if( DeviceFound != null ) {
						DeviceFound( 0, device.Name, device.Address );
					}
					return;
				}
			}
		}
		
		static byte[] rb = new byte[10];
		
		//定时器线程函数
		static void t_MyTimerThread()
		{
			while( true ) {
				
				byte b = (byte)istream.ReadByte();
				if( ReceiveData != null ) {
					ReceiveData( b );
				}
			}
		}
	}
}

