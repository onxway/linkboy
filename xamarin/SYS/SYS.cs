﻿
using System;

#if OS_win
using n_ImagePanel;
#endif

#if OS_android
using n_MyActivity;
using n_MyView;
#endif

namespace n_SYS
{
//系统函数
public static class SYS
{
	public delegate void D_DealMessage( int d );
	public static D_DealMessage DealMessage;
	
	#if OS_android
	public static MyActivity A;
	public static MyView V;
	static Android.Views.InputMethods.InputMethodManager ime;
	#endif
	
	#if OS_win
	public static ImagePanel V;
	#endif
	
	//初始化
	public static void Init()
	{
		#if OS_android
		ime = (Android.Views.InputMethods.InputMethodManager)A.GetSystemService( Android.App.Activity.InputMethodService );
		#endif
	}
	
	public static void SetTitle( string s )
	{
		#if OS_win
		n_V.V.VMBox.Text = s;
		#endif
	}
	
	//=======================================================
	
	//从当前线程给UI线程发送消息
	public static void SendMessageFromThread( int d )
	{
		#if OS_android
		//A.HD.SendEmptyMessage( d );
		TrigDeal( d );
		#endif
		
		#if OS_win
		V.SendEmptyMessage( d );
		#endif
	}
	
	//接收UI线程的消息, 触发委托
	public static void TrigDeal( int d )
	{
		if( DealMessage != null ) {
			DealMessage( d );
		}
	}
	
	//=======================================================
	
	//触发系统刷新
	public static void Refresh()
	{
		#if OS_win
		V.ReDraw = true;
		#endif
		
		#if OS_android
		V.PostInvalidate();
		#endif
	}
	
	//=======================================================
	
	//设置信息是否显示
	public static void SetMessageVisible( bool b )
	{
		V.ShowMessage = b;
	}
	
	//设置信息显示起始位置
	public static void SetStartY( int y )
	{
		V.MesStartY = y;
	}
	
	public static void SetReceiveData( string e )
	{
		V.ReceiveData = e;
		Refresh();
	}
	
	public static void AddReceiveData( string e )
	{
		V.ReceiveData += e;
		Refresh();
	}
	
	public static void SetMessage( string e )
	{
		V.SysMessage = e;
		Refresh();
	}
	
	public static void AddMessage( string e )
	{
		V.SysMessage += e;
		Refresh();
	}
	
	//=======================================================
	
	//显示输入法
	public static void SwitchIme()
	{
		#if OS_android
		ime.ToggleSoftInput( 0, Android.Views.InputMethods.HideSoftInputFlags.NotAlways );
		#endif
	}
	
	//关闭输入法
	public static void CloseIme()
	{
		//ime.HideSoftInputFromWindow
		
		//Android.Views.InputMethods.InputMethodManager ime = (Android.Views.InputMethods.InputMethodManager)GetSystemService( InputMethodService );
		//ime.ToggleSoftInput( 0, Android.Views.InputMethods.HideSoftInputFlags.NotAlways );
	}
}
}




