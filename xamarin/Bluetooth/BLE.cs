﻿
using System;

using Android.App;
using Android.Content;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Android.OS;
using System.IO;
using System.Threading;

using Android.Graphics;
using System.Collections.Generic;
using Android.Util;
using n_Ble_inside;
using Android.Bluetooth;
using n_SYS;
using Java.Util;

namespace n_Ble
{
	public static class Ble
	{
		public delegate void D_ReceiveData( byte b );
		public static D_ReceiveData ReceiveData;
		
		public delegate void D_BleConnected();
		public static D_BleConnected BleConnected;
		
		public delegate void D_BleScan( int Index, string Name, string Address );
		public static D_BleScan BleScan;
		
		public static bool AutoScan;
		public static bool AutoConnect;
		public static string AutoMacAddr;
		
		public static bool ShowStatusMessage;
		
		public static bool isMt;

		static bool Restart;
		
		static Activity a;
		
		static myLeScanCallback mLeScanCallback;
		
		static BluetoothDevice[] BList;
		static int BListLength;
		
		static BluetoothGattCharacteristic BGC;
		static BluetoothGattCharacteristic BGCR;
		
		static BluetoothGattCharacteristic ATBGC;
		static BluetoothGattCharacteristic ATBGCR;
		
		static BluetoothReceiver bluetoothReceiver;
		
		static bool servicesdiscovered_flag;
		static bool connect_flag;
		
		static Thread t_MyTimer;
		
		static myServiceConnection msc;
		
		static int FirstIndex;
		static int SecondTIndex;
		static int SecondRIndex;
		
		static IntentFilter intentFilter;
		
		//初始化
		public static void Init( object t )
		{
			a = (Activity)t;
			
			mLeScanCallback = new myLeScanCallback();
			
			BList = new BluetoothDevice[100];
			BListLength = 0;
			
			ShowStatusMessage = true;
			
			AutoScan = false;
			
			AutoConnect = true;
			//AutoMacAddr = "D4:F5:13:77:6F:90";
			
			isMt = true;

			BGC = null;
			
			intentFilter = new IntentFilter(BLEService.ACTION_READ_Descriptor_OVER);
			intentFilter.AddAction(BLEService.ACTION_ServicesDiscovered_OVER);
			intentFilter.AddAction(BLEService.ACTION_STATE_CONNECTED);
			
			//？？？暂时没效果
			intentFilter.AddAction(BLEService.ACTION_READ_OVER);
			intentFilter.AddAction(BLEService.ACTION_DATA_CHANGE);
			
			servicesdiscovered_flag = false;
			connect_flag = false;
			Restart = true;
			
			t_MyTimer = new Thread( t_MyTimerThread );
			
			// 注册广播接收器
			bluetoothReceiver = new BluetoothReceiver();
			a.RegisterReceiver(bluetoothReceiver, intentFilter);
			
			msc = new myServiceConnection(a);
			bool b = a.BindService(new Intent(a, typeof(BLEService) ), msc, Bind.AutoCreate );
			if( !b ) {
				if( ShowStatusMessage ) {
					SYS.AddMessage( "服务绑定失败\n" );
				}
			}
			
			
			//var demoServiceIntent = new Intent ("BLEService");
			//bool b = BindService(demoServiceIntent, new myServiceConnection(this), Bind.AutoCreate );
			
			//Toast toast=Toast.MakeText(this.ApplicationContext, "结果: " + b, ToastLength.Short); 
			//toast.Show();
		}
		
		//重新建立蓝牙通信
		//注意: 调用这个函数后, 则需要重新设置各个参数, 如事件委托, MAC地址等
		public static void RestartBle()
		{
			Close();
			Init( a );
		}
		
		//停止扫描蓝牙设备
		public static void StopScan()
		{
			BLEService.mBLEService.stopscanBle( mLeScanCallback );
		}
		
		//连接到指定序号的蓝牙上
		public static string Connect( int i )
		{
			BLEService.mBLEService.stopscanBle( mLeScanCallback );
			
			BluetoothDevice device = BList[i];
			
			if( AutoMacAddr != device.Address ) {
				AutoMacAddr = device.Address;
			}
			
			if( device.Name.StartsWith( "MT-UartBle" ) ) {
				if( ShowStatusMessage ) {
					SYS.AddMessage( "开始连接到: " + device.Name + "\n" );
				}
				BLEService.mBLEService.conectBle(device);
				FirstIndex = 0;
				SecondTIndex = 0;
				SecondRIndex = 0;
			}
			if( device.Name.StartsWith( "MTSerialBle" ) ) {
				if( ShowStatusMessage ) {
					SYS.AddMessage( "开始连接到: " + device.Name + "\n" );
				}
				BLEService.mBLEService.conectBle(device);
				FirstIndex = 3;
				SecondTIndex = 0;
				SecondRIndex = 1;
			}
			if( device.Name.StartsWith( "MTSeriBle" ) ) {
				if( ShowStatusMessage ) {
					SYS.AddMessage( "开始连接到: " + device.Name + "\n" );
				}
				BLEService.mBLEService.conectBle(device);
				FirstIndex = 3;
				SecondTIndex = 0;
				SecondRIndex = 1;
			}
			if( device.Name.StartsWith( "remo" ) ) {
				if( ShowStatusMessage ) {
					SYS.AddMessage( "开始连接到: " + device.Name + "\n" );
				}
				isMt = false;
				BLEService.mBLEService.conectBle(device);
				FirstIndex = 2;
				SecondTIndex = 0;
				SecondRIndex = 0;
			}
			//如果一段时间后依然没有成功连接(connect_flag = false, 那么需要再调用如下方法)
			//BLEService.mBLEService.mBluetoothGatt.Connect();
			
			return AutoMacAddr;
		}
		
		//唤醒蓝牙接收器
		public static void Resume()
		{
			////注意这里, 刚启动APP时也会执行Resume,所以需要借助于null判断
			//if( bluetoothReceiver == null ) {
			//	return;
			//}
			
			IntentFilter intentFilter = new IntentFilter(BLEService.ACTION_READ_Descriptor_OVER);
			intentFilter.AddAction(BLEService.ACTION_ServicesDiscovered_OVER);
			intentFilter.AddAction(BLEService.ACTION_STATE_CONNECTED);
			
			//？？？暂时没效果
			intentFilter.AddAction(BLEService.ACTION_READ_OVER);
			intentFilter.AddAction(BLEService.ACTION_DATA_CHANGE);
			
			// 注册广播接收器
			a.RegisterReceiver(bluetoothReceiver, intentFilter);
			
			RestartBle();
		}
		
		//停止运行
		public static void Close()
		{
			if( t_MyTimer.IsAlive ) {
				t_MyTimer.Abort();
				t_MyTimer.Join();
			}
			
			//停止扫描
			BLEService.mBLEService.stopscanBle(mLeScanCallback);
			
			if( BLEService.mBLEService.mBluetoothGatt != null ) {
				BLEService.mBLEService.mBluetoothGatt.Close();
			}
			BLEService.mBLEService.disConectBle();
			
			try {
				a.UnregisterReceiver(bluetoothReceiver);
			}
			catch {
				if( ShowStatusMessage ) {
					SYS.AddMessage( "取消注册蓝牙接收服务失败!\n" );
				}
			}
			try {
				a.UnbindService( msc );
			}
			catch {
				if( ShowStatusMessage ) {
					SYS.AddMessage( "取消绑定服务失败!\n" );
				}
			}
		} 
		
		public static void SendAT( string Cmd )
		{
			byte[] b = new byte[Cmd.Length];
			for( int i = 0; i < b.Length; ++i ) {
				b[i] = (byte)Cmd[i];
			}
			if( BGC == null ) {
				if( ShowStatusMessage ) {
					SYS.AddMessage( "蓝牙尚未打开\n" );
				}
				return;
			}
			
			bool b1 = ATBGC.SetValue(b);
			bool b2 = BLEService.mBLEService.mBluetoothGatt.WriteCharacteristic(ATBGC);
			
			if( !b1 ) {
				if( ShowStatusMessage ) {
					SYS.AddMessage( "2 ERROR (SetValue)\n" );
				}
			}
			if( !b2 ) {
				if( ShowStatusMessage ) {
					SYS.AddMessage( "2 ERROR (WriteCharacteristic)\n" );
				}
			}
		}
		
		static int ttt = 0;
		
		//发送数据
		public static void SendByte( byte data )
		{
			if( BGC == null ) {
				SYS.AddMessage( "<SendData> 蓝牙尚未打开\n" );
				return;
			}
			//MyView.AddMessage( "发送数据... " );
			byte[] sendmsg = new byte[1];
			sendmsg[0] = data;
			
			bool b1 = BGC.SetValue(sendmsg);
			bool b2 = BLEService.mBLEService.mBluetoothGatt.WriteCharacteristic(BGC);
			
			if( !b1 ) {
				if( ShowStatusMessage ) {
					SYS.AddMessage( "2 ERROR (SetValue)\n" );
				}
			}
			if( !b2 ) {
				if( ShowStatusMessage ) {
					SYS.AddMessage( "2 ERROR (WriteCharacteristic)\n" );
				}
			}
			ttt++;
			if( ttt % 20 == 0 ) {
				SYS.AddMessage( "\n" );
			}
			SYS.AddMessage( (char)data + " " );

			//BLEService.mBLEService.mBluetoothGatt.ReadCharacteristic(BGC);
		}
		
		public static void SendBuffer( byte[] Buffer, int Number )
		{
			SendBuffer( Buffer, 0, Number );
		}
		
		public static void SendBuffer( byte[] Buffer, int Start, int Number )
		{
			if( BGC == null ) {
				SYS.AddMessage( "<SendBuffer> 蓝牙尚未打开\n" );
				return;
			}
			//MyView.AddMessage( "发送数据... " );
			byte[] sendmsg = new byte[Number];
			for( int i = 0; i < Number; ++i ) {
				sendmsg[i] = Buffer[Start + i];
			}
			bool b1 = BGC.SetValue(sendmsg);
			bool b2 = BLEService.mBLEService.mBluetoothGatt.WriteCharacteristic(BGC);
			
			if( !b1 ) {
				if( ShowStatusMessage ) {
					SYS.AddMessage( "2 ERROR (SetValue)\n" );
				}
			}
			if( !b2 ) {
				if( ShowStatusMessage ) {
					SYS.AddMessage( "2 ERROR (WriteCharacteristic)\n" );
				}
			}
			ttt++;
			if( ttt % 20 == 0 ) {
				SYS.AddMessage( "\n" );
			}
			
			//BLEService.mBLEService.mBluetoothGatt.ReadCharacteristic(BGC);
		}

		//发送数据
		public static void SendData( int Number, byte[] data )
		{
			if( BGC == null ) {
				SYS.AddMessage( "<SendData> 蓝牙尚未打开\n" );
				return;
			}
			//MyView.AddMessage( "发送数据... " );
			byte[] sendmsg = new byte[Number];
			
			for( int i = 0; i < Number; i++ ) {
				sendmsg[i] = data[i];
			}
			bool b1 = BGC.SetValue(sendmsg);
			bool b2 = BLEService.mBLEService.mBluetoothGatt.WriteCharacteristic(BGC);
			
			if( !b1 ) {
				if( ShowStatusMessage ) {
					SYS.AddMessage( "2 ERROR (SetValue)\n" );
				}
			}
			if( !b2 ) {
				if( ShowStatusMessage ) {
					SYS.AddMessage( "2 ERROR (WriteCharacteristic)\n" );
				}
			}
			
			//BLEService.mBLEService.mBluetoothGatt.ReadCharacteristic(BGC);
		}
		
		//发送数据
		public static void SendDataWithPack( int Number, byte[] data )
		{
			if( BGC == null ) {
				if( ShowStatusMessage ) {
					SYS.AddMessage( "<SendDataWithPack> 蓝牙尚未打开\n" );
				}
				return;
			}
			//MyView.AddMessage( "发送数据... " );
			byte[] sendmsg = new byte[3 + Number];
			sendmsg[0] = 0xAA;
			sendmsg[1] = (byte)Number;
			byte Sum = 0;
			for( int i = 0; i < Number; i++ ) {
				sendmsg[2 + i] = data[i];
				Sum ^= data[i];
			}
			sendmsg[2 + Number] = Sum;
			
			bool b1 = BGC.SetValue(sendmsg);
			bool b2 = BLEService.mBLEService.mBluetoothGatt.WriteCharacteristic(BGC);
			
			if( !b1 ) {
				if( ShowStatusMessage ) {
					SYS.AddMessage( "2 ERROR (SetValue)\n" );
				}
			}
			if( !b2 ) {
				if( ShowStatusMessage ) {
					SYS.AddMessage( "2 ERROR (WriteCharacteristic)\n" );
				}
			}
			
			//BLEService.mBLEService.mBluetoothGatt.ReadCharacteristic(BGC);
		}
		
		//取消休眠
		static void AlwaysWork()
		{
			SendAT( "AT+LDLY[0]" );
		}
		
		//定时器线程函数
		static void t_MyTimerThread()
		{
			while( true ) {
				
				while( !Restart ) {
					System.Threading.Thread.Sleep( 100 );
				}
				Restart = false;
				
				//开始扫描
				if( ShowStatusMessage ) {
					SYS.AddMessage( "启动扫描\n" );
				}
				BLEService.mBLEService.scanBle(mLeScanCallback);
				
				//等待连接成功
				while( !connect_flag ) { System.Threading.Thread.Sleep( 100 ); }
				
				//停止扫描
				//根据客服建议, 应尽量减少扫描时间
				BLEService.mBLEService.stopscanBle(mLeScanCallback); // 停止扫描
				
				//搜索服务
				if( ShowStatusMessage ) {
					SYS.AddMessage( "搜索服务...\n" );
				}
				BLEService.mBLEService.mBluetoothGatt.DiscoverServices();
				
				//等到服务发现
				while( !servicesdiscovered_flag ) { System.Threading.Thread.Sleep( 100 ); }
				
				//获取数据通道
				if( ShowStatusMessage ) {
					SYS.AddMessage( "获取数据通道\n" );
				}
				BGC = BLEService.mBLEService.mBluetoothGatt.Services[FirstIndex].Characteristics[SecondTIndex];
				BGCR = BLEService.mBLEService.mBluetoothGatt.Services[FirstIndex].Characteristics[SecondRIndex];
				//下边这段代码必须加, 否则无法接收数据(只能发送)
				BLEService.mBLEService.mBluetoothGatt.SetCharacteristicNotification( BGCR, true );
				
				//如果是Mt模块则需要做下边的工作
				if( isMt ) {
					string myUUID = null;
					if( isMt ) {
						myUUID = "00002902-0000-1000-8000-00805f9b34fb";
					}
					else {
						myUUID = "19B10001-E8F2-537E-4F6C-D104768A1214";
					}
					//打开模块的数据上传允许 2015.10.21
					BluetoothGattDescriptor descriptor = BGCR.GetDescriptor(UUID.FromString( myUUID ));
					byte[] d = new byte[BluetoothGattDescriptor.EnableNotificationValue.Count];
					BluetoothGattDescriptor.EnableNotificationValue.CopyTo( d, 0 );
					descriptor.SetValue( d );
					BLEService.mBLEService.mBluetoothGatt.WriteDescriptor( descriptor );
					
					ATBGC = BLEService.mBLEService.mBluetoothGatt.Services[4].Characteristics[0];
					ATBGCR = BLEService.mBLEService.mBluetoothGatt.Services[4].Characteristics[1];
					//下边这段代码必须加, 否则无法接收数据(只能发送)
					BLEService.mBLEService.mBluetoothGatt.SetCharacteristicNotification( ATBGCR, true );
					
					//打开模块的数据上传允许 2015.10.21
					BluetoothGattDescriptor descriptor1 = ATBGCR.GetDescriptor(UUID.FromString( myUUID ));
					byte[] d1 = new byte[BluetoothGattDescriptor.EnableNotificationValue.Count];
					BluetoothGattDescriptor.EnableNotificationValue.CopyTo( d1, 0 );
					descriptor1.SetValue( d1 );
					BLEService.mBLEService.mBluetoothGatt.WriteDescriptor( descriptor1 );
					
					System.Threading.Thread.Sleep( 500 );
					AlwaysWork();
					
					//修改蓝牙模块的波特率(MT系列)
					//System.Threading.Thread.Sleep( 500 );
					//SendAT( "AT+BAUD[G]" );
				}
				
				//System.Threading.Thread.Sleep( 500 );
				SYS.SetMessage( "" );

				//触发通信连接事件
				if( BleConnected != null ) {
					BleConnected();
				}
			}
		}
		
		//====================================================================
		
		//服务监听器
		class BluetoothReceiver : BroadcastReceiver
		{
			public override void OnReceive(Context context, Intent intent)
			{
				string action = intent.Action;
				
				if( action == BLEService.ACTION_READ_OVER ) {
					//if( intent.GetIntExtra( "value", -1 ) == (int)GattStatus.Success ) {
					if( ReceiveData != null ) {
						byte[] b = intent.GetByteArrayExtra( "value" );
						//ReceiveData( b );
					}
					return;
				}
				
				if( action == BLEService.ACTION_DATA_CHANGE ) {
					//if( intent.GetIntExtra( "value", -1 ) == (int)GattStatus.Success ) {
					if( ReceiveData != null ) {
						byte[] b = intent.GetByteArrayExtra( "value" );
						for( int i = 0; i < b.Length; ++i ) {
							ReceiveData( b[i] );
						}
					
					}
					return;
				}
				
				if( action == BLEService.ACTION_ServicesDiscovered_OVER ) {
					servicesdiscovered_flag = true;
					return;
				}
				if( action == BLEService.ACTION_STATE_CONNECTED ) {
					connect_flag = true;
					return;
				}
			}
		}
		
		//====================================================================
		
		//服务连接器
		class myServiceConnection : Java.Lang.Object, IServiceConnection
		{
			Activity activity;
			
			public myServiceConnection (Activity activity)
			{
				this.activity = activity;
			}
			
			public void OnServiceDisconnected(ComponentName name)
			{
				if( ShowStatusMessage ) {
					SYS.AddMessage( "服务已断开\n" );
				}
			}
			
			public void OnServiceConnected(ComponentName name, IBinder service)
			{
				if( ShowStatusMessage ) {
					SYS.AddMessage( "服务已连接\n" );
				}
				if( BLEService.mBLEService.initBle() ){
					if( ShowStatusMessage ) {
						SYS.AddMessage( "开启扫描定时器\n" );
					}
					//MyActivity.search_timer.SendEmptyMessageDelayed(0, 100);
					
					if( AutoScan ) {
						t_MyTimer.Start();
					}
				}
			}
		}
		//====================================================================
		
		//蓝牙设备回调函数
		class myLeScanCallback : Java.Lang.Object, BluetoothAdapter.ILeScanCallback
		{
			public void OnLeScan( BluetoothDevice device, int rssi, byte[] scanRecord )
			{
				if( device == null ) {
					if( ShowStatusMessage ) {
						SYS.AddMessage( "device null!\n" );
					}
					return;
				}
				for( int i = 0; i < BListLength; i++ ) {
					if( device.Address == BList[i].Address ) {
						return;
					}
				}
				if( device.Name == null ) {
					return;
				}
				if( ShowStatusMessage ) {
					SYS.AddMessage( "扫描到: " + device.Name + " (" + device.Address + ")\n" );
				}
				BList[BListLength] = device;
				BListLength++;
				
				if( AutoConnect ) {
					if( device.Address == AutoMacAddr ) {
						Connect( BListLength - 1 );
					}
					return;
				}
				//触发事件: 搜索到设备
				if( ShowStatusMessage ) {
					SYS.AddMessage( "触发事件: 搜索到设备\n" );
				}
				if( BleScan != null ) {
					BleScan( BListLength - 1, device.Name, device.Address );
				}
			}
		}
	}
}

