﻿
namespace n_Ble_inside
{
using Android.App;
using Android.Bluetooth;
using Android.Content;
using Android.OS;
using n_SYS;

[Service]
public class BLEService : Service
{
	public static BLEService mBLEService;
	
	public const string ACTION_DATA_CHANGE = "com.example.bluetooth.le.ACTION_DATA_CHANGE";
	public const string ACTION_RSSI_READ = "com.example.bluetooth.le.ACTION_RSSI_READ";
	public const string ACTION_STATE_CONNECTED = "com.example.bluetooth.le.ACTION_STATE_CONNECTED";
	public const string ACTION_STATE_DISCONNECTED = "com.example.bluetooth.le.ACTION_STATE_DISCONNECTED";
	public const string ACTION_WRITE_OVER = "com.example.bluetooth.le.ACTION_WRITE_OVER";
	public const string ACTION_READ_OVER = "com.example.bluetooth.le.ACTION_READ_OVER";
	public const string ACTION_READ_Descriptor_OVER = "com.example.bluetooth.le.ACTION_READ_Descriptor_OVER";
	public const string ACTION_WRITE_Descriptor_OVER = "com.example.bluetooth.le.ACTION_WRITE_Descriptor_OVER";
	public const string ACTION_ServicesDiscovered_OVER = "com.example.bluetooth.le.ACTION_ServicesDiscovered_OVER";
	
	public BluetoothManager mBluetoothManager;
	public BluetoothAdapter mBluetoothAdapter;
	public BluetoothGatt mBluetoothGatt;
	
	
	class myBluetoothGattCallback : BluetoothGattCallback
	{
		public override void OnConnectionStateChange (BluetoothGatt gatt, GattStatus status, ProfileState newState)
		{
			base.OnConnectionStateChange (gatt, status, newState);
			if (newState == ProfileState.Connected) { // 链接成功
//				mBluetoothGatt.discoverServices();
				mBLEService.broadcastUpdate(BLEService.ACTION_STATE_CONNECTED);
			}
			else if (newState == ProfileState.Disconnected) { // 断开链接
				mBLEService.broadcastUpdate(BLEService.ACTION_STATE_DISCONNECTED);
			}
		}
		
		public override void OnServicesDiscovered (BluetoothGatt gatt, GattStatus status)
		{
			base.OnServicesDiscovered (gatt, status);
			mBLEService.broadcastUpdate(BLEService.ACTION_ServicesDiscovered_OVER, (int)status);
		}
		
		public override void OnDescriptorRead (BluetoothGatt gatt, BluetoothGattDescriptor descriptor, GattStatus status)
		{
			base.OnDescriptorRead (gatt, descriptor, status);
			mBLEService.broadcastUpdate(BLEService.ACTION_READ_Descriptor_OVER, (int)status);
		}
		
		public override void OnCharacteristicRead (BluetoothGatt gatt, BluetoothGattCharacteristic characteristic, GattStatus status)
		{
			base.OnCharacteristicRead (gatt, characteristic, status);
			if (status == GattStatus.Success) {
				mBLEService.broadcastUpdate(BLEService.ACTION_READ_OVER, characteristic.GetValue());
			}
		}
		
		public override void OnCharacteristicChanged (BluetoothGatt gatt, BluetoothGattCharacteristic characteristic)
		{
			base.OnCharacteristicChanged (gatt, characteristic);
			mBLEService.broadcastUpdate(BLEService.ACTION_DATA_CHANGE, characteristic.GetValue());
		}
		
		public override void OnCharacteristicWrite (BluetoothGatt gatt, BluetoothGattCharacteristic characteristic, GattStatus status)
		{
			base.OnCharacteristicWrite (gatt, characteristic, status);
			mBLEService.broadcastUpdate(BLEService.ACTION_WRITE_OVER, (int)status);
		}
	}
	
	myBluetoothGattCallback mGattCallback = new myBluetoothGattCallback();
	
	IBinder mBinder = new Binder();
	public override IBinder OnBind(Intent intent)
	{
		mBLEService = this;
		return mBinder;
	}
	
	//初始化BLE
	public bool initBle()
	{
		mBluetoothManager = (BluetoothManager)this.GetSystemService(Context.BluetoothService);
		
		if( mBluetoothManager == null) {
			return false;
		}
		mBluetoothAdapter = mBluetoothManager.Adapter;
		if( mBluetoothAdapter == null ) {
			return false;
		}
		if( !mBluetoothAdapter.IsEnabled ) {
			mBluetoothAdapter.Enable();
		}
		return true;
	}
	
	// 扫描
	public void scanBle(BluetoothAdapter.ILeScanCallback callback)
	{
		mBluetoothAdapter.StartLeScan(callback);
	}
	
	// 停止扫描
	public void stopscanBle(BluetoothAdapter.ILeScanCallback callback)
	{
		mBluetoothAdapter.StopLeScan(callback);
	}
	
	// 发起连接
	public void conectBle(BluetoothDevice mBluetoothDevice)
	{
		disConectBle();
		mBluetoothGatt = mBluetoothDevice.ConnectGatt(ApplicationContext, true, mGattCallback);
	}
	
	// 关闭连接
	public void disConectBle()
	{
		if(mBluetoothGatt != null) {
			mBluetoothGatt.Disconnect();
		}
	}
	
	// 发送广播消息
	private void broadcastUpdate(string action)
	{
		Intent intent = new Intent(action);
		SendBroadcast(intent);
	}
	
	// 发送广播消息
	private void broadcastUpdate(string action, int value)
	{
		Intent intent = new Intent(action);
		intent.PutExtra("value", value);
		SendBroadcast(intent);
	}
	
	// 发送广播消息
	private void broadcastUpdate(string action, byte[] Value )
	{
		Intent intent = new Intent(action);
		intent.PutExtra("value", Value);
		SendBroadcast(intent);
	}
}
}

