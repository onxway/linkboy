﻿
using Android.Graphics;

namespace n_SG
{
public static class SG
{
	static Android.Graphics.Canvas canvas;
	static Paint paint;
	
	static RectF rectf;
	
	//初始化
	public static void Init()
	{
		rectf = new RectF( 0, 0, 0, 0 );
	}
	
	//设置图形对象
	public static void SetObject( Android.Graphics.Canvas c )
	{
		canvas = c;
		paint = new Paint() { Color = Color.Black, AntiAlias = true, StrokeWidth = 1 };
	}
	
	//=========================================================================
	
	//绘制字符串, 靠左显示
	public static void DrawStringAtLeft( string mes, Color c, float size, int x, int y )
	{
		paint.Color = c;
		
		paint.SetStyle( Paint.Style.Fill );
		
		paint.TextSize = size;
		paint.TextAlign = Paint.Align.Left;
		paint.Alpha = 255;
		
		Paint.FontMetricsInt fontMetrics = paint.GetFontMetricsInt();
		int baseline = y - (fontMetrics.Bottom - fontMetrics.Top) / 2 - fontMetrics.Top;
		
		canvas.DrawText( mes, x, baseline, paint );
	}
	
	//绘制字符串, 居中显示
	public static void DrawStringAtCenter( string mes, Color c, float size, int x, int y )
	{
		paint.Color = c;
		
		paint.SetStyle( Paint.Style.Fill );
		
		paint.TextSize = size;
		paint.TextAlign = Paint.Align.Center;
		paint.Alpha = 255;
		
		Paint.FontMetricsInt fontMetrics = paint.GetFontMetricsInt();
		int baseline = y - (fontMetrics.Bottom - fontMetrics.Top) / 2 - fontMetrics.Top;
		
		canvas.DrawText( mes, x, baseline, paint );
	}
	
	//绘制字符串, 靠右显示
	public static void DrawStringAtRight( string mes, Color c, float size, int x, int y )
	{
		paint.Color = c;
		
		paint.SetStyle( Paint.Style.Fill );
		
		paint.TextSize = size;
		paint.TextAlign = Paint.Align.Right;
		paint.Alpha = 255;
		
		Paint.FontMetricsInt fontMetrics = paint.GetFontMetricsInt();
		int baseline = y - (fontMetrics.Bottom - fontMetrics.Top) / 2 - fontMetrics.Top;
		
		canvas.DrawText( mes, x, baseline, paint );
	}
	
	//=========================================================================
	
	//绘制直线
	public static void DrawLine( Color c, int w, int x1, int y1, int x2, int y2 )
	{
		paint.Color = c;
		paint.SetStyle( Paint.Style.Stroke );
		paint.StrokeWidth = w;
		paint.Alpha = 255;
		
		canvas.DrawLine( x1, y1, x2, y2, paint );
	}
	
	//=========================================================================
	
	//绘制矩形
	public static void DrawRectangle( Color c, int w, int x, int y, int width, int height )
	{
		paint.Color = c;
		paint.SetStyle( Paint.Style.Stroke );
		paint.StrokeWidth = w;
		paint.Alpha = 255;
		
		canvas.DrawRect( x, y, x + width, y + height, paint );
	}
	
	//填充矩形
	public static void FillRectangle( Color c, int x, int y, int width, int height )
	{
		paint.Color = c;
		paint.SetStyle( Paint.Style.Fill );
		paint.Alpha = 255;
		
		canvas.DrawRect( x, y, x + width, y + height, paint );
	}
	
	//填充矩形
	public static void FillRectangle( Color c, int alpha, int x, int y, int width, int height )
	{
		paint.Color = c;
		paint.SetStyle( Paint.Style.Fill );
		paint.Alpha = alpha;
		
		canvas.DrawRect( x, y, x + width, y + height, paint );
	}
	
	//绘制圆角矩形
	public static void DrawRoundRectangle( Color c, int w, int rx, int ry, int x, int y, int width, int height )
	{
		paint.Color = c;
		paint.SetStyle( Paint.Style.Stroke );
		paint.StrokeWidth = w;
		paint.Alpha = 255;
		
		rectf.Left = x;
		rectf.Top = y;
		rectf.Right = x + width;
		rectf.Bottom = y + height;
		
		canvas.DrawRoundRect( rectf, rx, ry, paint );
	}
	
	//填充圆角矩形
	public static void FillRoundRectangle( Color c, int rx, int ry, int x, int y, int width, int height )
	{
		paint.Color = c;
		paint.SetStyle( Paint.Style.Fill );
		paint.Alpha = 255;
		
		rectf.Left = x;
		rectf.Top = y;
		rectf.Right = x + width;
		rectf.Bottom = y + height;
		
		canvas.DrawRoundRect( rectf, rx, ry, paint );
	}
	
	//填充圆角矩形, 带有透明度
	public static void FillRoundRectangle( Color c, int alpha, int rx, int ry, int x, int y, int width, int height )
	{
		paint.Color = c;
		paint.SetStyle( Paint.Style.Fill );
		paint.Alpha = alpha;
		
		rectf.Left = x;
		rectf.Top = y;
		rectf.Right = x + width;
		rectf.Bottom = y + height;
		
		canvas.DrawRoundRect( rectf, rx, ry, paint );
	}
	
	//=========================================================================
	
	//绘制圆形
	public static void DrawCircle( Color c, int w, int x, int y, int radius )
	{
		paint.Color = c;
		paint.SetStyle( Paint.Style.Stroke );
		paint.StrokeWidth = w;
		paint.Alpha = 255;
		
		canvas.DrawCircle( x, y, radius, paint );
	}
	
	//填充圆形
	public static void FillCircle( Color c, int x, int y, int radius )
	{
		paint.Color = c;
		paint.SetStyle( Paint.Style.Fill );
		paint.Alpha = 255;
		
		canvas.DrawCircle( x, y, radius, paint );
	}
	
	//填充圆形, 带有透明度
	public static void FillCircle( Color c, int alpha, int x, int y, int radius )
	{
		paint.Color = c;
		paint.SetStyle( Paint.Style.Fill );
		paint.Alpha = alpha;
		
		canvas.DrawCircle( x, y, radius, paint );
	}
	
	//=========================================================================
	
	//绘制弧形
	//c:			颜色
	//w:			线条宽度
	//x:			圆弧中心X坐标
	//y:			圆弧中心Y坐标
	//radius:		圆弧半径
	//startAngle:	起始角度
	//sweepAngle:	圆弧本身角度
	public static void DrawArc( Color c, int w, int x, int y, int radius, int startAngle, int sweepAngle )
	{
		paint.Color = c;
		paint.SetStyle( Paint.Style.Stroke );
		paint.StrokeWidth = w;
		paint.Alpha = 255;
		
		rectf.Left = x - radius;
		rectf.Top = y - radius;
		rectf.Right = x + radius;
		rectf.Bottom = y + radius;
		
		canvas.DrawArc( rectf, startAngle, sweepAngle, false, paint );
	}
	
	//=========================================================================
	
	public static Bitmap GetGrayBitmap( Bitmap bitmap, int A, int B )
    {
        int width = bitmap.Width;
        int height = bitmap.Height;
        Bitmap br = Bitmap.CreateBitmap( width, height, bitmap.GetConfig() );
        
        Canvas canvas = new Canvas( br );//以base为模板创建canvas对象
        canvas.DrawBitmap(bitmap, new Matrix(),new Paint());
        
        for(int i = 0; i < width; i++)//遍历像素点
        {
            for(int j = 0; j < height; j++)
            {
                int color = bitmap.GetPixel(i, j);
                
                int r = Color.GetRedComponent( color );
                int g = Color.GetGreenComponent( color );
                int b = Color.GetBlueComponent( color );
                int a = Color.GetAlphaComponent( color );
                
                int avg = (r+g+b)/3;//RGB均值
                
                if( avg > 230 ) {
                	br.SetPixel(i, j,Color.Argb(a, r, g, b));
                }
                else {
                	avg = A + (B-A) * avg / 255;
                	br.SetPixel(i, j,Color.Argb(a, avg, avg, avg));
                }
                
                
            }
        }
        return br;
    }
	
	public static Bitmap GetBlueBitmap( Bitmap bitmap )
    {
        int width = bitmap.Width;
        int height = bitmap.Height;
        Bitmap br = Bitmap.CreateBitmap( width, height, bitmap.GetConfig() );
         
        Canvas canvas = new Canvas( br );//以base为模板创建canvas对象
        canvas.DrawBitmap(bitmap, new Matrix(),new Paint());
        
        for(int i = 0; i < width; i++)//遍历像素点
        {
            for(int j = 0; j < height; j++)
            {
                int color = bitmap.GetPixel(i, j);
                
                int r = Color.GetRedComponent( color );
                int g = Color.GetGreenComponent( color );
                int b = Color.GetBlueComponent( color );
                int a = Color.GetAlphaComponent( color );
                
                int avg = (r+g+b)/3;//RGB均值
                int red_green = 255 - ( 255 - avg ) / 2;
                if( red_green < 0 ) {
                	red_green = 0;
                }
                
                
                if( avg > 230 ) {
                	br.SetPixel(i, j,Color.Argb(a, r, g, b));
                }
                else {
                	br.SetPixel(i, j,Color.Argb(a, red_green, red_green, 255) );
                }
            }
        }
        return br;
    }
	
	//绘制图片
	public static void DrawImage( Bitmap bitmap, int x, int y )
	{
		//paint.Alpha = 255;
		
		canvas.DrawBitmap( bitmap, x, y, null );
	}
	
	//绘制图片
	public static void DrawImage( Bitmap bitmap, int x, int y, int w, int h )
	{
		//paint.Alpha = 255;
		
		rectf.Left = x;
		rectf.Top = y;
		rectf.Right = x + w;
		rectf.Bottom = y + h;
		
		canvas.DrawBitmap( bitmap, x, y, null );
		canvas.DrawBitmap( bitmap, null, rectf, null );
	}
}
}


