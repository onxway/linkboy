﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Widget;
using Android.Runtime;
using Android.Util;
using Android.Views;

using System.Net;
using System.Net.Sockets;
using System.Threading;

using Android.Graphics;
using n_SG;

namespace n_SG
{
public abstract class MyPanel
{
	protected const int MaxNumber = 10;
	protected MyControl[][] ControlList;
	protected int[] ControlListLength;
	
	protected bool ObjectTouched;
	
	//屏幕尺寸, TCL手机为 320*480
	protected Rect r;
	
	protected Color BackColor;
	
	protected int LastX;
	protected int LastY;
	
	protected int StartX;
	protected int StartY;
	
	public MyPanel( Rect tr )
	{
		r = tr;
		
		ControlListLength = new int[ MaxNumber ];
		ControlList = new MyControl[ MaxNumber ][];
		for( int i = 0; i < MaxNumber; i++ ) {
			ControlList[i] = new MyControl[ 100 ];
			ControlListLength[i] = 0;
		}
		
		ObjectTouched = false;
		
		StartX = 0;
		StartY = 0;
		
		LastX = 0;
		LastY = 0;
	}
	
	protected void Add( int Level, MyControl c )
	{
		ControlList[Level][ ControlListLength[Level] ] = c;
		ControlListLength[Level]++;
	}
	
	//----------------------------------------------------
	
	public virtual void TouchDown( int x, int y )
	{
		ObjectTouched = false;
		
		for( int i = MaxNumber - 1; i >= 0; i-- ) {
			if( TouchDownLevel( i, x, y ) ) {
				break;
			}
		}
		if( !ObjectTouched ) {
			LastX = x;
			LastY = y;
		}
	}
	
	public void TouchUp( int x, int y )
	{
		for( int i = MaxNumber - 1; i >= 0; i-- ) {
			for( int j = 0; j < ControlListLength[i]; j++ ) {
				ControlList[i][j].TouchUp( StartX, StartY, x, y );
			}
		}
	}
	
	public void TouchMove( int x, int y )
	{
		if( !ObjectTouched ) {
			//StartX += x - LastX;
			StartY += y - LastY;
			//LastX = x;
			LastY = y;
		}
		for( int i = MaxNumber - 1; i >= 0; i-- ) {
			for( int j = 0; j < ControlListLength[i]; j++ ) {
				ControlList[i][j].TouchMove( StartX, StartY, x, y );
			}
		}
	}
	
	public abstract void Draw();
	
	public virtual bool TimeTick()
	{
		return false;
	}
	
	//----------------------------------------------------
	
	protected bool TouchDownLevel( int Level, int x, int y )
	{
		for( int i = 0; i < ControlListLength[Level]; i++ ) {
			ObjectTouched |= ControlList[Level][i].TouchDown( StartX, StartY, x, y );
			if( ObjectTouched ) {
				break;
			}
		}
		return ObjectTouched;
	}
	
	protected void FillBack( Color c )
	{
		SG.MRectangle.Fill( c, r.Left, r.Top, r.Right - r.Left, r.Bottom - r.Top );
	}
	
	protected void FillBack( Color c, int alpha )
	{
		SG.MRectangle.Fill( c, alpha, r.Left, r.Top, r.Right - r.Left, r.Bottom - r.Top );
	}
	
	protected void DrawAll()
	{
		for( int i = 0; i < MaxNumber; i++ ) {
			DrawLevel( i );
		}
	}
	
	protected void DrawLevel( int Level )
	{
		for( int i = 0; i < ControlListLength[Level]; i++ ) {
			ControlList[Level][i].Draw( StartX, StartY );
		}
	}
}
}

