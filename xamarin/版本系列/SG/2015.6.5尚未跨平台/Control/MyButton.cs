﻿
using System;
using Android.Graphics;
using n_SG;

namespace n_SG
{
public class MyButton : MyControl
{
	public delegate void d_TouchDown( object sender );
	public d_TouchDown TouchDownEvent;
	
	public delegate void d_TouchUp( object sender );
	public d_TouchUp TouchUpEvent;
	
	int SX;
	int SY;
	int Width;
	int Height;
	
	bool isTouchOn;
	
	Bitmap BackBitmap;
	Bitmap BackBitmap1;
	string Text;
	int FontSize;
	
	enum E_ButtonType
	{
		OnlyBitmap,
		OnlyString,
		Bitmap2
	}
	E_ButtonType ButtonType;
	public bool isTemp = false;
	
	//构造函数 - 文本信息为空, 显示图片
	public MyButton( Bitmap bm, int x, int y, int w, int h ) : base()
	{
		SX = x;
		SY = y;
		Width = w;
		Height = h;
		
		BackBitmap1 = bm;
		//BackBitmap = SG.GetGrayBitmap( BackBitmap1 );
		
		Text = null;
		isTouchOn = false;
		
		ButtonType = E_ButtonType.Bitmap2;
	}
	
	//构造函数 - 图片为空, 显示文本信息
	public MyButton( Color bc, string t, int fs, Color fc, int x, int y, int w, int h ) : base()
	{
		SX = x;
		SY = y;
		Width = w;
		Height = h;
		BackBitmap = null;
		
		BackColor = bc;
		
		Text = t;
		ForeColor = fc;
		FontSize = fs;
		
		ButtonType = E_ButtonType.OnlyString;
	}
	
	//=========================================================================
	
	//绘制控件
	public override void Draw( int StartX, int StartY )
	{
		switch( ButtonType ) {
			case E_ButtonType.OnlyBitmap: Draw_OnlyBitmap( StartX, StartY ); break;
			case E_ButtonType.OnlyString: Draw_OnlyString( StartX, StartY ); break;
			case E_ButtonType.Bitmap2: Draw_Bitmap2( StartX, StartY ); break;
			default: Draw_Default( StartX, StartY ); break;
		}
	}
	
	//绘制控件
	void Draw_OnlyBitmap( int StartX, int StartY )
	{
		if( isTouchOn ) {
			SG.MRectangle.Draw( BackColor, 1, SX, SY, Width, Height );
			SG.MBitmap.Draw( BackBitmap, SX + Width/2 - BackBitmap.Width/2, SY + Height/2 - BackBitmap.Height/2 );
		}
		else {
			SG.MRectangle.Fill( BackColor, SX, SY, Width, Height );
			SG.MBitmap.Draw( BackBitmap, SX + Width/2 - BackBitmap.Width/2, SY + Height/2 - BackBitmap.Height/2 );
		}
	}
	
	//绘制控件
	void Draw_OnlyString( int StartX, int StartY )
	{
		if( isTemp ) {
			if( isTouchOn ) {
				SG.MRectangle.Draw( ForeColor, 1, SX, SY, Width, Height );
			}
			else {
				//SG.FillRectangle( BackColor, SX, SY, Width, Height );
			}
			switch( TextAlign ) {
				case E_TextAlign.Left:		SG.MString.DrawAtLeft( Text, ForeColor, FontSize, SX, SY + Height/2 ); break;
				case E_TextAlign.Center:	SG.MString.DrawAtCenter( Text, ForeColor, FontSize, SX + Width/2, SY + Height/2 ); break;
				case E_TextAlign.Right:		SG.MString.DrawAtRight( Text, ForeColor, FontSize, SX + Width, SY + Height/2 ); break;
				default:					break;
			}
		}
		else {
			if( isTouchOn ) {
				SG.MRectangle.Draw( BackColor1, 1, SX, SY, Width, Height );
			}
			else {
				SG.MRectangle.Fill( BackColor, SX, SY, Width, Height );
			}
			switch( TextAlign ) {
				case E_TextAlign.Left:		SG.MString.DrawAtLeft( Text, ForeColor, FontSize, SX, SY + Height/2 ); break;
				case E_TextAlign.Center:	SG.MString.DrawAtCenter( Text, ForeColor, FontSize, SX + Width/2, SY + Height/2 ); break;
				case E_TextAlign.Right:		SG.MString.DrawAtRight( Text, ForeColor, FontSize, SX + Width, SY + Height/2 ); break;
				default:					break;
			}
		}
	}
	
	//绘制控件
	void Draw_StringToBitmap( int StartX, int StartY )
	{
		if( isTouchOn ) {
			SG.MRectangle.Fill( BackColor, SX, SY, Width, Height );
			SG.MBitmap.Draw( BackBitmap, SX + Width - BackBitmap.Width, SY + Height/2 - BackBitmap.Height/2 );
			SG.MString.DrawAtCenter( Text, Color.Orange, FontSize, SX + Width/2, SY + Height/2 );
		}
		else {
			SG.MRectangle.Fill( BackColor, SX, SY, Width, Height );
			SG.MBitmap.Draw( BackBitmap, SX + Width - BackBitmap.Width, SY + Height/2 - BackBitmap.Height/2 );
			SG.MString.DrawAtCenter( Text, ForeColor, FontSize, SX + Width/2, SY + Height/2 );
		}
	}
	
	//绘制控件
	void Draw_Bitmap2( int StartX, int StartY )
	{
		if( isTouchOn ) {
			//SG.FillRectangle( BackColor, SX, SY, Width, Height );
			SG.MBitmap.Draw( BackBitmap1, SX + Width/2 - BackBitmap.Width/2, SY + Height/2 - BackBitmap.Height/2 );
		}
		else {
			//SG.FillRectangle( BackColor, SX, SY, Width, Height );
			SG.MBitmap.Draw( BackBitmap, SX + Width/2 - BackBitmap.Width/2, SY + Height/2 - BackBitmap.Height/2 );
		}
	}
	
	//绘制控件
	void Draw_Default( int StartX, int StartY )
	{
		SG.MRectangle.Fill( Color.Red, SX, SY, Width, Height );	
	}
	
	//=========================================================================
	
	//触控按下事件
	public override bool TouchDown(  int StartX, int StartY, int x, int y )
	{
		if( x >= SX && x <= SX + Width && y >= SY && y <= SY + Height ) {
			if( TouchDownEvent != null ) {
				TouchDownEvent( this );
			}
			isTouchOn = true;
		}
		else {
			isTouchOn = false;
		}
		
		return isTouchOn;
	}
	
	//触控松开事件
	public override void TouchUp( int StartX, int StartY , int x, int y )
	{
		if( x >= SX && x <= SX + Width && y >= SY && y <= SY + Height ) {
			if( TouchUpEvent != null ) {
				TouchUpEvent( this );
			}
		}
		isTouchOn = false;
	}
	
	//触控移动事件
	public override bool TouchMove( int StartX, int StartY , int x, int y )
	{
		return false;
	}
}
}

