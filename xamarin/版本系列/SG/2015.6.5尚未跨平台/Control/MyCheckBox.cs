﻿
using System;
using Android.Graphics;
using n_SG;

namespace n_SG
{
public class MyCheckBox : MyControl
{
	public delegate void d_TouchDown( object sender );
	public d_TouchDown TouchDownEvent;
	
	public delegate void d_TouchUp( object sender );
	public d_TouchUp TouchUpEvent;
	
	int SX;
	int SY;
	int Width;
	int Height;
	
	bool isTouchOn;
	public bool Checked;
	
	Bitmap BackBitmap0;
	Bitmap BackBitmap1;
	
	//构造函数 - 文本信息为空, 显示图片
	public MyCheckBox( Bitmap bm0, Bitmap bm1, int x, int y, int w, int h ) : base()
	{
		SX = x;
		SY = y;
		Width = w;
		Height = h;
		
		BackBitmap0 = bm0;
		BackBitmap1 = bm1;
		
		Checked = false;
		isTouchOn = false;
	}
	
	//构造函数 - 文本信息为空, 显示图片
	public MyCheckBox( Bitmap bm, int x, int y, int w, int h ) : base()
	{
		SX = x;
		SY = y;
		Width = w;
		Height = h;
		
		BackBitmap0 = SG.MBitmap.GetGrayBitmap( bm, 128, 255 );
		BackBitmap1 = SG.MBitmap.GetBlueBitmap( BackBitmap0 );
		
		Checked = false;
		isTouchOn = false;
	}
	
	//=========================================================================
	
	//绘制控件
	public override void Draw( int StartX, int StartY )
	{
		if( Checked ) {
			SG.MBitmap.Draw( BackBitmap1, SX + Width/2 - BackBitmap1.Width/2, SY + Height/2 - BackBitmap1.Height/2 );
		}
		else {
			SG.MBitmap.Draw( BackBitmap0, SX + Width/2 - BackBitmap0.Width/2, SY + Height/2 - BackBitmap0.Height/2 );
		}
	}
	
	//=========================================================================
	
	//触控按下事件
	public override bool TouchDown(  int StartX, int StartY, int x, int y )
	{
		if( x >= SX && x <= SX + Width && y >= SY && y <= SY + Height ) {
			
			Checked = !Checked;
			
			if( TouchDownEvent != null ) {
				TouchDownEvent( this );
			}
			isTouchOn = true;
		}
		else {
			isTouchOn = false;
		}
		
		return isTouchOn;
	}
	
	//触控松开事件
	public override void TouchUp( int StartX, int StartY , int x, int y )
	{
		isTouchOn = false;
		if( x >= SX && x <= SX + Width && y >= SY && y <= SY + Height ) {
			if( TouchUpEvent != null ) {
				TouchUpEvent( this );
			}
		}
	}
	
	//触控移动事件
	public override bool TouchMove( int StartX, int StartY , int x, int y )
	{
		return false;
	}
}
}

