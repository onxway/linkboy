﻿
using System;
using Android.Graphics;
using n_SG;

namespace n_SG
{
public class MyCircleColorSelector : MyControl
{
	public delegate void d_ColorChanged( object sender );
	public d_ColorChanged ColorChanged;
	
	public int Red;
	public int Green;
	public int Blue;
	
	int SX;
	int SY;
	int Width;
	int Height;
	
	int MidX;
	int MidY;
	int R;
	int Padding = 10;
	
	int PointX;
	int PointY;
	
	bool isTouchOn;
	
	Bitmap bitmapAltered;
	
	//构造函数 - 文本信息为空, 显示图片
	public MyCircleColorSelector( int x, int y, int w ) : base()
	{
		SX = x;
		SY = y;
		Width = w;
		Height = w;
		
		MidX = SX + w / 2;
		MidY = SY + w / 2;
		R = w / 2 - Padding;
		
		PointX = MidX;
		PointY = MidY;
		
		Red = 0;
		Green = 0;
		Blue = 0;
		
		isTouchOn = false;
		
		bitmapAltered = Bitmap.CreateBitmap(w, w, Bitmap.Config.Argb8888 );
		Canvas c = new Canvas(bitmapAltered);
		SG.SetObject( c );
		DrawCircle();
		SG.SetPreObject();
	}
	
	//=========================================================================
	
	//绘制控件
	public override void Draw( int StartX, int StartY )
	{
		SG.MBitmap.Draw( bitmapAltered, SX, SY );
		SG.MCircle.Fill( Color.Black, PointX, PointY, 5 );
	}
	
	//绘制彩色圆环
	void DrawCircle()
	{
		int mX = Width / 2;
		int mY = Height / 2;
		for( double a = 0; a <= ColorWave.P2; a+= 0.004 ) {
			int TX = mX + (int)(R * Math.Sin( a ));
			int TY = mY + (int)(R * Math.Cos( a ));
			
			Color c = new Color( ColorWave.GetRed( a ), ColorWave.GetGreen( a ), ColorWave.GetBlue( a ) );
			SG.MLine.Draw( Color.White, c, 5, mX, mY, TX, TY );
		}
	}
	
	//=========================================================================
	
	//触控按下事件
	public override bool TouchDown(  int StartX, int StartY, int x, int y )
	{
		if( x >= SX && x <= SX + Width && y >= SY && y <= SY + Height ) {
			isTouchOn = true;
		}
		else {
			isTouchOn = false;
		}
		return isTouchOn;
	}
	
	//触控松开事件
	public override void TouchUp( int StartX, int StartY , int x, int y )
	{
		isTouchOn = false;
	}
	
	//触控移动事件
	public override bool TouchMove( int StartX, int StartY , int x, int y )
	{
		if( x >= SX && x <= SX + Width && y >= SY && y <= SY + Height ) {
			
			double OffsetX = x - MidX;
			double OffsetY = y - MidY;
			double r = Math.Sqrt( OffsetX * OffsetX + OffsetY * OffsetY );
			if( r >= R ) {
				//return;
			}
			double sa = Math.Acos( OffsetY / r );
			if( OffsetX < 0 ) {
				sa = ColorWave.P2 - sa;
			}
			if( r >= R ) {
				r = R;
			}
			//记录当前的选择点
			PointX = MidX + (int)(r * Math.Sin( sa ));
			PointY = MidY + (int)(r * Math.Cos( sa ));
			
			int Red0 = ColorWave.GetRed( sa );
			int Green0 = ColorWave.GetGreen( sa );
			int Blue0 = ColorWave.GetBlue( sa );
			
			Red = 255 - (255-Red0) * (int)r / R;
			Green = 255 - (255-Green0) * (int)r / R;
			Blue = 255 - (255-Blue0) * (int)r / R;
			
			if( ColorChanged != null ) {
				ColorChanged( this );
			}
			return true;
		}
		else {
			return false;
		}
	}
	
	static class ColorWave
	{
		const int Max = 0xFF;
		
		public const double P2 = Math.PI * 2;
		const double PD6 = P2 * 1 / 6;
		const double P0_6 = P2 * 0 / 6;
		const double P1_6 = P2 * 1 / 6;
		const double P2_6 = P2 * 2 / 6;
		const double P3_6 = P2 * 3 / 6;
		const double P4_6 = P2 * 4 / 6;
		const double P5_6 = P2 * 5 / 6;
		const double P6_6 = P2 * 6 / 6;
		
		//获取红色相位
		public static int GetRed( double a )
		{
			return GetBase( a );
		}
		
		//获取绿色相位
		public static int GetGreen( double a )
		{
			return GetBase( a - P2_6 );
		}
		
		//获取蓝色相位
		public static int GetBlue( double a )
		{
			return GetBase( a - P4_6 );
		}
		
		//获取基础相位
		public static int GetBase( double a )
		{
			if( a < 0 ) {
				a = -a;
				a = P2 - (a % P2);
			}
			a %= P2;
			
			if( a >= P4_6 && a < P5_6 ) {
				return Max & (int)(Max * (a - P4_6) / PD6);
			}
			if( a >= P5_6 && a < P6_6 ) {
				return Max;
			}
			if( a >= P0_6 && a < P1_6 ) {
				return Max;
			}
			if( a >= P1_6 && a < P2_6 ) {
				return Max & (Max - (int)(Max * (a - P1_6)/ PD6));
			}
			return 0;
		}
	}
}
}

