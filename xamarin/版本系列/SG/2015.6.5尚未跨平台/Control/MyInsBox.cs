﻿
using System;
using Android.Graphics;
using n_SG;

namespace n_SG
{
public class MyInsBox : MyControl
{
	public delegate void d_TouchDown( object sender );
	public d_TouchDown TouchDownEvent;
	public delegate void d_TouchUp( object sender );
	public d_TouchUp TouchUpEvent;
	
	public string UserIns;
	
	int SX;
	int SY;
	int Width;
	int Height;
	
	bool isTouchDown;
	bool isTouchOn;
	
	public string Text;
	
	static int MoveOffset;
	
	const int RoundWidth = 4;
	const int RoundHeight = 4;
	
	//构造函数
	public MyInsBox( string t, int x, int y, int w, int h ) : base()
	{
		Text = t;
		
		SX = x;
		SY = y;
		Width = w;
		Height = h;
		
		UserIns = null;
		
		isTouchDown = false;
		isTouchOn = false;
		
		MoveOffset = 0;
		
		//BackColor = new Color( 10, 100, 255 );
		BackColor = new Color( 70, 170, 255 );
	}
	
	//绘制控件
	public override void Draw( int StartX, int StartY )
	{
		Color c;
		if( isTouchDown ) {
			c = Color.OrangeRed;
		}
		else if( isTouchOn ) {
			//c = Color.DarkBlue;
			c = BackColor;
		}
		else {
			c = BackColor;
		}
		
		//填充背景框
		SG.MRectangle.FillRound( Color.WhiteSmoke, RoundWidth, RoundHeight, StartX + SX, StartY + SY, Width, Height );
		
		//绘制标题栏背景框
		SG.MRectangle.FillRound( c, RoundWidth, RoundHeight, StartX + SX, StartY + SY, Width, Height/2 + 10 );
		
		//绘制隔档色条, 隐藏标题栏下方圆角
		SG.MRectangle.Fill( Color.WhiteSmoke, StartX + SX, StartY + SY + Height/2, Width, 10 );
		
		//绘制文本标题
		SG.MString.DrawAtCenter( Text, Color.White, 25, StartX + SX + Width/2, StartY + SY + Height/4 );
		
		//绘制用户指令
		if( UserIns != null ) {
			SG.MString.DrawAtCenter( UserIns, Color.Gray, 25, StartX + SX + Width/2, StartY + SY + Height/4*3 );
		}
		
		//绘制背景框边栏
		SG.MRectangle.DrawRound( c, 1, RoundWidth, RoundHeight, StartX + SX, StartY + SY, Width, Height );
	}
	
	//触控按下事件
	public override bool TouchDown( int StartX, int StartY, int x, int y )
	{
		MoveOffset = StartY;
		
		if( x - StartX >= SX && x - StartX <= SX + Width && y - StartY >= SY && y - StartY <= SY + Height ) {
			if( TouchDownEvent != null ) {
				TouchDownEvent( this );
			}
			isTouchDown = true;
			isTouchOn = true;
		}
		else {
			isTouchDown = false;
			isTouchOn = false;
		}
		
		//return isTouchDown;
		return false;
	}
	
	//触控松开事件
	public override void TouchUp( int StartX, int StartY, int x, int y )
	{
		if( isTouchDown ) {
			if( TouchUpEvent != null ) {
				TouchUpEvent( this );
			}
		}
		isTouchDown = false;
		isTouchOn = false;
	}
	
	//触控移动事件
	public override bool TouchMove( int StartX, int StartY , int x, int y )
	{
		if( isTouchDown && Math.Abs( MoveOffset - StartY ) > 5 ) {
			isTouchDown = false;
		}
		return false;
	}
}
}

