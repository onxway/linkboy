﻿
using System;
using Android.Graphics;
using n_SG;

namespace n_SG
{
public abstract class MyControl
{
	Color v_ForeColor;
	public Color ForeColor {
		set {
			v_ForeColor = value;
			ForeColor1 = GetHighlightColor( value );
		}
		get {
			return v_ForeColor;
		}
	}
	
	public Color ForeColor1;
	
	Color v_BackColor;
	public Color BackColor {
		set {
			v_BackColor = value;
			BackColor1 = GetHighlightColor( value );
		}
		get {
			return v_BackColor;
		}
	}
	
	public Color BackColor1;
	
	Color GetHighlightColor( Color v )
	{
		byte CV = 20;
		Color r = new Color();
		if( v.R > 128 || v.G > 128 || v.B > 128 ) {
			if( v.R - CV >= 0 ) {
				r.R = (byte)(v.R - CV);
			}
			else {
				r.R = 0;
			}
			if( v.G - CV >= 0 ) {
				r.G = (byte)(v.G - CV);
			}
			else {
				r.G = 0;
			}
			if( v.B - CV >= 0 ) {
				r.B = (byte)(v.B - CV);
			}
			else {
				r.B = 0;
			}
		}
		else {
			if( v.R + CV <= 255 ) {
				r.R = (byte)(v.R + CV);
			}
			else {
				r.R = 255;
			}
			if( v.G + CV <= 255 ) {
				r.G = (byte)(v.G + CV);
			}
			else {
				r.G = 255;
			}
			if( v.B + CV <= 255 ) {
				r.B = (byte)(v.B + CV);
			}
			else {
				r.B = 255;
			}
		}
		return r;
	}
	
	public enum E_TextAlign
	{
		Left,
		Center,
		Right
	}
	public E_TextAlign TextAlign;
	
	//构造函数
	public MyControl()
	{
		TextAlign = E_TextAlign.Center;
	}
	
	//绘制控件
	public abstract void Draw( int StartX, int StartY );
	
	//触控按下事件
	public abstract bool TouchDown(  int StartX, int StartY, int x, int y );
	
	//触控松开事件
	public abstract void TouchUp( int StartX, int StartY , int x, int y );
	
	//触控移动事件
	public abstract bool TouchMove( int StartX, int StartY , int x, int y );
}
}


