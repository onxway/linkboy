﻿
using System;
using Android.Graphics;
using n_SG;

namespace n_SG
{
public class MyLabel : MyControl
{
	int SX;
	int SY;
	
	int Width;
	int Height;
	
	Bitmap BackBitmap;
	string Text;
	int FontSize;
	
	enum E_LabelType
	{
		OnlyBitmap,
		OnlyString,
		StringToBitmap,
	}
	
	E_LabelType LabelType;
	
	//构造函数 - 文本信息为空, 显示图片
	public MyLabel( Color bc, Bitmap bm, int x, int y, int w, int h ) : base()
	{
		SX = x;
		SY = y;
		
		Width = w;
		Height = h;
		
		BackBitmap = bm;
		Text = null;
		
		BackColor = bc;
		
		LabelType = E_LabelType.OnlyBitmap;
	}
	
	//构造函数 - 图片为空, 显示文本信息
	public MyLabel( Color bc, string t, int fs, Color fc, int x, int y, int w, int h ) : base()
	{
		SX = x;
		SY = y;
		
		Width = w;
		Height = h;
		
		BackBitmap = null;
		
		BackColor = bc;
		
		Text = t;
		ForeColor = fc;
		FontSize = fs;
		
		LabelType = E_LabelType.OnlyString;
	}
	
	//构造函数 - 文本信息在左边, 颜色为边框, 背景透明, 图片在右边
	public MyLabel( Color bc, string t, int fs, Color fc, Bitmap bm, int x, int y, int w, int h ) : base()
	{
		SX = x;
		SY = y;
		
		Width = w;
		Height = h;
		
		BackBitmap = bm;
		
		BackColor = bc;
		
		Text = t;
		ForeColor = fc;
		FontSize = fs;
		
		LabelType = E_LabelType.StringToBitmap;
	}
	
	//=========================================================================
	
	//绘制控件
	public override void Draw( int StartX, int StartY )
	{
		switch( LabelType ) {
			case E_LabelType.OnlyBitmap: Draw_OnlyBitmap( StartX, StartY ); break;
			case E_LabelType.OnlyString: Draw_OnlyString( StartX, StartY ); break;
			case E_LabelType.StringToBitmap: Draw_StringToBitmap( StartX, StartY ); break;
			default: Draw_Default( StartX, StartY ); break;
		}
	}
	
	//绘制控件
	void Draw_OnlyBitmap( int StartX, int StartY )
	{
		SG.MBitmap.Draw( BackBitmap, SX, SY );
	}
	
	//绘制控件
	void Draw_OnlyString( int StartX, int StartY )
	{
		switch( TextAlign ) {
			case E_TextAlign.Left:		SG.MString.DrawAtLeft( Text, ForeColor, FontSize, SX, SY + Height/2 ); break;
			case E_TextAlign.Center:	SG.MString.DrawAtCenter( Text, ForeColor, FontSize, SX + Width/2, SY + Height/2 ); break;
			case E_TextAlign.Right:		SG.MString.DrawAtRight( Text, ForeColor, FontSize, SX + Width, SY + Height/2 ); break;
			default:					break;
		}
	}
	
	//绘制控件
	void Draw_StringToBitmap( int StartX, int StartY )
	{
		SG.MBitmap.Draw( BackBitmap, SX + Width - BackBitmap.Width, SY + Height/2 - BackBitmap.Height/2 );
		SG.MString.DrawAtCenter( Text, ForeColor, FontSize, SX + Width/2, SY + Height/2 );
	}
	
	//绘制控件
	void Draw_Default( int StartX, int StartY )
	{
		SG.MRectangle.Fill( Color.Red, SX, SY, Width, Height );	
	}
	
	//=========================================================================
	
	//触控按下事件
	public override bool TouchDown(  int StartX, int StartY, int x, int y )
	{
		return false;
	}
	
	//触控松开事件
	public override void TouchUp( int StartX, int StartY , int x, int y )
	{
		
	}
	
	//触控移动事件
	public override bool TouchMove( int StartX, int StartY , int x, int y )
	{
		return false;
	}
}
}

