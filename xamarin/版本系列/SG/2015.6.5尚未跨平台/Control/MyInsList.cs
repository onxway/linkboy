﻿
using System;
using Android.Graphics;
using n_SG;

namespace n_SG
{
public class MyInsList : MyControl
{
	public delegate void d_ItemSelected( object sender, string Text );
	public d_ItemSelected ItemSelected;
	
	int SX;
	int SY;
	int Width;
	int Height;
	
	public bool Visible;
	
	int MoveOffset;
	bool isTouchDown;
	bool isTouchOn;
	
	string[] InsList;
	int InsNumber;
	
	const int PerHeight = 50;
	
	const int RoundWidth = 8;
	const int RoundHeight = 8;
	
	int SelectedItemIndex;
	
	//构造函数
	public MyInsList( int x, int y, int w ) : base()
	{
		InsNumber = 5;
		InsList = new string[InsNumber];
		InsList[0] = "打开台灯";
		InsList[1] = "关闭台灯";
		InsList[2] = "打开电视机";
		InsList[3] = "关闭电视机";
		InsList[4] = "播放一首音乐";
		
		SX = x;
		SY = y;
		Width = w;
		Height = PerHeight * InsNumber + RoundHeight * 2;
		
		Visible = false;
		MoveOffset = 0;
		isTouchDown = false;
		isTouchOn = false;
		
		SelectedItemIndex = -1;
		
		BackColor = new Color( 10, 100, 255 );
	}
	
	//触控按下事件
	public override bool TouchDown( int StartX, int StartY, int x, int y )
	{
		if( !Visible ) {
			return false;
		}
		MoveOffset = StartY;
		
		if( x >= SX && x <= SX + Width && y >= SY && y <= SY + Height ) {
			isTouchDown = true;
			isTouchOn = true;
			
			int ItemSX = SX + RoundWidth;
			int ItemWidth = Width - RoundWidth * 2;
			int ItemHeight = PerHeight;
			for( int i = 0; i < InsNumber; ++i ) {
				int ItemSY = SY + RoundHeight + i * ItemHeight;
				if( x >= ItemSX && x <= ItemSX + ItemWidth && y >= ItemSY && y <= ItemSY + ItemHeight ) {
					SelectedItemIndex = i;
					break;
				}
			}
		}
		else {
			isTouchDown = false;
			isTouchOn = false;
		}
		
		return isTouchDown;
	}
	
	//触控松开事件
	public override void TouchUp( int StartX, int StartY, int x, int y )
	{
		if( !Visible ) {
			return;
		}
		isTouchDown = false;
		isTouchOn = false;
		
		if( SelectedItemIndex != -1 && ItemSelected != null ) {
			ItemSelected( this, InsList[SelectedItemIndex] );
			SelectedItemIndex = -1;
		}
	}
	
	//触控移动事件
	public override bool TouchMove( int StartX, int StartY , int x, int y )
	{
		if( !Visible ) {
			return false;
		}
		if( isTouchDown && Math.Abs( MoveOffset - StartY ) > 5 ) {
			isTouchDown = false;
		}
		return isTouchOn;
	}
	
	//绘制控件
	public override void Draw( int StartX, int StartY )
	{
		if( !Visible ) {
			return;
		}
		//填充背景框
		SG.MRectangle.FillRound( Color.WhiteSmoke, RoundWidth, RoundHeight, SX, SY, Width, Height );
		
		//绘制背景框边栏
		SG.MRectangle.DrawRound( BackColor, 1, RoundHeight, 10, SX, SY, Width, Height );
		
		//绘制指令列表
		int ItemSX = SX + RoundWidth;
		int ItemWidth = Width - RoundWidth * 2;
		int ItemHeight = PerHeight;
		
		for( int i = 0; i < InsNumber; ++i ) {
			int ItemSY = SY + RoundHeight + i * ItemHeight;
			if( i == SelectedItemIndex ) {
				SG.MRectangle.Fill( BackColor, ItemSX, ItemSY, ItemWidth, ItemHeight );
				SG.MString.DrawAtLeft( InsList[i], Color.WhiteSmoke, 22, ItemSX + 20, ItemSY + ItemHeight/2 );
				SG.MCircle.Fill( Color.WhiteSmoke, ItemSX + 10, ItemSY + ItemHeight/2, 4 );
			}
			else {
				//SG.DrawRectangle( Color.Green, 1, ItemSX, ItemSY, ItemWidth, ItemHeight );
				SG.MString.DrawAtLeft( InsList[i], BackColor, 22, ItemSX + 20, ItemSY + ItemHeight/2 );
				SG.MCircle.Fill( BackColor, ItemSX + 10, ItemSY + ItemHeight/2, 4 );
			}
		}
	}
}
}

