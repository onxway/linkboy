﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Widget;
using Android.Runtime;
using Android.Util;
using Android.Views;

using System.Net;
using System.Net.Sockets;
using System.Threading;

using Android.Graphics;
using n_SG;
using n_SYS;

namespace n_Wifi
{
public static class Wifi
{
	static Thread t_Receive;
	static Thread t_Tick;
	
	static Socket stSend; //创建发送数据套接字
	static byte[] SendBuffer;
	static byte[] ReceiveBuffer;
	
	//初始化
	public static void Init()
	{
		TCP_ClientStart();
			
		t_Receive = new Thread (ReceiveThread);
		//t_Receive.Start();
		
		t_Tick = new Thread (TickThread);
		//t_Tick.Start();
	}
	
	//程序退出
	public static void Close()
	{
		t_Receive.Abort();
		t_Receive.Join();
		t_Tick.Abort();
		t_Tick.Join();
		
		if( stSend != null && stSend.Connected ) {
			stSend.Shutdown(SocketShutdown.Both);
			stSend.Close();
		}
	}
	
	static void TickThread()
	{
		while( true ) {
			if( stSend != null && stSend.Connected ) {
				SendTest( stSend );
			}
			System.Threading.Thread.Sleep( 5000 );
			//this.PostInvalidate();
		}
	}
	
	static void ReceiveThread()
	{
		while (true) {
			
			try {
				int byteCount = stSend.Receive( ReceiveBuffer, stSend.Available, SocketFlags.None );
			for( int i = 0; i < byteCount; i++ ) {
				test( ReceiveBuffer[i] );
			}
			}catch{
			}
		}
	}
	
	public static void test( byte a )
	{
		if( a == 0xAA ) {
			SYS.SetReceiveData( "S:" + data4 + data3 + data2 + data1 + data0 + " - R:" );
			return;
		}
		//ReceiveData += a.ToString( "X" ).PadLeft( 2, '0' );
		SYS.AddReceiveData( ((char)a).ToString() );
	}
	
	//开始SOCKET通信
	static void TCP_ClientStart()
	{
		SendBuffer = new byte[256];
		ReceiveBuffer = new byte[256];
		
		TCP_ClientReset();
	}
	
	static void TCP_ClientReset()
	{
		//以下代码是判断是否和远程终结点成功连接
		try
		{
			//初始化一个Socket实例
			stSend = new Socket( AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp );
			
			//根据IP地址和端口号创建远程终结点
			//IPEndPoint tempRemoteIP = new IPEndPoint( IPAddress.Parse ( "10.10.100.254" ) , 8899 ); //usr wifi模块IP地址
			IPEndPoint tempRemoteIP = new IPEndPoint( IPAddress.Parse ( "192.168.4.100" ) , 333 ); //ESP8266 模块IP地址 (AP模式下)
			//IPEndPoint tempRemoteIP = new IPEndPoint( IPAddress.Parse ( "192.168.0.109" ) , 333 ); //ESP8266 模块IP地址 (STA模式连接到ReC)
			
			EndPoint epTemp = (EndPoint)tempRemoteIP;
			
			//连接到远程主机的端口
			stSend.Connect( epTemp );
		}
		catch (Exception e)
		{
			SYS.AddMessage( "目标计算机拒绝连接请求！" + e );
		}
	}
	
	//发送指令序列
	static char data0 = '0';
	static char data1 = '0';
	static char data2 = '0';
	static char data3 = '0';
	static char data4 = '0';
	static void SendTest( Socket s )
	{
		string command = (int)data4 + "," + (int)data3 + "," + (int)data2 + "," + (int)data1 + "," + (int)data0;
		data0++;
		if( data0 > '9' ) {
			data0 = '0';
			data1++;
			if( data1 > '9' ) {
				data1 = '0';
				data2++;
				if( data2 > '9' ) {
					data2 = '0';
					data3++;
					if( data3 > '9' ) {
						data3 = '0';
						data4++;
						if( data4 > '9' ) {
							data4 = '0';
						}
					}
				}
			}
		}
		try {
			DirectSendData( s, command );
		}
		catch {
			try {
				TCP_ClientReset();
				DirectSendData( s, command );
				SYS.AddMessage( "重新尝试连接并发送" );
			}catch {
				SYS.AddMessage( "重发失败!!!" );
			}
		}
	}
	
	//直接发送数据
	static void DirectSendData( Socket s, string command )
	{
		string[] csp = command.Split( ',' );
		SendBuffer[0] = 0xAA;
		SendBuffer[1] = (byte)csp.Length;
		for( int i = 0; i < csp.Length; ++i ) {
			SendBuffer[i+2] = (byte)int.Parse( csp[i] );
		}
		s.Send( SendBuffer, csp.Length + 2, SocketFlags.None );
	}
}
}

