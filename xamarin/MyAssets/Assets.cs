
using System;
using Android.Graphics;
using Android.Views;
using Android.App;
using Android.Content;
using Android.Runtime;
using Android.Widget;
using Android.OS;
using n_SYS;
using System.IO;

namespace n_Assets
{
public static class Assets
{
	//初始化
	public static void Init()
	{
		
	}
	
	//读取assets图片
	public static Bitmap[] LoadAllICon( string BaseDir )
	{
		string[] FileList = SYS.A.Assets.List( BaseDir );
		
		Bitmap[] bl = new Bitmap[FileList.Length];
		int i = 0;
		
		foreach( string FileName in FileList ) {
			Stream s = SYS.A.Assets.Open( BaseDir + "/" + FileName );
			Bitmap b = BitmapFactory.DecodeStream( s );
			bl[i] = b;
			i++;
		}
		return bl;
	}
	
	//读取一张图片
	public static Bitmap OpenBitmap( string Path )
	{
		Stream s = SYS.A.Assets.Open( Path );
		return BitmapFactory.DecodeStream( s );
	}
}
}

