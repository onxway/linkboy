﻿
using Geeks;
using n_MyView;
using System;
using Android.Graphics;
using Android.Views;
using Android.App;
using Android.Content;
using Android.Runtime;
using Android.Widget;
using Android.OS;
using n_SYS;
using System.IO;
using Java.IO;

using System.Runtime.Serialization.Formatters.Binary;

namespace n_LocalIO
{
//系统函数
public static class LocalIO
{
	static Activity A;
	
	static byte[] Read = new byte[5000];
	static System.Text.UTF8Encoding Encoding;
	
	//初始化
	public static void Init( object o )
	{
		A = (Activity)o;
		Encoding = new System.Text.UTF8Encoding();
	}
	
	//保存文件
	public static void Save( string FileName, string Text )
	{
		Byte[] bytes = Encoding.GetBytes( Text );
		Stream outputStream = A.OpenFileOutput( FileName, FileCreationMode.Private );
		outputStream.Write( bytes, 0, bytes.Length );
		outputStream.Close();
	}
	
	//加载文件
	public static string Load( string FileName )
	{
		Stream input = A.OpenFileInput( FileName );
		int n = input.Read( Read, 0, Read.Length );
		input.Close();
		return Encoding.GetString( Read, 0, n );
	}
	
	//删除文件
	public static void Delete(string FileName)
	{
		A.DeleteFile(FileName);
	}
}
}



