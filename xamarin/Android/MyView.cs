﻿using Android.Content;
using Android.Views;
using System.Threading;
using Android.Graphics;
using n_M;
using n_SG;
using n_MyActivity;
using n_SYS;
using n_VM;

namespace n_MyView
{
	public class MyView : View
{
	Rect r;
	
	public bool ShowMessage = false;
	bool ShowFullMassage = true;
	bool ShowMultiLine = true;
	public string SysMessage;
	public string ReceiveData;
	public int MesStartY;
	
	public static Thread t_MyTimer;
	public static Thread t_SysTick;
	
	static bool FirstRun;
	
	public static bool isVM = true;
	
	public int RefreshTime;
	
	//构造函数
	public MyView(Context context ) : base(context, null)
	{
		SYS.A = context as MyActivity;
		SYS.V = this;
		
		//以下获取的是屏幕的尺寸
		//DisplayMetrics dm = new DisplayMetrics();  
		//a1.WindowManager.DefaultDisplay.GetMetrics( dm );
		//r = new Rect( 0, 0, dm.WidthPixels, dm.HeightPixels );
		
		//Display d = a1.WindowManager.DefaultDisplay;
		//r = new Rect( 0, 0, d.Width, d.Height );
		
		ReceiveData = "";
		SysMessage = "";
		MesStartY = 10;
		
		FirstRun = true;
		RefreshTime = 0;
	}
	
	//系统关闭
	public void Close()
	{
		if( t_MyTimer.IsAlive ) {
			t_MyTimer.Abort();
			t_MyTimer.Join();
		}
		if( t_SysTick.IsAlive ) {
			t_SysTick.Abort();
			t_SysTick.Join();
		}
		M.Close();
	}
	
	//================================================================================
	
	public bool MyOnKeyDown( Keycode k, KeyEvent e )
	{
		return M.MyOnKeyDown( k );
	}
	
	public void MyOnResume()
	{
		if( FirstRun ) {
			FirstRun = false;
			return;
		}
		M.MyOnResume();
		//t_MyTimer = new Thread( t_MyTimerThread );
		//t_MyTimer.Start();
	}
	
	//布局事件
	protected override void OnLayout( bool changed, int left, int top, int right,int bottom )
	{
		if( changed ) {
			int width = right - left;    //得到宽度
			int height = bottom - top;   //得到高度
			r = new Rect( 0, 0, width, height );
			M.Init( SYS.A, r.Width(), r.Height() );
			M.SetSize( r.Width(), r.Height() );
			
			t_MyTimer = new Thread( t_MyTimerThread );
			t_SysTick = new Thread( t_t_SysTickThread );
			//t_MyTimer.Start();
		}
	}
	
	void t_MyTimerThread()
	{
//		while( true ) {
//			M.TimeTick();
//			System.Threading.Thread.Sleep( 50 );
//		}
		
		if( !isVM ) {
			return;
		}
		System.Threading.Thread.Sleep( 100 );
		
		//int i = 0;
		while( true ) {
			
			//M.TimeTick();
			//System.Threading.Thread.Sleep( 1 );
			//n_SYS.SYS.AddMessage(  + " " );
			uint l = VM.运行();
			//if( i % 100 == 0 ) {
			//	n_SYS.SYS.AddMessage( "\n" );
			//}
			//i++;
		}
	}
	
	void t_t_SysTickThread()
	{
		System.Threading.Thread.Sleep( 100 );
		while( true ) {
			System.Threading.Thread.Sleep( 1 );
			
			if( n_MyView.MyView.myTimerEnable ) {
				lock( M.locker ) {
					n_VM.VM.清空系统参数();
					n_VM.VM.添加中断事件( n_MyView.MyView.myTimerEnableIndex );
				}
			}
		}
	}
	
	//系统绘图事件
	protected override void OnDraw(Android.Graphics.Canvas canvas)
	{
		try {

		
		//绘制控件
		M.Draw( canvas );
		
		if( ShowMessage ) {
		
			SG.SetObject( canvas );
			
			//绘制文本信息
			SG.MString.DrawAtLeft( ReceiveData, Color.Gray, 16, 5, MesStartY );
			
			if( ShowMultiLine ) {
				string[] s = SysMessage.Split( '\n' );
				for( int i = 0; i < s.Length; ++i ) {
					SG.MString.DrawAtLeft( s[i], Color.Gray, 16, 5, MesStartY + 15 + i * 15 );
				}
			}
			else {
				if( ShowFullMassage ) {
					int CharNumber = 20;
					for( int i = 0; i < 30; ++i ) {
						try {
							string s = SysMessage.Substring( i * CharNumber, CharNumber );
							SG.MString.DrawAtLeft( s, Color.Gray, 16, 5, MesStartY + 15 + i * 15 );
						}catch {
							SG.MString.DrawAtLeft( SysMessage.Substring(i * CharNumber ), Color.Gray, 16, 5, MesStartY + 15 + i * 15 );
							break;
						}
					}
				}
				else {
					SG.MString.DrawAtLeft( SysMessage, Color.Gray, 16, 5, MesStartY + 15 );
				}
			}
			
			SG.SetPreObject();
		}
		}
		catch {
			SysMessage += "刷新失败!\n";
		}
		base.OnDraw(canvas);
		
		if( RefreshTime > 0 ) {
			RefreshTime -= 1;
		}
	}
	
	//触摸事件
	public override bool OnTouchEvent(MotionEvent e)
	{
		int Action = (int)e.Action;
		switch (Action) {
		case (int)MotionEventActions.Down:
				M.TouchDown( (int)e.GetX (), (int)e.GetY () );
				break;
			case (int)MotionEventActions.Move:
				M.TouchMove( (int)e.GetX (), (int)e.GetY () );
				break;
			case (int)MotionEventActions.Up:
				M.TouchUp( (int)e.GetX (), (int)e.GetY () );
				break;
			default:
				break;
		}
		//刷新屏幕
		Invalidate();
		return true;
	}
		
		public static bool myKeyDown;
		public const int myKeyDownIndex = 0x01;
		public static bool myKeyPress;
		public const int myKeyPressIndex = 0x02;
		public static bool myKeyUp;
		public const int myKeyUpIndex = 0x03;
		public static bool myMouseDown;
		public const int myMouseDownIndex = 0x04;
		public static bool myMouseEnter;
		public const int myMouseEnterIndex = 0x05;
		public static bool myMouseHover;
		public const int myMouseHoverIndex = 0x06;
		public static bool myMouseLeave;
		public const int myMouseLeaveIndex = 0x07;
		public static bool myMouseMove;
		public const int myMouseMoveIndex = 0x08;
		public static bool myMouseUp;
		public const int myMouseUpIndex = 0x09;
		
		//临时占位
		public const int myReadData = 0x0A;
		
		public static bool mySerialPortReceived;
		public const int mySerialPortReceivedIndex = 0x10;
		public static bool myTimerEnable;
		public const int myTimerEnableIndex = 0x11;
}
}

