﻿using System;

using Android.App;
using Android.Content;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Android.OS;
using System.IO;
using System.Threading;

using Android.Graphics;
using System.Collections.Generic;
using Android.Util;
using n_MyView;

namespace n_MyActivity
{
	[Activity(MainLauncher = true, Icon = "@drawable/icon")]
	public class MyActivity : Activity
	{
		static MyView view1;
		
		public MyHandler HD;
		
		//==============================================================
		public class MyHandler : Handler
		{
			public override void HandleMessage(Message msg)
			{
				base.HandleMessage(msg);
				
				//获得通过handler.sendEmptyMessage发送的消息编码
				int what = msg.What;
				n_SYS.SYS.TrigDeal( what );
			}
		}
		
//		public override bool DispatchKeyEvent(KeyEvent e)
//		{
//			if( e.KeyCode == Keycode.Enter ) {
//				n_SYS.SYS.SwitchIme();
//				return true;
//			}
//			else {
//				return true;
//				//return base.DispatchKeyEvent(e);
//			}
//		}
		
		protected override void OnCreate(Bundle bundle)
		{
			base.OnCreate(bundle);
			
			HD = new MyHandler();
			
			view1 = new MyView( this );
			SetContentView(view1);
		}
		
		protected override void OnDestroy()
		{
			base.OnDestroy();
			view1.Close();
		}
		
		protected override void OnStop()
		{
			base.OnStop();
			
			//view1.Close();
		}
		
		protected override void OnResume()
		{
			base.OnResume();
			
			view1.MyOnResume();
		}
		
		public override bool OnKeyDown( Keycode k, KeyEvent e )
		{
			if( view1.MyOnKeyDown( k, e ) ) {
				return true;
			}
			else {
				return base.OnKeyDown( k, e );
			}
		}
	}
}

