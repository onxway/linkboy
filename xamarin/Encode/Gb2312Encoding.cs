﻿
//2014年12月11日 下午 6:09
//备注: 这是从网上找的程序, 转换GB2312到unicode
//今天发现一个BUG, 就是读取有些文件的时候出现异常
//最后终于决定改了下两个 if 判断语句, 问题解决了, 但是不清楚为什么...

namespace n_Gb2312Encoding
{
using System;
using System.Collections.Generic;
using System.Text;


public class Gb2312Encoding : Encoding
{
	StringBuilder sb;
	
	public Gb2312Encoding ()
	{
		sb = new StringBuilder( 50 );
	}
	
	//我自己加的, GBK 转unicode
	public string GBKtoUnicode( byte[] Buffer, int index, int Length )
	{
		sb.Clear();
		for( int i = 0; i < Length; ++i ) {
			if( Buffer[i+index] < 128 ) {
				sb.Append( ((char)Buffer[i+index]).ToString() );
			}
			else {
				ushort d = (ushort)((Buffer[i+index+1] + 256 * Buffer[i+index]) - 0x8080);
				i++;
				ushort OD;
				Gb2312toUnicodeDictinary.gb2312toUnicode.TryGetValue( d, out OD );
				sb.Append( ((char)OD).ToString() );
			}
		}
		return sb.ToString();
	}
	
	public override string WebName
	{
		get
		{
			return "gb2312";
		}
	}
	
	public override int GetBytes(char[] chars, int charIndex, int charCount, byte[] bytes, int byteIndex)
	{
		throw new NotImplementedException();
	}
	public override int GetChars(byte[] bytes, int byteIndex, int byteCount, char[] chars, int charIndex)
	{
		int j = 0;
		char c;
		for (int i = 0; i < byteCount; i += 2)
		{
			if (i + 1 >=byteCount)
			//if (i + 1 >= bytes.Length)
			{
				char[] last = Encoding.UTF8.GetChars(new byte[] { bytes[i] });
				chars[j]=last[0];
			}
			else
			{
				byte[] bb = new byte[] { bytes[i], bytes[i + 1] };
				if (Gb2312toUnicodeDictinary.TryGetChar(bb, out c))
				{
					chars[j] = c;
					j++;
				}
				else
				{
					char[] tt = Encoding.UTF8.GetChars(new byte[] { bb[1] });
					chars[j] = tt[0];
					j++;
					//测试下一个
					if (i + 2 >=byteCount)
					//if (i + 2 >= bytes.Length)
					{
						char[] tttt = Encoding.UTF8.GetChars(new byte[] { bb[0] });
						chars[j] = tttt[0];
						j++;
					}
					else
					{
						byte[] test = new byte[] { bb[0], bytes[i + 2] };
						if (Gb2312toUnicodeDictinary.TryGetChar(test, out c))
						{
							chars[j] = c;
							j++;
							i++;
						}
						else
						{
							char[] ttt = Encoding.UTF8.GetChars(new byte[] { bb[0] });
							chars[j] = ttt[0];
							j++;
						}
					}
				}
			}
		}
		return chars.Length;
	}
	
	public override int GetByteCount(char[] chars, int index, int count)
	{
		return count;
	}

	public override int GetCharCount(byte[] bytes, int index, int count)
	{
		return count;
	}
	
	public override int GetMaxByteCount(int charCount)
	{
		return charCount;
	}
	
	public override int GetMaxCharCount(int byteCount)
	{
		return byteCount;
	}
	
	public static int CharacterCount
	{
		get { return 7426; }
	}
	//...
}
}

		