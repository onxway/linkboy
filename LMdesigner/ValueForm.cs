﻿/*
 * 由SharpDevelop创建。
 * 用户： linkboy
 * 日期: 2016/8/6
 * 时间: 21:31
 * 
 * 要改变这种模板请点击 工具|选项|代码编写|编辑标准头文件
 */
using System;
using System.Drawing;
using System.Windows.Forms;

namespace linkboy_IDE.n_ValueForm
{
	/// <summary>
	/// Description of ValueForm.
	/// </summary>
	public partial class ValueForm : Form
	{
		bool isOK;
		
		public ValueForm()
		{
			//
			// The InitializeComponent() call is required for Windows Forms designer support.
			//
			InitializeComponent();
			
			isOK = false;
		}
		
		//显示窗体
		public string Run( string Mes )
		{
			isOK = false;
			
			this.label1.Text = Mes;
			
			//注意,直接设置Visible不能使窗体获得焦点
			//this.Visible = true;
			this.ShowDialog();
			
			while( this.Visible ) {
				System.Windows.Forms.Application.DoEvents();
			}
			if( isOK ) {
				return this.textBox1.Text;
			}
			return null;
		}
		
		void Button1Click(object sender, EventArgs e)
		{
			isOK = true;
			this.Visible =false;
		}
		
		void ValueFormFormClosing(object sender, FormClosingEventArgs e)
		{
			e.Cancel = true;
			this.Visible = false;
		}
	}
}


