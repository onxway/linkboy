﻿/*
 * 由 SharpDevelop 创建。
 * 用户: bbb_2
 * 日期: 2009-11-17
 * 时间: 11:15
 * 
 * 如果你想要更改该模板，那么请使用“工具 | 选项 | 编辑 | 编辑标准标题”。
 */
namespace n_ModuleForm
{
	partial class ModuleForm
	{
		/// <summary>
		/// Designer variable used to keep track of non-visual components.
		/// </summary>
		private System.ComponentModel.IContainer components = null;
		
		/// <summary>
		/// Disposes resources used by the form.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing) {
				if (components != null) {
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}
		
		/// <summary>
		/// This method is required for Windows Forms designer support.
		/// Do not change the method contents inside the source code editor. The Forms designer might
		/// not be able to load this method if it was changed manually.
		/// </summary>
		private void InitializeComponent()
		{
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ModuleForm));
			this.panel1 = new System.Windows.Forms.Panel();
			this.label17 = new System.Windows.Forms.Label();
			this.label16 = new System.Windows.Forms.Label();
			this.label15 = new System.Windows.Forms.Label();
			this.buttonB2 = new System.Windows.Forms.Button();
			this.label14 = new System.Windows.Forms.Label();
			this.buttonD1 = new System.Windows.Forms.Button();
			this.buttonA0 = new System.Windows.Forms.Button();
			this.textBoxEX = new System.Windows.Forms.TextBox();
			this.label13 = new System.Windows.Forms.Label();
			this.textBoxMesFile = new System.Windows.Forms.TextBox();
			this.label8 = new System.Windows.Forms.Label();
			this.textBoxType = new System.Windows.Forms.TextBox();
			this.label6 = new System.Windows.Forms.Label();
			this.textBoxImgPath = new System.Windows.Forms.TextBox();
			this.checkBox254 = new System.Windows.Forms.CheckBox();
			this.textBoxConfig = new System.Windows.Forms.TextBox();
			this.textBoxDriverPath = new System.Windows.Forms.TextBox();
			this.label5 = new System.Windows.Forms.Label();
			this.textBoxImageList = new System.Windows.Forms.TextBox();
			this.label4 = new System.Windows.Forms.Label();
			this.textBoxUserName = new System.Windows.Forms.TextBox();
			this.label2 = new System.Windows.Forms.Label();
			this.checkBoxCanSwap = new System.Windows.Forms.CheckBox();
			this.checkBoxYuanJian = new System.Windows.Forms.CheckBox();
			this.panel5 = new System.Windows.Forms.Panel();
			this.richTextBoxSim = new System.Windows.Forms.RichTextBox();
			this.buttonSetSimValue = new System.Windows.Forms.Button();
			this.组件名称textBox = new System.Windows.Forms.TextBox();
			this.buttonScale = new System.Windows.Forms.Button();
			this.textBoxScale = new System.Windows.Forms.TextBox();
			this.checkBox通信通道 = new System.Windows.Forms.CheckBox();
			this.checkBox控制器组件 = new System.Windows.Forms.CheckBox();
			this.保存当前组件button = new System.Windows.Forms.Button();
			this.打开组件文件button = new System.Windows.Forms.Button();
			this.panel2 = new System.Windows.Forms.Panel();
			this.buttonNote = new System.Windows.Forms.Button();
			this.添加端口button = new System.Windows.Forms.Button();
			this.textBoxPortExt = new System.Windows.Forms.TextBox();
			this.comboBox端口功能名称 = new System.Windows.Forms.ComboBox();
			this.textBox名称 = new System.Windows.Forms.TextBox();
			this.comboBox导线方向 = new System.Windows.Forms.ComboBox();
			this.comboBoxMasOrCli = new System.Windows.Forms.ComboBox();
			this.buttonRestore = new System.Windows.Forms.Button();
			this.buttonDelete = new System.Windows.Forms.Button();
			this.buttonSetNumber = new System.Windows.Forms.Button();
			this.label7 = new System.Windows.Forms.Label();
			this.label3 = new System.Windows.Forms.Label();
			this.label10 = new System.Windows.Forms.Label();
			this.label12 = new System.Windows.Forms.Label();
			this.label9 = new System.Windows.Forms.Label();
			this.导出组件button = new System.Windows.Forms.Button();
			this.label1 = new System.Windows.Forms.Label();
			this.打开图片文件button = new System.Windows.Forms.Button();
			this.checkBoxAllMove = new System.Windows.Forms.CheckBox();
			this.checkBoxRemo = new System.Windows.Forms.CheckBox();
			this.label11 = new System.Windows.Forms.Label();
			this.panel4 = new System.Windows.Forms.Panel();
			this.panel1.SuspendLayout();
			this.panel5.SuspendLayout();
			this.panel2.SuspendLayout();
			this.SuspendLayout();
			// 
			// panel1
			// 
			this.panel1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(169)))), ((int)(((byte)(204)))), ((int)(((byte)(218)))));
			this.panel1.Controls.Add(this.label17);
			this.panel1.Controls.Add(this.checkBox通信通道);
			this.panel1.Controls.Add(this.checkBoxRemo);
			this.panel1.Controls.Add(this.label16);
			this.panel1.Controls.Add(this.label15);
			this.panel1.Controls.Add(this.buttonB2);
			this.panel1.Controls.Add(this.label14);
			this.panel1.Controls.Add(this.buttonD1);
			this.panel1.Controls.Add(this.buttonA0);
			this.panel1.Controls.Add(this.textBoxEX);
			this.panel1.Controls.Add(this.label13);
			this.panel1.Controls.Add(this.textBoxMesFile);
			this.panel1.Controls.Add(this.label8);
			this.panel1.Controls.Add(this.textBoxType);
			this.panel1.Controls.Add(this.label6);
			this.panel1.Controls.Add(this.textBoxImgPath);
			this.panel1.Controls.Add(this.checkBox254);
			this.panel1.Controls.Add(this.textBoxConfig);
			this.panel1.Controls.Add(this.textBoxDriverPath);
			this.panel1.Controls.Add(this.label5);
			this.panel1.Controls.Add(this.textBoxImageList);
			this.panel1.Controls.Add(this.label4);
			this.panel1.Controls.Add(this.textBoxUserName);
			this.panel1.Controls.Add(this.label2);
			this.panel1.Controls.Add(this.checkBoxCanSwap);
			this.panel1.Controls.Add(this.checkBoxYuanJian);
			this.panel1.Controls.Add(this.panel5);
			this.panel1.Controls.Add(this.组件名称textBox);
			this.panel1.Controls.Add(this.buttonScale);
			this.panel1.Controls.Add(this.textBoxScale);
			this.panel1.Controls.Add(this.checkBox控制器组件);
			this.panel1.Controls.Add(this.保存当前组件button);
			this.panel1.Controls.Add(this.打开组件文件button);
			this.panel1.Controls.Add(this.panel2);
			this.panel1.Controls.Add(this.导出组件button);
			this.panel1.Controls.Add(this.label1);
			this.panel1.Controls.Add(this.打开图片文件button);
			this.panel1.Controls.Add(this.checkBoxAllMove);
			this.panel1.Controls.Add(this.label11);
			resources.ApplyResources(this.panel1, "panel1");
			this.panel1.Name = "panel1";
			// 
			// label17
			// 
			resources.ApplyResources(this.label17, "label17");
			this.label17.ForeColor = System.Drawing.Color.Black;
			this.label17.Name = "label17";
			// 
			// label16
			// 
			resources.ApplyResources(this.label16, "label16");
			this.label16.ForeColor = System.Drawing.Color.Black;
			this.label16.Name = "label16";
			// 
			// label15
			// 
			resources.ApplyResources(this.label15, "label15");
			this.label15.ForeColor = System.Drawing.Color.Black;
			this.label15.Name = "label15";
			// 
			// buttonB2
			// 
			this.buttonB2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(0)))));
			resources.ApplyResources(this.buttonB2, "buttonB2");
			this.buttonB2.ForeColor = System.Drawing.Color.Black;
			this.buttonB2.Name = "buttonB2";
			this.buttonB2.UseVisualStyleBackColor = false;
			this.buttonB2.Click += new System.EventHandler(this.ButtonB2Click);
			// 
			// label14
			// 
			resources.ApplyResources(this.label14, "label14");
			this.label14.ForeColor = System.Drawing.Color.Black;
			this.label14.Name = "label14";
			// 
			// buttonD1
			// 
			this.buttonD1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(0)))));
			resources.ApplyResources(this.buttonD1, "buttonD1");
			this.buttonD1.ForeColor = System.Drawing.Color.Black;
			this.buttonD1.Name = "buttonD1";
			this.buttonD1.UseVisualStyleBackColor = false;
			this.buttonD1.Click += new System.EventHandler(this.ButtonD1Click);
			// 
			// buttonA0
			// 
			this.buttonA0.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(0)))));
			resources.ApplyResources(this.buttonA0, "buttonA0");
			this.buttonA0.ForeColor = System.Drawing.Color.Black;
			this.buttonA0.Name = "buttonA0";
			this.buttonA0.UseVisualStyleBackColor = false;
			this.buttonA0.Click += new System.EventHandler(this.ButtonA0Click);
			// 
			// textBoxEX
			// 
			resources.ApplyResources(this.textBoxEX, "textBoxEX");
			this.textBoxEX.Name = "textBoxEX";
			// 
			// label13
			// 
			resources.ApplyResources(this.label13, "label13");
			this.label13.ForeColor = System.Drawing.Color.Black;
			this.label13.Name = "label13";
			// 
			// textBoxMesFile
			// 
			resources.ApplyResources(this.textBoxMesFile, "textBoxMesFile");
			this.textBoxMesFile.Name = "textBoxMesFile";
			// 
			// label8
			// 
			resources.ApplyResources(this.label8, "label8");
			this.label8.ForeColor = System.Drawing.Color.Black;
			this.label8.Name = "label8";
			// 
			// textBoxType
			// 
			resources.ApplyResources(this.textBoxType, "textBoxType");
			this.textBoxType.Name = "textBoxType";
			// 
			// label6
			// 
			resources.ApplyResources(this.label6, "label6");
			this.label6.ForeColor = System.Drawing.Color.Black;
			this.label6.Name = "label6";
			// 
			// textBoxImgPath
			// 
			this.textBoxImgPath.BackColor = System.Drawing.Color.WhiteSmoke;
			this.textBoxImgPath.BorderStyle = System.Windows.Forms.BorderStyle.None;
			resources.ApplyResources(this.textBoxImgPath, "textBoxImgPath");
			this.textBoxImgPath.Name = "textBoxImgPath";
			// 
			// checkBox254
			// 
			this.checkBox254.Checked = true;
			this.checkBox254.CheckState = System.Windows.Forms.CheckState.Checked;
			this.checkBox254.ForeColor = System.Drawing.Color.ForestGreen;
			resources.ApplyResources(this.checkBox254, "checkBox254");
			this.checkBox254.Name = "checkBox254";
			this.checkBox254.UseVisualStyleBackColor = true;
			this.checkBox254.CheckedChanged += new System.EventHandler(this.CheckBox254CheckedChanged);
			// 
			// textBoxConfig
			// 
			this.textBoxConfig.AcceptsReturn = true;
			this.textBoxConfig.AcceptsTab = true;
			resources.ApplyResources(this.textBoxConfig, "textBoxConfig");
			this.textBoxConfig.Name = "textBoxConfig";
			// 
			// textBoxDriverPath
			// 
			resources.ApplyResources(this.textBoxDriverPath, "textBoxDriverPath");
			this.textBoxDriverPath.Name = "textBoxDriverPath";
			// 
			// label5
			// 
			resources.ApplyResources(this.label5, "label5");
			this.label5.ForeColor = System.Drawing.Color.Black;
			this.label5.Name = "label5";
			// 
			// textBoxImageList
			// 
			resources.ApplyResources(this.textBoxImageList, "textBoxImageList");
			this.textBoxImageList.Name = "textBoxImageList";
			// 
			// label4
			// 
			resources.ApplyResources(this.label4, "label4");
			this.label4.ForeColor = System.Drawing.Color.Black;
			this.label4.Name = "label4";
			// 
			// textBoxUserName
			// 
			resources.ApplyResources(this.textBoxUserName, "textBoxUserName");
			this.textBoxUserName.Name = "textBoxUserName";
			// 
			// label2
			// 
			resources.ApplyResources(this.label2, "label2");
			this.label2.ForeColor = System.Drawing.Color.Black;
			this.label2.Name = "label2";
			// 
			// checkBoxCanSwap
			// 
			this.checkBoxCanSwap.ForeColor = System.Drawing.Color.Black;
			resources.ApplyResources(this.checkBoxCanSwap, "checkBoxCanSwap");
			this.checkBoxCanSwap.Name = "checkBoxCanSwap";
			this.checkBoxCanSwap.UseVisualStyleBackColor = true;
			// 
			// checkBoxYuanJian
			// 
			this.checkBoxYuanJian.ForeColor = System.Drawing.Color.Black;
			resources.ApplyResources(this.checkBoxYuanJian, "checkBoxYuanJian");
			this.checkBoxYuanJian.Name = "checkBoxYuanJian";
			this.checkBoxYuanJian.UseVisualStyleBackColor = true;
			// 
			// panel5
			// 
			this.panel5.BackColor = System.Drawing.Color.Silver;
			this.panel5.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
			this.panel5.Controls.Add(this.richTextBoxSim);
			this.panel5.Controls.Add(this.buttonSetSimValue);
			resources.ApplyResources(this.panel5, "panel5");
			this.panel5.Name = "panel5";
			// 
			// richTextBoxSim
			// 
			this.richTextBoxSim.BorderStyle = System.Windows.Forms.BorderStyle.None;
			resources.ApplyResources(this.richTextBoxSim, "richTextBoxSim");
			this.richTextBoxSim.Name = "richTextBoxSim";
			this.richTextBoxSim.TextChanged += new System.EventHandler(this.RichTextBoxSimTextChanged);
			// 
			// buttonSetSimValue
			// 
			this.buttonSetSimValue.BackColor = System.Drawing.Color.Orange;
			resources.ApplyResources(this.buttonSetSimValue, "buttonSetSimValue");
			this.buttonSetSimValue.Name = "buttonSetSimValue";
			this.buttonSetSimValue.UseVisualStyleBackColor = false;
			this.buttonSetSimValue.Click += new System.EventHandler(this.ButtonSetSimValueClick);
			// 
			// 组件名称textBox
			// 
			resources.ApplyResources(this.组件名称textBox, "组件名称textBox");
			this.组件名称textBox.Name = "组件名称textBox";
			this.组件名称textBox.TextChanged += new System.EventHandler(this.组件名称textBoxTextChanged);
			// 
			// buttonScale
			// 
			this.buttonScale.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(160)))), ((int)(((byte)(130)))), ((int)(((byte)(100)))));
			resources.ApplyResources(this.buttonScale, "buttonScale");
			this.buttonScale.ForeColor = System.Drawing.Color.White;
			this.buttonScale.Name = "buttonScale";
			this.buttonScale.UseVisualStyleBackColor = false;
			this.buttonScale.Click += new System.EventHandler(this.ButtonScaleClick);
			// 
			// textBoxScale
			// 
			resources.ApplyResources(this.textBoxScale, "textBoxScale");
			this.textBoxScale.Name = "textBoxScale";
			this.textBoxScale.DoubleClick += new System.EventHandler(this.TextBoxScaleDoubleClick);
			// 
			// checkBox通信通道
			// 
			this.checkBox通信通道.ForeColor = System.Drawing.Color.Black;
			resources.ApplyResources(this.checkBox通信通道, "checkBox通信通道");
			this.checkBox通信通道.Name = "checkBox通信通道";
			this.checkBox通信通道.UseVisualStyleBackColor = true;
			// 
			// checkBox控制器组件
			// 
			this.checkBox控制器组件.ForeColor = System.Drawing.Color.Black;
			resources.ApplyResources(this.checkBox控制器组件, "checkBox控制器组件");
			this.checkBox控制器组件.Name = "checkBox控制器组件";
			this.checkBox控制器组件.UseVisualStyleBackColor = true;
			// 
			// 保存当前组件button
			// 
			this.保存当前组件button.BackColor = System.Drawing.Color.CornflowerBlue;
			resources.ApplyResources(this.保存当前组件button, "保存当前组件button");
			this.保存当前组件button.ForeColor = System.Drawing.Color.White;
			this.保存当前组件button.Name = "保存当前组件button";
			this.保存当前组件button.UseVisualStyleBackColor = false;
			this.保存当前组件button.Click += new System.EventHandler(this.保存当前组件buttonClick);
			this.保存当前组件button.MouseDown += new System.Windows.Forms.MouseEventHandler(this.保存当前组件buttonMouseDown);
			// 
			// 打开组件文件button
			// 
			this.打开组件文件button.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(160)))), ((int)(((byte)(130)))), ((int)(((byte)(100)))));
			resources.ApplyResources(this.打开组件文件button, "打开组件文件button");
			this.打开组件文件button.ForeColor = System.Drawing.Color.White;
			this.打开组件文件button.Name = "打开组件文件button";
			this.打开组件文件button.UseVisualStyleBackColor = false;
			this.打开组件文件button.Click += new System.EventHandler(this.打开组件文件buttonClick);
			// 
			// panel2
			// 
			this.panel2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(108)))), ((int)(((byte)(171)))), ((int)(((byte)(176)))));
			this.panel2.Controls.Add(this.buttonNote);
			this.panel2.Controls.Add(this.添加端口button);
			this.panel2.Controls.Add(this.textBoxPortExt);
			this.panel2.Controls.Add(this.comboBox端口功能名称);
			this.panel2.Controls.Add(this.textBox名称);
			this.panel2.Controls.Add(this.comboBox导线方向);
			this.panel2.Controls.Add(this.comboBoxMasOrCli);
			this.panel2.Controls.Add(this.buttonRestore);
			this.panel2.Controls.Add(this.buttonDelete);
			this.panel2.Controls.Add(this.buttonSetNumber);
			this.panel2.Controls.Add(this.label7);
			this.panel2.Controls.Add(this.label3);
			this.panel2.Controls.Add(this.label10);
			this.panel2.Controls.Add(this.label12);
			this.panel2.Controls.Add(this.label9);
			resources.ApplyResources(this.panel2, "panel2");
			this.panel2.Name = "panel2";
			// 
			// buttonNote
			// 
			this.buttonNote.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
			resources.ApplyResources(this.buttonNote, "buttonNote");
			this.buttonNote.ForeColor = System.Drawing.Color.Black;
			this.buttonNote.Name = "buttonNote";
			this.buttonNote.UseVisualStyleBackColor = false;
			this.buttonNote.Click += new System.EventHandler(this.ButtonNoteClick);
			// 
			// 添加端口button
			// 
			this.添加端口button.BackColor = System.Drawing.Color.Teal;
			resources.ApplyResources(this.添加端口button, "添加端口button");
			this.添加端口button.ForeColor = System.Drawing.Color.White;
			this.添加端口button.Name = "添加端口button";
			this.添加端口button.UseVisualStyleBackColor = false;
			this.添加端口button.Click += new System.EventHandler(this.添加端口buttonClick);
			// 
			// textBoxPortExt
			// 
			resources.ApplyResources(this.textBoxPortExt, "textBoxPortExt");
			this.textBoxPortExt.Name = "textBoxPortExt";
			// 
			// comboBox端口功能名称
			// 
			resources.ApplyResources(this.comboBox端口功能名称, "comboBox端口功能名称");
			this.comboBox端口功能名称.FormattingEnabled = true;
			this.comboBox端口功能名称.Items.AddRange(new object[] {
									resources.GetString("comboBox端口功能名称.Items"),
									resources.GetString("comboBox端口功能名称.Items1"),
									resources.GetString("comboBox端口功能名称.Items2"),
									resources.GetString("comboBox端口功能名称.Items3"),
									resources.GetString("comboBox端口功能名称.Items4"),
									resources.GetString("comboBox端口功能名称.Items5"),
									resources.GetString("comboBox端口功能名称.Items6"),
									resources.GetString("comboBox端口功能名称.Items7"),
									resources.GetString("comboBox端口功能名称.Items8"),
									resources.GetString("comboBox端口功能名称.Items9"),
									resources.GetString("comboBox端口功能名称.Items10"),
									resources.GetString("comboBox端口功能名称.Items11"),
									resources.GetString("comboBox端口功能名称.Items12"),
									resources.GetString("comboBox端口功能名称.Items13"),
									resources.GetString("comboBox端口功能名称.Items14"),
									resources.GetString("comboBox端口功能名称.Items15"),
									resources.GetString("comboBox端口功能名称.Items16"),
									resources.GetString("comboBox端口功能名称.Items17"),
									resources.GetString("comboBox端口功能名称.Items18"),
									resources.GetString("comboBox端口功能名称.Items19"),
									resources.GetString("comboBox端口功能名称.Items20"),
									resources.GetString("comboBox端口功能名称.Items21"),
									resources.GetString("comboBox端口功能名称.Items22"),
									resources.GetString("comboBox端口功能名称.Items23"),
									resources.GetString("comboBox端口功能名称.Items24"),
									resources.GetString("comboBox端口功能名称.Items25"),
									resources.GetString("comboBox端口功能名称.Items26"),
									resources.GetString("comboBox端口功能名称.Items27"),
									resources.GetString("comboBox端口功能名称.Items28"),
									resources.GetString("comboBox端口功能名称.Items29"),
									resources.GetString("comboBox端口功能名称.Items30"),
									resources.GetString("comboBox端口功能名称.Items31"),
									resources.GetString("comboBox端口功能名称.Items32"),
									resources.GetString("comboBox端口功能名称.Items33"),
									resources.GetString("comboBox端口功能名称.Items34"),
									resources.GetString("comboBox端口功能名称.Items35"),
									resources.GetString("comboBox端口功能名称.Items36"),
									resources.GetString("comboBox端口功能名称.Items37"),
									resources.GetString("comboBox端口功能名称.Items38"),
									resources.GetString("comboBox端口功能名称.Items39"),
									resources.GetString("comboBox端口功能名称.Items40"),
									resources.GetString("comboBox端口功能名称.Items41"),
									resources.GetString("comboBox端口功能名称.Items42"),
									resources.GetString("comboBox端口功能名称.Items43"),
									resources.GetString("comboBox端口功能名称.Items44"),
									resources.GetString("comboBox端口功能名称.Items45"),
									resources.GetString("comboBox端口功能名称.Items46"),
									resources.GetString("comboBox端口功能名称.Items47"),
									resources.GetString("comboBox端口功能名称.Items48"),
									resources.GetString("comboBox端口功能名称.Items49")});
			this.comboBox端口功能名称.Name = "comboBox端口功能名称";
			// 
			// textBox名称
			// 
			resources.ApplyResources(this.textBox名称, "textBox名称");
			this.textBox名称.Name = "textBox名称";
			this.textBox名称.TextChanged += new System.EventHandler(this.TextBox名称TextChanged);
			// 
			// comboBox导线方向
			// 
			this.comboBox导线方向.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
			resources.ApplyResources(this.comboBox导线方向, "comboBox导线方向");
			this.comboBox导线方向.FormattingEnabled = true;
			this.comboBox导线方向.Items.AddRange(new object[] {
									resources.GetString("comboBox导线方向.Items"),
									resources.GetString("comboBox导线方向.Items1"),
									resources.GetString("comboBox导线方向.Items2"),
									resources.GetString("comboBox导线方向.Items3"),
									resources.GetString("comboBox导线方向.Items4")});
			this.comboBox导线方向.Name = "comboBox导线方向";
			// 
			// comboBoxMasOrCli
			// 
			this.comboBoxMasOrCli.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
			resources.ApplyResources(this.comboBoxMasOrCli, "comboBoxMasOrCli");
			this.comboBoxMasOrCli.FormattingEnabled = true;
			this.comboBoxMasOrCli.Items.AddRange(new object[] {
									resources.GetString("comboBoxMasOrCli.Items"),
									resources.GetString("comboBoxMasOrCli.Items1")});
			this.comboBoxMasOrCli.Name = "comboBoxMasOrCli";
			// 
			// buttonRestore
			// 
			this.buttonRestore.BackColor = System.Drawing.Color.LightSeaGreen;
			resources.ApplyResources(this.buttonRestore, "buttonRestore");
			this.buttonRestore.ForeColor = System.Drawing.Color.White;
			this.buttonRestore.Name = "buttonRestore";
			this.buttonRestore.UseVisualStyleBackColor = false;
			this.buttonRestore.Click += new System.EventHandler(this.ButtonRestoreClick);
			// 
			// buttonDelete
			// 
			this.buttonDelete.BackColor = System.Drawing.Color.Sienna;
			resources.ApplyResources(this.buttonDelete, "buttonDelete");
			this.buttonDelete.ForeColor = System.Drawing.Color.White;
			this.buttonDelete.Name = "buttonDelete";
			this.buttonDelete.UseVisualStyleBackColor = false;
			this.buttonDelete.Click += new System.EventHandler(this.ButtonDeleteClick);
			// 
			// buttonSetNumber
			// 
			this.buttonSetNumber.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
			resources.ApplyResources(this.buttonSetNumber, "buttonSetNumber");
			this.buttonSetNumber.ForeColor = System.Drawing.Color.Black;
			this.buttonSetNumber.Name = "buttonSetNumber";
			this.buttonSetNumber.UseVisualStyleBackColor = false;
			this.buttonSetNumber.Click += new System.EventHandler(this.ButtonSetNumberClick);
			// 
			// label7
			// 
			resources.ApplyResources(this.label7, "label7");
			this.label7.ForeColor = System.Drawing.Color.Black;
			this.label7.Name = "label7";
			// 
			// label3
			// 
			resources.ApplyResources(this.label3, "label3");
			this.label3.ForeColor = System.Drawing.Color.Black;
			this.label3.Name = "label3";
			// 
			// label10
			// 
			resources.ApplyResources(this.label10, "label10");
			this.label10.ForeColor = System.Drawing.Color.Black;
			this.label10.Name = "label10";
			// 
			// label12
			// 
			resources.ApplyResources(this.label12, "label12");
			this.label12.ForeColor = System.Drawing.Color.Black;
			this.label12.Name = "label12";
			// 
			// label9
			// 
			resources.ApplyResources(this.label9, "label9");
			this.label9.ForeColor = System.Drawing.Color.Black;
			this.label9.Name = "label9";
			// 
			// 导出组件button
			// 
			this.导出组件button.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(194)))), ((int)(((byte)(165)))), ((int)(((byte)(131)))));
			resources.ApplyResources(this.导出组件button, "导出组件button");
			this.导出组件button.ForeColor = System.Drawing.Color.White;
			this.导出组件button.Name = "导出组件button";
			this.导出组件button.UseVisualStyleBackColor = false;
			this.导出组件button.Click += new System.EventHandler(this.导出组件buttonClick);
			// 
			// label1
			// 
			resources.ApplyResources(this.label1, "label1");
			this.label1.ForeColor = System.Drawing.Color.Black;
			this.label1.Name = "label1";
			// 
			// 打开图片文件button
			// 
			this.打开图片文件button.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(160)))), ((int)(((byte)(130)))), ((int)(((byte)(100)))));
			resources.ApplyResources(this.打开图片文件button, "打开图片文件button");
			this.打开图片文件button.ForeColor = System.Drawing.Color.White;
			this.打开图片文件button.Name = "打开图片文件button";
			this.打开图片文件button.UseVisualStyleBackColor = false;
			this.打开图片文件button.Click += new System.EventHandler(this.打开图片文件buttonClick);
			// 
			// checkBoxAllMove
			// 
			this.checkBoxAllMove.ForeColor = System.Drawing.Color.Gray;
			resources.ApplyResources(this.checkBoxAllMove, "checkBoxAllMove");
			this.checkBoxAllMove.Name = "checkBoxAllMove";
			this.checkBoxAllMove.UseVisualStyleBackColor = true;
			// 
			// checkBoxRemo
			// 
			this.checkBoxRemo.ForeColor = System.Drawing.Color.Black;
			resources.ApplyResources(this.checkBoxRemo, "checkBoxRemo");
			this.checkBoxRemo.Name = "checkBoxRemo";
			this.checkBoxRemo.UseVisualStyleBackColor = true;
			// 
			// label11
			// 
			resources.ApplyResources(this.label11, "label11");
			this.label11.ForeColor = System.Drawing.Color.Black;
			this.label11.Name = "label11";
			// 
			// panel4
			// 
			resources.ApplyResources(this.panel4, "panel4");
			this.panel4.Name = "panel4";
			// 
			// ModuleForm
			// 
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None;
			resources.ApplyResources(this, "$this");
			this.BackColor = System.Drawing.Color.White;
			this.Controls.Add(this.panel4);
			this.Controls.Add(this.panel1);
			this.Name = "ModuleForm";
			this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.ModuleFormClosing);
			this.panel1.ResumeLayout(false);
			this.panel1.PerformLayout();
			this.panel5.ResumeLayout(false);
			this.panel2.ResumeLayout(false);
			this.panel2.PerformLayout();
			this.ResumeLayout(false);
		}
		private System.Windows.Forms.Label label17;
		private System.Windows.Forms.Button buttonNote;
		private System.Windows.Forms.Label label16;
		private System.Windows.Forms.Label label15;
		private System.Windows.Forms.Button buttonB2;
		private System.Windows.Forms.Label label14;
		private System.Windows.Forms.Button buttonD1;
		private System.Windows.Forms.Label label13;
		private System.Windows.Forms.TextBox textBoxEX;
		private System.Windows.Forms.Button buttonA0;
		private System.Windows.Forms.Label label11;
		private System.Windows.Forms.Button buttonSetNumber;
		private System.Windows.Forms.Label label8;
		private System.Windows.Forms.TextBox textBoxMesFile;
		private System.Windows.Forms.Label label6;
		public System.Windows.Forms.TextBox textBoxType;
		private System.Windows.Forms.TextBox textBoxImgPath;
		public System.Windows.Forms.CheckBox checkBox254;
		private System.Windows.Forms.Label label7;
		public System.Windows.Forms.TextBox textBoxPortExt;
		public System.Windows.Forms.TextBox textBoxConfig;
		private System.Windows.Forms.Label label5;
		public System.Windows.Forms.TextBox textBoxDriverPath;
		private System.Windows.Forms.Label label2;
		public System.Windows.Forms.TextBox textBoxUserName;
		private System.Windows.Forms.Label label4;
		public System.Windows.Forms.TextBox textBoxImageList;
		private System.Windows.Forms.CheckBox checkBoxCanSwap;
		public System.Windows.Forms.CheckBox checkBoxAllMove;
		private System.Windows.Forms.CheckBox checkBoxYuanJian;
		private System.Windows.Forms.RichTextBox richTextBoxSim;
		private System.Windows.Forms.Button buttonSetSimValue;
		private System.Windows.Forms.Panel panel5;
		private System.Windows.Forms.Button buttonDelete;
		private System.Windows.Forms.Button buttonRestore;
		private System.Windows.Forms.Button buttonScale;
		private System.Windows.Forms.Panel panel4;
		private System.Windows.Forms.CheckBox checkBoxRemo;
		public System.Windows.Forms.ComboBox comboBoxMasOrCli;
		public System.Windows.Forms.TextBox textBox名称;
		public System.Windows.Forms.ComboBox comboBox导线方向;
		private System.Windows.Forms.Label label12;
		private System.Windows.Forms.Label label10;
		private System.Windows.Forms.Label label9;
		private System.Windows.Forms.CheckBox checkBox通信通道;
		private System.Windows.Forms.TextBox textBoxScale;
		private System.Windows.Forms.CheckBox checkBox控制器组件;
		private System.Windows.Forms.Button 保存当前组件button;
		private System.Windows.Forms.Button 打开组件文件button;
		public System.Windows.Forms.ComboBox comboBox端口功能名称;
		private System.Windows.Forms.Button 添加端口button;
		private System.Windows.Forms.Panel panel2;
		private System.Windows.Forms.Label label3;
		private System.Windows.Forms.TextBox 组件名称textBox;
		private System.Windows.Forms.Button 导出组件button;
		private System.Windows.Forms.Label label1;
		private System.Windows.Forms.Button 打开图片文件button;
		private System.Windows.Forms.Panel panel1;
	}
}
