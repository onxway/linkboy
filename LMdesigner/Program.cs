﻿
using System;
using System.Windows.Forms;
using n_ModuleForm;
using n_FileType;

namespace n_Main
{
internal sealed class Program
{
	public static ModuleForm ModuleBox;
	
	[STAThread]
	private static void Main(string[] args)
	{
		/** 
		 * 当前用户是管理员的时候，直接启动应用程序
		 * 如果不是管理员，则使用启动对象启动程序，以确保使用管理员身份运行
		 */
		//获得当前登录的Windows用户标示
		System.Security.Principal.WindowsIdentity identity = System.Security.Principal.WindowsIdentity.GetCurrent();
		System.Security.Principal.WindowsPrincipal principal = new System.Security.Principal.WindowsPrincipal(identity);
		//判断当前登录用户是否为管理员
		if (principal.IsInRole(System.Security.Principal.WindowsBuiltInRole.Administrator)) {
			//如果是管理员，则直接运行
			
			Application.EnableVisualStyles();
			Application.SetCompatibleTextRenderingDefault(false);
			
			//初始化底层移植库
			n_OS.OS.Init();
			
			n_SG.SG.Init();
			
			n_GUIcoder.ResourceList.Init();
			
			string DefaultFilePath = null;
			
			//判断是否加载文件
			DefaultFilePath = p_Win.CommandLine.GetStartPath( args );
			
			ExtNameBind.FileExternNameReg_M();
			
			ModuleBox = new ModuleForm( DefaultFilePath );
			Application.Run( ModuleBox );
		}
		else {
			
			//创建启动对象
			System.Diagnostics.ProcessStartInfo startInfo = new System.Diagnostics.ProcessStartInfo();
			//设置运行文件
			startInfo.FileName = System.Windows.Forms.Application.ExecutablePath;
			//设置启动参数
			startInfo.Arguments = String.Join(" ", args);
			//设置启动动作,确保以管理员身份运行
			startInfo.Verb = "runas";
			//如果不是管理员，则启动UAC
			System.Diagnostics.Process.Start(startInfo);
			//退出
			System.Windows.Forms.Application.Exit();
		}
	}
}
}


